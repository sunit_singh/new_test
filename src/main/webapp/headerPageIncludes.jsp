<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>
<%@page import="java.util.List"%>

<%
String sContentPageName = "";

sContentPageName = (String) request.getAttribute(com.sciformix.sciportal.web.WebConstants.REQUEST_PARAMETER_CONTENT_PAGE_NAME);
if (sContentPageName == null)
{
	sContentPageName = "contentDefaultPage.jsp";
}
sContentPageName = "/WEB-INF/pages/" + sContentPageName;

List<AppInfo> listApps = (List<AppInfo>) request.getSession().getAttribute("listBusinessApps");
boolean bFirstApp = false;
AppInfo oAppInfoUserPrefs = null;
AppInfo oAppInfoAppSettings = null;
AppInfo oAppInfoProjSettings = null;
boolean bIsSystemAdminUser = false;
boolean bIsProjectAdminUser = false;
UserSession oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
UserInfo oSessionUser = (oUserSession != null ? oUserSession.getUserInfo() : null);
if (oSessionUser == null)
{
	response.sendRedirect(WebUtils.getSessionExpiredPageUri());
}
else
{
	oAppInfoUserPrefs = AppRegistry.getUserPrefsApp();
	oAppInfoAppSettings = AppRegistry.getAppSettingsApp();
	oAppInfoProjSettings = AppRegistry.getProjSettingsApp();
	bIsSystemAdminUser = oSessionUser.isSystemAdmin();
	bIsProjectAdminUser = oSessionUser.isProjectAdmin();
}

String sUserName = (oSessionUser!=null?oSessionUser.getUserDisplayName():"Anonymous");
String sUserPreferencesUrl = (oAppInfoUserPrefs != null ? "LandingServlet?portal_appid=" + oAppInfoUserPrefs.getAppId() : "");
String sLogoutUrl = "LoginServlet?logoutRequest=yes";
%>
