
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<% 
response.setHeader("Cache-Control","no-cache, no-store, must-revalidate"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires", -1); 
response.setHeader("Expires","0");
%> 

<%
UserSession g_oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
UserInfo g_oSessionUser = (g_oUserSession != null ? g_oUserSession.getUserInfo() : null);
if (g_oSessionUser == null)
{
	response.sendRedirect(WebUtils.getSessionExpiredPageUri());
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
	<link rel="shortcut icon" href="favicon.ico">
	<script src="js/jquery.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sciformix Safety Portal</title>

	<%@include file="cssPageIncludes.jsp"%>
	<script type="text/javascript">
		$(document).ready(function() {
			$('input[type="text"]').keydown(function(event){
			    if(event.keyCode == 13) {
			      event.preventDefault();
			      return false;
			    }
		  	});
		});
	
	</script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col customPositionFixed">
          <div class="left_col scroll-view">
            <!-- sidebar title and menu -->
            <%@include file="sidebarTitleAndMenu.jsp"%>
            <!-- sidebar title and menu -->
           </div>
        </div>

        <!-- top navigation -->
        <%@include file="topNavigationBar.jsp"%>
        <!-- /top navigation -->

        <!-- page content -->
		<%
		String g_sContentPageName = (String) request.getAttribute(WebConstants.REQUEST_PARAMETER_CONTENT_PAGE_NAME);
		g_sContentPageName = (g_sContentPageName != null ? g_sContentPageName : "contentDefaultPage.jsp");
		g_sContentPageName = "/WEB-INF/pages/" + g_sContentPageName;
		%>
        <div class="right_col" role="main">
        	<%-- <% request.setAttribute("_jsp_jspPageObject", WebConstants.REQUEST_PARAMETER_CONTENT_PAGE_NAME); %> --%>
       		<%@include file="../../validator.jsp" %>
        	<%@include file="errorPage.jsp" %>
        	<jsp:include page="<%= g_sContentPageName %>" flush="true" />
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <%@include file="footerPageIncludes.jsp"%>
        <!-- /footer content -->
      </div>
    </div>

	<%@include file="jsPageIncludes.jsp"%>
  </body>
</html>
