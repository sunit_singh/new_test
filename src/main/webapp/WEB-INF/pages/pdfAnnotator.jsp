<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvPortalApp.AppConfig"%>
<%@page import="org.apache.wink.json4j.JSONArray"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.io.*,java.util.*"%>
<%@page import="com.sciformix.commons.SciError"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="org.apache.wink.json4j.JSONArray"%>
 <%@page import="com.sciformix.commons.utils.*"%>
 <%@page import="java.util.regex.Matcher" %>
 <%@page import="java.util.regex.Pattern" %>
 <%@page import="com.sciformix.commons.utils.DataTable" %>

<!DOCTYPE html >
<%
String sNewPortalAppId = (String) request.getAttribute("new-portalappid");
String sNewPortalAction = (String) request.getAttribute("new-portalaction");
String sDownloadFilePath = (String) request.getSession().getAttribute("s-downloadFile-path");
String highlightedDownloadFilePath=(String) request.getSession().getAttribute("highlighted-downloadFile-path");
String[] arrSourceDocTypes = ((String) request.getAttribute("r_drop_down_values")).split(",");
String[] dropDownForHighlighter=((String) request.getAttribute("annotation_style_drop_down")).split(",");
String parsedText= (String) request.getSession().getAttribute("parsedData");
String highlightedText= (String) request.getSession().getAttribute("highlightedText");
 int downLoadCount = 1;
File file = new File(sDownloadFilePath!=null?sDownloadFilePath:"");
File highlightedFile=new File(highlightedDownloadFilePath!=null?highlightedDownloadFilePath:"");
%>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">


</style>
<script>
	<%@include file="../js/error.js" %>
</script>
</head>
<script type="text/javascript">

var annotateClick_counter=0;	
	
	function validateFormForHighlighter() 
	{
		clearErrorMessages();
		var fileName=document.getElementById("file_for_highlight").value;
		if(fileName=="")
		{
			displayErrorMessage("UI-1004");
			bErrorDetected = true;
		}
		else if(!fileName.endsWith(".pdf"))
		{
			 displayErrorMessage("UI-1089");
    			bErrorDetected = true;
    			document.getElementById('file_for_highlight').value="";
		} 
		else
		{
			document.frmmain.portal_action.value	=	"highlightFile";
			if(validateFormParameters())
			{  
				if (annotateClick_counter>0){return false;} //you can set the number to any
			    //your call
			     annotateClick_counter++; //incremental
				document.frmmain.submit();
			}
		}
	}
	function validateFormParameters () {
		clearErrorMessages();
		oFormObject = document.forms["frmmain"];
		return validateInputParameter(oFormObject);
	}
	
	function submitForm(actionValue, link) 
	{
		document.frmmain.portal_action.value	= actionValue;
		//document.frmmain.submit();
		  if(validateFormParameters()){  
			 document.frmmain.submit();
			  } 
		// Disable after one click
		link.onclick = function(event) 
		{
			//event.preventDefault();
	        //event.preventDefault();
	        //clearErrorMessages();
	       // displayErrorMessage("UI-1083");
	    }
	}
</script>
<body>
<%@include file="uiFormCreationMethods.jsp"%>
<csrf:form name="frmmain" id="frmmain" class="form-horizontal"
		method="post" action="LandingServlet" enctype="multipart/form-data">
		<input type="hidden" id="portal_appid" name="portal_appid" value="SOURCE_DOC_UTILS_PORTAL_APP">
			<input type="hidden" id="portal_action" name="portal_action" value="">
		
	<%if(sNewPortalAction == null)
	{	
	%>		

		<!-- this is for Highlighter -->
		<div class="title_left">
			<h3><%=AppRegistry.getToolsApp().getAppName() %> - Annotator</h3>
			
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="row">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
							Source Document Type  * </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div>
								
								<select class="form-control" id="Annotation Style"
									name="Annotation Style">
								<%for(int nIndex = 0 ; nIndex < dropDownForHighlighter.length ; nIndex++){ %>	
									<option value="Case Id annotator"><%=dropDownForHighlighter[nIndex] %></option>				
									<%} %>					
								</select>

							</div>
						</div>
					</div>
			</div>
			<div class="row">

						<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="casedata_file_parse">
										Upload Source Document
										<span class="required"> *</span>
									</label>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input type="file" name="file_for_highlight" id="file_for_highlight">
									</div>
								</div>		
			</div>
			<div class="row">
					
						<div 
								style="display:inline;padding-left: 280px;">

								<button type="button" class="btn btn-success" value="Back"
									onclick="submitForm('') ;">Back</button>

							</div>
						
							<div
								style="display: inline; ">

								<button type="button" class="btn btn-success" value="Highlight"
									onclick="javascript:validateFormForHighlighter() ;">Annotate</button>

							</div>
					
			</div>
			
		</div>
		</div>
		
		
	<%}else if(sNewPortalAction == "showHighlightedFile"){ %>
		<div class="title_left">
			<h3><%=AppRegistry.getToolsApp().getAppName() %> - Annotator</h3>
		</div>
		
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				Download: <A HREF="javascript:submitForm('download_split_file' , this);"><u> <%= file.getName()%></u><!-- <i class="glyphicon glyphicon-save-file"></i> --></A>
			</div>
			</div>
			
		<div>
		<br>
		<br>
		<%=highlightedText%>
		</div>
		
		
		<div class="row">

						<div class="col-sm-12">
							<div class="x_content"
								style="display: block; padding-left: 380px;">

								<button type="button" class="btn btn-success" value="Next File"
									onclick="submitForm('pdfAnnotator.jsp') ;">Next File</button>

							</div>
						</div>
			</div>
	

	
	<%} %>
</csrf:form>
</body>
</html>
