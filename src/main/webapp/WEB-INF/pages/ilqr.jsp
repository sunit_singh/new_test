<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRTemplate"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.sciformix.sciportal.pvcp.PvcpCaseMaster"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDBCaseFieldCategoryType"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.List" %>
<%@page import="com.sciformix.sciportal.user.UserInfo" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFields"%>
<%@page import="com.sciformix.commons.ObjectIdPair" %>

<%
	Map<String, ArrayList<SafetyDbCaseFields>>	mapQualityReviewTabData = null;
	//Map<String,Integer>							l_fieldcatmap			= null;
	SafetyDbCaseFieldsTemplate					l_scft					= null;
	List<UserInfo>								l_listusers				= null;		
	String 										l_reviewtype			= null,
												l_layouttype			= null;	
	PvcpCaseMaster								o_pvcpcasemaster		= null;
	QRTemplate									o_orttemplate			= null;
	Map<String,String>							l_errortypes			= null;
	Map<String,List<String>>					l_enableUdfMap			= null;
	
	
	int reviewround			=	(Integer)request.getAttribute("reviewround");
	String projUdfPropDelm	=	":";
	l_enableUdfMap 	 		= 	(Map<String,List<String>>)request.getAttribute("enableUdfList");
	l_scft					=	(SafetyDbCaseFieldsTemplate) request.getAttribute("fldactivesafetydbtemplate");
	l_listusers				=	(List<UserInfo>) request.getAttribute("list_of_users");
	l_reviewtype			=	(String)	request.getAttribute("fldreviewtype");
	l_layouttype			=	(String)	request.getAttribute("UserLayoutType");
	mapQualityReviewTabData	=	(Map<String, ArrayList<SafetyDbCaseFields>>) request.getAttribute("fldsafetydbcasefields");
	o_pvcpcasemaster 		= 	(PvcpCaseMaster) request.getAttribute("pvcp_case_record");
	l_errortypes			=	(Map<String,String>) request.getAttribute("errortypes");
	o_orttemplate			=	(QRTemplate) request.getAttribute("qrttemplate");
	
	
	int		l_totalfields	=	0,
			l_cat1total		=	0,
			l_cat2total		=	0,
			l_cat3total		=	0,
			l_cat4total		=	0,
			l_cat5total		=	0,
			l_udfDisplayNameIndex = 1,
			l_udfDatatypeIndex = 2;
	String[] udfPropValue 		= null;
	String[] udfPropSepValue	= null;
	for (String l_tabname : mapQualityReviewTabData.keySet()) { 
		l_totalfields	=	l_totalfields + mapQualityReviewTabData.get(l_tabname).size();
	}	
	String []	l_categories	=	null;
	boolean	l_ismrnameshow	=	(Boolean)	request.getAttribute("ismrnameshow");
	String	l_cataliases	=	(String) 	request.getAttribute("categoryaliases");	
	int		l_catcount		=	0;
	
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
	List<String>	l_udfvaluelist	=	new ArrayList<String>(); 
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf1());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf2());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf3());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf4());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf5());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf6());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf7());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf8());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf9());
	l_udfvaluelist.add(o_pvcpcasemaster.getUdf10());
	
%>

<html> 
	<head>
		<link rel="stylesheet" href="css/SciCustom.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.nonblock.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.brighttheme.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.buttons.css"> 
		<link rel="stylesheet" href="css/pnotify/pnotify.material.css">
		
		<script src="js/jquery.js"></script>
		<script src="js/pnotify/pnotify.js"></script>
		<script src="js/pnotify/pnotify.nonblock.js"></script>
		<script src="js/pnotify/pnotify.buttons.js"></script>
		<script src="js/pnotify/pnotify.confirm.js"></script>
		<script src="js/pnotify/pnotify.reference.js"></script>
		<script src="js/moment/moment.min.js"></script>
		<script>
			var statisticsShown		=	false;
			var	totalFields			=	<%= l_totalfields%>;
			var	fieldsReviewed		=	0;
			var	fieldsWithoutError	=	0;
			var	fieldsMissing		=	0;
			var	fieldsIncorrect		=	0;
			var	fieldsCorrected		=	0;
			var cat1count			=	0;
			var cat2count			=	0;
			var cat3count			=	0;
			var cat1reviewcount		=	0;
			var cat2reviewcount		=	0;
			var cat3reviewcount		=	0;
			var fieldsDictionary	=	[];
			var fieldCatDictionary	=	[];
			var	displayText			=	"<div class='x_content'><table class='table'><tbody>"
									+	"<tr><td><label>Fields Reviewed</label></td><td><label id='fieldsreviewed'> fieldsReviewedreplace/totalFieldsreplace</label></td></tr>"
									/* +	"<tr><td><label>Fields Without Error</label></td><td><label id='fieldswithouterror'>fieldsWithoutErrorreplace/fieldsReviewedreplace</label></td></tr>"
									+	"<tr><td><label>Fields Incorrect</label></td><td><label id='fieldsincorrect'>fieldsIncorrectreplace/fieldsReviewedreplace</label></td></tr>" */
									+	"<tr><td><label>Case Quality</label></td><td><label id='casequality'>caseequalityreplace%</label></td></tr>"
									/*+	"<tr><td><label>Case Result</label></td><td><label id='caseresult'>"+"caseresultreplace"+"</label></td></tr>"
									+	"<tr><td><label>Cat1 Quality</label></td><td><label id='cat1quality'>cat1qualityreplace%</label></td></tr>"
									+	"<tr><td><label>Cat2 Quality</label></td><td><label id='cat2quality'>cat2qualityreplace%</label></td></tr>"
									+	"<tr><td><label>Cat3 Quality</label></td><td><label id='cat3quality'>cat3qualityreplace%</label></td></tr>" */
									+	"</tbody></table></div>"
									; 
			var displayStatText;
			var i	=	0;
			var notice;
			$(window).load(function(){
				$('.btnNext').click(function(){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				});
				$('.btnPrevious').click(function(){
					$('.nav-tabs > .active').prev('li').find('a').trigger('click');
				});
				$('[data-toggle="tooltip"]').tooltip();
				replaceString();
			});
			
			function replaceString () {
				displayStatText	=	displayText;
				displayStatText	=	displayStatText.replace(/fieldsReviewedreplace/g,fieldsReviewed);
				displayStatText	=	displayStatText.replace("totalFieldsreplace",totalFields);
				/* displayStatText	=	displayStatText.replace("fieldsWithoutErrorreplace",fieldsWithoutError);
				displayStatText	=	displayStatText.replace("fieldsIncorrectreplace",fieldsIncorrect); */
				
				/* if (cat1reviewcount == 0) {
					displayStatText	=	displayStatText.replace("cat1qualityreplace%","X");
				} else {
					if (cat1count == 0) {
						displayStatText	=	displayStatText.replace("cat1qualityreplace%",0);
					} else {
						displayStatText	=	displayStatText.replace("cat1qualityreplace",((cat1count*100)/(cat1reviewcount)).toFixed(2));
					}
				}
				if (cat2reviewcount == 0) {
					displayStatText	=	displayStatText.replace("cat2qualityreplace%","X");
				} else {
					if (cat2count == 0) {
						displayStatText	=	displayStatText.replace("cat2qualityreplace%",0);
					} else {
						displayStatText	=	displayStatText.replace("cat2qualityreplace",((cat2count*100)/(cat2reviewcount)).toFixed(2));
					}
				}
				if (cat3reviewcount == 0) {
					displayStatText	=	displayStatText.replace("cat3qualityreplace%","X");
				} else {
					if (cat3count == 0) {
						displayStatText	=	displayStatText.replace("cat3qualityreplace%",0);
					} else {
						displayStatText	=	displayStatText.replace("cat3qualityreplace",((cat3count*100)/(cat3reviewcount)).toFixed(2));
					}
				} */
				if (fieldsReviewed > 0) {
					if (fieldsWithoutError == 0) {
						displayStatText	=	displayStatText.replace("caseequalityreplace",0);
					} else {
						var	caseequalityreplace	=	((fieldsWithoutError*100)/(fieldsReviewed)).toFixed(2);
						displayStatText	=	displayStatText.replace("caseequalityreplace",caseequalityreplace);
						
					}
					/* if (fieldsIncorrect > 0) {
						displayStatText	=	displayStatText.replace("caseresultreplace","Failed");
					} else {
						displayStatText	=	displayStatText.replace("caseresultreplace","Passed");
					} */
				} else {
					/* displayStatText	=	displayStatText.replace("caseresultreplace",""); */
					displayStatText	=	displayStatText.replace("caseequalityreplace%","X");
				}
			}
			/* function notApplicableDisabled (currObject) 
			{
				var	fieldName	=	currObject;
				if (document.getElementById(fieldName+"togglecheckbox").checked) 
				{
					document.getElementById(fieldName+"comment").readOnly = false;
				} else {
					if(fieldsDictionary[fieldName] === undefined ) {
						
					} else {
						if (fieldsDictionary[fieldName] == 2) {
							fieldsMissing--;
							fieldsReviewed--;
							document.getElementById(fieldName+"2").checked	=	false;
						} else if (fieldsDictionary[fieldName] == 3) {
							fieldsIncorrect--;
							fieldsReviewed--;
							document.getElementById(fieldName+"3").checked	=	false;
						} else if (fieldsDictionary[fieldName] == 1) {
							fieldsWithoutError--;
							fieldsReviewed--;
							document.getElementById(fieldName+"1").checked	=	false;
						} else {
							
						} 
						delete fieldsDictionary[fieldName];
						document.getElementById(fieldName+"label2").classList.add('custom-btn-grey');
						document.getElementById(fieldName+"label2").classList.remove('custom-btn-red');
						document.getElementById(fieldName+"label1").classList.add('custom-btn-grey');
						document.getElementById(fieldName+"label1").classList.remove('custom-btn-green');
						document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
						document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
					}
					fieldsDictionary[fieldName]	=	"-1";
					var corrCheckBox = document.getElementById(fieldName+"checkboxlabel4").children;
					for (var i=0; i < corrCheckBox.length; i++) {
						if ((corrCheckBox[i].tagName == "I") || (corrCheckBox[i].tagName == "i")) {
							
							if (document.getElementById(fieldName+"checkbox").checked) {
								
								document.getElementById(fieldName+"checkboxlabel4").classList.add('custom-btn-grey');
								document.getElementById(fieldName+"checkboxlabel4").classList.remove('custom-btn-green');
								corrCheckBox[i].classList.remove("glyphicon-thumbs-up");
								corrCheckBox[i].classList.add("glyphicon-thumbs-down");
								document.getElementById(fieldName+"checkbox").checked	=	false;
								fieldsCorrected--;
							} /* else {
								document.getElementById(fieldName+"checkboxlabel4").classList.add('custom-btn-grey');
								document.getElementById(fieldName+"checkboxlabel4").classList.remove('custom-btn-green');
								corrCheckBox[i].classList.remove("glyphicon-thumbs-up");
								corrCheckBox[i].classList.add("glyphicon-thumbs-down");
							} 
						}
					}	
					document.getElementById(fieldName+"comment").value = "";
					document.getElementById(fieldName+"comment").readOnly = true;
				}
				updateStatistics();
			}	 */
			function enableDisableErrorType (operation, fieldName) {
				var l_errortypecount	=	<%= l_errortypes.keySet().size() %>;
				if (operation == "success") {
					document.getElementById(fieldName+"errortype")[0].disabled = false; 
					document.getElementById(fieldName+"errortype")[0].selected = true;
					for (var l_i=0; l_i < l_errortypecount; l_i++) {
						document.getElementById(fieldName+"errortype")[l_i+1].disabled = true; 
					}
				} else if (operation == "failure") {
					document.getElementById(fieldName+"errortype")[0].disabled = true;
					document.getElementById(fieldName+"errortype")[0].selected = false;
					for (var l_i=0; l_i < l_errortypecount; l_i++) {
						document.getElementById(fieldName+"errortype")[l_i+1].disabled = false; 
					}
					document.getElementById(fieldName+"errortype")[1].selected = true;
				} else {
					document.getElementById(fieldName+"errortype")[0].disabled = false; 
					document.getElementById(fieldName+"errortype")[0].selected = true;
					for (var l_i=0; l_i < l_errortypecount; l_i++) {
						document.getElementById(fieldName+"errortype")[l_i+1].disabled = false; 
					}
				}
			}
			function changeNoErrorColor (currObject, categorytype) {
				var	fieldName	=	currObject.name;
				var	fieldId		=	currObject.id;
				/* if (document.getElementById(fieldName+"togglecheckbox").checked) { */
					document.getElementById(fieldName+"label1").classList.add('custom-btn-green');
					document.getElementById(fieldName+"label1").classList.remove('custom-btn-grey');
					/* document.getElementById(fieldName+"label2").classList.add('custom-btn-grey');
					document.getElementById(fieldName+"label2").classList.remove('custom-btn-red'); */
					document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
					document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
					if(fieldsDictionary[fieldName] === undefined ) {
						fieldsReviewed++;
						catReviewUpdate(categorytype);
						catCountUpdate(categorytype,1);
					} else {
						if (fieldsDictionary[fieldName] == 2) {
							fieldsMissing--;
							catCountUpdate(categorytype,1);
						} else if (fieldsDictionary[fieldName] == 3) {
							fieldsIncorrect--;
							catCountUpdate(categorytype,1);
						} else {
							fieldsReviewed++;
						}
					}
					fieldsWithoutError++;
					fieldsDictionary[fieldName] = "1";
					enableDisableErrorType("success",fieldName);
					/* document.getElementById(fieldName+"comment").value = "";
					document.getElementById(fieldName+"comment").readOnly = false; */
					updateStatistics();
				/* } else {
					document.getElementById(fieldName+"1").checked	=	false;
				} */	
			}
			function catCountUpdate (categorytype,operation) {
				if (categorytype == 1) {
					if (operation == 1) {
						cat1count++;
					} else {
						if (cat1count > 0) {
							cat1count--;
						}
					}
				} else if (categorytype == 2) {
					if (operation == 1) {
						cat2count++;
					} else {
						if (cat2count > 0) {
							cat2count--;
						}
					}
				} else if (categorytype == 3) {
					if (operation == 1) {
						cat3count++;
					} else {
						if (cat3count > 0) {
							cat3count--;
						}
					}
				}
			}
			function catReviewUpdate (categorytype) {
				if (categorytype == 1) {
					cat1reviewcount++;
				} else if (categorytype == 2) {
					cat2reviewcount++;
				} else if (categorytype == 3) {
					cat3reviewcount++;
				}
			}
			/* function changeMissingDataColor (currObject) {
				var	fieldName	=	currObject.name;
				var	fieldId		=	currObject.id;
				if (document.getElementById(fieldName+"togglecheckbox").checked) {
					document.getElementById(fieldName+"label2").classList.add('custom-btn-red');
					document.getElementById(fieldName+"label2").classList.remove('custom-btn-grey');
					document.getElementById(fieldName+"label1").classList.add('custom-btn-grey');
					document.getElementById(fieldName+"label1").classList.remove('custom-btn-green');
					document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
					document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
					if(fieldsDictionary[fieldName] === undefined ) {
						fieldsReviewed++;
					} else {
						if (fieldsDictionary[fieldName] == 1) {
							fieldsWithoutError--;
						} else if (fieldsDictionary[fieldName] == 3) {
							fieldsIncorrect--;
						} else {
							fieldsReviewed++;
						}
					}
					fieldsMissing++;
					fieldsDictionary[fieldName] = "2";
					document.getElementById(fieldName+"comment").value = "";
					document.getElementById(fieldName+"comment").readOnly = true;
					updateStatistics();
				} else {
					document.getElementById(fieldName+"2").checked	=	false;
				}	
			} */
			function changeIncorrectDataColor (currObject, categorytype) {
				var	fieldName	=	currObject.name;
				var	fieldId		=	currObject.id;
				/* if (document.getElementById(fieldName+"togglecheckbox").checked) { */
					document.getElementById(fieldName+"label3").classList.add('custom-btn-red');
					document.getElementById(fieldName+"label3").classList.remove('custom-btn-grey');
					document.getElementById(fieldName+"label1").classList.add('custom-btn-grey');
					document.getElementById(fieldName+"label1").classList.remove('custom-btn-green');
					/* document.getElementById(fieldName+"label2").classList.add('custom-btn-grey');
					document.getElementById(fieldName+"label2").classList.remove('custom-btn-red'); */
					if(fieldsDictionary[fieldName] === undefined ) {
						fieldsReviewed++;
						catReviewUpdate(categorytype);
					} else {
						if (fieldsDictionary[fieldName] == 1) {
							fieldsWithoutError--;
							catCountUpdate(categorytype,2);
						} else if (fieldsDictionary[fieldName] == 2) {
							fieldsMissing--;
						} else {
							fieldsReviewed++;
						}
					}
					fieldsIncorrect++;
					fieldsDictionary[fieldName] = "3";
					/* document.getElementById(fieldName+"comment").value = "";
					document.getElementById(fieldName+"comment").readOnly = true; */
					enableDisableErrorType("failure",fieldName); 
					updateStatistics();
				/* } else {
					document.getElementById(fieldName+"3").checked	=	false;
				}	 */
			}
			function changeCheckboxColor (currObject) {
				var	fieldName	=	currObject;
				/* if (document.getElementById(fieldName+"togglecheckbox").checked) { */
					var corrCheckBox = document.getElementById(fieldName+"checkboxlabel4").children;
					for (var i=0; i < corrCheckBox.length; i++) {
						if ((corrCheckBox[i].tagName == "I") || (corrCheckBox[i].tagName == "i")) {
							if (document.getElementById(fieldName+"checkbox").checked) {
								document.getElementById(fieldName+"checkboxlabel4").classList.add('custom-btn-green');
								document.getElementById(fieldName+"checkboxlabel4").classList.remove('custom-btn-grey');
								corrCheckBox[i].classList.add("glyphicon-thumbs-up");
								corrCheckBox[i].classList.remove("glyphicon-thumbs-down");
								fieldsCorrected++;
							} else {
								document.getElementById(fieldName+"checkboxlabel4").classList.add('custom-btn-grey');
								document.getElementById(fieldName+"checkboxlabel4").classList.remove('custom-btn-green');
								corrCheckBox[i].classList.remove("glyphicon-thumbs-up");
								corrCheckBox[i].classList.add("glyphicon-thumbs-down");
								fieldsCorrected--;
							}
						}
					}				
					updateStatistics();
				/* } else {
					document.getElementById(fieldName+"checkbox").checked	=	false;
				}	 */
			}
			function resetTabData () {
				commonResetSetTabData("reset");
			}
			
			function setNoErrorTabData() {
				commonResetSetTabData("set");
			}
			function commonResetSetTabData (p_action) {
				var currentTab = document.getElementById('myTab').children;
				for (var i=0; i < currentTab.length; i++) {
					if (currentTab[i].className == "active") {
						var	currentTabId	=	currentTab[i].children[0].id;
						<%
						for (String l_s : mapQualityReviewTabData.keySet()) { %>
							if (currentTabId == "<%=l_s%>") {
							<%	for (SafetyDbCaseFields l_k : mapQualityReviewTabData.get(l_s)) { //TODO:Change the fieldname to take double underscore%>
									var fieldName	=	"__fldfieldmnemonic<%=l_k.getCaseFieldMnemonic()%>";
									var radioValue	=	fieldsDictionary[fieldName];
									var catType		=	<%=l_k.getCaseFieldType().getCatCode()%>;
									/* if (document.getElementById(fieldName+"togglecheckbox").checked) { */
										if (p_action == "set") {
											document.getElementById(fieldName+"label1").classList.remove('custom-btn-grey');
											document.getElementById(fieldName+"label1").classList.add('custom-btn-green');
											document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
											document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
											
											if (document.getElementById(fieldName+"1").checked /* || document.getElementById(fieldName+"2").checked */ || document.getElementById(fieldName+"3").checked) {
												/* if (radioValue == 2) {
													fieldsMissing--;
													fieldsWithoutError++;
												} else */ if (radioValue == 3) {
													fieldsIncorrect--;
													fieldsWithoutError++;
												} else {
												}
											} else {
												fieldsWithoutError++;
												fieldsReviewed++;
											}
											fieldsDictionary[fieldName] = "1";
											document.getElementById(fieldName+"1").checked	=	true;
										} else if (p_action == "reset") {
											document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
											document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
											document.getElementById(fieldName+"label1").classList.add('custom-btn-grey');
											document.getElementById(fieldName+"label1").classList.remove('custom-btn-green');
											/* document.getElementById(fieldName+"label2").classList.add('custom-btn-grey');
											document.getElementById(fieldName+"label2").classList.remove('custom-btn-red'); */
											if ((document.getElementById(fieldName+"1").checked /* || document.getElementById(fieldName+"2").checked */ || document.getElementById(fieldName+"3").checked)) {
												if (radioValue == 1) {
													fieldsWithoutError--;
													catCountUpdate(catType,2);
												} /* else if (radioValue == 2) {
													fieldsMissing--;
												} */ else {
													fieldsIncorrect--;
												}
												fieldsReviewed--;
												if (catType == 1) {
													cat1reviewcount--;
												} else if (catType == 2) {
													cat2reviewcount--;
												} else if (catType == 3) {
													cat3reviewcount--;
												}
												
												$('input[name=__fldfieldmnemonic<%=l_k.getCaseFieldMnemonic()%>]').attr('checked',false);
												delete fieldsDictionary[fieldName];
											}
											document.getElementById(fieldName+"comment").value = "";
											enableDisableErrorType("all",fieldName);
											/* var corrCheckBox = document.getElementById(fieldName+"checkboxlabel4").children;
											for (var j=0; j < corrCheckBox.length; j++) {
												if (corrCheckBox[j].tagName == "I" || corrCheckBox[j].tagName == "i") {
													if (document.getElementById(fieldName+"checkbox").checked) {
														document.getElementById(fieldName+"checkboxlabel4").classList.add('custom-btn-grey');
														document.getElementById(fieldName+"checkboxlabel4").classList.remove('custom-btn-green');
														corrCheckBox[j].classList.remove("glyphicon-thumbs-up");
														corrCheckBox[j].classList.add("glyphicon-thumbs-down");
														fieldsCorrected--;
														document.getElementById(fieldName+"checkbox").checked	=	false;
													}
												}
											} */
										} else {
											
										}
									/* } */
									
							<%	} %>
								break;
							}
						<%	
						}
						%>
						break;
					}
				}
				updateStatistics();
			}
			
			function resetAllTabData () {
				var retVal = confirm("The current review will be restarted.Do you want to continue?");
				if (retVal == false) {
					return false;
				} else {
					document.frmmain.reset();
					var radioElements = document.getElementsByTagName('input');
					var	prevField	=	"";
			        for (var i = 0; i < radioElements.length; i++) {
			        	/* if ((prevField != radioElements[i].name) && (radioElements[i].type == 'checkbox') && (radioElements[i].class == 'hidden')) {
			            	var fieldName	=	radioElements[i].name;
			            	var corrCheckBox = document.getElementById(fieldName+"label4").children;
							for (var j=0; j < corrCheckBox.length; j++) {
								if (corrCheckBox[j].tagName == "I" || corrCheckBox[j].tagName == "i") {
									document.getElementById(fieldName+"label4").classList.add('custom-btn-grey');
									document.getElementById(fieldName+"label4").classList.remove('custom-btn-green');
									corrCheckBox[j].classList.remove("glyphicon-thumbs-up");
									corrCheckBox[j].classList.add("glyphicon-thumbs-down");
								}
							}
							prevField	=	fieldName;
			            } else */ if ((prevField != radioElements[i].name) && (radioElements[i].type == 'radio')) {
			            	var fieldName	=	radioElements[i].name;
			            	document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
							document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
							document.getElementById(fieldName+"label1").classList.add('custom-btn-grey');
							document.getElementById(fieldName+"label1").classList.remove('custom-btn-green');
							document.getElementById(fieldName+"errortype").readOnly = false;
							document.getElementById(fieldName+"errortype").value = -1;
							prevField	=	fieldName;
			            }
			        }
			        fieldsDictionary	=	[];
			        fieldCatDictionary	=	[];
			        fieldsReviewed	=	fieldsWithoutError	=	fieldsMissing	=	fieldsIncorrect	=	fieldsCorrected	=	0;
			        cat1count		=	cat2count			=	cat3count		=	0;
			        cat1reviewcount	=	cat2reviewcount		=	cat3reviewcount	=	0;
			        updateStatistics();
				}
				
			}
			function setRemainingNoError () {
				<% for (String l_s : mapQualityReviewTabData.keySet()) {
					for (SafetyDbCaseFields l_k : mapQualityReviewTabData.get(l_s)) { //TODO:Change the fieldname to take double underscore%>
						fieldCatDictionary['__fldfieldmnemonic<%= l_k.getCaseFieldMnemonic()%>']	=	<%= l_k.getCaseFieldType().getCatCode()%>;
				<% } %>
				<% } %>
				var radioElements = document.getElementsByTagName('input');
			    var	prevField	=	"";
		        for (var i = 0; i < radioElements.length; i++) {
		            if ( (prevField != radioElements[i].name) && (radioElements[i].type == 'radio')) {
		            	var fieldName	=	radioElements[i].name;
		            	if (!(document.getElementById(fieldName+"1").checked /* || document.getElementById(fieldName+"2").checked  */|| document.getElementById(fieldName+"3").checked)) {
			            	document.getElementById(fieldName+"label1").classList.add('custom-btn-green');
							document.getElementById(fieldName+"label1").classList.remove('custom-btn-grey');
							/* document.getElementById(fieldName+"label2").classList.add('custom-btn-grey');
							document.getElementById(fieldName+"label2").classList.remove('custom-btn-red'); */
							document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
							document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
							document.getElementById(fieldName+"1").checked = true;
							++fieldsReviewed;
							++fieldsWithoutError;
							fieldsDictionary[fieldName] = "1";
							catReviewUpdate(fieldCatDictionary[fieldName]);
							catCountUpdate(fieldCatDictionary[fieldName],1);
							enableDisableErrorType("success",fieldName);
		            	}  else {
		            	}
						prevField	=	fieldName; 
		            }
		        }
		        updateStatistics();
			}
			
			function updateStatistics () {
				replaceString();
				if (statisticsShown == false) {
					notice	=	new PNotify({
				    	title: "Statistics",
				        name : "first",
				        type: "info",
				        text: displayStatText,
				         nonblock: {
				              nonblock: true,
				              nonblock_opacity: .9
				        },
				        styling: 'bootstrap3',
				        hide: false
				    });
					statisticsShown	=	true;
				}
				var options	=	{
			        				text: displayStatText
			        			};
				notice.update(options);
			}
			function formSubmit () 
			{
				clearErrorMessages();
				if (document.getElementById("fldcaseassoc").value == "-1") {
					displayErrorMessage("UI-1028");
					document.getElementById("fldcaseassoc").focus();
					return false;
				}
				if (totalFields != fieldsReviewed) {
					displayErrorMessage("UI-1030");
					return false;
				}
				if (fieldsReviewed != (fieldsWithoutError+fieldsIncorrect)) {
					displayErrorMessage("UI-1030");
					return false;
				}
				document.frmmain.fldmode.value					=	"create";
				document.frmmain.fldfieldsreviewed.value		=	fieldsReviewed;
				document.frmmain.fldfieldsWithoutError.value	=	fieldsWithoutError;
				document.frmmain.fldfieldsIncorrect.value		=	fieldsIncorrect;
				//document.frmmain.fldfieldsMissing.value		=	fieldsMissing;
				document.frmmain.fldcat1count.value				=	cat1count;
				document.frmmain.fldcat2count.value				=	cat2count;
				document.frmmain.fldcat3count.value				=	cat3count;
				document.frmmain.fldcat1reviewcount.value		=	cat1reviewcount;
				document.frmmain.fldcat2reviewcount.value		=	cat2reviewcount;
				document.frmmain.fldcat3reviewcount.value		=	cat3reviewcount;
				var	fieldjsondata	=	"";
				for (var key in fieldsDictionary) 
				{
					<% //TODO:Change the fieldname to take double underscore %>
					fieldjsondata	=	fieldjsondata+key.slice(18)+"~"+fieldsDictionary[key]+"~"+document.getElementById(key+"errortype").value+";";
				}
				fieldjsondata	=	fieldjsondata.slice(0,-1);
				fieldjsondata	=	fieldjsondata + "#,#";
				for (var key in fieldsDictionary) 
				{
					<% //TODO:Change the fieldname to take double underscore %>
					var l_comment	=	document.getElementById(key+"comment").value;
					if (l_comment != '' && (l_comment.indexOf("~") != -1 || l_comment.indexOf(";") != -1
							|| l_comment.indexOf("#,#") != -1 || l_comment.indexOf(":(") != -1 )) {
						displayErrorMessage("UI-1070");
						return false;
					}
					fieldjsondata	=	fieldjsondata+key.slice(18)+":("+document.getElementById(key+"comment").value+")#~#";
				}
				<%if(o_orttemplate.getRvwAllToCorrect()==1){ %>
				if (document.frmmain.cdbrcheckbox.checked) {
					document.frmmain.cdbrcheckboxvalue.value = 1;
				} else {
					document.frmmain.cdbrcheckboxvalue.value = 0;
				}
				<%}%>
				
				fieldjsondata	=	fieldjsondata.slice(0,-3);
				document.frmmain.fldjsondata.value	=	fieldjsondata;
				document.frmmain.fldscftseqid.value		=	"<%=l_scft.getSequenceId()%>";
				document.frmmain.fldcasereviewsumm.value = document.getElementById("casereviewsumm").value;
		        document.getElementById("portal_action").value = "ilqr_step4";
		        
		        if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			
			function typecomment(id){
				 
				$('input[rel="txtTooltip"]').attr('title',document.getElementById(id).value );
					 $('input[rel="txtTooltip"]').tooltip('toggle');
			}
			function validateFormParameters () {
				clearErrorMessages();
				oFormObject = document.forms["frmmain"];
				for (var i=0; i < oFormObject.length; i++) {
					if(oFormObject.elements[i].name.startsWith("__")) {
						document.getElementById(oFormObject.elements[i].id).disabled = true;
					}
				}	
				var result	=	validateInputParameter(oFormObject);
				if (!result) {
					for (var i=0; i < oFormObject.length; i++) {
						if(oFormObject.elements[i].name.startsWith("__")) {
							document.getElementById(oFormObject.elements[i].id).disabled = false;
						}
					}	
				}
				return result;
			}
			
		</script>
		
	</head> 
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldfieldsreviewed" name="fldfieldsreviewed" value="">
			<input type="hidden" id="fldfieldsWithoutError" name="fldfieldsWithoutError" value="">
			<input type="hidden" id="cdbrcheckboxvalue" name="cdbrcheckboxvalue" value="">
			<input type="hidden" id="fldfieldsMissing" name="fldfieldsMissing" value="">
			<input type="hidden" id="fldfieldsIncorrect" name="fldfieldsIncorrect" value="">
			<input type="hidden" id="fldfieldscorrected" name="fldfieldscorrected" value="">
			<input type="hidden" id="fldjsondata" name="fldjsondata" value="">
			<input type="hidden" id="fldscftseqid" name="fldscftseqid" value="">
			<input type="hidden" id="fldmode" name="fldmode" value="">
			<input type="hidden" id="fldcat1count" name="fldcat1count" value="">
			<input type="hidden" id="fldcat2count" name="fldcat2count" value="">
			<input type="hidden" id="fldcat3count" name="fldcat3count" value="">
			<input type="hidden" id="fldcat1reviewcount" name="fldcat1reviewcount" value="">
			<input type="hidden" id="fldcat2reviewcount" name="fldcat2reviewcount" value="">
			<input type="hidden" id="fldcat3reviewcount" name="fldcat3reviewcount" value="">
			<input type="hidden" id="fldreviewtype" name="fldreviewtype" value="<%= l_reviewtype%>">
			<input type="hidden" id ="reviewround"  name="reviewround"  value="<%=reviewround%>" />
			<input type="hidden" id ="fldcasereviewsumm"  name="fldcasereviewsumm"  value="" />
			<input type="hidden" id="fldcmseqid" name="fldcmseqid" value="<%=o_pvcpcasemaster.getSeqId() %>">
			<input type="hidden" id="fldinternalcaseid" name="fldinternalcaseid" value="<%=o_pvcpcasemaster.getInternalCaseId() %>">
		

			
			<div class="title_left">
				<h3>Quality Review Case Details Entry</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Details
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
						<div class="x_content" style="display: block;">
							<table class="table table-striped">
								<thead>
									<tr>
										<th><%= l_scft.getCaseIdAlias() %></th>
										<th><%= l_scft.getCaseVersionAlias() %></th>
										<th>Initial Receive Date</th>
										<th>Case Type</th>
										<th>Seriousness</th>
										<th>Case Associate</th>
										<% if (l_ismrnameshow) { %>
											<th>Medical Reviewer</th>
										<% } %>
										<% if(!l_enableUdfMap.isEmpty()) {
											for (String key : l_enableUdfMap.keySet()) {%>
												<th><%=l_enableUdfMap.get(key).get(0) %></th>
										<%	} 
										} %>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<input type="hidden" maxlength="100" name="fldcaseid" id="fldcaseid" class="form-control" value="<%=o_pvcpcasemaster.getCaseId() %>" />
											<label><%=o_pvcpcasemaster.getCaseId() %></label>
										</td>
										<td>
											<input type="hidden" maxlength="38" name="fldcaseversion" id="fldcaseversion" class="form-control" value="<%=o_pvcpcasemaster.getCaseVersion()%>" />
											<label><%=o_pvcpcasemaster.getCaseVersion()%></label>
										</td>
										<td>
											<label><%= WebUtils.formatForDisplayDate(o_pvcpcasemaster.getInitialRecvDate(), "dd-MMM-YYYY") %></label>
										</td>
										<td>
											<label><%= o_pvcpcasemaster.getCaseType() %></label>
										</td>
										<td>
											<label>
												<%= o_pvcpcasemaster.getCaseSeriousness()%>
											</label>	
										</td>
										<td>
											<%-- <select class="form-control" name="fldcaseassoc" id="fldcaseassoc" value="">
												<option value="-1">(Select a user)</option>
												<% for (UserInfo l_userinfo : l_listusers) { %>
													<option value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
												<% } %>
											</select> --%>
											
											<select class="form-control" name="fldcaseassoc" id="fldcaseassoc">
												<option value="-1">(Select a user)</option>
												<% for (UserInfo l_userinfo : l_listusers) { 
													if (l_userinfo.getUserSeqId() == o_pvcpcasemaster.getPcmDeUserSeqId()) { %>
														<option selected value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
												 <% } else { %>
												 		<option value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
												 <% } %>	
												<% } %>
											</select> 
										</td>
										<% if (l_ismrnameshow) { %>
											<td>
												<select class="form-control" name="fldmedicalreviewer" id="fldmedicalreviewer">
													<option value="-1">(Select a user)</option>
													<% for (UserInfo l_userinfo : l_listusers) { %>
														<option value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
													<% } %>
												</select>
											</td>	
										<% } %>
										<% if(!l_enableUdfMap.isEmpty()) {
											for (int l_i=1; l_i<=10; l_i++) {
												if (l_enableUdfMap.containsKey(QRConstant.ENABLE_UDF_MAP_KEY+l_i)) { %>
													<td>
													<% String	l_udfprojvalue	= l_enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY+l_i).get(1);	
														if (l_udfprojvalue != null) { %>
														<select  name ="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>"
														 	id ="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" class ="form-control">
															<% for (String l_udfvalue : StringUtils.splitOnComma(l_udfprojvalue)) { 
																String[]	l_udfdetails = l_udfvalue.split(":"); 
																if(l_udfdetails[0].equals(l_udfvaluelist.get(l_i-1))) { %>
																	<option selected value ="<%= l_udfdetails[0]%>"><%= l_udfdetails[1]%></option>
															 <% } else { %>
															 		<option value ="<%= l_udfdetails[0]%>"><%= l_udfdetails[1]%></option>
															 <% } %>	
															<% } %>
														</select>
												 	<% } else { 
												 		if (StringUtils.isNullOrEmpty(l_udfvaluelist.get(l_i-1))) {%>
															<input type="text" name="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" maxlength="4000"
																id="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" class="form-control" value=""/>
													 <% } else { %>
													 		<input type="text" name="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" maxlength="4000"
																id="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" class="form-control" value="<%=l_udfvaluelist.get(l_i-1)%>"/>
													 		
													 <% } %>	
													<% } %>
													</td>
												<% }
											}
										} %>	
									</tr>
								</tbody>
							</table>
						</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Level Comments
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<%if(o_orttemplate.getRvwAllToCorrect()==1){ %>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="cdbrcheckbox">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Correction done by reviewer">
                         CDBR
                        </span>
							</label>
							<div class="col-md-10 col-sm-10 col-xs-12">
								<label>
									<input type="checkbox" id="cdbrcheckbox" name="cdbrcheckbox"
									 class="js-switch" data-switchery="true" style="display: none;">
									<!--  <span class="switchery switchery-default" 
									 style="box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset; 
									 border-color: rgb(223, 223, 223); background-color: rgb(255, 255, 255);
									  transition: border 0.4s, box-shadow 0.4s;"><small style="left: 0px; 
									  transition: background-color 0.4s, left 0.2s;"></small></span> -->
								</label>
							</div>	
						</div>
						<%} %>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="fldemailcclist">
								Comments
							</label>
							<div class="col-md-10 col-sm-10 col-xs-12">
								<textarea class="form-control vresize" id="casereviewsumm" name="casereviewsumm" rows="6" ></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
						 	Case Field Details
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<% if ("Single Page".equals(l_layouttype)) { %>
							<table class="table table-bordered" id="detailstable">
								<thead>
									<tr>
										<th>Tab Name</th>
										<th>Field</th>
										<th>Review Status</th>
										<th>Error Type</th>
										<!-- <th>Corrected</th> -->
										<th>Comments</th>
									</tr>
								</thead>
								<tbody>	
									<%	int		l_tabindex	=	0;
										for (String l_tabname : mapQualityReviewTabData.keySet()) { 
											int		l_counttemp	=	1;
											String	l_bgcolor	=	null;
											for (SafetyDbCaseFields l_tabdata : mapQualityReviewTabData.get(l_tabname)) { %>
												<tr>
													<% if (l_counttemp == 1 ) { %>
														<td rowspan="<%=mapQualityReviewTabData.get(l_tabname).size()%>">
															<b><%=l_tabname %></b>
														</td>
													<% } %>	
													<td>
												<% if (l_catcount == 0 || l_catcount == 1) { %>
												<% } else if (l_catcount == 2) {
													if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
														<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
													<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
														<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
													<% } %>
												<% } else if (l_catcount == 3) { 
													if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
														<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
													<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
														<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
													<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
														<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
													<% } %>
												<% } else { %>
												<% } %>
												
														<label >
															<%= l_tabdata.getCaseFieldName() %>
														</label>
													</td>
													<td>
														<div data-toggle="buttons">
															<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label1" class="btn btn-sm custom-btn-grey" data-toggle-class="btn-primary"
															 	data-toggle="tooltip" data-placement="left" data-original-title="Correct">
																<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>1" value="1"
																	 data-parsley-multiple="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" onchange="changeNoErrorColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);"  />
																	<i class="glyphicon glyphicon-ok"></i>
															</label>
															<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label3" class="btn btn-sm custom-btn-grey" data-toggle-class="btn-primary"
																 data-toggle="tooltip" data-placement="right" data-original-title="Incorrect">
																<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>3" value="3"
																 	data-parsley-multiple="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" onchange="changeIncorrectDataColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);">
																	<i class="glyphicon glyphicon-remove"></i>
															</label>
														</div>
													</td>
													<td>
														<select name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" 
															id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" class="form-control">
															<option value="-1">No Error</option>
															<% for (String l_errorcode : l_errortypes.keySet()) { %>
																<option value="<%= l_errorcode %>" disabled><%= l_errortypes.get(l_errorcode) %></option>
															<% } %>
														</select>
													</td>
													<td>
														<input type="text" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" maxlength="4000"
															id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" class="form-control" value="" 
															rel="txtTooltip"  data-toggle="tooltip" data-placement="top" 
															oninput="typecomment('__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment');" onmouseover="typecomment('__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment')"/>
													</td>
												</tr> <%  
											l_counttemp++;
										} 
										l_tabindex++; %>
								<%	} %>
							</tbody>
							</table>
							<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
								Submit
							</button>
						<% } else { %>
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
										
										<% int	l_count = 1;
										for (String l_tabname : mapQualityReviewTabData.keySet()) { 
											if (l_count == 1) { %>
												<li role="presentation" class="active">
													<a href="#tab_content<%=l_count%>" role="tab" id="<%= l_tabname %>" data-toggle="tab" 
														aria-expanded="true" tabindex="<%=l_count%>>">
														<%= l_tabname %>
													</a>
												</li>
										<%  } else { %>
												<li role="presentation" class="">
													<a href="#tab_content<%=l_count%>" role="tab" id="<%= l_tabname %>" data-toggle="tab" 
														aria-expanded="false" tabindex="<%=l_count%>>">
														<%= l_tabname %>
													</a>
												</li>
										<%  }
											l_count++;	
										%>
									<%  }
									%>
								</ul>
								<div id="myTabContent" class="tab-content">
								<% 
									int	l_tabdetails = 1;
									//l_fieldcatmap	=	new HashMap<String,Integer> ();
									String	l_bgcolor	=	null;
									for (String l_tabname : mapQualityReviewTabData.keySet()) { %>
										<div role="tabpanel" id="tab_content<%=l_tabdetails%>" aria-labelledby="<%= l_tabname %>" 
										<% 	if (l_tabdetails == 1) 
										{
										%>
												 class="tab-pane fade active in">
										<%	} else { %>
												 class="tab-pane fade">
													
										<%	}
										%>		
											<table class="table table-striped" id="detailstable">
												<thead>
													<tr>
														<!-- <th></th> -->
														<th>Field</th>
														<th>Review Status</th>													
														<!-- <th>Corrected</th> -->
														<th>Error Type</th>
														<th>
															<div class="col-md-6">
																Comments
															</div>	
														</th>
													</tr>
												</thead>
												<tbody>		
													<% 	for (SafetyDbCaseFields l_tabdata : mapQualityReviewTabData.get(l_tabname)) { 
														//l_fieldcatmap.put(l_tabdata.getCaseFieldMnemonic(), l_tabdata.getCaseFieldType().getCatCode()); %>
														<tr>
															<%-- <td>
																<input type="hidden" checked id="<%= l_tabdata %>togglecheckbox" name="<%= l_tabdata %>togglecheckbox" onchange="notApplicableDisabled('<%= l_tabdata %>');">
															</td> --%>
															<td>
															<% if (l_catcount == 0 || l_catcount == 1) { %>
															<% } else if (l_catcount == 2) {
																if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																	<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																	<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																<% } %>
															<% } else if (l_catcount == 3) { 
																if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																	<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																	<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
																	<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
																<% } %>
															<% } else { %>
															<% } %>
																<label>
																	<%= l_tabdata.getCaseFieldName() %>
																</label>
															</td>
															<td>
																<div data-toggle="buttons">
																	<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label1" class="btn btn-sm custom-btn-grey"
																	 	data-toggle-class="btn-primary" data-toggle="tooltip" data-placement="left" data-original-title="Correct">
																		<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" 
																			id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>1" value="1"
																			 onchange="changeNoErrorColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);" />
																			<i class="glyphicon glyphicon-ok"></i>
																	</label>
																	<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label3" class="btn btn-sm custom-btn-grey" 
																		data-toggle-class="btn-primary" data-toggle="tooltip" data-placement="right" data-original-title="Incorrect">
																		<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" 
																			id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>3" value="3" 
																			onchange="changeIncorrectDataColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);">
																			<i class="glyphicon glyphicon-remove"></i>
																	</label>
																</div>
															</td>
															<td>
																<select name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" 
																	id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" class="form-control">
																	<option value="-1">No Error</option>
																	<% for (String l_errorcode : l_errortypes.keySet()) { %>
																		<option value="<%= l_errorcode %>"><%= l_errortypes.get(l_errorcode) %></option>
																	<% } %>
																</select>
															</td>
															<td>
																<input type="text" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" maxlength="4000"
																	id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" class="form-control" value="" />
															</td>
														</tr>
													<%  
														} 
													%>
												</tbody>
											</table>
											<% 	if (l_tabdetails == 1) {
											%>
												 
												<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
													Submit
												</button>
												<a class="btn btn-primary btn-sm btnNext pull-right">Next</a>
											<%	} else if (l_tabdetails == mapQualityReviewTabData.keySet().size()) { %>
													
													<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
														Submit
													</button>
													<a class="btn btn-primary btn-sm btnPrevious pull-right">Previous</a>
											<%	} else { %>
													<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
														Submit
													</button>
													<a class="btn btn-primary btn-sm btnNext pull-right">Next</a> 
													<a class="btn btn-primary btn-sm btnPrevious pull-right">Previous</a>
											<%	}
												l_tabdetails++;
											%>
	    									
										</div>
									<% 	}
									%>
								</div>
							</div>
						<% } %>
						<div>
							<% if ("Single Page".equals(l_layouttype)) { %>
								<a href="javascript:setRemainingNoError();" class="btn btn-primary btn-sm">
									Mark Rest as No Error
								</a>
								<a href="javascript:resetAllTabData();" class="btn btn-primary btn-sm">
									Restart Review
								</a>
							<% } else { %>
								<a href="javascript:setRemainingNoError();" class="btn btn-primary btn-sm">
									Mark Rest as No Error
								</a>
								<a href="javascript:resetAllTabData();" class="btn btn-primary btn-sm">
									Restart Review
								</a>
								<a href="javascript:resetTabData();" class="btn btn-primary btn-sm">
									Reset Tab
								</a>
							<% } %>
						</div>
					</div> 
				</div>
			</div>
		</csrf:form>
	</body> 
</html>