<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDBCaseFieldCategoryType"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.commons.SciConstants.StringConstants"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.commons.SciEnums.EmailSendSetting"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.SciEnums.RecordState"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFields" %>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.apps.qr.QRTemplate" %>

<%
	QRTemplate					l_qrtemplate				=	(QRTemplate)	request.getAttribute("fldtemplatedata");
	SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate	=	(SafetyDbCaseFieldsTemplate) request.getAttribute("safetydbcasefields");
	List<SafetyDbCaseFields>	l_safetydbcasefieldlist		=	l_safetydbcasefieldtemplate.getCaseFieldList();
	RecordState					l_templatestate				=	l_qrtemplate.getState();
	String						l_mode						=	(String) request.getAttribute("fldmode");
	String						l_cataliases				=	(String) request.getAttribute("categoryaliases");	
	int							l_catcount					=	0;
	String []					l_categories				=	null;
	
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
	String sNewPortalAppId = (String) request.getAttribute("new-portalappid");
	String sDownloadAllowed = ConfigHome.getPrjConfigSettings(sNewPortalAppId+StringConstants.PERIOD+QRConstant.DOWNLOAD_ALLOWED,WebUtils.getAuthorizedProjectId(request));
%>
<!DOCTYPE html>
<html>
	<head>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
		$(window).load(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
			
			function fnback () {
				document.frmmain.submit();
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnchangetemplatestate (sNewState)
			{
				document.frmmain.fldtemplatenewstate.value	=	sNewState;
				document.frmmain.fldtemplateseqid.value		=	'<%= l_qrtemplate.getSequenceId() %>';
				document.frmmain.portal_action.value		=	"updatetemplatestate";	
				document.frmmain.fldmode.value				=	"view";
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function fnedit (tabindex) {
				if (tabindex == 1) {
					document.frmmain.fldmode.value	=	"editattribute";
				} else {
					document.frmmain.fldmode.value	=	"editreviewfield";
				}
				document.frmmain.portal_action.value	=	"gettemplatedetails";
				document.frmmain.fldtemplateseqid.value	=	'<%= l_qrtemplate.getSequenceId()%>';
				if (validateFormParameters())
				 {
				document.frmmain.submit();
			}
			}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldtemplateseqid" name="fldtemplateseqid" value="">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="QR-PORTAL-APP">
			<input type="hidden" id="fldmode" name="fldmode" value="">
			<input type="hidden" id="fldtemplatenewstate" name="fldtemplatenewstate" value="">
			<div role="main">
				<div>
					<div class="page_title">
						<div class="title_left">
							<h3>Quality Review Template</h3>
						</div>
					</div>
					<div class="clearfix">
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										View Template Details
									</h2>
									<div class="clearfix">
									</div>
								</div>
								<div class="x_content">
									<div class="form-group">
										<% if ("view".equals(l_mode)) {
											if (l_qrtemplate.getState() == RecordState.UNDER_CONFIGURATION) { %>
												<a href="javascript:fnchangetemplatestate(1);"><i class="glyphicon glyphicon-ok-sign"></i> Activate</a> <% 	
											} else if (l_qrtemplate.getState() == RecordState.ACTIVE) { %>
												<a href="javascript:fnchangetemplatestate(900);"><i class="glyphicon glyphicon-ok-sign"></i> De-Activate</a> <% 	
											}
										} %>
	                  					
									</div>
									<div class="x_title">
										<h4>
											Template Meta-Data
										</h4>
										<div class="clearfix">
										</div>
									</div>
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldtemplatename">
												Template Name
											</label>
										</b>	
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_qrtemplate.getTemplateName() %>
											</label>
										</div>
									</div>	
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Template State
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_templatestate.displayName() %>
											</label>
										</div>
									</div>
									<br/>
									<div class="" role="tabpanel" data-example-id="togglable-tabs">
										<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
											<li role="presentation" class="active">
												<a href="#attributefieldstab" role="tab" id="#attributefieldstab" data-toggle="tab" 
													aria-expanded="true" tabindex="1">
													Attribute Details
												</a>
											</li>
											<li role="presentation" class="">
												<a href="#reviewfieldtabs" role="tab" id="#reviewfieldtabs" data-toggle="tab" 
													aria-expanded="false" tabindex="2">
													Review Fields
												</a>
											</li>
										</ul>
										<div id="myTabContent" class="tab-content">
											<div role="tabpanel" id="attributefieldstab" aria-labelledby="Attribute Details" class="tab-pane fade active in">
												<%-- <div class="x_title">
													<h4>
														Review Completion Notification Email
													</h4>
													<div class="clearfix">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailsendsetting">
														Configuration
													</label>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<% if (l_qrtemplate != null && l_qrtemplate.getRvwComplNtfnSetting() != null) { %>
																<label class="control-label">
																	<%= l_qrtemplate.getRvwComplNtfnSetting().getDisplayName() %>
																</label>
														 <% } else { %>
														 		<label class="control-label">
																	None
																</label>
														 <% } %>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailcclist">
														CC List
													</label>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<% if(l_qrtemplate.getRvwComplNtfnCCAddr() != null) { %>	
															<input class="form-control" type="text" readonly  value="<%= l_qrtemplate.getRvwComplNtfnCCAddr()%>">
														<% } else { %>
															<input class="form-control" type="text" readonly value="None">
														<% } %>
													</div>
												</div> --%>
												<div class="x_title">
													<h4>
														Case Result Criteria
													</h4>
													<div class="clearfix">
													</div>
												</div>
												<div class="form-group">
							                    	<label class="control-label col-md-3 col-sm-3 col-xs-12">Criteria Mode</label>
							                        <div class="col-md-4 col-sm-4 col-xs-12">
							                        	<% if(l_qrtemplate.getCaseResultCriteriaMode() == 1) { %>	
															<label class="control-label">
																Number
															</label>
														<% } else { %>
															<label class="control-label">
																Percent
															</label>
														<% } %>
							                        </div>
						                     	</div>
												
												<% if (l_catcount == 0 || l_catcount == 1) { %>
												<% } else if (l_catcount == 2 || l_catcount == 3) { %> 
													
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">
															<%= l_categories[0] %>
														</label>
														<div class="col-md-4 col-sm-4 col-xs-12">
													 		<label class="control-label">
													 			<% if (l_qrtemplate.getCat1QualityThreshold() != -1) { %>	
													 				<%= l_qrtemplate.getCat1QualityThreshold() %>
													 			<% } else { %>
													 			<% } %>
															</label>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">
															<%= l_categories[1] %>
														</label>
														<div class="col-md-4 col-sm-4 col-xs-12">
													 		<label class="control-label">
													 			<% if (l_qrtemplate.getCat2QualityThreshold() != -1) { %>	
													 				<%= l_qrtemplate.getCat2QualityThreshold() %>
													 			<% } else { %>
													 			<% } %>
															</label>
														</div>
													</div>
													<% if (l_catcount == 3) { 	%>
														<div class="form-group">
															<label class="control-label col-md-3 col-sm-3 col-xs-12">
																<%= l_categories[2] %>
															</label>
															<div class="col-md-4 col-sm-4 col-xs-12">
														 		<label class="control-label">
																	<% if (l_qrtemplate.getCat3QualityThreshold() != -1) { %>	
													 				<%= l_qrtemplate.getCat3QualityThreshold() %>
													 			<% } else { %>
													 			<% } %>
																</label>
															</div>
														</div>
													<% } %>
												<% } else { %>
												<% } %>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12">
														Overall Threshold
													</label>
													<div class="col-md-4 col-sm-4 col-xs-12">
												 		<label class="control-label">
												 			<% if (l_qrtemplate.getCaseQualityThreshold() != -1) { %>	
												 				<%= l_qrtemplate.getCaseQualityThreshold() %>
												 			<% } else { %>
												 			<% } %>
														</label>
													</div>
												</div>
												<%-- <div class="x_title">
													<h4>
														Case Classification
													</h4>
													<div class="clearfix">
													</div>
												</div>
												<% if (l_catcount == 0 || l_catcount == 1) { %>
												<% } else if (l_catcount == 2 || l_catcount == 3) { %> 
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailsendsetting">
															<%= l_categories[0] %>
														</label>
														<div class="col-md-4 col-sm-4 col-xs-12">
													 		<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationCat1() != null) { %>
													 			<input class="form-control" type="text" readonly value="<%= l_qrtemplate.getCaseClassificationCat1() %>">
													 		<% } else { %>
													 			<input class="form-control" type="text" readonly value="None">
													 		<% } %>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailsendsetting">
															<%= l_categories[1] %>
														</label>
														<div class="col-md-4 col-sm-4 col-xs-12">
													 		<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationCat2() != null) { %>
													 			<input class="form-control" type="text" readonly value="<%= l_qrtemplate.getCaseClassificationCat2() %>">
													 		<% } else { %>
													 			<input class="form-control" type="text" readonly value="None">
													 		<% } %>
														</div>
													</div>
													<% if (l_catcount == 3) { 	%>
														<div class="form-group">
															<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailsendsetting">
																<%= l_categories[2] %>
															</label>
															<div class="col-md-4 col-sm-4 col-xs-12">
														 		<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationCat3() != null) { %>
														 			<input class="form-control" type="text" readonly value="<%= l_qrtemplate.getCaseClassificationCat3() %>">
														 		<% } else { %>
														 			<input class="form-control" type="text" readonly value="None">
														 		<% } %>
															</div>
														</div>
													<% } %>
												<% } else { %>
												<% } %>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailsendsetting">
														Overall
													</label>
													<div class="col-md-4 col-sm-4 col-xs-12">
												 		<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationOverall() != null) { %>
												 			<input class="form-control" type="text" readonly value="<%= l_qrtemplate.getCaseClassificationOverall() %>">
												 		<% } else { %>
												 			<input class="form-control" type="text" readonly value="None">
												 		<% } %>
													</div>
												</div> --%>
												<div class="x_title">
													<h4>
														Other Settings
													</h4>
													<div class="clearfix">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldreviewercorrectionallowed">
														<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Correction done by reviewer">
                        									CDBR
                      			  						</span>
													</label>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<% if (l_qrtemplate != null && l_qrtemplate.getRvwAllToCorrect() == 1) {%>
															<label class="control-label">
																Yes
															</label>	
														<% } else { %>
														 	<label class="control-label">
																No
															</label>	
														 <% } %>
													</div>	
												</div>
													
												<%-- <div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldreviewsummrequired">
														Review Summary Required
													</label>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<% if (l_qrtemplate != null && l_qrtemplate.getRvwSumRequired() == 1) {%>
															<label class="control-label">
																Yes
															</label>	
														<% } else { %>
														 	<label class="control-label">
																No
															</label>	
														 <% } %>
													</div>	
												</div> --%>
												<div class="ln_solid"></div>
			                          			
			                          				<% if (! (l_qrtemplate.getState() == RecordState.ACTIVE || l_qrtemplate.getState() == RecordState.DEACTIVATED)) { %>
			                          					<div class="form-group">
															<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
																<button type="button" class="btn btn-primary" onclick="fnback();" value="Back">
																	Back
																</button>
																<button type="button" class="btn btn-success" onclick="fnedit(1);" value="Edit">
																	Edit
																</button>
															</div>
														</div>	
													<% } else { %>
														<div align="center">
															<button type="button" class="btn btn-primary" onclick="fnback();" value="Back">
																Back
															</button>
														</div>
													<% } %>
											</div>
											<div role="tabpanel" id="reviewfieldtabs" aria-labelledby="Review Fields" class="tab-pane fade">
												<table class="table table-striped">
			                      					<thead>
			                        					<tr>
			                        						<th>Field Name</th>
			                          						<th>Field Group</th>
			                          						<th align="center">Review Field</th>
			                          				</thead>
			                          				<tbody>
			                          					<% if (l_safetydbcasefieldlist != null && l_safetydbcasefieldlist.size() > 0) { %>
			                          						<% for (SafetyDbCaseFields l_safetydbcasefield : l_safetydbcasefieldlist) { %>
			                          							<tr>
				                          							<td>
				                          								<% if (l_catcount == 0 || l_catcount == 1) { %>
																		<% } else if (l_catcount == 2) {
																			if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																				<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																			<% } else if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																				<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																			<% } %>
																		<% } else if (l_catcount == 3) { 
																			if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																				<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																			<% } else if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																				<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																			<% } else if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
																				<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
																			<% } %>
																		<% } else { %>
																		<% } %>
				                          								<%= l_safetydbcasefield.getCaseFieldName() %>
				                          							</td>
				                          							<td>
				                          								<%= l_safetydbcasefield.getCaseFieldTab() %>
				                          							</td>
				                          							<% if (l_qrtemplate != null && l_qrtemplate.getTemplateCaseFields() != null) {
		                          										if (l_qrtemplate.getTemplateCaseFields().indexOf(l_safetydbcasefield.getCaseFieldMnemonic()) != -1) { %>
		                          											<td align="center">
		                          												<i class="glyphicon glyphicon-check"></i>	
		                          											</td> <%
				                          								} else { %>
				                          									<td align="center">
		                     													<i class="glyphicon glyphicon-unchecked"></i>
		                     												</td>
				                          							 <% }
		                          									} else { %>
		                        										<td align="center">
		                     												<i class="glyphicon glyphicon-unchecked"></i>
		                     											</td> <%
		                          									} %>
				                          						</tr>	<%
			                          						}	
			                          					} %>
			                          				</tbody>
			                          			</table>
			                          			<div class="ln_solid"></div>
			                          			
			                          				<% if (! (l_qrtemplate.getState() == RecordState.ACTIVE || l_qrtemplate.getState() == RecordState.DEACTIVATED)) { %>
			                          					<div class="form-group">
															<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
																<button type="button" class="btn btn-primary" onclick="fnback();" value="Back">
																	Back
																</button>
																<button type="button" class="btn btn-success" onclick="fnedit(2);" value="Edit">
																	Edit
																</button>
															</div>
														</div>	
													<% } else { %>
														<div align="center">
															<button type="button" class="btn btn-primary" onclick="fnback();" value="Back">
																Back
															</button>
														</div>
													<% } %>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>		
					</div>
					<div class="clearfix">
					</div>
				</div>
			</div>		
		</csrf:form>
	</body>
</html>