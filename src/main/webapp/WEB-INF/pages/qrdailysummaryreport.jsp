<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.apps.qr.QRDailySummaryReport" %>
<%@page import="java.util.List" %>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%
	List<QRDailySummaryReport>	l_qrdailysummaryreportlist	=	(List<QRDailySummaryReport>) request.getAttribute("dailysummarydata");
	String						l_reporttype				=	(String)	request.getAttribute("fldreporttype");
%>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Quality Review 
		</title>
		<script type="text/JavaScript">
			function fnback () {
				document.getElementById("portal_action").value = "";
				document.frmmain.submit();
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnexport () {
				document.getElementById("portal_action").value = "exportdsr";
				document.getElementById("fldreporttype").value = '<%=l_reporttype%>';	
				if (validateFormParameters()) {
					document.frmmain.submit();
				} else{
					return false;
				}
			}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldreporttype" name="fldreporttype" value="">
			<div class="title_left">
				<h3>Daily Summary Report</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Report Summary
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<table class="table table-striped">
							<thead>
								<tr>
									<th></th>
									<th>Serious</th>
									<th></th>
									<th></th>
									<th></th>
									<th>Non-Serious</th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<thead>
								<tr>
									<th>Date</th>
									<th>Total Reviewed</th>
									<th>Passed</th>
									<th>Failed</th>
									<th>Quality(%)</th>
									<th>Total Reviewed</th>
									<th>Passed</th>
									<th>Failed</th>
									<th>Quality(%)</th>
								</tr>
							</thead>
							<tbody>
								<% if (l_qrdailysummaryreportlist != null && l_qrdailysummaryreportlist.size() > 0) { 
									for (QRDailySummaryReport l_qrdailysummaryreport : l_qrdailysummaryreportlist) { %>
										<tr>
											<td>
												<%= WebUtils.formatForDisplayDate(l_qrdailysummaryreport.getDate(), "EEE, MMM dd ,YYYY") %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getSeriousTotalCaseReviewed() %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getSeriousTotalCasePassed() %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getSeriousTotalCaseFailed() %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getSeriousAverageQuality() %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getNonSeriousTotalCaseReviewed() %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getNonSeriousTotalCasePassed() %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getNonSeriousTotalCaseFailed() %>
											</td>
											<td>
												<%= l_qrdailysummaryreport.getNonSeriousAverageQuality() %>
											</td>
										</tr>
									<% } %>
								<% } else { %>
									<tr>
										<td colspan="9">
											<i>There are no reports in the system.</i>
										</td>
									</tr>
								<% } %>
							</tbody>		
						</table>
						<div class="ln_solid"></div>
						<button type="button" class="btn btn-primary" onclick="fnback();" value="Back">
							Back
						</button>
						<% if (l_qrdailysummaryreportlist != null && l_qrdailysummaryreportlist.size() > 0) { %>
							<button type="button" class="btn btn-primary" onclick="fnexport();" value="Export">
								Export
							</button>
						<% } %>	
					</div>
				</div>
			</div>
		</csrf:form>	
	</body>
</html>