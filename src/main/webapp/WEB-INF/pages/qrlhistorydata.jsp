<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.user.UserHome"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRReviewData"%>
<%@page import="com.sciformix.commons.SciEnums.EmailSendSetting"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@include file="uiFormCreationMethods.jsp"%>

<!DOCTYPE html>
<html>
	<head>
	</head>
<%
	List<QRReviewData> o_qrreviewdatalist = null;
	o_qrreviewdatalist = (List<QRReviewData>) request.getAttribute("qrdatalist");
	DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	UserInfo o_userinfo = (UserInfo) request.getAttribute("loggedinuser");
	String s_reviewTypeName = null;
	List<ObjectIdPair<String,String>>	l_reviewtypelist	=	(List<ObjectIdPair<String,String>>) request.getAttribute("reviewtypelist");
	
	for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) 
	{
		if (o_qrreviewdatalist.get(0).getReviewType().equals(l_reviewtypedata.getObjectSeqId())) 
		{
			s_reviewTypeName = l_reviewtypedata.getObjectName();
		}
	}

%>
<script type="text/JavaScript">
	function fnsubmit(value,caseintrid)
	{
		document.frmmain.portal_action.value	=	value;
		if (validateFormParameters()) {
			document.frmmain.submit();
		}
	}
	function fnback(value)
	{
		document.frmmain.portal_action.value	=	value;
		document.frmmain.submit();
		
	}
	function validateFormParameters () {
		oFormObject = document.forms["frmmain"];
		return validateInputParameter(oFormObject);
	}
	
	function fneditreviewcase(p_sequenceid,daycount) {
		document.frmmain.fldsequenceid.value		=	p_sequenceid;
		document.frmmain.portal_action.value		=	"editqrldetailsblank";
		if (validateFormParameters()) {
			document.frmmain.submit();
		}
	}
	
</script>
	<body>
	<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="">
			<input type="hidden" maxlength="100" name="fldcaseid" value=" <%=o_qrreviewdatalist.get(0).getCaseId() %>" />
			<input type="hidden" name="fldcaseversion"  value="<%=o_qrreviewdatalist.get(0).getCaseVersion() %>" />
			<input type="hidden" name="fldserious" id="fldserious" class="form-control" value="<%=o_qrreviewdatalist.get(0).getCaseSeriousness() %>" />
			<input type="hidden" name="fldreviewtype" id="fldreviewtype" class="form-control" value="<%=o_qrreviewdatalist.get(0).getReviewType()%>" />
			<input type="hidden" id="fldsequenceid" name="fldsequenceid" value="">
			<input type="hidden" id="fldreporttype" name="fldreporttype" value="">
			<div class="title_left">
				<h3>Case Review Summary</h3> 
			</div>
			<div class="row">
				<div class="col-sm-12" style="align: center;">
						<h4 style="padding: 20px; background-color: #e6ffe6; color: #45a945; border-radius: 10px;" align= "center">
					<%=s_reviewTypeName %> completed for Case Id:<%=o_qrreviewdatalist.get(0).getCaseId() %> and Version: <%=o_qrreviewdatalist.get(0).getCaseVersion() %>
					</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>
								Case Details
							</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content" style="display: block;">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Case Id</th>
										<th>Case Version</th>
										<th>Initial Receive Date</th>
										<th>Seriousness</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
										<%=o_qrreviewdatalist.get(0).getCaseId() %>
										</td>
										
										<td>
										<%=o_qrreviewdatalist.get(0).getCaseVersion() %>
										</td>
										
										<td>
										<%= WebUtils.formatForDisplayDate(o_qrreviewdatalist.get(0).getCaseRecvDate(), "dd-MMM-YYYY") %>
										</td>
										
										<td>
										<%=o_qrreviewdatalist.get(0).getCaseSeriousness() %>
										</td>
									
									</tr>
									
								</tbody>		
							</table>
							
						</div>
					</div>
				</div>
			</div>	
			
			
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
						
							<h2>
								Report Summary for <%=s_reviewTypeName %>
								
							</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content" style="display: block;">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Review Round</th>
										<th>Case Id</th>
										<th>Case Associate</th>
										<th>Case Reviewer</th>
										<th>Quality(%)</th>
										<th>Total Fields Reviewed</th>
										<th>Case Result</th>
										
										<th></th>
									</tr>
								</thead>
								<tbody>
								
									<% if (o_qrreviewdatalist != null) {%>
									<%for (QRReviewData o_qrreviewdata : o_qrreviewdatalist){%>
											<tr>
												<td>
													<%= o_qrreviewdata.getReviewRound() %>
												</td>
												<td>
													<%= o_qrreviewdata.getCaseId() %>
												</td>
												<td>
													<%=o_qrreviewdata.getCaseAssociateName() %>
												</td>
												<td>
													<%=o_qrreviewdata.getCreatedByName()%>
												</td>
												<td>
													<%=o_qrreviewdata.getCaseQuality() %>
												</td>
												<td>
													<%= o_qrreviewdata.getFieldsReviewed() %>
												</td>
												<td>
													<%= o_qrreviewdata.getCaseResult() %>
												</td>
											
												<td>
												 <%if(o_qrreviewdata.getCreatedBy()==o_userinfo.getUserSeqId()){ %>
													<a HREF="javascript:fneditreviewcase('<%=o_qrreviewdata.getSequenceId()%>','<%=df.format(o_qrreviewdata.getCreatedOn())%>')">
														<i class="fa fa-pencil"></i>
													</a>
												
												<%} %> 
												</td>
											</tr>
										<% }} %>
								</tbody>		
							</table>
							
						</div>
					</div>
				</div>
			</div>	
			
			<div class="form-group">
				<div align= "center">
					<button type="button" class="btn btn-primary" onclick="fnback('qualityreview');">
						Back
					</button>
				   <!--  <button type="button" class="btn btn-success"  onclick="fnback();">
				     	Edit
				    </button> -->
				    <button type="button" class="btn btn-success"  onclick="fnsubmit('startnewmrround');">
				    	Start New Round
				    </button>
				</div>
			</div> 
			
			
		</csrf:form>
	</body>
</html>