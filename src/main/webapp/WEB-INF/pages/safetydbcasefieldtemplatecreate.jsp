<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
	<head>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
		function validateFormParameters() {
			clearErrorMessages();
			oFormObject = document.forms["frmmain"];
			return validateInputParameter(oFormObject);
		}
		function fncreatenewscft () {
			clearErrorMessages();
			if (document.frmmain.fldtemplatename.value == "") {
				displayErrorMessage("UI-1005");
				return false;
			}
			if (document.frmmain.fldsafetydbname.value == "") {
				displayErrorMessage("UI-1041");
				return false;
			}
			if (document.frmmain.fldsafetydbversion.value == "") {
				displayErrorMessage("UI-1042");
				return false;
			}
			if (document.frmmain.fldcaseidalias.value == "") {
				displayErrorMessage("UI-1043");
				return false;
			}
			if (document.frmmain.fldcaseversionalias.value == "") {
				displayErrorMessage("UI-1044");
				return false;
			}
			if (document.frmmain.fldcaseidalias.value == document.frmmain.fldcaseversionalias.value) {
				displayErrorMessage("UI-1045");
				return false;
			}
			if(document.frmmain.fldmasterfile.value == "") {
				displayErrorMessage("UI-1021");
				return false;
			}
			document.frmmain.portal_action.value	=	"createnewscft";
			if(validateFormParameters()){ 
				document.frmmain.submit();
			}
		}
		function fncancel () {
			document.frmmain.submit();
		}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="SCFT">
			<div role="main">
				<div class="page_title">
					<div class="title_left">
						<h3>Template Maintenance</h3>
					</div>
				</div>
				<div class="clearfix">
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>
									Create Safety DB Case Field Template
								</h2>
								<div class="clearfix">
								</div>
							</div>
							<div class="x_content">
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldtemplatename">
										Template Name
										<span class="required"> *</span>
									</label>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input class="form-control" type="text" id="fldtemplatename" name="fldtemplatename" value="" maxlength="64">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldsafetydbname">
										Safety DB Name
										<span class="required"> *</span>
									</label>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input class="form-control" type="text" id="fldsafetydbname" name="fldsafetydbname" value="" maxlength="64">
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldsafetydbversion">
										Safety DB Version
										<span class="required"> *</span>
									</label>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input class="form-control" type="text" id="fldsafetydbversion" name="fldsafetydbversion" value="" maxlength="64">
										<!-- Pattern need to be fetched from application settings -->
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcaseidalias">
										Case Id Alias
										<span class="required"> *</span>
									</label>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input class="form-control" type="text" id="fldcaseidalias" name="fldcaseidalias" value="" maxlength="64">
										<!-- Pattern need to be fetched from application settings -->
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcaseversionalias">
										Case Version Alias
										<span class="required"> *</span>
									</label>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input class="form-control" type="text" id="fldcaseversionalias" name="fldcaseversionalias" value="" maxlength="64">
										<!-- Pattern need to be fetched from application settings -->
									</div>
								</div>		
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldmasterfile">
										Upload Case File
										<span class="required"> *</span>
									</label>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input type="file" name="fldmasterfile" id="fldmasterfile">
									</div>
								</div>		
								<div class="ln_solid"></div>
								<div class="form-group">
			                    	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
										<button type="button" class="btn btn-primary" onclick="fncancel();">
											Cancel
										</button>
			                        	<button type="button" class="btn btn-success" value="Submit" onclick="fncreatenewscft();">
			                        		Submit
			                        	</button>
			                        </div>
			                	</div>
							</div>
						</div>
					</div>		
				</div>
				<div class="clearfix">
				</div>
			</div>		
		</csrf:form>
	</body>
</html>