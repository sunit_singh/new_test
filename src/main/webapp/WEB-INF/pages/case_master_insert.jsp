<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
	<head>
	<script src="js/moment/moment.min.js"></script>
		
	<script>
  		$(document).ready(function() {
	    	$('#'+'<%=QRConstant.CASE_MASTER_INTERNAL_IRD%>').daterangepicker({
	        	singleDatePicker: true,
	        	singleClasses: "picker_3",
	          	minDate : '01/01/2012',
				maxDate : moment(),
				dateLimit : {
					days : 365
				},
				 locale: {
			            format: 'DD-MMM-YYYY'
			        }
	        	});
      	});
    </script>
    <!-- /bootstrap-daterangepicker -->
    
	<script type="text/JavaScript">
			function validateFormParameters () {
				clearErrorMessages();
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			
			function insertcasefilesubmit () {
				document.frmmain.portal_action.value	=	"insertcaserecord";
				if(!validateInput() && validateFormParameters()){ 
					document.frmmain.submit();
				}
			}
			function validateDate() {
				  var dtValue =document.getElementById("<%=QRConstant.CASE_MASTER_INTERNAL_IRD%>").value;
				  var dtRegex = new RegExp("^([0]?[1-9]|[1-2]\\d|3[0-1])-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)-[1-2]\\d{3}$", 'i');
				  return dtRegex.test(dtValue);
			}
			function validateInput()
			{
				clearErrorMessages();
				var bErrorDetected = false;
				var caseid=document.getElementById("<%=QRConstant.CASE_MASTER_INTERNAL_COL_CASE_ID%>").value;
				var version=document.getElementById("<%=QRConstant.CASE_MASTER_INTERNAL_COL_VERSION%>").value;
				var ird=document.getElementById("<%=QRConstant.CASE_MASTER_INTERNAL_IRD%>").value;
				var caseseriousness=document.getElementById("<%=QRConstant.CASE_MASTER_INTERNAL_COL_SERIOUSNESS%>").value;
				var	todaydate	=moment();
				
				if(caseid==""||caseid.trim()==""){
					displayErrorMessage("UI-1055");
					bErrorDetected = true;
				}else if(version==""||version.trim()=="")
				{
					displayErrorMessage("UI-1056");
					bErrorDetected = true;
				}
				else if(caseseriousness==""||caseseriousness.trim()=="")
				{
					displayErrorMessage("UI-1072");
					bErrorDetected = true;
				}
				return bErrorDetected;
			} 
			function fncancel () {
				document.frmmain.submit();
			}
			function checkFileColumns() {
				document.frmmain.portal_action.value	=	"checkFileColumns";
				if(validateFormParameters()){
					document.frmmain.submit();
				}
			}
		</script>
	</head>
	<body>
	<%	
		Map<String,List<String>> enableUdfMap= null; 
		Map<String,String>			l_fileformats	=	null;
		Map<String,String>	l_columnmapping = null;
		String selectedfileformat = null;
		List<UserInfo> l_projectUserList = null;
		
		enableUdfMap = (Map<String,List<String>>) request.getAttribute("enableUdfList");
		l_fileformats	=	(Map<String,String>) request.getAttribute("fileformats");
		l_columnmapping	=	(Map<String,String>)request.getAttribute("columnmappingdetails");
		selectedfileformat	=	(String)request.getAttribute("selfileformat");
		l_projectUserList	=	(List<UserInfo>)request.getAttribute("list_of_users");
		int udfDiplayNameIndex = 0;
		int udfDataTypeIndex = 1;
	%>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldfiletype" name="fldfiletype" value="QRLCM">
			<div role="main">
				<div class="page_title">
					<div class="title_left">
						<h3>Insert Case Master  </h3>
					</div>
				</div>
				<div class="clearfix">
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title ">
								
								<h2>
									Insert Case Master Record
								</h2>
								<div class="clearfix">
								</div>
							</div>
							<div class="x_content" >
								
							<div  id = "insertform" >
								<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldfileformat">
												File Format
												<span class="required"> *</span>
											</label>
											
											<div class="col-md-4 col-sm-4 col-xs-12">
			                       				<select class="form-control" name="fldfileformat" id="fldfileformat" onchange= "checkFileColumns()">
			                       				
			                       				<%if (StringUtils.isNullOrEmpty(selectedfileformat)) {%>
			                       					<option value ="select" selected="selected">Select</option>
			                       					<% for (String l_fileformatcode : l_fileformats.keySet()) { %>
														<option value ="<%= l_fileformatcode%>"><%= l_fileformats.get(l_fileformatcode)%></option>
													<%}
													} 
			                       				else{
			                       					for (String l_fileformatcode : l_fileformats.keySet()) { 
														if(l_fileformats.get(l_fileformatcode).equals(selectedfileformat))
														{%>
															<option value ="<%= l_fileformatcode%>" selected="selected"><%= l_fileformats.get(l_fileformatcode)%></option>
														<%}else{ %>
														<option value ="<%= l_fileformatcode%>"><%= l_fileformats.get(l_fileformatcode)%></option>
														<%}
			                       					}}
													%>
														<%-- <option value ="<%= l_fileformatcode%>" selected="selected"><%= l_fileformats.get(l_fileformatcode)%></option>
														<%}else{ %>
														<option value ="<%= l_fileformatcode%>"><%= l_fileformats.get(l_fileformatcode)%></option>
													<% }
													} --%>
													
												</select> 
											</div>
								</div>	
								
								<%if(l_columnmapping !=null){
								for (String l_internalname : l_columnmapping.keySet()) { %>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">
											<%if (l_internalname.startsWith("UDF")){
												if(enableUdfMap.containsKey(l_internalname)){%>
												<%=l_columnmapping.get(l_internalname) %>
											<%}}else{ %>
												<%=l_columnmapping.get(l_internalname) %>
												<span class="required"> *</span>
											<%} %>
											
										</label>
										
										<% if(l_internalname.equals(QRConstant.CASE_MASTER_INTERNAL_IRD)){%>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<input type = "text" name ="<%=l_internalname %>" id ="<%=l_internalname %>" class ="form-control" placeholder = "DD-MMM-YYYY" >
											</div>
										<% } else if(l_internalname.equals(QRConstant.CASE_MASTER_INTERNAL_DENAME)){%>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<select  name ="<%=l_internalname %>" id ="<%=l_internalname %>" class ="form-control">
													<option value="-1">(Select a user)</option>
													<% for (UserInfo l_userinfo : l_projectUserList) { %>
														<option value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
													<% } %>
												</select>
											</div>
										<%} else if(l_internalname.equals(QRConstant.CASE_MASTER_INTERNAL_COL_SERIOUSNESS)){%>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<select  name ="<%=l_internalname %>" id ="<%=l_internalname %>" class ="form-control">
													<option value="">Select Seriousness</option>
													<option value ="Serious">Serious</option>
													<option value ="Non-Serious">Non-Serious</option>
												</select>
											</div>
										<%} else if (l_internalname.startsWith("UDF")){ 
											
											if(enableUdfMap.containsKey(l_internalname)){
											if (enableUdfMap.get(l_internalname).get(1) != null) { %>
												<div class="col-md-4 col-sm-4 col-xs-12">
													<select  name ="<%=l_internalname %>" id ="<%=l_internalname %>" class ="form-control">
														<% for (String l_udfvalue : StringUtils.splitOnComma(enableUdfMap.get(l_internalname).get(1))) { 
															String[]	l_udfdetails = l_udfvalue.split(":"); %>
															<option value ="<%= l_udfdetails[0]%>"><%= l_udfdetails[1]%></option>
														<% } %>
													</select>
												</div>
										 <% } else {%>
										 		<div class="col-md-4 col-sm-4 col-xs-12">
													<input type = "text" name ="<%=l_internalname %>" id ="<%=l_internalname %>" class ="form-control" >
												</div>
										 <% } %>
											
										<% }} else { %>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<input type = "text" name ="<%=l_internalname %>" id ="<%=l_internalname %>" class ="form-control" >
											</div>
										<% } %>
									</div>	
							    <% }}%>
								
								<div class="ln_solid"></div>
								
								<div class="form-group">
			                    	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
										<button type="button" class="btn btn-primary" onclick="fncancel();">
											Cancel
										</button>
			                        	<button type="button" class="btn btn-success" value="Submit" onclick="insertcasefilesubmit();">
			                        		Submit
			                       		 </button>
			                        </div>
			                	</div>
								
							</div>
							</div>
						</div>
					</div>		
				</div>
				<div class="clearfix">
			</div>
			</div>	
		</csrf:form>
	</body>
</html>