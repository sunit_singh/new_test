<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@ taglib
	uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld"
	prefix="csrf"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRReviewData"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>QR Graph</title>

<%
	Integer l_startdatecount = (Integer) request.getAttribute("fldstartdatecount"),
			l_enddatecount = (Integer) request.getAttribute("fldenddatecount"),
			l_seriousCaseThreshold = (Integer) request.getAttribute("seriouscasethreshold"),
			l_nonSeriousCaseThreshold = (Integer) request.getAttribute("nonseriouscasethreshold");
	List<QRReviewData> l_qualitylid = (List<QRReviewData>) request.getAttribute("qualityloglist");
	DateFormat df = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
	DateFormat dateOutput = new SimpleDateFormat("dd-M-yyyy");
%>
<script src="js/moment/moment.min.js"></script>
<script type="text/JavaScript">

/* Date Range Picker */
	var startDate;
	var endDate;
	var startDateCount =<%=l_startdatecount%>;
	var endDateCount =<%=l_enddatecount%>;
	startDateCount = -startDateCount;
	endDateCount = -endDateCount;

	$(document)
			.ready(
					function() {
						$('#reportrange_right')
								.daterangepicker(
										{
											startDate : moment().subtract(
													startDateCount, 'days'),
											endDate : moment().subtract(
													endDateCount, 'days'),
											minDate : '01/01/2012',
											maxDate : moment(),
											dateLimit : {
												days : 365
											},
											showDropdowns : true,
											showWeekNumbers : true,
											timePicker : false,
											timePickerIncrement : 1,
											timePicker12Hour : true,
											ranges : {
												'Last 7 Days' : [
														moment().subtract(6,
																'days'),
														moment() ],
												'Last 30 Days' : [
														moment().subtract(29,
																'days'),
														moment() ],
												'This Month' : [
														moment().startOf(
																'month'),
														moment() ],
												'Last Month' : [
														moment()
																.subtract(1,
																		'month')
																.startOf(
																		'month'),
														moment().subtract(1,
																'month').endOf(
																'month') ]
											},
											opens : 'right',
											buttonClasses : [ 'btn btn-default' ],
											applyClass : 'btn-small btn-primary',
											cancelClass : 'btn-small',
											format : 'MM/DD/YYYY',
											separator : ' to ',
											locale : {
												applyLabel : 'Submit',
												cancelLabel : 'Clear',
												fromLabel : 'From',
												toLabel : 'To',
												customRangeLabel : 'Custom',
												daysOfWeek : [ 'Su', 'Mo',
														'Tu', 'We', 'Th', 'Fr',
														'Sa' ],
												monthNames : [ 'January',
														'February', 'March',
														'April', 'May', 'June',
														'July', 'August',
														'September', 'October',
														'November', 'December' ],
												firstDay : 1
											}
										},
										function(start, end) {
											$('#reportrange_right span')
													.html(
															start
																	.format('D MMMM YYYY')
																	+ ' - '
																	+ end
																			.format('D MMMM YYYY'));
											startDate = start.startOf('day');
											endDate = end.startOf('day');
										});
						$('#reportrange_right span').html(
								moment().subtract(startDateCount, 'days')
										.format('D MMMM YYYY')
										+ ' - '
										+ moment().subtract(endDateCount,
												'days').format('D MMMM YYYY'));
						startDate = moment().startOf('day').subtract(
								startDateCount, 'days');
						endDate = moment().startOf('day').subtract(
								endDateCount, 'days');
					});

	
	
	/* Plot Graph */
	$(function() {
		var data = [];
		var data1 = [];
		var th_serious = [];
		var th_nonserious = [];
		var chartColours = [ '#3F97EB', '#96CA59', '#3F97EB', '#96CA59',
				'#2c7282', '#5a8022', '#2c7282' ];
		
		<%for (QRReviewData qrdata : l_qualitylid) {%>
				
			<%String outputDate = df.format(qrdata.getCreatedOn()).split(" ")[0];%>
			th_nonserious.push([<%=dateOutput.parse(outputDate).getTime()%>, <%=l_nonSeriousCaseThreshold%>]);
			th_serious.push([<%=dateOutput.parse(outputDate).getTime()%>, <%=l_seriousCaseThreshold%>]);
			
			<%if (qrdata.getCaseSeriousness().equals("Serious"))
			{%>
				data.push([<%=dateOutput.parse(outputDate).getTime()%>,	<%=qrdata.getCaseQuality()%>]);
			<%} else {%>
				data1.push([<%=dateOutput.parse(outputDate).getTime()%>,<%=qrdata.getCaseQuality()%>]);
			<%}
		}%>
		var datasets = {
				"Serious-Threshold" : {
					data : th_serious,
					lines : {
						show : false,
						lineWidth : 0,
						fill : false
					},
					dashes : {
						show : true,
						lineWidth : 3
					},
					points : {
						show : false,
						radius : 0.5,
						symbol : "circle",
						lineWidth : 3.0
					}
	
				},
				"Non-Serious-Threshold" : {
					data : th_nonserious,
					lines : {
						show : false,
						lineWidth : 0,
						fill : false
					},
					dashes : {
						show : true,
						lineWidth : 3
					},
					points : {
						radius : 0.5,
						symbol : "circle",
						lineWidth : 3.0
					}
	
				},
				"Serious" : {
					label : "Serious",
					data : data,
					lines : {
						fillColor : "rgba(150, 202, 89, 0.12)"
					}, //#96CA59 rgba(150, 202, 89, 0.42)
					points : {
						show : true,
						radius : 4.5,
						symbol : "circle",
						lineWidth : 3.0
					}
				},
				"Non-Serious" : {
					label : "Non-Serious",
					data : data1,
					lines : {
						fillColor : "rgba(150, 202, 89, 0.12)"
					}, //#96CA59 rgba(150, 202, 89, 0.42)
					points : {
						show : true,
						radius : 4.5,
						symbol : "circle",
						lineWidth : 3.0
					}
				}
			};
	
			// hard-code color indices to prevent them from shifting as
			var i = 0;
		$.each(datasets, function(key, val) {
			val.color = i;
			++i;
		});

		// insert checkboxes 
		var choiceContainer = $("#choices");
		$.each(datasets, function(key, val) {
			if (key == "Serious" || key == "Non-Serious") {
				choiceContainer.append("<input type='checkbox'  name='" + key
						+ "' checked='checked' id='id" + key
						+ "' onclick='checkmode(" + key + ")' ></input>"
						+ "<label for='id" + key + "'>" + val.label
						+ "</label> &nbsp;&nbsp;&nbsp;");
			}
		});

		choiceContainer.find("input").click(plotAccordingToChoices);

		var threshold = null;
		function plotAccordingToChoices() {
			var data = [];
			threshold = "";
			choiceContainer.find("input:checked").each(function() {
				var key = $(this).attr("name");
				threshold += key;
				if (key && datasets[key]) {
					data.push(datasets[key]);
					if (key == "Serious" || key == "Non-Serious") {
						data.push(datasets[key + "-Threshold"]);
					}
				}
			});
			choiceContainer.find("input").each(function() {
				var key = $(this).attr("name");
				threshold += key;
				
				/* if (key && datasets[key]) {
					data.push(datasets[key]);
					if(key == "Serious"||key == "Non-Serious"){
						data.push(datasets[key+"-Threshold"]);
					}
				} */
			});
			/* var options =  */
			var thresholdmarking = [];
			if (threshold == "Non-Serious") {
				thresholdmarking = [ //show line on threshold value
				{
					color : '#96CA59',
					lineWidth : 3,
					yaxis : {
						from : 60,
						to : 60
					},
				}, ]
			}
			;

			if (threshold == "Serious") {
				thresholdmarking = [ //show line on threshold value
				{
					color : '#3F97EB',
					lineWidth : 3,
					yaxis : {
						from : 40,
						to : 40
					}
				}, ]
			}
			;
			if (threshold == "SeriousNon-Serious"
					|| threshold == "Non-SeriousSerious") {
				thresholdmarking = [ //show line on threshold value
				{
					color : '#96CA59',
					lineWidth : 3,
					yaxis : {
						from : 60,
						to : 60
					}
				}, {
					color : '#3F97EB',
					lineWidth : 3,
					yaxis : {
						from : 40,
						to : 40
					}
				} ]
			}
			;
			var options = {
				grid : {
					show : true,
					aboveData : true,
					color : "#3f3f3f",
					labelMargin : 10,
					axisMargin : 0,
					borderWidth : 0,
					borderColor : null,
					minBorderMargin : 0,
					clickable : true,
					hoverable : true,
					autoHighlight : true,
					mouseActiveRadius : 100,
				},
				legend : {
					position : "ne",
					margin : [ 0, -25 ],
					noColumns : 0,
					labelBoxBorderColor : null,
					labelFormatter : function(label, series) {
						return label + '&nbsp;&nbsp;';
					},
					width : 40,
					height : 1
				},
				colors : chartColours,
				shadowSize : 0,
				series : {
					lines : {
						show : true,
						fill : true
					},
				},
				yaxis : {
					min : 0,
					max : 100
				},
				xaxis : {
					mode : "time",
					tickSize : [ 1, "day" ],
					timeformat : "%d/%m",
					timezone : "browser",
				},
			};

			if (data.length > 0) {
				$.plot("#placeholder", data, options);
				$(
						"<div id='tooltip' class='tooltip' data-toggle='tooltip'></div>")
						.css({
							position : "absolute",
							display : "block",
							border : "1px solid #000000",
							padding : "auto",
							"background-color" : "#000000",
							"color" : "#fff",
							opacity : 0.80
						}).appendTo("body");

				$("#placeholder")
						.bind(
								"plothover",
								function(event, pos, item) {
									if (item) {
										var x = item.datapoint[0].toFixed(2), y = item.datapoint[1]
												.toFixed(2);

										$("#tooltip").html(
												item.series.label + " : " + y
														+ " %").css({
											top : item.pageY - 30,
											left : item.pageX - 50
										}).fadeIn(200);
									} else {
										$("#tooltip").hide();
									}
								});
			}
		}
		plotAccordingToChoices();
	});
	
	
	function fngetbydategraph() {
		document.getElementById("portal_action").value = "view_graph";
		var	todaydate	=	moment().startOf('day');
		document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
		document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days');
		if (validateFormParameters())
		 {
			document.frmmain.submit();
		 }
	}
	function validateFormParameters () {
		oFormObject = document.forms["frmmain"];
		return validateInputParameter(oFormObject);
	}
</script>
</head>
<body>
	<csrf:form name="frmmain" id="frmmain" class="form-horizontal"
		method="post" action="LandingServlet" enctype="multipart/form-data">
		<input type="hidden" id="portal_appid" name="portal_appid"
			value="QR-PORTAL-APP">
		<input type="hidden" id="fldstartdatecount" name="fldstartdatecount"
			value="">
		<input type="hidden" id="fldenddatecount" name="fldenddatecount"
			value="">
		<input type="hidden" id="portal_action" name="portal_action" value="">

		<div id="mainContent">
			<%@include file="uiFormCreationMethods.jsp"%>
			<div class="page-title">
				<div class="title_left">
					<h3>Quality Review Graph</h3>
				</div>
			</div>
			<!--  -->
			<div class="x_panel">
				<div class="x_title">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="reportrange_right"
							class="pull-left col-md-4 col-sm-12 col-xs-12"
							style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
							<i class="glyphicon glyphicon-calendar fa fa-calendar"></i> <span></span>
							<b class="caret"></b>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-12 ">
							<button type="button" class="btn btn-success"
								onclick="fngetbydategraph();" value="Search">Search</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<%if(!l_qualitylid.isEmpty() ){ %>
					<div class="x_content">
						<!-- <h2>
							<small>Serious Cases VS Non Serious Cases</small>
						</h2> -->
						<!-- placeholder div For Graph Plotting -->
	
						<div id="placeholder" class="demo-placeholder"></div>
	
					</div>
					<!-- choices div for check boxes -->
					<p id="choices"></p>
				<%}else{ %>
					No Record Found !
				<%} %>
			</div>
		</div>
	</csrf:form>
</body>
</html>

