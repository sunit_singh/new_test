<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.commons.SciConstants.StringConstants"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%
int	l_sequenceid	=	(Integer)	request.getAttribute("fldsequenceid");
String sNewPortalAppId = (String) request.getAttribute("new-portalappid");
System.out.println(sNewPortalAppId+StringConstants.PERIOD+QRConstant.DOWNLOAD_ALLOWED+"==>>"+WebUtils.getAuthorizedProjectId(request));
String sDownloadAllowed = ConfigHome.getPrjConfigSettings(sNewPortalAppId+StringConstants.PERIOD+QRConstant.DOWNLOAD_ALLOWED,WebUtils.getAuthorizedProjectId(request));
%>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Quality Review Confirmation
		</title>
		<script type="text/JavaScript">
			function fnreviewnext () {
				document.getElementById("portal_action").value = "qualityreview";
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnviewdetails(p_sequenceid) {
				document.frmmain.fldsequenceid.value	=	'<%= l_sequenceid %>';
				document.frmmain.portal_action.value	=	"viewqrldetails";
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function downloadReviewInExcel() {
				document.getElementById("portal_action").value = "downloadReviewExcel";	
				document.getElementById("fldsequenceid").value = '<%= l_sequenceid %>';
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldsequenceid" name="fldsequenceid" value="">
			
			<div class="title_left">
				<h3>Quality Review Confirmation</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Details
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<label>The Case Review data has been saved successfully.</label>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content" style="display: block;">
						<button type="button" class="btn btn-primary btn-sm" value="Review" onclick="fnreviewnext();">
							Review Next Case
						</button>
						<button type="button" class="btn btn-primary btn-sm" value="Report" onclick="fnviewdetails();">
							View Current Case Report
						</button>
						<%if(sDownloadAllowed!=null && sDownloadAllowed.equalsIgnoreCase("true")){ %>
						<button type="button" class="btn btn-primary btn-sm" value="Download" onclick="downloadReviewInExcel();">
							Download
						</button>
						<%} %>
						<%-- Download: <A HREF="javascript:downloadReviewInExcel();"><u> <%= request.getAttribute("caseId")%></u><!-- <i class="glyphicon glyphicon-save-file"></i> --></A> --%>
					</div>
				</div>
			</div>
		</csrf:form>
	</body>
</html>			