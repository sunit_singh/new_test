<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.mail.MailTemplate"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
			function validateFormParameters() {
				clearErrorMessages();
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function editTemplate(pid, clickValue) {
					var form = document.getElementById("frmmain");
					document.getElementById("portal_action").value =clickValue;
					document.getElementById("projsettings_appid").value ='EMAIL-TEMP-APP';
					document.getElementById("click_Operation_Project_Id").value = pid;
					if(!validateInput(pid,clickValue) && validateFormParameters()){ 
						form.submit();
					}
			}
			function cancelPage() {	
				document.getElementById("portal_action").value ='cancel';
				document.getElementById("projsettings_appid").value ='EMAIL-TEMP-APP';
				document.frmmain.submit();
			}
			function cancelUpdateTemplate(value, id) {
				document.getElementById("portal_action").value =value ;
				document.getElementById("projsettings_appid").value ='EMAIL-TEMP-APP' ;
				document.getElementById("click_Operation_Project_Id").value = id;
				document.frmmain.submit();
			}
			function validateInput(pid,clickValue) {
				clearErrorMessages();
				var bErrorDetected = false;
				var pattern = /[a-zA-Z0-9][a-zA-Z0-9\\s_-]+[a-zA-Z0-9]/;
				if(pid=='new')
				{	var templateName=document.getElementById("template_Name").value;
					var mailPurpose=document.getElementById("purpose").value;
					var mailSubject=document.getElementById("subject").value;
					var mailBody=document.getElementById("body").value;
					
					if(templateName==""||templateName.trim()==""){
						displayErrorMessage("UI-1005");
						bErrorDetected = true;
					} else if(!pattern.test(templateName))
					{	displayErrorMessage("UI-1022");
						bErrorDetected = true;
					}else if(mailPurpose==""||mailPurpose.trim()==""){
						displayErrorMessage("UI-1023");
						bErrorDetected = true;
					}else if(mailSubject==""||mailSubject.trim()==""){
						displayErrorMessage("UI-1024");
						bErrorDetected = true;
					}else if(mailBody==""||mailBody.trim()==""){
						displayErrorMessage("UI-1025");
						bErrorDetected = true;
					}
					return bErrorDetected;
				} else
					if(clickValue=='updateTemplate'){
					var mailSubject=document.getElementById("mail_Subject").value;
					var mailBody=document.getElementById("mail_body").value;
					if(mailSubject==""||mailSubject.trim()==""){
						displayErrorMessage("UI-1024");
						bErrorDetected = true;
					}else if(mailBody==""||mailBody.trim()==""){
						displayErrorMessage("UI-1025");
						bErrorDetected = true;
					}
					return bErrorDetected;
				}
				return bErrorDetected;
			}
		</script>
	</head>	
<body>
<%MailTemplate mailTemplate =(MailTemplate)request.getAttribute("mailTemplate"); %>
<csrf:form name="frmmain" id="frmmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
<input type="hidden" id="portal_action" name="portal_action" value="">
<input type="hidden" id="click_Operation_Project_Id" name="click_Operation_Project_Id" value="">
<input type="hidden" id="projsettings_appid" name="projsettings_appid"  />
	
 	<div class="page-title">
 		<div class="title_left">
 		<%if (mailTemplate!=null) {%>
                	<h3>Email Template Updation</h3>
                <%}else{ %>
				  <h3>Email Template Creation</h3>
				  <%} %>
 		
 		
 		</div></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <%if (mailTemplate!=null) {%>
                	<div class="x_title"><h3><small> Update Email Template</small>
                <%}else{ %>
				  <div class="x_title"><h3><small> Create a Email Template</small>
				  <%} %>
					<%if(request.getAttribute("successMsg")!=null){ %>
					<b style="color: blue;"><%=request.getAttribute("successMsg") %></b>
					<%} %>
					</h3> 
				 
				  	<div class="clearfix"></div>
				  </div>
				  <div class="x_content">
                      	<div class="form-group">	
	                  		<b>
	                  			<label class="control-label col-md-3 col-sm-3 col-xs-12">
	                  				Mail Template Name
	                  				<%if (mailTemplate==null) {%>
	                  				<span class="required"> *</span>
	                  				<%} %>
	                  			</label>
	                  		</b>
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  		<%if (mailTemplate!=null) {%>
	                  			<label class="control-label"><%=mailTemplate.getTemplateName()%></label>
            				<%} else{ %>
            					<input type="text"  name="new_Template_Name" id="template_Name"
            					class="form-control" maxlength= 64>
            				<%} %>
	                  		</div>
	                  	</div>
	                  	
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
	                  			Project Purpose
	                  			<%if (mailTemplate==null) {%>
	                  			<span class="required"> *</span>
	                  			<%} %>
	                  		</label>
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  		<%if (mailTemplate!=null) {%>
	                  			<label class="control-label"><%=mailTemplate.getPurpose()%></label>
            				<%} else{ %>
	                  			<input type="text" name="new_Purpose" id="purpose" class="form-control">
	                  		<%} %>	
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
	                  			Email Subject<span class="required"> *</span>
	                  		</label>
	                  		<div class="col-md-4 col-sm-4 col-xs-12 ">
	                  		<%if (mailTemplate!=null) {%>
	                  			<input type="text" name="mail_Subject" class="form-control" id="mail_Subject" value="<%=mailTemplate.getSubjectTemplate()%>">
	                  		<%} else{ %>
	                  			<input type="text" name="new_Subject" class="form-control" id="subject">
	                  		<%} %>
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12" >
	                  			Email Body<span class="required"> *</span>
	                  		</label>
	                  		<div class="col-md-8 col-sm-8 col-xs-12">
	                  		<%if (mailTemplate!=null) {%>
	                  			<textarea id="mail_body" name="mail_body" rows="10" cols="80" 
	                  			class="resizable_textarea form-control col-md-7 col-xs-12"><%=mailTemplate.getBodyTemplate() %></textarea>
	                  		<%} else{ %>
	                  			<textarea id="body" name="new_body" rows="10" cols="80" 
	                  			class="resizable_textarea form-control col-md-7 col-xs-12"></textarea>
	                  		<%} %>
	                  		</div>
	                  	</div>
	                  	<div class="ln_solid"></div>	
                  	 	<div class="form-group">	
                  	  		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  		<%if (mailTemplate!=null) {%>
	                  			<button type="button" class="btn btn-primary" onclick="cancelUpdateTemplate('EditTemplateView', '<%=mailTemplate.getSequenceId()%>')">Cancel</button> 
	                  			<button type="button" class="btn btn-success" onclick="editTemplate('<%=mailTemplate.getSequenceId()%>','updateTemplate')">Update</button>
	                  		<%} else{ %>
	                  			<button type="button" class="btn btn-primary" onclick=" cancelPage()">Cancel</button>
	                  			<button type="button" class="btn btn-success" onclick="editTemplate('-1','create')">Submit</button>
	                  		<%} %>
					 			
							</div>		
	              		</div>	 
			  </div>
             </div>
          </div>
 		</div>
</csrf:form>
</body>
</html>