<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%@page import="com.sciformix.commons.SciError"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>
<%@page import="com.sciformix.sciportal.apps.AppUserPreferenceMetadata"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplate"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@page import="com.sciformix.commons.utils.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body >

<csrf:form name="PROJSETTINGS_form" id="PROJSETTINGS_form" action="LandingServlet" method="post" enctype="multipart/form-data" data-parsley-validate="" class="form-horizontal form-label-left"> 
<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" />
<input type="hidden" id="portal_action" name="portal_action" value="" />
<input type="hidden" id="portal_action_mode" name="portal_action_mode" value="" />
<div>
<%@include file="uiFormCreationMethods.jsp"%>

<%
String sAppId = null;
IPortalApp oPortalApp = null;
String sNewPortalAction = (String) request.getAttribute("new-portalaction");
%>
<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="NGV-PORTAL-APP" />
<input type="hidden" id="clickValue" name="clickValue"  />

<!-- NewPortalAction: <%=sNewPortalAction%> -->
<%
if (sNewPortalAction == null || sNewPortalAction.trim().length() == 0)
{
	%>
	<%@include file="ngv_projsettings_tplrslist.jsp" %>
	<%
}
else if (sNewPortalAction.equals("ngv_tplrs_viewedit"))
{
	%>
	<%@include file="ngv_projsettings_tplrsview.jsp" %>
	<%
}
else if (sNewPortalAction.startsWith("ngv_tplrsimport"))
{
	%>
	<%@include file="ngv_projsettings_tplrsimport.jsp" %>
	<%
}
else if (sNewPortalAction.startsWith("ngv_tplrs_tpl"))
{
	%>
	<%@include file="ngv_projsettings_tplrscreate.jsp" %>
	<%
}
else if (sNewPortalAction.equals("ngv_template_createnew_2"))
{
	%>
	<%@include file="ngv_projsettings_tplrscreate.jsp" %>
	<%
}
else if (sNewPortalAction.equals("ngv_template_addtpl_2"))
{
	%>
	<%@include file="ngv_projsettings_tplrscreate.jsp" %>
	<%
}
else if (sNewPortalAction.equals("ngv_templateruleset_uploadheader_2"))
{
	%>
	<%@include file="ngv_projsettings_tplrscreate.jsp" %>
	<%
}
%>
</div>
</csrf:form>
</body>
</html>