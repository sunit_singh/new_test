<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.commons.SciConstants.StringConstants"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.commons.ObjectIdPair"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="com.sciformix.sciportal.apps.qr.QRReviewData"%>

<%
	String						l_reporttype				=	(String)	request.getAttribute("fldreporttype"),
								l_reviewdate				=	(String)	request.getAttribute("fldreviewdate"),
								l_reviewtype				=	(String)	request.getAttribute("fldreviewtype");
	Integer						l_startdatecount			=	(Integer)	request.getAttribute("fldstartdatecount"),
								l_enddatecount				=	(Integer)	request.getAttribute("fldenddatecount"),
								l_searchtype				=	(Integer)	request.getAttribute("fldsearchtype");
	List<QRReviewData>			l_qrreviewdatalist			=	(List<QRReviewData>) request.getAttribute("reviewdata");
	SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate	=	(SafetyDbCaseFieldsTemplate)request.getAttribute("fldactivesafetydbtemplate");
	String						l_searchedcaseid			=	(String)	request.getAttribute("fldsearchedcaseid");
	List<ObjectIdPair<String,String>>	l_reviewtypelist	=	(List<ObjectIdPair<String,String>>) request.getAttribute("reviewtypelist");
	String						l_userreviewtype			=	(String)	request.getAttribute("userreviewtype");
	int							l_viewmaxdays				=	QRPortalApp.getMaxDaysAllowedForView(),
								l_reportmaxdays				=	QRPortalApp.getMaxDaysAllowedForReport();
	String sNewPortalAppId = (String) request.getAttribute("new-portalappid");
	String sDownloadAllowed = ConfigHome.getPrjConfigSettings(sNewPortalAppId+StringConstants.PERIOD+QRConstant.DOWNLOAD_ALLOWED,WebUtils.getAuthorizedProjectId(request));	
%>

<!DOCTYPE html>
<html>
	<head>
		<title>
			Review Quality Details
		</title>
		<script src="js/moment/moment.min.js"></script>
		<script type="text/JavaScript">
			var startDate;
			var endDate;
			var startDateCount = <%= l_startdatecount%>;
			var endDateCount = <%= l_enddatecount%>;
			startDateCount	=	-startDateCount;
			endDateCount	=	-endDateCount;
			$(document).ready(function() {
			    $('#reportrange_right').daterangepicker( {
			   		startDate: moment().subtract(startDateCount, 'days'),
				    endDate: moment().subtract(endDateCount, 'days'),
				    minDate: '01/01/2012',
				    maxDate: moment(),
				    dateLimit: {
				    	days: 365
				    },
				    showDropdowns: true,
				    showWeekNumbers: true,
				    timePicker: false,
				    timePickerIncrement: 1,
				    timePicker12Hour: true,
				    ranges: {
				    	'Today': [moment(), moment()],
				        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				        'This Month': [moment().startOf('month'), moment()],
				        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				    },
				    opens: 'right',
				    buttonClasses: ['btn btn-default'],
				    applyClass: 'btn-small btn-primary',
				    cancelClass: 'btn-small',
				    format: 'MM/DD/YYYY',
				    separator: ' to ',
				    locale: {
					    applyLabel: 'Submit',
					    cancelLabel: 'Clear',
					    fromLabel: 'From',
					    toLabel: 'To',
					    customRangeLabel: 'Custom',
					    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
					    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
					    firstDay: 1
				    }
				},
			    function(start, end) {
			        $('#reportrange_right span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
			        startDate = start.startOf('day');
			        endDate = end.startOf('day');
			    }
			    );
			    $('#reportrange_right span').html(moment().subtract(startDateCount, 'days').format('D MMMM YYYY') + ' - ' + moment().subtract(endDateCount, 'days').format('D MMMM YYYY'));
			    startDate 	=	moment().startOf('day').subtract(startDateCount, 'days');
			    endDate		=	moment().startOf('day').subtract(endDateCount, 'days');
			    fnsearchmode(<%=l_searchtype%>);
			    
			    $('[data-toggle="tooltip"]').tooltip();
			 });
				
	      		
			function fnback () {
				document.frmmain.portal_action.value	=	"";
				document.frmmain.submit();
			}
			function fnviewdetails(p_sequenceid) {
				document.frmmain.fldsequenceid.value		=	p_sequenceid;
				document.frmmain.portal_action.value		=	"viewqrldetails";
				document.frmmain.fldreporttype.value 		= 	'<%=l_reporttype%>';
				document.frmmain.fldstartdatecount.value	=	'<%=l_startdatecount%>';
				document.frmmain.fldenddatecount.value		=	'<%=l_enddatecount%>';
				document.frmmain.fldsearchtype.value		=	'<%=l_searchtype%>';
				document.frmmain.fldsearchedcaseid.value	= 	'<%=l_searchedcaseid%>';
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fneditreviewcase(p_sequenceid) {
				document.frmmain.fldsequenceid.value		=	p_sequenceid;
				document.frmmain.portal_action.value		=	"editqrldetailsblank";
				document.frmmain.fldreporttype.value 		= 	'<%=l_reporttype%>';
				document.frmmain.fldstartdatecount.value	=	'<%=l_startdatecount%>';
				document.frmmain.fldenddatecount.value		=	'<%=l_enddatecount%>';
				document.frmmain.fldsearchtype.value		=	'<%=l_searchtype%>';
				document.frmmain.fldsearchedcaseid.value	= 	'<%=l_searchedcaseid%>';
				oFormObject = document.forms["frmmain"];
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function fnsearchmode(mode) {
				if (mode == 1) {
					document.getElementById("searchbydate").checked = true;
					document.frmmain.fldsearchedcaseid.value	= "";
					document.frmmain.fldsearchedcaseid.readOnly = true;
					document.frmmain.fldsearchtype.value = 1;
				} else {
					document.frmmain.fldsearchedcaseid.readOnly = false;
					document.frmmain.fldsearchtype.value = 2;
					document.getElementById("searchbycaseid").checked = true;
				}
			}
			function fnshowreport () {
				clearErrorMessages();
				document.getElementById("portal_action").value = "preparereport";
				document.getElementById("fldreporttype").value = '<%=l_reporttype%>';
				var	todaydate	=	moment().startOf('day');
				document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
				document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days');
				
				if (document.frmmain.fldsearchtype.value == 1) {
					if ((!(<%=l_viewmaxdays%> == 0)) && Math.abs(document.frmmain.fldstartdatecount.value - document.frmmain.fldenddatecount.value) > <%=(l_viewmaxdays-1)%>) {
						displayErrorMessage("UI-1093");
						return false;
					}
				}	
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function fnexport () {
				clearErrorMessages();
				if (document.getElementById("searchbycaseid").checked) {
					displayErrorMessage("UI-1088");	
					return false;
				}
				document.getElementById("portal_action").value = "exportfullreport";
				document.getElementById("fldreporttype").value = '<%=l_reporttype%>';
				
				var	todaydate	=	moment().startOf('day');
				document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
				document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days');
				var reviewtypedata = document.getElementById("fldreviewtype").value;
				
				if (document.frmmain.fldsearchtype.value == 1) {
					if ((!(<%=l_reportmaxdays%> == 0)) && Math.abs(document.frmmain.fldstartdatecount.value - document.frmmain.fldenddatecount.value) > <%=(l_reportmaxdays-1)%>) {
						displayErrorMessage("UI-1093");
						return false;
					}
				}
				if(reviewtypedata == "") {
					displayErrorMessage("UI-1086");	
					return false;
				} else {
					if (validateFormParameters()) {
						document.frmmain.submit();
				 	}
				}
			}
			function fnsearch() {
				document.getElementById("portal_action").value = "searchbycaseid";	
				document.getElementById("fldreporttype").value = '<%=l_reporttype%>';
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function fnreviewcasehistory(caseid, caseversion) {
				document.getElementById("portal_action").value = "showcasehistory";	
				document.getElementById("flhistroycaseid").value = caseid;
				document.getElementById("fldhistroycaseversion").value = caseversion;
			
				document.getElementById("fldreporttype").value = '<%=l_reporttype%>';
				var	todaydate	=	moment().startOf('day');
				document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
				document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days')
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function downloadReviewInExcel(seqId, caseversion) {
				document.getElementById("portal_action").value = "downloadReviewExcel";	
				document.getElementById("fldsequenceid").value = seqId;
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldsequenceid" name="fldsequenceid" value="">
			<input type="hidden" id="fldreporttype" name="fldreporttype" value="">
			<input type="hidden" id="fldstartdatecount" name="fldstartdatecount" value="">
			<input type="hidden" id="fldenddatecount" name="fldenddatecount" value="">
			<input type="hidden" id="fldsearchtype" name="fldsearchtype" value="">
			<input type="hidden" id="flhistroycaseid" name="flhistroycaseid" value="">
			<input type="hidden" id="fldhistroycaseversion" name="fldhistroycaseversion" value="">
			
			<div class="title_left">
				<% if ("CA".equals(l_reporttype)) { %>
					<h3>Case Processor Summary</h3>
				<% } else if ("QR".equals(l_reporttype)) { %>
					<h3>Quality Reviewer Summary</h3>
				<% } else if ("FL".equals(l_reporttype)) { %>	
					<h3>Full Summary</h3>
				<% } %>
				
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Report Summary
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
					<% if ("FL".equals(l_reporttype)) { %>
						<div class="form-group">	
	               			<label class="control-label col-md-3 col-sm-3 col-xs-12">
	                  			Review Type
	                  			<span class="required"> *</span>
	                  		</label>
		                  	<div class="col-md-4 col-sm-4 col-xs-12">
		                  		<select id="fldreviewtype" name="fldreviewtype" class="form-control">
		                  					<option value="">ALL</option>
		                  			<% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) {%>
		                  				<% 	if(l_reviewtypedata.getObjectSeqId().equals(l_reviewtype))
		                  						{
		                  						%>
		                  						<option value="<%=l_reviewtypedata.getObjectSeqId() %>" selected="selected"><%= l_reviewtypedata.getObjectName() %></option>
		                  					<%	}else{%>
		                  						<option value="<%=l_reviewtypedata.getObjectSeqId() %>"><%= l_reviewtypedata.getObjectName() %></option>
		                  					<%	}%>
		                  			<% 	} %>
		                  		</select>
		                	</div>
	                 	</div>
	                 	<% } %>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">
								Search By
							</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                        	<label>
	                        		<input type="radio"  name="searchby"  id="searchbydate" value="date" onclick="return fnsearchmode(1);"  checked /> 
                       				Date
                       			</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                        	<label>
	                        		<input type="radio"  name="searchby" id="searchbycaseid" value="caseid" onclick="return fnsearchmode(2);" /><%= l_safetydbcasefieldtemplate.getCaseIdAlias() %>
	                        	</label>
	                        </div>
                       </div>
	                    <div class="form-group">
	                    	<label class="control-label col-md-3 col-sm-3 col-xs-12">
								Review Date
							</label>
							<div class="col-md-4 col-sm-4 col-xs-12">
			                    <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
		                			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		                			<span></span> <b class="caret"></b>
		                		</div>
		                	</div>	
	                	</div>	
	                	<div class="form-group">
							<label class="control-label col-md-3 col-sm-12 col-xs-12 ">
								<%= l_safetydbcasefieldtemplate.getCaseIdAlias() %>
							</label>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<% if (StringUtils.isNullOrEmpty(l_searchedcaseid)) { %>
									<input type="text" maxlength="20" name="fldsearchedcaseid" id="fldsearchedcaseid" class="form-control" value=""  />
								<% } else { %>
									<input type="text" maxlength="20" name="fldsearchedcaseid" id="fldsearchedcaseid" class="form-control" value="<%=l_searchedcaseid%>"  />
								<% } %>
							</div>
							
						</div>	
						<div class="form-group">
				    		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="button" class="btn btn-success" onclick="fnshowreport();" value="Search">
									Search
								</button>
							</div>
						</div>	
						
						<div class="ln_solid"></div>
						<table class="table table-striped">
							<thead>
								<tr>
									<th><%= l_safetydbcasefieldtemplate.getCaseIdAlias() %></th>
									<th><%= l_safetydbcasefieldtemplate.getCaseVersionAlias() %></th>
									<th>Review Round</th>
									<th>Review Type</th>
									<th>Initial Receive Date</th>
									<th>Seriousness</th>
									<!-- <th>Review Type</th> -->
									<% if ("CA".equals(l_reporttype)) { %>
										<th>Case Reviewer</th>
									<% } else if ("QR".equals(l_reporttype)) { %>
										<th>Case Associate</th>
									<% } else if ("FL".equals(l_reporttype)) { %>	
										<th>Case Reviewer</th>
										<th>Case Associate</th>	
									<% } %>
									<th>Case Quality(%)</th>
									<th>Case Result</th>
									<th></th>
									<% if ("QR".equals(l_reporttype)) { %>
										<th></th>
									<% } %>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<% if (l_qrreviewdatalist != null && l_qrreviewdatalist.size() > 0) {
									for (QRReviewData 	l_qrreviewdata : l_qrreviewdatalist) { %>
										<tr>
											<td>
												<%=l_qrreviewdata.getCaseId()%>
											</td>
											<td>
												<%=l_qrreviewdata.getCaseVersion()%>
											</td>
											<td>
												<%=l_qrreviewdata.getReviewRound()%>
											</td>
											<% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) {  
													if (l_reviewtypedata.getObjectSeqId().equals(l_qrreviewdata.getReviewType())) { %>
														<td><%=l_reviewtypedata.getObjectName() %></td>
												 <% } %>
											<% } %>
											<td>
												<%= new SimpleDateFormat("dd-MMM-yyyy").format(l_qrreviewdata.getCaseRecvDate()) %>
											</td>
											
											<td>
												<%=l_qrreviewdata.getCaseSeriousness()%>
											</td>
											<%-- <td>
												<%=l_qrreviewdata.getReviewType()%>
											</td> --%>
											<% if ("CA".equals(l_reporttype)) { %>
												<td>
													<%=l_qrreviewdata.getCreatedByName()%>
												</td>
											<% } else if ("QR".equals(l_reporttype)) { %>
												<td>
													<%=l_qrreviewdata.getCaseAssociateName()%>
												</td>
											<% } else if ("FL".equals(l_reporttype)) { %>	
												<td>
													<%=l_qrreviewdata.getCreatedByName()%>
												</td>
												<td>
													<%=l_qrreviewdata.getCaseAssociateName()%>
												</td>
											<% } %>
											
											<td>
												<%=l_qrreviewdata.getCaseQuality()%>
											</td>
											<td>
												<%=l_qrreviewdata.getCaseResult()%>
											</td>
											<td>
												<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="View">
												<a HREF="javascript:fnviewdetails('<%=l_qrreviewdata.getSequenceId()%>')">
													<i class="glyphicon glyphicon-eye-open"></i>
												</a>
	                      			  			</span>
											</td>
											<% if ("QR".equals(l_reporttype)) { %>
											<td>
												<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Edit">
													<a HREF="javascript:fneditreviewcase('<%=l_qrreviewdata.getSequenceId()%>')">
														<i class="fa fa-pencil"></i>
													</a>
		                      			  		</span>
											</td>
												<% } %>
											<td>
											<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Case History">
	                        					<a href="javascript:fnreviewcasehistory('<%=l_qrreviewdata.getCaseId()%>', '<%=l_qrreviewdata.getCaseVersion()%>')" >
	                        						<i class="fa fa-history" data-original-title="History"></i>
	                        					</a>
											</span>
											</td>
											<%if(sDownloadAllowed!=null && sDownloadAllowed.equalsIgnoreCase("true")){ %>
											<td>
											<% UserSession userSession =WebUtils.getUserSession(request);
											userSession.getAuthorizedProjects();
											%>
											<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Download">
	                        					<a href="javascript:downloadReviewInExcel('<%=l_qrreviewdata.getSequenceId()%>', '<%=l_qrreviewdata.getCaseVersion()%>')" >
	                        						<i class="fa fa-download" data-original-title="Download"></i>
	                        					</a>
											</span>
											</td>
											<%} %>
										</tr> <%
									}
								} else { %>
									<tr>
										<% if ("CA".equals(l_reporttype)) { %>
											<td colspan="9">&nbsp;
												<i>No Quality Review Log</i>
											</td>
										<% } else if ("QR".equals(l_reporttype)) { %>
											<td colspan="9">&nbsp;
												<i>No Quality Review Log</i>
											</td>
										<% } else if ("FL".equals(l_reporttype)) { %>	
											<td colspan="10">&nbsp;
												<i>No Quality Review Log</i>
											</td>
										<% } %> 
									</tr>
								<% } %>
							</tbody>
						</table>
						<div class="ln_solid"></div>
						<button type="button" class="btn btn-primary" onclick="fnback();" value="Back">
							Back
						</button>
						<% if ("FL".equals(l_reporttype)) { 
							if (l_qrreviewdatalist!= null && l_qrreviewdatalist.size() > 0) { %>
								<button type="button" class="btn btn-primary" onclick="fnexport();" value="Export">
									Export
								</button>
								<!-- <div class="btn-group">
			                    	<button type="button" class="btn btn-primary" onclick="fnexport();" value="Export">Export</button>
			                      	<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			                        	<span class="caret"></span>
			                        	<span class="sr-only">Toggle Dropdown</span>
			                      	</button>
			                      	<ul class="dropdown-menu" role="menu">
			                        	<li>
			                        		<a href="#">Action</a>
			                        	</li>
			                        	<li><a href="#">Another action</a>
			                        	</li>
			                        	<li><a href="#">Something else here</a>
			                        	</li>
			                      </ul>
			                    </div> -->
						 <% } %>	
						<% } %>	
					</div>
				</div>
			</div>
		</csrf:form>	
	</body>
</html>