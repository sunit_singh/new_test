<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Quality Review 
		</title>
		
		<script type="text/JavaScript">
			function formSubmit (p_redirect) {
				document.getElementById("portal_action").value = p_redirect;
				if (validateFormParameters())
				 {
				document.frmmain.submit();
			}
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnshowreport (p_reporttype) {
				document.getElementById("portal_action").value = "preparereport";
				document.getElementById("fldreporttype").value = p_reporttype;	
				if (validateFormParameters())
				 {
				document.frmmain.submit();
			}
			}
			function fnprepareerrdb () {
				document.getElementById("portal_action").value = "prepareerrdb";
				if (validateFormParameters())
				 {
				document.frmmain.submit();
			}
			}
			function CaseMasterSubmit (value) {
				document.getElementById("portal_action").value = value;
				if (validateFormParameters())
				 {
				document.frmmain.submit();
			}
			}
		</script>
	</head>
<%
String sProjectName = WebUtils.getAuthorizedProject(request);
int nProjectId = WebUtils.getAuthorizedProjectId(request);
%>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldreporttype" name="fldreporttype" value="">
			<input type="hidden" id="fldfiletype" name="fldfiletype" value="QRLCM">
			<div class="title_left">
				<h3>Quality Review Dashboard 
				<%if(nProjectId != -1){%>
					-  <%=sProjectName%>
					<%} %>
				</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div id="Content1">
				 		<div class="x_title">
							<h2>
								General
							</h2>
							<div class="clearfix"></div>
						</div>
					
						<div class="x_content">
							<a class="btn btn-app" onclick="formSubmit('qualityreview');" style="width: = 2cm; height: 2cm">
                      			<label> Case <br> Review</label>
                    		</a>
						
							<a class="btn btn-app" onclick="formSubmit('ilqr_case_master');" style="width: = 2cm; height: 2cm">
                      			<label> Upload<br /> Case <br> Master</label>
                    		</a>
                    		
                    		<a class="btn btn-app" onclick="formSubmit('casemasterinsertview');" style="width: = 2cm; height: 2cm">
                      			<label> Ad Hoc<br /> Case <br> Master</label>
                    		</a>
                    		
                    		<a class="btn btn-app" onclick="formSubmit('view_cases');" style="width: = 2cm; height: 2cm">
                      			<label> Review <br> Case List</label>
                    		</a>
                    	</div>
					</div> 
					<div id="Content2">
				 		<div class="x_title">
							<h2>
								Personal Dashboard
							</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<a class="btn btn-app" onclick="fnshowreport('CA');" style="width: = 2cm; height: 2cm">
                      			<label> Case <br> Processor <br> Summary</label>
                    		</a>
                    		<a class="btn btn-app" onclick="fnshowreport('QR');" style="width: = 2cm; height: 2cm">
                      			<label> Quality <br/> Reviewer <br/> Summary</label>
                    		</a>
                    		
						</div>
					</div> 
					<div id="Content2">
				 		<div class="x_title">
							<h2>
								Team Dashboard
							</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<a class="btn btn-app" onclick="fnshowreport('FL');" style="width: = 2cm; height: 2cm">
                      			<label> Full <br> Summary</label>
                    		</a>
                    		<a class="btn btn-app" onclick="fnshowreport('DS');" style="width: = 2cm; height: 2cm">
                      			<label> Daily <br/> Summary</label>
                    		</a>
                    		<a class="btn btn-app" onclick="fnprepareerrdb();" style="width: = 2cm; height: 2cm">
                      			<label> Error <br/> Dashboard</label>
                    		</a>
                    		<a class="btn btn-app" onclick="formSubmit('view_graph');" style="width: = 2cm; height: 2cm">
                      			<label> Quality <br> Review <br /> Graph</label>
                    		</a>
                    		<a class="btn btn-app" onclick="formSubmit('preparecpdb');" style="width: = 2cm; height: 2cm">
                      			<label> CP <br/>Dashboard </label>
                    		</a>
						</div>
					</div>
				</div>
			</div>
		</csrf:form>	
	</body>
</html>