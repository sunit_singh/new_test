<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.commons.ObjectIdPair" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%
	List<ObjectIdPair<String,String>>	l_reviewtypelist	=	(List<ObjectIdPair<String,String>>) request.getAttribute("reviewtypelist");
	String								l_userreviewtype	=	(String)	request.getAttribute("userreviewtype");
	SafetyDbCaseFieldsTemplate			l_activescft		=	(SafetyDbCaseFieldsTemplate)request.getAttribute("fldactivesafetydbtemplate");
	String								l_emptycaserecord	=	(String)	request.getAttribute("emptyCaseRecord");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">

	function frmsubmit() {
		document.frmmain.portal_action.value = "search";
		if (validateInput() && validateFormParameters()) {
			document.frmmain.submit();
		}
	}
	function validateFormParameters () {
		oFormObject = document.forms["frmmain"];
		return validateInputParameter(oFormObject);
	}
	function validateInput() {
		clearErrorMessages();
		var bErrorFlag = true;
		var l_caseid		=	document.frmmain.fldcaseid.value;
		var l_caseversion	=	document.frmmain.fldcaseversion.value;
		
		if(l_caseid.trim() == "") {
			displayErrorMessage("UI-1055");
			document.frmmain.fldcaseid.focus();
			bErrorFlag = false;
		} else if(l_caseversion.trim() == "") {
			displayErrorMessage("UI-1056");
			document.frmmain.fldcaseversion.focus();
			bErrorFlag = false;
		}
		return bErrorFlag;
	} 
	
	function fncancel(value) {
		document.frmmain.submit();
	}
	function fnback() {
		document.frmmain.submit();
	}
</script>
</head>
<body>
	<csrf:form name="frmmain" id="frmmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
		<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
		<input type="hidden" id="portal_action" name="portal_action" value="">
		<input type="hidden" id="projsettings_appid" name="projsettings_appid"  />
 		<div class="page-title">
 			<div class="title_left">
 		        	<h3>Quality Review</h3>
    		</div>
 		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
            	<div class="x_panel">
                	<div class="x_title">
                  		<h3>
                  			<small>Enter Following Case Details</small>
                  		</h3>	
				   		<div class="clearfix"></div>
				  	</div>
				  	<div class="x_content">
                  		<div class="form-group">	
	               			<label class="control-label col-md-3 col-sm-3 col-xs-12">
	                  			Review Type
	                  			<span class="required"> *</span>
	                  		</label>
		                  	<div class="col-md-4 col-sm-4 col-xs-12">
		                  		<select id="fldreviewtype" name="fldreviewtype" class="form-control">
		                  			<% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) {
		                  				for (String l_reviewtypecode : l_userreviewtype.split("~")) {
		                  					if (l_reviewtypecode.equals(l_reviewtypedata.getObjectSeqId())) { %>
		                  						<option value="<%=l_reviewtypecode %>"><%= l_reviewtypedata.getObjectName() %></option>
		                  				<% 	}
		                  				}
		                  			} %>
		                  		</select>
		                	</div>
	                 	</div>
	                 
		                <div class="form-group">	
		               		<label class="control-label col-md-3 col-sm-3 col-xs-12">
		                  		<%= l_activescft.getCaseIdAlias() %>
		                  		<span class="required"> *</span>
		                  	</label>
		                  	<div class="col-md-4 col-sm-4 col-xs-12">
		                  		<input type="text"  name="fldcaseid" id="fldcaseid"
	            					class="form-control">
	            			</div>
		                </div>
	                 
	                 	<div class="form-group">	
	               			<label class="control-label col-md-3 col-sm-3 col-xs-12">
	                  			<%= l_activescft.getCaseVersionAlias() %>
	                  			<span class="required"> *</span>
	                  		</label>
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  			<input type="text"  name="fldcaseversion" id="fldcaseversion" class="form-control">
            				</div>
	                	</div>
	                 	<div class="ln_solid"></div>	
                  	 	<div class="form-group" align = "center">	
	                  	 	<button type="button" class="btn btn-primary" onclick="fnback();">
								Back
							</button>
		                  	<button type="button" class="btn btn-primary" onclick="frmsubmit();">
		                  		Search
		                  	</button> 
		               </div>	 
	              </div>
			  	</div>
          	</div>
 		</div>
 		<%if(!StringUtils.isNullOrEmpty(l_emptycaserecord)){ %>
 			<div class="row">
			  	<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<label>! <%=l_emptycaserecord %> .</label>
							<div class="clearfix"></div>
						</div>
						<div class="x_content" style="display: block;">
						<h3>Do you want to enter case details and continue with review ?</h3>
							<div class="ln_solid"></div>	
							<button type="button" class="btn btn-primary" >
		                  			Yes
		                  	</button>
		                  	<button type="button" class="btn btn-primary" onclick="fncancel('qualityreview');">
		                  			No
		                  	</button> 
						</div>
					</div>
				</div>
			</div>		
	     <%} %> 
</csrf:form>
</body>
</body>
</html>