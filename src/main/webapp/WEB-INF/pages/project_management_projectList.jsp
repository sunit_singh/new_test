<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>

<script type="text/javascript">
function clickMod(value, id)
{
	var form=document.getElementById("projectManagement");
	document.getElementById("portal_action").value = value;
	document.getElementById("selectedProjectId").value =id
	
	if(validateFormParameters()){ 
		form.submit();
	}
}
function validateFormParameters () {
	oFormObject = document.forms["projectManagement"];
	return validateInputParameter(oFormObject);
}
</script>
<%String successMsg = (String) request.getAttribute("msg"); %>
<div class="page-title">
	<div class="title_left">
		<h3>Project Listing</h3>
	</div>
</div>
	<div class ="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				 <div class="x_title">
					<h3>
						<small>List of Projects</small> 	
					</h3>
					<% if(successMsg !=null){%>
						<i class='glyphicon'></i>&nbsp;<strong><%=successMsg %></strong>
					<%} %>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
						<h2><small>View/Create/Modify Project</small></h2>
						<div id="templateList">
							<table class="table table-striped">
								<thead>
							    	<tr>
							        	<th></th>
							        	<th><!-- State --></th>
							        	<th>Project Name (Seq Id)</th>
							        	<th>Description</th>
							       </tr>
						         </thead>
					             <tbody>
					             <%if (projectList.isEmpty()){ %>
					             	<tr>
										<td colspan="7">&nbsp;<i>No records found</i></td>
										</tr>
					             <%}else {
					             for(ProjectInfo projectInfo : projectList){ %>
					                <tr>
										<td>
											<a href="javascript:clickMod('view', '<%=projectInfo.getProjectSequenceId()%>')">
												<i class="glyphicon glyphicon-eye-open"></i>
											</a> &nbsp;&nbsp;
											<a href="javascript:clickMod('edit', '<%=projectInfo.getProjectSequenceId()%>')">
												<i class="fa fa-pencil"></i>
											</a> 
										</td>
										<td>
											<% if(projectInfo.getProjectState().equals(ProjectInfo.ProjectState.ACTIVE)) {%>
											Active
											<%}%>
										</td>
										<td>
											<%=projectInfo.getProjectName()%>&nbsp;(<%=projectInfo.getProjectSequenceId()%>)
										</td>
										<td>
											<%=projectInfo.getProjectDescription() %>
										</td>
									</tr>
									<%} }%>
								</tbody>
							</table>
						</div>
						<div class="ln_solid"></div>
				        <div>
				        	<button type="button" class="btn btn-primary" onclick="clickMod('Create','-1')">Create New</button>
						</div> 
				   </div><!-- x Content End -->
				</div><!-- X panel End -->
			</div>
		</div><!-- row class end -->