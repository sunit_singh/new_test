<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplate"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>
<script type="text/javascript">
<%@ page trimDirectiveWhitespaces="true" %>

function hideOrShowTemplateContent(templateid)
{
	if (templateid == '00')
	{
		var divTemplateContent = document.getElementById("div_templatecontent_" + templateid);
		
		if (divTemplateContent.style.display == "none")
		{
			divTemplateContent.style.display = "block";
		}
		else
		{
			divTemplateContent.style.display = "none";
		}
	}
	else
	{
	//	var divTemplateContent = document.getElementById("div_templatecontent_" + templateid);
		var divrowTemplateContent = document.getElementById("divrow_templatecontent_" + templateid);
		
		if (divrowTemplateContent.style.display == "none")
		{
			//divTemplateContent.style.display = "block";
			divrowTemplateContent.style.display = "block";
		}
		else
		{
			//divTemplateContent.style.display = "none";
			divrowTemplateContent.style.display = "none";
		}
	}
}

function submitForm(value, mode)
{
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = "NGV_Protal_App";
	document.getElementById("clickValue").value =value;
	document.getElementById("portal_action_mode").value = mode;
	if (validateFormParameters())
	 {
		form.submit();
	 }
}
function validateFormParameters () {
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}
function changeRulesetState(sNewState)
{
	document.getElementById("ngv_tplrs_state").value = sNewState;
	
	submitForm("ngv_templateruleset_changestate", "");
}
function setAsDefault(nNewDefaultTemplateId)
{
	document.getElementById("ngv_selectedtemplateid").value = nNewDefaultTemplateId;
	submitForm("ngv_template_setdefaulttemplate", "");
}
function editTemplate(nTemplateIdToEdit)
{
	document.getElementById("ngv_selectedtemplateid").value = nTemplateIdToEdit;
	submitForm("ngv_template_edittemplate", "edit");
}
function viewTemplate(nTemplateIdToEdit, sReadOnly)
{
	document.getElementById("ngv_selectedtemplateid").value = nTemplateIdToEdit;
	document.getElementById("ngv_editTemplateFlag").value = sReadOnly;
	submitForm("ngv_template_edittemplate", "view");
}
</script>
<%
final int MAX_CONTENT_SIZE = 60;

NgvTemplateRuleset oTemplateRuleset = null;
List<NgvTemplate> listTemplates = null;
int nTemplateRulesetId = -1;
String sTemplateRulesetName = null;
String sTemplateRulesetState = null;
boolean bActivateRulset = false;
boolean bDeactivateRulset = false;
String sTemplateHeader = null;
String sTemplateHeaderPreview = null;
int nDefaultTemplateId = -1;
boolean bUserHasEditPrivilege = false;
boolean bObjectCanBeEdited = false;
String sNonEditMessage = null;
boolean bEditAllowed = false;

bUserHasEditPrivilege = true;//Currently there are no privileges separate for this

oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");

if (oTemplateRuleset != null)
{
	nTemplateRulesetId = oTemplateRuleset.getRulesetId();
	sTemplateRulesetName = oTemplateRuleset.getRulesetName();
	sTemplateRulesetState = oTemplateRuleset.getState().displayName();
	sTemplateHeader = oTemplateRuleset.getHeader();
	nDefaultTemplateId = oTemplateRuleset.getDefaultTemplateSeqId();
	
	bActivateRulset = (oTemplateRuleset.getState() == com.sciformix.commons.SciEnums.RecordState.UNDER_CONFIGURATION);
	bDeactivateRulset = (oTemplateRuleset.getState() == com.sciformix.commons.SciEnums.RecordState.ACTIVE);
	bObjectCanBeEdited = (oTemplateRuleset.getState() == com.sciformix.commons.SciEnums.RecordState.UNDER_CONFIGURATION);
}
else
{
	sTemplateRulesetName = "";
	sTemplateRulesetState = "";
}

bEditAllowed = bObjectCanBeEdited && bUserHasEditPrivilege;
if (!bObjectCanBeEdited && bUserHasEditPrivilege)
{
	sNonEditMessage = "Active Template Rulesets cannot be modified";
}
else if (bObjectCanBeEdited && !bUserHasEditPrivilege)
{
	sNonEditMessage = "No privileges to edit";
}
else
{
	sNonEditMessage = "";
}

sTemplateHeader = (sTemplateHeader!=null ? sTemplateHeader : "(None)");

if (sTemplateHeader.length() > MAX_CONTENT_SIZE)
{
	sTemplateHeaderPreview = sTemplateHeader.substring(0, MAX_CONTENT_SIZE) + "... (total: " + sTemplateHeader.length() +" characters)";
}
else
{
	sTemplateHeaderPreview = sTemplateHeader;
}

listTemplates = oTemplateRuleset.getTemplateList();

%>
	<input type="hidden" id="ngv_tplrulesetid" name="ngv_tplrulesetid" value="<%=nTemplateRulesetId%>" />
	<input type="hidden" id="ngv_tplrs_state" name="ngv_tplrs_state" value="" />
	<input type="hidden" id="ngv_selectedtemplateid" name="ngv_selectedtemplateid" value="" />
	<input type="hidden" id="ngv_editTemplateFlag" name="ngv_editTemplateFlag" value="" />
	
	
	<div class="center_col" role="main">
    	<div class="">
          	<%=displayFormHeader("Template Ruleset") %>
            
            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip("View/modify your Template Ruleset", false, false) %>
                  
                  <div class="x_content">
                  	<div class="form-group">
	                  	<% if (bActivateRulset) {%><A HREF="javascript:changeRulesetState('ACTIVE');"><i class="glyphicon glyphicon-ok-sign"></i> Activate</A>&nbsp;|&nbsp; <%} %>
	                  	<% if (bDeactivateRulset) {%><A HREF="javascript:changeRulesetState('DEACTIVATED');"><i class="glyphicon glyphicon-remove-sign"></i> De-Activate</A>&nbsp;|&nbsp; <%} %>
	                  	<A HREF="javascript:submitForm('ngv_templateruleset_export', '');"><i class="glyphicon glyphicon-save-file"></i> Export</A>
					</div>
					<%=displayLabelPair("Template Ruleset Name", false, sTemplateRulesetName)%>
                  	
                  	<%=displayLabelPair("Template Ruleset State", false, sTemplateRulesetState)%>
					
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Template Ruleset Header</label>
						<div class="col-md-9 col-sm-9 col-xs-12" style="font-family:Courier New">
							<!-- <a href="javascript:hideOrShowTemplateContent('00')"> -->
							<a data-target="#myModal" data-toggle="modal">
								<i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="left" title="View header contents"></i>
							</a>
							<% if (bEditAllowed) { %>
							<A HREF="javascript:submitForm('ngv_templateruleset_uploadheader', '');"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="left" title="Edit header contents"></i></A>
							<%} %>
		                  	<%=WebUtils.formatForDisplay(sTemplateHeaderPreview)%>
		                  	<!-- <div id="div_templatecontent_00" style="display:none; border-radius:10px; border: 2px solid #73AD21; background: #00FF00; padding: 5px;"> -->
		                  		<%-- <B><U>Complete Template Header</U></B>
		                  		<BR>
		                  		<PRE><%=WebUtils.formatForDisplay(sTemplateHeader)%></PRE> --%>
		                  		<div id="myModal" class="modal fade" role="dialog">
									<div class="modal-dialog modal-lg">
								  		<div class="modal-content">
											<div class="modal-header">
											  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span>
											  </button>
											  <h4 class="modal-title" id="myModalLabel2">
											  	<B>Complete Template Header</B>
											  </h4>
											</div>
											<div class="modal-body">
												<textarea readonly name="templateheaderdetails" style="overflow:inherit;resize:vertical;height:400px; width:800px;">
													<%=WebUtils.formatForDisplay(sTemplateHeader)%>
												</textarea>	
											</div>
										</div>
									</div>
								</div>			
		                  	<!-- </div> -->
	                  	</div>
					</div>
                  	<p></p>
                  	<h2><small>List of Templates</small></h2>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Template Name</th>
                          <th>Criteria</th>
                          <th>Content</th>
                        </tr>
                      </thead>
                      <tbody>
						<%
						if (listTemplates != null && listTemplates.size() > 0)
						{
							
							String sTemplateContentPreview = null;
							String sTemplateContent = null;
							String sTemplateCriteria = null;
							for (NgvTemplate oTemplate : listTemplates)
							{
								sTemplateContent = (oTemplate.getTemplateContent() != null)? oTemplate.getTemplateContent().getContent() : "(None)";
								if (sTemplateContent.length() > MAX_CONTENT_SIZE)
								{
									sTemplateContentPreview = sTemplateContent.substring(0, MAX_CONTENT_SIZE) + "... (total: " + sTemplateContent.length() +" characters)";
								}
								else
								{
									sTemplateContentPreview = sTemplateContent;
								}
								sTemplateCriteria = (oTemplate.getTemplateCriteria() != null) ? oTemplate.getTemplateCriteria() : "(not specified)";
								
								%>
								<tr>
		                          <th valign="top"><span style=" white-space: nowrap;">
	                          		<%
	                          			if (nDefaultTemplateId == oTemplate.getTemplateSeqId())
										{ 
											%><a href="javascript:;" data-toggle="tooltip" data-placement="left" title="Already default template"><i class="glyphicon glyphicon-certificate"></i></a><%
										} else if ((nDefaultTemplateId != oTemplate.getTemplateSeqId()) && (bEditAllowed)) { 
											%><a href="javascript:setAsDefault('<%=oTemplate.getTemplateSeqId()%>')" data-toggle="tooltip" data-placement="left" title="Set as default template"><i class="glyphicon glyphicon-star-empty"></i></a><%
										}
									%>&nbsp;
									<%-- <%if(!sTemplateRulesetState.equals("Deactivated") && !sTemplateRulesetState.equals("Active")){ %>
									<a href="javascript:viewTemplate('<%=oTemplate.getTemplateSeqId()%>', 'readOnly');" data-toggle="tooltip" data-placement="left" title="Preview contents"><i class="glyphicon glyphicon-eye-open"></i></a>
									<%}else{ %> --%>
									<a href="javascript:viewTemplate('<%=oTemplate.getTemplateSeqId()%>', 'readOnly');" data-toggle="tooltip" data-placement="left" title="Preview contents"><i class="glyphicon glyphicon-eye-open"></i></a>
									<%-- <%} %> --%>
									<%if (bEditAllowed) {%>
									&nbsp;<A HREF="javascript:editTemplate('<%=oTemplate.getTemplateSeqId()%>');" data-toggle="tooltip" data-placement="left" title="View/edit template"><i class="fa fa-pencil"></i></A>
									<%} %>
		                          </span></th>
		                          <td valign="top"><span style=" white-space: nowrap;"><%=oTemplate.getTemplateName()%></span></td>
		                         <td style="font-family:Courier New" valign="top"><span style="word-wrap:break-word;"><%=sTemplateCriteria%></span></td> 
		                          <td style="font-family:Courier New" valign="top">
		                          	<%=WebUtils.formatForDisplay(sTemplateContentPreview)%>
		                          </td>
		                        </tr>
								<%
							}
						}//end-if listTemplates
						else
						{
							%><TR><TD colspan="4">&nbsp;<i>No templates</i></TD><%
						}//end-else listTemplates
						%>
					  </tbody>
					</table>
					
					<% if (bEditAllowed) {%>
                  		<button type="button" class="btn btn-primary" onclick="javascript:submitForm('ngv_tplrs_tplviewedit_sb', 'create');">Create New</button>
                  	<% } %>
					
                  </div> <!-- end-div x_content -->
                  
                </div><!-- end-div panel -->
              </div><!-- end-div col-md-12 col-sm-12 col-xs-12 -->
            </div><!-- end-div row -->
            
		</div><!-- end-div "" -->
	</div><!-- end-div center_col -->


