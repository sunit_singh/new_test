<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.ngv.CaseDataValidationRuleset"%>
<%@page import="com.sciformix.sciportal.apps.ngv.CaseDataValidationRule"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.apps.ngv.CaseDataValidationRule"%>
<%@page import="com.sciformix.sciportal.apps.ngv.CaseDataValidationRule.Type"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<script type="text/javascript">
function submitForm(value)
{
	
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
	document.getElementById("clickValue").value =value;
	if (validateFormParameters())
	 {
		form.submit();
	}
}
function validateFormParameters () {
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}
function userSelected (p_value) {
	if (p_value == 1) {
		$("[id=cases]").hide();

	} else if (p_value == 2) {
		$("[id=cases]").show();
		} else {
		
	}
}

function validateRuleSet(value)
{
	var caseValidationRuleSetName = document.getElementById("ngv_addval_valrulesetname").value;
	
	if(caseValidationRuleSetName =="")
	{
		displayErrorMessage("UI-1036");
		return false;
	}else
	{
		submitForm(value);
	}
	
}


function setdefaultdata () {
	var	ruletype	=	'<%= request.getAttribute("ruletype") %>';
	if (ruletype == "validity") {
		document.getElementById("validityCriteria").checked = true;
		usertypeselection('1');
	} else if (ruletype == "error")  {
		document.getElementById("errorCriteria").checked = true;
		usertypeselection('2');
	}
	else if (ruletype == "warning")  {
		document.getElementById("warningCriteria").checked = true;
		usertypeselection('3');
	}
}
</script>

<input type="hidden" id="fldprojectid" name="fldprojectid" value="0">
<%
String sNewPortalActionLocal = (String) request.getAttribute("new-portalaction");

if (sNewPortalActionLocal.equals("ngv_validate_createnew_2"))
{
%>

		<div class="center_col" role="main">
          <div class="">
          	<%=displayFormHeader("Case Validation Ruleset Creation") %>
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip("Create a Case Validation Ruleset", false, false) %>
                  
                  <div class="x_content">
                  	 <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left"> 
						 <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Mode</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="iCheck"  id="createFromScratch" onclick="javascript:return disableImport();"  checked /> Create from scratch</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
							<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
                       </div>
                       
<!--                           <label class="control-label">Select File</label>
							<input id="input-22" name="input22[]" type="file" class="file-loading" accept="text/plain" multiple>
							<script>
							$(document).on('ready', function() {
							    $("#input-22").fileinput({
							        previewFileType: "text",
							        allowedFileExtensions: ["txt", "md", "ini", "text"],
							        previewClass: "bg-warning"
							    });
							});
							</script>
 -->
                  	 	<%=displayTextBox("ngv_addval_valrulesetname", "Case Validation Ruleset Name", true, "")%> 
                  	   <div class="ln_solid"></div>	
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  <button type="button" class="btn btn-primary"
									onclick="return cancelCreateRulesetForm();">Cancel</button>
									
					 <button type="button" class="btn btn-success"
									onclick="return submitForm('ngv_valrsimport_step1sb');">Submit</button>
					</div>	
									
					</div>	 			     
	        		<%--  <jsp:include page="/WEB-INF/pages/formSubmitToolbar.jsp" flush="true" />  --%>
	        		<!-- </form> -->
	        		<script>
	        		
	        		function disableImport()
	        		{
	        			var importFromFile = document.getElementById("createFromScratch").value;
						if(importFromFile == "on")
						{
							document.getElementById("ngv_validaters_importfile").disabled = true;
							document.getElementById("ngv_addval_valrulesetname").disabled = false;
						} 
						return true;
	        		}
	        			        			   
	        		function enableImport()
	        		{
	        			var importFromFile = document.getElementById("importFromFile").value;
						if(importFromFile == "on")
						{
							document.getElementById("ngv_validaters_importfile").disabled = false;
							document.getElementById("ngv_addval_valrulesetname").value = "";
							document.getElementById("ngv_addval_valrulesetname").disabled = true;
						} 
						return true;
	        		}
	        		function submitImport(action)
					{
						//return validateTemplateCreation('ngv_tplrs_tplviewedit_savetpl');
						var form = document.getElementById("PROJSETTINGS_form");
						document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
						document.getElementById("clickValue").value =action;
						if (validateFormParameters())
						 {
							form.submit();
						 }
					}
	        		
						function submitForm(action)
						{
							clearErrorMessages();
							if (document.getElementById("createFromScratch").checked) 
							{
								var casevalidationrulesetname	=	document.getElementById("ngv_addval_valrulesetname").value;
								if (casevalidationrulesetname == "") 
								{
									displayErrorMessage("UI-1036");
									return false;
								}
								else
									{
									var form = document.getElementById("PROJSETTINGS_form");
									document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
									document.getElementById("clickValue").value = "ngv_validate_createnew_3";
									if (validateFormParameters())
									 {
										form.submit();
									 }
								}
							} 
						}
						
						function cancelForm()
						{
						
							var form = document.getElementById("PROJSETTINGS_form");
							document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
							document.getElementById("clickValue").value = "ngv_validate_createnew_3";
							form.submit();
						}
						
						function cancelCreateRulesetForm()
						{
						
							var form = document.getElementById("PROJSETTINGS_form");
							document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
							document.getElementById("clickValue").value = null;
							form.submit();
						}
						
						
	        		</script>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>

	<!-- <tr><td>&nbsp;Template Ruleset name</td><td>&nbsp;<INPUT type="text" name="ngv_addtpl_tplrulesetname" id="ngv_addtpl_tplrulesetname" /></td><td>&nbsp;</td></tr> -->
	<!-- <td colspan="3" align="center">&nbsp;<input type="submit" class="btn btn-default" value="Next" onclick="return validateRuleSet('ngv_template_createnew_3');" /></td> -->
<%
}
else if (sNewPortalActionLocal.equals("ngv_valrs_valviewedit_dp"))
{
	String sEdittemplateFLag =  (String) request.getAttribute("ngv_editTemplateFlag");
	CaseDataValidationRuleset oCaseDataValidationRuleset = (CaseDataValidationRuleset) WebUtils.getSessionAttribute(request,"ngv_validate_ruleset");
	// NgvRuleRuleset oTemplateRuleset = (NgvRuleRuleset) request.getAttribute("ngv_template_ruleset");
	String sTemplateRulesetName = (oCaseDataValidationRuleset!=null?oCaseDataValidationRuleset.getTemplateName():"");
	int nTemplateRulesetId = (oCaseDataValidationRuleset!=null ? oCaseDataValidationRuleset.getSequenceId() : -1);
	
	String sMode = (String) request.getAttribute("portal_action_mode");
	sMode = (StringUtils.isNullOrEmpty(sMode) ? "view" : sMode);
	String sFormTitle = (sMode.equals("create") ? "Rule Creation" : sMode.equals("edit") ? "Rule Modification" : "Rule View");
	String sFormTooltip = (sMode.equals("create") ? "Create a Rule" : sMode.equals("edit") ? "Modify the Rule" : "View Rule configuration");
	
	
	CaseDataValidationRule oRule = (CaseDataValidationRule) request.getAttribute("ngv_selectedTemplate");
	boolean bTemplateEditMode = (oRule!=null);
	
	String sCurrentRuleName = (oRule !=null ? oRule.getRuleName() : "");
	String sCurrentRuleType = (oRule !=null ?  oRule.getRuleType().name() : "");
	String sCurrentRuleCriteria = (oRule !=null ? oRule.getCriteria()	 : "");
	String sCurrentRuleApplicability = (oRule !=null ? oRule.getApplicability() : "");

	%>
		<div class="center_col" role="main">
          <div class="">
          	<%=displayFormHeader(sFormTitle) %>
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip(sFormTooltip, false, false) %>
                  
                  <div class="x_content">
                  	<!-- <form id="PROJSETTINGS_form" data-parsley-validate class="form-horizontal form-label-left"> -->
	                  	<%=displayLabelPair("Case Validation Ruleset Name", false, sTemplateRulesetName)%>
	                  	<%if(sEdittemplateFLag!=null && sEdittemplateFLag.equals("readOnly")){ %>
	                  		<%=displayTextBox("ngv_addval_rulename", "Rule Name", true, sCurrentRuleName, "", bTemplateEditMode)%>
	                  	
	                  		<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validation Rule Type</label>
							<%if(sCurrentRuleType.equalsIgnoreCase("validity")) { %>
							
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" checked /> Validity Criteria</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error"  disabled/> Error Criteria</label>
	                         </div>
                       </div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" disabled/> Warning Criteria</label>
	                         </div>
                       </div>
                       <%}
							else if(sCurrentRuleType.equalsIgnoreCase("error")) { %>
							
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" disabled/> Validity Criteria</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error" checked  /> Error Criteria</label>
	                         </div>
                       </div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" disabled/> Warning Criteria</label>
	                         </div>
                       </div>
                       <%}
						else if(sCurrentRuleType.equalsIgnoreCase("warning")) { %>
							
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" disabled/> Validity Criteria</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error"   disabled/> Error Criteria</label>
	                         </div>
                       </div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" checked /> Warning Criteria</label>
	                         </div>
                       </div>
                       <%}
						else  { %>
						
                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" disabled/> Validity Criteria</label>
                         </div>
                    </div>
                    <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error"   disabled/> Error Criteria</label>
                         </div>
                   </div>
                    <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" disabled/> Warning Criteria</label>
                         </div>
                   </div>
                   <%}
							%>
	                  		<%=displayTextBox("ngv_addval_valcriteria", "Criteria", true, sCurrentRuleCriteria,"",bTemplateEditMode)%>
	                  		
	                  		<%if(sCurrentRuleApplicability.equalsIgnoreCase("all")) { %>
	                  	<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Applicability</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability"  id="applicabilityAll" value="all" checked />All cases</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability" id="applicabilityCertain" value="certain"  disabled/>Cases that meet the criteria</label>
	                         </div>
	                    </div>
	                    <%}
	                  		else if(sCurrentRuleApplicability.contains("$")) { %>
		                  	<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Applicability</label>
		                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
		                           <label><input type="radio"  name="applicability"  id="applicabilityAll" value="all"  disabled/>All cases</label>
		                         </div>
		                    </div>
		                    <div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
		                        <div class="radio col-md-4 col-sm-4 col-xs-12">
		                           <label><input type="radio"  name="applicability" id="applicabilityCertain" value="certain" checked />Cases that meet the criteria</label>
		                         </div>
		                    </div>
		                    <%}
	                  		else  { %>
	                  		<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Applicability</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability"  id="applicabilityAll" value="all"  disabled/>All cases</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability" id="applicabilityCertain" value="certain" disabled/>Cases that meet the criteria</label>
	                         </div>
	                    </div>
	                    <%}
	                  		if(!sCurrentRuleApplicability.equalsIgnoreCase("all"))
		                  	{
	                    %>
	                    <div id="cases" >
    						<div class="form-group">
	                  	<%=displayTextBox("ngv_certaincriteria", "", true, sCurrentRuleApplicability,"",bTemplateEditMode)%>
	                  	</div>
	                  	</div>
	                  	<%} %>
	                  	<div class="ln_solid"></div>	
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  <button type="button" class="btn btn-primary"
									onclick="return cancelViewEditTemplateForm();">Cancel</button>
					</div>		
	                  	<%}else{ %>
		                  	<%=displayTextBox("ngv_addval_rulename", "Rule Name", true, sCurrentRuleName, "", bTemplateEditMode)%>
	                  	
	                  		<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validation Rule Type</label>
	                  		<%if(sCurrentRuleType.equalsIgnoreCase("validity")) { %>
							
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" checked /> Validity Criteria</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error"  /> Error Criteria</label>
	                         </div>
                       </div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" /> Warning Criteria</label>
	                         </div>
                       </div>
                       <%}
							else if(sCurrentRuleType.equalsIgnoreCase("error")) { %>
							
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" /> Validity Criteria</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error" checked  /> Error Criteria</label>
	                         </div>
                       </div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" /> Warning Criteria</label>
	                         </div>
                       </div>
                       <%}
						else if(sCurrentRuleType.equalsIgnoreCase("warning")) { %>
							
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" /> Validity Criteria</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error"   /> Error Criteria</label>
	                         </div>
                       </div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" checked/> Warning Criteria</label>
	                         </div>
                       </div>
                       <%}
						else { %>
						
                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
                           <label><input type="radio"  name="ruletype"  id="validityCriteria" value="validity" /> Validity Criteria</label>
                         </div>
                    </div>
                    <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                           <label><input type="radio"  name="ruletype" id="errorCriteria" value="error"   /> Error Criteria</label>
                         </div>
                   </div>
                    <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                           <label><input type="radio"  name="ruletype" id="warningCriteria" value="warning" /> Warning Criteria</label>
                         </div>
                   </div>
                   <%}
							%>
	                  		<%=displayTextBox("ngv_addval_valcriteria", "Criteria", true, sCurrentRuleCriteria,"")%>
	                  	
	                  	<%if(sCurrentRuleApplicability.equalsIgnoreCase("all")) { %>
	                  	<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Applicability</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability"  id="applicabilityAll" value="all" checked onclick="return userSelected(1);"/>All cases</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability" id="applicabilityCertain" value="certain"  onclick="return userSelected(2);"/>Cases that meet the criteria</label>
	                         </div>
	                    </div>
	                    <%}
	                  	else if(sCurrentRuleApplicability.contains("$")) { %>
	                  	<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Applicability</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">	
	                           <label><input type="radio"  name="applicability"  id="applicabilityAll" value="all" onclick="return userSelected(1);"/>All cases</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability" id="applicabilityCertain" value="certain" checked onclick="return userSelected(2);"/>Cases that meet the criteria</label>
	                         </div>
	                    </div>
	                    <%}
	                  		else  { %>
	                  		<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Applicability</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability"  id="applicabilityAll" value="all"  onclick="return userSelected(1);"/>All cases</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="applicability" id="applicabilityCertain" value="certain" onclick="return userSelected(2);"/>Cases that meet the criteria</label>
	                         </div>
	                    </div>
	                    <%}
	                  	if(sCurrentRuleApplicability.equalsIgnoreCase("all") || sCurrentRuleApplicability.equalsIgnoreCase("") )
	                  	{
	                    %>
	                     <div id="cases" style="display:none;">
    						<div class="form-group">
                        <%=displayTextBox("ngv_certaincriteria", "", true, "","")%>
                        </div>
                        </div>
                       <%}
	                  	else
	                  	{

		                    %>
		                     <div id="cases">
	    						<div class="form-group">
	                        <%=displayTextBox("ngv_certaincriteria", "", true, sCurrentRuleApplicability,"")%>
	                        </div>
	                        </div>
	                       <%
	                  	}
	                  	%>
	                  		
					<div class="ln_solid"></div>	
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  <button type="button" class="btn btn-primary"
									onclick="return cancelViewEditTemplateForm();">Cancel</button>
									
					 <button type="button" class="btn btn-success"
									onclick="return submitForm();">Submit</button>
					</div>		
	                  	<%} %>
		        		<%-- <jsp:include page="/WEB-INF/pages/formSubmitToolbar.jsp" flush="true" /> --%>
		        		
					</div>	 
						<input type="hidden" id="ngv_tplrulesetid" name="ngv_tplrulesetid" value="<%=nTemplateRulesetId%>" />
						<input type="hidden" id="ngv_selectedtemplateid" name="ngv_selectedtemplateid" value="<%=(oRule!=null?oRule.getRuleSeqId():-1)%>" />
	        		<!-- </form> -->
                  </div><!-- end-div x_content -->
                </div><!-- end-div x_panel -->
              </div><!-- end-div ... -->
            </div><!-- end-div row -->

          </div><!-- end-div "" -->
        </div><!-- end-div center_col -->
	<script>
		function validateTemplateCreation(value)
		{
			clearErrorMessages();
			var ruleName = document.getElementById("ngv_addval_rulename").value;
			var ruleCriteria = document.getElementById("ngv_addval_valcriteria").value;
			var ruleApplicability = document.getElementById("ngv_certaincriteria").value;
			
			if(ruleName == "")
			{
				displayErrorMessage("UI-1031");
				return false;
			}
			else if(!(document.getElementById("validityCriteria").checked || document.getElementById("errorCriteria").checked || document.getElementById("warningCriteria").checked))
			{
				displayErrorMessage("UI-1032");
				return false;
			}
			else if(ruleCriteria == "")
			{
				displayErrorMessage("UI-1033");
				return false;
			}
			else if(!(document.getElementById("applicabilityAll").checked || document.getElementById("applicabilityCertain").checked))
			{
				
				displayErrorMessage("UI-1034");
				return false;
			}
		 	else if(document.getElementById("applicabilityCertain").checked && ruleApplicability == "")
			{
				displayErrorMessage("UI-1035");
				return false;
			} 
			else
			{
				var form = document.getElementById("PROJSETTINGS_form");
				document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
				document.getElementById("clickValue").value = value;
				  if (validateFormParameters())
				 { 
					form.submit();
				 } 
			}
		}
		
		function submitForm()
		{
			
			return validateTemplateCreation('ngv_valrs_valviewedit_savetpl');
		}
		
		function cancelForm()
		{
		
			var form = document.getElementById("PROJSETTINGS_form");
			document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
			document.getElementById("clickValue").value = "ngv_validate_viewtpl";
				form.submit();
		}
		function cancelViewEditTemplateForm()
		{
		
			var form = document.getElementById("PROJSETTINGS_form");
			document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
			document.getElementById("clickValue").value = "ngv_validate_viewtpl";
				form.submit();
		}
	</script>

<%
}
%>
</div>
