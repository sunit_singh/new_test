<%@page import="java.util.List"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDBCaseFieldCategoryType"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.sciformix.commons.ObjectPair" %>
<%@ page import=" com.sciformix.sciportal.apps.qr.QRTemplate" %>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.SciEnums.RecordState"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFields" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%
	String						l_mode								=	(String) request.getAttribute("fldmode");
	SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate			=	(SafetyDbCaseFieldsTemplate) request.getAttribute("safetydbcasefields");
	QRTemplate					l_qrtemplate						=	(QRTemplate) request.getAttribute("fldtemplatedata");
	List<SafetyDbCaseFields>	l_safetydbcasefieldlist				=	l_safetydbcasefieldtemplate.getCaseFieldList();
	RecordState					l_templatestate						=	l_qrtemplate.getState();
	String						l_cataliases						=	(String) request.getAttribute("categoryaliases");	
	int							l_catcount							=	0;
	String []					l_categories						=	null;
	
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
%>

<!DOCTYPE html>
<html>
	<head>
		
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
			var fieldsDictionary	=	[];
			function fncancel () {
				document.frmmain.fldtemplateseqid.value	=	'<%= l_qrtemplate.getSequenceId() %>';
				document.frmmain.portal_action.value	=	"gettemplatedetails";
				document.frmmain.fldmode.value			=	"view";
				document.frmmain.submit();
			}
			
			function fnsubmit () {
				var	l_casefieldmapping	=	"";
				var checkboxelements = document.getElementsByTagName('input');
		        for (var i = 0; i < checkboxelements.length; i++) {
		        	if ((checkboxelements[i].type == 'checkbox') && checkboxelements[i].checked && !(checkboxelements[i].name == "fldselectall")) {
		        		l_casefieldmapping	=	l_casefieldmapping+checkboxelements[i].name.slice(18)+"~";
		        	}
		        }
		        l_casefieldmapping	=	l_casefieldmapping.slice(0,-1);
		        document.frmmain.fldcasefieldmapping.value		=	l_casefieldmapping;
		        document.frmmain.fldtemplateseqid.value			=	'<%= l_qrtemplate.getSequenceId()%>';
		        document.frmmain.portal_action.value			=	"updateqrtcasefield";	
		        document.frmmain.fldmode.value					=	"view";
		        if (validateFormParameters()) {
		        	document.frmmain.submit();
				}
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnselectall () {
				var checkboxelements = document.getElementsByTagName('input');
				if (document.getElementById("fldselectall").checked) {
					for (var i = 0; i < checkboxelements.length; i++) {
				        if (checkboxelements[i].type == 'checkbox') {
				        	checkboxelements[i].checked = true;
				        }
					}
				} else {
					for (var i = 0; i < checkboxelements.length; i++) {
				        if (checkboxelements[i].type == 'checkbox') {
				        	checkboxelements[i].checked = false;
				        }
					}
				}
		       
			}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="QR-PORTAL-APP">
			<input type="hidden" id="fldtemplateseqid" name="fldtemplateseqid" value="">
			<input type="hidden" id="fldcasefieldmapping" name="fldcasefieldmapping" value="">
			<input type="hidden" id="fldmode" name="fldmode" value ="">
			<div role="main">
				<div>
					<div class="page_title">
						<div class="title_left">
							<h3>Quality Review Template</h3>
						</div>
					</div>
					<div class="clearfix">
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										Review Field Mapping
									</h2>
									<div class="clearfix">
									</div>
								</div>
								<div class="x_content">
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldtemplatename">
												Template Name
											</label>
										</b>	
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_qrtemplate.getTemplateName() %>
											</label>
										</div>
									</div>	
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Template State
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_templatestate.displayName() %>
											</label>
										</div>
									</div>
									<table class="table table-striped">
                      					<thead>
                        					<tr>
                        						<th>Field Name</th>
                          						<th>Field Group</th>
                          						<th align="center">Review Field</th>
                          				</thead>
                          				<tbody>
                          					<% if (l_safetydbcasefieldlist != null) { %>
                          						<tr>
                          							<td></td>
                          							<td></td>
                          							<td align="center">
                          								<input type="checkbox" name="fldselectall" id="fldselectall" value="Select" onclick="fnselectall()">
                          							</td>
                          						</tr>
                          						<% for (SafetyDbCaseFields l_safetydbcasefield : l_safetydbcasefieldlist) { %>
                          							<tr>
	                          							<td>
	                          								<% if (l_catcount == 0 || l_catcount == 1) { %>
															<% } else if (l_catcount == 2) {
																if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																	<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																<% } else if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																	<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																<% } %>
															<% } else if (l_catcount == 3) { 
																if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																	<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																<% } else if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																	<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																<% } else if (l_safetydbcasefield.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
																	<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
																<% } %>
															<% } else { %>
															<% } %>
	                          								<%= l_safetydbcasefield.getCaseFieldName() %>
	                          							</td>
	                          							<td>
	                          								<%= l_safetydbcasefield.getCaseFieldTab() %>
	                          							</td>
	                          							<% if (l_qrtemplate != null && l_qrtemplate.getTemplateCaseFields() != null) {
		                          								if (l_qrtemplate.getTemplateCaseFields().indexOf(l_safetydbcasefield.getCaseFieldMnemonic()) != -1) { %>
                          											<td align="center">
                          												<input type="checkbox" name="__fldfieldmnemonic<%= l_safetydbcasefield.getCaseFieldMnemonic() %>"
                          												 	id="__<%= l_safetydbcasefield.getCaseFieldMnemonic() %>" checked>
                          											</td> <%
		                          								} else { %>
		                          									<td align="center">
                     													<input type="checkbox" name="__fldfieldmnemonic<%= l_safetydbcasefield.getCaseFieldMnemonic() %>"
                     														id="__<%= l_safetydbcasefield.getCaseFieldMnemonic() %>"> 
                     												</td>
		                          							 <% }
		                          							} else { %>
                        										<td align="center">
                     												<input type="checkbox" name="__fldfieldmnemonic<%= l_safetydbcasefield.getCaseFieldMnemonic() %>"
                     													id="__<%= l_safetydbcasefield.getCaseFieldMnemonic() %>"> 
                     											</td> <%
		                          						} %>
	                          						</tr>	<%
                          						}	
                          					} %>
                          				</tbody>
                          			</table>
                          			<div class="ln_solid"></div>
                         			<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
											<button type="button" class="btn btn-primary" onclick="fncancel();" value="Cancel">
												Cancel
											</button>
											<button type="button" class="btn btn-success" onclick="fnsubmit();" value="Submit">
												Submit
											</button>
		                          		</div>
		                          	</div>
								</div>
							</div>
						</div>		
					</div>
					<div class="clearfix">
					</div>
				</div>
			</div>		
		</csrf:form>
	</body>
</html>