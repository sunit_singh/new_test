<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplate"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>

<script type="text/javascript">
function submitForm(value)
{ 
	clearErrorMessages();
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value= "NGV_Protal_App" ;
	document.getElementById("clickValue").value =value;
	if (validateFormParameters())
	 {
		form.submit();
	 }
	
}
function validateFormParameters () {
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}
function validateTemplateHeaderContent(value)
{
	submitForm(value);
}

function validateRuleSet(value)
{
	var templateRuleSetName = document.getElementById("ngv_addtpl_tplrulesetname").value;
	
	if(templateRuleSetName =="")
	{
		displayErrorMessage("UI-1008");
		return false;
	}else
	{
		submitForm(value);
	}
	
}

function validateInput(value, updateTemplate)
{
	var templateContent = null;
	var templateName = document.getElementById("ngv_addtpl_tplname").value;
	
	var templateCriteria = document.getElementById("ngv_addtpl_tplcriteria").value;
	
	if(updateTemplate == null)
	{
		templateContent = document.getElementById("ngv_addtpl_tpltext_file").value;
	}else
	{
		
		templateContent = document.getElementById("ngv_addtpl_tpltext").value;
	}
	
	if(templateName =="")
	{
		displayErrorMessage("UI-1005");
		return false;
	}
	else if(templateCriteria == "")
	{
		displayErrorMessage("UI-1007");
		return false;
	}else if(templateContent == "")
	{
		displayErrorMessage("UI-1006");
		return false;
	}
	else
	{
		var form = document.getElementById("PROJSETTINGS_form");
		document.getElementById("portal_action").value = "NGV_Protal_App";
		document.getElementById("clickValue").value =value;
		if (validateFormParameters())
		 {
			form.submit();
		 }
	}
}
</script>
<%
String sNewPortalActionLocal = (String) request.getAttribute("new-portalaction");

if (sNewPortalActionLocal.equals("ngv_template_createnew_2"))
{
%>

		<div class="center_col" role="main">
          <div class="">
          	<%=displayFormHeader("Template Ruleset Creation") %>
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip("Create a Template Ruleset", false, false) %>
                  
                  <div class="x_content">
                  	 <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left"> 
						 <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Mode</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="iCheck"  id="createFromScratch" onclick="javascript:return disableImport();"  checked /> Create from scratch</label>
	                         </div>
	                    </div>
	                    <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                           <label><input type="radio"  name="iCheck" id="importFromFile" onclick="javascript:return enableImport();" /> Import from file</label>
                         </div>
                       </div>
	                    <div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                           <input type="file" name="ngv_templaters_importfile" id="ngv_templaters_importfile" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" disabled="disabled"/>
                         </div>
                       </div> 
                          
	                    <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
							<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
                       </div>
                       
<!--                           <label class="control-label">Select File</label>
							<input id="input-22" name="input22[]" type="file" class="file-loading" accept="text/plain" multiple>
							<script>
							$(document).on('ready', function() {
							    $("#input-22").fileinput({
							        previewFileType: "text",
							        allowedFileExtensions: ["txt", "md", "ini", "text"],
							        previewClass: "bg-warning"
							    });
							});
							</script>
 -->
                  	 	<%=displayTextBox("ngv_addtpl_tplrulesetname", "Template Ruleset Name", true, "")%> 
                  	   <div class="ln_solid"></div>	
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  <button type="button" class="btn btn-primary"
									onclick="return cancelCreateRulesetForm();">Cancel</button>
									
						<button type="button" class="btn btn-success"
									onclick="return submitForm('ngv_tplrsimport_step1sb');">Submit</button>
					
					
					</div>	
									
					</div>	 			     
	        		<%--  <jsp:include page="/WEB-INF/pages/formSubmitToolbar.jsp" flush="true" />  --%>
	        		<!-- </form> -->
	        		<script>
	        		
	        		function disableImport()
	        		{
	        			var importFromFile = document.getElementById("createFromScratch").value;
						if(importFromFile == "on")
						{
							document.getElementById("ngv_templaters_importfile").disabled = true;
							document.getElementById("ngv_addtpl_tplrulesetname").disabled = false;
							document.getElementById("ngv_templaters_importfile").value = "";
						} 
						return true;
	        		}
	        			        			   
	        		function enableImport()
	        		{
	        			var importFromFile = document.getElementById("importFromFile").value;
						if(importFromFile == "on")
						{
							document.getElementById("ngv_templaters_importfile").disabled = false;
							document.getElementById("ngv_addtpl_tplrulesetname").value = "";
							document.getElementById("ngv_addtpl_tplrulesetname").disabled = true;
						} 
						return true;
	        		}
	        		function submitImport(action)
					{
						//return validateTemplateCreation('ngv_tplrs_tplviewedit_savetpl');
						var form = document.getElementById("PROJSETTINGS_form");
						document.getElementById("portal_action").value = "NGV_Protal_App";
						document.getElementById("clickValue").value =action;
						
						if (validateFormParameters())
						 {
							form.submit();
						 }
					}
	        		
						function submitForm(action)
						{
							clearErrorMessages();
							if (document.getElementById("createFromScratch").checked) {
								var temprulesetname	=	document.getElementById("ngv_addtpl_tplrulesetname").value;
								if (temprulesetname == "") {
									displayErrorMessage("UI-1008");
									return false;
								}
							} else {
								var fileName=document.getElementById("ngv_templaters_importfile").value;
								if(fileName=="")
								{
									displayErrorMessage("UI-1017");
									return false;
								}
							}
							
							var fileName=document.getElementById("ngv_templaters_importfile").value;
							if(fileName=="")
							{
								var form = document.getElementById("PROJSETTINGS_form");
								document.getElementById("portal_action").value = "NGV_Protal_App";
								document.getElementById("clickValue").value ="ngv_template_createnew_3";
								
								if (validateFormParameters())
								 {
									form.submit();
								 }
							}else
							{
								
								var form = document.getElementById("PROJSETTINGS_form");
								document.getElementById("portal_action").value = "NGV_Protal_App";
								document.getElementById("clickValue").value =action;
								
								if (validateFormParameters())
								 {
									form.submit();
								 }
							}
							
						}
						
						function cancelForm()
						{
						
							var form = document.getElementById("PROJSETTINGS_form");
							document.getElementById("portal_action").value = "NGV_Protal_App";
							document.getElementById("clickValue").value ="ngv_template_createnew_3";
							form.submit();
						}
						
						function cancelCreateRulesetForm()
						{
						
							var form = document.getElementById("PROJSETTINGS_form");
							document.getElementById("portal_action").value = "NGV_Protal_App";
							document.getElementById("clickValue").value =null;
							form.submit();
						}
						
						
	        		</script>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>

	<!-- <tr><td>&nbsp;Template Ruleset name</td><td>&nbsp;<INPUT type="text" name="ngv_addtpl_tplrulesetname" id="ngv_addtpl_tplrulesetname" /></td><td>&nbsp;</td></tr> -->
	<!-- <td colspan="3" align="center">&nbsp;<input type="submit" class="btn btn-default" value="Next" onclick="return validateRuleSet('ngv_template_createnew_3');" /></td> -->
<%
}
else if (sNewPortalActionLocal.equals("ngv_tplrs_tplviewedit_dp"))
{
	String sEdittemplateFLag =  (String) request.getAttribute("ngv_editTemplateFlag");
	NgvTemplateRuleset oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
	// NgvTemplateRuleset oTemplateRuleset = (NgvTemplateRuleset) request.getAttribute("ngv_template_ruleset");
	String sTemplateRulesetName = (oTemplateRuleset!=null?oTemplateRuleset.getRulesetName():"");
	int nTemplateRulesetId = (oTemplateRuleset!=null ? oTemplateRuleset.getRulesetId() : -1);
	
	String sMode = (String) request.getAttribute("portal_action_mode");
	sMode = (StringUtils.isNullOrEmpty(sMode) ? "view" : sMode);
	String sFormTitle = (sMode.equals("create") ? "Template Creation" : sMode.equals("edit") ? "Template Modification" : "Template View");
	String sFormTooltip = (sMode.equals("create") ? "Create a Template" : sMode.equals("edit") ? "Modify the Template" : "View Template configuration");
	
	
	NgvTemplate oTemplate = (NgvTemplate) request.getAttribute("ngv_selectedTemplate");
	boolean bTemplateEditMode = (oTemplate!=null);
	
	String sCurrentTemplateName = (oTemplate !=null ? oTemplate.getTemplateName() : "");
	String sCurrentTemplateCriteria = (oTemplate !=null ? oTemplate.getTemplateCriteria() : "");
	String sCurrentTemplateContent = (oTemplate !=null && oTemplate.getTemplateContent() != null ? oTemplate.getTemplateContent().getContent() : "");
	%>
		<div class="center_col" role="main">
          <div class="">
          	<%=displayFormHeader(sFormTitle) %>
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip(sFormTooltip, false, false) %>
                  
                  <div class="x_content">
                  	<!-- <form id="PROJSETTINGS_form" data-parsley-validate class="form-horizontal form-label-left"> -->
	                  	<%=displayLabelPair("Template Ruleset Name", false, sTemplateRulesetName)%>
	                  	<%if(sEdittemplateFLag!=null && sEdittemplateFLag.equals("readOnly")){ %>
	                  		<%=displayTextBox("ngv_addtpl_tplname", "Template Name", true, sCurrentTemplateName, "", bTemplateEditMode)%>
	                  	
	                  		<%=displayTextBox("ngv_addtpl_tplcriteria", "Template Criteria", true, sCurrentTemplateCriteria,"",bTemplateEditMode)%>
	                  	
	                  		<%=displayTextArea("ngv_addtpl_tpltext", "Template Content", true, 10, 80, sCurrentTemplateContent, bTemplateEditMode)%>
	                  	<div class="ln_solid"></div>	
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  <button type="button" class="btn btn-primary"
									onclick="return cancelViewEditTemplateForm();">Cancel</button>
					</div>		
	                  	<%}else
	                  	   {
	                  		if(!bTemplateEditMode)
	                  		{	
		                  		sCurrentTemplateName = (String)request.getAttribute("r_templateName");
		                  		sCurrentTemplateCriteria = (String)request.getAttribute("r_criteria");
		                  		sCurrentTemplateContent = (String)request.getAttribute("r_templateContent");
	                  		}
	                  		%>
	                  		
		                  	<%=displayTextBox("ngv_addtpl_tplname", "Template Name", true, sCurrentTemplateName, "", bTemplateEditMode)%>
		                  	
		                  	<%=displayTextBox("ngv_addtpl_tplcriteria", "Template Criteria", true, sCurrentTemplateCriteria)%>
		                  	
		                  	<%=displayTextArea("ngv_addtpl_tpltext", "Template Content", true, 10, 80, sCurrentTemplateContent, false)%>
		                  		<div class="ln_solid"></div>	
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  <button type="button" class="btn btn-primary"
									onclick="return cancelViewEditTemplateForm();">Cancel</button>
									
					 <button type="button" class="btn btn-success"
									onclick="return submitForm();">Submit</button>
					</div>		
	                  	<%} %>
		        		<%-- <jsp:include page="/WEB-INF/pages/formSubmitToolbar.jsp" flush="true" /> --%>
		        		
					</div>	 
						<input type="hidden" id="ngv_tplrulesetid" name="ngv_tplrulesetid" value="<%=nTemplateRulesetId%>" />
						<input type="hidden" id="ngv_selectedtemplateid" name="ngv_selectedtemplateid" value="<%=(oTemplate!=null?oTemplate.getTemplateSeqId():-1)%>" />
	        		<!-- </form> -->
                  </div><!-- end-div x_content -->
                </div><!-- end-div x_panel -->
              </div><!-- end-div ... -->
            </div><!-- end-div row -->

          </div><!-- end-div "" -->
        </div><!-- end-div center_col -->
	<script>
		function validateTemplateCreation(value)
		{
			clearErrorMessages();
			var templateName = document.getElementById("ngv_addtpl_tplname").value;
			var templateCriteria = document.getElementById("ngv_addtpl_tplcriteria").value;
			var templateContent = document.getElementById("ngv_addtpl_tpltext").value;
			
			if(templateName =="")
			{
				displayErrorMessage("UI-1005");
				return false;
			}
			else if(templateCriteria == "")
			{
				displayErrorMessage("UI-1007");
				return false;
			}else if(templateContent == "")
			{
				displayErrorMessage("UI-1006");
				return false;
			}
			else
			{
				var form = document.getElementById("PROJSETTINGS_form");
				document.getElementById("portal_action").value = "NGV_Protal_App";
				document.getElementById("clickValue").value =value;
			
				if (validateFormParameters())
				 {
					form.submit();
				 }
			}
		}
		
		function submitForm()
		{
			
			return validateTemplateCreation('ngv_tplrs_tplviewedit_savetpl');
		}
		
		function cancelForm()
		{
		
			var form = document.getElementById("PROJSETTINGS_form");
			document.getElementById("portal_action").value = "NGV_Protal_App";
			document.getElementById("clickValue").value ="ngv_template_viewtpl";
		
			form.submit();
		}
		function cancelViewEditTemplateForm()
		{
		
			var form = document.getElementById("PROJSETTINGS_form");
			document.getElementById("portal_action").value = "NGV_Protal_App";
			document.getElementById("clickValue").value ="ngv_template_viewtpl";
		
			form.submit();
		}
	</script>

<%
}
else if (sNewPortalActionLocal.equals("ngv_templateruleset_uploadheader_2"))
{
	NgvTemplateRuleset oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
	int nTemplateRulesetId = (oTemplateRuleset!=null ? oTemplateRuleset.getRulesetId() : -1);
	String sTemplateRulesetName = oTemplateRuleset.getRulesetName();
	String sTemplateHeader = oTemplateRuleset.getHeader();

	sTemplateHeader = (sTemplateHeader != null ? sTemplateHeader : "");
	String sMode = (String) request.getAttribute("portal_action_mode");
	sMode = (StringUtils.isNullOrEmpty(sMode) ? "view" : sMode);
	String sFormTitle = (sMode.equals("create") ? "Template Creation" : sMode.equals("edit") ? "Template Modification" : sMode.equals("header")?"Ruleset Header": "Template");
	
	NgvTemplate oTemplate = (NgvTemplate) request.getAttribute("ngv_selectedTemplate");

%>
	<div class="center_col" role="main">
          <div class="">
          	<%=displayFormHeader(sFormTitle) %>
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                  	<!-- <form id="PROJSETTINGS_form" data-parsley-validate class="form-horizontal form-label-left"> -->
	                  	<%=displayTextBox("ngv_addtpl_tplrulesetname", "Template Ruleset Name", true, sTemplateRulesetName, "", true)%>
	                  	
	                  	<%=displayTextArea("ngv_addtpl_tplheader", "Header Content", false, 8, 60, sTemplateHeader, false)%>
	                  	
		        		<%-- <jsp:include page="/WEB-INF/pages/formSubmitToolbar.jsp" flush="true" /> --%>
		        		<input type="hidden" id="ngv_tplrulesetid" name="ngv_tplrulesetid" value="<%=nTemplateRulesetId%>" />
						<input type="hidden" id="ngv_selectedtemplateid" name="ngv_selectedtemplateid" value="<%=(oTemplate!=null?oTemplate.getTemplateSeqId():-1)%>" />
						<div class="form-group">	
                  	  		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  			<button type="button" class="btn btn-primary"
									onclick="return cancelForm();">Cancel</button>
								 <button type="button" class="btn btn-success"
									onclick="return validateTemplateHeaderContent('ngv_template_savetplheader');">Save</button>
							</div>	
						</div>
                  </div><!-- end-div x_content -->
                </div><!-- end-div x_panel -->
              </div><!-- end-div ... -->
            </div><!-- end-div row -->

          </div><!-- end-div "" -->
        </div><!-- end-div center_col -->
        <script>
	        function cancelForm() {
			
				var form = document.getElementById("PROJSETTINGS_form");
				document.getElementById("portal_action").value = "NGV_Protal_App";
				document.getElementById("clickValue").value ="ngv_template_viewtpl";
				form.submit();
			}
        </script>
	
	
	<%-- <tr><td>&nbsp;Template Ruleset Name</td><td>&nbsp;<INPUT type="text" name="ngv_addtpl_tplrulesetname" id="ngv_addtpl_tplrulesetname" value="<%=sTemplateRulesetName%>" disabled/></td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;Template Content</td><td>&nbsp;<textarea rows="8" cols="60" name="ngv_addtpl_tplheader" id="ngv_addtpl_tplheader"><%=sTemplateHeader%></textarea></td><td>&nbsp;</td></tr>
	<tr>
		<td colspan="3">&nbsp;<input type="submit" class="btn btn-default" value="Save" onclick="return validateTemplateHeaderContent('ngv_template_savetplheader');" /></td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr> --%>

<%
}
%>