<%@page import="java.util.ArrayList"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.apps.qr.QRReviewData" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDBCaseFieldCategoryType"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFields"%>
<%@page import="com.sciformix.sciportal.pvcp.PvcpCaseMaster"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="com.sciformix.sciportal.user.UserInfo" %>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.commons.utils.StringUtils" %>

<% try {
	Map<String,List<String>> l_enableUdfMap	= 	(Map<String,List<String>>)request.getAttribute("enableUdfList");;
	QRReviewData		l_qrreviewdata		=	(QRReviewData) request.getAttribute("reviewdata");
	String				l_layouttype		=	(String)	request.getAttribute("UserLayoutType");
	String				l_reporttype		=	(String)	request.getAttribute("fldreporttype");
	List<UserInfo>		l_listusers			=	(List<UserInfo>)	request.getAttribute("list_of_users");
	Integer				l_startdatecount	=	(Integer)	request.getAttribute("fldstartdatecount"),
						l_enddatecount		=	(Integer)	request.getAttribute("fldenddatecount"),
						l_searchtype		=	(Integer)	request.getAttribute("fldsearchtype");
	String				l_cataliases		=	(String) 	request.getAttribute("categoryaliases");
	String				l_searchedcaseid	=	(String)	request.getAttribute("fldsearchedcaseid");	
	PvcpCaseMaster		o_pvcpcasemaster 	= 	(PvcpCaseMaster) request.getAttribute("pvcp_case_record");
	
	boolean				l_ismrnameshow		=	(Boolean)	request.getAttribute("ismrnameshow");
	Map<String,String>	l_errortypes		=	null;
	String []			l_categories		=	null;
	String[] 			udfPropValue 		= 	null;
	String[] 			udfPropSepValue		= 	null;
	String projUdfPropDelm					=	":";
	int					l_cdbrflagstring	=	(Integer)request.getAttribute("cdbrflag");
	int					l_catcount			=	0;
	int					l_cat1count			=	0,
						l_cat2count			=	0,
						l_cat3count			=	0,
						l_cat1reviewcount	=	0,
						l_cat2reviewcount	=	0,
						l_cat3reviewcount	=	0;
	
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
	l_errortypes		=	(Map<String,String>) request.getAttribute("errortypes");				
	for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
		for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) { 
			switch (l_tabdata.getCaseFieldType()) {
				case CAT1 : l_cat1reviewcount++;
							if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
								l_cat1count++;
							}
				break;
				case CAT2 : l_cat2reviewcount++;
							if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
								l_cat2count++;
							}
				break;
				case CAT3 : l_cat3reviewcount++;
							if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
								l_cat3count++;
							}
				break;
				case NOCAT	:
				break;
				
			}
		}
	}
	List<String>	l_udfvaluelist	=	new ArrayList<String>(); 
	if (o_pvcpcasemaster != null) {
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf1());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf2());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf3());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf4());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf5());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf6());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf7());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf8());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf9());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf10());
	}
	
%>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Review Quality Details
		</title>
		<link rel="stylesheet" href="css/SciCustom.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.nonblock.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.brighttheme.css">
		<link rel="stylesheet" href="css/pnotify/pnotify.buttons.css"> 
		<link rel="stylesheet" href="css/pnotify/pnotify.material.css">
		
		<script src="js/jquery.js"></script>
		<script src="js/pnotify/pnotify.js"></script>
		<script src="js/pnotify/pnotify.nonblock.js"></script>
		<script src="js/pnotify/pnotify.buttons.js"></script>
		<script src="js/pnotify/pnotify.confirm.js"></script>
		<script src="js/pnotify/pnotify.reference.js"></script>
		<script src="js/moment/moment.min.js"></script>
		<script type="text/JavaScript">
			var statisticsShown		=	false;
			var fieldsDictionary	=	[];
			var	totalFields			=	<%= l_qrreviewdata.getFieldsReviewed()%>;
			var	fieldsReviewed		=	<%= l_qrreviewdata.getFieldsReviewed()%>;
			var	fieldsWithoutError	=	<%= l_qrreviewdata.getFieldsWithoutError()%>;
			var	fieldsIncorrect		=	<%= l_qrreviewdata.getFieldsError()%>;
			var cat1count			=	<%= l_cat1count%>;
			var cat2count			=	<%= l_cat2count%>;
			var cat3count			=	<%= l_cat3count%>;
			var cat1reviewcount		=	<%= l_cat1reviewcount%>;
			var cat2reviewcount		=	<%= l_cat2reviewcount%>;
			var cat3reviewcount		=	<%= l_cat3reviewcount%>;
			
			var	displayText			=	"<div class='x_content'><table class='table'><tbody>"
									+	"<tr><td><label>Fields Reviewed</label></td><td><label id='fieldsreviewed'> fieldsReviewedreplace/totalFieldsreplace</label></td></tr>"
									/* +	"<tr><td><label>Fields Without Error</label></td><td><label id='fieldswithouterror'>fieldsWithoutErrorreplace/fieldsReviewedreplace</label></td></tr>"
									+	"<tr><td><label>Fields Incorrect</label></td><td><label id='fieldsincorrect'>fieldsIncorrectreplace/fieldsReviewedreplace</label></td></tr>" */
									+	"<tr><td><label>Case Quality</label></td><td><label id='casequality'>caseequalityreplace%</label></td></tr>"
									/* +	"<tr><td><label>Case Result</label></td><td><label id='caseresult'>"+"caseresultreplace"+"</label></td></tr>"
									+	"<tr><td><label>Cat1 Quality</label></td><td><label id='cat1quality'>cat1qualityreplace%</label></td></tr>"
									+	"<tr><td><label>Cat2 Quality</label></td><td><label id='cat2quality'>cat2qualityreplace%</label></td></tr>"
									+	"<tr><td><label>Cat3 Quality</label></td><td><label id='cat3quality'>cat3qualityreplace%</label></td></tr>" */
									+	"</tbody></table></div>"
									;
			var displayStatText; 
			
			
			$(window).load(function(){
				$('.btnNext').click(function(){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				});
				$('.btnPrevious').click(function(){
					$('.nav-tabs > .active').prev('li').find('a').trigger('click');
				});
				$('[data-toggle="tooltip"]').tooltip();
				updateStatistics();
			});
			function fncancel () {
				<% if (StringUtils.isNullOrEmpty(l_reporttype)) { %>
					document.frmmain.portal_action.value = "ilqr_step2";
				<% } else { %>
					document.frmmain.portal_action.value 		= "preparereport";
					document.frmmain.fldreporttype.value		= "<%=l_reporttype%>";	
					document.frmmain.fldstartdatecount.value	= '<%=l_startdatecount%>';
					document.frmmain.fldenddatecount.value		= '<%=l_enddatecount%>';
					document.frmmain.fldsearchtype.value		= '<%=l_searchtype%>';
					document.frmmain.fldsearchedcaseid.value	= '<%=l_searchedcaseid%>';
				<% } %>
				document.frmmain.submit();
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				for (var i=0; i < oFormObject.length; i++) {
					if(oFormObject.elements[i].name.startsWith("__")) {
						document.getElementById(oFormObject.elements[i].id).disabled = true;
					}
				}	
				var result	=	validateInputParameter(oFormObject);
				if (!result) {
					for (var i=0; i < oFormObject.length; i++) {
						if(oFormObject.elements[i].name.startsWith("__")) {
							document.getElementById(oFormObject.elements[i].id).disabled = false;
						}
					}	
				}
				return result;
			}
			function replaceString () {
				displayStatText	=	displayText;
				displayStatText	=	displayStatText.replace(/fieldsReviewedreplace/g,fieldsReviewed);
				displayStatText	=	displayStatText.replace("totalFieldsreplace",totalFields);
				/* displayStatText	=	displayStatText.replace("fieldsWithoutErrorreplace",fieldsWithoutError);
				displayStatText	=	displayStatText.replace("fieldsIncorrectreplace",fieldsIncorrect); */
				
				/* if (cat1reviewcount == 0) {
					displayStatText	=	displayStatText.replace("cat1qualityreplace%","X");
				} else {
					if (cat1count == 0) {
						displayStatText	=	displayStatText.replace("cat1qualityreplace%",0);
					} else {
						displayStatText	=	displayStatText.replace("cat1qualityreplace",((cat1count*100)/(cat1reviewcount)).toFixed(2));
					}
				}
				if (cat2reviewcount == 0) {
					displayStatText	=	displayStatText.replace("cat2qualityreplace%","X");
				} else {
					if (cat2count == 0) {
						displayStatText	=	displayStatText.replace("cat2qualityreplace%",0);
					} else {
						displayStatText	=	displayStatText.replace("cat2qualityreplace",((cat2count*100)/(cat2reviewcount)).toFixed(2));
					}
				}
				if (cat3reviewcount == 0) {
					displayStatText	=	displayStatText.replace("cat3qualityreplace%","X");
				} else {
					if (cat3count == 0) {
						displayStatText	=	displayStatText.replace("cat3qualityreplace%",0);
					} else {
						displayStatText	=	displayStatText.replace("cat3qualityreplace",((cat3count*100)/(cat3reviewcount)).toFixed(2));
					}
				} */
				if (fieldsReviewed > 0) {
					if (fieldsWithoutError == 0) {
						displayStatText	=	displayStatText.replace("caseequalityreplace",0);
					} else {
						var	caseequalityreplace	=	((fieldsWithoutError*100)/(fieldsReviewed)).toFixed(2);
						displayStatText	=	displayStatText.replace("caseequalityreplace",caseequalityreplace);
						
					}
					/* if (fieldsIncorrect > 0) {
						displayStatText	=	displayStatText.replace("caseresultreplace","Failed");
					} else {
						displayStatText	=	displayStatText.replace("caseresultreplace","Passed");
					} */
				} else {
					/* displayStatText	=	displayStatText.replace("caseresultreplace",""); */
					displayStatText	=	displayStatText.replace("caseequalityreplace%","X"); 
				}
			}
			function enableDisableErrorType (operation, fieldName) {
				var l_errortypecount	=	<%= l_errortypes.keySet().size() %>;
				if (operation == "success") {
					document.getElementById(fieldName+"errortype")[0].disabled = false; 
					document.getElementById(fieldName+"errortype")[0].selected = true;
					for (var l_i=0; l_i < l_errortypecount; l_i++) {
						document.getElementById(fieldName+"errortype")[l_i+1].disabled = true; 
					}
				} else if (operation == "failure") {
					document.getElementById(fieldName+"errortype")[0].disabled = true;
					document.getElementById(fieldName+"errortype")[0].selected = false;
					for (var l_i=0; l_i < l_errortypecount; l_i++) {
						document.getElementById(fieldName+"errortype")[l_i+1].disabled = false; 
					}
					document.getElementById(fieldName+"errortype")[1].selected = true;
				} else {
					document.getElementById(fieldName+"errortype")[0].disabled = false; 
					document.getElementById(fieldName+"errortype")[0].selected = true;
					for (var l_i=0; l_i < l_errortypecount; l_i++) {
						document.getElementById(fieldName+"errortype")[l_i+1].disabled = false; 
					}
				}
			}
			function changeNoErrorColor (currObject, categorytype) {
				var	fieldName	=	currObject.name;
				var	fieldId		=	currObject.id;
				document.getElementById(fieldName+"label1").classList.add('custom-btn-green');
				document.getElementById(fieldName+"label1").classList.remove('custom-btn-grey');
				document.getElementById(fieldName+"label3").classList.add('custom-btn-grey');
				document.getElementById(fieldName+"label3").classList.remove('custom-btn-red');
				if(fieldsDictionary[fieldName] === undefined ) {
					fieldsReviewed++;
					catReviewUpdate(categorytype);
					catCountUpdate(categorytype,1);
				} else {
					if (fieldsDictionary[fieldName] == 2) {
						fieldsMissing--;
						catCountUpdate(categorytype,1);
					} else if (fieldsDictionary[fieldName] == 3) {
						fieldsIncorrect--;
						catCountUpdate(categorytype,1);
					} else {
						fieldsReviewed++;
					}
				}
				fieldsWithoutError++;
				fieldsDictionary[fieldName] = "1";
				enableDisableErrorType("success",fieldName);
				updateStatistics();	
			}
			function changeIncorrectDataColor (currObject, categorytype) {
				var	fieldName	=	currObject.name;
				var	fieldId		=	currObject.id;
				document.getElementById(fieldName+"label3").classList.add('custom-btn-red');
				document.getElementById(fieldName+"label3").classList.remove('custom-btn-grey');
				document.getElementById(fieldName+"label1").classList.add('custom-btn-grey');
				document.getElementById(fieldName+"label1").classList.remove('custom-btn-green');
				if(fieldsDictionary[fieldName] === undefined ) {
					fieldsReviewed++;
					catReviewUpdate(categorytype);
				} else {
					if (fieldsDictionary[fieldName] == 1) {
						fieldsWithoutError--;
						catCountUpdate(categorytype,2);
					} else if (fieldsDictionary[fieldName] == 2) {
						fieldsMissing--;
					} else {
						fieldsReviewed++;
					}
				}
				fieldsIncorrect++;
				fieldsDictionary[fieldName] = "3";
				enableDisableErrorType("failure",fieldName); 
				updateStatistics();
			}
			function catCountUpdate (categorytype,operation) {
				if (categorytype == 1) {
					if (operation == 1) {
						cat1count++;
					} else {
						if (cat1count > 0) {
							cat1count--;
						}
					}
				} else if (categorytype == 2) {
					if (operation == 1) {
						cat2count++;
					} else {
						if (cat2count > 0) {
							cat2count--;
						}
					}
				} else if (categorytype == 3) {
					if (operation == 1) {
						cat3count++;
					} else {
						if (cat3count > 0) {
							cat3count--;
						}
					}
				}
			}
			function catReviewUpdate (categorytype) {
				if (categorytype == 1) {
					cat1reviewcount++;
				} else if (categorytype == 2) {
					cat2reviewcount++;
				} else if (categorytype == 3) {
					cat3reviewcount++;
				}
			}
			function updateStatistics () {
				replaceString();
				if (statisticsShown == false) {
					notice	=	new PNotify({
				    	title: "Statistics",
				        name : "first",
				        type: "info",
				        text: displayStatText,
				         nonblock: {
				              nonblock: true,
				              nonblock_opacity: .9
				        },
				        styling: 'bootstrap3',
				        hide: false,
				    });
					statisticsShown	=	true;
				}
				var options	=	{
			        				text: displayStatText
			        			};
				notice.update(options);
			}
			function fnsetdata () {
				<% for (String l_fieldmnemonic : l_qrreviewdata.getQrlReviewFieldDetails().keySet()) { %>
					if(fieldsDictionary['__fldfieldmnemonic<%=l_fieldmnemonic%>'] === undefined ) {
						fieldsDictionary['__fldfieldmnemonic<%=l_fieldmnemonic%>'] = <%=l_qrreviewdata.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldStatus()%>;
						if (fieldsDictionary['__fldfieldmnemonic<%=l_fieldmnemonic%>'] == 1) {
							enableDisableErrorType("success","__fldfieldmnemonic<%=l_fieldmnemonic%>");
						} else if (fieldsDictionary['__fldfieldmnemonic<%=l_fieldmnemonic%>'] == 3) {
							var count = 1;
							enableDisableErrorType("failure","__fldfieldmnemonic<%=l_fieldmnemonic%>");
							<% for (String l_errorcode : l_errortypes.keySet()) {
							 	if (l_errorcode.equals(l_qrreviewdata.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldErrorCode())) { %>
							 		document.getElementById("__fldfieldmnemonic<%=l_fieldmnemonic%>"+"errortype")[count].selected = true;
							 <% } %>
							 	count++;
							<% } %>
						}
					} else {
						
					}
				<% } %>
			}
			function formSubmit () 
			{
				clearErrorMessages();
				/* if (document.getElementById("fldserious").value == "-1") {
					displayErrorMessage("UI-1029");
					return false;
				} */
				if (document.getElementById("fldcaseassoc").value == "-1") {
					displayErrorMessage("UI-1028");
					return false;
				}
				if (fieldsReviewed != (fieldsWithoutError+fieldsIncorrect)) {
					displayErrorMessage("UI-1030");
					return false;
				}

				document.frmmain.fldfieldsreviewed.value		=	fieldsReviewed;
				document.frmmain.fldfieldsWithoutError.value	=	fieldsWithoutError;
				document.frmmain.fldfieldsIncorrect.value		=	fieldsIncorrect;
				document.frmmain.fldcasequality.value			=	((fieldsWithoutError*100)/(fieldsReviewed)).toFixed(2);
				document.frmmain.fldmode.value					=	"edit";
				document.frmmain.fldcat1count.value				=	cat1count;
				document.frmmain.fldcat2count.value				=	cat2count;
				document.frmmain.fldcat3count.value				=	cat3count;
				document.frmmain.fldcat1reviewcount.value		=	cat1reviewcount;
				document.frmmain.fldcat2reviewcount.value		=	cat2reviewcount;
				document.frmmain.fldcat3reviewcount.value		=	cat3reviewcount;
				var	fieldjsondata	=	"";
				for (var key in fieldsDictionary) 
				{
					fieldjsondata	=	fieldjsondata+key.slice(18)+"~"+fieldsDictionary[key]+"~"+document.getElementById(key+"errortype").value+";";
				}
				fieldjsondata	=	fieldjsondata.slice(0,-1);
				fieldjsondata	=	fieldjsondata + "#,#";
				for (var key in fieldsDictionary) 
				{
					var l_comment	=	document.getElementById(key+"comment").value;
					if (l_comment != '' && (l_comment.indexOf("~") != -1 || l_comment.indexOf(";") != -1
							|| l_comment.indexOf("#,#") != -1 || l_comment.indexOf(":(") != -1 )) {
						displayErrorMessage("UI-1070");
						return false;
					}
					fieldjsondata	=	fieldjsondata+key.slice(18)+":("+document.getElementById(key+"comment").value+")#~#";
				}
				fieldjsondata	=	fieldjsondata.slice(0,-3);
				<%if(l_cdbrflagstring != -1){ %>
				if (document.frmmain.cdbrcheckbox.checked) {
					document.frmmain.cdbrcheckboxvalue.value = 1;
				} else {
					document.frmmain.cdbrcheckboxvalue.value = 0;
				}
				<%}%>
				document.frmmain.fldjsondata.value	=	fieldjsondata;
				document.frmmain.fldscftseqid.value	=	"<%=l_qrreviewdata.getQrTemplate().getSequenceId()%>";
				document.frmmain.fldcasereviewsumm.value = document.getElementById("casereviewsumm").value;
		        document.getElementById("portal_action").value = "ilqr_step4";
		        if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
		</script>
	</head>
	<body onload="fnsetdata();">
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldreporttype" name="fldreporttype" value="">
			<input type="hidden" id="fldfieldsreviewed" name="fldfieldsreviewed" value="">
			<input type="hidden" id="cdbrcheckboxvalue" name="cdbrcheckboxvalue" value="">
			<input type="hidden" id="fldfieldsWithoutError" name="fldfieldsWithoutError" value="">
			<input type="hidden" id="fldfieldsIncorrect" name="fldfieldsIncorrect" value="">
			<input type="hidden" id="fldcasequality" name="fldcasequality" value="">
			<input type="hidden" id="fldjsondata" name="fldjsondata" value="">
			<input type="hidden" id="fldscftseqid" name="fldscftseqid" value="">
			<input type="hidden" id="fldsequenceid" name="fldsequenceid" value="<%=l_qrreviewdata.getSequenceId()%>">
			<input type="hidden" id="fldmode" name="fldmode" value="">
			<input type="hidden" id="fldcaseid" name="fldcaseid" value="<%=l_qrreviewdata.getCaseId()%>">
			<input type="hidden" id="fldcaseversion" name="fldcaseversion" value="<%=l_qrreviewdata.getCaseVersion()%>">
			<input type="hidden" id="fldreviewversion" name="fldreviewversion" value="<%=l_qrreviewdata.getReviewVersion()+1%>">
			<input type="hidden" id="fldstartdatecount" name="fldstartdatecount" value="">
			<input type="hidden" id="fldenddatecount" name="fldenddatecount" value="">
			<input type="hidden" id="fldcat1count" name="fldcat1count" value="">
			<input type="hidden" id="fldcat2count" name="fldcat2count" value="">
			<input type="hidden" id="fldcat3count" name="fldcat3count" value="">
			<input type="hidden" id="fldcat1reviewcount" name="fldcat1reviewcount" value="">
			<input type="hidden" id="fldcat2reviewcount" name="fldcat2reviewcount" value="">
			<input type="hidden" id="fldcat3reviewcount" name="fldcat3reviewcount" value="">
			<input type="hidden" id="fldreviewtype" name="fldreviewtype" value="<%=l_qrreviewdata.getReviewType()%>">
			<input type="hidden" id ="fldcasereviewsumm"  name="fldcasereviewsumm"  value="" />
			<input type="hidden" id ="reviewround"  name="reviewround"  value="<%=l_qrreviewdata.getReviewRound()%>" />
			<input type="hidden" id="fldcmseqid" name="fldcmseqid" value="<%=o_pvcpcasemaster.getSeqId() %>">
			<input type="hidden" id="fldinternalcaseid" name="fldinternalcaseid" value="<%=o_pvcpcasemaster.getInternalCaseId() %>">
			<input type="hidden" id="fldsearchedcaseid" name="fldsearchedcaseid" value="">
			<input type="hidden" id="fldsearchtype" name="fldsearchtype" value="">
			
			<div class="title_left">
				<h3>Quality Review Case Details Edit</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Details
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><%= ((SafetyDbCaseFieldsTemplate)l_qrreviewdata.getScft()).getCaseIdAlias() %></th>
									<th><%= ((SafetyDbCaseFieldsTemplate)l_qrreviewdata.getScft()).getCaseVersionAlias() %></th>
									<th>Initial Receive Date</th>
									<th>Case Type</th>
									<th>Seriousness</th>
									<!-- <th>Review Type</th> -->
									<th>Case Associate</th>
									<% if (l_ismrnameshow) { %>
										<th>Medical Reviewer</th>
									<% } %>
									<% if(!l_enableUdfMap.isEmpty()) {
										for (String key : l_enableUdfMap.keySet()) {%>
											<th><%=l_enableUdfMap.get(key).get(0) %></th>
									<%	} 
									} %>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<%=l_qrreviewdata.getCaseId()%>
									</td>
									<td>
										<%=l_qrreviewdata.getCaseVersion()%>
									</td>
									<td>
										<%= new SimpleDateFormat("dd-MMM-yyyy").format(l_qrreviewdata.getCaseRecvDate()) %>
									</td>
									 <td>
										<%=o_pvcpcasemaster.getCaseType() != null ? o_pvcpcasemaster.getCaseType(): ""%>
									</td>
									<td>
										<%=l_qrreviewdata.getCaseSeriousness()%>
									</td>
									<td>
										<select class="form-control" name="fldcaseassoc" id="fldcaseassoc">
											<option value="-1">(Select a user)</option>
											<% for (UserInfo l_userinfo : l_listusers) { %>
												<% if (l_qrreviewdata.getCaseAssociate() == l_userinfo.getUserSeqId()) { %>
													<option selected value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
												<% } else { %>
													<option value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
												<% } %>
												
											<% } %>
										</select>
									</td>
									<% if (l_ismrnameshow) { %>
										<td>
											<select class="form-control" name="fldmedicalreviewer" id="fldmedicalreviewer">
												<option value="-1">(Select a user)</option>
												<% for (UserInfo l_userinfo : l_listusers) { %>
													<% if (l_qrreviewdata.getMedicalReviewerId() == l_userinfo.getUserSeqId()) { %>
														<option selected value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
													<% } else { %>
														<option value ="<%= l_userinfo.getUserSeqId()%>"><%= l_userinfo.getUserDisplayName()%> (<%= l_userinfo.getUserId() %>) </option>
													<% } %>
												<% } %>
											</select>
										</td>
									<% } %>
									<% if(!l_enableUdfMap.isEmpty()) {
										for (int l_i=1; l_i<=10; l_i++) {
											if (l_enableUdfMap.containsKey(QRConstant.ENABLE_UDF_MAP_KEY+l_i)) { %>
												<td>
												<% String	l_udfprojvalue	= l_enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY+l_i).get(1);	
													if (l_udfprojvalue != null) { %>
													<select  name ="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>"
													 	id ="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" class ="form-control">
														<% for (String l_udfvalue : StringUtils.splitOnComma(l_udfprojvalue)) { 
															String[]	l_udfdetails = l_udfvalue.split(":"); 
															if(l_udfdetails[0].equals(l_udfvaluelist.get(l_i-1))) { %>
																<option selected value ="<%= l_udfdetails[0]%>"><%= l_udfdetails[1]%></option>
														 <% } else { %>
														 		<option value ="<%= l_udfdetails[0]%>"><%= l_udfdetails[1]%></option>
														 <% } %>	
														<% } %>
													</select>
											 	<% } else { 
											 		if (StringUtils.isNullOrEmpty(l_udfvaluelist.get(l_i-1))) {%>
															<input type="text" name="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" maxlength="4000"
																id="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" class="form-control" value=""/>
													 <% } else { %>
													 		<input type="text" name="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" maxlength="4000"
																id="fld<%=QRConstant.ENABLE_UDF_MAP_KEY+l_i%>" class="form-control" value="<%=l_udfvaluelist.get(l_i-1)%>"/>
													 <% } %>
													
												<% } %>
												</td>
											<% }
										}
									} %>	
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Level Comments
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<%if(l_cdbrflagstring != -1){ %>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="cdbrcheckbox">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Correction done by reviewer">
                         		CDBR
                        	</span>
							</label>
							<div class="col-md-10 col-sm-10 col-xs-12">
								<label>
									<input type="checkbox" id="cdbrcheckbox" name="cdbrcheckbox"
									 class="js-switch" data-switchery="true" style="display: none;">
								</label>
							</div>	
						</div>
						<%} %>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="fldemailcclist">
								Comments
							</label>
							<div class="col-md-10 col-sm-10 col-xs-12">
								<textarea class="form-control vresize" id="casereviewsumm" name="casereviewsumm" rows="6" ><%=l_qrreviewdata.getCaseReviewSummary()%></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
						 	Case Field Details
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<% if ("Single Page".equals(l_layouttype)) { %>
							<table class="table table-bordered" id="detailstable">
								<thead>
									<tr>
										<th>Tab Name</th>
										<th>Field</th>
										<th>Review Status</th>
										<th>Error Type</th>
										<th>Comments</th>
									</tr>
								</thead>
								<tbody>	<%	
									int		l_tabindex	=	0;
									for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
										int		l_counttemp	=	1; 
										for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) { %>
										<tr>
											<% if (l_counttemp == 1 ) { %>
												<td rowspan="<%=l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname).size()%>">
													<b><%=l_tabname %></b>
												</td>
											<% } %>	
											<td>
												<% if (l_catcount == 0 || l_catcount == 1) { %>
												<% } else if (l_catcount == 2) {
													if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
														<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
													<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
														<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
													<% } %>
												<% } else if (l_catcount == 3) { 
													if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
														<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
													<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
														<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
													<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
														<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
													<% } %>
												<% } else { %>
												<% } %>
												<label>
													<%= l_tabdata.getCaseFieldName() %>
												</label>
											</td>
											<td>
												<% 	int	l_reviewstatus	=	l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus();
													if (l_reviewstatus == 1) { %>
														<div data-toggle="buttons">
															<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label1" class="btn btn-sm custom-btn-green" data-toggle-class="btn-primary" 
																data-toggle="tooltip" data-placement="left" data-original-title="Correct">
																<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>1" value="1"
																 	checked onchange="changeNoErrorColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);" />
																	<i class="glyphicon glyphicon-ok"></i>
															</label>
															<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label3" class="btn btn-sm custom-btn-grey" data-toggle-class="btn-primary"
																 data-toggle="tooltip" data-placement="right" data-original-title="Incorrect">
																<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>3" value="3"
																 	onchange="changeIncorrectDataColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);">
																	<i class="glyphicon glyphicon-remove"></i>
															</label>
														</div>
														
												<% } else if (l_reviewstatus == 2) { %>
														<label class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary">
															<i class="glyphicon glyphicon-remove"></i>
														</label>
												<% } else if (l_reviewstatus == 3) { %>
														<div data-toggle="buttons">
															<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label1" class="btn btn-sm custom-btn-grey" data-toggle-class="btn-primary" 
																data-toggle="tooltip" data-placement="left" data-original-title="Correct">
																<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>1"
																 	value="1" onchange="changeNoErrorColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);" />
																	<i class="glyphicon glyphicon-ok"></i>
															</label>
															<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label3" class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary" 
																data-toggle="tooltip" data-placement="right" data-original-title="Incorrect">
																<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>3"
																 value="3" checked onchange="changeIncorrectDataColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);">
																	<i class="glyphicon glyphicon-remove"></i>
															</label>
														</div>
												<% } %> 
											</td>
											<td>
												<select name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" 
													id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" class="form-control">
													<option value="-1">No Error</option>
													<% for (String l_errorcode : l_errortypes.keySet()) { %>
														<option value="<%= l_errorcode %>" disabled><%= l_errortypes.get(l_errorcode) %></option>
													<% } %>
												</select>
											</td>
											<td>
												<input type="text" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" maxlength="4000" 
												 	id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" class="form-control" 
													value="<%= l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldComment() %>" />
											</td>
										</tr>
										<%  
												l_counttemp++;
											} 
											l_tabindex++;
										%>
									<%	} %>
								</tbody>
							</table>
							<button type="button" class="btn btn-sm btn-primary " onclick="fncancel();" value="Cancel">
								Cancel
							</button>
							<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
								Submit
							</button>
						<% } else { %>
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
										<% int	l_count = 1;
										for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
											if (l_count == 1) { %>
												<li role="presentation" class="active">
													<a href="#tab_content<%=l_count%>" role="tab" id="<%= l_tabname %>" data-toggle="tab" aria-expanded="true" tabindex="<%=l_count%>>">
														<%= l_tabname %>
													</a>
												</li>
										<%  } else { %>
												<li role="presentation" class="">
													<a href="#tab_content<%=l_count%>" role="tab" id="<%= l_tabname %>" data-toggle="tab" aria-expanded="false" tabindex="<%=l_count%>>">
														<%= l_tabname %>
													</a>
												</li>
										<%  }
											l_count++;	
										%>
									<%  }
									%>
								</ul>
								<div id="myTabContent" class="tab-content">
								<% 
									int	l_tabdetails = 1;
									String	l_bgcolor	=	null;
									for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { %>
										<div role="tabpanel" id="tab_content<%=l_tabdetails%>" aria-labelledby="<%= l_tabname %>" 
										<% 	if (l_tabdetails == 1) {
										%>
												 class="tab-pane fade active in">
										<%	} else { %>
												 class="tab-pane fade">
													
										<%	}
										%>		
											<table class="table table-striped" id="detailstable">
												<thead>
													<tr>
														<th>Field</th>
														<th>Review Status</th>
														<th>Error Type</th>
														<th>Comments</th>
													</tr>
												</thead>
												<tbody>		
													<% 	for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) { %>
														<tr>
															<td>
																<% if (l_catcount == 0 || l_catcount == 1) { %>
																<% } else if (l_catcount == 2) {
																	if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																		<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																	<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																		<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																	<% } %>
																<% } else if (l_catcount == 3) { 
																	if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																		<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																	<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																		<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																	<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
																		<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
																	<% } %>
																<% } else { %>
																<% } %>
																<label>
																	<%= l_tabdata.getCaseFieldName() %>
																</label>
															</td>
															<td>
																<% 	int	l_reviewstatus	=	l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus();
																	if (l_reviewstatus == 1) { %>
																		<div data-toggle="buttons">
																			<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label1" class="btn btn-sm custom-btn-green" data-toggle-class="btn-primary" 
																				data-toggle="tooltip" data-placement="left" data-original-title="Correct">
																				<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>1"
																				 	value="1" checked onchange="changeNoErrorColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);" />
																					<i class="glyphicon glyphicon-ok"></i>
																			</label>
																			<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label3" class="btn btn-sm custom-btn-grey" data-toggle-class="btn-primary" 
																				data-toggle="tooltip" data-placement="right" data-original-title="Incorrect">
																				<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>3"
																					 value="3" onchange="changeIncorrectDataColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);">
																					<i class="glyphicon glyphicon-remove"></i>
																			</label>
																		</div>
																<% } else if (l_reviewstatus == 2) { %>
																		<label class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary">
																			<i class="glyphicon glyphicon-remove"></i>
																		</label>
																<% } else if (l_reviewstatus == 3) { %>
																		<div data-toggle="buttons">
																			<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label1" class="btn btn-sm custom-btn-grey" data-toggle-class="btn-primary" 
																				data-toggle="tooltip" data-placement="left" data-original-title="Correct">
																				<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>1" value="1"
																				 	onchange="changeNoErrorColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);" />
																					<i class="glyphicon glyphicon-ok"></i>
																			</label>
																			<label id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>label3" class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary" 
																				data-toggle="tooltip" data-placement="right" data-original-title="Incorrect">
																				<input type="radio" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>" id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>3"
																				 	value="3" checked onchange="changeIncorrectDataColor(this,<%= l_tabdata.getCaseFieldType().getCatCode() %>);">
																					<i class="glyphicon glyphicon-remove"></i>
																			</label>
																		</div>
																<% } %> 
															</td>
															<%-- <td>
		                              							<%	if (l_reviewfielddata.get(l_tabdata.fieldName).isReviewCorrected()) { %>
		                              									<label class="btn btn-sm custom-btn-green">
																			<i class="glyphicon glyphicon-thumbs-up"></i>
																		</label>
		                              							<%  } else { %>
		                              									<label class="btn btn-sm custom-btn-grey">
																			<i class="glyphicon glyphicon-thumbs-down"></i>
																		</label>
		                              							<%  } %>
															</td> --%>
															<td>
																<select name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" 
																	id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>errortype" class="form-control">
																	<option value="-1">No Error</option>
																	<% for (String l_errorcode : l_errortypes.keySet()) { %>
																		<option value="<%= l_errorcode %>" disabled><%= l_errortypes.get(l_errorcode) %></option>
																	<% } %>
																</select>
															</td>
															<td>
																<input type="text" name="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" maxlength="4000" 
																	id="__fldfieldmnemonic<%= l_tabdata.getCaseFieldMnemonic() %>comment" class="form-control" 
																	value="<%= l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldComment() %>" />
															</td>
														</tr>
													<%  
														} 
													%>
												</tbody>
											</table>
											<% 	if (l_tabdetails == 1) {
											%>
												<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
													Submit
												</button>
												<a class="btn btn-primary btn-sm btnNext pull-right">Next</a>
												<button type="button" class="btn btn-sm btn-primary" onclick="fncancel();" value="Cancel">
													Cancel
												</button>
											<%	} else if (l_tabdetails == l_qrreviewdata.getSafetyDbCaseFieldList().keySet().size()) { %>
													<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
														Submit
													</button>
													<a class="btn btn-primary btn-sm btnPrevious pull-right">Previous</a>
													<button type="button" class="btn btn-sm btn-primary" onclick="fncancel();" value="Cancel">
														Cancel
													</button>
											<%	} else { %>
													<button type="button" class="btn btn-primary btn-sm pull-right" value="Submit" onclick="formSubmit();">
														Submit
													</button>
													<a class="btn btn-primary btn-sm btnNext pull-right">Next</a> 
													<a class="btn btn-primary btn-sm btnPrevious pull-right">Previous</a>
													<button type="button" class="btn btn-sm btn-primary" onclick="fncancel();" value="Cancel">
														Cancel
													</button>
											<%	}
												l_tabdetails++;
											%>
										</div>
									<% 	}
									%>
								</div>
							</div> <%
						} %>	
					</div> 
				</div>
			</div>
		</csrf:form>	
	</body>
</html>
<% } catch (Throwable t) {
	} %>	