<%@page import="java.util.List"%>
<%@page import="com.sciformix.commons.ObjectIdPair"%>
<%
/* //Just be careful that the attribute name is unique to this file
if (request.getAttribute("uiFormCreationMethods.jsp") != null) {
	return;
}
request.setAttribute("uiFormCreationMethods.jsp", true);
 */
 %>

<%!
public String displayFormHeader(String sPageTitle)
{
    String sHtml = null;
    
    sHtml = "<div class=\"page-title\"><div class=\"title_left\"><h3>" + sPageTitle +"</h3></div></div>";
	
	return sHtml;
}
%>

<%!
public String displayFormTooltip(String sFormTooltip, boolean bDisplayCollapseOption, boolean bDisplayCloseOption)
{
    String sHtml = null;
    
    sHtml = "<div class=\"x_title\"><h2><small>" + sFormTooltip + "</small></h2><ul class=\"nav navbar-right panel_toolbox\">";
    
    if (bDisplayCollapseOption)
    {
    	sHtml += "<li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a></li>";
    }
    if (bDisplayCloseOption)
    {
    	sHtml += "<li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a></li>";
    }
    
    sHtml += "</ul><div class=\"clearfix\"></div></div>";
	
	return sHtml;
}
%>

<%!
public String displaySelectFileBox(String sControlId, String sLabel, boolean bRequired, String sCurrentOption, boolean bDisabled)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 for=\"" + sControlId + "\">");
	sbHtml.append(sLabel);
	if (bRequired) sbHtml.append("<span class=\"required\"> *</span>"); 
	sbHtml.append("</label>");
	//sbHtml.append("<div class=\"col-md-9 col-sm-9 col-xs-12\">");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<input type=\"file\" id=\"" + sControlId + "\" name=\"" + sControlId + "[]\"  id=\"" + sControlId + "\" ");
	if (sCurrentOption != null && sCurrentOption.length()>0) sbHtml.append(" value=\"").append(sCurrentOption).append("\"");
	//sbHtml.append(" class=\"form-control col-md-7 col-xs-12\">");
	if(bDisabled)sbHtml.append("disabled=\"disabled\"");
	sbHtml.append("  multiple=\"multiple\">");
	sbHtml.append("</div></div>");
	
	return sbHtml.toString();
}

%>


<%!
public String displayFormHiddenVariables(String sPortalAppId, String sPortalAction)
{
    String sHtml = null;

	sHtml = "<input type=\"hidden\" id=\"portal_appid\" name=\"portal_appid\" value=\"" + sPortalAppId + "\" >";
	sHtml += "<input type=\"hidden\" id=\"portal_action\" name=\"portal_action\" value=\"" + sPortalAction + "\" >";
	
	return sHtml;
}
%>

<%!
public String displayTextBox(String sControlId, String sLabel, boolean bRequired, String sCurrentOption)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 for=\"" + sControlId + "\">");
	sbHtml.append(sLabel);
	if (bRequired) sbHtml.append("<span class=\"required\"> *</span>");
	sbHtml.append("</label>");
	//sbHtml.append("<div class=\"col-md-9 col-sm-9 col-xs-12\">");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12 \">");
	sbHtml.append("<input type=\"text\" id=\"" + sControlId + "\" name=\"" + sControlId + "\"  id=\"" + sControlId + "\" ");
	/* if (bRequired) sbHtml.append("required=\"required\""); */
	if (sCurrentOption != null && sCurrentOption.length()>0) sbHtml.append(" value=\"").append(sCurrentOption).append("\"");
	//sbHtml.append(" class=\"form-control col-md-7 col-xs-12\">");
	sbHtml.append(" class=\"form-control\">");
	sbHtml.append("</div></div>");
	
	return sbHtml.toString();
}
%>

<%!
public String displayTextArea(String sControlId, String sLabel, boolean bRequired, int rows, int cols, String sCurrentOption, boolean bReadonly)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 for=\"" + sControlId + "\">");
	sbHtml.append(sLabel);
	if (bRequired) sbHtml.append("<span class=\"required\"> *</span>");
	sbHtml.append("</label>");
	sbHtml.append("<div class=\"col-md-8 col-sm-8 col-xs-12\">");
	sbHtml.append("<textarea id=\"" + sControlId + "\"  name=\"" + sControlId + "\" rows=\"").append(rows).append("\" cols=\"").append(cols).append("\" ");
	/* if (bRequired) sbHtml.append("required=\"required\""); */
	if (bReadonly) sbHtml.append("readonly ");
	sbHtml.append(" class=\"resizable_textarea form-control col-md-7 col-xs-12\">");
	if (sCurrentOption != null && sCurrentOption.length()>0) sbHtml.append(sCurrentOption);
	sbHtml.append("</textarea></div></div>");
	
	return sbHtml.toString();
}
%>

<%!
public String displayTextBox(String sControlId, String sLabel, boolean bRequired, String sCurrentOption, String sStyleForContent)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 for=\"" + sControlId + "\">");
	sbHtml.append(sLabel);
	if (bRequired) sbHtml.append("<span class=\"required\"> *</span>");
	sbHtml.append("</label>");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<input type=\"text\" id=\"" + sControlId + "\"  name=\"" + sControlId + "\" id=\"" + sControlId + "\" ");
	/* if (bRequired) sbHtml.append("required=\"required\""); */
	if (sCurrentOption != null && sCurrentOption.length()>0) sbHtml.append(" value=\"").append(sCurrentOption).append("\"");
	if (sStyleForContent != null && sStyleForContent.length()>0) sbHtml.append(" style=\"").append(sStyleForContent).append("\"");
	sbHtml.append(" class=\"form-control\">");
	sbHtml.append("</div></div>");
	
	return sbHtml.toString();
}
%>

<%!
public String displayTextBox(String sControlId, String sLabel, boolean bRequired, String sCurrentOption, String sStyleForContent, boolean bReadonly)
{
	StringBuilder sbHtml = null;
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 for=\"" + sControlId + "\">");
	sbHtml.append(sLabel);
	if (bRequired) sbHtml.append("<span class=\"required\"> *</span>");
	sbHtml.append("</label>");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<input type=\"text\" id=\"" + sControlId + "\"  name=\"" + sControlId + "\" id=\"" + sControlId + "\" ");
	/* if (bRequired) sbHtml.append("required=\"required\""); */
	if (bReadonly) sbHtml.append("readonly ");
	if (sCurrentOption != null && sCurrentOption.length()>0) sbHtml.append(" value=\"").append(sCurrentOption).append("\"");
	if (sStyleForContent != null && sStyleForContent.length()>0) sbHtml.append(" style=\"").append(sStyleForContent).append("\"");
	sbHtml.append(" class=\"form-control\">");
	sbHtml.append("</div></div>");
	
	return sbHtml.toString();
}
%>

<%!
public String displayLabelPair(String sLabel, boolean bRequired, String sCurrentValue, String sStyleForContent)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\" >");
	sbHtml.append("<b><label class=\"control-label col-md-3 col-sm-3 col-xs-12\">");
	sbHtml.append(sLabel);
	if (bRequired) sbHtml.append("<span class=\"required\"> *</span>");
	sbHtml.append("</label></b>");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<label ");
	if (sStyleForContent != null && sStyleForContent.length()>0) sbHtml.append(" style=\"").append(sStyleForContent).append("\"");
	sbHtml.append(" class=\"control-label\">");
	if (sCurrentValue != null && sCurrentValue.length()>0) sbHtml.append(sCurrentValue);
	sbHtml.append("</label>");
	sbHtml.append("</div></div>");
	
	return sbHtml.toString();
}
%>



<%!
public String displayTotalCharLabelPair(String sLabel, boolean bRequired, String sCurrentValue, String sStyleForContent)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div id='narrativeLength' ");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12\">");
	sbHtml.append(sLabel);
	if (bRequired) sbHtml.append("<span class=\"required\"> *</span>");
	sbHtml.append("</label>");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<label ");
	if (sStyleForContent != null && sStyleForContent.length()>0) sbHtml.append(" style=\"").append(sStyleForContent).append("\"");
	sbHtml.append(" class=\"control-label\">");
	if (sCurrentValue != null && sCurrentValue.length()>0) sbHtml.append(sCurrentValue);
	sbHtml.append("</label>");
	sbHtml.append("</div></div>");
	
	
	
	return sbHtml.toString();
}
%>

<%!
public String displayLabelPair(String sLabel, boolean bRequired, String sCurrentValue)
{
	return displayLabelPair(sLabel, bRequired, sCurrentValue, null);
}
%>

<%!
public String displaySelectBox(String sControlId, String sLabel, boolean bRequired, String[] arrOptions, String sCurrentOption)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 ");
	sbHtml.append("\">").append(sLabel);
	if (bRequired) sbHtml.append(" *");
	sbHtml.append("</label>");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<select class=\"form-control\" id=\"" + sControlId + "\" name=\"" + sControlId + "\">");
	if (arrOptions != null)
	{
		for (String sOption : arrOptions)
		{
			if (sOption!=null)
			{
				sbHtml.append("<option value=\"" + sOption + "\" ");
				if (sCurrentOption != null && sOption.equals(sCurrentOption))
				{
					sbHtml.append("SELECTED");
				}
				sbHtml.append(">").append(sOption).append("</option>");
			}		
		}
	}
	sbHtml.append("</select></div></div>");
	
	return sbHtml.toString();
}
%>

<%!
public String displaySelectBox(String sControlId, String sLabel, boolean bRequired, List<ObjectIdPair<String,String>> arrOptions, String sCurrentOption)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 ");
	sbHtml.append("\">").append(sLabel);
	if (bRequired) sbHtml.append(" *");
	sbHtml.append("</label>");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<select class=\"form-control\" id=\"" + sControlId + "\" name=\"" + sControlId + "\">");
	if (arrOptions != null)
	{
		for (ObjectIdPair<String,String> sOption : arrOptions)
		{
			if (sOption!=null)
			{
				sbHtml.append("<option value=\"" + sOption.getObjectSeqId() + "\" ");
				if (sCurrentOption != null && sOption.getObjectSeqId().equals(sCurrentOption))
				{
					sbHtml.append("SELECTED");
				}
				sbHtml.append(">").append(sOption.getObjectName()).append("</option>");
			}		
		}
	}
	sbHtml.append("</select></div></div>");
	
	return sbHtml.toString();
}
%>

<%!
public String displaySelectBoxMultiple(String sControlId, String sLabel, boolean bRequired, List<ObjectIdPair<String,String>> arrOptions, String sCurrentOption)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 ");
	sbHtml.append("\">").append(sLabel);
	if (bRequired) sbHtml.append(" *");
	sbHtml.append("</label>");
	sbHtml.append("<div class=\"col-md-4 col-sm-4 col-xs-12\">");
	sbHtml.append("<select class=\"form-control\" multiple id=\"" + sControlId + "\" name=\"" + sControlId + "\">");
	if (arrOptions != null)
	{
		for (ObjectIdPair<String,String> sOption : arrOptions)
		{
			if (sOption!=null)
			{
				sbHtml.append("<option value=\"" + sOption.getObjectSeqId() + "\" ");
				if (sCurrentOption != null)
				{
					for (String sSelectedValue : sCurrentOption.split("~")) {
						if (sSelectedValue.equals(sOption.getObjectSeqId())) {
							sbHtml.append("selected");
							break;
						}
					}
				}
				sbHtml.append(">").append(sOption.getObjectName()).append("</option>");
			}		
		}
	}
	sbHtml.append("</select></div></div>");
	sbHtml.append("<input type=\"hidden\" name=\""+ sControlId + "_multiple\" id=\"" + sControlId + "_multiple\" >");
	
	return sbHtml.toString();
}
%>

<%!
public String displaySwitch(String sControlId, String sLabel, boolean bRequired, boolean bInitialState)
{
	StringBuilder sbHtml = null;
	
	sbHtml = new StringBuilder();
	sbHtml.append("<div class=\"form-group\">");
	sbHtml.append("<label class=\"control-label col-md-3 col-sm-3 col-xs-12 ");
	//...required-for
	sbHtml.append("\">").append(sLabel);
	if (bRequired) sbHtml.append(" *");
	sbHtml.append("</label>");
	sbHtml.append("<label><input type=\"checkbox\" id=\"" + sControlId + "\" name=\"" + sControlId + "\" class=\"js-switch\" ");
	if (bInitialState) sbHtml.append(" checked=\"checked\" ");
	sbHtml.append(" data-switchery=\"true\" style=\"display: none;\">");
	sbHtml.append("</label></div>");
	
	return sbHtml.toString();
}
%>
