<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>

<script type="text/javascript">
	function ModifyProject(value)
	{
		var form=document.getElementById("projectManagement");
		document.getElementById("portal_action").value = value;
		
		if (!validateInput() && validateFormParameters())
		 {
			 form.submit();
		 }
	}
	function validateFormParameters () {
		clearErrorMessages();
		oFormObject = document.forms["projectManagement"];
		return validateInputParameter(oFormObject);
	}
	
	function validateInput()
	{
		clearErrorMessages();
		var bErrorDetected = false;
		var pattern = /[a-zA-Z0-9][a-zA-Z0-9\\s_-]{1,62}[a-zA-Z0-9]/;
		var ProjectName=document.getElementById("projName").value;
		var ProjectDescription=document.getElementById("projDescription").value;
		
		if(ProjectName==""||ProjectName.trim()==""){
			displayErrorMessage("UI-1018");
			bErrorDetected = true;
		} else if(!pattern.test(ProjectName))
		{
			displayErrorMessage("UI-1019");
			bErrorDetected = true;
		}else if(ProjectDescription==""||ProjectDescription.trim()=="")
		{
			displayErrorMessage("UI-1020");
			bErrorDetected = true;
		}
		return bErrorDetected;
	} 

	function fncancel()
	{
		var form=document.getElementById("projectManagement");
		document.getElementById("portal_action").value = "";
		form.submit();
		
	}
</script>

<% ProjectInfo p_oProjectInfo= (ProjectInfo)request.getAttribute("selectedProjectInfo");
   String invalidInputMsg = (String) request.getAttribute("invalidInput");
%>
<% if(invalidInputMsg !=null){%>
	<i class='glyphicon glyphicon-exclamation-sign'></i>&nbsp;<strong><%=invalidInputMsg %></strong>
<%} %>

<div id="mainContent">
	<div class="page-title">
		<div class="title_left">
			<%if(p_oProjectInfo==null){
			 %>
			<h3>Create Project</h3>
			<%}else if(sNewPortalAction.equals("viewProject")){ %>
			<h3>View Project</h3>
			<% }else{%>
			<h3>Modify Project</h3>
			<%} %>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="">
			<div class="x_panel">
				
				<% if(p_oProjectInfo!=null){%>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
						Project Id: </label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label class="control-label col-md-1 col-sm-1 col-xs-12"><%=p_oProjectInfo.getProjectSequenceId()%></label>
					</div>
					<input type="hidden" name="projectId" id="projectId"
					value="<%=p_oProjectInfo.getProjectSequenceId()%>">
				</div>
				<%} %>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
						Project Name: 
					<%if(sNewPortalAction.equals("viewProject")) 
	                  		{%>
					</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" name="projName" class="form-control"
							id="projName" value="<%=p_oProjectInfo.getProjectName()%>"
							readonly="readonly">
						<%}else if(sNewPortalAction.equals("editProject")){ %>
						<span>*</span> </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" name="projName" class="form-control"
								value="<%=p_oProjectInfo.getProjectName()%>" id="projName" maxlength="64">
							<%}else{ %>
							<span>*</span> </label>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text" name="projName" class="form-control"
									id="projName" maxlength="64">
								<%} %>
							</div>
						</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
							Project Description: 
						<%if(sNewPortalAction.equals("viewProject")) 
	                  	{%>
						</label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" name="projDescription" class="form-control"
									id="projDescription"
									value="<%=p_oProjectInfo.getProjectDescription()%>"
									readonly="readonly">
						<%}else if(sNewPortalAction.equals("editProject")){ %>
							<span class="required"> *</span> 
						</label>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text" name="projDescription" class="form-control"
									id="projDescription"
									value="<%=p_oProjectInfo.getProjectDescription()%>">
									<%}else{ %>
								<span class="required"> *</span> 
						</label>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text" name="projDescription" class="form-control"
									id="projDescription">
						<%} %>
							</div>
					</div>

					<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="button" class="btn btn-primary"
								onclick="fncancel();">Cancel</button>
							<%if(p_oProjectInfo==null) 
	                  		{%>
								<button type="button" id="submitEditForm"
									class="btn btn-success"
									onclick="ModifyProject('createProject')">Submit</button>
							<%}else if(sNewPortalAction.equals("editProject")) {%>
								<button type="button" id="submitEditForm"
									class="btn btn-success"
									onclick="ModifyProject('editProject')">Submit</button>
							<%} %>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>