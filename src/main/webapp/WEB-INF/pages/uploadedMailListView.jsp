<%@page import="java.util.ArrayList"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.mb.MBPortalApp"%>
<%@page import="com.sciformix.sciportal.mb.MBMailInfo"%>
<%@page import="com.sciformix.sciportal.mb.MailBox"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mail Box List</title>

<script type="text/javascript">
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
 });



	function back(action)
	{
		var form=document.getElementById("frmain");
		document.getElementById("portal_action").value = action;
		form.submit();
	}

</script>
</head>

<%
Map<String, List<MBMailInfo>> mailListMap = null;
List<MBMailInfo> mailList =	null,
		duplicateMailList = null;
MailBox mailBox = null;
mailListMap = (Map<String, List<MBMailInfo>>)request.getAttribute("mailListMap");
mailList = mailListMap.get(MBPortalApp.SAVED_MAIL_KEY);
duplicateMailList = mailListMap.get(MBPortalApp.DUPLICATE_MAIL_KEY);
mailBox = (MailBox)request.getAttribute("mailBox");
 %>
<body>
	<csrf:form name="frmain" id="frmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="MB-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
		<div class="page-title">
			<div class="title_left">
				<h3>
				 	Mail List 
				</h3>
			</div>
		</div>
	
	<div class="x_panel">
		<div class="x_title">
			<h2 class = "col-md-2 col-sm-2 col-xs-12" >
				Uploaded Mail List
			</h2>
			<%
			if(duplicateMailList !=null && !duplicateMailList.isEmpty()){%>
				<div class ="col-md-10 col-sm-10 col-xs-12" >
					<h5 style="color: red">
						Email already exist in system for message subject: <%=MBPortalApp.joinMailIdString(duplicateMailList)%>.
					</h5>
				</div>	
			<%} %>
			<div class="clearfix"></div> 
		</div> 
		<div class="x_content">
			<%if(mailList !=null && !mailList.isEmpty()){%>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Workflow</th>
							<th>Date</th>
							<th>Mail Id</th>
							<th>Email</th>
							<th>Sender</th>
							<!-- <th>To</th>
							<th>Cc</th> -->
							<th>Subject</th>
							<th>Recipient</th>
						</tr>
					</thead>
					<tbody>
				<%
					for(MBMailInfo mailInfo : mailList ){%>
					<tr>
						<td>
						
						</td>
						<td>
							<%=WebUtils.formatForDisplay(mailInfo.getMailTimeStamp())%><br/>
							<small><%=WebUtils.formatForDisplayDuration(mailInfo.getMailTimeStamp())%></small>
						</td>
						<td>
							<%=mailInfo.getMailAppSeqId() %>
						</td>
						<td>
							<%if(StringUtils.emptyString(mailInfo.getMailSender()).toLowerCase().
									contains
							(StringUtils.emptyString(mailBox.getEmailAddress()).toLowerCase())){%>
								<!-- <img src="images/SenderIcon.png"/>	 -->
							<%}
							if(StringUtils.emptyString(mailInfo.getMailRecipientsTo()).toLowerCase().
									contains
							   (StringUtils.emptyString(mailBox.getEmailAddress()).toLowerCase())){%>	
								<!-- <span class="glyphicon" aria-hidden="true">
									<img src="images/ToIcon.png"/>
								</span> -->
							<%}
							if(StringUtils.emptyString(mailInfo.getMailRecipientsCc() ).toLowerCase().
									contains
								(StringUtils.emptyString(mailBox.getEmailAddress()).toLowerCase())){%>
								<!-- <span class="glyphicon" aria-hidden="true">
									<img src="images/CcIcon.png"/>
								</span> -->
									
							<%}if(mailInfo.getMailAttachmentCount()>0){%>
								<span class="glyphicon glyphicon-paperclip" data-toggle="tooltip" 
								data-placement="left" title="<%=mailInfo.getMailAttachmentCount() %>" aria-hidden="true"></span>
							<%}%>					
						</td>
						<td><%=mailInfo.getMailSender() %></td>
						<%-- <td class="col-md-2 col-sm-2 col-xs-2"><%=mailInfo.getMailRecipientsTo()%></td>
						<td class="col-md-2 col-sm-2 col-xs-2"><%=StringUtils.emptyString(mailInfo.getMailRecipientsCc())%></td>
						 --%>
						<td><%=mailInfo.getMailSubject() %></td>
						<%-- <td><%=mailInfo.getMailAttachmentCount() %></td> --%>
						<td>
						To: <%=mailInfo.getMailRecipientsTo() %>
						<%if(!StringUtils.isNullOrEmpty(mailInfo.getMailRecipientsCc())){%>
							<Br />
							<Br />
						Cc:	<%=mailInfo.getMailRecipientsCc() %>
						<%} %>
						</td>
					</tr>
				<%}%>
				</tbody>
				</table>
				<%}else{%>
						 No Data Found
				<%}%>
			</div>
		</div>
		<div class="form-group" align="center">
			<input type = "button" class="btn btn-primary" value ="Back" onclick ="back('uploadmsgfileview');">
		</div>
	</csrf:form>
</body>
</html>