<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.ngv.CaseDataValidationRuleset"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
	<head>
<script type="text/javascript">
function submitForm(value)
{
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
	document.getElementById("clickValue").value =value;
	document.getElementById("fldprojectid").value="-1";
	if (validateFormParameters())
	 {
		form.submit();
	}
}
function validateFormParameters () {
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}
function viewTemplateRuleset(nRulesetId)
{
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
	document.getElementById("clickValue").value = "ngv_validate_viewtpl";
	document.getElementById("ngv_tplrulesetid").value = nRulesetId;
	
	if (validateFormParameters())
	 {
		form.submit();
	}
}

</script>
<%
List<CaseDataValidationRuleset> listRulesets = null;

listRulesets = (List<CaseDataValidationRuleset>) request.getAttribute("ngv_listRulesets");

%>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="SCFT">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="-1">
			
	<div class="center_col" role="main">
    	<div class="">
      <%=displayFormHeader("Case Validation Rulesets") %>
            
            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				 <%=displayFormTooltip("View/create/modify Case Validation Rulesets", false, false) %> 
                  
                  <div class="x_content">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Case Validation Ruleset Name</th>
                          <th>State</th>
                        </tr>
                      </thead>
                      <tbody>
						<% if (listRulesets != null && listRulesets.size() > 0)
						{
							for (CaseDataValidationRuleset oCaseValidationRuleset : listRulesets)
							{
								%>
								<tr>
		                          <th><A HREF="javascript:viewTemplateRuleset('<%=oCaseValidationRuleset.getSequenceId()%>');"><i class="fa fa-pencil"></i></A></th>
		                          <td><%=oCaseValidationRuleset.getTemplateName()%></td>
		                          <td><%=oCaseValidationRuleset.getState().displayName()%></td>
		                        </tr>
								<%
							}//end-for NgvTemplateRuleset
						}//end-if listRulesets
						else
						{
							%><TR><TD colspan="3">&nbsp;<i>No template rulesets</i></TD><%
						}//end-else listRulesets
						%>
                      </tbody>
                    </table>
                    <div class="ln_solid"></div>
                  	<button type="button" class="btn btn-primary" onclick="javascript:submitForm('ngv_validate_createnew');">Create New</button>
					<input type="hidden" id="ngv_tplrulesetid" name="ngv_tplrulesetid" value="" />
                  </div> <!-- end-div x_content -->
                  
                </div><!-- end-div panel -->
              </div><!-- end-div col-md-12 col-sm-12 col-xs-12 -->
            </div><!-- end-div row -->
            
		</div><!-- end-div "" -->
	</div><!-- end-div center_col -->
	</csrf:form>
	</body>
	</html>
	


