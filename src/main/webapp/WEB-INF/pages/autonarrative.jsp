<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>
<%@taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvPortalApp.AppConfig"%>
<%@page import="org.apache.wink.json4j.JSONArray"%>
<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.LinkedHashMap" %>
<%@page import="java.io.*,java.util.*"%>
<%@page import="com.sciformix.commons.SciError"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvPortalApp"%>
<%@page import="org.apache.wink.json4j.JSONArray"%>
 <%@page import="com.sciformix.commons.utils.*"%>
 <%@page import="java.util.regex.Matcher" %>
 <%@page import="java.util.regex.Pattern" %>
<!DOCTYPE html >
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">


</style>
<script>
	<%@include file="../js/error.js" %>
</script>
</head>
<body onload="calculateLength()">

<%@include file="uiFormCreationMethods.jsp"%>
<DIV>
<h2>Narrative Generator Tool</h2>

<%
List<String> listFileNames = null;
String sFileNames = "";
String sNarrative = null;
String sUpdateMarkerFlag = null;
String sUpdateMarkerExcludeChars = null;
String sTemplateRulesetName = null;
String sTemplateName = null;
List<String> markerList = null;
Map<String, String> mapMissingMarker = null;
String sNewPortalAppId = (String) request.getAttribute("new-portalappid");
String sNewPortalAction = (String) request.getAttribute("new-portalaction");
String[] sarrApplicableSafetyDatabases = new String[] {"RSS"};//(String[]) request.getAttribute("ApplicableSafetyDatabases");
IPortalApp oPortalApp = null;
NgvTemplateRuleset oTemplateRuleset = null;
NgvPortalApp oMyPortalApp = null;
UserSession oUserSession = null;
String sPersmissibleFileExtensions = null;
oPortalApp = AppRegistry.getApp(sNewPortalAppId);
oMyPortalApp = (NgvPortalApp) oPortalApp;
List<String> listofCases = null;
List<String> warningList = null;

oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
oTemplateRuleset = oMyPortalApp!=null?oMyPortalApp.getTemplateRuleset(oUserSession.getUserInfo()):null;
if(oTemplateRuleset!=null && oTemplateRuleset.getState().displayName().equals("Under Configuration"))
{	
	sTemplateRulesetName = oTemplateRuleset!=null && oTemplateRuleset.getRulesetName()!=null?oTemplateRuleset.getRulesetName()+" ("+oTemplateRuleset.getState().displayName()+")":"";
}else
{
	sTemplateRulesetName = oTemplateRuleset!=null && oTemplateRuleset.getRulesetName()!=null?oTemplateRuleset.getRulesetName():"";
}	

sUpdateMarkerFlag = (oMyPortalApp!=null ? oMyPortalApp.getAppConfigSetting(NgvPortalApp.AppConfig.ENABLE_UPDATE_MARKER) : "");
sPersmissibleFileExtensions = (oMyPortalApp!=null ? oMyPortalApp.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_EXTENSIONS) : "");
sUpdateMarkerExcludeChars = (oMyPortalApp!=null ? oMyPortalApp.getAppConfigSetting(NgvPortalApp.AppConfig.REGEX_FOR_UPDATE_MARKES) : "");

%>
<!-- enctype="multipart/form-data" -->
<csrf:form name="NGV_form" id="NGV_form" action="LandingServlet" method="post"  enctype="multipart/form-data" data-parsley-validate="" >
<input type="hidden" id="portal_appid" name="portal_appid" value="NGV-PORTAL-APP" >
<input type="hidden" id="portal_action" name="portal_action" value="">
<input type="hidden" id="missing_Marker" name="missing_Marker" value="">

<input type="hidden" id="r_caseId" name="r_caseId" value="">
<input type="hidden" id="r_dbName" name="r_dbName" value="">

<button class="btn btn-default"  id="addMarker" value="false" style="display: none">Add</button>
<%	
List<SciError> errorMsgsList = new ArrayList<SciError>();//(ArrayList)request.getAttribute(WebConstants.RESPONSE_ERROR_LIST)
List<String> errorNoList = new ArrayList();
if(errorMsgsList != null)
{
for(int i=0; i< errorMsgsList.size(); i++)
{
	errorNoList.add(errorMsgsList.get(i).errorMessage());
}
}
%>
<%	listofCases =  (List<String>)request.getAttribute("WarnSatisfiedCase");
   
%>

		<% if(listofCases!=null)
		{
			%>
			<div id="_errordiv_svr" class="alert alert-danger alert-dismissible fade in" role="alert">
			<%
		
		%>
		<%-- <tr><td style="font-weight: bold;width:500px; "><%=key %></td><td><input type="text" name='<%=key%>' id='<%=key%>' value = '<%=mapMissingMarker.get(key)%>' ></td></tr> --%>
		<tr>
			<td>
				
				<i class="glyphicon glyphicon-exclamation-sign"></i>&nbsp;<strong>One or more case validation rules : &nbsp;<%=listofCases%> have failed ; Either correct the errors in Safety database and regenerate the narrative OR review the generated narrative completely before use.</strong> </div>
			</td>
		</tr>
		
					
</div>
	<% } %>
		
		
		

<%if(request.getAttribute("s_narrative") == null)
{ %>
<div id="screen_for_step1">
	
	<%
	
		//System.out.println(errorNoList+ "\n");
		if (errorMsgsList != null && errorMsgsList.size()>0 )
		{
			for(int i=0; i< errorMsgsList.size(); i++)
			{
				%>
				<h4><%=errorMsgsList.get(i).errorMessage() %>:<%=errorMsgsList.get(i).errorMessage()%></h4>
				<%
			}
		}
		else
		{ %>
	
	
	
	<%if((oMyPortalApp!=null && !oMyPortalApp.allowsUserApplicationDisable()) || oMyPortalApp == null || oTemplateRuleset == null)
	{
		
	}else{%>
	<div class="center_col" role="main">
    	<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 <div class="x_content">
                
                     
            
                  <%=displayTextBox("caseID", "Case Id", true, "")%>
             	 <div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div>  
                   <div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div> 
                    <div class="form-group">
                        <div class="radio col-md-4 col-sm-4 col-xs-12"></div>
                	</div>
                  <%=displaySelectFileBox("ngv_casedata_file", "Case File", true, "",false)%>
              	<div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div>
                    
                    <div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div> 
                    <div class="form-group">
                        <div class="radio col-md-4 col-sm-4 col-xs-12"></div>
                	</div>
                 
                  <%=displayLabelPair("Ruleset", false, sTemplateRulesetName!=null?sTemplateRulesetName:"")%>
                    
                    	<div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div>   
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  	  <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
					 <button type="primary" class="btn btn-primary"
									onclick="return SendServerToStep2();">Next</button>
					</div>	
	               </div>
       			</div>
      		 </div>
      	   </div>
        </div>
      </div>
                
	</div>
</div>
<%}} }else{
	
	markerList = (ArrayList<String>)request.getAttribute("r_missingMarkerList");
/* 
	if (errorMsgsList != null && errorMsgsList.size()>0 )
	{
		for(int i=0; i< errorMsgsList.size(); i++)
		{
			String regEx = "\\{(.*?)\\}";
 			Pattern pattern = Pattern.compile(regEx);
 			Matcher matcher = pattern.matcher(errorMsgsList.get(i).errorMessage());
 			while(matcher.find()){
 				markerList.add(matcher.group().toString().substring(1, matcher.group().toString().length()-1).trim());
 			}
			
		}
	} */
%>

<div id="screen_for_step2">
<div class="center_col" role="main">
    	<div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 <div class="x_content">
   
                <%--   <%=displaySelectBox("safety_db_name", "Source Database", true, sarrApplicableSafetyDatabases, "")%> --%>
                <%--   <%=displayTextBox("safety_db_name", "Source Database", false,  (String)request.getAttribute("r_dbName"),null,true)%>
                   <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                   </div> --%>
                  <%=displayTextBox("caseID", "Case Id", false, (String)request.getAttribute("r_caseId"),null,true)%>
                <div class="form-group">
                        <div class="radio col-md-4 col-sm-4 col-xs-12"></div>
                </div>
                <div class="form-group">
                        <div class="radio col-md-4 col-sm-4 col-xs-12"> </div>
                </div>
              
                   <%
                    
                   listFileNames = (ArrayList)request.getSession().getAttribute("r_fileNameList");
                   if(listFileNames!=null)
                   for(int nFileNameIndex = 0;nFileNameIndex<listFileNames.size();nFileNameIndex++)
                   {
                	   sFileNames = sFileNames + listFileNames.get(nFileNameIndex)+",";
                   }
                   sFileNames = sFileNames.substring(0, sFileNames.length()-1);
                   request.setAttribute("r_fileNameList", listFileNames);
                   %>   
                    <%=displayLabelPair("File", false, sFileNames)%>
                  <div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div> 
               
                 <%=displayLabelPair("Ruleset", false, sTemplateRulesetName!=null?sTemplateRulesetName:"")%>
                 <div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div> 
               <%sTemplateName = (String)request.getAttribute("s_templateName"); %>
                 <%=displayLabelPair("Template", false, sTemplateName!=null?sTemplateName:"")%>
                 <div class="form-group">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
                         </div>
                        
                   </div> 
                 <div class="form-group">
	       			 <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> 
					<%=displayTotalCharLabelPair("Total Characters", false, "", "")%>
                 </div>         
	<table width="100%" id="main_table" >
	<tr>
	<td valign="top">
	<%-- <table id="left_table" width="75%">
	<tr>
		<td style="font-weight: bold; width:210px; ">Source Database:</td><td > <%=request.getAttribute("r_dbName")%> </td>	
	</tr>
	<tr>	
		<td style="font-weight: bold;width:210px; ">Case Id:</td><td> <%=request.getAttribute("r_caseId")%></td>
	</tr>	
	</table> --%>
		
	<%
	sNarrative = request.getAttribute("s_narrative").toString();
	mapMissingMarker =  (LinkedHashMap<String, String>)request.getSession().getAttribute("s_missingMarkers");
	if(sUpdateMarkerFlag!=null && sUpdateMarkerFlag.equals("true"))
	{ %>
	<table id="marker_table"  width="75%" >
	
		<%
	
		if(markerList!=null)
		for(int i=0;i<markerList.size();i++)
		{
			
			sNarrative = sNarrative.replaceAll(markerList.get(i), "<span style=\"color:red;font-weight:bold\">"+markerList.get(i)+"</span>");		
		%>
		<tr ><td style="font-weight: bold;width:500px; ">
			
						 <label class="control-label col-md-3 col-sm-3 col-xs-12"><%=markerList.get(i)%></label>
			</td>
			<td>			  
						 <input type="text" name='<%=markerList.get(i)%>' id='<%=markerList.get(i)%>' class="form-control">
						  <%-- <label class="control-label col-md-3 col-sm-3 col-xs-12">Case Id: <%=request.getAttribute("r_caseId")%></label> --%> 
                       
             </td>            
     		
		<%-- 	<%=markerList.get(i) %></td><td><input type="text" name='<%=markerList.get(i)%>' id='<%=markerList.get(i)%>'> --%>
		</tr>
		
		<%}
			request.setAttribute("makerList", markerList);
		%>
		
		<%
		
		if(mapMissingMarker!=null)
		for(String key:mapMissingMarker.keySet())
		{
		%>
		<%-- <tr><td style="font-weight: bold;width:500px; "><%=key %></td><td><input type="text" name='<%=key%>' id='<%=key%>' value = '<%=mapMissingMarker.get(key)%>' ></td></tr> --%>
		<tr><td style="font-weight: bold;width:500px; ">
				 <label class="control-label col-md-3 col-sm-3 col-xs-12"><%=key%></label>
			</td>
		
			<td>
				<input type="text" name='<%=key%>' id='<%=key%>' value = '<%=mapMissingMarker.get(key)%>' class="form-control">
			</td>
		</tr>
		<%}
		%>
		<tr>
		<td style="font-weight: bold;"></td>
		
		
		<% if( markerList != null && markerList.size()>0)
		{ %> <td>
			<button class="btn btn-primary"  id="addMarker" value="true" onclick="javascript:return updateMissingMarkers()">Update Markers</button>
			</td>
		<% } %>
		
		
		
		
		<% if( mapMissingMarker != null && mapMissingMarker.size()>0)
		{ %> <td>
			<button class="btn btn-primary"  id="addMarker" value="true" onclick="javascript:return updateMissingMarkers()">Update Markers</button>
			</td>
		<% } %>
		
		
		
			</tr>
		
	
	</table>
	<%}else{ 
		if(markerList!=null && markerList.size()>0)
		{
	%>
		<table id="marker_table"  width="75%" >
		<tr ><td style="font-weight: bold;width:500px; ">
				 <label >Missing Markers</label>
			 </td>
		</tr>
	<%
		
			for(int i=0;i<markerList.size();i++)
			{
				
				sNarrative = sNarrative.replaceAll(markerList.get(i), "<span style=\"color:red;font-weight:bold\">"+markerList.get(i)+"</span>");		
		%>
	
			<tr ><td style="font-weight: bold;width:500px; ">
			
					 <label class="control-label col-md-3 col-sm-3 col-xs-12"><%=markerList.get(i)%></label>	
			</td>
			<td>			  
						
						  <%-- <label class="control-label col-md-3 col-sm-3 col-xs-12">Case Id: <%=request.getAttribute("r_caseId")%></label> --%> 
                       
             </td>            
     		
		<%-- 	<%=markerList.get(i) %></td><td><input type="text" name='<%=markerList.get(i)%>' id='<%=markerList.get(i)%>'> --%>
		</tr>
	<%	}
		}	
	 %>	
		</table>
	<%} %>
	</td>
	<td>	
	<table id="right_table" width="80%" align="top" >
	<tr align="center"><td><div id="narrativeLength"></div></td></tr>
	<tr>
	<%
		sNarrative = sNarrative.replaceAll("\r\n", "<BR/>");
	 %>
		<td>
		<%if(!(mapMissingMarker == null && markerList.size()==0) )
		{
		%>
	        <div  id="narratvieData"  onkeydown="calculateLength()" data-parsley-minlength="20" data-parsley-maxlength="100" spellcheck="true" style="border: 1px solid gray;background-color: white;box-shadow: 2px 2px 2px 0 lightgray inset;margin-left: 50px;margin-right:70px;width:700px;" contenteditable="true">
	              <%=sNarrative%>
			</div>
		<%}
		else
		{%>	
			    <div  id="narratvieData" onkeydown="calculateLength()" data-parsley-minlength="20" data-parsley-maxlength="100" spellcheck="true" style="border: 1px solid gray;background-color: white;box-shadow: 2px 2px 2px 0 lightgray inset;margin-left: 265px;margin-right:70px;width:700px;" contenteditable="true">
	              <%=sNarrative%>
				</div>
		
		<%} %>	
		</td>
	</tr>
	<tr align="center">
		<td>
			
	<%
	String sMissingMarkerCompletelyFilled = (String)request.getAttribute("r_missingMarkerFlag");
	if((sMissingMarkerCompletelyFilled!=null && sMissingMarkerCompletelyFilled.equals("true")) || (mapMissingMarker==null && markerList!=null && markerList.size()==0)){ %>
		<button class="btn btn-success" onclick="copyToClipboardDiv('narratvieData')" >Copy To Clipboard</button>
	<%}else {%>
	   	<button class="btn btn-big" onclick="copyToClipboardDiv('narratvieData')" >Copy To Clipboard</button>
	   	<%} %>
	  
		</td>
	</tr>	
	</table>
	</td>
</tr>	
	</table>	
	
	
	</div>		
				
	
	
		 <% request.getSession().setAttribute("r_caseId", request.getAttribute("r_caseId"));
		 request.getSession().setAttribute("r_dbName", request.getAttribute("r_dbName"));
		%>
		

</div>
</div>
</div>
</div>
</div>
</div>

<%} %>
	
</csrf:form>

</DIV>

	<script type="text/javascript">
	function validateFormParameters () {
		clearErrorMessages();
		oFormObject = document.forms["NGV_form"];
		return validateInputParameter(oFormObject);
	}
	
	function validateInput()
	{
		
		clearErrorMessages();
	
		var bErrorDetected = false;
		var pattern = /^[A-Za-z0-9]+$/;
		var caseIdPattern = /^[A-Za-z0-9]+$/;
		var caseID=document.getElementById("caseID").value;
		var fileName=document.getElementById("ngv_casedata_file");
	
		
		if(caseID=="")
		{
			displayErrorMessage("UI-1003");
			bErrorDetected = true;
		}
		else if(!caseIdPattern.test(caseID))
		{
			displayErrorMessage("UI-1010");
			bErrorDetected = true;
		}
		
		if(fileName==""){
			displayErrorMessage("UI-1004");
			bErrorDetected = true;
		}
		/* else if(fileName.match(pattern))
		{
			displayErrorMessage("UI-1011");
			return false;
		} */
		else
		{
			
			var allowedFileExtensions = '<%=sPersmissibleFileExtensions%>';
			var name = null;
			var acutualFileNameWithExt = null;
			var fileExtension = null;
			var acutualFileNameWithoutExt = null;
			var fileNameStartsWith = null;
			
			for (var i = 0; i < fileName.files.length; ++i) 
			{
				 name = fileName.files.item(i).name;
				 acutualFileNameWithExt = name.split("\\").pop();
				 fileExtension= acutualFileNameWithExt.split(".").pop();
				 acutualFileNameWithoutExt = acutualFileNameWithExt.split(".")[0];
				 fileNameStartsWith = acutualFileNameWithoutExt.substring(0, caseID.length);
				 
				if(!pattern.test(acutualFileNameWithoutExt))
				{
					displayErrorMessage("UI-1011");
					//bErrorDetected = true;
					return false;
				}
				else if(allowedFileExtensions.indexOf(fileExtension) == -1)
				{
					displayErrorMessage("UI-1012");
					// bErrorDetected = true;
					return false;
				}
				else if(acutualFileNameWithoutExt.search(caseID) < 0)
				{
					displayErrorMessage("UI-1013");
					//bErrorDetected = true;
					return false;
				}
			}
		}
		
		return (!bErrorDetected);
	}
</script>
	
	<script>
	
	
	function calculateLength()
	{
		
		var narrativeText = $("#narratvieData").text();
		
		if(narrativeText!=null && narrativeText != "")
		{
			document.getElementById("narrativeLength").innerHTML = $("#narratvieData").text().length+" characters";
			document.getElementById("narrativeLength").style.fontWeight = "700";
		}
	} 
	
	function updateMissingMarkers()
	{
		clearErrorMessages();
		var form = document.getElementById("NGV_form");
		var missingMarkerKeyValue = "";
		var tempvalue = null;
		var pattern = '<%=sUpdateMarkerExcludeChars%>';
		
			<%if(markerList!=null){
				
			//	System.out.println("In update missign marker function==> "+request.getAttribute("makerList"));
			
				for(int i=0;i<markerList.size();i++)
				{%>
					tempvalue = document.getElementById('<%=markerList.get(i)%>').value ; 
					if(tempvalue.match(pattern)!=null)
					{
						displayErrorMessage("UI-1016");
						return false;	
					}
						missingMarkerKeyValue = missingMarkerKeyValue + '<%=markerList.get(i)%>' + "="+document.getElementById('<%=markerList.get(i)%>').value + ","
				<%}
			}
			if(mapMissingMarker!=null)
			{
				for(String key:mapMissingMarker.keySet())
				{%>
					tempvalue = document.getElementById('<%=key%>').value ; 
					if(tempvalue.match(pattern)!=null)
					{
						displayErrorMessage("UI-1016");
						return false;	
					}
					missingMarkerKeyValue = missingMarkerKeyValue + '<%=key%>' + "="+document.getElementById('<%=key%>').value + ","
				
				<%
				}
			}	
				%>
		document.getElementById('missing_Marker').value = missingMarkerKeyValue;
		document.getElementById('portal_action').value ="ngv_step3" ;
		if(validateFormParameters())
		{	
			form.submit();
		}
	}
	
	
	function copyToClipboardDiv(containerid) 
	{
		if (document.selection) { 
		    var range = document.body.createTextRange();
		    range.moveToElementText(document.getElementById(containerid));
		    range.select().createTextRange();
		    document.execCommand("Copy"); 

		} else if (window.getSelection) {
		
		     var range = document.createRange();
		     range.selectNodeContents(document.getElementById(containerid));
		     window.getSelection().removeAllRanges();
		     window.getSelection().addRange(range);
		     document.execCommand("Copy");
		}
		
	}
		
		
		function SendServerToStep2() {
			 var temp = validateInput();
			 if (temp == true)
			 {
				var form = document.getElementById("NGV_form");
				document.getElementById("portal_action").value = "ngv_step2";
				document.getElementById("portal_appid").value = "NGV-PORTAL-APP";
				if(validateFormParameters())
				{
					form.submit();
				}
			 }
			else
			{
			 return false;
			}
		}
		function SendServerToStep3() {
			var form = document.getElementById("NGV_form");
			document.getElementById("portal_action").value = "ngv_step3";
			document.getElementById("portal_appid").value = "NGV-PORTAL-APP";
			if(validateFormParameters())
			{
				form.submit();
			}
		}
	</script>
		<script>
			function InitForStep1()
			{
				//document.getElementById("screen_for_step1").style.display = "block";
				//document.getElementById("screen_for_step2").style.display = "none";
				//document.getElementById("screen_for_step3").style.display = "none";
				
			}
			function SwitchToStep2()
			{
			// alert("hii");
			/* 	document.getElementById("screen_for_step1").style.display = "none";
				document.getElementById("screen_for_step2").style.display = "block";
				document.getElementById("screen_for_step3").style.display = "none"; */
			}
			function SwitchToStep3()
			{
				document.getElementById("screen_for_step1").style.display = "none";
				document.getElementById("screen_for_step2").style.display = "none";
				document.getElementById("screen_for_step3").style.display = "block";
			}
		</script>
    <script type="text/javascript">
    	<%	
    	if (sNewPortalAction != null && sNewPortalAction.equals("ngv_step2"))
    	{
			%>SwitchToStep2();<%
    	}
    	else
    	{
    		/* System.out.println("\n\nSwitching to step one\n\n"+markerList);
    		if(markerList==null || markerList.size()==0){
    			
    		} */
    		%>InitForStep1();<%
    	}
    	%>
	</script>
	
	
	<script type="text/javascript">
	
	</script>
	</body>
	</html>