<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums.EmailSendSetting"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@include file="uiFormCreationMethods.jsp"%>

<!DOCTYPE html>
<html>
	<head>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
			function fncreatenewqrt () {
				clearErrorMessages();
				if (document.frmmain.fldtemplatename.value == "") {
					displayErrorMessage("UI-1005");
					document.frmmain.fldtemplatename.focus();
					return false;
				}
				document.frmmain.portal_action.value	=	"createnewqrt";
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fncancel () {
				document.frmmain.submit();
			}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="QR-PORTAL-APP">
			<div role="main">
				<div>
					<div class="page_title">
						<div class="title_left">
							<h3>Quality Review Template</h3>
						</div>
					</div>
					<div class="clearfix">
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										Create Template
									</h2>
									<div class="clearfix">
									</div>
								</div>
								<div class="x_content">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldtemplatename">
											Template Name
											<span class="required"> *</span>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<input class="form-control" type="text" id="fldtemplatename" name="fldtemplatename" value="" pattern="[a-zA-Z0-9][a-zA-Z0-9\\s_-]+[a-zA-Z0-9]" maxlength="255">
											<!-- Pattern need to be fetched from application settings -->
										</div>
									</div>	
									<div class="ln_solid"></div>
									<div class="form-group">
				                    	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
											<button type="button" class="btn btn-primary" onclick="fncancel();">
												Cancel
											</button>
				                        	<button type="button" class="btn btn-success" value="Submit" onclick="fncreatenewqrt();">
				                        		Submit
				                        	</button>
				                        </div>
				                	</div>
								</div>
							</div>
						</div>		
					</div>
					<div class="clearfix">
					</div>
				</div>
			</div>		
		</csrf:form>
	</body>
</html>