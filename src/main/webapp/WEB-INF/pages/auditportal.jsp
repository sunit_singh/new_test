<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHome"%>
<%@page import="com.sciformix.sciportal.project.ProjectHome"%>
<%@page import="com.sciformix.commons.WebErrorCodes.ProjectManagement"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDetailsDb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.io.*,java.util.*"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDb"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
final String DUMMY_USER_RECORD_FOR_SELECTION = "Select a user";
// final String DUMMY_PROJECT_RECORD_FOR_SELECTION = "(Project neutral)";
final int MAX_DISPLAY_LENGTH = 16;

List<AuditInfoDb> listAuditTrail = (List<AuditInfoDb>) request.getAttribute("s_audit");
pageContext.setAttribute("list",listAuditTrail);
List<String> projList =new ArrayList<String>();

String sOldValueToDisplay = null;
String sNewValueToDisplay = null;
AuditInfoDetailsDb oAuditInfoDetailsDb = null;
String[] sarrOptionValuesUserFilter = null;
String[] sarrOptionValuesProjectFilter = null;
String[] sarrOptionValuesLookbackDurationFilter = null;
List<String> listUserIds = null;
String sSelectedUserId = "";
String sProjectId = null;
StringBuffer	oAuditPayload	=	null;
UserSession userSession = null;
UserInfo userInfo = null;
List<ProjectInfo> allProjectList = null;
String p_name = null;
int nProjectId = 0;
sSelectedUserId = StringUtils.emptyString((String)request.getAttribute("select_criteria_user"));


sProjectId = (String)request.getAttribute("select_criteria_project");

 listUserIds = (List<String>)request.getAttribute("list_of_users");
 p_name = (String)request.getAttribute("selectedProject");

if (listUserIds != null)
{
	sarrOptionValuesUserFilter = new String[listUserIds.size() + 1];
	sarrOptionValuesUserFilter[0] = DUMMY_USER_RECORD_FOR_SELECTION;
	
	for (int nCounter = 0; nCounter < listUserIds.size(); nCounter++)
	{
		sarrOptionValuesUserFilter[nCounter + 1] = listUserIds.get(nCounter);
	}
}
else
{
	sarrOptionValuesUserFilter = new String[] {DUMMY_USER_RECORD_FOR_SELECTION};
}

Arrays.sort(sarrOptionValuesUserFilter);

sarrOptionValuesLookbackDurationFilter = new String[] {"Last 7 days"};

userSession = WebUtils.getUserSession(request);
userInfo =	WebUtils.getUserSession(request).getUserInfo();
WebUtils.setRequestAttribute(request, "userInfoSession", userInfo);

List<ObjectIdPair<Integer,String>> l_listauthorizedprojectids 	=	(userSession != null ? userSession.getAuthorizedProjectIds() : null);

allProjectList = ProjectHome.getAllProjects();


%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script src="js/jquery.js"></script>
<SCRIPT>
var mapAuditObjects;

mapAuditObjects = new Array();

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

function submitAuditFilterForm()
{
	clearErrorMessages();
	var form = document.getElementById("frmaudit");	
	if (document.frmaudit.auditusertype.value == "all") {
		var sSelectedUser = document.getElementById("select_criteria_user").value;
		if (sSelectedUser == "<%=DUMMY_USER_RECORD_FOR_SELECTION%>")
		{
			displayErrorMessage("UI-1015");
			return false;
		}
	}
	document.getElementById("portal_action").value = "fetch_results";
	
	if (validateFormParameters())
	 {
		 form.submit();
	 }
}


function viewDetails(nAuditSeqId)
{
	var l_oAuditJsObject;
	
	l_oAuditJsObject = mapAuditObjects[nAuditSeqId];
	if (l_oAuditJsObject != undefined)
	{
		alert("Value for " + nAuditSeqId 
				+ "\r" + "Date-time: " + l_oAuditJsObject.actionDateTime
				+ "\r" + "Server: "    + l_oAuditJsObject.actionServer
				+ "\r" + "App: " + l_oAuditJsObject.actionApp
				+ "\r" + "Action type: " + l_oAuditJsObject.actionType
				+ "\r" + "Action Code: " + l_oAuditJsObject.actionCode
				+ "\r" + "User SeqId: " + l_oAuditJsObject.actionUserSeqId
				+ "\r" + "User Id: " + l_oAuditJsObject.actionUserId
				+ "\r" + "Client IP: " + l_oAuditJsObject.actionClientIp
				+ "\r" + "Client Host: " + l_oAuditJsObject.actionClientHost
				+ "\r" + "Payload: " + l_oAuditJsObject.actionPayload
				+ "\r" + "Details: " + l_oAuditJsObject.detailsCount
			);
			var nDisplayCounter = 0;
			for (nDisplayCounter = 0; nDisplayCounter < l_oAuditJsObject.detailsCount; nDisplayCounter++)
			{
				alert("Detail #" + (nDisplayCounter+1)
					+ "\r" + "Field: " + l_oAuditJsObject.details[nDisplayCounter].fieldName
					+ "\r" + "Datatype: " + l_oAuditJsObject.details[nDisplayCounter].fieldDatatype
					+ "\r" + "Old value: " + l_oAuditJsObject.details[nDisplayCounter].fieldOldValue
					+ "\r" + "New value: " + l_oAuditJsObject.details[nDisplayCounter].fieldNewValue
					);
			}
	}
}
function changeToolTip(currObject,nAuditSeqId) {
	
	var l_oAuditJsObject;
	var displaydata;
	l_oAuditJsObject = mapAuditObjects[nAuditSeqId];
	if (l_oAuditJsObject != undefined)
	{
		/* alert("Value for " + nAuditSeqId 
				+ "\r" + "Date-time: " + l_oAuditJsObject.actionDateTime
				+ "\r" + "Server: "    + l_oAuditJsObject.actionServer
				+ "\r" + "App: " + l_oAuditJsObject.actionApp
				+ "\r" + "Action type: " + l_oAuditJsObject.actionType
				+ "\r" + "Action Code: " + l_oAuditJsObject.actionCode
				+ "\r" + "User SeqId: " + l_oAuditJsObject.actionUserSeqId
				+ "\r" + "User Id: " + l_oAuditJsObject.actionUserId
				+ "\r" + "Client IP: " + l_oAuditJsObject.actionClientIp
				+ "\r" + "Client Host: " + l_oAuditJsObject.actionClientHost
				+ "\r" + "Payload: " + l_oAuditJsObject.actionPayload
				+ "\r" + "Details: " + l_oAuditJsObject.detailsCount
			); */
			var nDisplayCounter = 0;
			for (nDisplayCounter = 0; nDisplayCounter < l_oAuditJsObject.detailsCount; nDisplayCounter++)
			{
				/* alert("Detail #" + (nDisplayCounter+1)
					+ "\r" + "Field: " + l_oAuditJsObject.details[nDisplayCounter].fieldName
					+ "\r" + "Datatype: " + l_oAuditJsObject.details[nDisplayCounter].fieldDatatype
					+ "\r" + "Old value: " + l_oAuditJsObject.details[nDisplayCounter].fieldOldValue
					+ "\r" + "New value: " + l_oAuditJsObject.details[nDisplayCounter].fieldNewValue
					); */
				displaydata	=	"Detail #" + (nDisplayCounter+1) + "\r" + "Field: " + l_oAuditJsObject.details[nDisplayCounter].fieldName + "\r" + "Datatype: " + l_oAuditJsObject.details[nDisplayCounter].fieldDatatype + "\r" + "Old value: " + l_oAuditJsObject.details[nDisplayCounter].fieldOldValue + "\r" + "New value: " + l_oAuditJsObject.details[nDisplayCounter].fieldNewValue
			}
	}
}	
	
function usertypeselection (p_value) {
	if (p_value == 1) {
		$("[id=auditusertypesystem]").hide();
		$("[id=auditusertypeapplication]").show();
		$("[id=selectProjectDropDown]").show();	
	} else if (p_value == 2) {
		$("[id=auditusertypesystem]").show();
		$("[id=auditusertypeapplication]").hide();
		$("[id=selectProjectDropDown]").hide();		
	} else {
		
	}
}
function setdefaultdata () {
	var	l_auditusertype	=	'<%= request.getAttribute("auditusertype") %>';
	if (l_auditusertype == "all") {
		document.getElementById("auditusertypeall").checked = true;
		usertypeselection('1');
	} else if (l_auditusertype == "system")  {
		document.getElementById("auditusertypesys").checked = true;
		usertypeselection('2');
	}
}
function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["frmaudit"];
	return validateInputParameter(oFormObject);
}

function fnfilteruser(projectName) {
	var formName = document.getElementById("frmaudit");
	document.getElementById("portal_action").value = "getuserList";
	document.getElementById("project_name").value = projectName;
	document.getElementById("portal_appid").value = 'AUDIT-PORTAL-APP';
	
	if(validateFormParameters()){ 
		formName.submit();
	}
}

</SCRIPT>

</head>
<body  onload="setdefaultdata();">
<csrf:form name="frmaudit" id="frmaudit" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="AUDIT-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="project_name" name="project_name" value="">
	<div>
		<%@include file="uiFormCreationMethods.jsp"%>
		<div class="page-title">
			<div class="title_left">
				<h3>Audit Trail Viewer</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class ="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
	   			<div class="x_panel">
	   				<div class="x_title">
						<h2>
						 	Audit Trail
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
			 		<div class="x_content">	          			
	          					 <%if(userInfo.isAuditor() || userInfo.isProjectAuditor()) {
	          				 	 %>	
			          			<div id="audituserprojecttypeapplication" >
		    						<div class="form-group">
		    						<div class="form-group" id="selectProjectDropDown">
											<label class="control-label col-md-3 col-sm-3 col-xs-12 ">Projects *</label>    						
	    								<div class="col-md-4 col-sm-4 col-xs-12">
											
											<select class="form-control" id="select_criteria_project" name="select_criteria_project" onChange="fnfilteruser(this.value)">
												<%if (userInfo.isAuditor())
												{%>
													<option value="">Project Neutral</option>
													<% for (ProjectInfo oProjectInfo: allProjectList)
													{ %>
														
														<%if(oProjectInfo !=  null)
														{
															if(!StringUtils.isNullOrEmpty(p_name)){
																if(oProjectInfo.getProjectSequenceId()==Integer.parseInt(p_name)){
																	%>
																<option value="<%=oProjectInfo.getProjectSequenceId() %>" selected="selected"><%=oProjectInfo.getProjectName()%></option>
																<%}else{%>
																<option value="<%=oProjectInfo.getProjectSequenceId() %>"><%=oProjectInfo.getProjectName()%></option>
																	<%}%>
																<%}else{%>
																<option value="<%=oProjectInfo.getProjectSequenceId() %>"><%=oProjectInfo.getProjectName()%></option>
																<%}%>
														<%}else {%>
																<option value="<%=oProjectInfo.getProjectSequenceId() %>"><%=oProjectInfo.getProjectName()%></option>
															<%}%>
														<%}%>
												<%}else{ %>	
												
												<% for (ObjectIdPair<Integer,String> l_authorizedprojects : l_listauthorizedprojectids) 
													{
													if(!StringUtils.isNullOrEmpty(p_name))
	                  								 {%>
														<% if(l_authorizedprojects.getObjectSeqId()==Integer.parseInt(p_name))
	                  										{%>
	                  											<option value="<%=l_authorizedprojects.getObjectSeqId() %>" selected="selected"><%= l_authorizedprojects.getObjectName() %></option>
	                  										<%}else{ %>
	                  											<option value="<%=l_authorizedprojects.getObjectSeqId() %>"><%= l_authorizedprojects.getObjectName() %></option>
	                  									<%} %>
													<%}else{ %>
																<option value="<%=l_authorizedprojects.getObjectSeqId() %>"><%= l_authorizedprojects.getObjectName() %></option>
													<%}
													}%>	
												
													
													<%} %>
											</select>
												
										</div>
		    						</div>
		    					</div>	   					   				
    						<%} %>
    								
	          			<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
	                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="auditusertype"  id="auditusertypeall" value="all" onclick="return usertypeselection(1);"  checked /> User</label>
	                         </div>
	                    </div>
	                    
	                    <%if((userInfo.isAuditor())){ %>		
	                    
	                    <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	                           <label><input type="radio"  name="auditusertype" id="auditusertypesys" value="system" onclick="return usertypeselection(2);" /> System</label>
	                         </div>
                       </div>
                       
                       <% } %>
                       
	                    <div id="auditusertypeapplication" >
    						<div class="form-group">
    									<%=displaySelectBox("select_criteria_user", "Filter User", true, sarrOptionValuesUserFilter, StringUtils.emptyString(sSelectedUserId))%>
    									<%=displaySelectBox("select_criteria_timeduration", "Lookback Duration", true, sarrOptionValuesLookbackDurationFilter, "")%>
    						</div>
    					</div>	
	                    
                       <div id="auditusertypesystem" style="display:none">
    						<div class="form-group">
    							<%=displaySelectBox("select_criteria_timeduration", "Lookback Duration", true, sarrOptionValuesLookbackDurationFilter, "")%>
    						</div>
    					</div>
	          			
	          			<%if(userInfo.isAuditor() || (userInfo.isProjectAuditor() && l_listauthorizedprojectids.size()>0)){ %>
				  		<div class="form-group">
				    		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				      			<button type="button" class="btn btn-success" onclick="return submitAuditFilterForm();">Submit</button>
				    		</div>
				  		</div>
				  		<%} %>
	          			<% if (listAuditTrail != null) { %>
				  		<div class="ln_solid"></div>
	                    <table class="table table-striped" >
				  			<SMALL><%=listAuditTrail.size()%> records found</SMALL>
				  				<thead>
			                        <tr>
			                        	<th/>
			                          	<th>When<BR/><SMALL><SMALL>Action date-time</SMALL></SMALL></th>
			                          	<th>Who<BR/><SMALL><SMALL>Userid | User Sequence Id</SMALL></SMALL></th>
			                          	<th>Where<BR/><SMALL><SMALL>Client IP address | Client host Name</SMALL></SMALL></th>
			                          	<th>What<BR/><SMALL><SMALL>Action code | Action Type</SMALL></SMALL></th>
			                          	<th>Parameters<BR/></th>
			                          	<!-- <th>Details<BR/><SMALL><SMALL>&nbsp;</SMALL></SMALL></th> -->
			                        </tr>
		                      	</thead>
	                      		<tbody><%
	                      			if (listAuditTrail.size() > 0)
									{
										for (AuditInfoDb oAuditInfoObj : listAuditTrail)
										{ %>
											<SCRIPT>
													var oAuditJsObject = new Object();
													var oAuditDetailsJsObject;
													oAuditJsObject.actionDateTime = "<%=oAuditInfoObj.getActionDateTime()%>";
													oAuditJsObject.actionServer = "<%=oAuditInfoObj.getActionServer()%>";
													oAuditJsObject.actionApp = '<%=oAuditInfoObj.getActionApp()%>';
													oAuditJsObject.actionType = '<%=oAuditInfoObj.getActionType()%>'; 
													oAuditJsObject.actionCode = '<%=oAuditInfoObj.getActionCode()%>';
													oAuditJsObject.actionUserSeqId = '<%=oAuditInfoObj.getActionUserSeqId()%>';
													oAuditJsObject.actionUserId = '<%=oAuditInfoObj.getActionUserId()%>';
													oAuditJsObject.actionClientIp = '<%=oAuditInfoObj.getActionClientIp()%>';
													oAuditJsObject.actionClientHost = '<%=oAuditInfoObj.getActionClientHost()%>';
													oAuditJsObject.actionPayload =  '<%=oAuditInfoObj.getActionPayload()%>';
													oAuditJsObject.detailsCount = <%=oAuditInfoObj.getDetails().size()%>;
													oAuditJsObject.details = new Array();
													
													mapAuditObjects['<%=oAuditInfoObj.getAuditSeqId()%>'] = oAuditJsObject;
													
											</SCRIPT>
											<tr>
												<td>
						                        	<a id="<%=oAuditInfoObj.getAuditSeqId()%>" class="" data-toggle="modal" 
					                          			data-target="#myModal<%=oAuditInfoObj.getAuditSeqId()%>" >
					                          			<i class="glyphicon glyphicon-eye-open"></i>
					                          		</a>
					                          		<%-- <a href="javascript:viewDetails('<%=oAuditInfoObj.getAuditSeqId()%>');" data-toggle="tooltip" data-placement="left" title="View event complete details">
					                        		
					                        		</a> --%>
						                          	<div id="myModal<%=oAuditInfoObj.getAuditSeqId()%>" class="modal fade" role="dialog">
														<div class="modal-dialog modal-lg">
													  		<div class="modal-content">
																<div class="modal-header">
																  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span>
																  </button>
																  <h2 id="myModalLabel2">Audit Information</h2>
																</div>
																<div class="modal-body">
																	<div class="x_panel">
																		<div class="x_title">
																			<h2>Audit Metadata</h2>
																			<div class="clearfix"></div>
																		</div>
																		<div class="x_content">
																			<table class='table table-striped'>
																				<tr>
																					<td>
																						<label>Audit Seq Id</label>
																					</td>
																					<td>
																						<%=oAuditInfoObj.getAuditSeqId()%>
																					</td>
																					<td>
																						<label>Timestamp</label>
																					</td>
																					<td>
																						<%=WebUtils.formatForDisplayDate(oAuditInfoObj.getActionDateTime())%>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<label>User Name</label>
																					</td>
																					<td>
																						<%=WebUtils.formatForDisplay(oAuditInfoObj.getActionUserId(), "N/A")%>
																					</td>
																					<td>
																						<label>User Seq Id</label>
																					</td>
																					<td>
																						<%= oAuditInfoObj.getActionUserSeqId() %>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<label>Client IP</label>
																					</td>
																					<td>
																						<%=WebUtils.formatForDisplay(oAuditInfoObj.getActionClientIp(), "N/A") %>
																					</td>
																					<td>
																						<label>Client Host</label>
																					</td>
																					<td>
																						<%=WebUtils.formatForDisplay(oAuditInfoObj.getActionClientHost(), "N/A")%>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<label>Application Name</label>
																					</td>
																					<td>
																						<%=WebUtils.formatForDisplay(oAuditInfoObj.getActionApp(), "None")%>
																					</td>
																					<td>
																						<label>Server</label>
																					</td>
																					<td>
																						<%=oAuditInfoObj.getActionServer()%>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<label>Action Code</label>
																					</td>
																					<td>
																						<%=oAuditInfoObj.getActionCode()%>
																					</td>
																					<td>
																						<label>Action Type</label>
																					</td>
																					<td>
																						<%=oAuditInfoObj.getActionType()%>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<label>Project Id</label>
																					</td>
																					<td>
																						<%=oAuditInfoObj.getProjectId()%>
																					</td>
																					<td>
																						<label></label>
																					</td>
																					<td>
																						
																					</td>
																				</tr>
																			</table>
																		</div>
																	</div>
																	<div class="x_panel">
																		<div class="x_title">
																			<h2>Payload Details</h2>
																			<div class="clearfix"></div>
																		</div>
																		<div class="x_content">
																			<table class='table table-striped'>
																				<thead>
																					<tr>
																						<th>
																							Field Name
																						</th>	
																						<th>
																							Field Value
																						</th>
																					</tr>
																				</thead>
																				<tbody>
																					<%
																						oAuditPayload	=	new StringBuffer(WebUtils.formatForDisplay(oAuditInfoObj.getActionPayload()));
														                        		if (oAuditPayload != null && oAuditPayload.length() != 0) {
														                        			oAuditPayload.deleteCharAt(0);
														                        			oAuditPayload.deleteCharAt(oAuditPayload.length()-1);
														                        			for (String payload : oAuditPayload.toString().split(",")) {
														                        				payload	=	payload.replaceAll("(\\{|\\}|\")+", "");	%>
														                        				<tr>
														                        					<% 	for (String payloadData : payload.split(":")) { %>
														                        						<td style="word-wrap:break-word;">
																											<%=payloadData %>
																										</td>
														                        					<% } %>
																								</tr>
														                        		  <%}
														                        		} else { %>
														                        			<tr>
														                        				<td>
														                        					None
														                        				</td>
														                        				<td>
														                        					None
														                        				</td>
														                        			</tr> <% 
														                        		}
														                        	%>
														                        </tbody>	
																			</table>
																		</div>
																	</div>
																	<%	if (oAuditInfoObj.getDetails() != null && oAuditInfoObj.getDetails().size() > 0) { %>			
																		<div class="x_panel">
																			<div class="x_title">
																				<h2>Audit Details</h2>
																				<div class="clearfix"></div>
																			</div>
																			<div class="x_content">
																				<table class='table table-bordered table-striped' style="table-layout:fixed;">
																					<thead>
																						<tr class="headings">
																							<th class="column-title">Field</th>
																							<th class="column-title">Data Type</th>
																							<th class="column-title">Old Value</th>
																							<th class="column-title">New Value</th>
																						</tr>
																					</thead>
																					<tbody>
															                    		<%
															                          		for (int nCounter = 0; nCounter < oAuditInfoObj.getDetails().size(); nCounter++) {
															                          			oAuditInfoDetailsDb = oAuditInfoObj.getDetails().get(nCounter); 
															                          			sOldValueToDisplay = oAuditInfoDetailsDb.getFieldOldValue();
															                          			sNewValueToDisplay = oAuditInfoDetailsDb.getFieldNewValue();
															                          		
															                          			//sOldValueToDisplay = WebUtils.formatForDisplay(sOldValueToDisplay, "(none)", MAX_DISPLAY_LENGTH);
															                          			//sNewValueToDisplay = WebUtils.formatForDisplay(sNewValueToDisplay, "(none)", MAX_DISPLAY_LENGTH);
															                          		
															                          			if (nCounter > 0) {
																                          			%><BR/><%
																                          		}
																	                          	%>
																	                          	<tr>
																							  		<td>
																							  			<%=oAuditInfoDetailsDb.getFieldName()%>
																							  		</td>
																							  		<td>
																							  			<%=oAuditInfoDetailsDb.getFieldDatatype()%>
																							  		</td>
																							  		<td style="word-wrap:break-word;">
																							  			<%--  --%>
																							  			<c:out value="<%=sOldValueToDisplay %>"  escapeXml="true"/>
																							  		</td>
																							  		<td style="word-wrap:break-word;">
																							  			<%-- <%= sNewValueToDisplay %> --%>
																							  			<c:out value="<%=sNewValueToDisplay %>" escapeXml="true"/>
																							  		</td>
																							  	</tr>
																	                          	<SCRIPT>
																	                          		oAuditDetailsJsObject = new Object();
																	                          		oAuditDetailsJsObject.fieldName = '<%=oAuditInfoDetailsDb.getFieldName()%>';
																	                          		oAuditDetailsJsObject.fieldDatatype = '<%=oAuditInfoDetailsDb.getFieldDatatype()%>';
																	                          		//DEV-NOTE:Old and New values need to be escaped for storage/presentation in Javascript
																	                          		oAuditDetailsJsObject.fieldOldValue = '<%=oAuditInfoDetailsDb.getFieldName()%>';
																	                          		oAuditDetailsJsObject.fieldNewValue = '<%=oAuditInfoDetailsDb.getFieldName()%>';
																	                          		
																	                          		mapAuditObjects['<%=oAuditInfoObj.getAuditSeqId()%>'].details[<%=nCounter%>] = oAuditDetailsJsObject;
																	                          	</SCRIPT>
																                          		<%
															                          		}
															                        	%>
															                        </tbody>	
								                        						</table>
								                        					</div>
								                        				</div>	
								                        			<%	} else {
										                          		}
										                          	%>		
																</div>
																<div class="modal-footer">
																  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																</div>
															</div>
													  	</div>
													</div>
					                          	</td>
						                        <td>
						                        	<%=WebUtils.formatForDisplay(oAuditInfoObj.getActionDateTime())%><BR/><SMALL><%=WebUtils.formatForDisplayDuration(oAuditInfoObj.getActionDateTime())%></SMALL>
						                        </td>
						                        <td>
						                        	<%=WebUtils.formatForDisplay(oAuditInfoObj.getActionUserId(), "n/a")%><BR/><SMALL><%="Id:" + oAuditInfoObj.getActionUserSeqId() %></SMALL>
						                        </td>
						                        <td>
						                        	<%=WebUtils.formatForDisplay(oAuditInfoObj.getActionClientIp(), "n/a") %><BR/><SMALL><%=WebUtils.formatForDisplay(oAuditInfoObj.getActionClientHost(), "n/a")%></SMALL>
						                        </td>
						                        <td>
						                        	<% if (oAuditInfoObj.getActionType().equalsIgnoreCase("LOGIN")) {
								                          	%><i class="fa fa-sign-in" style="color:blue"></i><%
					                          		} else if (oAuditInfoObj.getActionType().equalsIgnoreCase("LOGOUT")) {
					                          			%><i class="fa fa-sign-out" style="color:blue"></i><%
					                          		} else if (oAuditInfoObj.getActionType().equalsIgnoreCase("DATA-INSERT")) {
					                          			%><i class="fa fa-asterisk" style="color:blue"></i><%
					                          		} else if (oAuditInfoObj.getActionType().equalsIgnoreCase("DATA-UPDATE")) {
					                          			%><i class="fa fa-retweet" style="color:blue"></i><%
					                          		} else if (oAuditInfoObj.getActionType().equalsIgnoreCase("DATA-DELETE")) {
					                          			%><i class="fa fa-close" style="color:blue"></i><%
					                          		} else {
					                          			%><i class="fa fa-question" style="color:blue"></i><%
						                          	}%>
						                          
						                          	<%=oAuditInfoObj.getActionCode()%><BR/><SMALL><%=WebUtils.formatForDisplay(oAuditInfoObj.getActionApp(), "(none)")%></SMALL></td>
						                        <td>
						                        	<%
						                        		if (oAuditPayload != null && oAuditPayload.length() != 0) {
						                        			for (String s : oAuditPayload.toString().split(",")) {
						                        				s	=	s.replaceAll("(\\{|\\}|\")+", "");	%>
						                        				<%=s %>
						                        				<br/>
						                        		  <%}
						                        		}
						                        	%>	
						                        </td>
											</tr> <%
										}
									} else {	%>
										<TR><TD colspan="7">&nbsp;<i>No records found</i></TD><%
									} %>
	                    		</tbody>
							</TABLE>
						<%}%>
					</div>
				</div>
			</div>
		</div>	
	</div>
</csrf:form>
</body>
</html>