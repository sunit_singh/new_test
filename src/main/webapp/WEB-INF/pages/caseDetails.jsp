<div id="screen_for_step2">
	<h4><u>Step 2</u></h4>
	<table width="75%">
		<tr><td style="font-weight: bold">Case Id</td><td><input type="text" value="IN-201610-12345" disabled/></td></tr>
		<tr><td style="font-weight: bold">Source Database</td><td><input type="text" value="RSS" disabled/></td></tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr><td valign="top" style="font-weight: bold">Case Details</td><td>
				<table width="100%">
					<tr><td align="left" style="font-weight: bold"><b>Drug:</b>&nbsp;</td><td>ABCD</td></tr>
					<tr><td align="left" style="font-weight: bold"><b>Reporting Date:</b>&nbsp;</td><td>01 Oct 2016 9:00am GMT</td></tr>
					<tr><td align="left" style="font-weight: bold"><b>Reported Severity:</b>&nbsp;</td><td>Normal</td></tr>
				</table>
			</td></tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<!-- <tr><td>&nbsp;</td><td><input type="button" class="btn btn-default" value="Next >" onclick="javascript:SwitchToStep3()" /></td></tr> -->
		<tr><td>&nbsp;</td><td><input type="button" class="btn btn-default" value="Next >" onclick="javascript:SendServerToStep3()" /></td></tr>
	</table>
</div>