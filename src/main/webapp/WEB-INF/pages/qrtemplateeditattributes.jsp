<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.commons.SciConstants.StringConstants"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.commons.SciEnums.EmailSendSetting"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.SciEnums.RecordState"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFields" %>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.apps.qr.QRTemplate" %>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>

<%
	QRTemplate					l_qrtemplate				=	(QRTemplate) request.getAttribute("fldtemplatedata");
	SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate	=	(SafetyDbCaseFieldsTemplate) request.getAttribute("safetydbcasefields");
	List<SafetyDbCaseFields>	l_safetydbcasefieldlist		=	l_safetydbcasefieldtemplate.getCaseFieldList();
	RecordState					l_templatestate				=	l_qrtemplate.getState();
	String						l_mode						=	(String) request.getAttribute("fldmode");
	String						l_cataliases				=	(String) request.getAttribute("categoryaliases");	
	int							l_catcount					=	0;
	String []					l_categories				=	null;
	
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
	String sAppId = (String) request.getAttribute("r_appId");
	String sDownloadAllowed = ConfigHome.getPrjConfigSettings(sAppId+StringConstants.PERIOD+QRConstant.DOWNLOAD_ALLOWED,WebUtils.getAuthorizedProjectId(request));
%>
<!DOCTYPE html>
<html>
	<head>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
		$(window).load(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
			
			function fnonload() {
				<% if (l_qrtemplate != null && l_qrtemplate.getCaseResultCriteriaMode() == 1) { %>
					fnupdatecriteriamode(1);
				<% } else { %>
					fnupdatecriteriamode(2);
				<% } %>
			}
				
			function fnupdatecriteriamode (criteriamode) {
				if (criteriamode == "1") {
					document.getElementById("fldcriteriamodenumber").checked	=	true;
					document.getElementById("fldcriteriamode"+"label1").classList.add('btn-primary');
					document.getElementById("fldcriteriamode"+"label1").classList.remove('btn-default'); 
					document.getElementById("fldcriteriamode"+"label1").classList.remove('active');
					document.getElementById("fldcriteriamode"+"label2").classList.add('btn-default');
					document.getElementById("fldcriteriamode"+"label2").classList.remove('btn-primary');
					
					<% if (l_catcount == 0 || l_catcount == 1) { %>
					<% } else if (l_catcount == 2 || l_catcount == 3) { %>
						document.getElementById("fldcat1criteriaselect")[0].disabled = false;
						document.getElementById("fldcat1criteriaselect")[0].selected = true;
						document.getElementById("fldcat1criteriaselect")[1].disabled = true;
						document.getElementById("fldcat2criteriaselect")[0].disabled = false;
						document.getElementById("fldcat2criteriaselect")[0].selected = true;
						document.getElementById("fldcat2criteriaselect")[1].disabled = true;
						<% if (l_catcount == 3) { %>
							document.getElementById("fldcat3criteriaselect")[0].disabled = false;
							document.getElementById("fldcat3criteriaselect")[0].selected = true;
							document.getElementById("fldcat3criteriaselect")[1].disabled = true;
						<% } %>
					<% } %>
					document.getElementById("fldoverallcriteriaselect")[1].disabled = true;
					document.getElementById("fldoverallcriteriaselect")[0].disabled = false;
					document.getElementById("fldoverallcriteriaselect")[0].selected = true;
				} else {
					document.getElementById("fldcriteriamodepercent").checked	=	true;
					document.getElementById("fldcriteriamode"+"label1").classList.add('btn-default');
					document.getElementById("fldcriteriamode"+"label1").classList.remove('btn-primary');
					document.getElementById("fldcriteriamode"+"label2").classList.add('btn-primary');
					document.getElementById("fldcriteriamode"+"label2").classList.remove('active');
					document.getElementById("fldcriteriamode"+"label2").classList.remove('btn-default');
					<% if (l_catcount == 0 || l_catcount == 1) { %>
					<% } else if (l_catcount == 2 || l_catcount == 3) { %>
						document.getElementById("fldcat1criteriaselect")[1].disabled = false;
						document.getElementById("fldcat1criteriaselect")[1].selected = true;
						document.getElementById("fldcat1criteriaselect")[0].disabled = true;
						document.getElementById("fldcat2criteriaselect")[1].disabled = false;
						document.getElementById("fldcat2criteriaselect")[1].selected = true;
						document.getElementById("fldcat2criteriaselect")[0].disabled = true;
						<% if (l_catcount == 3) { %>
							document.getElementById("fldcat3criteriaselect")[1].disabled = false;
							document.getElementById("fldcat3criteriaselect")[1].selected = true;
							document.getElementById("fldcat3criteriaselect")[0].disabled = true;
						<% } %>
					<% } else { %>
					<% } %>
					document.getElementById("fldoverallcriteriaselect")[1].disabled = false;
					document.getElementById("fldoverallcriteriaselect")[1].selected = true;
					document.getElementById("fldoverallcriteriaselect")[0].disabled = true;
				}
			}
			function fncancel () {
				clearErrorMessages();
				document.frmmain.fldtemplateseqid.value	=	'<%= l_qrtemplate.getSequenceId() %>';
				document.frmmain.portal_action.value	=	"gettemplatedetails";
				document.frmmain.fldmode.value			=	"view";
		        document.frmmain.submit();
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnsubmit () {
				clearErrorMessages();
				if (document.frmmain.fldcorrectionallowedcheckbox.checked) {
					document.frmmain.fldcorrectionallowed.value = 1;
				} else {
					document.frmmain.fldcorrectionallowed.value = 0;
				}
				if (document.frmmain.fldcriteriamode.value == "1") {
					<% if (l_catcount == 0 || l_catcount == 1) { %>
					<% } else if (l_catcount == 2 || l_catcount == 3) { %>
						if (document.frmmain.fldcat1criteria.value < 0) {
							displayErrorMessage("UI-1079");
							return false;
						}	
						if (document.frmmain.fldcat2criteria.value < 0) {
							displayErrorMessage("UI-1079");
							return false;
						}
						<% if (l_catcount == 3) { %>
							if (document.frmmain.fldcat3criteria.value < 0) {
								displayErrorMessage("UI-1079");
								return false;
							}
						<% } %>
					<% } else { %>
					<% } %>
					
					if (document.frmmain.fldoverallcriteria.value < 0) {
						displayErrorMessage("UI-1079");
						return false;
					}
				} else {
					<% if (l_catcount == 0 || l_catcount == 1) { %>
					<% } else if (l_catcount == 2 || l_catcount == 3) { %>
						if (document.frmmain.fldcat1criteria.value > 100 || document.frmmain.fldcat1criteria.value < 0) {
							displayErrorMessage("UI-1080");
							return false;
						}	
						if (document.frmmain.fldcat2criteria.value > 100 || document.frmmain.fldcat2criteria.value < 0) {
							displayErrorMessage("UI-1080");
							return false;
						}
						<% if (l_catcount == 3) { %>
							if (document.frmmain.fldcat3criteria.value > 100 || document.frmmain.fldcat3criteria.value < 0) {
								displayErrorMessage("UI-1080");
								return false;
							}
						<% } %>
					<% } else { %>
					<% } %>
					if (document.frmmain.fldoverallcriteria.value > 100 || document.frmmain.fldoverallcriteria.value < 0) {
						displayErrorMessage("UI-1080");
						return false;
					}
					
				}
		        document.frmmain.fldtemplateseqid.value		=	'<%= l_qrtemplate.getSequenceId()%>';
		        document.frmmain.portal_action.value		=	"updateqrtattribute";	
		        document.frmmain.fldmode.value				=	"view";
				if (validateFormParameters()) {
		        	document.frmmain.submit();
				} else{
					return false;
				}
			}
			
		</script>
	</head>
	<body onload="fnonload();">
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="QR-PORTAL-APP">
			<input type="hidden" id="fldtemplateseqid" name="fldtemplateseqid" value="">
			<input type="hidden" id="fldmode" name="fldmode" value ="">
			<input type="hidden" id="fldcorrectionallowed" name="fldcorrectionallowed" value="">
			
			<div role="main">
				<div>
					<div class="page_title">
						<div class="title_left">
							<h3>Quality Review Template</h3>
						</div>
					</div>
					<div class="clearfix">
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										Edit Template Attributes
									</h2>
									<div class="clearfix">
									</div>
								</div>
								<div class="x_content">
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldtemplatename">
												Template Name
											</label>
										</b>	
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_qrtemplate.getTemplateName() %>
											</label>
										</div>
									</div>	
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Template State
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_templatestate.displayName() %>
											</label>
										</div>
									</div>
									<%-- <div class="x_title">
										<h4>
											Review Completion Notification Email
										</h4>
										<div class="clearfix">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailsendsetting">
											Configuration
											<span class="required"> *</span>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<select class="form-control" name="fldemailsendsetting" id="fldemailsendsetting">
												<option value="-1">(Select Email Send Configuration)</option>
												<% if (l_qrtemplate != null && l_qrtemplate.getRvwComplNtfnSetting() != null) { 
													for (EmailSendSetting l_emailsendsetting : EmailSendSetting.values()) { 
														if (l_emailsendsetting == l_qrtemplate.getRvwComplNtfnSetting()) { %>
															<option value ="<%= l_emailsendsetting.getSendCode()%>" selected><%= l_emailsendsetting.getDisplayName()%></option>
													 <% } else { %>
													 		<option value ="<%= l_emailsendsetting.getSendCode()%>"><%= l_emailsendsetting.getDisplayName()%></option>	
													 <% } %>
												 <% } %>	 
												<% } else { 
													for (EmailSendSetting l_emailsendsetting : EmailSendSetting.values()) { %>
														<option value ="<%= l_emailsendsetting.getSendCode()%>"><%= l_emailsendsetting.getDisplayName()%></option>
												 <% } %>
												<% } %>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldemailcclist">
											CC List
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<% if(l_qrtemplate.getRvwComplNtfnCCAddr() != null) { %>	
												<input class="form-control" type="text" id="fldemailcclist" name="fldemailcclist" value="<%= l_qrtemplate.getRvwComplNtfnCCAddr()%>">
											<% } else { %>
												<input class="form-control" type="text" id="fldemailcclist" name="fldemailcclist" value="">
											<% } %>
										</div>
									</div> --%>
									<div class="x_title">
										<h4>
											Case Result Criteria
										</h4>
										<div class="clearfix">
										</div>
									</div>
									<div class="form-group">
				                    	<label class="control-label col-md-3 col-sm-3 col-xs-12">Criteria Mode</label>
				                        <div class="col-md-6 col-sm-6 col-xs-12">
				                        	<div class="btn-group" data-toggle="buttons">
				                            	<label id="fldcriteriamodelabel1" class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
				                              		<input type="radio" name="fldcriteriamode" value="1" id="fldcriteriamodenumber" checked onchange="fnupdatecriteriamode('1');"> &nbsp; Number &nbsp;
				                            	</label>
				                            	<label id="fldcriteriamodelabel2" class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
				                              		<input type="radio" name="fldcriteriamode" value="2" id="fldcriteriamodepercent" onchange="fnupdatecriteriamode('2');"> Percent
				                            	</label>
				                          	</div>
				                        </div>
			                     	</div>
			                     	<% if (l_catcount == 0 || l_catcount == 1) { %>
									<% } else if (l_catcount == 2 || l_catcount == 3) { %> 
										
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcat1criteria">
												<%= l_categories[0] %>
											</label>
											<div class="col-md-2 col-sm-2 col-xs-12">
												<select class="form-control" name="fldcat1criteriaselect" id="fldcat1criteriaselect">
													<option value="L">Lower</option>
													<option value="G" disabled>Greater</option>
												</select>	
											</div>
											<% if (l_qrtemplate.getCat1QualityThreshold() != -1) { %>	
												<div class="col-md-2 col-sm-2 col-xs-12">
										 			<input class="form-control" type="text" id="fldcat1criteria" name="fldcat1criteria" 
										 				value="<%= l_qrtemplate.getCat1QualityThreshold()%>">
												</div>
											<% } else { %>
												<div class="col-md-2 col-sm-2 col-xs-12">
										 			<input class="form-control" type="text" id="fldcat1criteria" name="fldcat1criteria" 
										 				value="">
												</div>
											<% } %>	
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcat2criteria">
												<%= l_categories[1] %>
											</label>
											<div class="col-md-2 col-sm-2 col-xs-12">
												<select class="form-control" name="fldcat2criteriaselect" id="fldcat2criteriaselect">
													<option value="L">Lower</option>
													<option value="G" disabled>Greater</option>
												</select>	
											</div>
											<% if (l_qrtemplate.getCat2QualityThreshold() != -1) { %>	
												<div class="col-md-2 col-sm-2 col-xs-12">
										 			<input class="form-control" type="text" id="fldcat2criteria" name="fldcat2criteria" 
										 				value="<%= l_qrtemplate.getCat2QualityThreshold()%>">
												</div>
											<% } else { %>
												<div class="col-md-2 col-sm-2 col-xs-12">
										 			<input class="form-control" type="text" id="fldcat2criteria" name="fldcat2criteria" 
										 				value="">
												</div>
											<% } %>	
										</div>
										<% if (l_catcount == 3) { 	%>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcat3criteria">
													<%= l_categories[2] %>
												</label>
												<div class="col-md-2 col-sm-2 col-xs-12">
													<select class="form-control" name="fldcat3criteriaselect" id="fldcat3criteriaselect">
														<option value="L">Lower</option>
														<option value="G" disabled>Greater</option>
													</select>	
												</div>
												<% if (l_qrtemplate.getCat3QualityThreshold() != -1) { %>	
													<div class="col-md-2 col-sm-2 col-xs-12">
											 			<input class="form-control" type="text" id="fldcat3criteria" name="fldcat3criteria" 
											 				value="<%= l_qrtemplate.getCat3QualityThreshold()%>">
													</div>
												<% } else { %>
													<div class="col-md-2 col-sm-2 col-xs-12">
											 			<input class="form-control" type="text" id="fldcat3criteria" name="fldcat3criteria" 
											 				value="">
													</div>
												<% } %>	
											</div>
										<% } %>
									<% } else { %>
									<% } %>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldoverallcriteria">
											Overall Threshold
										</label>
										<div class="col-md-2 col-sm-2 col-xs-12">
											<select class="form-control" name="fldoverallcriteriaselect" id="fldoverallcriteriaselect">
												<option value="L">Lower</option>
												<option value="G" disabled>Greater</option>
											</select>	
										</div>
										<% if (l_qrtemplate.getCaseQualityThreshold() != -1) { %>	
											<div class="col-md-2 col-sm-2 col-xs-12">
									 			<input class="form-control" type="text" id="fldoverallcriteria" name="fldoverallcriteria" 
									 				value="<%= l_qrtemplate.getCaseQualityThreshold()%>">
											</div>
										<% } else { %>
											<div class="col-md-2 col-sm-2 col-xs-12">
									 			<input class="form-control" type="text" id="fldoverallcriteria" name="fldoverallcriteria" 
									 				value="">
											</div>
										<% } %>	
									</div>
									<%-- <div class="x_title">
										<h4>
											Case Classification
										</h4>
										<div class="clearfix">
										</div>
									</div>
									<% if (l_catcount == 0 || l_catcount == 1) { %>
									<% } else if (l_catcount == 2 || l_catcount == 3) { %> 
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcat1classification">
												Category1
											</label>
											<div class="col-md-4 col-sm-4 col-xs-12">
										 		<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationCat1() != null) { %>
										 			<input class="form-control" type="text" id="fldcat1classification" name="fldcat1classification" 
										 				value="<%= l_qrtemplate.getCaseClassificationCat1() %>">
										 		<% } else { %>
										 			<input class="form-control" type="text" id="fldcat1classification" name="fldcat1classification" value="">
										 		<% } %>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcat2classification">
												Category2
											</label>
											<div class="col-md-4 col-sm-4 col-xs-12">
										 		<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationCat2() != null) { %>
										 			<input class="form-control" type="text" id="fldcat2classification" name="fldcat2classification" 
										 				value="<%= l_qrtemplate.getCaseClassificationCat2() %>">
										 		<% } else { %>
										 			<input class="form-control" type="text" id="fldcat2classification" name="fldcat2classification" value="">
										 		<% } %>
											</div>
										</div>
										<% if (l_catcount == 3) { 	%>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcat3classification">
													Category3
												</label>
												<div class="col-md-4 col-sm-4 col-xs-12">
											 		<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationCat3() != null) { %>
											 			<input class="form-control" type="text" id="fldcat3classification" name="fldcat3classification" 
											 				value="<%= l_qrtemplate.getCaseClassificationCat3() %>">
											 		<% } else { %>
											 			<input class="form-control" type="text" id="fldcat3classification" name="fldcat3classification" value="">
											 		<% } %>
												</div>
											</div>
										<% } %>
									<% } else { %>
									<% } %>
									
									
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldoverallclassification">
											Overall
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<% if (l_qrtemplate != null && l_qrtemplate.getCaseClassificationOverall() != null) { %>
									 			<input class="form-control" type="text" id="fldoverallclassification" name="fldoverallclassification" 
									 				value="<%= l_qrtemplate.getCaseClassificationOverall() %>">
									 		<% } else { %>
									 			<input class="form-control" type="text" id="fldoverallclassification" name="fldoverallclassification" value="">
									 		<% } %>
										</div>
									</div> --%>
									<div class="x_title">
										<h4>
											Other Settings
										</h4>
										<div class="clearfix">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcorrectionallowedcheckbox">
											<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Correction done by reviewer">
                        					 	CDBR
                        					</span>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<% if (l_qrtemplate != null && l_qrtemplate.getRvwAllToCorrect() == 1) { %>
													<label>
														<input type="checkbox" id="fldcorrectionallowedcheckbox" checked name="fldcorrectionallowedcheckbox"
															 class="js-switch" data-switchery="true" style="display: none;">
													</label>
											 <% } else { %>		
											 		<label>
														<input type="checkbox" id="fldcorrectionallowedcheckbox" name="fldcorrectionallowedcheckbox" 
															class="js-switch" data-switchery="true" style="display: none;">
													</label>
											 <% } %>
										</div>	
									</div>
									<%if(sDownloadAllowed!=null && sDownloadAllowed.equalsIgnoreCase("true")){ %>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldcasereviewdwnldtmp">
												Upload Review Template
												<span class="required"> </span>
											</label>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<input type="file" name="fldcasereviewdwnldtmp" id="fldcasereviewdwnldtmp">
											</div>
										</div>	
									<%} %>
									<%-- <div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldreviewsummrequired">
											Review Summary Required
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<% if (l_qrtemplate != null && l_qrtemplate.getRvwSumRequired() == 1) { %>
													<label>
														<input type="checkbox" id="fldreviewsummrequired" checked name="fldreviewsummrequired"
															 class="js-switch" data-switchery="true" style="display: none;">
													</label>
											 <% } else { %>		
											 		<label>
														<input type="checkbox" id="fldreviewsummrequired" name="fldreviewsummrequired" 
															class="js-switch" data-switchery="true" style="display: none;">
													</label>
											 <% } %>
										</div>	
									</div> --%>
									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
											<button type="button" class="btn btn-primary" onclick="fncancel();" value="Cancel">
												Cancel
											</button>
											<button type="button" class="btn btn-success" onclick="fnsubmit();" value="Submit">
												Submit
											</button>
		                          		</div>
		                          	</div>
								</div>
							</div>
						</div>		
					</div>
					<div class="clearfix">
					</div>
				</div>
			</div>		
		</csrf:form>
	</body>
</html>