<%@page import="com.sciformix.sciportal.pvcp.PvcpCaseMaster"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="com.sciformix.commons.ObjectIdPair"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDBCaseFieldCategoryType"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFields"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRReviewData" %>
<%@page import="com.sciformix.commons.utils.StringUtils" %>

<!DOCTYPE html>
<% 
	QRReviewData		l_qrreviewdata		=	(QRReviewData) request.getAttribute("reviewdata");
	String				l_layouttype		=	(String)	request.getAttribute("UserLayoutType");
	String				l_reporttype		=	(String)	request.getAttribute("fldreporttype");
	Integer				l_startdatecount	=	(Integer)	request.getAttribute("fldstartdatecount"),
						l_enddatecount		=	(Integer)	request.getAttribute("fldenddatecount"),
						l_searchtype		=	(Integer)	request.getAttribute("fldsearchtype");
	boolean				l_ismrnameshow		=	(Boolean)	request.getAttribute("ismrnameshow");
	String				l_cataliases		=	(String) 	request.getAttribute("categoryaliases");	
	Map<String,String>	l_errortypes		= 	(Map<String,String>) request.getAttribute("errortypes");
	String				l_searchedcaseid	=	(String)	request.getAttribute("fldsearchedcaseid");	
	List<ObjectIdPair<String,String>>	l_reviewtypelist	=	(List<ObjectIdPair<String,String>>) request.getAttribute("reviewtypelist");
	Map<String,List<String>>	l_enableUdfMap	 	 		= 	(Map<String,List<String>>)request.getAttribute("enableUdfList");
	PvcpCaseMaster				o_pvcpcasemaster 			= 	(PvcpCaseMaster) request.getAttribute("pvcp_case_record");
	int					l_catcount			=	0;
	String []			l_categories		=	null;
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
	List<String>	l_udfvaluelist	=	new ArrayList<String>(); 
	if (o_pvcpcasemaster != null) {
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf1());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf2());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf3());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf4());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf5());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf6());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf7());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf8());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf9());
		l_udfvaluelist.add(o_pvcpcasemaster.getUdf10());
	}
	
%>
<html>
	<head>
		<link rel="stylesheet" href="css/SciCustom.css">
		<title>
			Review Quality Details
		</title>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
			function fnback () {
				<% if (StringUtils.isNullOrEmpty(l_reporttype)) { %>
					document.getElementById("portal_action").value = "qualityreview";
				<% } else { %>
					document.frmmain.portal_action.value 		= "preparereport";
					document.frmmain.fldreporttype.value 		= "<%=l_reporttype%>";
					document.frmmain.fldstartdatecount.value	= '<%=l_startdatecount%>';
					document.frmmain.fldenddatecount.value		= '<%=l_enddatecount%>';
					document.frmmain.fldsearchtype.value		= '<%=l_searchtype%>';
					document.frmmain.fldsearchedcaseid.value	= '<%=l_searchedcaseid%>';
				<% } %>
				document.frmmain.submit();
			}	
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			$(window).load(function(){
				$('.btnNext').click(function(){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				});
				$('.btnPrevious').click(function(){
					$('.nav-tabs > .active').prev('li').find('a').trigger('click');
				});
				$('[data-toggle="tooltip"]').tooltip();
			});
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldreporttype" name="fldreporttype" value="">
			<input type="hidden" id="fldstartdatecount" name="fldstartdatecount" value="">
			<input type="hidden" id="fldenddatecount" name="fldenddatecount" value="">
			<input type="hidden" id="fldsearchedcaseid" name="fldsearchedcaseid" value="">
			<input type="hidden" id="fldsearchtype" name="fldsearchtype" value="">
			
			<div class="title_left">
				<h3>Quality Review Case Details</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Details
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><%= ((SafetyDbCaseFieldsTemplate)l_qrreviewdata.getScft()).getCaseIdAlias() %></th>
									<th><%= ((SafetyDbCaseFieldsTemplate)l_qrreviewdata.getScft()).getCaseVersionAlias() %></th>
									<th>Review Type</th>
									<th>Initial Receive Date</th>
									<th>Seriousness</th>
									<!-- <th>Review Type</th> -->
									<th>Case Associate</th>
									<% if (l_ismrnameshow) { %>
										<th>Medical Reviewer</th>
									<% } %>
									<% if(!l_enableUdfMap.isEmpty()) {
										for (String key : l_enableUdfMap.keySet()) {%>
											<th><%=l_enableUdfMap.get(key).get(0) %></th>
									<%	} 
									} %>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<%=l_qrreviewdata.getCaseId()%>
									</td>
									<td>
										<%=l_qrreviewdata.getCaseVersion()%>
									</td>
									<% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) {  
											if (l_reviewtypedata.getObjectSeqId().equals(l_qrreviewdata.getReviewType())) { %>
												<td><%=l_reviewtypedata.getObjectName() %></td>
										 <% } %>
									<% } %>
									<td>
										<%= new SimpleDateFormat("dd-MMM-yyyy").format(l_qrreviewdata.getCaseRecvDate()) %>
									</td>
									<td>
										<%=l_qrreviewdata.getCaseSeriousness()%>
									</td>
									<%-- <td>
										<%=l_qrreviewdata.getReviewType()%>
									</td> --%>
									<td>
										<%=l_qrreviewdata.getCaseAssociateName()%>
									</td>
									<% if (l_ismrnameshow) { %>
										<td>
											<%=l_qrreviewdata.getMedicalReviewerName()%>
										</td>
									<% } %>
									<% if(!l_enableUdfMap.isEmpty()) {
										for (int l_i=1; l_i <= 10; l_i++) { %>
							         		<% if(l_enableUdfMap.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)) {
							         			if (l_enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1) != null) { %>
													<% for (String l_udfvalue : StringUtils.splitOnComma(l_enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1))) { 
														String[]	l_udfdetails = l_udfvalue.split(":"); 
														if (l_udfdetails[0].equals(l_udfvaluelist.get(l_i-1))) { %>
															<td><%=StringUtils.emptyString(l_udfdetails[1])%></td>
													 <% } %>
													<% } %>
														
											 <% } else {%>
											 		<td><%=StringUtils.emptyString(l_udfvaluelist.get(l_i-1))%></td>
											 <% } %>
							         		<% } %>
							         	<% } %>
							         <% } %>	
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Statistics
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Fields Reviewed</th>
									<th>Fields Without Error</th>
									<th>Fields Incorrect</th>
									<th>Case Quality(%)</th>
									<% if (l_catcount == 0 || l_catcount == 1) { %>
									<% } else { 
										for (String l_categoryname : l_categories) { %>
											<th><%= l_categoryname %>(%)</th>
									 <% } %>
									<% } %>
									<th>Case Result</th>
								</tr>
								
							</thead>
							<tbody>
								<tr>
									<td>
										<%=l_qrreviewdata.getFieldsReviewed()%>
									</td>
									<td>
										<%=l_qrreviewdata.getFieldsWithoutError()%>
									</td>
									<td>
										<%=l_qrreviewdata.getFieldsError()%>
									</td>
									<td>
										<%=l_qrreviewdata.getCaseQuality()%>
									</td>
									<% if (l_catcount == 0 || l_catcount == 1) { %>
									<% } else if (l_catcount == 2) { %>
										<td>
											<%=l_qrreviewdata.getCaseCat1Quality()%>
										</td>
										<td>
											<%=l_qrreviewdata.getCaseCat2Quality()%>
										</td>
									<% } else if (l_catcount == 3) { %>
										<td>
											<%=l_qrreviewdata.getCaseCat1Quality()%>
										</td>
										<td>
											<%=l_qrreviewdata.getCaseCat2Quality()%>
										</td>
										<td>
											<%=l_qrreviewdata.getCaseCat3Quality()%>
										</td>
									<% } else { %>
									<% } %>
									<td>
										<%=l_qrreviewdata.getCaseResult()%>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Review Summary
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="fldemailcclist">
								Review Summary
							</label>
							<div class="col-md-10 col-sm-10 col-xs-12">
								<textarea class="form-control vresize" readonly id="casereviewsumm" name="casereviewsumm" rows="6"><%=l_qrreviewdata.getCaseReviewSummary()%></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
						 	Case Field Details
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<% if ("Single Page".equals(l_layouttype)) { %>
							<table class="table table-bordered" id="detailstable" style="table-layout:fixed;">
								<thead>
									<tr>
										<th>Tab Name</th>
										<th>Field</th>
										<th>Review Status</th>
										<th>Error Type</th>
										<th style="word-wrap:break-word;">Comments</th>
									</tr>
								</thead>
								<tbody>	<%	
									int		l_tabindex	=	0;
									for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
										int		l_counttemp	=	1;
										for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) { %>
											<tr>
												<% if (l_counttemp == 1 ) { %>
													<td rowspan="<%=l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname).size()%>">
														<b><%=l_tabname %></b>
													</td>
												<% } %>	
												<td>
													<% if (l_catcount == 0 || l_catcount == 1) { %>
													<% } else if (l_catcount == 2) {
														if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
															<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
														<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
															<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
														<% } %>
													<% } else if (l_catcount == 3) { 
														if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
															<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
														<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
															<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
														<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
															<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
														<% } %>
													<% } else { %>
													<% } %>
													<label>
														<%= l_tabdata.getCaseFieldName() %>
													</label>
												</td>
												<td>
													<% 	int	l_reviewstatus	=	l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus();
														if (l_reviewstatus == 1) { %>
															<label class="btn btn-sm custom-btn-green" data-toggle-class="btn-primary" data-toggle="tooltip"
																			 data-placement="left" data-original-title="Correct">
																<i class="glyphicon glyphicon-ok"></i>
															</label>
													<% } else if (l_reviewstatus == 2) { %>
															<label class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary">
																<i class="glyphicon glyphicon-remove"></i>
															</label>
													<% } else if (l_reviewstatus == 3) { %>
															<label class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary" data-toggle="tooltip"
																			 data-placement="right" data-original-title="Incorrect">
																<i class="glyphicon glyphicon-ban-circle"></i>
															</label>
													<% } %> 
												</td>
												<td>
													<% if (l_reviewstatus == 3) { %>
														<%=l_errortypes.get(l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldErrorCode())%> 
													<% } %>
												</td>
												<td style="word-wrap:break-word;">
													<%= l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldComment() %>
												</td>
											</tr>
											<%  
													l_counttemp++;
												} 
												l_tabindex++;
											%>
									<%	} %>
								</tbody>
							</table>
							<% if (StringUtils.isNullOrEmpty(l_reporttype)) { %>
								<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
									Review Next Case
								</button>
							<% } else { %>
								<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
									Back
								</button>
							<% } %>
							
						<% } else { %>
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
										<% int	l_count = 1;
										for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
											if (l_count == 1) { %>
												<li role="presentation" class="active">
													<a href="#tab_content<%=l_count%>" role="tab" id="<%= l_tabname %>" data-toggle="tab" aria-expanded="true" tabindex="<%=l_count%>>">
														<%= l_tabname %>
													</a>
												</li>
										<%  } else { %>
												<li role="presentation" class="">
													<a href="#tab_content<%=l_count%>" role="tab" id="<%= l_tabname %>" data-toggle="tab" aria-expanded="false" tabindex="<%=l_count%>>">
														<%= l_tabname %>
													</a>
												</li>
										<%  }
											l_count++;	
										%>
									<%  }
									%>
								</ul>
								<div id="myTabContent" class="tab-content">
								<% 
									int	l_tabdetails = 1;
									for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { %>
										<div role="tabpanel" id="tab_content<%=l_tabdetails%>" aria-labelledby="<%= l_tabname %>" 
										<% 	if (l_tabdetails == 1) {
										%>
												 class="tab-pane fade active in">
										<%	} else { %>
												 class="tab-pane fade">
													
										<%	}
										%>		
											<table class="table table-striped" id="detailstable" style="table-layout:fixed;">
												<thead>
													<tr>
														<th>Field</th>
														<th>Review Status</th>
														<th>Error Type</th>
														<th>Comments</th>
													</tr>
												</thead>
												<tbody>		
													<% 	for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) { %>
														<tr>
															<td>
																<% if (l_catcount == 0 || l_catcount == 1) { %>
																<% } else if (l_catcount == 2) {
																	if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																		<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																	<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																		<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																	<% } %>
																<% } else if (l_catcount == 3) { 
																	if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT1) { %>
																		<i class="fa fa-star fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[0] %>"></i>
																	<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT2) { %>
																		<i class="fa fa-star-half-empty fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[1] %>"></i>
																	<% } else if (l_tabdata.getCaseFieldType() == SafetyDBCaseFieldCategoryType.CAT3) { %>
																		<i class="fa fa-star-o fa-lg custom-green" data-toggle="tooltip" data-placement="top" data-original-title="<%= l_categories[2] %>"></i>
																	<% } %>
																<% } else { %>
																<% } %>
																<label>
																	<%= l_tabdata.getCaseFieldName() %>
																</label>
															</td>
															<td>
																<% 	int	l_reviewstatus	=	l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus();
																	if (l_reviewstatus == 1) { %>
																		<label class="btn btn-sm custom-btn-green" data-toggle-class="btn-primary"
													 						data-toggle="tooltip" data-placement="left" data-original-title="Correct">
																			<i class="glyphicon glyphicon-ok"></i>
																		</label>
																<% } else if (l_reviewstatus == 2) { %>
																		<label class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary">
																			<i class="glyphicon glyphicon-remove"></i>
																		</label>
																<% } else if (l_reviewstatus == 3) { %>
																		<label class="btn btn-sm custom-btn-red" data-toggle-class="btn-primary" data-toggle="tooltip"
																			 data-placement="right" data-original-title="Incorrect">
																			<i class="glyphicon glyphicon-ban-circle"></i>
																		</label>
																<% } %> 
															</td>
															<%-- <td>
		                              							<%	if (l_reviewfielddata.get(l_tabdata.fieldName).isReviewCorrected()) { %>
		                              									<label class="btn btn-sm custom-btn-green">
																			<i class="glyphicon glyphicon-thumbs-up"></i>
																		</label>
		                              							<%  } else { %>
		                              									<label class="btn btn-sm custom-btn-grey">
																			<i class="glyphicon glyphicon-thumbs-down"></i>
																		</label>
		                              							<%  } %>
															</td> --%>
															
															<td>
																<% if (l_reviewstatus == 3) { %>
																	<%=l_errortypes.get(l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldErrorCode())%> 
																<% } %>
															</td>
															
															<td style="word-wrap:break-word;">
																<%= l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldComment() %>
															</td>
														</tr>
													<%  
														} 
													%>
												</tbody>
											</table>
											<% 	if (l_tabdetails == 1) {
											%>
												<a class="btn btn-primary btn-sm btnNext pull-right">Next</a>
												<% if (StringUtils.isNullOrEmpty(l_reporttype)) { %>
													<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
														Review Next Case
													</button>
												<% } else { %>
													<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
														Back
													</button>
												<% } %>
											<%	} else if (l_tabdetails == l_qrreviewdata.getSafetyDbCaseFieldList().keySet().size()) { %>
													<a class="btn btn-primary btn-sm btnPrevious pull-right">Previous</a>
													<% if (StringUtils.isNullOrEmpty(l_reporttype)) { %>
														<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
															Review Next Case
														</button>
													<% } else { %>
														<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
															Back
														</button>
													<% } %>
											<%	} else { %>
													<a class="btn btn-primary btn-sm btnNext pull-right">Next</a> 
													<a class="btn btn-primary btn-sm btnPrevious pull-right">Previous</a>
													<% if (StringUtils.isNullOrEmpty(l_reporttype)) { %>
														<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
															Review Next Case
														</button>
													<% } else { %>
														<button type="button" class="btn btn-sm btn-primary" onclick="fnback();" value="Back">
															Back
														</button>
													<% } %>
											<%	}
												l_tabdetails++;
											%>
										</div>
									<% 	}
									%>
								</div>
							</div> <%
						} %>	
					</div> 
				</div>
			</div>
		</csrf:form>	
	</body>
</html>