<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%List<ProjectInfo> projectList= (List<ProjectInfo>)request.getAttribute("projectsList");%>
</head>
<body>
<csrf:form name="projectManagementApp" id="projectManagement" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="PROJMNG-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="portal_action_mode" name="portal_action_mode" value="" />
	<input type="hidden" id="selectedProjectId" name="selectedProjectId" value= "-1" />
	
	<div id="mainContent">
		<%@include file="uiFormCreationMethods.jsp"%>
		
		<%
		String sAppId = null;
		IPortalApp oPortalApp = null;
		String sNewPortalAction = (String) request.getAttribute("new-portalaction");
		%>
		
		<%if (sNewPortalAction == null || sNewPortalAction.trim().length() == 0)
		{%>
			<%@include file="project_management_projectList.jsp" %>
		
		<%}else if (sNewPortalAction.equals("createProject")||sNewPortalAction.equals("viewProject")||sNewPortalAction.equals("editProject"))
		{%>
			<%@include file="project_mangement_form.jsp" %>
		<%}%>
    </div>		  
</csrf:form>
</body>
</html>