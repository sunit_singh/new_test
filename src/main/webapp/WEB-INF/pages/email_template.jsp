<%@page import="com.sciformix.sciportal.mail.MailTemplate"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDetailsDb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.io.*,java.util.*"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDb"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	List<MailTemplate> mailTemplateList = (List<MailTemplate>) request.getAttribute("mailTemplateList");
	String successmsg = (String) request.getAttribute("msg");
	int tempId=0;
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script>
			<%@include file="../js/validation.js" %>
		</script>
		<script type="text/javascript">
	
			function navigatePage(value, id)
			{
				var form = document.getElementById("PROJSETTINGS_form");
				document.getElementById("portal_action").value =value ;
				document.getElementById("projsettings_appid").value ='EMAIL-TEMP-APP' ;
				document.getElementById("click_Operation_Project_Id").value = id;
				if (validateFormParameters()) {
					form.submit();
				}
			}
			function validateFormParameters () {
				oFormObject = document.forms["PROJSETTINGS_form"];
				return validateInputParameter(oFormObject);
			}
		</script>
	</head>
<body>
<csrf:form name="PROJSETTINGS_form" id="PROJSETTINGS_form" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="click_Operation_Project_Id" name="click_Operation_Project_Id" value="">
	<input type="hidden" id="statusValue" name="statusValue" value="">
	<input type="hidden" id="projsettings_appid" name="projsettings_appid"  />
	
	<div id="mainContent">
		<%@include file="uiFormCreationMethods.jsp"%>
		<div class="page-title">
			<div class="title_left">
				<h3>Project Email Template Listing</h3>
			</div>
		</div>
		
		<div class ="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
	   			<div class="x_panel">
	   				<div class="x_title">
						<h3>
							<small>List of Templates</small> 
						</h3>
							<%if(successmsg!=null){  %>
								<%=successmsg %>
							<% }%>	
					</div>	
					<h2><small>View/Create/Modify Email Template</small></h2>
				
					<div name="List of all templates" id="Template List" class="x_content" >
						<div id="templateList">
						<table class="table table-striped" >
				  				<thead>
			                        <tr>
			                        	<th/>
			                          	<!-- <th>Project Id</th> -->
			                          	<th>Template Name (Seq Id)</th>
			                          	<th>Purpose</th>
			                          	<th>Status</th>
			                        </tr>
		                      	</thead>
	                      		<tbody>
	                      		<%if(mailTemplateList != null && !mailTemplateList.isEmpty()){
		                      			for(MailTemplate mailTemplate:mailTemplateList)
		                      			{ %>
	                      				<tr>
											<td>
											<a onclick="navigatePage('EditTemplateView', '<%=mailTemplate.getSequenceId()%>')">
											<i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="left" title="View/Modify Mail Template"></i>
											</a>
											</td>
											<%-- <td>
											<%= mailTemplate.getProjectId()%>
											</td> --%>
											<td>
											<%= mailTemplate.getTemplateName()%>  &nbsp; (<%= mailTemplate.getSequenceId()%>)
											</td>
											<td>
											<%=mailTemplate.getPurpose()%> 
											</td>
											<td>
											<%=mailTemplate.getState().displayName()%>
											</td>
										</tr>
									
									<%	}
	                      			}else
	                      			{%>
	                      				<tr>
	                      					<td colspan="4"> 
	                      						<i> &nbsp; No Records Found.</i>
	                      					</td>
	                      				</tr>
	                      			<%} %>
	                    		</tbody>
						</table>
						</div>
						<div class="ln_solid"></div>
                        	<div>
                        		<a class="" 
					                 data-target="#createTemp">
										<button type="button" class="btn btn-primary" onclick="navigatePage('createView', '-1')">Create New</button>
								</a>
                        	</div> 
                    </div><!-- x-content End -->
					
				</div><!--x panel end  -->
			</div>	
		</div><!-- raw End-->
	</div> <!-- Outer Div -->
</csrf:form>
</body>
</html>