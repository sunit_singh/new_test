<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.ngv.CaseDataValidationRule"%>
<%@page import="com.sciformix.sciportal.apps.ngv.CaseDataValidationRuleset"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>

<script type="text/javascript">
<%@ page trimDirectiveWhitespaces="true" %>
function hideOrShowTemplateContent(templateid)
{
	if (templateid == '00')
	{
		var divTemplateContent = document.getElementById("div_templatecontent_" + templateid);
		
		if (divTemplateContent.style.display == "none")
		{
			divTemplateContent.style.display = "block";
		}
		else
		{
			divTemplateContent.style.display = "none";
		}
	}
	else
	{
	//	var divTemplateContent = document.getElementById("div_templatecontent_" + templateid);
		var divrowTemplateContent = document.getElementById("divrow_templatecontent_" + templateid);
		
		if (divrowTemplateContent.style.display == "none")
		{
			//divTemplateContent.style.display = "block";
			divrowTemplateContent.style.display = "block";
		}
		else
		{
			//divTemplateContent.style.display = "none";
			divrowTemplateContent.style.display = "none";
		}
	}
}
function validateFormParameters () {
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}
function submitForm(value, mode)
{
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = 'NGV_Portal_Validation_App';
	document.getElementById("clickValue").value =value;
	document.getElementById("portal_action_mode").value = mode;
	
	if (validateFormParameters())
	 {
		form.submit();
	}
}

function changeRulesetState(sNewState)
{
	document.getElementById("ngv_tplrs_state").value = sNewState;
	
	submitForm("ngv_validateruleset_changestate", "");
}
function setAsDefault(nNewDefaultTemplateId)
{
	document.getElementById("ngv_selectedtemplateid").value = nNewDefaultTemplateId;
	
	submitForm("ngv_validate_setdefaultrule", "");
}
function editTemplate(nTemplateIdToEdit)
{
	document.getElementById("ngv_selectedtemplateid").value = nTemplateIdToEdit;
	submitForm("ngv_validate_editrule", "edit");
}
function viewTemplate(nTemplateIdToEdit, sReadOnly)
{
	document.getElementById("ngv_selectedtemplateid").value = nTemplateIdToEdit;
	document.getElementById("ngv_editTemplateFlag").value = sReadOnly;
	submitForm("ngv_validate_editrule", "view");
}
</script>
<%
final int MAX_CONTENT_SIZE = 60;

CaseDataValidationRuleset oValidationRuleset = null;
List<CaseDataValidationRule> listTemplates = null;
int nTemplateRulesetId = -1;
String sTemplateRulesetName = null;
String sTemplateRulesetState = null;
boolean bActivateRulset = false;
boolean bDeactivateRulset = false;
String sTemplateHeader = null;
String sTemplateHeaderPreview = null;
int nDefaultTemplateId = -1;
boolean bUserHasEditPrivilege = false;
boolean bObjectCanBeEdited = false;
String sNonEditMessage = null;
boolean bEditAllowed = false;

bUserHasEditPrivilege = true;//Currently there are no privileges separate for this

oValidationRuleset = (CaseDataValidationRuleset) WebUtils.getSessionAttribute(request, "ngv_validate_ruleset");

if (oValidationRuleset != null)
{
	nTemplateRulesetId = oValidationRuleset.getSequenceId();
	sTemplateRulesetName = oValidationRuleset.getTemplateName();
	sTemplateRulesetState = oValidationRuleset.getState().displayName();
	
	bActivateRulset = (oValidationRuleset.getState() == com.sciformix.commons.SciEnums.RecordState.UNDER_CONFIGURATION);
	bDeactivateRulset = (oValidationRuleset.getState() == com.sciformix.commons.SciEnums.RecordState.ACTIVE);
	bObjectCanBeEdited = (oValidationRuleset.getState() == com.sciformix.commons.SciEnums.RecordState.UNDER_CONFIGURATION);
}
else
{
	sTemplateRulesetName = "";
	sTemplateRulesetState = "";
}

bEditAllowed = bObjectCanBeEdited && bUserHasEditPrivilege;
if (!bObjectCanBeEdited && bUserHasEditPrivilege)
{
	sNonEditMessage = "Active Template Rulesets cannot be modified";
}
else if (bObjectCanBeEdited && !bUserHasEditPrivilege)
{
	sNonEditMessage = "No privileges to edit";
}
else
{
	sNonEditMessage = "";
}

listTemplates = (List<CaseDataValidationRule>) request.getAttribute("ngv_list_of_Rules");
%>
	<input type="hidden" id="ngv_tplrulesetid" name="ngv_tplrulesetid" value="<%=nTemplateRulesetId%>" />
	<input type="hidden" id="ngv_tplrs_state" name="ngv_tplrs_state" value="" />
	<input type="hidden" id="ngv_selectedtemplateid" name="ngv_selectedtemplateid" value="" />
	<input type="hidden" id="ngv_editTemplateFlag" name="ngv_editTemplateFlag" value="" />
	
	<div class="center_col" role="main">
    	<div class="">
          	<%=displayFormHeader("Case Validation Ruleset") %>
            
            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip("View/modify your Case Validation Ruleset", false, false) %>
                  
                  <div class="x_content">
                  	<div class="form-group">
	                  	<% if (bActivateRulset) {%><A HREF="javascript:changeRulesetState('ACTIVE');"><i class="glyphicon glyphicon-ok-sign"></i> Activate</A>&nbsp; <%} %>
	                  	<% if (bDeactivateRulset) {%><A HREF="javascript:changeRulesetState('DEACTIVATED');"><i class="glyphicon glyphicon-remove-sign"></i> De-Activate</A>&nbsp;<%} %>
	                  	
					</div>
					<%=displayLabelPair("Case Validation Ruleset Name", false, sTemplateRulesetName)%>
                  	
                  	<%=displayLabelPair("Case Validation Ruleset State", false, sTemplateRulesetState)%>
					
					<p></p>
                  	<h2><small>List of Rules</small></h2>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Rule Name</th>
                          <th>Rule Type</th>
                          <th>Criteria</th>
                          <th>Applicability</th>
                        </tr>
                      </thead>
                      <tbody>
						<%
						if (listTemplates != null && listTemplates.size() > 0)
						{
							String sCriteria = null;
							String sApplicability = null;
							
							for (CaseDataValidationRule oRule : listTemplates)
							{
							
								sCriteria = (oRule.getCriteria() != null) ? oRule.getCriteria() : "(not specified)";
								
								sApplicability = (oRule.getApplicability() != null) ? oRule.getApplicability() : "(not specified)";
								
								%>
								<tr>
		                          <th valign="top"><span style=" white-space: nowrap;">
	                          		<%-- <%
	                          			if (nDefaultTemplateId == oRule.getRuleSeqId())
										{ 
											%><a href="javascript:;" data-toggle="tooltip" data-placement="left" title="Already default template"><i class="glyphicon glyphicon-certificate"></i></a><%
										} else if ((nDefaultTemplateId != oRule.getRuleSeqId()) && (bEditAllowed)) { 
											%><a href="javascript:setAsDefault('<%=oRule.getRuleSeqId()%>')" data-toggle="tooltip" data-placement="left" title="Set as default template"><i class="glyphicon glyphicon-star-empty"></i></a><%
										}
									%>&nbsp; --%>
									<%-- <%if(!sTemplateRulesetState.equals("Deactivated") && !sTemplateRulesetState.equals("Active")){ %>
									<a href="javascript:viewTemplate('<%=oTemplate.getTemplateSeqId()%>', 'readOnly');" data-toggle="tooltip" data-placement="left" title="Preview contents"><i class="glyphicon glyphicon-eye-open"></i></a>
									<%}else{ %> --%>
									<a href="javascript:viewTemplate('<%=oRule.getRuleSeqId()%>', 'readOnly');" data-toggle="tooltip" data-placement="left" title="Preview contents"><i class="glyphicon glyphicon-eye-open"></i></a>
									<%-- <%} %> --%>
									<%if (bEditAllowed) {%>
									&nbsp;<A HREF="javascript:editTemplate('<%=oRule.getRuleSeqId()%>');" data-toggle="tooltip" data-placement="left" title="View/edit template"><i class="fa fa-pencil"></i></A>
									<%} %>
		                          </span></th>
		                          <td valign="top"><span style=" white-space: nowrap;"><%=oRule.getRuleName()%></span></td>
		                          <td valign="top"><span style=" white-space: nowrap;"><%=oRule.getRuleType()%></span></td>
		                         <td style="font-family:Courier New" valign="top"><span style="word-wrap:break-word;"><%=sCriteria%></span></td> 
		                         <td style="font-family:Courier New" valign="top"><span style="word-wrap:break-word;"><%=sApplicability%></span></td>
		                          		                        </tr>
								<%
							}
						}//end-if listTemplates
						else
						{
							%><TR><TD colspan="5">&nbsp;<i>No rules</i></TD><%
						}//end-else listTemplates
						%>
					  </tbody>
					</table>
					
					<% if (bEditAllowed) {%>
                  		<button type="button" class="btn btn-primary" onclick="javascript:submitForm('ngv_valrs_valviewedit_sb', 'create');">Create New</button>
                  	<% } %>
					
                  </div> <!-- end-div x_content -->
                  
                </div><!-- end-div panel -->
              </div><!-- end-div col-md-12 col-sm-12 col-xs-12 -->
            </div><!-- end-div row -->
            
		</div><!-- end-div "" -->
	</div><!-- end-div center_col -->


