<%@page import="com.sciformix.commons.ObjectIdPair"%>
<%@page import="com.sciformix.commons.SciConstants.StringConstants"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.commons.SciServiceException"%>
<%@page import="com.sciformix.sciportal.project.ProjectHome"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@page import="com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHome"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf"%>
<%@ page import="java.util.List"%>
<%@ page import="java.io.*,java.util.*"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<script src="js/jquery.js"></script>
<script>
	<%@include file="../js/usermanagement.js" %>
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	
	String message = null;
	String sarrOptionValuesUserFilter = null;
	String sarrOptionValuesLookbackDurationFilter = null;
	List<String> listUserIds = null;
	String sSelectedUserId = null;
	int iUserId = 0;
	UserInfo userInfo = new UserInfo();
	
	
	message = (String)request.getAttribute("message");
	List<UserInfo> userList = null;
	if (request.getAttribute("userList") != null) {
		userList = (List<UserInfo>) request.getAttribute("userList");

	}
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Management</title>

<script type="text/JavaScript">
	function validateFormParameters() {
		clearErrorMessages();
		oFormObject = document.forms["userfrmmain"];
		return validateInputParameter(oFormObject);
	}
	function formSubmit(p_redirect) {
		
		clearErrorMessages();
		if (p_redirect == 1) {
			document.getElementById("portal_action").value = "search";
			document.getElementById("userfetchdivcondition").style.display = 'none';
		} else if (p_redirect == 2) {
			document.getElementById("portal_action").value = "create";
		} else if (p_redirect == 3) {
			document.getElementById("portal_action").value = "changeCriteria";
		} else {
			return false;
		}
		
		if(validateFormParameters()){ 
			document.userfrmmain.submit();
		}else{
			return false;
		} 
	}

	function fetchtypeselection(p_value) {
		if (p_value == 1) {
			$("[id=userfetchcriteria]").hide();
		} else if (p_value == 2) {
			$("[id=userfetchcriteria]").show();
		} else {

		}
	}

	$(document).ready(function() {
		var handleDataTableButtons = function() {
			if ($("#datatable-buttons").length) {
				$("#datatable-buttons").DataTable({
					dom : "Bfrtip",
					buttons : [ {
						extend : "copy",
						className : "btn-sm"
					}, {
						extend : "csv",
						className : "btn-sm"
					}, {
						extend : "excel",
						className : "btn-sm"
					}, {
						extend : "pdfHtml5",
						className : "btn-sm"
					}, {
						extend : "print",
						className : "btn-sm"
					}, ],
					responsive : true
				});
			}
		};

		TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons();
				}
			};
		}();

		$('#datatable').dataTable();

		$('#datatable-keytable').DataTable({
			keys : true
		});

		$('#datatable-responsive').DataTable();

		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});

		$('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});

		var $datatable = $('#datatable-checkbox');

		$datatable.dataTable({
			'order' : [ [ 1, 'asc' ] ],
			'columnDefs' : [ {
				orderable : false,
				targets : [ 0 ]
			} ]
		});
		$datatable.on('draw.dt', function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_flat-green'
			});
		});

		TableManageButtons.init();
	});

	// shorting table

	function sortTable(n) {
		var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
		table = document.getElementById("datatable");
		switching = true;

		dir = "asc";

		while (switching) {

			switching = false;
			rows = table.getElementsByTagName("TR");

			for (i = 1; i < (rows.length - 1); i++) {

				shouldSwitch = false;

				x = rows[i].getElementsByTagName("TD")[n];
				y = rows[i + 1].getElementsByTagName("TD")[n];

				if (dir == "asc") {
					if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
						shouldSwitch = true;
						break;
					}
				} else if (dir == "desc") {
					if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
						shouldSwitch = true;
						break;
					}
				}
			}
			if (shouldSwitch) {

				rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
				switching = true;

				switchcount++;
			} else {

				if (switchcount == 0 && dir == "asc") {
					dir = "desc";
					switching = true;
				}
			}
		}
	}
</script>
<style>
.viewDetail {
	margin-bottom: 20px;
	margin-left: 100px;
}
</style>
</head>

<body>



	<csrf:form name="userfrmmain" id="userfrmmain" class="form-horizontal"
		method="post" action="LandingServlet">
		<input type="hidden" id="portal_appid" name="portal_appid"
			value="USRMGMNT-PORTAL-APP">
		<input type="hidden" id="portal_action" name="portal_action" value="">
		<input type="hidden" id="select_id" name="select_id" value="">


		<div class="title_left">
			<h3>User Management</h3>
		</div>

	
	
	<%
					if (message != null && message != "") {
				%>
				<div class="row">
					<div class="col-sm-12" style="align: center;">
						<h4
							style="padding: 20px; background-color: #e6ffe6; color: #45a945; border-radius: 10px; padding-left: 200px;"><%= message %></h4>
					</div>
				</div>
				<%
					}
				%>

	<div class="container">

		<div class="row">


			<table id="datatable" class="table table-striped">


				<thead>
					<tr>
						<th></th>

						<th onclick="sortTable(0)">User Id (Seq Id)</th>

						<th>User Name</th>						
						<th>User Roles</th>
						<th>User Projects</th>

					</tr>
				</thead>
				<tbody>
					<%
						if (userList.size() > 0) {
								for (UserInfo user : userList) 
								{
									List<ObjectIdPair<Integer,String>> listUserProjects = null;
									String sSerializedUserProjectNames = null;
									
									listUserProjects = ProjectUserManagementHome.getUserListOfProjects(user.getUserSeqId());
									sSerializedUserProjectNames = StringUtils.serializeNames(listUserProjects, StringConstants.COMMA_WITH_SPACE);
									%>
									<tr>
										<td>
												<i class="fa fa-pencil" style="padding-right: 5px;" onclick="editUserDetails('<%=user.getUserId()%>','edit')"></i>
												<i class="glyphicon glyphicon-eye-open" style="padding-right: 5px;" onclick="editUserDetails('<%=user.getUserId()%>','view')"></i>
										</td>
										<td><%=user.getUserId()%> (<%=user.getUserSeqId()%>)</td>
										<td><%=user.getUserDisplayName()%></td>
										<td><%=user.getUserRoleNames()%></td>
										<td><%=sSerializedUserProjectNames%></td>
									</tr>
									<%
									}
							}
					%>
				</tbody>
			</table>

			<div class="row">
				<div class="col-sm-12">
					<div class="pull-left col-sm-4" style="display: block;">
						<button type="button" class="btn btn-success btn-sm" value=""
							onclick="formSubmit(2);">
							Create <span aria-hidden="true"></span>
						</button>
					</div>
				</div>
			</div>

		</div>
		</div>
</csrf:form> 
</body>
</html>