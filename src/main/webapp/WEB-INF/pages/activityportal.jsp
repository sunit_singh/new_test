<%@page import="com.sciformix.sciportal.api.activity.ActivityDetailsInfoDb"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.project.ProjectHome"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.api.activity.ActivityInfoDb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%@ page import="java.util.Arrays" %>    
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>    
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.user.UserHome"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDb"%>
<%@page import="com.sciformix.sciportal.project.ProjectHomeImpl"%>


<%
final String DUMMY_USER_RECORD_FOR_SELECTION = "Select a user";

UserInfo userInfo = null;
String[] sarrOptionValuesLookbackDurationFilter = null;
List<ProjectInfo> allProjectList = null;
UserSession userSession = null;
String p_name = null;
String[] sarrOptionValuesUserFilter = null;
List<String> listUserIds = null;
String sSelectedUserId = "";
List<ActivityInfoDb> listActivityTrail = null;
ActivityDetailsInfoDb oActivityDetailsInfoDb = null;
StringBuffer oActivityDetails  =	null;
StringBuffer oActivityPayload	=	null;
String[] sarrOptionValuesUserFilterall = null;

userInfo =	WebUtils.getUserSession(request).getUserInfo();
allProjectList = ProjectHome.getAllProjects();
sarrOptionValuesLookbackDurationFilter = new String[] {"Last 7 days"};

userSession = WebUtils.getUserSession(request);
p_name = (String)request.getAttribute("selectedProject");
listUserIds = (List<String>)request.getAttribute("list_of_users");
sSelectedUserId = StringUtils.emptyString((String)request.getAttribute("select_criteria_user"));

listActivityTrail = (List<ActivityInfoDb>) request.getAttribute("s_activity");

if (listUserIds != null)
{
	sarrOptionValuesUserFilter = new String[listUserIds.size() + 1];
	sarrOptionValuesUserFilter[0] = DUMMY_USER_RECORD_FOR_SELECTION;
	
	for (int nCounter = 0; nCounter < listUserIds.size(); nCounter++)
	{
		sarrOptionValuesUserFilter[nCounter + 1] = listUserIds.get(nCounter);
	}
	
}
else
{
	sarrOptionValuesUserFilter = new String[] {DUMMY_USER_RECORD_FOR_SELECTION};
}

Arrays.sort(sarrOptionValuesUserFilter);

List<ObjectIdPair<Integer,String>> l_listauthorizedprojectids 	=	(userSession != null ? userSession.getAuthorizedProjectIds() : null);



%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">

function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["frmactivity"];
	return validateInputParameter(oFormObject);
}

function submitActivityFilterForm()
{
	clearErrorMessages();
	var form = document.getElementById("frmactivity");
	var sSelectedUser = document.getElementById("select_criteria_user").value;
	
	if (document.frmactivity.auditusertype.value == "all") {
		var sSelectedUser = document.getElementById("select_criteria_user").value;
		if (sSelectedUser == "<%=DUMMY_USER_RECORD_FOR_SELECTION%>")
		{
			displayErrorMessage("UI-1015");
			return false;
		}
	}
	document.getElementById("portal_action").value = "fetch_results";
	if (validateFormParameters())
	 {
		form.submit();
	 }
}

function usertypeselection (p_value) {
	if (p_value == 1) {
		$("[id=auditusertypesystem]").hide();
		$("[id=auditusertypeapplication]").show();
		$("[id=selectProjectDropDown]").show();	
	} else if (p_value == 2) {
		$("[id=auditusertypesystem]").show();
		$("[id=auditusertypeapplication]").hide();
		$("[id=selectProjectDropDown]").hide();	
			<%
				List<String> l_alluserlist = UserHome.getListOfUserIds();
				
				if (l_alluserlist != null)
				{
					l_alluserlist.remove(UserHome.retrieveBootstrapUser().getUserId());
					sarrOptionValuesUserFilterall = new String[l_alluserlist.size() + 1];
					sarrOptionValuesUserFilterall[0] = DUMMY_USER_RECORD_FOR_SELECTION;
					
					for (int nCounter = 0; nCounter < l_alluserlist.size(); nCounter++)
					{
						sarrOptionValuesUserFilterall[nCounter + 1] = l_alluserlist.get(nCounter);
					
					}
					
				}
				else
				{
					sarrOptionValuesUserFilterall = new String[] {DUMMY_USER_RECORD_FOR_SELECTION};
				}
			%>
	} else {
		
	}
}

function setdefaultdata () {
	var	l_auditusertype	=	'<%= request.getAttribute("auditusertype") %>';
	if (l_auditusertype == "all") {
		document.getElementById("auditusertypeall").checked = true;
		usertypeselection('1');
	} else if (l_auditusertype == "system")  {
		document.getElementById("auditusertypesys").checked = true;
		usertypeselection('2');
	}
}

function fnfilteruser(projectName) {
	var formName = document.getElementById("frmactivity");
	document.getElementById("portal_action").value = "getuserList";
	document.getElementById("project_name").value = projectName;
	document.getElementById("portal_appid").value = 'ACTIVITY-PORTAL-APP';
	
	if(validateFormParameters()){ 
		formName.submit();
	} 
	
}


</script>

<title>Insert title here</title>
</head>
	<body  onload="setdefaultdata();">
		<csrf:form name="frmactivity" id="frmactivity" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
		<input type="hidden" id="portal_appid" name="portal_appid" value="ACTIVITY-PORTAL-APP" >
		<input type="hidden" id="portal_action" name="portal_action" value="">
		<input type="hidden" id="project_name" name="project_name" value="">
		<div>
			<%@include file="uiFormCreationMethods.jsp"%>
			<div class="page-title">
				<div class="title_left">
					<h3>Activity Trail Viewer</h3>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class ="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
		   			<div class="x_panel">
		   				<div class="x_title">
							<h2>
							 	Activity Trail
							</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
				 		<div class="x_content">	          			
		          					 <%if(userInfo.isAuditor() || userInfo.isProjectAuditor()) {
		          				 	 %>	
				          			<div id="audituserprojecttypeapplication" >
			    						<div class="form-group">
			    						<div class="form-group" id="selectProjectDropDown">
												<label class="control-label col-md-3 col-sm-3 col-xs-12 ">Projects *</label>    						
		    								<div class="col-md-4 col-sm-4 col-xs-12">
												
												<select class="form-control" id="select_criteria_project" name="select_criteria_project" onchange="fnfilteruser(this.value)">
													<%if (userInfo.isAuditor())
													{%>
														<option value="">Project Neutral</option>
														<% for (ProjectInfo oProjectInfo: allProjectList)
														{ %>
															
															<%if(oProjectInfo !=  null)
															{
																if(!StringUtils.isNullOrEmpty(p_name)){
																	if(oProjectInfo.getProjectSequenceId()==Integer.parseInt(p_name)){
																		%>
																	<option value="<%=oProjectInfo.getProjectSequenceId() %>" selected="selected"><%=oProjectInfo.getProjectName()%></option>
																	<%}else{%>
																	<option value="<%=oProjectInfo.getProjectSequenceId() %>"><%=oProjectInfo.getProjectName()%></option>
																		<%}%>
																	<%}else{%>
																	<option value="<%=oProjectInfo.getProjectSequenceId() %>"><%=oProjectInfo.getProjectName()%></option>
																	<%}%>
															<%}else {%>
																	<option value="<%=oProjectInfo.getProjectSequenceId() %>"><%=oProjectInfo.getProjectName()%></option>
																<%}%>
															<%}%>
													<%}else{ %>	
													
													<% for (ObjectIdPair<Integer,String> l_authorizedprojects : l_listauthorizedprojectids) 
														{
														if(!StringUtils.isNullOrEmpty(p_name))
		                  								 {%>
															<% if(l_authorizedprojects.getObjectSeqId()==Integer.parseInt(p_name))
		                  										{%>
		                  											<option value="<%=l_authorizedprojects.getObjectSeqId() %>" selected="selected"><%= l_authorizedprojects.getObjectName() %></option>
		                  										<%}else{ %>
		                  											<option value="<%=l_authorizedprojects.getObjectSeqId() %>"><%= l_authorizedprojects.getObjectName() %></option>
		                  									<%} %>
														<%}else{ %>
																	<option value="<%=l_authorizedprojects.getObjectSeqId() %>"><%= l_authorizedprojects.getObjectName() %></option>
														<%}
														}%>	
													
														
														<%} %>
												</select>
													
											</div>
			    						</div>
			    					</div>	   					   				
	    						<%} %>
	                       
		                      <div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
		                  		<div class="radio col-md-4 col-sm-4 col-xs-12">
		                           <label><input type="radio"  name="auditusertype"  id="auditusertypeall" value="all" onclick="return usertypeselection(1);"  checked /> User</label>
		                         </div>
		                   	 </div>
		                    
		                    <%if((userInfo.isAuditor())){ %>		
		                    
		                    <div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
		                        <div class="radio col-md-4 col-sm-4 col-xs-12">
		                           <label><input type="radio"  name="auditusertype" id="auditusertypesys" value="system" onclick="return usertypeselection(2);" /> System</label>
		                         </div>
	                       </div>
	                       
	                      	 <% } %>
	                       
		                    <div id="auditusertypeapplication" >
	    						<div class="form-group">
   									<%=displaySelectBox("select_criteria_user", "Filter User", true, sarrOptionValuesUserFilter, StringUtils.emptyString(sSelectedUserId))%>
   									<%=displaySelectBox("select_criteria_timeduration", "Lookback Duration", true, sarrOptionValuesLookbackDurationFilter, "")%>
   								</div>
	    					</div>	
		                    
	                       <div id="auditusertypesystem" style="display:none">
	    						<div class="form-group">
	    							<%=displaySelectBox("select_criteria_user_system", "Filter User", true, sarrOptionValuesUserFilterall, StringUtils.emptyString(sSelectedUserId))%>
	    							<%=displaySelectBox("select_criteria_timeduration", "Lookback Duration", true, sarrOptionValuesLookbackDurationFilter, "")%>
	    						</div>
	    					</div>
		          			
		          			<%if(userInfo.isAuditor() || (userInfo.isProjectAuditor() && l_listauthorizedprojectids.size()>0)){ %>
					  		<div class="form-group">
					    		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					      			<button type="button" class="btn btn-success" onclick="return submitActivityFilterForm();">Submit</button>
					    		</div>
					  		</div>
					  		<%} %>
		          			<% if (listActivityTrail != null) { %>
					  		<div class="ln_solid"></div>
		                    <table class="table table-striped" >
					  			<%-- <SMALL><%=listActivityTrail.size()%> records found</SMALL> --%>
					  				<thead>
				                        <tr>
				                        	<th/>
				                          	<th>When<BR/><SMALL><SMALL>Action date-time</SMALL></SMALL></th>
				                          	<th>Who<BR/><SMALL><SMALL>Userid | User Sequence Id</SMALL></SMALL></th>
				                          	<th>Where<BR/><SMALL><SMALL>Activity App</SMALL></SMALL></th>
				                          	<th>What<BR/><SMALL><SMALL>Activity Type | Activity Code</SMALL></SMALL></th>
				                        </tr>
			                      	</thead>
			                      	
			                      	<tbody><%
		                      			if (listActivityTrail.size() > 0)
										{
											for (ActivityInfoDb oActivityInfoObj : listActivityTrail)
											{ %>
												
												<tr>
													<td>
							                        	<a id="<%=oActivityInfoObj.getActivitySeqId()%>" class="" data-toggle="modal" 
						                          			data-target="#myModal<%=oActivityInfoObj.getActivitySeqId()%>" >
						                          			<i class="glyphicon glyphicon-eye-open"></i>
						                          		</a>
						                          		
						                          		<!-- Model display start  -->
						                          		
						                          		<div id="myModal<%=oActivityInfoObj.getActivitySeqId()%>" class="modal fade" role="dialog">
														<div class="modal-dialog modal-lg">
													  		<div class="modal-content">
																<div class="modal-header">
																  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span>
																  </button>
																  <h2 id="myModalLabel2">Activity Information</h2>
																</div>
																<div class="modal-body">
																	<div class="x_panel">
																		<div class="x_title">
																			<h2>Activity Detail</h2>
																			<div class="clearfix"></div>
																		</div>
																		<div class="x_content">
																			<table class='table table-striped'>
																				<tr>
																					<td>
																						<label>Activity Seq Id</label>
																					</td>
																					<td>
																						<%=oActivityInfoObj.getActivitySeqId()%>
																					</td>
																					<td>
																						<label>Timestamp</label>
																					</td>
																					<td>
																						<%=WebUtils.formatForDisplayDate(oActivityInfoObj.getActivityDateTime())%>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<label>Activity Code</label>
																					</td>
																					<td>
																							<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityCode(), "N/A") %>																			
																					</td>
																					<td>
																							<label>Activity Type</label>					
																					</td>
																					<td>
																							<%= oActivityInfoObj.getActivityType() %>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<label>Project Id/Name</label>
																					</td>
																					<td>
																							<%=oActivityInfoObj.getProjectSeqId()%> / <% if(oActivityInfoObj.getProjectSeqId()!=-1)
																																			{%>
																								<%=ProjectHomeImpl.getProjectGivenProjectId(oActivityInfoObj.getProjectSeqId()).getProjectName()%>
																																		<% } %>
																					</td>
																					<td>
																							<label>User Seq Id/Name</label>
																					</td>
																					<td>
																						<%= oActivityInfoObj.getActivityUserSeqId() %> / <%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityUserId(), "N/A")%>
																					</td>
																				</tr>
																				<tr>
																					<td>
																							<label>Server </label>
																					</td>
																					<td>
																							<%=oActivityInfoObj.getActivityServer()%>
																					</td>
																					<td>
																						<label>Application Name</label>
																					</td>
																					<td>
																							<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityApp(), "None")%>
																					</td>
																				</tr>
																				<tr>
																					<td>
																							<label>Client Timestamp</label>
																					</td>
																					<td>
																							<%=WebUtils.formatForDisplayDate(oActivityInfoObj.getActivityClientDateTime())%>
																					</td>
																					<td>
																							<label>Client Machine Domain</label>
																					</td>
																					<td>
																							<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityClientHostDomain(), "N/A") %>	
																					</td>
																				</tr>
																				<tr>
																					<td>
																							<label>Client IP/Host</label>
																					</td>
																					<td>
																						<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityClientIp(), "N/A") %> / 
																						<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityClientHost(), "N/A")%>
																					</td>
																					<td>
																							<label>Machine Logged-in User</label>
																					</td>
																					<td>
																							<%=WebUtils.formatForDisplay(oActivityInfoObj.getMachineLoginUser(), "N/A") %>
																					</td>
																				</tr>
																			</table>
																		</div>
																	</div>
																	<!-- System Payload start -->
																	
																	<div class="x_panel">
																		<div class="x_title">
																			<h2>Payload</h2>
																			<div class="clearfix"></div>
																		</div>
																		<div class="x_content">
																			<table class='table table-striped'>
																				<thead>
																					<tr>
																						<th>
																							Field Name
																						</th>	
																						<th>
																							Field Value
																						</th>
																					</tr>
																				</thead>
																				<tbody>
																					<%
																					oActivityPayload	=	new StringBuffer(WebUtils.formatForDisplay(oActivityInfoObj.getSystemPayload()));
														                        		if (oActivityPayload != null && oActivityPayload.length() != 0) {
														                        			oActivityPayload.deleteCharAt(0);
														                        			oActivityPayload.deleteCharAt(oActivityPayload.length()-1);
														                        			for (String payload : oActivityPayload.toString().split(",")) {
														                        				payload	=	payload.replaceAll("(\\{|\\}|\")+", "");	%>
														                        				<tr>
														                        					<% 	for (String payloadData : payload.split(":")) { %>
														                        						<td style="word-wrap:break-word;">
																											<%=payloadData %>
																										</td>
														                        					<% } %>
																								</tr>
														                        		  <%}
														                        		} else { %>
														                        			<tr>
														                        				<td>
														                        					None
														                        				</td>
														                        				<td>
														                        					None
														                        				</td>
														                        			</tr> <% 
														                        		}
														                        	%>
														                        </tbody>	
																			</table>
																		</div>
																	</div>
																	
																	<!-- System Payload ends -->
																	
																	<%	if (oActivityInfoObj.getDetails() != null && oActivityInfoObj.getDetails().size() > 0) { %>			
																		<div class="x_panel">
																			<div class="x_title">
																				<h2>Activity Details</h2>
																				<div class="clearfix"></div>
																			</div>
																			<div class="x_content">
																				<table class='table table-bordered table-striped' style="table-layout:fixed;">
																					<thead>
																						<tr class="headings">
																							<th class="column-title">Time</th>
																							<th class="column-title">Step</th>
																							<th class="column-title">Details</th>
																						</tr>
																					</thead>
																					<tbody>
															                    		<%
															                          		for (int nCounter = 0; nCounter < oActivityInfoObj.getDetails().size(); nCounter++) {
															                          			oActivityDetailsInfoDb = oActivityInfoObj.getDetails().get(nCounter); 
															                          			
															                          			if (nCounter > 0) {
																                          			%><%
																                          		}
																	                          	%>
																	                          	<tr>
																							  		<td>
																							  			<%=WebUtils.formatForDisplay(oActivityDetailsInfoDb.getActivityDateTime())%><BR/><SMALL><%=WebUtils.formatForDisplayDuration(oActivityDetailsInfoDb.getActivityDateTime())%></SMALL>
																							  		</td>
																							  		<td>
																							  			<%=oActivityDetailsInfoDb.getActivityStepCode()%>
																							  		</td>
																							  		<td>
																							  		<%	oActivityDetails	=	new StringBuffer(WebUtils.formatForDisplay(oActivityDetailsInfoDb.getActivityPayload()));
																																					  			
																				                        		if (oActivityDetails != null && oActivityDetails.length() != 0) {
																				                        			for (String s : oActivityDetails.toString().split(",")) {
																				                        				s	=	s.replaceAll("(\\{|\\}|\")+", "");
																				                        				s   =   s.replace("\\r\\n","");
																				                        				s	=	s.replace("\\","");
																				                        				s   =   s.replace("ActivityPayload :","");%>
																				                        				<%=s %>
																				                        				<br/>
																				                        		  <%}
																				                        		}
																				                        	%>	
																				                        	
																							  		</td>
																							  	</tr>
																	                          	
																                          		<%
															                          		}
															                        	%>
															                        </tbody>	
								                        						</table>
								                        					</div>
								                        				</div>	
								                        			<%	} else {
										                          		}
										                          	%>		
																</div>
																<div class="modal-footer">
																  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																</div>
															</div>
													  	</div>
													</div>
						                          		
						                          		<!-- Model display ends -->
						                          		
						                          	</td>
							                        <td>
							                        	<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityDateTime())%><BR/><SMALL><%=WebUtils.formatForDisplayDuration(oActivityInfoObj.getActivityDateTime())%></SMALL>
							                        </td>
							                        <td>
							                        	<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityUserId(), "n/a")%><BR/><SMALL><%="Id:" + oActivityInfoObj.getActivityUserSeqId()%></SMALL>
							                        </td>
							                        <td>
							                        	<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityApp(), "n/a") %><BR/>
							                        </td>
							                        <td>
							                          	<%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityType(), "n/a")%><BR/><SMALL><%=WebUtils.formatForDisplay(oActivityInfoObj.getActivityCode(), "(none)")%></SMALL></td>
							                     
												</tr> <%
											}
										} else {	%>
											<TR><TD colspan="7">&nbsp;<i>No records found</i></TD><%
										} %>
		                    		</tbody>
								</TABLE>
							<%}%>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</csrf:form>
	
	</body>
</html>