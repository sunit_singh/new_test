<%@page import="com.sciformix.commons.ObjectIdPair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDBCaseFieldCategoryType"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRErrorDashboardData"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%
	QRErrorDashboardData[]				l_errordbdata		=	(QRErrorDashboardData[]) request.getAttribute("errordbdata");
	Map<String,String>					l_errortypes		= 	(Map<String,String>) request.getAttribute("errortypes");
	String []							l_categories		= 	null;
	String								l_cataliases		=	(String) 	request.getAttribute("categoryaliases");	
	int									l_catcount			=	0;
	List<ObjectIdPair<String,String>>	l_reviewtypelist	=	(List<ObjectIdPair<String,String>>) request.getAttribute("reviewtypelist");
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
	
%>
<!DOCTYPE html>
<html>
	<head>
		<script>
	      $(document).ready(function() {
	    	  <% if (l_errordbdata != null && l_errordbdata.length > 0) { 
				for (int l_i=0; l_i < l_errordbdata.length; l_i++) { %>
					<% if (l_i == 0) { %>
						Morris.Donut({
				        	element: 'donut_daily',
				        	data: [
				        	<% 	if (l_errordbdata[l_i].getErrorFieldsCountMap() != null && l_errordbdata[l_i].getErrorFieldsCountMap().size() > 0) {
				        			int l_count = 0;
				        			for (String l_fieldname : l_errordbdata[l_i].getErrorFieldsCountMap().keySet()) { 
				        				if (l_count < 5) { %>
				        					{label: '<%=l_fieldname%>', value: <%= l_errordbdata[l_i].getErrorFieldsCountMap().get(l_fieldname) %>},
				        			 <% } 
				        			 l_count++;
				        		  	} %>
				        		  	],
				        		  	colors: ['#8E44AD', '#1ABC9C', '#58D68D', '#cc3300', '#3498DB'],
				        	<% } else { %>
				        		{label: 'Fields in Error', value: 0},
				        		],
				        		colors: ['#00cc00'],
				        	<% } %>
				              
				              formatter: function (y) {
				                return y;
				              },
				              resize: true
				        });
					<% } else if (l_i == 1) { %>
						Morris.Donut({
				        	element: 'donut_month',
				        	data: [
				        	<% 	if (l_errordbdata[l_i].getErrorFieldsCountMap() != null && l_errordbdata[l_i].getErrorFieldsCountMap().size() > 0) {
					        		int l_count = 0;
				        			for (String l_fieldname : l_errordbdata[l_i].getErrorFieldsCountMap().keySet()) { 
				        				if (l_count < 5) { %>
				        					{label: '<%=l_fieldname%>', value: <%= l_errordbdata[l_i].getErrorFieldsCountMap().get(l_fieldname) %>},
				        			 <% } 
				        			 l_count++;
				        		  	} %>
				        		  	],
				        		  	colors: ['#8E44AD', '#1ABC9C', '#58D68D', '#cc3300', '#3498DB'],
				        	<% } else { %>
				        		{label: 'Fields in Error', value: 0},
				        		],
				        		colors: ['#00cc00'],
				        	<% } %>	 
				              
				              formatter: function (y) {
				                return y;
				              },
				              resize: true
				        });
					<% } else { %>
						Morris.Donut({
				        	element: 'donut_prevmonth',
				        	data: [
				        	<% 	if (l_errordbdata[l_i].getErrorFieldsCountMap() != null && l_errordbdata[l_i].getErrorFieldsCountMap().size() > 0) {
					        		int l_count = 0;
				        			for (String l_fieldname : l_errordbdata[l_i].getErrorFieldsCountMap().keySet()) { 
				        				if (l_count < 5) { %>
				        					{label: '<%=l_fieldname%>', value: <%= l_errordbdata[l_i].getErrorFieldsCountMap().get(l_fieldname) %>},
				        			 <% } 
				        			 l_count++;
				        		  	} %>
				        		  	],
				        		  	colors: ['#8E44AD', '#1ABC9C', '#58D68D', '#cc3300', '#3498DB'],
				        	<% } else { %>
				        		{label: 'Fields in Error', value: 0},
				        		],
				        		colors: ['#00cc00'],
				        	<% } %>	 
				              formatter: function (y) {
				                return y;
				              },
				              resize: true
				        });
					<% } %>
			 <% } } %>
	      });
	    </script>
	</head>
 	<body>
 		<csrf:form name="frmaudit" id="frmaudit" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	  		<div class="title_left">
				<h3>Error Dashboard</h3>
			</div>
			<% if (l_errordbdata != null && l_errordbdata.length > 0) { 
				for (int l_i=0; l_i < l_errordbdata.length; l_i++) {%>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>
									<% if (l_i == 0) { %>
										Top 5 Error Fields (Today)
									<% } else if (l_i == 1) { %>
										Top 5 Error Fields (Current Month)
									<% } else { %>
										Top 5 Error Fields (Previous Month)
									<% } %>
									
									
								</h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<% if (l_i == 0) { %>
									<div id="donut_daily"></div>
								<% } else if (l_i == 1) { %>
									<div id="donut_month"></div>	
								<% } else { %>
									<div id="donut_prevmonth"></div>
								<% } %>
								
							</div>
							
						</div>
					</div>
				<% } %>	
			 <% } %>
			<% if (l_errordbdata != null && l_errordbdata.length > 0) { 
				for (int l_i=0; l_i < l_errordbdata.length; l_i++) {%>
				<% if (l_catcount > 1) {  %>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>
									<% if (l_i == 0) { %>
										Category Wise Errors (Today)
									<% } else if (l_i == 1) { %>
										Category Wise Errors (Current Month)
									<% } else { %>
										Category Wise Errors (Previous Month)
									<% } %>
								</h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Category</th>
											<th>Error Count </th>
										</tr>
									</thead>
									<tbody>
										<% for (int l_j=0; l_j < l_catcount; l_j++) {  %>
											<tr>
												<td><%=l_categories[l_j] %></td>
												<% if (l_errordbdata[l_i].getFieldCatCountMap().get(SafetyDBCaseFieldCategoryType.toEnum(l_j+1).name()) != null) { %>
													<td><%= l_errordbdata[l_i].getFieldCatCountMap().get(SafetyDBCaseFieldCategoryType.toEnum(l_j+1).name()) %></td>
												<% } else { %>
													<td>0</td>
												<% } %>
												
											</tr>
										<% } %>
									</tbody>	
								</table>
							</div>
							
						</div>
					</div>
				<% } %>	
				
		 <% } %>
		<% } %>
		<% if (l_errordbdata != null && l_errordbdata.length > 0) { 
			for (int l_i=0; l_i < l_errordbdata.length; l_i++) {%>
			<% if (l_errordbdata[l_i].getErrorTypeCountMap() != null && l_errordbdata[l_i].getErrorTypeCountMap().size() > 0) {  %>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>
								<% if (l_i == 0) { %>
									Error Type (Today)
								<% } else if (l_i == 1) { %>
									Error Type (Current Month)
								<% } else { %>
									Error Type (Previous Month)
								<% } %>
							</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Error Type</th>
										<th>Error Count</th>
									</tr>
								</thead>
								<tbody>
									<% for (String l_errorcode : l_errortypes.keySet()) {  %>
										<tr>
											<td><%=l_errortypes.get(l_errorcode) %></td>
											<% if (l_errordbdata[l_i].getErrorTypeCountMap().get(l_errorcode) != null) { %>
												<td><%= l_errordbdata[l_i].getErrorTypeCountMap().get(l_errorcode) %></td>
											<% } else { %>
												<td>0</td>
											<% } %>
											
										</tr>
									<% } %>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			<% } else { %>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>
								<% if (l_i == 0) { %>
									Error Type (Today)
								<% } else if (l_i == 1) { %>
									Error Type (Current Month)
								<% } else { %>
									Error Type (Previous Month)
								<% } %>
							</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Error Type</th>
										<th>Error Count</th>
									</tr>
								</thead>
								<tbody>
									<% for (String l_errorcode : l_errortypes.keySet()) {  %>
										<tr>
											<td><%=l_errortypes.get(l_errorcode) %></td>
											<td>0</td>
										</tr>
									<% } %>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			<% } %>	
		 <% } %>
		<% } %>
		<% if (l_errordbdata != null && l_errordbdata.length > 0) { 
			for (int l_i=0; l_i < l_errordbdata.length; l_i++) {%>
			<% if (l_errordbdata[l_i].getReviewTypeCountMap() != null && l_errordbdata[l_i].getReviewTypeCountMap().size() > 0) {  %>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>
								<% if (l_i == 0) { %>
									Review Type (Today)
								<% } else if (l_i == 1) { %>
									Review Type (Current Month)
								<% } else { %>
									Review Type (Previous Month)
								<% } %>
							</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Review Name</th>
										<th>Error Count</th>
									</tr>
								</thead>
								<tbody>
									<% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) {  %>
										<tr>
											<td><%=l_reviewtypedata.getObjectName() %></td>
											<% if (l_errordbdata[l_i].getReviewTypeCountMap().get(l_reviewtypedata.getObjectSeqId()) != null) { %>
												<td><%= l_errordbdata[l_i].getReviewTypeCountMap().get(l_reviewtypedata.getObjectSeqId()) %></td>
											<% } else { %>
												<td>0</td>
											<% } %>
											
										</tr>
									<% } %>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			<% } else { %>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>
								<% if (l_i == 0) { %>
									Review Type (Today)
								<% } else if (l_i == 1) { %>
									Review Type (Current Month)
								<% } else { %>
									Review Type (Previous Month)
								<% } %>
							</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Review Name</th>
										<th>Error Count</th>
									</tr>
								</thead>
								<tbody>
									<% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) {  %>
										<tr>
											<td><%=l_reviewtypedata.getObjectName() %></td>
											<% if (l_errordbdata[l_i].getReviewTypeCountMap().get(l_reviewtypedata.getObjectSeqId()) != null) { %>
												<td><%= l_errordbdata[l_i].getReviewTypeCountMap().get(l_reviewtypedata.getObjectSeqId()) %></td>
											<% } else { %>
												<td>0</td>
											<% } %>
											
										</tr>
									<% } %>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			<% } %>	
		 <% } %>
		<% } %>
		<% if (l_errordbdata != null && l_errordbdata.length > 0) { 
				for (int l_i=0; l_i < l_errordbdata.length; l_i++) {%>
					<% if (l_errordbdata[l_i].getErrorFieldsCountMap() != null && l_errordbdata[l_i].getErrorFieldsCountMap().size() > 0) {  %>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										<% if (l_i == 0) { %>
											Field Errors (Today)
										<% } else if (l_i == 1) { %>
											Field Errors (Current Month)
										<% } else { %>
											Field Errors (Previous Month)
										<% } %>
									</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Field Name </th>
												<th>Error Count </th>
											</tr>
										</thead>
										<tbody>
											<% for (String l_fieldname : l_errordbdata[l_i].getErrorFieldsCountMap().keySet()) {  %>
												<tr>
													<td><%=l_fieldname %></td>
													<td><%= l_errordbdata[l_i].getErrorFieldsCountMap().get(l_fieldname) %></td>
												</tr>
											<% } %>
										</tbody>	
									</table>
								</div>
							</div>
						</div>
					<% } else { %>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										<% if (l_i == 0) { %>
											Field Errors (Today)
										<% } else if (l_i == 1) { %>
											Field Errors (Current Month)
										<% } else { %>
											Field Errors (Previous Month)
										<% } %>
									</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table class="table table-striped">
										<tr> 
											<td>No records found</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					<% } %>	
					
			 <% } %>
			<% } %> 
		<%-- <% else { %>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<label>No records found</label>
						</div>
					</div>
				</div>		
			<% } %>	 --%>
		</csrf:form>	
  </body>
    
</html>