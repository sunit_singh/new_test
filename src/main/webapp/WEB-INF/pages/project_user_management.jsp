<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDetailsDb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
function validateFormParameters() {
	clearErrorMessages();
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}
function clickMod(value, id)
{
	clearErrorMessages();
	var formName=document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = value;
	document.getElementById("selectedProjectId").value =id;
	document.getElementById("projsettings_appid").value ='PROJCET-USER-MANGEMENT';
	if(validateFormParameters()){ 
		formName.submit();
	}
}
function showEditMenu (action ,uId, pId)
{
	clearErrorMessages();
	var formName=document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = action;
	document.getElementById("projsettings_appid").value ='PROJCET-USER-MANGEMENT';
	document.getElementById("selectedUserId").value =uId;
	if(validateFormParameters()){ 
		formName.submit();
	}
}

</script>

<% ProjectInfo p_oProjectInfo= (ProjectInfo)request.getAttribute("selectedProjectInfo");
List<UserInfo> userList= (List<UserInfo>)request.getAttribute("projectUsersList");
int currentuerseqid= (Integer)request.getAttribute("currentuserid");
String delMsg= (String)request.getAttribute("msg");
%>

</head>
<body>
<csrf:form name="PROJSETTINGS_form" id="PROJSETTINGS_form" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="selectedProjectId" name="selectedProjectId" value="">
	<input type="hidden" id="selectedUserId" name="selectedUserId" value="">
	<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="">
	
	
<div class="page-title">
	<div class="title_left">
		<h3>Project User Management</h3>
	</div>
</div>

<div class ="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h3>
						<small>Manage Users</small> 	
					</h3>
					<%if(delMsg!=null){  %>
						<%=delMsg %>
					<% }%>
					<div class="clearfix"></div>
				</div>
				
				<div class="x_content">
					<div class="form-group">
						<label class="col-md-2 col-sm-3 col-xs-12 ">
							Project Id: </label>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<label>
								<%=p_oProjectInfo.getProjectSequenceId()%>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 col-sm-3 col-xs-12 ">
							Project Name: </label>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<label>
								<%=p_oProjectInfo.getProjectName()%>
							</label>
						</div>
					</div>
					
					<!-- User data -->
					<div class="x_panel">
						<div class="x_title">
							<h3>
								<small>Member users</small> 	
							</h3>
						<div class="clearfix"></div>
						</div>
						
						<div class="x_content">
							<table class="table table-striped">
								<thead>
							    	<tr>
							        	<th></th>
							        	<th>User Id (Seq Id)</th>
							        	<th>Name</th>
								   </tr>
						         </thead>
					             <tbody>
					             <%if(userList.isEmpty()){ %>
					             	<tr>
										<td colspan="7">&nbsp;<i>No records found</i></td>
										</tr>
					            <%}else {
					            	for(UserInfo user: userList){ %>
					            
					             	<tr>
					             		<td>
					             		<%if(user.getUserSeqId()!=currentuerseqid) {%>
					             			<a onclick="showEditMenu('removeUser','<%=user.getUserSeqId() %>','<%=p_oProjectInfo.getProjectSequenceId()%>')">
												<i class="glyphicon glyphicon-remove-circle" data-toggle="tooltip" data-placement="left" title="Remove User"></i> 
											</a>
										<%} %>
										</td>
					             		<td><%=user.getUserId() %> &nbsp; (<%=user.getUserSeqId() %>)</td>
					             		<td><%=user.getUserDisplayName() %></td>
					             	</tr>
					             	<%}
					            	}%> 
								 </tbody>
							</table>	 
						</div> <!-- User x_Content ends -->
					</div><!--User x_panel ends  -->
					
					<button type="button" id="submitEditForm" onclick="clickMod('addUserView', '<%=p_oProjectInfo.getProjectSequenceId()%>')" class="btn btn-primary" onclick=>
						Add Users
					</button>
					
				</div><!--main_x_Content  -->
			</div> <!-- "x_panelends -->
		</div>
	</div><!-- row ends -->
</csrf:form>
</body>
</html>