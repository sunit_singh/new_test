<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib
	uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld"
	prefix="csrf"%>
<%@ page import="java.io.*,java.util.*"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	String message = null;
	UserInfo userInfo = null;
	UserInfo userInfoSession = null;
	boolean flag = false;
	boolean flagItServiceDesk = false;
	String viewFlag = "test";
	
	message = (String)request.getAttribute("message");

	userInfo = (UserInfo)request.getAttribute("userInfo");
	userInfoSession = (UserInfo)request.getAttribute("userInfoSession");

	
 	if(!userInfoSession.isSystemAdmin())
 	{
 		flagItServiceDesk = true;
 	}
	
	
	if(request.getAttribute("viewFlag")!=null){
		viewFlag = request.getAttribute("viewFlag").toString();
		
		if(userInfo.getUserId().equals("@bootstrap_user@")){
			viewFlag = "view";
		}
		
		}else if(userInfo != null)
		{	
			if(userInfo.getUserId().equals("@bootstrap_user@")){
				viewFlag = "view";
			}
			flag = true;
		}else{
			flag = false;
		}
	
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Management</title>

<script type="text/JavaScript">
	function validateFormParameters() {
		clearErrorMessages();
		oFormObject = document.forms["frmmain"];
		return validateInputParameter(oFormObject);
	}


	function formSubmit(p_redirect) {
		
		clearErrorMessages();
		var invalidInput = true;
		if (p_redirect == 1) {
			
			var invalidInput=validateInput();			
			if(invalidInput==false){ 
				document.getElementById("portal_action").value = "updateuser";
					if(validateFormParameters()){ 
						document.frmmain.submit();	
					}else{
						return false;
					} 
				}  
						
		} else if (p_redirect == 2) {
			
			var invalidInput=validateInput();			
			if(invalidInput==false){ 
				document.getElementById("portal_action").value = "createuser";
					if(validateFormParameters()){ 
						
						document.frmmain.submit();	
					}else{
						return false;
					} 
				}  
			
		} else {
			return false;
		}
	}	
	function fnback() {
		document.getElementById("portal_action").value = "back";
		document.frmmain.submit();
	}
function validateInput()
{
	clearErrorMessages();
	var bErrorDetected = false;
	var pattern = new RegExp (/[a-zA-Z0-9][a-zA-Z0-9\\s_-]+[a-zA-Z0-9]/);
	
	var emailPattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

	var UserId=document.getElementById("userId").value;
	var UserDisplayName=document.getElementById("userDisplayName").value;
	var UserEmailId=document.getElementById("emailId").value;
	var UserAuthType=document.getElementById("userAuthType").value;
	
	if(UserId==""||UserId.trim()==""){
		displayErrorMessage("UI-1046");
		bErrorDetected = true;
	} else if(!pattern.test(UserId))
	{
		displayErrorMessage("UI-1047");
		bErrorDetected = true;
	}else if(UserDisplayName==""||UserDisplayName.trim()=="")
	{
		displayErrorMessage("UI-1048");
		bErrorDetected = true;
	}else if(UserEmailId==""||UserEmailId.trim()=="")
	{
		displayErrorMessage("UI-1049");
		bErrorDetected = true;
	} else if(!emailPattern.test(UserEmailId))
	{
		displayErrorMessage("UI-1051");
		bErrorDetected = true;
	} else if(UserAuthType=="-1"||UserAuthType.trim()=="")
	{
		displayErrorMessage("UI-1050");
		bErrorDetected = true;
	} 
	else
	{
		return false;
	}
} 


</script>

</head>

<body>
	<csrf:form name="frmmain" id="frmmain" class="form-horizontal"
		method="post" action="LandingServlet">
		<input type="hidden" id="portal_appid" name="portal_appid"
			value="USRMGMNT-PORTAL-APP">
		<input type="hidden" id="portal_action" name="portal_action" value="">
		<input type="hidden" id="userRole" name="userRole" value="">
		<div class="title_left">
			<h3>User Management Dashboard</h3>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<!-- <h2>Create User</h2> -->

					<%if(viewFlag.contains("view")){ %>
					<h2>View User</h2>
					<%}else if(flag){ %>
					<h2>Update User</h2>
					<%}else{ %>
					<h2>Create User</h2>
					<%} %>
					<ul class="nav navbar-right panel_toolbox">

					</ul>
					<div class="clearfix"></div>
				</div>

				<%
					if (message != null && message != "") {
				%>
				<div class="row">
					<div class="col-sm-12" style="align: center;">
						<h4
							style="padding: 20px; background-color: #e6ffe6; color: #45a945; border-radius: 10px; padding-left: 200px;"><%= message %></h4>
					</div>
				</div>
				<%
					}
				%>
				<div class="row">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">User
							Id * </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div>
								<%if(viewFlag.contains("view")){ %>
								<input type="text" id="userId" name="userId"
									class="form-control" placeholder="User Id"
									value="<%=userInfo.getUserId() %>" disabled="disabled"
									readOnly="readonly" autofocus>
								<%}else if(flag){ %>
								<%-- 	<%if(flag){ %> --%>
								<input type="text" id="userId" name="userId"
									class="form-control" placeholder="User Id"
									value="<%= userInfo.getUserId() %>" readOnly="readonly"
									autofocus>
								<%}else{ %>
								<input type="text" id="userId" name="userId"
									class="form-control" placeholder="User Id" autofocus >
								<%} %>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
							Complete Name * </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div>
								<%if(viewFlag.contains("view")){ %>
								<input type="text" id="userDisplayName" name="userDisplayName"
									class="form-control" placeholder="User Display Name"
									value="<%= userInfo.getUserDisplayName() %>"
									disabled="disabled" readOnly="readonly" autofocus>
								<%}else if(flag){ %>
								<%-- <%if(flag){ %> --%>
								<input type="text" id="userDisplayName" name="userDisplayName"
									class="form-control" placeholder="User Display Name"
									value="<%= userInfo.getUserDisplayName() %>" autofocus>
								<%}else{ %>
								<input type="text" id="userDisplayName" name="userDisplayName"
									class="form-control" placeholder="User Display Name" autofocus>
								<%} %>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Email
							Address * </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div>
								<%if(viewFlag.contains("view")){ %>
								<input type="text" id="emailId" name="emailId"
									class="form-control" placeholder="Email Id"
									value="<%= userInfo.getUserEmail() %>" disabled="disabled"
									readOnly="readonly" autofocus>
								<%}else if(flag){ %>

								<input type="text" id="emailId" name="emailId"
									class="form-control" placeholder="Email Id"
									value="<%= userInfo.getUserEmail() %>" autofocus>
								<%}else{ %>
								<input type="text" id="emailId" name="emailId"
									class="form-control" placeholder="Email Id" autofocus>
								<%} %>

							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
							Auth Type * </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div>
								<%if(viewFlag.contains("view")){ %>
								<select class="form-control" id="userAuthType"
									disabled="disabled" readOnly="readonly" name="userAuthType">									
									<option value="<%= userInfo.getUserAuthType().value()%>" selected="selected">LDAP</option>
								</select>
								<%}else if(flag){ %>
								<select class="form-control" id="userAuthType"
									name="userAuthType">									
									<option value="<%= userInfo.getUserAuthType().value()%>" selected="selected">LDAP</option>
								</select>
								<%}else{ %>
								<select class="form-control" id="userAuthType"
									name="userAuthType">
									<option value="<%= userInfoSession.getUserAuthType().value()%>" selected="selected">LDAP</option>									
								</select>
								<%} %>

							</div>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
						</label>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div>
								<%if(flag){ %>

								<input type="hidden" name="userSeqId"
									value="<%= userInfo.getUserSeqId() %>">
								<%}else{ %>

								<%} %>
							</div>
						</div>
					</div>
				</div>

				<!-- -Check box added -->
				<div class="row">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Add-on
							Roles </label>


						
						<div class="col-md-4 col-sm-4 col-xs-12">

							<%if(viewFlag.contains("view")){ %>

							<input type="checkbox" id="projectAdmin" name="projectAdmin"
								value="projectAdmin" disabled="disabled"
								readonly="readonly" <%if(userInfo.isProjectAdmin()){ %>
								checked="checked" <%}%> autofocus>Project Admin</input>
								
							<%}else if(flag){ %>
							<input type="checkbox" id="projectAdmin" name="projectAdmin"
								value="projectAdmin " <%if(userInfo.isProjectAdmin()){ %>
								checked="checked" <%}%> 
								<%if(flagItServiceDesk){ %>
								disabled="disabled" readonly="readonly" <%}%> autofocus>Project Admin</input>

							<%}else{ %>
							<%if(flagItServiceDesk){ %>
							<span class=" control-label col-md-12 col-sm-8 col-xs-12"
									id=showAddOnRoleMsg>Add On Role given by System Admin</span>
							<%}else{%>
							<input type="checkbox" id="projectAdmin" name="projectAdmin"
								value="projectAdmin" autofocus>Project
							Admin</input>
							
							<%}} %>

						</div>

					</div>
				</div>

				<div class="row">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
						<div class="col-md-4 col-sm-4 col-xs-12">

							<%if(viewFlag.contains("view")){ %>

							<input type="checkbox" id="auditor" name="auditor"
								value="auditor " readonly="readonly" disabled="disabled"
								<%if(userInfo.isAuditor()){ %> checked="checked" <%}%> autofocus>Auditor</input>

							<%}else if(flag){ %>

							<input type="checkbox" id="auditor" name="auditor"
								value="auditor " <%if(userInfo.isAuditor()){ %>
								checked="checked" <%}%> <%if(flagItServiceDesk){ %>
								disabled="disabled" readonly="readonly" <%}%> autofocus>Auditor</input>

							<%}else{ %>
							<%if(flagItServiceDesk){ %>
							
							<%}else{%>
							<input type="checkbox" id="auditor" name="auditor"
								value="Auditor" autofocus>Auditor</input>
							<%} }%>


						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
							<div class="col-md-4 col-sm-4 col-xs-12">

								<%if(viewFlag.contains("view")){ %>

								<input type="checkbox" id="systemAdmin" name="systemAdmin"
									value="systemAdmin " <%if(userInfo.isSystemAdmin()){ %>
									checked="checked" <%}%> disabled="disabled" readonly="readonly"
									autofocus>System Admin</input>
								<%}else if(flag){ %>
								<input type="checkbox" id="systemAdmin" name="systemAdmin"
									value="systemAdmin " <%if(userInfo.isSystemAdmin()){ %>
									checked="checked" <%}%> <%if(flagItServiceDesk){ %>
								disabled="disabled" readonly="readonly" <%}%> autofocus>System Admin</input>
								<%}else{ %>
								<%if(flagItServiceDesk){ %>
							
							<%}else{%>
								<input type="checkbox" id="systemAdmin" name="systemAdmin"
									value="systemAdmin" autofocus>System Admin</input>
								<%}} %>



							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
							<div class="col-md-4 col-sm-4 col-xs-12">

								<%if(viewFlag.contains("view")){ %>
								<input type="checkbox" id="itServiceDesk" name="itServiceDesk"
									value="itServiceDesk " <%if(userInfo.isItServiceDeskUser()){ %>
									checked="checked" <%}%> disabled="disabled" readonly="readonly"
									autofocus>IT ServiceDesk</input>
								<%}else if(flag){ %>
								<input type="checkbox" id="itServiceDesk" name="itServiceDesk"
									value="itServiceDesk " <%if(userInfo.isItServiceDeskUser()){ %>
									checked="checked" <%}%> <%if(flagItServiceDesk){ %>
								disabled="disabled" readonly="readonly" <%}%> autofocus>IT ServiceDesk</input>
								<%}else{ %>
								<%if(flagItServiceDesk){ %>
							
							<%}else{%>
								<input type="checkbox" id="itServiceDesk" name="itServiceDesk"
									value="itServiceDesk" autofocus>IT ServiceDesk</input>
								<%}} %>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
							<div class="col-md-4 col-sm-4 col-xs-12">

								<%if(viewFlag.contains("view")){ %>
								<input type="checkbox" id="projectAuditor" name="projectAuditor"
									value="projectAuditor " <%if(userInfo.isProjectAuditor()){ %>
									checked="checked" <%}%> disabled="disabled" readonly="readonly"
									autofocus>Project Auditor</input>
								<%}else if(flag){ %>
								<input type="checkbox" id="projectAuditor" name="projectAuditor"
									value="projectAuditor " <%if(userInfo.isProjectAuditor()){ %>
									checked="checked" <%}%> <%if(flagItServiceDesk){ %>
								disabled="disabled" readonly="readonly" <%}%> autofocus>Project Auditor</input>
								<%}else{ %>
								<%if(flagItServiceDesk){ %>
							
							<%}else{%>
								<input type="checkbox" id="projectAuditor" name="projectAuditor"
									value="projectAuditor" autofocus>Project Auditor</input>
								<%}} %>

							</div>
						</div>
					</div>
					
				
					<!-- check box closed -->


					<div class="row">

						<div class="col-sm-12">
							<div class="x_content"
								style="display: block; padding-left: 380px;">
								<button type="button" class="btn btn-primary" value="Back"
									onclick="fnback();">Back</button>
								<%if(viewFlag.contains("view")){ %>

								<%}else if(flag){ %>

								<button type="button" class="btn btn-success" value="Update"
									onclick="formSubmit(1);">Update</button>

								<%}else{ %>

								<button type="button" class="btn btn-success" value="Create"
									onclick="formSubmit(2) ;">Create</button>

								<%} %>
							</div>
						</div>
					</div>
				</div>

				

			</div>
	</csrf:form>

	<%if(flag || viewFlag.contains("view")){%>
	<input type="hidden" id="userTypeVal" value="<%=userInfo.getUserType()%>">
	<input type="hidden" id="userAuthTypeVal" value="<%=userInfo.getUserAuthType().value()%>">

	<%} %>

</body>
</html>