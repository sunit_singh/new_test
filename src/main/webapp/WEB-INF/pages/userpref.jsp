<%@page import="com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHome"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.sciportal.user.UserUtils"%>
<%@page import="com.sciformix.sciportal.project.ProjectHome"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.user.UserHome"%>
<%@page import="com.sciformix.sciportal.apps.sys.UserPrefsPortalApp"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@page import="com.sciformix.commons.SciError"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>
<%@page import="com.sciformix.sciportal.apps.AppUserPreferenceMetadata"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.user.UserPreference"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.web.WebConstants"%>

<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@page import="com.sciformix.commons.utils.*"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%

List<AppInfo> listApps = AppRegistry.listBusinessApps();
listApps = (listApps != null ? listApps : new ArrayList<AppInfo>());

UserSession oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
UserInfo oSessionUser = (oUserSession != null ? oUserSession.getUserInfo() : null);
List<String> listAppId = new ArrayList<String>();
List<String> listPreferenceKey = new ArrayList<String>();
IPortalApp oPortalApp = null;
List<AppUserPreferenceMetadata> listAppUserPreference = null;
String[] sarrOptionValues = null;
List<ObjectIdPair<String,String>>	listPreferenceValuePair	=	null;
UserPreference oCurrentUserPreference = null;
String sPreferenceCurrentValue = null;
List<UserPreference> listUserPreferences = null;
Map<String, UserPreference> mapUserPreferencesByAppAndKey = null;
List<ObjectIdPair<Integer,String>> listAuthorizedProjectNames = null;
int projectId = -1;
String selectedProjectId = null;
String sAppId = null;
String sPreferenceKey = null;
String sPreferenceDisplayName = null;
boolean bMandatoryPreference = false;
boolean userPrefAvailable = false;
boolean reviewTypAvl = true;

listUserPreferences = UserHome.retrieveUserPreferences(oSessionUser);
mapUserPreferencesByAppAndKey = new HashMap<String, UserPreference>();
for (UserPreference oPreference : listUserPreferences)
{
	if(oPreference.getProjectSeqId()== -1)
	{	
	mapUserPreferencesByAppAndKey.put(oPreference.getAppName() + "::" + oPreference.getKey(), oPreference);
	}
	else
	{
	mapUserPreferencesByAppAndKey.put(oPreference.getAppName() + "::" + oPreference.getKey()+"::"+oPreference.getProjectSeqId(), oPreference);
	}
}

listAuthorizedProjectNames = oUserSession.getAuthorizedProjectIds();

%>

<%@include file="uiFormCreationMethods.jsp"%>

<csrf:form id="userpref_FRM" name="userpref_FRM" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post">
	<%=displayFormHiddenVariables("USRPREF-PORTAL-APP", "userpref_save") %>
	
	<div class="center_col" role="main">
    	<div class="">
          	<%=displayFormHeader("User Preferences") %>
            
            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip("View/modify your application preferences", false, false) %>
                  
                  <div class="x_content">
                  <h4> Applicable Projects</h4>
                  <div class="form-group">
                  
                  <%
                  if (listAuthorizedProjectNames != null && !listAuthorizedProjectNames.isEmpty())
                  {
                	  boolean bAtleastOneProjectAdded = false;
                	  userPrefAvailable = oUserSession.isUserPreferenceAvailable(projectId, UserUtils.PROJECT_MANAGEMENT_PORTAL_APP, UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID);
                	  if(userPrefAvailable)
                	  {
                	  	oCurrentUserPreference = mapUserPreferencesByAppAndKey.get(UserUtils.PROJECT_MANAGEMENT_PORTAL_APP + "::" + UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID);
                	  	selectedProjectId =	oCurrentUserPreference.getValue();
                	  }
                	  if(listAuthorizedProjectNames.size()==1&&userPrefAvailable&& oCurrentUserPreference.getValue()!=null)
                	  {%> 
                	  <label class="control-label col-md-3 col-sm-3 col-xs-12 "> Mapped Project: </label>
                		 <label class="control-label"> <%=listAuthorizedProjectNames.get(0).getObjectName()%></label>
                	 <%bAtleastOneProjectAdded = true;
                	  }
                	  else{ 
                	  %>
                	  <label class="control-label col-md-3 col-sm-3 col-xs-12 "> Select Current Project</label>
                	  <div class="col-md-4 col-sm-4 col-xs-12">
    	                  <select class="form-control" id="pref_user_default_project" name="pref_user_default_project">
    	             <%
                 	 	if(!userPrefAvailable || StringUtils.isNullOrEmpty(oCurrentUserPreference.getValue()))
         					{%>
         					<option value="" selected="selected">--Select Project--</option>
         					<%
                 	 	  	for (ObjectIdPair<Integer,String> project : listAuthorizedProjectNames)
    	                  		{%>
                		  			<option value="<%=project.getObjectSeqId() %>" ><%=project.getObjectName() %></option>
    	                  		<%}
         					}
                 	 	else{
         					for (ObjectIdPair<Integer,String> project : listAuthorizedProjectNames)
    	                  	{
         						if(Integer.parseInt(oCurrentUserPreference.getValue()) == project.getObjectSeqId())
         						{%>
         							<option value="<%=project.getObjectSeqId() %>" selected="selected" ><%=project.getObjectName() %></option>
         						<%}
         						else
         						{%>
                		  			<option value="<%=project.getObjectSeqId() %>" ><%=project.getObjectName() %></option>
    	                  		<%}
         					}
    	                  bAtleastOneProjectAdded = true;
    	                 } %> 
                 	 </select></div>
                 	 <%
                	  }
	              }else{
                  %>
                  <label class=" "> No project assigned to user</label>
                  <%} %>
                  
                </div>
                
                <div id ="app_specific_userpreferences" > 
                <div class="ln_solid"></div>
	                <div id = "proejct_independent_apps">
	              	<%
						boolean bFirstAppAdded = false;
						boolean bInitialState = false;
						
						for(AppInfo oAppInfo : listApps)
						{
							sAppId = oAppInfo.getAppId();
							
							if(!oAppInfo.getPortalApp().applicationProjectSpecific())
							{
							%>
							<div class="ln_solid"></div> 
							<%
							listAppId.add(sAppId);
							oPortalApp = AppRegistry.getApp(sAppId);
							if (oPortalApp.requiresUserPreferences())
							{
								if (bFirstAppAdded)
								{
									%>
			                     	<div class="ln_solid"></div>
				                    <% 
								}
								bFirstAppAdded = true;
								%>
		                     	<h4><%=oAppInfo.getAppName()%></h4>
			                    <% 
			                    if (oPortalApp.allowsUserApplicationDisable())
			        			{
			        				sPreferenceKey = "_APPLICATION_ENABLED_";
			        				
			        				oCurrentUserPreference = mapUserPreferencesByAppAndKey.get(sAppId + "::" + sPreferenceKey);
			        				sPreferenceCurrentValue = (oCurrentUserPreference != null) ? oCurrentUserPreference.getValue() : "";
			        				
			        				sPreferenceKey = "__userprefenableapp_" + sAppId;
			        				bInitialState = (sPreferenceCurrentValue.equalsIgnoreCase("YES"));
				                    %>
				                    <%=displaySwitch(sPreferenceKey, "Application Enabled", true, bInitialState)%>
			                    	<%
			                    }//end-if allowsUserApplicationDisable
			        			
			        			 listAppUserPreference = oPortalApp.getAppUserPreference();
			        			 for (AppUserPreferenceMetadata oAppUserPreference : listAppUserPreference)
			        			{
			        				sPreferenceKey = oAppUserPreference.getPreferenceKey();
			        				listPreferenceKey.add(sPreferenceKey);
			        				sPreferenceDisplayName = oAppUserPreference.getPreferenceKeyName();
			        				bMandatoryPreference = oAppUserPreference.isMandatory();
			        				
			        				oCurrentUserPreference = mapUserPreferencesByAppAndKey.get(sAppId + "::" + sPreferenceKey);
			        				sPreferenceCurrentValue = (oCurrentUserPreference != null) ? oCurrentUserPreference.getValue() : null;
			        				
				        				if (oAppUserPreference.getPreferenceType() == AppUserPreferenceMetadata.PreferenceType.ENUMERATED_SINGLE_VALUED)
				        				{
					        				sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey();
					        				listPreferenceValuePair = oAppUserPreference.getOptionValues(oUserSession);
			                    			%>
			                    			<%=displaySelectBox(sPreferenceKey, sPreferenceDisplayName, bMandatoryPreference, listPreferenceValuePair, sPreferenceCurrentValue)%>
			                      			<%
			                      		} else if (oAppUserPreference.getPreferenceType() == AppUserPreferenceMetadata.PreferenceType.ENUMERATED_MULTIPLE_VALUED) {
			                      			sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey();
			                      			listPreferenceValuePair = oAppUserPreference.getOptionValues(oUserSession);
			        						%>
			                    			<%=displaySelectBoxMultiple(sPreferenceKey, sPreferenceDisplayName, bMandatoryPreference, listPreferenceValuePair, sPreferenceCurrentValue)%>
			                      			<%
			                      		} 
			        				
		                    	} //end-for AppUserPreferenceMetadata 
		                    	%>
	                    		<!-- <div class="ln_solid"></div> -->
								<%			
							}//end-if requiresUserPreferences
						}}//end-for AppInfo
						%> 
					</div>
						
					<div class="ln_solid"></div>
					
					<%
					if (listAuthorizedProjectNames != null && !listAuthorizedProjectNames.isEmpty() && !StringUtils.isNullOrEmpty(selectedProjectId))
	                {
						boolean bFirstProjAppAdded = false;
	               		boolean bInitialProjAppState = false;
	               		boolean revietypeAvailFlag = true;
					%>
	               		<div class="col-md-12 col-sm-12 col-xs-12">  <!-- Project Dependent User Preference UI -->
	                    <div class="x_panel">
	                      <div class="x_title">
	                        <h2>Project Dependent Apps</h2>
	                        <ul class="nav navbar-right panel_toolbox">
	                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                          </li>
	                        </ul>
	                        <div class="clearfix"></div>
	                      </div>
	                      <div class="x_content">

	                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
	                          <ul id="projectTabs" class="nav nav-tabs bar_tabs" role="tablist">
	                         
	                         <%for(ObjectIdPair<Integer,String> project : listAuthorizedProjectNames){
	                        	 if(project.getObjectSeqId() == Integer.parseInt(selectedProjectId)){ %>
	                            <li role="project_tab" class="active">
	                            	<a href="#tab_content_<%=project.getObjectSeqId()%>" id="<%=project.getObjectSeqId()%>" role="tab" data-toggle="tab" aria-expanded="false">
		                            	<%=project.getObjectName()%>
		                            </a>
	                            </li>
	                            <%}else{ %>
	                             <li role="project_tab" class="">
	                            	<a href="#tab_content_<%=project.getObjectSeqId()%>" id="<%=project.getObjectSeqId()%>" role="tab" data-toggle="tab" aria-expanded="false">
		                            	<%=project.getObjectName()%>
		                            </a>
	                            </li>
	                         <%}} %>
	                          </ul>
	                          <div id="myTabContent" class="tab-content">
	                          <%for(ObjectIdPair<Integer,String> project : listAuthorizedProjectNames){
	                        	 
	                        	 %>
	                        	 <%if(project.getObjectSeqId() == Integer.parseInt(selectedProjectId)){ %>
		                            	<div role="tabpanel" class="tab-pane fade active in" id="tab_content_<%=project.getObjectSeqId()%>" aria-labelledby="home-tab">
	                            	<%}else{ %>
		                            	<div role="tabpanel" class="tab-pane fade" id="tab_content_<%=project.getObjectSeqId()%>" aria-labelledby="home-tab">
	                            	<%} %>
	                            
	                             <% 
	                              for(AppInfo oAppInfo : listApps)
								{
	                            	  
									sAppId = oAppInfo.getAppId();
									
									if(oAppInfo.getPortalApp().applicationProjectSpecific())
									{
									revietypeAvailFlag = true;	 
									listAppId.add(sAppId);
									oPortalApp = AppRegistry.getApp(sAppId);
									if (oPortalApp.requiresUserPreferences())
									{
										listAppUserPreference = oPortalApp.getAppUserPreference();
										for (AppUserPreferenceMetadata oAppUserPreference : listAppUserPreference)
					        			{
											sPreferenceKey = oAppUserPreference.getPreferenceKey();
						        			listPreferenceKey.add(sPreferenceKey);
						        			sPreferenceDisplayName = oAppUserPreference.getPreferenceKeyName();
						        			bMandatoryPreference = oAppUserPreference.isMandatory();
						        				
						        			oCurrentUserPreference = mapUserPreferencesByAppAndKey.get(sAppId + "::" + sPreferenceKey+"::"+project.getObjectSeqId());
						        			sPreferenceCurrentValue = (oCurrentUserPreference != null) ? oCurrentUserPreference.getValue() : null;
						        				
							        			if (oAppUserPreference.getPreferenceType() == AppUserPreferenceMetadata.PreferenceType.ENUMERATED_SINGLE_VALUED)
							        			{
								        			sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey()+"_"+project.getObjectSeqId();
								        			listPreferenceValuePair = oAppUserPreference.getOptionValues(oUserSession , project.getObjectSeqId());
								        		} else if (oAppUserPreference.getPreferenceType() == AppUserPreferenceMetadata.PreferenceType.ENUMERATED_MULTIPLE_VALUED) {
						                    		sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey()+"_"+project.getObjectSeqId();
						                      		listPreferenceValuePair = oAppUserPreference.getOptionValues(oUserSession , project.getObjectSeqId());
						        					if(listPreferenceValuePair == null){
						        						revietypeAvailFlag = false;
						        					}
						                      	}
						        		}
										
										bFirstAppAdded = true;
										
										if(revietypeAvailFlag)
										{%>
				                     	<h4><%=oAppInfo.getAppName()%></h4>
					                    <% 
					                    if (oPortalApp.allowsUserApplicationDisable())
					        			{
					        				sPreferenceKey = "_APPLICATION_ENABLED_";
					        				
					        				oCurrentUserPreference = mapUserPreferencesByAppAndKey.get(sAppId + "::" + sPreferenceKey+"::"+project.getObjectSeqId());
					        				sPreferenceCurrentValue = (oCurrentUserPreference != null) ? oCurrentUserPreference.getValue() : "";
					        				
					        				sPreferenceKey = "__userprefenableapp_" + sAppId+"_"+project.getObjectSeqId();
					        				bInitialState = (sPreferenceCurrentValue.equalsIgnoreCase("YES"));
						                    %>
						                    <%=displaySwitch(sPreferenceKey, "Application Enabled", true, bInitialState)%>
					                    	<%
					                    }//end-if allowsUserApplicationDisable
					        			
					        			 listAppUserPreference = oPortalApp.getAppUserPreference();
					        			 for (AppUserPreferenceMetadata oAppUserPreference : listAppUserPreference)
					        			 {
					        				sPreferenceKey = oAppUserPreference.getPreferenceKey();
					        				listPreferenceKey.add(sPreferenceKey);
					        				sPreferenceDisplayName = oAppUserPreference.getPreferenceKeyName();
					        				bMandatoryPreference = oAppUserPreference.isMandatory();
					        				
					        				oCurrentUserPreference = mapUserPreferencesByAppAndKey.get(sAppId + "::" + sPreferenceKey+"::"+project.getObjectSeqId());
					        				sPreferenceCurrentValue = (oCurrentUserPreference != null) ? oCurrentUserPreference.getValue() : null;
					        				
						        				if (oAppUserPreference.getPreferenceType() == AppUserPreferenceMetadata.PreferenceType.ENUMERATED_SINGLE_VALUED)
						        				{
							        				sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey()+"_"+project.getObjectSeqId();
							        				listPreferenceValuePair = oAppUserPreference.getOptionValues(oUserSession , project.getObjectSeqId());
					                    			%>
					                    			<%=displaySelectBox(sPreferenceKey, sPreferenceDisplayName, bMandatoryPreference, listPreferenceValuePair, sPreferenceCurrentValue)%>
					                      			<%
					                      		} else if (oAppUserPreference.getPreferenceType() == AppUserPreferenceMetadata.PreferenceType.ENUMERATED_MULTIPLE_VALUED) {
					                      			sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey()+"_"+project.getObjectSeqId();
					                      			listPreferenceValuePair = oAppUserPreference.getOptionValues(oUserSession , project.getObjectSeqId());
					        						%>
					                    			<%=displaySelectBoxMultiple(sPreferenceKey, sPreferenceDisplayName, bMandatoryPreference, listPreferenceValuePair, sPreferenceCurrentValue)%>
					                      			<%
					                      		} 
					        			} //end-for AppUserPreferenceMetadata 
				                    	}else {%>
			                    		Review Type is not maintained for the project.
										<%}			
									}//end-if requiresUserPreferences
								}}%>
	                            </div>
	                           <%} %>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                  </div>
					<%}%> 
	        		<%-- <jsp:include page="/WEB-INF/pages/formSubmitToolbar.jsp" flush="true" /> --%>
	        		<!-- <div class="ln_solid"></div> -->
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
							<button type="button" class="btn btn-primary" onclick="return cancelForm();">Cancel</button>
					    	<button type="button" class="btn btn-success" onclick="return submitForm();">Submit</button>
					    </div>
					</div>
					</div>
	        		<script type="text/javascript">
	        		
		        		function validateFormParameters() {
		        			clearErrorMessages();
		        			oFormObject = document.forms["userpref_FRM"];
		        			return validateInputParameter(oFormObject);
		        		}
	        			function cancelForm () {
	        				document.userpref_FRM.portal_appid.value = "";
	        				document.userpref_FRM.portal_action.value = "";
	        				document.userpref_FRM.submit();
	        			}
	        			function submitForm () {
	        				var l_selectelements = document.getElementsByTagName('select');
	        				for (var l_i = 0; l_i < l_selectelements.length; l_i++) { 
	        					if (l_selectelements[l_i].multiple) {
	        						var valuesare="";
	        						var l_options = document.getElementById(l_selectelements[l_i].id) && document.getElementById(l_selectelements[l_i].id).options;
	        						var opt;
	        						for (var i=0, iLen=l_options.length; i<iLen; i++) {
	        							opt = l_options[i];
	        						    if (opt.selected) {
	        						    	valuesare	=	valuesare + opt.value + "~";
	        						    }
	        						}
	        						valuesare	=	valuesare.slice(0,-1);
	        						var l_elementname	=	document.getElementById(l_selectelements[l_i].id+"_multiple");
	        						l_elementname.value	=	valuesare;
	        					}
	        				}
	        				if(validateFormParameters()){ 
	        					document.userpref_FRM.submit();
	        				}
	        			}
	        		</script>
                  </div> <!-- end-div x_content -->
                  
                </div><!-- end-div panel -->
              </div><!-- end-div col-md-12 col-sm-12 col-xs-12 -->
            </div><!-- end-div row -->
            
		</div><!-- end-div "" -->
	</div><!-- end-div center_col -->
</csrf:form>