<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.pvcp.PvcpCaseMaster"%>
<%@page import="java.util.Map"%>

<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<%
PvcpCaseMaster o_pvcpcpcasemaster =(PvcpCaseMaster)request.getAttribute("pcvpcasedetails");
%>
<script type="text/JavaScript">
	function fnsubmit(value,caseintrid) {
		document.frmmain.portal_action.value	=	value;
		document.frmmain.case_internal_id.value	=	caseintrid;
		if (validateFormParameters()) {
			document.frmmain.submit();
		}
	}
	function validateFormParameters () {
		oFormObject = document.forms["projectManagement"];
		return validateInputParameter(oFormObject);
	}
	
	function fncancel(value) {
		document.frmmain.portal_action.value	=	value;
		document.frmmain.submit();
	}
</script>
<body>
	<csrf:form name="frmmain" id="frmmain" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="case_internal_id" name="case_internal_id" value="">
	
	<div class="page-title">
		<div class="title_left">
			<h3>PVCP Case Record Details</h3>
		</div>
	</div>
	<div class ="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				 <div class="x_title">
					<h3>
						<small>Record Details</small> 	
					</h3>
					<div class="clearfix"></div>
				</div>	
					<div class="x_content">
						<div id="templateList">
						<%if(o_pvcpcpcasemaster != null){ %>
							<table class="table table-striped">
								<thead>
							    	<tr>
							        	<th>Case Id</th>
							        	<th>Case Version</th>
							        	<th>Ird</th>
							        	<th>Case Type</th>
							        	<th>Product</th>
							        	<th>Seriousness</th>
							        	<th>DE Name</th>
							       </tr>
						         </thead>
						         <tbody>
						         	<tr>
							         	<td>
							         	<%=o_pvcpcpcasemaster.getCaseId() %>
							         	</td>
							         	<td>
							         	<%=o_pvcpcpcasemaster.getCaseVersion()%>
							         	</td>
							         	<td>
							         	<%=o_pvcpcpcasemaster.getInitialRecvDate() %>
							         	</td>
							         	<td>
							         	<%=o_pvcpcpcasemaster.getCaseType() %>
							         	</td>
							         	<td>
							         	<%=o_pvcpcpcasemaster.getProduct() %>
							         	</td>
							         	<td>
							         	<%=o_pvcpcpcasemaster.getCaseSeriousness() %>
							         	</td>
							         	<td>
							         	<%=o_pvcpcpcasemaster.getReviewType1UserName() %>
							         	</td>
							         	
						         	</tr>
						         </tbody>
					          </table>
					          <%} else{%>
					           No Record Found
					          <%} %>
						</div>
				   </div><!-- x Content End -->
				   <div class="ln_solid"></div>
					<div class="form-group">
			        	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
							<button type="button" class="btn btn-primary" onclick="fncancel('qualityreview');">
								Back
							</button>
							<%if(o_pvcpcpcasemaster != null){ %>
			                <button type="button" class="btn btn-success" value="Submit" onclick="fnsubmit('casereview','<%=o_pvcpcpcasemaster.getInternalCaseId()%>');">
			                	Case Review
			                </button>
			                <%} %>
			            </div>
			        </div>			
			</div>
		</div>		
	</div>	
	</csrf:form>
</body>
</html>