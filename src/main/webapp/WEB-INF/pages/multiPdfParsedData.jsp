<%@page import="com.sciformix.commons.file.FileParsingConfiguration"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.sourcedocutil.SourceDocUtilPortalApp"%>
<%@page import="java.util.LinkedHashSet"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.io.*"%>
<%@ page import="java.util.List" %>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.utils.ParsedDataTable"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
function submitForm(value)
{
	document.getElementById("portal_action").value = value;
	
	if (validateFormParameters())
	 {
		 document.frmain.submit();
	 }
}
function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["frmain"];
	return validateInputParameter(oFormObject);
}

function exportFile(action) {
	submitForm(action);
}
</script> 
<title>Parsed Data</title>
</head>
<% 
ParsedDataTable o_parseddatatable = null;
String activeTabFlag = null;
int noOfFiles = -1;
Map<String, Set<String>> groupMap = null;
Set<String> subGroupSet = null;
String parsedFileType = null;
String fileNameString = null;
String fileName = null;
String excelFileName = null;
parsedFileType = (String)request.getAttribute("parseFileType");
o_parseddatatable = (ParsedDataTable)request.getAttribute("parseddatatable");
request.getSession().setAttribute("parseDataTable", o_parseddatatable);
activeTabFlag = (String)request.getAttribute("activeTabFlag");
FileParsingConfiguration fileParseConfig = (FileParsingConfiguration)request.getAttribute("filePassConfig");
groupMap = (Map<String, Set<String>>)request.getAttribute("groupMap");
Map<String,List<String>> multipleFileData = (Map<String, List<String>>)request.getAttribute("keyValueArray");
request.getSession().setAttribute("multiplefileData",multipleFileData);
noOfFiles = (Integer)request.getAttribute("noOfFiles");
request.getSession().setAttribute("noOfFiles",noOfFiles);
fileNameString =(String) request.getAttribute("fileNameString");
request.getSession().setAttribute("fileNameString",fileNameString);
fileName = new File(fileNameString).getName();
excelFileName = fileName.replace(".csv",".xls");
%>
<body>
	<csrf:form name="frmain" id="frmain" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
		<input type="hidden" id="portal_appid" name="portal_appid" value="SOURCE_DOC_UTILS_PORTAL_APP" >
		<input type="hidden" id="portal_action" name="portal_action" value="">
		<input type="hidden" id="activeTabFlag" name="activeTabFlag" value="">
		

		<div class="title_left">
			<h3><%=AppRegistry.getToolsApp().getAppName() %> - Multiple <%=parsedFileType %> Files</h3>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				Download CSV: <A HREF="javascript:exportFile('multifileexportcsv');"><u> <%=fileName %></u><!-- <i class="glyphicon glyphicon-save-file"></i> --></A>
				&nbsp;&nbsp;&nbsp;&nbsp;Download Excel: <A HREF="javascript:exportFile('multifileExportExcel');"><u> <%=excelFileName %></u><!-- <i class="glyphicon glyphicon-save-file"></i> --></A>
			</div>
			
		</div>
		
		<div class="row">

						<div class="col-sm-12">
							<div class="x_content"
								style="display: block; padding-left: 380px;">

								<button type="button" class="btn btn-success" value="Next File"
									onclick="submitForm('pdfParser.jsp') ;">Next File</button>

							</div>
						</div>
			</div>
	
	
</csrf:form>
	
	
</body>					
</html>