<%@page import="java.util.ArrayList"%>
<%@page import="com.sciformix.sciportal.user.UserHome"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRTemplate"%>
<%@page import="com.sciformix.sciportal.pvcp.PvcpCaseMaster"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
			function validateFormParameters() {
				clearErrorMessages();
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnback(value) {
				document.frmmain.portal_action.value	=	value;
				document.frmmain.submit();
			}
			function fnupdate(id) {
				document.frmmain.portal_action.value	=	'updatecaserecord';
				document.getElementById("selected_caseid").value = id;
				if(validateFormParameters()){ 
					document.frmmain.submit();
				}
			}
		</script>
	</head>


<%
	Map<String,List<String>> enableUdfMap= null; 
	PvcpCaseMaster PVCPExistingCase = null;
	String		l_fileformat	= (String) 	request.getAttribute("fileformat");
	String		backflag	= (String) 	request.getAttribute("backflag");
	PVCPExistingCase		= (PvcpCaseMaster) 	request.getAttribute("existingCaseRecord");
	List<PvcpCaseMaster> l_casemasterlist =(List<PvcpCaseMaster>) request.getAttribute("caserecordlist");
	Map<String,String>	l_columnmapping	=	(Map<String,String>)request.getAttribute("columnmappingdetails");
	Map<String, Map<String,PvcpCaseMaster >> m_duplicatecasemastermap = (Map<String, Map<String,PvcpCaseMaster>>)request.getAttribute("duplicatecases");
	Map<String,PvcpCaseMaster> m_existingrecords=null;
	Map<String,PvcpCaseMaster> m_userrecords=null;
	request.getSession().setAttribute("updateCasemastermap", m_duplicatecasemastermap);
	if(!m_duplicatecasemastermap.isEmpty())
	{
	
	 m_existingrecords = m_duplicatecasemastermap.get("dbrecord");
	
	 m_userrecords = m_duplicatecasemastermap.get("userrecord");
	}
	enableUdfMap = (Map<String,List<String>>) request.getAttribute("enableUdfList");

%>
<body>
	<csrf:form name="frmmain" id="frmmain" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="selected_caseid" name="selected_caseid" value="">
	<input type="hidden" id="fldfiletype" name="fldfiletype" value="QRLCM">
	<input type="hidden" id="fldfileformat" name="fldfileformat" value="<%=l_fileformat %>">
	<input type="hidden" id="backflag" name="backflag" value="<%=backflag %>">
	<div class="page-title">
		<div class="title_left">
			<h3>Case Record Review</h3>
		</div>
	</div>
	
	<% if(!m_existingrecords.isEmpty()) { %>
		<!-- <div class="page-title">
			<div class="title_left">
				<h3>Duplicate Case Record</h3>
			</div>
		</div> -->
		<%	for (int i=0; i<m_existingrecords.size(); i++) {
			PvcpCaseMaster o_caseMasterexecRec = m_existingrecords.get("ExistingRecord_"+i); 
			PvcpCaseMaster o_caseMasteruserrec = m_userrecords.get("UserRecord_"+i); 
			List<String>	l_udfvalueexeclist	=	new ArrayList<String>(),
							l_udfvalueuserlist	=	new ArrayList<String>(); 
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf1());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf2());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf3());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf4());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf5());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf6());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf7());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf8());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf9());
			l_udfvalueexeclist.add(o_caseMasterexecRec.getUdf10());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf1());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf2());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf3());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf4());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf5());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf6());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf7());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf8());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf9());
			l_udfvalueuserlist.add(o_caseMasteruserrec.getUdf10()); %>
			
			<div class ="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						 <div class="x_title">
							<h3>
								<small>Existing case record for Case Id:<%=o_caseMasterexecRec.getCaseId() %> </small> 	
							</h3>
							<div class="clearfix"></div>
						</div>	
							<div class="x_content">
								<div id="templateList">
									<table class="table table-striped">
										<thead>
											<tr>
											<th></th>
									    		<% for (String l_internalname : l_columnmapping.keySet()) { %>
									        		<th><%= l_columnmapping.get(l_internalname) %></th>
									        	<% } %>
									       </tr>
								         </thead>
								         <tbody>
								         
								         	<tr>
								         		<td>Existing Values</td>
									         	
									         	<td>
									         	<%=o_caseMasterexecRec.getCaseId() %>
									         	</td>
									         	<td>
									         	<%if (o_caseMasterexecRec.getCaseVersion()!=null){%>
									         		<%=o_caseMasterexecRec.getCaseVersion()%>
									         	<%} %>
									         	</td>
									         	<td>
									         	<%if (o_caseMasterexecRec.getInitialRecvDate()!=null){%>
									         		<%= WebUtils.formatForDisplayDate(o_caseMasterexecRec.getInitialRecvDate(), "dd-MMM-YYYY") %>
									         	<%} %>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(o_caseMasterexecRec.getCaseType())%>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(o_caseMasterexecRec.getProduct())%>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(o_caseMasterexecRec.getCaseSeriousness())%>
									         	</td>
									         	<td>
									         		<% if (UserHome.retrieveUserBySeqId(o_caseMasterexecRec.getPcmDeUserSeqId()) != null ) { %>
									         			<%=UserHome.retrieveUserBySeqId(o_caseMasterexecRec.getPcmDeUserSeqId()).getUserDisplayName()%>
									         			(<%=UserHome.retrieveUserBySeqId(o_caseMasterexecRec.getPcmDeUserSeqId()).getUserId()%>)
									         		<% } %>	
									         	</td>
									         	<% for (int l_i=1; l_i <= 10; l_i++) { %>
									         		<% if(l_columnmapping.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)) {
									         			if (enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1) != null) { %>
															<% for (String l_udfvalue : StringUtils.splitOnComma(enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1))) { 
																String[]	l_udfdetails = l_udfvalue.split(":"); 
																if (l_udfdetails[0].equals(l_udfvalueexeclist.get(l_i-1))) { %>
																	<td><%=StringUtils.emptyString(l_udfdetails[1])%></td>
															 <% } %>
															<% } %>
																
													 <% } else {%>
													 		<td><%=StringUtils.emptyString(l_udfvalueexeclist.get(l_i-1))%></td>
													 <% } %>
									         		<% } %>
									         	<% } %>
									         </tr>
								         	
								         	<tr>
								         		<td>New Values</td>
									         	
									         	<td>
									         	<%=o_caseMasteruserrec.getCaseId() %>
									         	</td>
									         	<td>
									         	<%=o_caseMasteruserrec.getCaseVersion()%>
									         	</td>
									         	<td>
									         	<%if (o_caseMasteruserrec.getInitialRecvDate()!=null){%>
									         	<%= WebUtils.formatForDisplayDate(o_caseMasteruserrec.getInitialRecvDate(), "dd-MMM-YYYY") %>
									         	<%} %>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(o_caseMasteruserrec.getCaseType())%>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(o_caseMasteruserrec.getProduct())%>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(o_caseMasteruserrec.getCaseSeriousness())%>
									         	</td>
									         	<td>
									         		<% if (UserHome.retrieveUserBySeqId(o_caseMasteruserrec.getPcmDeUserSeqId()) != null ) { %>
									         			<%=UserHome.retrieveUserBySeqId(o_caseMasteruserrec.getPcmDeUserSeqId()).getUserDisplayName()%>
									         			(<%=UserHome.retrieveUserBySeqId(o_caseMasteruserrec.getPcmDeUserSeqId()).getUserId()%>)
									         		<% } %>	
									         	</td>
									         	<% for (int l_i=1; l_i <= 10; l_i++) { %>
									         		<% if(l_columnmapping.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)) {
									         			if (enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1) != null) { %>
															<% for (String l_udfvalue : StringUtils.splitOnComma(enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1))) { 
																String[]	l_udfdetails = l_udfvalue.split(":"); 
																if (l_udfdetails[0].equals(l_udfvalueuserlist.get(l_i-1))) { %>
																	<td><%=StringUtils.emptyString(l_udfdetails[1])%></td>
															 <% } %>
															<% } %>
																
													 <% } else {%>
													 		<td><%=StringUtils.emptyString(l_udfvalueuserlist.get(l_i-1))%></td>
													 <% } %>
									         		<% } %>
									         	<% } %>
									         </tr>
								         </tbody>
							         </table>
								</div>
						   </div><!-- x Content End -->
						 
						   <div class="form-group" align = "center">	
						  		<button type="button" class="btn btn-primary" onclick="fnupdate('<%=o_caseMasteruserrec.getInternalCaseId()%>');">
						 			Override
						 		</button>
			 			  </div>		
					</div>
				</div>		
			</div>	
	<%} } else if(PVCPExistingCase != null){
	List<String>	l_udfvalueexeclist	=	new ArrayList<String>();
					 
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf1());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf2());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf3());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf4());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf5());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf6());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf7());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf8());
			l_udfvalueexeclist.add(PVCPExistingCase.getUdf9());%>
	<div class ="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						 <div class="x_title">
							<h3>
								<small>Case record for case Id: <%=PVCPExistingCase.getCaseId() %> added successfully </small> 	
							</h3>
							<div class="clearfix"></div>
						</div>	
							<div class="x_content">
								<div id="templateList">
									<table class="table table-striped">
										<thead>
											<tr>
												<% for (String l_internalname : l_columnmapping.keySet()) { %>
									        		<th><%= l_columnmapping.get(l_internalname) %></th>
									        	<% } %>
									       </tr>
								         </thead>
								         <tbody>
								         
								         	<tr>
								         		<td>
									         	<%=PVCPExistingCase.getCaseId() %>
									         	</td>
									         	<td>
									         	<%=PVCPExistingCase.getCaseVersion() %>
									         	</td>
									         	<td>
									         	<%if (PVCPExistingCase.getInitialRecvDate()!=null){%>
									         		<%= WebUtils.formatForDisplayDate(PVCPExistingCase.getInitialRecvDate(), "dd-MMM-YYYY") %>
									         	<%} %>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(PVCPExistingCase.getCaseType())%>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(PVCPExistingCase.getProduct())%>
									         	</td>
									         	<td>
									         		<%=StringUtils.emptyString(PVCPExistingCase.getCaseSeriousness())%>
									         	</td>
									         	<td>
									         		<% if (UserHome.retrieveUserBySeqId(PVCPExistingCase.getPcmDeUserSeqId()) != null ) { %>
									         			<%=UserHome.retrieveUserBySeqId(PVCPExistingCase.getPcmDeUserSeqId()).getUserDisplayName()%>
									         			(<%=UserHome.retrieveUserBySeqId(PVCPExistingCase.getPcmDeUserSeqId()).getUserId()%>)
									         		<% } %>	
									         	</td>
									         	<% for (int l_i=1; l_i <= 10; l_i++) { %>
									         		<% if(l_columnmapping.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)) {
									         			if (enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1) != null) { %>
															<% for (String l_udfvalue : StringUtils.splitOnComma(enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1))) { 
																String[]	l_udfdetails = l_udfvalue.split(":"); 
																if (l_udfdetails[0].equals(l_udfvalueexeclist.get(l_i-1))) { %>
																	<td><%=StringUtils.emptyString(l_udfdetails[1])%></td>
															 <% } %>
															<% } %>
																
													 <% } else {%>
													 		<td><%=StringUtils.emptyString(l_udfvalueexeclist.get(l_i-1))%></td>
													 <% } %>
									         		<% } %>
									         	<% } %>
									       </tr>
								         </tbody>
							         </table>
						</div>
					 </div><!-- x Content End -->
				</div>		
			</div>	
		</div>
	<%} %>
	
	
	<% if(PVCPExistingCase == null){  %>
	<div class ="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				 <div class="x_title">
					<h3>
						<small>List of Current Case Records</small> 	
					</h3>
					<div class="clearfix"></div>
				</div>	
					<div class="x_content">
						<div id="templateList">
							<table class="table table-striped">
								<thead>
							    	<tr>
							    		<% for (String l_internalname : l_columnmapping.keySet()) { %>
							    			<%if (l_internalname.startsWith("UDF")){
												if(enableUdfMap.containsKey(l_internalname)){%>
												<th><%= l_columnmapping.get(l_internalname) %></th>
											<%}}else{ %>
												<th><%= l_columnmapping.get(l_internalname) %></th>
											<%} %>
							        		
							        	<% } %>
							       </tr>
						         </thead>
						         <tbody>
						         <%if(l_casemasterlist!=null){%>
						         
						         	<% for(PvcpCaseMaster o_casemaster: l_casemasterlist) { 
						         		List<String>	l_udfvaluelist	=	new ArrayList<String>(); 
						         		l_udfvaluelist.add(o_casemaster.getUdf1());
						         		l_udfvaluelist.add(o_casemaster.getUdf2());
						         		l_udfvaluelist.add(o_casemaster.getUdf3());
						         		l_udfvaluelist.add(o_casemaster.getUdf4());
						         		l_udfvaluelist.add(o_casemaster.getUdf5());
						         		l_udfvaluelist.add(o_casemaster.getUdf6());
						         		l_udfvaluelist.add(o_casemaster.getUdf7());
						         		l_udfvaluelist.add(o_casemaster.getUdf8());
						         		l_udfvaluelist.add(o_casemaster.getUdf9());
						         		l_udfvaluelist.add(o_casemaster.getUdf10()); %>
						         	<tr>
							         	<td>
							         	<%=o_casemaster.getCaseId() %>
							         	</td>
							         	<td>
							         	<%=o_casemaster.getCaseVersion()%>
							         	</td>
							         	<td>
							         		<%= WebUtils.formatForDisplayDate(o_casemaster.getInitialRecvDate(), "dd-MMM-YYYY") %>
							         	</td>
							         	<td>
							         		<%=StringUtils.emptyString(o_casemaster.getCaseType()) %>
							         	</td>
							         	<td>
							         		<%=StringUtils.emptyString(o_casemaster.getProduct()) %>
							         	</td>
							         	<td>
							         		<%=StringUtils.emptyString(o_casemaster.getCaseSeriousness()) %>
							         	</td>
							         	<td>
							         		<% if (UserHome.retrieveUserBySeqId(o_casemaster.getPcmDeUserSeqId()) != null ) { %>
							         			<%=UserHome.retrieveUserBySeqId(o_casemaster.getPcmDeUserSeqId()).getUserDisplayName()%>
							         			(<%=UserHome.retrieveUserBySeqId(o_casemaster.getPcmDeUserSeqId()).getUserId()%>)
							         		<% } %>	
							         	</td>
							         	<% for (int l_i=1; l_i <= 10; l_i++) { %>
							         		<% if(l_columnmapping.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)) {
							         			if(enableUdfMap.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)){
								         			if (!StringUtils.isNullOrEmpty(enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1))) { 
								         				if (!StringUtils.isNullOrEmpty(l_udfvaluelist.get(l_i-1))) { %>
														<% for (String l_udfvalue : StringUtils.splitOnComma(enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1))) { 
															String[]	l_udfdetails = l_udfvalue.split(":"); 
															if (l_udfdetails[0].equals(l_udfvaluelist.get(l_i-1))) { %>
																<td><%=StringUtils.emptyString(l_udfdetails[1])%></td>
														 <% } else { %>
														 <% }%>
														<% } %>
														<% } else { %>
															<td></td>
														<% } %>
												 <% }else {%>
											 		<td><%=StringUtils.emptyString(l_udfvaluelist.get(l_i-1))%></td>
											 	<% } 
								         		} %>
							         		<% } %>
							         	<% } %>
							         	</tr>
						         	<%} 
						         	} 
						         	else{%>
						         	<tr>No records found</tr>
						         	<%}%>
						         </tbody>
					          </table>
						</div>
				   </div><!-- x Content End -->			
			</div>
		</div>		
	</div>
	<%} %>	
	
	 <div>
	     <div class="ln_solid"></div>
	 </div> 	
     <div class="form-group">	
     	<div align = "center">
     	<%if(backflag != null && backflag.equals("insert")){ %>
     		<button type="button" class="btn btn-primary" onclick="fnback('casemasterinsertview');">
	 			Back
	 		</button>
	 	<%}else{ %>
	 		<button type="button" class="btn btn-primary" onclick="fnback('ilqr_case_master');">
	 			Back
	 		</button>
	 	<%} %>
	 	</div>		
	 </div>	 
	</csrf:form>
</body>
</html>