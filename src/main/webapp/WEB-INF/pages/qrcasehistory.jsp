<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.user.UserHome"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRReviewData"%>
<%@page import="com.sciformix.commons.SciEnums.EmailSendSetting"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.config.ConfigMetadata"%>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@include file="uiFormCreationMethods.jsp"%>

<!DOCTYPE html>
<html>
	<head>
	<script type="text/javascript">
	$(document).ready(function() {
		  $('[data-toggle="tooltip"]').tooltip();
	 });
	
	function fncancel(){
		document.getElementById("portal_action").value = "preparereport";	
		document.frmmain.submit();	
	}
	</script>
	</head>
<%
	Map<String, List<QRReviewData>> qrreviewdatamap  = null;
	SafetyDbCaseFieldsTemplate	l_scft	=	(SafetyDbCaseFieldsTemplate) request.getAttribute("fldactivesafetydbtemplate");
	QRReviewData o_qrcasereviewdata = null;
	qrreviewdatamap = (Map<String, List<QRReviewData>>) request.getAttribute("casehistorymap");
	String caseid =  (String) request.getAttribute("caseid");
	String caseversion =  (String) request.getAttribute("caseversion");
	Set<String> keyset = qrreviewdatamap.keySet();
	Map<String, ConfigMetadata> reviewtypekeyvaluemap = null;
	int i = 0;
	for(String key : keyset)
	{
		if(qrreviewdatamap.get(key).get(i)!=null)
		{
			o_qrcasereviewdata = qrreviewdatamap.get(key).get(i);
			break;
		}
		i++;
	}
	List<ObjectIdPair<String,String>>	l_reviewtypelist	=	(List<ObjectIdPair<String,String>>) request.getAttribute("reviewtypelist");
	//Back button
	int startdate =  (Integer) request.getAttribute("startdate");
	int enddate =  (Integer) request.getAttribute("enddate");
	String reporttype =  (String) request.getAttribute("fldreporttype");
	String fldsearchedcaseid =  (String) request.getAttribute("fldsearchedcaseid");
	int fldsearchtype =  (Integer) request.getAttribute("fldsearchtype");
	
	
%>
<body>
	<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
		<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
		<input type="hidden" id="portal_action" name="portal_action" value="">
		
		<!-- backbutton -->
		<input type="hidden" id="fldstartdatecount" name="fldstartdatecount" value="<%=startdate%>">
		<input type="hidden" id="fldenddatecount" name="fldenddatecount" value="<%=enddate%>">
		<input type="hidden" id="fldreporttype" name="fldreporttype" value="<%=reporttype%>">
		<input type="hidden" id="fldsearchtype" name="fldsearchtype" value="<%=fldsearchtype%>">
		<input type="hidden" id="fldsearchedcaseid" name="fldsearchedcaseid" value="<%=fldsearchedcaseid%>">
		
		<div class="title_left">
			<h3>Case Review History</h3> 
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Case Details
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><%= l_scft.getCaseIdAlias() %></th>
									<th><%= l_scft.getCaseVersionAlias() %></th>
									<th>Initial Receive Date</th>
									<th>Seriousness</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<%=o_qrcasereviewdata.getCaseId() %>
									</td>
										
									<td>
										<%=o_qrcasereviewdata.getCaseVersion() %>
									</td>
										
									<td>
										<%= WebUtils.formatForDisplayDate(o_qrcasereviewdata.getCaseRecvDate(), "dd-MMM-YYYY") %>
									</td>
								
									<td>
										<%=o_qrcasereviewdata.getCaseSeriousness() %>
									</td>
								</tr>
							</tbody>		
						</table>
					</div>
				
					<div id="wizard" class="form_wizard wizard_horizontal" >
                      <ul class="wizard_steps " style="margin-left: -3.0cm">
	                      <% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) {
                      			if(keyset.size() != 1) { %>
			                      	<li class="col-md-3 col-sm-3 col-xs-3"  >
			                          <a href="#step-1" class="selected" style="width: 10cm;" >
			                            <span class="step_no"  data-toggle="tooltip" data-placement="top" data-original-title="<%=l_reviewtypedata.getObjectName()%>">
			                            	<%=l_reviewtypedata.getObjectSeqId()%>
			                            </span>
			                            <span class="step_descr">
			                            <i class="fa fa-long-arrow-down" style="font-size: 25px">    </i> 
			                            </span>
			                          </a>
			                        </li>
                        	 <% } else { %>
			                     	<li class="col-md-3 col-sm-3 col-xs-3"  >
			                          <a href="#step-1" class="selected" style="width: 10cm;" >
			                            <span class="step_no"  data-toggle="tooltip" data-placement="top" data-original-title="<%=l_reviewtypedata.getObjectName()%>">
			                            	<%=l_reviewtypedata.getObjectSeqId()%>
			                            </span>
			                            <span class="step_descr">
			                            <i class="fa fa-long-arrow-down" style="font-size: 25px">    </i> 
			                            </span>
			                          </a>
			                        </li>
                        	 <% }
	                      	} %>
                      </ul>
                      </div>
                                   
					<% for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) { %>
						<div class="col-md-3 col-sm-3 col-xs-3"> <%
							if (qrreviewdatamap.containsKey(l_reviewtypedata.getObjectSeqId())) {
								for (QRReviewData o_qrreviewdata : qrreviewdatamap.get(l_reviewtypedata.getObjectSeqId())) { %>
									<div class="x_panel tile">
			                			<div class="x_title">
			                  				<h2>Round <%=o_qrreviewdata.getReviewRound()%> </h2>
			                  				<ul class="nav navbar-right panel_toolbox">
			                    				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
			                    				</li>
			                   				</ul>
			                  				<div class="clearfix"></div>
			                			</div>
			                			<div class="x_content">
			                 				<b>Case Reviewer : </b> <%=o_qrreviewdata.getCreatedByName()%><br />
			                 				<b>Case Associate : </b> <%=o_qrreviewdata.getCaseAssociateName()%><br />
											<b>Quality : </b> <%=o_qrreviewdata.getCaseQuality() %> %<br />
											<b>Total Fields Reviewed : </b> <%= o_qrreviewdata.getFieldsReviewed() %><br />
											<b>Case Result : </b> 	<%= o_qrreviewdata.getCaseResult() %> <br />
											<b>Reviewed On : </b> 	<%=WebUtils.formatForDisplayDuration(o_qrreviewdata.getCreatedOn()) %>
										</div>
			              			</div>
	            			 <% }
							} else { %>
								<div class="x_panel tile">
		                				<h2 align="center"> Pending </h2>
		                  		</div>
						 	<% } %>
						</div>
					<% } %>
				</div>
			</div>
		</div>		
		<div class="form-group">
			<div align= "center">
				<button type="button" class="btn btn-primary" onclick="fncancel();">
					Back
				</button>
			    
			</div>
		</div>
			
			
	</csrf:form>
</body>
</html>