<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="java.util.Map"%>

<script type="text/javascript">

function submitForm(value)
{
	var fileName=document.getElementById("ngv_templaters_importfile").value;
	if(fileName=="")
	{
		displayErrorMessage("UI-1014");
		return false;
	}else
	{
		var form = document.getElementById("PROJSETTINGS_form");
		document.getElementById("portal_action").value = "NGV_Protal_App";
		document.getElementById("clickValue").value =value;
		
		if (validateFormParameters())
		 {
			form.submit();
		 }
	}
}
function validateFormParameters () {
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}

function submitRuleSet(value)
{
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = "NGV_Protal_App";
	document.getElementById("clickValue").value =value;
	if (validateFormParameters())
	 {
		form.submit();
	 }
}
</script>
	<div class="center_col" role="main">
	 <div class="">
	  <div class="row" >
	   <div class="col-md-12 col-sm-12 col-xs-12" >
<%
String sNewPortalActionLocal = (String) request.getAttribute("new-portalaction");

if (sNewPortalActionLocal.equals("ngv_tplrsimport_step1dp"))
{
%>

 <div class="ln_solid"></div>	
   		 <div class="form-group" >
						<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12" >
                        Template Ruleset File
                        
                        <input type="file" name="ngv_templaters_importfile" id="ngv_templaters_importfile" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                       
                         </div>
         </div>
	<!-- 	<input type="file" name="ngv_templaters_importfile" id="ngv_templaters_importfile" /> -->
			 <div class="ln_solid"></div>	
                  	 <div class="form-group" >	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  	  <button type="submit" class="btn btn-primary"
									onsubmit="LandingServlet?portal_appid=PROJSETTINGS-PORTAL-APP">Cancel</button>
		<input type="submit" class="btn btn-primary" value="Next" onclick="return submitForm('ngv_tplrsimport_step1sb');" />
		
		</div>
		</div>

<%
}
else if (sNewPortalActionLocal.equals("ngv_tplrsimport_step2dp"))
{
	if(request.getAttribute("r_rulesetXmlDocument")!=null)
	{ %>
	<INPUT type="hidden" name="r_rulesetXmlDocument" id="r_rulesetXmlDocument" value="<%=request.getAttribute("r_rulesetXmlDocument")%>" />
	<INPUT type="hidden" name="r_import_file_name" id="r_import_file_name" value="<%=request.getAttribute("r_import_file_name")%>" />
	<%
		
	}
%>
 <div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
                        <div class="radio col-md-4 col-sm-4 col-xs-12">
	Template Ruleset name <INPUT type="text" name="ngv_addtpl_tplrulesetname" id="ngv_addtpl_tplrulesetname" value="<%=request.getAttribute("ngv_addtpl_tplrulesetname")%>" />
	</div>
	</div>
	 <div class="ln_solid"></div>	
                  	 <div class="form-group">	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" align="center">
		<input type="button" class="btn btn-primary" value="Next" onclick="javascript:submitRuleSet('ngv_tplrsimport_step2sb');" />
	</div>
	</div>

<%
}
%>
	</div>
	</div>
	</div>
	</div>