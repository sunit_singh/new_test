<%@page import="com.sciformix.sciportal.apps.mb.MBPortalApp"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.mb.MBMailInfo"%>
<%@page import="com.sciformix.sciportal.mb.MailBox"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript">
	function uploadFiles(action)
		{
			var form=document.getElementById("frmain");
			document.getElementById("portal_action").value = action;
			if (!validateInput())
			 {
				 form.submit();
			 }
		}
		function back()
		{
			var form=document.getElementById("frmain");
			document.getElementById("portal_action").value = "";
		    form.submit();
		}
	/* function validateFormParameters () {
		clearErrorMessages();
		oFormObject = document.forms["frmain"];
		return validateInputParameter(oFormObject);
	} */
	function validateInput()
	{
		clearErrorMessages();
		var bErrorDetected = false;
		var fileUpload =document.getElementById("msgFiles").value;
		var maiBoxValue =document.getElementById("selectMailBox").value;
		
		if(maiBoxValue==""||maiBoxValue.trim()==""){
			displayErrorMessage("UI-1091");
			bErrorDetected = true;
		}else 
		if(fileUpload==""||fileUpload.trim()==""){
			displayErrorMessage("UI-1092");
			bErrorDetected = true;
		} 
		return bErrorDetected;
	} 
</script>
<%List<MailBox> mailBoxList = (List<MailBox>)request.getAttribute("mblist");
	request.setAttribute("mailBox", mailBoxList);
Map<String, List<MBMailInfo>> mailListMap = null;
mailListMap = (Map<String, List<MBMailInfo>>)request.getAttribute("mailListMap");
	
%>
<body>
	<csrf:form name="frmain" id="frmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="MB-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">	
		<div class="page-title">
				<div class="title_left">
					<h3>
						Upload Outlook Mail
					</h3>
				</div>
		</div>
		
		<div class="x_panel">
			<div class="x_title">
			
					<h2 class = "col-md-2 col-sm-2 col-xs-12" >
						Upload Msg Files
					</h2>
					<%if(mailListMap != null && !mailListMap.isEmpty()){
						if(mailListMap.get(MBPortalApp.DUPLICATE_MAIL_KEY) != null && !mailListMap.get(MBPortalApp.DUPLICATE_MAIL_KEY).isEmpty())
						{%>
						<div class ="col-md-10 col-sm-10 col-xs-12" >
							<h5 style="color: red">
								Email already exist in system for message subject: <%=MBPortalApp.joinMailIdString(mailListMap.get(MBPortalApp.DUPLICATE_MAIL_KEY))%> .
					 		</h5>
					 	</div>
					 <%}
					}%>
					<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
							Select Mail Box: 
						</label>
						
						<div class="col-md-4 col-sm-4 col-xs-12">
						<select class= "form-control" id ="selectMailBox" name="selectMailBox">
							<option value=""> -- Select --</option>
							<%for(MailBox mailBox : mailBoxList){ %>
							<option value="<%=mailBox.getSequenceId()%>"> <%=mailBox.getTemplateName()%></option>
							<%} %>
						</select>
						</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
						Select Files: 
					</label>
					
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="file" id="msgFiles" name="msgFiles" multiple="multiple">
					</div>
				</div>
				<div class="ln_solid"></div>
				<div class="form-group" align="center">
					<input type = "button" class="btn btn-primary" value ="Back" onclick ="back();">
					<input type = "button" class="btn btn-success" value ="Upload" onclick ="uploadFiles('uploadmsgfile');">
				</div>
			</div>
		</div>
	</csrf:form>
</body>
</html>