<%@page import="com.sciformix.sciportal.user.UserHome"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.pvcp.PvcpCaseMaster"%>
<%@page import="java.util.Map"%>

<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<%
Map<String,List<String>> enableUdfMap =	(Map<String,List<String>>) request.getAttribute("enableUdfList"); 
List<PvcpCaseMaster> l_casemasterlist =(List<PvcpCaseMaster>) request.getAttribute("caserecordlist");
Map<String,String>	l_columnmapping	=	(Map<String,String>)request.getAttribute("columnmappingdetails");
%>

<script type="text/javascript">
function formcancel() 
{
	//document.viewpvcrcases.portal_action.value	=	value;
	document.viewpvcrcases.submit();
}
</script>
<body>
	<csrf:form name="viewpvcrcases" id="viewpvcrcases" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<div class="page-title">
		<div class="title_left">
			<h3>Case Record Review</h3>
		</div>
	</div>
	<div class ="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				 <div class="x_title">
					<h3>
						<small>List of Case Records</small> 	
					</h3>
					<div class="clearfix"></div>
				</div>	
					<div class="x_content">
						<div id="templateList">
						 <% if(l_casemasterlist!=null){
						 if(!l_casemasterlist.isEmpty()){%>
						   <table class="table table-striped">
								<thead>
							    	<tr>
							        	<% for (String l_internalname : l_columnmapping.keySet()) { %>
							    			<%if (l_internalname.startsWith("UDF")){
												if(enableUdfMap.containsKey(l_internalname)){%>
												<th><%= l_columnmapping.get(l_internalname) %></th>
											<%}}else{ %>
												<th><%= l_columnmapping.get(l_internalname) %></th>
											<%} %>
							        		
							        	<% } %>
							       </tr>
						         </thead>
						         <tbody>
						         	<%for(PvcpCaseMaster o_casemaster: l_casemasterlist) { 
						         		List<String>	l_udfvaluelist	=	new ArrayList<String>(); 
						         		l_udfvaluelist.add(o_casemaster.getUdf1());
						         		l_udfvaluelist.add(o_casemaster.getUdf2());
						         		l_udfvaluelist.add(o_casemaster.getUdf3());
						         		l_udfvaluelist.add(o_casemaster.getUdf4());
						         		l_udfvaluelist.add(o_casemaster.getUdf5());
						         		l_udfvaluelist.add(o_casemaster.getUdf6());
						         		l_udfvaluelist.add(o_casemaster.getUdf7());
						         		l_udfvaluelist.add(o_casemaster.getUdf8());
						         		l_udfvaluelist.add(o_casemaster.getUdf9());
						         		l_udfvaluelist.add(o_casemaster.getUdf10()); %>
						         	<tr>
							         	<td>
							         		<%=o_casemaster.getCaseId() %>
							         	</td>
							         	<td>
							         		<%=o_casemaster.getCaseVersion()%>
							         	</td>
							         	<td>
							         		<%= WebUtils.formatForDisplayDate(o_casemaster.getInitialRecvDate(), "dd-MMM-YYYY") %>
							         	</td>
							         	<td>
							         		<%=StringUtils.emptyString(o_casemaster.getCaseType()) %>
							         	</td>
							         	<td>
							         		<%=StringUtils.emptyString(o_casemaster.getProduct()) %>
							         	</td>
							         	<td>
							         		<%=StringUtils.emptyString(o_casemaster.getCaseSeriousness()) %>
							         	</td>
							         	<td>
							         		<% if (UserHome.retrieveUserBySeqId(o_casemaster.getPcmDeUserSeqId()) != null ) { %>
							         			<%=UserHome.retrieveUserBySeqId(o_casemaster.getPcmDeUserSeqId()).getUserDisplayName()%>
							         			(<%=UserHome.retrieveUserBySeqId(o_casemaster.getPcmDeUserSeqId()).getUserId()%>)
							         		<% } %>	
							         	</td>
							         	
							         	<% for (int l_i=1; l_i <= 10; l_i++) { %>
							         		<% if(l_columnmapping.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)) {
							         			if(enableUdfMap.containsKey(QRConstant.ENABLE_UDF_MAP_KEY + l_i)){
							         			if (enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1) != null) { %>
													<% for (String l_udfvalue : StringUtils.splitOnComma(enableUdfMap.get(QRConstant.ENABLE_UDF_MAP_KEY + l_i).get(1))) { 
														String[]	l_udfdetails = l_udfvalue.split(":"); 
														if (l_udfdetails[0].equals(l_udfvaluelist.get(l_i-1))) { %>
															<td><%=StringUtils.emptyString(l_udfdetails[1])%></td>
													 <% } %>
													<% } %>
														
											 <% }else {%>
										 		<td><%=StringUtils.emptyString(l_udfvaluelist.get(l_i-1))%></td>
										 <% } } %>
							         		<% } %>
							         	<% } %>
							         	<%-- <%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF1)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf1())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF2)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf2())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF3)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf3())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF4)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf4())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF5)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf5())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF6)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf6())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF7)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf7())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF8)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf8())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF9)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf9())%></td>
							        	<% } %>
							        	<%if(l_columnmapping.containsKey(QRConstant.CASE_MASTER_INTERNAL_UDF10)){%>
							        		<td><%=StringUtils.emptyString(o_casemaster.getUdf10())%></td>
							        	<% } %> --%>
							      	</tr>
						         	<%}%>
						         </tbody>
						      </table>
					            <%}else{ %>
						         	No Record Found
						        <%}}else{ %>
						         	No Record Found
						         <%} %>
						</div>
				   </div><!-- x Content End -->			
			</div>
			<div class="form-group">	
              <div align="center">
              <div class="In_solid"></div>
             	<button type="button" class="btn btn-primary" onclick="formcancel();">
					Back
				</button>
	          </div>		
	        </div>	 
		</div>		
	</div>	
	
	</csrf:form>
</body>
</html>