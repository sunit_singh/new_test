<%@page import="com.sciformix.commons.file.FileParsingConfiguration.FileParsingField"%>
<%@page import="com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue"%>
<%@page import="com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue"%>
<%@page import="com.sciformix.commons.file.FileParsingConfiguration"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.sourcedocutil.SourceDocUtilPortalApp"%>
<%@page import="java.util.LinkedHashSet"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.utils.ParsedDataTable"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
function submitForm(value)
{
	document.getElementById("portal_action").value = value;
	
	if (validateFormParameters())
	 {
		 document.frmain.submit();
	 }
}
function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["frmain"];
	return validateInputParameter(oFormObject);
}

function exportFile(action) {
	submitForm(action);
}
</script> 
<title>Parsed Data</title>
</head>
<% 
ParsedDataTable o_parseddatatable = null;
String activeTabFlag = null;
Map<String, Set<String>> groupMap = null;
Set<String> subGroupSet = null,
			collationgroup = null;
String parsedFileType = null;
parsedFileType = (String)request.getAttribute("parseFileType");
o_parseddatatable = (ParsedDataTable)request.getAttribute("parseddatatable");
request.getSession().setAttribute("parseDataTable", o_parseddatatable);
activeTabFlag = (String)request.getAttribute("activeTabFlag");
FileParsingConfiguration fileParseConfig = (FileParsingConfiguration)request.getAttribute("filePassConfig");
groupMap = (Map<String, Set<String>>)request.getAttribute("groupMap");
collationgroup = (Set<String>)request.getAttribute("collationgroup");
String fileType = null;
if(o_parseddatatable.isColumnPresent("fileType"))
	fileType = o_parseddatatable.getColumnValues("fileType").get(0).displayValue().toString();


%>
<body>
	<csrf:form name="frmain" id="frmain" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
		<input type="hidden" id="portal_appid" name="portal_appid" value="SOURCE_DOC_UTILS_PORTAL_APP" >
		<input type="hidden" id="portal_action" name="portal_action" value="">
		<input type="hidden" id="activeTabFlag" name="activeTabFlag" value="">
		<div id="mainContent">
			<%@include file="uiFormCreationMethods.jsp"%>
			<div class="page-title">
				<div class="title_left">
					<h3><%=AppRegistry.getToolsApp().getAppName() %> - <%=parsedFileType %></h3>
				</div>
			</div>
			<div class ="row">
				<div class="x_panel">
	   				<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs wizard_steps anchor" role="tablist">
							<li role="summary" class="active">
								<a href="#tab-1" role="tab" id="" data-toggle="tab" aria-expanded="true" tabindex=1 class="selected" 
								isdone="1" rel="1">
									Summary
								</a>
							</li>
							<li role="critical">
								<a href="#tab-2" role="tab" id="" data-toggle="tab" aria-expanded="true" tabindex=2	isdone="1" rel="1">
									Critical
								</a>
							</li>
							<%int tabIndex = 3;
							if(fileType.equalsIgnoreCase("MESA"))
							{
								for (String collationGroup : fileParseConfig.getCollationGroupList()){ %>
								<li role="data" >
									<a  href="#tab-<%=tabIndex%>" role="tab" id="" data-toggle="tab" aria-expanded="true" tabindex=3 isdone="1" rel="1">
										<%=collationGroup %>
									</a>
								</li>
								<%tabIndex++;
								}	
							}
							else
							{
							for (String group : groupMap.keySet()){ %>
								<li role="data" >
									<a  href="#tab-<%=tabIndex%>" role="tab" id="" data-toggle="tab" aria-expanded="true" tabindex=3 isdone="1" rel="1">
										<%=group %>
									</a>
								</li>
								<%tabIndex++;
								}
								}%>
							<div align= "right" style="font-size: 15px"> <!-- By  Match Status Export -->
								<!-- <A id = "exportStatus" HREF="javascript:exportFile('exporttext');">
									<i class="glyphicon glyphicon-save-file">
									</i>
									Export as Text
								</A>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<A id = "exportStatus" HREF="javascript:exportFile('exportexcel');"> -->
								 <A id = "exportStatus" HREF="javascript:exportFile('exportcsv');">
									<i class="glyphicon glyphicon-save-file">
									</i> Export as CSV
								</A>
							</div>
						</ul>
						
						<div id="myTabContent" class="tab-content stepContainer">
							<div role="tabpanel" id="tab-1" aria-labelledby="Tab 1" class="tab-pane fade content  active in ">
								<div class="x_content">
								<%if(o_parseddatatable != null) {%>
									<!-- <div class="form-group">
										<label class="control_label col-md-2 col-sm-2 col-xs-12 ">
											Parsing Status: </label>
										<div class="col-md-3 col-sm-2 col-xs-12">
												100% Success
										</div>
									</div> -->
									
									<div class="form-group">
										<label class="col-md-2 col-sm-2 col-xs-12 ">
											Total Fields: </label>
										<div class="col-md-3 col-sm-2 col-xs-12">
												<%=o_parseddatatable.colCount()%>
										</div>
									</div> 
									
									<div class="form-group">
										<label class="col-md-2 col-sm-2 col-xs-12 ">
											Parsed Fields: </label>
										<div class="col-md-3 col-sm-2 col-xs-12">
												<%=o_parseddatatable.colCount()%>
										</div>
									</div> 
									
									<%-- <div class="form-group">
										<label class="col-md-2 col-sm-2 col-xs-12 ">
											Missing Data: </label>
										<div class="col-md-3 col-sm-2 col-xs-12">
												<%=SourceDocUtilPortalApp.setDataCount(o_parseddatatable.colCount(), o_parseddatatable)%>
										</div>
									</div>  --%>
								<%}else { %>
									No Data Found
								<%} %>
								</div>
							</div>
							
							<div role="tabpanel" id="tab-2" aria-labelledby="Tab 2" class="tab-pane fade content ">
							<div class="x_content">
									 <%if(o_parseddatatable != null) {%>
									<table class="table table-striped">
										<%for (int nIndex = 0; nIndex < o_parseddatatable.colCount(); nIndex++) 
										 {
											String key = o_parseddatatable.getMnemonic(nIndex);
											if(fileParseConfig.getField(key) != null && fileParseConfig.getField(key).isCritical()
													&& fileParseConfig.getField(key).isDisplayOnScreen()){%>
											<tr>
												<td class = "col-md-4 col-sm-4 col-xs-12 "> <b><%=fileParseConfig.getField(key).getFieldLabel()%></b></td>
													<td><%=SourceDocUtilPortalApp.keyDisplayValue(o_parseddatatable.getColumnValues(nIndex).get(0).displayValue()) %></td>
											</tr>
										<%}}%>
									</table>
									<%}else{ %>
									No Data Found
									<%} %>
								</div>
							</div>
							
							<%int tabPanelIndex = 3 ; 
							
							if(fileType.equalsIgnoreCase("MESA"))
							{
								String colgroup = null;
								//String fileType = null;
								ParsedDataValue keyCountValue = null;
								String m_sLabel = null;
								int repeatCountForGroup = 0;
								int repeatCountForSubGroup = 0;
								String mnemonic = null;
								String displayValue = null;
								String groupNamewithCount= null;
								String keyName = null;
								String subGroupNamewithCount = null;
								
								for(String collationGroup : fileParseConfig.getCollationGroupList() )
								{%>
								<div role="tabpanel" id="tab-<%=tabPanelIndex %>" aria-labelledby="Tab <%=collationGroup %> 3" class="tab-pane fade content">
								<div class="x_content">
									<%for (String group : fileParseConfig.getGrouplistByCollationGroup(collationGroup)){
										%>
										 <div class="x_panel">
											<div class="x_title">
												<h2>
														<%=group %>
												</h2>
												<ul class="nav navbar-right panel_toolbox">
                      								<li>
                      									<a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                      								</li>
                     							</ul>
												<div class="clearfix"></div>
											</div>
											<div>
										 	<div class="x_content">
											 	<table class="table table-striped">
										<%if(o_parseddatatable != null) {
										String subGroupValue = null;
										if(o_parseddatatable.isColumnPresent(group+"_Count"))
													{
											keyCountValue = o_parseddatatable.getColumnValues(group+"_Count").get(0);
											repeatCountForGroup = Integer.valueOf(keyCountValue.displayValue().toString());
															
															
															//tempKey = propValue;
											for(int groupIndex=1; groupIndex<= repeatCountForGroup; groupIndex++)
															{
												groupNamewithCount = group+groupIndex;
												
												%>
												
												<tr><th style="font-size: 16px"><br> <%=groupNamewithCount %><br></th></tr>
												
												<%
												for(String subgroup : fileParseConfig.getsubGroupListByGroupName(group))
																{
													if(subgroup.equalsIgnoreCase(group))
																	{
														for(FileParsingField tField: fileParseConfig.getFieldListBySubGroup(subgroup))
																		{
																			
																			keyName = tField.getMnemonic();
																			m_sLabel = tField.getFieldLabel();
																			mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
															mnemonic = mnemonic.replace(" ", "");
															mnemonic = mnemonic.replace("/", "");
															
																			if(o_parseddatatable.isColumnPresent(mnemonic))
																				displayValue = o_parseddatatable.getColumnValues(mnemonic).get(0).displayValue().toString();
																			else
																			{
																				//when it does not found the key
																				//it should never come to this block
																				displayValue = "";
																			}
																			
																			%>
																			<tr>
																				<td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=m_sLabel%> </b></td>
																				<td><%=displayValue %></td>
																			</tr>
																			<%
																			
																		}
																	}
																	else  //subgroup is different than group
																	{
																		if(o_parseddatatable.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
																		{
																			keyCountValue = o_parseddatatable.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
															repeatCountForSubGroup = Integer.valueOf(keyCountValue.displayValue().toString());
																			
															 for(int subgroupIndex=1; subgroupIndex<= repeatCountForSubGroup; subgroupIndex++)
																			{
																				subGroupNamewithCount = subgroup+subgroupIndex;
																%>
												
																<tr><th style="font-size: 16px"><br> <%=subGroupNamewithCount %><br></th></tr>
												
																<%
																				for(FileParsingField tField: fileParseConfig.getFieldListBySubGroup(subgroup))
																				{
																					
																					keyName = tField.getMnemonic();
																					m_sLabel = tField.getFieldLabel();
																					mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
																	mnemonic = mnemonic.replace(" ", "");
																	mnemonic = mnemonic.replace("/", "");
																	
																					if(o_parseddatatable.isColumnPresent(mnemonic))
																						displayValue =  o_parseddatatable.getColumnValues(mnemonic).get(0).displayValue().toString();
																					else
																					{
																						//when it does not found the key
																						//it should never come to this block
																						displayValue = "";
																					}
																					
																					%>
																					<tr>
																						<td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=m_sLabel%> </b></td>
																						<td><%=displayValue %></td>
																					</tr>
																					<%
																					
																				}
																				
																			}
																		}
																		else //if subgroup count is not present in the dataTable
																		{
																			for(FileParsingField tField: fileParseConfig.getFieldListBySubGroup(subgroup))
																			{
																				
																				keyName = tField.getMnemonic();
																				m_sLabel = tField.getFieldLabel();
																				mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
																mnemonic = mnemonic.replace(" ", "");
																mnemonic = mnemonic.replace("/", "");
																
																			if(o_parseddatatable.isColumnPresent(mnemonic))
																			displayValue = o_parseddatatable.getColumnValues(mnemonic).get(0).displayValue().toString();
																			else
																			{
																				//when it does not found the key
																				//it should never come to this block
																				displayValue = "";
																			}

																			%>
																			<tr>
																				<td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=m_sLabel%> </b></td>
																				<td><%=displayValue %></td>
																			</tr>
																			<%
																			}

																		}
																		
																	}
									
															}
															
															
															}
														}//if group count is not present in datatable
														else
														{
											/* if(!o_parseddatatable.isColumnPresent(group))
											{ */
																
													groupNamewithCount = group;
													for(String subgroup : fileParseConfig.getsubGroupListByGroupName(group))
																		{
														if(subgroup.equalsIgnoreCase(group))
																		{
															for(FileParsingField tField: fileParseConfig.getFieldListBySubGroup(subgroup))
																			{
																				
																				keyName = tField.getMnemonic();
																			m_sLabel = tField.getFieldLabel();
																				mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
																mnemonic = mnemonic.replace(" ", "");
																mnemonic = mnemonic.replace("/", "");
																
																			if(o_parseddatatable.isColumnPresent(mnemonic))
															{
																
																			displayValue = o_parseddatatable.getColumnValues(mnemonic).get(0).displayValue().toString();
															}
																			else
																			{
																			displayValue  ="";
																			}

																			%>
																			<tr>
																				<td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=m_sLabel%> </b></td>
																				<td><%=displayValue %></td>
																			</tr>
																			<%	
																			}
																		}
																		else  //subgroup is different than group
																		{
																			if(o_parseddatatable.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
																			{
																				keyCountValue = o_parseddatatable.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
																repeatCountForSubGroup = Integer.valueOf(keyCountValue.displayValue().toString());
																 for(int subgroupIndex=1; subgroupIndex<= repeatCountForSubGroup; subgroupIndex++)
																				{
																					subGroupNamewithCount = subgroup+subgroupIndex;
																	%>
												
																	<tr><th style="font-size: 16px"><br> <%=subGroupNamewithCount %><br></th></tr>
												
																	<%
																					for(FileParsingField tField: fileParseConfig.getFieldListBySubGroup(subgroup))
																					{
																						
																						keyName = tField.getMnemonic();
																						m_sLabel = tField.getFieldLabel();
																		m_sLabel = groupNamewithCount+" "+subGroupNamewithCount+" "+tField.getFieldLabel();
																						mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
																		mnemonic = mnemonic.replace(" ", "");
																		mnemonic = mnemonic.replace("/", "");
																		
																					if(o_parseddatatable.isColumnPresent(mnemonic))
																					displayValue =  o_parseddatatable.getColumnValues(mnemonic).get(0).displayValue().toString();
																					else
																					{
																						//when it does not found the key
																						//it should never come to this block
																						displayValue = "";
																					}

																					%>
																					<tr>
																						<td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=m_sLabel%> </b></td>
																						<td><%=displayValue %></td>
																					</tr>
																					<%	
																					}
																					
																				}
																			
																			}
																			else //if subgroup count is not present in the dataTable
																			{
																				for(FileParsingField tField: fileParseConfig.getFieldListBySubGroup(subgroup))
																				{
																					
																					keyName = tField.getMnemonic();
																					
																					m_sLabel = tField.getFieldLabel();
																	//m_sLabel = groupNamewithCount+" "+subgroup+" "+tField.getFieldLabel();
																					mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
																	mnemonic = mnemonic.replace(" ", "");
																	mnemonic = mnemonic.replace("/", "");
																	
																				if(o_parseddatatable.isColumnPresent(mnemonic))
																				displayValue = o_parseddatatable.getColumnValues(mnemonic).get(0).displayValue().toString();
																				else
																				{
																					//when it does not found the key
																					//it should never come to this block
																					displayValue = "";
																				}
																				%>
																				<tr>
																					<td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=m_sLabel%> </b></td>
																					<td><%=displayValue %></td>
																				</tr>
																				<%
																					
																				}

																			}
																			
																		}
																	}
											//	}
																}
													
										}//if ends of pardataTable not null
										%>										
										</table>
										</div>
									</div>	
								</div>
								<% } %>	
									</div>
								</div>
								<%
								tabPanelIndex ++;}
							}//end mesa loop here
												else
												{	
								for (String group : groupMap.keySet()){%>
								<div role="tabpanel" id="tab-<%=tabPanelIndex %>" aria-labelledby="Tab <%=group %> 3" class="tab-pane fade content">
									<div class="x_content">
										<%if(o_parseddatatable != null) {
											String subGroupValue = null;
											for(String subgroup : groupMap.get(group) )
											{%>
											 <div class="x_panel">
												<div class="x_title">
													<h2>
														<%=subgroup %>	
													</h2>
													<ul class="nav navbar-right panel_toolbox">
	                      								<li>
	                      									<a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
	                      								</li>
	                     							</ul>
													<div class="clearfix"></div>
												</div>
											 	<div class="x_content">
												 	<table class="table table-striped">
												 	
													<% 
													String colgroup = null;
												//	String fileType = null;
													ParsedDataValue keyCountValue = null;
													String m_sLabel = null;
													int repeatCount = 0;
													String mnemonic = null;
													String displayValue = null;
													String groupNamewithCount= null;
													String keyName = null;
													String subGroupNamewithCount = null;
													
													
													/* 
													else
													{ */	
												for (int nIndex = 0; nIndex < o_parseddatatable.colCount(); nIndex++) 
												 {
													//String key = SourceDocUtilPortalApp.datatableKey(o_parseddatatable.getMnemonic(nIndex));
													String key = o_parseddatatable.getMnemonic(nIndex);
													
													if(key.endsWith("_Count") && SourceDocUtilPortalApp.isFieldGroup(fileParseConfig,key,subgroup)  )
                                                    {
                                                        String mnKey = SourceDocUtilPortalApp.keyConfigKey(fileParseConfig, key);
                                                           if(!StringUtils.emptyString(colgroup).equals(fileParseConfig.getField(mnKey).getCollationgroup()))
                                                           {       colgroup = fileParseConfig.getField(mnKey).getCollationgroup();
                                                                  for(int count=1; count <=  Integer.parseInt((String)o_parseddatatable.getColumnValues(nIndex).get(0).displayValue());count++)
                                                                  {
                                                                         for(String multimnemonic :SourceDocUtilPortalApp.getCollationFieldsMnemonics(colgroup, fileParseConfig))
                                                                         {
                                                                                if(SourceDocUtilPortalApp.keyDisplayValueMutiField(o_parseddatatable, multimnemonic+"_"+count)!= null){
                                                                  %>
                                                                         <tr>
                                                                                <td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=fileParseConfig.getField(multimnemonic).getFieldLabel() +" "+count%> </b></td>
                                                                                <td><%=SourceDocUtilPortalApp.keyDisplayValueMutiField(o_parseddatatable, multimnemonic+"_"+count) %></td>
                                                                         </tr>
                                                                  <%}}
                                                                  }
                                                           }     
                                                    }
													else{
													if(fileParseConfig.getField(key) != null && SourceDocUtilPortalApp.isFieldGroup(fileParseConfig, key, subgroup)){%>
														<tr>
															<td class = "col-md-4 col-sm-4 col-xs-12"> <b><%=fileParseConfig.getField(key).getFieldLabel()%></b></td>
															<td><%=SourceDocUtilPortalApp.keyDisplayValue(o_parseddatatable.getColumnValues(nIndex).get(0).displayValue()) %></td>
														</tr>
														<% }
													}
												}
													//}
												
												%>
												</table> 
											</div>
										</div>	
										<%subGroupValue = subgroup;
										}%>
									<%}else{ %>
									No Data Found
									<%} %>
								</div>
							</div>
							<%tabPanelIndex ++;
								} 
							}%>	
						</div>
					</div>
				</div>
				
				<div align = "center">
					<button type="button" class="btn btn-success" onclick="submitForm('pdfParser.jsp');">Next File</button>
				</div>
			</div>
		</div>
	</csrf:form>
</body>					
</html>