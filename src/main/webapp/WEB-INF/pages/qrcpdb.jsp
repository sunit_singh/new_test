<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="com.sciformix.commons.SciConstants.StringConstants"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.commons.ObjectIdPair"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="com.sciformix.sciportal.apps.qr.QRReviewData"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRCPDashboardReport" %>
<%
	String									l_reviewdate		=	(String)	request.getAttribute("fldreviewdate");
	Integer									l_startdatecount	=	(Integer)	request.getAttribute("fldstartdatecount"),
											l_enddatecount		=	(Integer)	request.getAttribute("fldenddatecount"),
											l_searchtype		=	(Integer)	request.getAttribute("fldsearchtype");
	Map<String,List<QRCPDashboardReport>>	l_cpreportlist		=	(Map<String,List<QRCPDashboardReport>>) request.getAttribute("cpreport");
	List<ObjectIdPair<String,String>>		l_reviewtypelist	=	(List<ObjectIdPair<String,String>>) request.getAttribute("reviewtypelist");
	String									l_cataliases		=	(String) 	request.getAttribute("categoryaliases");	
	int										l_catcount			=	0;
	String []								l_categories		= 	null;
	boolean									l_isrecordexist		=	false;
	
	if (StringUtils.isNullOrEmpty(l_cataliases)) {
		
	} else {
		l_categories	=	l_cataliases.split("~");	
		l_catcount		=	l_categories.length;
	}
%>

<!DOCTYPE html>
<html>
	<head>
		<script src="js/moment/moment.min.js"></script>
		<script type="text/JavaScript">
			var startDate;
			var endDate;
			var startDateCount = <%= l_startdatecount%>;
			var endDateCount = <%= l_enddatecount%>;
			startDateCount	=	-startDateCount;
			endDateCount	=	-endDateCount;
			$(document).ready(function() {
			    $('#reportrange_right').daterangepicker( {
			   		startDate: moment().subtract(startDateCount, 'days'),
				    endDate: moment().subtract(endDateCount, 'days'),
				    minDate: '01/01/2012',
				    maxDate: moment(),
				    dateLimit: {
				    	days: 365
				    },
				    showDropdowns: true,
				    showWeekNumbers: true,
				    timePicker: false,
				    timePickerIncrement: 1,
				    timePicker12Hour: true,
				    ranges: {
				    	'Today': [moment(), moment()],
				        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				        'This Month': [moment().startOf('month'), moment()],
				        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				    },
				    opens: 'right',
				    buttonClasses: ['btn btn-default'],
				    applyClass: 'btn-small btn-primary',
				    cancelClass: 'btn-small',
				    format: 'MM/DD/YYYY',
				    separator: ' to ',
				    locale: {
					    applyLabel: 'Submit',
					    cancelLabel: 'Clear',
					    fromLabel: 'From',
					    toLabel: 'To',
					    customRangeLabel: 'Custom',
					    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
					    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
					    firstDay: 1
				    }
				},
			    function(start, end) {
			        $('#reportrange_right span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
			        startDate = start.startOf('day');
			        endDate = end.startOf('day');
			    }
			    );
			    $('#reportrange_right span').html(moment().subtract(startDateCount, 'days').format('D MMMM YYYY') + ' - ' + moment().subtract(endDateCount, 'days').format('D MMMM YYYY'));
			    startDate 	=	moment().startOf('day').subtract(startDateCount, 'days');
			    endDate		=	moment().startOf('day').subtract(endDateCount, 'days');
			    
			    $('[data-toggle="tooltip"]').tooltip();
			 });
				
			function fnshowreport () {
				clearErrorMessages();
				document.getElementById("portal_action").value = "preparecpdb";
				var	todaydate	=	moment().startOf('day');
				document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
				document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days');
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			
			function fnback () {
				document.frmmain.portal_action.value	=	"";
				document.frmmain.submit();
			}
			function fnexport () {
				clearErrorMessages();
				document.getElementById("portal_action").value = "cpdbexport";
				var	todaydate	=	moment().startOf('day');
				document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
				document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days');
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldstartdatecount" name="fldstartdatecount" value="">
			<input type="hidden" id="fldenddatecount" name="fldenddatecount" value="">
			
			<div class="title_left">
				<h3>CP Dashboard Summary</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							CP Dashboard Summary
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
	                  
	                    <div class="form-group">
	                    	<label class="control-label col-md-3 col-sm-3 col-xs-12">
								Review Date
							</label>
							<div class="col-md-4 col-sm-4 col-xs-12">
			                    <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
		                			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		                			<span></span> <b class="caret"></b>
		                		</div>
		                	</div>	
	                	</div>	
	                	
						<div class="form-group">
				    		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="button" class="btn btn-success" onclick="fnshowreport();" value="Search">
									Search
								</button>
							</div>
						</div>	
						
						<% if (l_cpreportlist != null && l_cpreportlist.size() > 0) { 
							for (ObjectIdPair<String,String> l_reviewtypedata : l_reviewtypelist) { %>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2>
											<%= l_reviewtypedata.getObjectName() %>
										</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content" style="display: block;">
										<table class="table table-striped table-bordered">
											<thead>
												<tr>
													<th></th>
													<th colspan="4" style="text-align:center">Reviewed Case Count Details</th>
													<th></th>
													<% if (l_catcount == 2) { %>
														<th colspan="2" style="text-align:center">Category Wise Field Error Count</th>
														<th colspan="2" style="text-align:center">Category Wise Quality</th>
													<% } else if (l_catcount == 3) { %>	
														<th colspan="3" style="text-align:center">Category Wise Field Error Count</th>
														<th colspan="3" style="text-align:center">Category Wise Quality</th>
													<% } else { %>
														<th colspan="3" style="text-align:center">Field Count Details</th>
													<% } %>
													<th></th>
												</tr>
											</thead>
											<thead>
												<tr>
													<th class="col-md-2 col-sm-12 col-xs-12 form-group">CP Name</th>
													<th>Total</th>
													<th>Passed (Without Error)</th>
													<th>Passed (With Error)</th>
													<th>Failed</th>
													<th>Average Quality</th>
													<% if (l_catcount > 1) { %>
														<th><%= l_categories[0] %></th>
														<th><%= l_categories[1] %></th>
														<% if (l_catcount > 2) { %>
															<th><%= l_categories[2] %></th>
														<% } %>
														<th><%= l_categories[0] %></th>
														<th><%= l_categories[1] %></th>
														<% if (l_catcount > 2) { %>
															<th><%= l_categories[2] %></th>
														<% } %>
													<% } %>
													<% if (l_catcount == 1 || l_catcount == 0) { %>
														<th>Fields Reviewed</th>
														<th>Fields Correct</th>
														<th>Fields Incorrect</th>
													<% } %>
													<th>Case Quality</th>
													
												</tr>
											</thead>
											<tbody> <%
												if (l_cpreportlist.get(l_reviewtypedata.getObjectSeqId()) != null && l_cpreportlist.get(l_reviewtypedata.getObjectSeqId()).size() > 0) {
													l_isrecordexist	=	true;
													for (QRCPDashboardReport l_cpreport : l_cpreportlist.get(l_reviewtypedata.getObjectSeqId())) { %>
														<tr>
															<td class="col-md-2 col-sm-12 col-xs-12 form-group"><%= l_cpreport.getUserDisplayName() %></td>
															<td><%= l_cpreport.getTotalCaseReviewed() %></td>
															<td><%= l_cpreport.getTotalCaseWithoutError() %></td>
															<td><%= l_cpreport.getTotalCasePassed() - l_cpreport.getTotalCaseWithoutError() %></td>
															<td><%= l_cpreport.getTotalCaseReviewed() - l_cpreport.getTotalCasePassed()%></td>
															<% if (l_cpreport.getTotalAvgQuality() == 0) { %>
																<td>0</td>
															<% } else { %>
																<td><%= BigDecimal.valueOf(l_cpreport.getTotalAvgQuality()).setScale(2, BigDecimal.ROUND_HALF_UP) %>(%)</td>
															<% } %>
																
															<% if (l_catcount > 1) { %>
																<td><%= l_cpreport.getTotalCat1FieldsError() %></td>
																<td><%= l_cpreport.getTotalCat2FieldsError() %></td>
																<% if (l_catcount > 2) { %>
																	<td><%= l_cpreport.getTotalCat3FieldsError() %></td>
																<% } %>
																<% if (l_cpreport.getTotalCat1Quality() == 0) { %>
																	<td>0</td>
																<% } else { %>
																	<td><%= BigDecimal.valueOf(l_cpreport.getTotalCat1Quality()).setScale(2, BigDecimal.ROUND_HALF_UP) %>(%)</td>
																<% } %>
																<% if (l_cpreport.getTotalCat2Quality() == 0) { %>
																	<td>0</td>
																<% } else { %>
																	<td><%= BigDecimal.valueOf(l_cpreport.getTotalCat2Quality()).setScale(2, BigDecimal.ROUND_HALF_UP) %>(%)</td>
																<% } %>
																<% if (l_catcount > 2) { %>
																	<% if (l_cpreport.getTotalCat3Quality() == 0) { %>
																		<td>0</td>
																	<% } else { %>
																		<td><%= BigDecimal.valueOf(l_cpreport.getTotalCat3Quality()).setScale(2, BigDecimal.ROUND_HALF_UP) %>(%)</td>
																	<% } %>
																<% } %>
															<% } %>
															<% if (l_catcount == 1 || l_catcount == 0) { %>
																<td><%= l_cpreport.getTotalFieldsReviewed() %></td>
																<td><%= l_cpreport.getTotalFieldsPassed() %></td>
																<td><%= l_cpreport.getTotalFieldsFailed() %></td>
															<% } %>
															<td><%=  BigDecimal.valueOf(l_cpreport.getTotalCaseQuality()).setScale(2, BigDecimal.ROUND_HALF_UP) %>(%)</td>
														</tr>
													<% } %>
												<% } else { %>
													<tr>
														<% if (l_catcount == 2) { %>
															<td colspan="15">&nbsp;
																<i>No Case Processor Record Found</i>
															</td>
														<% } else if (l_catcount == 3) { %>
															<td colspan="18">&nbsp;
																<i>No Case Processor Record Found</i>
															</td>
														<% } else { %>
															<td colspan="10">&nbsp;
																<i>No Case Processor Record Found</i>
															</td>
														<% } %>
													</tr>
												<% } %>
											</tbody>
										</table>
									</div>
								</div>
							</div>				
						 <% } %>
						<% } else { %>
							<tr>
								<td colspan="10">
									<i>There are no quality review performed in the system.</i>
								</td>
							</tr>
						<% } %>
							
						
						<div class="ln_solid"></div>
						<button type="button" class="btn btn-primary" onclick="fnback();" value="Back">
							Back
						</button>
						<% if (l_isrecordexist) {  %>
							<button type="button" class="btn btn-primary" onclick="fnexport();" value="Export">
								Export
							</button>
						<% } %>	
						
					</div>
				</div>
			</div>
		</csrf:form>	
	</body>
</html>