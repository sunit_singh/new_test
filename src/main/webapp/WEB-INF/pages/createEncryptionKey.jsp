<%@page import="com.sciformix.sciportal.apps.sys.config.EncryptionKey"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Encryption Key Creation</title>
<script type="text/javascript">
function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["frmmain"];
	return validateInputParameter(oFormObject);
}

function submitform(action)
{
	if(!validateKeyGenetationParameter() &&  validateFormParameters()){ 
		document.frmmain.portal_action.value	=	action;
		document.frmmain.submit();
	}
}

function cancelform(action)
{
	document.frmmain.submit();
}

function validateKeyGenetationParameter()
{
	clearErrorMessages();
	var bErrorDetected = false;
	var keyidregex = /^[A-Za-z0-9]*$/;
	var keytype=document.getElementById('encrType').value;
	var keypurpose=document.getElementById('encrPurpose').value;
	var keyid=document.getElementById('keyId').value;
	
	if(keytype==""||keytype.trim()==""){
		displayErrorMessage("UI-1089");
		bErrorDetected = true;
	}else if(keypurpose==""||keypurpose.trim()=="")
	{
		displayErrorMessage("UI-1090");
		bErrorDetected = true;
	}
	else if(keyid==""||keyid.trim()=="")
	{
		displayErrorMessage("UI-1091");
		bErrorDetected = true;
	}
	else if(!keyidregex.test(keyid))
	{
		displayErrorMessage("UI-1092");
		bErrorDetected = true;
	}	
	return bErrorDetected;
}
</script>
</head>
<% 
	String activeTab = null;
	activeTab = (String)request.getAttribute("activeTab");
 %>
<body>
<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="SYSCONFIG-PORTAL-APP">
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="active_tab" name="active_tab" value="<%=activeTab%>">
	
	<div role="main">
		<div class="page_title">
			<div class="title_left">
				<h3>Create Encryption Key</h3>
			</div>
		</div>
		<div class="clearfix">
		</div>
		<div class="row">
			<div class="x_panel">
				<div class="x_title ">
					<h2>
						Fill Encryption Key Details
					</h2>
					<div class="clearfix">
				</div>
				</div>
				<div class="x_content" >
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
							Key Type
							<span class="required"> *</span>
						</label>
						<div class="col-md-4 col-sm-4 col-xs-12">
				        	<select class="form-control" name="encrType" id="encrType">
				            	<option value ="" ></option>
				            	<option value ="<%=EncryptionKey.KeyType.PKI %>" ><%=EncryptionKey.KeyType.PKI.getDisplayName()%></option>
				            </select> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
							Key Purpose
							<span class="required"> *</span>
						</label>
						<div class="col-md-4 col-sm-4 col-xs-12">
				        	<select class="form-control" name="encrPurpose" id="encrPurpose">
				            	<option value =""></option>
				            	<option value ="<%=EncryptionKey.KeyPurpose.WAPI%>" ><%=EncryptionKey.KeyPurpose.WAPI.getDisplayName()%></option>
				            
				            </select> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
							Key Id
							<span class="required"> *</span>
						</label>
						<div class="col-md-4 col-sm-4 col-xs-12">
				        	<input type = "text" class = "form-control" id = "keyId" name ="keyId"/> 
						</div>
					</div>
					<div class="ln_solid"></div>
					<div id = "button div" class= "col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
						<button type="button" class="btn btn-primary" onclick="cancelform()">Cancel</button>
						<button type="button" class="btn btn-success" onclick="submitform('saveencryptionkey')">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>					
</csrf:form>				
</body>
</html>