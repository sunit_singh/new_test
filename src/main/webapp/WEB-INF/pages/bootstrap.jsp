<%@page import="com.sciformix.sciportal.utils.SystemUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bootstrap Page for System Admin</title>
</head>
<body>
<%@include file="../../errorPage.jsp" %>
<%
//TODO:autocomplete="off"
String sDBTestSuccessful = (String)request.getAttribute("s_dbtestsuccessful");
String sLDAPSaveSuccessful = (String)request.getAttribute("s_ldapsavesuccessful");
String sNewPortalActionLocal = (String) request.getAttribute("new-portalaction");
%>

<csrf:form name="bootstrap_form" id="bootstrap_form" method="post" action="BootstrapServlet">
<%

if(sNewPortalActionLocal.equals("env_not_configured"))
{ 
%>
Environmental variable for installation folder not provided. Please update the variable and restart the bootstrap process.
<br/><br/>
<%	
}
else if(sNewPortalActionLocal.equals("already_bootstrapped"))
{ 
%>
Instance already bootstrapped. Please take backup of existing configuration files before proceeding with the bootstrap.
<br/><br/>
<input type="button" class="btn btn-default" value="Reset Bootstrap" onclick="javascript:validateAndSubmit('reset_bootstrap')" />
<input type="button" class="btn btn-default" value="Exit" onclick="javascript:validateAndSubmit('exit')" />

<%	
}
else if(sNewPortalActionLocal.equals("get_db_details"))
{
	String sDBHost = request.getParameter("dbhost") != null? request.getParameter("dbhost"): "";
	String sDBPort = request.getParameter("dbport") != null? request.getParameter("dbport"): "";
	String sDBSID = request.getParameter("dbsid") != null? request.getParameter("dbsid"): "";
	String sDBUsername = request.getParameter("dbusername") != null? request.getParameter("dbusername"): "";
	String sDBPassword = request.getParameter("dbpassword") != null? request.getParameter("dbpassword"): "";
	String sDBSchemaOwner = request.getParameter("dbschemaowner") != null? request.getParameter("dbschemaowner"): "";
%>
	<table>
		<tr><td><b>Database Configuration</b></td></tr>
		<tr>
			<td>Host</td>
			<td><input type="text" name="dbhost" id="dbhost" value="<%= sDBHost %>" /></td>
		</tr>
		<tr>
			<td>Port</td>
			<td><input type="text" name="dbport" id="dbport" value="<%= sDBPort %>"/></td>
		</tr>
		<tr>
			<td>SID</td>
			<td><input type="text" name="dbsid" id="dbsid" value="<%= sDBSID %>"/></td>
		</tr>		
		<tr>
			<td>Connect User</td>
			<td><input type="text" name="dbusername" id="dbusername"value="<%= sDBUsername %>" /></td>
		</tr>
		
		<tr>
			<td>Connect Password</td>
			<td><input type="password" name="dbpassword" id="dbpassword" value="<%= sDBPassword %>"/></td>
		</tr>
		<tr>
			<td>Schema Owner</td>
			<td><input type="text" name="dbschemaowner" id="dbschemaowner" value="<%= sDBSchemaOwner %>"/></td>
		</tr>
	
	</table>
<br/>
<input type="button" class="btn btn-default" value="Continue" onclick="javascript:validateAndSubmit('save_db')" />
<%
}
else if(sNewPortalActionLocal.equals("ldap_details_exist"))
{ 
%>
LDAP details already exist.
<br/><br/>
<input type="button" class="btn btn-default" value="Recreate LDAP settings" onclick="javascript:validateAndSubmit('recreate_ldap')" />
<input type="button" class="btn btn-default" value="Exit" onclick="javascript:validateAndSubmit('exit')" />

<%	
}
else if(sNewPortalActionLocal.equals("get_ldap_details"))
{ 
	String sLDAPDomain = request.getParameter("ldapdomain") != null? request.getParameter("ldapdomain"): "";
	String sLDAPUrl = request.getParameter("ldapurl") != null? request.getParameter("ldapurl"): "";
	String sLDAPUsername = request.getParameter("ldapusername") != null? request.getParameter("ldapusername"): "";
	String sLDAPPassword = request.getParameter("ldappassword") != null? request.getParameter("ldappassword"): "";
%>

	<table>
		<tr><td><b>LDAP Configuration</b></td></tr>
		<tr>
			<td>Domain</td>
			<td><input type="text" name="ldapdomain" id="ldapdomain" value="<%= sLDAPDomain %>"/></td>
		</tr>
		<tr>
			<td>URL</td>
			<td><input type="text" name="ldapurl" id="ldapurl" value="<%= sLDAPUrl %>"/></td>
		</tr>
		<tr>
			<td>Username</td>
			<td><input type="text" name="ldapusername" id="ldapusername" value="<%= sLDAPUsername %>" /></td>
		</tr>		
		<tr>
			<td>Password</td>
			<td><input type="password" name="ldappassword" id="ldappassword" value="<%= sLDAPPassword %>"/></td>
		</tr>
	</table>
<br/>
<input type="button" class="btn btn-default" value="Save" onclick="javascript:validateAndSubmit('save_ldap')" />
<%
}
else if (sNewPortalActionLocal.equals("bootstrap_complete"))
{
%>
Bootstrap complete. Kindly restart the server for applying the changes.
<%	
}
else
{
	//Do nothing
}
%>	
<br/><br/>
<% 
if(sDBTestSuccessful !=null && sDBTestSuccessful.equalsIgnoreCase("false"))
{
%>
	<script>
	displayErrorMessage("DB Connection failed. Kindly confirm the values");
	</script>
<%
}
else
{
	//Do nothing
}

if(sLDAPSaveSuccessful !=null && sLDAPSaveSuccessful.equalsIgnoreCase("false"))
{
	
%>
	<script>
	displayErrorMessage("LDAP Connection failed. Kindly confirm the values");
	</script>
<%
}
else
{
	//Do nothing
}
%>

<input type="hidden" id="bootstrap_action" name="bootstrap_action" value="">
<input type="hidden" id="portal_action" name="portal_action" value="">

</csrf:form>	
</body>
<script>
function validateAndSubmit(action) 
{
	clearErrorMessages();
	
	var bErrorDetected = false;
	
	if(action == "save_db")
	{
		var sDBHost = document.getElementById("dbhost").value;
		var sDBPort = document.getElementById("dbport").value;
		var sDBSid = document.getElementById("dbsid").value;
		var sDBUsername = document.getElementById("dbusername").value;
		var sDBPassword = document.getElementById("dbpassword").value;
		var sDBSchemaOwner = document.getElementById("dbschemaowner").value;
		
		
		if(sDBHost == "")
		{
			displayErrorMessage("Please specify DB Host");
			//return false;
			bErrorDetected = true;
		}
		
		if(sDBPort == "")
		{
			displayErrorMessage("Please specify DB Port");
			//return false;
			bErrorDetected = true;
		}
		
		if(sDBSid == "")
		{
			displayErrorMessage("Please specify DB SID");
			//return false;
			bErrorDetected = true;
		}
		
		if(sDBUsername == "")
		{
			displayErrorMessage("Please specify DB Connect User");
			//return false;
			bErrorDetected = true;
		}
		
		if(sDBPassword == "")
		{
			displayErrorMessage("Please specify DB Connect Password");
			//return false;
			bErrorDetected = true;
		}
		
		if(sDBSchemaOwner == "")
		{
			displayErrorMessage("Please specify DB Schema Owner");
			//return false;
			bErrorDetected = true;
		}
	}
	else if(action == "save_ldap")
	{
		var sLDAPDomain = document.getElementById("ldapdomain").value;
		var sLDAPUrl = document.getElementById("ldapurl").value;
		var sLDAPUsername = document.getElementById("ldapusername").value;
		var sLDAPPassword = document.getElementById("ldappassword").value;

		if(sLDAPDomain == "")
		{
			displayErrorMessage("Please specify LDAP Domain");
			//return false;
			bErrorDetected = true;
		}
		
		if(sLDAPUrl == "")
		{
			displayErrorMessage("Please specify LDAP URL");
			//return false;
			bErrorDetected = true;
		}
		
		if(sLDAPUsername == "")
		{
			displayErrorMessage("Please specify LDAP Username");
			//return false;
			bErrorDetected = true;
		}
		
		if(sLDAPPassword == "")
		{
			displayErrorMessage("Please specify LDAP Password");
			//return false;
			bErrorDetected = true;
		}
	 	//return (!bErrorDetected);
	}
	else if(action == "reset_bootstrap" || action == "recreate_ldap" || action == "exit")
	{
		//Continue
		
	}	
	else 
	{
		alert("Illegal action");
		return false
	}
	
	if(!bErrorDetected)
	{
		var form = document.getElementById("bootstrap_form");
		document.getElementById("bootstrap_action").value = action;
		form.submit();
	}
	else
	{
		return false;
	}
}
</script>		
</html>