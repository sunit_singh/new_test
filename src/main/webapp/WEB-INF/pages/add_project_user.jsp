<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDetailsDb"%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">

function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}

function addUsers(value, id)
{
	clearErrorMessages();
	
	var validity= false;
	  if(value=='addUser')
		{
		var validity =validateInput();
		var formName=document.getElementById("PROJSETTINGS_form");
		document.getElementById("portal_action").value = value;
		document.getElementById("projsettings_appid").value ='PROJCET-USER-MANGEMENT';
		document.getElementById("selectedProjectId").value =id;
		
		if (!validateInput() && validateFormParameters())
		 {
			formName.submit();
		 }
	}
}

function cancel(value, id)
{
	
		var formName=document.getElementById("PROJSETTINGS_form");
		document.getElementById("portal_action").value = value;
		document.getElementById("projsettings_appid").value ='PROJCET-USER-MANGEMENT';
		document.getElementById("selectedProjectId").value =id;
		formName.submit();
	
}

 function validateInput()
{
	clearErrorMessages();
	var bErrorDetected = false;
	var anyBoxesChecked = false;
	var pattern = new RegExp (/[a-zA-Z0-9][a-zA-Z0-9\\s_-]+[a-zA-Z0-9]/);	
	var emailPattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	
    $('#' + 'PROJSETTINGS_form' + ' input[type="checkbox"]').each(function() {
       if ($(this).is(":checked")) {
    	 
            anyBoxesChecked 		= true;
            var clientName 			=  document.getElementById("__clientName"+this.value).value;
		    var clientEmailId 		=  document.getElementById("__clientEmailId"+this.value).value;
			var clientSafetyDBId 	=  document.getElementById("__clientSafetyDBId"+this.value).value;
      
       
       if(anyBoxesChecked==false){
	   		displayErrorMessage("UI-1026");
	   		bErrorDetected = true;
  		} 
  		else if(clientName==""||clientName.trim()=="")
  		{
			displayErrorMessage("UI-1074");
			bErrorDetected = true;
			
		} else if(!pattern.test(clientName))
		{	
			displayErrorMessage("UI-1075");
			bErrorDetected = true;
			
		}else if(clientEmailId==""||clientEmailId.trim()=="")
		{	
			displayErrorMessage("UI-1076");
			bErrorDetected = true;
			
		} else if(!emailPattern.test(clientEmailId))
		{
			displayErrorMessage("UI-1077");
			bErrorDetected = true;
			
		} else if(clientSafetyDBId==""||clientSafetyDBId.trim()=="")
		{
			displayErrorMessage("UI-1078");
			bErrorDetected = true;
		} 
	  
    	}
       
   });
       
    return bErrorDetected;   
}		
  
function updateUser(value, pId, uId)
{
	var formName=document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = value;
	document.getElementById("selectedUserId").value =uId;
	document.getElementById("projsettings_appid").value ='PROJCET-USER-MANGEMENT';
// 	formName.submit();
	if (validateFormParameters())
	 {
		formName.submit();
	 }

}

function funEnableText(uId)
{
	var selid = document.getElementById('selectUser'+uId).value;
	 $('#' + 'PROJSETTINGS_form' + ' input[type="checkbox"]').each(function() {
	       if ($(this).is(":checked")) {
	    	   	document.getElementById('__clientName'+uId).disabled = false;
	    	    document.getElementById('__clientEmailId'+uId).disabled = false;
	    	    document.getElementById('__clientSafetyDBId'+uId).disabled = false;
	       	}});
	 
	 if (document.getElementById('selectUser'+uId).checked == false) {
    	 	document.getElementById('__clientName'+uId).disabled = true;
  	    	document.getElementById('__clientEmailId'+uId).disabled = true;
  	    	document.getElementById('__clientSafetyDBId'+uId).disabled = true; 
  	  		document.getElementById('__clientName'+uId).value = "";
	    	document.getElementById('__clientEmailId'+uId).value = "";
	    	document.getElementById('__clientSafetyDBId'+uId).value = ""; 
  	    }
}

</script>
<% ProjectInfo p_oProjectInfo= (ProjectInfo)request.getAttribute("selectedProjectInfo");
	String opMode=(String)request.getAttribute("selectedOperation");
	List<UserInfo> u_UserList= (List<UserInfo>)request.getAttribute("usersList");
%>

</head>
<body>
<csrf:form name="PROJSETTINGS_form" id="PROJSETTINGS_form" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="selectedProjectId" name="selectedProjectId" value="">
	<input type="hidden" id="selectedUserId" name="selectedUserId" value="">
	<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="">
	
<div class="page-title">
	<div class="title_left">
		<h3>Add User </h3>
	</div>
</div>
<input type ="hidden" id="updateUserId" name="updateUserId">
<div class ="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h3>
						<small>Add Users to Project</small> 	
					</h3>
					<div class="clearfix"></div>
				</div>
				
				<div class="x_content">
					<div class="form-group">
						<label class="col-md-2 col-sm-3 col-xs-12 ">
							Project Id: </label>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<label>
								<%=p_oProjectInfo.getProjectSequenceId()%>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 col-sm-3 col-xs-12 ">
							Project Name: </label>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<label>
								<%=p_oProjectInfo.getProjectName()%>
							</label>
						</div>
					</div> 
					
					<!-- User List -->
					<div class="x_panel">
						<div class="x_title">
							<h3>
								<small>User List</small> 	
							</h3>
						<div class="clearfix"></div>
						</div>
						<div class="x_content">
							
							<table class="table table">
								<thead>
									<tr>
										<th></th>
										<th>User Name</th>
										<th>Client Name</th>
										<th>Client Email Id</th>
										<th>Client Safety DB Id</th>
									</tr>
								</thead>
								<tbody>
									<%if(u_UserList.isEmpty())
									{
										%>
										<tr>
										<td colspan="7">&nbsp;<i>No records found</i></td>
										</tr>
										<%
									}else {
									for(UserInfo u_oUserInfo: u_UserList)
									{%>
									<tr>
										<td>
											<input type="checkbox" id="selectUser<%=u_oUserInfo.getUserSeqId()%>" name="selectUser" value="<%=u_oUserInfo.getUserSeqId()%>" onclick="funEnableText(<%=u_oUserInfo.getUserSeqId()%>)">
										</td>
										<td><%=u_oUserInfo.getUserDisplayName() %></td>
										<td><input type="text" id="__clientName<%=u_oUserInfo.getUserSeqId()%>" name="__clientName<%=u_oUserInfo.getUserSeqId()%>" value="" disabled="disabled"></td>
										<td><input type="text" id="__clientEmailId<%=u_oUserInfo.getUserSeqId()%>" name="__clientEmailId<%=u_oUserInfo.getUserSeqId()%>" value="" disabled="disabled"></td>
										<td><input type="text" id="__clientSafetyDBId<%=u_oUserInfo.getUserSeqId()%>" name="__clientSafetyDBId<%=u_oUserInfo.getUserSeqId()%>" value="" disabled="disabled"></td>
									</tr>
									<%}}%>
									
								</tbody>
							</table>
						</div>
					</div> <!-- User list x panel ends -->
										
					<%
					if(!u_UserList.isEmpty())
									{%>
						<button type="button" id="submitEditForm" class="btn btn-success" 
						onclick="cancel('cancel','<%=p_oProjectInfo.getProjectSequenceId()%>')">Cancel</button>
						<button type="button" id="submitEditForm" class="btn btn-success"
						onclick="addUsers('addUser','<%=p_oProjectInfo.getProjectSequenceId()%>')">Add</button>
							<%
							}else {%>
						<button type="button" id="submitEditForm" class="btn btn-success" 
						onclick="cancel('cancel','<%=p_oProjectInfo.getProjectSequenceId()%>')">Cancel</button>
						<%}%>
				
				
				</div> <!-- x_content ends -->
			</div> <!-- x_panal ends -->
		</div>
	</div><!-- row ends -->
</csrf:form>
</body>
</html>				
					
				