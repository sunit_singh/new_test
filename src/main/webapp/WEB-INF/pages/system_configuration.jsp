<%-- <%@page import="com.sciformix.sciportal.apps.sys.config.EncryptionKey"%>
 --%><%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.apps.sys.SysConfigPortalApp"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.apps.sys.config.SystemConfigConstants"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.sciportal.integration.ServerConnection"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.system.SystemInfo"%>
<%@page import="com.sciformix.sciportal.utils.LDAPUtils"%>
<%@page import="com.sciformix.sciportal.apps.sys.config.EncryptionKey"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@include file="../../errorPage.jsp" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">

function clickMod(value, id)
{
	document.getElementById("mainContent").style.display = "none";
	//document.getElementById("selectedConnection").value = id;
	if (value=='view') 
	{
		/* document.getElementById('type'+id).disabled=true ;
		document.getElementById('alias'+id).readOnly= true; */
		document.getElementById('server'+id).readOnly= true;
		document.getElementById('submitEditForm'+id).style.display = 'none';
		document.getElementById('statusText'+id).style.display = 'block';
		document.getElementById('statusSwitch'+id).style.display = 'none';
		document.getElementById('ViewOrModify'+id).style.display = 'block';
	}
	/* else if(value=='edit')
	{
		// document.getElementById('type'+id).disabled= true;
		//document.getElementById('alias'+id).readOnly= false; 
		document.getElementById('server'+id).readOnly= false;
		document.getElementById('submitEditForm'+id).style.display = '';
		document.getElementById('statusText'+id).style.display = 'none';
		document.getElementById('statusSwitch'+id).style.display = 'block';
		document.getElementById('ViewOrModify'+id).style.display = 'block';
	}
	else if(value=='create')
	{
		document.getElementById('CreateConnection').style.display = 'block';
	} */
	
	else if(value=='createencrkey')
	{
		document.getElementById("active_tab").value	=	"encryption";
		submitForm(value);
	}
}

function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["sysconfig"];
	return validateInputParameter(oFormObject);
}

function submitForm(action)
{
	document.getElementById("portal_action").value	=	action;
	if(validateFormParameters()){ 
		document.sysconfig.submit();
	}
}
function cancelCreateForm(id)
{
	document.getElementById("mainContent").style.display = 'block';
	document.getElementById('CreateConnection').style.display = 'none';	
	document.getElementById('ViewOrModify'+id).style.display = 'none';
}
function exportKey(keyId)
{
	document.getElementById("export_key").value	=	keyId;
	submitForm('exportKey');
}


/*
 function CreateConnection(op_Value)
{
	var formName=document.getElementById("sysconfig");
	document.getElementById("portal_action").value = op_Value;
	formName.submit();
}
function editConnection(id, value)
{
	var formName=document.getElementById("sysconfig");
	document.getElementById("portal_action").value = value;
	document.getElementById("edit_conn_id").value = id;
	formName.submit();

} */
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
</head>
<%
SystemInfo sysInfo = null;
boolean emptyflag= false;
Set<String> keySet = null;
String successMessage = null;
String activeTab = null;
sysInfo = (SystemInfo)request.getAttribute("systemInfo");
String serverName=LDAPUtils.getLDAPPrimaryDomain();
List<ServerConnection> serverConnectionList=(List<ServerConnection>) request.getAttribute("serverConnectionList");
keySet = (Set<String>)request.getAttribute("keySet");
successMessage = (String)request.getAttribute("successmessage");
activeTab = (String)request.getAttribute("activeTab");


%>

<body>
<csrf:form name="sysconfig" id="sysconfig" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="SYSCONFIG-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="edit_conn_id" name="edit_conn_id" value="">
	<input type="hidden" id="active_tab" name="active_tab" value="">
	<input type="hidden" id="export_key" name="export_key" value="">
	
	<div id="mainContent">
		<%@include file="uiFormCreationMethods.jsp"%>
		<div class="page-title">
			<div class="title_left">
				<h3>System Configuration</h3>
			</div>
		</div>
		
		<div class ="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
	   			<div class="x_panel">
	   				<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs wizard_steps anchor" role="tablist">
						
							<%if(StringUtils.isNullOrEmpty(activeTab)){ %>
							<li role="presentation" class="active">
							<%}else { %>
							<li role="presentation">
							<%} %>
								<a href="#tab-1" role="tab" id="" data-toggle="tab" aria-expanded="true" tabindex=1 class="selected" 
								isdone="1" rel="1">
									About
								</a>
							</li>
							<%if(activeTab != null && activeTab.equals("serconn")){ %>
							<li role="presentation" class="active">
							<%}else { %>
							<li role="presentation">
							<%} %>	
								<a href="#tab-2" id="" data-toggle="tab" aria-expanded="true" tabindex=2 class="selected"
								isdone="0" rel="2">
									Server Connections
								</a>
							</li>
							<%if(activeTab != null && activeTab.equals("encryption")){ %>
							<li role="presentation" class="active">
							<%}else { %>
							<li role="presentation">
							<%} %>	
								<a href="#tab-3" id="" data-toggle="tab" aria-expanded="true" tabindex=2 class="selected"
								isdone="0" rel="2">
									Encryption Keys
								</a>
							</li>
						</ul>
						<div id="myTabContent" class="tab-content stepContainer">
							<%if(StringUtils.isNullOrEmpty(activeTab)){ %>
							<div role="tabpanel" id="tab-1" aria-labelledby="Tab 1" class="tab-pane fade content  active in ">
							<%}else { %>
							<div role="tabpanel" id="tab-1" aria-labelledby="Tab 1" class="tab-pane fade content">
							<%} %>
									<div class="x_content">
		          						<div class="form-group">
											<label class="col-md-2 col-sm-2 col-xs-12">Name</label>
											<div class=" col-md-3 col-sm-2 col-xs-12"><%=sysInfo.getName()%></div>
		                  				</div>
		                    			
		                    			<div class="form-group">
											<label class=" col-md-2 col-sm-2 col-xs-12">Type</label>
											<div class=" col-md-3 col-sm-2 col-xs-12"><%=sysInfo.getType()%></div>
		                  				</div>
		                    			
		                    			<div class="form-group">
											<label class=" col-md-2 col-sm-2 col-xs-12">Bootstrap</label>
											<div class=" col-md-3 col-sm-2 col-xs-12">
													<%=sysInfo.getBootstrap()%>
											</div>
		                  				</div>
		                  				
		                  				<div class="form-group">
											<label class=" col-md-2 col-sm-2 col-xs-12">Database</label>
											<div class="col-md-3 col-sm-2 col-xs-12"><%=sysInfo.getPrimaryDbInfo()%></div>
		                  				</div>
		                  				
		                  				<div class="form-group">
											<label class=" col-md-2 col-sm-2 col-xs-12">Application Version</label>
											<div class=" col-md-3 col-sm-2 col-xs-12"><%=sysInfo.getApplicationVersion() %></div>
		                  				</div>
		  							</div>
								</div>
							<%if(activeTab != null && activeTab.equals("serconn")){ %>
							<div role="tabpanel" id="tab-2" aria-labelledby="Tab 1" class="tab-pane fade content active in">
							<%}else { %>
							<div role="tabpanel" id="tab-2" aria-labelledby="Tab 1" class="tab-pane fade content ">
							<%} %>
									<div class="x_content">
	          						<div id="Template List" class="x_content">
										<div id="templateList">
											<table class="table table-striped">
									  				<thead>
								                        <tr>
								                        	<th>
								                          	</th>
								                          	<!-- <th>
								                          	</th> -->
								                          	<th>Type</th>
								                          	<th>Alias</th>
								                          	<th>Server</th>
								                        </tr>
							                      	</thead>
						                      		<tbody>
						                      		<% for (ServerConnection serv_conn : serverConnectionList){
						                      			%>
						                      			<tr>
															<td>
															<a href="javascript:clickMod('view','<%=serv_conn.getType()%><%=serv_conn.getAlias()%>')">
																<i class="glyphicon glyphicon-eye-open"></i>
															<%-- </a> &nbsp;
																<a href="javascript:clickMod('edit','<%=serv_conn.getType()%><%=serv_conn.getAlias()%>')">
																<i class="fa fa-pencil"></i>
															</a>  --%>
															</td>
															<%-- <td>
															<%=serv_conn.getState() %>
															</td>
															 --%><td>
															<%=serv_conn.getType() %>
															</td>
															<td>
															<%=serv_conn.getAlias() %>
															</td>
															<td>
															Not Assigned
															</td>
														</tr>
													<%} %>
													</tbody>
												</table>
											</div>
											<!--<div class="ln_solid"></div>
				                        	<div>
				                        		<a class="" data-target="#createConnection">
														<button type="button" class="btn btn-primary" onclick="clickMod('create')">Add New</button>
												</a>
				                        	</div>  -->
				                   		</div>
	                    			</div>
								</div>
							<%if(activeTab != null && activeTab.equals("encryption")){ %>
							<div role="tabpanel" id="tab-3" aria-labelledby="Tab 3" class="tab-pane fade content active in ">
							<%}else { %>
							<div role="tabpanel" id="tab-3" aria-labelledby="Tab 3" class="tab-pane fade content ">
							<%} %>	
								<div id="Template List" class="x_content">
									<div id="templateList">
									<% if(successMessage !=null){%>
										<i class='glyphicon'></i>&nbsp;<strong><%=successMessage %></strong>
									<%} %>
										<table class="table table-striped">
							  				<thead>
								                <tr>
								                	<th>
								                    </th>
								                    <th>Key Type</th>
								                    <th>Key Id</th>
								                    <th>Purpose</th>
								                </tr>
							                </thead>
						                    <tbody>
						                    	<%for(String keyId : keySet){ %>
							                   	<tr>
													<td>
														<a href="javascript:exportKey('<%=keyId%>')">
																<i class="fa fa-download" data-original-title="Download">
														</i>
													</td>
													<td>
														<%=EncryptionKey.KeyType.toEnum(ConfigHome.getSysConfigSetting
																(SysConfigPortalApp.generateCommonConfigKey
																(keyId, SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_TYPE)))
														.getDisplayName()%>
													</td>
													<td>
														<%=ConfigHome.getSysConfigSetting
														(SysConfigPortalApp.generateCommonConfigKey(keyId, SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_ID)) 
														%>
													
													</td>
													<td>
														<%=EncryptionKey.KeyPurpose.toEnum(ConfigHome.getSysConfigSetting
																(SysConfigPortalApp.generateCommonConfigKey(keyId, 
																SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_PURPOSE))).getDisplayName()
														%>
													</td>
												</tr>
												<%} %>
											</tbody>
										</table>
									</div>
									<div class="ln_solid"></div>
				                    	<div>
				                        	<button type="button" class="btn btn-primary" onclick="clickMod('createencrkey')">Create New</button>
										</div>  
				                   	</div>
	                    		</div>
							</div>	
						</div><!--Tab panel End  -->	
				</div><!--x panel end  -->
			</div>	
		</div><!-- raw End-->
	</div> <!-- Outer Div -->
	<% for (ServerConnection serv_conn : serverConnectionList){%>
												
	<div  id="ViewOrModify<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" style="display: none;">
		
	      <div class="">
          	<div class="page-title"><div class="title_left"><h3>View Server Connection</h3></div></div><div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				 
				  <div class="x_content">
				  	  	<div class="form-group">	
	                  		<b>
	                  			<label class="control-label col-md-3 col-sm-3 col-xs-12">
	                  				Type
	                  			</label>
	                  		</b>
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  			<select name="type<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" id="type<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" class="btn btn-default dropdown-toggle " aria-expanded="true" disabled="disabled">
  									<option value="<%=serv_conn.getType()%>" selected="selected"><%=serv_conn.getType()%></option>
  								</select>
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
	                  			Alias
	                  		</label>
	                  		<div class="col-md-4 col-sm-4 col-xs-12 ">
	                  		<input type="text" name="alias<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" id="alias<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" class="form-control" value="<%=serv_conn.getAlias() %>" readonly="readonly">
	                  		</div>
	                  		<div class="col-md-2 col-sm-2 col-xs-12">
	                  			 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="field 2"></i>
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12" id="serverField">
	                  			Server
	                  		</label>
	                  		
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  			<input type="text" name="server<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" id="server<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" class="form-control" value="Not define">
	                  		</div>
	                  		<div class="col-md-2 col-sm-2 col-xs-12">
	                  			<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="field 3"></i>
	                  		</div>
	                 	</div>
	                 	<div class="form-group" >
                        	<!-- <label class="control-label col-md-3 col-sm-3 col-xs-12">
                       		Status:
                        	</label> -->
                        	<div id="statusText<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" class="btn-group col-md-6 col-sm-6 col-xs-12 " data-toggle="buttons" style="display: none">
                        	<label class="control-label">
                       			<%-- <%=serv_conn.getState() %> --%>
                        	</label>
                        	</div>
                      		
                      		<div id="statusSwitch<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" class="btn-group col-md-6 col-sm-6 col-xs-12 " data-toggle="buttons">
                        		<label class="btn btn-default active">
                          			<input type="radio" name="edit_status<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" id="option1"  value="<%=ServerConnection.STATE.ACTIVE%>" checked="checked"><%=ServerConnection.STATE.ACTIVE %>
                        		</label>
								<label class="btn btn-default">
								   <input type="radio" name="edit_status<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" id="option2" value="<%=ServerConnection.STATE.DEACTIVE %>"><%=ServerConnection.STATE.DEACTIVE %>
								</label>
							</div>
						</div>  
	                  	<div class="ln_solid"></div>
	                  	
                  	  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                  <button type="button" class="btn btn-primary" onclick="cancelCreateForm('<%=serv_conn.getType()%><%=serv_conn.getAlias()%>')">Cancel</button>
					  <button type="submit" id="submitEditForm<%=serv_conn.getType()%><%=serv_conn.getAlias()%>" class="btn btn-success" onclick="editConnection('<%=serv_conn.getType()%><%=serv_conn.getAlias()%>', 'edit')">Submit</button>
					  </div>		
	              	</div>	 
				 </div>
                </div>
              </div>
             </div>
          </div>
   <%} %>
	<div id="CreateConnection" style="display: none;">
		<div class="page-title">
			<div class="title_left">
				<h3>Adding Server Connection</h3>
			</div>
		</div>
		<div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<div class="x_panel">
					<div class="x_content">
				  		<div class="form-group">	
	                  		<b>
	                  			<label class="control-label col-md-3 col-sm-3 col-xs-12">
	                  				Type<span class="required"> *</span>
	                  			</label>
	                  		</b>
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  			<select name="add_type" id="add_type" class="btn btn-default dropdown-toggle " aria-expanded="true" >
  								<option value="<%=ServerConnection.TYPE.LDAP%>" selected="selected" ><%=ServerConnection.TYPE.LDAP%></option>
  								<option value="<%=ServerConnection.TYPE.SMTP%>"><%=ServerConnection.TYPE.SMTP%></option>
  								<%-- <option value="<%=ServerConnection.TYPE.SHAREPOINT%>"><%=ServerConnection.TYPE.SHAREPOINT%></option> --%>
  							</select>
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
	                  			Alias<span class="required"> *</span>
	                  		</label>
	                  		<div class="col-md-4 col-sm-4 col-xs-12 ">
	                  		<input type="text" name="add_Alias" id="add_Alias" class="form-control" required="required">
	                  		</div>
	                  		<div class="col-md-2 col-sm-2 col-xs-12">
	                  			 <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="field 2"></i>
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12"  id="field">
	                  			Field#2<span class="required"> *</span>
	                  		</label>
	                  		
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  			<input type="text" name="add_server" id="add_server" class="form-control" value="Not define">
	                  		</div>
	                  		<div class="col-md-2 col-sm-2 col-xs-12">
	                  			<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="field 3"></i>
	                  		</div>
	                 	</div>
	                 	<div class="form-group" >
                        	<label class="control-label col-md-3 col-sm-3 col-xs-12">
                       		Status:
                        	</label>
                        	
                      		<div id="statusSwitch" class="btn-group col-md-6 col-sm-6 col-xs-12 " data-toggle="buttons">
                        		<label class="btn btn-default active">
                          			<input type="radio" name="add_status" id="option1"  value="<%=ServerConnection.STATE.ACTIVE %>" checked="checked"><%=ServerConnection.STATE.ACTIVE %>
                        		</label>
								<label class="btn btn-default">
								   <input type="radio" name="add_status" id="option2" value="<%=ServerConnection.STATE.DEACTIVE %>"><%=ServerConnection.STATE.DEACTIVE %>
								</label>
							</div>
						</div>  
	                  	<div class="ln_solid"></div>
	                  	
                  	  	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		                  	<button type="button" class="btn btn-primary" onclick="cancelCreateForm('')">Cancel</button>
						  	<button type="button" id="submitEditForm" class="btn btn-success" onclick="CreateConnection('create')">Submit</button>
						</div>		
	              	</div>	 
				</div>
			</div>
		</div>
	</div>
</csrf:form>
</body>
</html>