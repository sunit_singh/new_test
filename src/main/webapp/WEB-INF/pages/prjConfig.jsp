<%@page import="java.util.HashSet"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="com.sciformix.sciportal.config.ConfigHome"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>
<%@page import="com.sciformix.sciportal.config.ConfigMetadata"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="com.sciformix.commons.utils.*"%>

<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
 
<html>
<head>

<script type="text/javascript">
	function validateFormParameters() {
		clearErrorMessages();
		oFormObject = document.forms["frmmain"];
		return validateInputParameter(oFormObject);
	}
	function projectPropertiesNotSet() {
		displayErrorMessage("UI-1085");	
	}
	function saveSettings() {
		document.getElementById("portal_action").value = "prjsettings_update";
		document.getElementById("projsettings_appid").value ='PRJ-CONFIG';
		if(validateFormParameters()){ 
			document.frmmain.submit();
		}
	}
	
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%
Map<String, Map<String, String>> map_projconfig = (Map<String, Map<String, String>>)request.getAttribute("projconfigmap");

Map<String, Set<String>> map_projconfig_update = new HashMap<String, Set<String>>();

Set<String> keyset = new HashSet<String>();

ConfigMetadata oAppConfigMetadata = null;

List<AppInfo> listApps = AppRegistry.listBusinessApps();
listApps.addAll(AppRegistry.listSystemApps());
String sAppId = null;
String updateProperties = (String)request.getAttribute("updateSucccess");

%>
</head>
<body>
<csrf:form name="frmmain" id="frmmain" action="LandingServlet" method="post">
<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
<input type="hidden" id="portal_action" name="portal_action" value="prjsettings_update" />
<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="">

<h3>Project Properties Configuration</h3>
<h5>&nbsp;&nbsp;REQUIRES SERVER RESTART FOR UPDATING PROPERTIES VALUES &nbsp;&nbsp;</h5>
<%if(updateProperties != null){ %>

	<div class="row">
						<div class="col-sm-12" style="align: center;">
							<h4 align ="center"
								style="padding: 20px; background-color: #e6ffe6; color: #45a945; border-radius: 10px;">Project Properties Updated Successfully</h4>
						</div>
	</div>

<!-- <h5>&nbsp;&nbsp; Project Properties Updated Successfully  &nbsp;&nbsp;</h5> -->
<%} %>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-sm-12 col-xs-8">

	<table class="table">
		<% if (map_projconfig != null)
		{ %>
			<% if (listApps != null)
					{
					for(AppInfo oAppInfo : listApps)
					{ 
						sAppId = oAppInfo.getAppId(); 	
			
						if(map_projconfig.get(sAppId)!=null){%>
							<tr><td colspan="3"><h2><b><%=oAppInfo.getAppName()%></b></h2></td></tr>
							<% for (String innerKey : map_projconfig.get(sAppId).keySet()){
								keyset.add(innerKey);%>
								
								<% 
								oAppConfigMetadata = AppRegistry.getAppConfigMetadata(innerKey);
							if (oAppConfigMetadata != null)
							{
								%>	<tr><td width=""><%=StringUtils.emptyString(oAppConfigMetadata.getDisplayName())%></td>
									<td><input class="form-control"  type="text" name="__prjConfig<%=innerKey%>" id="__prjConfig<%=innerKey%>" value="<%=StringUtils.emptyString(map_projconfig.get(sAppId).get(innerKey))%>" /></td>
									<td><i><%=StringUtils.emptyString(oAppConfigMetadata.getTooltip())%></i></td></tr>
										
							<%}} 
							map_projconfig_update.put(sAppId, keyset);
						} %>
					<%} %>
				<%} %>
					 
			<%} else { %>
					<script type="text/javascript">
    					projectPropertiesNotSet();
    				</script>
		<%} %>
	</table>
	<%if (oAppConfigMetadata != null)
	{ %>
	<div align ="center"><input type="button" class="btn btn-success" value="Save" onclick="javascript:saveSettings();" /></div>
	<%} %>
</div>
</div>

</csrf:form>
</body>
</html>