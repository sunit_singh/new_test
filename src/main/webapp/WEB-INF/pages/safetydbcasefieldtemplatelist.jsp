<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%
	List<SafetyDbCaseFieldsTemplate>	l_scftlist	=	(List<SafetyDbCaseFieldsTemplate>) request.getAttribute("templatelist");
%>

<!DOCTYPE html>
<html>
	<head>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
		function validateFormParameters() {
			clearErrorMessages();
			oFormObject = document.forms["frmmain"];
			return validateInputParameter(oFormObject);
		}
		function fnviewtemplatedetails (p_templateseqid) {
			document.frmmain.fldtemplateseqid.value	=	p_templateseqid;
			document.frmmain.portal_action.value	=	"gettemplatedetails";
			if(validateFormParameters()){ 
				document.frmmain.submit();
			}
		}
		function fncreatenew () {
			document.frmmain.portal_action.value	=	"createnewscftblank";
			if(validateFormParameters()){ 
				document.frmmain.submit();
			}
		}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="1">
			<input type="hidden" id="fldtemplateseqid" name="fldtemplateseqid" value="">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="SCFT">
			<div role="main">
				<div class="page_title">
					<div class="title_left">
						<h3>Template Maintenance</h3>
					</div>
				</div>
				<div class="clearfix">
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>
									View/Create Template
								</h2>
								<div class="clearfix">
								</div>
							</div>
							<div class="x_content">
								<table class="table table-striped">
                     					<thead>
                       					<tr>
                       						<th></th>
                         						<th>Template Name (Seq Id)</th>
                         						<th>State</th>
                         					</tr>
                         				</thead>
                         				<tbody>
                         					<% if (l_scftlist != null && l_scftlist.size()>0) {
                         						for (SafetyDbCaseFieldsTemplate l_obj : l_scftlist) { %>
                         							<tr>
                         								<td>
                         									<a HREF="javascript:fnviewtemplatedetails('<%=l_obj.getSequenceId()%>');">
                         										<i class="glyphicon glyphicon-eye-open">
                         										</i>
                         									</a>
                         								</td>
                         								<td>
                         									<%= l_obj.getTemplateName() %> (<%=l_obj.getSequenceId() %>)
                         								</td>
                         								<td>
                         									<%= l_obj.getState().displayName() %>
                         								</td>
                         							</tr> <%
                         						}
                         					} else { %>
											<tr>
												<td colspan="3">&nbsp;
													<i>No Quality Review Templates</i>
												</td> 
											</tr> <%
										} %>
                         				</tbody>
                         			</table>	
								<div class="ln_solid"></div>
								<button type="button" class="btn btn-primary" onclick="fncreatenew();" value="Create New">
									Create New
								</button>
							</div>
						</div>
					</div>		
				</div>
				<div class="clearfix">
				</div>
			</div>
		</csrf:form>
	</body>
</html>