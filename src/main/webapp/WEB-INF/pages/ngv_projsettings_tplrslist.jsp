<%@page import="java.util.Map"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset"%>
<%@page import="java.util.List"%>

<script type="text/javascript">
function submitForm(value)
{
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value ='NGV_Protal_App';
	document.getElementById("clickValue").value =value;
	 if (validateFormParameters())
	 {
		form.submit();
	 } 
}
function validateFormParameters () {
	oFormObject = document.forms["PROJSETTINGS_form"];
	return validateInputParameter(oFormObject);
}

function viewTemplateRuleset(nRulesetId)
{
	var form = document.getElementById("PROJSETTINGS_form");
	document.getElementById("portal_action").value = 'NGV_Protal_App';
	document.getElementById("clickValue").value ="ngv_template_viewtpl";
	document.getElementById("ngv_tplrulesetid").value = nRulesetId;
	
	if (validateFormParameters())
	 {
		form.submit();
	 }
}

</script>
<%
List<NgvTemplateRuleset> listRulesets = null;

listRulesets = (List<NgvTemplateRuleset>) request.getAttribute("ngv_listRulesets");

%>

	<div class="center_col" role="main">
    	<div class="">
          	<%=displayFormHeader("Narrative Template Rulesets") %>
            
            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <%=displayFormTooltip("View/create/modify Template Rulesets", false, false) %>
                  
                  <div class="x_content">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Template Ruleset Name</th>
                          <th>State</th>
                        </tr>
                      </thead>
                      <tbody>
						<% if (listRulesets != null && listRulesets.size() > 0)
						{
							for (NgvTemplateRuleset oTemplateRuleset : listRulesets)
							{
								%>
								<tr>
		                          <th><A HREF="javascript:viewTemplateRuleset('<%=oTemplateRuleset.getRulesetId()%>');"><i class="fa fa-pencil"></i></A></th>
		                          <td><%=oTemplateRuleset.getRulesetName()%></td>
		                          <td><%=oTemplateRuleset.getState().displayName()%></td>
		                        </tr>
								<%
							}//end-for NgvTemplateRuleset
						}//end-if listRulesets
						else
						{
							%><TR><TD colspan="3">&nbsp;<i>No template rulesets</i></TD><%
						}//end-else listRulesets
						%>
                      </tbody>
                    </table>
                    <div class="ln_solid"></div>
                  	<button type="button" class="btn btn-primary" onclick="javascript:submitForm('ngv_template_createnew');">Create New</button>
					<input type="hidden" id="ngv_tplrulesetid" name="ngv_tplrulesetid" value="" />
                  </div> <!-- end-div x_content -->
                  
                </div><!-- end-div panel -->
              </div><!-- end-div col-md-12 col-sm-12 col-xs-12 -->
            </div><!-- end-div row -->
            
		</div><!-- end-div "" -->
	</div><!-- end-div center_col -->


