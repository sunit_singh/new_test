<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums.RecordState"%>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFields" %>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="com.sciformix.sciportal.config.ConfigTplObj" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%
	SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate		=	(SafetyDbCaseFieldsTemplate)	request.getAttribute("fldtemplatedata");
	RecordState					l_templatestate					=	l_safetydbcasefieldtemplate.getState();
	List<SafetyDbCaseFields>	l_safetydbcasefieldlist	=	l_safetydbcasefieldtemplate.getCaseFieldList();
%>

<!DOCTYPE html>
<html>
	<head>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
		function validateFormParameters() {
			clearErrorMessages();
			oFormObject = document.forms["frmmain"];
			return validateInputParameter(oFormObject);
		}
		function fnchangetemplatestate(sNewState)
		{
			document.frmmain.fldtemplatenewstate.value	=	sNewState;
			document.frmmain.fldtemplateseqid.value		=	'<%= l_safetydbcasefieldtemplate.getSequenceId() %>';
			document.frmmain.portal_action.value		=	"updatetemplatestate";	
			if(validateFormParameters()){ 
				document.frmmain.submit();
			}
		}
		function fnback() {
			document.frmmain.fldprojectid.value	=	'<%= l_safetydbcasefieldtemplate.getProjectId() %>';
			document.frmmain.submit();
		}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldtemplateseqid" name="fldtemplateseqid" value="">
			<input type="hidden" id="fldtemplatenewstate" name="fldtemplatenewstate" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="1">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="SCFT">
			
			<div role="main">
				<div class="show" id="landingpage">
					<div class="page_title">
						<div class="title_left">
							<h3>Case Field Details</h3>
						</div>
					</div>
					<div class="clearfix">
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										View
									</h2>
									<div class="clearfix">
									</div>
								</div>
								<div class="x_content">
									<div class="form-group">
										<% if (l_templatestate == RecordState.UNDER_CONFIGURATION) { %>
											<a href="javascript:fnchangetemplatestate(1);"><i class="glyphicon glyphicon-ok-sign"></i> Activate</a>
										<% } else if (l_templatestate == RecordState.ACTIVE) { %>
											<a href="javascript:fnchangetemplatestate(900);"><i class="glyphicon glyphicon-ok-sign"></i> De-Activate</a>
										<% } %>
	                  					
									</div>
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Template Name
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_safetydbcasefieldtemplate.getTemplateName() %>
											</label>
										</div>
									</div>
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Template State
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_templatestate.displayName() %>
											</label>
										</div>
									</div>
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Safety DB Name
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_safetydbcasefieldtemplate.getSafetyDbName() %>
											</label>
										</div>
									</div>
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Safety DB Version
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_safetydbcasefieldtemplate.getSafetyDbVersion() %>
											</label>
										</div>
									</div>
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Case Id Alias
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_safetydbcasefieldtemplate.getCaseIdAlias() %>
											</label>
										</div>
									</div>
									<div class="form-group">
										<b>
											<label class="control-label col-md-3 col-sm-3 col-xs-12">
												Case Version Alias
											</label>
										</b>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<label class="control-label">
												<%= l_safetydbcasefieldtemplate.getCaseVersionAlias() %>
											</label>
										</div>
									</div>
									<div class="ln_solid"></div>
									<table class="table table-striped">
                      					<thead>
                        					<tr>
                        						<th>Field Name</th>
                          						<th>Field Category</th>
                          						<th>Field Group</th>
                          						<th>Field Hint</th>
                          						<th>Field Mnemonic</th>
                          					</tr>
                          				</thead>
                          				<tbody>
                          					<% if (l_safetydbcasefieldlist != null) {
                          						for (SafetyDbCaseFields l_safetydbcasefield : l_safetydbcasefieldlist) { %>
                          							<tr>
	                          							<td>
	                          								<%= l_safetydbcasefield.getCaseFieldName() %>
	                          							</td>
	                          							<td>
	                          								<%= l_safetydbcasefield.getCaseFieldType().getCatCode() %>
	                          							</td>
	                          							<td>
	                          								<%= l_safetydbcasefield.getCaseFieldTab() %>
	                          							</td>
	                          							<td>
	                          								<% if (StringUtils.isNullOrEmpty(l_safetydbcasefield.getCaseFieldHint())) { %>
	                          									
	                          								<% } else { %>
	                          									<%= l_safetydbcasefield.getCaseFieldHint() %>
	                          								<% } %>	
	                          							</td> 
	                          							<td>
	                          								<%= l_safetydbcasefield.getCaseFieldMnemonic() %>
	                          							</td>
	                          						</tr>	<%
                          						}	
                          					} 
                          					%>
                          				</tbody>
                          			</table>
                          			<div class="ln_solid"></div>
									<button type="button" class="btn btn-primary" onclick="fnback();" value="Create New">
										Back
									</button>
								</div>
							</div>
						</div>		
					</div>
					<div class="clearfix">
					</div>
				</div>
			</div>		
		</csrf:form>
	</body>
</html>	