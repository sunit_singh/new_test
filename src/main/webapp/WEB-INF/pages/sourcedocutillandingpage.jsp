<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDetailsDb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%
	String sProjectName = WebUtils.getAuthorizedProject(request);
	int nProjectId = WebUtils.getAuthorizedProjectId(request);
%>
	<script type="text/javascript">
		function validateFormParameters() {
			clearErrorMessages();
			oFormObject = document.forms["toolsLandingPage_form"];
			return validateInputParameter(oFormObject);
		}
		function clickEvent(appid)
		{
			clearErrorMessages();
			<%-- <%if(nProjectId != -1){%> --%>
				var formName=document.getElementById("toolsLandingPage_form");
				document.getElementById("tools_appid").value = appid;
				 if(validateFormParameters()){ 
					formName.submit();
				 } 
<%-- 			<%}else{%>	
				displayErrorMessage("UI-1084");
			<%}%> --%>
		}
	</script>

</head>
<body>

<csrf:form name="toolsLandingPage_form" id="toolsLandingPage_form" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="SOURCE_DOC_UTILS_PORTAL_APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="tools_appid" name="tools_appid" value="">
	
	<div id="mainContent">
	<%@include file="uiFormCreationMethods.jsp"%>
		<div class="page-title">
			<div class="title_left">
				<h3><%=AppRegistry.getToolsApp().getAppName() %>
				<%if(nProjectId != -1){%>
					-  <%=sProjectName%>
					<%} %>
				</h3>
			</div>
		</div>
		
		<div class ="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
				 	<div id="Content 1">
				 		<div class="x_title">
							<h2>
								General
							</h2>
							<div class="clearfix"></div>
						</div>
					
						<div class="x_content">
							<a class="btn btn-app" onclick="clickEvent('PDF-SPLITTER');"style="width: = 2cm; height: 2cm">
                      			<label> PDF <br> Splitter </label>
                    		</a>
                    		<a class="btn btn-app" onclick="clickEvent('PDF-PARSER');" style="width: = 2cm; height: 2cm">
                      			 <label> PDF <br> Parser </label>
                    		</a>
                    		<a class="btn btn-app" onclick="clickEvent('PDF-ANNOTATOR');" style="width: = 2cm; height: 2cm">
                      			<label> PDF <br> Annotator </label>
                    		</a>
                    		<!-- <a class="btn btn-app" style="width: = 2cm; height: 2cm">
                      			 
                    		</a> -->
						</div>
					</div> 
					<br /> <br />

					
				</div> <!--x panel End  -->
			</div>
		</div><!--row end  -->
		
	</div><!-- Main Content End -->
	
</csrf:form>
</body>
</html>