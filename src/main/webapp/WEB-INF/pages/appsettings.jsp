<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
  
<%@page import="com.sciformix.commons.SciError"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>
<%@page import="com.sciformix.sciportal.config.ConfigMetadata"%>

<%@page import="com.sciformix.sciportal.apps.AppUserPreferenceMetadata"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="com.sciformix.commons.utils.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


<script type="text/javascript">
var mapKeyAndDatatypes = new Array();

function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["APPSETTINGS_form"];
	return validateInputParameter(oFormObject);
}

function saveSettings()
{
	var form = document.getElementById("APPSETTINGS_form");
	oFormObject = document.forms["APPSETTINGS_form"];
	
	if (validateFormParameters())
	 {
		 form.submit();
	 }
	
}

/* function refreshSysConfig(action)
{
	var form = document.getElementById("APPSETTINGS_form");
	oFormObject = document.forms["APPSETTINGS_form"];
	document.getElementById("portal_action").value=action;
	form.submit();
} */
</script>

</head>
<body>
<!-- <div class ="col-md-12 col-sm-12 col-xs-12">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<h4>App Settings</h4> 
		<h5>&nbsp;&nbsp;REQUIRES SERVER RESTART !!!&nbsp;&nbsp;</h5>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12" align="right">
	   	<button type="button" class="btn btn-primary" onclick="refreshSysConfig('refreshsysconfig')">Refresh System Configuration</button>
	</div>
</div> -->
<h4>App Settings</h4> 
	<h5>&nbsp;&nbsp;REQUIRES SERVER RESTART !!!&nbsp;&nbsp;</h5>
<csrf:form name="APPSETTINGS_form" id="APPSETTINGS_form" action="LandingServlet" method="post">
<input type="hidden" id="portal_appid" name="portal_appid" value="APPSETTINGS-PORTAL-APP" />
<input type="hidden" id="portal_action" name="portal_action" value="appsettings_step2" />

<table>
<tr><td colspan="3">&nbsp;</td></tr>

<%
List<AppInfo> listApps = AppRegistry.listBusinessApps();
			listApps.addAll(AppRegistry.listSystemApps());
Map<String, String> mapAppConfig = null;
String sAppId = null;
IPortalApp oPortalApp = null;
ConfigMetadata oAppConfigMetadata = null;

mapAppConfig = AppRegistry.getGeneralConfigSettings();
if (mapAppConfig != null)
{
	%><tr><td colspan="3">&nbsp;<b>General Settings</b></td></tr><%
	for (String sKey : mapAppConfig.keySet())
	{
		oAppConfigMetadata = AppRegistry.getAppConfigMetadata(sKey);
		if (oAppConfigMetadata != null)
		{
			%><tr><td>&nbsp;<%=oAppConfigMetadata.getDisplayName()%></td>
			<td>&nbsp;<input type="text" name="__appSettings<%=sKey%>" id="__appSettings<%=sKey%>" value="<%=mapAppConfig.get(sKey)%>" /></td>
			<td>&nbsp;<i><%=oAppConfigMetadata.getTooltip()%></i></td></tr>
			<SCRIPT>mapKeyAndDatatypes["<%=sKey%>"] = "<%=oAppConfigMetadata.getValueDatatype()%>"</SCRIPT>
			<%
		}
		else
		{
			%><tr><td >&nbsp;<%=sKey%></td><td style="font-family:Courier New">&nbsp;<%=mapAppConfig.get(sKey)%></td><td>&nbsp;</td></tr><%
		}
	}
}

if (listApps != null)
{
	for(AppInfo oAppInfo : listApps)
	{
		sAppId = oAppInfo.getAppId();
		oPortalApp = AppRegistry.getApp(sAppId);
		
		%><tr><td colspan="3">&nbsp;</td></tr><%
		%><tr><td colspan="3">&nbsp;<b><%=oAppInfo.getAppName()%></b></td></tr><%
		
		mapAppConfig = oPortalApp.getAppConfigSettings();
		if (mapAppConfig != null)
		{
			for (String sKey : mapAppConfig.keySet())
			{
				oAppConfigMetadata = AppRegistry.getAppConfigMetadata(sKey);
				if (oAppConfigMetadata != null)
				{
					%><tr><td>&nbsp;<%=oAppConfigMetadata.getDisplayName()%></td>
					<% if (StringUtils.isNullOrEmpty(mapAppConfig.get(sKey))) { %>
						<td>&nbsp;<input type="text" name="__appSettings<%=sKey%>" id="<%=sKey%>" value="" /></td>
					<% } else { %>
						<td>&nbsp;<input type="text" name="__appSettings<%=sKey%>" id="<%=sKey%>" value="<%=mapAppConfig.get(sKey)%>" /></td>
					<% } %>
					
					<td>&nbsp;<i><%=oAppConfigMetadata.getTooltip()%></i></td></tr>
					<%
				}
				else
				{
					%><tr><td >&nbsp;<b><%=sKey%></b></td><td style="font-family:Courier New">&nbsp;<%=mapAppConfig.get(sKey)%></td><td>&nbsp;</td></tr><%
				}
			}
		}
	}
	
}
%>

<tr><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="3" align="center"><input type="button" class="btn btn-success" value="Save" onclick="javascript:saveSettings();" /></td></tr>
<tr><td colspan="3">&nbsp;</td></tr>
</table>
</csrf:form>
</body>
</html>