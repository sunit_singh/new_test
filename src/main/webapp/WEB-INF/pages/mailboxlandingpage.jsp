<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Mail Box Landing Page
		</title>
		
		<script type="text/JavaScript">
			function formSubmit (p_redirect) {
				document.getElementById("portal_action").value = p_redirect;
				if (validateFormParameters())
				 {
				document.frmain.submit();
				}
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmain"];
				return validateInputParameter(oFormObject);
			}
		</script>
	</head>
<%
ProjectInfo p_oProjectInfo= (ProjectInfo)request.getAttribute("authproject");
%>
<body>
	<csrf:form name="frmain" id="frmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="MB-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">	
	
			<div class="title_left">
				<h3>Mail Box Dashboard 
				<%if(p_oProjectInfo != null){%>
					-  <%=p_oProjectInfo.getProjectName()%>
					<%} %>
				</h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div id="Content1">
				 		<div class="x_title">
							<h2>
								General
							</h2>
							<div class="clearfix"></div>
						</div>
					
						<div class="x_content">
							<a class="btn btn-app" onclick="formSubmit('mailBox');" style="width: = 2cm; height: 2cm">
                      			<label>Mail Box <br> View</label>
                    		</a>
						
							<a class="btn btn-app" onclick="formSubmit('uploadmsgfileview');" style="width: = 2cm; height: 2cm">
                      			<label> Upload<br /> Msg <br> Files</label>
                    		</a>
                    	</div>
					</div> 
				</div>
			</div>
		</csrf:form>	
	</body>
</html>