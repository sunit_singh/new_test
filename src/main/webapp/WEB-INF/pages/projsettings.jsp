<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.audit.AuditInfoDetailsDb"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%
	String sProjectName = WebUtils.getAuthorizedProject(request);
	int nProjectId = WebUtils.getAuthorizedProjectId(request);
%>
	<script type="text/javascript">
		function validateFormParameters() {
			clearErrorMessages();
			oFormObject = document.forms["PROJSETTINGS_form"];
			return validateInputParameter(oFormObject);
		}
		function clickEvent(appid)
		{
			clearErrorMessages();
			<%if(nProjectId != -1){%>
				var formName=document.getElementById("PROJSETTINGS_form");
				document.getElementById("projsettings_appid").value = appid;
				if(validateFormParameters()){ 
					formName.submit();
				}
			<%}else{%>	
				displayErrorMessage("UI-1084");
			<%}%>
		}
	</script>

</head>
<body>

<csrf:form name="PROJSETTINGS_form" id="PROJSETTINGS_form" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="">
	<input type="hidden" id="fldprojectid" name="fldprojectid" value="<%=nProjectId%>">
	
	<div id="mainContent">
	<%@include file="uiFormCreationMethods.jsp"%>
		<div class="page-title">
			<div class="title_left">
				<h3>Project Configuration 
				<%if(nProjectId != -1){%>
					-  <%=sProjectName%>
					<%} %>
				</h3>
			</div>
		</div>
		
		<div class ="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
				 	<div id="Content 1">
				 		<div class="x_title">
							<h2>
								General
							</h2>
							<div class="clearfix"></div>
						</div>
					
						<div class="x_content">
							<a class="btn btn-app" onclick="clickEvent('PROJCET-USER-MANGEMENT');"style="width: = 2cm; height: 2cm">
                      			<label> Project <br> User <br> Management</label>
                    		</a>
                    		<a class="btn btn-app" onclick="clickEvent('EMAIL-TEMP-APP');" style="width: = 2cm; height: 2cm">
                      			 <label> Project <br> Email <br> Template</label>
                    		</a>
                    		<a class="btn btn-app" onclick="clickEvent('SCFT');" style="width: = 2cm; height: 2cm">
                      			<label> Project <br> Safety DB Case Field <br> Template</label>
                    		</a>
                    		<a class="btn btn-app" onclick="clickEvent('PRJ-CONFIG');" style="width: = 2cm; height: 2cm">
                      			<label> Project Properties <br> Configuration </label>
                    		</a>
                    		<a class="btn btn-app" onclick="clickEvent('MB-PORTAL-APP');" style="width: = 2cm; height: 2cm">
                      			 <label>  <br>Mail Box<br> </label>
                    		</a>
                    		<!-- <a class="btn btn-app" style="width: = 2cm; height: 2cm">
                      			 
                    		</a> -->
						</div>
					</div> 
					<br /> <br />
					<div id="Content 2">
						<div class="x_title">
							<h2>
								Narrative Generator & Validator (NGV)	
							</h2>
							<div class="clearfix"></div>
						</div>
						
						<div class="x_content">
							<a class="btn btn-app" onclick="clickEvent('NGV-PORTAL-APP');" style="width: = 2cm; height: 2cm">
                      			<label> Narrative<br> Template<br> Rulesets </label>
                    		</a>
                    		<a class="btn btn-app" onclick="clickEvent('NGV_Portal_Validation_App');" style="width: = 2cm; height: 2cm">

                      			<label> Case <br>Validation<br> Rulesets </label>
                    		</a>
                    	<!-- 	<a class="btn btn-app" style="width: = 2cm; height: 2cm">
                      			
                    		</a>
                    		<a class="btn btn-app" style="width: = 2cm; height: 2cm">
                      			 
                    		</a> -->
						</div>
					</div>
					<br /> <br />
					<div id="Content 3">
						<div class="x_title">
							<h2>
								Quality Review Log
							</h2>
							<div class="clearfix"></div>
						</div>
						
						<div class="x_content">
							<a class="btn btn-app"  onclick="clickEvent('QR-PORTAL-APP')"; style="width: = 2cm; height: 2cm">
                      			<label> Quality <br> Review <br> Templates </label>
                    		</a>
						</div>
					</div>
					
				</div> <!--x panel End  -->
			</div>
		</div><!--row end  -->
		
	</div><!-- Main Content End -->
	
</csrf:form>
</body>
</html>