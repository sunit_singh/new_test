<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
	<head>
		<script src="js/moment/moment.min.js"></script>
		<script type="text/JavaScript">
			function validateFormParameters () {
				clearErrorMessages();
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			
			function fnupload() {
				clearErrorMessages();
				if(document.frmmain.casemasterfile.value == "") {
					displayErrorMessage("UI-1052");
					return false;
				} 
				document.frmmain.portal_action.value	=	"uploadCaseFile";
				if(validateFormParameters ()){ 
					document.frmmain.submit();
				}
			}
			function fncancel () {
				document.frmmain.submit();
			}
		</script>
	</head>
	<body>
	<%	
		Map<String,List<String>> enableUdfMap= null; 
		Map<String,String>			l_fileformats	=	null;	
		
		enableUdfMap = (Map<String,List<String>>) request.getAttribute("enableUdfList");
		l_fileformats	=	(Map<String,String>) request.getAttribute("fileformats");
		int udfDiplayNameIndex = 0;
		int udfDataTypeIndex = 1;
	%>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldfiletype" name="fldfiletype" value="QRLCM">
			<div role="main">
				<div class="page_title">
					<div class="title_left">
						<h3>Case Master Upload</h3>
					</div>
				</div>
				<div class="clearfix">
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title ">
								
								<h2>
									Upload Case Master File
								</h2>
								<div class="clearfix">
								</div>
							</div>
							<div class="x_content" >
								<div id = "uploadform" >
                       				<!-- <div class="ln_solid"></div> -->
                       				<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fldfileformat">
											File Format
											<span class="required"> *</span>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
		                       				<select class="form-control" name="fldfileformat" id="fldfileformat">
												<% for (String l_fileformatcode : l_fileformats.keySet()) { %>
													<option value ="<%= l_fileformatcode%>"><%= l_fileformats.get(l_fileformatcode)%></option>
												<% } %>
											</select> 
										</div>
									</div>		
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">
											Upload Case File
											<span class="required"> *</span>
										</label>
										<div class="col-md-4 col-sm-4 col-xs-12">
											<input type="file" name="casemasterfile" id="casemasterfile">
										</div>
									</div>
										
									</div>	
									<div class="form-group">
									
				                    	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
											<button type="button" class="btn btn-primary" onclick="fncancel();">
												Cancel
											</button>
				                        	<button type="button" class="btn btn-success" value="Submit" onclick="fnupload();">
				                        		Submit
				                        	</button>
				                        </div>
				                	</div>
				          </div>
						</div>
					</div>		
				</div>
				<div class="clearfix">
			</div>
			</div>	
		</csrf:form>
	</body>
</html>