<%@page import="com.sciformix.sciportal.user.UserHome"%>
<%@page import="com.sciformix.sciportal.apps.qr.QRConstant"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.commons.utils.DataTable"%>
<%@page import="org.apache.commons.csv.CSVRecord"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="com.sciformix.commons.utils.WorkbookWorkSheetWrapper"%>
<%@page import="org.apache.commons.csv.CSVParser"%>
<%@page import="com.sciformix.sciportal.pvcp.PvcpCaseMaster"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.IPortalApp"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script type="text/javascript">
			function validateFormParameters() {
				clearErrorMessages();
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fnsave(value) {
				document.frmmain.portal_action.value	=	value;
				if(validateFormParameters()){ 
					document.frmmain.submit();
				}
			}
			function fnback(value) {
				document.frmmain.portal_action.value	=	value;
				document.frmmain.submit();
			}
		</script>
	</head>

	
<%

DataTable o_datatable = (DataTable)request.getAttribute("parsedata");
String		l_fileformat	= (String) 	request.getAttribute("fileformat");
Map<String,String>	l_columnmapping	=	(Map<String,String>)request.getAttribute("columnmappingdetails");
Set<Object>	l_casedataset	=	null;
boolean errorflag = false;
Date dt_irddate = null;
String error = null;
request.getSession().setAttribute("uploadcasemasterdetails", o_datatable);

List<Object> ldatatablecaseid =null;
List<Object> ldatatablecaseversion = null;
l_casedataset	=	new HashSet<Object>();

/* Check Duplicate cases present in case master upload */
if(o_datatable.isColumnPresent("CASEID") && o_datatable.isColumnPresent("VERSION"))
{
	ldatatablecaseid = o_datatable.getColumnValues("CASEID");
	ldatatablecaseversion = o_datatable.getColumnValues("VERSION");
	for(int l_i = 0; l_i< ldatatablecaseid.size(); l_i++) {
		if (l_casedataset.add((String)ldatatablecaseid.get(l_i)+"_"+ldatatablecaseversion.get(l_i))) {
			
		} else {
			errorflag=true;
		%>
			<script type="text/javascript">
				displayErrorMessage("Duplicate Case Id "+"<%=(String)ldatatablecaseid.get(l_i)%>" +" and case version "+"<%=(String)ldatatablecaseversion.get(l_i)%>");
			</script>
		<%	
		}
	}
}%>
<body>
	<csrf:form name="frmmain" id="frmmain" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="QR-PORTAL-APP">
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="fldfiletype" name="fldfiletype" value="QRLCM">
	<input type="hidden" id="fldfileformat" name="fldfileformat" value="<%=l_fileformat %>">
	<div class="page-title">
		<div class="title_left">
			<h3>Confirm Case Record details</h3>
		</div>
	</div>
	<div class ="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				 <div class="x_title">
					<h3>
						<small>List of Case Records</small> 	
					</h3>
					<div class="clearfix"></div>
				</div>	
				
			
					<div class="x_content">
						<div id="templateList">
						<%if(o_datatable != null){ %>
							<table class="table table-striped">
								<thead>
							    	<tr>
							    	<%for(int i = 0; i< o_datatable.colCount(); i++) {%>
							    	<%if(o_datatable.getColumnHeader(i)!=null) {%>
							    	<th>
							    		<%=l_columnmapping.get(o_datatable.getColumnHeader(i))%>
							    	</th>
								    	<%} 
							    	} %>
							       </tr>
						         </thead>
						         <tbody>
						         
						         <%for(int i = 0; i< o_datatable.rowCount(); i++) {%>
						         	 <tr>
						        	 <%
						        	 	for(int j = 0; j<o_datatable.colCount(); j++) {
						        		if(o_datatable.getColumnValues(o_datatable.getColumnHeader(j))!=null){%>
						        		<!-- Show Case Master Fields -->
						        		<td>
						        		<%if(o_datatable.getColumnHeader(j).equals(QRConstant.CASE_MASTER_INTERNAL_DENAME)){
							        		if(!StringUtils.isNullOrEmpty((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i)))
							        		{%>
							        		<%=UserHome.retrieveUserBySeqId(Integer.parseInt((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i))).getUserDisplayName()%>
							        		(<%=UserHome.retrieveUserBySeqId(Integer.parseInt((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i))).getUserId()%>)
							        		<%}
						        		}else{ %>
						        		<%=StringUtils.emptyString((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i))%>
						        		<%} %>
						        		</td>
						        		<!--Error Messages for empty fields -->
						        		
						        		<%if(o_datatable.getColumnHeader(j).equals(QRConstant.CASE_MASTER_INTERNAL_COL_CASE_ID)){
						        			//TODO: Change the below displayErrorMessage to show display name for CASE ID
						        			if(StringUtils.isNullOrEmpty((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i)))
						        			{	errorflag=true;
						        				error = l_columnmapping.get(QRConstant.CASE_MASTER_INTERNAL_COL_CASE_ID)
						        						+" column value is mandatory in case: "
						        						+(String)o_datatable.getColumnValues(o_datatable.getColumnHeader(0)).get(i);
						        			%>
						        		<script type="text/javascript">
						        			displayErrorMessage("<%=error%>");
					        			</script>
						        		
						        		<%}} %>
						        		
						        		<%if(o_datatable.getColumnHeader(j).equals(QRConstant.CASE_MASTER_INTERNAL_COL_VERSION)){
						        			//TODO: Change the below displayErrorMessage to show display name for VERSION
						        			if(StringUtils.isNullOrEmpty((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i)))
						        			{ errorflag=true;
						        			error = l_columnmapping.get(QRConstant.CASE_MASTER_INTERNAL_COL_VERSION)
						        						+" column value is mandatory in case: "
						        						+(String)o_datatable.getColumnValues(o_datatable.getColumnHeader(0)).get(i);
						        			%>
						        		<script type="text/javascript">
							        			displayErrorMessage("<%=error%>");
					        			</script>
						        		
						        		<%}} %>
						        		
						        		<%if(o_datatable.getColumnHeader(j).equals(QRConstant.CASE_MASTER_INTERNAL_COL_SERIOUSNESS)){
						        			//TODO: Change the below displayErrorMessage to show display name for CASESERIOUSNESS
						        			if(StringUtils.isNullOrEmpty((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i)))
						        			{ errorflag=true;
						        			error = l_columnmapping.get(QRConstant.CASE_MASTER_INTERNAL_COL_SERIOUSNESS)
						        						+" column value is mandatory in case: "
						        						+(String)o_datatable.getColumnValues(o_datatable.getColumnHeader(0)).get(i);
						        			%>
						        		<script type="text/javascript">
							        			displayErrorMessage("<%=error%>");
					        			</script>
						        		
						        		<%}} %>
						        		
						        		<%
						        		if(o_datatable.getColumnHeader(j).equals(QRConstant.CASE_MASTER_INTERNAL_IRD))
						        		{
						        			//TODO: Change the below displayErrorMessage to show display name for IRD
						        			if(StringUtils.isNullOrEmpty((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i)))
						        			{ errorflag=true;
						        			error = l_columnmapping.get(QRConstant.CASE_MASTER_INTERNAL_IRD)
						        						+" column value is mandatory in case: "
						        						+(String)o_datatable.getColumnValues(o_datatable.getColumnHeader(0)).get(i);
						        			%>
						        		<script type="text/javascript">
							        			displayErrorMessage("<%=error%>");
					        			</script>
						        		
						        		<%}else{ 
						        		try {
											dt_irddate =new SimpleDateFormat("dd-MMM-yyyy").parse((String)o_datatable.getColumnValues(o_datatable.getColumnHeader(j)).get(i));
											Date current = new Date();
												if(dt_irddate.after(current) || (dt_irddate.equals(current))){
													errorflag=true;
													error = l_columnmapping.get(QRConstant.CASE_MASTER_INTERNAL_IRD)
						        						+" column value is mandatory in case: "
						        						+(String)o_datatable.getColumnValues(o_datatable.getColumnHeader(0)).get(i);
													%>
													<script type="text/javascript">
								        			displayErrorMessage("<%=error%>");
								        			</script>
										   <% 	} 
											}
										catch(ParseException prex)
										{
										errorflag=true;
						        		%>
										<script type="text/javascript">
					        				displayErrorMessage("Incorrect "+ "<%=l_columnmapping.get(QRConstant.CASE_MASTER_INTERNAL_IRD) %>" +" format in case: "+"<%=(String)o_datatable.getColumnValues(o_datatable.getColumnHeader(0)).get(i)%>");
					        			</script>
							    		<%} }
							    		}%>
						        		
						        		<%}} %>
									</tr>
							    	<%}%>
			   				      </tbody>
					          </table>
					          
				   <%}%>
					 	
						</div>
						<div>
	     				<div class="ln_solid"></div>
	 				</div>
						<div class="form-group">	
     					<div align = "center">
     					<button type="button" class="btn btn-primary" onclick="fnback('ilqr_case_master');">
	 						Back
	 					</button>
		 				<%if (!errorflag){%>
			 				<button id ="savedata" type="button" class="btn btn-success" onclick="fnsave('save');">
			 					Save
			 				</button>
			 			<%} %>
	 		
	 				</div>		
					</div>	
				   </div><!-- x Content End -->		
				   
	 				
     				
				</div>
			</div>
		</div> 
	</csrf:form>
</body>
			
	
	 
</html>