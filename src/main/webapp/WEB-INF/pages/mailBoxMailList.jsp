<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.mb.MBPortalApp"%>
<%@page import="com.sciformix.sciportal.mb.MBMailInfo"%>
<%@page import="com.sciformix.sciportal.mb.MailBox"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
  
<%
	Integer	l_startdatecount	=	(Integer)	request.getAttribute("fldstartdatecount"),
			l_enddatecount		=	(Integer)	request.getAttribute("fldenddatecount");
%>  
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mail Box List</title>

<script src="js/moment/moment.min.js"></script>
<script type="text/javascript">
	var startDate;
	var endDate;
	var startDateCount = <%= l_startdatecount%>;
	var endDateCount = <%= l_enddatecount%>;
	startDateCount	=	-startDateCount;
	endDateCount	=	-endDateCount;
	$(document).ready(function() {
	    $('#reportrange_right').daterangepicker( {
	   		startDate: moment().subtract(startDateCount, 'days'),
		    endDate: moment().subtract(endDateCount, 'days'),
		    minDate: '01/01/2012',
		    maxDate: moment(),
		    dateLimit: {
		    	days: 365
		    },
		    showDropdowns: true,
		    showWeekNumbers: true,
		    timePicker: false,
		    timePickerIncrement: 1,
		    timePicker12Hour: true,
		    ranges: {
		    	'Today': [moment(), moment()],
		        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		        'This Month': [moment().startOf('month'), moment()],
		        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		    },
		    opens: 'right',
		    buttonClasses: ['btn btn-default'],
		    applyClass: 'btn-small btn-primary',
		    cancelClass: 'btn-small',
		    format: 'MM/DD/YYYY',
		    separator: ' to ',
		    locale: {
			    applyLabel: 'Submit',
			    cancelLabel: 'Clear',
			    fromLabel: 'From',
			    toLabel: 'To',
			    customRangeLabel: 'Custom',
			    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
			    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			    firstDay: 1
		    }
		},
	    function(start, end) {
	        $('#reportrange_right span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
	        startDate = start.startOf('day');
	        endDate = end.startOf('day');
	    }
	    );
	    $('#reportrange_right span').html(moment().subtract(startDateCount, 'days').format('D MMMM YYYY') + ' - ' + moment().subtract(endDateCount, 'days').format('D MMMM YYYY'));
	    startDate 	=	moment().startOf('day').subtract(startDateCount, 'days');
	    endDate		=	moment().startOf('day').subtract(endDateCount, 'days');
	    $('[data-toggle="tooltip"]').tooltip();
	 });
	function dateFilter(action)
	{
		
		var form=document.getElementById("frmmain");
		document.getElementById("portal_action").value = action;
		var	todaydate	=	moment().startOf('day');
		document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
		document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days');
		if (validateFormParameters())
		{
			 form.submit();
		}
	}
	function validateFormParameters () {
		clearErrorMessages();
		oFormObject = document.forms["frmmain"];
		return validateInputParameter(oFormObject);
	}
	
	function back(action) {
		var form=document.getElementById("frmmain");
		document.getElementById("portal_action").value = action;
		form.submit();
	}
	function fnexport() {
		clearErrorMessages();
		var	todaydate	=	moment().startOf('day');
		document.frmmain.fldstartdatecount.value = startDate.diff(todaydate,'days');
		document.frmmain.fldenddatecount.value 	= endDate.diff(todaydate,'days');
		document.getElementById("portal_action").value = "exportmails";
		document.frmmain.submit();
	}

</script>
</head>

<%
ProjectInfo p_oProjectInfo = null;
List<MBMailInfo> mailList = null;
MailBox mailBox = null; 
 p_oProjectInfo= (ProjectInfo)request.getAttribute("authproject");
 mailList = (List<MBMailInfo>)request.getAttribute("mailList");
 mailBox = (MailBox)request.getSession().getAttribute("mailBox");
%>
<body>
	<csrf:form name="frmmain" id="frmmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="MB-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="fldstartdatecount" name="fldstartdatecount" value="">
	<input type="hidden" id="fldenddatecount" name="fldenddatecount" value="">
		<div class="page-title">
			<div class="title_left">
				<h3>
				 	Mails for <%=mailBox.getTemplateName() %>
				</h3>
			</div>
		</div>
		
		<div class="x_panel">
			<div class="x_title">
			<br/>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
						Date Range : 
					</label>
					
					
							<div class="col-md-4 col-sm-4 col-xs-12">
			                    <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
		                			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		                			<span></span> <b class="caret"></b>
		                		</div>
		                	</div>	
	                		
					
					<div class="col-md-4 col-sm-4 col-xs-12">
					 	<input type = "button" class="btn btn-success" value ="Fetch" onclick ="dateFilter('filterMailList');">
					</div>
				</div>
				<div class="clearfix"></div> 
			</div> 
			
			<div class="x_content">
			
			<%if(mailList != null){
				if(!mailList.isEmpty()){%>
			<table class="table table-striped">
				<thead>
					<tr>
						<!-- <th></th>
						 -->
						<th>Workflow</th>
						<th>Date</th>
						<th>Mail Id</th>
						<th>Email</th>
						<th>Sender</th>
						<!-- <th>To</th>
						<th>Cc</th> -->
						<th>Subject</th>
						<th>Recipient</th>
					</tr>
				</thead>
				<tbody>
				<%
					for(MBMailInfo mailInfo : mailList ){%>
					<tr>
						<td>
						
						</td>
						<td>
							<%=WebUtils.formatForDisplay(mailInfo.getMailTimeStamp())%><br/>
							<small><%=WebUtils.formatForDisplayDuration(mailInfo.getMailTimeStamp())%></small>
						</td>
						<td>
							<%=mailInfo.getMailAppSeqId() %>
						</td>
						<td>
							<%if(StringUtils.emptyString(mailInfo.getMailSender()).toLowerCase().
									contains
							(StringUtils.emptyString(mailBox.getEmailAddress()).toLowerCase())){%>
								<!-- <img src="images/SenderIcon.png"/>	 -->
							<%}
							if(StringUtils.emptyString(mailInfo.getMailRecipientsTo()).toLowerCase().
									contains
							   (StringUtils.emptyString(mailBox.getEmailAddress()).toLowerCase())){%>	
								<!-- <span class="glyphicon" aria-hidden="true">
									<img src="images/ToIcon.png"/>
								</span> -->
							<%}
							if(StringUtils.emptyString(mailInfo.getMailRecipientsCc() ).toLowerCase().
									contains
								(StringUtils.emptyString(mailBox.getEmailAddress()).toLowerCase())){%>
								<!-- <span class="glyphicon" aria-hidden="true">
									<img src="images/CcIcon.png"/>
								</span> -->
									
							<%}if(mailInfo.getMailAttachmentCount()>0){%>
								<span class="glyphicon glyphicon-paperclip" data-toggle="tooltip" 
								data-placement="left" title="<%=mailInfo.getMailAttachmentCount() %>" aria-hidden="true"></span>
							<%}%>					
						</td>
						<td><%=mailInfo.getMailSender() %></td>
						<%-- <td class="col-md-2 col-sm-2 col-xs-2"><%=mailInfo.getMailRecipientsTo()%></td>
						<td class="col-md-2 col-sm-2 col-xs-2"><%=StringUtils.emptyString(mailInfo.getMailRecipientsCc())%></td>
						 --%>
						<td><%=mailInfo.getMailSubject() %></td>
						<%-- <td><%=mailInfo.getMailAttachmentCount() %></td> --%>
						<td>
						To: <%=mailInfo.getMailRecipientsTo() %>
						<%if(!StringUtils.isNullOrEmpty(mailInfo.getMailRecipientsCc())){%>
							<Br />
							<Br />
						Cc:	<%=mailInfo.getMailRecipientsCc() %>
						<%} %>
						</td>
					</tr>
				
				<%}%>
				</tbody>
			</table>
			
			<%}else{%>
				 No Data Found
			<%} %>
			<div align='center'>
				<% if(!mailList.isEmpty()){ %>
					<input type = "button" class="btn btn-primary" value ="Export" onclick ="fnexport();">
				<% } %>
				<input type = "button" class="btn btn-primary" value ="Back" onclick ="back('mailBox');">
			</div>	
		<% } %>
			</div>
		</div>
			
	</csrf:form>
</body>
</html>