<%@page import="com.sciformix.sciportal.mb.MailBox"%>
<%@page import="java.util.List"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.sciportal.project.ProjectInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mail Box Landing Page</title>

<script type="text/javascript">
function viewMailBox(action)
	{
		var form=document.getElementById("frmain");
		document.getElementById("portal_action").value = action;
		if (!validateInput() && validateFormParameters())
		 {
			 form.submit();
		 }
	}
function back()
{
	var form=document.getElementById("frmain");
	document.getElementById("portal_action").value = "";
	form.submit();
}	
function validateFormParameters () {
	clearErrorMessages();
	oFormObject = document.forms["frmain"];
	return validateInputParameter(oFormObject);
}
function validateInput()
{
	clearErrorMessages();
	var bErrorDetected = false;
	var maiBoxValue =document.getElementById("selectMailBox").value;
	
	if(maiBoxValue==""||maiBoxValue.trim()==""){
		displayErrorMessage("UI-1091");
		bErrorDetected = true;
	} 
	return bErrorDetected;
} 
	
	
</script>
</head>

<%
List<MailBox> mailBoxList = (List<MailBox>)request.getAttribute("mblist");
ProjectInfo p_oProjectInfo= (ProjectInfo)request.getAttribute("authproject");
%>
<body>
	<csrf:form name="frmain" id="frmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="MB-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">	
		<div class="page-title">
				<div class="title_left">
					<h3>
					 Mail Box - <%=p_oProjectInfo.getProjectName()%>
					</h3>
				</div>
		</div>
		
		<div class="x_panel">
			<div class="x_title">
					<h2>
						Mail Box Available
					</h2>
					<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
						Select Mail Box: 
					</label>
					
					<div class="col-md-4 col-sm-4 col-xs-12">
					<select class= "form-control" id ="selectMailBox" name="selectMailBox">
						<option value=""> -- Select --</option>
						<%for(MailBox mailBox : mailBoxList){ %>
						<option value="<%=mailBox.getSequenceId()%>"> <%=mailBox.getTemplateName()%></option>
						<%} %>
					</select>
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12">
					 <input type = "button" class="btn btn-success" value ="View" onclick ="viewMailBox('viewmailbox');">
					</div>
				</div>
			</div>
		</div>
		<div align="center">
			 <input type = "button" class="btn btn-primary" value ="Back" onclick ="back();">
		</div>
	</csrf:form>
</body>
</html>