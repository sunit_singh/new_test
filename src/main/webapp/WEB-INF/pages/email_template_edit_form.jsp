<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums.RecordState"%>
<%@page import="com.sciformix.sciportal.mail.MailTemplate"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		
		<script type="text/javascript">
			function validateFormParameters() {
				clearErrorMessages();
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fncancel() {	
				document.getElementById("portal_action").value ='cancel';
				document.getElementById("projsettings_appid").value ='EMAIL-TEMP-APP';
				document.frmmain.submit();
			}
			function viewMailTemplate() {
				document.getElementById("viewTemp").style.display = "block";
			}
			function updateTemplate(sid, clickValue,statusValue) {
				document.getElementById("portal_action").value =clickValue;
				document.getElementById("projsettings_appid").value ='EMAIL-TEMP-APP';
				document.getElementById("statusValue").value =statusValue ;
				document.getElementById("click_Operation_Project_Id").value = sid;
				if(validateFormParameters()){ 
					document.frmmain.submit();
				}
			}
			function editMailTemplate(seqId,clickValue) {
				document.getElementById("portal_action").value =clickValue;
				document.getElementById("projsettings_appid").value ='EMAIL-TEMP-APP';
				document.getElementById("click_Operation_Project_Id").value = seqId;
				if(validateFormParameters()){ 
					document.frmmain.submit();
				}
			}
	
		</script>
		<title>Insert title here</title>
	</head>
<body>
<%MailTemplate mailTemplate =(MailTemplate)request.getAttribute("mailTemplate");
String successmsg = (String) request.getAttribute("msg");
%>
<csrf:form name="frmmain" id="frmmain" data-parsley-validate="" class="form-horizontal form-label-left" action="LandingServlet" method="post"  enctype="multipart/form-data">
	<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
	<input type="hidden" id="portal_action" name="portal_action" value="">
	<input type="hidden" id="click_Operation_Project_Id" name="click_Operation_Project_Id" value="">
	<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="">
	<input type="hidden" id="statusValue" name="statusValue" value="">
	
	<div id="mainContent">
		<%@include file="uiFormCreationMethods.jsp"%>
		<div class="page-title">
			<div class="title_left">
				<h3>Edit Template</h3>
			</div>
		</div>
		
		<div class ="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
	   			<div class="x_panel">
	   				<div class="x_title">
						<h3>
							<small>View/Modify Email Template</small>
						</h3>
						<%if(successmsg!=null){  %>
								<%=successmsg %>
							<% }%>	
					</div>	
						
						<div id="editTemplateDive" class="x_content" >
						
						<div class="form-group">
							<%if(mailTemplate.getState().displayName()==RecordState.ACTIVE.displayName()) {%>
							<a href="javascript:updateTemplate('<%=mailTemplate.getSequenceId()%>','changeStatus','900');"><i class="glyphicon glyphicon-ok-sign"></i> Deactivate</a>
							 <%}else if(mailTemplate.getState().displayName()==RecordState.UNDER_CONFIGURATION.displayName()){%>
						    <a href="javascript:updateTemplate('<%=mailTemplate.getSequenceId()%>','changeStatus','1');"><i class="glyphicon glyphicon-ok-sign"></i> Active</a>
						     <%}%>
						 </div> 
												 
							<div class="form-group">
                       			<label class="control-label col-md-3 col-sm-3 col-xs-12">
                       			Email Subject:<span class="required">
                        		</label>
                        		<div class="col-md-6 col-sm-6 col-xs-12">
                          			<label class="control-label"><%=mailTemplate.getSubjectTemplate()%></label>
                          		</div>
                        	</div> 
                        	<div class="form-group">
                       			<label class="control-label col-md-3 col-sm-3 col-xs-12">
                       			Email Status:<span class="required">
                        		</label>
                        		<div class="col-md-6 col-sm-6 col-xs-12">
                          			<label class="control-label"><%=mailTemplate.getState().displayName()%></label>
                          		</div>
                        	</div> 
                        	<div class="form-group">
                        	
                        	<label class="control-label col-md-3 col-sm-3 col-xs-12">
                       			Body Content:
                        	</label>
                        		<div class="col-md-6 col-sm-6 col-xs-12">
                          		<label class="control-label">
                          		<div align="left" style="width: inherit;" >
                          		<c:out value="<%=mailTemplate.getBodyTemplate()%>" escapeXml="true"/>
                       			</div>
                       			</label>
                       			</div>
                       			<div class="col-md-2 col-sm-2 col-xs-12">
                       			<a onclick="viewMailTemplate()">
									<i class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="left" title="View Mail Template"></i>
								</a>
									&nbsp;
								<%if(mailTemplate.getState().displayName()==RecordState.UNDER_CONFIGURATION.displayName()) {%>
									<a onclick="editMailTemplate('<%=mailTemplate.getSequenceId()%>','editPage')">
										<i class="fa fa-pencil" data-toggle="tooltip" data-placement="left" title="Edit Mail Template"></i>
									</a>
								<%} %>
									</div>
                          		</div>
                      		</div> 
                     	</div>
					</div>
				</div>
			</div>
			
		<div  id="viewTemp" style="display: none;">
	      <div class="">
          	<div class="page-title"><div class="title_left"><h3>Email Template View</h3></div></div><div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
				  <div class="x_content">
                      	<div class="form-group">	
	                  		<b>
	                  			<label class="control-label col-md-3 col-sm-3 col-xs-12">
	                  				Mail Template Name:
	                  			</label>
	                  		</b>
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  			<input type="text"  name="new_Template_Name" id="template_Name" value="<%=mailTemplate.getTemplateName() %>" disabled="disabled"
            					class="form-control"maxlength= 64 >
	                  		</div>
	                  	</div>
	                  	
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
	                  			Project Purpose:
	                  		</label>
	                  		<div class="col-md-4 col-sm-4 col-xs-12">
	                  			<input type="text" class="form-control" name="new_Purpose" id="purpose" value="<%=mailTemplate.getPurpose() %>" disabled="disabled">
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12 ">
	                  			Email Subject:
	                  		</label>
	                  		<div class="col-md-4 col-sm-4 col-xs-12 ">
	                  		<input type="text" id="mail_Subject" name = "mail_Subject" class="form-control" value = "<%=mailTemplate.getSubjectTemplate()%>" disabled="disabled">
	                  		</div>
	                  	</div>
	                  	<div class="form-group">
	                  		<label class="control-label col-md-3 col-sm-3 col-xs-12" >
	                  			Email Body:
	                  		</label>
	                  		<div class="col-md-8 col-sm-8 col-xs-12">
	                  			<textarea id="body" name="new_body" rows="10" cols="80" disabled="disabled"
	                  			class="resizable_textarea form-control col-md-7 col-xs-12"><%=mailTemplate.getBodyTemplate() %>
	                  			</textarea>
	                  		</div>
	                  	</div>
	                  	 <div class="form-group">
                        	<label class="control-label col-md-3 col-sm-3 col-xs-12">
                       		Status:
                        	</label>
                        	<label class="control-label ">
                        	<%=mailTemplate.getState().displayName()%>
	                  		</label>
                      </div>
	              </div>	 
				</div>
                </div>
              </div>
             </div>
          </div>
         
         
         <div class="form-group">	
         	<div align="center">
	            <button type="button" class="btn btn-primary" onclick="fncancel()">Cancel</button>
			 </div>		
	     </div>	
		
	</csrf:form>
</body>
</html>