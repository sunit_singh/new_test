<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.sciformix.commons.SciEnums"%>
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@ page import="com.sciformix.sciportal.apps.qr.QRTemplate" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<%
	List<QRTemplate>	l_qrtemplate	=	(List<QRTemplate>) request.getAttribute("templatelist");
%>

<!DOCTYPE html>
<html>
	<head>
		<script src="js/jquery.js"></script>
		<script type="text/JavaScript">
			function fnviewtemplatedetails (p_templateseqid) {
				document.frmmain.fldtemplateseqid.value	=	p_templateseqid;
				document.frmmain.portal_action.value	=	"gettemplatedetails";
				document.frmmain.fldmode.value			=	"view";
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
			function validateFormParameters () {
				oFormObject = document.forms["frmmain"];
				return validateInputParameter(oFormObject);
			}
			function fncreatenew () {
				document.frmmain.portal_action.value	=	"createnewqrtblank";
				if (validateFormParameters()) {
					document.frmmain.submit();
				}
			}
		</script>
	</head>
	<body>
		<csrf:form name="frmmain" id="frmmain" class="form-horizontal" method="post" action="LandingServlet" enctype="multipart/form-data">
			<input type="hidden" id="portal_appid" name="portal_appid" value="PROJSETTINGS-PORTAL-APP" >
			<input type="hidden" id="portal_action" name="portal_action" value="">
			<input type="hidden" id="fldprojectid" name="fldprojectid" value="">
			<input type="hidden" id="fldtemplateseqid" name="fldtemplateseqid" value="">
			<input type="hidden" id="projsettings_appid" name="projsettings_appid" value="QR-PORTAL-APP">
			<input type="hidden" id="fldmode" name="fldmode" value ="">
			<div role="main">
				<div class="show" id="landingpage">
					<div class="page_title">
						<div class="title_left">
							<h3>Template Maintenance</h3>
						</div>
					</div>
					<div class="clearfix">
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										View/Create Template
									</h2>
									<div class="clearfix">
									</div>
								</div>
								<div class="x_content">
									<table class="table table-striped">
                      					<thead>
                        					<tr>
                        						<th></th>
                          						<th>Template Name (Seq Id)</th>
                          						<th>State</th>
                          					</tr>
                          				</thead>
                          				<tbody>
                          					<% if (l_qrtemplate != null && l_qrtemplate.size()>0) {
                          						for (QRTemplate l_obj : l_qrtemplate) { %>
                          							<tr>
                          								<td>
                          									<a HREF="javascript:fnviewtemplatedetails('<%=l_obj.getSequenceId()%>');">
                          										<i class="fa fa-pencil">
                          										</i>
                          									</a>
                          								</td>
                          								<td>
                          									<%= l_obj.getTemplateName() %> (<%=l_obj.getSequenceId()%>)
                          								</td>
                          								<td>
                          									<%= l_obj.getState().displayName() %>
                          								</td>
                          							</tr> <%
                          						}
                          					} else { %>
												<tr>
													<td colspan="3">&nbsp;
														<i>No Quality Review Templates</i>
													</td> 
												</tr> <%
											} %>
                          				</tbody>
                          			</table>	
									<div class="ln_solid"></div>
									<button type="button" class="btn btn-primary" onclick="fncreatenew();" value="Create New">
										Create New
									</button>
								</div>
							</div>
						</div>		
					</div>
					<div class="clearfix">
					</div>
				</div>
			</div>		
		</csrf:form>
	</body>
</html>