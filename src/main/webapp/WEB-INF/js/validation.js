var sObjectNameRegEx = /^[A-Za-z0-9 _.-]*$/;
var sEmailRegEx = /^[a-zA-Z0-9]+\.?[a-zA-Z0-9]+@[a-zA-Z0-9]+(\.+[a-zA-Z]{2,4})$/;  
var sFtlRegEx = /<{1}( *\/*)*(script)|(script)( *\/*)*>{1}/;
var sIntRegEx = /^\-?\d*$/;  
var sNumRegEx = /^[0-9]\d*(\.\d+)?$/;
var sConfigSettingRegEx= /<{1}( *\/*)*(script)|(script)( *\/*)*>{1}/;
var sUserIdRegEx = /^[A-Za-z0-9.@]{1,50}$/;
var sPasswordRegEx = null;
var sFileRegEx = /^[\w]?\:?[\w\s-\\\,\.\]\[\~\@\#\$\%\^\&\(\)\+\=\.\`]+\.[A-Za-z]{2,4}$/;
var sRuleCriterionRegEx = /^[A-Za-z0-9 ()$_.-]*$/;
var sCaseIdRegEx = /^[A-Za-z0-9 _.-]*$/;
var sCaseVersionRegEx = /^[A-Za-z0-9 :/]*$/;
var sDateRegEx = /^(([0]?[1-9])|([1-2][0-9])|([3][0-1]))\-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)\-([2][0-9]{3})$/;
var sDescriptionRegEx = /<{1}( *\/*)*(script)|(script)( *\/*)*>{1}/;
var sRestrictDescriptionRegEx = /(<{1}( *\/*)*(script)|(script)( *\/*)*>{1})|\#|\,|\~|\;|\:|\(|\)/;
var sAdvancedRegEx = null;
var isMessageDisplayed = false;

function validateInputParameter(oFormObject)
{
	
	var appId = oFormObject.elements["portal_appid"].value;
	var contextName = jspPageName;
	var sDataType = "";
	var returnFlag = true;
	isMessageDisplayed = false;
	if(!sObjectNameRegEx.test(appId))
		{
			returnFlag = displayErrorField(oFormObject,appId);
		}
	else if(!sObjectNameRegEx.test(contextName))
		{
			returnFlag = displayErrorField(contextName);
		}
	else{
		for(var i=0; i < oFormObject.length; i++)
		{
			if(oFormObject.elements[i].value.trim() != "" && oFormObject.elements[i].value)
			{	
			oFormObject.elements[i].style.borderColor  = "";  //remove error fields highlight
			
				var fieldname = contextName+"."+oFormObject.elements[i].name;
				if(!($('#'+oFormObject.elements[i].name).is(':disabled')))
				{
					if(oFormObject.elements[i].name.startsWith("__"))
					{
						var keyPresFlag = true;
						for(var key in wildCardValidationMap[appId])
							{
								var keyArray=key.split(".");
								if(oFormObject.elements[i].name.startsWith(keyArray[1]))
								{
									keyPresFlag = false;
									sDataType  = wildCardValidationMap[appId][key].sDataType;
									if(sDataType.toLowerCase() == "email")
									{
										if (!sEmailRegEx.test(oFormObject.elements[i].value)) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									} else if(sDataType.toLowerCase() == "ftl")
									{
										if ( sFtlRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "integer")
									{
										if (!sIntRegEx.test(oFormObject.elements[i].value)) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "number")
									{
										if (!sNumRegEx.test(oFormObject.elements[i].value)) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "file")
									{
										if (!sFileRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "configsettings")
									{
										if (sConfigSettingRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "userid")
									{
										if (!sUserIdRegEx.test(oFormObject.elements[i].value)) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "password")
									{
											
									}else if(sDataType.toLowerCase() == "rulecriterion")
									{
										if (!sRuleCriterionRegEx.test(oFormObject.elements[i].value)) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "caseid")
									{
										if (!sCaseIdRegEx.test(oFormObject.elements[i].value)) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "caseversion")
									{
										if (!sCaseVersionRegEx.test(oFormObject.elements[i].value)) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "date")
									{
										if (!sDateRegEx.test(oFormObject.elements[i].value.toUpperCase())) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "description")
									{
										if (sDescriptionRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}else if(sDataType.toLowerCase() == "restricteddescription")
									{
										if (sRestrictDescriptionRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}
									else if(sDataType.toLowerCase() == "advance")
									{
										sAdvancedRegEx = validationMap[fieldname].sRegEx;
										var regExForJs = new RegExp('^'+sAdvancedRegEx);
										if(!regExForJs.test(oFormObject.elements[i].value))
										{	
											returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
										}
									}
								}
							}
							if(keyPresFlag)  //dynamic Object Name fields validation 
							{
								if(!sObjectNameRegEx.test(oFormObject.elements[i].value))
								{
									returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
								}
							}
					}else if(validationMap[appId] != null && fieldname in validationMap[appId])
					{
						sDataType  = validationMap[appId][fieldname].sDataType;
						if(sDataType.toLowerCase() == "email")
						{
							if (!sEmailRegEx.test(oFormObject.elements[i].value)) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						} else if(sDataType.toLowerCase() == "ftl")
						{
							if (sFtlRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "integer")
						{
							if (!sIntRegEx.test(oFormObject.elements[i].value)) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "number")
						{
							if (!sNumRegEx.test(oFormObject.elements[i].value)) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "configsettings")
						{
							if (sConfigSettingRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "file")
						{
							if (!sFileRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "userid")
						{
							if (!sUserIdRegEx.test(oFormObject.elements[i].value)) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "password")
						{
							
						}else if(sDataType.toLowerCase() == "rulecriterion")
						{
							if (!sRuleCriterionRegEx.test(oFormObject.elements[i].value)) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "caseid")
						{
							if (!sCaseIdRegEx.test(oFormObject.elements[i].value)) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "caseversion")
						{
							if (!sCaseVersionRegEx.test(oFormObject.elements[i].value)) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "date")
						{
							if (!sDateRegEx.test(oFormObject.elements[i].value.toUpperCase())) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "description")
						{
							if (sDescriptionRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "restricteddescription")
						{
							if (sRestrictDescriptionRegEx.test(oFormObject.elements[i].value.toLowerCase())) {
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}else if(sDataType.toLowerCase() == "advance")
						{
							sAdvancedRegEx = validationMap[fieldname].sRegEx;
							var regExForJs = new RegExp('^'+sAdvancedRegEx);
							if(!regExForJs.test(oFormObject.elements[i].value))
							{	
								returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
							}
						}
					}else{
						
						if(!sObjectNameRegEx.test(oFormObject.elements[i].value))
						{
							returnFlag = displayErrorField(oFormObject,oFormObject.elements[i].name);
						}
					}
				}
			}
		}
	}
	if (returnFlag) {
		var inputElement	=	document.createElement("input");
		inputElement.setAttribute("type", "hidden");
		inputElement.setAttribute("name", "_jsp_jspPageObject");
		inputElement.setAttribute("value", jspPageName);
		oFormObject.appendChild(inputElement);
	}
	return returnFlag;
}
function displayErrorField(oFormObject, fieldId)
{
	if (isMessageDisplayed) {
		// do nothing
	} else {
		displayErrorMessage("UI-1087");
		isMessageDisplayed = true;
	}
     oFormObject.elements[fieldId].style.borderColor  = "Red";
    return false;
}