
var listErrorMessages = [];
listErrorMessages["UI-1001"] = "Please specify valid username";
listErrorMessages["UI-1002"] = "Please specify valid password";
listErrorMessages["UI-1003"] = "Please enter Case Id";
listErrorMessages["UI-1004"] = "Please select Case File";
listErrorMessages["UI-1005"] = "Please specify template name";
listErrorMessages["UI-1006"] = "Please specify template content";
listErrorMessages["UI-1007"] = "Please specify template criteria";
listErrorMessages["UI-1008"] = "Please specify template ruleset name";
listErrorMessages["UI-1009"] = "Please specify template header content";
listErrorMessages["UI-1010"] = "Invalid Case Id";
listErrorMessages["UI-1011"] = "Invalid file name";
listErrorMessages["UI-1012"] = "Invalid file extension";
listErrorMessages["UI-1013"] = "File name should contains Case Id";
listErrorMessages["UI-1014"] = "Please select template ruleset file";
listErrorMessages["UI-1015"] = "Please select Filter User";
listErrorMessages["UI-1016"] = "Some Special characters not allowed in update markers";
listErrorMessages["UI-1017"] = "Please attach the file before clicking submit";
listErrorMessages["UI-1018"] = "Please Enter Project Name";
listErrorMessages["UI-1019"] = "Invalid Project Name";
listErrorMessages["UI-1020"] = "Please Enter Project Description";
listErrorMessages["UI-1021"] = "Please upload the safety case file";
listErrorMessages["UI-1022"] = "Invalid Template Name";
listErrorMessages["UI-1023"] = "Please Enter Mail Purpose";
listErrorMessages["UI-1024"] = "Please Enter Mail Subject";
listErrorMessages["UI-1025"] = "Please Enter Mail Body";
listErrorMessages["UI-1026"] = "Please Select User";
listErrorMessages["UI-1027"] = "Please Select Roles";
listErrorMessages["UI-1028"] = "Please select Case Associate";
listErrorMessages["UI-1029"] = "Please select Case Seriousness";
listErrorMessages["UI-1030"] = "Please select Review Status for all the fields";
listErrorMessages["UI-1031"] = "Please specify Rule Name";
listErrorMessages["UI-1032"] = "Please select Rule Type";
listErrorMessages["UI-1033"] = "Please specify Rule Criteria";
listErrorMessages["UI-1034"] = "Please Select the Required Applicability";
listErrorMessages["UI-1035"] = "Please specify Applicability Criteria in the Textbox for Cases that Meet the criteria";
listErrorMessages["UI-1036"] = "Please specify Case Validation Ruleset name";
listErrorMessages["UI-1037"] = "Please enter mandatory fields";
listErrorMessages["UI-1038"] = "Please enter Initial Received Date in DD-MMM-YYYY format.";
listErrorMessages["UI-1039"] = "Please enter numbers only";
listErrorMessages["UI-1040"] = "Future date cannot be entered";
listErrorMessages["UI-1041"] = "Please enter Safety DB Name";
listErrorMessages["UI-1042"] = "Please enter Safety DB Version";
listErrorMessages["UI-1043"] = "Please enter Case Id Alias";
listErrorMessages["UI-1044"] = "Please enter Case Version Alias";
listErrorMessages["UI-1045"] = "Case Id Alias and Case Version Alias cannot be same";
listErrorMessages["UI-1046"] = "Please specify User Id";
listErrorMessages["UI-1047"] = "Please specify valid User_Id";
listErrorMessages["UI-1048"] = "Please specify Complete_Name";
listErrorMessages["UI-1049"] = "Please specify Email Id";
listErrorMessages["UI-1050"] = "Please specify Auth Type";
listErrorMessages["UI-1051"] = "Please specify valid Email Id";
listErrorMessages["UI-1052"] = "Please upload the case master file";
listErrorMessages["UI-1053"] = "Please select valid Email Send Setting";
listErrorMessages["UI-1054"] = "Please select review type";
listErrorMessages["UI-1055"] = "Please specify case id";
listErrorMessages["UI-1056"] = "Please specify case version";
listErrorMessages["UI-1057"] = "Please specify File Context";
listErrorMessages["UI-1058"] = "Please specify File Description";
listErrorMessages["UI-1059"] = "Please upload RIS File";
listErrorMessages["UI-1060"] = "Please specify search input";
listErrorMessages["UI-1061"] = "Please select user";
listErrorMessages["UI-1062"] = "Please specify from date";
listErrorMessages["UI-1063"] = "Please specify to date";
listErrorMessages["UI-1064"] = "Please enter From Date in DD-MM-YYYY format.";
listErrorMessages["UI-1065"] = "Please enter To Date in DD-MM-YYYY format.";
listErrorMessages["UI-1066"] = "Future date cannot be entered in from date field";
listErrorMessages["UI-1067"] = "Future date cannot be entered in to date field";
listErrorMessages["UI-1068"] = "Please specify atleast one field for search";
listErrorMessages["UI-1069"] = "Please select citation";
listErrorMessages["UI-1070"] = "Special Characters are not allowed. Please enter characters other than that";
listErrorMessages["UI-1071"] = "Future date cannot be enterded in Initial Receive Date";
listErrorMessages["UI-1072"] = "Please select Case Seriousness";
listErrorMessages["UI-1073"] = "Please specify Initial Receive Date";
listErrorMessages["UI-1074"] = "Please specify client Name";
listErrorMessages["UI-1075"] = "Please specify valid client Name";
listErrorMessages["UI-1076"] = "Please specify client Email";
listErrorMessages["UI-1077"] = "Please specify valid client Email";
listErrorMessages["UI-1078"] = "Please specify client SafetyDBId";
listErrorMessages["UI-1079"] = "The value cannot be less than 0";
listErrorMessages["UI-1080"] = "The value cannot be greater than 100";
listErrorMessages["UI-1081"] = "Special Characters are not allowed. Please enter characters other than that";
listErrorMessages["UI-1082"] = "Please select Reg Class";
listErrorMessages["UI-1083"] = "File already downloaded, please re-execute it";
listErrorMessages["UI-1084"] = "Current project not selected in user preferences";
listErrorMessages["UI-1085"] = "Server not restarted for assigned project";
listErrorMessages["UI-1086"] = "Review Type All Cannot be Exported need to select specific review type";
listErrorMessages["UI-1087"] = "Invalid field value";
listErrorMessages["UI-1088"] = "Report can be exported in Date mode only. Please select the Date mode and export";
listErrorMessages["UI-1089"] = "Unsupported file Type. Please select PDF file type only";
listErrorMessages["UI-1090"] = "Multiple File Upload is not supported for the selected File Type";
listErrorMessages["UI-1091"] = "Please select mail box";
listErrorMessages["UI-1092"] = "Please upload msg file";
listErrorMessages["UI-1093"] = "The difference between the start date and the end date is greater than the permissible value. Please contact system administrator";



var gerr_listErrors = [];

function createErrorMessageString(sErrorCode)
{
	if (listErrorMessages[sErrorCode] == undefined)
	{
		return sErrorCode;
	}
	else
	{
		return "<i class='glyphicon glyphicon-exclamation-sign'></i>&nbsp;<strong>" + sErrorCode + "</strong>" + ": " + listErrorMessages[sErrorCode];
	}
}

function displayErrorMessage(sErrorCode)
{
	var CONSTANT_PREFIX = '<div>';
	var CONSTANT_SUFFIX = '</div>';
	
	gerr_listErrors[gerr_listErrors.length] = sErrorCode;
	
	document.getElementById("_errordiv_lcl").innerHTML = CONSTANT_PREFIX;
	for (var nCounter = 0; nCounter < gerr_listErrors.length; nCounter++)
	{
		document.getElementById("_errordiv_lcl").innerHTML += createErrorMessageString(gerr_listErrors[nCounter]) + "<BR/>";
	}
	document.getElementById("_errordiv_lcl").innerHTML += CONSTANT_SUFFIX;
	document.getElementById("_errordiv_lcl").style.display = "block";
}

function clearErrorMessages()
{
	var serverrdiv = document.getElementById("_errordiv_svr");
	if (serverrdiv == null) {
		
	} else {
		serverrdiv.style.display = "none";
	}
	document.getElementById("_errordiv_lcl").innerHTML = "";
	gerr_listErrors = [];
}