function validateInput()
	{
		var pattern = /[!@#$%^&*()";/|{~`}]/g;
		var caseID=document.getElementById("caseID").value;
		var fileName=document.getElementById("ngv_casedata_file").value;
		var safteyDbName=document.getElementById("safety_db_name").value;
		
	//	alert(caseID.match(pattern)+"==>>"+fileName.match(pattern));
		if(caseID==""){
			displayErrorMessage("ERR-UI-1003");
			return false;
		}else if(caseID.match(pattern))
		{
				displayErrorMessage("ERR-UI-1010");
				return false;
		}
		if(fileName==""){
			displayErrorMessage("ERR-UI-1004");
			return false;
		}else if(fileName.match(pattern))
		{
			displayErrorMessage("ERR-UI-1011");
			return false;
		}else{
			var fileExtension= fileName.split("\\").pop().split(".").pop();
			
			if(fileExtension)
			//alert(fileName.split("\\").pop());
			return false;
		}
		
		if(safteyDbName==""){
			alert("Please select saftey database: ");
			return false;
		} 
			return true;
	}