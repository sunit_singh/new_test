<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>
<%@page import="java.util.List"%>
<%
List<AppInfo> listApps = (List<AppInfo>) request.getSession().getAttribute("listBusinessApps");
AppInfo l_sb_oAppInfoSystemConfigurationSetting = AppRegistry.getSystemConfigurationSettingApp();
AppInfo l_sb_oAppInfoAppSettings = AppRegistry.getAppSettingsApp();
AppInfo l_sb_oAppInfoProjSettings = AppRegistry.getProjSettingsApp();
AppInfo l_sb_oAppInfoAuditTrailViewer = AppRegistry.getAuditTrailViewerApp();
AppInfo l_sb_oAppInfoUserManagement = AppRegistry.getUserManagementApp();
AppInfo l_sb_oAppInfoProjManagement=AppRegistry.getProjectManagementSettingApp();
AppInfo l_sb_oAppTools=AppRegistry.getToolsApp(); 
AppInfo l_sb_oAppInfoActivityTrailViewer = AppRegistry.getActivityTrailViewerApp();

UserSession l_sb_oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
UserInfo l_sb_oSessionUser = (l_sb_oUserSession != null ? l_sb_oUserSession.getUserInfo() : null);

boolean l_sb_bIsSystemAdminUser = l_sb_oSessionUser.isSystemAdmin();
boolean l_sb_bIsProjectAdminUser = l_sb_oSessionUser.isProjectAdmin();
boolean l_sb_bIsAuditorUser = l_sb_oSessionUser.isAuditor();
boolean l_sb_bIsITServiceDeskUser = l_sb_oSessionUser.isItServiceDeskUser();
boolean l_sb_bIsProjectAuditorUser = l_sb_oSessionUser.isProjectAuditor();


%>
            <div class="navbar nav_title" style="border: 0;">
              <a href="javascript:;" class="site_title"><img src="images/sciformix_icon.png" alt="..." class="" width="41" height="41"> <span>Safety Portal</span></a>
            </div>

            <div class="clearfix"></div>

            <br />
            
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <!-- <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="javvascript:;">Dashboard</a></li>
                    </ul>
                  </li> -->
                  <li><a><i class="fa fa-desktop"></i> Applications <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                  	<%
                  	if (listApps != null)
                  	{
                  		for(AppInfo oAppInfo : listApps)
                  		{
                  			%><li><a href="LandingServlet?portal_appid=<%=oAppInfo.getAppId()%>"><%=oAppInfo.getAppName()%></a></li><%
                  		}
                  	}                  	
                  	%>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-wrench"></i> Tools <span class="fa fa-chevron-down"></span></a>
                   		 <ul class="nav child_menu">
                   		 	 <li><a href="LandingServlet?portal_appid=<%=l_sb_oAppTools.getAppId()%>"><%=l_sb_oAppTools.getAppName()%></a></li>
                   		 	<%-- <li><a href="LandingServlet?portal_appid=<%=l_sb_oAppTools.getAppId()%>"><%=l_sb_oAppTools.getAppName() %></a></li> --%>
                   		 </ul>
                   </li>
                  <li><a><i class="fa fa-cloud"></i> Audit Trail Viewer <span class="fa fa-chevron-down"></span></a>
                  	<% if (l_sb_bIsAuditorUser || l_sb_bIsProjectAuditorUser) {%>
                    <ul class="nav child_menu">
	                  <% if (l_sb_bIsProjectAuditorUser && l_sb_oAppInfoAuditTrailViewer!=null) {%>
                      	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoAuditTrailViewer.getAppId()%>">Audit Trail Viewer</a></li>
                      	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoActivityTrailViewer.getAppId()%>">Activity Trail Viewer</a></li>
                      <%} %>
                       <% if(!(l_sb_bIsProjectAuditorUser && l_sb_oAppInfoAuditTrailViewer!=null)&& l_sb_oAppInfoAuditTrailViewer!=null){%>
                      	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoAuditTrailViewer.getAppId()%>">Audit Trail Viewer</a></li>
                      	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoActivityTrailViewer.getAppId()%>">Activity Trail Viewer</a></li>
                      <%} %>
                    </ul>
                    <%} else {%>
                    <ul class="nav child_menu">
                      <li>(no access)</li>
                    </ul>
                    <%} %>
                  </li>
                  <li><a><i class="fa fa-cogs"></i> Project Settings <span class="fa fa-chevron-down"></span></a>
                  	<% if (l_sb_bIsProjectAdminUser) {%>
                    <ul class="nav child_menu">
	                  <% if (l_sb_bIsProjectAdminUser && l_sb_oAppInfoProjSettings!=null) {%>
                      	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoProjSettings.getAppId()%>">Project Configuration</a></li>
                      <%} %>
                    </ul>
                    <%} else {%>
                    <ul class="nav child_menu">
                      <li>(no access)</li>
                    </ul>
                    <%} %>
                  </li>
                  <li><a><i class="fa fa-tasks"></i> System Settings <span class="fa fa-chevron-down"></span></a>
                  	<% if (l_sb_bIsSystemAdminUser || l_sb_bIsITServiceDeskUser) {%>
                    <ul class="nav child_menu">
	                  <% if (l_sb_bIsSystemAdminUser && l_sb_oAppInfoAppSettings!=null) {%>
                      	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoAppSettings.getAppId()%>">App Configuration</a></li>
                      <%} %>
						<% if (l_sb_bIsSystemAdminUser && l_sb_oAppInfoSystemConfigurationSetting!=null) {%>
                      	 <li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoSystemConfigurationSetting.getAppId()%>">System Configuration</a></li> 
                      <%} %>
 						<% if (l_sb_bIsITServiceDeskUser && l_sb_oAppInfoUserManagement!=null) {%>
                      	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoUserManagement.getAppId()%>">User Management</a></li>
                       <% }
 						
 						 %>
  						<% if (!(l_sb_bIsITServiceDeskUser && l_sb_oAppInfoUserManagement!=null) && l_sb_oAppInfoUserManagement!=null) {%>
                       	<li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoUserManagement.getAppId()%>">User Management</a></li>
                        <% }
 						
 						
 						if (l_sb_bIsSystemAdminUser && l_sb_oAppInfoProjManagement!=null) {%>
                      	 <li><a href="LandingServlet?portal_appid=<%=l_sb_oAppInfoProjManagement.getAppId()%>"><%=l_sb_oAppInfoProjManagement.getAppName()%></a></li> 
                      <%} %>
                    </ul>
                    <%} else {%>
                    <ul class="nav child_menu">
                      <li>(no access)</li>
                    </ul>
                    <%}%>
                  </li>
                  
                 
                </ul>
              </div>
              <div class="menu_section">
              </div>
            </div>
