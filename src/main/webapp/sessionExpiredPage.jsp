<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="../../cssPage.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Session Expired</title>
<% 
request.getSession().invalidate();
response.setHeader("Cache-Control","no-cache, no-store, must-revalidate"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires", -1); 
response.setHeader("Expires","0");
%> 

<script>
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        var end =setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (--timer < 0) {
               	window.location = "loginPage.jsp";
                clearInterval(end);
            }
        }, 1000);
    }

    window.onload = function () {
        var fiveSecond = 5,
            display = document.querySelector('#time');
        startTimer(fiveSecond, display);
    };
</script>

</head>
<body>
<div align="center">Your session has been expired, you will be redirecting to login page within <span id="time"></span> seconds</div>
<div align="center">If you don't want to wait  <a href="loginPage.jsp">click here </a>to login again</div>
<div id="message"></div>
</body>
</html>