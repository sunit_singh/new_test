<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sciformix.commons.SciError"%>
<%@page import="java.util.List"%>

<script>
	<%@include file="/WEB-INF/js/error.js" %>
</script>

<%
List<SciError> ge_errorMsgsList = (ArrayList<SciError>)request.getAttribute(WebConstants.RESPONSE_ERROR_LIST);
if (ge_errorMsgsList != null && ge_errorMsgsList.size() > 0)
{
	%>
	<div id="_errordiv_svr" class="alert alert-danger alert-dismissible fade in" role="alert">
	<!-- One or more errors encountered: 
	<span aria-hidden="true">�</span>
	-->
	<%
	for (SciError error : ge_errorMsgsList)
	{
	%>
		<div>
		<i class="glyphicon glyphicon-exclamation-sign"></i>&nbsp;<strong><%=error.errorCode().errorCode() %>:</strong>&nbsp;<%=error.errorMessage()%></div>
	<%
		if(error.errorCode().errorCode() == 53)
		{
			request.setAttribute(WebConstants.TEMP_FLAG, "true");
		}	
	}
	%>
	</div>
	<%
}
request.setAttribute(WebConstants.RESPONSE_ERROR_LIST, null);

%>

<div id="_errordiv_lcl" class="alert alert-danger alert-dismissible fade in" role="alert" style="display:none">
</div>

