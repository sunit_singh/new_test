<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>

<script>
</script>

<%


UserSession l_tnb_oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);

AppInfo l_tnb_oAppInfoUserPrefs = AppRegistry.getUserPrefsApp();
UserInfo l_tnb_oSessionUser = (l_tnb_oUserSession != null ? l_tnb_oUserSession.getUserInfo() : null);

String l_tnb_sUserName = (l_tnb_oSessionUser!=null?l_tnb_oSessionUser.getUserDisplayName():"Anonymous");
String l_tnb_sInitials = "";
String[] l_tnb_sarr = l_tnb_sUserName.split(" ");

for(int i = 0; i < l_tnb_sarr.length; i++) 
{
	l_tnb_sInitials += l_tnb_sarr[i].substring(0,1).toUpperCase();
}

String l_tnb_sUserPreferencesUrl = (l_tnb_oAppInfoUserPrefs != null ? "LandingServlet?portal_appid=" + l_tnb_oAppInfoUserPrefs.getAppId() : "");
String l_tnb_sLogoutUrl = "LoginServlet?logoutRequest=yes";
%>
<!--         <div class="top_nav" style="background-color:rgb(33,150,243)"> -->
             <div class="top_nav" style="background-color:rgb(33,150,243)">
        <!--  <div><IMG src="images/sciformix_header.png" alt="Sciformix Logo" width="200" /> -->
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars custom-bar-white"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="" style="background-color:rgb(0,90,145)">
                	<a href="<%=l_tnb_sLogoutUrl%>">
                		<i class="fa fa-sign-out outerLogoutButton">
                		</i>
                	</a>
                </li>
                <li class=""  style="background-color:rgb(0,90,145)">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="<%=l_tnb_sUserName%>" >
                    <b>
                    <%=l_tnb_sInitials%>
                    <span style="display:inline!important;" class=" fa fa-angle-down"></span>
                    </b>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <% if (l_tnb_oAppInfoUserPrefs != null) { %>
                    <li>
                      <a href="<%=l_tnb_sUserPreferencesUrl%>"><i class="fa fa-edit pull-right"></i>
                        <span>Preferences</span>
                      </a>
                    </li>
                    <%} %>
                    <li><a href="<%=l_tnb_sLogoutUrl%>"><i class="fa fa-sign-out pull-right"></i> Logout</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
         </div>
