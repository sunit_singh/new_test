	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/icheck.js"></script>
	
	<script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="js/moment/moment.min.js"></script>
	
    <script type="text/javascript" src="js/fastclick.js"></script>
    <script type="text/javascript" src="js/nprogress.js"></script>
    <script type="text/javascript" src="js/Chart.js"></script>
    <script type="text/javascript" src="js/gauge.js"></script>
    <script type="text/javascript" src="js/bootstrap-progressbar.js"></script>
    <script type="text/javascript" src="js/bootstrap-wysiwyg.min.js"></script>
    <script type="text/javascript" src="js/skycons.js"></script>
    <script type="text/javascript" src="js/jquery.flot.js"></script>
    <script type="text/javascript" src="js/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="js/jquery.flot.time.js"></script>
    <script type="text/javascript" src="js/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="js/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="js/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="js/chart/jquery.flot.spline.min.js"></script>
    <script type="text/javascript" src="js/chart/curvedLines.js"></script>
    <script type="text/javascript" src="js/chart/jquery.flot.axislabels.js"></script>
    <script type="text/javascript" src="js/chart/Flot_Dashes.js"></script>
    <!-- DateJS -->
    <script type="text/javascript" src="js/chart/date.js"></script>
    <!-- JQVMap -->
    <script type="text/javascript" src="js/jquery.vmap.js"></script>
    <!-- <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script> -->
    <!-- <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script> -->
    <!-- bootstrap-daterangepicker -->
    <!-- <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script> -->

    <script type="text/javascript" src="js/switchery.js"></script>
    <script type="text/javascript" src="js/prettify.min.js"></script>
    <script type="text/javascript" src="js/transitionize.js"></script>
    <script type="text/javascript" src="js/morris/morris.min.js"></script>
    <script type="text/javascript" src="js/morris/raphael.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script type="text/javascript" src="js/custom.js"></script>
	