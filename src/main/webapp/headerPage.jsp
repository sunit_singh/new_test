<%@page import="com.sciformix.sciportal.user.UserSession"%>
<%@page import="com.sciformix.sciportal.utils.WebUtils"%>
<%@page import="com.sciformix.sciportal.apps.AppInfo"%>
<%@page import="com.sciformix.sciportal.apps.AppRegistry"%>
<%@page import="com.sciformix.sciportal.user.UserInfo"%>
<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@ page import="java.util.List" %>

<%
List<AppInfo> listApps = (List<AppInfo>) request.getSession().getAttribute("listBusinessApps");
boolean bFirstApp = false;
AppInfo oAppInfoUserPrefs = null;
AppInfo oAppInfoAppSettings = null;
AppInfo oAppInfoProjSettings = null;
boolean bIsSystemAdminUser = false;
boolean bIsProjectAdminUser = false;
UserSession oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
UserInfo oSessionUser = (oUserSession != null ? oUserSession.getUserInfo() : null);
if (oSessionUser == null)
{
	response.sendRedirect(WebUtils.getSessionExpiredPageUri());
}
else
{
oAppInfoUserPrefs = AppRegistry.getUserPrefsApp();
oAppInfoAppSettings = AppRegistry.getAppSettingsApp();
oAppInfoProjSettings = AppRegistry.getProjSettingsApp();
bIsSystemAdminUser = oSessionUser.isSystemAdmin();
bIsProjectAdminUser = oSessionUser.isProjectAdmin();
}
%>



<div id="_header_main" style="background:white">
<table width="100%">
<tr><td><IMG src="images/sciformix_logo.png" alt="Sciformix Logo" width="150" /></td>
<td align="right">
	<b>Welcome&nbsp;<%=(oSessionUser!=null?oSessionUser.getUserDisplayName():"Anonymous")%></b>
	<br>
	<% if(oSessionUser!=null && oAppInfoUserPrefs!=null)
	{
		%>
		<a href="LandingServlet?portal_appid=<%=oAppInfoUserPrefs.getAppId()%>"><%=oAppInfoUserPrefs.getAppName()%></a>&nbsp;|&nbsp;
		<%
	}
	
	if(oSessionUser!=null)
	{
	%>
	<a href="LoginServlet?logoutRequest=yes">Logout</a>
	<%
	}
	%>
</td></tr>
</table>
</div>

<div id="_header_menu" style="background:blue; color:white">
<table width="100%">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2">
<%
if (listApps != null)
{
	bFirstApp = true;
	for(AppInfo oAppInfo : listApps)
	{
		if (!bFirstApp)
		{
			%>|&nbsp;<%
		}
		%><a style="color:white" href="LandingServlet?portal_appid=<%=oAppInfo.getAppId()%>"><%=oAppInfo.getAppName()%></a>&nbsp;<%
		bFirstApp = false;
	}
}

if(oSessionUser!=null && oAppInfoAppSettings!=null && bIsProjectAdminUser)
{
	if (!bFirstApp)
	{
		%>|&nbsp;<%
	}
	%><a style="color:white" href="LandingServlet?portal_appid=<%=oAppInfoProjSettings.getAppId()%>"><%=oAppInfoProjSettings.getAppName()%></a>&nbsp;<%
}

if(oSessionUser!=null && oAppInfoAppSettings!=null && bIsSystemAdminUser)
{
	if (!bFirstApp)
	{
		%>|&nbsp;<%
	}
	%><a style="color:white" href="LandingServlet?portal_appid=<%=oAppInfoAppSettings.getAppId()%>"><%=oAppInfoAppSettings.getAppName()%></a>&nbsp;<%
}

if (bFirstApp)
{
	%><i>No Apps for use</i><%
}

%>


</td></tr>
</table>
</div>