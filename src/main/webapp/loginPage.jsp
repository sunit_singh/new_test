<%@page import="com.sciformix.sciportal.utils.SystemUtils"%>
<%@page import="com.sciformix.sciportal.utils.BootstrapUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
  
<!DOCTYPE html>
<html>
<head>

<%@include file="cssPageIncludes.jsp" %>

<%
	String l_loginUseridValidationRegex	=	"[A-Za-z0-9.@]{1,50}$";
	if (BootstrapUtils.isInstanceBootstrapped()) {
		l_loginUseridValidationRegex	=	SystemUtils.getLoginUseridValidationRegex();
	}
	
%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sciformix Safety Portal</title>
<link rel="shortcut icon" href="favicon.ico">

<script type="text/javascript">
	function validateInput()
	{
		var userName=document.getElementById("userName").value;
		var password=document.getElementById("password").value;
		clearError();
		
		if(userName=="")
		{
			displayErrorMessage("UI-1001");
			return false;
		}
		if(password=="")
		{
			displayErrorMessage("UI-1002");
			return false;
		}
		return true;
	}
	
	function submitForm()
	{
		var form = document.getElementById("scp_login_frm");
		form.submit();
	}
	
	function clearError()
	{
		//alert("clearError - 1 " + document.getElementById("_errordiv_svr").innerHTML);
		document.getElementById("_errordiv_svr").innerHTML = "";
		//alert("clearError - 2 " + document.getElementById("_errordiv_svr").innerHTML);
	}
	
</script>
<% 
response.setHeader("Cache-Control","no-cache, no-store, must-revalidate"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires", -1); 
response.setHeader("Expires","0");

%> 
<style type="text/css">


</style>
</head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
	 <!-- <div align="center"><IMG src="images/sciformix_header.png" alt="Sciformix Logo" width="600" height="160" /></div>
       -->
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <csrf:form id="scp_login_frm" class="form-horizontal" action="LoginServlet" method="post" onsubmit="return validateInput();">
              <div><IMG src="images/sciformix_logo.png" alt="Sciformix Logo" width="200" /></div>
              <br/>
              <h1>Safety Portal</h1>
              <div class="pull-left">Username</div>
              <div>
                <input type="text" id="userName" name="userName" class="form-control" pattern="<%= l_loginUseridValidationRegex %>" placeholder="Username" autofocus required="" autocomplete="off">
              </div>
              <div class="pull-left">Password</div>
              <div>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="" autocomplete="off">
              </div>
              <div>
                <!--<a class="btn btn-default submit" href="javascript:submitForm();">Login</a>-->
                  <button class="btn btn-default submit" type="submit" >Login</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div>
                  <p>Powered by SciPortal</p>
                  <p>2018 Sciformix. All rights reserved</p>
                </div>
              </div>
              
            </csrf:form>
          </section>
        </div>

      </div>
    </div>
  

	
<%@include file="errorPage.jsp" %>

</body>
</html>
