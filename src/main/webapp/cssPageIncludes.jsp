    <link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/all.css">
	
	<link rel="stylesheet" href="css/maps/jquery-jvectormap-2.0.3.css">

    <link rel="stylesheet" href="css/nprogress.css">
    <link rel="stylesheet" href="css/flat/green.css">
    <link rel="stylesheet" href="css/bootstrap-progressbar-3.3.4.css">
    <link rel="stylesheet" href="css/jqvmap.css" >
    <link rel="stylesheet" href="css/daterangepicker.css">
    <link rel="stylesheet" href="css/switchery.css">
    <link rel="stylesheet" href="css/prettify.min.css">
    <link rel="stylesheet" href="css/bootstrap-wysiwyg-style.css">

    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="stylesheet" href="css/SciCustom.css">
