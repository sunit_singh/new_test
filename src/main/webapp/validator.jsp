<!-- validation Fields form fields-->
<%@page import="com.sciformix.commons.utils.StringUtils"%>
<%@page import="org.apache.wink.json4j.JSONObject"%>
<%@page import="com.sciformix.sciportal.validation.ValidationHome"%>
<%@page import="com.sciformix.sciportal.validation.ValidationFieldDetails"%>
<%@page import="com.sciformix.sciportal.web.WebConstants"%>
<%@page import="java.util.Map"%>
<%
	Map<String, Map<String, ValidationFieldDetails>> validationFieldDetailsMap = ValidationHome.getValidationFieldDetailsMap();
	Map<String, Map<String, ValidationFieldDetails>> wildCardValidationFieldDetailsMap = ValidationHome.getWildCardalidationFieldDetailsMap();
	JSONObject standerdJsonObj = new JSONObject(validationFieldDetailsMap);
	JSONObject wildCardJsonObj = new JSONObject(wildCardValidationFieldDetailsMap);
	String		l_jsppagename	=	(String)request.getAttribute(WebConstants.REQUEST_PARAMETER_CONTENT_PAGE_NAME);
	if (!StringUtils.isNullOrEmpty(l_jsppagename)) {
		l_jsppagename	=	l_jsppagename.replaceAll(".jsp","");
	}
%>
<script type="text/javascript">
var validationMap = JSON.parse('<%=standerdJsonObj%>');
var wildCardValidationMap = JSON.parse('<%=wildCardJsonObj%>');
var jspPageName	=	'<%=l_jsppagename%>';
</script>
<script>
	<%@include file="/WEB-INF/js/validation.js" %>
</script>
