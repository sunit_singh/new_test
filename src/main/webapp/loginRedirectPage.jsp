<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>

<% 
response.setHeader("Cache-Control","no-cache, no-store, must-revalidate"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader("Expires", -1); 
response.setHeader("Expires","0");
%> 

<html>
	<head>
		<script type="text/JavaScript">
			function redirectRequest() {
				document.frmmain.submit();
			}
		</script>
	</head>
	<body onload="redirectRequest()">
		<csrf:form id="frmmain" name="frmmain" method="post" action="LandingServlet">
			
		</csrf:form>
	</body>
</html>