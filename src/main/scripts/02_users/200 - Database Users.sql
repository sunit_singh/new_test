
-- Part 1: Create User to serve as the 'Schema Owner' of all the objects
-- Part 1a: Create User
CREATE USER scfx_so IDENTIFIED BY "scfx_SO_pwd"
  DEFAULT TABLESPACE users
  TEMPORARY TABLESPACE temp
  QUOTA UNLIMITED ON users;

-- Part 1b: Setup User privileges
--TODO: REVOKE CREATE SESSION FROM SCFX_SO;
--REVOKE CONNECT


-->>
ALTER USER SCFX_SO quota unlimited on TBS_SYSTEM;
ALTER USER SCFX_SO quota unlimited on TBS_USER;
ALTER USER SCFX_SO quota unlimited on TBS_PROJECT;
ALTER USER SCFX_SO quota unlimited on TBS_AUDIT;
ALTER USER SCFX_SO quota unlimited on TBS_COMMON;
ALTER USER SCFX_SO quota unlimited on TBS_APP;
-->>

-- Part 2: Create User Roles for Application Users
-- Part 2a: Create User Role for Application User with Read-write access
CREATE ROLE SCFX_ROLE_CU_RW;
GRANT CONNECT TO SCFX_ROLE_CU_RW;
GRANT CREATE SESSION TO SCFX_ROLE_CU_RW;

-- Part 2b: Create User Role for Application User with Read-only access (primarily for support)
CREATE ROLE SCFX_ROLE_CU_RO;
GRANT CONNECT TO SCFX_ROLE_CU_RO;
GRANT CREATE SESSION TO SCFX_ROLE_CU_RO;


-- Part 3: Create Application Connect User as one with Read-write access to the schema
-- Part 3a: Create User
CREATE USER scfx_cu IDENTIFIED BY "scfx_CU_pwd"
  DEFAULT TABLESPACE users
  TEMPORARY TABLESPACE temp;

-- Part 3b: Setup User privileges
-- DEV-NOTE: Explicit privileges on each object needs to be granted post-object creation
GRANT SCFX_ROLE_CU_RW TO scfx_cu;


-- Part 4: Create Application Support User as one with Read-only access to the schema
-- DEV-NOTE: This user could be granted read-write access if really required too
-- Part 4a: Create User
CREATE USER scfx_su1 IDENTIFIED BY "scfx$1234"
  DEFAULT TABLESPACE users
  TEMPORARY TABLESPACE temp;

-- Part 4b: Setup User privileges
-- DEV-NOTE: Explicit privileges on each object needs to be granted post-object creation
GRANT SCFX_ROLE_CU_RO TO scfx_su1;


