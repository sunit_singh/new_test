CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('system', 10015, 'Error refreshing system config');

--Namespace Starts - activity
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('activity', 111001, 'Error fetching user activity trail');
--Namespace Ends - activity

--Namespace Starts - mailbox
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('mailbox', 12101, 'Error saving mailbox');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('mailbox', 12102, 'Error fetching mailbox list');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('mailbox', 12103, 'Error fetching mail list');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('mailbox', 12104, 'Error parsing msg file');

CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('mailbox', 12105, 'Error project details not found');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('mailbox', 12106, 'Error parsing email fields');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('mailbox', 12107, 'Error fetching Json array values');
--Namespace Ends - mailbox

CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('appseq', 12001, 'Error registering the sequence');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('appseq', 12002, 'Sequence is already present in the system');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('appseq', 12003, 'Error retrieving application sequence mnemonic');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('appseq', 12004, 'Maximum sequence number already riched. Please contact the administrator.');