-- Namespace start - webservices
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 100000, 'Success.');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110000, 'Invalid Input. UserID/Password cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110001, 'Authentication denied as UserID contains invalid characters.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110002, 'Authentication denied as User is not registered.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110003, 'Authentication Failed. Password does not match.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110004, 'Something went wrong. Please contact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110005, 'Invalid Input. ActivityCode cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110006, 'Activity Registration failed as UserId or Password do not match. Please re-authenticate.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110007, 'Activity Registration failed. Please contact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110008, 'Invalid Input. TemplateId cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110009, 'Template ID does not exist.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110010, 'Template Export Failed. Please contact administrator.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110011, 'Invalid Template ID.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110012, 'Template Export failed as UserId or Password do not match. Please re-authenticate.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110013, 'Activity not registered. Please recheck the Activity ID.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110014, 'Invalid Input. ActivityID is mandatory and has to be numeric.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110015, 'Invalid Input. ProjectSeqId is mandatory and has to be numeric.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110016, 'Invalid Input. ActivityType cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110017, 'Auhentication not completed. Please authenticate and retry.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110018, 'Invalid Input. TemplateId is mandatory and has to be numeric.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110019, 'Invalid Input. KeyID cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110020, 'Authentication failed. Please contact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110021, 'Invalid Input. ActivityApp cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110022, 'Invalid Input. Timestamp not valid.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110023, 'Web APIs are currently not active. Please conact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110024, 'Bad Request. Please check the json structure.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110025, 'Authentication Failed. Incorrect KeyId.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110026, 'Tool Version not latest. Kindly download the latest version from the portal.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110027, 'Invalid Input. Timezone cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110028, 'Invalid Input. Timestamp cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110029, 'Invalid Input. ToolName cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110030, 'Invalid Input. ToolVersion cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110031, 'Invalid Input. ClientReqId cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110032, 'Invalid Input. MachineName cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110033, 'Invalid Input. MachineDomain cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110034, 'Invalid Input. IPAddress cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110035, 'Invalid Input. OSName cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110036, 'Invalid Input. OSVersion cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110037, 'Invalid Input. OSArchitecture cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110038, 'Invalid Input. JDKVersion cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110039, 'Invalid Input. MachineLoggedInUser cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110040, 'Invalid Input. MachineLoggedInUserDomain cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110041, 'Tool Verification Failed for ToolName. Incorrect ToolName.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110042, 'Tool Verification Failed for ToolVersion. Incorrect ToolVersion.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110043, 'Server Side Runtime IQ Check Failed for OS Version.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110044, 'Server Side Runtime IQ Check Failed for JAVA Version.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110045, 'Invalid Input. ClientIQHash cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110046, 'Client IQ Check Failed. Cannot allow further processing.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110047, 'Invalid Timestamp Format. Please correct the format (dd-MM-yyyy HH.mm.ss).');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110048, 'Invalid ProjectSeqId. User not allocated to requested project.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110049, 'Invalid BrowserVersion : CHROME. Version not allowed.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110050, 'Invalid BrowserVersion : Internet Explorer. Version not allowed.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110051, 'Invalid BrowserVersion : CHROME. Version not supported.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110052, 'Invalid BrowserVersion : Internet Explorer. Version not supported.');
-- Namespace end - webservices

-- Namespace start - activity
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('activity', 1001, 'Error fetching user info for activity');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('activity', 1002, 'Error fetching project info for activity');
-- Namespace end - Activity

-- Namespace start - Mailbox
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1101, 'Error Mail Box Name Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1102, 'Error Mail Box Name Invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1103, 'Error Mail Box Email Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1104, 'Error Mail Box Description Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1105, 'Error Mail Box Mnemonics Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1106, 'Error Mail Box Name not unique ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1107, 'Error Updated value same as existing');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1108, 'Error Mail Box Mnemonics Invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1109, 'Error Uploading MSG File');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1110, 'Error in exporting mail contents');

-- Namespace end - Mailbox