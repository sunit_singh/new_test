--CALL SCFX_SO.CMNPRC_REGISTER_BIZAPP ('MB-PORTAL-APP', 'Mail Box', 'com.sciformix.sciportal.apps.mb.MBPortalApp', 'com.sciformix.sciportal.web.apps.mb.MBPortalWebApp');
--Activity Related scripts - start
CALL SCFX_SO.CMNPRC_REGISTER_SYSAPP ('ACTIVITY-PORTAL-APP', 'Activity Trail Viewer', 'com.sciformix.sciportal.apps.activity.ActivityPortalApp', 'com.sciformix.sciportal.web.apps.activity.ActivityPortalWebApp');
--Activity Related scripts - end

-- Added for WebAPIs
CALL SCFX_SO.CMNPRC_REGISTER_SYSAPP ('WEBAPI-CLIENT-TOOL', 'WebAPI Client Tool', 'com.sciformix.sciportal.apps.sys.UserPrefsPortalApp', 'com.sciformix.sciportal.web.apps.sys.UserPrefsPortalWebApp');

CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP','view.max.days','Max View Days','Max View Days','7','7');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP','report.max.days','Max Report Days','Max Report Days','7','7');