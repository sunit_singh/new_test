CREATE OR REPLACE FUNCTION SCFX_SO.GETNEXTNUMBER (proj_seqid IN NUMBER, seq_purpose IN VARCHAR2, context_seqid IN NUMBER)
RETURN NUMBER IS 
 curr_number NUMBER;
 max_number NUMBER;
BEGIN
  select LASTNUMBER, MAXNUMBER 
  INTO curr_number,max_number
  from SCFX_SO.CMN_APPCONTEXTSEQUENCE
  where PRJSEQID =  proj_seqid
  AND SEQPURPOSE = seq_purpose
  AND CONTEXTSEQID = context_seqid
  AND NUMBERCONTEXT = 'U'
  FOR UPDATE;
  
  curr_number :=  curr_number+1;
  if curr_number <= max_number then
    UPDATE SCFX_SO.CMN_APPCONTEXTSEQUENCE
    set LASTNUMBER = curr_number
    where PRJSEQID =  proj_seqid
    AND SEQPURPOSE = seq_purpose
    AND CONTEXTSEQID = context_seqid
    AND NUMBERCONTEXT = 'U';
    COMMIT;
  else
    ROLLBACK;
    raise_application_error( -20001, 'Current Number cannot exceed the maximum allowed number' );
  end if;
  RETURN curr_number;
END;
/
GRANT EXECUTE  ON SCFX_SO.GETNEXTNUMBER TO SCFX_ROLE_CU_RW;
--No Privileges to be granted for Stored Procedures to SCFX_ROLE_CU_RO
/