--CPAT Download Tool (Values will change.)
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.jarname', 'JarName', 'Jar Name', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.permissibletoolnames', 'Toolname', 'Toolname', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.permissibletoolversions','ToolVersion', 'ToolVersion', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.permissiblejavaversions','JavaVersion', 'JavaVersion', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.permissibleosversions','OSVersion', 'OSVersion', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.clienthash','ClientHash', 'Client Hash', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.allowedchrome','AllowedChrome', 'Allowed Chrome Versions', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.disallowedchrome','DisallowedChrome', 'Disallowed Chrome Versions', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.allowedie','Allowed IE', 'Allowed IE Versions', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('WEBAPI-CLIENT-TOOL', 'tool.CPAT.disallowedie','Disallowed IE', 'Disallowed IE Versions', 'STRING', '');