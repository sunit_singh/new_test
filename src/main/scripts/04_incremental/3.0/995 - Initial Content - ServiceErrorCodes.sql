--Namespace Start - tools
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('tools', 11601, 'Error during downloading file');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('tools', 11602, 'Error during parsing file');
--Namespace Start - tools

CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('system', 10012, 'Error Removing User Preferences');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('system', 10013, 'Error creating users');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('system', 10014, 'Error updating users');

CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('ngv', 11126, 'Error Fetching list of rules from database');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('ngv', 11127, 'Error Fetching Validation RuleSets');


CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11211, 'Error Uploading case master file');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11212, 'Error Fetching case records from database');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11213, 'Error parsing case records from PVCP case master file');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11214, 'Uploaded casemaster file is empty');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11215, 'Case master record date should be in "dd-MMM-yyyy" format');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11216, 'Error Saving Case Master Record to DB');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11217, 'Error Updating Case Master Record');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11218, 'Error Fetching Case Review History By Internal Case Id');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11219, 'Starting date for the project is not configured or it is not correct');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11220, 'Template cannot be activated as none of the fields for review are checked');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11221, 'Case Review download template sheet number not maintained at project level or is not a number');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11222, 'Review Type is not maintained at project level');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11223, 'Please define the threshold value for category');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11224, 'Please select one of the fields in the same category for which the category threshold is defined');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11225, 'Please enter correct value in the threshold for category');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11226, 'Error creating CP Dashboard');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('qr', 11227, 'Error exporting CP Data');

CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('prjset', 11607, 'Error updating project config');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('common', 11704, 'Error in parsing uploaded file');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('common', 11705, 'Error in parsing column');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('common', 11706, 'Please maintain the mapping in file upload');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('common', 11707, 'Query cannot be found for the given query id.');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('common', 11708, 'Please maintain the file format property for the project. Kindly get in touch with the system administrator to proceed further');

--Namespace Start - projectuser
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('projectuser', 11501, 'Error Saviong Project User Information');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('projectuser', 11502, 'Error Fetching User List');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('projectuser', 11503, 'Error Fetching Project User');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('projectuser', 11504, 'Error Removing Project User');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('projectuser', 11505, 'Error Saving Project');
--Namespace End - projectuser

--Namespace Starts - job
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('job', 11801, 'Error saving job execution track');
--Namespace ends - job