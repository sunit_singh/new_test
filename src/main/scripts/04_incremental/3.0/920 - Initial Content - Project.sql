CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.type.all', 'List of all the reviews applicable for the project', 'RT1:Review Type 1,RT2:Review Type 2,RT3:Review Type 3,RT4:Review Type 4,RT5:Review Type 5', 'STRING', 'RT1:Review Type 1,RT2:Review Type 2,RT3:Review Type 3,RT4:Review Type 4,RT5:Review Type 5');

CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.configuration.RT1', 'Case Completion E-Mail Notification Setting Review Type 1', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.cc.list.RT1', 'Email List for CC Review Type 1', 'a@a.com,b@b.com,c@c.com', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.initial.RT1', 'Email purpose for initial Review Type 1', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.edit.RT1', 'Email purpose for edit Review Type 1', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.qrt.mapping.RT1', 'Quality Review Template to be used for the Review Type 1', 'Enter Quality Review Template Sequence Id', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.configuration.RT2', 'Case Completion E-Mail Notification Setting Review Type 2', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.cc.list.RT2', 'Email List for CC Review Type 2', 'a@a.com,b@b.com,c@c.com', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.initial.RT2', 'Email purpose for initial Review Type 2', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.edit.RT2', 'Email purpose for edit Review Type 2', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.qrt.mapping.RT2', 'Quality Review Template to be used for the Review Type 2', 'Enter Quality Review Template Sequence Id', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.configuration.RT3', 'Case Completion E-Mail Notification Setting Review Type 3', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.cc.list.RT3', 'Email List for CC Review Type 3', 'a@a.com,b@b.com,c@c.com', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.initial.RT3', 'Email purpose for initial Review Type 3', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.edit.RT3', 'Email purpose for edit Review Type 3', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.qrt.mapping.RT3', 'Quality Review Template to be used for the Review Type 3', 'Enter Quality Review Template Sequence Id', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.configuration.RT4', 'Case Completion E-Mail Notification Setting Review Type 4', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.cc.list.RT4', 'Email List for CC Review Type 4', 'a@a.com,b@b.com,c@c.com', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.initial.RT4', 'Email purpose for initial Review Type 4', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.edit.RT4', 'Email purpose for edit Review Type 4', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.qrt.mapping.RT4', 'Quality Review Template to be used for the Review Type 4', 'Enter Quality Review Template Sequence Id', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.configuration.RT5', 'Case Completion E-Mail Notification Setting Review Type 5', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.review.completion.cc.list.RT5', 'Email List for CC Review Type 5', 'a@a.com,b@b.com,c@c.com', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.initial.RT5', 'Email purpose for initial Review Type 5', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'email.purpose.edit.RT5', 'Email purpose for edit Review Type 5', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.qrt.mapping.RT5', 'Quality Review Template to be used for the Review Type 5', 'Enter Quality Review Template Sequence Id', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'start.month.day', 'Starting date of a month for the project', '1-28', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases', 'Category and their aliases', 'Cat 1~Cat 2~Cat 3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.errortype', 'Error Code and their name applicable for the project', 'E1:Error1,E2:Error2,E3:Error3,E4:Error4,E5:Error5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type', 'Report Type for the Full Summary Report', '1-6', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.completion.downloadflag', 'Case Review download flag', 'true/false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'input.date.format', 'Input date format to be used to parse the date', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'output.date.format', 'Output date format to be used within system', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'graph.threshold.seriousness', 'Threshold value for Serious cases', '1-100', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'graph.threshold.nonseriousness', 'Threshold value for Non-Serious cases', '1-100', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'casereview.download.sheet.number', 'Sheet number of the download template', '', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'cm.file.format', 'List of all the file formats', 'F1:File Format 1,F2:File Format 2,F3:File Format 3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display', 'Medical Reviewer Display', 'true or false', 'STRING', '');

--udf support
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf1.displayname', 'Case Master udf1 field display name', 'Display Name Column Name for UDF 1', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf1.datatype', 'Case Master udf1 field datatype', 'Data type for UDF 1', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf2.displayname', 'Case Master udf2 field display name', 'Display Name Column Name for UDF 2', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf2.datatype', 'Case Master udf2 field datatype', 'Data type for UDF 2', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf3.displayname', 'Case Master udf3 field display name', 'Display Name Column Name for UDF 3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf3.datatype', 'Case Master udf3 field datatype', 'Data type for UDF 3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf4.displayname', 'Case Master udf4 field display name', 'Display Name Column Name for UDF 4', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf4.datatype', 'Case Master udf4 field datatype', 'Data type for UDF 4', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf5.displayname', 'Case Master udf5 field display name', 'Display Name Column Name for UDF 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf5.datatype', 'Case Master udf5 field datatype', 'Data type for UDF 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf6.displayname', 'Case Master udf6 field display name', 'Display Name Column Name for UDF 6', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf6.datatype', 'Case Master udf6 field datatype', 'Data type for UDF 6', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf7.displayname', 'Case Master udf7 field display name', 'Display Name Column Name for UDF 7', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf7.datatype', 'Case Master udf7 field datatype', 'Data type for UDF 7', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf8.displayname', 'Case Master udf8 field display name', 'Display Name Column Name for UDF 8', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf8.datatype', 'Case Master udf8 field datatype', 'Data type for UDF 8', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf9.displayname', 'Case Master udf9 field display name', 'Display Name Column Name for UDF 9', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf9.datatype', 'Case Master udf9 field datatype', 'Data type for UDF 9', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf10.displayname', 'Case Master udf10 field display name', 'Display Name Column Name for UDF 10', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_PRJ_CONFIG_SETTING ('QR-PORTAL-APP', 'review.udf10.datatype', 'Case Master udf10 field datatype', 'Data type for UDF 10', 'STRING', '');