--Namespace Start - Tools
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('tool', 801, 'Error in splitting file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('tool', 802, 'Error in downloading zip file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('tool', 803, 'Input file not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('tool', 804, 'File already been downloaded');
--Namespace Start - Tools
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 50, 'Application not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 51, 'Portal action not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 52, 'Invalid login credentials for user');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 53, 'Application not enabled for user');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 54, 'User is not registered');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 55, 'Invalid action');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 56, 'Invalid user type');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 63, 'Invalid input parameter');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 114, 'Multiple files are not allowed');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 115, 'Maximum upload file count exceeded');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 131, 'Invalid case id submitted');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 132, 'Mandatory columns not provided');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 133, 'Template ruleset cannot be activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 134, 'Template ruleset cannot be de-activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 135, 'Error updating template ruleset state');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 136, 'Invalid template ruleset name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 137, 'Non unique template ruleset name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 138, 'Invalid template name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 139, 'Template name not unique');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 140, 'Template rule criteria invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 141, 'Template rule criteria not unique');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 142, 'Case Validation Failed for Valid RuleType');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 143, 'Case Validation Failed for Error RuleType');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 144, 'Case Validation Failed for Warning RuleType');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 145, 'Case Validation Ruleset cannot be activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 146, 'Case Validation Ruleset cannot be de-activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 147, 'Rule Criteria invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 148, 'Rule Applicability invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 149, 'Invalid Rule Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 150, 'Rule Name not unique');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 151, 'Invalid Validation Ruleset Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 152, 'Non unique Validation Ruleset Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 153, 'Validation Rules not configured for Case Validation Ruleset. Hence Validation cannot be performed!!');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 154, 'Columns specified in Criteria is missing in Case File');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 155, 'Error encoding input file content');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 156, 'Error decoding input file content');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 206, 'None of the Safety Case Field template is active');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 251, 'Invalid Case Id ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 252, 'Invalid Case Version');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 253, 'Case ID cannot be blank');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 254, 'Please Select Case Master File');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 217, 'Please upload CSV or EXEL file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 218, 'Please upload CSV or EXEL file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 219, 'Error getting case record values from database');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 220, 'Case record does not exist in the system');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 221, 'Error Type is not configured for the project');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 222, 'Review Type is not selected in User Preference or it is not maintained at project level');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 223, 'Category is not configured for the project');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 224, 'Previous review type is not complete');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 225, 'Error creating error dashboard');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 255, 'Please Specify Case Id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 256, 'Please Specify Case Version');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 257, 'Please Specify Case Seriousness');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 258, 'Please Specify Case Initial Receive Date');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 259, 'Invalid Initial Receive Date Format');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 260, 'UDF Fields Mapping Not Maintain In File Upload');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('prjset', 351, 'Template Name is invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('prjset', 352, 'Template Name is not unique');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 406, 'Template name is not unique');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectusermanagement', 502, 'Please enter proper client Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectusermanagement', 503, 'Please enter proper client email Id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectusermanagement', 504, 'Please enter SafetyDBID');

--Namespace Start -User management
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 901, 'Please specify user id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 902, 'Please specify user name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 903, 'Please specify user email');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 904, 'Please select authentication type');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 905, 'User Id Already Exist');

--Namespace End - User management