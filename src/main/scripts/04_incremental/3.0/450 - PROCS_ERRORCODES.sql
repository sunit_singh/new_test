--Procedure for Web Error Code Registration
CREATE OR REPLACE PROCEDURE SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE(error_code_namespace VARCHAR2, error_code NUMBER, error_msg_text NVARCHAR2) AS
	ROWCOUNT NUMBER(1);
BEGIN
	ROWCOUNT := 0;
	SELECT COUNT(*) INTO ROWCOUNT FROM SCFX_SO.SYS_ERRORMSGS where ERRCATEGORY = 'web' and ERRNAMESPACE = LOWER(error_code_namespace) and ERRCODE = error_code;
	IF ROWCOUNT = 1 THEN
		UPDATE SCFX_SO.SYS_ERRORMSGS
		SET MSGTEXT = error_msg_text
		WHERE ERRCATEGORY = 'web' and ERRNAMESPACE = LOWER(error_code_namespace) and ERRCODE = error_code;
	ELSE
		INSERT INTO SCFX_SO.SYS_ERRORMSGS (ERRCATEGORY, ERRNAMESPACE, ERRCODE, MSGLANG, MSGTEXT)
    	VALUES ('web', LOWER(error_code_namespace), error_code, 'en', error_msg_text);
	END IF;
    COMMIT;
END;
/
GRANT EXECUTE  ON SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE TO SCFX_ROLE_CU_RW;

--Procedure for Service Error Code Registration
CREATE OR REPLACE PROCEDURE SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE(error_code_namespace VARCHAR2, error_code NUMBER, error_msg_text NVARCHAR2) AS
	ROWCOUNT NUMBER(1);
BEGIN
	ROWCOUNT := 0;
	SELECT COUNT(*) INTO ROWCOUNT FROM SCFX_SO.SYS_ERRORMSGS where ERRCATEGORY = 'svc' and ERRNAMESPACE = LOWER(error_code_namespace) and ERRCODE = error_code;
	IF ROWCOUNT = 1 THEN
		UPDATE SCFX_SO.SYS_ERRORMSGS
		SET MSGTEXT = error_msg_text
		WHERE ERRCATEGORY = 'svc' and ERRNAMESPACE = LOWER(error_code_namespace) and ERRCODE = error_code;
	ELSE
		INSERT INTO SCFX_SO.SYS_ERRORMSGS (ERRCATEGORY, ERRNAMESPACE, ERRCODE, MSGLANG, MSGTEXT)
    	VALUES ('svc', LOWER(error_code_namespace), error_code, 'en', error_msg_text);
	END IF;
    COMMIT;
END;
/
GRANT EXECUTE  ON SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE TO SCFX_ROLE_CU_RW;
/