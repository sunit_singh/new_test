alter table SCFX_SO.APP_QRL_REVIEWLOG add (CASEREVIEWSUMMSEQID	NUMBER(38,0));
alter table SCFX_SO.APP_QRL_REVIEWLOG add (CDBR	NUMBER(38,0)  DEFAULT -1);

-- Case Master DDL Start --
CREATE SEQUENCE SCFX_SO.PVCP_CASE_MASTER_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;
GRANT SELECT ON SCFX_SO.PVCP_CASE_MASTER_SEQ TO SCFX_ROLE_CU_RW;

CREATE TABLE SCFX_SO.PVCP_CASE_MASTER (
	PCMCASESEQID			NUMBER(38,0)	NOT NULL,
	PRJSEQID 				NUMBER(38,0) 	NOT NULL,
	PCMCASEID 				VARCHAR2(255)	NOT NULL,
	PCMCASEVERSION 			VARCHAR2(255) 	NOT NULL,
	PCMCASE_IRD 			DATE 			,
	PCMCASETYPE 			VARCHAR2(255) 	,
	PCMCASE_PRODUCT 		VARCHAR2(255) 	,
	PCMCASESERIOUSNESS 		VARCHAR2(255) 	,
	PCMDESRC 				NUMBER(38,0) 	,
	PCMDETYPE 				NUMBER(38,0) 	,
	PCMDEUSERSEQID 			NUMBER(38,0) 	,
	PCMDEUSERNAME 			VARCHAR2(255) 	,
	PCMRT1SRC 				NUMBER(38,0) 	,
	PCMRT1TYPE 				NUMBER(38,0) 	,
	PCMRT1USERSEQID 		NUMBER(38,0) 	,
	PCMRT1USERNAME 			VARCHAR2(255) 	,
	PCMRT2SRC 				NUMBER(38,0) 	,
	PCMRT2TYPE 				NUMBER(38,0) 	,
	PCMRT2USERSEQID 		NUMBER(38,0) 	,
	PCMRT2USERNAME 			VARCHAR2(255) 	,
	PCMRT3SRC 				NUMBER(38,0) 	,
	PCMRT3TYPE 				NUMBER(38,0) 	,
	PCMRT3USERSEQID 		NUMBER(38,0) 	,
	PCMRT3USERNAME 			VARCHAR2(255) 	,
	PCMRT4SRC 				NUMBER(38,0) 	,
	PCMRT4TYPE 				NUMBER(38,0) 	,
	PCMRT4USERSEQID 		NUMBER(38,0) 	,
	PCMRT4USERNAME 			VARCHAR2(255) 	,
	PCMRT5SRC 				NUMBER(38,0) 	,
	PCMRT5TYPE 				NUMBER(38,0) 	,
	PCMRT5USERSEQID 		NUMBER(38,0) 	,
	PCMRT5USERNAME 			VARCHAR2(255) 	,
	PCMUDF1 				VARCHAR2(255)	,
	PCMUDF2 				VARCHAR2(255) 	,
	PCMUDF3 				VARCHAR2(255) 	,
	PCMUDF4 				VARCHAR2(255) 	,
	PCMUDF5 				VARCHAR2(255) 	,
	PCMUDF6 				VARCHAR2(255) 	,
	PCMUDF7 				VARCHAR2(255) 	,
	PCMUDF8 				VARCHAR2(255) 	,
	PCMUDF9 				VARCHAR2(255) 	,
	PCMUDF10 				VARCHAR2(255) 	,
	PCMINTERNALCASEID 		VARCHAR2(255) 	NOT NULL,
	DTCREATED 				DATE 			NOT NULL,
	USRCREATED 				NUMBER(38,0) 	NOT NULL,
	CONSTRAINT PVCP_CASE_MASTER_PK PRIMARY KEY (PCMCASESEQID),
	CONSTRAINT PVCP_CASE_MASTER_UK UNIQUE (PRJSEQID, PCMCASEID , PCMCASEVERSION )
) TABLESPACE TBS_APP;

GRANT SELECT, INSERT, UPDATE, DELETE ON SCFX_SO.PVCP_CASE_MASTER TO SCFX_ROLE_CU_RW;
GRANT SELECT ON SCFX_SO.PVCP_CASE_MASTER TO SCFX_ROLE_CU_RO;
-- Case Master DDL End --

CREATE TABLE SCFX_SO.APP_QRT_DETAILS (
    CTOSEQID                        NUMBER(38,0)      NOT NULL,
    CASEQUALITYTHRESHOLD            NUMBER(5,2)       NOT NULL,
    CAT1QUALITYTHRESHOLD            NUMBER(5,2)       NULL,
    CAT2QUALITYTHRESHOLD            NUMBER(5,2)       NULL,
    CAT3QUALITYTHRESHOLD            NUMBER(5,2)       NULL,
    CASECLASSIFICATIONOVERALL       VARCHAR2(4000)    NULL,
    CASECLASSIFICATIONCAT1          VARCHAR2(4000)    NULL,
    CASECLASSIFICATIONCAT2          VARCHAR2(4000)    NULL,
    CASECLASSIFICATIONCAT3          VARCHAR2(4000)    NULL,
    REVIEWERALLOWEDCORRECT          NUMBER(2,0),
   	CASECRITERIAMODE 				NUMBER(2,0), 
	CASECRITERIA 					VARCHAR2(4000 BYTE),
	RVWCMPTDWNLDCONTENTSEQID 		NUMBER(38,0),
    CONSTRAINT APP_QRT_DETAILS_PK PRIMARY KEY (CTOSEQID)
) TABLESPACE TBS_APP;

GRANT SELECT, INSERT, UPDATE, DELETE ON SCFX_SO.APP_QRT_DETAILS TO SCFX_ROLE_CU_RW;
GRANT SELECT ON SCFX_SO.APP_QRT_DETAILS TO SCFX_ROLE_CU_RO;


CREATE TABLE SCFX_SO.APP_QRL_ERRORDATA (
	RLSEQID        	NUMBER(38,0)    NOT NULL,
	PRJSEQID		NUMBER(38,0) 	NOT NULL,
	SCFTCTOSEQID	NUMBER(38,0) 	NOT NULL,
	QRTCTOSEQID		NUMBER(38,0) 	NOT NULL,
  	CMSEQID         NUMBER(38,0) 	NOT NULL,
	REVIEWTYPE		VARCHAR2(10) 	NOT NULL,
	REVIEWROUND		NUMBER(38,0) 	NOT NULL,
	DTCREATED		DATE 			NOT NULL,
  	INTERNALCASEID  VARCHAR2(255)   NOT NULL,
  	CASEASSOC       NUMBER(38,0) 	NOT NULL,
  	CASESERIOUSNESS VARCHAR2(100),
  	FIELDMNEMONIC   VARCHAR2(64)    NOT NULL,
	ERRORTYPE       VARCHAR2(10)    NOT NULL,
  	ERRORCOMMENT    VARCHAR2(4000),
  	FIELDCAT		NUMBER(38,0)    NOT NULL,
	CONSTRAINT APP_QRL_ERRORDATA_UK UNIQUE (PRJSEQID, INTERNALCASEID, REVIEWTYPE, REVIEWROUND, FIELDMNEMONIC)
) TABLESPACE TBS_APP;

GRANT SELECT, INSERT, UPDATE, DELETE ON SCFX_SO.APP_QRL_ERRORDATA TO SCFX_ROLE_CU_RW;
GRANT SELECT ON SCFX_SO.APP_QRL_ERRORDATA TO SCFX_ROLE_CU_RO;