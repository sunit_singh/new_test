CALL SCFX_SO.SYSPRC_DELETE_SVC_ERROR_CODE('tool', 11601);
CALL SCFX_SO.SYSPRC_DELETE_SVC_ERROR_CODE('tool', 11602);

CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('SourceDocUtil', 11601, 'Error during downloading file');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('SourceDocUtil', 11602, 'Error during parsing file');

--Namespace Starts - serverconfig
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('serverconfig', 11901, 'Encryption Key Pair Generation Error');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('serverconfig', 11902, 'Error Key Encryption');
CALL SCFX_SO.SYSPRC_REGISTER_SVC_ERROR_CODE('serverconfig', 11903, 'Error Key Decryption');
--Namespace ends - serverconfig