
--Annotated System

CREATE OR REPLACE PROCEDURE SCFX_SO.CMNPRC_ANNOTATE_SYSTEM (system_name VARCHAR2, system_type VARCHAR2) AS
BEGIN
	SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEMCONFIG','system.name','System Name','System Name', system_name, system_name);
	
	SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEMCONFIG','system.type','System Type','System Type', system_type, system_type);
END;
/
GRANT EXECUTE  ON SCFX_SO.CMNPRC_ANNOTATE_SYSTEM TO SCFX_ROLE_CU_RW;
--No Privileges to be granted for Stored Procedures to SCFX_ROLE_CU_RO


CREATE OR REPLACE PROCEDURE SCFX_SO.CMNPRC_UPDATE_SYSTEMVERSION (system_version VARCHAR2) AS
  L_COMPLETE_KEY_NAME VARCHAR2(255);
  L_APP_KEY_COUNT INTEGER;
BEGIN

   L_COMPLETE_KEY_NAME := 'SYSTEMCONFIG.system.version';
   
    BEGIN
     SELECT COUNT(*) INTO L_APP_KEY_COUNT FROM SCFX_SO.SYS_CONFIG WHERE CFGKEY = L_COMPLETE_KEY_NAME;
   EXCEPTION
    WHEN OTHERS
    THEN
      L_APP_KEY_COUNT := 0;
   END;
   
	IF L_APP_KEY_COUNT = 0 THEN
		SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEMCONFIG','system.version','Application Version','Application Version', system_version, system_version);
    ELSE 
		UPDATE SCFX_SO.SYS_CONFIG SET CFGVALUE = system_version WHERE CFGKEY = L_COMPLETE_KEY_NAME;
		
		COMMIT;
	END IF;
END;
/
GRANT EXECUTE  ON SCFX_SO.CMNPRC_UPDATE_SYSTEMVERSION TO SCFX_ROLE_CU_RW;

CREATE OR REPLACE PROCEDURE SCFX_SO.MIGRATE_QRL AS
L_DBSERVER_HOSTNAME VARCHAR2(255);
L_DB_USERNAME VARCHAR2(255);
L_CLIENT_HOSTNAME VARCHAR2(255);
L_CLIENT_IPADDRESS VARCHAR2(255);
BEGIN
    FOR PROJECT_QRTREC IN (
        SELECT PRJSEQID, CFGVALUE
          FROM SCFX_SO.PRJ_CONFIG
          where CFGKEY = 'QR-PORTAL-APP.review.qrt.mapping.RT1' and PRJSEQID in (2,3,4)
    )
    LOOP
      UPDATE SCFX_SO.APP_QRL_REVIEWLOG
      SET QRTCTOSEQID = PROJECT_QRTREC.CFGVALUE
      WHERE PRJSEQID = PROJECT_QRTREC.PRJSEQID;
      INSERT INTO SCFX_SO.ADT_AUDITTRAIL (AUDITSEQID, ACTIONDATETIME, ACTIONSERVER, ACTIONAPP, ACTIONTYPE, 
          ACTIONCODE, ACTIONUSERSEQID, ACTIONUSERID, ACTIONCLIENTIP, ACTIONCLIENTHOST, 
          ACTIONPAYLOAD1, ACTIONPAYLOAD2, CHILDRECORDS, PROJECTID)
      VALUES (SCFX_SO.ADT_AUDITSEQID_SEQ.nextval, SYSDATE, L_DBSERVER_HOSTNAME, 'PLSQL/MIGRATE_QRL',
        'MIGRATION OF QRL', 'QRL QRTCTOSEQID updated from 1 to the configured one', 0, L_DB_USERNAME, L_CLIENT_IPADDRESS,
        L_CLIENT_HOSTNAME, '[{"Project Seq Id":'|| PROJECT_QRTREC.PRJSEQID ||'},{"Old QRTCTOSEQID":"1"},
		{"New QRTCTOSEQID":"' || PROJECT_QRTREC.CFGVALUE || '"}]', '', 0, -1);
   END LOOP;
   FOR PROJECT_QRTREC IN (
        SELECT RLSEQID,CASEID, CASEVERSION, PRJSEQID
          FROM SCFX_SO.APP_QRL_REVIEWLOG
          where REVIEWTYPE = 'IL' order by RLSEQID
    )
    LOOP
        BEGIN
          UPDATE SCFX_SO.APP_QRL_REVIEWLOG
          SET REVIEWTYPE = 'RT1'
          WHERE RLSEQID = PROJECT_QRTREC.RLSEQID;
          INSERT INTO SCFX_SO.ADT_AUDITTRAIL (AUDITSEQID, ACTIONDATETIME, ACTIONSERVER, ACTIONAPP, ACTIONTYPE, 
          	ACTIONCODE, ACTIONUSERSEQID, ACTIONUSERID, ACTIONCLIENTIP, ACTIONCLIENTHOST, 
          	ACTIONPAYLOAD1, ACTIONPAYLOAD2, CHILDRECORDS, PROJECTID)
      		VALUES (SCFX_SO.ADT_AUDITSEQID_SEQ.nextval, SYSDATE, L_DBSERVER_HOSTNAME, 'PLSQL/MIGRATE_QRL',
        		'MIGRATION OF QRL', 'QRL Review Type updated from IL to RT1', 0, L_DB_USERNAME, L_CLIENT_IPADDRESS,
        		L_CLIENT_HOSTNAME, '[{"Old Review Type":"IL"},{"New Review Type":"RT1"}]', '', 0, -1);
        EXCEPTION
          when others then 
            UPDATE SCFX_SO.APP_QRL_REVIEWLOG
            SET REVIEWTYPE = 'RT1', REVIEWROUND = 0
            WHERE RLSEQID = PROJECT_QRTREC.RLSEQID;
            INSERT INTO SCFX_SO.ADT_AUDITTRAIL (AUDITSEQID, ACTIONDATETIME, ACTIONSERVER, ACTIONAPP, ACTIONTYPE, 
         		ACTIONCODE, ACTIONUSERSEQID, ACTIONUSERID, ACTIONCLIENTIP, ACTIONCLIENTHOST, 
          		ACTIONPAYLOAD1, ACTIONPAYLOAD2, CHILDRECORDS, PROJECTID)
      		VALUES (SCFX_SO.ADT_AUDITSEQID_SEQ.nextval, SYSDATE, L_DBSERVER_HOSTNAME, 'PLSQL/MIGRATE_QRL',
        		'MIGRATION OF QRL', 'QRL Review Type updated from IL to RT1', 0, L_DB_USERNAME, L_CLIENT_IPADDRESS,
        		L_CLIENT_HOSTNAME, '[{"Old Review Type":"IL"},{"New Review Type":"RT1"}]', '', 0, -1);
            CONTINUE;
        END;
   END LOOP; 
   COMMIT;
END;
/
GRANT EXECUTE  ON SCFX_SO.MIGRATE_QRL TO SCFX_ROLE_CU_RW;
--No Privileges to be granted for Stored Procedures to SCFX_ROLE_CU_RO
/

CREATE OR REPLACE PROCEDURE SCFX_SO.MIGRATE_QRL_PVCP AS
L_DBSERVER_HOSTNAME VARCHAR2(255);
L_DB_USERNAME VARCHAR2(255);
L_CLIENT_HOSTNAME VARCHAR2(255);
L_CLIENT_IPADDRESS VARCHAR2(255);
BEGIN
    FOR REVIEWLOG_CASES IN (
        SELECT PRJSEQID AS PRJID, CASEID, CASEVERSION, CASERECVDATE, CASESERIOUSNESS, CASEASSOC, USRCREATED AS REVIEWER,
          REGCLASS, DTCREATED AS REVIEWDATE
          FROM SCFX_SO.APP_QRL_REVIEWLOG
          where reviewactive = 1 and (caseid,caseversion) not in (select pcmcaseid,pcmcaseversion from SCFX_SO.PVCP_CASE_MASTER)
    )
    LOOP
     INSERT INTO SCFX_SO.PVCP_CASE_MASTER(PCMCASESEQID, PRJSEQID, PCMINTERNALCASEID, PCMCASEID, PCMCASEVERSION, PCMCASE_IRD,
      PCMCASESERIOUSNESS, PCMDEUSERSEQID, PCMRT1USERSEQID, PCMUDF1, DTCREATED, USRCREATED) 
      VALUES ( SCFX_SO.PVCP_CASE_MASTER_SEQ.nextval, REVIEWLOG_CASES.PRJID,
        REVIEWLOG_CASES.CASEID || '_' || REVIEWLOG_CASES.CASEVERSION, REVIEWLOG_CASES.CASEID, REVIEWLOG_CASES.CASEVERSION,
        REVIEWLOG_CASES.CASERECVDATE, REVIEWLOG_CASES.CASESERIOUSNESS, REVIEWLOG_CASES.CASEASSOC, REVIEWLOG_CASES.REVIEWER,
        REVIEWLOG_CASES.REGCLASS, REVIEWLOG_CASES.REVIEWDATE, REVIEWLOG_CASES.REVIEWER);
      INSERT INTO SCFX_SO.ADT_AUDITTRAIL (AUDITSEQID, ACTIONDATETIME, ACTIONSERVER, ACTIONAPP, ACTIONTYPE, 
          ACTIONCODE, ACTIONUSERSEQID, ACTIONUSERID, ACTIONCLIENTIP, ACTIONCLIENTHOST, 
          ACTIONPAYLOAD1, ACTIONPAYLOAD2, CHILDRECORDS, PROJECTID)
      VALUES (SCFX_SO.ADT_AUDITSEQID_SEQ.nextval, SYSDATE, L_DBSERVER_HOSTNAME, 'PLSQL/MIGRATE_QRL_PVCP',
        'MIGRATION OF REVIEWLOG IN PVCP', 'Creation of PVCP Case Master Record from REVIEWLOG', 0, L_DB_USERNAME, L_CLIENT_IPADDRESS,
        L_CLIENT_HOSTNAME, '[{"Project Seq Id":'|| REVIEWLOG_CASES.PRJID ||'},{"Case Id":'|| REVIEWLOG_CASES.CASEID ||'},
		{"Case Version":'|| REVIEWLOG_CASES.CASEVERSION ||'}]', '', 0, -1);
   END LOOP;
   COMMIT;
END;
/
GRANT EXECUTE  ON SCFX_SO.MIGRATE_QRL_PVCP TO SCFX_ROLE_CU_RW;
--No Privileges to be granted for Stored Procedures to SCFX_ROLE_CU_RO
/

CREATE OR REPLACE PROCEDURE SCFX_SO.CMNPRC_ADD_SYS_CONFIG_RAW (app_name VARCHAR2, key_name_suffix VARCHAR2, key_display_name VARCHAR2,
key_tooltip VARCHAR2, key_value_datatype VARCHAR2, key_default_value VARCHAR2) AS
  L_COMPLETE_KEY_NAME VARCHAR2(255);
  L_APP_KEY_COUNT INTEGER;
  L_NEW_KEY_SERIALNO INTEGER;
BEGIN

   L_COMPLETE_KEY_NAME := key_name_suffix;
   
    BEGIN
     SELECT COUNT(*) INTO L_APP_KEY_COUNT FROM SCFX_SO.CMN_CONFIG_MD WHERE CFGCATEGORY = 'app_name' GROUP BY CFGCATEGORY;
   EXCEPTION
    WHEN OTHERS
    THEN
      L_APP_KEY_COUNT := 0;
   END;
   
  L_NEW_KEY_SERIALNO := L_APP_KEY_COUNT + 1;

    INSERT INTO SCFX_SO.CMN_CONFIG_MD (CFGTYPE, CFGCATEGORY, SERIALNO, CFGKEY, CFGDISPLAYNAME, CFGTOOLTIP, CFGVALUETYPE, CFGDEFAULTVALUE)
    VALUES ('SYSTEM', app_name, L_NEW_KEY_SERIALNO, L_COMPLETE_KEY_NAME, key_display_name, key_tooltip, key_value_datatype, key_default_value);
    
    INSERT INTO SCFX_SO.SYS_CONFIG (CFGCATEGORY, CFGKEY, CFGVALUE)
    VALUES (app_name, L_COMPLETE_KEY_NAME, key_default_value);
    
    COMMIT;
END;
/
GRANT EXECUTE  ON SCFX_SO.CMNPRC_ADD_SYS_CONFIG_RAW TO SCFX_ROLE_CU_RW;
--No Privileges to be granted for Stored Procedures to SCFX_ROLE_CU_RO
/
