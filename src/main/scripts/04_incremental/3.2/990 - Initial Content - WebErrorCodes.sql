CALL SCFX_SO.SYSPRC_DELETE_WEB_ERROR_CODE('tool', 801);
CALL SCFX_SO.SYSPRC_DELETE_WEB_ERROR_CODE('tool', 802);
CALL SCFX_SO.SYSPRC_DELETE_WEB_ERROR_CODE('tool', 803);
CALL SCFX_SO.SYSPRC_DELETE_WEB_ERROR_CODE('tool', 804);
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 801, 'Error in splitting file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 802, 'Error in downloading zip file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 803, 'Input file not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 804, 'File already been downloaded');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 805, 'Error in file upload');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 806, 'Error in export text file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 807, 'Error in export excel file ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 808, 'Error in export csv file ');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 226, 'Case record does not exist in the Case Master');
--Namespace Start -System Config
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 701, 'Please specify key id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 702, 'Please select key purpose');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 703, 'Please select key type');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 704, 'Invalid key id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 705, 'Key id already Exist');
--Namespace End - System Config