delete from SCFX_SO.CMN_CONFIG where cfgkey like 'QR-PORTAL-APP.email.send.configuration%';
delete from SCFX_SO.CMN_CONFIG_MD where cfgkey like 'QR-PORTAL-APP.email.send.configuration%';
commit;

CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.1', 'Project Level Email Send Configuration 1', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.2', 'Project Level Email Send Configuration 2', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.3', 'Project Level Email Send Configuration 3', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.4', 'Project Level Email Send Configuration 4', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.5', 'Project Level Email Send Configuration 5', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.6', 'Project Level Email Send Configuration 6', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.7', 'Project Level Email Send Configuration 7', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.8', 'Project Level Email Send Configuration 8', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.9', 'Project Level Email Send Configuration 9', 'S or F or A or N', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'email.send.configuration.10', 'Project Level Email Send Configuration 10', 'S or F or A or N', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.1', 'Medical Reviewer Name Display Configuration 1', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.2', 'Medical Reviewer Name Display Configuration 2', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.3', 'Medical Reviewer Name Display Configuration 3', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.4', 'Medical Reviewer Name Display Configuration 4', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.5', 'Medical Reviewer Name Display Configuration 5', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.6', 'Medical Reviewer Name Display Configuration 6', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.7', 'Medical Reviewer Name Display Configuration 7', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.8', 'Medical Reviewer Name Display Configuration 8', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.9', 'Medical Reviewer Name Display Configuration 9', 'true or false', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'mr.name.display.10', 'Medical Reviewer Name Display Configuration 10', 'true or false', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.1', 'Category Aliases 1', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.2', 'Category Aliases 2', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.3', 'Category Aliases 3', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.4', 'Category Aliases 4', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.5', 'Category Aliases 5', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.6', 'Category Aliases 6', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.7', 'Category Aliases 7', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.8', 'Category Aliases 8', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.9', 'Category Aliases 9', 'Cat1~Cat2~Cat3', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'category.aliases.10', 'Category Aliases 10', 'Cat1~Cat2~Cat3', 'STRING', '');

CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.1', 'Report Type for FSR 1', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.2', 'Report Type for FSR 2', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.3', 'Report Type for FSR 3', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.4', 'Report Type for FSR 4', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.5', 'Report Type for FSR 5', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.6', 'Report Type for FSR 6', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.7', 'Report Type for FSR 7', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.8', 'Report Type for FSR 8', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.9', 'Report Type for FSR 9', '1 or 2 or 3 or 4 or 5', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'report.fsr.type.10', 'Report Type for FSR 10', '1 or 2 or 3 or 4 or 5', 'STRING', '');
