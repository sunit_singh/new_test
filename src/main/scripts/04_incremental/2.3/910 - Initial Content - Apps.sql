
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryColumns', 'Apotex Mandatory Columns', 'Apotex Mandatory Columns', 'STRING-PDF', 'safetyreportid');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.columnsForDates', 'Apotex Columns for dates', 'Apotex Columns for dates', 'STRING', 'transmissiondate,receivedate,receiptdate');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.columnCaseId', 'Apotex Column for caseId', 'Apotex Column for caseId', 'STRING', 'safetyreportid');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.skipTags', 'Apotex Skip Tags', 'Apotex Skip Tags', 'STRING', 'safetyreport,sender,receiver,narrativeincludeclinical,primarysource,patient,drug,summary,drugreactionrelatedness');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.drug', 'Apotex Drug Tags', 'Apotex Drug Tags', 'STRING', 'drugcharacterization,medicinalproduct,obtaindrugcountry,drugauthorizationholder,drugdosageform,drugadministrationroute,drugindicationmeddraversion,drugindication,actiondrug,drugrecurreadministration,drugstartdate,drugenddate,drugtreatmentduration,drugdosagetext,drugtreatmentdurationunit');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.safetyreport', 'Apotex General Info Tags', 'Apotex General Info Tags', 'STRING', 'safetyreportid,receivedate,reporttype,serious,seriousnessdeath,seriousnesslifethreatening,seriousnesshospitalization,seriousnessdisabling,seriousnesscongenitalanomaliseriousnessother');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.reportduplicate', 'Apotex Reportduplicate Tags', 'Apotex Reportduplicate Tags', 'STRING', 'duplicatenumb');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.medicalhistoryepisode', 'Apotex Medicalhistoryepisode Tags', 'Apotex Medicalhistoryepisode Tags', 'STRING', 'patientepisodenamemeddraversion,patientepisodename,patientmedicalcontinue,patientmedicalcomment');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.reaction', 'Apotex Reaction Tags', 'Apotex Reaction Tags', 'STRING', 'primarysourcereaction,reactionmeddraversionllt,reactionmeddrallt,reactionmeddraversionpt,reactionmeddrapt,reactionoutcome,reactionstartdate,reactionenddate');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.patient', 'Apotex Patient Tags', 'Apotex Patient Tags', 'STRING', 'patientinitial,patientweight,patientheight,patientsex,patientepisodename');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.drugreactionrelatedness', 'Apotex Drugreactionrelatedness Tags', 'Apotex Drugreactionrelatedness Tags', 'STRING', 'drugreactionassesmeddraversion,drugreactionasses,drugassessmentsource,drugassessmentmethod,drugresult');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.primarysource', 'Apotex Primarysource Tags', 'Apotex Primarysource Tags', 'STRING', 'reportercountry,qualification');

CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.activesubstance', 'Apotex Activesubstance Tags', 'Apotex Activesubstance Tags', 'STRING', 'activesubstancename');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('NGV-PORTAL-APP', 'safetydbARGUS.casefile.PDFXml.incomingDateFormat', 'Apotex IncomingDateFormat Tags', 'Apotex IncomingDateFormat Tags', 'STRING', 'yyyyMMdd');

CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.1', 'Regulator Class 1', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.2', 'Regulator Class 2', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.3', 'Regulator Class 3', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.4', 'Regulator Class 4', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.5', 'Regulator Class 5', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.6', 'Regulator Class 6', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.7', 'Regulator Class 7', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.8', 'Regulator Class 8', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.9', 'Regulator Class 9', 'CM:Cosmetic~AB:Absolute', 'STRING', '');
CALL SCFX_SO.CMNPRC_ADD_CONFIG_SETTING ('QR-PORTAL-APP', 'reg.class.10', 'Regulator Class 10', 'CM:Cosmetic~AB:Absolute', 'STRING', '');