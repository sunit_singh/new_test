CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','email.retry.count','E-Mail Retry Count','It will store the number of times e-mail should be tried to be sent in case of failure.','5','5');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','email.thread.sleep','E-Mail Thread Sleep Time','It will store the time in milliseconds for which the thread should be in sleep.','500','500');
