--Web Error Codes
--Namespace Start - Tools
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 801, 'Error in splitting file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 802, 'Error in downloading zip file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 803, 'Input file not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 804, 'File already been downloaded');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 805, 'Error in file upload');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 806, 'Error in export text file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 807, 'Error in export excel file ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 808, 'Error in export csv file ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 809, 'Uploaded File is not of the file Type Selected');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('sourcedocutil', 810, 'Error while File Splitting');


--Namespace Start - Tools
 
--Namespace Start - system 
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 1, 'System is not bootstrapped');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 2, 'Invalid session');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 3, 'Login credentials not supplied');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 4, 'Error saving user preference');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 5, 'System has reported an exception. Kindly get in touch with the system administrator to proceed further.');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 50, 'Application not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 51, 'Portal action not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 52, 'Invalid login credentials for user');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 53, 'Application not enabled for user');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 54, 'User is not registered');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 55, 'Invalid action');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 56, 'Invalid user type');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 61, 'This functionality is available only to Project members');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 62, 'This functionality is not available to shared Project resources');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('system', 63, 'Invalid input parameter');
--Namespace End - system

--Namespace Start - ngv
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 101, 'Case Id is empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 102, 'Safety DB not selected');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 103, 'Case file not uploaded');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 104, 'Ruleset import file not uploaded');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 105, 'Case file parsing failed');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 106, 'Case file not available');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 107, 'Template not configured for generating narrative');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 108, 'Template ruleset emty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 109, 'Error exporting file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 110, 'Default template not selected');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 111, 'Error saving parsed file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 112, 'Template ruleset not selected in User Preferences');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 113, 'Template ruleset deactivated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 114, 'Multiple files are not allowed');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 115, 'Maximum upload file count exceeded');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 131, 'Invalid case id submitted');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 132, 'Mandatory columns not provided');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 133, 'Template ruleset cannot be activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 134, 'Template ruleset cannot be de-activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 135, 'Error updating template ruleset state');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 136, 'Invalid template ruleset name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 137, 'Non unique template ruleset name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 138, 'Invalid template name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 139, 'Template name not unique');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 140, 'Template rule criteria invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 141, 'Template rule criteria not unique');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 142, 'Case Validation Failed for Valid RuleType');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 143, 'Case Validation Failed for Error RuleType');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 144, 'Case Validation Failed for Warning RuleType');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 145, 'Case Validation Ruleset cannot be activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 146, 'Case Validation Ruleset cannot be de-activated');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 147, 'Rule Criteria invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 148, 'Rule Applicability invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 149, 'Invalid Rule Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 150, 'Rule Name not unique');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 151, 'Invalid Validation Ruleset Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 152, 'Non unique Validation Ruleset Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 153, 'Validation Rules not configured for Case Validation Ruleset. Hence Validation cannot be performed!!');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 154, 'Columns specified in Criteria is missing in Case File');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 155, 'Error encoding input file content');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('ngv', 156, 'Error decoding input file content');

--Namespace End - ngv


--Namespace Start - audit
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('audit', 151, 'Error fetching user info for audit');
--Namespace End - audit

--Namespace Start - qr
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 201, 'Error uploading master file in system');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 202, 'Safety DB is not selected. Please select the same in Preferences');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 203, 'Review Type is not selected. Please select the same in Preferences');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 204, 'Initial Received Date cannot be parsed. Please enter the date in DD-MMM-YYYY format.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 205, 'Quality Review Type is not defined');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 206, 'None of the Safety Case Field template is active');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 207, 'Case Associate is invalid. Please select the correct one');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 208, 'Case Serious is invalid. Please select the correct one');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 209, 'Safety DB Case Field template is invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 210, 'Fields Reviewed count is incorrect.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 211, 'Mismatch in fields reviewed count');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 213, 'Error saving quality review log data');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 214, 'Error in quality review sequence id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 215, 'Error in Report Generation');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 216, 'Error in parsing field');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 251, 'Invalid Case Id ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 252, 'Invalid Case Version');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 253, 'Case ID cannot be blank');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 254, 'Please Select Case Master File');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 217, 'Please upload CSV or EXEL file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 218, 'Please upload CSV or EXEL file');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 219, 'Error getting case record values from database');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 220, 'Case record does not exist in the system');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 221, 'Error Type is not configured for the project');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 222, 'Review Type is not selected in User Preference or it is not maintained at project level');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 223, 'Category is not configured for the project');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 224, 'Previous review type is not complete');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 225, 'Error creating error dashboard');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 226, 'Case record does not exist in the Case Master');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 255, 'Please Specify Case Id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 256, 'Please Specify Case Version');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 257, 'Please Specify Case Seriousness');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 258, 'Please Specify Case Initial Receive Date');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 259, 'Invalid Initial Receive Date Format');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('qr', 260, 'UDF Fields Mapping Not Maintain In File Upload');

--Namespace End - qr

--Namespace Start - prjset
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('prjset', 301, 'Please specify template name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('prjset', 302, 'Template not found');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('prjset', 303, 'Error uploading file in system');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('prjset', 351, 'Template Name is invalid ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('prjset', 352, 'Template Name is not unique ');
--Namespace End - prjset

--Namespace Start - mailSet
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 401, 'Please specify mail template name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 402, 'Please specify mail template purpose');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 403, 'Please specify mail template subject');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 404, 'Please specify mail template boby');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 405, 'Mail template name invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 406, 'Template name is not unique');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailset', 407, 'Updated values same as existing values');
--Namespace End - mailset

--Namespace Start - projectuser
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectusermanagement', 501, 'Please select user');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectusermanagement', 502, 'Please enter proper client Name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectusermanagement', 503, 'Please enter proper client email Id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectusermanagement', 504, 'Please enter SafetyDBID');

--Namespace End - projectuser

--Namespace Start - projectmanagement
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectmanagement', 601, 'Please specify project name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectmanagement', 602, 'Project name invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectmanagement', 603, 'Please specify project description');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectmanagement', 604, 'Project name already exist');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('projectmanagement', 605, 'Updated values same as existing values');

--Namespace End - projectmanagement

--Namespace Start -User management
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 901, 'Please specify user id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 902, 'Please specify user name');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 903, 'Please specify user email');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 904, 'Please select authentication type');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('usermanagement', 905, 'User Id Already Exist');
--Namespace End - User management

--Namespace Start -System Config
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 701, 'Please specify key id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 702, 'Please select key purpose');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 703, 'Please select key type');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 704, 'Invalid key id');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 705, 'Key id already Exist');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('systemconfig', 706, 'Error in key export');
--Namespace End - System Config

-- Namespace start - webservices
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 100000, 'Success.');

CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110000, 'Invalid Input. UserID/Password cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110001, 'Authentication denied as UserID contains invalid characters.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110002, 'Authentication denied as User is not registered.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110003, 'Authentication Failed. Password does not match.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110004, 'Something went wrong. Please contact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110005, 'Invalid Input. ActivityCode cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110006, 'Activity Registration failed as UserId or Password do not match. Please re-authenticate.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110007, 'Activity Registration failed. Please contact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110008, 'Invalid Input. TemplateId cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110009, 'Template ID does not exist.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110010, 'Template Export Failed. Please contact administrator.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110011, 'Invalid Template ID.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110012, 'Template Export failed as UserId or Password do not match. Please re-authenticate.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110013, 'Activity not registered. Please recheck the Activity ID.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110014, 'Invalid Input. ActivityID is mandatory and has to be numeric.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110015, 'Invalid Input. ProjectSeqId is mandatory and has to be numeric.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110016, 'Invalid Input. ActivityType cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110017, 'Auhentication not completed. Please authenticate and retry.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110018, 'Invalid Input. TemplateId is mandatory and has to be numeric.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110019, 'Invalid Input. KeyID cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110020, 'Authentication failed. Please contact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110021, 'Invalid Input. ActivityApp cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110022, 'Invalid Input. Timestamp not valid.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110023, 'Web APIs are currently not active. Please conact system admin.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110024, 'Bad Request. Please check the json structure.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110025, 'Authentication Failed. Incorrect KeyId.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110026, 'Tool Version not latest. Kindly download the latest version from the portal.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110027, 'Invalid Input. Timezone cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110028, 'Invalid Input. Timestamp cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110029, 'Invalid Input. ToolName cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110030, 'Invalid Input. ToolVersion cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110031, 'Invalid Input. ClientReqId cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110032, 'Invalid Input. MachineName cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110033, 'Invalid Input. MachineDomain cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110034, 'Invalid Input. IPAddress cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110035, 'Invalid Input. OSName cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110036, 'Invalid Input. OSVersion cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110037, 'Invalid Input. OSArchitecture cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110038, 'Invalid Input. JDKVersion cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110039, 'Invalid Input. MachineLoggedInUser cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110040, 'Invalid Input. MachineLoggedInUserDomain cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110041, 'Tool Verification Failed for ToolName. Incorrect ToolName.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110042, 'Tool Verification Failed for ToolVersion. Incorrect ToolVersion.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110043, 'Server Side Runtime IQ Check Failed for OS Version.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110044, 'Server Side Runtime IQ Check Failed for JAVA Version.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110045, 'Invalid Input. ClientIQHash cannot be null/blank.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110046, 'Client IQ Check Failed. Cannot allow further processing.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110047, 'Invalid Timestamp Format. Please correct the format (dd-MM-yyyy HH.mm.ss).');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110048, 'Invalid ProjectSeqId. User not allocated to requested project.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110049, 'Invalid BrowserVersion : CHROME. Version not allowed.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110050, 'Invalid BrowserVersion : Internet Explorer. Version not allowed.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110051, 'Invalid BrowserVersion : CHROME. Version not supported.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110052, 'Invalid BrowserVersion : Internet Explorer. Version not supported.');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('webservices', 110053, 'Invalid Input Parameter : ');
-- Namespace end - webservices

-- Namespace start - activity
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('activity', 1001, 'Error fetching user info for activity');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('activity', 1002, 'Error fetching project info for activity');
-- Namespace end - Activity

-- Namespace start - Mailbox
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1101, 'Error Mail Box Name Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1102, 'Error Mail Box Name Invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1103, 'Error Mail Box Email Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1104, 'Error Mail Box Description Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1105, 'Error Mail Box Mnemonics Empty');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1106, 'Error Mail Box Name not unique ');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1107, 'Error Updated value same as existing');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1108, 'Error Mail Box Mnemonics Invalid');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1109, 'Error Uploading MSG File');
CALL SCFX_SO.SYSPRC_REGISTER_WEB_ERROR_CODE('mailbox', 1110, 'Error in exporting mail contents');

-- Namespace end - Mailbox
