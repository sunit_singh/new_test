-- Create Bootstrap User
CALL SCFX_SO.USRPRC_CREATE_USER ('@bootstrap_user@', 'Bootstrap User', 'bootstrap_user@sciformix.com');

-- Update authtype as LDAP
update scfx_so.usr_users set userauthtype = 2 where userid = '@bootstrap_user@';
