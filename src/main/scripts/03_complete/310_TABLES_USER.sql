--TODO: Add Table and Column comments

CREATE SEQUENCE SCFX_SO.USR_USERSEQID_SEQ START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;

GRANT SELECT ON SCFX_SO.USR_USERSEQID_SEQ TO SCFX_ROLE_CU_RW;


CREATE TABLE SCFX_SO.USR_USERS
(
	USERSEQID		INTEGER,
	USERID			VARCHAR2(4000),
	USERDISPLAYNAME	VARCHAR2(4000),
	USERSHORTNAME	VARCHAR2(4000),
	USEREMAIL		VARCHAR2(4000),
	USERTYPE		INTEGER,
	USERAUTHTYPE	INTEGER NOT NULL CHECK(USERAUTHTYPE=2),
	USERAUTHVALUE1	VARCHAR2(4000),
	USERAUTHVALUE2	VARCHAR2(4000),
	USERSTATE		INTEGER,
	DTCREATED		DATE,
	DTLASTATTRIBUPDATED	DATE,
	DTLASTLOGIN		DATE,
	DTCURRENTLOGIN	DATE,
	CONSTRAINT USR_USERS_PK PRIMARY KEY (USERSEQID),
	CONSTRAINT USR_USERS_UK1 UNIQUE (USERID)
) TABLESPACE TBS_USER;

CREATE INDEX SCFX_SO.USR_USERS_IDX1 ON SCFX_SO.USR_USERS(USERAUTHTYPE, USERAUTHVALUE1);

GRANT SELECT, INSERT, UPDATE, DELETE ON SCFX_SO.USR_USERS TO SCFX_ROLE_CU_RW;
GRANT SELECT ON SCFX_SO.USR_USERS TO SCFX_ROLE_CU_RO;


CREATE TABLE SCFX_SO.USR_USERS_PREFS
(
	PROJECTSEQID	INTEGER,
	USERSEQID		INTEGER,
	USERPREFAPP		VARCHAR2(256),
	USERPREFKEY		VARCHAR2(256),
	USERPREFVAL1	VARCHAR2(4000),
	USERPREFVAL2	VARCHAR2(4000),
	DTCREATED		DATE,
	DTLASTUPDATED	DATE,
	CONSTRAINT USR_USERS_PREFS_PK PRIMARY KEY (PROJECTSEQID,USERSEQID, USERPREFAPP, USERPREFKEY)
) TABLESPACE TBS_USER;

GRANT SELECT, INSERT, UPDATE, DELETE ON SCFX_SO.USR_USERS_PREFS TO SCFX_ROLE_CU_RW;
GRANT SELECT ON SCFX_SO.USR_USERS_PREFS TO SCFX_ROLE_CU_RO;
