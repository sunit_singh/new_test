
-- Sample User creation
	CALL SCFX_SO.USRPRC_CREATE_USER ('testuser1', 'Test User One', 'testuser1@sciportal.com');

-- before executing following script user gkorde must have cretaed. 	
call SCFX_SO.USRPRC_ADD_USER_ROLE ('gkorde', 'PROJECT-ADMIN');
call SCFX_SO.USRPRC_ADD_USER_ROLE ('gkorde', 'SYSTEM-ADMIN');
call SCFX_SO.USRPRC_ADD_USER_ROLE ('gkorde', 'AUDITOR');

INSERT INTO SCFX_SO.USR_USERS (USERSEQID, USERID, USERDISPLAYNAME, USERSHORTNAME, USEREMAIL, USERTYPE, USERAUTHTYPE, USERAUTHVALUE1, USERAUTHVALUE2, USERSTATE, DTCREATED, DTLASTATTRIBUPDATED, DTLASTLOGIN, DTCURRENTLOGIN)
VALUES (SCFX_SO.USR_USERSEQID_SEQ.nextval, '@bootstrap_user@', '@bootstrap_user@', '@bootstrap_user@', '@bootstrap_user@', 0, -1, NULL, NULL, 1, SYSDATE, SYSDATE, NULL, NULL);


--Assign User to Project, Note:- 'Test_Project' need to created before executing the proc.
CALL SCFX_SO.USRPRC_ADD_USER_TO_PROJECT('gmishra', 'Test_Project', 'Client_Name', 'client@gmail.com', 'clientsafetyBd');
CALL SCFX_SO.USRPRC_ADD_USER_TO_PROJECT('ajkulkarni', 'Test_Project', 'Client_Name', 'client@gmail.com', 'clientsafetyBd');
CALL SCFX_SO.USRPRC_ADD_USER_TO_PROJECT('pjadhav2', 'Test_Project', 'Client_Name', 'client@gmail.com', 'clientsafetyBd');
CALL SCFX_SO.USRPRC_ADD_USER_TO_PROJECT('gwandhare', 'Test_Project', 'Client_Name', 'client@gmail.com', 'clientsafetyBd');
CALL SCFX_SO.USRPRC_ADD_USER_TO_PROJECT('arai', 'Test_Project', 'Client_Name', 'client@gmail.com', 'clientsafetyBd');

-- Query Repository Test Script Start
CALL SCFX_SO.CMNPRC_QUERYREPO('QR-PORTAL-APP','DENAMERETREIVAL','Used to get the DE Name','select userseqid from %s.PRJ_PROJECTS_USERS where PRJSEQID = ? and clientname = ?');
-- Query Repository Test Script End

CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','CASEID','#AER','STRING','N','CASE ID');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','VERSION','Case Version','STRING','N','CASE VERSION');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','IRD','Initial Receive Date','DATE','N','IRDate');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','CASETYPE','Case Type','STRING','N','CASE TYPE');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','PRODUCT','Case Product','STRING','N','PRO DUCT');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','CASESERIOUSNESS','Case Seriousness','STRING','F','<#assign sTemp = ''Serious''><#if Ser == "N"><#assign sTemp = ''Non-Serious''></#if>${sTemp}~Ser');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','DENAME','CP Name','INTEGER','S','1~PRJSEQID~DE NAME');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(1,'QR-PORTAL-APP','QRLCM','F1','UDF1','Reg Class','STRING','N','Regulatory Class');

CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(3,'QR-PORTAL-APP','QRLCM','F1','CASEID','#AER','STRING','N','Argus case ID');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(3,'QR-PORTAL-APP','QRLCM','F1','VERSION','Case Version','STRING','N','Ini/FU');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(3,'QR-PORTAL-APP','QRLCM','F1','IRD','Initial Receive Date','DATE','N','Aware Date');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(3,'QR-PORTAL-APP','QRLCM','F1','CASETYPE','Case Type','STRING','N','Case type');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(3,'QR-PORTAL-APP','QRLCM','F1','PRODUCT','Case Product','STRING','N','Product');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(3,'QR-PORTAL-APP','QRLCM','F1','CASESERIOUSNESS','Case Seriousness','STRING','F','<#assign sTemp = ''Serious''><#if Ser == "N"><#assign sTemp = ''Non-Serious''></#if>${sTemp}~Ser');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(3,'QR-PORTAL-APP','QRLCM','F1','DENAME','CP Name','INTEGER','S','1~PRJSEQID~Assigned To');

CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(2,'QR-PORTAL-APP','QRLCM','F1','CASEID','#AER','STRING','N','Case Num');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(2,'QR-PORTAL-APP','QRLCM','F1','VERSION','Case Version','STRING','N','Case Version');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(2,'QR-PORTAL-APP','QRLCM','F1','IRD','Initial Receive Date','DATE','N','Aware Date');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(2,'QR-PORTAL-APP','QRLCM','F1','CASETYPE','Case Type','STRING','N','Case Report Type');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(2,'QR-PORTAL-APP','QRLCM','F1','PRODUCT','Case Product','STRING','N','Company Product');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(2,'QR-PORTAL-APP','QRLCM','F1','CASESERIOUSNESS','Case Seriousness','STRING','F','<#assign sTemp = ''Serious''><#if Seriousness == "N"><#assign sTemp = ''Non-Serious''></#if>${sTemp}~Seriousness');
CALL SCFX_SO.CMNPRC_FILEUPLCOLUMNMAPPING(2,'QR-PORTAL-APP','QRLCM','F1','DENAME','CP Name','INTEGER','S','1~PRJSEQID~Assigned to');

CALL SCFX_SO.CMNPRC_ANNOTATE_SYSTEM('AKASH-LOCAL', 'TEST');
CALL SCFX_SO.CMNPRC_UPDATE_SYSTEMVERSION('V1.0');

