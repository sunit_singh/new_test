CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','sciportal.login.userid.regex','Login Username Validation Regex','Login Username Validation Regex','[A-Za-z0-9.@]+','[A-Za-z0-9.@]+');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','case.seriousness.type','Case Seriousness Type','It will store all the types applicable for case seriousness.','Serious~Non-Serious','Serious~Non-Serious');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','thread.server.hostname','Hostname of the server on which thread will be running','Hostname of the server on which thread will be running','SoftTeam-2','SoftTeam-2');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','thread.server.jobschedulerid','jobschedulerid of the server on which multiple scheduled task will run','jobschedulerid of the server on which multiple scheduled task will run','2','2'); 
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','email.retry.count','E-Mail Retry Count','It will store the number of times e-mail should be tried to be sent in case of failure.','5','5');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','email.thread.sleep','E-Mail Thread Sleep Time','It will store the time in milliseconds for which the thread should be in sleep.','500','500');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','common.validation.regex','Common Validation Regex','Common Validation Regex','[A-Za-z0-9_-]+','[A-Za-z0-9_-]+');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('SYSTEM','sciportal.sdHeaderList','Sciportal SDHeaderList','Sciportal SDHeaderList','String','Report1.Adverse Reaction Report Number,Report1.Latest AER Version Number,Report1.Initial Received Date,Report1.Latest Received Date,Report1.Source of Report,Report1.Market Authorization Holder AER Number,Report1.Type of Report,Report1.Reporter Type,Report1.Serious report?,Report1.Death,Report1.Disability,Report1.Congenital Anomaly,Report1.Life Threatening,Report1.Hospitalization,Report1.Other Medically Important Conditions,Report1.Patient Information,Report1.Age,Report1.Gender,Report1.Height,Report1.Weight,Report1.Report Outcome');

CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_RAW ('LDAP.SERVERCONNECTIONS','sciportal.ldap._LIST','LDAP Connection List','LDAP Connection List','PRIMARY','PRIMARY');
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_RAW ('SMTP.SERVERCONNECTIONS','sciportal.smtp._LIST','SMTP Connection List','SMTP Connection List','1,2','1,2');

-- Web service enabled - script
CALL SCFX_SO.CMNPRC_ADD_SYS_CONFIG_SETTING ('WEBAPIS','enabled','Web APIs Enabled','Web APIs Enabled','boolean','false');