CREATE DATABASE sciportaldb
   USER SYS IDENTIFIED BY sys_password
   USER SYSTEM IDENTIFIED BY system_password
   LOGFILE GROUP 1 ('C:/oraclexe/app/oracle/oradata/sciportaldb/redo01.log') SIZE 100M,
   MAXLOGFILES 5
   MAXLOGMEMBERS 5
   MAXLOGHISTORY 1
   MAXDATAFILES 100
   CHARACTER SET AL32UTF8
   NATIONAL CHARACTER SET AL16UTF16
   EXTENT MANAGEMENT LOCAL
   DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/system01.dbf' SIZE 325M REUSE
   SYSAUX DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/sysaux01.dbf' SIZE 325M REUSE
   DEFAULT TABLESPACE users
      DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/users01.dbf'
      SIZE 500M REUSE AUTOEXTEND ON MAXSIZE UNLIMITED
   DEFAULT TEMPORARY TABLESPACE tempts1
      TEMPFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/temp01.dbf'
      SIZE 20M REUSE
   UNDO TABLESPACE undotbs
      DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/undotbs01.dbf'
      SIZE 200M REUSE AUTOEXTEND ON MAXSIZE UNLIMITED;


CREATE TABLESPACE TBS_SYSTEM
  DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/TBS_SYSTEM_DATA.dbf'
  SIZE 200M AUTOEXTEND ON;
      
CREATE TABLESPACE TBS_USER
  DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/TBS_USER_DATA.dbf'
  SIZE 200M AUTOEXTEND ON;

CREATE TABLESPACE TBS_PROJECT
  DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/TBS_PROJECT_DATA.dbf'
  SIZE 200M AUTOEXTEND ON;

CREATE TABLESPACE TBS_AUDIT
  DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/TBS_AUDIT_DATA.dbf'
  SIZE 1000M AUTOEXTEND ON;

CREATE TABLESPACE TBS_COMMON
  DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/TBS_COMMON_DATA.dbf'
  SIZE 1000M AUTOEXTEND ON;

CREATE TABLESPACE TBS_APP
  DATAFILE 'C:/oraclexe/app/oracle/oradata/sciportaldb/TBS_APP_DATA.dbf'
  SIZE 1000M AUTOEXTEND ON;

