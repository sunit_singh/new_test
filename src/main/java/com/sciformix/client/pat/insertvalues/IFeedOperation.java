package com.sciformix.client.pat.insertvalues;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.sciformix.commons.utils.DataTable;

public interface IFeedOperation {

	public abstract void login(WebDriver driver, Map<String, String> propMap,String p_sUsername,String p_sPassword);
	
	public abstract void feedCaseIdTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);

	public abstract void feedPatientTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);
	
	public abstract void feedMedicalHistoryTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);
	
	public abstract void feedDrugHistoryTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);
	
	public abstract void feedReactionTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);

	public abstract void feedDrugTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);

	public abstract void feedReporterTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);
	
	public abstract void feedDiagnosticTab(WebDriver driver, Map<String, String> sceptrePropMap, Map<String, String> sceptreMap);
		
	public abstract Map<String, String> dateSplitter(Map<String, String> sceptreMap);
	
	public abstract void searchcase(WebDriver driver, Map<String, String> sceptrePropMap,String p_sCaseId);
	
	public abstract boolean feedReporterHCPData(WebDriver driver, Map<String,String> map,DataTable oDataTable,DataTable oParsedDataTable,boolean hcp);

	public abstract boolean feedPatientData(WebDriver driver, Map<String,String> map,DataTable oDataTable,DataTable oParsedDataTable);

	public abstract boolean feedProductData(WebDriver driver, Map<String,String> map,DataTable oDataTable,DataTable oParsedDataTable);
	
	public abstract boolean feedEventData(WebDriver driver, Map<String,String> map,DataTable oDataTable);
	
	public abstract boolean searchcase(WebDriver driver, Map<String,String> map,DataTable oDataTable,DataTable oParsedDataTable);

	public abstract void locateCaseOperation(WebDriver driver,Map<String,String> map,String caseId);
	
	public abstract boolean feedAdditionInfo(WebDriver driver, Map<String,String> map,DataTable oDataTable,DataTable oParsedDataTable);

}
