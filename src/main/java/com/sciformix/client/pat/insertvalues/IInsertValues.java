package com.sciformix.client.pat.insertvalues;

import java.util.Map;

public interface IInsertValues {
	//public void load(String sUserName, String sPass, String sCaseId);
	public Map<String, String> load(Map<String, String> values);
}
