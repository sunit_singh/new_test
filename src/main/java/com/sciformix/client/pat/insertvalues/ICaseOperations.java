package com.sciformix.client.pat.insertvalues;

import java.io.File;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.ParsedDataTable;

public interface ICaseOperations {
	
	public  void updateCase(WebDriver driver,Map<String,String> map, DataTable oDataTable,String caseId)throws SciException; 
	public  File locateCaseAndDownloadSD(WebDriver driver,Map<String,String> map,String caseId)throws SciException;
	void updateCase(WebDriver driver, Map<String, String> map, ParsedDataTable oDataTable, String caseId)
			throws SciException;
	
}
