package com.sciformix.client.pat.toolb;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.FormSubmitEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.sceptre.SceptreConstants;
import com.sciformix.client.pat.commons.CpatFieldMap;
import com.sciformix.client.pat.commons.CpatFieldMap.CpatField;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.PreviewCaseDoc;
import com.sciformix.client.pat.commons.ToolDetails;
import com.sciformix.client.pat.commons.screens.CpatCasePreviewScreen;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;


public class CpatToolB extends CpatToolBase implements IAction
{

	private static final String SCEPTRE_FILE_TYPE = "E2B";
	
	private static Logger log = LoggerFactory.getLogger(CpatToolB.class);
	
	private static JButton previewScreenContinueButton =  null;
	private static JButton previewScreenAbortButton = null;

	public static void main(String[] args)
	{
		 log.info("Starting CPAT Tool B");

		 SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		 FileConfigurationHome.init(oStartupLogger, parserConfiguration);

		 if(args.length > 0)
		 {
			 testModePropMap = CpatToolHome.loadExternalProperties(args[0]);
			 isTestMode = true;
		 }
		 
		ToolDetails	l_tooldetails = new ToolDetails();
		l_tooldetails.setToolName(CpatToolBConstants.Tool.TOOL_NAME);
		l_tooldetails.setToolDescription(CpatToolBConstants.Tool.TOOL_DESCRIPTION);
		l_tooldetails.setToolDisplayName(CpatToolBConstants.Tool.TOOL_DISPLAY_NAME);
		l_tooldetails.setToolHeading(CpatToolBConstants.Tool.TOOL_HEADING);
		l_tooldetails.setToolTitle(CpatToolBConstants.Tool.TOOL_TITLE);
		l_tooldetails.setToolUserInputScreen(CpatToolBConstants.Tool.SCEPTRE_USER_INFPUTS_SCREEN);
		l_tooldetails.setToolVersion(CpatToolBConstants.Tool.TOOL_VERSION);
		 
		 CpatToolB oTool = new CpatToolB(l_tooldetails);
		 log.info("ToolName : " + oTool.getToolDetails().getToolName() + " ToolVersion : " + oTool.getToolDetails().getToolVersion());
		 System.out.println("ToolName : " + oTool.getToolDetails().getToolName() + " ToolVersion : " + oTool.getToolDetails().getToolVersion());
		 oTool.render(CPAT_ACTION.INITIAL);
	}

	public CpatToolB(ToolDetails p_tooldetails)
	{
		super(p_tooldetails);
	}	

	private void render(CPAT_ACTION p_eAction)
	{
		performAction(p_eAction);
		
	}

	public void performAction(CPAT_ACTION p_eAction, Object... oParameters) 
	{

		if (p_eAction == CPAT_ACTION.INITIAL) 
		{
			log.info("Login Screen");
			
			CpatToolHome.showLoginScreen(this, this);
			
		}
		
		else if (p_eAction == CPAT_ACTION.LOGIN_SUCCESSFUL) 
		{
			log.info("Login Successful");
			
			performAction(CPAT_ACTION.COLLECT_USER_INPUTS, oParameters);

		} 
		
		else if (p_eAction == CPAT_ACTION.COLLECT_USER_INPUTS) 
		{
			log.info("Collect User Inputs");
			
			CpatToolHome.showUserInputsScreen(this, this);
		} 
		
		else if(p_eAction == CPAT_ACTION.USER_INPUTS_COLLECTED)
		{
			performAction(CPAT_ACTION.LOCATE_CASE, oParameters);
		}
		
		else if(p_eAction == CPAT_ACTION.LOCATE_CASE)
		{
			log.info("Locate Case");
			CpatToolBHelper.locateCase(this, this, userInputsParams.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID), this.userInputsParams);
		}
		
		else if (p_eAction == CPAT_ACTION.DOWNLOAD_SD) 
		{
			log.info("Download SD");
			CpatToolBHelper.downloadCaseSD(this, this);

		} 
	
		else if (p_eAction == CPAT_ACTION.PREVIEW) 
		{
			log.info("Preview");
			Map<String, Object> params = null;
			
			List<PreviewCaseDoc> listPreviewCaseDocs =  (List<PreviewCaseDoc>) oParameters[0];
			ParsedDataTable oSummaryDataTable = (ParsedDataTable) oParameters[1];
			Map<String, String> sceptreMap = (Map<String, String>) oParameters[2];
			
			params = new HashMap<>();
			
			params.put(CpatToolConstants.ContentParams.CASE_DOC_LIST, listPreviewCaseDocs);
			params.put(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE, oSummaryDataTable);
			params.put(CpatToolConstants.ContentParams.CASE_FILE_TYPE, SCEPTRE_FILE_TYPE);
			params.put(CpatToolConstants.ContentParams.SCEPTRE_MAP, sceptreMap);

			CpatToolBHelper.showPreviewScreen(this, this, params);
	
		}
		
		else if(p_eAction == CPAT_ACTION.DUPLICATE_SEARCH)
		{
			log.info("Duplicate Search");
			ParsedDataTable oSummaryDataTable = (ParsedDataTable) oParameters[0];
			Map<String, String> sceptreMap = (Map<String, String>) oParameters[1];
			
			CpatToolBHelper.performDuplicateSearch(this, this, oSummaryDataTable, sceptreMap);
			
		}
		
		else if (p_eAction == CPAT_ACTION.DATA_ENTRY) 
		{
			log.info("Data Entry");
			ParsedDataTable summaryDataTable = (ParsedDataTable) oParameters[0];
			Map<String, String> sceptreMap = (Map<String, String>) oParameters[1];
			boolean isDuplicate = (boolean) oParameters[2];
			
			CpatToolBHelper.performDataEntry(this, this, this.userInputsParams.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID), sceptreMap, isDuplicate);
		} 
		
		else if (p_eAction == CPAT_ACTION.NG) {

		}
		
		else if (p_eAction == CPAT_ACTION.LOGOUT)
		{

			CpatToolBHelper.logout();
		}

	}
	

	protected void loadCpatFieldMap() {
		s_mapCpatFieldsFromXml = new CpatFieldMap("JNJ-SCEP", "v1.0", "E2B", "SCEPTRE");
				
		/**
		 * Case ID Tab Mappings
		 */
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.CaseIdTabKeys.SAFETY_REPORT_ID,"FIELD",SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.CaseIdTabKeys.FIRST_RECEIVED_DATE,"FIELD",SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.CaseIdTabKeys.FIRST_RECEIVED_DATE,"FIELD",SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.CaseIdTabKeys.DATE_OF_RECENT_INFO,"FIELD",SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.CaseIdTabKeys.FIRST_RECEIVED_DATE,"FIELD",SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT));
	
		
		/**
		 * Patient Tab Mappings
		 */
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PatientTabKeys.REPORTER_POSTCODE,"FIELD",SceptreConstants.PatientTabKeys.POSTAL_CODE));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PatientTabKeys.PATIENT_GPMEDICAL_RECORD_NO,"FIELD",SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PatientTabKeys.PATIENT_SPECIALIST_RECORD_NO,"FIELD",SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PatientTabKeys.PATIENT_HOSPITAL_RECORD_NO,"FIELD",SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER));
		
		/**
		 * Reporter Tab
		 */
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_TITLE,"FIELD",SceptreConstants.ReporterTabKeys.TITLE));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_GIVEN_NAME,"FIELD",SceptreConstants.ReporterTabKeys.GIVEN_NAME));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_MIDDLE_NAME,"FIELD",SceptreConstants.ReporterTabKeys.MIDDLE_NAME));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_FAMILY_NAME,"FIELD",SceptreConstants.ReporterTabKeys.FAMILY_NAME));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_ORGANIZATION,"FIELD",SceptreConstants.ReporterTabKeys.ORGANIZATION));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_DEPARTMENT,"FIELD",SceptreConstants.ReporterTabKeys.DEPARTMENT));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_STREET,"FIELD",SceptreConstants.ReporterTabKeys.ADDRESS1));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_CITY,"FIELD",SceptreConstants.ReporterTabKeys.CITY));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_STATE,"FIELD",SceptreConstants.ReporterTabKeys.PROVINCE_STATE));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.REPORTER_POSTCODE,"FIELD",SceptreConstants.ReporterTabKeys.POSTAL_CODE));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.PHONE,"FIELD",SceptreConstants.ReporterTabKeys.PHONE1_NUMBER));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.FAX,"FIELD",SceptreConstants.ReporterTabKeys.FAX_NUMBER));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.EMAIL_ADDRESS,"FIELD",SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.SENDER_TITLE,"FIELD",SceptreConstants.ReporterTabKeys.COMPANY_TITLE));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.SENDER_GIVEN_NAME,"FIELD",SceptreConstants.ReporterTabKeys.COMPANY_GIVEN_NAME));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.ReporterTabKeys.SENDER_FAMILY_NAME,"FIELD",SceptreConstants.ReporterTabKeys.COMPANY_FAMILY_NAME));

		/**
		 * Drug Tab
		 */
		// Added by Shambhu
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_EXPIRY_DATE ,"FIELD", SceptreConstants.DrugTabKeys.EXPIRATION));
//		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.DrugTabKeys.NARRATIVE_EXTERNAL_SOURCE_NUMBER,"FIELD", SceptreConstants.DrugTabKeys.PRODUCT_QUALITY));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_CODE,"FIELD", SceptreConstants.Name.PRODUCTCODE));
		
		/**
		 * Drug History
		 */
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PDTTabKeys.PDT_REACTION,"FIELD",SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PDTTabKeys.PDT_INDICATION,"FIELD",SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PDTTabKeys.PDT_REPORTED_DRUG_NAME ,"FIELD",SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME));
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PDTTabKeys.PDT_START_DATE,"FIELD",SceptreConstants.DrugHistoryTabKeys.START));		
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PDTTabKeys.PDT_END_DATE,"FIELD",SceptreConstants.DrugHistoryTabKeys.END));		
		
		/**
		 * Pre Medical History
		 */
		s_mapCpatFieldsFromXml.addField(new CpatField(CpatToolBConstants.PMHTabKeys.PMH_CONTINUING,"FIELD",SceptreConstants.MedicalHistoryTabKeys.CONTINUING));
		
	}
	
	public List<String> validateUserInputs(Map<String, String> mapExtractedParameters)
	{
		log.info("Validate User Inputs");
		
		List<String> listErrors = null;
		
		String sUserName = null;
		String sPassword = null;
		
		String sDatabase = null;
		String sActionMode = null;
		
		String sCaseId = null;
		String sProductListPath = null;
		
		Pattern caseIdPattern =null;
		Matcher caseIdMatcher = null;
		
		File file = null;
		String fileName = null;
		String fileExtension = null;
		
		DataTable productListDataTable = null;
		List<String> columnsList = null;
				
		listErrors = new ArrayList<String>();
		
		sUserName = mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_USERNAMAE);
		sPassword = mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_PASSWORD);
		sDatabase = mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_DATABASE);
		sActionMode = mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_OPERATIONMODE);
		sCaseId = mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID);
		sProductListPath = mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_PRODUCT_LIST);
		
		
		if(StringUtils.isNullOrEmpty(sUserName))
		{
			listErrors.add(CpatToolBErrorMessages.UserInputScreen.LIST_ERROR_USERNAME);
		}
		
		if(StringUtils.isNullOrEmpty(sPassword))
		{
			listErrors.add(CpatToolBErrorMessages.UserInputScreen.LIST_ERROR_PASSWORD);
		}
		
		if(StringUtils.isNullOrEmpty(sDatabase))
		{
			listErrors.add(CpatToolBErrorMessages.UserInputScreen.LIST_ERROR_DBNAME);
		}
		
		if(StringUtils.isNullOrEmpty(sActionMode))
		{
			listErrors.add(CpatToolBErrorMessages.UserInputScreen.LIST_ERROR_ACTION);	
		}
		
		if(StringUtils.isNullOrEmpty(sCaseId))
		{
			listErrors.add(CpatToolBErrorMessages.UserInputScreen.LIST_ERROR_CASEID);
		}
		else
		{
			caseIdPattern = Pattern.compile("\\d{11}");
			caseIdMatcher = caseIdPattern.matcher(sCaseId);
			if(!caseIdMatcher.matches())
			{
				listErrors.add(CpatToolBErrorMessages.UserInputScreen.CASEID_PATTERN_ERR);
			}
						
		}
			
		if(StringUtils.isNullOrEmpty(sProductListPath))
		{
			listErrors.add(CpatToolBErrorMessages.UserInputScreen.PRODLIST_PATH_ERR);
		}
		else
		{
			file = new File(sProductListPath);
			
			// Valida file
			if(!file.exists())
			{
				listErrors.add(CpatToolBErrorMessages.UserInputScreen.PRODLIST_UNAVAIL_ERR);
			}
			
			// Valid Extension
			fileName = file.getName();
			
			fileExtension = fileName.substring(fileName.lastIndexOf(StringConstants.PERIOD) + 1);
			
			if(!fileExtension.equalsIgnoreCase(CpatToolConstants.PRODUCTLIST_EXTENSION))
			{
				listErrors.add(CpatToolBErrorMessages.UserInputScreen.PRODLIST_TYPE_ERR);
			}
			else
			{
				// Valid File Contents
				productListDataTable = CpatToolBHelper.getRowDataFromExcel(sProductListPath);
				
				columnsList = CpatToolBConstants.productListColumns;
				
				for(String column: columnsList)
				{
					if(!productListDataTable.isColumnPresent(column))
					{
						listErrors.add(CpatToolBErrorMessages.UserInputScreen.PRODLIST_COL_HEADER_ERR + column);
					}
					else
					{
						if(productListDataTable.getColumnValues(column).size() == 0)
						{
							listErrors.add(CpatToolBErrorMessages.UserInputScreen.PRODLIST_COL_VAL_ERR + column);
						}
					}
				}
				
			}
		}
		
		return listErrors;
	}

	@Override
	public void userInputsScreenEventHandler(final CpatToolBase oToolBase, final IAction oCallbackAction, Component comp, final JFrame frame) 
	{
		log.info("userInputsScreenEventHandler");
		
		JEditorPane jep = null;
		
		jep = (JEditorPane) comp;
		
		jep.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent event) {
				if (event instanceof FormSubmitEvent) {
					CpatToolBHelper.inputsCollectedActionListener(oToolBase, oCallbackAction, frame, (FormSubmitEvent) event);
				} else {
					// TODO:Something here
				}
			}

		});
	}

	@Override
	public void previewScreenEventHandler(final CpatToolBase oToolBase, final IAction oCallbackAction, Component comp,
			final JFrame oWindowFrame, final ParsedDataTable summaryDataTable, final Map<String, String> sceptreMap) 
	{
		log.info("previewScreenEventHandler");
		
		JButton btn = null;
		JPanel panel = (JPanel) comp;
		
		Component[] components = panel.getComponents();
		
		Component[] childComponents = null;
		
		for(int i=0; i<components.length; i++)
		{
			if(components[i] instanceof JPanel)
			{
				childComponents = ((JPanel) components[i]).getComponents();
				
				for(int j=0; j< childComponents.length; j++)
				{
					if( childComponents[j] instanceof JButton)
					{
						btn = (JButton) childComponents[j];
						
						if(btn.getText().equals(CpatToolConstants.UI.PREVIEW_BUTTON_CONTINUE))
						{
							previewScreenContinueButton = btn;
						}
						else
						{
							previewScreenAbortButton = btn;
						}
					}
				}
				
			}
		}
		
		previewScreenContinueButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				oWindowFrame.setVisible(false);
				oCallbackAction.performAction(CPAT_ACTION.DUPLICATE_SEARCH, summaryDataTable, sceptreMap);
			}
		});
		
		
		previewScreenAbortButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Map<String, String> payload = null;
				boolean isSuccess = false;
				
				payload = new HashMap<>();
				payload.put(CpatToolBConstants.PayloadKeys.USER_CHOICE, CpatToolBConstants.PreviewScreen.PAYLOAD_ABORT);
				
				isSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.PREVIEW , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.PREVIEW, "", payload);
				if(isSuccess)
				{
					oWindowFrame.dispose();
					
					CpatToolBHelper.closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				
			}
		});
		
		oWindowFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) 
			{
				if(CpatCasePreviewScreen.triggeredOnResultScreen)
				{
					oWindowFrame.dispose();
				}
				else
				{
					String[] buttons = {CpatToolConstants.OPTION_YES, CpatToolConstants.OPTION_NO};
					int rc = JOptionPane.showOptionDialog(null, CpatToolConstants.ABORT_MSG_PREVIEW_SCREEN, CpatToolConstants.OPTION_DIALOG_TITLE,
							JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[1]);
					
					if(rc == 0)
					{
						previewScreenAbortButton.doClick();
					}					
				}
			}
		});

	}

}

