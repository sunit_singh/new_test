package com.sciformix.client.pat.toolb;

import java.util.ArrayList;
import java.util.List;

public class CpatToolBConstants 
{
	public static final List<String> productListColumns 			= 				new ArrayList<>();

	public static final String VALID_CASE_QUEUE						=				"Global DSS Workflow: Case_Processing"	;
	public static final String AER 									=				"AER"									;
	public static final String COLLATION_KEY_LIST					= 				"FollowUpCollationKeys"					;
	public static final String PRODUCT_REG_CLASS 					= 				"COSMETIC"								;
	public static final String CPAT_SDSVALIDATION_ENABLE			=				"cpat.sdsvalidation.enable"				;
	public static final String CPAT_SEARCHCRITERIA_2				=				"cpat.SearchCriteria.2"					;
	


	static
	{
		productListColumns.add("GENERIC_NAME");
		productListColumns.add("REG_CLASS");
		productListColumns.add("PRODUCT_FORM");
	}
	

	/*	code by shambhu
	 * 
	 * 	AgeGroup Enum expecting number of days of age to calculate age group
	 * 
	 * 	To get Age Group caller needs to call getAgeGroup()
	 * 
	 */
	public static enum AgeGroup 
	{
		Neonate(0, 28), Infant(29, 730), Child(731, 5109), Adolescent(5110, 6934) , Adult(6935, 24089) , Elderly(24090, 47450);

	    private final int from;
	    private final int to;

	    private AgeGroup(int from, int to)
	    {
	        this.from = from;
	        this.to = to;
	    }
	    
	    public static AgeGroup getAgeGroup(int age)
	    {
	    	for(AgeGroup ageGroup : values())
	    	{
	    		if(age >= ageGroup.from && age <= ageGroup.to )
	    		{
	    			return ageGroup;
	    		}
	    	}
	    	return null;
	    }
	    
	    public int getMinAge() 
	    {
	    	return from;
	    }
	    public int getMaxAge()
	    {
	    	return to;
	    }
	}

	public static class Tool
	{
		public static final String TOOL_TITLE						=				"Sciformix C-PAT SCEPTRE"		;
		public static final String TOOL_NAME						=				"CPAT.B"						;
		public static final String TOOL_DISPLAY_NAME				=				"Sciformix C-PAT Tool B"		;
		public static final String TOOL_VERSION						=				"CPAT.B.01.01"					;
		public static final String SCEPTRE_USER_INFPUTS_SCREEN		=				"sceptreLogin.html"				;			
		public static final String TOOL_DESCRIPTION					=				"Case Processor Process Assist Tool  for SCEPTRE Safety Database";
		public static final String TOOL_HEADING						=				"Sciformix C-PAT SCEPTRE (Tool B)";
	}
	
	/*******code by shambhu**********/
	public static class ProductWhiteList
	{
		public static final String GENERIC_NAME						=				"GENERIC_NAME"					;			
		public static final String REG_CLASS						=				"REG_CLASS"						;			
		public static final String PRODUCT_FORM						=				"PRODUCT_FORM"					;			
		public static final String APPROVAL_NUMBER					=				"APPROVAL_NUMBER"				;			
		public static final String REG_OTC							=				"otc"							;			
		public static final String REG_DIETARY_SUPPLEMENT			=				"Dietary Supplement"				;			
		
	}
	
	public static class UserInputs
	{
		public static final String SCEPTRE_USERNAMAE				=				"SCEPTRE_USERNAME"			   	;
		public static final String SCEPTRE_PASSWORD					=				"SCEPTRE_PASSWORD"				;
		public static final String SCEPTRE_OPERATIONMODE			=				"SCEPTRE_ACTIONMODE"			;
		public static final String SCEPTRE_CASE_ID					=				"SCEPTRE_CASEID"				;
		public static final String SCEPTRE_PRODUCT_LIST				=				"SCEPTRE_PRODUCTLIST"			;
		public static final String SCEPTRE_DATABASE					=				"SCEPTRE_DATABASE"				;
	}
	
	public static class CaseDetails
	{
		public static final String LOCAL_CASE_ID 					=				"SCP_LOCAL_CASE_ID"				;
		public static final String CASE_TYPE						=				"CASE_TYPE"						;
		public static final String CASE_VERSION						=				"CASE_VERSION"					;
		public static final String CASE_QUEUE						=				"CASE_QUEUE"					;
	}
	
	public static class CaseIdTabKeys
	{
		public static final String SOURCES_A1112					=				"SourcesA1112"					;
		public static final String SAFETY_REPORT_ID					=				"SafetyReportID"				;
		public static final String PRIMARY_SOURCE_COUNTRY			=				"PrimarySourceCountry"			;
		public static final String OCCUR_COUNTRY					=				"OccurCountry"					;
		public static final String FIRST_RECEIVED_DATE 				= 				"FirstReceivedDate"				;
		public static final String DATE_OF_RECENT_INFO				=				"DateOfRecentInfo"		 		;
		public static final String TRANSMISSION_DATE				=				"TransmissionDate"				;
		public static final String REPORT_TYPE 						=				"TypeofReport"					;
		public static final String MESSAGE_DATE 					= 				"MessageDate"					;
		public static final String OTHER_CASE_IDENTIFIERS			=				"OtherCaseIdentifiersInPreviousTransmission" ;
		public static final String LIFE_THREATENING 				= 				"LifeThreatening"				;
		public static final String DISABLINGINCAPACITATING 			= 				"DisablingIncapacitating"		;
		public static final String RESULTED_IN_DEATH				=				"ResultedInDeath"				;
		public static final String CAUSED_PROLONGED_HOSP			=				"CausedProlongedHospitalization";
		public static final String CONGENITALANOMALYBIRTHDEFECT 	= 				"CongenitalAnomalybirthdefect"	;
		public static final String OTHER_MED_IMP 					= 				"OtherMedicallyImportantCondition"	;
		public static final String DATE_OF_DEATH 					= 				"PatientDeath_DateOfDeath"		;
		public static final String WAS_AUTOPSY_DONE 				= 				"PatientDeath_WasAutopsyDone"	;
		
				
	}
	
	public static class PatientTabKeys
	{
		public static final String REPORTER_POSTCODE				=				"Reporter_Postcode"             ;
		public static final String PATIENT_INITIALS					=				"PatientInitial"				;
		public static final String PATIENT_DATE_OF_BIRTH			=				"Patient_DateOfBirth"			;
		public static final String PATIENT_HEIGHT					=				"Patient_Heightcm"              ;
		public static final String PATIENT_WEIGHT					=				"Patient_Weightkg"              ;
		public static final String PATIENT_LMP_DATE					=				"Patient_LMPdate"               ;
		public static final String PATIENT_GPMEDICAL_RECORD_NO		=               "Patient_GPmedicalRecordNo"     ;
		public static final String PATIENT_SPECIALIST_RECORD_NO     =               "Patient_SpecialistRecordNo"    ;
		public static final String PATIENT_HOSPITAL_RECORD_NO       =               "Patient_HospitalRecordNo"      ;
		public static final String PATIENT_GENDER 					= 				"Patient_Sex"         			;
		public static final String PATIENT_ONSET_AGE				=				"Patient_OnsetAge";
		public static final String PATIENT_AGE_GROUP				=				"Patient_AgeGroup";
	}
	
	public static class PMHTabKeys
	{
		public static final String PMH_EPISODE_NAME                 =               "PMH_EpisodeName"               ;
		public static final String PMH_START_DATE                   =       		"PMH_StartDate"                 ;
		public static final String PMH_END_DATE                     =     			"PMH_EndDate"                   ;
		public static final String PMH_CONTINUING                   =       		"PMH_Continuing"                ;
		public static final String PMH_COMMENTS						= 				"PMH_Comments"					;
	}
	
	public static class PDTTabKeys
	{
		public static final String PDT_REPORTED_DRUG_NAME			=				"PDT_DrugName"				;
		public static final String PDT_START_DATE                   =       		"PDT_Startdate"                 ;
		public static final String PDT_END_DATE                     =     			"PDT_Enddate"                   ;
		public static final String PDT_INDICATION                   =       		"PDT_Indication"                ;
		public static final String PDT_INDICATION_MEDRA_VERSION     =               "PDT_IndicationMedDRAversion"   ;
		public static final String PDT_REACTION                     =     			"PDT_Reaction"                  ;
		public static final String PDT_REACTION_MEDRA_VERSION       =               "PDT_ReactionMedDRAversion"     ;
	}
	
	public static class DrugTabKeys
	{
		public static final String DRUG_PARENT_ROUTE_OF_ADMIN       =               "Drug_ParentRouteOfAdmin"       ;
		public static final String DRUG_DATE_START_DRUG             =      			"Drug_StartDateOfDrug"          ;
		public static final String DRUG_lAST_ADMIN                  =    			"Drug_EndDate"                  ;
		public static final String DRUG_DURATION_OF_DRUG_ADMIN      =               "Drug_DurationOfDrugAdmin"      ;
		public static final String DRUG_DOSAGE_TEXT                 =         		"Drug_DosageText"               ;
		//commented by shambhu as duplicate values assign for same key
		//public static final String DRUG_START_DATE                  =        		"Drug_StartDateOfDrug"          ;
		public static final String DRUG_END_DATE                    =      			"Drug_EndDate"                  ;
		public static final String DRUG_BATCH_NO                    =      			"Drug_Batch_LotNo"              ;
		public static final String DRUG_START_PERIOD                =          		"Drug_StartPeriod"              ;
		public static final String DRUG_LAST_PERIOD                 =         		"Drug_LastPeriod"               ;
		public static final String DRUG_RESULT                      =    			"DRR_Result"                    ;
		public static final String DRUG_COUNTRY_OBTAINED            =               "Drug_CountryDrugObtained"      ;
		public static final String DRUG_NAME_OF_HOLDER_APPLICANT    =               "Drug_NameOfHolder_Applicant"   ;
		public static final String DRUG_MEDICIANL_PRODUCT_NAME		=				"Drug_MedicinalProductName"		;

		/**************code by shammbhu*******************/
		public static final String DRUG_ROUTE_OF_ADMINISTRATION		=				"Drug_RouteOfAdministration"	;
		public static final String NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION=		"Narrative_PatientRouteofAdministration"		;
		public static final String NARRATIVE_PRODUCT_STOP_DATE		=				"Narrative_ProductStopDate"		;
		public static final String NARRATIVE_PRODUCT_START_DATE		=				"Narrative_ProductStartDate"	;
		public static final String DRUG_DOSE						=				"Drug_Dose"						;
		public static final String NARRATIVE_PRODUCT_CODE			=				"Narrative_ProductCode"			;
		public static final String DRUG_DRUGINDICATION				=				"Drug_DrugIndication"			;
		public static final String NARRATIVE_NARRATIVE_INDICATION	=				"Narrative_NarrativeIndication"	;
		public static final String NARRATIVE_PRODUCT_LOT_NUMBER		=				"Narrative_ProductLotNumber"	;
		public static final String NARRATIVE_PRODUCT_EXPIRY_DATE	=				"Narrative_ProductExpiryDate"	;
		public static final String NARRATIVE_EXTERNAL_SOURCE_NUMBER	=				"Narrative_ExternalSourceNumber";
		public static final String DAYS								=				"Days"							;
		public static final String DRUG_USE_FOR_UNKNOWN_INDICATION	=				"Drug use for Unknown Indication";
		public static final String PRODUCT_USED_FOR_UNKNOWN_INDICATION=				"Product used for Unknown Indication";
		public static final String NARRATIVE_PRODUCTNAME			=				"Narrative_ProductName"			;
		public static final String NARRATIVE_PRODUCT_DOSE_OR_QUANTITY=				"Narrative_ProductDoseOrQuantity";
		public static final String DRUG_DOSE_NO						=				"Drug_DoseNo"					;
		public static final String NARRATIVE_PRODUCT_USE_FREQUENCY	=				"Narrative_ProductUseFrequency"	;
	
	}
	
	public static class ReporterTabKeys
	{
		public static final String REPORTER_TITLE                   =       		"Reporter_title"                ;
		public static final String REPORTER_MIDDLE_NAME             =             	"Reporter_MiddleName"           ;
		public static final String REPORTER_ORGANIZATION            =              	"Reporter_Organization"         ;
		public static final String REPORTER_DEPARTMENT              =            	"Reporter_Department"           ;
		public static final String REPORTER_STREET                  =        		"Reporter_Street"               ;
		
		public static final String REPORTER_GIVEN_NAME				=				"Reporter_GivenName"			;
		public static final String REPORTER_FAMILY_NAME				=				"Reporter_FamilyName"			;
		
		
		public static final String SENDER_TITLE                      =    			"Sender_Title"                   ;
		public static final String SENDER_GIVEN_NAME                =          		"Sender_GivenName"              ;
		public static final String SENDER_FAMILY_NAME               =           	"Sender_FamilyName"             ;
		
		/*****code by shambhu****/
		public static final String REPORTER_QUALIFICATION           =           	"Reporter_Qualification"        ;
		public static final String NARRATIVE_REPORTERTYPE           =           	"Narrative_ReporterType"        ;
		public static final String REPORTER_CITY 					=           	"Reporter_City"			        ;
		public static final String REPORTER_STATE 					=           	"Reporter_State"		        ;
		public static final String REPORTER_POSTCODE				=           	"Reporter_Postcode"		        ;
		public static final String REPORTER_COUNTRY					=           	"Reporter_Country"		        ;
		public static final String PHONE 							= 				"Sender_Telno"					;
		public static final String FAX								=				"Sender_Faxno"					;
		public static final String EMAIL_ADDRESS					=				"Sender_Emailaddress"			;
		public static final String SALUTATION 						= 				"Receiver_Title"				;
		public static final String STUDY_NAME 						= 				"Reporter_Studyname"			;
		public static final String SPONSOR_STUDY_NO					=				"Reporter_SponsorStudyNo"		;
		public static final String OBSERVED_STUDY_TYPE				=				"Reporter_ObservedStudyType"	;
	}
	
	public static class DiagnosticTabKeys
	{
		public static final String TEST_TEST_NAME                   =       		"Test_TestName"                 ;
		public static final String TEST_TEST_UNIT                   =       		"Test_TestUnit"                 ;
		public static final String TEST_NORMAL_LOW_RANGE            =              	"Test_NormalLowRange"           ;
		public static final String TEST_NORMAL_HIGH_RANGE           =               "Test_NormalHighRange"          ;
		public static final String TEST_TEST_DATE                   =       		"Test_TestDate"                 ;
		public static final String TEST_TEST_RESULT                 =         		"Test_TestResult"               ;
		public static final String TEST_MORE_INFO                   =       		"Test_MoreInfo"					;
	}
	
	public static class ReactionTabKeys
	{
		public static final String REACTION_OUTCOME     			 =               "Reaction_Outcome"   			;
		public static final String NARRATIVE_REACTIONOUTCOME		 =               "Narrative_ReactionOutcome"   	;
		public static final String REACTION_STARTDATE				 =               "Reaction_StartDate"		   	;
		public static final String NARRATIVE_REACTION_START_DATE	 =               "Narrative_ReactionStartDate" 	;
		public static final String REACTION_ENDDATE	 				 =               "Reaction_EndDate"			 	;
		public static final String NARRATIVE_REACTION_STOPDATE	 	 =               "Narrative_ReactionStopDate" 	;
		public static final String REACTION_DURATION			 	 =               "Reaction_Duration"		 	;
		public static final String DURATION_DAYS					 =				 "Day"							;
		public static final String REACTION_FIRST_TIME 				 = 				 "Reaction_ReactionFirstTime"	;
		public static final String REACTION_LAST_TIME 				 = 				 "Reaction_ReactionLastTime"	;
		public static final String DAYS 			 				 = 				 "Days"							;
	}
	
	public static class NarrativeKeys
	{
		public static final String VERBATIM							=				"Narrative_VerbatimEnglishLanguage"	;
		public static final String PRIMARY_CONSUMER_COUNTRY			=				"Narrative_PrimaryConsumerCountry"	;
		public static final String EVENT_COUNTRY_OCCURED 			= 				"Narrative_EventCountryOccurred" 	;
		public static final String REPORT_TYPE 						= 				"Narrative_ReportType"				;
		public static final String PATIENT_INITIALS 				= 				"Narrative_PatientInitials"			;
		public static final String PATIENT_GENDER 					= 				"Narrative_PatientGender"			;
		public static final String PATIENT_HEIGHT 					= 				"Narrative_PatientHeight"			;
		public static final String PATIENT_WEIGHT 					= 				"Narrative_PatientWeight"			;
		public static final String PATIENT_DOB 						= 				"Narrative_PatientDOB"				;
		public static final String PATIENTS_ALLERGIES_DESC			= 				"Narrative_PatientAllergiesDescription"			;
		public static final String RELEVANT_MEDICAL_HISTORY_DESC    =               "Narrative_RelevantMedicalHistoryDescription"	;
		public static final String RELEVANT_MEDICAL_HISTORY         =               "Narrative_RelevantMedicalHistory"  ;
		public static final String PRODUCT_START_DATE				=				"Narrative_ProductStartDate"        ;
		public static final String PRODUCT_END_DATE					=				"Narrative_ProductStopDate"			;
		public static final String PRODUCT_NAME						=				"Narrative_ProductName"				;
		public static final String REPORTED_INDICATION				=				"Narrative_NarrativeIndication"		;
		public static final String PATIENTS_ALLERGIES				=				"Patient's allergies"				;
		public static final String DEATH 							= 				"Narrative_Death"					;
		public static final String ADMITTED_HOSPITAL_EVENT			=				"Narrative_AdmittedToHospitalRelatedToTheEvent"	;
		public static final String DATE_OF_DEATH					=				"Narrative_DateofDeathAndReportedCauseOfDeath";
		
		public static final String DATE_HOSPITALIZATION_ADMISSION	=				"Narrative_DateOfHospitalizationAdmission";
		public static final String DATE_HOSPITALIZATION_DISCHARGE	=				"Narrative_DateOfHospitalizationDischarge";
		public static final String EVENT_ASSOCIATED_BIRTH_DEFECT	=				"Narrative_EventAssociatedWithABirthDefect";
		public static final String REPORTED_CAUSE_BIRTH_DEFECT		=				"Narrative_ReportedCauseofBirthDefect";
		public static final String PREGNANT							=				"Narrative_Pregnant";
		public static final String PREGNANT_DUE_DATE				=				"Narrative_PregnancyDueDate";
		public static final String WAS_BREAST_FEEDING				=				"Narrative_WasPatientBreastfeeding";
		public static final String CHILD_UNFAVOURABLE_EVENT			=				"Narrative_DidChildHaveUnfavorableEvent";
		public static final String EVENT_ASSOCIATED_MISCARRIAGE		=				"Narrative_IsEventAssociatedWithAMiscarriage";
		public static final String DATE_CAUSE_MISCARRIAGE			=				"Narrative_DateandReportedCauseofMiscarriage";
		
		public static final String CASE_NOTES_ENGLISH_LANGUAGE		=				"Narrative_CaseNotesEnglishLanguage";
		public static final String ACTIVITY_CATEGORY				=				"Narrative_ActivityCategory"		;
		
		public static final String AWARENESS_DATE					=				"Narrative_AwarenessDate"			;
		public static final String CONSUMER_COUNTRY 				= 				"Narrative_ConsumerCountry"			;
		public static final String NARRATIVE_PERMISSION_TO_CONTACT	= 				"Narrative_PermissiontoContact"			;
		
	}
	
	public static class DownloadSD
	{
		public static final String E2B_DOCUMENT_NAME_VALUE			=				"E2B_PDF_REPORT"				;
		public static final String DOCUMENT_TYPE_VALUE				=				"IRT Supporting"				;
	}
	
	public static class CaseType
	{
		public static final String INITIAL							=				"INITIAL"						;
		public static final String FOLLOWUP							=				"FOLLOWUP"						;
	}
	
	public static class DATEFORMATS
	{
		public static final String DF_TRANSMISSION_DATE				=				"dd-MMM-yyyy"					;
		public static final String DF_MESSAGE_DATE					=				"dd-MMM-yyyy HH:mm:ss"			;
		public static final String DOB_1							=				"dd-MM-yyyy"					;
	}
	
	public static class SceptreValidationFields
	{
		public static final String REPORT_TYPE						=				"Spontaneous"					;
		public static final String SERIOUSNESS						=				"Not Serious"					;
		public static final String SERIOUS 							= 				"No"							;
		public static final String LIFE_THREATENING					=				"No"							;
		public static final String CAUSED_PROLONGED_HOSPITALIZATION = 				"No"							;
		public static final String DISABLING_INCAPACITATING 		= 				"No"							;
		public static final String CONGENITAL_ANOMALY_BIRTH_DEFECT 	= 				"No"							;
		public static final String RESULTS_IN_DEATH 				= 				"No"							;
		public static final String OTHER_MEDICALLY_IMPORTANT_CONDITION = 			"No"							;
		public static final String CASE_PRIORITY 					= 				"5";
		
	}

	public static class PayloadKeys
	{
		public static final String TAB								=				"TAB"							;
		public static final String FIELDS 							= 				"FIELD_LIST"					;
		public static final String QUEUE 							= 				"CASE_QUEUE"					;
		public static final String ACTIVITY_ID 						= 				"SCIFORMIX_ACTIVITY_ID"			;
		public static final String USER_CHOICE						=				"USER_CHOICE"					;
		public static final String POTENTIAL_DUPLICATES 			= 				"POTENTIAL_DUPLICATE_CASEIDS"	;
		public static final String USR_MSG 							= 				"USER_MSG"						;
		public static final String CODING_STATUS_FAIL 				= 				"CODING_STATUS_FAIL"			;
		public static final String ADDITIONAL_SD_LIST				=				"ADDITIONAL_SD_LIST"			;
		public static final String PENDING_OTHERIDS 				= 				"PENDING_OTHERIDS"				;
		public static final String PENDING_LINKED_CASENOS 			= 				"PENDING_LINKED_CASENOS"		;
		public static final String REACTION_KEYWORDS 				= 				"REACTION_KEYWORDS"				;
		public static final String PENDING_DRUG_ADDITIONALINFO 		= 				"PENDING_DRUG_ADDITIONALINFO"	;
		
		
	}
	
	public static class Tabs
	{
		public static final String CASE_ID							=				"Case ID"						;
		public static final String PATIENT							=				"Patient"						;
		public static final String MEDICAL_HISTORY					=				"Medical History"				;
		public static final String DRUG_HISTORY						=				"Drug History"					;
		public static final String REACTION							=				"Reaction"						;
		public static final String DRUG								=				"Drug"							;
		public static final String REPORTER							=				"Reporter"						;
		public static final String CASENOTES						=				"Case Notes"					;
		
	}
	
	
	
	public static class CodingStatus
	{
		public static final String FAIL								=				"FAIL"							;
	}
	
	public static class DateDifference
	{
		public static final String YEAR								=				"YEAR"							;
		public static final String YEARS							=				"YEARS"							;
		public static final String MONTHS							=				"MONTHS"						;
		public static final String MONTH							=				"MONTH"						;
		public static final String DAY								=				"DAY"							;
		public static final String DAYS								=				"DAYS"							;
		public static final String HOURS 							= 				"HOURS"							;
		public static final String MINUTES 							= 				"MINUTES"						;
		public static final String SECONDS 							= 				"SECONDS"						;
	}
	
	public static class SceptreDataMap
	{
		public static final String LOCAL_CASE_ID					=				"LOCAL_CASE_ID"					;
		public static final String REPORTER_COUNTRY					=				"REPORTER_COUNTRY"				;
	}
	
	/******code by shambhu***
	 * for other identification number
	 * */
	public static class OtherIdentificationNumber
	{
		
		public static final String OTHER_ID_NUMBER_LIST				=				"Other Identification Numbers";
		public static final String EXTERNAL_SOURCENUMBER			=				"Narrative_ExternalSourceNumber";
		public static final String DUPLICATE_NUMBER					=				"DuplicateNumber";
		public static final String GCC_PLATFORM_CASENUMBER			=				"Narrative_GCCPlatformCaseNumber";
		public static final String LINKED_REPORT_NO					=				"LinkedReportNo";
	}
	
	public static class PropertiesMap
	{
		public static final String ACTIVITY_CATEGORY_LIST			=				"ActivityCategoryList"			;
		public static final Object REACTION_KEYWORDS 				= 				"ReactionKeywordsList"			;
		public static final String HEIGHT_CENTIMETER 				=	 			"Height.Centimeter"				;
		public static final String HEIGHT_INCHES 					=	 			"Height.Inches"					;
		public static final String WEIGHT_KILOGRAM 					=	 			"Weight.Kilogram"				;
		public static final String WEIGHT_POUND 					=	 			"Weight.Pound"					;
		
	}
	
	public static class DupSummaryKeys
	{
		public static final String AER								=				"AER Number"					;
		public static final String PRODUCT_NAME						=				"Medicinal Product Name"		;
		public static final String PATIENT_INITIALS					=				"Patient Initials"				;
		public static final String PATIENT_DOB						=				"Patient DOB"					;
		public static final String PATIENT_ONSET_AGE				=				"Patient Age"					;
		public static final String PATIENT_GENDER					=				"Patient Gender"				;
		public static final String PRIMARY_SOURCE_COUNTRY			=				"Country of Primary Source"		;
		public static final String REPORTER_GIVENNAME				=				"Reporter Given Name"			;
		public static final String REPORTER_FAMILYNAME				=				"Reporter Family Name"			;
	}
	
	public static class API
	{
		public static final String OK								=				"OK"							;
		public static final String STATUS							=				"Status"						;
		public static final String OBJSEQID							=				"objectSeqId"					;
		public static final String OBJNAME							=				"objectName"					;
	}
	
	public static class PreviewScreen
	{
		public static final String PAYLOAD_ABORT					=				"ABORT"							;
	}
	
	public static class SearchStatusScreen
	{
		public static final String PAYLOAD_ABORT					=				"ABORT"							;
		public static final String DUPLICATE_LABEL					=				"No duplicates found"			;
	}
	
	public static class FileType
	{
		public static final String E2B								=				"E2B"							;
		public static final String SCEPTRE							=				"SCEPTRE"						;
		public static final String HEALTH_CANADA 					= 				"Health Canada"					;
		public static final String MESA 							=	 			"MESA"							;
	}
	public static class Units
	{
		public static final String UNIT 							=	 			"Unit"							;
		public static final String CENTIMETERS 						=	 			"Centimeters"					;
		public static final String INCHES 							=	 			"Inches"						;
		public static final String KILOGRAMS 						=	 			"Kilograms"						;
		public static final String POUNDS 							=	 			"Pounds"						;
		
	}
}
