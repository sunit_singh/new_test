package com.sciformix.client.pat.toolb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.html.FormSubmitEvent;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.BaseWebApplicationWriter.BROWSER;
import com.sciformix.client.automation.CaseDocument;
import com.sciformix.client.automation.SearchResults;
import com.sciformix.client.automation.sceptre.SceptreConstants;
import com.sciformix.client.automation.sceptre.SceptreConstants.SceptreSearchFields;
import com.sciformix.client.automation.sceptre.SceptreWebApplicationWriter;
import com.sciformix.client.pat.commons.CaseDocumentComparator;
import com.sciformix.client.pat.commons.CpatFieldMap.CpatField;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.ParsedDataWithConfiguration;
import com.sciformix.client.pat.commons.PreviewCaseDoc;
import com.sciformix.client.pat.commons.ScpUser;
import com.sciformix.client.pat.commons.WorkbookWorkSheetWrapperDEA;
import com.sciformix.client.pat.commons.screens.CpatCasePreviewScreen;
import com.sciformix.client.pat.commons.screens.CpatResultScreen;
import com.sciformix.client.pat.commons.screens.CpatSearchStatusScreen;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse.STATUS;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarAA;
import com.sciformix.commons.rulesengine.Criterion;
import com.sciformix.commons.rulesengine.Criterion.Operator;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;
import com.sciformix.commons.utils.StringUtils;

public class CpatToolBHelper {
	
	private static final Logger log = LoggerFactory.getLogger(CpatToolBHelper.class);
	
	private static final String CONFIG_FOR_INSERT_PROPERTIES = "ConfigForSceptre.properties";
	
	public static Map<String, String> propMap = new HashMap<>();
	
	private static SceptreWebApplicationWriter oAppWriter = null;
	
    private static List<String> reportTypeList = null;
    
    private static int headerIndex;
	
    private static  Map<String, Map<String, Object>> sceptreTabValues  = new HashMap<>();
    
    private static Map<String, String> whiteListValuesMap = null;
    private static DataTable productListDataTable = null;
    
    //Other identification number from parsed dataTable(summaryDataTable)
    private static Set<String> SetOfOtherIdentificationNumbersFromSD = null;
    
    //Number Of Linked Cases From Parsed data Tabele
    private static Set<String> setOfLinkedCasesFromSD = null;
    
    //Case Type INITIAL or FOLLOWUP
    private static String caseType = null;
    
    private static List<String> fupNewFieldsMnemonicList = null;
    
    private static List<String> propFileList = null;
    
	static 
	{
		propFileList = new ArrayList<>();
		propFileList.add(CONFIG_FOR_INSERT_PROPERTIES);
		propFileList.add(CpatToolConstants.CPAT_CONFIG);
		
		for(String prop: propFileList)
		{
			propMap.putAll(CpatToolHome.loadProperties(prop));	
		}
	}
	

	/**
	 * Locat Case  by AER
	 * @param oToolBase
	 * @param oCallbackAction
	 * @param p_aerNo
	 * @param userInputMap
	 */
	public static void locateCase(CpatToolBase oToolBase, IAction oCallbackAction, String p_aerNo, Map<String, String> userInputMap){
		log.info("Case Id: " + p_aerNo);
		
		SearchResults oSearchResults = null;
		
		Map<String, String> payload = null;
		Map<String, Object> sceptreDataMap = null;
		Map<String, List<String>> readSceptreMap = null;
		
		String localCaseId = null;
		String caseVersion = null;
		String caseQueue = null;
		String serverUrl = null;
		
		ScpUser oScpUser = null;

		boolean takeOwnership = true;
		boolean isSceptreValidationsPass = true;

		payload = new HashMap<>();
		
		sceptreDataMap = new HashMap<>();
		
		oScpUser = oToolBase.scpUser;
	    
		oAppWriter = new SceptreWebApplicationWriter(BROWSER.CHROME);
		
		try
		{
			serverUrl = propMap.get(CpatToolHome.getPropertyNameAppendedWithZone(oScpUser.getZone(), CpatToolConstants.SCEPTRE_SERVER_SUFFIX));
			
			// Validate server url
			if (StringUtils.isNullOrEmpty(serverUrl)) 
			{
				payload.put(CpatToolBConstants.PayloadKeys.USR_MSG, "Invalid Server URL");
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);

				JOptionPane.showMessageDialog(null, "Invalid Server URL");
				
				closeWriterDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);

				return;
			}
			
			oAppWriter.launchBrowser(serverUrl);
			
			oToolBase.browserMapDetails = oAppWriter.getBrowserdetails();
			
			oAppWriter.login(userInputMap.get(CpatToolBConstants.UserInputs.SCEPTRE_USERNAMAE),	userInputMap.get(CpatToolBConstants.UserInputs.SCEPTRE_PASSWORD));
			userInputMap.remove(CpatToolBConstants.UserInputs.SCEPTRE_PASSWORD);
			
			oAppWriter.prepareForSearch();
			
			oSearchResults = oAppWriter.locateCase(p_aerNo);
			
			// Check if search results are found
			if (!oSearchResults.resultsFound()) 
			{
				payload.put("USER_MSG", "No Results found with AER: " + p_aerNo);
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);

				JOptionPane.showMessageDialog(null, "No Case found with AER: " + p_aerNo);
				
				closeWriterDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);

				return;
			}

			caseVersion = getMaxCaseVersion(oSearchResults, p_aerNo);

			// Check if caseVersion is blank
			if (StringUtils.isNullOrEmpty(caseVersion)) 
			{
				payload.put("USER_MSG", "Search Results contains different AER than one serached for");
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
				
				JOptionPane.showMessageDialog(null, "Search Results contains different AER than one serached for");
				
				closeWriterDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);

				return;
			}

			if (caseVersion.equals("0")) 
			{
				caseType = CpatToolBConstants.CaseType.INITIAL;
			} 
			else 
			{
				caseType = CpatToolBConstants.CaseType.FOLLOWUP;
			}

			caseQueue = getCaseProcessingQueue(oSearchResults, p_aerNo, caseVersion);

			if (!caseQueue.equalsIgnoreCase(CpatToolBConstants.VALID_CASE_QUEUE)) 
			{
				payload.put("USER_MSG", "Case Queue cannot be processed");
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
				
				JOptionPane.showMessageDialog(null, "Case Queue cannot be processed");
				
				closeWriterDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);

				return;
			}

			localCaseId = oAppWriter.getLocalCaseId(p_aerNo, caseVersion, takeOwnership);

			readSceptreMap = getSceptreFieldsForReading(caseType);

			sceptreTabValues = oAppWriter.getFieldValues(readSceptreMap);
			
			sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB).put(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID, localCaseId);
			sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB).put(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_CASEIDNUMBER, oAppWriter.fetchOtherIDNumbers());
			
			//perform sceptre level validation before download sd documents
			isSceptreValidationsPass = performSceptreValidations(oToolBase, oCallbackAction, payload);
			
			if(!isSceptreValidationsPass)
			{
				return;
			}
			userInputMap.put(CpatToolBConstants.CaseDetails.CASE_TYPE, caseType);
			userInputMap.put(CpatToolBConstants.CaseDetails.CASE_VERSION, caseVersion);
			userInputMap.put(CpatToolBConstants.CaseDetails.CASE_QUEUE, caseQueue);
			
			sceptreDataMap.put(CpatToolBConstants.SceptreDataMap.LOCAL_CASE_ID, localCaseId);
			
			oCallbackAction.performAction(CPAT_ACTION.DOWNLOAD_SD);
			
		}catch(Exception excep)
		{
			log.error("Locate Case Failed: ", null, excep);
			payload.put("USER_MSG", "Locate Case Failed");
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
			
			JOptionPane.showMessageDialog(null, "Locate Case Failed. Please check logs");
			closeWriterDriver();
		}
	}

	public static void downloadCaseSD(CpatToolBase oToolBase, IAction oCallbackAction) 
	{
		List<Criterion> listCriteria = null;
		List<CaseDocument> listCaseDocument = null;
		List<String> sdIdentificationList = null;
		List<PreviewCaseDoc> listParsedCaseDocs = null;
		
		StringBuilder sdCriteria = null;
		StringBuilder sdNames, invalidSdNames = null;
		
		String docName = null;
		String docType = null;
		
		String caseDocVersion = null;
		String aerNo = null;
		
		String chromeVersion = null;
		String ieVersion = null;
		
		String sceptreLocalCaseId = null;
		
		Map<String, String> payload = null;
		Map<String, String> userInputMap = null;
		Map<String, Object> resultScreenMap = null;
		
		Map<String, String> sceptreMap = null;
		
		File caseDocFile = null;
		
		ParsedDataTable oCaseDocDataTable = null;
		ParsedDataTable summaryDataTable = null;
		
		SciPortalWebApiResponse response = null;

		ScpUser oScpUser = null;
		
		int activityId = 0;
		
		int validSdCounter = 0;
		
		boolean isValidSd = true;
		boolean isValidSdSummary = true;
		boolean isRegisterActivitySuccess = false , isDateFormateSupported = false;
		
		listCaseDocument = new ArrayList<>();
		listCriteria = new ArrayList<>();
		sdIdentificationList = new ArrayList<>();
		listParsedCaseDocs = new ArrayList<>();
	
		sdCriteria = new StringBuilder();
		sdNames = new StringBuilder();
		invalidSdNames = new StringBuilder();
		
		payload = new HashMap<>();
		sceptreMap = new HashMap<>();
		
		try
		{
			userInputMap = oToolBase.userInputsParams;
			
			resultScreenMap = new HashMap<>();
			
			oScpUser = oToolBase.scpUser;
			
			sdIdentificationList = Arrays.asList(propMap.get("SourceDocument.WhiteList").split(StringConstants.COMMA));
			
			sceptreLocalCaseId = String.valueOf(sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB).get(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID));
			
			aerNo = userInputMap.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID);
			
			oAppWriter.gotoCaseIDTab();
			listCaseDocument = oAppWriter.getAllDocumentsDetails();
			for(CaseDocument doc: listCaseDocument)
			{
				docName = String.valueOf(doc.getoAttributeMap().get(SceptreConstants.DownloadSD.DOCUMENT_NAME));
				docType = String.valueOf(doc.getoAttributeMap().get(SceptreConstants.DownloadSD.DOCUMENT_TYPE));
				
				if(isValWhiteListedContains(sdIdentificationList, docName) && docType.equalsIgnoreCase(CpatToolBConstants.DownloadSD.DOCUMENT_TYPE_VALUE))
				{
					validSdCounter++;
				}
				else
				{
					invalidSdNames = invalidSdNames.length() > 0 ? invalidSdNames.append("," + docName) : sdNames.append(docName);
				}
			}
			
			if(validSdCounter == 0 )
			{
				payload.put("USER_MSG", "No document matches the required criteria");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
			  	
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "No document matches the required criteria");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
				}
				
				return;
			}
			
			resultScreenMap.put(CpatToolConstants.ResultScreen.ADD_SDLIST, invalidSdNames.toString());
			oToolBase.resultScreenMap = resultScreenMap;
			
			listCriteria.add(Criterion.create(Operator.CONTAINS, SceptreConstants.DownloadSD.DOCUMENT_NAME, CpatToolBConstants.DownloadSD.E2B_DOCUMENT_NAME_VALUE));
			listCriteria.add(Criterion.create(Operator.MATCHES, SceptreConstants.DownloadSD.DOCUMENT_TYPE, CpatToolBConstants.DownloadSD.DOCUMENT_TYPE_VALUE));
			
			for(int i = 0; i < listCriteria.size(); i++)
			{
				sdCriteria = sdCriteria.length() > 0 ? sdCriteria.append("," + listCriteria.get(i).print()) : sdCriteria.append(listCriteria.get(i).print());
			}
			
			if(CpatToolBase.isTestMode)
			{
				listCaseDocument = CpatToolHome.provideListCaseDoc(CpatToolBase.testModePropMap);
			}
			else
			{
				listCaseDocument = oAppWriter.downloadSourceDocument(listCriteria);				
			}
			
			log.info("Doc Size: " + listCaseDocument.size());
			
			if(listCaseDocument.size() > 0)
			{	
				for(int i = 0; i < listCaseDocument.size(); i++)
				{
					docName = String.valueOf(listCaseDocument.get(i).getoAttributeMap().get(SceptreConstants.DownloadSD.DOCUMENT_NAME));
					caseDocVersion = String.valueOf(listCaseDocument.get(i).getoAttributeMap().get(SceptreConstants.DownloadSD.CASE_VERSION));
					
					caseDocFile = listCaseDocument.get(i).getPath();
					
					oCaseDocDataTable = new PdfDocParserAvatarAA(caseDocFile.getAbsolutePath(),CpatToolBase.isTestMode).parse();
					
					listParsedCaseDocs.add(new PreviewCaseDoc(caseDocFile, oCaseDocDataTable, caseDocVersion));
					
					sdNames = sdNames.length() > 0 ? sdNames.append("," + docName) : sdNames.append(docName);
						
				}
				
				// Validate Source Document(s)
				
				isValidSd = performMandatorySDValidations(oToolBase, oCallbackAction, payload, listParsedCaseDocs);
				
				if(!isValidSd)
				{
					return;
				}
				
				isValidSd = performOptionalSDValidations(oToolBase, oCallbackAction, payload, listParsedCaseDocs);
				if(!isValidSd)
				{
					return;
				}
				
				summaryDataTable = mergeAllCaseDocs(listParsedCaseDocs);
				
				// Validation for Product Code & get values from Product excel file for generic name , Reg class , product code , and formulation
				whiteListValuesMap = getProductValuesFromWhiteList(oToolBase, oCallbackAction, summaryDataTable);
				if(whiteListValuesMap == null)
				{
					payload.put("USER_MSG", "Product Code not found in Product List");
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				  	
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "Product Code not found in Product List");
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
					}
					
					return;
				}
				
				if(!CpatToolHome.isKeywordFound(whiteListValuesMap.get(CpatToolBConstants.ProductWhiteList.REG_CLASS), CpatToolBConstants.PRODUCT_REG_CLASS))
				{
					payload.put("USER_MSG", "No support for Product REG_CLASS: " + whiteListValuesMap.get(CpatToolBConstants.ProductWhiteList.REG_CLASS));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				  	
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "No support for Product REG_CLASS: " + whiteListValuesMap.get(CpatToolBConstants.ProductWhiteList.REG_CLASS));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
					}
					
					return;
				}
					
				isDateFormateSupported = validateNarrativeDateFields(oToolBase, oCallbackAction, payload, summaryDataTable);
				
				if(!isDateFormateSupported)
				{
					return;
				}
				
				performBusinessLogic(oToolBase, listParsedCaseDocs, summaryDataTable, sceptreMap);
	            
				/**
	             * SD Summary checks with Sceptre         
	             */
				
				isValidSdSummary = validateSdSummary(oToolBase, oCallbackAction, payload, summaryDataTable, sceptreMap);
					
				if(!isValidSdSummary)
				{
					return;
				}
				
				userInputMap.put(CpatToolBConstants.CaseDetails.LOCAL_CASE_ID, sceptreLocalCaseId);
				
				// Call Register Activity
				payload.put("CASE_TYPE", userInputMap.get(CpatToolBConstants.CaseDetails.CASE_TYPE));
				payload.put("CASE_QUEUE", userInputMap.get(CpatToolBConstants.CaseDetails.CASE_QUEUE));
				payload.put("SD_COUNT", String.valueOf(listCaseDocument.size()));
				payload.put("SD_NAME", sdNames.toString());
				
				if(oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERNAME).equalsIgnoreCase(CpatToolConstants.Browser.CHROME))
				{
					chromeVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
					ieVersion = StringConstants.EMPTY;
				}
				else
				{
					chromeVersion = StringConstants.EMPTY;
					ieVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
				}
				
				response = oToolBase.scpClient.registerActivity(oScpUser.getUserId(), oScpUser.getPassword(),
							oScpUser.getUserProjectsList().get(0).getProjectSeqId(), oToolBase.getToolDetails().getToolName(),
							CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId,
							CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload, chromeVersion, ieVersion);
				
				if(response.getStatus() == STATUS.SUCCESS)
				{
					activityId = Integer.parseInt(response.getData().toString());
					
					if(activityId > 0)
					{
						oToolBase.activityId = activityId;
						oCallbackAction.performAction(CPAT_ACTION.PREVIEW, listParsedCaseDocs, summaryDataTable, sceptreMap);			
					}
					
					else
					{
						JOptionPane.showMessageDialog(null, "Invalid Activity Id");
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Error: " + response.getCode() + " - " + response.getMessage());
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
			}
			else
			{
				payload.put("SD_IDENTIFICATION_CRITERIA", sdCriteria.toString());
				payload.put("USER_MSG", "No Source Documents found for AER: " + aerNo);
				
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
			  	
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "No Source Documents found for AER: " + aerNo);
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
				}
				
			}
			
			
		}
		catch(Exception excep)
		{
			log.error("SD Extraction failed", null, excep);
			excep.printStackTrace();
			payload.put("USER_MSG", "SD Extraction failed");
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
			
			JOptionPane.showMessageDialog(null, "SD Extraction failed. Please check logs ");
			closeWriterDriver();
		}
		
		
		
	}

	private static boolean isValWhiteListedContains(List<String> list, String val) 
	{
		for(String s: list)
		{
			if(val.toUpperCase().contains(s))
			{
				return true;
			}
		}
		
		return false;
	}

	private static boolean isValWhiteListedMatches(List<String> list, String val) 
	{
		for(String s: list)
		{
			if(val.equalsIgnoreCase(s))
			{
				return true;
			}
		}
		
		return false;
	}
	

	private static String getCaseProcessingQueue(SearchResults oSearchResults, String p_aerNo, String caseVersion) 
	{	
		// AER and Case Version is unique
		String caseQueue = null;
		
		for(int i=0; i<oSearchResults.resultCount(); i++)
		{
			if(p_aerNo.equals(String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER)))
					&& caseVersion.equals(String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.VERSION))))
			{
				caseQueue = String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.QUEUE));

			}

		}

		return caseQueue;
	}


	private static String getMaxCaseVersion(SearchResults oSearchResults, String p_aerNo) 
	{
		
		String caseVersion = null;
		String resultsCaseVersion = null;
		
		for(int i=0; i<oSearchResults.resultCount(); i++)
		{
			if(p_aerNo.equals(String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER))))
			{
				resultsCaseVersion = String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.VERSION)); 
				
				if(StringUtils.isNullOrEmpty(caseVersion))
				{
					caseVersion = resultsCaseVersion;
				}
				else
				{
					if(Integer.parseInt(resultsCaseVersion) > Integer.parseInt(caseVersion))
					{
						caseVersion = resultsCaseVersion;
					}
				}
			}

		}
		
		return caseVersion;
		
	}


	public static ParsedDataTable mergeAllCaseDocs(List<PreviewCaseDoc> listParsedCaseDocs)
	{
		ParsedDataTable oSummaryDataTable = new ParsedDataTable();
		ParsedDataTable oTable = null;
		ParsedDataValue oValue = null;
		
		// Sort 
		Collections.sort(listParsedCaseDocs, new CaseDocumentComparator());
		
		for (int i = listParsedCaseDocs.size() - 1; i >= 0; i--)
		{
			oTable = listParsedCaseDocs.get(i).getDataTable();
			
			for (int j = 0; j < oTable.colCount(); j++)
			{	
				if(!oSummaryDataTable.isColumnPresent(oTable.getMnemonic(j)))
				{
					oValue = oTable.getColumnValues(j).get(0);
					
					if(!StringUtils.isNullOrEmpty(String.valueOf(oValue.displayValue())))
					{
						oSummaryDataTable.addColumn(oTable.getMnemonic(j), oTable.getLabel(j), oValue);
					}					
				}
			}
		}
		
		return oSummaryDataTable;
	}

	public static void performDataEntry(CpatToolBase oToolBase, IAction oCallbackAction, String p_caseId, Map<String, String> p_parsedDataMap, boolean isDuplicate)
	{

		log.info("Sceptre Map: " + p_parsedDataMap);
		log.info("Case Iid: " + p_caseId);
		
		String html = null;
		
		Map<String, String> payload = null;
		Map<String, Object> params = null;
		Map<String, String> caseNotesMap = null;
		Map<String, String> codingStatusFailKeys = null;
		
		Map<String, Object> codingStatusMap = null;
		Map<String, Object> resultScreenMap = null;
		Map<String, List<String>> dataEntryDoneMap = null;
		Map<String, Object> prevSceptreValMap = null;

		List<String> listFieldsPopulatedForCaseIdTab = null;
		List<String> listFieldsPopulatedForPatientTab = null;
		List<String> listFieldsPopulatedForMedicalHistoryTab = null;
		List<String> listFieldsPopulatedForDrugHistoryTab = null;
		List<String> listFieldsPopulatedForReactionTab = null;
		List<String> listFieldsPopulatedForDrugTab = null;
		List<String> listFieldsPopulatedForReporterTab = null;
		
		List<String> notesList = null;
		
		List<String> readCodingStatusList = null;
		
		String caseVersion = null;
		String tab = null;
		String addSdList = null;
		String pendingOtherIds = null;
		String pendingLinkedCaseNos = null;
		String reactionKeysFound = null;
		String pendingDrugAdditionalInfo = null;
		
		StringBuilder codingFail = null;
		
		boolean bTakeOwnership = true , isTabFeedSuccess = true;
		
		payload = new HashMap<>();
		params = new HashMap<>();
		caseNotesMap = new HashMap<>();

		codingStatusFailKeys = new HashMap<>();
		codingStatusMap = new HashMap<>();
		dataEntryDoneMap = new HashMap<>();
		
		readCodingStatusList = new ArrayList<>();
		notesList = new ArrayList<>();
		
		codingFail = new StringBuilder();
		prevSceptreValMap = new HashMap<>();
		
		try{
			
			resultScreenMap = oToolBase.resultScreenMap;
			caseVersion = oToolBase.userInputsParams.get(CpatToolBConstants.CaseDetails.CASE_VERSION);

			if(oAppWriter.isDuplicateSearchPage())
			{
				oAppWriter.refreshForSearch();
			}
			else
			{
				oAppWriter.prepareForSearch();
			}
			
			oAppWriter.locateCase(p_caseId);
			oAppWriter.getLocalCaseId(p_caseId, caseVersion, bTakeOwnership);
			
				
			/**
			 * D.E - Case ID Tab
			 */
			tab = "CASE ID";
			listFieldsPopulatedForCaseIdTab = performCaseIDTab(oToolBase, p_parsedDataMap);
			payload.put(CpatToolBConstants.PayloadKeys.TAB, CpatToolBConstants.Tabs.CASE_ID);
			payload.put(CpatToolBConstants.PayloadKeys.FIELDS, convertListToString(listFieldsPopulatedForCaseIdTab));
			isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			if(!isTabFeedSuccess)
			{
				closeWriterDriver();
				return;				
			}
			
			
			/**
			 * D.E - PATIENT Tab
			 */
			tab = "PATIENT";
			listFieldsPopulatedForPatientTab = oAppWriter.feedPatientTab(p_parsedDataMap);
			payload.put(CpatToolBConstants.PayloadKeys.TAB, CpatToolBConstants.Tabs.PATIENT);
			payload.put(CpatToolBConstants.PayloadKeys.FIELDS, convertListToString(listFieldsPopulatedForPatientTab));
			isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			if(!isTabFeedSuccess)
			{
				closeWriterDriver();
				return;				
			}
			
			
			/**
			 * D.E - Medical History Tab
			 */
			tab = "MEDICAL HISTORY";
			if (!isNullEmptyORNonAcceptable(p_parsedDataMap , SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE))
			{
				listFieldsPopulatedForMedicalHistoryTab = performMedicalHistoryTab(p_parsedDataMap);
				readCodingStatusList.add(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS);
				oAppWriter.readData(codingStatusMap, readCodingStatusList);
				
				for(String key: readCodingStatusList)
				{
					if(String.valueOf(codingStatusMap.get(key)).equalsIgnoreCase(CpatToolBConstants.CodingStatus.FAIL))
					{
						codingStatusFailKeys.put(CpatToolBConstants.Tabs.MEDICAL_HISTORY, key);
					}
				}
				
				payload.put(CpatToolBConstants.PayloadKeys.TAB, CpatToolBConstants.Tabs.MEDICAL_HISTORY);
				payload.put(CpatToolBConstants.PayloadKeys.FIELDS, convertListToString(listFieldsPopulatedForMedicalHistoryTab));
				isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
				if(!isTabFeedSuccess)
				{
					closeWriterDriver();
					return;				
				}
			}
			else
			{
				listFieldsPopulatedForMedicalHistoryTab = new ArrayList<>();
			}
			
			/**
			 * D.E - Drug History Tab
			 */

			tab = "DRUG HISTORY";
			if (!isNullEmptyORNonAcceptable(p_parsedDataMap , SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME))
			{
				listFieldsPopulatedForDrugHistoryTab = performDrugHistoryTab(p_parsedDataMap);
				readCodingStatusList.clear();
				codingStatusMap.clear();
				readCodingStatusList.add(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1);
				oAppWriter.readData(codingStatusMap, readCodingStatusList);
				
				for(String key: readCodingStatusList)
				{
					if(String.valueOf(codingStatusMap.get(key)).equalsIgnoreCase(CpatToolBConstants.CodingStatus.FAIL))
					{
						codingStatusFailKeys.put(CpatToolBConstants.Tabs.DRUG_HISTORY, key);
					}
				}

				payload.put(CpatToolBConstants.PayloadKeys.TAB, CpatToolBConstants.Tabs.DRUG_HISTORY);
				payload.put(CpatToolBConstants.PayloadKeys.FIELDS, convertListToString(listFieldsPopulatedForDrugHistoryTab));
				isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
				if(!isTabFeedSuccess)
				{
					closeWriterDriver();
					return;				
				}
			}
			else
			{
				listFieldsPopulatedForDrugHistoryTab = new ArrayList<>();
			}
			
			
			
			/**
			 * D.E - Reaction Tab
			 */
			tab = "REACTION";
			listFieldsPopulatedForReactionTab = oAppWriter.feedReactionTab(p_parsedDataMap);
			payload.put(CpatToolBConstants.PayloadKeys.TAB, CpatToolBConstants.Tabs.REACTION);
			payload.put(CpatToolBConstants.PayloadKeys.FIELDS, convertListToString(listFieldsPopulatedForReactionTab));
			isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			if(!isTabFeedSuccess)
			{
				closeWriterDriver();
				return;				
			}
			
			
			/**
			 * D.E - Drug Tab
			 */
			/*******code by shambhu*********/
			tab = "DRUG";
			listFieldsPopulatedForDrugTab = performDrugTab(p_parsedDataMap);
			payload.put(CpatToolBConstants.PayloadKeys.TAB, CpatToolBConstants.Tabs.DRUG);
			payload.put(CpatToolBConstants.PayloadKeys.FIELDS, convertListToString(listFieldsPopulatedForDrugTab));
			isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			if(!isTabFeedSuccess)
			{
				closeWriterDriver();
				return;				
			}
			
			/**
			 * D.E - Reporter Tab
			 */
			tab = "REPORTER";
			listFieldsPopulatedForReporterTab = oAppWriter.feedReporterTab(p_parsedDataMap);
			payload.put(CpatToolBConstants.PayloadKeys.TAB, CpatToolBConstants.Tabs.REPORTER);
			payload.put(CpatToolBConstants.PayloadKeys.FIELDS, convertListToString(listFieldsPopulatedForReporterTab));
			isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			if(!isTabFeedSuccess)
			{
				closeWriterDriver();
				return;				
			}
			
			
			/**
			 * D.E - Case Note updated
			 */
			/*caseNote = "Data Entry Completed for Cased Id, Patient, Drug, Reporter Tabs";
			caseNotesMap.put(SceptreConstants.CASENOTE_CUSTOMNOTE, caseNote);
			oAppWriter.updateCaseNote(caseNotesMap);
			payload.clear();
			payload.put(CpatToolBConstants.Payload.TAB, CpatToolBConstants.Tabs.CASENOTES);
			payload.put(CpatToolBConstants.Payload.FIELDS, caseNote);
			isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolBConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolBConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			if(!isTabFeedSuccess)
			{
				return;				
			}*/
			
			
			/**
			 * D.E Summary
			 */
			tab="SUMMARY";
			if(codingStatusFailKeys != null)
			{
				for(String key: codingStatusFailKeys.keySet())
				{
					codingFail.append(key + "-" + codingStatusFailKeys.get(key) + ";");
				}
			}
			
			addSdList = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.ADD_SDLIST);
			pendingOtherIds = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.PENDING_OTHER_IDENTIFICATION_NOS);
			pendingLinkedCaseNos = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.PENDING_LINKED_CASE_NOS);
			reactionKeysFound = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.REACTION_KEYWORD_FOUND);
			pendingDrugAdditionalInfo = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.PENDING_DRUG_ADDITIONALINFO);
			
			payload.clear();
			payload.put(CpatToolBConstants.PayloadKeys.QUEUE, oToolBase.userInputsParams.get(CpatToolBConstants.CaseDetails.CASE_QUEUE));
			payload.put(CpatToolBConstants.PayloadKeys.ACTIVITY_ID, String.valueOf(oToolBase.activityId));
			payload.put(CpatToolBConstants.PayloadKeys.CODING_STATUS_FAIL, codingFail.toString());
			
			payload.put(CpatToolBConstants.PayloadKeys.ADDITIONAL_SD_LIST, addSdList);
			payload.put(CpatToolBConstants.PayloadKeys.PENDING_OTHERIDS, pendingOtherIds);
			payload.put(CpatToolBConstants.PayloadKeys.PENDING_LINKED_CASENOS, pendingLinkedCaseNos);
			payload.put(CpatToolBConstants.PayloadKeys.REACTION_KEYWORDS, reactionKeysFound);
			payload.put(CpatToolBConstants.PayloadKeys.PENDING_DRUG_ADDITIONALINFO, pendingDrugAdditionalInfo);
			
			isTabFeedSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY_SUMMARY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY_SUMMARY, "", payload);
			if(!isTabFeedSuccess)
			{
				closeWriterDriver();
				return;				
			}

			
			// Prepare Result Screen
			dataEntryDoneMap.put(CpatToolBConstants.Tabs.CASE_ID, listFieldsPopulatedForCaseIdTab);
			dataEntryDoneMap.put(CpatToolBConstants.Tabs.PATIENT, listFieldsPopulatedForPatientTab);
			dataEntryDoneMap.put(CpatToolBConstants.Tabs.MEDICAL_HISTORY, listFieldsPopulatedForMedicalHistoryTab);
			dataEntryDoneMap.put(CpatToolBConstants.Tabs.DRUG_HISTORY, listFieldsPopulatedForDrugHistoryTab);
			dataEntryDoneMap.put(CpatToolBConstants.Tabs.REACTION, listFieldsPopulatedForReactionTab);
			dataEntryDoneMap.put(CpatToolBConstants.Tabs.DRUG, listFieldsPopulatedForDrugTab);
			dataEntryDoneMap.put(CpatToolBConstants.Tabs.REPORTER, listFieldsPopulatedForReporterTab);
			
			notesList.add("�Check if either Company, Non Company Co Suspect, Concomitant products are available in Source Document�");

			resultScreenMap.put(CpatToolConstants.ResultScreen.DATAENTRY_DONE_LIST, dataEntryDoneMap);
			resultScreenMap.put(CpatToolConstants.ResultScreen.LOOKUP_FAILED_STATUS, codingStatusFailKeys);
			
			resultScreenMap.put(CpatToolConstants.ResultScreen.NOTES_LIST, notesList);
			
			html = generateHtmlForResultScreen(resultScreenMap);
			
			for(String tabKeys: sceptreTabValues.keySet())
			{
				prevSceptreValMap.putAll(sceptreTabValues.get(tabKeys)); 
			}
			
			params.put(CpatToolConstants.ContentParams.RESULT_SCREEN_DATA, html);
			params.put(CpatToolConstants.ContentParams.DE_FIELDLIST, dataEntryDoneMap);
			params.put(CpatToolConstants.ContentParams.SCEPTRE_MAP, p_parsedDataMap);
			params.put(CpatToolConstants.ContentParams.PRV_VAL_SCEPTRE_MAP, prevSceptreValMap);
			params.put(CpatToolConstants.ContentParams.WRITER_INSTANCE, oAppWriter);
			showResultScreen(oToolBase, oCallbackAction, params);
			
			log.info("Case Feed Done!");
			
		}catch(Exception e){
			log.error("Data Entry Failed for TAB: " + tab, null, e);
			payload.clear();
			payload.put("USER_MSG", "Data Entry Failed for TAB: " + tab);
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			
			JOptionPane.showMessageDialog(null, "Data Entry Failed for TAB: " + tab);
			closeWriterDriver();
		}
		
	}
	
	public static void showResultScreen(CpatToolBase p_oToolBase, IAction p_oCallbackAction, Map<String, Object> params) 
	{
		new CpatResultScreen(p_oToolBase, p_oCallbackAction, params);
		CpatCasePreviewScreen.displayPreviewScreenView();
	}

	public static void performDuplicateSearch(CpatToolBase oToolBase, IAction oCallbackAction, ParsedDataTable summaryDataTable, Map<String, String> sceptreMap)
	{
		log.info("Perform duplicate search");
		String caseId = null;
		String caseVersion = null;
		String localCaseId = null;
	
		String caseProductName = null;
		
		Map<String, String> payload = null;
		Map<String, Object> params = null;
		
		final String defaultCaseVersion = "0";

		List<Criterion> listCriteria = null;
		Map<SceptreConstants.SceptreSearchFields, Object> mapCriteria = null;

		SearchResults oResults = null;
		Map<String, ParsedDataTable> aerResults = null;
		
		List<Map<String, String>> dupSummaryList = null; 
		List<String> potentialDuplicateAerNoList = null;
		
		ParsedDataTable dsSummaryDataTable = null;
		
		Map<String, String> userInputMap = null;
		
		List<CaseDocument> listCaseDocument = null;

		boolean isDuplicateCase = false;
		boolean takeOwnership = false;
		
		payload = new HashMap<>();
		params = new HashMap<>();
				
		aerResults = new HashMap<String, ParsedDataTable>();
		dupSummaryList = new ArrayList<Map<String, String>>();
		
		mapCriteria = new HashMap<SceptreConstants.SceptreSearchFields, Object>();	
		
		try
		{
			userInputMap = oToolBase.userInputsParams;
			caseId = userInputMap.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID);
			localCaseId = userInputMap.get(CpatToolBConstants.CaseDetails.LOCAL_CASE_ID);
			caseVersion = userInputMap.get(CpatToolBConstants.CaseDetails.CASE_VERSION);

			mapCriteria.put(SceptreConstants.SceptreSearchFields.LOCAL_CASE_ID, "*"+localCaseId+"*");
			mapCriteria.put(SceptreConstants.SceptreSearchFields.SHOW_DELETED_CASES, "yes");
			
			oAppWriter.prepareForSearch();
			oResults = oAppWriter.search(mapCriteria);
			
			caseProductName = getSuspectProductNameFromSearchResults(oResults, caseId, caseVersion);
			
			if(caseProductName != null)
			{
				potentialDuplicateAerNoList = getPotentialDuplicateAerNumbers(caseId, defaultCaseVersion, caseProductName, mapCriteria, oResults, sceptreMap, summaryDataTable);
				
				if(potentialDuplicateAerNoList.size() == 0)
				{
					isDuplicateCase = false;
				}
				else
				{	
					for(String potentialDupAer: potentialDuplicateAerNoList)
					{
						oAppWriter.clearSearch();
						
						oAppWriter.locateCase(potentialDupAer);
						oAppWriter.getLocalCaseId(potentialDupAer, defaultCaseVersion, takeOwnership);
						
						listCriteria = new ArrayList<Criterion>();
						listCriteria.add(Criterion.create(Operator.CONTAINS, SceptreConstants.DownloadSD.DOCUMENT_NAME, CpatToolBConstants.DownloadSD.E2B_DOCUMENT_NAME_VALUE));
						listCriteria.add(Criterion.create(Operator.MATCHES, SceptreConstants.DownloadSD.DOCUMENT_TYPE, CpatToolBConstants.DownloadSD.DOCUMENT_TYPE_VALUE));
						
						listCaseDocument = oAppWriter.downloadSourceDocument(listCriteria);
						
						if(!listCaseDocument.isEmpty())
						{
							dsSummaryDataTable = getSummaryDataTableFromCase(listCaseDocument);
							aerResults.put(potentialDupAer, dsSummaryDataTable);	
							
						}
						
						oAppWriter.prepareForSearch();
					}
					
					dupSummaryList.add(getDupSummaryMap(String.valueOf(userInputMap.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID)), summaryDataTable));
					for (String key: aerResults.keySet())
					{
						dupSummaryList.add(getDupSummaryMap(key, aerResults.get(key)));
					}
				}
				
				params.put(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE, summaryDataTable);
				params.put(CpatToolConstants.ContentParams.SCEPTRE_MAP, sceptreMap);
				params.put(CpatToolConstants.ContentParams.DUP_SUMMARYLIST, dupSummaryList);
				params.put(CpatToolConstants.ContentParams.ISDUPLICATE, isDuplicateCase);
				
				CpatSearchStatusScreen oSearchScreen = new CpatSearchStatusScreen(oToolBase, oCallbackAction, params);
				
			}
			else
			{
				JOptionPane.showMessageDialog(null, "No Results Found.");
				payload.put("USER_MSG", "No Results Found");
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
				
				closeWriterDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
			}
			
		
		}catch(Exception excp)
		{
			log.error("Could not perform Duplicate Search: ", null, excp);
			
			JOptionPane.showMessageDialog(null, "Could not perform Duplicate Search. Please check logs");
			payload.put("USER_MSG", "Could not perform Duplicate Search. Please check logs");
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
			
			closeWriterDriver();
			oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
		}
			
	}

	private static Map<String, String> transformSceptreSearchValMap(
			Map<String, Map<String, Object>> sceptreSearchValMap) 
	{
		Map<String, String> map = null;
		
		map = new HashMap<>();
		
		if(sceptreSearchValMap != null)
		{
			for(String tabKey: sceptreSearchValMap.keySet())
			{
				for(String key: sceptreSearchValMap.get(tabKey).keySet())
				{
					map.put(key, String.valueOf(sceptreSearchValMap.get(tabKey).get(key)));
				}
			}
						
		}
		
		return map;
	}

	public static Map<String, String> getDupSummaryMap(String aerNo, ParsedDataTable summaryDataTable) 
	{

		Map<String, String> dupSummaryMap = new LinkedHashMap<String, String>();
		
		String reportedDrugName = null;
		String patientInitials = null;
		String patientDob= null;
		String patientGender = null;
		String primarySourceCountry = null;
		String patientOnsetAge = null;
		String reporterGivenName = null;
		String reporterFamilyName = null;
		
		// Reported Drug Name
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_MEDICIANL_PRODUCT_NAME}))
		{
			reportedDrugName = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_MEDICIANL_PRODUCT_NAME).get(0).displayValue());
		}
		else if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCTNAME}))
		{
			reportedDrugName = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCTNAME).get(0).displayValue());
		}
		else
		{
			reportedDrugName = StringConstants.EMPTY;
		}
		
		// Patient Initials
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_INITIALS}))
		{
			patientInitials = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_INITIALS).get(0).displayValue());
		}
		else if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_INITIALS}))
		{
			patientInitials = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_INITIALS).get(0).displayValue());
		}
		else
		{
			patientInitials = StringConstants.EMPTY;
		}
		
		// Patient DOB
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_DATE_OF_BIRTH}))
		{
			patientDob = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_DATE_OF_BIRTH).get(0).displayValue());
		}
		else if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_DOB}))
		{
			patientDob = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_DOB).get(0).displayValue());
		}
		else
		{
			patientDob = StringConstants.EMPTY;
		}
		
		// Patient Gender
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_GENDER}))
		{
			patientGender = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_GENDER).get(0).displayValue());
		}
		else if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_GENDER}))
		{
			patientGender = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_GENDER).get(0).displayValue());
		}
		else
		{
			patientGender = StringConstants.EMPTY;
		}
		
		// Primary Source Country
		if(summaryDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY))
		{
			primarySourceCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY).get(0).displayValue());
		}
		else if(summaryDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY))
		{
			primarySourceCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY).get(0).displayValue());
		}
		else
		{
			primarySourceCountry = StringConstants.EMPTY;
		}
		
		// Patient On Set Age
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_ONSET_AGE}))
		{
			patientOnsetAge = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_ONSET_AGE).get(0).displayValue());
		}
		else
		{
			patientOnsetAge = StringConstants.EMPTY;
		}
		
		// Reporter Given Name
		if(summaryDataTable.isColumnPresent(CpatToolBConstants.ReporterTabKeys.REPORTER_GIVEN_NAME))
		{
			reporterGivenName = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.REPORTER_GIVEN_NAME).get(0).displayValue());
		}
		else
		{
			reporterGivenName = StringConstants.EMPTY;
		}
		
		// Reporter Family Name
		if(summaryDataTable.isColumnPresent(CpatToolBConstants.ReporterTabKeys.REPORTER_FAMILY_NAME))
		{
			reporterFamilyName = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.REPORTER_FAMILY_NAME).get(0).displayValue());
		}
		else
		{
			reporterFamilyName = StringConstants.EMPTY;
		}
		
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.AER, aerNo);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.PRODUCT_NAME, reportedDrugName);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.PATIENT_INITIALS, patientInitials);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.PATIENT_DOB, patientDob);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.PATIENT_GENDER, patientGender);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.PRIMARY_SOURCE_COUNTRY, primarySourceCountry);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.PATIENT_ONSET_AGE, patientOnsetAge);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.REPORTER_GIVENNAME, reporterGivenName);
		dupSummaryMap.put(CpatToolBConstants.DupSummaryKeys.REPORTER_FAMILYNAME, reporterFamilyName);
		
		return dupSummaryMap;
	}

	private static ParsedDataTable getSummaryDataTableFromCase(List<CaseDocument> listCaseDocument) throws SciException, SciServiceException
	{
		List<PreviewCaseDoc> listParsedCaseDocs = null;
		ParsedDataTable dsSummaryDataTable = null;
		ParsedDataTable oDataTable = null;
		File file = null;
		String caseDocVersion = null;
						
		listParsedCaseDocs = new ArrayList<>();
		
		for(int i = 0; i < listCaseDocument.size(); i++)
		{
			file = listCaseDocument.get(i).getPath();
			oDataTable = new PdfDocParserAvatarAA(file.getAbsolutePath()).parse();
			caseDocVersion = String.valueOf(listCaseDocument.get(i).getoAttributeMap().get(SceptreConstants.DownloadSD.CASE_VERSION));
			
			listParsedCaseDocs.add(new PreviewCaseDoc(file, oDataTable, caseDocVersion));
		}			
		
		dsSummaryDataTable = mergeAllCaseDocs(listParsedCaseDocs);
		
		return dsSummaryDataTable;
	}


	public static void logout()
	{
		oAppWriter.logout();
	}

		
	public static void validateMapValues(CpatToolBase oToolBase, List<PreviewCaseDoc> listParsedCaseDocs, Map<String, String> parsedDataMap, ParsedDataTable summaryDataTable , Map<String, String> p_WhiteListMap) throws SciException
	{	
		fupNewFieldsMnemonicList = getFollowUpMnemonicList(listParsedCaseDocs, oToolBase.userInputsParams.get(CpatToolBConstants.CaseDetails.CASE_VERSION));
	
		if(caseType.equals(CpatToolBConstants.CaseType.INITIAL))
		{
			validateCaseIdTab(oToolBase, parsedDataMap, summaryDataTable, listParsedCaseDocs);
		}
		else
		{
			validateCaseIdTabForFollowup(oToolBase, parsedDataMap, summaryDataTable, listParsedCaseDocs);
		}
		validateCaseIdTabForCommon(oToolBase, parsedDataMap, summaryDataTable);
		validatePatientTab(oToolBase, parsedDataMap, summaryDataTable);
		validationForMedicalHistory(oToolBase, parsedDataMap, summaryDataTable);
		//validationForDrugHistory(oToolBase, parsedDataMap, summaryDataTable);
		validateDrugTab(oToolBase, parsedDataMap, summaryDataTable);
		validateReactionTab(oToolBase, parsedDataMap, summaryDataTable);
		validateReportedTab(oToolBase, parsedDataMap, summaryDataTable);
		validateDerivedFields(oToolBase, parsedDataMap, summaryDataTable, p_WhiteListMap);
		
		handleSceptreOverridingFields(oToolBase, parsedDataMap);
	}

	
	private static void validateDerivedFields(CpatToolBase oToolBase, Map<String, String> parsedDataMap,
			ParsedDataTable summaryDataTable, Map<String, String> p_WhiteListMap) 
	{
		Map<String, String> valuesForDateDifference = null;

		String sReactionStartDateField = null;
		String sPatientDobField = null;

		valuesForDateDifference = new HashMap<>();
		/********code by shambhu******/
		// Patient Onset age
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE}))
		{
			sReactionStartDateField = CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE;
		}
		else
		{
			sReactionStartDateField = CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE;
		}
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_DATE_OF_BIRTH}))
		{
			sPatientDobField = CpatToolBConstants.PatientTabKeys.PATIENT_DATE_OF_BIRTH;
		}
		else
		{
			sPatientDobField = CpatToolBConstants.NarrativeKeys.PATIENT_DOB;
		}
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{sPatientDobField, sReactionStartDateField}))
		{
			valuesForDateDifference = getDifferenceBetweenDate(
					String.valueOf(
							summaryDataTable.getColumnValues(sPatientDobField)
									.get(0).displayValue()),
					String.valueOf(
							summaryDataTable.getColumnValues(sReactionStartDateField)
									.get(0).displayValue()));
		
			if(isKeyContainsValueInMap(valuesForDateDifference, new String[]{CpatToolBConstants.DateDifference.YEARS}))
			{
				parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE, valuesForDateDifference.get(CpatToolBConstants.DateDifference.YEARS));
				parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE_UNIT, CpatToolBConstants.DateDifference.YEAR);
			}
			else if(isKeyContainsValueInMap(valuesForDateDifference , new String[]{CpatToolBConstants.DateDifference.MONTHS}))
			{
				parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE, valuesForDateDifference.get(CpatToolBConstants.DateDifference.MONTHS));
				parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE_UNIT, CpatToolBConstants.DateDifference.MONTH);
			}
			else if(isKeyContainsValueInMap(valuesForDateDifference , new String[]{CpatToolBConstants.DateDifference.DAYS}))
			{
				parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));
				parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE_UNIT, CpatToolBConstants.DateDifference.DAY);
				
			}
		}
		
		
		// Age group
		if(isKeyContainsValueInMap(valuesForDateDifference , new String[]{CpatToolBConstants.DateDifference.YEARS}))
		{
			int nDays = 0;
			
			nDays = getDaysFromYear(Integer.parseInt(valuesForDateDifference.get(CpatToolBConstants.DateDifference.YEARS)));

			parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE_GROUP, CpatToolBConstants.AgeGroup.getAgeGroup(nDays).name());
		}
		else if(isKeyContainsValueInMap(valuesForDateDifference , new String[]{CpatToolBConstants.DateDifference.MONTHS}))
		{
			int nDays = 0;
			
			nDays = getDaysFromMonth(Integer.parseInt(valuesForDateDifference.get(CpatToolBConstants.DateDifference.MONTHS)));

			parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE_GROUP, CpatToolBConstants.AgeGroup.getAgeGroup(nDays).name());
			
		}
		else if(isKeyContainsValueInMap(valuesForDateDifference , new String[]{CpatToolBConstants.DateDifference.DAYS}))
		{
			parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE_GROUP, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));			
		}
		else
		{
			parsedDataMap.put(SceptreConstants.PatientTabKeys.AGE_GROUP, "Unknown");
		}
		
		
		// REACTION TAB: Drug Administration to Start of this Reaction
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_FIRST_TIME}))
		{
			String[] sarr_DASRDruationWithUnit = null;
			
			sarr_DASRDruationWithUnit = getSeparatedDuration(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReactionTabKeys.REACTION_FIRST_TIME).get(0).displayValue()));
			
			if(sarr_DASRDruationWithUnit != null)
			{
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION, sarr_DASRDruationWithUnit[0]);
				
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT, sarr_DASRDruationWithUnit[1]);
			}
		}
		else 
		{
			if(parsedDataMap.containsKey(SceptreConstants.DrugTabKeys.DOSAGE_START) && parsedDataMap.containsKey(SceptreConstants.ReactionTabKeys.REACTION_STARTED))
			{
				valuesForDateDifference = getDifferenceBetweenDate(parsedDataMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED), parsedDataMap.get(SceptreConstants.DrugTabKeys.DOSAGE_START));
				if(valuesForDateDifference.containsKey(CpatToolBConstants.DateDifference.DAYS))
				{
					parsedDataMap.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));
					parsedDataMap.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT, CpatToolBConstants.ReactionTabKeys.DAYS);
				}
			}
		}
		
		// REACTION TAB: Last Dose to Start of This Reaction
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_LAST_TIME}))
		{
			String[] sarr_LDSRDruationWithUnit = null;
			
			sarr_LDSRDruationWithUnit = getSeparatedDuration(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReactionTabKeys.REACTION_LAST_TIME).get(0).displayValue()));
		
			if(sarr_LDSRDruationWithUnit != null)
			{
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION, sarr_LDSRDruationWithUnit[0]);
				
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT, sarr_LDSRDruationWithUnit[1]);
			}
		}
		else 
		{
			if(parsedDataMap.containsKey(SceptreConstants.DrugTabKeys.DOSAGE_END) && parsedDataMap.containsKey(SceptreConstants.ReactionTabKeys.REACTION_STARTED))
			{
				valuesForDateDifference = getDifferenceBetweenDate(parsedDataMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED), parsedDataMap.get(SceptreConstants.DrugTabKeys.DOSAGE_END));
				if(valuesForDateDifference.containsKey(CpatToolBConstants.DateDifference.DAYS))
				{
					parsedDataMap.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));
					parsedDataMap.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT, CpatToolBConstants.ReactionTabKeys.DAYS);
				}
			}
		}
	}


	/**
	 * Validate Patient Tab
	 * @param oToolBase
	 * @param parsedDataMap
	 * @param summaryDataTable
	 */
	private static void validatePatientTab(CpatToolBase oToolBase, Map<String, String> sceptreMap,
			ParsedDataTable summaryDataTable) 
	{
		String countryICU = null , sGender = null, sCountryPrimarySource = null;
		
		String dob = null;
		
		/*date:4/5/2018*/
		
		// Primary Source Country
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY}))
		{ 
			sCountryPrimarySource  = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY).get(0).displayValue());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY}))
		{
			sCountryPrimarySource = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY).get(0).displayValue());
		}
				
		if(!StringUtils.isNullOrEmpty(sCountryPrimarySource))
		{
			countryICU = getICU(getCountries(), sCountryPrimarySource);
			
			if(!StringUtils.isNullOrEmpty(countryICU))
			{
				sceptreMap.put(SceptreConstants.Name.ICU, countryICU);
			}
			
			if(countryICU.equalsIgnoreCase("JJCEMEA"))
			{
				sceptreMap.put(SceptreConstants.PatientTabKeys.PATIENT_ID, "Private");
			}
			else
			{
				if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_INITIALS}))
				{
					sceptreMap.put(SceptreConstants.PatientTabKeys.PATIENT_ID, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_INITIALS).get(0).displayValue()));
				}
				else if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_INITIALS}))
				{
					sceptreMap.put(SceptreConstants.PatientTabKeys.PATIENT_ID, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_INITIALS).get(0).displayValue()));
				}
			}
		}
		
		
		// Patient Gender
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_GENDER}))
		{
			sGender = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_GENDER).get(0).displayValue());
		
			
			if(genderIsNotInList(sGender))
			{
				//do nothing
			}
			else if(sGender.equalsIgnoreCase("M"))
			{
				sGender = "Male";
			}
			else if(sGender.equalsIgnoreCase("F"))
			{
				sGender = "Female";
			}
			else
			{
				sGender = "Unknown";
			}
			sceptreMap.put(SceptreConstants.PatientTabKeys.PATIENT_SEX, sGender);
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_GENDER}))
		{
			sGender =  String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_GENDER).get(0).displayValue());
			
			if(genderIsNotInList(sGender))
			{
				//do nothing
			}
			else if(sGender.equalsIgnoreCase("M"))
			{
				sGender = "Male";
			}
			else if(sGender.equalsIgnoreCase("F"))
			{
				sGender = "Female";
			}
			else
			{
				sGender = "Unknown";
			}
			sceptreMap.put(SceptreConstants.PatientTabKeys.PATIENT_SEX, sGender);
		}
		else
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.PATIENT_SEX, "Unknown");
		}
		
		// Patient Height
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_HEIGHT}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.HEIGHT, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_HEIGHT).get(0).displayValue()));
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_HEIGHT}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.HEIGHT, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_HEIGHT).get(0).displayValue()));
		}
	
		
		// Patient Height Unit
		splitHeightAppendUnit(sceptreMap, SceptreConstants.PatientTabKeys.HEIGHT);
		
		// Patient Weight
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_WEIGHT}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.WEIGHT, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_WEIGHT).get(0).displayValue()));
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_WEIGHT}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.WEIGHT, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_WEIGHT).get(0).displayValue()));
		}
		
		
		// Patient Wieght Unit
		splitWeightAppendUnit(sceptreMap, SceptreConstants.PatientTabKeys.WEIGHT);
		
		// Patient DOB
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_DATE_OF_BIRTH}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_DATE_OF_BIRTH).get(0).displayValue()));
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_DOB}))
		{
			dob = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_DOB).get(0).displayValue());
			if(!checkIfCellContainsDateValue(dob, CpatToolBConstants.DATEFORMATS.DF_TRANSMISSION_DATE))
			{
				Date oDate = null;
				
				oDate = parse(dob);
				
				if(oDate == null)
				{
					JOptionPane.showMessageDialog(null, "Date formate for DOB not supported");
					
					closeWriterDriver();
				}
				else
				{
					dob = new SimpleDateFormat(CpatToolBConstants.DATEFORMATS.DF_TRANSMISSION_DATE).format(parse(dob));
				}
			}
			sceptreMap.put(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH, dob);
		}
		
		
		// Patient Onset Age
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_ONSET_AGE}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.AGE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_ONSET_AGE).get(0).displayValue()));
		}
		
		
		// Patient Age Group
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_AGE_GROUP}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.AGE_GROUP, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_AGE_GROUP).get(0).displayValue()));
		}
		
		//patient LMP date
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_LMP_DATE}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_LMP_DATE).get(0).displayValue()));
		}
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.NARRATIVE_PERMISSION_TO_CONTACT}))
		{
			sceptreMap.put(SceptreConstants.PatientTabKeys.PERMISSION_TO_CONTACT_PHYSICIAN, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.NARRATIVE_PERMISSION_TO_CONTACT).get(0).displayValue()));
		}
	}


	/**
	 * Validate Case Id Tab
	 * @param oToolBase
	 * @param sceptreMap
	 * @param summaryDataTable
	 * @param listParsedCaseDocs 
	 */
	private static void validateCaseIdTab(CpatToolBase oToolBase, Map<String, String> sceptreMap,
			ParsedDataTable summaryDataTable, List<PreviewCaseDoc> listParsedCaseDocs) 
	{
		String sPrimarySourceCountry = null , sOccureCountry = null, sFirstTransmissionDate = null;
		
		sFirstTransmissionDate = getFirstDocValOfCurrentVersion(oToolBase, listParsedCaseDocs, CpatToolBConstants.CaseIdTabKeys.TRANSMISSION_DATE);
		
		// Primary Source Country
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY}))
		{ 
			sceptreMap.put(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY).get(0).displayValue()));
			sPrimarySourceCountry  = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY).get(0).displayValue());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY}))
		{
			sceptreMap.put(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY).get(0).displayValue()));
			sPrimarySourceCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY).get(0).displayValue());
		}
		
		// Occur Country
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.CaseIdTabKeys.OCCUR_COUNTRY}))
		{
			//sceptreMap.put(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.OCCUR_COUNTRY).get(0).displayValue()));
			sOccureCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.OCCUR_COUNTRY).get(0).displayValue());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.EVENT_COUNTRY_OCCURED}))
		{
			//sceptreMap.put(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.EVENT_COUNTRY_OCCURED).get(0).displayValue()));
			sOccureCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.EVENT_COUNTRY_OCCURED).get(0).displayValue());
		}
		
		if(!StringUtils.isNullOrEmpty(sOccureCountry))
		{
			if(!StringUtils.isNullOrEmpty(sPrimarySourceCountry))
			{
				if(!sPrimarySourceCountry.trim().equalsIgnoreCase(sOccureCountry.trim()))
				{
					sceptreMap.put(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED, sOccureCountry.trim()); 
				}
			}
			else
			{
				sceptreMap.put(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED, sOccureCountry.trim());
			}
		}
		// Case Priority
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY, "5");
		
		// Received by case processing centre
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER, sFirstTransmissionDate);
		
		// Type Of Report
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.CaseIdTabKeys.REPORT_TYPE}))
		{
			sceptreMap.put(SceptreConstants.CaseIdTabKeys.REPORT_TYPE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.REPORT_TYPE).get(0).displayValue()));
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.REPORT_TYPE}))
		{
			sceptreMap.put(SceptreConstants.CaseIdTabKeys.REPORT_TYPE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.REPORT_TYPE).get(0).displayValue()));
		}
		
		// Regulatory Relevant update
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE, "Yes");
		
	}
	
	
	private static void validateCaseIdTabForCommon(CpatToolBase oToolBase, Map<String, String> sceptreMap,
			ParsedDataTable summaryDataTable) 
	{
		//Other identification number from parsed dataTable(summaryDataTable)
		getSetOFOtherIDNumberFromParsedData(summaryDataTable);
		
		//linked cases from from parsed dataTable(summaryDataTable)
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.OtherIdentificationNumber.LINKED_REPORT_NO}))
		{
			setOfLinkedCasesFromSD = getSetOfValuesOfGivenLineByCommaSeparator(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.OtherIdentificationNumber.LINKED_REPORT_NO).get(0).displayValue()));
		}

		// Classification
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.ADVERSEEVENT, "Yes");
	}
	
	
	private static void validateCaseIdTabForFollowup(CpatToolBase oToolBase, Map<String, String> sceptreMap,
			ParsedDataTable summaryDataTable, List<PreviewCaseDoc> listParsedCaseDocs) 
	{
		String firstDateOfRecentInfo = null;

		String firstReceivedDate = null;

		String activityCategory = null;

		String latestActivityAwarnessDate = null;

		String versionInitiallyReceivedDate = null;

		String firstTransmissionDate = null;

		List<String> activityCategoryList = null;
		
		activityCategoryList = Arrays.asList(propMap.get(CpatToolBConstants.PropertiesMap.ACTIVITY_CATEGORY_LIST).split(StringConstants.COMMA));
		
		firstDateOfRecentInfo = getFirstDocValOfCurrentVersion(oToolBase, listParsedCaseDocs, CpatToolBConstants.CaseIdTabKeys.DATE_OF_RECENT_INFO);
		firstTransmissionDate = getFirstDocValOfCurrentVersion(oToolBase, listParsedCaseDocs, CpatToolBConstants.CaseIdTabKeys.TRANSMISSION_DATE);
		
		
		// Version Initially Received Date
		if(summaryDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.FIRST_RECEIVED_DATE))
		{
			firstReceivedDate = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.FIRST_RECEIVED_DATE).get(0).displayValue());			
		}
		
		versionInitiallyReceivedDate = firstDateOfRecentInfo;
		
		if(firstDateOfRecentInfo.equals(firstReceivedDate))
		{
			if(summaryDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.ACTIVITY_CATEGORY))
			{
				activityCategory = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.ACTIVITY_CATEGORY).get(0).displayValue());

				for(String cat: activityCategoryList)
				{
					if(activityCategory.equalsIgnoreCase(cat))
					{
						if(summaryDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.AWARENESS_DATE))
						{
							latestActivityAwarnessDate = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.AWARENESS_DATE).get(0).displayValue());
							versionInitiallyReceivedDate = latestActivityAwarnessDate;
						}
					}
				}
			}
		}
		
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED, versionInitiallyReceivedDate);
		
		//ICH Expedited
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START, versionInitiallyReceivedDate);
		
		//Received by local safety unit
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT, firstDateOfRecentInfo);
		
		//Recieve by case proccesing centre
		sceptreMap.put(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER, firstTransmissionDate);
	}
	
	
	/******************code by shambhu vanzara*****************************************/
	private static boolean isKeyContainsNonAcceptableValue(ParsedDataTable p_SummaryDataTable , String[] p_sKeys)
	{
		boolean isContainsValue = false;
		
		String sValue = null;
		
		int nTrueCount = 0 , nKeyCount = 0;
		
		nKeyCount = p_sKeys.length;
		
		
		for(String sKey : p_sKeys)
		{
			log.info("current Key is " +sKey);
			
			if(p_SummaryDataTable.isColumnPresent(sKey))
			{
				log.info("current Key is " +sKey + "Preset");
				
				sValue = p_SummaryDataTable.getColumnValues(sKey).get(0).displayValue().toString();
				
				if (!StringUtils.isNullOrEmpty(sValue) && !sValue.equalsIgnoreCase("None")
						&& !sValue.equalsIgnoreCase("No") && !sValue.equalsIgnoreCase("NA")
						&& !sValue.equalsIgnoreCase("N/A") && !sValue.equalsIgnoreCase("unknown")
						&& !sValue.equalsIgnoreCase("Not Applicable") && !sValue.equalsIgnoreCase("NI"))				
				{
					nTrueCount++;
				}
			}
		}
		
		if(nTrueCount == nKeyCount)
		{
			isContainsValue = true;
		}
		
		return isContainsValue;
	}
	
	private static boolean isKeyContainsValue(ParsedDataTable p_SummaryDataTable , String[] p_sKeys)
	{
		boolean isContainsValue = false;
		
		String sValue = null;
		
		int nTrueCount = 0 , nKeyCount = 0;
		
		nKeyCount = p_sKeys.length;
		
		
		for(String sKey : p_sKeys)
		{
			log.info("current Key is " +sKey);
			
			if(p_SummaryDataTable.isColumnPresent(sKey))
			{
				log.info("current Key is " +sKey + "Preset");
				
				sValue = p_SummaryDataTable.getColumnValues(sKey).get(0).displayValue().toString();
				
				if (!StringUtils.isNullOrEmpty(sValue))
				{
					nTrueCount++;
				}
			}
		}
		
		if(nTrueCount == nKeyCount)
		{
			isContainsValue = true;
		}
		
		return isContainsValue;
	}
	
	/******************code by shambhu vanzara*****************************************/
	private static boolean isKeyContainsValueInMap(Map<String ,String> p_oDataTableMap , String[] p_sKeys)
	{
		boolean isContainsValue = false;
		
		int nTrueCount = 0 , nKeyCount = 0;
		
		nKeyCount = p_sKeys.length;
		
		if(p_oDataTableMap != null)
		{
			for(String sKey : p_sKeys)
			{
				log.info("current Key is " +sKey);
				
				if(p_oDataTableMap.containsKey(sKey))
				{
					log.info("current Key is " +sKey + "Preset");
					
					if(!StringUtils.isNullOrEmpty(p_oDataTableMap.get(sKey)))
					{
						nTrueCount++;
					}
				}
			}
			
		}
		if(nTrueCount == nKeyCount)
		{
			isContainsValue = true;
		}
		
		return isContainsValue;
	}
	
	//common for follow-up and initial as well for validating and deriving sceptre fields values
	public static void validateDrugTabCommon(CpatToolBase oToolBase, Map<String, String> parsedDataMap, ParsedDataTable summaryDataTable , Map<String, String> p_ProductWhiteList) throws SciException
	{
		String sReactionStartDate = null , sProductStopDate= null , sSuspect = null;
		
		//Since only Suspect Products are in scope, default this field to "Company Drug"
	//	parsedDataMap.put(SceptreConstants.DrugTabKeys.MADEBY, "Yes");	
		//Hardcode to "Suspect"
		sSuspect = SceptreConstants.DrugTabKeys.CC_SUSPECT;
		
		parsedDataMap.put(SceptreConstants.DrugTabKeys.CHARACTERIZATION, sSuspect);	
		//Hardcode to "1"
		parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_RANK, "1");
		//Hardcode always checked
		parsedDataMap.put(SceptreConstants.DrugTabKeys.FIRST_REACTION, "Yes");
		//Hardcode to "1"
		parsedDataMap.put(SceptreConstants.DrugTabKeys.INDICATION_RANK, "1");
		//hardcode to "1"
		parsedDataMap.put(SceptreConstants.DrugTabKeys.BATCH_RANK, "1");
		
		
		//business logic for Product name
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_MEDICIANL_PRODUCT_NAME}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_MEDICIANL_PRODUCT_NAME).get(0)
							.displayValue().toString());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCTNAME}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCTNAME).get(0)
							.displayValue().toString());
		}
		
		
		//business logic for route of admin
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_ROUTE_OF_ADMINISTRATION}))
		{
			log.info("contains value "+CpatToolBConstants.DrugTabKeys.DRUG_ROUTE_OF_ADMINISTRATION);
			if(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_ROUTE_OF_ADMINISTRATION).get(0).displayValue().toString().equalsIgnoreCase("none"))
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.ROUTEOFADMIN, "TOPICAL");
			}
			else
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.ROUTEOFADMIN,
						summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_ROUTE_OF_ADMINISTRATION).get(0)
						.displayValue().toString());
			}
			
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION}))
		{
			log.info("contains value "+CpatToolBConstants.DrugTabKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION);

			if(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION).get(0).displayValue().toString().equalsIgnoreCase("none"))
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.ROUTEOFADMIN, "TOPICAL");
			}
			else
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.ROUTEOFADMIN,
						summaryDataTable
						.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION)
						.get(0).displayValue().toString());
			}
			
			log.info("Route of admin from Narrative "+summaryDataTable
							.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION)
							.get(0).displayValue().toString());
		}
		else
		{
			log.info("Else part of route of Admin");
			parsedDataMap.put(SceptreConstants.DrugTabKeys.ROUTEOFADMIN, "TOPICAL");
		}
		
		
		//business logic for Drug Duration
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DURATION_OF_DRUG_ADMIN}))
		{
			String[] sarrValueAndunit = null;
			log.info("Drug duration" +CpatToolBConstants.DrugTabKeys.DRUG_DURATION_OF_DRUG_ADMIN);
			
			sarrValueAndunit = getSeparatedDuration(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_DURATION_OF_DRUG_ADMIN).get(0).displayValue()));
			if(sarrValueAndunit != null)
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.DRUG_DURATION, sarrValueAndunit[0]);		
				parsedDataMap.put(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT, sarrValueAndunit[1]);		
			}

		}
		else
		{
			Map<String , String > valuesForDateDifference = null ;
			String sProductStartDate = null;
			String sProductEndDate = null;
			
			valuesForDateDifference = new HashMap<>();
		
			log.info("Drug start date "+CpatToolBConstants.DrugTabKeys.DRUG_DATE_START_DRUG + " end date "+CpatToolBConstants.DrugTabKeys.DRUG_END_DATE);
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DATE_START_DRUG }))
			{
				sProductStartDate = CpatToolBConstants.DrugTabKeys.DRUG_DATE_START_DRUG ;
			}
			else
			{
				sProductStartDate = CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_START_DATE;
			}
			
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_END_DATE}))
			{
				sProductEndDate = CpatToolBConstants.DrugTabKeys.DRUG_END_DATE;
			}
			else
			{
				sProductEndDate = CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE;
			}
			
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{sProductStartDate, sProductEndDate}))
			{
				valuesForDateDifference = getDifferenceBetweenDate(
						summaryDataTable.getColumnValues(sProductStartDate).get(0).displayValue().toString(),
						summaryDataTable.getColumnValues(sProductEndDate).get(0).displayValue().toString()
						);				
			}
			
			if(valuesForDateDifference.containsKey(CpatToolBConstants.DateDifference.DAYS))
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.DRUG_DURATION, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));
				parsedDataMap.put(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT, CpatToolBConstants.DrugTabKeys.DAYS);
			}	
		}
		
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE }))
		{
			sReactionStartDate = CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE;
		}		
		else
		{
			sReactionStartDate = CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE;
		}
		
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_END_DATE}))
		{
			sProductStopDate = CpatToolBConstants.DrugTabKeys.DRUG_END_DATE;
		}
		else
		{
			sProductStopDate = CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE;
		}

		//business logic for dosage status
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{sReactionStartDate , sProductStopDate}))
		{
			try 
			{
				int compare = firstDateAfterOrBeforeLastDate(String.valueOf(summaryDataTable.getColumnValues(sProductStopDate).get(0).displayValue()), String.valueOf(summaryDataTable.getColumnValues(sReactionStartDate).get(0).displayValue()));
				 
		        if (compare  < 0)
		        {
		        	parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_STATUS, "Not Applicable");
		        }
		        else
		        {
		        	parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_STATUS, "Unknown");
		        }
			}
			catch (SciException e)
			{
				log.error("Error while comparing date" +e);
				throw new SciException("Error while comparing date" +e);
			}
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{sProductStopDate}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_STATUS, "Stopped");
		}
		else
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_STATUS, "Unknown");
		}


		//business logic for drug start date
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DATE_START_DRUG}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_START,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_DATE_START_DRUG).get(0)
							.displayValue().toString());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_START_DATE}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_START,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_START_DATE)
					.get(0).displayValue().toString());
		}
		
		//business logic for drug end date
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_END_DATE}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_END,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_END_DATE).get(0)
							.displayValue().toString());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSAGE_END,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE)
					.get(0).displayValue().toString());
		}
		
		
		//business logic for drug indication
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DRUGINDICATION}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_INDICATION,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_DRUGINDICATION).get(0)
							.displayValue().toString());
		}
		else if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_NARRATIVE_INDICATION}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_INDICATION,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_NARRATIVE_INDICATION)
							.get(0).displayValue().toString());
		}
		else
		{
			if(p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.REG_CLASS).equalsIgnoreCase(CpatToolBConstants.ProductWhiteList.REG_OTC) || p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.REG_CLASS).equalsIgnoreCase(CpatToolBConstants.ProductWhiteList.REG_DIETARY_SUPPLEMENT))
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_INDICATION , CpatToolBConstants.DrugTabKeys.DRUG_USE_FOR_UNKNOWN_INDICATION);
			}
			else
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_INDICATION , CpatToolBConstants.DrugTabKeys.PRODUCT_USED_FOR_UNKNOWN_INDICATION);
			}
		}
		
		//business logic for drug batch lot
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_BATCH_NO}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.BATCHLOT,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_BATCH_NO)
					.get(0).displayValue().toString());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_LOT_NUMBER}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.BATCHLOT,
					summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_LOT_NUMBER)
					.get(0).displayValue().toString());
		}
		else
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.BATCHLOT, "UNKNOWN");
		}
		

		//business logic for dose
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DOSE_NO}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSE, summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_DOSE_NO).get(0).displayValue().toString());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DOSE}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSE, summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_DOSE).get(0).displayValue().toString());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_DOSE_OR_QUANTITY}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSE, summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_DOSE_OR_QUANTITY).get(0).displayValue().toString());
		}
		else
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.DOSE,  "");
		}
		
		//business logic for dosage
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DOSAGE_TEXT}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.NUM_DOSAGES, summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_DOSAGE_TEXT).get(0).displayValue().toString());
		}
		else
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.NUM_DOSAGES, "");
		}
		
		//business logic for interval
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_USE_FREQUENCY}))
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.INTERVAL, summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_USE_FREQUENCY).get(0).displayValue().toString());
		}
		else
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.INTERVAL, "");
		}
		
		appendDoseValues(oToolBase , parsedDataMap);
				
	}
	
	
	//Adding product code, generic name and formulation in scepte Map for further use while data entry
	public static void putProductCodeAndDetailsInDataMap(ParsedDataTable summaryDataTable , Map<String, String> parsedDataMap , Map<String, String> p_ProductWhiteList)
	{
		String sProductCode = null;
		//product code
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_CODE}))
		{
			sProductCode = getProductCode(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_CODE).get(0).displayValue().toString());

			if(!StringUtils.isNullOrEmpty(sProductCode))
			{
				parsedDataMap.put(SceptreConstants.Name.PRODUCTCODE, sProductCode);
			}
		}
		
		//generic name
		if(!StringUtils.isNullOrEmpty(p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.GENERIC_NAME)))
		{
			log.info("Generic name added in Map "+p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.GENERIC_NAME));
			parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_GENERICNAME , p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.GENERIC_NAME));
		}
		//Formulation
		if(!StringUtils.isNullOrEmpty(p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.PRODUCT_FORM)))
		{
			log.info("Formulation added in Map "+p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.PRODUCT_FORM));
			parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTFORM , p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.PRODUCT_FORM));
		}
		//reg class
		if(!StringUtils.isNullOrEmpty(p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.REG_CLASS)))
		{
			log.info("Reg class added in Map "+p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.REG_CLASS));
			parsedDataMap.put(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTCHARACTERISTICS , p_ProductWhiteList.get(CpatToolBConstants.ProductWhiteList.REG_CLASS));
		}
	}
	
	
	//For Initial case validating and deriving sceptre fields values
	public static void validateDrugTabForInitial(CpatToolBase oToolBase, Map<String, String> parsedDataMap, ParsedDataTable summaryDataTable , Map<String, String> p_ProductWhiteList) throws SciException
	{
		String sReactionStartDateForActionTaken = null , sDrugEndDateForActionTaken = null;
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE}))
		{
			sReactionStartDateForActionTaken = CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE;
		}
		else
		{
			sReactionStartDateForActionTaken = CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE;
		}
		
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_END_DATE}))
		{
			sDrugEndDateForActionTaken = CpatToolBConstants.DrugTabKeys.DRUG_END_DATE;
		}
		else
		{
			sDrugEndDateForActionTaken = CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE;
		}
		
		//Action taken /previously Hardcode to "Unknown
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{sReactionStartDateForActionTaken , sDrugEndDateForActionTaken}))
		{
			try 
			{
				int compare = firstDateAfterOrBeforeLastDate(String.valueOf(summaryDataTable.getColumnValues(sDrugEndDateForActionTaken).get(0).displayValue()), String.valueOf(summaryDataTable.getColumnValues(sReactionStartDateForActionTaken).get(0).displayValue()));
				
				if (compare  > 0) 
		        {
					parsedDataMap.put(SceptreConstants.DrugTabKeys.ACTIONTAKEN, "Drug Withdrawn/Discontinued");
		        } 
		        else if (compare  < 0)
		        {
		        	parsedDataMap.put(SceptreConstants.DrugTabKeys.ACTIONTAKEN, "Not Applicable");
		        }
		        else
		        {
		        	parsedDataMap.put(SceptreConstants.DrugTabKeys.ACTIONTAKEN, "Unknown");
		        }
			}
			catch (SciException e)
			{
				log.error("Error while comparing date" +e);
				throw new SciException("Error while comparing date" +e);
			}
			
		}
		else
		{
			parsedDataMap.put(SceptreConstants.DrugTabKeys.ACTIONTAKEN, "Unknown");
		}
		
		//business logic for Start of Administration to Start of First Reaction:
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_START_PERIOD}))
		{
			String[] sarrValueAndunit = null;
			sarrValueAndunit = getSeparatedDuration(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_START_PERIOD).get(0).displayValue()));
			
			if(sarrValueAndunit != null)
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION, sarrValueAndunit[0]);
				parsedDataMap.put(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION_UNIT, sarrValueAndunit[1]);
			}
		}
		else 
		{
			Map<String , String> valuesForDateDifference = null;
			
			String sReactionStartDateForFR = null;
			String sProductStartDateForFR = null;
			
			valuesForDateDifference = new HashMap<>();
			
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE}))
			{
				sReactionStartDateForFR = CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE;
			}
			else
			{
				sReactionStartDateForFR = CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE;
			}
			
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_DATE_START_DRUG}))
			{
				sProductStartDateForFR = CpatToolBConstants.DrugTabKeys.DRUG_DATE_START_DRUG;
			}
			else
			{
				sProductStartDateForFR = CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_START_DATE;
			}

			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{sReactionStartDateForFR, sProductStartDateForFR}))
			{
				valuesForDateDifference = getDifferenceBetweenDate(
						summaryDataTable.getColumnValues(sProductStartDateForFR).get(0).displayValue().toString(),
						summaryDataTable.getColumnValues(sReactionStartDateForFR).get(0).displayValue().toString()
						);
			}
			
			if(valuesForDateDifference.containsKey(CpatToolBConstants.DateDifference.DAYS))
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));
				parsedDataMap.put(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION_UNIT, CpatToolBConstants.DrugTabKeys.DAYS);
			}
		}
		
		//business logic for Last Dose of Drug to Start of First Reaction
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_LAST_PERIOD}))
		{
			String[] sarrValueAndunit = null;
			sarrValueAndunit = getSeparatedDuration(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.DRUG_LAST_PERIOD).get(0).displayValue()));
		
			parsedDataMap.put(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION, sarrValueAndunit[0]);
			parsedDataMap.put(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION_UNIT, sarrValueAndunit[1]);
		}
		else 
		{
			Map<String , String> valuesForDateDifference = null;
			String sReactionStartDate = null;
			String sProductEndDate = null;
			
			valuesForDateDifference = new HashMap<>();
			
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE}))
			{
				sReactionStartDate = CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE;
			}
			else
			{
				sReactionStartDate = CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE;
			}
			
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.DRUG_END_DATE}))
			{
				sProductEndDate = CpatToolBConstants.DrugTabKeys.DRUG_END_DATE;
			}
			else
			{
				sProductEndDate = CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE;
			}
			
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{sReactionStartDate, sProductEndDate}))
			{
				valuesForDateDifference = getDifferenceBetweenDate(
						summaryDataTable.getColumnValues(sProductEndDate).get(0).displayValue().toString(),
						summaryDataTable.getColumnValues(sReactionStartDate).get(0).displayValue().toString());				
			}
			
			if(valuesForDateDifference.containsKey(CpatToolBConstants.DateDifference.DAYS))
			{
				parsedDataMap.put(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));
				parsedDataMap.put(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION_UNIT, CpatToolBConstants.DrugTabKeys.DAYS);
			}
		}
		
	}
	
	
	public static void validateReportedTab(CpatToolBase oToolBase, Map<String, String> parsedDataMap, ParsedDataTable summaryDataTable )
	{
		String qualification = null;
		String caseType = null;
		
		caseType = oToolBase.userInputsParams.get(CpatToolBConstants.CaseDetails.CASE_TYPE);
		
		// BL Qualification
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReporterTabKeys.REPORTER_QUALIFICATION}))
		{
			
			parsedDataMap.put(SceptreConstants.ReporterTabKeys.QUALIFICATION, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.REPORTER_QUALIFICATION).get(0).displayValue()));
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReporterTabKeys.NARRATIVE_REPORTERTYPE}))
		{
			parsedDataMap.put(SceptreConstants.ReporterTabKeys.QUALIFICATION, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.NARRATIVE_REPORTERTYPE).get(0).displayValue()));
		}
		
		if(parsedDataMap.containsKey(SceptreConstants.ReporterTabKeys.QUALIFICATION))
		{
			
			if(parsedDataMap.get(SceptreConstants.ReporterTabKeys.QUALIFICATION).contains("/"))
			{
				qualification = parsedDataMap.get(SceptreConstants.ReporterTabKeys.QUALIFICATION);
				qualification = qualification.replace("/", " or ");
				
				parsedDataMap.put(SceptreConstants.ReporterTabKeys.QUALIFICATION, qualification);
			}
		}
		

		// Only for Initial Cases
		if(caseType.equals(CpatToolBConstants.CaseType.INITIAL))
		{
			// Reporter Country
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReporterTabKeys.REPORTER_COUNTRY}))
			{
				parsedDataMap.put(SceptreConstants.ReporterTabKeys.COUNTRY, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.REPORTER_COUNTRY).get(0).displayValue()));
			}
			else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.CONSUMER_COUNTRY}))
			{
				parsedDataMap.put(SceptreConstants.ReporterTabKeys.COUNTRY, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.CONSUMER_COUNTRY).get(0).displayValue()));
			}

			// Schedule
			parsedDataMap.put(SceptreConstants.ReporterTabKeys.SCHEDULE , "Follow-up via LSO/Clinical/Med Affairs");
			
		}
	}
	
	
	public static void validateReactionTab(CpatToolBase oToolBase, Map<String, String> parsedDataMap, ParsedDataTable summaryDataTable)
	{
		List<String> validOutComeList = null;
		Map<String , String > valuesForDateDifference = null ;
		String sReactionOutcome = null;
		
		validOutComeList = Arrays.asList(propMap.get("Reaction.OutcomeList").split(","));

		valuesForDateDifference = new HashMap<>();
		//business logic for outcome
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_OUTCOME}))
		{
			sReactionOutcome = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReactionTabKeys.REACTION_OUTCOME).get(0).displayValue());
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTIONOUTCOME}))
		{
			sReactionOutcome = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTIONOUTCOME).get(0).displayValue());

		}
		
		if(!StringUtils.isNullOrEmpty(sReactionOutcome))
		{
			if(isValWhiteListedMatches(validOutComeList , sReactionOutcome))
			{
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.OUTCOME , sReactionOutcome);
			}
			else
			{
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.OUTCOME , "Unknown");
			}
		}
		else
		{
			parsedDataMap.put(SceptreConstants.ReactionTabKeys.OUTCOME , "Unknown");
		}
		
		//business logic for start date
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE}))
		{
			parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED, String.valueOf(summaryDataTable
					.getColumnValues(CpatToolBConstants.ReactionTabKeys.REACTION_STARTDATE).get(0).displayValue()));
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE}))
		{
			parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED, String.valueOf(summaryDataTable
					.getColumnValues(CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE).get(0).displayValue()));
		}
		
		//business logic for end date
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_ENDDATE}))
		{
			parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_END, String.valueOf(summaryDataTable
					.getColumnValues(CpatToolBConstants.ReactionTabKeys.REACTION_ENDDATE).get(0).displayValue()));
		}
		else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_STOPDATE}))
		{
			parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_END, String.valueOf(summaryDataTable
					.getColumnValues(CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_STOPDATE).get(0).displayValue()));
		}
		
		//business logic for duration
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.REACTION_DURATION}))
		{
			String[] sarr_ReactionDurationWithUnit = null;
			sarr_ReactionDurationWithUnit = getSeparatedDuration(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReactionTabKeys.REACTION_DURATION).get(0).displayValue()));
			
			if(sarr_ReactionDurationWithUnit != null)
			{
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION, sarr_ReactionDurationWithUnit[0]);
				
				parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT, sarr_ReactionDurationWithUnit[1]);
			}
		}
		else
		{
			if(parsedDataMap.containsKey(SceptreConstants.ReactionTabKeys.REACTION_STARTED) && parsedDataMap.containsKey(SceptreConstants.ReactionTabKeys.REACTION_END))
			{
				valuesForDateDifference = getDifferenceBetweenDate(parsedDataMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED), parsedDataMap.get(SceptreConstants.ReactionTabKeys.REACTION_END));
				if(valuesForDateDifference.containsKey(CpatToolBConstants.DateDifference.DAYS))
				{
					parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION, valuesForDateDifference.get(CpatToolBConstants.DateDifference.DAYS));
					parsedDataMap.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT, CpatToolBConstants.ReactionTabKeys.DURATION_DAYS);
				}
			}
			
		}
		
		
		loadSDReactionKeywordsInResultScreen(oToolBase, summaryDataTable);
		
	}

	private static void validationForMedicalHistory(CpatToolBase oToolBase, Map<String, String> sceptreMap, ParsedDataTable summaryDataTable)
	{
		StringBuilder medicalHistoryComments = null;
		StringBuilder reportedDisease = null;
		String continuing = null;
		String parsedValue = null;
		
		medicalHistoryComments = new StringBuilder();
		reportedDisease = new StringBuilder();
		
		// Reported Disease
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PMHTabKeys.PMH_EPISODE_NAME}))
		{
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PMHTabKeys.PMH_EPISODE_NAME).get(0).displayValue()));
		}
		else
		{
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENTS_ALLERGIES}))
			{
				parsedValue = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENTS_ALLERGIES).get(0).displayValue());
				reportedDisease = reportedDisease.length() > 0? reportedDisease.append(StringConstants.COMMA + parsedValue): reportedDisease.append(parsedValue);
			}
			else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.RELEVANT_MEDICAL_HISTORY}))
			{
				parsedValue = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.RELEVANT_MEDICAL_HISTORY).get(0).displayValue());
				reportedDisease = reportedDisease.length() > 0? reportedDisease.append(StringConstants.COMMA + parsedValue): reportedDisease.append(parsedValue);
			}
			
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE, reportedDisease.toString());
		}
		
		
		// Start Date
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PMHTabKeys.PMH_START_DATE}))
		{
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.STARTDATE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PMHTabKeys.PMH_START_DATE).get(0).displayValue()));
		}
		
		// End Date
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PMHTabKeys.PMH_END_DATE}))
		{
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.ENDDATE, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PMHTabKeys.PMH_END_DATE).get(0).displayValue()));
		}
		
		// Medical History Comments
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PMHTabKeys.PMH_COMMENTS}))
		{
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT, String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PMHTabKeys.PMH_COMMENTS).get(0).displayValue()));
		}
		else
		{
			if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENTS_ALLERGIES_DESC}))
			{
				parsedValue = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENTS_ALLERGIES_DESC).get(0).displayValue());
				medicalHistoryComments = medicalHistoryComments.length() > 0? medicalHistoryComments.append(StringConstants.COMMA + parsedValue): medicalHistoryComments.append(parsedValue);
			}
			else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.RELEVANT_MEDICAL_HISTORY_DESC}))
			{
				parsedValue = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.RELEVANT_MEDICAL_HISTORY_DESC).get(0).displayValue());
				medicalHistoryComments = medicalHistoryComments.length() > 0? medicalHistoryComments.append(StringConstants.COMMA + parsedValue): medicalHistoryComments.append(parsedValue);
			}
			
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT, medicalHistoryComments.toString());
		}
		
		// Continuing
		if(sceptreMap.containsKey(SceptreConstants.MedicalHistoryTabKeys.CONTINUING))
		{
			continuing = sceptreMap.get(SceptreConstants.MedicalHistoryTabKeys.CONTINUING);
			if(!continuing.equalsIgnoreCase("YES") && !continuing.equalsIgnoreCase("NO"))
			{
				continuing = "Unknown";
			}
			
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.CONTINUING, continuing);
		}
		else
		{
			sceptreMap.put(SceptreConstants.MedicalHistoryTabKeys.CONTINUING, "Unknown");
		}
		
	}

	private static void splitHeightAppendUnit(Map<String , String> p_mSceptreMap , String p_sKey)
	{
		String l_sKeyValueForAlphabet = null ,l_sKeyValueForNumber = null , sKeyUnit = null , sReplacedValue  = null;
		
		List<String> listCentimeter = null;
		List<String> listInches = null;
		
		if(p_mSceptreMap.containsKey(p_sKey))
		{
			l_sKeyValueForAlphabet = p_mSceptreMap.get(p_sKey);
			
			if(!StringUtils.isNullOrEmpty(l_sKeyValueForAlphabet))
			{
				sReplacedValue = l_sKeyValueForAlphabet.replaceAll("[^\\d.]+|\\.(?!\\d)", "");
				
				if(!StringUtils.isNullOrEmpty(sReplacedValue))
				{
					l_sKeyValueForNumber = String.valueOf(Float.valueOf(sReplacedValue));
				}
				l_sKeyValueForAlphabet = l_sKeyValueForAlphabet.replaceAll("\\P{L}+", "").trim();
				
				listCentimeter = Arrays.asList(propMap.get(CpatToolBConstants.PropertiesMap.HEIGHT_CENTIMETER).split(","));
				listInches = Arrays.asList(propMap.get(CpatToolBConstants.PropertiesMap.HEIGHT_INCHES).split(","));
				
				if(isValWhiteListedMatches(listCentimeter, l_sKeyValueForAlphabet))
				{
					System.out.println("centimeter");
					sKeyUnit = CpatToolBConstants.Units.CENTIMETERS;
				}
				else if(isValWhiteListedMatches(listInches, l_sKeyValueForAlphabet))
				{
					System.out.println("Inches");
					sKeyUnit = CpatToolBConstants.Units.INCHES;
				}
				else
				{
					sKeyUnit = StringConstants.EMPTY;
				}
				
				if(!StringUtils.isNullOrEmpty(l_sKeyValueForNumber) && !StringUtils.isNullOrEmpty(sKeyUnit))
				{
					if(isValidFloatNumber(l_sKeyValueForNumber))
					{
						p_mSceptreMap.put(p_sKey	+	CpatToolBConstants.Units.UNIT , sKeyUnit);
						p_mSceptreMap.put(p_sKey	,	l_sKeyValueForNumber);
					}
					else
					{
						p_mSceptreMap.remove(p_sKey);
					}
				}
				else
				{
					p_mSceptreMap.remove(p_sKey);
				}
				
			}
			else
			{
				p_mSceptreMap.remove(p_sKey);
			}
		}
		
	}	
	
	private static void splitWeightAppendUnit(Map<String , String> p_mSceptreMap , String p_sKey)
	{
		String l_sKeyValueForAlphabet = null ,l_sKeyValueForNumber = null , sKeyUnit = null , sReplacedValue = null ;
		
		List<String> listKilogram = null;
		List<String> listPound = null;
		
		if(p_mSceptreMap.containsKey(p_sKey))
		{
			l_sKeyValueForAlphabet = p_mSceptreMap.get(p_sKey);
			
			if(!StringUtils.isNullOrEmpty(l_sKeyValueForAlphabet))
			{
				sReplacedValue = l_sKeyValueForAlphabet.replaceAll("[^\\d.]+|\\.(?!\\d)", "");
				
				if(!StringUtils.isNullOrEmpty(sReplacedValue))
				{
					l_sKeyValueForNumber = String.valueOf(Float.valueOf(sReplacedValue));
				}
				l_sKeyValueForAlphabet = l_sKeyValueForAlphabet.replaceAll("\\P{L}+", "").trim();
				
				listKilogram = Arrays.asList(propMap.get(CpatToolBConstants.PropertiesMap.WEIGHT_KILOGRAM).split(","));
				listPound = Arrays.asList(propMap.get(CpatToolBConstants.PropertiesMap.WEIGHT_POUND).split(","));
				
				if(isValWhiteListedMatches(listKilogram, l_sKeyValueForAlphabet))
				{
					System.out.println("Kilogram");
					sKeyUnit = CpatToolBConstants.Units.KILOGRAMS;
				}
				else if(isValWhiteListedMatches(listPound, l_sKeyValueForAlphabet))
				{
					System.out.println("Pound");
					sKeyUnit = CpatToolBConstants.Units.POUNDS;
				}
				else
				{
					sKeyUnit = StringConstants.EMPTY;
				}
				
				if(!StringUtils.isNullOrEmpty(l_sKeyValueForNumber) && !StringUtils.isNullOrEmpty(sKeyUnit))
				{
					if(isValidFloatNumber(l_sKeyValueForNumber))
					{
						p_mSceptreMap.put(p_sKey	+	CpatToolBConstants.Units.UNIT , sKeyUnit);
						p_mSceptreMap.put(p_sKey	,	l_sKeyValueForNumber);
					}
					else
					{
						p_mSceptreMap.remove(p_sKey);
					}
				}
				else
				{
					p_mSceptreMap.remove(p_sKey);
				}
				
			}
			else
			{
				p_mSceptreMap.remove(p_sKey);
			}
			
		}
	}
	
	
	public int getAge(String p_sFormat , String p_sInput) throws java.text.ParseException 
	{
		
		SimpleDateFormat sdf = null;
		Calendar dob = null, today = null;
		int curYear = 0, dobYear = 0, age = 0, curMonth = 0, dobMonth = 0, curDay = 0, dobDay = 0;

		sdf = new SimpleDateFormat(p_sFormat);
		dob = Calendar.getInstance();
		dob.setTime(sdf.parse(p_sInput));
		today = Calendar.getInstance();
		curYear = today.get(Calendar.YEAR);
		dobYear = dob.get(Calendar.YEAR);
		
		age = curYear - dobYear;
		
		// if dob is month or day is behind today's month or day
		curMonth = today.get(Calendar.MONTH);
		dobMonth = dob.get(Calendar.MONTH);
		
		if (dobMonth > curMonth) 
		{ 
			// this year can't be counted!
			age--;
		}
		else if (dobMonth == curMonth) 
		{
			// same month? check for day
			curDay = today.get(Calendar.DAY_OF_MONTH);
			dobDay = dob.get(Calendar.DAY_OF_MONTH);
			if (dobDay > curDay) { // this year can't be counted!
				age--;

			}
		}
		return age;
    }
	
	
	public static String convertListToString(List<String> list)
	{
		StringBuilder sb = new StringBuilder();
			
		for(int i = 0;  i < list.size(); i++)
		{
			sb = sb.length() > 0 ? sb.append("," + list.get(i)):sb.append(list.get(i));
		}
		
		return sb.toString();
	}
	
	
	public static DataTable getRowDataFromExcel(String p_sProductPath)
	{
		
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		boolean bHeaderRowPresent = true;
		int nSheetIndex = 0, nHeaderRowIndex = 1;
		
		String sColumnsToCollapse = "";
	
		try
		{
			oWorksheetWrapper = parseWorkbook(p_sProductPath, bHeaderRowPresent, nSheetIndex, nHeaderRowIndex);
			
			productListDataTable = convertToDatatable(oWorksheetWrapper, sColumnsToCollapse);
			
			log.info("get row from execl method return data table col size" +productListDataTable.colCount());
			log.info("get row from execl method return data table row size" +productListDataTable.rowCount());
			
			
		}
		catch(SciException sciExcep)
		{
			log.error("Product List xlsx parsing failed: " + sciExcep);
		}
		
		return productListDataTable;
	}
	
	
	public static Map<String ,String> getRowDataBasedOnProductCode(DataTable oExcelDataTable ,String p_sProductCode)
	{
		
		log.info("Product code is " +p_sProductCode);
		
		List<Object> genericNameList = null, reg_classList = null , productCodeList = null , productFormList = null ;
		Map<String ,String> rowByproductCode = null;
		
		genericNameList = oExcelDataTable.getColumnValues(CpatToolBConstants.ProductWhiteList.GENERIC_NAME);
		reg_classList = oExcelDataTable.getColumnValues(CpatToolBConstants.ProductWhiteList.REG_CLASS);
		productFormList = oExcelDataTable.getColumnValues(CpatToolBConstants.ProductWhiteList.PRODUCT_FORM);
		productCodeList = oExcelDataTable.getColumnValues(CpatToolBConstants.ProductWhiteList.APPROVAL_NUMBER);
		
		log.info("Excel Data column count " +oExcelDataTable.colCount());
		log.info("Excel Data row count  " + oExcelDataTable.rowCount());
		log.info("Excel Data 0th header " + oExcelDataTable.getColumnHeader(0));
		log.info("Excel Data 1st header " + oExcelDataTable.getColumnHeader(1));
		log.info("Excel Data 2nd header " + oExcelDataTable.getColumnHeader(2));
		log.info("Excel Data 3rd header " + oExcelDataTable.getColumnHeader(3));
		log.info("generic list " +genericNameList .size());
		log.info("form list " +productFormList.size());
		log.info("Product code list " +productCodeList.size());
		
		
		
		int count = 0;
		for(int rowIndex = 0 ; rowIndex < oExcelDataTable.rowCount() ; rowIndex++)
		{
		
			if(p_sProductCode.equals(productCodeList.get(rowIndex)))
			{
				if(count > 0)
				{
					rowByproductCode = null;
					break;
				}
				
				log.info("current Product code " +productCodeList.get(rowIndex));
				log.info("current generic " + String.valueOf(genericNameList.get(rowIndex)));
				log.info("current reg class " + String.valueOf(reg_classList.get(rowIndex)));
				log.info("current product form " + String.valueOf(productFormList.get(rowIndex)));
				
				rowByproductCode = new HashMap<>();
				
				rowByproductCode.put(CpatToolBConstants.ProductWhiteList.GENERIC_NAME , String.valueOf(genericNameList.get(rowIndex)));
				rowByproductCode.put(CpatToolBConstants.ProductWhiteList.REG_CLASS , String.valueOf(reg_classList.get(rowIndex)));
				rowByproductCode.put(CpatToolBConstants.ProductWhiteList.PRODUCT_FORM , String.valueOf(productFormList.get(rowIndex)));
				rowByproductCode.put(CpatToolBConstants.ProductWhiteList.APPROVAL_NUMBER , String.valueOf(productCodeList.get(rowIndex)));
				
				count++;
			}
		}
		return rowByproductCode;
	}
	
	
	public static String getProductCode(String p_sProductCode)
	{
		String suffix = null;
		Pattern productCodePattern = null;
		Matcher matcher = null;
		if (p_sProductCode.contains("_"))
		{
			suffix = p_sProductCode.substring(p_sProductCode.lastIndexOf("_"));
			productCodePattern = Pattern.compile("^_[a-zA-Z0-9]{2}-[a-zA-Z0-9]{2}");
			matcher = productCodePattern.matcher(suffix);
			if (matcher.matches())
			{
				p_sProductCode = p_sProductCode.replace(suffix, "");
			}
		}
		return p_sProductCode;
	}
	
	public static Map<String, String> getDifferenceBetweenDate(String p_StartDate , String p_EndDate)
	{
		
		Calendar oCalendar1 = null;
		Calendar oCalendar2 = null;
		
		Date oDate1 = null;
		Map<String , String> differenceValueMap = null;
		SimpleDateFormat format = null;
		Date oDate2 = null;
		long diffInMillies = 0 ,diffInMilliesPlusDay = 0, diffSeconds = 0 , diffMinutes = 0, diffHours = 0 , diffDays = 0 , diffMonths = 0 , diffYears = 0;
		
		//HH converts hour in 24 hours format (0-23), day calculation
		format = new SimpleDateFormat("dd-MMM-yyyy");

		if(!StringUtils.isNullOrEmpty(p_StartDate) && !StringUtils.isNullOrEmpty(p_EndDate))
		{
			try
			{
				oDate1 = format.parse(p_StartDate);
				oDate2 = format.parse(p_EndDate);

				oCalendar1 = getCalendar(oDate1);
			    oCalendar2 = getCalendar(oDate2);
				
				//in milliseconds
				diffInMillies = Math.abs(oDate2.getTime() - oDate1.getTime());
				//add 1 day(for same date count is 1 whole day)   
				diffInMilliesPlusDay = diffInMillies + TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);
				
				diffSeconds = TimeUnit.SECONDS.convert(diffInMilliesPlusDay, TimeUnit.MILLISECONDS);
				diffMinutes = TimeUnit.MINUTES.convert(diffInMilliesPlusDay, TimeUnit.MILLISECONDS);
				diffHours = TimeUnit.HOURS.convert(diffInMilliesPlusDay, TimeUnit.MILLISECONDS);
				diffDays = TimeUnit.DAYS.convert(diffInMilliesPlusDay, TimeUnit.MILLISECONDS);
			
				diffYears = getDiffYears(oCalendar1 , oCalendar2);
				diffMonths = diffYears * 12 + oCalendar2.get(Calendar.MONTH) - oCalendar1.get(Calendar.MONTH);		
				
				//calculate years from milliseconds
				differenceValueMap = new HashMap<>();
				
				differenceValueMap.put(CpatToolBConstants.DateDifference.YEARS, diffYears+"");
				differenceValueMap.put(CpatToolBConstants.DateDifference.MONTHS, diffMonths+"");
				differenceValueMap.put(CpatToolBConstants.DateDifference.DAYS, diffDays+"");
				differenceValueMap.put(CpatToolBConstants.DateDifference.HOURS, diffHours+"");
				differenceValueMap.put(CpatToolBConstants.DateDifference.MINUTES, diffMinutes+"");
				differenceValueMap.put(CpatToolBConstants.DateDifference.SECONDS, diffSeconds+"");
			}
			catch (ParseException parseExeption)
			{
				log.error("Error in parsing product start date or product end date" + parseExeption);
			}

		}
		
		return differenceValueMap;
	}

	
	public static WorkbookWorkSheetWrapperDEA parseWorkbook(String p_sStagedFileName, boolean p_bIsHeaderPresent, int p_nSheetIndex,int headerRowIndex) throws SciException
	{
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		int nNumberOfColumns = 0;
		int nNumberOfDataRows = 0;
		Workbook oWorkbook = null;
		Sheet oSheet = null;
	//	int HeaderRowIndex=3;
		int headerFinderCounter=1;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;
		
		List<Object> listTableRow = null;
		List<Object> listHeaderRow = null;
		List<List<Object>> listDataTable = null;
		File oWorkbookFile = null;
		try
		{
			headerIndex = headerRowIndex;
			oWorkbookFile = new File(p_sStagedFileName);
			oWorkbook = WorkbookFactory.create(oWorkbookFile);
			
			oSheet = oWorkbook.getSheetAt(p_nSheetIndex);
			
			listDataTable = new ArrayList<List<Object>>();
			
			oIteratorWorkbookRows = oSheet.rowIterator();
			if (p_bIsHeaderPresent)
			{
				/*while(oIteratorWorkbookRows.hasNext() && (headerFinderCounter<HeaderRowIndex))
				{
					headerFinderCounter++;
					continue;
				}*/
				while (oIteratorWorkbookRows.hasNext())
				{
					
					if((headerFinderCounter==headerRowIndex))
					{
						
					}	
					else
					{
							oIteratorWorkbookRows.next();
							headerFinderCounter++;
							continue;
					}
					
					oSingleWorkbookRow = oIteratorWorkbookRows.next();
					listHeaderRow = new ArrayList<Object>();
					
					if(oSingleWorkbookRow!=null)
					{
						// Iterate through the cells
						for (int nRowCellIndex = 0; nRowCellIndex < oSingleWorkbookRow.getLastCellNum(); nRowCellIndex++)
						{
							oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
							listHeaderRow.add(getWorkbookCellContent(oSingleWorkbookCell));
							
							nNumberOfColumns++;
						}
					}
				break;
				}
				
			}
			else
			{
				// throw new Exception("Header row not present use case not supported as yet");
				//listErrors.add(new SciError(p_sErrorKey));
				//Ignore and continue
			}
			
			//NOTE: Iterator might have moved one workbook row if an header was expected and found 
			while (oIteratorWorkbookRows.hasNext())
			{
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				listTableRow = new ArrayList<Object>(nNumberOfColumns);
				
				// Iterate through the cells.
				if(oSingleWorkbookRow!=null)
				{
					// Iterate through the cells.
					for (int nRowCellIndex = 0; nRowCellIndex < nNumberOfColumns; nRowCellIndex++)
					{
						oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
						listTableRow.add(getWorkbookCellContent(oSingleWorkbookCell));
					}
					listDataTable.add(listTableRow);
					nNumberOfDataRows++;
				}
			}
			
			oWorksheetWrapper = new WorkbookWorkSheetWrapperDEA(oSheet.getSheetName(), p_nSheetIndex, p_bIsHeaderPresent, nNumberOfColumns, nNumberOfDataRows, 
					listHeaderRow, listDataTable);
			
		}
		catch (FileNotFoundException fnfExcep)
		{
			log.error("Error parsing file as file not found", fnfExcep);
			throw new SciException("Error parsing file as file not found", fnfExcep);
		}
		catch (InvalidFormatException invExcep)
		{
			log.error("Error parsing file as file format incorrect", invExcep);
			throw new SciException("Error parsing file as file format incorrect", invExcep);
		}
		catch (IOException ioExcep)
		{
			log.error("Error parsing file as file", ioExcep);
			throw new SciException("Error parsing file", ioExcep);
		}
		finally
		{
			try
				{
					oWorkbook.close();
				}
				catch (Exception ioExcep)
				{
					//Ignore and continue
					log.error("Error closing workbook file", ioExcep);
				}
		}
		return oWorksheetWrapper;
	}
	
	
	public static DataTable convertToDatatable(WorkbookWorkSheetWrapperDEA p_oWorksheetWrapper, String p_sColumnsToCollapse)
	{
		DataTable oDataTable = null;

		oDataTable = new DataTable();
		
		p_oWorksheetWrapper.appendDataIntoDataTable(oDataTable, false, p_sColumnsToCollapse);
		
		return oDataTable;
	}

	
	@SuppressWarnings("deprecation")
	private static Object getWorkbookCellContent(Cell oSingleWorkbookCell)
	{
		boolean bCheckIfCellContainsDateValue = false;
		DataFormatter df = new DataFormatter(true);
	
		if (oSingleWorkbookCell!=null)
		{
			switch (oSingleWorkbookCell.getCellType()) 
			{
				case Cell.CELL_TYPE_BLANK:
					return oSingleWorkbookCell.getStringCellValue();
				case Cell.CELL_TYPE_STRING:
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_NUMERIC:
					//giving wrong date
					String dateFormat="";
					if(headerIndex==1)
						dateFormat="MM/dd/yy";
					else
					 dateFormat="";
					bCheckIfCellContainsDateValue = checkIfCellContainsDateValue(df.formatCellValue(oSingleWorkbookCell), dateFormat);
					if(bCheckIfCellContainsDateValue)
					{
						return getActualFormattedDateValue(df.formatCellValue(oSingleWorkbookCell), dateFormat);
					}
					else
					{
						oSingleWorkbookCell.setCellType(Cell.CELL_TYPE_STRING);
					}
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_BOOLEAN:
					return oSingleWorkbookCell.getBooleanCellValue();
				case Cell.CELL_TYPE_FORMULA:
					return oSingleWorkbookCell.getCellFormula();
				case Cell.CELL_TYPE_ERROR:
					return null;
				default:
					return new String("");
			}
		}
		else
		{
			return null;
		}
	}

	public static String[] getCountries()
	{
		return propMap.get("Countries").split(",");
	}
	
	
	public static String getICU(String[] p_sarrCountry , String p_sCountry)
	{
	
		String sICU = null;
		
		for(int i=0 ; i < p_sarrCountry.length ; i++)
		{
			if(p_sarrCountry[i].equalsIgnoreCase(p_sCountry))
			{
				sICU =  "JJCEMEA";
				break;
			}
		}
		
		if(sICU == null)
		{
			sICU =  "JJCLATAM";
		}
		
		
		return sICU;
	}
	private  static String getActualFormattedDateValue(String p_sDateValue, String dateFormat) 
	{
		String sIncommingDateFormat = null;
		//String sIncommingDateFormat2 = null;
		String sFormattedDate = null;
		String sDateFormat = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		Date tempFormattedDate = null;
		SimpleDateFormat formatter = null;
		
		if(p_sDateValue!=null)
		{
			if(dateFormat==null || dateFormat.equals(""))
			{
			String middleDatePart="";
			String startDatePart="";
			String endDatePart="";
			if(p_sDateValue.contains("/"))
			{
			String[] temp=p_sDateValue.split("/");
			startDatePart=temp[0];
			middleDatePart=temp[1];
			endDatePart=temp[2];
					//p_sDateValue.substring(p_sDateValue.lastIndexOf("/")+1,p_sDateValue.length());
			}
			if(p_sDateValue.contains("/"))
			{
				if(endDatePart.length()==2 )
					sIncommingDateFormat ="dd/MM/yy";
				else
					sIncommingDateFormat ="dd/MM/yyyy";
			}
			else
			sIncommingDateFormat = "dd-MMM-yyyy";//ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString());
			}
			else
				sIncommingDateFormat=dateFormat;
			sDateFormat = "dd-MMM-yyyy";//ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT.toString());
			//TODO: Check this
			try
			{
				incomingformatter = new SimpleDateFormat(sIncommingDateFormat);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
				//tempFormattedDate=new SimpleDateFormat("dd-MMM-yyyy").parse(p_sDateValue); 
			}
			catch(ParseException parseExcep)
			{
				log.error("Date formatting failed for: Date/Format - " + p_sDateValue +  "/" + sIncommingDateFormat, parseExcep);
				return "";
			}
			
			formatter = new SimpleDateFormat(sDateFormat);
			sFormattedDate = formatter.format(oFormattedDate);
		}
		else
		{
			return "";
		}
		return sFormattedDate;
	}
	
	private static boolean checkIfCellContainsDateValue(String p_sColumnValue, String p_inputdateformatter) 
	{
		
		try
		{
			SimpleDateFormat formatter = new SimpleDateFormat(p_inputdateformatter);
			formatter.parse(p_sColumnValue);
		}
		catch(Exception excep)
		{
			return false;
		}
		return true;
	}
	
	public static Map<String,String> getProductValuesFromWhiteList(CpatToolBase oToolBase , IAction oCallbackAction, ParsedDataTable p_oSummaryDataTable)
	{
		Map<String , String > dataByProductCodeMap = null;
		String sProductCode = null;
		
		if(p_oSummaryDataTable.isColumnPresent(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_CODE))
		{
			if(!StringUtils.isNullOrEmpty(p_oSummaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_CODE).get(0).displayValue().toString()))
			{
				log.info("Data table in cpat tool"+ productListDataTable.colCount() + "ro size "+productListDataTable.rowCount());
				
				sProductCode =CpatToolBHelper.getProductCode(p_oSummaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_CODE).get(0).displayValue().toString());
				
				dataByProductCodeMap = CpatToolBHelper.getRowDataBasedOnProductCode(productListDataTable , sProductCode);
			}
		}
		
		return dataByProductCodeMap;
	}
	
	private static String[] getSeparatedDuration(String sKeys)
	{
		String[] strArr = null;
		
		strArr = sKeys.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
		
		if(strArr.length == 2)
		{
			return strArr;
		}
		else
		{
			return null;
		}
	}

	
	public static String generateHtmlForResultScreen(Map<String, Object> resultScreenMap)
	{
		String tabName = null;
		String codeStatus = null;
		String extraSDNameList = null;
		String pendingOtherIdNos = null;
		String pendingLinkedCaseNos = null;
		String reactionKeywordsFound = null;
		String pendingDrugAdditionalInfo = null;
		
		int pendingDrugAddInfoCount = 0;

		StringBuilder htmlText = null;
		StringBuilder fieldNames= null;
		
		List<String> fieldList = null;
		List<String> notesList = null;
		
		String sdNameArray[] = null;
		
		Map<String, String> codingErrorStatus = null;
		Map<String, List<String>> deTabListData = null;

		int sdNo = 0;
		
		htmlText = new StringBuilder();
		
		extraSDNameList = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.ADD_SDLIST);
		codingErrorStatus = (Map<String, String>) resultScreenMap.get(CpatToolConstants.ResultScreen.LOOKUP_FAILED_STATUS);
		deTabListData = (Map<String, List<String>>) resultScreenMap.get(CpatToolConstants.ResultScreen.DATAENTRY_DONE_LIST);
		pendingOtherIdNos = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.PENDING_OTHER_IDENTIFICATION_NOS);
		pendingLinkedCaseNos = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.PENDING_LINKED_CASE_NOS);
		notesList = (List<String>) resultScreenMap.get(CpatToolConstants.ResultScreen.NOTES_LIST);
		reactionKeywordsFound = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.REACTION_KEYWORD_FOUND);
		pendingDrugAdditionalInfo = (String) resultScreenMap.get(CpatToolConstants.ResultScreen.PENDING_DRUG_ADDITIONALINFO);
		
		htmlText.append("<table width=\"100%\" border='1' bgcolor='#d6d6c2'>");
		htmlText.append("<tr align='center'><td colspan='2'><b><u><font size='4'>Data Entry Done</font></u></b></td></tr>");
		htmlText.append("<tr bgcolor='#A19F9B'><td >Tab</td><td >Field Names</td></tr>");
		
		if(deTabListData == null)
		{
			htmlText.append("<tr><td colspan='2'>No Tabs are filled</td></tr>");
		}
		else
		{
			if(deTabListData.size()==0)
			{
				htmlText.append("<tr><td colspan='2'>No Tabs are filled</td></tr>");
			}
			else
			{
				
				for(Map.Entry<String, List<String>> entry: deTabListData.entrySet())
				{
					tabName = entry.getKey();
					fieldList = entry.getValue();
					
					fieldNames = new StringBuilder();
					
					if(fieldList != null)
					{
						for(int index = 0 ; index< fieldList.size(); index++)
						{
							if(index==0)
							{
								fieldNames.append(fieldList.get(index));
							}
							else
							{
								fieldNames.append(", "+ fieldList.get(index));
							}
						}
						
					}
					
					htmlText.append("<tr>");
					htmlText.append("<td width='150' style='word-wrap:word-break;'>"+tabName+"</td>");
					htmlText.append("<td width='200' style='word-wrap:word-break;'>"+fieldNames.toString()+"</td>");
					htmlText.append("</tr>");
				}
			}	
		}
		
		htmlText.append("</table>");
		
		htmlText.append("<br>");
		
		// Left Right Table Start
		htmlText.append("<table width=\"100%\">");
		htmlText.append("<tr>");
		htmlText.append("<td>");
		
		//start of Sd Names Table
		htmlText.append("<table width=\"100%\" border='1' bgcolor='#d6d6c2'>");
		htmlText.append("<tr align='center'><td colspan='2'><b><u><font size='4'>Extra Source Documents</font></u></b></td></tr>");
		htmlText.append("<tr bgcolor='#A19F9B'><td>Sr No</td><td>Doc Names</td></tr>");
		
		if(StringUtils.isNullOrEmpty(extraSDNameList))
		{
			htmlText.append("<tr><td colspan='2'>No Additional SD present</td></tr>");
		}
		else if(extraSDNameList.contains(","))
		{
			sdNameArray = extraSDNameList.split(",");
		
			for(int sdIndex=0 ; sdIndex<sdNameArray.length; sdIndex++)
			{
				sdNo = sdIndex+1;
				htmlText.append("<tr>");
				htmlText.append("<td>"+sdNo+"</td>");
				htmlText.append("<td>"+sdNameArray[0]+"</td>");
				htmlText.append("</tr>");
			}
		}
		else
		{
			htmlText.append("<tr>");
			htmlText.append("<td>1</td>");
			htmlText.append("<td>"+extraSDNameList+"</td>");
			htmlText.append("</tr>");
		}
		
		htmlText.append("</table>");
		
		//end of sd Names Table
		htmlText.append("</td>");
		
		htmlText.append("<td>");
		
		//start of Coding standardFailedStatus
		htmlText.append("<table width=\"100%\" border='1' bgcolor='#d6d6c2'>");
		htmlText.append("<tr align='center'><td colspan='2'><b><u><font size='4'>Coding Status Failure</font></u></b></td></tr>");
		htmlText.append("<tr bgcolor='#A19F9B'><td>Tab</td><td>Lookup Status</td></tr>");
		
		
		if(codingErrorStatus == null)
		{
			htmlText.append("<tr><td colspan='2'>No Lookup Failure</td></tr>");
		}
		else
		{
			if(codingErrorStatus.size()==0)
			{
				htmlText.append("<tr><td colspan='2'>No Lookup Failure</td></tr>");
			}
			else
			{
				for(Map.Entry<String, String> entry: codingErrorStatus.entrySet())
				{
					tabName = entry.getKey();
					codeStatus = entry.getValue();
					
					htmlText.append("<tr>");
					htmlText.append("<td>"+tabName+"</td>");
					htmlText.append("<td>"+codeStatus+"</td>");
					htmlText.append("</tr>");
					//setting text in table
					
				}			
			}		
		}
		
		htmlText.append("</table>");
		htmlText.append("</td>");
		htmlText.append("</tr>");	
		
		htmlText.append("<br>");

		htmlText.append("<tr>");
		htmlText.append("<td>");
		
		if(StringUtils.isNullOrEmpty(pendingOtherIdNos))
		{
			pendingOtherIdNos = "<i>None</i>";
		}
		
		if(StringUtils.isNullOrEmpty(pendingLinkedCaseNos))
		{
			pendingLinkedCaseNos = "<i>None</i>";
		}
		
		// Other Identification Nos & Linked Case Nos
		htmlText.append("<table width=\"100%\" border='1' bgcolor='#d6d6c2'>");
		htmlText.append("<tr align='center'><td colspan='2'><b><u><font size='4'>Verification Failed</font></u></b></td></tr>");
		
		htmlText.append("<tr>");
		htmlText.append("<td bgcolor='#A19F9B' style='word-wrap:word-break;'>"+"Other Identification Numbers"+"</td>");
		htmlText.append("<td style='word-wrap:word-break;'>"+pendingOtherIdNos+"</td>");
		htmlText.append("</tr>");
		
		htmlText.append("<tr>");
		htmlText.append("<td bgcolor='#A19F9B' style='word-wrap:word-break;'>"+"Linked Case Numbers"+"</td>");
		htmlText.append("<td style='word-wrap:word-break;'>"+ pendingLinkedCaseNos +"</td>");
		htmlText.append("</tr>");
		
		htmlText.append("</table>");
		
		htmlText.append("</td>");
		
		htmlText.append("<td>");
		
		// Reaction Keyword		
		
		if(StringUtils.isNullOrEmpty(reactionKeywordsFound))
		{
			reactionKeywordsFound = "<i>None</i>";
		}
		
		htmlText.append("<table width=\"100%\" border='1' bgcolor='#d6d6c2'>");
		htmlText.append("<tr align='center'><td colspan='2'><b><u><font size='4'>Reaction Keyword found in SD Summary Section</font></u></b></td></tr>");
		
		htmlText.append("<tr>");
		htmlText.append("<td colspan='2' style='word-wrap:word-break;'>"+reactionKeywordsFound+"</td>");
		htmlText.append("</tr>");
		
		htmlText.append("</table>");
				
		htmlText.append("</td>");
		
		htmlText.append("</tr>");
		
		// Pending Drug Additional Info
		
		if(StringUtils.isNullOrEmpty(pendingDrugAdditionalInfo))
		{
			pendingDrugAdditionalInfo = "<i>None</i>";
		}else
		{
			pendingDrugAddInfoCount = pendingDrugAdditionalInfo.length();
		}
		
		htmlText.append("<tr>");
		htmlText.append("<td>");
		
		htmlText.append("<table width=\"100%\" border='1' bgcolor='#d6d6c2'>");
		htmlText.append("<tr align='center'><td colspan='2'><b><u><font size='4'>Pending Drug Additional Info (character count: " +pendingDrugAddInfoCount+ ")</font></u></b></td></tr>");
		
		htmlText.append("<tr>");
		htmlText.append("<td colspan='2' style='word-wrap:word-break;'>"+pendingDrugAdditionalInfo+"</td>");
		htmlText.append("</tr>");
		
		htmlText.append("</table>");
		
		htmlText.append("</td>");
		htmlText.append("</tr>");
		
		
		// Left Right Table END
		htmlText.append("</table>");
		
		// Notes
		htmlText.append("<br> <br>");
		htmlText.append("<div>");
		htmlText.append("<p><b><u><font size='4'>Notes:</font></u></b></p>");
		
		if(notesList == null)
		{
			htmlText.append("<p><i>None</i></p>");
		}
		else
		{
			htmlText.append("<ul>");

			for(String note: notesList)
			{
				htmlText.append("<li>" + note + "</li>");
			}

			htmlText.append("</ul>");
		}
		
		htmlText.append("</div>");
		
		
		return htmlText.toString();
	}
	
	private static boolean deleteDirectories(String p_sFilePath)
	{
		boolean bDeleted = false;
		
		File dir = new File(p_sFilePath);
		
		if(dir.isDirectory() == false)
		{
			log.info("Not a directory. Do nothing");
			return bDeleted;
		}
		
		File[] listFiles = dir.listFiles();
		
		for(File file : listFiles)
		{
			log.info("Deleting "+file.getName());
			file.delete();
		}
		
		return bDeleted;
	}
	
	
	public static boolean createDirectories(String sPath)
	{
		Path path = null;
		path = Paths.get(sPath);
        boolean bDirectoryCreated = false;
		
        
        try
        {
			//if directory exists?
	        if (Files.exists(path)) 
	        {
	        	deleteDirectories(sPath);
	        }
	        else
	        {
	        	
	        }
	      
        	Files.createDirectories(path);
        	bDirectoryCreated = true;
        }
        catch (IOException e)
        {
        	//fail to create directory
        	e.printStackTrace();
        	log.error("Fail to create directory "+sPath);
        	
        }
        
        return bDirectoryCreated;
	}

	public static int firstDateAfterOrBeforeLastDate(String p_sFirstDate , String p_sLastDate) throws SciException
	{
		SimpleDateFormat oSimpleDateFoemate = null;
		int compareReturnValue = 0;
		oSimpleDateFoemate = new SimpleDateFormat("dd-MMM-yyyy");
        Date oLastDate = null;
		Date oFirstDate = null;
		
		try
		{
			oFirstDate = oSimpleDateFoemate.parse(p_sFirstDate);
			oLastDate = oSimpleDateFoemate.parse(p_sLastDate);
		}
		catch (ParseException e)
		{
			log.error("Error parsing file as file format incorrect", e);
			throw new SciException("Error parsing file as file format incorrect", e);
		}

        System.out.println("date1 : " + oSimpleDateFoemate.format(oFirstDate));
        System.out.println("date2 : " + oSimpleDateFoemate.format(oLastDate));

        compareReturnValue = oFirstDate.compareTo(oLastDate);
        
        return compareReturnValue ;
	}
	
	public static boolean isDataDifferentinDifferentVersion(Map<String,ParsedDataTable> versionCollatedDataTableMap, String fileType)
	{
		boolean dataDiff  =false;
		String fieldString = null;
		String[] fieldArray  =null;
		String value = null;
		int keyCounter = 0;
		Map<String, List<ParsedDataWithConfiguration>> keyValueMap = null;
		List<ParsedDataTable> m_DdataTableList = new ArrayList<ParsedDataTable>();
		List<ParsedDataWithConfiguration> valueList = null; 
		ParsedDataWithConfiguration parsedDataValueWithConfig = null;
		
		//adding all dataTables from map to list
		m_DdataTableList.addAll(versionCollatedDataTableMap.values());
		
		if(propMap.containsKey("CollationKey"))
		{
			fieldString = propMap.get("CollationKey");
		}
		else
		{
			fieldString = "";
		}
		//properties file cannot have ## as it considered as comment hence keep $$ instead of ## and replacing it to ## here
		fieldString = fieldString.replace("$$", "##");
		
		fieldArray = fieldString.split(", ");
		
		//converting list of dataTables to single map with key as field name and value list
		//keyValueMap = CpatCasePreviewScreen.collateDataForMultipleDataTable(m_DdataTableList, fileType);
		
		for(String key: fieldArray)
		{
			keyCounter = 0;
			
			//for fields which are multiples
			if(key.endsWith(" 1"))
			{
				key = key.replace(" 1", "");
				if(keyValueMap.containsKey(key))
				{
					dataDiff = getDiffStatus(keyValueMap, dataDiff, key);
					if(dataDiff)
						break;
				}
				else
				{
					
					for(keyCounter=1;;keyCounter++)
					{
						key = key+" "+keyCounter;
						if(keyValueMap.containsKey(key))
						{
							dataDiff = getDiffStatus(keyValueMap, dataDiff, key);
							if(dataDiff)
								break;
						}
						else//no more incremental key for the current key hence break
						{
							break;
						}
					}
					
				}
				
			}
			else
			{
				dataDiff = getDiffStatus(keyValueMap, dataDiff, key);
				/*if(keyValueMap.containsKey(key))
				{
					valueList = keyValueMap.get(key);
					
						value =(String) valueList.get(0).getParsedDataValue().displayValue();
						
						for(int innerIndex = 0; innerIndex< valueList.size(); innerIndex++)
						{
							if(innerIndex!=0)
							{
								if(!value.equalsIgnoreCase((String)valueList.get(innerIndex).getParsedDataValue().displayValue()))
								{
									//value does not match with the other dataTable values hence setting dataDiff true and break
									dataDiff = true;
									break;
								}
							}
						}
						
				}*/
			}
			
			if(dataDiff)
				break;
		}
		
		return dataDiff;
	}
	
	public static boolean getDiffStatus(Map<String, List<ParsedDataWithConfiguration>> keyValueMap, boolean valueDiffStatus, String key)
	{
		List<ParsedDataWithConfiguration> valueList = null;
		String value = null;
		
		if(keyValueMap.containsKey(key))
		{
			valueList = keyValueMap.get(key);
			
				value =(String) valueList.get(0).getParsedDataValue().displayValue();
				
				for(int innerIndex = 0; innerIndex< valueList.size(); innerIndex++)
				{
					if(innerIndex!=0)
					{
						if(!value.equalsIgnoreCase((String)valueList.get(innerIndex).getParsedDataValue().displayValue()))
						{
							//value does not match with the other dataTable values hence setting dataDiff true and break
							valueDiffStatus = true;
							break;
						}
					}
				}
				
		}
		return valueDiffStatus;
	}
	
	public static void inputsCollectedActionListener(CpatToolBase oToolBase, IAction oCallbackAction, JFrame frame, FormSubmitEvent event)
	{
		Map<String, String> mapExtractedParameters = null;
		Map<String, String> payload = null;
		List<String> listErrors = null;
		ScpUser oScpUser = null;
		
		File productFile = null;
		
		SciPortalWebApiResponse response = null;
		
		int activityId = -1;
		
		oScpUser = oToolBase.scpUser;
		try
		{
			mapExtractedParameters = CpatToolHome.extractEventParameters(event);
			
			//Perform Validation
			listErrors = oToolBase.validateUserInputs(mapExtractedParameters);
			if (listErrors != null && listErrors.size() > 0)
			{
				JOptionPane.showMessageDialog(null, listErrors.get(0));
			}
			else
			{
				
				// Create the main Activity
				response = oToolBase.scpClient.registerActivity(oScpUser.getUserId(), oScpUser.getPassword(),
							oScpUser.getUserProjectsList().get(0).getProjectSeqId(), oToolBase.getToolDetails().getToolName(),
							"CPAT-TOOL-PROCESSING", activityId, "CPAT TOOL PROCESSING", "",
							null, "", "");
				
				if(response.getStatus() == STATUS.SUCCESS)
				{
					activityId = Integer.parseInt(response.getData().toString());
					
					// If Response code is Successful
					if(activityId > 0)
					{
						oToolBase.activityId = activityId;
						oToolBase.userInputsParams = mapExtractedParameters;
						
						productFile = new File(mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_PRODUCT_LIST));
						
						payload = new HashMap<>();
						payload.put("DESTINATION_SYSTEM_URL", propMap.get(CpatToolHome.getPropertyNameAppendedWithZone(oScpUser.getZone(), CpatToolConstants.SCEPTRE_SERVER_SUFFIX)));
						payload.put("DESTINATION_SYSTEM_USERNAME", mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_USERNAMAE));
						payload.put("DESTINATION_SYSTEM_OPERATION_MODE", mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_OPERATIONMODE));
						payload.put("DESTINATION_SYSTEM_CASE_ID", mapExtractedParameters.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID));
						payload.put("DESTINATION_SYSTEM_PRODUCT_LIST_NAME", productFile.getName());
						payload.put("INPUT_VALIDATION_STATUS", "Success");
						
						response = oToolBase.scpClient.registerActivity(oScpUser.getUserId(), oScpUser.getPassword(),
									oScpUser.getUserProjectsList().get(0).getProjectSeqId(), oToolBase.getToolDetails().getToolName(),
									CpatToolConstants.ACTIVITY_TYPE.USER_INPUTS_COLLECTED, oToolBase.activityId,
									CpatToolConstants.ACTIVITY_CODE.USER_INPUTS_COLLECTED, "", payload, "", "");
						
						if(response.getStatus() == STATUS.SUCCESS)
						{
							activityId = Integer.parseInt(response.getData().toString());
							
							if(activityId > 0)
							{
								frame.dispose();
								oCallbackAction.performAction(CPAT_ACTION.USER_INPUTS_COLLECTED ,  mapExtractedParameters, activityId );
							}
							else
							{
								JOptionPane.showMessageDialog(null, "Invalid Activity Id");
							}
							
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Error: " + response.getCode() + " - " + response.getMessage());
						}
						
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Invalid Activity Id");
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Error: " + response.getCode() + " - " + response.getMessage());
				}
				
			}
		}catch(Exception e)
		{
			log.error("User Input Collection Failed: ", null, e);
			JOptionPane.showMessageDialog(null, "User Input Collection Failed");
		}
		
	}
	
	private static boolean performSceptreValidations(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, String> payload) 
	{
		boolean isRegisterActivitySuccess = false;
		
		Map<String, Object> sceptreCaseIDTabValues = null;
		
		// Setting Values
		reportTypeList = Arrays.asList(propMap.get("ReportType.WhiteList").split(","));
		
		sceptreCaseIDTabValues = sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB);
		
		// Case ID TAB Validations in Sceptre
		if(sceptreCaseIDTabValues != null)
		{
			// Local Case Id Validation
			if(sceptreCaseIDTabValues.containsKey(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID))
			{
				
				if (StringUtils.isNullOrEmpty(String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID))))
				{
					payload.put("USER_MSG", "Local Case ID cannot be blank in SCEPTRE");
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "Local Case ID cannot be blank in SCEPTRE");
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
					}	
					
					return false;
				} 					
				
			}
			else
			{
				payload.put("USER_MSG", "Could not read Local Case ID from SCEPTRE");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "Could not read Local Case ID from SCEPTRE");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
				}	
				
				return false;
			}
			
			if(caseType.equalsIgnoreCase(CpatToolBConstants.CaseType.INITIAL))
			{
				// Report type
				if(sceptreCaseIDTabValues.containsKey(SceptreConstants.CaseIdTabKeys.REPORT_TYPE))
				{
					
					if (!isValWhiteListedMatches(reportTypeList,
							String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.REPORT_TYPE))))
					{
						payload.put("USER_MSG", "Report type must be 'Spontaneous'");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "Report type must be 'Spontaneous'");
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
						}	
						
						return false;
					} 					
					
				}
				else
				{
					payload.put("USER_MSG", "Could not read Report Type from SCEPTRE");
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "Could not read Report type from SCEPTRE");
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
					}	
					
					return false;
				}
			}
			else
			{
				// Report type for follow-up
				if(sceptreCaseIDTabValues.containsKey(SceptreConstants.CaseIdTabKeys.REPORT_TYPE_FOLLOWUP))
				{
					
					if (!isValWhiteListedMatches(reportTypeList,
							String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.REPORT_TYPE_FOLLOWUP))))
					{
						payload.put("USER_MSG", "Report type must be 'Spontaneous'");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "Report type must be 'Spontaneous'");
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
						}	
						
						return false;
					} 					
					
				}
				else
				{
					payload.put("USER_MSG", "Could not read Report Type from SCEPTRE");
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "Could not read Report type from SCEPTRE");
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
					}	
					
					return false;
				}

			}
			
			
			//Seriousness Check
			if(sceptreCaseIDTabValues.containsKey(SceptreConstants.CaseIdTabKeys.SERIOUS))
			{
				
				if (!String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.SERIOUS))
						.equalsIgnoreCase(CpatToolBConstants.SceptreValidationFields.SERIOUS))
				{
					payload.put("USER_MSG", "Serious must be 'Not Serious'");
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "Serious must be 'Not Serious'");
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
					}	
					
					return false;
				} 					
				
			}
			else
			{
				payload.put("USER_MSG", "Could not read Serious from SCEPTRE");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "Could not read Serious from SCEPTRE");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
				}	
				
				return false;
			}
			
			// Case Priority
			if(sceptreCaseIDTabValues.containsKey(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY))
			{
				
				if (!String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY))
						.equalsIgnoreCase(CpatToolBConstants.SceptreValidationFields.CASE_PRIORITY))
				{
					payload.put("USER_MSG", "Case Priority must be 5");
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "Case Priority must be 5");
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
					}	
					
					return false;
				} 					
				
			}
			else
			{
				payload.put("USER_MSG", "Could not read Case Priority from SCEPTRE");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "Could not read Case Priority from SCEPTRE");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
				}	
				
				return false;
			}
		}
		else
		{
			payload.put("USER_MSG", "Could not read data from Case ID TAB in SCEPTRE");
			isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
			
			if(isRegisterActivitySuccess)
			{
				JOptionPane.showMessageDialog(null, "Could not read data from Case ID TAB in SCEPTRE");
				
				closeWriterDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);							
			}	
			
			return false;
		}
		
		return true;
	}

	public static int getDiffYears(Calendar p_oCalendar1 , Calendar p_oCalendar2) 
	{
		int diff = 0;
		
	    diff = p_oCalendar2.get(Calendar.YEAR) - p_oCalendar1.get(Calendar.YEAR);
	    
	    if (p_oCalendar1.get(Calendar.MONTH) > p_oCalendar2.get(Calendar.MONTH) || 
	        (p_oCalendar1.get(Calendar.MONTH) == p_oCalendar2.get(Calendar.MONTH) && p_oCalendar1.get(Calendar.DATE) > p_oCalendar2.get(Calendar.DATE))) 
	    {
	        diff--;
	    }
	
	    return diff;
	}

	private static boolean performMandatorySDValidations(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, String> payload, List<PreviewCaseDoc> listParsedCaseDocs)
	{
		String sceptreLocalCaseId = null;
		String sdLocalCaseId = null;
		
		String reportType = null;
		String lifeThreatening = null;
		String disablingIncap = null;
		String resultedInDeath = null;
		String proLongedHosp = null;
		String congenitalAnomaly = null;
		String otherMedicallyImpCondition = null;
		String dateOfDeath = null;
		String wasAutopsyDone = null;
		
		String admittedToHospital = null;
		String hospitalizationAdmissionDate = null;
		String hospitalizationDischargeDate = null;
		String eventWithBirthDefect = null;
		String reportedCauseBirthDefect = null;
		String death = null;
		String pregnant = null;
		String pregnancyDueDate = null;
		String wasPatientBreastFeeding = null;
		String childUnfavourableEvent = null;
		String isEventMiscarriage = null;
		String dateCauseMiscarriage = null;
		
		String studyName = null;
		String sponsorStudyNo = null;
		String observedStudyType = null;
		
		String caseType = null;
		
		String sdName = null;
		
		Set<String> otherIdNosList = null;
		
		ParsedDataTable oDataTable = null;
		
		boolean isRegisterActivitySuccess = false;
		
		caseType = oToolBase.userInputsParams.get(CpatToolBConstants.CaseDetails.CASE_TYPE);
		
		sceptreLocalCaseId = String.valueOf(sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB).get(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID));
		
		otherIdNosList = (Set<String>) sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB).get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_CASEIDNUMBER);
		
		if(otherIdNosList == null)
		{
			otherIdNosList = new HashSet<>();
		}
		
		// Add Local Case ID as well
		otherIdNosList.add(sceptreLocalCaseId);
		
		for(PreviewCaseDoc doc: listParsedCaseDocs)
		{
			sdName = doc.getPdfFile().getName();
			oDataTable = doc.getDataTable();
			
			
			if(caseType.equals(CpatToolBConstants.CaseType.INITIAL))
			{
				// Only INITIAL Cases
				
				// Local Case ID
				if(!oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.SAFETY_REPORT_ID))
				{
					payload.put("USER_MSG", "No value found in SD for Local Case ID for SD: " + sdName);
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				  	
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "No value found in SD for Local Case ID for SD: " + sdName );
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
					}
					
					return false;
				}
				else
				{
					sdLocalCaseId = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.SAFETY_REPORT_ID).get(0).displayValue());
					
					if(!sdLocalCaseId.equals(sceptreLocalCaseId))
					{
						payload.put("USER_MSG", "SD Local Case ID does not match with SCEPTRE Local Case ID for SD: " + sdName);
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					  	
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Local Case ID does not match with SCEPTRE Local Case ID for SD: " + sdName );
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;	
					}
					
				}
				
			}
			else
			{
				// Only Follow Up cases
				
				if(!oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.SAFETY_REPORT_ID))
				{
					payload.put("USER_MSG", "No value found in SD for Local Case ID for SD: " + sdName);
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				  	
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "No value found in SD for Local Case ID for SD: " + sdName );
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
					}
					
					return false;
				}
				else
				{
					sdLocalCaseId = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.SAFETY_REPORT_ID).get(0).displayValue());
					
					if(!otherIdNosList.contains(sdLocalCaseId))
					{
						payload.put("USER_MSG", "SD Local Case ID does not match with SCEPTRE Local Case ID, Other Identification Nos list for SD: " + sdName);
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					  	
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Local Case ID does not match with SCEPTRE Local Case ID, Other Identification Nos list for SD: " + sdName );
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;	
					}
					
				}
				
			}
			
			
			// Common for INITIAL as well as FOLLOW UP
			
			// Report Type
			if(isKeyContainsNonAcceptableValue(oDataTable, new String[]{CpatToolBConstants.CaseIdTabKeys.REPORT_TYPE}))
			{
				reportType = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.REPORT_TYPE).get(0).displayValue());
			}
			else if(isKeyContainsNonAcceptableValue(oDataTable, new String[]{CpatToolBConstants.NarrativeKeys.REPORT_TYPE}))
			{
				reportType = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.REPORT_TYPE).get(0).displayValue());
			}
			else
			{
				reportType = StringConstants.EMPTY;
			}
			
			if(StringUtils.isNullOrEmpty(reportType))
			{
				payload.put("USER_MSG", "No value found in SD for Report Type for SD: " + sdName);
			  	isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
			  	
			  	if(isRegisterActivitySuccess)
			  	{
			  		JOptionPane.showMessageDialog(null, "Report Type is not supported. Only support for Spontaneous Case Types provided..Hence Cannot Continue for SD: " + sdName);
			  		
			  		closeWriterDriver();
			  		oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);			  		
			  		
			  	}
			  	
			  	return false;
			}
			else
			{
				if(!isValWhiteListedMatches(reportTypeList, reportType))
		    	{
		    		payload.put("USER_MSG", "Report Type is not supported. Only support for Spontaneous Case Types provided..Hence Cannot Continue for SD: " + sdName);
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				  	
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, "Report Type is not supported. Only support for Spontaneous Case Types provided..Hence Cannot Continue for SD: " + sdName);
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
				  	
				  	return false;
		    	}
			}
			
			// Life Threatening
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.LIFE_THREATENING))
			{
				lifeThreatening = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.LIFE_THREATENING).get(0).displayValue());
				if(!isNoNoneBlankCheck(lifeThreatening))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Life Threatening", lifeThreatening, sdName)); 
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Life Threatening", lifeThreatening, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
			
			// Disabling / Incapicitating
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.DISABLINGINCAPACITATING))
			{
				disablingIncap = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.DISABLINGINCAPACITATING).get(0).displayValue());
				if(!isNoNoneBlankCheck(disablingIncap))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Disabling / Incapicitating", disablingIncap, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Disabling / Incapicitating", disablingIncap, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
			
		    // Results in Death
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.RESULTED_IN_DEATH))
			{
				resultedInDeath = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.RESULTED_IN_DEATH).get(0).displayValue());
				if(!isNoNoneBlankCheck(resultedInDeath))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Results in Death", resultedInDeath, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Results in Death", resultedInDeath, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
			}
		    else if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.DEATH))
		    {
		    	resultedInDeath = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.DEATH).get(0).displayValue());
		    	if(!isNoNoneBlankCheck(resultedInDeath))
		    	{
		    		payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Results in Death", resultedInDeath, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Results in Death", resultedInDeath, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
						
					}
		
					return false;
		    	}
		    }
		    
		    
		    // Caused/ Prolonged Hosp
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.CAUSED_PROLONGED_HOSP))
			{
				proLongedHosp = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.CAUSED_PROLONGED_HOSP).get(0).displayValue());
				if(!isNoNoneBlankCheck(proLongedHosp))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Casued/Prolonged Hospitalized", proLongedHosp, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Casued/Prolonged Hospitalized", proLongedHosp, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
			}
		    else if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.ADMITTED_HOSPITAL_EVENT))
		    {
		    	proLongedHosp = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.ADMITTED_HOSPITAL_EVENT).get(0).displayValue());
		    	if(!isNoNoneBlankCheck(proLongedHosp))
		    	{
		    		payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Casued/Prolonged Hospitalized", proLongedHosp, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Casued/Prolonged Hospitalized", proLongedHosp, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
		    	}
		    }
		    
		    // Congenital Anomaly/Birth Defect
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.CONGENITALANOMALYBIRTHDEFECT))
			{
				congenitalAnomaly = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.CONGENITALANOMALYBIRTHDEFECT).get(0).displayValue());
				if(!isNoNoneBlankCheck(congenitalAnomaly))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Congenital Anomaly Birth Defect", congenitalAnomaly, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Congenital Anomaly Birth Defect", congenitalAnomaly, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    
		    // Other Medically Important Condition
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.OTHER_MED_IMP))
			{
				otherMedicallyImpCondition = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.OTHER_MED_IMP).get(0).displayValue());
				if(!isNoNoneBlankCheck(otherMedicallyImpCondition))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Other Medically Important Condition", otherMedicallyImpCondition, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Other Medically Important Condition", otherMedicallyImpCondition, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    // Date of Death
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.DATE_OF_DEATH))
			{
				dateOfDeath = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.DATE_OF_DEATH).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(dateOfDeath))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Date Of Death", dateOfDeath, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Date Of Death", dateOfDeath, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    else if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.DATE_OF_DEATH))
			{
				dateOfDeath = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.DATE_OF_DEATH).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(dateOfDeath))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Date Of Death", dateOfDeath, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Date Of Death", dateOfDeath, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
			}
		    
		    
		    // Was Autopsy done
		    if(oDataTable.isColumnPresent(CpatToolBConstants.CaseIdTabKeys.WAS_AUTOPSY_DONE))
			{
				wasAutopsyDone = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.WAS_AUTOPSY_DONE).get(0).displayValue());
				if(!isNoNoneBlankCheck(wasAutopsyDone))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Was Autopsy Done", wasAutopsyDone, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Was Autopsy Done", wasAutopsyDone, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    //Admitted to Hospital Related to the Event
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.ADMITTED_HOSPITAL_EVENT))
			{
				admittedToHospital = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.ADMITTED_HOSPITAL_EVENT).get(0).displayValue());
				if(!isNoNoneBlankCheck(admittedToHospital))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Admitted to Hospital related to the Event", admittedToHospital, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Admitted to Hospital related to the Event", admittedToHospital, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    	
		    //Date of Hospitalization Admission
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.DATE_HOSPITALIZATION_ADMISSION))
			{
				hospitalizationAdmissionDate = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.DATE_HOSPITALIZATION_ADMISSION).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(hospitalizationAdmissionDate))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Date of Hospitalization Admitted", hospitalizationAdmissionDate, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Date of Hospitalization Admitted", hospitalizationAdmissionDate, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    //Date of Hospitalization Discharge
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.DATE_HOSPITALIZATION_DISCHARGE))
			{
				hospitalizationDischargeDate = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.DATE_HOSPITALIZATION_DISCHARGE).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(hospitalizationDischargeDate))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Date of Hospitalization Discharge", hospitalizationDischargeDate, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Date of Hospitalization Discharge", hospitalizationDischargeDate, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    //Event Associated with a Birth Defect
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.EVENT_ASSOCIATED_BIRTH_DEFECT))
			{
				eventWithBirthDefect = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.EVENT_ASSOCIATED_BIRTH_DEFECT).get(0).displayValue());
				if(!isNoNoneBlankCheck(eventWithBirthDefect))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Event Associated with a Birth Defect", eventWithBirthDefect, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Event Associated with a Birth Defect", eventWithBirthDefect, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    	
		    //Reported cause of birth defect
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.REPORTED_CAUSE_BIRTH_DEFECT))
			{
				reportedCauseBirthDefect = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.REPORTED_CAUSE_BIRTH_DEFECT).get(0).displayValue());
				if(!isNoNoneBlankCheck(reportedCauseBirthDefect))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Reported cause of Birth Defect", reportedCauseBirthDefect, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Reported cause of Birth Defect", reportedCauseBirthDefect, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    	
		    // Pregnant
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.PREGNANT))
			{
				pregnant = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PREGNANT).get(0).displayValue());
				if(!isNoNoneBlankCheck(pregnant))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Pregnancy", pregnant, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Pregnancy", pregnant, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    // Pregnancy Due Date
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.PREGNANT_DUE_DATE))
			{
				pregnancyDueDate = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PREGNANT_DUE_DATE).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(pregnancyDueDate))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Pregnancy due date", pregnancyDueDate, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Pregnancy due date", pregnancyDueDate, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    // Was Patient Breastfeeding
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.WAS_BREAST_FEEDING))
			{
				wasPatientBreastFeeding = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.WAS_BREAST_FEEDING).get(0).displayValue());
				if(!isNoNoneBlankCheck(wasPatientBreastFeeding))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Was Patient Breastfeeding", wasPatientBreastFeeding, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Was Patient Breastfeeding", wasPatientBreastFeeding, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		
		    // Did Child Have Unfavorable Event
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.CHILD_UNFAVOURABLE_EVENT))
			{
				childUnfavourableEvent = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.CHILD_UNFAVOURABLE_EVENT).get(0).displayValue());
				if(!isNoNoneBlankCheck(childUnfavourableEvent))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Did Child Have Unfavourable Event", childUnfavourableEvent, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Did Child Have Unfavourable Event", childUnfavourableEvent, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    //Is Event Associated with a Miscarriage
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.EVENT_ASSOCIATED_MISCARRIAGE))
			{
				isEventMiscarriage = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.EVENT_ASSOCIATED_MISCARRIAGE).get(0).displayValue());
				if(!isNoNoneBlankCheck(isEventMiscarriage))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Is Event Associated with a Miscarriage", isEventMiscarriage, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Is Event Associated with a Miscarriage", isEventMiscarriage, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    //Date and Reported Cause of Miscarriage
		    if(oDataTable.isColumnPresent(CpatToolBConstants.NarrativeKeys.DATE_CAUSE_MISCARRIAGE))
			{
				dateCauseMiscarriage = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.DATE_CAUSE_MISCARRIAGE).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(dateCauseMiscarriage))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Date and Reported Cause of Miscarriage", dateCauseMiscarriage, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Date and Reported Cause of Miscarriage", dateCauseMiscarriage, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    //Study Name
		    if(oDataTable.isColumnPresent(CpatToolBConstants.ReporterTabKeys.STUDY_NAME))
			{
				studyName = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.STUDY_NAME).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(studyName))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Study Name", studyName, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Study Name", studyName, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    
		    // Sponsor Study No
		    if(oDataTable.isColumnPresent(CpatToolBConstants.ReporterTabKeys.SPONSOR_STUDY_NO))
			{
				sponsorStudyNo = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.SPONSOR_STUDY_NO).get(0).displayValue());
			
				if(!StringUtils.isNullOrEmpty(sponsorStudyNo))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Sponsor Study Number", sponsorStudyNo, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Sponsor Study Number", sponsorStudyNo, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		    
		    // Observed Study type
		    if(oDataTable.isColumnPresent(CpatToolBConstants.ReporterTabKeys.OBSERVED_STUDY_TYPE))
			{
				observedStudyType = String.valueOf(oDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.OBSERVED_STUDY_TYPE).get(0).displayValue());
				if(!StringUtils.isNullOrEmpty(observedStudyType))
				{
					payload.put("USER_MSG", displayNoNoneBlankErrorMessage("Observed Study type", observedStudyType, sdName));
					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
					
					if(isRegisterActivitySuccess)
					{
						JOptionPane.showMessageDialog(null, displayNoNoneBlankErrorMessage("Observed Study type", observedStudyType, sdName));
						
						closeWriterDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
					}
					
					return false;
				}
				
			}
		    
		}
		
		return true;
		
	}
	
	private static boolean performOptionalSDValidations(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, String> payload, List<PreviewCaseDoc> listParsedCaseDocs)
	{
		List<String> seriousnessKeywordList = null; 
		List<String> narrativeKeys = null;
		String parsedValue = null;
		ParsedDataTable oDataTable = null;
		boolean isRegisterActivitySuccess = false;
		
		String[] buttons = { "Continue", "Abort"};
		int option = -1;
		
		String sdName = null;
				
		narrativeKeys = new ArrayList<>();
		seriousnessKeywordList = Arrays.asList(propMap.get("SeriousnessList").split(StringConstants.COMMA));
		
		for(PreviewCaseDoc doc: listParsedCaseDocs)
		{
			sdName = doc.getPdfFile().getName();
			oDataTable = doc.getDataTable();
			
			narrativeKeys.clear();
			
			for(int i = 0; i < oDataTable.colCount(); i++)
		    {
		    	if(oDataTable.getMnemonic(i).startsWith(CpatToolBConstants.NarrativeKeys.CASE_NOTES_ENGLISH_LANGUAGE))
		    	{
		    		if(!StringUtils.isNullOrEmpty(String.valueOf(oDataTable.getColumnValues(i).get(0).displayValue())))
		    		{
		    			narrativeKeys.add(oDataTable.getMnemonic(i));
		    		}
		    	}
		    	
		    	if(oDataTable.getMnemonic(i).equals(CpatToolBConstants.NarrativeKeys.VERBATIM))
		    	{
		    		if(!StringUtils.isNullOrEmpty(String.valueOf(oDataTable.getColumnValues(i).get(0).displayValue())))
		    		{
		    			narrativeKeys.add(oDataTable.getMnemonic(i));
		    		}
		    	}
		    }
		    
		    for(String key: narrativeKeys)
		    {
		    	for(String seriousness: seriousnessKeywordList)
		    	{
		    		if(oDataTable.isColumnPresent(key))
		    		{
		    			parsedValue = String.valueOf(oDataTable.getColumnValues(key).get(0).displayValue());
		    			if(CpatToolHome.isKeywordFound(parsedValue, seriousness))
		    			{   
		    				while(option == -1)
		    				{
		    					option = JOptionPane.showOptionDialog(null, "Seriousness keyword '" + seriousness + "' found in the narrative summary for SD: " + sdName, "Confirmation",
		    							JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[0]);
		    				}
		    				
		    				if(option == 1)
		    				{
		    					payload.put("USER_MSG", "Seriousness keyword '" + seriousness + "' found in the narrative summary for SD: " + sdName);
		    					isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
		    					
		    					if(isRegisterActivitySuccess)
		    					{
		    						JOptionPane.showMessageDialog(null, "Seriousness keyword '" + seriousness + "' found in the narrative summary for SD: " + sdName);
		    						
		    						closeWriterDriver();
		    						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);						
		    					}
		    					
		    					return false;
		    					
		    				}
		    			}
		    		}
		    	}
		    }
		
		}
		
		
		return true;
	}
	
	public static Calendar getCalendar(Date date) 
	{
	    Calendar cal = null;
	    cal = Calendar.getInstance();
	    cal.setTime(date);
	    return cal;
	}
	
	private static boolean validateSdSummary(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, String> payload, ParsedDataTable summaryDataTable, Map<String, String> sceptreMap) throws SciException
	{
		
		if(propMap.containsKey(CpatToolBConstants.CPAT_SDSVALIDATION_ENABLE))
		{
			if(propMap.get(CpatToolBConstants.CPAT_SDSVALIDATION_ENABLE).equalsIgnoreCase("true"))
			{
				String caseType = null;
				String sceptreDOB = null;
				String sceptrePatientInitials = null;
				String sceptreGender = null;
				String sceptreReporterGivenName = null;
				String sceptreReporterFamilyName = null;
				String sceptreCountryOfPrimarySource = null;
				
				String sceptreCountryAEOccured = null;
				List<String> sceptreCountrySpecificApprovalNosList = null;
				String sceptreReporterCountry = null;
				
				/************code*********/
				String countryForIdentifyPID = null;
				
				String summaryDOB = null;
				String summaryPatientInitials = null;
				String summaryGender = null;
				String summaryReporterGivenName = null;
				String summaryReporterFamilyName = null;
				String summaryCountryOfPrimarySource = null;
				
				String summaryCountryAEOccured = null;
				String summaryProductCode = null;
				String summaryReporterCountry = null;
				
				Map<String, Object> sceptreCaseIDTabValues = null;
				Map<String, Object> sceptreReporterTabValues = null;
				Map<String, Object> sceptrePatientTabValues = null;
				Map<String, Object> sceptreDrugTabValues = null;
				
				boolean isRegisterActivitySuccess = false;
				
				caseType = oToolBase.userInputsParams.get(CpatToolBConstants.CaseDetails.CASE_TYPE);
				
				sceptreCaseIDTabValues = sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB);
				sceptreReporterTabValues = sceptreTabValues.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB);
				sceptrePatientTabValues = sceptreTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_TAB);
				sceptreDrugTabValues = sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB);
				
				
				sceptreDOB = String.valueOf(sceptrePatientTabValues.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH));
				sceptrePatientInitials = String.valueOf(sceptrePatientTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_ID));
				sceptreGender = String.valueOf(sceptrePatientTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_SEX));
				
				sceptreReporterGivenName = String.valueOf(sceptreReporterTabValues.get(SceptreConstants.ReporterTabKeys.GIVEN_NAME));
				sceptreReporterFamilyName = String.valueOf(sceptreReporterTabValues.get(SceptreConstants.ReporterTabKeys.FAMILY_NAME));
				
				
				if(caseType.equalsIgnoreCase(CpatToolBConstants.CaseType.FOLLOWUP))
				{
					sceptreCountryOfPrimarySource = String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE_FOLLOWUP));
				}
				else
				{
					sceptreCountryOfPrimarySource = String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE));
				}
				
				
				// Primary Source Country
				if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY}))
				{ 
					summaryCountryOfPrimarySource  = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY).get(0).displayValue());
				}
				else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY}))
				{
					summaryCountryOfPrimarySource = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PRIMARY_CONSUMER_COUNTRY).get(0).displayValue());
				}
				
				// Occur Country
				if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.CaseIdTabKeys.OCCUR_COUNTRY}))
				{
					summaryCountryAEOccured = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.CaseIdTabKeys.OCCUR_COUNTRY).get(0).displayValue());
				}
				else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.EVENT_COUNTRY_OCCURED}))
				{
					summaryCountryAEOccured = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.EVENT_COUNTRY_OCCURED).get(0).displayValue());
				}
				
				
				// Patient ID
				if(summaryCountryOfPrimarySource != null)
				{
					countryForIdentifyPID = getICU(getCountries(), summaryCountryOfPrimarySource);
					
					if(!StringUtils.isNullOrEmpty(countryForIdentifyPID))
					{
						if(countryForIdentifyPID.equalsIgnoreCase("JJCEMEA"))
						{
							summaryPatientInitials = "Private";
						}
						else
						{
							if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_INITIALS}))
							{
								summaryPatientInitials = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_INITIALS).get(0).displayValue());
							}
							else if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_INITIALS}))
							{
								summaryPatientInitials = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_INITIALS).get(0).displayValue());
							}
						}
					}
				}
				
				
				// Patient Gender
				if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.PatientTabKeys.PATIENT_GENDER}))
				{
					summaryGender = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.PatientTabKeys.PATIENT_GENDER).get(0).displayValue());
					
					
					if(genderIsNotInList(summaryGender))
					{
						//do nothing
					}
					else if(summaryGender.equalsIgnoreCase("M"))
					{
						summaryGender = "Male";
					}
					else if(summaryGender.equalsIgnoreCase("F"))
					{
						summaryGender = "Female";
					}
					else
					{
						summaryGender = "Unknown";
					}
					
				}
				else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_GENDER}))
				{
					summaryGender =  String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_GENDER).get(0).displayValue());
					
					if(genderIsNotInList(summaryGender))
					{
						//do nothing
					}
					else if(summaryGender.equalsIgnoreCase("M"))
					{
						summaryGender = "Male";
					}
					else if(summaryGender.equalsIgnoreCase("F"))
					{
						summaryGender = "Female";
					}
					else
					{
						summaryGender = "Unknown";
					}
				}
				else
				{
					summaryGender = "Unknown";
				}
				
				
				
				// Is null empty checks for Summary Values
				if(!sceptreMap.containsKey(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH))
				{
					summaryDOB = StringConstants.EMPTY;
				}
				else
				{
					summaryDOB = sceptreMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH);
				}
				
				if(!sceptreMap.containsKey(SceptreConstants.ReporterTabKeys.GIVEN_NAME))
				{
					summaryReporterGivenName = StringConstants.EMPTY;
				}
				else
				{
					summaryReporterGivenName = sceptreMap.get(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
				}
				
				if(!sceptreMap.containsKey(SceptreConstants.ReporterTabKeys.FAMILY_NAME))
				{
					summaryReporterFamilyName = StringConstants.EMPTY;
				}
				else
				{
					summaryReporterFamilyName = sceptreMap.get(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
				}
				
				
				// DOB
				if(!StringUtils.isNullOrEmpty(summaryDOB) && !StringUtils.isNullOrEmpty(sceptreDOB))
				{
					
					if(firstDateAfterOrBeforeLastDate(summaryDOB, sceptreDOB) != 0)
					{
						payload.put("USER_MSG", "SD Summary 'DOB' does not match with Sceptre");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Summary 'DOB' does not match with Sceptre");
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;
					} 			
				}
				
				// Patient ID
				if(!StringUtils.isNullOrEmpty(summaryPatientInitials) && !StringUtils.isNullOrEmpty(sceptrePatientInitials))
				{
					if(!summaryPatientInitials.equalsIgnoreCase(sceptrePatientInitials))
					{
						payload.put("USER_MSG", "SD Summary 'Patient Initials' does not match with Sceptre");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Summary 'Patient Initials' does not match with Sceptre");
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;
					} 			
				}
				
				// Patient Gender
				if(!StringUtils.isNullOrEmpty(summaryGender) && !StringUtils.isNullOrEmpty(sceptreGender))
				{
					if(!summaryGender.equalsIgnoreCase(sceptreGender))
					{
						payload.put("USER_MSG", "SD Summary 'Patient Sex' does not match with Sceptre");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Summary 'Patient Sex' does not match with Sceptre");
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;
					} 			
				}
				
				// Reporter Given Name
				if(!StringUtils.isNullOrEmpty(summaryReporterGivenName) && !StringUtils.isNullOrEmpty(sceptreReporterGivenName))
				{
					if(!summaryReporterGivenName.equalsIgnoreCase(sceptreReporterGivenName))
					{
						payload.put("USER_MSG", "SD Summary 'Reporter Given Name' does not match with Sceptre");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Summary 'Reporter Given Name' does not match with Sceptre");
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;
					} 			
				}
				
				// Reporter Family Name
				if(!StringUtils.isNullOrEmpty(summaryReporterFamilyName) && !StringUtils.isNullOrEmpty(sceptreReporterFamilyName))
				{
					if(!summaryReporterFamilyName.equalsIgnoreCase(sceptreReporterFamilyName))
					{
						payload.put("USER_MSG", "SD Summary 'Reporter Family Name' does not match with Sceptre");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Summary 'Reporter Family Name' does not match with Sceptre");
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;
					} 			
				}
				
				// Country of Primary Source
				if(!StringUtils.isNullOrEmpty(summaryCountryOfPrimarySource) && !StringUtils.isNullOrEmpty(sceptreCountryOfPrimarySource))
				{
					if(!summaryCountryOfPrimarySource.equalsIgnoreCase(sceptreCountryOfPrimarySource))
					{
						payload.put("USER_MSG", "SD Summary 'Country of Primary Source' does not match with Sceptre");
						isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
						
						if(isRegisterActivitySuccess)
						{
							JOptionPane.showMessageDialog(null, "SD Summary 'Country of Primary Source' does not match with Sceptre");
							
							closeWriterDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
						}
						
						return false;
					} 			
				}
				
				
				// Exclusive FUP checks
				if(caseType.equals(CpatToolBConstants.CaseType.FOLLOWUP))
				{
					sceptreCountryAEOccured = String.valueOf(sceptreCaseIDTabValues.get(SceptreConstants.CaseIdTabKeys.COUNTRY_WHEREAE_OCCOURED_FOLLOWUP));
					sceptreCountrySpecificApprovalNosList = (List<String>) sceptreDrugTabValues.get(SceptreConstants.DrugTabKeys.GET_APPROVAL_NUMBERS);
					sceptreReporterCountry = String.valueOf(sceptreReporterTabValues.get(SceptreConstants.ReporterTabKeys.COUNTRY));
					
					summaryProductCode = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_CODE).get(0).displayValue());
					
					if(StringUtils.isNullOrEmpty(summaryCountryAEOccured))	
					{
						summaryCountryAEOccured = StringConstants.EMPTY;
					}
					
					if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReporterTabKeys.REPORTER_COUNTRY}))
					{
						summaryReporterCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.REPORTER_COUNTRY).get(0).displayValue());
					}
					else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.CONSUMER_COUNTRY}))
					{
						summaryReporterCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.CONSUMER_COUNTRY).get(0).displayValue());
					}
					
					
					// Country where AE Occured
					if(!StringUtils.isNullOrEmpty(summaryCountryAEOccured) && !StringUtils.isNullOrEmpty(sceptreCountryAEOccured))
					{
						if(!summaryCountryAEOccured.equalsIgnoreCase(sceptreCountryAEOccured))
						{
							payload.put("USER_MSG", "SD Summary 'Country where AE Occured' does not match with Sceptre");
							isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
							
							if(isRegisterActivitySuccess)
							{
								JOptionPane.showMessageDialog(null, "SD Summary 'Country where AE Occured' does not match with Sceptre");
								
								closeWriterDriver();
								oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
							}
							
							return false;
						} 	 			
					}
					
					// Country specific approval list
					if(!sceptreCountrySpecificApprovalNosList.isEmpty() && !StringUtils.isNullOrEmpty(summaryProductCode))
					{
						if(!sceptreCountrySpecificApprovalNosList.contains(getProductCode(summaryProductCode)))
						{
							payload.put("USER_MSG", "SD Summary 'Product Code' does not belong to Country Specific Approval Nos of Sceptre");
							isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
							
							if(isRegisterActivitySuccess)
							{
								JOptionPane.showMessageDialog(null, "SD Summary 'Product Code' does not belong to Country Specific Approval Nos of Sceptre");
								
								closeWriterDriver();
								oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
							}
							
							return false;
						} 	 			
					}
					
					
					// Reporter Country
					if(!StringUtils.isNullOrEmpty(summaryReporterCountry) && !StringUtils.isNullOrEmpty(sceptreReporterCountry))
					{
						if(!summaryReporterCountry.equalsIgnoreCase(sceptreReporterCountry))
						{
							payload.put("USER_MSG", "SD Summary 'Reporter Country' does not match with Sceptre");
							isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
							
							if(isRegisterActivitySuccess)
							{
								JOptionPane.showMessageDialog(null, "SD Summary 'Reporter Country' does not match with Sceptre");
								
								closeWriterDriver();
								oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);					
							}
							
							return false;
						} 	 			
					}
					
				}
			}	
		}
 		
		return true;
	}
	public static int getDaysFromYear(int p_sYear)
	{
		return p_sYear * 365 ;
	}
	
	public static int getDaysFromMonth(int p_sMonth)
	{
		return p_sMonth * 30 ;
	}
	
	/**
	 * By Shambhu
	 * ValidateDrugTab
	 * @param oToolBase
	 * @param parsedDataMap
	 * @param summaryDataTable
	 * @throws SciException 
	 */
	private static void validateDrugTab(CpatToolBase oToolBase, Map<String, String> parsedDataMap,
			ParsedDataTable summaryDataTable) throws SciException 
	{
		putProductCodeAndDetailsInDataMap(summaryDataTable, parsedDataMap , whiteListValuesMap);
		validateDrugTabCommon(oToolBase, parsedDataMap, summaryDataTable, whiteListValuesMap);
		if(caseType.equals(CpatToolBConstants.CaseType.INITIAL))
		{
			validateDrugTabForInitial(oToolBase, parsedDataMap, summaryDataTable, whiteListValuesMap);
		} 
		
	}
	
	public static boolean isNoNoneBlankCheck(String text)
	{
		boolean isCheckPass = false;
		
		text = text.trim();
		
		if(text.equalsIgnoreCase("NO") || text.equalsIgnoreCase("NONE") || StringUtils.isNullOrEmpty(text))
		{
			isCheckPass = true;
		}
		
		return isCheckPass;
		
	}
	
	private static String displayNoNoneBlankErrorMessage(String key, String value, String sdName)
	{			
		return "Cannot process case because '" + key + "' = " + value + " for source document: " + sdName;
	}

	
	private static void performBusinessLogic(CpatToolBase oToolBase, List<PreviewCaseDoc> listParsedCaseDocs, ParsedDataTable summaryDataTable,
			Map<String, String> sceptreMap) throws SciException 
	{
		List<CpatField> listCpatFieldsMapping = null; 
				
		listCpatFieldsMapping = oToolBase.s_mapCpatFieldsFromXml.getFields();
		
		// Convert Data Table into Map with updated Keys
		for (int i = 0; i < summaryDataTable.colCount(); i++) 
		{
			for (CpatField cpatField : listCpatFieldsMapping) 
			{
				if (summaryDataTable.getMnemonic(i).equals(cpatField.getSdMnemonic())) 
				{
					if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{summaryDataTable.getMnemonic(i)}))
					{
						sceptreMap.put(cpatField.getWriterMnemonic(), summaryDataTable.getColumnValues(i).get(0).displayValue().toString());
					}
				}
			}
		}
		
		validateMapValues(oToolBase, listParsedCaseDocs, sceptreMap, summaryDataTable , whiteListValuesMap);
		
	}
	
	private static String getSuspectProductNameFromSearchResults(SearchResults oResults, String caseId,
			String caseVersion) 
	{
		String suspectProductName = null;
		
		String srAerNo = null;
		String srCaseVersion = null;
		
		for(int i=0; i < oResults.resultCount(); i++)
		{
			srAerNo = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER));
			srCaseVersion = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.VERSION));
			
			if(caseId.equals(srAerNo) && caseVersion.equals(srCaseVersion))
			{
				suspectProductName = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.SUSPECTPRODUCT));
				break;
			}
		}
				
		return suspectProductName;
	}
	
	private static List<String> getPotentialDuplicateAerNumbers(String caseId, String caseVersion, String caseProductName, Map<SceptreSearchFields, Object> mapCriteria, SearchResults oResults, Map<String, String> sceptreMap, ParsedDataTable summaryDataTable) 
	{
		List<String> potentialDupAerList = null;
		
		Set<String> potentialDupAerSet = null;
		
		String caseSuspectProductNameLike = null;
		
		String srAerNo = null;
		String srCaseVersion = null;
		String srSuspectProduct = null;
		
		String caseReporterGivenName = null;
		String caseReporterFamilyName = null;
		String caseReporterCountry = null;
		String casePatientInitials = null;
		String casePatientDOB = null;
		
		potentialDupAerSet = new HashSet<>();
		
		caseSuspectProductNameLike = caseProductName.substring(0, (caseProductName.length()*75)/100);
		
		// Search Criteria 1: Local Case Id
		for(int i = 0; i < oResults.resultCount(); i++)
		{
			srAerNo = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER));
			
			if(!srAerNo.equals(caseId))
			{
				srCaseVersion = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.VERSION));
				srSuspectProduct = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.SUSPECTPRODUCT));
				
				if(srSuspectProduct.toUpperCase().contains(caseSuspectProductNameLike.toUpperCase()) && srCaseVersion.equals(caseVersion))
				{
					potentialDupAerSet.add(srAerNo);
				}
			}
		}
		
		if(propMap.containsKey(CpatToolBConstants.CPAT_SEARCHCRITERIA_2))
		{
			if(propMap.get(CpatToolBConstants.CPAT_SEARCHCRITERIA_2).equalsIgnoreCase("true"))
			{
				// Search Criteria 2: Reporter Country, Reporter Family Name, Given name, DOB, Patient Initials
				oAppWriter.clearSearch();
				mapCriteria.clear();
				
				// Reporter Country
				if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.ReporterTabKeys.REPORTER_COUNTRY}))
				{
					caseReporterCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReporterTabKeys.REPORTER_COUNTRY).get(0).displayValue());
				}
				else if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.CONSUMER_COUNTRY}))
				{
					caseReporterCountry = String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.CONSUMER_COUNTRY).get(0).displayValue());
				}
				
				
				caseReporterGivenName = sceptreMap.get(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
				caseReporterFamilyName = sceptreMap.get(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
				casePatientInitials = sceptreMap.get(SceptreConstants.PatientTabKeys.PATIENT_ID);
				casePatientDOB = sceptreMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH);
				
				if(!StringUtils.isNullOrEmpty(casePatientInitials))
				{
					if(casePatientInitials.equalsIgnoreCase("PRIVATE"))
					{
						casePatientInitials = StringConstants.EMPTY;
					}
				}
				
				if(!StringUtils.isNullOrEmpty(caseReporterCountry))
				{
					
					if(!StringUtils.isNullOrEmpty(caseReporterGivenName) || !StringUtils.isNullOrEmpty(caseReporterFamilyName) || !StringUtils.isNullOrEmpty(casePatientInitials) || !StringUtils.isNullOrEmpty(casePatientDOB))
					{
						mapCriteria.put(SceptreConstants.SceptreSearchFields.SHOW_DELETED_CASES, "yes");
						mapCriteria.put(SceptreConstants.SceptreSearchFields.COUNTRY, caseReporterCountry);
						
						if(!StringUtils.isNullOrEmpty(caseReporterGivenName))
						{
							mapCriteria.put(SceptreConstants.SceptreSearchFields.REPORTER_GIVENNAME, caseReporterGivenName);
						}
						
						if(!StringUtils.isNullOrEmpty(caseReporterFamilyName))
						{
							mapCriteria.put(SceptreConstants.SceptreSearchFields.REPORTER_FAMILYNAME, caseReporterFamilyName);
						}
						
						if(!StringUtils.isNullOrEmpty(casePatientInitials))
						{
							mapCriteria.put(SceptreConstants.SceptreSearchFields.PATIENT_INITIALS, casePatientInitials);
						}
						
						if(!StringUtils.isNullOrEmpty(casePatientDOB))
						{
							mapCriteria.put(SceptreConstants.SceptreSearchFields.PATIENT_DOB1, casePatientDOB);
						}
						
					}
					
					oResults = oAppWriter.search(mapCriteria);
					
					for(int i = 0; i < oResults.resultCount(); i++)
					{
						srAerNo = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER));
						srCaseVersion = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.VERSION));
						srSuspectProduct = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.SUSPECTPRODUCT));
						
						if(!srAerNo.equals(caseId))
						{
							if(srSuspectProduct.toUpperCase().contains(caseSuspectProductNameLike.toUpperCase()) && srCaseVersion.equals(caseVersion))
							{
								potentialDupAerSet.add(srAerNo);
							}
						}
					}
				}
			}
		}
		
		
		potentialDupAerList = new ArrayList<>(potentialDupAerSet);
		
		return potentialDupAerList;
	}


/***********code by shambhu************/
		
	private static void getSetOFOtherIDNumberFromParsedData(ParsedDataTable summaryDataTable)
	{
		SetOfOtherIdentificationNumbersFromSD = new HashSet<>();
		
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.OtherIdentificationNumber.EXTERNAL_SOURCENUMBER}))
		{
			for(String otherIdNumber :getSetOfValuesOfGivenLineByCommaSeparator(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.OtherIdentificationNumber.EXTERNAL_SOURCENUMBER).get(0).displayValue())))
			{
				SetOfOtherIdentificationNumbersFromSD.add(otherIdNumber);
			}
			
		}
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.OtherIdentificationNumber.DUPLICATE_NUMBER}))
		{
			for(String otherIdNumber :getSetOfValuesOfGivenLineByCommaSeparator(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.OtherIdentificationNumber.DUPLICATE_NUMBER).get(0).displayValue())))
			{
				SetOfOtherIdentificationNumbersFromSD.add(otherIdNumber);
			}
		}
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.OtherIdentificationNumber.GCC_PLATFORM_CASENUMBER}))
		{
			for(String otherIdNumber :getSetOfValuesOfGivenLineByCommaSeparator(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.OtherIdentificationNumber.GCC_PLATFORM_CASENUMBER).get(0).displayValue())))
			{
				SetOfOtherIdentificationNumbersFromSD.add(otherIdNumber);
			}
		}
		if(isKeyContainsNonAcceptableValue(summaryDataTable, new String[]{CpatToolBConstants.OtherIdentificationNumber.LINKED_REPORT_NO}))
		{
			for(String otherIdNumber :getSetOfValuesOfGivenLineByCommaSeparator(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.OtherIdentificationNumber.LINKED_REPORT_NO).get(0).displayValue())))
			{
				SetOfOtherIdentificationNumbersFromSD.add(otherIdNumber);
			}
		}
	}
	
	private static List<String> getNotMatchedOtherIDList(List<String> p_sceptreOtherIDNumbersList)
	{
		boolean bOtherIdentificationMatches = false;
		
		List<String> sceptreAndSDNotMatchsOtherIDNumbersList = null;

		List<String> sceptreAndSDMatchsOtherIDNumbersList = null;
		
		sceptreAndSDNotMatchsOtherIDNumbersList = new ArrayList<>();
		
		sceptreAndSDMatchsOtherIDNumbersList = new ArrayList<>();
		
		if(SetOfOtherIdentificationNumbersFromSD != null)
		{
			for(String sSDOtherIDNumber : SetOfOtherIdentificationNumbersFromSD)
			{
				bOtherIdentificationMatches = false;
				
				for(String sceptreOtherIDNumber : p_sceptreOtherIDNumbersList)
				{
					if(sceptreOtherIDNumber.equals(sSDOtherIDNumber))
					{
						sceptreAndSDMatchsOtherIDNumbersList.add(sSDOtherIDNumber);
						
						bOtherIdentificationMatches = true;
						
						break;
					}
				}
				
				if(!bOtherIdentificationMatches)
				{
					sceptreAndSDNotMatchsOtherIDNumbersList.add(sSDOtherIDNumber);
				}
			}
		}
		return sceptreAndSDNotMatchsOtherIDNumbersList;
		
	}
	
	private static List<String> getLinkedCaseAERNumberFromSceptre()
	{
		SearchResults oSearchResults = null;
		
		oSearchResults = oAppWriter.fetchLinkedNumber();
		
		List<String> listAERNumber = null;

		listAERNumber = new ArrayList<>();
		
		if(oSearchResults.resultsFound())
		{
			
			for(int i=0; i<oSearchResults.resultCount(); i++)
			{
				listAERNumber.add(String.valueOf(oSearchResults.getValue(i, SceptreConstants.Name.AER_NUMBER)));
			}

		}
		return listAERNumber;
	}
	

	private static List<String> getNotMatchedAERNumberList(List<String> p_sceptreAERNumberList)
	{
		boolean bOtherIdentificationMatches = false;
		
		List<String> sceptreAndSDNotMatchsAERNumberList = null;

		List<String> sceptreAndSDMatchsAERNumbersList = null;
		
		sceptreAndSDNotMatchsAERNumberList = new ArrayList<>();
		
		sceptreAndSDMatchsAERNumbersList = new ArrayList<>();
		
		if(setOfLinkedCasesFromSD != null)
		{
			for(String sSDAERNumber : setOfLinkedCasesFromSD)
			{
				bOtherIdentificationMatches = false;
				
				for(String sceptreAERNumber : p_sceptreAERNumberList)
				{
					if(sceptreAERNumber.equals(sSDAERNumber))
					{
						sceptreAndSDMatchsAERNumbersList.add(sSDAERNumber);
						
						bOtherIdentificationMatches = true;
						
						break;
					}
				}
				
				if(!bOtherIdentificationMatches)
				{
					sceptreAndSDNotMatchsAERNumberList.add(sSDAERNumber);
				}
			}
		}
		return sceptreAndSDNotMatchsAERNumberList;

	}
	
	
	private static Set<String> getSetOfValuesOfGivenLineByCommaSeparator(String p_sString)
	{
		String[] sarr_Numebers = null;
		
		Set<String> setOfNumber = null;

		setOfNumber = new HashSet<>();
		
		if(p_sString.contains(","))
		{
			sarr_Numebers = p_sString.split(",");
		}
		
		if(sarr_Numebers != null)
		{
			if(sarr_Numebers.length > 0)
			{
				
				for(String oneNumber : sarr_Numebers)
				{
					setOfNumber.add(oneNumber.trim());
				}
			}
			else
			{
				setOfNumber .add(p_sString);
			}
		}
		else
		{
			setOfNumber .add(p_sString);
		}
		
		return setOfNumber;
	}

	private static String getCommaSeparatedStringFromList(List<String> p_sListString)
	{
		StringBuilder builder = null;
		builder = new StringBuilder();
		
		for(String numberList : p_sListString)
		{
			builder.append(numberList);
			builder.append(",");
		}
		return builder.toString();
	}
	private static void putValuesToResultScreen(CpatToolBase p_oToolBase , List<String> p_sListString)
	{
		StringBuilder builder = null;
		builder = new StringBuilder();
		
		for(String numberList : p_sListString)
		{
			builder.append(numberList);
		}
		if(!StringUtils.isNullOrEmpty(builder.toString()))
		{
			p_oToolBase.resultScreenMap.put(CpatToolBConstants.OtherIdentificationNumber.OTHER_ID_NUMBER_LIST, builder.toString());
		}
	}
	private static List<String> performMedicalHistoryTab(Map<String , String> p_parsedDataMap)
	{
		Map<String , Object> mapOfSceptreValues = null;
		List<String> readMedicalHistoryFieldsList = null , listFieldsPopulatedForMedicalHistoryTab = null ;
		String sReportedDisease = null;
		
		mapOfSceptreValues = new HashMap<>();
		readMedicalHistoryFieldsList = new ArrayList<>();
		
		readMedicalHistoryFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE);

		mapOfSceptreValues = oAppWriter.getValuesOfListFields(SceptreConstants.MedicalHistoryTabKeys.TAB, readMedicalHistoryFieldsList);
		
		sReportedDisease = (String) mapOfSceptreValues.get(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE);

		if(StringUtils.isNullOrEmpty(sReportedDisease))
		{
			listFieldsPopulatedForMedicalHistoryTab = oAppWriter.feedMedicalHistoryTab(p_parsedDataMap);
		}
		else
		{
			listFieldsPopulatedForMedicalHistoryTab = oAppWriter.addNewMedicalHistory(p_parsedDataMap);
		}
		
		return listFieldsPopulatedForMedicalHistoryTab;
	}
	
	private static List<String> performDrugHistoryTab(Map<String , String> p_parsedDataMap)
	{
		Map<String , Object> mapOfSceptreValues = null;
		List<String> readDrugHistoryFieldsList = null , listFieldsPopulatedForDrugHistoryTab = null ;
		String sReportedDrug = null;
		
		mapOfSceptreValues = new HashMap<>();
		readDrugHistoryFieldsList = new ArrayList<>();
		
		readDrugHistoryFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);

		mapOfSceptreValues = oAppWriter.getValuesOfListFields(SceptreConstants.DrugHistoryTabKeys.TAB, readDrugHistoryFieldsList);
		
		sReportedDrug = (String) mapOfSceptreValues.get(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);

		if(StringUtils.isNullOrEmpty(sReportedDrug))
		{
			listFieldsPopulatedForDrugHistoryTab = oAppWriter.feedDrugHistory(p_parsedDataMap);
		}
		else
		{
			listFieldsPopulatedForDrugHistoryTab = oAppWriter.addNewDrugHistory(p_parsedDataMap);
		}
		
		return listFieldsPopulatedForDrugHistoryTab;
	}
	
	private static  List<String> getFollowUpMnemonicList(List<PreviewCaseDoc> listPreviewCaseDocs, String version)
	{
		List<String> followUpMnemonicList = null;
		List<PreviewCaseDoc> previousDocList = null;
		ParsedDataTable currentSummaryDataTable = null;
		ParsedDataTable previousSummaryDataTable = null;
		FileParsingConfiguration oParsingConfiguration = null;
		int caseVersion = 0,
			currentVersion = 0;
		int maxCount = 1;
		int keyCount = 1;
		currentVersion = Integer.parseInt(version);
		String[] collationMnemonic = propMap.get(CpatToolBConstants.COLLATION_KEY_LIST).split(SciConstants.StringConstants.COMMA);
	
		
		previousDocList = new ArrayList<>();
		
		oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AA_KEY);
		
		for(PreviewCaseDoc doc : listPreviewCaseDocs)
		{
			caseVersion = Integer.parseInt(doc.getCaseDocVersion());
			
			if(caseVersion < currentVersion)
			{
				previousDocList.add(doc);
			}
			
		}
		
		currentSummaryDataTable = mergeAllCaseDocs(listPreviewCaseDocs);
		previousSummaryDataTable =  mergeAllCaseDocs(previousDocList);
		
		followUpMnemonicList = new ArrayList<>();
		if(previousDocList!= null && !previousDocList.isEmpty())
		{
			for(String colMnemonic : collationMnemonic)
			{
				//Check for multiple values
				if (!oParsingConfiguration.getField(colMnemonic.trim()).isMultivalued()) 
				{
					if(currentSummaryDataTable.isColumnPresent(colMnemonic.trim()+"_Count"))
					{
						keyCount =Integer.parseInt(currentSummaryDataTable.getColumnValues(colMnemonic.trim()+"_Count").get(0).displayValue().toString());
						if(maxCount < keyCount)
						{
							maxCount = keyCount;
						}
					}
					for(int index =1; index<= maxCount; index++)
					{
						checkForCollatedDataDiff(currentSummaryDataTable, previousSummaryDataTable, colMnemonic.trim()+"_"+index, followUpMnemonicList);
					}
				}
				else {
					checkForCollatedDataDiff(currentSummaryDataTable, previousSummaryDataTable, colMnemonic.trim(), followUpMnemonicList);
				}
			}
		}
		return followUpMnemonicList;
	}
	
	private static void checkForCollatedDataDiff(ParsedDataTable currentSummaryDataTable, 
	ParsedDataTable previousSummaryDataTable, String mnemonic, List<String> followUpMnemonicList )
	{
		if(previousSummaryDataTable.isColumnPresent(mnemonic) &&
				currentSummaryDataTable.isColumnPresent(mnemonic)) {
			if(!previousSummaryDataTable.getColumnValues(mnemonic).get(0).displayValue().equals
					(currentSummaryDataTable.getColumnValues(mnemonic).get(0).displayValue()))
			{
				followUpMnemonicList.add(mnemonic);
			}
		}else
		if(previousSummaryDataTable.isColumnPresent(mnemonic) ||
				currentSummaryDataTable.isColumnPresent(mnemonic))
		{
			followUpMnemonicList.add(mnemonic);
		}
	}

	@SuppressWarnings("unchecked")
	private static List<String> performDrugTab(Map<String , String> p_parsedDataMap)
	{
		String sGenericName = null , sFormulation = null;
		
		
		List<String> listFieldsPopulatedForDrugTab = null ;
		
		if(caseType.equals(CpatToolBConstants.CaseType.INITIAL))
		{
			if(p_parsedDataMap != null)
			{
				sGenericName = p_parsedDataMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_GENERICNAME);
				sFormulation = p_parsedDataMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTFORM);
				
				if (sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.GENERICDRUGNAME).equals(sGenericName)
						&& sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.DOSAGE_FORM).equals(sFormulation)) 
				{
					listFieldsPopulatedForDrugTab = oAppWriter.feedDrugTab(p_parsedDataMap);
					
					acceptRequestAlertIfDataChanged(p_parsedDataMap);
					
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Drug Code does not match. Tool will delete the drug and add the new details");
					
					oAppWriter.deletedrug(String.valueOf(sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME)));
					
					if(!p_parsedDataMap.containsKey(SceptreConstants.DrugTabKeys.ACTIONTAKEN))
					{
						p_parsedDataMap.put(SceptreConstants.DrugTabKeys.ACTIONTAKEN, SceptreConstants.Name.UNKNOWN);
					}
					
					listFieldsPopulatedForDrugTab=oAppWriter.addNewDrug(p_parsedDataMap);
				}
				
				updateInidicationIfError(p_parsedDataMap);

				acceptRequestAlertIfDataChanged(p_parsedDataMap);
				
				oAppWriter.drugCountrySpecificApproval(p_parsedDataMap.get(SceptreConstants.Name.PRODUCTCODE));
			
				if(!StringUtils.isNullOrEmpty((String)sceptreTabValues.get(SceptreConstants.ReactionTabKeys.TAB).get(SceptreConstants.ReactionTabKeys.REPORTED_REACTION)))
				{
					oAppWriter.performDrugSubTab();
				}
			}
		}
		else
		{
			listFieldsPopulatedForDrugTab = oAppWriter.feedDrugTab(p_parsedDataMap);
			
			acceptRequestAlertIfDataChanged(p_parsedDataMap);
			
			updateInidicationIfError(p_parsedDataMap);

			if(!StringUtils.isNullOrEmpty((String)sceptreTabValues.get(SceptreConstants.ReactionTabKeys.TAB).get(SceptreConstants.ReactionTabKeys.REPORTED_REACTION)))
			{
				oAppWriter.performDrugSubTab();
			}
		}
		
		return listFieldsPopulatedForDrugTab;
	}
		
	public static void showPreviewScreen(CpatToolB oToolBase, IAction oCallbackAction, Map<String, Object> params) 
	{
		Map<String, String> payload = null;
		
		try
		{
			params.put(CpatToolConstants.ContentParams.FUP_NEWVAL_LIST, fupNewFieldsMnemonicList);
			
			CpatToolHome.showPreviewScreen(oToolBase, oCallbackAction, params);			
		}
		catch(Exception e)
		{
			log.error("Preview Screen cannot be loaded", null, e);
			
			payload = new HashMap<>();
			payload.put("USER_MSG", "Locate Case Failed. Please check logs");
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.PREVIEW, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.PREVIEW, "", payload);
			
			JOptionPane.showMessageDialog(null, "Preview Screen cannot be loaded");
			closeWriterDriver();
			
		}
	}

	@SuppressWarnings("unchecked")
	private static List<String> performCaseIDTab(CpatToolBase oToolBase , Map<String , String> p_parsedDataMap)
	{

		List<String> listFieldsPopulatedForCaseIdTab = null , sceptreOtherIDNumbersList = null , sceptreAndSDNotMatchsOtherIDNumbersList = null
					, sceptreLinkedCasesList = null , sceptreAndSDNotMatchsAERNumbersList = null , listFieldsToFetch = null , resultCaseCharacteristics = null;
		
		Map<String , Object> mapOfFieldsFromSceptre = null;
		
		listFieldsToFetch = new ArrayList<>();
		
		//Case Characteristics has to be call after or before save caseid 
		if(caseType.equalsIgnoreCase(CpatToolBConstants.CaseType.INITIAL))
		{
			oAppWriter.caseCharacteristicsLookUp(SceptreConstants.Name.MEDICALLY_IMPORTANT, CpatToolBConstants.CaseType.INITIAL);
		}
		else
		{
			 if(fupNewFieldsMnemonicList != null)
			{
				if(fupNewFieldsMnemonicList.size() == 0)
				{
					oAppWriter.caseCharacteristicsLookUp(SceptreConstants.Name.MEDICALLY_IMPORTANT, CpatToolBConstants.CaseType.FOLLOWUP);
				}
				else
				{
					oAppWriter.caseCharacteristicsLookUp(SceptreConstants.Name.MEDICALLY_IMPORTANT, CpatToolBConstants.CaseType.INITIAL);
				}
			}
			else
			{
				oAppWriter.caseCharacteristicsLookUp(SceptreConstants.Name.MEDICALLY_IMPORTANT, CpatToolBConstants.CaseType.FOLLOWUP);
			}
		}	
		
		//for regulatory relevent update fetch CC selected values
		listFieldsToFetch.add(SceptreConstants.CaseIdTabKeys.LOOKUP_CCSELECT);
		
		mapOfFieldsFromSceptre = oAppWriter.getValuesOfListFields(SceptreConstants.CaseIdTabKeys.CASEID_TAB, listFieldsToFetch);
		
		//fetch list of CC values
		resultCaseCharacteristics =(List<String>) mapOfFieldsFromSceptre.get(SceptreConstants.CaseIdTabKeys.LOOKUP_CCSELECT);
		
		if(resultCaseCharacteristics !=null)
		{
			//if medically important then regulatory relevent update should be yes else no
			if(resultCaseCharacteristics.contains(SceptreConstants.Name.MEDICALLY_IMPORTANT))
			{
				p_parsedDataMap.put(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE, "Yes");
			}
			else
			{
				p_parsedDataMap.put(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE, "No");
			}
		}
		else
		{
			p_parsedDataMap.put(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE, "No");
		}
		
		//feeding case id tab
		listFieldsPopulatedForCaseIdTab = oAppWriter.feedCaseIdTab(p_parsedDataMap);
		
		//get other id list from sceptre to check with sd other id list 
		sceptreOtherIDNumbersList = new ArrayList<>((Set<String>) sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB).get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_CASEIDNUMBER));
		
		/**
		 * getNotMatchedOtherIDList have SD other id list to check with
		 * sceptre Other id list, it will return not matched id number list after
		 * compare with sceptre other ids
		 */
		sceptreAndSDNotMatchsOtherIDNumbersList = getNotMatchedOtherIDList(sceptreOtherIDNumbersList);
		
		if(sceptreAndSDNotMatchsOtherIDNumbersList != null)
		{
			if(sceptreAndSDNotMatchsOtherIDNumbersList.size() > 0)
			{
				oToolBase.resultScreenMap.put(CpatToolConstants.ResultScreen.PENDING_OTHER_IDENTIFICATION_NOS, getCommaSeparatedStringFromList(sceptreAndSDNotMatchsOtherIDNumbersList));
			}
		}

		//get list of linked case from scepter from Search result 
		sceptreLinkedCasesList = getLinkedCaseAERNumberFromSceptre();
		/**
		 * getNotMatchedAERNumberList have Set of SD AER Numnber to check with
		 * sceptre AER Number list, it will return not matched AER number list after
		 * compare with sceptre AER Number
		 */
		sceptreAndSDNotMatchsAERNumbersList = getNotMatchedAERNumberList(sceptreLinkedCasesList);
		
		if(sceptreAndSDNotMatchsAERNumbersList != null)
		{
			if(sceptreAndSDNotMatchsAERNumbersList.size() > 0)
			{
				oToolBase.resultScreenMap.put(CpatToolConstants.ResultScreen.PENDING_LINKED_CASE_NOS, getCommaSeparatedStringFromList(sceptreAndSDNotMatchsAERNumbersList));
			}
		}
		
		return listFieldsPopulatedForCaseIdTab;
	}
	
	private static String getFirstDocValOfCurrentVersion(CpatToolBase oToolBase, List<PreviewCaseDoc> listParsedCaseDocs,
			String key) 
	{
		String currentCaseVersion = null;
		List<PreviewCaseDoc> currentVersionDocList = null;
		String value = null;
		
		currentCaseVersion = oToolBase.userInputsParams.get(CpatToolBConstants.CaseDetails.CASE_VERSION);
		currentVersionDocList = new ArrayList<>();
		
		for(PreviewCaseDoc doc: listParsedCaseDocs)
		{
			if(doc.getCaseDocVersion().equals(currentCaseVersion))
			{
				currentVersionDocList.add(doc);				
			}
		}
		
		Collections.sort(currentVersionDocList, new CaseDocumentComparator());
		
		if(currentVersionDocList.get(0).getDataTable().isColumnPresent(key))
		{
			value = String.valueOf(currentVersionDocList.get(0).getDataTable().getColumnValues(key).get(0).displayValue());			
		}
		else
		{
			value = "";
		}
		
		return value;
	}
	
	public static void closeWriterDriver()
	{
		if(oAppWriter != null)
		{
			oAppWriter.closeWebDriver();
		}
		
		CpatCasePreviewScreen.disposePreviewScreenView();
	}
	
	private static boolean isValidNumber(String sVal)
	{
		boolean isNumber = false;
		
		try
		{	
			Integer.parseInt(sVal);	
			isNumber = true;
		}
		catch(NumberFormatException excep)
		{
			// Do Nothing
		}
		
		return isNumber;
	}
	
	private static void loadSDReactionKeywordsInResultScreen(CpatToolBase oToolBase, ParsedDataTable summaryDataTable)
	{
		List<String> reactionKeyWordList = null;
		List<String> reactionsFoundList = null;
		List<String> narrativeKeys = null;
		
		String reactionsFound = null;
		String parsedValue = null;
		
		reactionsFoundList = new ArrayList<>();
		narrativeKeys = new ArrayList<>();
		reactionKeyWordList = Arrays.asList(propMap.get(CpatToolBConstants.PropertiesMap.REACTION_KEYWORDS).split(StringConstants.COMMA));
		
		for(int i = 0; i < summaryDataTable.colCount(); i++)
	    {
	    	if(summaryDataTable.getMnemonic(i).startsWith(CpatToolBConstants.NarrativeKeys.CASE_NOTES_ENGLISH_LANGUAGE))
	    	{
	    		if(!StringUtils.isNullOrEmpty(String.valueOf(summaryDataTable.getColumnValues(i).get(0).displayValue())))
	    		{
	    			narrativeKeys.add(summaryDataTable.getMnemonic(i));
	    		}
	    	}
	    	
	    	if(summaryDataTable.getMnemonic(i).equals(CpatToolBConstants.NarrativeKeys.VERBATIM))
	    	{
	    		if(!StringUtils.isNullOrEmpty(String.valueOf(summaryDataTable.getColumnValues(i).get(0).displayValue())))
	    		{
	    			narrativeKeys.add(summaryDataTable.getMnemonic(i));
	    		}
	    	}
	    }
		
		for(String key: narrativeKeys)
	    {
	    	for(String reaction: reactionKeyWordList)
	    	{
	    		parsedValue = String.valueOf(summaryDataTable.getColumnValues(key).get(0).displayValue());
	    		if(CpatToolHome.isKeywordFound(parsedValue, reaction))
	    		{
	    			reactionsFoundList.add(reaction);
	    		}
	    	}
	    }
		
		reactionsFound = getCommaSeparatedStringFromList(reactionsFoundList);
		
		oToolBase.resultScreenMap.put(CpatToolConstants.ResultScreen.REACTION_KEYWORD_FOUND, reactionsFound);
		
	}

	private static final List<String> formats ()
	{
		List<String> listFormate = null;
			
		listFormate = new ArrayList<>();
/*
		listFormate.add("dd:MM:yyyy");
		listFormate.add("dd/MM/yyyy");
		listFormate.add("dd-MM-yyyy");*/
		listFormate.add("dd-MMM-yyyy");
//		listFormate.add("dd/MMM/yyyy");
//		listFormate.add("dd:MMM:yyyy");
//		listFormate.add("ddMMyyyy");
//		listFormate.add("ddMMMyyyy");
		
		return listFormate;
	}

	public static Date parse(String p_sDateString)
	{
		SimpleDateFormat sdf = null;
		
		Date oDate = null;
		
		boolean bIsParsedSuccess = false;
		
		if (p_sDateString != null) 
		{
			for (String parse : formats()) 
			{
				sdf = new SimpleDateFormat(parse);
				
				try 
				{
					oDate = sdf.parse(p_sDateString);
		
					System.out.println("Printing the value of " + parse);
				
					bIsParsedSuccess = true;
					
				} catch (ParseException e) 
				{
					continue;
				}
				
				if(bIsParsedSuccess)
				{
					break;
				}
			}
		}
		return oDate;
	}
	
	private static boolean isNullEmptyORNonAcceptable(Map<String, String> p_parsedDataMap , String p_sStringValue)
	{
		if (StringUtils.isNullOrEmpty(p_parsedDataMap.get(p_sStringValue))
				|| p_parsedDataMap.get(p_sStringValue).equalsIgnoreCase("None")
				|| p_parsedDataMap.get(p_sStringValue).equalsIgnoreCase("No")
				|| p_parsedDataMap.get(p_sStringValue).equalsIgnoreCase("NA")
				|| p_parsedDataMap.get(p_sStringValue).equalsIgnoreCase("unknown")
				|| p_parsedDataMap.get(p_sStringValue).equalsIgnoreCase("N/A")
				|| p_parsedDataMap.get(p_sStringValue).equalsIgnoreCase("Not Applicable")
				|| p_parsedDataMap.get(p_sStringValue).equalsIgnoreCase("NI")) 
		{
			return true;
		}
		
		return false;
	}

	private static void updateInidicationIfError(Map<String , String> p_parsedDataMap)
	{
		String sIndicationFromSD = null , sLLTFromSceptre = null;
		boolean isErrorContainsIndication = false;
		List<String> readLLTIndication = null;
		Map<String ,Object> lltValue = null;
		
		readLLTIndication = new ArrayList<>();
		lltValue = new HashMap<>();
		
		if(p_parsedDataMap != null)
		{
			 sIndicationFromSD = p_parsedDataMap.get(SceptreConstants.DrugTabKeys.REPORTED_INDICATION);
			
			if (!sIndicationFromSD.equalsIgnoreCase(CpatToolBConstants.DrugTabKeys.DRUG_USE_FOR_UNKNOWN_INDICATION)
					&& !sIndicationFromSD.equalsIgnoreCase(CpatToolBConstants.DrugTabKeys.PRODUCT_USED_FOR_UNKNOWN_INDICATION))
			{
				isErrorContainsIndication = oAppWriter.isErrorContainsKeyWord(sIndicationFromSD);
				
				if(isErrorContainsIndication)
				{
					readLLTIndication.add(SceptreConstants.DrugTabKeys.LOW_LEVEL_TERM);
					
					oAppWriter.readData(lltValue, readLLTIndication);
					
					sLLTFromSceptre = (String) lltValue.get(SceptreConstants.DrugTabKeys.LOW_LEVEL_TERM);
					
					oAppWriter.modifyIndication(sLLTFromSceptre);
					
				}
			}
		}
	}

	private static boolean validateNarrativeDateFields(CpatToolBase oToolBase, IAction oCallbackAction,
			Map<String, String> payload, ParsedDataTable summaryDataTable) 
	{
		boolean isRegisterActivitySuccess = false;
		
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_START_DATE} ))
		{
			if(parse(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_START_DATE).get(0).displayValue())) == null)
			{
				payload.put("USER_MSG", "SD Summary Date Format For 'Product start date' is Not Supported");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "SD Summary Date Format For 'Product start date' is Not Supported");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					
				}
				return false;
			}
		}
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE} ))
		{
			if(parse(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_STOP_DATE).get(0).displayValue())) == null)
			{
				payload.put("USER_MSG", "SD Summary Date Format For 'Product stop date' is Not Supported");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "SD Summary Date Format For 'Product stop date' is Not Supported");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				return false;
			}
		}
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_EXPIRY_DATE} ))
		{
			if(parse(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.DrugTabKeys.NARRATIVE_PRODUCT_EXPIRY_DATE).get(0).displayValue())) == null)
			{
				payload.put("USER_MSG", "SD Summary Date Format For 'Product Expiry date' is Not Supported");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "SD Summary Date Format For 'Product Expiry date' is Not Supported");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				return false;
			}
		}
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.PATIENT_DOB} ))
		{
			if(parse(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.PATIENT_DOB).get(0).displayValue())) == null)
			{
				payload.put("USER_MSG", "SD Summary Date Format For 'Patient DOB' is Not Supported");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "SD Summary Date Format For 'Patient DOB' is Not Supported");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				return false;
			}
		}
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE} ))
		{
			if(parse(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_START_DATE).get(0).displayValue())) == null)
			{
				payload.put("USER_MSG", "SD Summary Date Format For 'Reaction Start Date' is Not Supported");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "SD Summary Date Format For 'Reaction Start Date' is Not Supported");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				return false;
			}
		}
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_STOPDATE} ))
		{
			if(parse(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.ReactionTabKeys.NARRATIVE_REACTION_STOPDATE).get(0).displayValue())) == null)
			{
				payload.put("USER_MSG", "SD Summary Date Format For 'Reaction Stop Date' is Not Supported");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "SD Summary Date Format For 'Reaction Stop Date' is Not Supported");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				return false;
			}
		}
		if(isKeyContainsValue(summaryDataTable, new String[]{CpatToolBConstants.NarrativeKeys.AWARENESS_DATE} ))
		{
			if(parse(String.valueOf(summaryDataTable.getColumnValues(CpatToolBConstants.NarrativeKeys.AWARENESS_DATE).get(0).displayValue())) == null)
			{
				payload.put("USER_MSG", "SD Summary Date Format For 'Reaction Stop Date' is Not Supported");
				isRegisterActivitySuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload);
				
				if(isRegisterActivitySuccess)
				{
					JOptionPane.showMessageDialog(null, "SD Summary Date Format For 'Reaction Stop Date' is Not Supported");
					
					closeWriterDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				return false;
			}
		}
			return true;
	}

	private static void acceptRequestAlertIfDataChanged(Map<String , String> p_parsedDataMap)
	{
		String	 sSceptreMadeBy = null , sSceptreCharacterization = null , sSceptreActionTaken = null
				,sSDMadeBy = null , sSDCharacterization = null , sSDActionTaken = null; 
		
		sSceptreMadeBy = (String) sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.MADEBY);
		sSceptreCharacterization = (String) sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.CHARACTERIZATION);
		sSceptreActionTaken= (String) sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.ACTIONTAKEN);

		//Always yes bcoz company drug
		sSDMadeBy = "yes";
		sSDCharacterization = p_parsedDataMap.get(SceptreConstants.DrugTabKeys.CHARACTERIZATION);
		if(caseType.equalsIgnoreCase(CpatToolBConstants.CaseType.INITIAL))
		{
			sSDActionTaken = p_parsedDataMap.get(SceptreConstants.DrugTabKeys.ACTIONTAKEN);
		}
		
		if(caseType.equalsIgnoreCase(CpatToolBConstants.CaseType.INITIAL))
		{
			if(!sSceptreMadeBy.equalsIgnoreCase(sSDMadeBy) || !sSceptreActionTaken.equalsIgnoreCase(sSDActionTaken) || !sSceptreCharacterization.equalsIgnoreCase(sSDCharacterization))
			{
				if(oAppWriter.isAlertPresent())
				{
					oAppWriter.getAlert();
				}
			}
		}
		else
		{
			if(!sSceptreMadeBy.equalsIgnoreCase(sSDMadeBy) || !sSceptreCharacterization.equalsIgnoreCase(sSDCharacterization))
			{
				if(oAppWriter.isAlertPresent())
				{
					oAppWriter.getAlert();
				}
			}
		}
	}
	
	private static boolean genderIsNotInList(String p_sGender)
	{
		List<String> listOfGender = null;
		boolean isGenderSame = false;
		listOfGender = new ArrayList<>();
		
		listOfGender.add("Male");
		listOfGender.add("Female");
		listOfGender.add("Not Specified");
		listOfGender.add("Unknown");
		
		for(String sGender : listOfGender)
		{
			if(sGender.equalsIgnoreCase(p_sGender))
			{
				isGenderSame = true;
				break;
			}
		}
		
		return isGenderSame;
	}
	
	private static void appendDoseValues(CpatToolBase oToolBase ,Map<String,String> p_SceptreMap)
	{
		String sDose = null , sDosage = null,  sInterval = null;
		StringBuilder builder = null;
		
		builder = new StringBuilder();
		
		sDose = p_SceptreMap.get(SceptreConstants.DrugTabKeys.DOSE);
		sDosage = p_SceptreMap.get(SceptreConstants.DrugTabKeys.NUM_DOSAGES);
		sInterval = p_SceptreMap.get(SceptreConstants.DrugTabKeys.INTERVAL);
		
		if(!StringUtils.isNullOrEmpty(sDose))
		{
			builder.append(SceptreConstants.Name.DOSE);
			builder.append(":");
			builder.append(sDose);
			builder.append(";");
		}
		if(!StringUtils.isNullOrEmpty(sDosage))
		{
			if(!sDosage.contains("%"))
			{
				builder.append(SceptreConstants.Name.DOSAGE);
				builder.append(":");
				builder.append(sDosage);
				builder.append(";");
			}
		}
		if(!StringUtils.isNullOrEmpty(sInterval))
		{
			builder.append(SceptreConstants.Name.INTERVAL);
			builder.append(":");
			builder.append(sInterval);
		}
		
		if(!StringUtils.isNullOrEmpty(builder.toString()))
		{
			if(builder.toString().length() <= 99)
			{
				p_SceptreMap.put(SceptreConstants.DrugTabKeys.DOSAGE_TEXT, builder.toString());
			}
			else
			{
				oToolBase.resultScreenMap.put(CpatToolConstants.ResultScreen.PENDING_DRUG_ADDITIONALINFO,  builder.toString());
			}
		}
	}
	
	public static Map<String, List<String>> getSceptreFieldsForReading(String caseType)
	{
		Map<String, List<String>> readSceptreFieldsMap = null;
		
		readSceptreFieldsMap = new LinkedHashMap<>();
		
		if(caseType.equals(CpatToolBConstants.CaseType.INITIAL))
		{
			readSceptreFieldsForInitial(readSceptreFieldsMap);			
		}
		else
		{
			readSceptreFieldsForFUP(readSceptreFieldsMap);			
		}
		
		return readSceptreFieldsMap;
	}
	
	public static void readSceptreFieldsForInitial(Map<String, List<String>> tabWiseFieldMap)
	{
		List<String> caseIdTabFieldsList = null;
		List<String> drugTabFieldsList = null;
		List<String> reporterTabFieldsList = null;
		List<String> patientTabFieldsList = null;
		List<String> reactionTabFieldsList = null;
		List<String> drugHistoryTabFieldsList = null;
		List<String> medicalHistoryTabFieldsList = null;
		
		caseIdTabFieldsList = new ArrayList<>();
		drugTabFieldsList = new ArrayList<>();
		reporterTabFieldsList = new ArrayList<>();
		patientTabFieldsList = new ArrayList<>();
		reactionTabFieldsList = new ArrayList<>();
		drugHistoryTabFieldsList = new ArrayList<>();
		medicalHistoryTabFieldsList = new ArrayList<>();
		
		// CASE ID TAB
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.SERIOUS);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.REPORT_TYPE);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.ADVERSEEVENT);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_LOOKUP);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE);
		
		
		//Drug TAB
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.ACTIONTAKEN);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_STATUS);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_STATUS);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_START);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_END);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.REPORTED_INDICATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.BATCHLOT);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.EXPIRATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION_UNIT);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.MADEBY);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.GENERICDRUGNAME);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.CHARACTERIZATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DRUG_DURATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_RANK);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.FIRST_REACTION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.BATCH_RANK);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.INDICATION_RANK);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_FORM);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_TEXT);
		

		// Drug History
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.START);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.END);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION);
		
		
		// Medical History
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.STARTDATE);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.ENDDATE);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.CONTINUING);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT);
		
		// Patient
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.POSTAL_CODE);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.HEIGHT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.HEIGHT_UNIT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.WEIGHT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.WEIGHT_UNIT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.PATIENT_ID);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.PATIENT_SEX);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.AGE);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.AGE_UNIT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.AGE_GROUP);
		
		// Reaction
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REPORTED_REACTION);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_END);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.OUTCOME);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION);
		
		// Reporter
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.QUALIFICATION);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.TITLE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.MIDDLE_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.ORGANIZATION);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.DEPARTMENT);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.ADDRESS1);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.CITY);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.PROVINCE_STATE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.POSTAL_CODE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COUNTRY);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.PHONE1_NUMBER);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.FAX_NUMBER);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COMPANY_TITLE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COMPANY_GIVEN_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COMPANY_FAMILY_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.SCHEDULE);
		
		
		// Add to map
		tabWiseFieldMap.put(SceptreConstants.CaseIdTabKeys.CASEID_TAB, caseIdTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.PatientTabKeys.PATIENT_TAB, patientTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.MedicalHistoryTabKeys.TAB, medicalHistoryTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.DrugHistoryTabKeys.TAB, drugHistoryTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.ReactionTabKeys.TAB, reactionTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.DrugTabKeys.TAB, drugTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.ReporterTabKeys.REPORTER_TAB, reporterTabFieldsList);
		
	}
	
	public static void readSceptreFieldsForFUP(Map<String, List<String>> tabWiseFieldMap)
	{
		List<String> caseIdTabFieldsList = null;
		List<String> drugTabFieldsList = null;
		List<String> reporterTabFieldsList = null;
		List<String> patientTabFieldsList = null;
		List<String> reactionTabFieldsList = null;
		List<String> drugHistoryTabFieldsList = null;
		List<String> medicalHistoryTabFieldsList = null;
		
		caseIdTabFieldsList = new ArrayList<>();
		drugTabFieldsList = new ArrayList<>();
		reporterTabFieldsList = new ArrayList<>();
		patientTabFieldsList = new ArrayList<>();
		reactionTabFieldsList = new ArrayList<>();
		drugHistoryTabFieldsList = new ArrayList<>();
		medicalHistoryTabFieldsList = new ArrayList<>();
		
		// CASE ID TAB
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE_FOLLOWUP);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.COUNTRY_WHEREAE_OCCOURED_FOLLOWUP);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.SERIOUS);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.REPORT_TYPE_FOLLOWUP);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.ADVERSEEVENT);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_LOOKUP);
		caseIdTabFieldsList.add(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE);
		
		
		//Drug TAB
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.ACTIONTAKEN);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_STATUS);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_STATUS);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_START);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_END);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.REPORTED_INDICATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.BATCHLOT);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.EXPIRATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION_UNIT);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.MADEBY);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.GENERICDRUGNAME);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.CHARACTERIZATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DRUG_DURATION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_RANK);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.FIRST_REACTION);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.BATCH_RANK);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.INDICATION_RANK);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.GET_APPROVAL_NUMBERS);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_FORM);
		drugTabFieldsList.add(SceptreConstants.DrugTabKeys.DOSAGE_TEXT);
		
		
		// Drug History
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.START);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.END);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);
		drugHistoryTabFieldsList.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION);
		
		
		// Medical History
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.STARTDATE);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.ENDDATE);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.CONTINUING);
		medicalHistoryTabFieldsList.add(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT);
		
		// Patient
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.POSTAL_CODE);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.HEIGHT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.HEIGHT_UNIT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.WEIGHT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.WEIGHT_UNIT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.PATIENT_ID);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.PATIENT_SEX);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.AGE);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.AGE_UNIT);
		patientTabFieldsList.add(SceptreConstants.PatientTabKeys.AGE_GROUP);
		
		// Reaction
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REPORTED_REACTION);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_END);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.OUTCOME);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT);
		reactionTabFieldsList.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION);
		
		// Reporter
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.QUALIFICATION);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.TITLE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.MIDDLE_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.ORGANIZATION);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.DEPARTMENT);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.ADDRESS1);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.CITY);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.PROVINCE_STATE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.POSTAL_CODE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COUNTRY);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.PHONE1_NUMBER);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.FAX_NUMBER);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COMPANY_TITLE);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COMPANY_GIVEN_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.COMPANY_FAMILY_NAME);
		reporterTabFieldsList.add(SceptreConstants.ReporterTabKeys.SCHEDULE);

		// Add to map
		tabWiseFieldMap.put(SceptreConstants.CaseIdTabKeys.CASEID_TAB, caseIdTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.PatientTabKeys.PATIENT_TAB, patientTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.MedicalHistoryTabKeys.TAB, medicalHistoryTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.DrugHistoryTabKeys.TAB, drugHistoryTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.ReactionTabKeys.TAB, reactionTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.DrugTabKeys.TAB, drugTabFieldsList);
		tabWiseFieldMap.put(SceptreConstants.ReporterTabKeys.REPORTER_TAB, reporterTabFieldsList);
	}
	
	private static void handleSceptreOverridingFields(CpatToolBase oToolBase, Map<String, String> sceptreMap)
	{
		String sReporterGivenName = null , sReporterFamilyName = null , sPatientSex = null, sOutcome = null, sActionTaken = null;;
		
		sReporterGivenName = (String) sceptreTabValues.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB).get(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
		sReporterFamilyName = (String) sceptreTabValues.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB).get(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
		sPatientSex = (String) sceptreTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_TAB).get(SceptreConstants.PatientTabKeys.PATIENT_SEX);
		sOutcome= (String) sceptreTabValues.get(SceptreConstants.ReactionTabKeys.TAB).get(SceptreConstants.ReactionTabKeys.OUTCOME);
		sActionTaken = (String) sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.ACTIONTAKEN);
		
		if (!StringUtils.isNullOrEmpty(sReporterGivenName)) 
		{
			if(sceptreMap.containsKey(SceptreConstants.ReporterTabKeys.GIVEN_NAME))
			{
				if (sReporterGivenName.equalsIgnoreCase("private")) 
				{
					sceptreMap.remove(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
				}
			}
	
		}
		if(!StringUtils.isNullOrEmpty(sReporterFamilyName))
		{
			if(sceptreMap.containsKey(SceptreConstants.ReporterTabKeys.FAMILY_NAME))
			{
				if(sReporterFamilyName.equalsIgnoreCase("private"))
				{
					sceptreMap.remove(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
				}
			}
		}
		
		removeIfOverridingInvalidValue(sPatientSex, sceptreMap, SceptreConstants.PatientTabKeys.PATIENT_SEX);
		removeIfOverridingInvalidValue(sOutcome, sceptreMap, SceptreConstants.ReactionTabKeys.OUTCOME);
		removeIfOverridingInvalidValue(sActionTaken, sceptreMap, SceptreConstants.DrugTabKeys.ACTIONTAKEN);
		
	}
	
	
	private static void removeIfOverridingInvalidValue(String p_sSceptreValue, Map<String, String> sceptreMap , String p_sSDKey)
	{
		if (!StringUtils.isNullOrEmpty(p_sSceptreValue)) 
		{
			if(sceptreMap.containsKey(p_sSDKey))
			{
				if (p_sSceptreValue.equalsIgnoreCase(sceptreMap.get(p_sSDKey))
						|| sceptreMap.get(p_sSDKey).equalsIgnoreCase("Unknown"))
				{
					sceptreMap.remove(p_sSDKey);
				}
				
			}
		}
	}
	private static boolean isValidFloatNumber(String sVal)
	{
		boolean isNumber = false;
		
		try
		{	
			Float.parseFloat(sVal);	
			isNumber = true;
		}
		catch(NumberFormatException excep)
		{
			// Do Nothing
		}
		
		return isNumber;
	}
	
}
	
