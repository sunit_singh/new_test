package com.sciformix.client.pat.toolb;

public class CpatToolBErrorMessages 
{
	public static final String ERROR							= 				"Error: ";
	
	public static class API
	{
		public static final String API_FAILED 					= 				"Create API Failed";
		public static final String ACTIVITYID_ERR 				= 				"Invalid Activity Id";
	}
	
	public static class Login
	{
		public static final String NO_PRJ_ERR					=				"No Projects available";
		public static final String MULTIPLE_PRJ_ERR 			= 				"Multiple Projects found. No support for Multiple Projects yet.";
		public static final String LOGIN_FAILED 				= 				"Login Failed";
		public static final String NO_ZONE_ERR 					= 				"No Zone Available";
		public static final String RENDERING_FAILED 			= 				"Login Screen Rendering Failed";
		public static final String EMPTY_CREDENTIALS			=				"Login credentials not supplied";
		
	}
	
	public static class ToolDescScreen
	{
		public static final String RENDERING_FAILED				=				"Tool Description Screen Rendering Failed";
	}
	
	public static class UserInputScreen
	{
		public static final String RENDERING_FAILED				=				"User Input Screen Rendering Failed";
		public static final String LIST_ERROR_USERNAME			=				"Please provide valid Username";
		public static final String LIST_ERROR_PASSWORD			=				"Please provide valid Password";
		public static final String LIST_ERROR_DBNAME			=				"Please provide valid Database Name";
		public static final String LIST_ERROR_ACTION			=				"Please select appropriate Action";
		public static final String LIST_ERROR_CASEID			=				"Please provide valid Case Id";
		
		public static final String CASEID_PATTERN_ERR			=				"Case ID should be 11 digit number";
		public static final String PRODLIST_PATH_ERR			=				"Please provide valid Product List Path";
		public static final String PRODLIST_UNAVAIL_ERR			=				"File does not exist.";
		public static final String PRODLIST_TYPE_ERR 			= 				"Product List should have XLSX file type";
		public static final String PRODLIST_COL_HEADER_ERR 		= 				"Unsupported Product List contents. Product List does not have column: ";
		public static final String PRODLIST_COL_VAL_ERR 		= 				"Unsupported Product List contents. No data found for column: ";
		
		
		
	}
}
