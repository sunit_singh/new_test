package com.sciformix.client.pat.toola;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.DataTable;


public class ARRNWarningUI {
	private static final String SD_PDF_PATH = "SCP_SOURCEDOCUMENT";
	private static Logger log=LoggerFactory.getLogger(ARRNWarningUI.class);
	
	public static void createWarningPage(List<Object> aRRNList, final Map<String, String> proptyMap,final DataTable oDataTable)
	{
		Map<String, Object> ARRNMap=null;
		JLabel warnLabel=null;
		Object[][] arrayToProcess=null;
		ARRNumber ARRNumber=null;
		JScrollPane scrollPane=null;
		JLabel lblHeading=null;
		JLabel fileNameLabel=null;
		JPanel headerPanel=null;
		JButton abortButton=null;
		JButton button=null;
		JPanel warnPanel=null;
		boolean rConcillationFlag=false;
		String comments=null;
		Icon reCFlagIcon=null;
		int caseCount=0;
		String sdPath=null;
		File sdFile=null;
		JLabel blankLabel=null;
		String imageIconPath=null;
		InputStream initialStreamForStatusFlag=null;
		
		caseCount=aRRNList.size();
		imageIconPath=System.getProperty("user.dir");
		final JFrame frame = new JFrame("Sciformix Case Processor Assist Tool (CPAT)");
		//String[] columns={"Sr No.","Fields","Value Found in SD","Verification"};
		String[] columns={"Case No.","ARR Number","Reconciliation Status","Comment"};
		arrayToProcess=new Object[caseCount][4];
		
		for(int listIndex=0; listIndex<aRRNList.size(); listIndex++)
		{
			comments=null;
			rConcillationFlag=false;
			ARRNMap=(Map<String, Object>)aRRNList.get(listIndex);
			ARRNumber=(ARRNumber)ARRNMap.get("ARRNumber");
			if(ARRNumber.getARRNVerifyFlag() && ARRNumber.getMAHVerifyFlag() 
					&& ARRNumber.getIRDVerifyFlag() && ARRNumber.getSeriousReportVerifyFlag() 
					&& ARRNumber.getAgeVerifyFlag() && ARRNumber.getGenderVerifyFlag()
					&& ARRNumber.getProductVerifyFlag() && ARRNumber.getARTVerifyFlag())
			{
				rConcillationFlag=true;
				
			}
			else
			{
				comments=setComments(comments, "ARRNumber", ARRNumber.getARRNVerifyFlag());
				comments=setComments(comments, "Market Authorization Holder AER Number", ARRNumber.getMAHVerifyFlag());
				comments=setComments(comments, "Initial Received Date", ARRNumber.getIRDVerifyFlag());
				comments=setComments(comments, "Serious Report", ARRNumber.getSeriousReportVerifyFlag());
				comments=setComments(comments, "Age", ARRNumber.getAgeVerifyFlag());
				comments=setComments(comments, "Gender", ARRNumber.getGenderVerifyFlag());
				comments=setComments(comments, "Product", ARRNumber.getProductVerifyFlag());
				comments=setComments(comments, "Adverse Reaction Terms", ARRNumber.getARTVerifyFlag());
			}
			
			if(rConcillationFlag)
			{
				comments=StringConstants.EMPTY;
			}
			else
			{
				comments=comments+" mismatched.";
			}
			if(rConcillationFlag)
			{
				initialStreamForStatusFlag=ClassLoader.getSystemResourceAsStream("success_icon.png");
			//	reCFlagIcon = new ImageIcon("C:\\Users\\mpatil1\\Desktop\\success1.png");
				
			}
			else
			{
				initialStreamForStatusFlag=ClassLoader.getSystemResourceAsStream("fail_icon.png");
			//	reCFlagIcon = new ImageIcon("C:\\Users\\mpatil1\\Desktop\\fail1.png");
			}
			
			try {
				reCFlagIcon = new ImageIcon(ImageIO.read(initialStreamForStatusFlag));
			} catch (IOException e1) {
				log.error("failed to load reconcillation status icon");
			}
			
			
			arrayToProcess[listIndex][0]=listIndex+1;
			arrayToProcess[listIndex][1]=ARRNumber.getARRNumber();
			arrayToProcess[listIndex][2]=reCFlagIcon;
			arrayToProcess[listIndex][3]=comments;
		}
		
		
		DefaultTableModel model = new DefaultTableModel(arrayToProcess, columns)
        {
			
            public Class getColumnClass(int column)
            {
            	
                return getValueAt(0, column).getClass();
            }
        };
        	JTable table = new JTable(model);
        	table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			table.getColumnModel().getColumn(0).setMaxWidth(60);
			table.getColumnModel().getColumn(1).setMaxWidth(90);
			table.getColumnModel().getColumn(2).setMaxWidth(130);
			table.getColumnModel().getColumn(3).setMaxWidth(350);
			table.setEnabled(false);
			
		//	table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			JPanel panel=new JPanel();
			panel.setSize(550, 600);
			
	    
			
/*	    if(ARRNumber.isMissingARRNInExcelFlag())
	    {
*/	    //	Color color=new Color(255,0,0);
			Color color=new Color(0,100,0);
			sdPath=proptyMap.get(SD_PDF_PATH);
			sdFile=new File(sdPath);
			fileNameLabel=new JLabel("File Name: "+sdFile.getName());
			warnLabel=new JLabel("Number of Cases Found: "+caseCount);
			blankLabel=new JLabel(" ");
	    //	warnLabel.setForeground(color);
	    	warnLabel.setFont(new Font("Arial",Font.BOLD,12));
	    	fileNameLabel.setFont(new Font("Arial",Font.BOLD,12));
/*	    }
	    else
	    {
	    	Color color=new Color(0,100,0);
	    	warnLabel=new JLabel("All ARRN numbers are present. Please click Continue to process");
	    	warnLabel.setForeground(color);
	    	warnLabel.setFont(new Font("Arial",Font.BOLD,12));
	    }
*/	    
	    scrollPane = new JScrollPane(table);
	    table.setFillsViewportHeight(true);
	    lblHeading = new JLabel("ARR Number Verification");
	    lblHeading.setFont(new Font("Arial",Font.TRUETYPE_FONT,24));
	    headerPanel=new JPanel();
	    headerPanel.add(lblHeading);
	    frame.getContentPane().setLayout(new BorderLayout());
	    
	    warnPanel=new JPanel();
	   
	    warnPanel.add(fileNameLabel);
	   warnPanel.add(blankLabel);
	    warnPanel.add(warnLabel);
	    warnPanel.setLayout(new BoxLayout(warnPanel, BoxLayout.Y_AXIS));
	    button=new JButton("OK");
	    button.setSize(50,50);
	    panel.add(button,BorderLayout.PAGE_END);
	    
	    abortButton=new JButton("Abort");
	    abortButton.setSize(50,50);
	    panel.add(abortButton,BorderLayout.PAGE_END);

	    try {
			frame.setIconImage(ImageIO.read(new File("sciformix_icon.png")));
			InputStream initialStream=ClassLoader.getSystemResourceAsStream("sciformix_icon.png");
			frame.setIconImage(ImageIO.read(initialStream));
		} catch (IOException e) {
			log.error("Failed to load image icon <"+e.getMessage()+" >");
		}
	    
	    frame.getContentPane().add(headerPanel,BorderLayout.PAGE_START);
	    frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
	    frame.getContentPane().add(warnPanel,BorderLayout.PAGE_START);
	    frame.getContentPane().add(panel, BorderLayout.PAGE_END);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    //frame.setSize(550, 400);
	    frame.setSize(650, 400);
	    frame.setVisible(true);
	    frame.setLocationRelativeTo(null);
	    
	    
	   abortButton.addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { 
		       System.exit(0);
		    } 
		});
	   
	   button.addActionListener(new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { 
		       frame.dispose();
		       String fileType=null;
		       String filePath=null;
		       String p_sCaseID=null;
		       File downloadedFile=null;
		       WebDriver driver=null;
		       try
				{	
					p_sCaseID=StringConstants.EMPTY;
					fileType=proptyMap.get("fileType");
					filePath=proptyMap.get("sdpath");
					downloadedFile=new File(filePath);
					
//					PDFViewerHelper obj=new PDFViewerHelper();
//					obj.getPDFDetails(driver, proptyMap,oDataTable,p_sCaseID, downloadedFile.getAbsolutePath(), fileType);
					
				} catch (Exception ex) 
				{
					log.error("Error while displaying Warning Page");
				}finally 
				{
				}
		       
		    } 
	   });
	   
		
	}
	private static String setComments(String comments, String field, boolean flag)
	{
		if(!flag)
		{
			if(comments==null)
			{
				comments=field;
			}
			else
			{
			comments=comments+StringConstants.COMMA+field;
			}
		}
		return comments;
	}
}
