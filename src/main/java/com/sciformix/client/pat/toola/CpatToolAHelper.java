package com.sciformix.client.pat.toola;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.html.FormSubmitEvent;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.Case;
import com.sciformix.client.automation.Products;
import com.sciformix.client.automation.SearchResults;
import com.sciformix.client.automation.argus.ArgusConstants;
import com.sciformix.client.automation.argus.ArgusConstants.FETCH_SCOPE;
import com.sciformix.client.automation.argus.ArgusWebApplicationWriter;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.WorkbookWorkSheetWrapperDEA;
import com.sciformix.client.pat.commons.screens.CpatVerificationScreen;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.OfficeUtils;
import com.sciformix.commons.utils.ParsedDataTable;

public class CpatToolAHelper extends CpatToolHome {
	public static enum PROPOSED_ACTION {
		CREATE_NEW_CASE, TREAT_AS_DUPLICATE, TREAT_AS_FOLLOWUP;
	}

	public static int prodCounterForVMap = 0;
	public static int prodCounterForMMap = 0;
	private static int headerIndex;
	private IAction oCallbackAction = null;
	public static PROPOSED_ACTION eProposedAction = null;
	private static final Logger log = LoggerFactory.getLogger(OfficeUtils.class);

	public static void showVerificationScreen(CpatToolBase cpatToolBase, IAction oCallbackAction,
			Map<String, String> mapExtractedParams) {
		// TODO Auto-generated method stub
		System.out.println("Time to present the third screen");

		Map<String, Object> params = new HashMap<>();
		params.put("ExtractedParams", mapExtractedParams);
		new CpatVerificationScreen(cpatToolBase, oCallbackAction, params);

	}

	public static void generateWebWriterForDuplicateSearch(IAction oCallbackAction, Map<String, String> propMap,
			final ParsedDataTable oDataTable) {
		ArgusWebApplicationWriter oWebWriter = null;
		Products product = null, product2 = null;
		Map<ArgusConstants.ArgusSearchFields, Object> mapCriteria = null;
		SearchResults oResults = null;
		List<Case> oCaseList = null;
		int m_nEventCount = 0;
		int m_nProductCount = 0;
		int m_nlinkedDupRecCount = 0;
		boolean m_bNoResultFoundByAERNumber = false;
		boolean m_bNoResultFoundByMAHAERNumber = false, m_bNoCaseResultFoundByPatinfoProduct = false;
		boolean m_bNoCaseResultFoundByLinkedNumber = false;
		boolean m_bResultFoundForAgeGender = false;
		Map<String, String> convertedData2Map = null;
		String sUserName = null;
		String sPassword = null;
		String sLinkedDuplicateRecordCountFromMap = null;
		String sLinkedDuplicateAERNumber = null;
		String sPatientAge = null;
		String sPatientGender = null;
		String sCountry = null;
		String sAdverseReactionTerm = null;
		String returnMessage = null;

		String sProductCountFromMap = null;
		String sEventCountFromMap = null;
		String sUrl = null;
		Products oProduct = null;
		String sEventName = null;
		String sProductDosageForm = null;
		DataTable productFromExcelDataTable = null;
		Map<String, Map<String, Products>> allProductMap = null;
		Map<String, Products> verifiedProductMap = null;
		String productExcelPath = null;
		String adverseReactionReportNum = null;
		String marketAuthNumber = null;
		boolean isDuplicate = false;
		boolean isInitial = false;
		boolean isFollowup = false;
		boolean isReporterTypeSame = false;
		boolean isProductDetailsSame = false;

		WebDriver driver = null;

		try {
			oProduct = new Products();
			productFromExcelDataTable = new DataTable();
			oWebWriter = new ArgusWebApplicationWriter();
			mapCriteria = new HashMap<ArgusConstants.ArgusSearchFields, Object>();

			sUrl = propMap.get("server.url");

			driver = oWebWriter.launchBrowser(sUrl);

			System.out.println(
					"*************************************************************************************************");
			for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) {
				String sKey = oDataTable.getMnemonic(nIndex);
				String sValue = oDataTable.getColumnValues(nIndex).get(oDataTable.getColumnValues(nIndex).size() - 1)
						.displayValue().toString();
				System.out.println(sKey + " : " + sValue);
			}
			System.out.println(
					"*************************************************************************************************");
			convertedData2Map = convertDataTableToMap(oDataTable);

			// DemoWriter.check(convertedData2Map);
			product = new Products();

			sUserName = propMap.get("SCP_USERNAME");
			sPassword = propMap.get("SCP_PASSWORD");
			oWebWriter.login(sUserName, sPassword);

			oWebWriter.isHomePage();

			mapCriteria.clear();

			adverseReactionReportNum = oDataTable.getColumnValues("Report1.Adverse Reaction Report Number").get(0)
					.displayValue().toString();

			mapCriteria.put(ArgusConstants.ArgusSearchFields.AERNUMBER, adverseReactionReportNum);

			oWebWriter.prepareForSearch();
			oResults = oWebWriter.search(mapCriteria, propMap);

			if (oResults != null && oResults.resultsFound()) {
				printSearchResultsRecord(oResults);

				oCaseList = getCaseData(oResults, oWebWriter, FETCH_SCOPE.ALL);

				isReporterTypeSame = isReporterTypeSame(propMap, oCaseList.get(0));

				isProductDetailsSame = isProductDetailsSame(propMap, oCaseList.get(0));

				if (isReporterTypeSame && isProductDetailsSame) {

					returnMessage = "Search on AER identified incoming AER " + adverseReactionReportNum
							+ " as DUPLICATE of existing Case Id ";
					oCallbackAction.performAction(CPAT_ACTION.DUPLICATE, returnMessage, propMap, oDataTable, driver);
				} else {
					System.out.println("No results found");
					m_bNoResultFoundByAERNumber = true;

					returnMessage = "Search on AER identified incoming AER " + adverseReactionReportNum
							+ " as a FOLLOW UP of existing Case Id ";
					oCallbackAction.performAction(CPAT_ACTION.FOLLOWUP, returnMessage, propMap, oDataTable, driver);
				}
			} else {
				System.out.println("No results found");
				m_bNoResultFoundByAERNumber = true;
			}

			// -----------------------------------------------------------------------------------------------
			if (m_bNoResultFoundByAERNumber) {
				mapCriteria.clear();

				marketAuthNumber = oDataTable.getColumnValues("Report1.Market Authorization Holder AER Number").get(0)
						.displayValue().toString();

				mapCriteria.put(ArgusConstants.ArgusSearchFields.AERNUMBER, marketAuthNumber);
				//
				oWebWriter.prepareForSearch();
				oResults = oWebWriter.search(mapCriteria, propMap);

				if (oResults != null && oResults.resultsFound()) {

					printSearchResultsRecord(oResults);

					oCaseList = getCaseData(oResults, oWebWriter, FETCH_SCOPE.ALL);

					isReporterTypeSame = isReporterTypeSame(propMap, oCaseList.get(0));

					isProductDetailsSame = isProductDetailsSame(propMap, oCaseList.get(0));

					if (isReporterTypeSame && isProductDetailsSame) {

						returnMessage = "Search on AER and MAH AER identified incoming AER " + adverseReactionReportNum
								+ " as DUPLICATE of existing Case Id ";
						oCallbackAction.performAction(CPAT_ACTION.DUPLICATE, returnMessage, propMap, oDataTable,
								driver);
					} else {
						System.out.println("No results found");
						m_bNoResultFoundByMAHAERNumber = true;
						returnMessage = "Search on AER and MAH AER identified incoming AER " + adverseReactionReportNum
								+ " as a FOLLOW UP of existing Case Id ";
						oCallbackAction.performAction(CPAT_ACTION.FOLLOWUP, returnMessage, propMap, oDataTable, driver);
					}
				} else {
					System.out.println("No results found");
					m_bNoResultFoundByMAHAERNumber = true;
				}
			}

			// ------------------------------------------------------------------------------------------------------
			if (m_bNoResultFoundByMAHAERNumber) {
				mapCriteria.clear();
				sLinkedDuplicateRecordCountFromMap = oDataTable.getColumnValues("LinkDupRecord Count").get(0)
						.displayValue().toString();
				m_nlinkedDupRecCount = Integer.parseInt(sLinkedDuplicateRecordCountFromMap);

				for (int index = 1; index <= m_nlinkedDupRecCount; index++) {
					sLinkedDuplicateAERNumber = oDataTable
							.getColumnValues("Report1.Link_Duplicate_Record" + index + ".Link AER** Number").get(0)
							.displayValue().toString();
					mapCriteria.put(ArgusConstants.ArgusSearchFields.AERNUMBER, sLinkedDuplicateAERNumber);
					oWebWriter.prepareForSearch();
					oResults = oWebWriter.search(mapCriteria, propMap);

					if (oResults != null && oResults.resultsFound()) {

						printSearchResultsRecord(oResults);

						oCaseList = getCaseData(oResults, oWebWriter, FETCH_SCOPE.ALL);

						isReporterTypeSame = isReporterTypeSame(propMap, oCaseList.get(0));

						isProductDetailsSame = isProductDetailsSame(propMap, oCaseList.get(0));

						if (isReporterTypeSame && isProductDetailsSame) {
							returnMessage = "Search on Linked Duplicate Information identified incoming AER "
									+ adverseReactionReportNum + " as DUPLICATE of existing Case Id ";
							oCallbackAction.performAction(CPAT_ACTION.DUPLICATE, returnMessage, propMap, oDataTable,
									driver);
						} else {
							System.out.println("No results found");
							m_bNoCaseResultFoundByLinkedNumber = true;
							returnMessage = "Search on Linked Duplicate Information identified incoming AER "
									+ adverseReactionReportNum + " as a FOLLOW UP of existing Case Id ";
							oCallbackAction.performAction(CPAT_ACTION.FOLLOWUP, returnMessage, propMap, oDataTable,
									driver);
						}
					} else {
						System.out.println("No results found");
						m_bNoCaseResultFoundByLinkedNumber = true;
					}
				}
			}
			if (m_bNoCaseResultFoundByLinkedNumber) {
				mapCriteria.clear();
				sPatientAge = oDataTable.getColumnValues("Report1.Age").get(0).displayValue().toString();
				mapCriteria.put(ArgusConstants.ArgusSearchFields.PATIENTAGE, sPatientAge);
				sPatientGender = oDataTable.getColumnValues("Report1.Gender").get(0).displayValue().toString();
				mapCriteria.put(ArgusConstants.ArgusSearchFields.PATIENTGENDER, sPatientGender);

				oWebWriter.prepareForSearch();
				oResults = oWebWriter.search(mapCriteria, propMap);

				if (oResults != null && oResults.resultsFound()) {

					printSearchResultsRecord(oResults);

					oCaseList = getCaseData(oResults, oWebWriter, FETCH_SCOPE.ALL);

					returnMessage = "Age/Gender not available � incoming AER " + adverseReactionReportNum
							+ " is a NEW CASE";
					oCallbackAction.performAction(CPAT_ACTION.BOOKCASE, returnMessage, propMap, oDataTable, driver);
				} else {
					System.out.println("No results found");
					m_bResultFoundForAgeGender = true;
				}
			}

			if (m_bResultFoundForAgeGender) {

				mapCriteria.clear();
				sPatientAge = oDataTable.getColumnValues("Report1.Age").get(0).displayValue().toString();
				mapCriteria.put(ArgusConstants.ArgusSearchFields.PATIENTAGE, sPatientAge);
				sPatientGender = oDataTable.getColumnValues("Report1.Gender").get(0).displayValue().toString();
				mapCriteria.put(ArgusConstants.ArgusSearchFields.PATIENTGENDER, sPatientGender);
				sCountry = oDataTable.getColumnValues("Country").get(0).displayValue().toString();
				mapCriteria.put(ArgusConstants.ArgusSearchFields.COUNTRY, sCountry);

				sProductCountFromMap = oDataTable.getColumnValues("Product Count").get(0).displayValue().toString();
				m_nProductCount = Integer.parseInt(sProductCountFromMap);

				sEventCountFromMap = oDataTable.getColumnValues("Event Count").get(0).displayValue().toString();
				m_nEventCount = Integer.parseInt(sEventCountFromMap);

				productExcelPath = propMap.get("SCP_PRODUCTCONFIG");

				productFromExcelDataTable = CpatToolAHelper.getDataFromProductExcel(productExcelPath);

				allProductMap = CpatToolAHelper.PDFVerification(oDataTable, productFromExcelDataTable, false);

				verifiedProductMap = allProductMap.get("verifiedProductMap");

				for (int index = 1; index <= verifiedProductMap.size(); index++) {

					oProduct = verifiedProductMap.get("Product" + index);

					mapCriteria.put(ArgusConstants.ArgusSearchFields.PRODUCT, oProduct);

					oWebWriter.prepareForSearch();

					oResults = oWebWriter.search(mapCriteria, propMap);

					if (oResults != null) {
						if (oResults.resultsFound()) {
							printSearchResultsRecord(oResults);

							for (int jindex = 0; jindex < m_nEventCount; jindex++) {
								sAdverseReactionTerm = oDataTable
										.getColumnValues("Report1.event" + jindex + ".Adverse Reaction Term(s)").get(0)
										.displayValue().toString();
								mapCriteria.clear();
								mapCriteria.put(ArgusConstants.ArgusSearchFields.EVENT, sAdverseReactionTerm);

								oWebWriter.searchPage();
								oResults = oWebWriter.search(mapCriteria, propMap);

								if (oResults.resultsFound()) {
									oCaseList = getCaseData(oResults, oWebWriter, FETCH_SCOPE.ALL);

									isReporterTypeSame = isReporterTypeSame(propMap, oCaseList.get(0));

									isProductDetailsSame = isProductDetailsSame(propMap, oCaseList.get(0));

									if (isReporterTypeSame && isProductDetailsSame) {
										returnMessage = "Search on Age, Gender, Country , Product and Event Information identified incoming AER "
												+ adverseReactionReportNum + " as DUPLICATE of existing Case Id ";
										oCallbackAction.performAction(CPAT_ACTION.DUPLICATE, returnMessage, propMap,
												oDataTable, driver);
									} else {
										System.out.println("No results found");
										m_bNoCaseResultFoundByLinkedNumber = true;
										returnMessage = "Search on AER and MAH AER identified incoming AER "
												+ adverseReactionReportNum + " as a FOLLOW UP of existing Case Id ";
										oCallbackAction.performAction(CPAT_ACTION.FOLLOWUP, returnMessage, propMap,
												oDataTable, driver);

									}
								}
							} // event loop
						} else {
							m_bNoCaseResultFoundByPatinfoProduct = true;

						}
					} else {
						m_bNoCaseResultFoundByPatinfoProduct = true;

					}
				} // for loop product

			}
			returnMessage = "No duplicates identified basis AER Number/MAH AER Number and Patient Info/Product/Event\nNo existing cases identified for this Patient\nTherefore, incoming AER "
					+ adverseReactionReportNum + "  is a NEW CASE";
			oCallbackAction.performAction(CPAT_ACTION.BOOKCASE, returnMessage, propMap, oDataTable, driver);
		}

		catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static boolean isReporterTypeSame(Map<String, String> p_oDataMap, Case p_oCase) {

		String m_sSDReporterType = null, m_sArgusReporterType = null;

		m_sSDReporterType = p_oDataMap.get("PrimaryReporter");

		m_sArgusReporterType = (String) p_oCase.getAttributeValue("ReporterType");

		if (m_sSDReporterType.equalsIgnoreCase(m_sArgusReporterType)) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public static boolean isProductDetailsSame(Map<String, String> p_oDataTableMap, Case p_oCase) {
		Map<String, Products> oArgusProductsMap = null;
		DataTable m_oProductExcelData = null;

		String m_sProdPath = null;
		Map<String, Map<String, Products>> m_oAllProductMap = null;
		boolean m_bIsProductsame = false;

		m_sProdPath = "D:\\DevlopmentTool_Shambhu\\DEACheckOut8_DEC\\DEA\\resources\\GRA Approved and Withdrawn Products Report.xlsx";

		m_oProductExcelData = CpatToolAHelper.getDataFromProductExcel(m_sProdPath);

		// m_oAllProductMap=m_oProductUtils.PDFVerification(p_oDataTableMap ,
		// m_oProductExcelData , false);

		oArgusProductsMap = (Map<String, Products>) p_oCase.getAttributeValue("Products");

		m_bIsProductsame = checkProductsDetailsEquality(oArgusProductsMap, m_oAllProductMap);

		return m_bIsProductsame;
	}

	public static boolean checkProductsDetailsEquality(Map<String, Products> p_oProductMapFromArgus,
			Map<String, Map<String, Products>> p_ProductMapFromSD) {
		boolean m_bCheckDose = false, m_bCheckDoseUnit = false, m_bCheckFrequency = false, m_bHPRole = false,
				m_bRouteOfAdmin = false, m_bCheckindication = false;
		Products product = null, newProduct = null;
		Map<String, Products> m_mNewVerifiedProductMap = null, m_mNewMissingProductMap = null, m_mMergedMap = null;
		int count = 0;
		String toBreakLoop = null, m_sNewProductRouteAdmin = null;
		String[] m_saNewProductIndication = null, m_saArgusProductDose = null;

		try {

			m_mMergedMap = new HashMap<>();
			m_mNewVerifiedProductMap = p_ProductMapFromSD.get("verifiedProductMap");
			m_mNewMissingProductMap = p_ProductMapFromSD.get("missingProductMap");

			m_mMergedMap = mergeMap(m_mNewVerifiedProductMap, m_mNewMissingProductMap);

			for (int j = 1; j <= m_mMergedMap.size(); j++) {

				if ((m_mMergedMap.get("Product" + j)) != null) {
					newProduct = m_mMergedMap.get("Product" + j);
				}

				m_bCheckDose = false;
				m_bCheckDoseUnit = false;
				m_bCheckFrequency = false;
				m_bHPRole = false;
				m_bRouteOfAdmin = false;
				m_bCheckindication = false;

				m_saNewProductIndication = newProduct.getIndication();
				m_sNewProductRouteAdmin = newProduct.getRoutOfAdmin();

				for (int i = 0; i < m_saNewProductIndication.length; i++) {
					if (m_saNewProductIndication[i].equals("")) {
						m_saNewProductIndication[i] = "product used for unknown indication";
					}

				}

				if (m_sNewProductRouteAdmin.equals("")) {
					m_sNewProductRouteAdmin = "unknown";
				}

				for (int i = 1; i <= p_oProductMapFromArgus.size(); i++) {

					if ((p_oProductMapFromArgus.get("Product" + i)) != null) {
						product = p_oProductMapFromArgus.get("Product" + i);
					}

					m_saArgusProductDose = product.getDose();

					for (int k = 0; k < m_saArgusProductDose.length; k++) {
						if (!m_saArgusProductDose[k].equals("")) {

							if (!m_saArgusProductDose[k].contains(".") && !m_saArgusProductDose[k].equals("")) {
								m_saArgusProductDose[k] = m_saArgusProductDose[k] + ".0";
							}
						}
					}

					if (newProduct.getProductName().equals(product.getProductName())) {
						toBreakLoop = "break";

						// checkProductExistence =
						// checkAttributeEquality(newProduct.getDosageForm(),
						// product.getDosageForm() , checkProductExistence);

						// checkProductExistence =
						// checkArrayAttributeEquality(newProduct.getFrequency(),
						// product.getFrequency() , checkProductExistence);

						m_bCheckDose = checkArrayAttributeEquality(newProduct.getDose(), m_saArgusProductDose);

						m_bCheckindication = checkArrayAttributeEquality(m_saNewProductIndication,
								product.getIndication());

						m_bCheckDoseUnit = checkArrayAttributeEquality(newProduct.getDoseUnit(), product.getDoseUnit());

						m_bHPRole = checkAttributeEquality(newProduct.getHPRole(), product.getHPRole());

						m_bRouteOfAdmin = checkAttributeEquality(m_sNewProductRouteAdmin, product.getRoutOfAdmin());

					}

					if (toBreakLoop != null && toBreakLoop.equals("break")) {
						toBreakLoop = "";
						break;
					}
				}

				if (m_bCheckDose && m_bCheckindication && m_bCheckDoseUnit && m_bHPRole && m_bRouteOfAdmin) {
					count++;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (count == m_mMergedMap.size()) {
			return true;
		} else {
			return false;
		}

	}

	public static Map<String, Products> mergeMap(Map<String, Products> firstMap, Map<String, Products> secondMap) {
		Products product = null;
		int size = secondMap.size();
		Map<String, Products> tmp = null;

		tmp = new HashMap<String, Products>(firstMap);

		for (String key : firstMap.keySet()) {
			if (!secondMap.containsKey(key)) {
				product = tmp.get(key);
				secondMap.put(key, product);
			} else {
				product = tmp.get(key);
				secondMap.put("Product" + (size + 1), product);
				size++;
			}

		}

		return secondMap;
	}

	public static boolean checkArrayAttributeEquality(String[] p_saProd1Attribute, String[] p_saProd2Attribute) {
		boolean m_bCheckProductExistence = false;

		int count = 0;

		for (int i = 0; i < p_saProd1Attribute.length; i++) {
			for (int j = 0; j < p_saProd2Attribute.length; j++) {
				if (p_saProd1Attribute[i].equalsIgnoreCase(p_saProd2Attribute[j])) {
					count++;
				}
			}
		}

		if (count == p_saProd1Attribute.length) {
			m_bCheckProductExistence = true;
		}

		return m_bCheckProductExistence;
	}

	public static boolean checkAttributeEquality(String p_saProd1Attribute, String p_saProd2Attribute) {
		boolean m_bCheckProductExistence = false;

		if ((p_saProd1Attribute.equalsIgnoreCase(p_saProd2Attribute))) {
			m_bCheckProductExistence = true;
		}

		return m_bCheckProductExistence;
	}

	private static Map<String, String> convertDataTableToMap(ParsedDataTable pDataTable) {
		Map<String, String> convertedDataMap = new HashMap<String, String>();

		for (int index = 0; index < pDataTable.colCount(); index++) {
			String dataKey = pDataTable.getMnemonic(index);
			String dataValue = pDataTable.getColumnValues(index).get(0).toString();
			convertedDataMap.put(dataKey, dataValue);
		}

		return convertedDataMap;
	}

	public static List<Case> getCaseData(SearchResults p_oResults, ArgusWebApplicationWriter p_oWebWriter,
			FETCH_SCOPE p_oFetchScope) throws InterruptedException {

		int resultCount = 0;
		String sCaseId = null;
		Case oCase = null;
		List<Case> listOfResultCaseData = null;

		resultCount = p_oResults.resultCount();
		listOfResultCaseData = new ArrayList<>();

		for (int i = 0; i < resultCount; i++) {
			sCaseId = (String) p_oResults.getValue(i, ArgusConstants.ArgusSearchResultColumns.CASEID);

			oCase = p_oWebWriter.retrieveCase(sCaseId, p_oFetchScope);

			listOfResultCaseData.add(oCase);
		}

		return listOfResultCaseData;
	}

	private static void printSearchResultsRecord(SearchResults oResults) {
		for (int nRowCounter = 0; nRowCounter < oResults.resultCount(); nRowCounter++) {
			System.out.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.CASEID));
			System.out.println(
					oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.INITIAL_RECEIPT_DATE));
			System.out.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.PRODUCTS));
			System.out.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.REPORT_TYPE));
			System.out.println(
					oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.PROJECT_ID_STUDY_ID));
			System.out.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.REPORTER));
			System.out.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.COUNTRY));
			System.out.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.EVENTS));
			System.out
					.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.PATIENT_INITIALS));
			System.out.println(oResults.getValue(nRowCounter, ArgusConstants.ArgusSearchResultColumns.PATIENT_ID));
		}
	}

	public static DataTable getDataFromProductExcel(String prodPath) {
		String path = System.getProperty("user.dir");
		String fileName = "GRA Approved and Withdrawn Products Report.xlsx";
		String str = "";
		path = path + "\\resources\\" + fileName;
		DataTable oDataTableInitial = new DataTable();
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		String p_sStagedFileName = prodPath;
		boolean bHeaderRowPresent = true;
		int nSheetIndex = 2;
		String sColumnsToCollapse = "";
		try {
			int headerRowIndex = 3;
			oWorksheetWrapper = parseWorkbook(p_sStagedFileName, bHeaderRowPresent, nSheetIndex, headerRowIndex);
		} catch (SciException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		oDataTableInitial = convertToDatatable(oWorksheetWrapper, sColumnsToCollapse);
		for (int index = 0; index < oDataTableInitial.colCount(); index++) {
			String sKey = oDataTableInitial.getColumnHeader(index);
			String sValue = oDataTableInitial.getColumnValues(index).toString();
			System.out.println(sKey + " : " + sValue);
		}
		return oDataTableInitial;
	}

	public static Map<String, Map<String, Products>> PDFVerification(ParsedDataTable oDataTable,
			DataTable ExcelDataTable, boolean prodForBooking) {
		String productKey = null, productValue = null, HPRoleKey = null, HPRoleValue = null,
				dosageFormulationKey = null, dosageFormulationValue = null, rootOfAdminKey = null,
				rootOfAdminValue = null, countryValue = null;
		String sProductCount = null;
		Date aWithdrawalDate = null;

		String[] m_saDoseKey = null, m_saDoseUnitKey = null, m_saFrequencyKey = null, m_saIndicationKey = null,
				m_saDoseValue = null, m_saDoseUnitValue = null, m_saFrequencyValue = null, m_saIndicationValue = null;

		prodCounterForVMap = 0;
		prodCounterForMMap = 0;
		boolean dosageIgnore = false;
		boolean prodFound = false;

		sProductCount = oDataTable.getColumnValues("Product Count").get(0).displayValue().toString();
		int productCount = Integer.parseInt(sProductCount);
		Map<String, Products> verifiedProductMap = new HashMap<String, Products>();
		Map<String, Products> missingProductMap = new HashMap<String, Products>();

		List<Object> prodValueList = ExcelDataTable.getColumnValues("Product Name");
		List<Object> countryValueList = ExcelDataTable.getColumnValues("Country");
		List<Object> dosageFormValueList = ExcelDataTable.getColumnValues("Dosage Form (SAP Terms)");
		List<Object> actualWithdrawalDateValueList = ExcelDataTable.getColumnValues("Actual Withdrawal Date");

		for (int index = 1; index <= productCount; index++) {
			if (prodFound && prodForBooking)
				break;

			m_saDoseKey = new String[1];
			m_saDoseUnitKey = new String[1];
			m_saFrequencyKey = new String[1];
			m_saIndicationKey = new String[1];

			productKey = oDataTable.getColumnValues("Report1.product" + index + ".Product Description").get(0)
					.displayValue().toString();
			HPRoleKey = oDataTable.getColumnValues("Report1.product" + index + ".Health Product Role").get(0)
					.displayValue().toString();
			dosageFormulationKey = oDataTable.getColumnValues("Report1.product" + index + ".Dosage Form").get(0)
					.displayValue().toString();
			rootOfAdminKey = oDataTable.getColumnValues("Report1.product" + index + ".Route of Administration").get(0)
					.displayValue().toString();
			m_saDoseKey[0] = oDataTable.getColumnValues("Report1.product" + index + ".Dose").get(0).displayValue()
					.toString();
			// m_saDoseUnitKey[0] =
			// oDataTable.getColumnValues("Report1.product"+index+".Dose
			// Unit").get(0).displayValue().toString();
			m_saFrequencyKey[0] = oDataTable.getColumnValues("Report1.product" + index + ".Frequency").get(0)
					.displayValue().toString();
			m_saIndicationKey[0] = oDataTable.getColumnValues("Report1.product" + index + ".Indication(s)").get(0)
					.displayValue().toString();

			if (productKey != null) {

				m_saDoseValue = new String[1];
				m_saDoseUnitValue = new String[1];
				m_saFrequencyValue = new String[1];
				m_saIndicationValue = new String[1];

				productValue = productKey;
				HPRoleValue = HPRoleKey;
				dosageFormulationValue = dosageFormulationKey;
				rootOfAdminValue = rootOfAdminKey;
				m_saDoseValue[0] = m_saDoseKey[0] != null ? m_saDoseKey[0] : "";
				m_saDoseUnitValue[0] = m_saDoseUnitKey[0] != null ? m_saDoseUnitKey[0] : "";
				m_saFrequencyValue[0] = m_saFrequencyKey[0] != null ? m_saFrequencyKey[0] : "";
				m_saIndicationValue[0] = m_saIndicationKey[0] != null ? m_saIndicationKey[0] : "";
				countryValue = oDataTable.getColumnValues("Country").get(0).displayValue().toString();

				if (productValue != null && productValue != "") {

					if (productValue.contains("-"))
						productValue = productValue.split("-")[1];
					if (productValue.contains(" "))
						productValue = productValue.split(" ")[0];

					if (HPRoleValue.equalsIgnoreCase("suspect")) {
						for (int vIndex = 0; vIndex < prodValueList.size(); vIndex++) {
							prodFound = false;
							if (prodValueList.get(vIndex).toString().equalsIgnoreCase(productValue)) {

								if ((dosageFormulationValue.trim().toUpperCase().contains("SOLUTION")
										&& dosageFormulationValue.trim().toUpperCase().contains("INTRAVENOUS"))
										|| dosageFormValueList.get(vIndex).toString().toLowerCase()
												.contains("injectable")) {
									dosageIgnore = true;
									dosageFormulationValue = "Injection";
								}
								if ((dosageFormulationValue.trim().toUpperCase().contains("NOT SPECIFIED")
										|| dosageFormulationValue.trim().toUpperCase().contains("UNKNOWN"))) {
									dosageIgnore = true;
								}

								if (countryValueList.get(vIndex).toString().equalsIgnoreCase(countryValue)
										&& (dosageIgnore || dosageFormValueList.get(vIndex).toString().toLowerCase()
												.contains(dosageFormulationValue.toLowerCase()))) {

									if (dosageFormulationValue.trim().equalsIgnoreCase("unknown")
											|| dosageFormulationValue.trim().equalsIgnoreCase("not specified")) {
										dosageIgnore = true;
										dosageFormulationValue = dosageFormValueList.get(vIndex).toString().trim();
										// dosageFormulationValue="unknown";
									}

									String awDate = actualWithdrawalDateValueList.get(vIndex).toString();
									System.out.println("Actual Withdrawal date is : " + awDate);
									try {
										if (awDate != null && (!awDate.equalsIgnoreCase(""))) {

											aWithdrawalDate = new SimpleDateFormat("dd-MMM-yyyy").parse(awDate);
											Calendar fourbackcal = Calendar.getInstance();
											fourbackcal.add(Calendar.YEAR, -4);
											Date date1 = new Date();
											date1 = fourbackcal.getTime();

											Date date2 = new Date();

											long diff = date2.getTime() - date1.getTime();

											long diffValue = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

											long diff1 = date2.getTime() - date1.getTime();

											long diffValue1 = TimeUnit.DAYS.convert(diff1, TimeUnit.MILLISECONDS);

											if (diffValue1 > diffValue) {
												continue;

											} else {

												prodFound = true;
											}
										} else {
											aWithdrawalDate = null;
											prodFound = true;
											break;
										}
									}

									catch (Exception ex) {
										ex.printStackTrace();
									}

									if (prodForBooking || prodFound)
										break;
								}
							}
						}
						if (prodFound) {

							prodCounterForVMap++;
							productKey = "Product" + prodCounterForVMap;
							Products prod = new Products(productValue, HPRoleValue, dosageFormulationValue,
									rootOfAdminValue, m_saDoseValue, m_saDoseUnitValue, m_saFrequencyValue,
									StringConstants.EMPTY, m_saIndicationValue, aWithdrawalDate, true);
							verifiedProductMap.put(productKey, prod);
						} else {
							prodCounterForMMap++;
							productKey = "Product" + prodCounterForMMap;
							Products prod = new Products(productValue, HPRoleValue, dosageFormulationValue,
									rootOfAdminValue, m_saDoseValue, m_saDoseUnitValue, m_saFrequencyValue,
									StringConstants.EMPTY, m_saIndicationValue, aWithdrawalDate, false);
							missingProductMap.put(productKey, prod);

						}

					} else {
						prodCounterForMMap++;
						productKey = "Product" + prodCounterForMMap;
						Products prod = new Products(productValue, HPRoleValue, dosageFormulationValue,
								rootOfAdminValue, m_saDoseValue, m_saDoseUnitValue, m_saFrequencyValue,
								StringConstants.EMPTY, m_saIndicationValue, aWithdrawalDate, false);
						missingProductMap.put(productKey, prod);

					}
				}
			}

		}

		Map<String, Map<String, Products>> productMap = new HashMap<String, Map<String, Products>>();
		productMap.put("verifiedProductMap", verifiedProductMap);
		productMap.put("missingProductMap", missingProductMap);
		return productMap;
	}

	public static WorkbookWorkSheetWrapperDEA parseWorkbook(String p_sStagedFileName, boolean p_bIsHeaderPresent,
			int p_nSheetIndex, int headerRowIndex) throws SciException {
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		int nNumberOfColumns = 0;
		int nNumberOfDataRows = 0;
		Workbook oWorkbook = null;
		Sheet oSheet = null;
		// int HeaderRowIndex=3;
		int headerFinderCounter = 1;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;

		List<Object> listTableRow = null;
		List<Object> listHeaderRow = null;
		List<List<Object>> listDataTable = null;
		File oWorkbookFile = null;
		try {
			headerIndex = headerRowIndex;
			oWorkbookFile = new File(p_sStagedFileName);
			oWorkbook = WorkbookFactory.create(oWorkbookFile);

			oSheet = oWorkbook.getSheetAt(p_nSheetIndex);

			listDataTable = new ArrayList<List<Object>>();

			oIteratorWorkbookRows = oSheet.rowIterator();
			if (p_bIsHeaderPresent) {
				/*
				 * while(oIteratorWorkbookRows.hasNext() &&
				 * (headerFinderCounter<HeaderRowIndex)) {
				 * headerFinderCounter++; continue; }
				 */
				while (oIteratorWorkbookRows.hasNext()) {

					if ((headerFinderCounter == headerRowIndex)) {

					} else {
						oIteratorWorkbookRows.next();
						headerFinderCounter++;
						continue;
					}

					oSingleWorkbookRow = oIteratorWorkbookRows.next();
					listHeaderRow = new ArrayList<Object>();

					if (oSingleWorkbookRow != null) {
						// Iterate through the cells
						for (int nRowCellIndex = 0; nRowCellIndex < oSingleWorkbookRow
								.getLastCellNum(); nRowCellIndex++) {
							oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
							listHeaderRow.add(getWorkbookCellContent(oSingleWorkbookCell));

							nNumberOfColumns++;
						}
					}
					break;
				}

			} else {
				// throw new Exception("Header row not present use case not
				// supported as yet");
				// listErrors.add(new SciError(p_sErrorKey));
				// Ignore and continue
			}

			// NOTE: Iterator might have moved one workbook row if an header was
			// expected and found
			while (oIteratorWorkbookRows.hasNext()) {
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				listTableRow = new ArrayList<Object>(nNumberOfColumns);

				// Iterate through the cells.
				if (oSingleWorkbookRow != null) {
					// Iterate through the cells.
					for (int nRowCellIndex = 0; nRowCellIndex < nNumberOfColumns; nRowCellIndex++) {
						oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
						listTableRow.add(getWorkbookCellContent(oSingleWorkbookCell));
					}
					listDataTable.add(listTableRow);
					nNumberOfDataRows++;
				}
			}

			oWorksheetWrapper = new WorkbookWorkSheetWrapperDEA(oSheet.getSheetName(), p_nSheetIndex,
					p_bIsHeaderPresent, nNumberOfColumns, nNumberOfDataRows, listHeaderRow, listDataTable);

		} catch (FileNotFoundException fnfExcep) {
			log.error("Error parsing file as file not found", fnfExcep);
			throw new SciException("Error parsing file as file not found", fnfExcep);
		} catch (InvalidFormatException invExcep) {
			log.error("Error parsing file as file format incorrect", invExcep);
			throw new SciException("Error parsing file as file format incorrect", invExcep);
		} catch (IOException ioExcep) {
			log.error("Error parsing file as file", ioExcep);
			throw new SciException("Error parsing file", ioExcep);
		} finally {
			try {
				oWorkbook.close();
			} catch (Exception ioExcep) {
				// Ignore and continue
				log.error("Error closing workbook file", ioExcep);
			}
		}
		return oWorksheetWrapper;
	}

	public static DataTable convertToDatatable(WorkbookWorkSheetWrapperDEA p_oWorksheetWrapper,
			String p_sColumnsToCollapse) {
		DataTable oDataTable = null;

		oDataTable = new DataTable();

		p_oWorksheetWrapper.appendDataIntoDataTable(oDataTable, false, p_sColumnsToCollapse);

		return oDataTable;
	}

	@SuppressWarnings("deprecation")
	private static Object getWorkbookCellContent(Cell oSingleWorkbookCell) {
		boolean bCheckIfCellContainsDateValue = false;
		DataFormatter df = new DataFormatter(true);

		if (oSingleWorkbookCell != null) {
			switch (oSingleWorkbookCell.getCellType()) {
			case Cell.CELL_TYPE_BLANK:
				return oSingleWorkbookCell.getStringCellValue();
			case Cell.CELL_TYPE_STRING:
				return oSingleWorkbookCell.getStringCellValue().trim();
			case Cell.CELL_TYPE_NUMERIC:
				// giving wrong date
				String dateFormat = "";
				if (headerIndex == 1)
					dateFormat = "MM/dd/yy";
				else
					dateFormat = "";
				bCheckIfCellContainsDateValue = checkIfCellContainsDateValue(df.formatCellValue(oSingleWorkbookCell),
						dateFormat);
				if (bCheckIfCellContainsDateValue) {
					return getActualFormattedDateValue(df.formatCellValue(oSingleWorkbookCell), dateFormat);
				} else {
					oSingleWorkbookCell.setCellType(Cell.CELL_TYPE_STRING);
				}
				return oSingleWorkbookCell.getStringCellValue().trim();
			case Cell.CELL_TYPE_BOOLEAN:
				return oSingleWorkbookCell.getBooleanCellValue();
			case Cell.CELL_TYPE_FORMULA:
				return oSingleWorkbookCell.getCellFormula();
			case Cell.CELL_TYPE_ERROR:
				return null;
			default:
				return new String("");
			}
		} else {
			return null;
		}
	}

	private static String getActualFormattedDateValue(String p_sDateValue, String dateFormat) {
		String sIncommingDateFormat = null;
		// String sIncommingDateFormat2 = null;
		String sFormattedDate = null;
		String sDateFormat = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		Date tempFormattedDate = null;
		SimpleDateFormat formatter = null;

		if (p_sDateValue != null) {
			if (dateFormat == null || dateFormat.equals("")) {
				String middleDatePart = "";
				String startDatePart = "";
				String endDatePart = "";
				if (p_sDateValue.contains("/")) {
					String[] temp = p_sDateValue.split("/");
					startDatePart = temp[0];
					middleDatePart = temp[1];
					endDatePart = temp[2];
					// p_sDateValue.substring(p_sDateValue.lastIndexOf("/")+1,p_sDateValue.length());
				}
				if (p_sDateValue.contains("/")) {
					if (endDatePart.length() == 2)
						sIncommingDateFormat = "dd/MM/yy";
					else
						sIncommingDateFormat = "dd/MM/yyyy";
				} else
					sIncommingDateFormat = "dd-MMM-yyyy";// ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString());
			} else
				sIncommingDateFormat = dateFormat;
			sDateFormat = "dd-MMM-yyyy";// ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT.toString());
			// TODO: Check this
			try {
				incomingformatter = new SimpleDateFormat(sIncommingDateFormat);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
				// tempFormattedDate=new
				// SimpleDateFormat("dd-MMM-yyyy").parse(p_sDateValue);
			} catch (ParseException parseExcep) {
				log.error("Date formatting failed for: Date/Format - " + p_sDateValue + "/" + sIncommingDateFormat,
						parseExcep);
				return "";
			}

			formatter = new SimpleDateFormat(sDateFormat);
			sFormattedDate = formatter.format(oFormattedDate);
		} else {
			return "";
		}
		return sFormattedDate;
	}

	private static boolean checkIfCellContainsDateValue(String p_sColumnValue, String p_inputdateformatter) {

		try {
			// TODO: Check this
			SimpleDateFormat formatter = new SimpleDateFormat(p_inputdateformatter);
			formatter.parse(p_sColumnValue);
		} catch (Exception excep) {
			return false;
		}
		return true;
	}

	public static void inputsCollectedActionListener(CpatToolBase oToolBase, IAction oCallbackAction, JFrame frame,
			FormSubmitEvent event) 
	{
		Map<String, String> mapExtractedParameters = null;
		List<String> listErrors = null;
		
		int activityId = 0;
		
		try
		{
			mapExtractedParameters = CpatToolAHelper.extractEventParameters(event);
			
			//Perform Validation
			listErrors = oToolBase.validateUserInputs(mapExtractedParameters);
			if (listErrors != null && listErrors.size() > 0)
			{
				JOptionPane.showMessageDialog(null, listErrors.get(0));
			}
			else
			{
				//activityId = CpatBToolHelper.callRegisterActivityApi("KEY1", oScpUser.getUserId(), oScpUser.getPassword(), oScpUser.getUserProjectsList().get(0).getProjectSeqId(), "CPAT", "a", -1, "a", "User Inputs Collected","a");
				activityId = 1;
				
				// If Response code is Successful
				if(activityId > 0)
				{
					frame.dispose();
					
					oCallbackAction.performAction(CPAT_ACTION.USER_INPUTS_COLLECTED, mapExtractedParameters, activityId);
				}
				
			}			
		}
		catch(Exception e)
		{
			log.error("User Input Collection Failed: ", null, e);
			JOptionPane.showMessageDialog(null, "User Input Collection Failed");
		}
		
	}

}
