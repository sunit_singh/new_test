package com.sciformix.client.pat.toola;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.openqa.selenium.WebDriver;

import com.sciformix.client.automation.argus.ArgusWebApplicationWriter;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.ParsedDataTable;

public class DuplicateWindow {

	static String newCaseString;

	// Buttons
	private final static JButton cntToBookCase() {
		JButton cntbutton = new JButton("Book Case");
		int pad = 20;
		cntbutton.setMargin(new Insets(pad, pad, pad, pad));
		cntbutton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		cntbutton.setBackground(Color.LIGHT_GRAY);
		cntbutton.createToolTip();
		cntbutton.setToolTipText("Continue for Case Bookin as No Duplicates or Followup Found");
		cntbutton.setForeground(new Color(0, 0, 0));
		cntbutton.setBounds(100, 260, 200, 40);
		return cntbutton;
	}

	private final static JButton skipAndProceedToNextCase() {
		JButton skipButton = new JButton("Skip to Next Case");
		int pad = 20;
		skipButton.setMargin(new Insets(pad, pad, pad, pad));
		skipButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		skipButton.setBackground(Color.LIGHT_GRAY);
		skipButton.createToolTip();
		skipButton.setToolTipText("Duplicate or Followup Found. Skip the current case and Proceed to the next case");
		skipButton.setForeground(new Color(0, 0, 0));
		skipButton.setBounds(320, 230, 200, 40);
		return skipButton;
	}

	private final static JButton abortCase() {
		JButton abortButton = new JButton("Abort Case");
		int pad = 20;
		abortButton.setMargin(new Insets(pad, pad, pad, pad));
		abortButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		abortButton.setBackground(Color.LIGHT_GRAY);
		abortButton.createToolTip();
		abortButton.setToolTipText("Duplicate or Followup Found. Skip the current case and Proceed to the next case");
		abortButton.setForeground(new Color(0, 0, 0));
		abortButton.setBounds(290, 230, 140, 40);
		return abortButton;
	}

	private final static JButton proceedToNextCase() {
		JButton abortButton = new JButton("Proceed to Next Case");
		int pad = 20;
		abortButton.setMargin(new Insets(pad, pad, pad, pad));
		abortButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		abortButton.setBackground(Color.LIGHT_GRAY);
		abortButton.createToolTip();
		abortButton.setToolTipText("Duplicate or Followup Found. Skip the current case and Proceed to the next case");
		abortButton.setForeground(new Color(0, 0, 0));
		abortButton.setBounds(70, 230, 190, 40);
		return abortButton;
	}

	private final static JButton createFollowup() {
		JButton abortButton = new JButton("Create Followup Case");
		int pad = 20;
		abortButton.setMargin(new Insets(pad, pad, pad, pad));
		abortButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		abortButton.setBackground(Color.LIGHT_GRAY);
		abortButton.createToolTip();
		abortButton.setToolTipText("Duplicate or Followup Found. Skip the current case and Proceed to the next case");
		abortButton.setForeground(new Color(0, 0, 0));
		//abortButton.setBounds(40, 260, 250, 40);
		abortButton.setBounds(40, 230, 250, 40);
		 
		return abortButton;
	}

	// Panels
	
	public final static void createNewCase(final CpatToolBase oToolBase, final IAction oCallbackAction,final WebDriver driver, final Map<String, String> propMap, final ParsedDataTable dataTable,String returnMessage) {
		JPanel createNewCasePanel = new JPanel();
		final JFrame frame = new JFrame();
	
		final JButton createButton = cntToBookCase();
		JButton skipButton = skipAndProceedToNextCase();
		JButton abortButton = abortCase();
		InputStream initialStream=ClassLoader.getSystemResourceAsStream("sciformix_icon.png");
	
		try {
			frame.setIconImage(ImageIO.read(initialStream));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String [] createNewMsg = returnMessage.split("\n");
		JLabel jlabel = new JLabel("<html><ul><li>"+createNewMsg[0]+"</li></ul></html>");
		JLabel jlabel1 = new JLabel("<html><ul><li>"+createNewMsg[1]+"</li></ul></html>");
		JLabel jlabel2 = new JLabel("<html><ul><li>"+createNewMsg[2]+"</li></ul></html>");
		jlabel.setFont(new Font("Serif", Font.ROMAN_BASELINE, 15));
		jlabel.setForeground(new Color(128, 0, 64));
		jlabel1.setFont(new Font("Serif", Font.ROMAN_BASELINE, 15));
		jlabel1.setForeground(new Color(128, 0, 64));
		jlabel2.setFont(new Font("Serif", Font.ROMAN_BASELINE, 15));
		jlabel2.setForeground(new Color(128, 0, 64));
		createNewCasePanel.setLayout(null);
		jlabel.setBorder(BorderFactory.createTitledBorder(""));

		
		createNewCasePanel.add(jlabel);
		jlabel1.setBorder(BorderFactory.createTitledBorder(""));
		createNewCasePanel.add(jlabel1);
		jlabel2.setBorder(BorderFactory.createTitledBorder(""));
		createNewCasePanel.add(jlabel2);
		createNewCasePanel.add(createButton);
		createNewCasePanel.add(skipButton);
		jlabel.setBounds(50, 70, 550, 50);
		jlabel1.setBounds(50, 130, 550, 50);
		jlabel2.setBounds(50, 170, 550, 50);
		frame.add(createNewCasePanel);
		frame.setTitle("Sciformix Case Processor Assist Tool (CPAT)");
		frame.setSize(650, 370);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		createButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) 
			{
				ArgusWebApplicationWriter argusWriter = new ArgusWebApplicationWriter();
				frame.dispose();
				try {
					argusWriter.createCase(oToolBase,oCallbackAction, propMap, dataTable);
				} catch (SciException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		skipButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) 
			{
//				ApotexCaseOperation apotexCaseOperation = new ApotexCaseOperation();
//				frame.dispose();
//				try {
////					apotexCaseOperation.nextCase(driver, propMap, dataTable,returnMessage);
//				} catch (SciException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}
		});
	
		
	}

	

	public final static void createFollowupCase(final WebDriver driver, final Map<String, String> propMap, final DataTable dataTable,String returnMessage) {
		JPanel duplicateCasePanel = new JPanel();
		final JFrame frame = new JFrame();
		JButton skipButton = createFollowup();
		JButton abortButton = skipAndProceedToNextCase();
		JLabel jlabel = new JLabel(
				"<html>"+returnMessage+"</html>");
		jlabel.setFont(new Font("Serif", Font.ROMAN_BASELINE, 15));
		jlabel.setForeground(new Color(128, 0, 64));
		jlabel.setBounds(50, 70, 450, 50);
		jlabel.setBorder(BorderFactory.createTitledBorder(""));
		duplicateCasePanel.setLayout(null);
		duplicateCasePanel.add(jlabel);
		duplicateCasePanel.add(skipButton);
		duplicateCasePanel.add(abortButton);
	
		InputStream initialStream=ClassLoader.getSystemResourceAsStream("sciformix_icon.png");
		try {
			frame.setIconImage(ImageIO.read(initialStream));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		frame.add(duplicateCasePanel);
		frame.setTitle("Sciformix Case Processor Assist Tool (CPAT)");
		//frame.setSize(550, 350);
		frame.setSize(650, 370);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		skipButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) 
			{
				
			}
		});

		abortButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) 
			{
				frame.dispose();
//				CpatLogin login = new CPAT
			}
		});
		
	}

	private final static JPanel createFollowUpCasePanel() {
		JPanel followUpCasePanel = new JPanel();
		JButton skipButton = proceedToNextCase();
		JButton abortButton = abortCase();
		JLabel jlabel = new JLabel(
				"Search on AER and MAH AER identified incoming AER aaa as DUPLICATE of existing Case Id ccc");
		JLabel jlabel1 = new JLabel(
				"Incoming AER aaa is a DUPLICATE of existing Case ccc because Patient details and Product/Event match for Products ppp[] and Events eee[]");
		jlabel.setFont(new Font("Serif", Font.BOLD, 15));
		jlabel.setForeground(Color.RED);
		jlabel1.setFont(new Font("Serif", Font.BOLD, 15));
		jlabel1.setForeground(Color.RED);
		followUpCasePanel.setLayout(null);
		followUpCasePanel.add(jlabel);
		followUpCasePanel.add(jlabel1);
		followUpCasePanel.add(skipButton);
		followUpCasePanel.add(abortButton);
		jlabel.setBounds(50, 70, 650, 50);
		jlabel1.setBounds(50, 110, 750, 50);

		return followUpCasePanel;
	}

//	public static void main(String[] args) {
//
//		CaseStatusUpdate cases=new CaseStatusUpdate();
//		CaseStatus case1=new CaseStatus();
//		try {
//			cases.updateCsvFile(case1);
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		try {
//			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
//				| UnsupportedLookAndFeelException e) {
//
//			e.printStackTrace();
//		}
//		JFrame frame = new JFrame();
//		int count = 1;
//		DuplicateWindow d = new DuplicateWindow();
//		DataTable dataTable = null;
//		Map<String, String> propMap = null;
//		WebDriver driver = null;
//		String returnMessage = null;
////		Login login=new Login();
//		
////			login.continueToNextCase(driver, propMap, dataTable, "case is duplicate");
//		//createFollowupCase(driver, propMap, dataTable, "case is followup");
//		//d.createNewCase(driver, propMap, dataTable, "No duplicates identified basis AER Number/MAH AER Number and Patient Info/Product/Event\nNo existing cases identified basis AER Number/MAH AER Number and Patient Info/Product/Event\nTherefore, incoming AER ");
//	}

}
