package com.sciformix.client.pat.toola;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.sciformix.client.automation.Products;
import com.sciformix.client.pat.commons.WorkbookWorkSheetWrapperDEA;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;


public class ProductUtils {
public int prodCounterForVMap=0;
public int prodCounterForMMap=0;

	public Map<String, Map<String, Product>> PDFVerification(ParsedDataTable SDDataTable, DataTable ExcelDataTable, boolean prodForBooking)
	{
		String  productKey=null, productValue=null, 
				HPRoleKey=null, HPRoleValue=null,
				dosageFormulationKey=null, dosageFormulationValue=null,
				rootOfAdminKey=null, rootOfAdminValue=null,
				doseKey=null, doseValue=null,
			    doseUnitKey=null, doseUnitValue=null, 
			    freqKey=null, freqValue=null,
			    AERIndicationKey=null, AERIndicationValue=null,
			    countryValue=null;
		Date aWithdrawalDate = null;	   
		prodCounterForVMap=0;
		prodCounterForMMap=0;
		boolean dosageIgnore=false;
		//int foundIndex=0;
		boolean prodFound=false;
		//Integer.p
		int productCount=Integer.parseInt(SDDataTable.getColumnValues("Product Count").get(0).toString());
		Map<String, Product> verifiedProductMap=new HashMap<String,Product>();
		Map<String, Product> missingProductMap=new HashMap<String,Product>();
		
		List<Object> prodValueList=ExcelDataTable.getColumnValues("Product Name");
		List<Object> countryValueList=ExcelDataTable.getColumnValues("Country");
		List<Object> dosageFormValueList=ExcelDataTable.getColumnValues("Dosage Form (SAP Terms)");
		List<Object> approvedNameList=ExcelDataTable.getColumnValues("Approved Name");
		List<Object> actualWithdrawalDateValueList=ExcelDataTable.getColumnValues("Actual Withdrawal Date");
//		List prodValueList=ExcelDataTable.getColumnValues("Product Name");
	
		for(int index=1;index<=productCount;index++)
		{
			if(prodFound && prodForBooking)
				break;
			productKey="Product"+index;
			HPRoleKey="PCM Description.Health Product Role"+index;
			dosageFormulationKey="PCM Description.Dosage Form"+index;
			rootOfAdminKey="PatientRouteofAdministration"+index;
			doseKey="PCM Description.Dose"+index;
			doseUnitKey="PCM Description.Unit"+index;
			freqKey="PCM Description.Frequency"+index;
			AERIndicationKey="AER Description.Indication"+index;
			
			if(SDDataTable.isColumnPresent(productKey))
			{
				productValue=(String)SDDataTable.getColumnValues(productKey).get(0).displayValue().toString();
				HPRoleValue=(String)SDDataTable.getColumnValues(HPRoleKey).get(0).displayValue().toString();
				dosageFormulationValue=(String)SDDataTable.getColumnValues(dosageFormulationKey).get(0).displayValue().toString();
				rootOfAdminValue=(String)SDDataTable.getColumnValues(rootOfAdminKey).get(0).displayValue().toString();
				doseValue=(String)SDDataTable.getColumnValues(doseKey).get(0).displayValue().toString();
				doseUnitValue=(String)SDDataTable.getColumnValues(doseUnitKey).get(0).displayValue().toString();
				freqValue=(String)SDDataTable.getColumnValues(freqKey).get(0).displayValue().toString();
				AERIndicationValue=(String)SDDataTable.getColumnValues(AERIndicationKey).get(0).displayValue().toString();
				countryValue=(String)SDDataTable.getColumnValues("Country").get(0).displayValue().toString();
				
				if(productValue!=null && productValue!="")
				{
					//excel column names
					
					if(productValue.contains("-"))
						productValue=productValue.split("-")[1];
					if(productValue.contains(" "))
						productValue=productValue.split(" ")[0];
					
				/*	if(HPRoleValue.equalsIgnoreCase("Concomitant"))
						missingProductMap.put(key, value)*/
					if(HPRoleValue.equalsIgnoreCase("suspect"))
					{
					for(int vIndex=0; vIndex<prodValueList.size(); vIndex++)
					{
						prodFound=false;
						if(prodValueList.get(vIndex).toString().equalsIgnoreCase(productValue))
						{

							if((dosageFormulationValue.trim().toUpperCase().contains("SOLUTION") &&
									dosageFormulationValue.trim().toUpperCase().contains("INTRAVENOUS")) || dosageFormValueList.get(vIndex).toString().toLowerCase().contains("injectable"))
							{
								dosageIgnore=true;
								dosageFormulationValue="Injection";
							}
							if((dosageFormulationValue.trim().toUpperCase().contains("NOT SPECIFIED") ||
									dosageFormulationValue.trim().toUpperCase().contains("UNKNOWN")) )
							{
								dosageIgnore=true;
							}
							
							if(countryValueList.get(vIndex).toString().equalsIgnoreCase(countryValue) && (dosageIgnore ||
							   dosageFormValueList.get(vIndex).toString().toLowerCase().contains(dosageFormulationValue.toLowerCase())))
							{
								
								if(dosageFormulationValue.trim().equalsIgnoreCase("unknown") || dosageFormulationValue.trim().equalsIgnoreCase("not specified"))
								{
									dosageIgnore=true;
									dosageFormulationValue=dosageFormValueList.get(vIndex).toString().trim();
									//dosageFormulationValue="unknown";
								}
								
								String awDate=actualWithdrawalDateValueList.get(vIndex).toString();
								
								try
								{
								if(awDate!=null && (!awDate.equalsIgnoreCase("")))
								{

								aWithdrawalDate = new SimpleDateFormat("dd-MMM-yyyy").parse(awDate);
								Calendar fourbackcal = Calendar.getInstance();
								fourbackcal.add(Calendar.YEAR, -4);
								Date date1 = new Date();
								date1 = fourbackcal.getTime();
								
								Date date2 = new Date();
						
								long diff = date2.getTime() - date1.getTime();
								
								long diffValue = TimeUnit.DAYS.convert(diff,TimeUnit.MILLISECONDS);
								
								long diff1 = date2.getTime() - aWithdrawalDate.getTime();
								
								long diffValue1 = TimeUnit.DAYS.convert(diff1,TimeUnit.MILLISECONDS);
								
								if(diffValue1 > diffValue)
								{
									continue;
									
								}
								else
								{
									
									prodFound=true;
								}
									}
									else
									{
										aWithdrawalDate = null;
										prodFound=true;
										break;
									}
								}
								
								catch(ParseException ex)
								{
									ex.printStackTrace();
								}
								
								if(prodForBooking || prodFound)
								break;
							}
							}
					}
					if(prodFound)
					{
						
						
							prodCounterForVMap++;
							productKey="Product"+prodCounterForVMap;
							Product prod=new Product(productValue,HPRoleValue,dosageFormulationValue,
									rootOfAdminValue,doseValue,doseUnitValue,freqValue,StringConstants.EMPTY,AERIndicationValue,aWithdrawalDate,true);
							verifiedProductMap.put(productKey, prod);
					}
					else
					{
						prodCounterForMMap++;
						productKey="Product"+prodCounterForMMap;
						Product prod=new Product(productValue,HPRoleValue,dosageFormulationValue,
								rootOfAdminValue,doseValue,doseUnitValue,freqValue,StringConstants.EMPTY,AERIndicationValue,aWithdrawalDate,false);
						missingProductMap.put(productKey, prod);
						
					}
					
					
					}
					else
					{
						prodCounterForMMap++;
						productKey="Product"+prodCounterForMMap;
						Product prod=new Product(productValue,HPRoleValue,dosageFormulationValue,
								rootOfAdminValue,doseValue,doseUnitValue,freqValue,StringConstants.EMPTY,AERIndicationValue,aWithdrawalDate,false);
						missingProductMap.put(productKey, prod);
						
					}
				}
			}
			
		}
		
			
		Map<String, Map<String, Product>> productMap=new HashMap<String, Map<String, Product>>();
		productMap.put("verifiedProductMap", verifiedProductMap);
		productMap.put("missingProductMap", missingProductMap);
		return productMap;
	}
	
	
	public DataTable getDataFromProductExcel(String prodPath)
	{
		String path=System.getProperty("user.dir");
		String fileName="GRA Approved and Withdrawn Products Report.xlsx";
		path=path+"\\resources\\"+fileName;
		DataTable oDataTableInitial=new DataTable();
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		String p_sStagedFileName=prodPath;
		boolean bHeaderRowPresent=true;
		int nSheetIndex=2;
		String sColumnsToCollapse="";
		try {
			int headerRowIndex=3;
			oWorksheetWrapper = CpatToolAHelper.parseWorkbook(p_sStagedFileName, bHeaderRowPresent, nSheetIndex,headerRowIndex);
		} catch (SciException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		oDataTableInitial = CpatToolAHelper.convertToDatatable(oWorksheetWrapper, sColumnsToCollapse);
		return oDataTableInitial;
	}
	
	public Map<String, Map<String, Products>> PDFVerification1(Map<String ,String> SDDataTableMap, ParsedDataTable ExcelDataTable, boolean prodForBooking)
	{
		String  productKey=null, productValue=null, 
				HPRoleKey=null, HPRoleValue=null,
				dosageFormulationKey=null, dosageFormulationValue=null,
				rootOfAdminKey=null, rootOfAdminValue=null,
				countryValue=null;
		Date aWithdrawalDate = null;
		
		String[] 	m_saDoseKey = null , m_saDoseUnitKey = null, m_saFrequencyKey = null , m_saIndicationKey = null
					,m_saDoseValue = null , m_saDoseUnitValue = null , m_saFrequencyValue = null , m_saIndicationValue = null ;
		
		prodCounterForVMap=0;
		prodCounterForMMap=0;
		boolean dosageIgnore=false;
		boolean prodFound=false;
		
		int productCount=Integer.parseInt(SDDataTableMap.get("Product Count").toString());
		Map<String, Products> verifiedProductMap=new HashMap<String,Products>();
		Map<String, Products> missingProductMap=new HashMap<String,Products>();
		
		List<ParsedDataValue> prodValueList=ExcelDataTable.getColumnValues("Product Name");
		List<ParsedDataValue> countryValueList=ExcelDataTable.getColumnValues("Country");
		List<ParsedDataValue> dosageFormValueList=ExcelDataTable.getColumnValues("Dosage Form (SAP Terms)");
		List<ParsedDataValue> actualWithdrawalDateValueList=ExcelDataTable.getColumnValues("Actual Withdrawal Date");

		
		for(int index=1;index<=productCount;index++)
		{
			if(prodFound && prodForBooking)
				break;
			
			m_saDoseKey = new String[1];
			m_saDoseUnitKey = new String[1];
			m_saFrequencyKey = new String[1];
			m_saIndicationKey = new String[1];
			
			productKey = "Product"+index;
			HPRoleKey = "PCM Description.Health Product Role"+index;
			dosageFormulationKey = "PCM Description.Dosage Form"+index;
			rootOfAdminKey = "PatientRouteofAdministration"+index;
			m_saDoseKey[0] = "PCM Description.Dose"+index;
			m_saDoseUnitKey[0] = "PCM Description.Unit"+index;
			m_saFrequencyKey[0] = "PCM Description.Frequency"+index;
			m_saIndicationKey[0] = "AER Description.Indication"+index;
			
			if(SDDataTableMap.get(productKey) != null)
			{

				m_saDoseValue = new String[1] ;
				m_saDoseUnitValue = new String[1];
				m_saFrequencyValue = new String[1];
				m_saIndicationValue = new String[1];
				
				productValue = (String)SDDataTableMap.get(productKey);
				HPRoleValue = (String)SDDataTableMap.get(HPRoleKey);
				dosageFormulationValue = (String)SDDataTableMap.get(dosageFormulationKey);
				rootOfAdminValue = (String)SDDataTableMap.get(rootOfAdminKey);
				m_saDoseValue[0] = (String)SDDataTableMap.get(m_saDoseKey[0]) != null ? (String)SDDataTableMap.get(m_saDoseKey[0]) :"";
				m_saDoseUnitValue[0] = (String)SDDataTableMap.get(m_saDoseUnitKey[0]) != null ? (String)SDDataTableMap.get(m_saDoseUnitKey[0]) : "";
				m_saFrequencyValue[0] = (String)SDDataTableMap.get(m_saFrequencyKey[0]) != null ? (String)SDDataTableMap.get(m_saFrequencyKey[0]) : "";
				m_saIndicationValue[0] = (String)SDDataTableMap.get(m_saIndicationKey[0]) != null ? (String)SDDataTableMap.get(m_saIndicationKey[0]) : "";
				countryValue=(String)SDDataTableMap.get("Country");
				
				if(productValue!=null && productValue!="")
				{
					
					if(productValue.contains("-"))
						productValue=productValue.split("-")[1];
					if(productValue.contains(" "))
						productValue=productValue.split(" ")[0];
					
					if(HPRoleValue.equalsIgnoreCase("suspect"))
					{
					for(int vIndex=0; vIndex<prodValueList.size(); vIndex++)
					{
						prodFound=false;
						if(prodValueList.get(vIndex).toString().equalsIgnoreCase(productValue))
						{

							if((dosageFormulationValue.trim().toUpperCase().contains("SOLUTION") &&
									dosageFormulationValue.trim().toUpperCase().contains("INTRAVENOUS")) || dosageFormValueList.get(vIndex).toString().toLowerCase().contains("injectable"))
							{
								dosageIgnore=true;
								dosageFormulationValue="Injection";
							}
							if((dosageFormulationValue.trim().toUpperCase().contains("NOT SPECIFIED") ||
									dosageFormulationValue.trim().toUpperCase().contains("UNKNOWN")) )
							{
								dosageIgnore=true;
							}
							
							if(countryValueList.get(vIndex).toString().equalsIgnoreCase(countryValue) && (dosageIgnore ||
							   dosageFormValueList.get(vIndex).toString().toLowerCase().contains(dosageFormulationValue.toLowerCase())))
							{
								
								if(dosageFormulationValue.trim().equalsIgnoreCase("unknown") || dosageFormulationValue.trim().equalsIgnoreCase("not specified"))
								{
									dosageIgnore=true;
									dosageFormulationValue=dosageFormValueList.get(vIndex).toString().trim();
									//dosageFormulationValue="unknown";
								}
								
								String awDate=actualWithdrawalDateValueList.get(vIndex).toString();
								
								try
								{
								if(awDate!=null && (!awDate.equalsIgnoreCase("")))
								{

								aWithdrawalDate = new SimpleDateFormat("dd-MMM-yyyy").parse(awDate);
								Calendar fourbackcal = Calendar.getInstance();
								fourbackcal.add(Calendar.YEAR, -4);
								Date date1 = new Date();
								date1 = fourbackcal.getTime();
								
								Date date2 = new Date();
						
								long diff = date2.getTime() - date1.getTime();
								
								long diffValue = TimeUnit.DAYS.convert(diff,TimeUnit.MILLISECONDS);
								
								long diff1 = date2.getTime() - aWithdrawalDate.getTime();
								
								long diffValue1 = TimeUnit.DAYS.convert(diff1,TimeUnit.MILLISECONDS);
								
								if(diffValue1 > diffValue)
								{
									continue;
									
								}
								else
								{
									
									prodFound=true;
								}
									}
									else
									{
										aWithdrawalDate = null;
										prodFound=true;
										break;
									}
								}
								
								catch(ParseException ex)
								{
									ex.printStackTrace();
								}
								
								if(prodForBooking || prodFound)
								break;
							}
							}
					}
					if(prodFound)
					{
						
						
						prodCounterForVMap++;
						productKey="Product"+prodCounterForVMap;
						Products prod=new Products(productValue,HPRoleValue,dosageFormulationValue,
								rootOfAdminValue,m_saDoseValue , m_saDoseUnitValue ,m_saFrequencyValue,StringConstants.EMPTY,m_saIndicationValue,aWithdrawalDate,true);
						verifiedProductMap.put(productKey, prod);
					}
					else
					{
						prodCounterForMMap++;
						productKey="Product"+prodCounterForMMap;
						Products prod=new Products(productValue,HPRoleValue,dosageFormulationValue,
								rootOfAdminValue,m_saDoseValue, m_saDoseUnitValue, m_saFrequencyValue,StringConstants.EMPTY,m_saIndicationValue,aWithdrawalDate,false);
						missingProductMap.put(productKey, prod);
						
					}
					
					
					}
					else
					{
						prodCounterForMMap++;
						productKey="Product"+prodCounterForMMap;
						Products prod=new Products(productValue,HPRoleValue,dosageFormulationValue,
								rootOfAdminValue,m_saDoseValue,m_saDoseUnitValue,m_saFrequencyValue,StringConstants.EMPTY,m_saIndicationValue,aWithdrawalDate,false);
						missingProductMap.put(productKey, prod);
						
					}
				}
			}
			
		}
		
			
		Map<String, Map<String, Products>> productMap=new HashMap<String, Map<String, Products>>();
		productMap.put("verifiedProductMap", verifiedProductMap);
		productMap.put("missingProductMap", missingProductMap);
		return productMap;
	}


	public Map<String, Map<String, Product>> PDFVerification(DataTable oDataTable, DataTable productExcelData,
			boolean prodForBooking) {
		// TODO Auto-generated method stub
		return null;
	}

}
