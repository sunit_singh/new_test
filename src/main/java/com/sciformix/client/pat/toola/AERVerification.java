package com.sciformix.client.pat.toola;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.WorkbookWorkSheetWrapperDEA;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.ParsedDataTable;


public class AERVerification {
	private static final String SPACE_JUNK = "\\xa0";
	private static final String STRING_SEPARATOR = ", ";
	private static final String YEARFORMAT_DD_MMM_YYYY = "dd-MMM-yyyy";
	private static final String YEARFORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	private static final Logger log = LoggerFactory.getLogger(AERVerification.class);
	
	public static Map<String, Object> verifyARRNumber(DataTable oDataTable, String filePath)
	{	
		ARRNumber ARRNumber=null;
		Map<String, Object> ARRNMap=null;
		DataTable dataTable=null;
		
		
		dataTable=getDataFromExcel(filePath);
		
		//ARRNumber = convertExtractedData(oDataTable);
		
		ARRNMap = compareInputCaseWithReconciliationTable(dataTable, ARRNumber, 1);
		
		return ARRNMap;
	}
	
	public static ARRNumber convertExtractedData(ParsedDataTable oDataTable)
	{
		ARRNumber ARRNumber=null;
		
		int eventCount=0;
		int productCount=0;
		String productKeyPrefix=null;
		String hpRole=null;
		String hpRoleKey=null;
		String[] ageArr=null;
		StringBuilder productString= new StringBuilder();
		StringBuilder eventString= new StringBuilder();
		String age=null, ird=null, gender=null, ageUnit=null, productKey=null,eventKey=null,
			   MAHNumber=null,seriousReport=null, ARRNumberString=null;
		
		ARRNumber=new ARRNumber();
		
		if(oDataTable.isColumnPresent("Report1.Adverse Reaction Report Number"))
			ARRNumberString=oDataTable.getColumnValues("Report1.Adverse Reaction Report Number").get(0).displayValue().toString();
			if(oDataTable.isColumnPresent("Report1.Initial Received Date"))
				ird=oDataTable.getColumnValues("Report1.Initial Received Date").get(0).displayValue().toString();
			if(oDataTable.isColumnPresent("Report1.Market Authorization Holder AER Number"))
				MAHNumber=oDataTable.getColumnValues("Report1.Market Authorization Holder AER Number").get(0).displayValue().toString();
			if(oDataTable.isColumnPresent("Report1.Serious report?"))
				seriousReport=oDataTable.getColumnValues("Report1.Serious report?").get(0).displayValue().toString();
			if(oDataTable.isColumnPresent("Report1.Age"))
				age=oDataTable.getColumnValues("Report1.Age").get(0).displayValue().toString();
			
			if(age.trim().contains(StringConstants.SPACE))
			{
				ageArr=age.split(StringConstants.SPACE);
				if(ageArr.length==1)
				{
					age=ageArr[0];
					ageUnit=StringConstants.EMPTY;
				}
				else if(ageArr.length==2)
				{
				age=ageArr[0];
				ageUnit=ageArr[1];
				}
				else
				{
					age=StringConstants.EMPTY;
					ageUnit=StringConstants.EMPTY;
				}
			}
			ARRNumber.setAge(age);
			ARRNumber.setAgeUnit(ageUnit);
			
			if(oDataTable.isColumnPresent("Report1.Gender"))
				gender=oDataTable.getColumnValues("Report1.Gender").get(0).displayValue().toString();
				
			ARRNumber.setARRNumber(ARRNumberString);
			ARRNumber.setMarketAuthHolder(MAHNumber);
			ARRNumber.setInitialReceivedDate(ird);
			ARRNumber.setSeriousReport(seriousReport);
			ARRNumber.setAge(age);
			ARRNumber.setGender(gender);
			if(oDataTable.isColumnPresent("Product Count"))
			productCount=Integer.parseInt(oDataTable.getColumnValues("Product Count").get(0).displayValue().toString());
			if(oDataTable.isColumnPresent("Event Count"))
			eventCount=Integer.parseInt(oDataTable.getColumnValues("Event Count").get(0).displayValue().toString());
			
			for(int prodIndex=1;prodIndex<=productCount;prodIndex++)
			{
					productKeyPrefix="Report1.product"+prodIndex;
					productKey=productKeyPrefix+".Product Description";
					hpRoleKey=productKeyPrefix+".Health Product Role";
			if(oDataTable.isColumnPresent(hpRoleKey))
				hpRole=oDataTable.getColumnValues(hpRoleKey).get(0).displayValue().toString();
			else
				hpRole=StringConstants.EMPTY;
			if(hpRole.toLowerCase().equals("suspect"))
			{
			if(productString.toString().equals(StringConstants.EMPTY))
				productString.append(oDataTable.getColumnValues(productKey).get(0).displayValue().toString());
			else
				productString.append(STRING_SEPARATOR+oDataTable.getColumnValues(productKey).get(0).displayValue().toString());
			}
			}
			ARRNumber.setSuspectedProducts(productString.toString());
			for(int eventIndex=1;eventIndex<=eventCount;eventIndex++)
			{
				eventKey="Report1.event"+eventIndex+".Adverse Reaction Term(s)";
				if(oDataTable.isColumnPresent(eventKey))
				{
				if(eventIndex==1)
				{
					
					eventString.append(oDataTable.getColumnValues(eventKey).get(0).displayValue().toString());
				}
				else
					eventString.append(STRING_SEPARATOR+oDataTable.getColumnValues(eventKey).get(0).displayValue().toString());
				}
			}
			ARRNumber.setAdverseReactionTerms(eventString.toString());
			
			return ARRNumber;
		}
	
	public static Map<String, Object> compareInputCaseWithReconciliationTable(DataTable dataTable, ARRNumber ARRNumber, int nCaseRecordCounter)
	{
		Map<String, Object> ARRNMap=null;
		int excelIndex=0;
		Date excelDate=null;
		String excelAge=null,
				tempARRN=null,destDate=null,
					tempString=null, excelAgeUnit=null;
		SimpleDateFormat excelFormat = null;
		SimpleDateFormat destFormat = null;
		String[] excelAgeArr=null;
		List<Integer> verifiedIndexList=null;
		String ageUnit=null;
		String[] ageArray=null;
		List<Object> excelARRNList=dataTable.getColumnValues("Adverse Reaction Report Number");
		List<Object> excelMarketAuthHolderList=dataTable.getColumnValues("Market Authorization Holder AER Number");
		List<Object> excelInitialRecievedDateList=dataTable.getColumnValues("Initial Received Date");
		List<Object> excelSeriousReportList=dataTable.getColumnValues("Serious report?");
		List<Object> excelAgeList=dataTable.getColumnValues("Age");
		List<Object> excelGenderList=dataTable.getColumnValues("Gender");
		List<Object> excelSuspectProdBrandList=dataTable.getColumnValues("Suspected Product Brand Name");
		List<Object> excelAdverseReactionTermList=dataTable.getColumnValues("Adverse Reaction Terms");
		
		ARRNMap=new HashMap<String, Object>();
		verifiedIndexList=new ArrayList<Integer>();
		
		excelFormat=new SimpleDateFormat(YEARFORMAT_YYYY_MM_DD);
		excelDate=new Date();
		
		destFormat=new SimpleDateFormat(YEARFORMAT_DD_MMM_YYYY);
		
		tempARRN=ARRNumber.getARRNumber();
		while(tempARRN.startsWith("0") &&  tempARRN.length()>1)
		{
			tempARRN=tempARRN.substring(tempARRN.indexOf("0")+1,tempARRN.length());
		}
		if(excelARRNList.contains(tempARRN))
	//	if (true)
		{
			excelIndex=excelARRNList.indexOf(tempARRN);
			excelIndex = (nCaseRecordCounter-1);
			ARRNumber.setARRNVerifyFlag(true);
			if(excelMarketAuthHolderList.get(excelIndex).toString().equalsIgnoreCase(ARRNumber.getMarketAuthHolder()))
				ARRNumber.setMAHVerifyFlag(true);
			else
				ARRNumber.setMAHVerifyFlag(false);
			if(ARRNumber.getInitialReceivedDate().toString()!=null && !ARRNumber.getInitialReceivedDate().toString().equals("") 
				&& ARRNumber.getInitialReceivedDate().toString().contains("-"))
			{
			try {
				excelDate=excelFormat.parse(ARRNumber.getInitialReceivedDate().toString());
				} catch (ParseException e) {
				log.error("failed to parse Date. < "+e.getMessage()+" >");
				}
				destDate=destFormat.format(excelDate);
			}
			if(destDate.equalsIgnoreCase(excelInitialRecievedDateList.get(excelIndex).toString()))
				ARRNumber.setIRDVerifyFlag(true);
			else
				ARRNumber.setIRDVerifyFlag(false);
			if(excelSeriousReportList.get(excelIndex).toString().equalsIgnoreCase(ARRNumber.getSeriousReport()))
				ARRNumber.setSeriousReportVerifyFlag(true);
			else
				ARRNumber.setSeriousReportVerifyFlag(false);
		
		tempString=excelAgeList.get(excelIndex).toString().trim().substring(2, 3);
		tempString=excelAgeList.get(excelIndex).toString().trim();
		tempString=tempString.replaceAll(SPACE_JUNK, StringConstants.SPACE);
		if(tempString.trim().contains(StringConstants.SPACE))
		{
			
			excelAgeArr=tempString.trim().split(StringConstants.SPACE);
			excelAge=excelAgeArr[0];
			excelAgeUnit=excelAgeArr[1];
		}
		
		
		if(ARRNumber.getAge().equals(excelAge) && ARRNumber.getAgeUnit().equalsIgnoreCase(excelAgeUnit))
	//	if(ARRNumber.getAge().equals(excelAge))/////////////// && ageUnit.equalsIgnoreCase(excelAgeUnit))
			ARRNumber.setAgeVerifyFlag(true);
		else
			ARRNumber.setAgeVerifyFlag(false);
		if(excelGenderList.get(excelIndex).toString().equalsIgnoreCase(ARRNumber.getGender()))
			ARRNumber.setGenderVerifyFlag(true);
		else
			ARRNumber.setGenderVerifyFlag(false);
		if(excelSuspectProdBrandList.get(excelIndex).toString().equalsIgnoreCase(ARRNumber.getSuspectedProducts()))
			ARRNumber.setProductVerifyFlag(true);
		else
			ARRNumber.setProductVerifyFlag(false);
		if(excelAdverseReactionTermList.get(excelIndex).toString().equalsIgnoreCase(ARRNumber.getAdverseReactionTerms()))
			ARRNumber.setARTVerifyFlag(true);
		else
			ARRNumber.setARTVerifyFlag(false);
		
		
		}
		else
		{
			ARRNumber.setMissingARRNInExcelFlag(true);
		}
		if(ARRNumber.getARRNVerifyFlag() && ARRNumber.getMAHVerifyFlag() 
				&& ARRNumber.getIRDVerifyFlag()  && ARRNumber.getAgeVerifyFlag()
				&& ARRNumber.getGenderVerifyFlag() && ARRNumber.getSeriousReportVerifyFlag()
				&& ARRNumber.getProductVerifyFlag() && ARRNumber.getARTVerifyFlag()) 
		{
			ARRNumber.setMissingARRNInExcelFlag(false);
			verifiedIndexList.add(excelIndex);
		}
		else
			ARRNumber.setMissingARRNInExcelFlag(true);
		
		ARRNMap.put("ARRNumber", ARRNumber);
		ARRNMap.put("VerifiedARRNExcelIndex", verifiedIndexList);
		
		return ARRNMap;
	}
	
	public static DataTable getDataFromExcel(String path)
	{
		int nSheetIndex=0;
		int headerRowIndex=0;
		boolean bHeaderRowPresent;
		String p_sStagedFileName=null;
		String sColumnsToCollapse=null;
		DataTable oDataTableInitial=null;
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		
		p_sStagedFileName=path;
		bHeaderRowPresent=true;
		sColumnsToCollapse=StringConstants.EMPTY;
		try {
			headerRowIndex=1;
			oWorksheetWrapper = CpatToolAHelper.parseWorkbook(p_sStagedFileName, bHeaderRowPresent, nSheetIndex,headerRowIndex);
		} catch (SciException e) {
			log.error("Error while parsing workbook of Excel. <"+e.getMessage()+" >");
		}
		
		oDataTableInitial = CpatToolAHelper.convertToDatatable(oWorksheetWrapper, sColumnsToCollapse);
		for(int index = 0 ; index < oDataTableInitial.colCount(); index++)
		{
			String sKey = oDataTableInitial.getColumnHeader(index);
			String sValue= oDataTableInitial.getColumnValues(index).get(oDataTableInitial.getColumnValues(index).size()-1).toString().trim();
			 System.out.println(sKey +" : "+sValue);
		}
		
		return oDataTableInitial;
	}

}
