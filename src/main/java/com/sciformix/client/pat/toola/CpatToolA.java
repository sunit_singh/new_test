package com.sciformix.client.pat.toola;

import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.argus.ArgusWebApplicationWriter;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.PreviewCaseDoc;
import com.sciformix.client.pat.commons.ScpUser;
import com.sciformix.client.pat.commons.ToolDetails;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;


/**
 * @author rdcruz
 *
 */
public class CpatToolA extends CpatToolBase implements IAction
{
	private static final String HEALTH_CANADA_FILE_TYPE = "HealthCanada-AERReport-PDF";

	/**
	 * Serial Version UID.
	 */
	
	private static final Logger log = LoggerFactory.getLogger(CpatToolA.class);
	
	private ScpUser oScpUser = null;
	
	private static int activityId = 0;
	
	public static void main(String[] args)
	{
		System.out.println("Starting CPATDemo");
		 // Needed for parser. TODO: Remove once merged in SciPortal
		 SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
//		 XmlParserHome.init(oStartupLogger);
		 FileConfigurationHome.init(oStartupLogger,parserConfiguration);
		
		ToolDetails	l_tooldetails = new ToolDetails();
		l_tooldetails.setToolName("Sciformix ToolA");
		l_tooldetails.setToolDescription("ToolA");
		l_tooldetails.setToolDisplayName("");
		l_tooldetails.setToolHeading("");
		l_tooldetails.setToolTitle("ToolA");
		l_tooldetails.setToolUserInputScreen("apotexLogin.html");
		l_tooldetails.setToolVersion("CPAT.A.00.00");
		 
		CpatToolA oTool = new CpatToolA(l_tooldetails);
		log.info("ToolName : " + oTool.getToolDetails().getToolName() + " ToolVersion : " + oTool.getToolDetails().getToolVersion());
		oTool.render(CPAT_ACTION.INITIAL);
	}
	
	public CpatToolA(ToolDetails p_tooldetails) 
	{
		super(p_tooldetails);
	}
	
	private void render(CPAT_ACTION p_eAction)
	{
		performAction(p_eAction);
	}
	
	@SuppressWarnings("unchecked")
	public void performAction(CPAT_ACTION p_eAction, Object... oParameters)
	{
		if (p_eAction == CPAT_ACTION.INITIAL)
		{
			log.info("Login");
			
			CpatToolHome.showLoginScreen(this,this);
		}
		//TODO:Login is successful
		else if (p_eAction == CPAT_ACTION.LOGIN_SUCCESSFUL)
		{
			log.info("Login Successful");
			
			CpatToolHome.showUserInputsScreen(this,this);
			
		}
		else if (p_eAction == CPAT_ACTION.USER_INPUTS_COLLECTED)
		{
			Map<String, String> mapExtractedParams = null;
			if(oParameters.length > 0 && (oParameters[0] instanceof Map))
			{
				mapExtractedParams = (Map<String,String>) oParameters[0];
			}
			//TODO:Call Splitter here
			System.out.println("Time to call the Splitter");
			
			
			//TODO:Then display THIRD Screen
			CpatToolAHelper.showVerificationScreen(this,this, mapExtractedParams);
		}
		else if(p_eAction == CPAT_ACTION.VERIFICATION_SUCCESSFUL)
		{
			Map<String, String> mapExtractedParams = null;
			ParsedDataTable oDataTable = null;
			List<PreviewCaseDoc> listPreviewCaseDocs = null;
			File file = null;
			
			Map<String, Object> params= null;
			
			listPreviewCaseDocs = new ArrayList<>();
			
			if(oParameters.length>0 && (oParameters[0] instanceof Map))
			{
				mapExtractedParams = (Map<String,String>) oParameters[0];
				oDataTable = (ParsedDataTable) oParameters[1];
			}
			
			file = new File(mapExtractedParams.get("sdpath"));
			listPreviewCaseDocs.add(new PreviewCaseDoc(file, oDataTable, null));
			
			params = new HashMap<>();
			
			params.put(CpatToolConstants.ContentParams.CASE_DOC_LIST, listPreviewCaseDocs);
			params.put(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE, oDataTable);
			params.put(CpatToolConstants.ContentParams.CASE_FILE_TYPE, HEALTH_CANADA_FILE_TYPE);
			
			CpatToolAHelper.showPreviewScreen(this,this,params);
		}
		else if(p_eAction == CPAT_ACTION.DUPLICATE_SEARCH)
		{
			Map<String,String> mapExtractedParams = null;
			ParsedDataTable oDataTable = null;
			
			if(oParameters.length>0 && (oParameters[1] instanceof Map))
			{
				oDataTable = (ParsedDataTable) oParameters[0];
				mapExtractedParams = (Map<String,String>) oParameters[1];
				
				
			}
			
			CpatToolAHelper.generateWebWriterForDuplicateSearch(this,mapExtractedParams, oDataTable);
		}
		else if(p_eAction == CPAT_ACTION.DUPLICATE)
		{
			String returnMessage = null;
			Map<String,String> mapExtractedParams = null;
			ParsedDataTable oDataTable = null;
			WebDriver driver =null;
			if(oParameters.length>0)
			{
				returnMessage = (String) oParameters[0];
				mapExtractedParams = (Map<String,String>) oParameters[1];
				oDataTable = (ParsedDataTable) oParameters[2];
				driver = (WebDriver) oParameters[3];
						
			}
			ArgusWebApplicationWriter argusWriter = new ArgusWebApplicationWriter();
			try {
				argusWriter.nextCase(this,this, mapExtractedParams, oDataTable,returnMessage);
			} catch (SciException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			
		}
		else if(p_eAction == CPAT_ACTION.BOOKCASE)
		{
			String returnMessage = null;
			Map<String,String> mapExtractedParams = null;
			ParsedDataTable oDataTable = null;
			WebDriver driver =null;
			if(oParameters.length>0)
			{
				returnMessage = (String) oParameters[0];
				mapExtractedParams = (Map<String,String>) oParameters[1];
				oDataTable = (ParsedDataTable) oParameters[2];
				driver = (WebDriver) oParameters[3];
						
			}
			
			DuplicateWindow.createNewCase(this,this,driver, mapExtractedParams, oDataTable,returnMessage);
		}
		else if(p_eAction == CPAT_ACTION.FOLLOWUP)
		{
			
		}
	}
	
	
	protected void loadCpatFieldMap()
	{
//		s_mapCpatFieldsFromXml = new CpatFieldMap("JNJ-SCEP", "v1.0", "E2B", "SCEPTRE");
//
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Qualification", "FIELD", "Reporter.Qualification"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Title", "FIELD", "Reporter.Title"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Given Name", "FIELD", "Reporter.GivenName"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Anonymous/ Refused", "FIELD", "Reporter.Annonymous"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Organization", "FIELD", "Reporter.Organizataion"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Department", "FIELD", "Reporter.Department"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Address 1", "FIELD", "Reporter.Address1"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).City", "FIELD", "Reporter.City"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).State/ Province", "FIELD", "Reporter.State"));
//		s_mapCpatFieldsFromXml.addField(new CpatField("Reporter(1 of 2).Post/ Zip Code", "FIELD", "Reporter.PostalCode"));
	}
	
	public List<String> validateUserInputs(Map<String, String> mapExtractedParameters )
	{
		List<String> listErrors = null;
		
		String sUsername=null;
		String sPassword=null;
		String sDatabase=null;
		String sActionMode=null;
		String sSourceDocument=null;
		String sReconciliationExcel=null;
		String sIntakeMail=null;
		String sContactLog=null;
		String sProductConfig=null;
		Pattern userNamePattern =null, passwordPattern=null;
		Matcher userNameMatcher = null, passwordMatcher=null;
		String sSourceDocumentFileName = null;
		String sSourceDocumentFileExtension = null;
		String sReconciliationExcelFileName = null;
		String sReconciliationExcelFileExtension = null;
		String sIntakeMailFileName = null;
		String sIntakeMailFileExtension = null;
		String sContactLogFileName = null;
		String sContactLogFileExtension = null;
		String sProductConfigFileName = null;
		String sProductConfigFileExtension = null;
		
		sUsername = mapExtractedParameters.get("SCP_USERNAME");
		sPassword = mapExtractedParameters.get("SCP_PASSWORD");
		sDatabase = mapExtractedParameters.get("SCP_DATABASE");
		sActionMode = mapExtractedParameters.get("SCP_ACTIONMODE");
		sSourceDocument = mapExtractedParameters.get("SCP_SOURCEDOCUMENT");
		sReconciliationExcel = mapExtractedParameters.get("SCP_EXCELPATH");
		sIntakeMail = mapExtractedParameters.get("SCP_INTAKEMAIL");
		sContactLog = mapExtractedParameters.get("SCP_CONTACTLOG");
		sProductConfig = mapExtractedParameters.get("SCP_PRODUCTCONFIG");
		
		
		listErrors = new ArrayList<String>();
		
		//Validate input data
		/*************UserName and Password Validation*************/
		if (StringUtils.isNullOrEmpty(sUsername) || StringUtils.isNullOrEmpty(sPassword))
		{
			listErrors.add("Login credentials not supplied");
		}
		else
		{
			userNamePattern = Pattern.compile("^[a-zA-Z]*$");
			userNameMatcher = userNamePattern.matcher(sUsername);
			if(userNameMatcher.matches())
			{
				System.out.println("UserName macthes the condition");
			}
			else
			{
				listErrors.add("Username Invalid");
			}
			
			passwordPattern = Pattern.compile("^[a-zA-Z0-9!@#$%^&*()]*$");
			passwordMatcher = passwordPattern.matcher(sPassword);
			if(passwordMatcher.matches())
			{
				System.out.println("Password macthes the condition");
			}
			else
			{
				listErrors.add("Password Invalid");
			}
		}
		
		/*************Source Document Validation*************/
		if(StringUtils.isNullOrEmpty(sSourceDocument))
		{
			listErrors.add("Please enter the Source Document Path");	
		}
		else
		{
			File sourceDocumentPath = new File(sSourceDocument);
			
			sSourceDocumentFileName = sourceDocumentPath.getName();
			
			sSourceDocumentFileExtension = sSourceDocumentFileName.substring(sSourceDocumentFileName.lastIndexOf('.') + 1 , sSourceDocumentFileName.length());
			
			if(sourceDocumentPath.exists() && !sSourceDocumentFileExtension.equalsIgnoreCase("pdf"))
			{
				listErrors.add("The Source Document uploaded is not a PDF File");
			}
			else if(sourceDocumentPath.exists() && sourceDocumentPath.length()==0)
			{
				listErrors.add("The Source Document uploaded is an Empty File");
			}
			else if(!sourceDocumentPath.exists())
			{
				listErrors.add("The Source Document uploaded does not exist");
			}
			
		}
		
		/*************Reconciliation Excel Validation*************/
		if(StringUtils.isNullOrEmpty(sReconciliationExcel))
		{
			listErrors.add("Please enter the Reconciliation Excel Path");	
		}
		else
		{
			File reconciliationExcelPath = new File(sReconciliationExcel);
			
			sReconciliationExcelFileName = reconciliationExcelPath.getName();
			
			sReconciliationExcelFileExtension = sReconciliationExcelFileName.substring(sReconciliationExcelFileName.lastIndexOf('.') + 1 , sReconciliationExcelFileName.length());
			
			if(reconciliationExcelPath.exists() && !sReconciliationExcelFileExtension.equalsIgnoreCase("xlsx"))
			{
				listErrors.add("The Reconciliation Excel uploaded is not a Excel File");
			}
			else if(reconciliationExcelPath.exists() && reconciliationExcelPath.length()==0)
			{
				listErrors.add("The Reconciliation Excel  uploaded is an Empty File");
			}
			else if(!reconciliationExcelPath.exists())
			{
				listErrors.add("The Reconciliation Excel uploaded does not exist");
			}
			
		}
		
		/*************Intake Mail Validation*************/
		if(StringUtils.isNullOrEmpty(sReconciliationExcel))
		{
			listErrors.add("Please enter the Intake Mail Path");	
		}
		else
		{
			File intakeMailPath = new File(sIntakeMail);
			
			sIntakeMailFileName = intakeMailPath.getName();
			
			sIntakeMailFileExtension = sIntakeMailFileName.substring(sIntakeMailFileName.lastIndexOf('.') + 1 , sIntakeMailFileName.length());
			
			if(intakeMailPath.exists() && !sIntakeMailFileExtension.equalsIgnoreCase("pdf"))
			{
				listErrors.add("The Intake Mail uploaded is not a PDF File");
			}
			else if(intakeMailPath.exists() && intakeMailPath.length()==0)
			{
				listErrors.add("The Intake Mail  uploaded is an Empty File");
			}
			else if(!intakeMailPath.exists())
			{
				listErrors.add("The Intake Mail uploaded does not exist");
			}
			
		}
		
		/*************Contact Log Validation*************/
		if(StringUtils.isNullOrEmpty(sReconciliationExcel))
		{
			listErrors.add("Please enter the Contact Log Path");	
		}
		else
		{
			
			File contactLogPath = new File(sContactLog);
			
			sContactLogFileName = contactLogPath.getName();
			
			sContactLogFileExtension = sContactLogFileName.substring(sContactLogFileName.lastIndexOf('.') + 1 , sContactLogFileName.length());
			
			if(contactLogPath.exists() && !sContactLogFileExtension.equalsIgnoreCase("txt"))
			{
				listErrors.add("The Contact Log uploaded is not a TXT File");
			}
			else if(contactLogPath.exists() && contactLogPath.length()==0)
			{
				listErrors.add("The Contact Log  uploaded is an Empty File");
			}
			else if(!contactLogPath.exists())
			{
				listErrors.add("The Contact Log uploaded does not exist");
			}
			
		}
		
		/*************Product Configuration Validation*************/
		if(StringUtils.isNullOrEmpty(sReconciliationExcel))
		{
			listErrors.add("Please enter the Product Configuration Path");	
		}
		else
		{
			
			File productConfigPath = new File(sProductConfig);
			
			sProductConfigFileName = productConfigPath.getName();
			
			sProductConfigFileExtension = sProductConfigFileName.substring(sProductConfigFileName.lastIndexOf('.') + 1 , sProductConfigFileName.length());
			
			if(productConfigPath.exists() && !(sProductConfigFileExtension.equalsIgnoreCase("xlsx")|| sProductConfigFileExtension.equalsIgnoreCase("xls")))
			{
				listErrors.add("The Product Configuration uploaded is not a Excel File");
			}
			else if(productConfigPath.exists() && productConfigPath.length()==0)
			{
				listErrors.add("The Product Configuration uploaded is an Empty File");
			}
			else if(!productConfigPath.exists())
			{
				listErrors.add("The Product Configuration uploaded does not exist");
			}
			
		}
		return listErrors;
	}

	@Override
	public void userInputsScreenEventHandler(CpatToolBase oToolBase, IAction oCallbackAction, Component comp,
			JFrame frame) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void previewScreenEventHandler(CpatToolBase oToolBase, IAction m_oCallbackAction, Component comp,
			JFrame oWindowFrame, ParsedDataTable summaryDataTable, Map<String, String> parsedDataMap) {
		// TODO Auto-generated method stub
		
	}
}

