package com.sciformix.client.pat.toola;

import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApotexInsertValues{

	private static final String CONFIG_FOR_INSERT_PROPERTIES = "ConfigForInsert.properties";
	private static final Logger log = LoggerFactory.getLogger(ApotexInsertValues.class);

	
	public Map<String, String> load(Map<String, String> propMap) {
		
		Properties prop = null;
		InputStream input = null;
		String count;
		String fileLocation = null;
		String 	passwordFromUser = null;
		
		try {
		
			fileLocation =CONFIG_FOR_INSERT_PROPERTIES;
			input=ClassLoader.getSystemResourceAsStream(fileLocation);
			log.debug("Loading property file Data");
			prop=new Properties();
			prop.load(input);
			//put prop data into map
			for (String name: prop.stringPropertyNames())
			{
			    propMap.put(name, prop.getProperty(name));
			}
			    passwordFromUser = propMap.get("SCP_PASSWORD");
			    passwordFromUser = java.net.URLDecoder.decode(passwordFromUser, "UTF-8");
			    count="0";
			    propMap.put("count",count);

		} catch (Exception io) 
		{
			log.error("Error in load properties< "+ io.getMessage()+" >");
		}
		return propMap;
	}
	
}
