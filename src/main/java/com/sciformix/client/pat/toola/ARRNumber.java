package com.sciformix.client.pat.toola;

public class ARRNumber {
 private String ARRNumber;
 private boolean ARRNVerifyFlag;
 private String marketAuthHolder;
 private boolean MAHVerifyFlag;
 private String initialReceivedDate;
 private boolean IRDVerifyFlag;
 private String seriousReport;
 private boolean seriousReportVerifyFlag;
 private String age;
 private boolean ageVerifyFlag;
 private String ageUnit;
 private String gender;
 private boolean genderVerifyFlag;
 private String suspectedProducts;
 private boolean productVerifyFlag;
 private String adverseReactionTerms;
 private boolean ARTVerifyFlag;
 private boolean missingARRNInExcelFlag;
 private boolean missingARRNInSDFlag;
public String getARRNumber() {
	return ARRNumber;
}
public void setARRNumber(String aRRNumber) {
	ARRNumber = aRRNumber;
}
public String getMarketAuthHolder() {
	return marketAuthHolder;
}
public void setMarketAuthHolder(String marketAuthHolder) {
	this.marketAuthHolder = marketAuthHolder;
}
public String getInitialReceivedDate() {
	return initialReceivedDate;
}
public void setInitialReceivedDate(String initialReceivedDate) {
	this.initialReceivedDate = initialReceivedDate;
}
public String getSeriousReport() {
	return seriousReport;
}
public void setSeriousReport(String seriousReport) {
	this.seriousReport = seriousReport;
}
public String getAge() {
	return age;
}
public void setAge(String age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getSuspectedProducts() {
	return suspectedProducts;
}
public void setSuspectedProducts(String suspectedProducts) {
	this.suspectedProducts = suspectedProducts;
}
public String getAdverseReactionTerms() {
	return adverseReactionTerms;
}
public void setAdverseReactionTerms(String adverseReactionTerms) {
	this.adverseReactionTerms = adverseReactionTerms;
}
public boolean getARRNVerifyFlag() {
	return ARRNVerifyFlag;
}
public void setARRNVerifyFlag(boolean aRRNVerifyFlag) {
	ARRNVerifyFlag = aRRNVerifyFlag;
}
public boolean getMAHVerifyFlag() {
	return MAHVerifyFlag;
}
public void setMAHVerifyFlag(boolean mAHVerifyFlag) {
	MAHVerifyFlag = mAHVerifyFlag;
}
public boolean getIRDVerifyFlag() {
	return IRDVerifyFlag;
}
public void setIRDVerifyFlag(boolean iRDVerifyFlag) {
	IRDVerifyFlag = iRDVerifyFlag;
}
public boolean getSeriousReportVerifyFlag() {
	return seriousReportVerifyFlag;
}
public void setSeriousReportVerifyFlag(boolean seriousReportVerifyFlag) {
	this.seriousReportVerifyFlag = seriousReportVerifyFlag;
}
public boolean getAgeVerifyFlag() {
	return ageVerifyFlag;
}
public void setAgeVerifyFlag(boolean ageVerifyFlag) {
	this.ageVerifyFlag = ageVerifyFlag;
}
public boolean getGenderVerifyFlag() {
	return genderVerifyFlag;
}
public void setGenderVerifyFlag(boolean genderVerifyFlag) {
	this.genderVerifyFlag = genderVerifyFlag;
}
public boolean getProductVerifyFlag() {
	return productVerifyFlag;
}
public void setProductVerifyFlag(boolean productVerifyFlag) {
	this.productVerifyFlag = productVerifyFlag;
}
public boolean getARTVerifyFlag() {
	return ARTVerifyFlag;
}
public void setARTVerifyFlag(boolean aRTVerifyFlag) {
	ARTVerifyFlag = aRTVerifyFlag;
}
public boolean isMissingARRNInExcelFlag() {
	return missingARRNInExcelFlag;
}
public void setMissingARRNInExcelFlag(boolean missingARRNInExcelFlag) {
	this.missingARRNInExcelFlag = missingARRNInExcelFlag;
}
public boolean isMissingARRNInSDFlag() {
	return missingARRNInSDFlag;
}
public void setMissingARRNInSDFlag(boolean missingARRNInSDFlag) {
	this.missingARRNInSDFlag = missingARRNInSDFlag;
}
public String getAgeUnit() {
	return ageUnit;
}
public void setAgeUnit(String ageUnit) {
	this.ageUnit = ageUnit;
}

 
 
}
