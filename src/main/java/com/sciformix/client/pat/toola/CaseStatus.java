package com.sciformix.client.pat.toola;

public class CaseStatus {
	String ARRNumber;
	String caseId;
	String reportInitial_FU;
	String reportSerious;
	String status;
	public String getARRNumber() {
		return ARRNumber;
	}
	public void setARRNumber(String aRRNumber) {
		ARRNumber = aRRNumber;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getReportInitial_FU() {
		return reportInitial_FU;
	}
	public void setReportInitial_FU(String reportInitial_FU) {
		this.reportInitial_FU = reportInitial_FU;
	}
	public String getReportSerious() {
		return reportSerious;
	}
	public void setReportSerious(String reportSerious) {
		this.reportSerious = reportSerious;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
}
