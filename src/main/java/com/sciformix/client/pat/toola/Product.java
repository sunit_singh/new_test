package com.sciformix.client.pat.toola;

import java.util.Date;

public class Product {

	String productName=null;
	String HPRole=null;
	String dosageForm=null;
	String routOfAdmin=null;
	String dose=null;
	String doseUnit=null;
	String frequency=null;
	String therapyDuration=null;
	String indication=null;
	String approvalDate=null;
	Date formatedWithdrawDate=null;
	boolean verifiedFlag=false;
	public Product()
	{
		
	}
	public Product(String productName, String HPRole, String dosageForm, String routOfAdmin, 
			String dose,String doseUnit, String freq, String therapyDuration, String indication, Date withdraDate, boolean verifiedFlag)
	{
		this.productName=productName;
		this.HPRole=HPRole;
		this.dosageForm=dosageForm;
		this.routOfAdmin=routOfAdmin;
		this.dose=dose;
		this.doseUnit=doseUnit;
		this.frequency=freq;
		this.therapyDuration=therapyDuration;
		this.indication=indication;
		this.formatedWithdrawDate=withdraDate;
		this.verifiedFlag=verifiedFlag;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getHPRole() {
		return HPRole;
	}
	public void setHPRole(String hPRole) {
		HPRole = hPRole;
	}
	public String getDosageForm() {
		return dosageForm;
	}
	public void setDosageForm(String dosageForm) {
		this.dosageForm = dosageForm;
	}
	public String getRoutOfAdmin() {
		return routOfAdmin;
	}
	public void setRoutOfAdmin(String routOfAdmin) {
		this.routOfAdmin = routOfAdmin;
	}
	public String getDose() {
		return dose;
	}
	public void setDose(String dose) {
		this.dose = dose;
	}
	
	public String getDoseUnit() {
		return doseUnit;
	}
	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getTherapyDuration() {
		return therapyDuration;
	}
	public void setTherapyDuration(String therapyDuration) {
		this.therapyDuration = therapyDuration;
	}
	public String getIndication() {
		return indication;
	}
	public void setIndication(String indication) {
		this.indication = indication;
	}
	public String getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}
	public Date getFormatedWithdrawDate() {
		return formatedWithdrawDate;
	}
	public void setFormatedWithdrawDate(Date formatedWithdrawDate) {
		this.formatedWithdrawDate = formatedWithdrawDate;
	}
	public boolean isVerifiedFlag() {
		return verifiedFlag;
	}
	public void setVerifiedFlag(boolean verifiedFlag) {
		this.verifiedFlag = verifiedFlag;
	}
	
}
