package com.sciformix.client.pat.commons;

import java.util.Map;

public abstract class 
	DuplicateFields
{
	protected boolean
		reporterGivenName
	;
	protected boolean
		reporterFamilyName
	;
	protected boolean
		patientDOB
	;
	protected boolean
		patientInitials
	;
	protected boolean
		patientGender
	;
	protected boolean
		country
	;
	public DuplicateFields(boolean reporterGivenName, boolean reporterFamilyName, boolean patientDOB,
			boolean patientInitials, boolean patientGender, boolean country) {
		super();
		this.reporterGivenName = reporterGivenName;
		this.reporterFamilyName = reporterFamilyName;
		this.patientDOB = patientDOB;
		this.patientInitials = patientInitials;
		this.patientGender = patientGender;
		this.country = country;
	}
	public abstract boolean isSearchToBePerformed(Map<String, Boolean> p_dupsearchcriteriamap);
	public abstract String createSearchCriteria();
	public boolean isReporterGivenName() {
		return reporterGivenName;
	}
	public boolean isReporterFamilyName() {
		return reporterFamilyName;
	}
	public boolean isPatientDOB() {
		return patientDOB;
	}
	public boolean isPatientInitials() {
		return patientInitials;
	}
	public boolean isPatientGender() {
		return patientGender;
	}
	public boolean isCountry() {
		return country;
	}
}
