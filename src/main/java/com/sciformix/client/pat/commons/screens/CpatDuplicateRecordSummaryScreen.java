package com.sciformix.client.pat.commons.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.DuplicateRecordFields;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.toolc.CpatToolC;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;

public class CpatDuplicateRecordSummaryScreen extends CpatBaseHtmlScreen implements ActionListener {
	
	private static final long serialVersionUID = 3350418882184107366L;
	private static Logger log = LoggerFactory.getLogger(CpatDuplicateRecordSummaryScreen.class);
	private static ParsedDataTable summaryDataTable = null;
	private static Map<String, List<DuplicateRecordFields>> dupRecList = null;
	private static CpatToolC oTool = null;
	
	public CpatDuplicateRecordSummaryScreen(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, Object> params)
	{
		super(oToolBase, oCallbackAction, oToolBase.getToolDetails().getToolTitle(), 600, 1200, null, params);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}

	@Override
	protected Component prepareContentFrame(String p_sPageToLoad, Map<String, Object> params) {
		log.info("Duplicate Record Summary Screen: Rendering");
		
		
		String irtDuplicateTable = null;
		String sceptreDuplicateTable = null;
		String currentCaseTable = null;
		DuplicateRecordFields currentCaseDataFields= null;
		boolean enableDataEntryFlag = false;
		
		List<DuplicateRecordFields> irtList = null, sceptrelist = null;
		
		JPanel mainPanel = null;
		JPanel headerPanel = null;
		JPanel bodyPanel = null;
		JPanel footerPanel = null;
		JLabel viewLabel = null;
		
		JScrollPane summaryScrollPane = null;
		
		JButton initialButton = null, abortButton = null, followUpButton = null;
		
		JLabel aerLabel = null, versionType = null; 
		JTextField inputAer = null;
		String [] selectVersionList = {"Major","Minor"}; 
		JComboBox<String> selectVersionType = null;
		
		dupRecList = (Map<String, List<DuplicateRecordFields>>)params.get(CpatToolConstants.UI.DUPLIACTE_REC_MAP);
		currentCaseDataFields = (DuplicateRecordFields)params.get(CpatToolConstants.ContentParams.DUPLICATE_RECORD);
		summaryDataTable = (ParsedDataTable) params.get(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE);
		enableDataEntryFlag = (boolean) params.get(CpatToolConstants.ContentParams.ENABLE_DATA_ENTRY);
		//Header
		headerPanel = new  JPanel();
			viewLabel = new JLabel("Duplicate Search Result");
		headerPanel.add(viewLabel);
		
		//Body
		irtList = dupRecList.get(CpatToolConstants.DUPLICATE_IRT_RECORD_LIST);
		sceptrelist = dupRecList.get(CpatToolConstants.DUPLICATE_SCEPTRE_RECORD_LIST);
		
		irtDuplicateTable = createHtmlBody(irtList,currentCaseDataFields, "Irt Duplicate Search Result");
		sceptreDuplicateTable = createHtmlBody(sceptrelist,currentCaseDataFields,  "Sceptre Duplicate Search Result");
		currentCaseTable = createCurrentCaseHtmlBody(currentCaseDataFields);
		
		bodyPanel = createHtmlContentViewPaneForSummary(createHtmlBodyDiv(currentCaseTable, irtDuplicateTable, sceptreDuplicateTable));
		
		summaryScrollPane = new JScrollPane(bodyPanel);
		//Footer 
		footerPanel = new JPanel();
			aerLabel = new JLabel(CpatToolConstants.UI.AER_NO);
			inputAer = new JTextField();
			inputAer.setPreferredSize( new Dimension(150, 24));
			versionType = new JLabel(CpatToolConstants.UI.VERSION_TYPE);
			selectVersionType = new JComboBox<>(selectVersionList);
		
			initialButton = new JButton(CpatToolConstants.UI.PREVIEW_BUTTON_INITIAL);
			followUpButton = new JButton(CpatToolConstants.UI.PREVIEW_BUTTON_FOLLOWUP);
			abortButton = new JButton(CpatToolConstants.UI.PREVIEW_BUTTON_ABORT);
			
		if(enableDataEntryFlag)
		{
			if(irtList != null && !irtList.isEmpty() || sceptrelist != null && !sceptrelist.isEmpty())
			{
				footerPanel.add(aerLabel);
				footerPanel.add(inputAer);
				footerPanel.add(versionType);
				footerPanel.add(selectVersionType);
			}
			
			footerPanel.add(initialButton);
			
			if(irtList != null && !irtList.isEmpty() || sceptrelist != null && !sceptrelist.isEmpty())
			{
				footerPanel.add(followUpButton);
			}
		}
			
		footerPanel.add(abortButton);
		
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(headerPanel, BorderLayout.NORTH);
		mainPanel.add(summaryScrollPane, BorderLayout.CENTER);
		mainPanel.add(footerPanel, BorderLayout.SOUTH);
		
		mainPanel.setBackground(Color.BLUE);
		return mainPanel;
	}

	@Override
	protected void addActionListener(Component comp, JFrame frame) {
		log.info("Preview Screen: Add Action Listeners");
		oTool = (CpatToolC) oToolBase;
		oTool.DuplicateSummaryScreenEventHandler(oToolBase, m_oCallbackAction, comp, oWindowFrame,dupRecList, summaryDataTable);		
	}
	
	/**
	 * @author gwandhare
	 * @param currentCaseField
	 * @param irtDuplicateTableString
	 * @param sceptreDuplicateTableString
	 * @return return html format string in duplicate preview body panel 
	 */
	private String createHtmlBodyDiv(String currentCaseField, String irtDuplicateTableString,String sceptreDuplicateTableString)
	{
		StringBuffer  duplicateSearchBody = new StringBuffer();
		
		if(!StringUtils.isNullOrEmpty(irtDuplicateTableString) || !StringUtils.isNullOrEmpty(sceptreDuplicateTableString))
		{
		duplicateSearchBody
				.append("<table>")
					.append("<tr>")
						.append("<td>"+currentCaseField+"</td>");
		if(!StringUtils.isNullOrEmpty(irtDuplicateTableString))
		{
			duplicateSearchBody.append("<td>"+irtDuplicateTableString+"</td>");
		}
		else
		{
			duplicateSearchBody.append("<td> No Duplicates Found in IRT</td>");
		}
		
		if(!StringUtils.isNullOrEmpty(sceptreDuplicateTableString))
		{
			duplicateSearchBody	.append("<td>"+sceptreDuplicateTableString+"</td>");
		}
		else
		{
			duplicateSearchBody.append("<td> No Duplicates Found in Sceptre</td>");
		}
		
		
		duplicateSearchBody.append("</tr>")
				.append("</table>");
		}
		else
		{
			duplicateSearchBody.append("<br /><br /><br /><b> No Duplicates Found</b> <br /><br /><br />");
		}
		return duplicateSearchBody.toString();
	}
	
	/**
	 * @author gwandhare
	 * @param field
	 * @return html table of duplicate field 
	 */
	private String createCurrentCaseHtmlBody(DuplicateRecordFields field)
	{
		StringBuffer  htmlBody = new StringBuffer();
		String countryOfOccur = null;
		if(field != null)
		{	
			if(StringUtils.emptyString(field.getCountryWhereOccure()).trim().equals(CpatToolConstants.STRING_NULL))
			{
				countryOfOccur = StringConstants.EMPTY;
			}
			else
			{
				countryOfOccur = field.getCountryWhereOccure();
			}
			
			htmlBody.append("<h3  align=\"center\">Current Case</h3> <br />");
			htmlBody.append("<table border=\"1\" bgcolor='#f2f9f9'> <thead>");
				htmlBody.append("<tr> <th align='left'>Aer No</th><td>"+StringUtils.emptyString(field.getAerNo())+"</td></tr>");
			htmlBody.append("</thead><tdody>");
				htmlBody.append("<tr> <th align='left'>Local Case ID</th><td>"+StringUtils.emptyString(field.getLocalCaseId())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Patient Initials</th><td>"+StringUtils.emptyString(field.getPatientInitials())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Patient Gender</th><td>"+StringUtils.emptyString(field.getPatientGender())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Patient DOB</th><td>"+StringUtils.emptyString(field.getPatientDOB())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Country of Occurrence</th><td>"+StringUtils.emptyString(countryOfOccur)+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reporter Family Name</th><td>"+StringUtils.emptyString(field.getReporterFamilyName())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reporter Given Name</th><td>"+StringUtils.emptyString(field.getReporterGivenName())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reporter Country</th><td>"+StringUtils.emptyString(field.getReporterCountry())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reported Drug Name</th><td>"+StringUtils.emptyString(field.getReportedDrugName())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reaction Rank</th><td>"+StringUtils.emptyString(field.getReactionRecordRank())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reported Reaction</th><td>"+StringUtils.emptyString(field.getReactionRecordReportedReaction())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reaction Seriousness</th><td>"+StringUtils.emptyString(field.getReactionRecordSeriousness())+"</td></tr>");
				htmlBody.append("<tr> <th align='left'>Reaction Outcome</th><td>"+StringUtils.emptyString(field.getReactionRecordOutcome())+"</td></tr>");
				
			htmlBody.append("<tbody></table>");
		}
		return htmlBody.toString();
		
	}
	
	/**
	 * @author gwandhare
	 * @param SourceDocField
	 * @param fieldValue
	 * @return  duplicate field in html row if match with source doc field else highlited  table row
	 */
	private String highlightDifferField(String SourceDocField, String fieldValue)
	{
		String htmlString = null;
		
		if(StringUtils.emptyString(fieldValue).trim().equals(CpatToolConstants.STRING_NULL))
		{
			fieldValue = StringConstants.EMPTY;
		}
		
		if(StringUtils.emptyString(SourceDocField).trim().equals(StringUtils.emptyString(fieldValue).trim()))
		{
			htmlString = "<td>"+StringUtils.emptyString(fieldValue)+"</td>";
		}
		else
		{
			htmlString = "<td bgcolor='#fff9aa'>"+StringUtils.emptyString(fieldValue)+"</td>";
		}
		return htmlString;
	}
	
	/**
	 * @author gwandhare
	 * @param field
	 * @return html table of duplicate field list
	 */
	private String createHtmlBody(List<DuplicateRecordFields>  duplicateFieldMap, DuplicateRecordFields sourceDoc ,String title)
	{
		StringBuffer  htmlBody = new StringBuffer();
		if(duplicateFieldMap != null && !duplicateFieldMap.isEmpty())
		{
			StringBuffer  tblLocIdRow = new StringBuffer();
			StringBuffer  tblPatInitRow = new StringBuffer();
			StringBuffer  tblPatGendredRow = new StringBuffer();
			StringBuffer  tblPatDobIdRow = new StringBuffer();
			StringBuffer  tblRepFamNameRow = new StringBuffer();
			StringBuffer  tblRepGivenNameRow = new StringBuffer();
			StringBuffer  tblRepCountryRow = new StringBuffer();
			StringBuffer  tblAerNoRow = new StringBuffer();
			StringBuffer  tblrankRow = new StringBuffer();
			StringBuffer  tblReportedReactionRow = new StringBuffer();
			StringBuffer  tblSeriousness = new StringBuffer();
			StringBuffer  tblOutcome = new StringBuffer();
			StringBuffer  tblDrugName = new StringBuffer();
			StringBuffer  tblOccurCountry = new StringBuffer();
			
			for(DuplicateRecordFields field: duplicateFieldMap)
			{
				tblAerNoRow.append("<th align=\"left\">"+StringUtils.emptyString(field.getAerNo())+"</th>");
				tblLocIdRow.append( highlightDifferField(sourceDoc.getLocalCaseId(),field.getLocalCaseId()));
				tblPatInitRow.append(highlightDifferField(sourceDoc.getPatientInitials(),field.getPatientInitials()));
				tblPatGendredRow.append(highlightDifferField(sourceDoc.getPatientGender(),field.getPatientGender()));
				tblRepFamNameRow.append(highlightDifferField(sourceDoc.getReporterFamilyName(),field.getReporterFamilyName()));
				tblRepGivenNameRow.append(highlightDifferField(sourceDoc.getReporterGivenName() ,field.getReporterGivenName()));
				tblPatDobIdRow.append(highlightDifferField(sourceDoc.getPatientDOB(),field.getPatientDOB()));
				tblRepCountryRow.append(highlightDifferField(sourceDoc.getReporterCountry(),field.getReporterCountry()));
				tblDrugName.append(highlightDifferField(sourceDoc.getReportedDrugName(),field.getReportedDrugName()));
				tblrankRow.append(highlightDifferField(sourceDoc.getReactionRecordRank(),field.getReactionRecordRank()));
				tblReportedReactionRow.append(highlightDifferField(sourceDoc.getReactionRecordReportedReaction(),field.getReactionRecordReportedReaction()));
				tblSeriousness.append(highlightDifferField(sourceDoc.getReactionRecordSeriousness(),field.getReactionRecordSeriousness()));
				tblOutcome.append(highlightDifferField(sourceDoc.getReactionRecordOutcome(),field.getReactionRecordOutcome()));
				tblOccurCountry.append(highlightDifferField(sourceDoc.getCountryWhereOccure(),field.getCountryWhereOccure()));
				
			}
				
			htmlBody.append("<h3 align=\"center\">"+title+"</h3> <br />");
			htmlBody.append("<table border=\"1\" bgcolor='#f2f9f9'><thead>");
				htmlBody.append("<tr>"+tblAerNoRow+"</tr>");
			htmlBody.append("</thead><tdody>");
				htmlBody.append("<tr>"+tblLocIdRow+"</tr>");
				htmlBody.append("<tr>"+tblPatInitRow+"</tr>");
				htmlBody.append("<tr>"+tblPatGendredRow+"</tr>");
				htmlBody.append("<tr>"+tblPatDobIdRow+"</tr>");
				htmlBody.append("<tr>"+tblOccurCountry+"</tr>");
				htmlBody.append("<tr>"+tblRepFamNameRow+"</tr>");
				htmlBody.append("<tr>"+tblRepGivenNameRow+"</tr>");
				htmlBody.append("<tr>"+tblRepCountryRow+"</tr>");
				htmlBody.append("<tr>"+tblDrugName+"</tr>");
				htmlBody.append("<tr>"+tblrankRow+"</tr>");
				htmlBody.append("<tr>"+tblReportedReactionRow+"</tr>");
				htmlBody.append("<tr>"+tblSeriousness+"</tr>");
				htmlBody.append("<tr>"+tblOutcome+"</tr>");
			htmlBody.append("<tbody></table>");
		}
		return htmlBody.toString();
	}
	
	/**
	 * @author gwandhare
	 * @param htmlTable
	 * @return Jpanel of passed html string
	 */
	public JPanel createHtmlContentViewPaneForSummary(String htmlTable)
	{
		log.info("createHtmlContentViewPaneForSummary");
		
		JPanel panel = null;
		JEditorPane jep = null;
		
		panel = new JPanel();
		jep = new JEditorPane();
		
		jep.setContentType("text/html");
		jep.setText(htmlTable);
		jep.setEditable(false);
		
		panel.add(jep);

		return panel;
	}
}
