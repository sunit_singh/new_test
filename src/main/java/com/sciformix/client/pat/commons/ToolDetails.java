package com.sciformix.client.pat.commons;

public class ToolDetails {
	
	private String toolName;
	private String toolDisplayName;
	private String toolVersion;
	private String toolTitle;
	private String toolDescription;
	private String toolHeading;
	private String toolUserInputScreen;
	
	public String getToolName() {
		return toolName;
	}
	public void setToolName(String toolName) {
		this.toolName = toolName;
	}
	public String getToolDisplayName() {
		return toolDisplayName;
	}
	public void setToolDisplayName(String toolDisplayName) {
		this.toolDisplayName = toolDisplayName;
	}
	public String getToolVersion() {
		return toolVersion;
	}
	public void setToolVersion(String toolVersion) {
		this.toolVersion = toolVersion;
	}
	public String getToolTitle() {
		return toolTitle;
	}
	public void setToolTitle(String toolTitle) {
		this.toolTitle = toolTitle;
	}
	public String getToolDescription() {
		return toolDescription;
	}
	public void setToolDescription(String toolDescription) {
		this.toolDescription = toolDescription;
	}
	public String getToolHeading() {
		return toolHeading;
	}
	public void setToolHeading(String toolHeading) {
		this.toolHeading = toolHeading;
	}
	public String getToolUserInputScreen() {
		return toolUserInputScreen;
	}
	public void setToolUserInputScreen(String toolUserInputScreen) {
		this.toolUserInputScreen = toolUserInputScreen;
	}
	
}
