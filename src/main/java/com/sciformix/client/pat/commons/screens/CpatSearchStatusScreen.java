package com.sciformix.client.pat.commons.screens;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.ScpUser;
import com.sciformix.client.pat.toolb.CpatToolBConstants;
import com.sciformix.client.pat.toolb.CpatToolBErrorMessages;
import com.sciformix.client.pat.toolb.CpatToolBHelper;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse.STATUS;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;

public class CpatSearchStatusScreen extends CpatBaseHtmlScreen {
	
	private static final long serialVersionUID = 600887759612418148L;

	private static Logger log = LoggerFactory.getLogger(CpatSearchStatusScreen.class);
	
	private static List<Map<String, String>> dupSummaryList = null;
	private static ParsedDataTable summaryDataTable = null;
	
	private static boolean isDuplicateCase = false;
	
	private static Map<String, String> sceptreMap = null;
	
	private static Map<String, String> payload = null;
	
	private static SciPortalWebApiResponse response = null;	
	
	private String potentialDupCaseIds = null;
	
	private static ScpUser oScpUser = null;
	
	private static JButton continueButton = null;
	private static JButton abortButton = null;
	
	public CpatSearchStatusScreen(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, Object> params) 
	{
		super(oToolBase, oCallbackAction, oToolBase.getToolDetails().getToolTitle(), 550, 1200, null, params);
	}

	@Override
	protected Component prepareContentFrame(String p_sPageToLoad, Map<String, Object> params) 
	{
		log.info("Cpat Search Status Screen: Rendering");
		JPanel mainPanel = null;
		
		JLabel infoLabel = null;
		JPanel actionPanel = null;
		
		JEditorPane dupSummaryPane = null;
		JScrollPane scrollPane = null;
		
		Map<String, String> userInputMap = null; 
		
		// Params
		summaryDataTable = (ParsedDataTable) params.get(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE);
		sceptreMap = (Map<String, String>) params.get(CpatToolConstants.ContentParams.SCEPTRE_MAP);
		dupSummaryList = (List<Map<String, String>>) params.get(CpatToolConstants.ContentParams.DUP_SUMMARYLIST);
		isDuplicateCase = (boolean) params.get(CpatToolConstants.ContentParams.ISDUPLICATE);
		
		
		payload = new HashMap<>();
		userInputMap = oToolBase.userInputsParams;
		oScpUser = oToolBase.scpUser;

		infoLabel = new JLabel("Case ID: " + userInputMap.get(CpatToolBConstants.UserInputs.SCEPTRE_CASE_ID), SwingConstants.CENTER);
	
		dupSummaryPane = new JEditorPane();
		dupSummaryPane.setContentType("text/html");
		dupSummaryPane.setEditable(false);
		dupSummaryPane.setCaretPosition(0);
		
		potentialDupCaseIds = getPotentialDupCaseIds(dupSummaryList);
		
		continueButton = new JButton(CpatToolConstants.UI.DUPLICATE_SCREEN_BUTTON_CONTINUE);
		continueButton.addActionListener(new ActionListener() 
		{
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int activityId = 0;
				String chromeVersion = null;
				String ieVersion = null;
	
				payload.put(CpatToolBConstants.PayloadKeys.POTENTIAL_DUPLICATES, potentialDupCaseIds);
				
				if(oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERNAME).equalsIgnoreCase(CpatToolConstants.Browser.CHROME))
				{
					chromeVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
					ieVersion = StringConstants.EMPTY;
				}
				else
				{
					chromeVersion = StringConstants.EMPTY;
					ieVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
				}
				response = oToolBase.scpClient.registerActivity(oScpUser.getUserId(), oScpUser.getPassword(),
							oScpUser.getUserProjectsList().get(0).getProjectSeqId(), oToolBase.getToolDetails().getToolName(),
							CpatToolConstants.ACTIVITY_TYPE.EXISTING_SEARCH , oToolBase.activityId,
							CpatToolConstants.ACTIVITY_CODE.EXISTING_SEARCH, "", payload, chromeVersion,
							ieVersion);
				
				if(response.getStatus() == STATUS.SUCCESS)
				{
					activityId = Integer.parseInt(response.getData().toString());
					
					if(activityId > 0)
					{
						oToolBase.activityId = activityId;
						
						disposeScreen();
						m_oCallbackAction.performAction(CPAT_ACTION.DATA_ENTRY, summaryDataTable, sceptreMap, isDuplicateCase);
					}
					else
					{
						JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.API.ACTIVITYID_ERR);
					}
					
				}
				else
				{
					JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.ERROR + response.getCode() + " - " + response.getMessage());
				}
				
								
			}
		});
		
		abortButton = new JButton(CpatToolConstants.UI.DUPLICATE_SCREEN_BUTTON_ABORT);
		abortButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				disposeScreen();
				payload.put(CpatToolBConstants.PayloadKeys.USER_CHOICE, CpatToolBConstants.SearchStatusScreen.PAYLOAD_ABORT);
				CpatToolHome.callCommonRegisterActivity(oToolBase, m_oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.EXISTING_SEARCH , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.EXISTING_SEARCH, "", payload);
				
				CpatToolBHelper.closeWriterDriver();
				m_oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
			}
		});
		
		if(dupSummaryList.size() > 1)
		{
			try 
			{
				dupSummaryPane.setText(getSummary(dupSummaryList));
			} catch (SciException e1)
			{
				log.error("Could not generate duplicate summary" , null , e1);
				dupSummaryPane.setText("Could not generate duplicate summary");
			}
		}
		else
		{
			dupSummaryPane.setText(CpatToolBConstants.SearchStatusScreen.DUPLICATE_LABEL);
		}
		
		actionPanel = new JPanel();
		actionPanel.add(continueButton);
		actionPanel.add(abortButton);
		
		scrollPane = new JScrollPane(dupSummaryPane);
		
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(infoLabel, BorderLayout.NORTH);
		mainPanel.add(scrollPane, BorderLayout.CENTER);
		mainPanel.add(actionPanel, BorderLayout.SOUTH);
		
		return mainPanel;
	}


	@Override
	protected void addActionListener(Component comp, JFrame frame) 
	{
		log.info("Search Status Screen: Adding Action Listeners");
		
		frame.addWindowListener(new WindowAdapter() 
		{
			@Override
			public void windowClosing(WindowEvent e) 
			{
				String[] buttons = { CpatToolConstants.OPTION_YES, CpatToolConstants.OPTION_NO};
				int rc = JOptionPane.showOptionDialog(null, CpatToolConstants.ABORT_MSG_DUPSUMMARY_SCREEN, CpatToolConstants.OPTION_DIALOG_TITLE,
				        JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[1]);	 
			    
			    if(rc == 0)
			    {
			    	abortButton.doClick();
			    }
			}
		});
	}
	
	private String getPotentialDupCaseIds(List<Map<String, String>> dupSumList) 
	{
		log.info("getPotentialDupCaseIds");
		
		StringBuilder sb = null;
		
		sb = new StringBuilder();
		
		for(int i = 1; i < dupSumList.size(); i++)
		{
			sb = sb.length() > 1? sb.append("," + dupSumList.get(i).get(CpatToolBConstants.AER)): sb.append(dupSumList.get(i).get(CpatToolBConstants.AER));
		}
		return sb.toString();
	}

	private String getSummary(List<Map<String, String>> dupSumList) throws SciException 
	{
		StringBuilder html = null;
		Map<String, String> map = null;
		String value = null;
		int firstRow = 0;
		String initialValue = null;
		html = new StringBuilder();
		
		html.append(getHtmlPrefix());
		
		for(String key : dupSumList.get(0).keySet())
		{
			html.append("<tr><td style='border: 1px solid black; background-color:#CECEBE;'><b>" + key + "</b></td>");
			firstRow++;
			
			for (int index = 0; index < dupSumList.size(); index++)
			{
				
				
				map = dupSumList.get(index);
				value = map.get(key);
				if(firstRow==1)
				{
					html.append("<td bgcolor='#CECEBE' style='border:1px solid black;'>" + value+ "</td>");
				}
				else if(index==0)
				{
					initialValue = value;
					html.append("<td bgColor='#F8F99A' style='border:1px solid black;'>" + value+ "</td>");
				}
				else
				{
					if(key.equalsIgnoreCase(CpatToolBConstants.DupSummaryKeys.PATIENT_DOB))
					{
						
						if(!StringUtils.isNullOrEmpty(initialValue) && !StringUtils.isNullOrEmpty(value))
						{
							if(CpatToolBHelper.firstDateAfterOrBeforeLastDate(initialValue, value) == 0)
							{
								html.append("<td bgColor='#F8F99A' style='border:1px solid black;'>" + value+ "</td>");	
							}
							else
							{
								html.append("<td style='border:1px solid black;'>" + value+ "</td>");
							}							
						}
						else
						{
							if(initialValue.equalsIgnoreCase(value))
							{
								html.append("<td bgColor='#F8F99A' style='border:1px solid black;'>" + value+ "</td>");		
							}
							else
							{
								html.append("<td style='border:1px solid black;'>" + value+ "</td>");
							}
						}
						
					}
					else 
					{
						if(initialValue.equalsIgnoreCase(value))
						{
							html.append("<td bgColor='#F8F99A' style='border:1px solid black;'>" + value+ "</td>");		
						}
						else
						{
							html.append("<td style='border:1px solid black;'>" + value+ "</td>");
						}
					}
				}
				
			}
			
			html.append("</tr>");
		}

		html.append(getHtmlSuffix());
		
		return html.toString();
	}

	private String getHtmlPrefix() 
	{
		log.info("getHtmlPrefix");
		
		String htmlPrefix = null;
		htmlPrefix = "<!DOCTYPE html><html><body style='background-color:#F5F6F7;'> <table style='border:0px; cellpadding:0; border-spacing:0; font-family:Arial;font-size:12; background-color:#F5F6F7;'>";
					
		return htmlPrefix;
	}
	
	private String getHtmlSuffix()
	{
		String htmlSuffix = null;
		htmlSuffix = "</table> </body> </html>";
		
		return htmlSuffix;
	}

}
