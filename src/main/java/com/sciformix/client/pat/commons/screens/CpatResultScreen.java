package com.sciformix.client.pat.commons.screens;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.BaseWebApplicationWriter;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.utils.StringUtils;

public class CpatResultScreen extends CpatBaseHtmlScreen
{
	private static final long serialVersionUID = 7456031135692746522L;

	private static final Logger log = LoggerFactory.getLogger(CpatResultScreen.class);
	
	private static BaseWebApplicationWriter oAppWriter = null;
	
	private static JButton button = null;
	
	private static TableRowSorter<TableModel> rowSorter = null;
	private static JTextField tfSearch = null;
	
	private static JPanel detailPanel = null;
	
	public CpatResultScreen(CpatToolBase p_oToolBase, IAction p_oCallbackAction, Map<String, Object> p_oParams) 
	{
		super(p_oToolBase, p_oCallbackAction, p_oToolBase.getToolDetails().getToolTitle(), 600, 800, null, p_oParams);
	}
	

	@Override
	protected Component prepareContentFrame(String p_sPageToLoad, Map<String, Object> params) 
	{
		log.info("Result Screen: Rendering");
		
		JTabbedPane tabPane = null;
		
		JPanel mainPanel = null;
		JPanel actionPanel = null;
		
		Map<String, List<String>> tabWiseFieldList = null;
		Map<String, Object> previousValMap = null;
		Map<String, Object> updatedValMap = null;
		
		String html = null;
		JEditorPane jep = null;
		
		JScrollPane scrollPane = null;
		JScrollPane detailsScrollPane = null;
		
		html = String.valueOf(params.get(CpatToolConstants.ContentParams.RESULT_SCREEN_DATA));
		
		if(params.containsKey(CpatToolConstants.ContentParams.DE_FIELDLIST))
		{
			tabWiseFieldList = (Map<String, List<String>>) params.get(CpatToolConstants.ContentParams.DE_FIELDLIST);			
		}
		
		if(params.containsKey(CpatToolConstants.ContentParams.PRV_VAL_SCEPTRE_MAP))
		{
			previousValMap = (Map<String, Object>) params.get(CpatToolConstants.ContentParams.PRV_VAL_SCEPTRE_MAP);			
		}
		
		if(params.containsKey(CpatToolConstants.ContentParams.SCEPTRE_MAP))
		{
			updatedValMap = (Map<String, Object>) params.get(CpatToolConstants.ContentParams.SCEPTRE_MAP);			
		}
	
		if(params.containsKey(CpatToolConstants.ContentParams.WRITER_INSTANCE))
		{
			oAppWriter = (BaseWebApplicationWriter) params.get(CpatToolConstants.ContentParams.WRITER_INSTANCE);			
		}
		
		
		if(StringUtils.isNullOrEmpty(html))
		{
			html = CpatToolConstants.ResultScreen.NO_RESULTS;
		}
		else
		{
			getDataEntryDetails(tabWiseFieldList, previousValMap, updatedValMap);	
			
			detailsScrollPane = new JScrollPane(detailPanel);
		}
		
		jep = new JEditorPane();
		jep.setContentType("text/html");
		jep.setText(html);
		jep.setEditable(false);
		jep.setCaretPosition(0);
		
		scrollPane = new JScrollPane(jep);
		
	
		//adding button
		button = new JButton(CpatToolConstants.UI.RESULT_SCREEN_BUTTON_OK);
		actionPanel = new JPanel();
		actionPanel.add(button);
		
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String[] buttons = {CpatToolConstants.OPTION_YES, CpatToolConstants.OPTION_NO};
				int rc = JOptionPane.showOptionDialog(null, CpatToolConstants.EXIT_MSG_RESULT_SCREEN, CpatToolConstants.OPTION_DIALOG_TITLE,
						JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[1]);
				
				if(rc == 0)
				{
					oAppWriter.quitDriver();
					
					CpatCasePreviewScreen.disposePreviewScreenView();
					disposeScreen();
					
					m_oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
			}
		});
		
		tabPane = new JTabbedPane();
		tabPane.add("Summary", scrollPane);
		
		if(detailPanel != null)
		{
			tabPane.addTab("Details", detailsScrollPane);
		}
		
		mainPanel = new JPanel(new BorderLayout());
		
		mainPanel.add(tabPane, BorderLayout.CENTER);
		mainPanel.add(actionPanel, BorderLayout.SOUTH);
		
		
		return mainPanel;
	}

	@Override
	protected void addActionListener(Component comp, JFrame frame) 
	{
		log.info("Result Screen: Adding Action Listeners");
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) 
			{
				button.doClick();
			}
		});
	}
	
	public void getDataEntryDetails(Map<String, List<String>> dataEntryKeysMap, Map<String, Object> sceptrePrevValMap, Map<String, Object> sceptreUpdatedValMap)
	{
		JPanel searchPanel = null;
		
		JLabel lblSearch = null;
		
		DefaultTableModel model = null;
		JTable table = null;
		
		
		JScrollPane scrollPane = null;
		
		Vector<String> columnNames = null;
		
		columnNames = new Vector<>();
		columnNames.add("Tab");
		columnNames.add("Field");
		columnNames.add("Sceptre Previous Value");
		columnNames.add("Sceptre Updated Value");
		
		model = new DefaultTableModel(columnNames, 0){
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		if(dataEntryKeysMap != null)
		{
			if(dataEntryKeysMap.size() > 0)
			{
				detailPanel = new JPanel(new BorderLayout());
				
				for(String tab: dataEntryKeysMap.keySet())
				{
					for(String key: dataEntryKeysMap.get(tab))
					{
						model.addRow(new Object[]{tab, key, sceptrePrevValMap.get(key), sceptreUpdatedValMap.get(key)});
					}
					
				}
				
				table = new JTable(model);
				
				rowSorter = new TableRowSorter<TableModel>(table.getModel());
				
				table.setRowSorter(rowSorter);
				
				scrollPane = new JScrollPane(table);
				
				
				// Search Text
				lblSearch = new JLabel("Search");
				tfSearch = new JTextField(30);
				
				tfSearch.getDocument().addDocumentListener(new DocumentListener() {
					
					public void removeUpdate(DocumentEvent e) {
						String text = tfSearch.getText();

						if (text.trim().length() == 0) {
							rowSorter.setRowFilter(null);
						} else {
							rowSorter
									.setRowFilter(RowFilter.regexFilter("(?i)" + text));
						}
					}
					
					public void insertUpdate(DocumentEvent e) {
						String text = tfSearch.getText();

						if (text.trim().length() == 0) {
							rowSorter.setRowFilter(null);
						} else {
							rowSorter
									.setRowFilter(RowFilter.regexFilter("(?i)" + text));
						}
					}
					
					public void changedUpdate(DocumentEvent e) {
						throw new UnsupportedOperationException("Invalid Operation.");
					}
				});
				
				searchPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
				searchPanel.add(lblSearch);
				searchPanel.add(tfSearch);
				
				
				detailPanel.add(scrollPane, BorderLayout.CENTER);
				detailPanel.add(searchPanel, BorderLayout.SOUTH);
			}
		}
	}
}
