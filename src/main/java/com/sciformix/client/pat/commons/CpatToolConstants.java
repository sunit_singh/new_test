package com.sciformix.client.pat.commons;

public class CpatToolConstants {
	public static String INVALID = "invalid";
	public static final String WILDCARD_ASTRIC = "*";
	public static final String DUPLICATE_IRT_RECORD_LIST = "irtreclist";
	public static final String DUPLICATE_SCEPTRE_RECORD_LIST = "sceptrereclist";
	public static final String SCEPTRE_SERVER_SUFFIX = "sceptre.server.";
	public static final String IRT_SERVER_SUFFIX = "irt.server.";
	public static final String CONFIG_FILE_SUFFIX = "config.file.";
	public static final String CPAT_CONFIG	= "cpat.properties";
	public static final String IRT_COMMON_CONFIG = "ConfigForIRTCommon.properties";
	public static final String ERROR_STRING = "Error :";
	public static final String VALUE_TRUE							=				"true";
	public static final String VALUE_FALSE							=				"false";
	public static final String STRING_NULL							=				"null";
	

	public static final String OPTION_DIALOG_TITLE					= 				"Message";
	public static final String OPTION_YES	 						= 				"Yes";
	public static final String OPTION_NO	 						= 				"No";
	public static final String ABORT_MSG_PREVIEW_SCREEN				=				"Are you sure you want to 'Abort' Preview Screen ?";
	public static final String ABORT_MSG_DUPSUMMARY_SCREEN			=				"Are you sure you want to 'Abort' Duplicate Summary Screen ?";
	public static final String EXIT_MSG_RESULT_SCREEN				=				"Are you sure you want to 'Exit' Result Screen ?";
	
	public static final String DROPDOWN_VAL							=				"--Select--";
	
	public static class SearchStatusScreen
	{
		public static final String ABORT_MSG					=				"Are you sure you want to 'Abort' Duplicate Summary Screen ?";
	}
	
	
	public static final String PRODUCTLIST_EXTENSION 				= 				"XLSX"									;
	
	public static class ResultScreen
	{
		public static final String DATAENTRY_DONE_LIST				=				"DataEntryTabList"				;
		public static final String ADD_SDLIST 						=				"extraSDNameList"				;
		public static final String LOOKUP_FAILED_STATUS				=				"codingFailedStatus"			;
		public static final String PENDING_OTHER_IDENTIFICATION_NOS = 				"PENDING_OTHER_IDENTIFICATION_NOS";
		public static final String PENDING_LINKED_CASE_NOS 			= 				"PENDING_LINKED_CASE_NOS"		;
		public static final String NOTES_LIST 						= 				"NOTES_LIST";
		public static final String REACTION_KEYWORD_FOUND 			= 				"REACTION_KEYWORDS_FOUND"		;
		
		public static final String NO_RESULTS						=				"No Result Data Found"			;
		public static final String PENDING_DRUG_ADDITIONALINFO 		= 				"PENDING_DRUG_ADDITIONALINFO"	;
	}
	
	public class FieldsLogicValues{
		public static final String YES = "Yes";
		public static final String NO = "No";
		public static final String UNSPECIFIED = "Unspecified";
		public static final String UNKNOWN = "Unknown";
		public static final String DRUG_DISCONTINUED = "Drug discontinued";
		public static final String NONE = "None";
		public static final String PRIVATE = "PRIVATE";
		public static final String TBODY = "tbody";
		public static final String SELF = "Self";
	}
	
	public static class ContentParams
	{
		public static final String RESULT_SCREEN_DATA 				= 				"RESULT_SCREEN_DATA"			;
		public static final String WRITER_INSTANCE 					= 				"WRITER_INSTANCE"				;
		public static final String CASE_DOC_LIST					= 				"CASE_DOC_LIST"					;
		public static final String CASE_SUMMARY_DATATABLE			= 				"CASE_SUMMARY_DATATABLE"		;
		public static final String CASE_FILE_TYPE 					= 				"CASE_FILE_TYPE" 				;
		public static final String SCEPTRE_MAP						=				"SCEPTRE_MAP"					;
		public static final String DUP_SUMMARYLIST 					= 				"DUPLICATE_SUMMARY_LIST"		;
		public static final String ISDUPLICATE 						= 				"IS_DUPLICATE"					;
		public static final String FUP_NEWVAL_LIST 					= 				"followUpFieldList"				;
		public static final String DUPLICATE_RECORD					= 				"DUPLICATE_RECORD"				;
		public static final String DE_FIELDLIST						=				"DE_FIELDLIST"					;
		public static final String PRV_VAL_SCEPTRE_MAP				=				"PRV_VAL_SCEPTRE_MAP"			;
		public static final String ENABLE_DATA_ENTRY				=				"ENABLE_DAT_ENTRY"			;
	}
	
	public static class FileType
	{
		public static final String E2B								=				"E2B"							;
		public static final String SCEPTRE							=				"SCEPTRE"						;
		public static final String HEALTH_CANADA 					= 				"Health Canada"					;
		public static final String MESA 							=	 			"MESA"							;
	}
	

	public static class UI
	{
		public static final String PREVIEW_BUTTON_CONTINUE			=				"Continue"						;
		public static final String PREVIEW_BUTTON_INITIAL			=				"Initial"						;
		public static final String PREVIEW_BUTTON_FOLLOWUP			=				"Follow-Up"						;
		public static final String PREVIEW_BUTTON_DUPLICATE			=				"Duplicate"						;
		
		public static final String PREVIEW_BUTTON_ABORT				=				"Abort"							;
		public static final String DUPLICATE_SCREEN_BUTTON_CONTINUE	= 				"Ignore & Continue"				;
		public static final String DUPLICATE_SCREEN_BUTTON_ABORT 	= 				"Abort"							;
		public static final String RESULT_SCREEN_BUTTON_OK 			= 				"OK"							;
		public static final String DUPLIACTE_REC_MAP 				= 				"DupilicateMap"					;		
		
		public static final String IRT 								= 				"IRT Duplicates"				;		
		public static final String SCEPTRE 							= 				"Sceptre Duplicates"			;		
		public static final String AER_NO 						= 					"AER NO : "						;		
		public static final String VERSION_TYPE 						= 			"Version Type : "				;		
	}

	public static class DuplicatePreviewScreen
	{
		public static final String PREVIEW_BUTTON_CONTINUE			=				"Continue"						;
		public static final String PREVIEW_BUTTON_ABORT				=				"Abort"							;
		public static final String DUPLICATE_SCREEN_BUTTON_CONTINUE	= 				"Ignore & Continue"				;
		public static final String DUPLICATE_SCREEN_BUTTON_ABORT 	= 				"Abort"							;
		public static final String RESULT_SCREEN_BUTTON_OK 			= 				"OK"							;
		public static final String DUPLIACTE_REC_MAP 				= 				"DupilicateMap"					;		
		
		public static final String AERNO 						= 				"AER NO"					;		
		public static final String IRT 								= 				"IRT Duplicates"				;		
		public static final String SCEPTRE 							= 				"Sceptre Duplicates"			;		
	
	}
	
	public static class ResultSummaryScreen
	{
		public static final String LOCAL_CASE_ID			=				"dataentry.generalinformation.localcaseid"	;
		
		public static final String CLASSIFICATION			=				"Classification"						;
		public static final String RECEIPT_SUMMARY			=				"Receipt Summary"							;
		public static final String CASE_DATA				= 				"Case Data"				;
		
	}
	
	public static class ACTIVITY_TYPE
	{
		public static final String PREVIEW							=				"TOOL-PREVIEW"					;
		public static final String EXISTING_SEARCH 					= 				"TOOL-DUPLICITY-CHECK"			;
		public static final String LOCATE_CASE 						= 				"TOOL-LOCATE-CASE"				;
		public static final String SD_EXTRACTION 					= 				"TOOL-SD-EXTRACTION"			;
		public static final String DATA_ENTRY 						= 				"TOOL-DATA-ENTRY-TAB"			;
		public static final String DATA_ENTRY_SUMMARY 				= 				"TOOL-DATA-ENTRY-SUMMARY"		;
		public static final String DUPLICITY_CHECK 					= 				"TOOL-DUPLICITY-CHECK"			;
		public static final String USER_INPUTS_COLLECTED 			= 				"TOOL-USER-INPUTS-COLLECTED"	;

		public static final String DATA_FETCHING					=				"DATA-FETCHING"					;
		public static final String DATA_FEEDING						=				"DATA-FEEDING"					;
		public static final String SD_DOWNLOAD						=				"TOOL-SD-DOWNLOAD"				;
		public static final String TOOL_PROCESSING					=				"TOOL-PROCESSING"				;
	}

	public static class ACTIVITY_CODE
	{
		public static final String PREVIEW							=				"PREVIEW"						;
		public static final String EXISTING_SEARCH					=				"EXISTING SEARCH"				;
		public static final String LOCATE_CASE 						= 				"LOCATE CASE"					;
		public static final String SD_EXTRACTION 					= 				"SD EXTRACTION"					;
		public static final String DATA_ENTRY 						= 				"DATA ENTRY TAB"				;
		public static final String DATA_ENTRY_SUMMARY 				= 				"DATA ENTRY SUMMARY"			;
		public static final String DUPLICITY_CHECK 					= 				"DUPLICITY CHECK"				;
		public static final String USER_INPUTS_COLLECTED 			= 				"USER INPUTS COLLECTED"			;

		public static final String DATA_FETCHING					=				"DATA FETCHING"					;
		public static final String DATA_FEEDING						=				"DATA FEEDING"					;
		public static final String SD_DOWNLOAD						=				"SD DOWNLOAD"					;
		public static final String TOOL_PROCESSING					=				"TOOL PROCESSING"				;
	}

	public static class BrowserDetails
	{
		public static final String BROWSERVERSION 	= "BrowserVersion";
		public static final String BROWSERNAME 	= "BrowserName";
	
	}

	public static class Browser
	{
		public static final String CHROME							=				"CHROME"						;
	}

	public static class ZONE_PROPS
	{
		public static final String DISCONNETED						=				"disconnected"					;
		public static final String TEST								=				"test"							;
		public static final String PREPRODUCTION					=				"preproduction"					;
		public static final String PRODUCTION						=				"production"					;
	}

	public static final String TOOL_DESC = "$toolDesc";
	public static final String TOOL_DISPLAY_NAME = "$toolDisplayName";
	public static final String TOOL_VERSION = "$toolVersion";
	public static final String TOOL_HEADING = "$toolHeading";

}
