package com.sciformix.client.pat.commons.screens;

import java.awt.Component;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Element;
import javax.swing.text.html.FormSubmitEvent;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.toola.AERVerification;
import com.sciformix.client.pat.toola.ARRNWarningUI;
import com.sciformix.client.pat.toola.ARRNumber;
import com.sciformix.client.pat.toola.ApotexInsertValues;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.file.IFileParser;
import com.sciformix.commons.file.IFileSplitter;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarBA;
import com.sciformix.commons.file.splitters.PdfSplitterAvatarAA;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.FileUtils;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.PdfUtils;

public class CpatVerificationScreen extends CpatBaseHtmlScreen 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String CANADA = "Canada";
	private static final String SD_PATH = "sdpath";
	private static final String SD_FILE_PREFIX = "sdfile_";
	private static final String SD_FILE_COUNT = "sdfilecount";
	private static final String SD_PDF_PATH = "SCP_SOURCEDOCUMENT";
	private static final String EXCEL_PATH = "SCP_EXCELPATH";
	private static final String USER_DIR = "user.dir";
	private static final String HEALTH_CANADA_FILETYPE_VALUE = "HealthCanada-AERReport-PDF";
	private static Logger log=LoggerFactory.getLogger(ARRNWarningUI.class);
	private String dataPrefix = null;
	private String appendedData = null;
	private static ParsedDataTable sDataTable = null;
	private static List<Object> verificationList = null;
	private static List<Object> listARRNMaps= null;
	private static Map<String,String> identificationMap = new HashMap<String,String>();

	public CpatVerificationScreen(CpatToolBase cpatToolBase,IAction p_oCallbackAction, Map<String, Object> mapExtractedParams)
	{
		super(cpatToolBase,p_oCallbackAction,"Sciformix Case Processor Assist Tool (CPAT)", 400, 650, "apotexReconcile.html",mapExtractedParams);
		
			
	}


	public CpatVerificationScreen() {
		
		super();
	}

	protected String constructVerificationStringForHTML(String constructString,Map<String,String> mapExtractedParams) throws Exception
	{
		Map<String,String> extractedPropertyFileParams = loadProperties(mapExtractedParams);
		
		String extractedHeaderCheckList = extractedPropertyFileParams.get("sdHeaderListToCheck");
		
		
		mapExtractedParams.put("firstTimeLogin", "true");
		
		identificationMap =	identifyCurrentSDToImport(mapExtractedParams,extractedHeaderCheckList);
		
		verificationList = performParseAndVerification(identificationMap);
	
	    return createVerificationPage(verificationList,identificationMap,sDataTable,constructString);
	}
	public String createVerificationPage(List<Object> aRRNList, final Map<String, String> proptyMap,final ParsedDataTable oDataTable,String constructString)
	{
		Map<String, Object> ARRNMap=null;
		Object[][] arrayToProcess=null;
		ARRNumber ARRNumber=null;
		boolean rConcillationFlag=false;
		String comments=null;
		
		int caseCount=0;
		String sdPath=null;
		File sdFile=null;
		

		String imgsrc = null;
		
		caseCount=aRRNList.size();
		arrayToProcess=new Object[caseCount][4];
		
		
		for(int i=0; i<aRRNList.size(); i++)
		{
			comments=null;
			rConcillationFlag=false;
			ARRNMap=(Map<String, Object>)aRRNList.get(i);
			ARRNumber=(ARRNumber)ARRNMap.get("ARRNumber");
			if(ARRNumber.getARRNVerifyFlag() && ARRNumber.getMAHVerifyFlag() 
					&& ARRNumber.getIRDVerifyFlag() && ARRNumber.getSeriousReportVerifyFlag() 
					&& ARRNumber.getAgeVerifyFlag() && ARRNumber.getGenderVerifyFlag()
					&& ARRNumber.getProductVerifyFlag() && ARRNumber.getARTVerifyFlag())
			{
				rConcillationFlag=true;
				
			}
			else
			{
				comments=setComments(comments, "ARRNumber", ARRNumber.getARRNVerifyFlag());
				comments=setComments(comments, "Market Authorization Holder AER Number", ARRNumber.getMAHVerifyFlag());
				comments=setComments(comments, "Initial Received Date", ARRNumber.getIRDVerifyFlag());
				comments=setComments(comments, "Serious Report", ARRNumber.getSeriousReportVerifyFlag());
				comments=setComments(comments, "Age", ARRNumber.getAgeVerifyFlag());
				comments=setComments(comments, "Gender", ARRNumber.getGenderVerifyFlag());
				comments=setComments(comments, "Product", ARRNumber.getProductVerifyFlag());
				comments=setComments(comments, "Adverse Reaction Terms", ARRNumber.getARTVerifyFlag());
			}
			
			if(rConcillationFlag)
			{
				comments=StringConstants.EMPTY;
			}
			else
			{
				comments=comments+" mismatched.";
			}
			if(rConcillationFlag)
			{
				imgsrc = this.getClass().getClassLoader().getResource("success_icon.png").toString();
			}
			else
			{

				imgsrc = this.getClass().getClassLoader().getResource("fail_icon.png").toString();
			}

			arrayToProcess[i][0]=i+1;
			arrayToProcess[i][1]=ARRNumber.getARRNumber();
			arrayToProcess[i][2]= imgsrc;
			arrayToProcess[i][3]=comments;
			
			dataPrefix = "<tr><td style='text-align: center;'>"+arrayToProcess[i][0]+"</td><td style='text-align: center;'>"+arrayToProcess[i][1]+"</td><td style='text-align: center;'><img src="+arrayToProcess[i][2]+"/></td><td style='text-align: center;'>"+arrayToProcess[i][3]+"</td></tr>";
			if(i < 1)
			{
			appendedData = dataPrefix;
			}
			else
			{
			appendedData = appendedData.concat(dataPrefix);
			}

		}
		
			sdPath=proptyMap.get(SD_PDF_PATH);
			sdFile=new File(sdPath);
			String caseCountStr = String.valueOf(caseCount);

	    	
	    	
	    	String fileNameBeingVerified = "FILENAME";
	    	String caseCountInSD = "CASECOUNT";
	    	String tableValues = "ROHINIMARKERFORRECONCILERESULTS";
	    	String headerPrefix = "<tr><th style='width:100%;word-wrap:word-break;'>Case No.   </th><th style='width:100%;word-wrap:word-break;'>ARR Number  </th><th style=style='width:100%;word-wrap:word-break;'>Reconciliation Status</th><th style='width:100%;word-wrap:word-break;'>Comment        </th></tr>";
		    String fileString = null;
		    String caseString = null;
		    String tableString = null;
	    	
	    	if(constructString.contains(fileNameBeingVerified))
	    	{
	    		fileString = constructString.replaceAll(fileNameBeingVerified, sdFile.getName()).trim();
	    	}
	    	if(fileString.contains(caseCountInSD))
	    	{
	    		caseString = fileString.replaceAll(caseCountInSD, caseCountStr);
	    	}
	    	if(caseString.contains(tableValues))
	    	{
	    		
	    		tableString = caseString.replaceAll(tableValues, headerPrefix+appendedData);
	    	}
	    return tableString;		
	}
	private static String setComments(String comments, String field, boolean flag)
	{
		if(!flag)
		{
			if(comments==null)
			{
				comments=field;
			}
			else
			{
			comments=comments+StringConstants.COMMA+field;
			}
		}
		return comments;
	}
	public  List<Object> performParseAndVerification(Map<String,String> inputMapParameters)
	{

		String fileType=null;
		String sdPath=null;
		String excelPath=null;
		String sFileContent=null;
		Map<String, Object> ARRNMap=null;
		DataTable dataTableReconciliationContents = null;
		

		try {
			sdPath=inputMapParameters.get(SD_PATH).toString();
			sFileContent = PdfUtils.getFileText(sdPath);

			if(sFileContent.contains(CANADA))
			{
				fileType=HEALTH_CANADA_FILETYPE_VALUE;
				inputMapParameters.put("fileType", fileType);

				IFileParser oDocParser=new PdfDocParserAvatarBA(sdPath);

				try {
					sDataTable=oDocParser.parse();
				} catch (SciServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				excelPath=inputMapParameters.get(EXCEL_PATH).toString();


				int nIdentifiedCaseCount = 0;

				nIdentifiedCaseCount = Integer.parseInt(inputMapParameters.get(SD_FILE_COUNT));
				String firstTimeLogin=inputMapParameters.get("firstTimeLogin");
				if(firstTimeLogin.equals("true"))
				{
					ARRNumber ARRNumber=null;


					listARRNMaps = new ArrayList<Object>(nIdentifiedCaseCount);

					dataTableReconciliationContents = AERVerification.getDataFromExcel(excelPath);

					for (int nCaseRecordCounter = 1; nCaseRecordCounter <= nIdentifiedCaseCount; nCaseRecordCounter++)
					{
						IFileParser oLocalDocParser = new PdfDocParserAvatarBA(inputMapParameters.get((SD_FILE_PREFIX + nCaseRecordCounter)).toString());
						ParsedDataTable oLocalDataTableExtractedContents = null;

						try {
							oLocalDataTableExtractedContents = oLocalDocParser.parse();
						} catch (SciServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						//ARRNumber set all values
						ARRNumber=new ARRNumber();
						ARRNumber = AERVerification.convertExtractedData(oLocalDataTableExtractedContents);

						ARRNMap = AERVerification.compareInputCaseWithReconciliationTable(dataTableReconciliationContents, ARRNumber, nCaseRecordCounter);

						listARRNMaps.add(ARRNMap);
					}


				}
			}
		}
		catch (SciException e) {

			e.printStackTrace();
		}

		return listARRNMaps;
	}

	protected Map<String, String> loadProperties(Map<String,String> mapExtractedParams)
	{
		ApotexInsertValues safetyInsert=new ApotexInsertValues();
		return safetyInsert.load(mapExtractedParams);
	}
	
	protected Map<String,String> identifyCurrentSDToImport(Map<String,String> inputMapParameters,String extractedHeaderCheckList) throws Exception
	{
		try {

			String outputFolder=null;
			File folder=null;
			File[] splittedFileList=null;
			IFileSplitter fileSplitter=null;
			String[] pdfHeaderList=null;
			String userPath=null;
			String sdPath=null;

			sdPath=inputMapParameters.get(SD_PDF_PATH);
			pdfHeaderList=extractedHeaderCheckList.split(",");
			userPath=System.getProperty(USER_DIR);

			fileSplitter=new PdfSplitterAvatarAA();
			outputFolder=fileSplitter.splitFile(sdPath, pdfHeaderList, userPath);
			folder=new File(outputFolder);

			splittedFileList=folder.listFiles();
			if(splittedFileList.length>0)
			{
				inputMapParameters.put(SD_PATH, splittedFileList[0].getAbsolutePath());

				for (int nSplittedFileCounter = 0; nSplittedFileCounter < splittedFileList.length; nSplittedFileCounter++)
				{
					inputMapParameters.put((SD_FILE_PREFIX + (nSplittedFileCounter+1)), splittedFileList[nSplittedFileCounter].getAbsolutePath());
				}
				inputMapParameters.put(SD_FILE_COUNT, ""+splittedFileList.length);
			}
			

		} catch (Exception e) {
			log.error("Failed to load property file");
			e.printStackTrace();
			throw new SciException("Failed to load property file");
		}
		return inputMapParameters;
	}
	protected void locateCaseForImport()
	{	

	}

	protected void addActionListener(JEditorPane jEditorPane, JFrame frame) {
			jEditorPane.addHyperlinkListener(new HyperlinkListener() {
				@Override
				public void hyperlinkUpdate(HyperlinkEvent event)
				{
					if (event instanceof FormSubmitEvent && ((FormSubmitEvent) event).getData().contains("Continue")) 
					{
						disposeScreen();
						
						m_oCallbackAction.performAction(CPAT_ACTION.VERIFICATION_SUCCESSFUL, identificationMap,sDataTable);
					}
					else
					{
						System.exit(0);
					}
				}

			});
		
	}


	@Override
	protected JEditorPane prepareContentFrame(String p_sPageToLoad, Map<String, Object> params) {

		String imgsrc=null;
		Element body=null;
		HTMLEditorKit hek =null;
		String content=null;
		HTMLDocument htdoc=null;
		InputStream initialStream=null;
		JEditorPane jep = null;
		Map<String, String> extractedParams = null;
		
		try 
		{
			log.debug("Creation of login page  started");
			imgsrc = this.getClass().getClassLoader().getResource("sciformix_logo.png").toString();
			jep = new JEditorPane();
			jep.setContentType("text/html");
			jep.setEditable(false);
			
			initialStream = ClassLoader.getSystemResourceAsStream(p_sPageToLoad);
			content = FileUtils.getStringFromInputStream(initialStream);
			
			extractedParams = new HashMap<>();
			
			for(String key: params.keySet())
			{
				extractedParams.put(key, String.valueOf(params.get(key)));
			}
			
			//AR//TODO:Needs change
			CpatVerificationScreen verificationScreen = new CpatVerificationScreen();
			content = verificationScreen.constructVerificationStringForHTML(content, extractedParams);
		
			hek = new HTMLEditorKit();
			jep.setEditorKit(hek);
			jep.setText(content);
			htdoc=(HTMLDocument)jep.getDocument();
			
			body = CpatToolHome.findBodyTabByHTMLDoc(htdoc);
//			htdoc.insertAfterStart(body, "<div id='wrapper' align='center'><br><img src='"+imgsrc+"' alt='Sciformix Logo' width=200 height=50>");
			
			((HTMLEditorKit) jep.getEditorKit()).setAutoFormSubmission(false);

		} catch (Exception e) 
		{
			log.error("Creation of login page failed due to <"+e.getMessage()+">");
			e.printStackTrace();
		} finally {
		}
		return jep;
	
		
	}


	@Override
	protected void addActionListener(Component comp, JFrame frame) 
	{
		// Do Nothing
		
	}
}
