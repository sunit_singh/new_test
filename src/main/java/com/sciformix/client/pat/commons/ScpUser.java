package com.sciformix.client.pat.commons;

import java.util.ArrayList;
import java.util.List;

public class ScpUser
{
	private String userId;
	private String password;
	
	private String zone;
	
	private List<ScpProject> userProjectsList = new ArrayList<>();
	
	public ScpUser(String userId, String password, String zone)
	{
		this.userId = userId;
		this.password = password;
		this.zone = zone;
	}

	public String getUserId() 
	{
		return userId;
	}

	public void setUserId(String userId) 
	{
		this.userId = userId;
	}

	public List<ScpProject> getUserProjectsList() 
	{
		return userProjectsList;
	}

	public void setUserProjectsList(List<ScpProject> userProjectsList) 
	{
		this.userProjectsList = userProjectsList;
	}
	
	public void addUserProjectsList(ScpProject oScpProject)
	{
		this.userProjectsList.add(oScpProject);
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
