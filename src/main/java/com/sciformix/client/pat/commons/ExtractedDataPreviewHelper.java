package com.sciformix.client.pat.commons;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarBA;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class ExtractedDataPreviewHelper 
{
	
	private static final String DOT_SPLITTER = "\\.";
	private static final String HEALTH_CANADA_FILETYPE_VALUE = "HealthCanada-AERReport-PDF";
	private static final String SCEPTRE_FILE_TYPE = "E2B";

	public static String settingPDFData(ParsedDataTable oDataTable, Map<String,String> propMap, String fileType, String filePath)
	{
		String sKey = null;
		String sValue = null;
		String sTrimValue = null;
		Integer validatedFieldCount = 0;
		Integer failedFieldCount = 0;
		Integer validationNAFiledCount = 0;
		Integer warningFieldCount = 0;
		Integer unvalidatedFieldCount = 0;
		ParsedDataValue pdValue = null;
		String vStatus = null;
		FileWriter fileWriter = null;
		StringBuffer text = new StringBuffer();
		int nTotalFields = 0, nMissingFieldValue=0;
		FileParsingConfiguration oFilePassConfig = null;
		
		try {
			nTotalFields = oDataTable.colCount();
			for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
			{
				sValue = oDataTable.getColumnValues(nIndex).get(oDataTable.getColumnValues(nIndex).size()-1).displayValue().toString();
				if(sValue.trim().equals(""))
					nMissingFieldValue++;
				pdValue = oDataTable.getColumnValues(nIndex).get(0);
				vStatus = (String)pdValue.validationState().displayName();
				if (vStatus.equalsIgnoreCase("Validation Failed")) 
				{
					failedFieldCount++;
				} 
				else if (vStatus.equalsIgnoreCase("Validated")) 
				{
					validatedFieldCount++;
				} 
				else if(vStatus.equalsIgnoreCase("Validation Not Applicable"))
				{
					validationNAFiledCount++;
				}
				else if(vStatus.equalsIgnoreCase("Unvalidated"))
				{
					unvalidatedFieldCount++;
				}
				else if(vStatus.equalsIgnoreCase("Validation Warnings"))
				{
					warningFieldCount++;
				}
				
				
			}
			
			if(fileType.equals("NL"))
			{
				
				text.append(getHtmlPrefix(nTotalFields,nMissingFieldValue));
	
				for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
				{
					 sKey = oDataTable.getMnemonic(nIndex);
					 sTrimValue = sKey.replaceAll(" ", "").trim();
					 sValue= oDataTable.getColumnValues(nIndex).get(oDataTable.getColumnValues(nIndex).size()-1).displayValue().toString().trim();
					 if(sValue.equals(":"))
						 sValue="";
					 if(sTrimValue.equals("ICHICSRMessageHeader") || sTrimValue.equals("Identificationofthecasesafetyreport")||sTrimValue.equals("linkedreport")||sTrimValue.equals("Primarysource(s)ofinformation")||sTrimValue.equals("Primarysource(s)ofinformation")||sTrimValue.equals("Sender")
							 ||sTrimValue.equals("Receiver") ||sTrimValue.equals("Patientcharacteristics") ||sTrimValue.equals("Drug(s)Information") ||sTrimValue.equals("activesubstance") ||sTrimValue.equals("Reaction(s)/Event(s)")
							 ||sTrimValue.equals("Narrativecasesummaryandfurtherinformation:"))
					 {
						 setHeaderStyle(text, sKey);
					 }else
					 { 
						 if(sKey.contains(StringConstants.PERIOD))
						 {
							 String[] sKeyArray=sKey.split(DOT_SPLITTER);
							 sKey=sKeyArray[sKeyArray.length-1];
						 }
						 if(sKey.contains("Casenarrativeincludingclinicalcourse,"))
						 {
							 
							 sKey=sKey.replace(",", ", ");
						
						 }
						 	 setKeyAndValue(text, sKey, sValue);
							 setColorCodeForAvailability(sValue, text);
					 }
					
				}
				
				text.append("<tr><td colspan='3'>&nbsp;</td></tr></table></html>");
			}//end of NLD type if loop
			else if(fileType.equals("Korea") || fileType.equals("Taiwan"))
			{
				
					
				text.append(getHtmlPrefix(nTotalFields, nMissingFieldValue));
	
				for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
				{
					 sKey = oDataTable.getMnemonic(nIndex);
					 sTrimValue=oDataTable.getMnemonic(nIndex).replaceAll(" ", "");
					 sTrimValue=sTrimValue.replaceAll("[^\\x20-\\x7F]", "");
					 String temp= "" + oDataTable.getColumnValues(nIndex).get(0).displayValue();
					 sValue=temp.trim();
					 if(sValue.equals(":"))
						 sValue="";
					 sTrimValue = sTrimValue.toLowerCase().trim();
					
					 if(sTrimValue.equals("productdetails") ||sTrimValue.equals("description")|| sTrimValue.equals("pcmdescription")||sTrimValue.equals("submissiondetails")||sTrimValue.equals("patientdetails")||sTrimValue.equals("hcpdetails"))
					 {
						 setHeaderStyle(text, sKey);
					 }else
					 { 
						 if(sKey.contains("."))
						 {
							 sKey = sKey.split(DOT_SPLITTER)[1];
						 }
						 setKeyAndValue(text, sKey, sValue);
						 setColorCodeForAvailability(sValue, text);
					 }
					
				}
				
				text.append("<tr><td colspan='3'>&nbsp;</td></tr></table></html>");
			}
			
			else if(fileType.equals("US"))
			{
	
				text.append(getHtmlPrefix(nTotalFields, nMissingFieldValue));
	
				for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
				{
					 sKey = oDataTable.getMnemonic(nIndex);
					 sTrimValue = oDataTable.getMnemonic(nIndex).replaceAll(" ", "");
					 String temp= "" + oDataTable.getColumnValues(nIndex).get(0).displayValue();
					 sValue=temp.trim();
					 if(sValue.equals(":"))
						 sValue="";
					 sTrimValue = sTrimValue.toLowerCase().trim();
					
					 if(sTrimValue.equals("productdetails") || sTrimValue.equals("aerdescription")||sTrimValue.equals("pcmdescription")||sTrimValue.equals("submissiondetails")||sTrimValue.equals("patientdetails")||sTrimValue.equals("hcpdetails"))
					 {
						 setHeaderStyle(text, sKey);
					 }else
					 { 
						if(sKey.contains("."))
						 {
							 sKey = sKey.split(DOT_SPLITTER)[1];
						 }
				  		 setKeyAndValue(text, sKey, sValue);
						 setColorCodeForAvailability(sValue, text);
					 }
					
				}
			}
			
				else if(fileType.equals(HEALTH_CANADA_FILETYPE_VALUE))
				{
	
					text.append(getHtmlPrefix(nTotalFields, nMissingFieldValue));
	
					for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
					{
						 sKey = oDataTable.getMnemonic(nIndex);
						 sTrimValue = sKey.replaceAll(" ", "");
						 if(sTrimValue.contains(StringConstants.PERIOD) && !sTrimValue.contains("product") && (sKey.indexOf(".")==sKey.lastIndexOf(".")))
						 sTrimValue=sTrimValue.split(DOT_SPLITTER)[1];
						 String temp= "" + oDataTable.getColumnValues(nIndex).get(0).displayValue();
						 sValue=temp.trim();
						 sTrimValue=sTrimValue.replace("\r", "");
						 if(sValue.equals(":"))
							 sValue="";
						 sTrimValue = sTrimValue.toLowerCase().trim();
						
						 if(((sTrimValue.length()>=7 && sTrimValue.length()<=9) && (sTrimValue.startsWith("report") || sTrimValue.startsWith("product")))
								 || sTrimValue.equals("reportinformation") || sTrimValue.equals("patientinformation")||sTrimValue.equals("link/duplicatereportinformation")||sTrimValue.equals("productinformation")||sTrimValue.equals("adversereactionterminformation"))
						 {
							 if(sKey.contains("."))
							 {
								sKey = sKey.split(DOT_SPLITTER)[1];
							 }
							 
							 setHeaderStyle(text, sKey);
							 
						 }else
						 { 
							if(sKey.contains("."))
							 {
								if(sKey.indexOf(".")!=sKey.lastIndexOf(".") && (sKey.contains("product") || sKey.contains("event") || sKey.contains("Link_Duplicate_Record")))
									sKey = sKey.split(DOT_SPLITTER)[2];
								else
									sKey = sKey.split(DOT_SPLITTER)[1];
							 }
							
							 setKeyAndValue(text, sKey, sValue);
							 setColorCodeForAvailability(sValue, text);
						 }
						
					}
				
				text.append("<tr><td colspan='3'>&nbsp;</td></tr></table></html>");
			}
			else if(fileType.equals(SCEPTRE_FILE_TYPE) || fileType.equals("SCEPTRE"))
				//else if(fileType.equals(SCEPTRE_FILE_TYPE) || fileType.equals(HEALTH_CANADA_FILETYPE_VALUE))
				{
					String xmlMnemonic = null;
					String m_sLabel = null;
					ParsedDataValue keyCountValue = null;
					int repeatCountForGroup=0;
					String dataTableKey = null;
					ValidationState vState= null;
					String displayValue = null;
					String collationGroup = null;
					if(fileType.equalsIgnoreCase(SCEPTRE_FILE_TYPE)|| fileType.equals("SCEPTRE"))
					{
						oFilePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AA_KEY);
					}
					else
					{
						oFilePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_BA_KEY);
						PdfDocParserAvatarBA oParser =new PdfDocParserAvatarBA(filePath);
						oDataTable = oParser.MapAutomatedKeysToMnemonics(oDataTable);
					}
					text.append(getHtmlPrefixForE2B(nTotalFields, nMissingFieldValue, validatedFieldCount, failedFieldCount, warningFieldCount, validationNAFiledCount, unvalidatedFieldCount));
					
					//creating critical table
					setCriticalTable(oFilePassConfig, text, oDataTable);
					
					for(String group: oFilePassConfig.getFieldGroupList())
					{
						//displaying groups
						text.append("<tr><td style:'border-bottom:5px'><b><font size='4'>"+group+"</font></b></td></tr>");
						text.append("<tr><td><table bgcolor='#DEDBDA' style='padding-left: 10px; '>");
						for(String subGroup: oFilePassConfig.getsubGroupListByGroupName(group))
						{	text.append("<tr><td><b><u><font size='3'>"+subGroup+"</font></u></b></td></tr>");
							text.append("<tr><td><table border='1' width='560'>");
							text.append("<th bgcolor='#A19F9B' style='word-wrap:word-break;'>Fields</th><th bgcolor='#A19F9B' style='word-wrap:word-break;'>Value</th><th bgcolor='#A19F9B' style='white-space: nowrap'>V</th>");
							
							for(FileParsingField field : oFilePassConfig.getFieldListBySubGroup(subGroup))
							{
								xmlMnemonic = field.getMnemonic();
								m_sLabel = oFilePassConfig.getField(xmlMnemonic).getFieldLabel();
								subGroup = oFilePassConfig.getField(xmlMnemonic).getM_sFieldSubGroup();
								collationGroup = oFilePassConfig.getField(xmlMnemonic).getCollationgroup();
									if(oDataTable.isColumnPresent(xmlMnemonic+"_Count"))
									{
										keyCountValue = oDataTable.getColumnValues(xmlMnemonic+"_Count").get(0);
										repeatCountForGroup = Integer.valueOf(keyCountValue.displayValue().toString());
										if(collationGroup!=null && !collationGroup.equalsIgnoreCase(""))
										{
											if(oFilePassConfig.getFieldListByCollationGroup(collationGroup).get(0).getMnemonic().equalsIgnoreCase(xmlMnemonic))
											{
											
												for(int index=1; index<= repeatCountForGroup; index++)
												{
													for(FileParsingField mField:oFilePassConfig.getFieldListByCollationGroup(collationGroup))
													{
													xmlMnemonic =  mField.getMnemonic();
													m_sLabel = mField.getFieldLabel();
													dataTableKey = xmlMnemonic+"_"+index;
														m_sLabel = m_sLabel+" "+index;
													if(oDataTable.isColumnPresent(dataTableKey))
													{
														vState = oDataTable.getColumnValues(dataTableKey).get(0).validationState();
														displayValue = oDataTable.getColumnValues(dataTableKey).get(0).displayValue().toString();
													}
													else
													{
														//when it does not found the key
														//it should never come to this block
														displayValue = "";
														vState = ValidationState.UNVALIDATED;
													}
													text.append("<tr><td>"+m_sLabel+"</td><td >"+displayValue+"</td>");
													setColorCodeForValidation(vState.displayName(), text);
													
												}
											}
											}
										}
										else
										{
										for(int index=1; index<= repeatCountForGroup; index++)
										{
											 
											m_sLabel = oFilePassConfig.getField(xmlMnemonic).getFieldLabel();
											dataTableKey = xmlMnemonic+"_"+index;
												m_sLabel = m_sLabel+" "+index;
											if(oDataTable.isColumnPresent(dataTableKey))
											{
												vState = oDataTable.getColumnValues(dataTableKey).get(0).validationState();
												displayValue = oDataTable.getColumnValues(dataTableKey).get(0).displayValue().toString();
											}
											else
											{
												//when it does not found the key
												//it should never come to this block
												displayValue = "";
												vState = ValidationState.UNVALIDATED;
											}
											text.append("<tr><td>"+m_sLabel+"</td><td >"+displayValue+"</td>");
											setColorCodeForValidation(vState.displayName(), text);
											
										}
										}
										
									}
									else if(oDataTable.isColumnPresent(xmlMnemonic))
									{
										dataTableKey = xmlMnemonic;
											
										m_sLabel = oFilePassConfig.getField(xmlMnemonic).getFieldLabel();
										displayValue = oDataTable.getColumnValues(xmlMnemonic).get(0).displayValue().toString();
										vState = oDataTable.getColumnValues(dataTableKey).get(0).validationState();
										text.append("<tr><td>"+m_sLabel+"</td><td >"+displayValue+"</td>");
										setColorCodeForValidation(vState.displayName(), text);
										
									}
									
							}//end of loop for field
							//key value table close
							text.append("</table></td></tr>");
						}
						//subgroup table close
						text.append("</table></td></tr>");
						
					}
			
				//group table closed
				text.append("<tr><td colspan='3'>&nbsp;</td></tr></table></html>");
			}
		
		} catch (Exception e) 
		{
			e.getStackTrace();
			//log.error("error in creating preview data < "+e.getMessage()+" >");
		}
		
		try {
			fileWriter = new FileWriter(System.getProperty("user.dir")+"\\dataTable.html");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter pwriter = new PrintWriter(fileWriter);
		pwriter.write(text.toString());
		if(fileWriter!=null )
		{
			try {
				fileWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(pwriter!=null )
		{
			pwriter.close();
		}
		return text.toString();
		}
	
	public static void setCriticalTable(FileParsingConfiguration oFilePassConfig, StringBuffer text, ParsedDataTable oDataTable)
	{
		String xmlMnemonic = null;
		String m_sLabel = null;
		String collationGroup = null;
		ParsedDataValue keyCountValue = null;
		String dataTableKey =  null;
		String displayValue  =null;
		ValidationState vState = null;
		int repeatCountForGroup=0;
		boolean isCritical = false;
		
		text.append("<tr><td><table bgcolor='#DEDBDA' style='padding-left: 10px; '>");
		text.append("<tr><td><b><u><font size='3'>Critical Fields</font></u></b></td></tr>");
		text.append("<tr><td><table border='1' width='560'>");
		text.append("<th bgcolor='#A19F9B' style='word-wrap:word-break;'>Fields</th><th bgcolor='#A19F9B' style='word-wrap:word-break;'>Value</th><th bgcolor='#A19F9B' style='white-space: nowrap'>V</th>");
		
		for(String group: oFilePassConfig.getFieldGroupList())
		{
			//displaying groups
			for(String subGroup: oFilePassConfig.getsubGroupListByGroupName(group))
			{	
				
				for(FileParsingField field : oFilePassConfig.getFieldListBySubGroup(subGroup))
				{
					
					xmlMnemonic = field.getMnemonic();
					isCritical = false;
					isCritical = field.isCritical();
					m_sLabel = oFilePassConfig.getField(xmlMnemonic).getFieldLabel();
					subGroup = oFilePassConfig.getField(xmlMnemonic).getM_sFieldSubGroup();
					collationGroup = oFilePassConfig.getField(xmlMnemonic).getCollationgroup();
						if(oDataTable.isColumnPresent(xmlMnemonic+"_Count"))
						{
							keyCountValue = oDataTable.getColumnValues(xmlMnemonic+"_Count").get(0);
							repeatCountForGroup = Integer.valueOf(keyCountValue.displayValue().toString());
							if(collationGroup!=null && !collationGroup.equalsIgnoreCase(""))
							{
								if(oFilePassConfig.getFieldListByCollationGroup(collationGroup).get(0).getMnemonic().equalsIgnoreCase(xmlMnemonic))
								{
								
									for(int index=1; index<= repeatCountForGroup; index++)
									{
										for(FileParsingField mField:oFilePassConfig.getFieldListByCollationGroup(collationGroup))
										{
											isCritical = oFilePassConfig.getField(xmlMnemonic).isCritical();	
										xmlMnemonic =  mField.getMnemonic();
										m_sLabel = mField.getFieldLabel();
										dataTableKey = xmlMnemonic+"_"+index;
											m_sLabel = m_sLabel+" "+index;
										if(oDataTable.isColumnPresent(dataTableKey))
										{
											vState = oDataTable.getColumnValues(dataTableKey).get(0).validationState();
											displayValue = oDataTable.getColumnValues(dataTableKey).get(0).displayValue().toString();
										}
										else
										{
											//when it does not found the key
											//it should never come to this block
											displayValue = "";
											vState = ValidationState.UNVALIDATED;
										}
										if(isCritical)
										{
										text.append("<tr><td>"+m_sLabel+"</td><td >"+displayValue+"</td>");
										setColorCodeForValidation(vState.displayName(), text);
										}
									}
								}
								}
							}
							else
							{
							for(int index=1; index<= repeatCountForGroup; index++)
							{
								isCritical = oFilePassConfig.getField(xmlMnemonic).isCritical();
								m_sLabel = oFilePassConfig.getField(xmlMnemonic).getFieldLabel();
								dataTableKey = xmlMnemonic+"_"+index;
									m_sLabel = m_sLabel+" "+index;
								if(oDataTable.isColumnPresent(dataTableKey))
								{
									vState = oDataTable.getColumnValues(dataTableKey).get(0).validationState();
									displayValue = oDataTable.getColumnValues(dataTableKey).get(0).displayValue().toString();
								}
								else
								{
									//when it does not found the key
									//it should never come to this block
									displayValue = "";
									vState = ValidationState.UNVALIDATED;
								}
								if(isCritical)
								{
								text.append("<tr><td>"+m_sLabel+"</td><td >"+displayValue+"</td>");
								setColorCodeForValidation(vState.displayName(), text);
								}
							}
							}
							
						}
						else if(oDataTable.isColumnPresent(xmlMnemonic))
						{
							dataTableKey = xmlMnemonic;
								
							m_sLabel = oFilePassConfig.getField(xmlMnemonic).getFieldLabel();
							displayValue = oDataTable.getColumnValues(xmlMnemonic).get(0).displayValue().toString();
							vState = oDataTable.getColumnValues(dataTableKey).get(0).validationState();
							if(isCritical)
							{
							text.append("<tr><td>"+m_sLabel+"</td><td >"+displayValue+"</td>");
							setColorCodeForValidation(vState.displayName(), text);
							}
						}
						
				}//end of loop for field
			}
		}
		//key value table close
		text.append("</table></td></tr>");
		//subgroup table close
		text.append("</table></td></tr>");
	}
	public static String getHtmlPrefix(int nTotalFields,int nMissingFieldValue)
	{
		String htmlPrefix=null;
		
		htmlPrefix="<html><head></head>"
				+ "<table style='border:0px;cellpadding:0;border-spacing:0;font-family:Arial;font-size:15;'>"
				+ "<tr><td colspan='2'>&nbsp;</td></tr> <tr style='background-color:#A19F9B'><td><b><u>Parsing Summary</u></b></td><td>&nbsp;</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Parsing Status</td><td>100% Success ("+nTotalFields+" out of "+nTotalFields+" fields parsed with NO errors)</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Missing Data</td><td>"+nMissingFieldValue+" out of "+ nTotalFields+ " are missing values</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td colspan='2'>&nbsp;</td></tr>"
				+"<tr><td colspan='2'>&nbsp;</td></tr></table> "
				+"<table style='border:0px;padding-right: 10px;cellpadding:0;border-spacing:0;font-family:Arial;font-size:12;background-color:#F5F6F7;width:530px;'>"
				;
				
		
		return htmlPrefix;
	}
	public static String getHtmlPrefixForE2B(int nTotalFields,int nMissingFieldValue, int validatedFieldCount, int failedFieldCount, int warningFieldCounts, int validationNAFiledCount,int unValidatedFieldCount)
	{
		String htmlPrefix=null;
		
		htmlPrefix="<html><head></head>"
				+ "<table style='border:0px;cellpadding:0;border-spacing:0;font-family:Arial;font-size:15;'>"
				+ "<tr><td colspan='2'>&nbsp;</td></tr> <tr style='background-color:#A19F9B'><td><b><u>Parsing Summary</u></b></td><td>&nbsp;</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Parsing Status</td><td>100% Success ("+nTotalFields+" out of "+nTotalFields+" fields parsed with NO errors)</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Missing Data</td><td>"+nMissingFieldValue+" out of "+ nTotalFields+ " are missing values</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Validated Data</td><td>"+validatedFieldCount+" out of "+ nTotalFields+ " are validated</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Validation not required Data</td><td>"+validationNAFiledCount+" out of "+ nTotalFields+ " not applicable for Validation</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Failed Data</td><td>"+failedFieldCount+" out of "+ nTotalFields+ " Failed</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td><b>Warning Data</td><td>"+warningFieldCounts+" out of "+ nTotalFields+ " have warnings</td></tr>"
				+"<tr style='background-color:#DEDBDA'><td colspan='2'>&nbsp;</td></tr>"
				+"<tr><td colspan='2'>&nbsp;</td></tr></table> "
				+"<table style='border:0px;padding-right: 10px;cellpadding:0;border-spacing:0;font-family:Arial;font-size:12;background-color:#F5F6F7;width:530px;'>"
				;
				
		
		return htmlPrefix;
	}
	private static void setColorCodeForAvailability(String sValue, StringBuffer text)
	{
		String cellValue= null;
		if (!sValue.equalsIgnoreCase("")) 
		{
			cellValue = "<td width='20' style='background-color:green;color:white;text-align:center'>P</td></tr>";
		} 
		else 
		{
			cellValue = "<td width='20' style='background-color:yellow;color:black;text-align:center'>M</td></tr>";
			
		}
		text.append(cellValue);
		
	}
	private static void setColorCodeForValidation(String sValue, StringBuffer text)
	{
		String cellValue= null;
		if (sValue.equalsIgnoreCase("Validation Failed")) 
		{
			cellValue = "<td width='20' style='background-color:red;color:white;text-align:center'>F</td></tr>";
		} 
		else if (sValue.equalsIgnoreCase("Validated")) 
		{
			cellValue = "<td width='20' style='background-color:green;color:white;text-align:center'>V</td></tr>";
		} 
		else if(sValue.equalsIgnoreCase("Validation Not Applicable"))
		{
			cellValue = "<td width='20' style='color:black;text-align:center'>NA</td></tr>";
			
		}
		else if(sValue.equalsIgnoreCase("Unvalidated"))
		{
			cellValue = "<td width='20' style='background-color:yellow;color:black;text-align:center'>M</td></tr>";
			
		}
		else if(sValue.equalsIgnoreCase("Validation Warnings"))
		{
			cellValue = "<td width='20' style='background-color:orange;color:black;text-align:center'>W</td></tr>";
			
		}
		else
		{
			cellValue = "<td width='20' style='color:black;text-align:center'>Other</td></tr>";
		}
		
		text.append(cellValue);
		
	}
	private static void setHeaderStyle(StringBuffer text, String sKey)
	{
		text.append("<tr><td colspan='3'>&nbsp;</td></tr>");
		 text.append("<tr style='background-color:#EAF2F8'><td colspan='3'><b><u>"+sKey+"</u></b></td></tr>");
	}
	private static void setKeyAndValue(StringBuffer text, String sKey, String sValue)
	{
		text.append("<tr><td><b>"+sKey+"</td><td>"+sValue+"</td>");
	}
	
}
