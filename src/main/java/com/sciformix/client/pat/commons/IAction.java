package com.sciformix.client.pat.commons;

import com.sciformix.commons.SciEnums.CPAT_ACTION;

public interface IAction 
{
	public void performAction(CPAT_ACTION p_eAction, Object... oParameters);
}
