package com.sciformix.client.pat.commons;

import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;

public class ParsedDataWithConfiguration {
	private ParsedDataValue parsedDataValue = null;
	private FileParsingField field = null;
	
	public ParsedDataWithConfiguration(ParsedDataValue m_oParsedDataValue, FileParsingField m_oField)
	{
		this.parsedDataValue = m_oParsedDataValue;
		this.field = m_oField;
	}
	public ParsedDataValue getParsedDataValue() {
		return parsedDataValue;
	}
	public void setParsedDataValue(ParsedDataValue parsedDataValue) {
		this.parsedDataValue = parsedDataValue;
	}
	public FileParsingField getField() {
		return field;
	}
	public void setField(FileParsingField field) {
		this.field = field;
	}
	
	
}
