package com.sciformix.client.pat.commons;

import java.util.Comparator;
import java.util.Date;

import com.sciformix.client.pat.toolb.CpatToolBConstants;
import com.sciformix.commons.utils.ParsedDataTable;

public class CaseDocumentComparator implements Comparator<PreviewCaseDoc> 
{
	
	@Override
	public int compare(PreviewCaseDoc o1, PreviewCaseDoc o2) 
	{
		String s1TransmissionDate = null;
		String s2TransmissionDate = null;

		String s1MessageDate = null;
		String s2MessageDate = null;
		
		Date d1TransmissionDate = null;
		Date d2TransmissionDate = null;
		
		Date d1MessageDate = null;
		Date d2MessageDate = null;
		
		ParsedDataTable p1 = null;
		ParsedDataTable p2 = null;
		
		p1 = o1.getDataTable();
		p2 = o2.getDataTable();
		
		
		int compareValue = 0;
		
		s1TransmissionDate = String.valueOf(p1.getColumnValues(CpatToolBConstants.CaseIdTabKeys.TRANSMISSION_DATE).get(0).displayValue());
		s2TransmissionDate = String.valueOf(p2.getColumnValues(CpatToolBConstants.CaseIdTabKeys.TRANSMISSION_DATE).get(0).displayValue());
		
		d1TransmissionDate = CpatToolHome.convertToDate(s1TransmissionDate, CpatToolBConstants.DATEFORMATS.DF_TRANSMISSION_DATE);
		d2TransmissionDate = CpatToolHome.convertToDate(s2TransmissionDate, CpatToolBConstants.DATEFORMATS.DF_TRANSMISSION_DATE);
		
		s1MessageDate = String.valueOf(p1.getColumnValues(CpatToolBConstants.CaseIdTabKeys.MESSAGE_DATE).get(0).displayValue());
		s2MessageDate = String.valueOf(p2.getColumnValues(CpatToolBConstants.CaseIdTabKeys.MESSAGE_DATE).get(0).displayValue());
				
		d1MessageDate = CpatToolHome.convertToDate(s1MessageDate, CpatToolBConstants.DATEFORMATS.DF_MESSAGE_DATE);
		d2MessageDate = CpatToolHome.convertToDate(s2MessageDate, CpatToolBConstants.DATEFORMATS.DF_MESSAGE_DATE);
		
		if(d1TransmissionDate.after(d2TransmissionDate))
		{
			compareValue =  1;
		}
		else if(d1TransmissionDate.before(d2TransmissionDate))
		{
			compareValue =  -1;
		}
		else
		{
			// When Transmission Dates are equal
			if(d1MessageDate.after(d2MessageDate))
			{
				compareValue  = 1;
			}
			else if(d1MessageDate.before(d2MessageDate))
			{
				compareValue = -1;
			}
			else
			{
				compareValue = 0;
			}
		}
		
		return compareValue;
		
	}
	
}
