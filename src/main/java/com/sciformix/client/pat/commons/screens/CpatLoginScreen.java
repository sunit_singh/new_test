package com.sciformix.client.pat.commons.screens;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.FormSubmitEvent;
import javax.swing.text.html.HTMLEditorKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.ScpProject;
import com.sciformix.client.pat.commons.ScpUser;
import com.sciformix.client.pat.toolb.CpatToolBConstants;
import com.sciformix.client.pat.toolb.CpatToolBErrorMessages;
import com.sciformix.client.webapiclient.SciPortalWebApiClient;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse.STATUS;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.utils.FileUtils;
import com.sciformix.commons.utils.StringUtils;

public class CpatLoginScreen extends CpatBaseHtmlScreen
{
	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 3350418882184107366L;

	private static final Logger log = LoggerFactory.getLogger(CpatLoginScreen.class);
	
	private static final String PROPS_FILE = "cpat.properties";
	
	private static final String KEYID = "sciportal.webapiclient.publickey.keyid";
	
	private static final String ZONES = "sciportal.tool.zones";
	
	private static final String ZONE_CONTENT = "$ZONES";
	
	
	public CpatLoginScreen(CpatToolBase p_oToolBase, IAction p_oCallbackAction) 
	{
		super(p_oToolBase, p_oCallbackAction, p_oToolBase.getToolDetails().getToolTitle(), 450, 450, "login.html",null);
	}
	

	protected void addActionListener(Component comp, final JFrame frame) 
	{
		log.info("Login Screen: Adding Action Listeners");
		
		JEditorPane jep = null;
		
		jep = (JEditorPane) comp;
		
		jep.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent event)
			{
				if(event.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
				{
					if (event instanceof FormSubmitEvent) 
					{
						loginActionListener((FormSubmitEvent)event);
					}
					else
					{
						new ToolDescriptionScreen(oToolBase);
					}
					
				}
				else
				{
					//TODO:Something here
				}
			}

		});
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) 
			{
				frame.dispose();
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	private void loginActionListener(FormSubmitEvent event)
	{
		log.info("Login Screen: Action Occured");
		
		String sZone = null;
		String sUsername = null;
		String sPassword = null;
		
		Map<String, String> mapExtractedParameters = null;
		
		SciPortalWebApiClient scpClient = null;
		SciPortalWebApiResponse response = null;
		
		Properties props = null;
		InputStream inputStream = null;
		
		ScpUser oScpUser = null;
		String projectStatus = null;
		
		try
		{
			mapExtractedParameters = CpatToolHome.extractEventParameters(event);
			
			sZone = mapExtractedParameters.get("SCP_ZONE");
			sUsername = mapExtractedParameters.get("SCP_USERNAME");
			sPassword = mapExtractedParameters.get("SCP_PASSWORD");
			
			if (StringUtils.isNullOrEmpty(sUsername) || StringUtils.isNullOrEmpty(sPassword))
			{
				JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.Login.EMPTY_CREDENTIALS);
			}
			else 
			{
				props = new Properties();
				inputStream = CpatLoginScreen.class.getClassLoader().getResourceAsStream(PROPS_FILE); 
				
				
					props.load(inputStream);
					
					scpClient = SciPortalWebApiClient.create(props, oToolBase.getToolDetails().getToolName(), 
									oToolBase.getToolDetails().getToolVersion(), props.getProperty(KEYID), sZone
								);
					
					if(scpClient != null)
					{
						response = scpClient.getProjects(sUsername, sPassword);
						if(response.getStatus().equals(STATUS.SUCCESS))
						{	
							if(response.getData() instanceof String)
							{
								JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.Login.NO_PRJ_ERR);
							}
							
							else
							{
								List<Map<String,Object>> projectMapList = (List<Map<String, Object>>) response.getData();
								
								if(projectMapList.size() == 1)
								{	
									projectStatus = String.valueOf(projectMapList.get(0).get(CpatToolBConstants.API.STATUS));
									
									if(projectStatus.equalsIgnoreCase(CpatToolBConstants.API.OK))
									{
										oToolBase.scpClient = scpClient;
										oScpUser = new ScpUser(sUsername, sPassword, sZone);
										
										for(Map<String, Object> projectMap: projectMapList)
										{
											ScpProject oProject = new ScpProject(Integer.parseInt(projectMap.get(CpatToolBConstants.API.OBJSEQID).toString()), projectMap.get(CpatToolBConstants.API.OBJNAME).toString());
											oScpUser.addUserProjectsList(oProject);
										}
										
										oToolBase.scpUser = oScpUser;
										
										disposeScreen();
										
										m_oCallbackAction.performAction(CPAT_ACTION.LOGIN_SUCCESSFUL);												
									}
									else
									{
										JOptionPane.showMessageDialog(null, projectStatus);
									}
									
								}
								else if(projectMapList.size() == 0)
								{
									JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.Login.NO_PRJ_ERR);
								}
								else if(projectMapList.size() > 1)
								{
									JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.Login.MULTIPLE_PRJ_ERR);
								}
								
							}
							
						}
						else
						{
							JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.ERROR + response.getCode() + " - " + response.getMessage());
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.API.API_FAILED);
					}
				
			}
		}catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.Login.LOGIN_FAILED);
			log.error("Login Failed", null, e);
		}
	}


	@Override
	protected JEditorPane prepareContentFrame(String p_sPageToLoad, Map<String, Object> params) 
	{
		log.info("Login Screen: Rendering");
		
		String content=null;
		
		InputStream initialStream=null;
		JEditorPane jep = null;
		
		Map<String, String> propMap = null;
		String optionStart = null;
		String optionEnd = null;
		
		StringBuilder zonesHtml = null;
		String toolZones = null;
		String[] arrZones = null;
		
		try 
		{	
			optionStart = "<option>";
			optionEnd = "</option>";
			
			zonesHtml = new StringBuilder();
			
			propMap = CpatToolHome.loadProperties(PROPS_FILE);
			
			toolZones = propMap.get(ZONES);
			
			if(!StringUtils.isNullOrEmpty(toolZones))
			{
				arrZones = toolZones.split(StringConstants.COMMA);
				
				for(String zone: arrZones)
				{
					zonesHtml = zonesHtml.append(optionStart + zone + optionEnd);
				}
				
				jep = new JEditorPane();
				jep.setContentType("text/html");
				jep.setEditable(false);
				
				initialStream = ClassLoader.getSystemResourceAsStream(p_sPageToLoad);
				content = FileUtils.getStringFromInputStream(initialStream);
				
				if(zonesHtml.length() > 0)
				{
					content = content.replace(ZONE_CONTENT, zonesHtml.toString());				
				}
				
				jep.setText(content);
				
				((HTMLEditorKit) jep.getEditorKit()).setAutoFormSubmission(false);
			}
			else
			{
				JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.Login.NO_ZONE_ERR);
			}
			
		} catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.Login.RENDERING_FAILED);
			log.error("Login Screen: Rendering Failed", null, e);
		}
		
		return jep;
	}
	
	
}
