package com.sciformix.client.pat.commons.screens;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.util.Map;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolb.CpatToolBErrorMessages;
import com.sciformix.commons.utils.FileUtils;

public class ToolDescriptionScreen extends CpatBaseHtmlScreen 
{	
	private static Logger log = LoggerFactory.getLogger(ToolDescriptionScreen.class);
	
	private static final long serialVersionUID = -9044471434286767925L;
	
	public ToolDescriptionScreen(CpatToolBase oToolBase){
		super(oToolBase, null, oToolBase.getToolDetails().getToolTitle(), 450, 450, "tooldesc.html", null);
	}
	
	@Override
	protected void addActionListener(Component comp, final JFrame frame) 
	{	
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) 
			{
				frame.dispose();
			}
		});
	}

	@Override
	protected JEditorPane prepareContentFrame(String p_sPageToLoad, Map<String, Object> params) 
	{
		log.info("Tool Description Screen: Rendering");
		
		String content=null;
		InputStream initialStream=null;
		JEditorPane jep = null;
		
		
		
		try 
		{
			jep = new JEditorPane();
			jep.setContentType("text/html");
			jep.setEditable(false);
			
			initialStream = ClassLoader.getSystemResourceAsStream(p_sPageToLoad);
			content = FileUtils.getStringFromInputStream(initialStream);
			
			content = content.replace(CpatToolConstants.TOOL_DISPLAY_NAME, oToolBase.getToolDetails().getToolDisplayName());
			content = content.replace(CpatToolConstants.TOOL_VERSION, oToolBase.getToolDetails().getToolVersion());
			content = content.replace(CpatToolConstants.TOOL_DESC, oToolBase.getToolDetails().getToolDescription());
			content = content.replace(CpatToolConstants.TOOL_HEADING, oToolBase.getToolDetails().getToolHeading());
			
			jep.setText(content);
			
		} catch (Exception e) 
		{
			//TODO: The error message class can remain same as the error thrown is same.
			JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.ToolDescScreen.RENDERING_FAILED);
			log.error("Creation of Tool Description page failed", null, e);
		} 
		
		return jep;
	}

}
