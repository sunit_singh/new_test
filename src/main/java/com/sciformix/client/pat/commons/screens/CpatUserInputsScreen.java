package com.sciformix.client.pat.commons.screens;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.util.Map;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.html.HTMLEditorKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.ScpUser;
import com.sciformix.client.pat.toolb.CpatToolBErrorMessages;
import com.sciformix.commons.utils.FileUtils;

public class CpatUserInputsScreen extends CpatBaseHtmlScreen {
	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 3350418882184107366L;

	private static final Logger log = LoggerFactory.getLogger(CpatLoginScreen.class);

	public CpatUserInputsScreen(CpatToolBase p_oToolBase, IAction p_oCallbackAction, String p_sUserInputsScreenFile, ScpUser p_oScpUser) 
	{
		super(p_oToolBase, p_oCallbackAction, p_oToolBase.getToolDetails().getToolTitle(), 715, 520, p_sUserInputsScreenFile, null);
	}

	protected void addActionListener(Component comp, final JFrame frame) 
	{	
		log.info("User Input Screen: Adding Action Listeners");
		oToolBase.userInputsScreenEventHandler(oToolBase, m_oCallbackAction, comp, frame);
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) 
			{
				frame.dispose();
			}
		});
	}

	
	/**
	 * Prepare Content
	 */
	@Override
	protected JEditorPane prepareContentFrame(String p_sPageToLoad, Map<String, Object> extractedParameters) 
	{
		log.info("User Input Screen: Rendering");
		
		String content=null;
		InputStream initialStream=null;
		JEditorPane jep = null;
		
		try 
		{	
			jep = new JEditorPane();
			jep.setContentType("text/html");
			jep.setEditable(false);
			
			initialStream = ClassLoader.getSystemResourceAsStream(p_sPageToLoad);
			content = FileUtils.getStringFromInputStream(initialStream);
			
			jep.setText(content);
			
			((HTMLEditorKit) jep.getEditorKit()).setAutoFormSubmission(false);

		} catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, CpatToolBErrorMessages.UserInputScreen.RENDERING_FAILED);
			log.error("Creation of User Input Screen page failed", null, e);
		}
		
		return jep;
	}

}
