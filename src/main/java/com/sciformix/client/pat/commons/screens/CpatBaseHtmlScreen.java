package com.sciformix.client.pat.commons.screens;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.IAction;



public abstract class CpatBaseHtmlScreen extends JFrame
{
	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 3350418882184107366L;

	private static final Logger log = LoggerFactory.getLogger(CpatBaseHtmlScreen.class);
	
	
	protected IAction m_oCallbackAction = null;
	
	protected JFrame oWindowFrame = null;
	
	protected CpatToolBase oToolBase = null;
	
	public CpatBaseHtmlScreen(CpatToolBase p_oToolBase, IAction p_oCallbackAction, String p_sWindowTitle, int p_nWindowHeight, int p_nWindowWidth, String p_sPageToLoad, Map<String, Object> params) 
	{
		m_oCallbackAction = p_oCallbackAction;
		oToolBase = p_oToolBase;
		prepareWindow(p_sWindowTitle, p_nWindowHeight, p_nWindowWidth, p_sPageToLoad, params);
	}
	
	public CpatBaseHtmlScreen() {
		// TODO Auto-generated constructor stub
	}

	private void prepareWindow(String p_sWindowTitle, int p_nWindowHeight, int p_nWindowWidth, String p_sPageToLoad, Map<String, Object> params) 
	{
		Component comp = null;
		
		//Step 1: Prepare Window Frame
		//oWindowFrame = prepareWindowFrame(p_sWindowTitle, p_nWindowHeight, p_nWindowWidth);
		
		oWindowFrame = new JFrame();
		
		//Step 2: Add Content Frame
		
		comp =  prepareContentFrame(p_sPageToLoad, params);
		
		if(comp!=null)
		{
			oWindowFrame.add(comp, BorderLayout.CENTER);
			
			addActionListener(comp, oWindowFrame);
			
			prepareWindowFrame(oWindowFrame, p_sWindowTitle, p_nWindowHeight, p_nWindowWidth);			
		}
		 
	}

	private void prepareWindowFrame(JFrame oWindowFrame, String p_sWindowTitle, int p_nWindowHeight, int p_nWindowWidth)
	{
		InputStream initialStream = null;
		
		oWindowFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		oWindowFrame.setSize(p_nWindowWidth, p_nWindowHeight);
		oWindowFrame.setLocationRelativeTo(null);
		oWindowFrame.setTitle(p_sWindowTitle);
		
		try
		{
			initialStream = ClassLoader.getSystemResourceAsStream("sciformix_icon.png");
			oWindowFrame.setIconImage(ImageIO.read(initialStream));
		}
		catch (IOException e)
		{
			log.error("Error while getting Icon Image < "+e.getStackTrace()+" > ");
			
		}
		
		oWindowFrame.setEnabled(true);
		oWindowFrame.setVisible(true);

	}
	
	
	protected abstract Component prepareContentFrame(String p_sPageToLoad, Map<String, Object> params);
	
	protected abstract void addActionListener(Component comp, JFrame frame); 
	
	protected void disposeScreen()
	{
		oWindowFrame.dispose();
		oWindowFrame = null;
	}
}
