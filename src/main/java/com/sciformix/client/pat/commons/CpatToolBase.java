package com.sciformix.client.pat.commons;

import java.awt.Component;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import com.sciformix.client.webapiclient.SciPortalWebApiClient;
import com.sciformix.commons.utils.ParsedDataTable;

public abstract class CpatToolBase
{
	private ToolDetails toolDetails;

	public CpatFieldMap s_mapCpatFieldsFromXml = null;
	
	public ScpUser scpUser = null;
	public SciPortalWebApiClient scpClient = null;
	public int activityId = -1;

	public Map<String, String> userInputsParams = null;
	
	public Map<String, String> browserMapDetails = null;

	public Map<String, Object> resultScreenMap = null;
	
	public static Map<String, String> testModePropMap= null;
	
	public static boolean isTestMode = false;
	
	protected static final String parserConfiguration = "AA_Parser,BA_Parser";
	
	//TODO: Should have created a Tool Class and passed the Tool Object rather than passing all the parameters
	protected CpatToolBase(ToolDetails p_tooldetails)
	{
		toolDetails = p_tooldetails;
		loadCpatFieldMap();
	}
	
	protected abstract void loadCpatFieldMap(); 
	
	public abstract List<String> validateUserInputs(Map<String, String> mapExtractedParameters);
	
	

	public abstract void userInputsScreenEventHandler(CpatToolBase oToolBase, IAction oCallbackAction, Component comp, JFrame frame);

	public abstract void previewScreenEventHandler(CpatToolBase oToolBase, IAction m_oCallbackAction, Component comp,
			JFrame oWindowFrame, ParsedDataTable summaryDataTable, Map<String, String> parsedDataMap);
	
	public ToolDetails getToolDetails() {
		return toolDetails;
	}

	public void setToolDetails(ToolDetails toolDetails) {
		this.toolDetails = toolDetails;
	}
}
