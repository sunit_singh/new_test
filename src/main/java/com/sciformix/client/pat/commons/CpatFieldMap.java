package com.sciformix.client.pat.commons;

import java.util.ArrayList;
import java.util.List;

public class CpatFieldMap
{
	public static class CpatField
	{
		private String m_sSdMnemonic = null;
		private String m_sMappingType = null;
		private String m_sWriterMnemonic = null;

		public CpatField(String p_sSdMnemonic, String p_sMappingType, String p_sWriterMnemonic) {
			m_sSdMnemonic = p_sSdMnemonic;
			m_sMappingType = p_sMappingType;
			m_sWriterMnemonic = p_sWriterMnemonic;
		}

		public void setSdMnemonic(String m_sSdMnemonic) {
			this.m_sSdMnemonic = m_sSdMnemonic;
		}

		public String getSdMnemonic() {
			return m_sSdMnemonic;
		}

		public void setMappingType(String m_sMappingType) {
			this.m_sMappingType = m_sMappingType;
		}

		public String getMappingType() {
			return m_sMappingType;
		}

		public void setWriterMnemonic(String m_sWriterMnemonic) {
			this.m_sWriterMnemonic = m_sWriterMnemonic;
		}

		public String getWriterMnemonic() {
			return m_sWriterMnemonic;
		}
	}
	
	private String m_sCpatFieldId = null;
	private String m_sParseVersion = null;
	private String m_sParser = null;
	private String m_sWriter = null;

	private List<CpatField> m_listFields = null;

	public CpatFieldMap(String p_sCpatFieldId, String p_sParseVersion, String p_sParser, String p_sWriter) {
		m_sCpatFieldId = p_sCpatFieldId;
		m_sParseVersion = p_sParseVersion;
		m_sParser = p_sParser;
		m_sWriter = p_sWriter;

		m_listFields = new ArrayList<CpatField>();
	}

	public String getCpatFieldId() {
		return m_sCpatFieldId;
	}

	public void setCpatFieldId(String m_sCpatFieldId) {
		this.m_sCpatFieldId = m_sCpatFieldId;
	}

	public String geParseVersion() {
		return m_sParseVersion;
	}

	public void setParseVersion(String m_sParseVersion) {
		this.m_sParseVersion = m_sParseVersion;
	}

	public String getParser() {
		return m_sParser;
	}

	public void setParser(String m_sParser) {
		this.m_sParser = m_sParser;
	}

	public String getWriter() {
		return m_sWriter;
	}

	public void setWriter(String m_sWriter) {
		this.m_sWriter = m_sWriter;
	}

	public List<CpatField> getFields() {
		return m_listFields;
	}

	public void addField(CpatField m_field) {
		this.m_listFields.add(m_field);
	}
}
