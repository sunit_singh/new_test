package com.sciformix.client.pat.commons;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.FormSubmitEvent;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.CaseDocument;
import com.sciformix.client.automation.sceptre.SceptreConstants;
import com.sciformix.client.pat.commons.screens.CpatCasePreviewScreen;
import com.sciformix.client.pat.commons.screens.CpatLoginScreen;
import com.sciformix.client.pat.commons.screens.CpatUserInputsScreen;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse.STATUS;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.utils.StringUtils;

public class CpatToolHome {

	private static final Logger log = LoggerFactory.getLogger(CpatToolHome.class);
		
	public static void initializeOperatingEnvironment()
	{
		try
		{
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("taskkill /F /IM iexplore.exe");
			
		}
		catch (IOException e)
		{
			log.error("Error in killing IE driver < "+e.getMessage()+" >");
		}
	}
	
	public static void showLoginScreen(CpatToolBase p_oToolBase ,IAction p_oCallbackAction)
	{
		//Prompt for authentication
		new CpatLoginScreen(p_oToolBase, p_oCallbackAction);
	}
	
	
	public static void showUserInputsScreen(CpatToolBase p_oToolBase, IAction p_oCallbackAction) 
	{
		new CpatUserInputsScreen(p_oToolBase, p_oCallbackAction, p_oToolBase.getToolDetails().getToolUserInputScreen(), p_oToolBase.scpUser);
	}
	
	
	public static Map<String, String> extractEventParameters(FormSubmitEvent event) throws UnsupportedEncodingException
	{
		String[] arrSubmittedData = null;
		String[] arrSingleKeyValue = null;
		Map<String, String> mapExtractedParameters = null;
		
		mapExtractedParameters = new HashMap<String, String>();
		
		arrSubmittedData = StringUtils.split(event.getData(), StringConstants.AMPERSAND);
		
		for (String sOneParamKeyValue : arrSubmittedData)
		{
			arrSingleKeyValue = StringUtils.split(sOneParamKeyValue, StringConstants.EQUAL);
			if (arrSingleKeyValue.length > 1)
			{
				mapExtractedParameters.put(arrSingleKeyValue[0], URLDecoder.decode(arrSingleKeyValue[1], "UTF-8").trim());	
			}
		}
		
		return mapExtractedParameters;
	}
	
	public static Element findBodyTabByHTMLDoc(HTMLDocument htdoc)
	{
		Element[] roots = htdoc.getRootElements();
        Element body = null;
        for( int i = 0; i < roots[0].getElementCount(); i++ ) {
            Element element = roots[0].getElement( i );
            if( element.getAttributes().getAttribute( StyleConstants.NameAttribute ) == HTML.Tag.BODY ) {
                body = element;
                break;
            }
        }
        return body;
	}
	
	public static void showPreviewScreen(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, Object> params) 
	{			
		CpatCasePreviewScreen oPreviewScreen = new CpatCasePreviewScreen(oToolBase, oCallbackAction, params);
	}

	public static Date convertToDate(String p_sDate, String p_sDateFormat)
	{
		Date m_oDate = null;
		SimpleDateFormat sdf = null;
		
		sdf = new SimpleDateFormat(p_sDateFormat);
		
		try {
			m_oDate = sdf.parse(p_sDate);
		} catch (ParseException e) {
			log.error("Could not parse date: " + e.getMessage());
			e.printStackTrace();
		}
		
		return m_oDate;
		
	}
	
	public static Map<String, String> loadProperties(String p_sConfigForInsertProperties) {
		Map<String, String> propMap = null;
		Properties configProp = null;
		
		propMap = new HashMap<String, String>();
		InputStream inputStream = CpatToolHome.class.getClassLoader().getResourceAsStream(p_sConfigForInsertProperties);
        try 
        {
        	configProp = new Properties();
            configProp.load(inputStream);
            
            for (final String name: configProp.stringPropertyNames())
			    propMap.put(name, configProp.getProperty(name));
        
        } 
        catch (IOException e) 
        {
        	log.error(e.getMessage());
            e.printStackTrace();
        }
        
        return propMap;
	}

	public static boolean callCommonRegisterActivity(CpatToolBase oToolBase, IAction oCallbackAction, String activityType, int activityId, String activityCode, String activityDesc, Map<String, String> payload )
	{
		SciPortalWebApiResponse response = null;
		ScpUser scpUser = oToolBase.scpUser;
		boolean isRegisterActivitySuccess = false;
		String chromeVersion = null;
		String ieVersion = null;
		
		if(oToolBase.browserMapDetails != null)
		{
			
			if(oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERNAME).equalsIgnoreCase(CpatToolConstants.Browser.CHROME))
			{
				chromeVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
				ieVersion = StringConstants.EMPTY;
			}
			else
			{
				chromeVersion = StringConstants.EMPTY;
				ieVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
			}
			
		}
		
		response = oToolBase.scpClient.registerActivity(scpUser.getUserId(), scpUser.getPassword(),
						scpUser.getUserProjectsList().get(0).getProjectSeqId(), oToolBase.getToolDetails().getToolName(),
						activityType, activityId, activityCode, "", payload, chromeVersion, ieVersion);
		
		if(response.getStatus() == STATUS.SUCCESS)
		{
			activityId = Integer.parseInt(response.getData().toString());
			
			if(activityId > 0)
			{
				oToolBase.activityId = activityId;	
				isRegisterActivitySuccess = true;
			}
			
			else
			{
				JOptionPane.showMessageDialog(null, "Invalid Activity Id");
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Error: " + response.getCode() + " - " + response.getMessage());
			oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
		}
		
		return isRegisterActivitySuccess;
	}
	public static Map<String, String> loadExternalProperties(String p_sConfigForInsertProperties) {
		Map<String, String> propMap = null;
		Properties configProp = null;
		FileReader reader = null;
		
		propMap = new HashMap<String, String>();
        try 
        {
        	reader = new FileReader(p_sConfigForInsertProperties);
        	configProp = new Properties();
            configProp.load(reader);
            
            for (final String name: configProp.stringPropertyNames())
			    propMap.put(name, configProp.getProperty(name));
        
        } 
        catch (IOException e) 
        {
        	log.error(e.getMessage());
            e.printStackTrace();
        }
        
        return propMap;
	}
	public static List<CaseDocument> provideListCaseDoc(Map<String, String> testModePropMap)
	{
		CaseDocument oCaseDocument = null;
		List<CaseDocument> listCaseDocuments = null;
		
		Map<String , Object> mAttributeMap = null;
		
		String count = null;
		
		int cnt = 0;
		
		File file = null;
		
		String fileName = null, filePath = null;
		
		String strVersion = null;
		
		int version = 0;
		
		int index = 0;
		
		listCaseDocuments = new ArrayList<>();
	    if(testModePropMap != null)
	    {
	    	count = testModePropMap.get("Cpat.Sourcedocument.Count");
	    	
	    	cnt = Integer.parseInt(count);
	    	
	    	for(int cntNum =1 ; cntNum<=cnt; cntNum++)
	    	{
	    		oCaseDocument = new CaseDocument();
	    		
	    		mAttributeMap = new HashMap<>();
	    		
	    		fileName = testModePropMap.get("Cpat.Sourcedocument."+cntNum);
	    		
	    		strVersion = testModePropMap.get("Cpat.Sourcedocument."+cntNum+".version");
	    		
	    		file = new File(fileName);
	    		
	    		oCaseDocument.setPath(file);
	    		
	    		index = fileName.lastIndexOf("\\");
	    		
	    		fileName = fileName.substring(index+1, fileName.length());
	    		
	    		mAttributeMap.put(SceptreConstants.DownloadSD.DOCUMENT_NAME,fileName);
	    		
	    		mAttributeMap.put(SceptreConstants.DownloadSD.CASE_VERSION, strVersion);
	    		
	    		oCaseDocument.setoAttributeMap(mAttributeMap);
	    		
	    		listCaseDocuments.add(oCaseDocument);
	    	
	    	}
		}
		
		return listCaseDocuments;
	}
	
	/**
	 * The method will be used to find a single keywords
	 * is there in the paragraph or not.
	 * @param text The paragraph in which the keyword
	 * 		  need to be find
	 * @param keyword The word that need to be found
	 * 		  in the passed text
	 * @return true if the keyword is found and false
	 *         if the keyword is not found
	 */
	
	public static boolean isKeywordFound(String text, String keyword)
	{	
		String regex = null; 
		Pattern pattern = null;
		Matcher matcher = null;
		
		regex = "\\b"+keyword.toUpperCase()+"\\b";
	    
		pattern = Pattern.compile(regex);
	    matcher = pattern.matcher(text.toUpperCase());
		
		return matcher.find();
	}	
	
	public static List<String> getListOfKeywords (String p_keywords, String p_delimiter) {
		return Arrays.asList(p_keywords.split(p_delimiter));
	}
	
	public static String getPropertyNameAppendedWithZone (String p_zone, String p_propprefix) {
		
		String serverUrl = null;
		
		if(p_zone.equalsIgnoreCase(CpatToolConstants.ZONE_PROPS.DISCONNETED))
		{
			serverUrl = p_propprefix + CpatToolConstants.ZONE_PROPS.DISCONNETED;
		}
		else if(p_zone.equalsIgnoreCase(CpatToolConstants.ZONE_PROPS.TEST))
		{
			serverUrl = p_propprefix + CpatToolConstants.ZONE_PROPS.TEST;
		}
		else if(p_zone.equalsIgnoreCase(CpatToolConstants.ZONE_PROPS.PREPRODUCTION))
		{
			serverUrl = p_propprefix + CpatToolConstants.ZONE_PROPS.PREPRODUCTION;
		}
		else if(p_zone.equalsIgnoreCase(CpatToolConstants.ZONE_PROPS.PRODUCTION))
		{
			serverUrl = p_propprefix + CpatToolConstants.ZONE_PROPS.PRODUCTION;
		}
		
		return serverUrl;
	}
	
	public static String convertListToStringAndReplacePrefix(List<String> list, String prefix)
	{
		StringBuilder sb = new StringBuilder();
		List<String> tempList = new ArrayList<String>();
		for(int i = 0;  i < list.size(); i++)
		{
			if(list.get(i).contains(prefix)){
				tempList.add(list.get(i).replaceAll(prefix, ""));
			}
		}
		
		for(int i = 0;  i < tempList.size(); i++)
		{
			sb = sb.length() > 0 ? sb.append("," + tempList.get(i)):sb.append(tempList.get(i));
		}
		
		return sb.toString();
	}
	
	public static String updateDropDownAsBlank(String val, String defaultValue)
	{
		if(val.equalsIgnoreCase(defaultValue))
		{
			return StringConstants.EMPTY;
		}
		else
		{
			return val;
		}
	}
}
