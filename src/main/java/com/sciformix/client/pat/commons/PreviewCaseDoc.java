package com.sciformix.client.pat.commons;

import java.io.File;

import com.sciformix.commons.utils.ParsedDataTable;

public class PreviewCaseDoc {
	private ParsedDataTable oDataTable = null;
	private File pdfFile = null;
	
	private String caseDocVersion = null;

	public PreviewCaseDoc(File pdfFile, ParsedDataTable pDataTable, String caseDocVersion) {
		this.pdfFile = pdfFile;
		this.oDataTable = pDataTable;
		this.caseDocVersion = caseDocVersion;
	}

	public ParsedDataTable getDataTable() {
		return oDataTable;
	}

	public void setDataTable(ParsedDataTable oDataTable) {
		this.oDataTable = oDataTable;
	}

	public File getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(File pdfFile) {
		this.pdfFile = pdfFile;
	}
	
	public String getCaseDocVersion()
	{
		return caseDocVersion;
	}
	
	public void setCaseDocVersion(String caseDocVersion)
	{
		this.caseDocVersion = caseDocVersion;
	}

}
