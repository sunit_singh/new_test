package com.sciformix.client.pat.commons;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.StringUtils;

public class WorkbookWorkSheetWrapperDEA
{
	private String m_sSheetName = null;
	private int m_nSheetIndex = 0;
	private boolean m_bIsHeaderRowPresent = false;
	private int m_nNumberOfCols = 0;
	private int m_nNumberOfDataRows = 0;
	private List<Object> m_listHeaderRowContent = null; 
	private List<List<Object>> m_listDataTableContent = null;
	private Map<Object, Integer> m_mapColumnReverseIndex = null;
	
	public WorkbookWorkSheetWrapperDEA(String p_sSheetName, int p_nSheetIndex, boolean p_bIsHeaderRowPresent, int p_nNumberOfCols, int p_nNumberOfDataRows,
	List<Object> p_oarrHeaderRowContent ,	List<List<Object>> p_oarrDataTableContent)
	{
		m_sSheetName = p_sSheetName;
		m_nSheetIndex = p_nSheetIndex;
		m_bIsHeaderRowPresent = p_bIsHeaderRowPresent;
		m_nNumberOfCols = p_nNumberOfCols;
		m_nNumberOfDataRows = p_nNumberOfDataRows;
		m_listHeaderRowContent = p_oarrHeaderRowContent;
		m_listDataTableContent = p_oarrDataTableContent;
		
		m_mapColumnReverseIndex = new HashMap<Object, Integer>();
		for (int nColIndex = 0; nColIndex < m_nNumberOfCols; nColIndex++)
		{
			m_mapColumnReverseIndex.put(m_listHeaderRowContent.get(nColIndex), nColIndex);
		}
	}

	public List<Object> getColumnValues(String p_sColumnName)
	{
		List<Object> listValues = null;
		
		//TODO:
		
		return listValues;
	}
	

	public List<Object> getRowValues(int p_nDataRowIndex)
	{
		if (p_nDataRowIndex < m_nNumberOfDataRows)
		{
			return m_listDataTableContent.get(p_nDataRowIndex);
		}
		else
		{
			return null;
		}
	}

	public Object getCellValue(String p_sColumnName, int p_nDataRowIndex)
	{
		Object oValue = null;
		Integer nIndexOfSelectedColumn = -1;
		
		nIndexOfSelectedColumn = m_mapColumnReverseIndex.get(p_sColumnName);
		if (nIndexOfSelectedColumn != null)
		{
			oValue = m_listDataTableContent.get(p_nDataRowIndex).get(nIndexOfSelectedColumn);
		}
		return oValue;
	}
	
	public void appendDataIntoDataTable(DataTable p_oDataTable, boolean p_bCollapseMultivaluedCells, String p_sColumnsToCollapse)
	{
		List<String> listColumnNames = null;
		String[] arrColumnNames = null;
		List<Object> oListSingleWorkbookRow = null;
		String sHeaderName = null;
		Object oWorkbookElementValue = null;
		boolean bColumnPresentInDataTable = false;
		boolean bNewColumnValueAlreadyPresent = false;
		List<Object> listDataTableValuesForColumn = null;
	
		arrColumnNames = p_sColumnsToCollapse == null ? new String[0] : p_sColumnsToCollapse.split(",");
		listColumnNames = new ArrayList<String>(Arrays.asList(arrColumnNames)); 
		for (int nWorkbookRowIndex = 0; nWorkbookRowIndex < m_listDataTableContent.size(); nWorkbookRowIndex++)
		{
			oListSingleWorkbookRow = m_listDataTableContent.get(nWorkbookRowIndex);
			
			for (int nWorkbookColumnIndex = 0; nWorkbookColumnIndex < m_nNumberOfCols; nWorkbookColumnIndex++) 
			{
				sHeaderName = (String) m_listHeaderRowContent.get(nWorkbookColumnIndex);
				oWorkbookElementValue = oListSingleWorkbookRow.get(nWorkbookColumnIndex);
				
				bColumnPresentInDataTable = p_oDataTable.isColumnPresent(sHeaderName);
				if (bColumnPresentInDataTable)
				{
					if (p_bCollapseMultivaluedCells)
					{
						bNewColumnValueAlreadyPresent = false;
						listDataTableValuesForColumn = p_oDataTable.getColumnValues(sHeaderName);
						
						/*for (Object oDataTableColumnValue : listDataTableValuesForColumn)
						{
							if (oDataTableColumnValue != null && oDataTableColumnValue.equals(oWorkbookElementValue) && !listColumnNames.contains(sHeaderName))
							{
								//Do not add this
							//	bNewColumnValueAlreadyPresent = true;
							}
							else if (oDataTableColumnValue == null && oWorkbookElementValue == null)
							{
								//Do not add this
							//	bNewColumnValueAlreadyPresent = true;
							}
						}*/
						
						if (!bNewColumnValueAlreadyPresent)
						{
							if((oWorkbookElementValue==null || String.valueOf(oWorkbookElementValue).equals("")) && listColumnNames.contains(sHeaderName))
							{
								p_oDataTable.appendColumnValue(sHeaderName, "");
							}else if(!StringUtils.isNullOrEmpty(String.valueOf(oWorkbookElementValue)) )
							{
								p_oDataTable.appendColumnValue(sHeaderName, oWorkbookElementValue);
							}
								//p_oDataTable.dumpContents();
						}
						else
						{
							//Skip and continue
						}
					}
					else
					{
						
						if((oWorkbookElementValue==null || String.valueOf(oWorkbookElementValue).equals("")) && !listColumnNames.contains(sHeaderName))
						{
							p_oDataTable.appendColumnValue(sHeaderName, "");
						}else if(oWorkbookElementValue!=null && !String.valueOf(oWorkbookElementValue).equals(""))
						{
							p_oDataTable.appendColumnValue(sHeaderName, oWorkbookElementValue);
						}
						//p_oDataTable.dumpContents();
					}
				}
				else
				{
					//Column itself is new to the DataTable
					if((oWorkbookElementValue==null || String.valueOf(oWorkbookElementValue).equals("")) && listColumnNames.contains(sHeaderName))
					{
						p_oDataTable.addColumn(sHeaderName, "");
					}else if(oWorkbookElementValue==null || String.valueOf(oWorkbookElementValue).equals(""))
					{
						p_oDataTable.addColumn(sHeaderName, "");
					}else{
						p_oDataTable.addColumn(sHeaderName, oWorkbookElementValue);
					}
					//p_oDataTable.addColumn(sHeaderName, oWorkbookElementValue);
					//p_oDataTable.dumpContents();
				}
			}
		}
	}
	
	/*public JSONArray extractData(boolean p_bCollapseMultivaluedCells, JSONArray jsonArrayExistingData) throws SciException
	{
		OrderedJSONObject jsonRow = null;
		List<Object> oListSingleRow = null;
		Object oHeaderName = null;
		Object oRowValue = null;
		boolean bCheckRowIsUnique = false;
		
		if(jsonArrayExistingData == null)
		{
			jsonArrayExistingData = new JSONArray();
		}
		
		try
		{
			for(int nRowIndex = 0; nRowIndex < m_listDataTableContent.size(); nRowIndex++)
			{
				oListSingleRow = m_listDataTableContent.get(nRowIndex);
				jsonRow = new OrderedJSONObject();
				for (int nColumnIndex = 0; nColumnIndex < m_nNumberOfCols; nColumnIndex++) 
				{
					oHeaderName = (String) m_listHeaderRowContent.get(nColumnIndex);
					oRowValue = oListSingleRow.get(nColumnIndex);

					// DEV-NOTE: Header can be forced into String directly;
					// however, for values, care is required for special data types
					if (oHeaderName != null)
					{
						jsonRow.put((String) oHeaderName, DataUtils.formatForJson(oRowValue));
					}
				}
				
				if (p_bCollapseMultivaluedCells)
				{
					bCheckRowIsUnique = checkRowIsUnique(jsonArrayExistingData, jsonRow);
					if (bCheckRowIsUnique)
					{
						jsonArrayExistingData.put(jsonRow);
					}
				}
				else
				{
					jsonArrayExistingData.put(jsonRow);
				}
			}
		}
		catch (JSONException jsonExcep)
		{
			log.error("Error extracting data", jsonExcep);
			throw new SciException(jsonExcep);
		}
		return jsonArrayExistingData;
	}
	
	private boolean checkRowIsUnique(JSONArray jsonArrayData, OrderedJSONObject jsonRow)
	{
		boolean bRowIsUnique = false;
		OrderedJSONObject jsonObjectRow =null;
		
		//DEV-NOTE: Assume row is unique UNTIL we encounter duplicate values
		bRowIsUnique = true;
		try
		{
			for (int nRowIndex = 0; nRowIndex < jsonArrayData.size(); nRowIndex++)
			{
				jsonObjectRow = (OrderedJSONObject) jsonArrayData.get(nRowIndex);
				if (jsonObjectRow.size() != jsonArrayData.size())
				{
					bRowIsUnique = true;
				}
				
				for (Object oElementKey : jsonObjectRow.keySet())
				{
					
					if(!jsonObjectRow.get(oElementKey).equals(jsonRow.get(oElementKey)))
					{
						bRowIsUnique = true;
					}
				}
				
			}
		}
		catch(Exception e)
		{
			
		}
		
		return bRowIsUnique;
	}
	*/
	
	public boolean isHeaderRowPresent() {
		return m_bIsHeaderRowPresent;
	}


	public int getNumberOfCols() {
		return m_nNumberOfCols;
	}


	public List<Object> getHeaderRowContent() {
		return m_listHeaderRowContent;
	}


	public List<List<Object>> getDataTableContent() {
		return m_listDataTableContent;
	}

	public int getNumberOfDataRows() {
		return m_nNumberOfDataRows;
	}

	public int getSheetIndex() {
		return m_nSheetIndex;
	}

	public String getSheetName() {
		return m_sSheetName;
	}
/*
	public JSONArray getSingleObjectJsonArray(JSONArray jsonArray) throws SciException
	{
		JSONArray oTempJsonArray = null;
		OrderedJSONObject oJsonObject = null;
		OrderedJSONObject oTempJsonObject = null;
		String sKeyValue = null;
		try
		{
			oTempJsonObject = new OrderedJSONObject();
			oTempJsonArray = new JSONArray();
			for(int i=0;i<jsonArray.size();i++)
			{
				oJsonObject = (OrderedJSONObject) jsonArray.get(i);
				for(Object key:oJsonObject.keySet())
				{
					
					if(oTempJsonObject.size()>0 && oTempJsonObject.containsKey(key)&&!oTempJsonObject.get(key).equals(oJsonObject.get(key)))
					{
						
						if(!oTempJsonObject.get(key).equals("")&&!oJsonObject.get(key).equals(""))
						{
							sKeyValue = oTempJsonObject.get(key)+","+oJsonObject.get(key);
							oTempJsonObject.put(key, sKeyValue);
							// System.out.println(sKeyValue);
						}
					}
					else
					{
						oTempJsonObject.put(key, oJsonObject.get(key));
					}
					
				}
			}
			Integer iIndex = null;
			System.out.println(oTempJsonObject);
			for(Object key:oTempJsonObject.keySet())
			{
				
				sKeyValue = oTempJsonObject.get(key).toString();
				if(sKeyValue.contains(","))
				{
					iIndex = sKeyValue.lastIndexOf(",");
					sKeyValue = new StringBuilder(sKeyValue).replace(iIndex, iIndex+1," AND ").toString();
					oJsonObject.put(key, sKeyValue);
				}else
				{
					oJsonObject.put(key, sKeyValue);
				}
				
			}
			System.out.println(oJsonObject.size());
			oTempJsonArray.put(oJsonObject);
			
		}
		catch (Exception excep)
		{
			log.error("Error extracting data", excep);
			throw new SciException(excep);
		}
		return oTempJsonArray;
	}
*/
}
