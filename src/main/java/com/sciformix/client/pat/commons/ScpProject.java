package com.sciformix.client.pat.commons;

public class ScpProject
{
	
	private int projectSeqId;
	private String projectName;
	
	public ScpProject(int projectSeqId, String projectName) 
	{
		this.projectSeqId = projectSeqId;
		this.projectName = projectName;
	}
	
	public int getProjectSeqId() 
	{
		return projectSeqId;
	}
	
	public void setProjectSeqId(int projectSeqId) 
	{
		this.projectSeqId = projectSeqId;
	}
	
	public String getProjectName() 
	{
		return projectName;
	}
	
	public void setProjectName(String projectName) 
	{
		this.projectName = projectName;
	}
	

}
