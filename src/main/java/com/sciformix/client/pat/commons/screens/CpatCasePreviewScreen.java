package com.sciformix.client.pat.commons.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import org.apache.commons.collections4.map.ListOrderedMap;
import org.icepdf.ri.common.ComponentKeyBinding;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.ExtractedDataPreviewHelper;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.ParsedDataWithConfiguration;
import com.sciformix.client.pat.commons.PreviewCaseDoc;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;
import com.sciformix.commons.utils.StringUtils;


public class CpatCasePreviewScreen extends CpatBaseHtmlScreen implements ActionListener
{
	
	private static Logger log = LoggerFactory.getLogger(CpatCasePreviewScreen.class);
	
	private static final long serialVersionUID = 3350418882184107366L;
	
	private static JPanel mainPanel = null;
	
	private static JScrollPane summaryScrollPane = null;
	
	private static List<PreviewCaseDoc> listPreviewCaseDoc = null;
	private static String fileType = null;
	
	private static ParsedDataTable summaryDataTable = null;
	
	private static Map<String, String> sceptreMap = null;
	
	public static boolean triggeredOnResultScreen = false; 
	
	private static JFrame viewFrame = null;
	
	public CpatCasePreviewScreen(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, Object> params)
	{
		super(oToolBase, oCallbackAction, oToolBase.getToolDetails().getToolTitle(), 600, 800, null, params);
	}
	
	
	@SuppressWarnings({ "unchecked", "unchecked" })
	@Override
	protected Component prepareContentFrame(String p_sPageToLoad, Map<String, Object> params) {
		// TODO Auto-generated method stub
		
		log.info("Preview Screen: Rendering");
		
		viewFrame = oWindowFrame;
		
		List<String> fileNameList = null,
					followUpFields = null;
		List<ParsedDataTable> listParsedDataTable = null;
		
		Map<String, Object> collatedMap = null;
		Map<String, List<ParsedDataWithConfiguration>> keyValueMap = null;		
		String collatedTable = null;

		
		JPanel headerPanel = null, footerPanel = null;
		JPanel bodyPanel = null;
		
		JLabel viewLabel = null;
		JComboBox viewListCombo = null;
		
		JButton continueButton = null, abortButton = null;
		
		// Params
		listPreviewCaseDoc = new ArrayList<>();
		fileNameList = new ArrayList<>();
		listParsedDataTable = new ArrayList<>();
		keyValueMap = new HashMap<>();
		collatedMap = new HashMap<>();
		
		listPreviewCaseDoc = (List<PreviewCaseDoc>) params.get(CpatToolConstants.ContentParams.CASE_DOC_LIST);
		summaryDataTable = (ParsedDataTable) params.get(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE);
		fileType = (String) params.get(CpatToolConstants.ContentParams.CASE_FILE_TYPE);
		sceptreMap = (Map<String, String>) params.get(CpatToolConstants.ContentParams.SCEPTRE_MAP);
		followUpFields = (List<String>) params.get("followUpFieldList");

		// First View will be Summary
		fileNameList.add("Summary");
		
		for(PreviewCaseDoc doc: listPreviewCaseDoc)
		{
			fileNameList.add(doc.getPdfFile().getName());
			listParsedDataTable.add(doc.getDataTable());
		}
		
		// Add Summary DataTable to the end
		listParsedDataTable.add(summaryDataTable);
		
		keyValueMap = collateDataForMultipleDataTable(listParsedDataTable, fileType);
		collatedMap = showCollatedData(fileNameList, keyValueMap,followUpFields,summaryDataTable, fileType);
				
		collatedTable = (String) collatedMap.get("collatedData");
		
		bodyPanel = createHtmlContentViewPaneForSummary(collatedTable);
		summaryScrollPane = new JScrollPane(bodyPanel);
		
		// Header Panel
		headerPanel = new  JPanel();
		
		viewLabel = new JLabel("View");
		
		viewListCombo = new JComboBox(fileNameList.toArray());
		viewListCombo.addActionListener(this);

		headerPanel.add(viewLabel);
		headerPanel.add(viewListCombo);
		
		
		// Footer Panel
		footerPanel = new JPanel();
		
		continueButton = new JButton(CpatToolConstants.UI.PREVIEW_BUTTON_CONTINUE);
		abortButton = new JButton(CpatToolConstants.UI.PREVIEW_BUTTON_ABORT);
		
		footerPanel.add(continueButton);
		footerPanel.add(abortButton);			

		
		// Content Panel
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(headerPanel, BorderLayout.NORTH);
		mainPanel.add(summaryScrollPane, BorderLayout.CENTER);
		mainPanel.add(footerPanel, BorderLayout.SOUTH);

		mainPanel.setBackground(Color.BLUE);
		
		return mainPanel;
	}
	
	@Override
	protected void addActionListener(Component comp, JFrame oWindowFrame) 
	{
		log.info("Preview Screen: Add Action Listeners");
		oToolBase.previewScreenEventHandler(oToolBase, m_oCallbackAction, comp, oWindowFrame, summaryDataTable, sceptreMap);		
	}
	
	public Map<String, List<ParsedDataWithConfiguration>> collateDataForMultipleDataTable(List<ParsedDataTable> m_DdataTableList,String fileType)
	{
		log.info("collateDataForMultipleDataTable");
		
		String displayName = null;
		String nDisplayName = null;
		List<ParsedDataWithConfiguration> valueList = null;
		String subGroup = null;
		FileParsingConfiguration oParsingConfiguration = null;
		FileParsingField field = null;
		if(fileType.equalsIgnoreCase(CpatToolConstants.FileType.E2B) || fileType.equalsIgnoreCase(CpatToolConstants.FileType.SCEPTRE))
		{
			oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AA_KEY);
		}
		else if(fileType.equalsIgnoreCase(CpatToolConstants.FileType.HEALTH_CANADA))
		{
			oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_BA_KEY);
			
		}else if(fileType.equalsIgnoreCase(CpatToolConstants.FileType.MESA))
		{
			oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AB_KEY);
		}
		
		Map<String, List<ParsedDataWithConfiguration>> keyMap = new ListOrderedMap<String, List<ParsedDataWithConfiguration>>();
		for(String mnemonic : oParsingConfiguration.getFieldMap().keySet())
		{
			field = oParsingConfiguration.getField(mnemonic);
			subGroup = oParsingConfiguration.getField(mnemonic).getM_sFieldSubGroup();
			displayName = oParsingConfiguration.getField(mnemonic).getFieldLabel().toString();

			int maxCount = 1;
			int keyCount = 1;
			if(!oParsingConfiguration.getField(mnemonic).isMultivalued())
			{
				for(ParsedDataTable oDataTable: m_DdataTableList)
				{
					if(oDataTable.isColumnPresent(mnemonic+"_Count"))
					{
						keyCount =Integer.parseInt(oDataTable.getColumnValues(mnemonic+"_Count").get(0).displayValue().toString());
						if(maxCount < keyCount)
						{
							maxCount = keyCount;
						}
					}
					else
					{
						//multivalued but count field is not present
					}
				}
				nDisplayName = displayName;
				for(int index =1; index<= maxCount; index++)
					{
						 
						valueList = new ArrayList<ParsedDataWithConfiguration>();
						if(maxCount==1)
						{
						}
						else
						{
						displayName = nDisplayName+" "+index;
						}
						for(ParsedDataTable oDataTable: m_DdataTableList)
						{
							if(oDataTable.isColumnPresent(mnemonic+"_"+index))
							{
								valueList.add(new ParsedDataWithConfiguration(oDataTable.getColumnValues(mnemonic+"_"+index).get(0),field));
								
							}
							else
							{
								if(index == 1)
								{
									if(oDataTable.isColumnPresent(mnemonic))
									{
										valueList.add(new ParsedDataWithConfiguration(oDataTable.getColumnValues(mnemonic).get(0),field));
										
									}	
									else
									{
										//empty
										valueList.add(new ParsedDataWithConfiguration(new StringParsedDataValue(StringConstants.EMPTY, StringConstants.EMPTY, DataState.ASIS, ValidationState.UNVALIDATED),field));
									}
										
								}
								else
								{
									//empty
									valueList.add(new ParsedDataWithConfiguration(new StringParsedDataValue(StringConstants.EMPTY, StringConstants.EMPTY, DataState.ASIS, ValidationState.UNVALIDATED),field));
								}
								//multivalued but count field is not present
							}
						}
						keyMap.put(subGroup+"##"+displayName, valueList);
					}
				//}
			}
			else
			{
				valueList = new ArrayList<ParsedDataWithConfiguration>();
				for(ParsedDataTable oDataTable: m_DdataTableList)
				{
					if(oDataTable.isColumnPresent(mnemonic))
					{
						valueList.add(new ParsedDataWithConfiguration(oDataTable.getColumnValues(mnemonic).get(0),field));
					}
					else
					{
						//empty
						valueList.add(new ParsedDataWithConfiguration(new StringParsedDataValue(StringConstants.EMPTY, StringConstants.EMPTY, DataState.ASIS, ValidationState.UNVALIDATED),field));
					}
						
				}
				keyMap.put(subGroup+"##"+displayName, valueList);
			}
			
			
		}
		//addingFileNames to keyMap
		List<ParsedDataWithConfiguration> fileNameList = new ArrayList<ParsedDataWithConfiguration>();
		List<ParsedDataWithConfiguration> fileTypeList = new ArrayList<ParsedDataWithConfiguration>();
		for(ParsedDataTable oDataTable: m_DdataTableList)
		{
			if(oDataTable.isColumnPresent("FileName"))
			{
				fileNameList.add(new ParsedDataWithConfiguration(oDataTable.getColumnValues("FileName").get(0), null));
			}
			else
			{
				//empty
				fileNameList.add(new ParsedDataWithConfiguration(new StringParsedDataValue(StringConstants.EMPTY, StringConstants.EMPTY, DataState.ASIS, ValidationState.UNVALIDATED),null));
			}
			
			if(oDataTable.isColumnPresent("fileType"))
			{
				fileTypeList.add(new ParsedDataWithConfiguration(oDataTable.getColumnValues("fileType").get(0), null));
			}
			else
			{
				fileTypeList.add(new ParsedDataWithConfiguration(new StringParsedDataValue(StringConstants.EMPTY, StringConstants.EMPTY, DataState.ASIS, ValidationState.UNVALIDATED),null));
			}
				
		}
		keyMap.put("FileName", fileNameList);
		keyMap.put("fileType", fileTypeList);
		
		return keyMap;
	}

	public Map<String, Object> showCollatedData(List<String> fileNameList,Map<String, List<ParsedDataWithConfiguration>> keyValueMap, List<String> followUpFields, ParsedDataTable summaryParsedData, String fileType)
	{
		log.info("showCollatedData");
		
		List<String> mandatoryKeyList = null;
		List<String> collatedValuesList = null;
		Map<String, Object> map = new HashMap<String,Object>();
		String fileHeaderString = null;
		String modifiedFileName = null;
		String subGroup = null;
		String displayName = null;
		FileParsingConfiguration oParsingConfiguration = null;
		
		String[] keyArray = null;
		String subGroupHeader = null;
		String group = null;
		StringBuffer text = new StringBuffer();
		
		if(fileType.equalsIgnoreCase("E2B") || fileType.equalsIgnoreCase("SCEPTRE"))
		{
			oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AA_KEY);
		}
		else if(fileType.equalsIgnoreCase("Health Canada"))
		{
			oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_BA_KEY);
			
		}else if(fileType.equalsIgnoreCase("MESA"))
		{
			oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AB_KEY);
		}
			fileHeaderString = "";
			for(int index=0; index<fileNameList.size(); index++)
			{
				modifiedFileName = fileNameList.get(index);
				if(modifiedFileName.contains("file_"))
				{
					modifiedFileName = modifiedFileName.substring(modifiedFileName.indexOf("file_")+5);
				}
				if(modifiedFileName.contains("_"))
				{
					modifiedFileName = modifiedFileName.substring(modifiedFileName.indexOf("_")+1);
				}
				
				if(!(index == fileNameList.size()-1 ))
				{
				fileHeaderString = fileHeaderString+ modifiedFileName+",";
				}
				else
				{
					fileHeaderString = fileHeaderString+ modifiedFileName;
				}
			}
			
			text.append("<html><body><table bgcolor='#ebebe0'>");
		
			//implement logic for the mandatory fields
			//mandatoryKeyList = createTableForMandatoryFieldsInCollatedTable(fileNameList, keyValueMap, text);
			text.append("<tr align='center'>");
			
			collatedValuesList = new ArrayList<>();	
			for(Map.Entry<String,List<ParsedDataWithConfiguration>> entry : keyValueMap.entrySet()) // get collated values List
			{
				List<ParsedDataWithConfiguration> valueList = null;
				valueList = entry.getValue();
				collatedValuesList.add((String) valueList.get(valueList.size()-1).getParsedDataValue().displayValue());
			}
			for(Map.Entry<String,List<ParsedDataWithConfiguration>> entry : keyValueMap.entrySet()){
				
				if(entry.getKey().equalsIgnoreCase("FileName") || entry.getKey().equalsIgnoreCase("fileType"))
				{
					
				}
				else
				{
				
					String fieldValue = null;
					
					
					
					if(fileType.equalsIgnoreCase("MESA"))
					{
					if(entry.getKey().contains("_"))
					{
						keyArray = entry.getKey().split("_");
						if(keyArray.length==2)
						{
							group = keyArray[0];
							subGroup = "";
							displayName = keyArray[1];
						}
						else if(keyArray.length==3)
						{
							group = keyArray[0];
							subGroup = keyArray[1];
							displayName = keyArray[2];
						}
						else
						{
							subGroup = "";
							displayName = entry.getKey();
						}
					}
					

					text.append("<td bgcolor='#A19F9B'>"+group+"</td>");
					text.append("<td bgcolor='#A19F9B'>"+subGroup+"</td>");
					text.append("<td bgcolor='#A19F9B'>"+displayName+"</td>");
					
					}
					else
					{
					if(entry.getKey().contains("##"))
					{
						keyArray = entry.getKey().split("##");
						if(keyArray.length==2)
						{
							subGroup = keyArray[0];
							displayName = keyArray[1];
						}
						else if(keyArray.length==3)
							{
								group = keyArray[0];
								subGroup = keyArray[1];
								displayName = keyArray[2];
							}
						else
						{
							subGroup = "";
							displayName = entry.getKey();
						}
					}
					
					if(subGroup.equalsIgnoreCase(subGroupHeader))
					{
						if(text.toString().endsWith("</table>"))
								{
							text.replace(text.length()-9, text.length()-1, "");
					}
					}
					else
					{
						subGroupHeader =subGroup;
					
						text.append("<tr align='center'><td><b><u><font size='4'>"+subGroup+"</font></u></b></td></tr>");
						text.append("<tr align='center'><td><table border='1' bgcolor='#d6d6c2'>");
						text.append("<tr bgcolor='#A19F9B'>");
						text.append("<td width='150'>Field Name</td>");
					
					    for(int fileIndex=0; fileIndex<fileNameList.size();fileIndex++)
					    {
					    	if(fileNameList.get(fileIndex).equalsIgnoreCase("Summary"))
					    		continue;
					    	text.append("<td width='200' style='word-wrap:word-break;'>"+fileNameList.get(fileIndex)+"</td>");
					    }
					
					    //header for collated data
					    text.append("<td>Collated Data</td>");
					    text.append("</tr>");
                    
					}

					text.append("<tr>");
					text.append("<td width='150' bgcolor='#A19F9B'>"+displayName+"</td>");
                
					}
					String collatedValue = null;
					String mnemonic = null;
					List<ParsedDataWithConfiguration> valueList = null;
					boolean valuePresent = false;
					String displayValueForInnerLoop = null, colletedValueHighLite = null;
					valueList = entry.getValue();
					
					for (int index=0; index< valueList.size(); index++) 
					{
					valuePresent = false;
					collatedValue =(String) valueList.get(valueList.size()-1).getParsedDataValue().displayValue();
						
						//trimming dots(..) which are creating problem for display
					collatedValue =collatedValue.replace("..", "");
					
					fieldValue = (String) valueList.get(index).getParsedDataValue().displayValue();
					colletedValueHighLite = (String) valueList.get(valueList.size()-1).getParsedDataValue().displayValue();
					mnemonic = valueList.get(index).getField().getMnemonic();
						
						//trimming dots(..) which are creating problem for display
					fieldValue =fieldValue.replace("..", "");
					
					//to highlight the field if value is taken in collated data from this
					if(!collatedValue.equalsIgnoreCase("") && collatedValue.equalsIgnoreCase(fieldValue) && index!=valueList.size()-1)
							{
						for(int innerIndex=index+1;innerIndex<(valueList.size()-1); innerIndex++)
						{
							displayValueForInnerLoop = (String) valueList.get(innerIndex).getParsedDataValue().displayValue();
							displayValueForInnerLoop = displayValueForInnerLoop.replace("..", "");
							if(collatedValue.equalsIgnoreCase(displayValueForInnerLoop))
							{
								valuePresent = true;
							}
						}
						//ignoring this value as same value present in next dataTable
						if(valuePresent)
						{
							text.append("<td style='word-wrap:word-break;' width='200'>"+fieldValue+"</td>");
							}
							else
						{
							text.append("<td style='word-wrap:word-break;' width='200' bgcolor='#ffff80'>"+fieldValue+"</td>");
						}
					}
					else
					{
						//System.out.println("FieldValue : "+fieldValue+"CollValue : "+colletedValueHighLite);
						if (!oParsingConfiguration.getField(mnemonic).isMultivalued()) 
						{  
							boolean multiValuHighlightFlag = false;
							int keyCount = 1, maxCount = 1;
							if(summaryParsedData.isColumnPresent(mnemonic+"_Count"))
							{
								keyCount =Integer.parseInt(summaryParsedData.getColumnValues(mnemonic+"_Count").get(0).displayValue().toString());
								if(maxCount < keyCount)
								{
									maxCount = keyCount;
								}
							}
							if(collatedValuesList != null && followUpFields != null)
							{
								for(int count =1; count<= maxCount; count++)
								{
									
									if(followUpFields.contains(mnemonic+"_"+count)&&collatedValuesList.contains(fieldValue)
											&&(fieldValue.equals(colletedValueHighLite)) &&!StringUtils.isNullOrEmpty(fieldValue))
									{
										text.append("<td style='word-wrap:word-break;' width='200' bgcolor='#ffb380'>"+fieldValue+"</td>");
										multiValuHighlightFlag = true;
										break;
									}
								}
							}
							if(!multiValuHighlightFlag)
							{
								text.append("<td style='word-wrap:word-break;' width='200'>"+fieldValue+"</td>");
							}
						}
						else {
							
							if(collatedValuesList != null && followUpFields != null)
							{
								if(followUpFields.contains(mnemonic)&&collatedValuesList.contains(fieldValue)
										&&(fieldValue.equals(colletedValueHighLite))&&!StringUtils.isNullOrEmpty(fieldValue))
								{
									text.append("<td style='word-wrap:word-break;' width='200' bgcolor='#ffb380'>"+fieldValue+"</td>");
								}
								else
								{
									text.append("<td style='word-wrap:word-break;' width='200'>"+fieldValue+"</td>");
								}
							}else
							{
								text.append("<td style='word-wrap:word-break;' width='200'>"+fieldValue+"</td>");
							}	
							
						}
					}
				}
				 text.append("</tr>");
				 text.append("</table>");
				}
			}
			text.append("</tr></table></body></html>");
			
			map.put("collatedData", text.toString());
			//map.put("mandatoryKeyList", mandatoryKeyList);
			
			return map;
			
	}
	
	public List<String> createTableForMandatoryFieldsInCollatedTable(List<String> fileNameList,Map<String, List<ParsedDataWithConfiguration>> keyValueMap, StringBuffer text)
	{
		log.info("createTableForMandatoryFieldsInCollatedTable");
		
		List<String> mandatoryKeyList  = new ArrayList<String>();
		String mandatoryKeyWithTab = null;
		String subGroup = null;
		Map<String, String> tabFieldMap = null;
		String displayName = null;
		boolean mandatory = false;
		String[] keyArray = null;
		String group = null;
		String fileType = null;
		String concatedFieldList = null;
		String tabName = null;
		fileType =(String) keyValueMap.get("fileType").get(0).getParsedDataValue().displayValue();
		text.append("<tr align='center'><td><b><u><font size='4'>Missing Mandatory Fields</font></u></b></td></tr>");
		text.append("<tr align='center'><td><table border='1' bgcolor='#d6d6c2'>");
		text.append("<tr bgcolor='#A19F9B'>");
		text.append("<td width='150'>Tab</td><td width='150'>Field Name</td>");
	
	    //header for collated data
	 //   text.append("<td>Collated Data</td>");
	    text.append("</tr>");
	    
	    tabFieldMap = new HashMap<String,String>();
	    
		for(Map.Entry<String,List<ParsedDataWithConfiguration>> entry : keyValueMap.entrySet())
		{
			FileParsingField fileParsingField= entry.getValue().get(0).getField();
			String collatedValue = null;
			List<ParsedDataWithConfiguration> valueList = null;
			valueList = entry.getValue();
			collatedValue =(String) valueList.get(valueList.size()-1).getParsedDataValue().displayValue();
			//trimming for junk values which are creating problem for display
			collatedValue =collatedValue.replace("..", "");
			
			if(fileParsingField != null)
			{
				mandatory = fileParsingField.isDEMandatory();
				tabName = fileParsingField.getDEGroup();
			}
			if(mandatory && collatedValue.equals(""))
			{
				if(entry.getKey().equalsIgnoreCase("FileName") || entry.getKey().equalsIgnoreCase("fileType"))
				{
					
				}
				else
				{
				
					if(fileType.equalsIgnoreCase("MESA"))
					{
					if(entry.getKey().contains("_"))
					{
						keyArray = entry.getKey().split("_");
						if(keyArray.length==2)
						{
							group = keyArray[0];
							subGroup = "";
							displayName = keyArray[1];
						}
						else if(keyArray.length==3)
						{
							group = keyArray[0];
							subGroup = keyArray[1];
							displayName = keyArray[2];
						}
						else
						{
							subGroup = "";
							displayName = entry.getKey();
						}
					}
					
		
					text.append("<td bgcolor='#A19F9B'>"+group+"</td>");
					text.append("<td bgcolor='#A19F9B'>"+subGroup+"</td>");
					text.append("<td bgcolor='#A19F9B'>"+displayName+"</td>");
					
					}
					else
					{
						
						
						
					
					if(entry.getKey().contains("##"))
					{
						keyArray = entry.getKey().split("##");
						if(keyArray.length==2)
						{
							subGroup = keyArray[0];
							displayName = keyArray[1];
						}
						else if(keyArray.length==3)
							{
								group = keyArray[0];
								subGroup = keyArray[1];
								displayName = keyArray[2];
							}
						else
						{
							subGroup = "";
							displayName = entry.getKey();
						}
					}
					
					mandatoryKeyWithTab  =tabName+"##"+displayName;
					}
					
					
					if(mandatory && collatedValue.equals(""))
					{
						if(tabFieldMap.containsKey(tabName))
						{
							concatedFieldList = tabFieldMap.get(tabName);
							concatedFieldList = concatedFieldList + ", "+displayName;
						}
						else
						{
							concatedFieldList = displayName;
						}
						tabFieldMap.put(tabName, concatedFieldList);
						
						mandatoryKeyList.add(mandatoryKeyWithTab);
				
					}		
				}
			}
		}
		
		
		if(mandatoryKeyList.size()!=0)
		{
			
			for(Map.Entry<String, String> entry:tabFieldMap.entrySet())
			{
				text.append("<tr>");
				text.append("<td style='word-wrap:word-break;' width='200'>"+entry.getKey()+"</td>");
				text.append("<td width='150' ><font color='red'>"+entry.getValue()+"</font></td>");
				text.append("</tr>");
			}
			
			
			text.append("<tr align='center'><td colspan='3'><font color='red'><b>Above Mandatory Values are missing.</b></font></td></tr>");
		}
		else
		{
			text.append("<tr align='center'><td colspan='3'><font color='green'><b>All Mandatory Values are Present. You can continue</b></font></td></tr>");
		}
		text.append("</table>");
		 text.append("</td></tr>");
		 
		 return mandatoryKeyList;
	}
	
	public JPanel createHtmlContentViewPaneForSummary(String htmlTable)
	{
		log.info("createHtmlContentViewPaneForSummary");
		
		JPanel panel = null;
		JEditorPane jep = null;
		
		panel = new JPanel();
		jep = new JEditorPane();
		
		jep.setContentType("text/html");
		jep.setText(htmlTable);
		jep.setEditable(false);
		jep.setCaretPosition(0);
		
		panel.add(jep);

		return panel;
	}

	public JSplitPane getVerticalSplitPaneForIndividualSD(ParsedDataTable oDataTable, String pdfPath, String fileType) throws IOException
	{
		
		JSplitPane horizontalSplitPane = null;
		JScrollPane rightSectionScrollPane = null;
		JTabbedPane jTabPane = null;
		
		// Right Section (HTML)
		jTabPane = createHtmlContentViewPane(oDataTable,fileType,pdfPath);
		rightSectionScrollPane = new JScrollPane(jTabPane);
		
		horizontalSplitPane = new JSplitPane();
		horizontalSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
		horizontalSplitPane.setLeftComponent(showPdf(pdfPath));
		horizontalSplitPane.setRightComponent(rightSectionScrollPane);
			
		return horizontalSplitPane;
		
	}
	
	public JTabbedPane createHtmlContentViewPane(ParsedDataTable oDataTable,String fileType, String filePath) throws IOException
	{
		JTabbedPane jTabPane = null;
		JEditorPane jep = null;
		
		jTabPane = new JTabbedPane();
		jep = new JEditorPane();
		jep.setContentType("text/html");
		jep.setText(getData(oDataTable,null,fileType, filePath));
		jep.setCaretPosition(0);
		
		jTabPane.add("Summary", jep);
			
		return jTabPane;

	}
	
	public String getData(ParsedDataTable oDataTable, Map<String,String> propMap, String fileType, String filePath) throws IOException
	{
		log.info("getData");
		return ExtractedDataPreviewHelper.settingPDFData(oDataTable, propMap, fileType, filePath);
	}
	
	private JPanel showPdf(String pdfPath) 
	{
		log.info("showPdf");
		
		SwingController controller = new SwingController();
		
		SwingViewBuilder factory = new SwingViewBuilder(controller);
		
		

		JPanel viewerComponentPanel = factory.buildViewerPanel();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		viewerComponentPanel.setMaximumSize(new Dimension(screen.width / 2, screen.height / 2));
		viewerComponentPanel.setMinimumSize(new Dimension(screen.width / 2, screen.height / 2));
		
		ComponentKeyBinding.install(controller, viewerComponentPanel);

		// add interactive mouse link annotation support via callback
		
		controller.getDocumentViewController().setAnnotationCallback(
		      new org.icepdf.ri.common.MyAnnotationCallback(
		             controller.getDocumentViewController()));
	
		//controller.openDocument(m_sPreviewPdfFilePath);
		controller.openDocument(pdfPath);
		
		return viewerComponentPanel;
	}


	@Override
	public void actionPerformed(ActionEvent e) 
	{
		log.info("Preview Screen: Action Performed");
		String viewName = null;
		JComboBox viewCombo = null;
		
		String filePath = null;
		File file = null;
		ParsedDataTable oDataTable = null;
		
		JSplitPane caseDocSplitPane = null;
		JScrollPane scrollPane = null;
		
		
		viewCombo = (JComboBox) e.getSource();
		viewName = (String) viewCombo.getSelectedItem();
		
		if(viewName.equalsIgnoreCase("Summary"))
		{
			for(Component comp: mainPanel.getComponents())
			{
				if(comp instanceof JSplitPane)
				{
					mainPanel.remove(comp);
					mainPanel.add(summaryScrollPane, BorderLayout.CENTER);
					
					mainPanel.revalidate();
					mainPanel.repaint();
				}
			}
			
		}
		else
		{
			
			for(PreviewCaseDoc doc: listPreviewCaseDoc)
			{
				file = doc.getPdfFile();
				
				if(file.getName().equals(viewName))
				{
					oDataTable = doc.getDataTable();
					filePath = file.getAbsolutePath();
					
					break;
				}
			}
			
			try {
				caseDocSplitPane = getVerticalSplitPaneForIndividualSD(oDataTable, filePath, fileType);

				for(Component comp: mainPanel.getComponents())
				{
					if(comp instanceof JScrollPane || comp instanceof JSplitPane)
					{
						mainPanel.remove(comp);
						mainPanel.add(caseDocSplitPane, BorderLayout.CENTER);
						
						mainPanel.revalidate();
						mainPanel.repaint();
					}
				}
				
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, "Could not render document view");
				log.error("Document View Rendering failed:", null, e);
			}
			
			
		}
		
	}
	
	public static void displayPreviewScreenView()
	{
		BorderLayout layout  = null;
		if(viewFrame != null)
		{
			layout = (BorderLayout) mainPanel.getLayout();
			
			mainPanel.remove(layout.getLayoutComponent(BorderLayout.SOUTH));
			
			triggeredOnResultScreen = true;
			viewFrame.setVisible(true);
		}
	}
	
	public static void disposePreviewScreenView()
	{
		if(viewFrame!=null)
		{
			viewFrame.dispose();			
		}
	}
}

