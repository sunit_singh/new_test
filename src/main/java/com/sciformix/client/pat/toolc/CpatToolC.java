package com.sciformix.client.pat.toolc;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.FormSubmitEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.DuplicateRecordFields;
import com.sciformix.client.automation.irt.FieldDetails;
import com.sciformix.client.automation.irt.IRTDuplicateSearchCriteria;
import com.sciformix.client.automation.irt.IRTE2BFieldsWriterMapper;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.PreviewCaseDoc;
import com.sciformix.client.pat.commons.ToolDetails;
import com.sciformix.client.pat.commons.screens.CpatCasePreviewScreen;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.DataValidationUtils;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;


public class CpatToolC extends CpatToolBase implements IAction{

	private static final String SCEPTRE_FILE_TYPE = "SCEPTRE";
	private static Logger log = LoggerFactory.getLogger(CpatToolC.class);
	private static Map<String, FieldDetails> l_writere2bfieldmapping;
	private static Map<String, Boolean> searchCriteriaMap;
	private static String VERIFICATION_MODE = "verification";
	private static boolean isInitial = false , isVerificationMode = false ,
			isDataEntryEnable = false;
	
	
	public static void main(String[] args) {
		
		log.info("Starting CPAT Tool C");
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		FileConfigurationHome.init(oStartupLogger, parserConfiguration);
		
		IRTE2BFieldsWriterMapper l_mapper =	IRTE2BFieldsWriterMapper.getInstance(
				CpatToolC.class.getClassLoader().getResourceAsStream("IRTWriterE2BMapper.txt"));
		l_writere2bfieldmapping	= l_mapper.getWriterE2BFieldMapping();
		
		IRTDuplicateSearchCriteria l_dupsearchcriteria = IRTDuplicateSearchCriteria.getInstance(
				CpatToolC.class.getClassLoader().getResourceAsStream("DuplicateSearchCriteria.txt"));
		searchCriteriaMap = l_dupsearchcriteria.getDuplicateSearchCriteriaMap();
		ToolDetails	l_tooldetails = new ToolDetails();
		l_tooldetails.setToolName(CpatToolCConstants.Tool.TOOL_NAME);
		l_tooldetails.setToolDescription(CpatToolCConstants.Tool.TOOL_DESCRIPTION);
		l_tooldetails.setToolDisplayName(CpatToolCConstants.Tool.TOOL_DISPLAY_NAME);
		l_tooldetails.setToolHeading(CpatToolCConstants.Tool.TOOL_HEADING);
		l_tooldetails.setToolTitle(CpatToolCConstants.Tool.TOOL_TITLE);
		l_tooldetails.setToolUserInputScreen(CpatToolCConstants.Tool.IRT_USER_INPUTS_SCREEN);
		l_tooldetails.setToolVersion(CpatToolCConstants.Tool.TOOL_VERSION);
		
		CpatToolC oTool = new CpatToolC(l_tooldetails);
		log.info("ToolName : " + oTool.getToolDetails().getToolName() + " ToolVersion : " + oTool.getToolDetails().getToolVersion());
		
		if(args.length > 0) {
			if(args[0].equals(VERIFICATION_MODE)) {
				isVerificationMode = true;
			} else {
				testModePropMap = CpatToolHome.loadExternalProperties(args[0]);
				isTestMode = true;
			} 
		} 
		
		log.info("Test Mode: " + isTestMode);
		log.info("Verification Mode: " + isVerificationMode);
		
		oTool.render(CPAT_ACTION.INITIAL);
	}
	
	public CpatToolC(ToolDetails p_tooldetails)
	{
		super(p_tooldetails);
	}
	
	// Render Screen Tool C
	private void render(CPAT_ACTION p_eAction)
	{
		performAction(p_eAction);
	}
	
	@SuppressWarnings("unchecked")
	public void performAction(CPAT_ACTION p_eAction, Object... oParameters) 
	{
		Map<String, String> payload = new HashMap<String, String>();
		Map<String, Object> params = null;
		params = new HashMap<>();

		try{
			
			if (p_eAction == CPAT_ACTION.INITIAL) 
			{
				log.info("Login");
				CpatToolHome.showLoginScreen(this, this);
			}
			
			else if (p_eAction == CPAT_ACTION.LOGIN_SUCCESSFUL) 
			{
				log.info("Login Successful");
				performAction(CPAT_ACTION.COLLECT_USER_INPUTS, oParameters);
	
			} 
			
			else if (p_eAction == CPAT_ACTION.COLLECT_USER_INPUTS) 
			{
				log.info("Collect User Inputs");
				CpatToolHome.showUserInputsScreen(this, this);
			} 
			
			else if(p_eAction == CPAT_ACTION.USER_INPUTS_COLLECTED)
			{
				performAction(CPAT_ACTION.DOWNLOAD_SD, oParameters);
			}
			
			else if (p_eAction == CPAT_ACTION.DOWNLOAD_SD) 
			{
				log.info("Download SD");
				log.info("User Input Parameters received: " + userInputsParams);
				CpatToolCHelper.searchCaseAndDownloadCasePdf(this, this, 
					userInputsParams.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO), 
					this.userInputsParams,isVerificationMode);
	
			}
			else if (p_eAction == CPAT_ACTION.PREVIEW) 
			{
				log.info("Preview");
				
				List<PreviewCaseDoc> listPreviewCaseDocs =  (List<PreviewCaseDoc>) oParameters[0];
				ParsedDataTable oSummaryDataTable = null;
				
				oSummaryDataTable	=	(ParsedDataTable) oParameters[1];
				
				params = new HashMap<>();
				
				params.put(CpatToolConstants.ContentParams.CASE_DOC_LIST, listPreviewCaseDocs);
				params.put(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE, oSummaryDataTable);
				params.put(CpatToolConstants.ContentParams.CASE_FILE_TYPE, SCEPTRE_FILE_TYPE);
	
				CpatToolHome.showPreviewScreen(this, this, params);
			}
			else if(p_eAction == CPAT_ACTION.DUPLICATE_SEARCH)
			{
				log.info("Duplicate Search");
				ParsedDataTable oSummaryDataTable = (ParsedDataTable) oParameters[0];
				CpatToolCHelper.performDuplicateSearch(this, this, oSummaryDataTable, 
					this.userInputsParams, searchCriteriaMap);
			}
			else if(p_eAction == CPAT_ACTION.DUPLICATE_SUMMARY)
			{
				log.info("Duplicate Summary Screen");
				Map<String, List<DuplicateRecordFields>> irtDuplicateRecMap = 
						(Map<String, List<DuplicateRecordFields>>) oParameters[0];
				DuplicateRecordFields recFild = (DuplicateRecordFields) oParameters[1];
				ParsedDataTable oSummaryDataTable = (ParsedDataTable) oParameters[2];
				
				params = new HashMap<>();
				
				params.put(CpatToolConstants.UI.DUPLIACTE_REC_MAP, irtDuplicateRecMap);
				params.put(CpatToolConstants.ContentParams.DUPLICATE_RECORD, recFild);
				params.put(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE, oSummaryDataTable);
				
				if(CpatToolCHelper.propMap.get(CpatToolCConstants.KEY_ENABLE_DATAENTRY).trim().
						equalsIgnoreCase(CpatToolConstants.VALUE_TRUE)){
					isDataEntryEnable = true;
				}
				params.put(CpatToolConstants.ContentParams.ENABLE_DATA_ENTRY, isDataEntryEnable);
				
				CpatToolCHelper.showDuplicateRecordSummaryScreen(this, this, params);
			}
			else if(p_eAction == CPAT_ACTION.CASECLASSIFICATION)
			{
				log.info("Case Classification");
				boolean continueFlag = true;
					
				String caseType = (String) oParameters[0];
				ParsedDataTable oSummaryDataTable = (ParsedDataTable) oParameters[1];
				String aerNo = (String) oParameters[2];
				String versionType = (String) oParameters[3];
				Map<String, String> l_dataentrymap	=	new HashMap<>();
				
				if (!isInitial) {
					CpatToolCHelper.initializeSceptreMap(aerNo); 
				}
				
				for (String l_writerkey : l_writere2bfieldmapping.keySet()) {
					FieldDetails	l_fielddetails	=	l_writere2bfieldmapping.get(l_writerkey);
					try
					{
						if (isInitial) {
							l_fielddetails.getFieldBusinessLogicClass().applyBusinessLogicInitial(
									l_dataentrymap
									, 	oSummaryDataTable
									, 	l_writerkey
									, 	l_fielddetails.getE2bKeyName()
									);
						} else {
							l_fielddetails.getFieldBusinessLogicClass().applyBusinessLogicFollowUp(
									l_dataentrymap
									, 	oSummaryDataTable
									, 	l_writerkey
									, 	l_fielddetails.getE2bKeyName()
									);
						}
						
					}
					catch(Exception e)
					{
						log.error("Error: ", e);
						if((!StringUtils.isNullOrEmpty(e.getMessage())) && (e.getMessage().equalsIgnoreCase(CpatToolCErrorMessages.DataEntryFailure.SERIOUS)
								|| e.getMessage().equalsIgnoreCase(CpatToolCErrorMessages.DataEntryFailure.PATIENT_ID)))
						{
							String[] buttons = {CpatToolConstants.OPTION_YES, CpatToolConstants.OPTION_NO};
							int rc = JOptionPane.showOptionDialog(null, e.getMessage() + ". Do you want to continue?", CpatToolConstants.OPTION_DIALOG_TITLE,
									JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[1]);
							
							
							if(rc == 1)
							{
								if(e.getMessage().equalsIgnoreCase(CpatToolCErrorMessages.DataEntryFailure.SERIOUS))
								{
									payload.put("USER_MESSAGE", "User opted for 'No' on Serious check");
								}
								else
								{
									payload.put("USER_MESSAGE", "User opted for 'No' on Patient ID check");
								}
								
								CpatToolHome.callCommonRegisterActivity(this, this, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , this.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
								CpatToolCHelper.quitWebDriver();
								performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
								
								continueFlag = false;
								break;
							}
							else
							{
								if(e.getMessage().equalsIgnoreCase(CpatToolCErrorMessages.DataEntryFailure.SERIOUS))
								{
									payload.put("USER_MESSAGE", "User opted for 'Yes' on Serious check");
								}
								else
								{
									payload.put("USER_MESSAGE", "User opted for 'Yes' on Patient ID check");
								}
								
								CpatToolHome.callCommonRegisterActivity(this, this, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , this.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
							}
						}
						else
						{
							JOptionPane.showMessageDialog(null,CpatToolCErrorMessages.GENERIC_ERROR);
							payload.put("USER_MESSAGE", "Case Classification Failed");
							CpatToolHome.callCommonRegisterActivity(this, this, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , this.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
							CpatToolCHelper.quitWebDriver();
							performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
							
							continueFlag = false;
							break;
						}
					}
				}
				
				if(continueFlag)
				{
					if(CpatToolCHelper.propMap.get(CpatToolCConstants.KEY_FEED_CLASSIFICATION_TAB).trim().equalsIgnoreCase(CpatToolConstants.VALUE_TRUE)){
						CpatToolCHelper.performCaseClassification(this, this, caseType, oSummaryDataTable, this.userInputsParams,aerNo,versionType);
					}
					performAction(CPAT_ACTION.DATA_ENTRY,oSummaryDataTable, aerNo, l_dataentrymap);						
				}
					
			}
			else if (p_eAction == CPAT_ACTION.DATA_ENTRY) 
			{
				log.info("Download SD");
				log.info("User Input Parameters received: " + userInputsParams);
				Map<String, String> l_dataentrymap	=	null;
				ParsedDataTable oSummaryDataTable =	null;
					String aerNo = null;
				
				oSummaryDataTable	=	(ParsedDataTable) oParameters[0];
				aerNo	=	(String) oParameters[1];
				l_dataentrymap	=	(Map<String, String>) oParameters[2];
				CpatToolCHelper.dataEntry(this, this, oSummaryDataTable, l_dataentrymap, aerNo);
			}
			else if (p_eAction == CPAT_ACTION.RESULT_SUMMARY) 
			{
				Map<String, Object> resultSummary =  (Map<String, Object> )oParameters[0];
				isInitial = false;
				CpatToolCHelper.showResultScreen(this, this, resultSummary);
			}
		} catch (Exception e){
			log.error("Error: ", e);
			JOptionPane.showMessageDialog(null, CpatToolCErrorMessages.GENERIC_ERROR);
			CpatToolCHelper.quitWebDriver();
			performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
		}	
	}

	@Override
	protected void loadCpatFieldMap() 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public List<String> validateUserInputs(Map<String, String> p_extractedparametersmap) 
	{
		log.info("Validate User Inputs");
		List<String> 	l_listerrors 			=	null;
		String 			l_nonmedicinalprodpath	=	null,
						l_filename				=	null,
						l_fileextension			=	null,
						l_username				=	null,
						l_receiptno				=	null;
		File			l_file					=	null;
		DataTable 		l_productlist			=	null;
		String casetype_prefix 					=	null;
		
		/**
		 * Things to validate
		 * 1.	Username cannot be blank
		 * 2.	Receipt No cannot be blank.
		 * 3.	It should start with "SFE"
		 * 4.	The length of the Receipt Number should always be 10.
		 * 5.	Non-Medicinal Product File should not be blank.
		 * 6.	The size should not be zero.
		 * 7.	The column name should be there.
		 */
		l_username	=	p_extractedparametersmap.get(CpatToolCConstants.UserInputs.IRT_USERNAME);
		l_receiptno	=	p_extractedparametersmap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO);
		l_nonmedicinalprodpath	=	p_extractedparametersmap.get(CpatToolCConstants.UserInputs.IRT_NONMEDICINAL_FILE);
		
		l_listerrors	=	new ArrayList<>();
		if (StringUtils.isNullOrEmpty(l_username)) {
			l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.ERROR_USERNAME_BLANK);
		}
		
		if (StringUtils.isNullOrEmpty(l_receiptno)) {
			l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.ERROR_RECEIPT_BLANK);
		} else {
			if (l_receiptno.length() != 10) {
				l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.ERROR_RECEIPT_LENGTH);
			}
			
			casetype_prefix = CpatToolCHelper.propMap.get(CpatToolCConstants.KEY_DOCTYPE).trim();
			if(casetype_prefix==null){
				l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.ERROR_CASE_TYPE_NOT_CONFIGURED);
				return l_listerrors;
			}
			
			//if (!(l_receiptno.toUpperCase().startsWith(CpatToolCConstants.CONSTANT_SFE))) {
			if (!(l_receiptno.toUpperCase().startsWith(casetype_prefix))) {
				l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.ERROR_RECEIPT_PREFIX_INVALID + casetype_prefix);
			}
			if (!(DataValidationUtils.validateData(l_receiptno, casetype_prefix + CpatToolCConstants.RECEIPTNO_REGEX))) {
				l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.ERROR_RECEIPT_INVALID);
			}
		}
		
		if(StringUtils.isNullOrEmpty(l_nonmedicinalprodpath)) {
			l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.PRODLIST_PATH_ERR);
		} else {
			l_file = new File(l_nonmedicinalprodpath);
			if(l_file.exists()) {
				l_filename = l_file.getName();
				l_fileextension = l_filename.substring(l_filename.lastIndexOf(StringConstants.PERIOD) + 1);
				
				if(!l_fileextension.equalsIgnoreCase(CpatToolConstants.PRODUCTLIST_EXTENSION)) {
					l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.PRODLIST_TYPE_ERR);
				} else {
					l_productlist	=	CpatToolCHelper.getRowDataFromExcel(l_nonmedicinalprodpath);
					for(String column: CpatToolCConstants.PRODUCT_FILE_COLUMN_NAMES) {
						if(!l_productlist.isColumnPresent(column)) {
							l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.PRODLIST_COL_HEADER_ERR + column);
						} else {
							if(l_productlist.getColumnValues(column).size() == 0) {
								l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.PRODLIST_COL_VAL_ERR + column);
							}
						}
					}
				}
			} else {
				l_listerrors.add(CpatToolCErrorMessages.UserInputScreen.PRODLIST_UNAVAIL_ERR);
			}
		}
		return l_listerrors;
	}

	@Override
	public void userInputsScreenEventHandler(final CpatToolBase oToolBase, final IAction oCallbackAction, Component comp, final JFrame frame) 
	{
		JEditorPane jep = null;
		
		jep = (JEditorPane) comp;
		
		jep.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent event) {
				if (event instanceof FormSubmitEvent) {
					CpatToolCHelper.inputsCollectedActionListener(oToolBase, oCallbackAction, frame, (FormSubmitEvent) event);
				} else {
					// TODO:Something here
				}
			}

		});
	}

	@Override
	public void previewScreenEventHandler(final CpatToolBase oToolBase, final IAction oCallbackAction, Component comp,
			final JFrame oWindowFrame, final ParsedDataTable summaryDataTable, Map<String, String> parsedDataMap) 
	{
		// Handle Continue and Abort only
		
		JButton btn = null;
		JPanel panel = (JPanel) comp;
		JButton continueButton = null, abortButton = null;
		
		
		Component[] components = panel.getComponents();
		
		Component[] childComponents = null;
		
		for(int i=0; i<components.length; i++)
		{
			if(components[i] instanceof JPanel)
			{
				childComponents = ((JPanel) components[i]).getComponents();
				
				for(int j=0; j< childComponents.length; j++)
				{
					if( childComponents[j] instanceof JButton)
					{
						btn = (JButton) childComponents[j];
						
						if(btn.getText().equals(CpatToolConstants.UI.PREVIEW_BUTTON_CONTINUE))
						{
							continueButton = btn;
						}
						else
						{
							abortButton = btn;
						}
					}
				}
				
			}
		}
		
		continueButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				oWindowFrame.setVisible(false);
				oCallbackAction.performAction(CPAT_ACTION.DUPLICATE_SEARCH, summaryDataTable);
			}
		});
		
		abortButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Map<String, String> payload = null;
				boolean isSuccess = false;
				
				payload = new HashMap<>();
				payload.put("USER_CHOICE", "ABORT");
				
				isSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, "TOOL-PREVIEW" , oToolBase.activityId, CPAT_ACTION.PREVIEW.toString(), "", payload);
				if(isSuccess)
				{
					oWindowFrame.dispose();
					CpatToolCHelper.quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				
			}
		});
		
		oWindowFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) 
			{
				if(CpatCasePreviewScreen.triggeredOnResultScreen)
				{
					oWindowFrame.dispose();
				}
				else
				{
					String[] buttons = {CpatToolConstants.OPTION_YES, CpatToolConstants.OPTION_NO};
					int rc = JOptionPane.showOptionDialog(null, CpatToolConstants.ABORT_MSG_PREVIEW_SCREEN, CpatToolConstants.OPTION_DIALOG_TITLE,
							JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[1]);
					
					if(rc == 0)
					{
						Map<String, String> payload = null;
						boolean isSuccess = false;
						
						payload = new HashMap<>();
						payload.put("USER_CHOICE", "ABORT");
						
						isSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, "TOOL-PREVIEW" , oToolBase.activityId, CPAT_ACTION.PREVIEW.toString(), "", payload);
						if(isSuccess)
						{
							oWindowFrame.dispose();
							CpatToolCHelper.quitWebDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
						}
					}					
				}
			}
		});
	}

	
	public void DuplicateSummaryScreenEventHandler(final CpatToolBase oToolBase, final IAction m_oCallbackAction, final Component comp,
			final JFrame oWindowFrame, final Map<String, List<DuplicateRecordFields>> dupRecList, final ParsedDataTable summaryDataTable) {
		JButton btn = null;
		JPanel panel = (JPanel) comp;
		JButton initialButton = null, folloUpButton = null,  abortButton = null;
		
		final Component[] components = panel.getComponents();
		
		Component[] childComponents = null;
		
		for(int i=0; i<components.length; i++)
		{
			if(components[i] instanceof JPanel)
			{
				childComponents = ((JPanel) components[i]).getComponents();
				
				for(int j=0; j< childComponents.length; j++)
				{
					if( childComponents[j] instanceof JButton)
					{
						btn = (JButton) childComponents[j];
						
						if(btn.getText().equals(CpatToolConstants.UI.PREVIEW_BUTTON_INITIAL))
						{
							initialButton = btn;
						}
						else if(btn.getText().equals(CpatToolConstants.UI.PREVIEW_BUTTON_FOLLOWUP))
						{
							folloUpButton = btn;
						}
						else
						{
							abortButton = btn;
						}
					}
				}
			}
		}
		
		if(initialButton != null)
		{
			initialButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) 
				{
					oWindowFrame.dispose();
					isInitial = true;
					m_oCallbackAction.performAction(CPAT_ACTION.CASECLASSIFICATION, CpatToolCConstants.CaseType.INITIAL, summaryDataTable, 
							SciConstants.StringConstants.EMPTY,SciConstants.StringConstants.EMPTY);
				}
			});
		}
		
		if(folloUpButton != null)
		{
			folloUpButton.addActionListener(
					
					new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) 
						{
							
							Component[] childComponents = null;
							String versionType = null; 
							JTextField aerField = null; 
							JComboBox<String> varsionTypeCombo = null; 
							String aerNo = null;
							for(int i=0; i<components.length; i++)
							{
								if(components[i] instanceof JPanel)
								{
									childComponents = ((JPanel) components[i]).getComponents();
									
									for(int j=0; j< childComponents.length; j++)
									{
										
										if(childComponents[j] instanceof JTextField)
										{
											aerField = (JTextField)childComponents[j];
											aerNo = aerField.getText();
										}
										else 
											if(childComponents[j] instanceof JComboBox<?>)
											{
												varsionTypeCombo = (JComboBox<String>)childComponents[j];
												versionType = String.valueOf(varsionTypeCombo.getSelectedItem());
											}
									}	
									
								}
							}
							
							
							if(StringUtils.emptyString(aerNo).equals(SciConstants.StringConstants.EMPTY))
							{
								JOptionPane.showMessageDialog(null, "Please Enter Aer No");
								
							}
							else if(StringUtils.emptyString(versionType).equals( SciConstants.StringConstants.EMPTY))
							{
								JOptionPane.showMessageDialog(null, "Please Select Version Type");
							}
							else 
							{
								oWindowFrame.dispose();
								isInitial = false;
								m_oCallbackAction.performAction(CPAT_ACTION.CASECLASSIFICATION, CpatToolCConstants.CaseType.FOLLOWUP, summaryDataTable,
										aerNo,versionType);
							}
						}
					});
			
		}
		
		abortButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Map<String, String> payload = null;
				boolean isSuccess = false;
				
				payload = new HashMap<>();
				payload.put("USER_CHOICE", "ABORT");
				
				isSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, m_oCallbackAction, "TOOL-PREVIEW" , oToolBase.activityId, CPAT_ACTION.PREVIEW.toString(), "", payload);
				if(isSuccess)
				{
					oWindowFrame.dispose();
					CpatToolCHelper.quitWebDriver();
					m_oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				}
				
			}
		});
		
		oWindowFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) 
			{
				String[] buttons = {CpatToolConstants.OPTION_YES, CpatToolConstants.OPTION_NO};
				int rc = JOptionPane.showOptionDialog(null, CpatToolConstants.ABORT_MSG_PREVIEW_SCREEN, CpatToolConstants.OPTION_DIALOG_TITLE,
						JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[1]);
				
				if(rc == 0)
				{
					Map<String, String> payload = null;
					boolean isSuccess = false;
					
					payload = new HashMap<>();
					payload.put("USER_CHOICE", "ABORT");
					
					isSuccess = CpatToolHome.callCommonRegisterActivity(oToolBase, m_oCallbackAction, "TOOL-PREVIEW" , oToolBase.activityId, CPAT_ACTION.PREVIEW.toString(), "", payload);
					if(isSuccess)
					{
						oWindowFrame.dispose();
						CpatToolCHelper.quitWebDriver();
						m_oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					}
				}
			}
		});
		
	}
}
