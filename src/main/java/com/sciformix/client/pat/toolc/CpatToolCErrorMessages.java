package com.sciformix.client.pat.toolc;

public class CpatToolCErrorMessages {
	public static final String ERROR							= 				"Error: ";
	public static final String OPTION_DIALOG_TITLE				= 				"Message";
	public static final String NO_RECEIPT_FOUND					=				"No Receipt found with Receipt No: ";
	public static final String GENERIC_ERROR					=				"Tool has encountered an error. Please contact tool administrator.";
	
	public static class API
	{
		public static final String API_FAILED 					= 				"Create API Failed";
		public static final String ACTIVITYID_ERR 				= 				"Invalid Activity Id";
	}
	
	public static class Login
	{
		public static final String NO_PRJ_ERR					=				"No Projects available";
		public static final String MULTIPLE_PRJ_ERR 			= 				"Multiple Projects found. No support for Multiple Projects yet.";
		public static final String LOGIN_FAILED 				= 				"Login Failed";
		public static final String NO_ZONE_ERR 					= 				"No Zone Available";
		public static final String RENDERING_FAILED 			= 				"Login Screen Rendering Failed";
		public static final String EMPTY_CREDENTIALS			=				"Login credentials not supplied";
		
	}
	
	public static class ToolDescScreen
	{
		public static final String RENDERING_FAILED				=				"Tool Description Screen Rendering Failed";
	}
	
	public static class UserInputScreen
	{
		public static final String RENDERING_FAILED				=				"User Input Screen Rendering Failed";
		public static final String ERROR_USERNAME_BLANK			=				"Username cannot be blank";
		public static final String ERROR_RECEIPT_BLANK			=				"Receipt No cannot be blank. Please provide a valid Receipt No";
		
		public static final String ERROR_RECEIPT_LENGTH			=				"Receipt No should be of 10 characters";
		//public static final String ERROR_RECEIPT_NOTSFE			=				"Receipt No should always start with SFE";
		public static final String ERROR_RECEIPT_PREFIX_INVALID	=				"Receipt No should always start with ";
		public static final String ERROR_RECEIPT_INVALID		=				"Please provide a valid Receipt No";
		public static final String PRODLIST_PATH_ERR			=				"Please provide valid Product List Path";
		public static final String PRODLIST_UNAVAIL_ERR			=				"File does not exist.";
		public static final String PRODLIST_TYPE_ERR 			= 				"Product List should have XLSX file type";
		public static final String PRODLIST_COL_HEADER_ERR 		= 				"Unsupported Product List contents. Product List does not have column: ";
		public static final String PRODLIST_COL_VAL_ERR 		= 				"Unsupported Product List contents. No data found for column: ";
		public static final String ERROR_CASE_TYPE_NOT_CONFIGURED
																= 				"Case type not configured. Please contact tool administrator.";
	}
	
	public static class PreviewScreen
	{
		public static final String ABORT_MSG					=				"Are you sure you want to 'Abort' Preview Screen ?";
	}
	
	public static class SearchStatusScreen
	{
		public static final String ABORT_MSG					=				"Are you sure you want to 'Abort' Duplicate Summary Screen ?";
	}
	
	public static class DataEntryFailure {
		public static final String DEATH	=	"Patient not dead, but Date of Death and Reported Cause of Death has been provided";
		public static final String BIRTH_DEFECT = "Event associated with Birth Defect is No, but Reported Cause of Birth Defect has been provided";
		public static final String HOSPITALIZATION_PROLONGED	=	"Patient was not admitted to the hospital, but either the Date of Hospital Admission or Date of Discharge has been provided";
		public static final String OTHER_MEDICALLY_IMP_CONDITION =	"Event was not associated with Miscarriage, but the Date and Reported Cause of Miscarriage was provided";
		public static final String PATIENT_ID = "Patient Initials from Source Document and Sceptre do not match";
		public static final String SERIOUS = "All the entries in Sceptre are found as Non-Serious but the receipt is categorized as Serious";
		public static final String AGE_GROUP = "Error Calculating age group, please check source document.";
		public static final String ARTICLE_TITLE = "Error getting Article Title, please check source document.";
		public static final String COUNTRY_OF_OCCURRENCE = "Error getting Country of Occurrence, please check source document.";
		public static final String DATE_OF_BIRTH = "Error getting Country of Occurrence, please check source document.";
		public static final String IRD = "Error getting Initial Receive Date, please check source document.";
		public static final String JOURNAL_TITLE = "Error getting Journal Title, please check source document.";
		public static final String OTHER_IDENTIFICATION_NUMBER = "Error getting Other Identification Number, please check source document.";
		public static final String PATIENT_ID_ERROR = "Error getting Patient Id, please check source document.";
		public static final String PATIENT_GENDER = "Error getting Patient Gender, please check source document.";
		public static final String PRIORITY = "Error getting Priority, please check source document.";
		public static final String REPORTER_FIRST = "Error getting Reporter First Name, please check source document.";
		public static final String REPORTER_LAST = "Error getting Reporter Last Name, please check source document.";
		
	}
}
