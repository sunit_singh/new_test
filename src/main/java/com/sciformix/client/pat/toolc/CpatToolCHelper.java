
package com.sciformix.client.pat.toolc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.html.FormSubmitEvent;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.BaseWebApplicationWriter;
import com.sciformix.client.automation.BaseWebApplicationWriter.BROWSER;
import com.sciformix.client.automation.SearchResults;
import com.sciformix.client.automation.irt.DuplicateRecordFields;
import com.sciformix.client.automation.irt.IRTCaseDocument;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.automation.irt.IRTDuplicateFields;
import com.sciformix.client.automation.irt.IRTWebApplicationWriter;
import com.sciformix.client.automation.sceptre.SceptreConstants;
import com.sciformix.client.automation.sceptre.SceptreConstants.SceptreSearchFields;
import com.sciformix.client.automation.sceptre.SceptreWebApplicationWriter;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.DuplicateFields;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.PreviewCaseDoc;
import com.sciformix.client.pat.commons.ScpUser;
import com.sciformix.client.pat.commons.WorkbookWorkSheetWrapperDEA;
import com.sciformix.client.pat.commons.screens.CpatCasePreviewScreen;
import com.sciformix.client.pat.commons.screens.CpatDuplicateRecordSummaryScreen;
import com.sciformix.client.pat.commons.screens.CpatResultScreen;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse.STATUS;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciException;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarAA;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class CpatToolCHelper {
	
	private static final Logger log = LoggerFactory.getLogger(CpatToolCHelper.class);
	
	private static Map<String,Object> resultSummaryMap= null;
	
	private static IRTWebApplicationWriter oAppWriter = null;
	
	public static Map<String, String> propMap = new HashMap<>();
	public static Map<String, String> sceptreFieldsMap = null;
	
	
	private static int headerIndex;
	
	public static DataTable productListDataTable = null;
	
	
	private static Map<String, String> receiptSummaryFeedMap = null;
	private static Map<String, String> caseDataFeedMap = null;
	private static Map<String, String> classificationDataMap = null;
	
	private static List<DuplicateRecordFields> irtDuplicateFieldsList = null,
			 sceptreDuplicateFieldsList = null;
	private static IRTCaseDocument caseDocument = null;
	
	public static Map<String, String> loadProperties(String p_sConfigForInsertProperties) {
		// TODO Code need to be modified. No need of map to store the properties
		Map<String, String> propMap = null;
		Properties configProp = null;
		
		propMap = new HashMap<String, String>();
		InputStream inputStream = BaseWebApplicationWriter.class.getClassLoader().getResourceAsStream(p_sConfigForInsertProperties);
        try 
        {
        	configProp = new Properties();
            configProp.load(inputStream);
            
            for (final String name: configProp.stringPropertyNames())
			    propMap.put(name, configProp.getProperty(name));
        
        } 
        catch (IOException e) 
        {
        	log.error("Error: " ,e);
            e.printStackTrace();
        }
        
        return propMap;
	}
	
	/**
	 * @param oCallbackAction 
	 * @param p_caseId
	 * @param userInputMap
	 * @param actId 
	 * @param oScpUser 
	 * @author gwandhare 
	 */
	public static void searchCaseAndDownloadCasePdf(CpatToolBase oToolBase, IAction oCallbackAction, String p_aerNo,
			Map<String, String> userInputMap, boolean isVerificationMode){
		log.info("Case Id: " + p_aerNo);
		
		SciPortalWebApiResponse response = null;
		ScpUser scpUser = null;
		boolean passwordWaitFlag = false,
				editPasswordWaitFlag = false,
				caseAlreadyProcessedFlag = false;
				
		IRTConstants.DocumentValidation docValidation = null;
		ParsedDataTableWrapper oPdtWrapper = null;
		
		String chromeVersion = null,
			   ieVersion = null;
		
		
		StringBuilder sdNames = null; 
		ParsedDataTable oCaseDocDataTable = null;
		ParsedDataTable summaryDataTable = null;
		IRTConstants.ReceiptState receiptState = null;
		File caseDocFile = null;
		List<IRTCaseDocument> listCaseDoc = null;
		int activityId = 0;
		
		List<PreviewCaseDoc> listParsedCaseDocs = null;
		
		List<String>  downloadCaseFieldList = null;
		List<String>  receiptFieldList = null;
		List<String>  caseDataFieldList = null;
		
		Map<String, String> payload = null;
		resultSummaryMap = new LinkedHashMap<String, Object>();
		
		scpUser = oToolBase.scpUser;
		payload = new HashMap<>();
		sdNames = new StringBuilder();
		
		//boolean isRegisterActivitySuccess = false;
		
		oAppWriter = new IRTWebApplicationWriter(BROWSER.CHROME, propMap);
		//productList = Arrays.asList(propMap.get("Product.WhiteList").split(",")); 
		try
		{
			oAppWriter.deleteAllDownloadedFiles();
			oAppWriter.launchBrowser(propMap.get(CpatToolHome.getPropertyNameAppendedWithZone(scpUser.getZone(), CpatToolConstants.IRT_SERVER_SUFFIX)));
			oToolBase.browserMapDetails = oAppWriter.getBrowserdetails();
			
			/*productListDataTable = getRowDataFromExcel(userInputMap.get(CpatToolCConstants.UserInputs.IRT_NONMEDICINAL_FILE));*/
			
			passwordWaitFlag =oAppWriter.login(userInputMap.get(CpatToolCConstants.UserInputs.IRT_USERNAME), propMap);
			resultSummaryMap.put(CpatToolCConstants.UI.USER_ID, userInputMap.get(CpatToolCConstants.UserInputs.IRT_USERNAME));
			
			if(!passwordWaitFlag)
			{
				JOptionPane.showMessageDialog(null, "Tool has Timed Out !" + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
				quitWebDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				return;
			}
			
			if(isVerificationMode)
			{
				if(oAppWriter.searchCaseXpathValidation(userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO)
						, propMap))
				{
					receiptFieldList = oAppWriter.readReceiptSummaryList(propMap);
					caseDataFieldList = oAppWriter.readCaseDataList(propMap);
					downloadCaseFieldList = oAppWriter.downloadCasePdfXpathValidation(propMap);
				}
				oAppWriter.generateCsvReport(receiptFieldList, caseDataFieldList, downloadCaseFieldList);
				
				payload.put("USER_MESSAGE", "Csv Report Generated for IRT fields");
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_FETCHING , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_FETCHING, "", payload);
				
				JOptionPane.showMessageDialog(null, "Csv Report Generated for IRT fields");
				quitWebDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				return;
			}
			
			resultSummaryMap.put(CpatToolCConstants.UI.RECPT_NO, userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
			
			receiptState =oAppWriter.searchCase(userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO),propMap);
			//searchResult =oAppWriter.searchCase("SFE3040241");
			if(!receiptState.equals(IRTConstants.ReceiptState.FOUND))
			{
				if(receiptState.equals(IRTConstants.ReceiptState.NOT_FOUND)) {
	
					payload.put("USER_MESSAGE", "No Case found with Receipt No: " + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
					CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
					
					JOptionPane.showMessageDialog(null, "No Case found with Receipt No: " + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				} else {
						
					payload.put("USER_MESSAGE", "Case Receipt locked by another user: " + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
					CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
					
					JOptionPane.showMessageDialog(null, "Case Receipt locked by another user: " + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
			}
			
			editPasswordWaitFlag = oAppWriter.editCasePdf(propMap);
			
			if(!editPasswordWaitFlag)
			{
				payload.put("USER_MESSAGE", "Tool has Timed Out !" + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.LOCATE_CASE , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.LOCATE_CASE, "", payload);
				
				JOptionPane.showMessageDialog(null, "Tool has Timed Out !" + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
				quitWebDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				return;
			}
		
			//read edit receipt form
			oAppWriter.readReceiptSummaryFields(propMap);
			
			caseAlreadyProcessedFlag = oAppWriter.isCaseAlreadyProccessed(propMap);
			
			if(caseAlreadyProcessedFlag)
			{
				JOptionPane.showMessageDialog(null, "LRN already generated for receipt no : "+userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
				quitWebDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				return;
			}
			
			listCaseDoc = new ArrayList<>();
			
			if(CpatToolBase.isTestMode)
			{
				caseDocument = provideListCaseDoc(CpatToolBase.testModePropMap).get(0);
				docValidation = IRTConstants.DocumentValidation.VALID;
			}
			else
			{ 
				docValidation = oAppWriter.downloadCasePdf(propMap, listCaseDoc);
				
				if(listCaseDoc != null)
				{
					caseDocument = listCaseDoc.get(0);
				}
			}
			if(docValidation.equals(IRTConstants.DocumentValidation.VALID) && caseDocument !=null)
			{
				listParsedCaseDocs = new ArrayList<PreviewCaseDoc>();
				if(caseDocument.getCaseFilename().equals(CpatToolConstants.INVALID))
				{
					
					payload.put("USER_MESSAGE", "Cannot process case as E2B file is invalid");
					CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_DOWNLOAD , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_DOWNLOAD, "", payload);
					
					JOptionPane.showMessageDialog(null, "Cannot process case as E2B file is invalid");
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
				else
				{
					caseDocFile = caseDocument.getCaseFile();
					
					oCaseDocDataTable = new PdfDocParserAvatarAA(caseDocFile.getAbsolutePath(), CpatToolBase.isTestMode).parse();
					
					summaryDataTable =oCaseDocDataTable;
					listParsedCaseDocs.add(new PreviewCaseDoc(caseDocFile, oCaseDocDataTable, null));
					sdNames = sdNames.length() > 0 ? sdNames.append("," + caseDocFile.getName()) : sdNames.append(caseDocFile.getName());
					
				}
				payload.put("USER_MESSAGE", "Case document downloaded successfully");
				CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_DOWNLOAD , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_DOWNLOAD, "", payload);
					
				/**
                 * SD Validations         
                 */
				oPdtWrapper = new ParsedDataTableWrapper(summaryDataTable);
			    
				// Safety Report ID vs Local Case ID
				if (StringUtils.isNullOrEmpty(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.CaseIdTabKeys.SAFETY_REPORT_ID))) {
					payload.put("USER_MESSAGE", "Safety Report ID in Source Document cannot be blank");
					callCommonRegisterActivity(oToolBase, oCallbackAction, 
						CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId,
						CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload
					);
					JOptionPane.showMessageDialog(null, "Safety Report ID in Source Document cannot be blank");
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
				
				// Product Regulatory Class
				if (StringUtils.isNullOrEmpty(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_PRODUCT_REGULATORY_CLASS))) {
					payload.put("USER_MESSAGE", "Regulatory class cannot be blank");
					callCommonRegisterActivity(oToolBase, oCallbackAction,
						CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId,
						CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload
					);
					
					JOptionPane.showMessageDialog(null, "Regulatory class cannot be blank");
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
				
				/*Death Validation Start*/
				if (oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_DEATH
					, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE)) {
					if(!oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.NarrativeKeys.NARRATIVE_DEATH, 
						CpatToolConstants.FieldsLogicValues.YES))
					{
						if(!oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_DEATH_AND_REPORTED_CAUSE_OF_DEATH)) {
							payload.put("USER_MESSAGE", CpatToolCErrorMessages.DataEntryFailure.DEATH);
							callCommonRegisterActivity(oToolBase, oCallbackAction,
								CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId,
								CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload
							);
							
							JOptionPane.showMessageDialog(null, CpatToolCErrorMessages.DataEntryFailure.DEATH);
							quitWebDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
							return;
						}
					}
				}
				/* Death Validation End*/
				
				/* Birth Defect Validation Start */
				if (oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
					, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE)) {
					if(!oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT, 
							CpatToolConstants.FieldsLogicValues.YES))
					{
						if(!oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT))
						{
							payload.put("USER_MESSAGE", CpatToolCErrorMessages.DataEntryFailure.BIRTH_DEFECT);
							callCommonRegisterActivity(oToolBase, oCallbackAction,
								CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId,
								CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload
							);
							
							JOptionPane.showMessageDialog(null, CpatToolCErrorMessages.DataEntryFailure.BIRTH_DEFECT);
							quitWebDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
							return;
						}
					}
				}
				/* Birth Defect Validation End */
				
				/*Hospitalization Prolonged Validation Start */
				if (oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED
						, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE)) {
					if(!oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.NarrativeKeys.NARRATIVE_ADMITTED_TO_HOSPITAL_RELATED_TO_EVENT, 
							CpatToolConstants.FieldsLogicValues.YES)) {
						if(!oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_HOSP_ADMISSION) 
								|| !oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_DISCHARGE))
						{
							payload.put("USER_MESSAGE", CpatToolCErrorMessages.DataEntryFailure.HOSPITALIZATION_PROLONGED);
							callCommonRegisterActivity(oToolBase, oCallbackAction,
								CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId,
								CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload
							);
							
							JOptionPane.showMessageDialog(null, CpatToolCErrorMessages.DataEntryFailure.HOSPITALIZATION_PROLONGED);
							quitWebDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
							return;
						}
					}
				}
				/*Hospitalization Prolonged Validation End */
				
				/*Other Medically Important Condition Validation Start */	
				if (oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION
						, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE)) {
					if(!oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATE_WITH_MISSCARRIGE, 
						CpatToolConstants.FieldsLogicValues.YES))
					{
						
						if(!oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_AND_REPORTED_CAUSE_OF_MISSCARRIGE))
						{
							payload.put("USER_MESSAGE", CpatToolCErrorMessages.DataEntryFailure.OTHER_MEDICALLY_IMP_CONDITION);
							callCommonRegisterActivity(oToolBase, oCallbackAction,
								CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId,
								CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload
							);
							
							JOptionPane.showMessageDialog(null, CpatToolCErrorMessages.DataEntryFailure.OTHER_MEDICALLY_IMP_CONDITION);
							quitWebDriver();
							oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
							return;
						}
					}
				}
				
				payload.put("SD_NAME", sdNames.toString());
				
				if(oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERNAME).equalsIgnoreCase(CpatToolConstants.Browser.CHROME))
				{
					chromeVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
					ieVersion = StringConstants.EMPTY;
				}
				else
				{
					chromeVersion = StringConstants.EMPTY;
					ieVersion = oToolBase.browserMapDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
				}
				
				response = oToolBase.scpClient.registerActivity(scpUser.getUserId(), 
						scpUser.getPassword(), scpUser.getUserProjectsList().get(0).getProjectSeqId(), 
						oToolBase.getToolDetails().getToolName(), CpatToolConstants.ACTIVITY_TYPE.SD_EXTRACTION, oToolBase.activityId, 
						CpatToolConstants.ACTIVITY_CODE.SD_EXTRACTION, "", payload,chromeVersion, ieVersion);

				if(response.getStatus() == STATUS.SUCCESS)
				{
					activityId = Integer.parseInt(response.getData().toString());
					
					if(activityId > 0)
					{
						payload.put("USER_MESSAGE", "Case Previewed Successfully.");
						callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.PREVIEW, oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.PREVIEW, "", payload);
						
						oToolBase.activityId = activityId;
						oCallbackAction.performAction(CPAT_ACTION.PREVIEW, listParsedCaseDocs, summaryDataTable);
					}
					
					else
					{
						JOptionPane.showMessageDialog(null, "Invalid Activity Id");
						quitWebDriver();
						oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
						return;
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Register Activity Failed");
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
			}
			else
			{
				//TODO: Call Register Activity
				if(docValidation.equals(IRTConstants.DocumentValidation.NO_DOCUMENT))
				{
					payload.put("USER_MESSAGE", "No Case Document Found !");
					CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_DOWNLOAD , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_DOWNLOAD, "", payload);
					
					JOptionPane.showMessageDialog(null, "No Case Document Found !");
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
				else if(docValidation.equals(IRTConstants.DocumentValidation.INVALID))
				{
					payload.put("USER_MESSAGE", "Invalid Source Document !");
					CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_DOWNLOAD , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_DOWNLOAD, "", payload);
					
					JOptionPane.showMessageDialog(null, "Invalid Source Document !");
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
				else 
				{
					payload.put("USER_MESSAGE", "Tool has encountered and error. Please contact tool administrator.");
					CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.SD_DOWNLOAD , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.SD_DOWNLOAD, "", payload);
					
					JOptionPane.showMessageDialog(null, "Tool has encountered and error. Please contact tool administrator.");
					quitWebDriver();
					oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
					return;
				}
			}
		}catch(Exception e){
			log.error("Error: ",e);
			quitWebDriver();
			JOptionPane.showMessageDialog(null, CpatToolCErrorMessages.GENERIC_ERROR);
			oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
			return;
		}
	}
	
	/**
	 * 
	 * @param oToolBase
	 * @param oCallbackAction
	 * @param summaryDataTable
	 * @param userInputMap
	 * @author gwandhare
	 * @see Perform duplicate search in Sceptre and Irt and shows duplicate summary screen
	 */
	public static void performDuplicateSearch(CpatToolBase oToolBase, IAction oCallbackAction, 
		ParsedDataTable summaryDataTable, Map<String, String> userInputMap, Map<String, Boolean> searchCriteriaMap)
	{
		Map<String, String> payload = new HashMap<String, String>();
		Map<String, List<DuplicateRecordFields>> duplicateRecordsMap = null;
		
		boolean sceptreLoginSuccessfullFlag = false;
		
		String	l_sceptreurl = propMap.get(CpatToolHome.getPropertyNameAppendedWithZone(oToolBase.scpUser.getZone(), CpatToolConstants.SCEPTRE_SERVER_SUFFIX));
		
		payload.put("USER_MESSAGE", "Duplicate Search Initiated.");
		CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
		
		log.info("Perform duplicate search");

		sceptreLoginSuccessfullFlag = irtDuplicateSearchSceptre(
			summaryDataTable,userInputMap.get(CpatToolCConstants.UserInputs.IRT_USERNAME),
			l_sceptreurl, searchCriteriaMap);
		
		if(!sceptreLoginSuccessfullFlag)
		{
			log.info("Sceptre login time out");
			JOptionPane.showMessageDialog(null, "Tool has Timed Out !" + userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
			quitWebDriver();
			oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
			return;
		}
		
		irtDuplicateSearch(summaryDataTable, 
			userInputMap.get(CpatToolCConstants.UserInputs.IRT_USERNAME),
			userInputMap.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO),
			searchCriteriaMap); 
		
		if(!sceptreDuplicateFieldsList.isEmpty()){
			payload.put("USER_MESSAGE", "Potential Duplicate Records found in Sceptre.");
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
		}
		
		if(!irtDuplicateFieldsList.isEmpty()){
			payload.put("USER_MESSAGE", "Potential Duplicate Records found in IRT.");
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
		}
		
		duplicateRecordsMap = new HashMap<>();
		
		duplicateRecordsMap.put(CpatToolCConstants.DUPLICATE_SCEPTRE_RECORD_LIST, sceptreDuplicateFieldsList);
		duplicateRecordsMap.put(CpatToolCConstants.DUPLICATE_IRT_RECORD_LIST, irtDuplicateFieldsList);
		oCallbackAction.performAction(CPAT_ACTION.DUPLICATE_SUMMARY, duplicateRecordsMap,getDuplicateFieldData(summaryDataTable), summaryDataTable);
	}
	
	
	//Sceptre Duplicate Search
	public static boolean irtDuplicateSearchSceptre(ParsedDataTable datatable,
		String scp_username,String p_sceptreurl, Map<String, Boolean> searchCriteriaMap)
	{
		DuplicateRecordFields dupRecField = null;
		Set<String> aerSet = new HashSet<String>();
		SceptreWebApplicationWriter sceptreAppWriter = null;
		SearchResults oResults = null,	 oDataTable = null;
		Map<SceptreSearchFields , Object> mapCriteria = null;
		
		boolean loginSuccessfullFlag = false;
		
		String sVersion = null ,  sLocalCaseID = null, sCurrentWindow = null;
		
		List<String> readCaseIdKeysList = null;
		List<String> readReporterKeysList = null;
		List<String> readPatientKeysList = null;
		List<String> readReactionKeysList = null;
		List<String> readDrugKeysList = null;
		
		Set<String> preCaseOpenWindowSet = null;
		Set<String> postCaseOpenWindowSet = null;
		
		//to read Values from sceptre
		Map<String, List<String>> readSceptreMap = null;
		//to read Values from sceptre for all tabs
		Map<String, Map<String, Object>> sceptreTabValues = null;
		
		sceptreDuplicateFieldsList = new ArrayList<DuplicateRecordFields>();
		
		readCaseIdKeysList = new ArrayList<String>();
		readReporterKeysList = new ArrayList<String>();
		readPatientKeysList = new ArrayList<String>();
		readReactionKeysList  = new ArrayList<String>();
		readDrugKeysList = new ArrayList<String>();
		
		readSceptreMap = new LinkedHashMap<String, List<String>>();
		sceptreTabValues = new HashMap<String, Map<String, Object>>();
		
		oAppWriter.swithToSceptreWindow();
		oAppWriter.openUrl(p_sceptreurl);
		sceptreAppWriter = new SceptreWebApplicationWriter(oAppWriter.getDriver());
		
		loginSuccessfullFlag = sceptreAppWriter.login(scp_username);
		
		if(!loginSuccessfullFlag)
		{
			return loginSuccessfullFlag;
		}
		
		// Get set of windows open before opening the case.
		preCaseOpenWindowSet = sceptreAppWriter.getSetOfWindow();
		
		// Get Current Window
		sCurrentWindow = sceptreAppWriter.getCurrentWindow();
		
		//First Duplicate Search Approach
		duplicateSearchLogicFirstApproach(datatable, SceptreConstants.SceptreSearchFields.LOCAL_CASE_ID, aerSet, sceptreAppWriter);
		
		//Second Duplicate Search Approach
		duplicateSearchLogicSecondApproch(datatable, SceptreConstants.SceptreSearchFields.OTHER_ID, aerSet, sceptreAppWriter);
		
		//Third Duplicate Search Approach
		duplicateSearchLogicThirdApproch(datatable, aerSet, sceptreAppWriter, searchCriteriaMap);

		//search all AER and get Fields
		mapCriteria = new HashMap<SceptreConstants.SceptreSearchFields, Object>();
		
		sceptreAppWriter.clearSearch();
		for(String sAER : aerSet) 
		{
			readCaseIdKeysList.clear(); 
			readReporterKeysList.clear();
			readPatientKeysList.clear();
			readReactionKeysList.clear();
			readDrugKeysList.clear();
			mapCriteria.clear();
			
			mapCriteria.put(SceptreConstants.SceptreSearchFields.AER_NUMBER, sAER);
			mapCriteria.put(SceptreConstants.SceptreSearchFields.SHOW_DELETED_CASES, IRTConstants.YES);

			oResults = sceptreAppWriter.search(mapCriteria);

			if(oResults.resultsFound()) 
			{
					dupRecField = new DuplicateRecordFields();
					
					//get max version
					sVersion = CpatToolCHelper.getMaxCaseVersion(oResults, sAER);
					sLocalCaseID = sceptreAppWriter.getLocalCaseId(sAER, sVersion, false);
					
					//add required fields to fetch
					//if case is initial 
				
				if(!sVersion.equals("0")) 
				{
					//followup
					readCaseIdKeysList.add(SceptreConstants.CaseIdTabKeys.COUNTRY_WHEREAE_OCCOURED_FOLLOWUP);
				}
				else 
				{
					readCaseIdKeysList.add(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED);
				}
				
				//add required fields for patient tab
				
				
				readPatientKeysList.add(SceptreConstants.PatientTabKeys.PATIENT_ID);
				readPatientKeysList.add(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH);
				readPatientKeysList.add(SceptreConstants.PatientTabKeys.PATIENT_SEX);
				readPatientKeysList.add(SceptreConstants.PatientTabKeys.AGE_GROUP);
				
				//add required fields for reporter details
				readReporterKeysList.add(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
				readReporterKeysList.add(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
				readReporterKeysList.add(SceptreConstants.ReporterTabKeys.QUALIFICATION);
				readReporterKeysList.add(SceptreConstants.ReporterTabKeys.COUNTRY);
				
				//add required fields for drug details
				readDrugKeysList.add(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME);
				
				//add required fields for reporter details
				readSceptreMap.put(SceptreConstants.CaseIdTabKeys.CASEID_TAB, readCaseIdKeysList);
				readSceptreMap.put(SceptreConstants.PatientTabKeys.PATIENT_TAB, readPatientKeysList);
				readSceptreMap.put(SceptreConstants.ReporterTabKeys.REPORTER_TAB, readReporterKeysList);
				readSceptreMap.put(SceptreConstants.ReactionTabKeys.TAB, readReactionKeysList);
				readSceptreMap.put(SceptreConstants.DrugTabKeys.TAB, readDrugKeysList);
				
				
				
				sceptreTabValues = sceptreAppWriter.getFieldValues(readSceptreMap);
				
				//Initialize search result
				oDataTable = new SearchResults();
				
				oDataTable = sceptreAppWriter.getReactionListAll();

				StringBuilder rank = new StringBuilder(); 
				StringBuilder reportedReaction = new StringBuilder(); 
				StringBuilder serious = new StringBuilder(); 
				StringBuilder outcome = new StringBuilder(); 

				for (int recCunt = 0; recCunt< oDataTable.resultCount(); recCunt++)
				{
					rank.append(oDataTable.getValue(recCunt, SceptreConstants.Name.REACTION_RANK));
					reportedReaction.append(oDataTable.getValue(recCunt, SceptreConstants.Name.REACTION_REPORTEDREACTION));
					serious.append(oDataTable.getValue(recCunt, SceptreConstants.Name.REACTION_SERIOUS));
					outcome.append(oDataTable.getValue(recCunt, SceptreConstants.Name.REACTION_OUTCOME));
				
					 if(recCunt<oDataTable.resultCount()-1)
					 {
						 rank.append(SciConstants.StringConstants.TILDE);
						 reportedReaction.append(SciConstants.StringConstants.TILDE);
						 serious.append(SciConstants.StringConstants.TILDE);
						 outcome.append(SciConstants.StringConstants.TILDE);
					 }
				}
				
				dupRecField.setAerNo(sAER);
				dupRecField.setReactionRecordRank(rank.toString());
				dupRecField.setReactionRecordReportedReaction(reportedReaction.toString());
				dupRecField.setReactionRecordSeriousness(serious.toString());
				dupRecField.setReactionRecordOutcome(outcome.toString());
				
				//set values to dupRecField for case id tab
				dupRecField.setCountryWhereOccure(String.valueOf(sceptreTabValues.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB).get(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED)));
				dupRecField.setLocalCaseId(sLocalCaseID);
				
				//set values to dupRecField for patient tab
				dupRecField.setPatientInitials(String.valueOf(sceptreTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_TAB).get(SceptreConstants.PatientTabKeys.PATIENT_ID)));
				dupRecField.setPatientDOB(String.valueOf(sceptreTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_TAB).get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH)));
				dupRecField.setPatientGender(String.valueOf(sceptreTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_TAB).get(SceptreConstants.PatientTabKeys.PATIENT_SEX)));
				dupRecField.setPatientAgeGroup(String.valueOf(sceptreTabValues.get(SceptreConstants.PatientTabKeys.PATIENT_TAB).get(SceptreConstants.PatientTabKeys.AGE_GROUP)));
				
				//set values to dupRecField for patient tab
				dupRecField.setReporterGivenName(String.valueOf(sceptreTabValues.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB).get(SceptreConstants.ReporterTabKeys.GIVEN_NAME)));
				dupRecField.setReporterFamilyName(String.valueOf(sceptreTabValues.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB).get(SceptreConstants.ReporterTabKeys.FAMILY_NAME)));
				dupRecField.setReporterCountry(String.valueOf(sceptreTabValues.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB).get(SceptreConstants.ReporterTabKeys.COUNTRY)));
				dupRecField.setReporterQualification(String.valueOf(sceptreTabValues.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB).get(SceptreConstants.ReporterTabKeys.QUALIFICATION)));
				
				//set values to dupRecField for DrugTab
				dupRecField.setReportedDrugName(String.valueOf(sceptreTabValues.get(SceptreConstants.DrugTabKeys.TAB).get(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME)));
				
				//set SET Object 
				sceptreDuplicateFieldsList.add(dupRecField);
				
				// Get set of windows after opening a case in sceptre. 
				postCaseOpenWindowSet = sceptreAppWriter.getSetOfWindow();
				
				// Close Case Notes Pop if available.
				sceptreAppWriter.closeCaseNotesWindowIfAvailable(preCaseOpenWindowSet, postCaseOpenWindowSet, sCurrentWindow);
			}			
			sceptreAppWriter.prepareForSearch();	
		}
		return loginSuccessfullFlag;
	}
	private static void duplicateSearchLogicFirstApproach(ParsedDataTable datatable, 
		SceptreSearchFields scepterField, Set<String> searchResultAer,SceptreWebApplicationWriter oAppWriter)
	{
		Map<SceptreSearchFields , Object> mapCriteria = null;
		SearchResults oResults = null;
		String srAerNo = null ;
		
		String localCaseId = CpatToolCHelper.getParsedDataFieldValue(datatable 
				,CpatToolCConstants.ReportSummaryTabKeys.GENINFO_LOC_CASE_ID);

		//String localCaseId = "000181261";
		
		oAppWriter.prepareForSearch();
		mapCriteria = new HashMap<SceptreConstants.SceptreSearchFields, Object>();
		
		mapCriteria.put(scepterField,SciConstants.StringConstants.ASTERISK+localCaseId+SciConstants.StringConstants.ASTERISK);
		mapCriteria.put(SceptreConstants.SceptreSearchFields.SHOW_DELETED_CASES, IRTConstants.YES);
		
		
		oResults = oAppWriter.search(mapCriteria);
		
		log.info("Perform Sceptre duplicate search first approach");;
		
		if(oResults != null && oResults.resultsFound()) 
		{
			for(int i = 0; i < oResults.resultCount(); i++)
			{
				srAerNo = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER));
					
				searchResultAer.add(srAerNo);
			}
		}
		
		mapCriteria.clear();
	}
	private static void duplicateSearchLogicSecondApproch(ParsedDataTable datatable, SceptreSearchFields scepterField, Set<String> searchResultAer,SceptreWebApplicationWriter oAppWriter)
	{
		Map<SceptreSearchFields , Object> mapCriteria = null;
		SearchResults oResults = null;
		String srAerNo = null ;
		
		String localCaseId = CpatToolCHelper.getParsedDataFieldValue(datatable ,
				CpatToolCConstants.ReportSummaryTabKeys.GENINFO_LOC_CASE_ID);

		
		oAppWriter.clearSearch();
		mapCriteria = new HashMap<SceptreConstants.SceptreSearchFields, Object>();
		
		mapCriteria.put(scepterField,SciConstants.StringConstants.ASTERISK+localCaseId+SciConstants.StringConstants.ASTERISK);
		mapCriteria.put(SceptreConstants.SceptreSearchFields.SHOW_DELETED_CASES, IRTConstants.YES);
		
		oResults = oAppWriter.search(mapCriteria);
		log.info("Perform Sceptre duplicate search second approach");;

		
		if(oResults != null && oResults.resultsFound()) 
		{
			for(int i = 0; i < oResults.resultCount(); i++)
			{
				srAerNo = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER));
					
				searchResultAer.add(srAerNo);
			}
		}
		
		mapCriteria.clear();
	}
	
	private static void duplicateSearchLogicThirdApproch(ParsedDataTable datatable
		, Set<String> searchResultAer, SceptreWebApplicationWriter oAppWriter
		, Map<String, Boolean> searchCriteriaMap) 
	{
		List<String> valuesList = new ArrayList<String>();
		Map<SceptreSearchFields , Object> mapCriteria = null;
		SearchResults oResults = null;	
		
		String srAerNo = null , reporterGivenName = null, reporterFamilyName= null,  
				patientDOB = null, patientInitials= null, patientGender = null, country = null;
		
		mapCriteria = new HashMap<SceptreConstants.SceptreSearchFields, Object>();
		
		reporterGivenName  = CpatToolCHelper.getParsedDataFieldValue(datatable ,
				CpatToolCConstants.ReportSummaryTabKeys.REPORTER_FIRST);
		
		reporterFamilyName =CpatToolCHelper.getParsedDataFieldValue(datatable ,
				CpatToolCConstants.ReportSummaryTabKeys.REPORTER_LAST);
		
		patientDOB =CpatToolCHelper.getParsedDataFieldValue(datatable , 
				CpatToolCConstants.ReportSummaryTabKeys.PATIENT_DOB);
		
		patientInitials =CpatToolCHelper.getParsedDataFieldValue(datatable ,
				CpatToolCConstants.ReportSummaryTabKeys.PATIENT_PATIENT_INITIALS);
		
		patientGender =CpatToolCHelper.getParsedDataFieldValue(datatable ,
				CpatToolCConstants.ReportSummaryTabKeys.PATIENT_SEX);
		
		country =CpatToolCHelper.getParsedDataFieldValue(datatable ,
				CpatToolCConstants.ReportSummaryTabKeys.REPORTER_COUNTRY);
		
		
		DuplicateFields l_dupfielddata = new IRTDuplicateFields(
											!StringUtils.isNullOrEmpty(reporterGivenName)
										, 	!StringUtils.isNullOrEmpty(reporterFamilyName)
										,	!StringUtils.isNullOrEmpty(patientDOB)
										, 	!StringUtils.isNullOrEmpty(patientInitials)
										, 	!StringUtils.isNullOrEmpty(patientGender)
										, 	!StringUtils.isNullOrEmpty(country)
										);
		if (l_dupfielddata.isSearchToBePerformed(searchCriteriaMap)) {
			
			if (!StringUtils.isNullOrEmpty(propMap.get(IRTConstants.SCEPTRE_SEARCH_RESULT_THRESHOLD))) {
				SceptreWebApplicationWriter.isThresholdToBeChecked = true;
				SceptreWebApplicationWriter.thresholdSearchResultCount = Integer.parseInt(propMap.get(IRTConstants.SCEPTRE_SEARCH_RESULT_THRESHOLD));
			}
			oResults = searchInSceptre(oAppWriter , mapCriteria, patientInitials,patientDOB, patientGender,
							reporterGivenName, reporterFamilyName, country);
			
			if (oResults == null) {
				JOptionPane.showMessageDialog(null, "Sceptre search reuslt count exceeds the threshold value.");
				return;
			}
		}
		
		if(oResults != null && oResults.resultsFound()) 
		{
			for(int i = 0; i < oResults.resultCount(); i++)
			{
				srAerNo = String.valueOf(oResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER));
					
				searchResultAer.add(srAerNo);
			}
		}
	}

	private static SearchResults searchInSceptre(SceptreWebApplicationWriter oAppWriter , Map<SceptreSearchFields , Object> p_mapCriteria
			,String patientInitials, String patientDOB, String patientGender, String reporterGivenName, String reporterFamilyName, String country )
	{
		SearchResults oResults = null;	
		
		if(!StringUtils.isNullOrEmpty(patientInitials))
		{
			p_mapCriteria.put(SceptreConstants.SceptreSearchFields.PATIENT_INITIALS,SciConstants.StringConstants.ASTERISK+ patientInitials +SciConstants.StringConstants.ASTERISK);
		}
		if(!StringUtils.isNullOrEmpty(patientDOB))
		{
			p_mapCriteria.put(SceptreConstants.SceptreSearchFields.PATIENT_DOB1, patientDOB);
		}
		if(!StringUtils.isNullOrEmpty(patientGender))
		{
			p_mapCriteria.put(SceptreConstants.SceptreSearchFields.PATIENT_GENDER, patientGender);
		}
		if(!StringUtils.isNullOrEmpty(reporterGivenName))
		{
			p_mapCriteria.put(SceptreConstants.SceptreSearchFields.REPORTER_GIVENNAME, SciConstants.StringConstants.ASTERISK + reporterGivenName + SciConstants.StringConstants.ASTERISK);
		}
		if(!StringUtils.isNullOrEmpty(reporterFamilyName))
		{
			p_mapCriteria.put(SceptreConstants.SceptreSearchFields.REPORTER_FAMILYNAME, SciConstants.StringConstants.ASTERISK + reporterFamilyName + SciConstants.StringConstants.ASTERISK);
		}
		if(!StringUtils.isNullOrEmpty(country))
		{
			p_mapCriteria.put(SceptreConstants.SceptreSearchFields.COUNTRY, country);
		}
		p_mapCriteria.put(SceptreConstants.SceptreSearchFields.SHOW_DELETED_CASES, IRTConstants.YES);
		
		oAppWriter.clearSearch();
		
		oResults = oAppWriter.search(p_mapCriteria);
		log.info("Perform Sceptre duplicate search third approach");
		
		return oResults;
	}
	
	private static boolean irtDuplicateSearch( ParsedDataTable dataTable, 
		String userName,String receiptNo, Map<String, Boolean> searchCriteriaMap)
	{
		boolean caseAlreadyProcessedFlag = false;
		
		oAppWriter.swithToIrtWindow();
		
		irtDuplicateFieldsList = new ArrayList<DuplicateRecordFields>();
		
		oAppWriter.irtDuplicateSearchFirstApproch(dataTable, propMap);
		oAppWriter.swithToIrtDuplicateWindow();
		oAppWriter.getIrtDuplicateRecordsAer(dataTable, propMap, irtDuplicateFieldsList, receiptNo);
		oAppWriter.swithToIrtWindow();
		
		oAppWriter.irtDuplicateSearchSecondApproch(dataTable, propMap);
		oAppWriter.swithToIrtDuplicateWindow();
		oAppWriter.getIrtDuplicateRecordsAer(dataTable, propMap, irtDuplicateFieldsList, receiptNo);
		oAppWriter.swithToIrtWindow();
		
		oAppWriter.irtDuplicateSearchThirdApproch(dataTable, propMap, searchCriteriaMap);
		oAppWriter.swithToIrtDuplicateWindow();
		oAppWriter.getIrtDuplicateRecordsAer(dataTable, propMap, irtDuplicateFieldsList,  receiptNo);
		oAppWriter.swithToIrtWindow();
		
		return caseAlreadyProcessedFlag;
	}
	
	public static void initializeSceptreMap(String aerNo)
	{
		if(sceptreDuplicateFieldsList !=null && !sceptreDuplicateFieldsList.isEmpty())
		{
			for(DuplicateRecordFields dupRecField : sceptreDuplicateFieldsList)
			{
				if(dupRecField.getAerNo().trim().equals(aerNo.trim()))
				{
					sceptreFieldsMap = new HashMap<>();
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.LOCAL_CASE_ID, StringUtils.emptyString(dupRecField.getLocalCaseId()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.COUNTRY_WHERE_OCCUR, StringUtils.emptyString(dupRecField.getCountryWhereOccure()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.PAT_DOB, StringUtils.emptyString(dupRecField.getPatientDOB()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.PAT_GENDER, StringUtils.emptyString(dupRecField.getPatientGender()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.PAT_INITIALS, StringUtils.emptyString(dupRecField.getPatientInitials()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.PAT_AGE_GROUP, StringUtils.emptyString(dupRecField.getPatientAgeGroup()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REPORTER_GIVEN_NAME, StringUtils.emptyString(dupRecField.getReporterGivenName()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REPORTER_FAMILY_NAME, StringUtils.emptyString(dupRecField.getReporterFamilyName()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REPORTER_COUNTRY, StringUtils.emptyString(dupRecField.getReporterCountry()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REPORTER_QUALIFICATION, StringUtils.emptyString(dupRecField.getReporterQualification()));
					
					
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REACTION_RANK, StringUtils.emptyString(dupRecField.getReactionRecordRank()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REACTION_REPORTED_REACTION, StringUtils.emptyString(dupRecField.getReactionRecordReportedReaction()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REACTION_SERIOUSNESS, StringUtils.emptyString(dupRecField.getReactionRecordSeriousness()));
					sceptreFieldsMap.put(IRTConstants.SceptreFieldData.REACTION_OUTCOME, StringUtils.emptyString(dupRecField.getReactionRecordOutcome()));
				
					break;
				}
			}
		}
	}
	
	/**
	 * 
	 * @param oToolBase
	 * @param oCallbackAction
	 * @param caseType
	 * @param summaryDataTable
	 * @param aerNo
	 * @param userInputMap
	 * @author gwandhare
	 * @see Perform case classification after duplicate search in sceptre as well as IRT
	 * 
	 */
	public static void performCaseClassification(CpatToolBase oToolBase, IAction oCallbackAction, String caseType,ParsedDataTable summaryDataTable, Map<String, String> userInputMap, String aerNo,String versionType)
	{
		log.info("Perform Case Classification");
		
		Map<String, String> payload = new HashMap<String, String>();
		
		classificationDataMap = new LinkedHashMap<>(); 
		classificationDataMap.put(CpatToolCConstants.ResultSummaryUI.CLASSIFICATION, caseType);
		classificationDataMap.put(CpatToolCConstants.ResultSummaryUI.VERSION_TYPE, versionType);
		classificationDataMap.put(CpatToolCConstants.ResultSummaryUI.FOLLOW_UP_AER, aerNo);
		
		if(caseType != null)
		{
			payload.put("USER_MESSAGE", "Case marked as: " + caseType);
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DUPLICITY_CHECK , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DUPLICITY_CHECK, "", payload);
			
			oAppWriter.caseClassification(caseType, propMap, summaryDataTable, aerNo, versionType);
			resultSummaryMap.put(CpatToolConstants.ResultSummaryScreen.CLASSIFICATION, classificationDataMap);
		}
	}
	
	/**
	 * @param oToolBase
	 * @param oCallbackAction
	 * @param summaryDataTable
	 * @author gwandhare 
	 * @see Feed case data tab and result summary tab and show result summary screen 
	 */
	public static void dataEntry(CpatToolBase oToolBase, IAction oCallbackAction, 
			ParsedDataTable summaryDataTable, Map<String, String> dataEntryMap, String aerNo)
	{
		log.info("Data Entry Started.");
		
		Map<String, String> payload = new HashMap<String, String>();
		List<String> keyList = new ArrayList<String>();
		
		try {
		
			receiptSummaryFeedMap = oAppWriter.feedReceiptSummaryMenuTab(summaryDataTable,propMap, dataEntryMap);
			
			caseDataFeedMap =oAppWriter.feedCaseDataTab(summaryDataTable, propMap, dataEntryMap, aerNo);
			
			resultSummaryMap.put(CpatToolConstants.ResultSummaryScreen.RECEIPT_SUMMARY, receiptSummaryFeedMap);
			resultSummaryMap.put(CpatToolConstants.ResultSummaryScreen.CASE_DATA, caseDataFeedMap);
			
			// Receipt Summary Actvity
			payload.put(CpatToolCConstants.Fields.TAB, CpatToolCConstants.Tabs.RECEIPT_SUMMARY);
			payload.put(CpatToolCConstants.Fields.FIELD_COUNT, String.valueOf(receiptSummaryFeedMap.size()));
			keyList.addAll(receiptSummaryFeedMap.keySet());
			payload.put(CpatToolCConstants.Fields.FIELD_LIST, CpatToolHome.convertListToStringAndReplacePrefix(keyList,CpatToolCConstants.PREFIX_DATAENTRY));
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			
			keyList.clear();
			payload.clear();
			
			// Case Data Actvity
			payload.put(CpatToolCConstants.Fields.TAB, CpatToolCConstants.Tabs.CASE_DATA);
			payload.put(CpatToolCConstants.Fields.FIELD_COUNT, String.valueOf(caseDataFeedMap.size()));
			keyList.addAll(caseDataFeedMap.keySet());
			payload.put(CpatToolCConstants.Fields.FIELD_LIST, CpatToolHome.convertListToStringAndReplacePrefix(keyList, CpatToolCConstants.PREFIX_CASEDATA));
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			
			payload.clear();
			payload.put(CpatToolCConstants.Fields.SCIFORMIX_ACTIVITY_ID, String.valueOf(oToolBase.activityId));
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY_SUMMARY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY_SUMMARY, "", payload);
			
			oCallbackAction.performAction(CPAT_ACTION.RESULT_SUMMARY, resultSummaryMap);
			log.info("Case Feeding Done");
		}
		catch(Exception exc)
		{
			JOptionPane.showMessageDialog(null, "Tool has encountered an error in data entry. Please contact tool administrator.");
			log.error("Data Entry Failed: ", exc);
			oAppWriter.quitWebDriver();
			
			payload.put("USER_MESSAGE", "Data Entry Failed.");
			CpatToolHome.callCommonRegisterActivity(oToolBase, oCallbackAction, CpatToolConstants.ACTIVITY_TYPE.DATA_ENTRY , oToolBase.activityId, CpatToolConstants.ACTIVITY_CODE.DATA_ENTRY, "", payload);
			oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
			return;
		}
		
	}
	
	public static void showResultScreen(CpatToolBase oToolBase, IAction oCallbackAction,  Map<String, Object> resultMap) 
	{
		Map<String, Object> param = new HashMap<>();
		CpatCasePreviewScreen.displayPreviewScreenView();
		param.put(CpatToolConstants.ContentParams.RESULT_SCREEN_DATA, getResultSummaryHtml(resultMap, oAppWriter.fieldNameMap));
		param.put(CpatToolConstants.ContentParams.WRITER_INSTANCE, oAppWriter);
		new CpatResultScreen(oToolBase, oCallbackAction, param);
		
		oAppWriter.deleteAllDownloadedFiles();
		
	}
	
	//Prepare Result Screen 
	private static String getResultSummaryHtml(Map<String, Object> resultSummaryMap, Map<String, String> fieldNameMap)
	{
		Map<String, String> receiptSummaryMap = null;
		Map<String, String> classificationMap = null;
		Map<String, String> caseDataMap = null;
		
		StringBuffer htmlString = new StringBuffer();
		
		String receiptSummaryTable = null,
				caseDataTable = null,
				classificationTable = null;
		String localCaseId = null; 
		
		String collateBodyTable = null;
		
		receiptSummaryMap = (Map<String, String>)resultSummaryMap.get(CpatToolConstants.ResultSummaryScreen.RECEIPT_SUMMARY);
		
		caseDataMap = (Map<String, String>)resultSummaryMap.get(CpatToolConstants.ResultSummaryScreen.CASE_DATA);
		
		if(resultSummaryMap.get(CpatToolConstants.ResultSummaryScreen.CLASSIFICATION) != null)
		{
			classificationMap = (Map<String, String>)resultSummaryMap.get(CpatToolConstants.ResultSummaryScreen.CLASSIFICATION);
		}
		
		receiptSummaryTable = createFieldValue(receiptSummaryMap, fieldNameMap);
		caseDataTable = createFieldValue(caseDataMap, fieldNameMap);
		classificationTable = createFieldValue(classificationMap ,fieldNameMap);
		
		localCaseId = (String)receiptSummaryMap.get(CpatToolConstants.ResultSummaryScreen.LOCAL_CASE_ID);
		
		collateBodyTable = collateHtmlBodyTable(classificationTable, caseDataTable, receiptSummaryTable, localCaseId);
		
		htmlString.append(collateBodyTable);
		
		return collateBodyTable;
	}
	
	private static String createFieldValue(Map<String, String> fieldSummaryMap, Map<String, String> fieldNameMap)
	{
		StringBuffer html = null; 
		String feedValue = null; 
		html = new StringBuffer(); 
		if(fieldSummaryMap !=null &&!fieldSummaryMap.isEmpty())
		{
			html.append("<table border = '1' style='background-color: #EFFEE8;'>"
							+ "<thead> "
							+	"<th align = 'left' style=\"width: 10cm;\">Field Name</th>"
							+ 	"<th align = 'left' style=\"width: 10cm;\">Field Values</th>"
							+ "</thead>"
							+ "<tbody>");
			
			for(String fieldName : fieldSummaryMap.keySet())
			{
				html.append("<tr>"
							+ "<td align = 'left'>"+fieldNameMap.get(fieldName)+"</td>"
							+"</td><td align = 'left'>");
				
				
				feedValue = fieldSummaryMap.get(fieldName);
				
				
				if(feedValue !=null)
				{
					if(feedValue.contains(StringConstants.COMMA_WITH_SPACE))
					{
						String[] fieldArray = feedValue.split(StringConstants.COMMA_WITH_SPACE) ;
						int count = 0;
						StringBuffer fieldAdd = new StringBuffer();
						for(String field : fieldArray)
						{
							if(field.startsWith(CpatToolConstants.ERROR_STRING))
							{
								field = field.replace(CpatToolConstants.ERROR_STRING, SciConstants.StringConstants.EMPTY);
								
								if(!field.trim().equals(CpatToolCConstants.STRING_NULL))
								{
									fieldAdd.append("<font color=\"red\">"+field+"</font>");
								}
							}
							else
							{
								fieldAdd.append(field);
							}
							
							if(count<fieldArray.length-1)
							{
								fieldAdd.append(StringConstants.COMMA_WITH_SPACE);
							}
						}
						feedValue = fieldAdd.toString();
					}
					else if(feedValue.startsWith(CpatToolConstants.ERROR_STRING))
					{
						feedValue = feedValue.replace(CpatToolConstants.ERROR_STRING, SciConstants.StringConstants.EMPTY);
						
						if(!feedValue.trim().equals(CpatToolCConstants.STRING_NULL))
						{
							feedValue = "<font color=\"red\">"+feedValue+"</font>";
						}
					}
					else if(feedValue.trim().equals(CpatToolCConstants.STRING_NULL))
					{
						feedValue = StringConstants.EMPTY;
					}	
				}
				
				if(feedValue.trim().equals(CpatToolCConstants.STRING_NULL))
				{
					feedValue = StringConstants.EMPTY;
				}
				
				html.append (StringUtils.emptyString(feedValue));			
				html.append( "</td>"
						+ "</tr>");
			}
			
			html.append(	"</tbody>"
					+ "</table>");
			
		}
		return html.toString();
	}
	
	private static String collateHtmlBodyTable(String classificationtable, String caseDataTable, String receiptSummaryTable, String localCaseId)
	{
		StringBuffer htmlString = null;
		htmlString = new StringBuffer();
		
		htmlString.append("");
		
		htmlString.append("<html>"
							+ "<body>"
								+"<div align='center'><b> Local Case Id : </b>"+localCaseId+" </div> <br />");
							
								if(!StringUtils.isNullOrEmpty(StringUtils.emptyString(classificationtable).trim()))
								{
									htmlString.append(
										"<div align='center'><label> <b>Case Classification </b></label><div> <br />"
										+classificationtable
									+"<br /><br />" );
								}
								
							htmlString.append("<div align='center'><label> <b>Case Data </b> </label><div> <br />"
									+caseDataTable
								+"<br /><br />"
								+ "<div align='center'><label><b> Receipt Summary </b></label><div> <br />"
									+receiptSummaryTable
								+"<br />"
							+ "</body>"	
						+ "</html>");
		
		
		return htmlString.toString();
	}
	
	public static void callCommonRegisterActivity(CpatToolBase oToolBase, IAction oCallbackAction, String activityType, int activityId, String activityCode, String activityDesc, Map<String, String> payload )
	{
		SciPortalWebApiResponse response = null;
		String chromeVersion = null;
		String ieVersion = null;
		ScpUser scpUser = oToolBase.scpUser;
		
		Map<String, String> browserDetails = oToolBase.browserMapDetails;
		if(browserDetails.get(CpatToolConstants.BrowserDetails.BROWSERNAME).equalsIgnoreCase(CpatToolCConstants.Browser.CHROME))
		{
			chromeVersion = browserDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
			ieVersion = StringConstants.EMPTY;
		}
		else
		{
			ieVersion = browserDetails.get(CpatToolConstants.BrowserDetails.BROWSERVERSION);
			chromeVersion = StringConstants.EMPTY;
		}
		
		response = oToolBase.scpClient.registerActivity(scpUser.getUserId(), scpUser.getPassword(), 
					scpUser.getUserProjectsList().get(0).getProjectSeqId(), 
					oToolBase.getToolDetails().getToolName(), activityType, activityId, activityCode, "",
					payload, chromeVersion, ieVersion);
		
		if(response.getStatus() == STATUS.SUCCESS)
		{
			activityId = Integer.parseInt(response.getData().toString());
			
			if(activityId > 0)
			{
				oToolBase.activityId = activityId;			
			}
			
			else
			{
				JOptionPane.showMessageDialog(null, "Invalid Activity Id");
				quitWebDriver();
				oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
				return;
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Error: " + response.getCode() + " - " + response.getMessage());
			quitWebDriver();
			oCallbackAction.performAction(CPAT_ACTION.COLLECT_USER_INPUTS);
			return;
		}
	}
	
	public static void inputsCollectedActionListener(CpatToolBase oToolBase, IAction oCallbackAction, JFrame frame, FormSubmitEvent event)
	{
		/*frame.dispose();
		oCallbackAction.performAction(CPAT_ACTION.USER_INPUTS_COLLECTED);*/
		Map<String, String> mapExtractedParameters = null;
		Map<String, String> payload = null;
		List<String> listErrors = null;
		ScpUser oScpUser = null;
		
		SciPortalWebApiResponse response = null;
		File productFile = null;
		
		int activityId = -1;
		
		oScpUser = oToolBase.scpUser;
		
		try
		{
			mapExtractedParameters = CpatToolHome.extractEventParameters(event);
			propMap	=	loadProperties(CpatToolConstants.CPAT_CONFIG);
			propMap.putAll(loadProperties(CpatToolConstants.IRT_COMMON_CONFIG));
			propMap.putAll(loadProperties(propMap.get(CpatToolHome.getPropertyNameAppendedWithZone(oScpUser.getZone(), CpatToolConstants.CONFIG_FILE_SUFFIX))));
			
			//Perform Validation
			listErrors = oToolBase.validateUserInputs(mapExtractedParameters);
			if (listErrors != null && listErrors.size() > 0)
			{
				JOptionPane.showMessageDialog(null, listErrors.get(0));
			}
			else
			{
				// Create the main Activity
				response = oToolBase.scpClient.registerActivity(oScpUser.getUserId(), oScpUser.getPassword(), 
							oScpUser.getUserProjectsList().get(0).getProjectSeqId(), oToolBase.getToolDetails().getToolName(), 
							CpatToolConstants.ACTIVITY_TYPE.TOOL_PROCESSING, activityId, 
							CpatToolConstants.ACTIVITY_CODE.TOOL_PROCESSING, "", null, "", "");
				
				if(response.getStatus() == STATUS.SUCCESS)
				{
					activityId = Integer.parseInt(response.getData().toString());
					
					// If Response code is Successful
					if(activityId > 0)
					{
						oToolBase.activityId = activityId;
						oToolBase.userInputsParams = mapExtractedParameters;
						
						productFile = new File(mapExtractedParameters.get(CpatToolCConstants.UserInputs.IRT_NONMEDICINAL_FILE));
						
						payload = new HashMap<>();
						payload.put("ENVIRONMENT", mapExtractedParameters.get(CpatToolCConstants.UserInputs.SERVER_ENVIRONMENT));
						payload.put("DESTINATION_IRT_SYSTEM_URL", 
							propMap.get(CpatToolHome.getPropertyNameAppendedWithZone(oScpUser.getZone(), CpatToolConstants.IRT_SERVER_SUFFIX)));
						payload.put("DESTINATION_SCEPTRE_SYSTEM_URL", 
							propMap.get(CpatToolHome.getPropertyNameAppendedWithZone(oScpUser.getZone(), CpatToolConstants.SCEPTRE_SERVER_SUFFIX)));
						payload.put("DESTINATION_SYSTEM_USERNAME", mapExtractedParameters.get(CpatToolCConstants.UserInputs.IRT_USERNAME));
						payload.put("DESTINATION_SYSTEM_RECEIPT_NUMBER", mapExtractedParameters.get(CpatToolCConstants.UserInputs.IRT_RECPT_NO));
						payload.put("DESTINATION_SYSTEM_PRODUCT_LIST_NAME", productFile.getName());
						payload.put("INPUT_VALIDATION_STATUS", "Success");
						
						response = oToolBase.scpClient.registerActivity(oScpUser.getUserId(), oScpUser.getPassword(),
									oScpUser.getUserProjectsList().get(0).getProjectSeqId(), oToolBase.getToolDetails().getToolName(),
									"TOOL-USER-INPUTS-COLLECTED", oToolBase.activityId,
									CPAT_ACTION.USER_INPUTS_COLLECTED.toString(), "", payload, "", "");
						
						if(response.getStatus() == STATUS.SUCCESS)
						{
							activityId = Integer.parseInt(response.getData().toString());
							
							if(activityId > 0)
							{
								frame.dispose();
								oCallbackAction.performAction(CPAT_ACTION.USER_INPUTS_COLLECTED ,  mapExtractedParameters, activityId );
							}
							else
							{
								JOptionPane.showMessageDialog(null, "Invalid Activity Id");
							}
							
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Error: " + response.getCode() + " - " + response.getMessage());
						}
						
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Invalid Activity Id");
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Error: " + response.getCode() + " - " + response.getMessage());
				}
				
			}
		}catch (Exception e) {
			log.error("User Input Collection Failed: ", e);
			JOptionPane.showMessageDialog(null, "User Input Collection Failed");
		}
	}
	
	// Excel Parser
	public static DataTable getRowDataFromExcel(String p_sProductPath)
	{
		
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		boolean bHeaderRowPresent = true;
		int nSheetIndex = 0, nHeaderRowIndex = 1;
		
		String sColumnsToCollapse = "";
	
		try
		{
			oWorksheetWrapper = parseWorkbook(p_sProductPath, bHeaderRowPresent, nSheetIndex, nHeaderRowIndex);
			
			productListDataTable = convertToDatatable(oWorksheetWrapper, sColumnsToCollapse);
			
			log.info("get row from execl method return data table col size" +productListDataTable.colCount());
			log.info("get row from execl method return data table row size" +productListDataTable.rowCount());
			
			
		}
		catch(SciException sciExcep)
		{
			log.error("Product List xlsx parsing failed: " + sciExcep);
		}
		
		return productListDataTable;
	}
	
	public static DataTable convertToDatatable(WorkbookWorkSheetWrapperDEA p_oWorksheetWrapper, String p_sColumnsToCollapse)
	{
		DataTable oDataTable = null;

		oDataTable = new DataTable();
		
		p_oWorksheetWrapper.appendDataIntoDataTable(oDataTable, false, p_sColumnsToCollapse);
		
		return oDataTable;
	}
	
	public static WorkbookWorkSheetWrapperDEA parseWorkbook(String p_sStagedFileName, boolean p_bIsHeaderPresent, int p_nSheetIndex,int headerRowIndex) throws SciException
	{
		WorkbookWorkSheetWrapperDEA oWorksheetWrapper = null;
		int nNumberOfColumns = 0;
		int nNumberOfDataRows = 0;
		Workbook oWorkbook = null;
		Sheet oSheet = null;
	//	int HeaderRowIndex=3;
		int headerFinderCounter=1;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;
		
		List<Object> listTableRow = null;
		List<Object> listHeaderRow = null;
		List<List<Object>> listDataTable = null;
		File oWorkbookFile = null;
		try
		{
			headerIndex = headerRowIndex;
			oWorkbookFile = new File(p_sStagedFileName);
			oWorkbook = WorkbookFactory.create(oWorkbookFile);
			
			oSheet = oWorkbook.getSheetAt(p_nSheetIndex);
			
			listDataTable = new ArrayList<List<Object>>();
			
			oIteratorWorkbookRows = oSheet.rowIterator();
			if (p_bIsHeaderPresent)
			{
				/*while(oIteratorWorkbookRows.hasNext() && (headerFinderCounter<HeaderRowIndex))
				{
					headerFinderCounter++;
					continue;
				}*/
				while (oIteratorWorkbookRows.hasNext())
				{
					
					if((headerFinderCounter==headerRowIndex))
					{
						
					}	
					else
					{
							oIteratorWorkbookRows.next();
							headerFinderCounter++;
							continue;
					}
					
					oSingleWorkbookRow = oIteratorWorkbookRows.next();
					listHeaderRow = new ArrayList<Object>();
					
					if(oSingleWorkbookRow!=null)
					{
						// Iterate through the cells
						for (int nRowCellIndex = 0; nRowCellIndex < oSingleWorkbookRow.getLastCellNum(); nRowCellIndex++)
						{
							oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
							listHeaderRow.add(getWorkbookCellContent(oSingleWorkbookCell));
							
							nNumberOfColumns++;
						}
					}
				break;
				}
				
			}
			else
			{
				// throw new Exception("Header row not present use case not supported as yet");
				//listErrors.add(new SciError(p_sErrorKey));
				//Ignore and continue
			}
			
			//NOTE: Iterator might have moved one workbook row if an header was expected and found 
			while (oIteratorWorkbookRows.hasNext())
			{
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				listTableRow = new ArrayList<Object>(nNumberOfColumns);
				
				// Iterate through the cells.
				if(oSingleWorkbookRow!=null)
				{
					// Iterate through the cells.
					for (int nRowCellIndex = 0; nRowCellIndex < nNumberOfColumns; nRowCellIndex++)
					{
						oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
						listTableRow.add(getWorkbookCellContent(oSingleWorkbookCell));
					}
					listDataTable.add(listTableRow);
					nNumberOfDataRows++;
				}
			}
			
			oWorksheetWrapper = new WorkbookWorkSheetWrapperDEA(oSheet.getSheetName(), p_nSheetIndex, p_bIsHeaderPresent, nNumberOfColumns, nNumberOfDataRows, 
					listHeaderRow, listDataTable);
			
		}
		catch (FileNotFoundException fnfExcep)
		{
			log.error("Error parsing file as file not found", fnfExcep);
			throw new SciException("Error parsing file as file not found", fnfExcep);
		}
		catch (InvalidFormatException invExcep)
		{
			log.error("Error parsing file as file format incorrect", invExcep);
			throw new SciException("Error parsing file as file format incorrect", invExcep);
		}
		catch (IOException ioExcep)
		{
			log.error("Error parsing file as file", ioExcep);
			throw new SciException("Error parsing file", ioExcep);
		}
		finally
		{
			try
				{
					oWorkbook.close();
				}
				catch (Exception ioExcep)
				{
					//Ignore and continue
					log.error("Error closing workbook file", ioExcep);
				}
		}
		return oWorksheetWrapper;
	}
	
	@SuppressWarnings("deprecation")
	private static Object getWorkbookCellContent(Cell oSingleWorkbookCell)
	{
		boolean bCheckIfCellContainsDateValue = false;
		DataFormatter df = new DataFormatter(true);
	
		if (oSingleWorkbookCell!=null)
		{
			switch (oSingleWorkbookCell.getCellType()) 
			{
				case Cell.CELL_TYPE_BLANK:
					return oSingleWorkbookCell.getStringCellValue();
				case Cell.CELL_TYPE_STRING:
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_NUMERIC:
					//giving wrong date
					String dateFormat="";
					if(headerIndex==1)
						dateFormat="MM/dd/yy";
					else
					 dateFormat="";
					bCheckIfCellContainsDateValue = checkIfCellContainsDateValue(df.formatCellValue(oSingleWorkbookCell), dateFormat);
					if(bCheckIfCellContainsDateValue)
					{
						return getActualFormattedDateValue(df.formatCellValue(oSingleWorkbookCell), dateFormat);
					}
					else
					{
						oSingleWorkbookCell.setCellType(Cell.CELL_TYPE_STRING);
					}
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_BOOLEAN:
					return oSingleWorkbookCell.getBooleanCellValue();
				case Cell.CELL_TYPE_FORMULA:
					return oSingleWorkbookCell.getCellFormula();
				case Cell.CELL_TYPE_ERROR:
					return null;
				default:
					return new String("");
			}
		}
		else
		{
			return null;
		}
	}
	
	private static boolean checkIfCellContainsDateValue(String p_sColumnValue, String p_inputdateformatter) 
	{
		
		try
		{
			SimpleDateFormat formatter = new SimpleDateFormat(p_inputdateformatter);
			formatter.parse(p_sColumnValue);
		}
		catch(Exception excep)
		{
			return false;
		}
		return true;
	}
	
	private  static String getActualFormattedDateValue(String p_sDateValue, String dateFormat) 
	{
		String sIncommingDateFormat = null;
		//String sIncommingDateFormat2 = null;
		String sFormattedDate = null;
		String sDateFormat = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		SimpleDateFormat formatter = null;
		
		if(p_sDateValue!=null)
		{
			if(dateFormat==null || dateFormat.equals(""))
			{
			String middleDatePart="";
			String startDatePart="";
			String endDatePart="";
			if(p_sDateValue.contains("/"))
			{
			String[] temp=p_sDateValue.split("/");
			startDatePart=temp[0];
			middleDatePart=temp[1];
			endDatePart=temp[2];
					//p_sDateValue.substring(p_sDateValue.lastIndexOf("/")+1,p_sDateValue.length());
			}
			if(p_sDateValue.contains("/"))
			{
				if(endDatePart.length()==2 )
					sIncommingDateFormat ="dd/MM/yy";
				else
					sIncommingDateFormat ="dd/MM/yyyy";
			}
			else
			sIncommingDateFormat = "dd-MMM-yyyy";//ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString());
			}
			else
				sIncommingDateFormat=dateFormat;
			sDateFormat = "dd-MMM-yyyy";//ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT.toString());
			//TODO: Check this
			try
			{
				incomingformatter = new SimpleDateFormat(sIncommingDateFormat);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
				//tempFormattedDate=new SimpleDateFormat("dd-MMM-yyyy").parse(p_sDateValue); 
			}
			catch(ParseException parseExcep)
			{
				log.error("Date formatting failed for: Date/Format - " + p_sDateValue +  "/" + sIncommingDateFormat, parseExcep);
				return "";
			}
			
			formatter = new SimpleDateFormat(sDateFormat);
			sFormattedDate = formatter.format(oFormattedDate);
		}
		else
		{
			return "";
		}
		return sFormattedDate;
	}
	
	
	//Parsed Data Table fieldValues Chack
	/**
	 * 
	 * @param obj
	 * @author gwandhare
	 */
	public static boolean isStringObjEmptyOrNull(Object obj)
	{
		if(obj != null && !StringUtils.isNullOrEmpty(obj.toString()))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param obj
	 * @author gwandhare
	 */
	public static String getDataTableStringValue(Object obj)
	{
		List<ParsedDataValue> parsedValueList = null;
		if(CpatToolCHelper.isStringObjEmptyOrNull(obj))
		{
			return SciConstants.StringConstants.EMPTY;
		}
		else
		{
			 parsedValueList = (List<ParsedDataValue>)obj;
			 if(parsedValueList.get(0) !=null )
			 {
				 if(parsedValueList.get(0).displayValue()!=null)
				 {
					 return parsedValueList.get(0).displayValue().toString();
				 }
			 }
		}
		 return SciConstants.StringConstants.EMPTY;
	}
	
	
	/**
	 * 
	 * @param parsedDataTable
	 * @param mnemonic
	 * @return Parsed Data Table column value, if column is not present or value is empty then written empty string
	 * @author gwandhare 
	 */
	public static String getParsedDataFieldValue(ParsedDataTable parsedDataTable, String mnemonic)
	{
		String value = SciConstants.StringConstants.EMPTY;
		if(parsedDataTable.isColumnPresent(mnemonic))
		{
			if(parsedDataTable.getColumnValues(mnemonic) != null && !parsedDataTable.getColumnValues(mnemonic).isEmpty())
			{
				if(parsedDataTable.getColumnValues(mnemonic).get(0) != null )
				{
					if(parsedDataTable.getColumnValues(mnemonic).get(0).displayValue() != null)
					{
						value = parsedDataTable.getColumnValues(mnemonic).get(0).displayValue().toString();
					}
				}
					
			}
		}
		return value;
	}	
	
	/**
	 * 
	 * @param DuplicateRecordFields
	 * @author gwandhare
	 * @return duplicate field record 
	 */
	public static DuplicateRecordFields getDuplicateFieldData(ParsedDataTable dataTable)
	{
		DuplicateRecordFields duplicateRecField = new DuplicateRecordFields();
		
				
		duplicateRecField.setLocalCaseId(getParsedDataFieldValue(dataTable,CpatToolCConstants.ReportSummaryTabKeys.GENINFO_LOC_CASE_ID));		
		
		duplicateRecField.setPatientInitials(getParsedDataFieldValue(dataTable,CpatToolCConstants.ReportSummaryTabKeys.PATIENT_PATIENT_INITIALS));
				
		duplicateRecField.setPatientGender(getParsedDataFieldValue(dataTable, CpatToolCConstants.ReportSummaryTabKeys.PATIENT_SEX));
		
		duplicateRecField.setPatientDOB(getParsedDataFieldValue(dataTable,CpatToolCConstants.ReportSummaryTabKeys.PATIENT_DOB));
		
		duplicateRecField.setReporterFamilyName(getParsedDataFieldValue(dataTable,CpatToolCConstants.ReportSummaryTabKeys.REPORTER_LAST));
		
		duplicateRecField.setReporterGivenName(getParsedDataFieldValue(dataTable,CpatToolCConstants.ReportSummaryTabKeys.REPORTER_FIRST));
	
		duplicateRecField.setReporterCountry(getParsedDataFieldValue(dataTable,CpatToolCConstants.ReportSummaryTabKeys.REPORTER_COUNTRY));
		
		return duplicateRecField;
	}
	
	public static String getMaxCaseVersion(SearchResults oSearchResults, String p_aerNo) 
	{
		
		String caseVersion = null;
		String resultsCaseVersion = null;
		
		for(int i=0; i<oSearchResults.resultCount(); i++)
		{
			if(p_aerNo.equals(String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER))))
			{
				resultsCaseVersion = String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.VERSION)); 
				
				if(StringUtils.isNullOrEmpty(caseVersion))
				{
					caseVersion = resultsCaseVersion;
				}
				else
				{
					if(Integer.parseInt(resultsCaseVersion) > Integer.parseInt(caseVersion))
					{
						caseVersion = resultsCaseVersion;
					}
				}
			}

		}
		
		return caseVersion;
		
	}
	public static String getCaseQueue(SearchResults oSearchResults, String p_aerNo) 
	{
		
		String caseQueue = null;
		
		for(int i=0; i<oSearchResults.resultCount(); i++)
		{
			if(p_aerNo.equals(String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.AER))))
			{
				caseQueue = String.valueOf(oSearchResults.getValue(i, SceptreConstants.SceptreSearchResultColumns.QUEUE)); 
			}

		}
		
		return caseQueue;
		
	}
	
	
	public static List<IRTCaseDocument> provideListCaseDoc(Map<String, String> testModePropMap)
	{
		IRTCaseDocument oCaseDocument = null;
		List<IRTCaseDocument> listCaseDocuments = null;
		
		Map<String , Object> mAttributeMap = null;
		
		String count = null;
		
		int cnt = 0;
		
		File file = null;
		
		String fileName = null, filePath = null;
		
		String strVersion = null;
		
		int index = 0;
		
		listCaseDocuments = new ArrayList<>();
	    if(testModePropMap != null)
	    {
	    	count = testModePropMap.get("Cpat.Sourcedocument.Count");
	    	
	    	cnt = Integer.parseInt(count);
	    	
	    	for(int cntNum =1 ; cntNum<=cnt; cntNum++)
	    	{
	    		oCaseDocument = new IRTCaseDocument();
	    		
	    		mAttributeMap = new HashMap<>();
	    		
	    		fileName = testModePropMap.get("Cpat.Sourcedocument."+cnt);
	    		
	    		strVersion = testModePropMap.get("Cpat.Sourcedocument."+cnt+".version");
	    		
	    		file = new File(fileName);
	    		
	    		oCaseDocument.setCaseFile(file);
	    		
	    		index = fileName.lastIndexOf("\\");
	    		
	    		fileName = fileName.substring(index+1, fileName.length());
	    		
	    		mAttributeMap.put(SceptreConstants.DownloadSD.DOCUMENT_NAME,fileName);
	    		
	    		mAttributeMap.put(SceptreConstants.DownloadSD.CASE_VERSION, strVersion);
	    		
	    		oCaseDocument.setCaseFilename(fileName);
	    		
	    		listCaseDocuments.add(oCaseDocument);
	    	
	    	}
		}
		
		return listCaseDocuments;
	}

	/**
	 * The method will check from the list
	 * of emea countries whether the passed country
	 * is in the list of emea countries or not.
	 * @param p_countryname The name of the country
	 * 		  to be checked	
	 * @return true if the passed country is in the
	 * 		   list of emea countries and false if
	 * 		   the property map is null, empty 
	 *         or the emea.countries property
	 *         not maintained in the property file
	 */

	public static boolean isCountryEMEA(String p_countryname) {
		boolean	l_iscountryemea = false;
		List<String> l_emeacountrylist = null;
		
		if (propMap != null && propMap.size()>0 && propMap.get(IRTConstants.EMEA_COUNTRIES) != null && (!StringUtils.isNullOrEmpty(p_countryname))) {
			l_emeacountrylist =	Arrays.asList(
									propMap.get(IRTConstants.EMEA_COUNTRIES).toUpperCase().split(",")
								);
			l_iscountryemea	=	l_emeacountrylist.contains(p_countryname.toUpperCase());
		}		
		return l_iscountryemea;
	}
	
	/*public static void initializeSceptreMap(String aerNo) {
		oAppWriter.initializeSceptreMap(aerNo);
	}*/

	/**
	 * The method can be used to determine
	 * whether a case is serious or not on
	 * the basis of case notes and verbatim.
	 * If the case notes and verbatim contains
	 * one of the serious keyword maintained at 
	 * the property level then the case is
	 * serious otherwise case is non-serious.
	 * @param p_sourcedocumentdata The source document
	 * 	      for a receipt or the case.
	 * @param p_keywords List of serious keywords
	 * @param p_delimiter The delimiter using which
	 *        the keywords are separated
	 * @return True if the case is serious otherwise
	 *         false
	 */
	public static boolean isCaseSerious (ParsedDataTable p_sourcedocumentdata, 
		String p_keywords, String p_delimiter
	) {
		boolean	l_iscaseserious = false;
		for (String l_data : CpatToolCHelper.getCaseNotesAndVerbatim(p_sourcedocumentdata)) {
			for (String l_seriouswords : CpatToolHome.getListOfKeywords(p_keywords, p_delimiter)) {
				if (CpatToolHome.isKeywordFound(l_data, l_seriouswords)) {
					l_iscaseserious = true;
					break;
				}
			}
		}
		return l_iscaseserious;
	}

	/**
	 * This method is used to combine the
	 * case notes and verbatim found in the
	 * source document for a receipt or case.
	 * @param p_sourcedocumentdata The source document
	 * 	      for a receipt or the case.
	 * @return List of all the case notes and verbatim
	 */
	public static List<String> getCaseNotesAndVerbatim (ParsedDataTable p_sourcedocumentdata) {
		String l_casenoteskey =	null;
		int	l_casenotescount = 0;
		List<String> l_verbatimandnotes	= new ArrayList<>();
		if (p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.CASE_NOTES_COUNT)) {
			l_casenoteskey	=	(String)p_sourcedocumentdata.getColumnValues(
									CpatToolCConstants.NarrativeKeys.CASE_NOTES_COUNT
								).get(0).displayValue();
		}
		l_casenotescount = StringUtils.isNullOrEmpty(l_casenoteskey) ? 0 : Integer.parseInt(l_casenoteskey);
		if (l_casenotescount == 0) {
			l_verbatimandnotes.add(
				(String)p_sourcedocumentdata.getColumnValues(
					CpatToolCConstants.NarrativeKeys.CASE_NOTES
				).get(0).displayValue()
			);
		} else {
			for (int l_i=1; l_i <= l_casenotescount; l_i++) {
				l_verbatimandnotes.add(
					(String)p_sourcedocumentdata.getColumnValues(
						CpatToolCConstants.NarrativeKeys.CASE_NOTES + "_" + l_i
					).get(0).displayValue()
				);
			}
		}
		l_verbatimandnotes.add(
			(String)p_sourcedocumentdata.getColumnValues(
				CpatToolCConstants.NarrativeKeys.VERBATIM_ENGLISH_LANGUAGE
			).get(0).displayValue()
		);
		return l_verbatimandnotes;
	}
	
	public static void showDuplicateRecordSummaryScreen(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, Object> params) 
	{			
		new CpatDuplicateRecordSummaryScreen(oToolBase, oCallbackAction, params);
	}

	public static void quitWebDriver()
	{
		oAppWriter.quitWebDriver();
		CpatCasePreviewScreen.disposePreviewScreenView();
	}

	
}
