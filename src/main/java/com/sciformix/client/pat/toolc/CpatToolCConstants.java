package com.sciformix.client.pat.toolc;

public class CpatToolCConstants {
	
	public static final String DUPLICATE_IRT_RECORD_LIST = "irtreclist";
	public static final String DUPLICATE_SCEPTRE_RECORD_LIST = "sceptrereclist";
	//public static final String CONSTANT_SFE	=	"SFE";
	//public static final String RECEIPTNO_REGEX	=	"SFE\\d{7}";
	public static final String RECEIPTNO_REGEX	=	"\\d{7}";
	public static final String DATE_FORMAT = "dd-MMM-yyyy";
	public static final String KEY_DOCTYPE = "irt.doc.type";
	public static final String STRING_NULL = "null";
	public static final String KEY_FEED_CLASSIFICATION_TAB = "irt.doc.feedclassificationtab";
	public static final String KEY_ENABLE_DATAENTRY = "irt.doc.enableDataEntry";
    public static final String SEE_NARRATIVE_CONSTANT = "See Narrative";
    public static final String PREFIX_DATAENTRY = "dataentry.";
    public static final String PREFIX_CASEDATA = "casedata.";
	
	public static final String[] PRODUCT_FILE_COLUMN_NAMES	=	{"GENERIC_NAME","REG_CLASS","PRODUCT_FORM"};	
	
	public class UI
	{
		public static final String USER_ID= "readdata";
		public static final String REPORT_SUMMARY= "reportsummary";
		public static final String CASE_DATA = "casedata";
		public static final String RECPT_NO	="receiptno";
		public static final String CASE_TYPE	="casetype";
		public static final String FOLLOE_UP_AER	="followupaer";
		
	}
	
	public class ResultSummaryUI
	{
		public static final String CLASSIFICATION	="Classification";
		public static final String FOLLOW_UP_AER	="Follow Up Aer No";
		public static final String VERSION_TYPE	="Version Type";
	}
	
	public class Action
	{
		public static final String READ_DATA= "readdata";
		public static final String WRITE_DATA = "writedata";
	}
	
	public class Tool
	{
		public static final String TOOL_TITLE						=				"Sciformix I-PAT agXchange";
		public static final String TOOL_NAME						=				"CPAT.C";
		public static final String TOOL_DISPLAY_NAME				=				"Sciformix C-PAT Tool C";
		public static final String TOOL_VERSION						=				"CPAT.C.01.00";
		public static final String IRT_USER_INPUTS_SCREEN			=				"IrtLogin.html";				
		public static final String TOOL_DESCRIPTION					=				"Intake Process Assist Tool  for agXchange IRT Database";
		public static final String TOOL_HEADING						=				"Sciformix I-PAT agXchange (Tool C)";
	}
	
	public class UserInputs
	{
		public static final String IRT_USERNAME						=				"IRT_USERNAME";
		public static final String IRT_RECPT_NO						=				"IRT_RECEIPT_NO";
		public static final String IRT_SOURCEDOCUMENT_FILE			=				"IRT_SOURCE_DOCUMENT";
		public static final String IRT_NONMEDICINAL_FILE			=				"NONMEDICINAL_PRODUCTLIST";
		public static final String SERVER_ENVIRONMENT				=				"IRT_SERVER_ENVIRONMENT";
		public static final String TOOL_ACTION						=				"IRT_TOOL_ACTION";
		
	}
	
	public class CaseDetails
	{
		public static final String LOCAL_CASE_ID 					=				"SCP_LOCAL_CASE_ID"				;
		public static final String CASE_TYPE						=				"CASE_TYPE"						;
		public static final String CASE_VERSION						=				"CASE_VERSION"					;
		public static final String CASE_QUEUE						=				"CASE_QUEUE"					;
	}
	
	public class CaseType
	{
		public static final String INITIAL							=				"INITIAL"						;
		public static final String FOLLOWUP							=				"FOLLOWUP"						;
		public static final String DUPLICATE						=				"DUPLICATE"						;
	}
	
	public class CaseDataTabKeys
	{
		public static final String SERIOUS_ASSESSMENT_REPORT_TYPE="TypeofReport";
		public static final String SERIOUS_ASSESSMENT_REPORT_SERIOUSNESS="Serious";
		public static final String SERIOUS_ASSESSMENT_DEATH="ResultedInDeath";
		public static final String SERIOUS_ASSESSMENT_LIFE_THREATENING="LifeThreatening";
	}
	
	public class ReportSummaryTabKeys
	{
		public static final String  GENINFO_REPORT_TYPE="TypeofReport";
		public static final String  GENINFO_LOC_CASE_ID="SafetyReportID";
		public static final String  GENINFO_INIT_REC_DATE="FirstReceivedDate";
		public static final String  GENINFO_LAST_REC_DATE="DateOfRecentInfo";
		public static final String  GENINFO_COUNTRY_OF_OCCUR="OccurCountry";

		public static final String SERUOUSNESS_CRIT_DEATH="ResultedInDeath";
		public static final String SERUOUSNESS_CRIT_BIRTH_DEFECT="CongenitalAnomalybirthdefect";
		public static final String SERUOUSNESS_CRIT_DISABILITY="DisablingIncapacitating";
		public static final String SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION="OtherMedicallyImportantCondition";
		public static final String SERUOUSNESS_CRIT_LIFE_THETENING="LifeThreatening";
		public static final String SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED="CausedProlongedHospitalization";

		public static final String ADD_REF_COMP_UNIT="SenderIdentifier";
		public static final String ADD_REF_OTHER_INDT_REF="Operating Company";
		public static final String ADD_REF_OTHER_INDT_NO="OtherCaseIdentifiersInPreviousTransmission";
	
		public static final String SUSP_DRUG_PRODUCT_FLAG="Test_DrugCharacterization";
		public static final String SUSP_DRUG_PREF_PROD_DESC="Drug_MedicinalProductName"; 
		public static final String SUSP_DRUG_FORMULATION="Drug_PharmaceuticalForm";
		public static final String SUSP_DRUG_ACTION_WITH_DRUG="Drug_ActionsTakenWithDrug";
		
		public static final String DRUG_THERAPY_ROUT_ADMIN="Drug_RouteOfAdministration";

		public static final String ADVERSE_EVENT_REPORTED_ITEM="Reaction_ReactionEventAsReportedByPrimarySource";
		public static final String ADVERSE_EVENT_SERIOUS="Reaction_TermHighlightedBytheReporter";
		public static final String ADVERSE_EVENT_OUTCOME="Reaction_Outcome";
		
		public static final String PATIENT_PATIENT_INITIALS="PatientInitial";
		public static final String PATIENT_SEX="Patient_Sex";
		public static final String PATIENT_AGE_GROUP="Patient_AgeGroup";
		public static final String PATIENT_DOB="Patient_DateOfBirth";
		public static final String PATIENT_AGE_EVENT="Patient_OnsetAge";
		
		public static final String PROTOCOL_PROTOCOL_NO="Reporter_SponsorStudyNo";
		
		public static final String REPORTER_QUALIFICATION="Reporter_Qualification";
		public static final String REPORTER_DO_NOT_REPORT_TIME="No";
		public static final String REPORTER_FIRST="Reporter_GivenName";
		public static final String REPORTER_LAST="Reporter_FamilyName";
		public static final String REPORTER_CITY="Reporter_City";
		public static final String REPORTER_STATE="Reporter_State";
		public static final String REPORTER_COUNTRY="Reporter_Country";
		public static final String REPORTER_ZIPCODE="Reporter_Postcode";
		
		public static final String LITERATURE_ART_TITLE="Reporter_LiteratureReferences";
		public static final String LITERATURE_JOURNAL_TITLE="Reporter_LiteratureReferences";
				
		public static final String DUPLICATE_NUMBER = "DuplicateNumber";
	}
	
	public class Browser
	{
		public static final String CHROME							=				"CHROME"						;
	}
	public class CaseIdTabKeys
	{
		public static final String SOURCES_A1112					=				"SourcesA1112"					;
		public static final String SAFETY_REPORT_ID					=				"SafetyReportID"				;
		public static final String PRIMARY_SOURCE_COUNTRY			=				"PrimarySourceCountry"			;
		public static final String OCCUR_COUNTRY					=				"OccurCountry"					;
		public static final String FIRST_RECEIVED_DATE 				= 				"FirstReceivedDate"				;
		public static final String DATE_OF_RECENT_INFO				=				"DateOfRecentInfo"		 		;
		public static final String TRANSMISSION_DATE				=				"TransmissionDate"				;
		public static final String REPORT_TYPE 						=				"TypeofReport"					;
		public static final String MESSAGE_DATE 					= 				"MessageDate"					;
		public static final String OTHER_CASE_IDENTIFIERS			=				"OtherCaseIdentifiersInPreviousTransmission" ;
				
	}
	
	public class PatientTabKeys
	{
		public static final String REPORTER_POSTCODE				=				"Reporter_Postcode"             ;
		public static final String PATIENT_INITIALS					=				"PatientInitial"				;
		public static final String PATIENT_DATE_OF_BIRTH			=				"Patient_DateOfBirth"			;
		public static final String PATIENT_HEIGHT					=				"Patient_Heightcm"              ;
		public static final String PATIENT_WEIGHT					=				"Patient_Weightkg"              ;
		public static final String PATIENT_LMP_DATE					=				"Patient_LMPdate"               ;
		public static final String PATIENT_GPMEDICAL_RECORD_NO		=               "Patient_GPmedicalRecordNo"     ;
		public static final String PATIENT_SPECIALIST_RECORD_NO     =               "Patient_SpecialistRecordNo"    ;
		public static final String PATIENT_HOSPITAL_RECORD_NO       =               "Patient_HospitalRecordNo"      ;
		public static final String PATIENT_GENDER 					= 				"Patient_Sex"         			;
		public static final String PATIENT_ONSET_AGE				=				"Patient_OnsetAge";
		public static final String PATIENT_AGE_GROUP				=				"Patient_AgeGroup";
	}
	
	public class PMHTabKeys
	{
		public static final String PMH_EPISODE_NAME                 =               "PMH_EpisodeName"               ;
		public static final String PMH_START_DATE                   =       		"PMH_StartDate"                 ;
		public static final String PMH_END_DATE                     =     			"PMH_EndDate"                   ;
		public static final String PMH_CONTINUING                   =       		"PMH_Continuing"                ;
		public static final String PMH_COMMENTS						= 				"PMH_Comments"					;
	}
	
	public class PDTTabKeys
	{
		public static final String PDT_START_DATE                   =       		"PDT_Startdate"                 ;
		public static final String PDT_END_DATE                     =     			"PDT_Enddate"                   ;
		public static final String PDT_INDICATION                   =       		"PDT_Indication"                ;
		public static final String PDT_INDICATION_MEDRA_VERSION     =               "PDT_IndicationMedDRAversion"   ;
		public static final String PDT_REACTION                     =     			"PDT_Reaction"                  ;
		public static final String PDT_REACTION_MEDRA_VERSION       =               "PDT_ReactionMedDRAversion"     ;
	}
	
	public class DrugTabKeys
	{
		public static final String DRUG_PARENT_ROUTE_OF_ADMIN       =               "Drug_ParentRouteOfAdmin"       ;
		public static final String DRUG_DATE_START_DRUG             =      			"Drug_StartDateOfDrug"          ;
		public static final String DRUG_lAST_ADMIN                  =    			"Drug_EndDate"                  ;
		public static final String DRUG_DURATION_OF_DRUG_ADMIN      =               "Drug_DurationOfDrugAdmin"      ;
		public static final String DRUG_DOSAGE_TEXT                 =         		"Drug_DosageText"               ;
		//commented by shambhu as duplicate values assign for same key
		//public static final String DRUG_START_DATE                  =        		"Drug_StartDateOfDrug"          ;
		public static final String DRUG_END_DATE                    =      			"Drug_EndDate"                  ;
		public static final String DRUG_BATCH_NO                    =      			"Drug_Batch_LotNo"              ;
		public static final String DRUG_START_PERIOD                =          		"Drug_StartPeriod"              ;
		public static final String DRUG_LAST_PERIOD                 =         		"Drug_LastPeriod"               ;
		public static final String DRUG_RESULT                      =    			"DRR_Result"                    ;
		public static final String DRUG_COUNTRY_OBTAINED            =               "Drug_CountryDrugObtained"      ;
		public static final String DRUG_NAME_OF_HOLDER_APPLICANT    =               "Drug_NameOfHolder_Applicant"   ;
		public static final String DRUG_MEDICIANL_PRODUCT_NAME		=				"Drug_MedicinalProductName"		;

		/**************code by shammbhu*******************/
		public static final String DRUG_ROUTE_OF_ADMINISTRATION		=				"Drug_RouteOfAdministration"	;
		public static final String DRUG_DOSE						=				"Drug_Dose"						;
		public static final String DRUG_DRUGINDICATION				=				"Drug_DrugIndication"			;
		public static final String DAYS								=				"Days"							;
		public static final String DRUG_USE_FOR_UNKNOWN_INDICATION	=				"Drug use for Unknown Indication";
		public static final String PRODUCT_USED_FOR_UNKNOWN_INDICATION=				"Product used for Unknown Indication";
	}
	
	public class ReporterTabKeys
	{
		public static final String REPORTER_TITLE                   =       		"Reporter_title"                ;
		public static final String REPORTER_MIDDLE_NAME             =             	"Reporter_MiddleName"           ;
		public static final String REPORTER_ORGANIZATION            =              	"Reporter_Organization"         ;
		public static final String REPORTER_DEPARTMENT              =            	"Reporter_Department"           ;
		public static final String REPORTER_STREET                  =        		"Reporter_Street"               ;
		
		public static final String REPORTER_GIVEN_NAME				=				"Reporter_GivenName"			;
		public static final String REPORTER_FAMILY_NAME				=				"Reporter_FamilyName"			;
		
		
		public static final String SENDER_TITLE                      =    			"Sender_Title"                   ;
		public static final String SENDER_GIVEN_NAME                =          		"Sender_GivenName"              ;
		public static final String SENDER_FAMILY_NAME               =           	"Sender_FamilyName"             ;
		
		/*****code by shambhu****/
		public static final String REPORTER_QUALIFICATION           =           	"Reporter_Qualification"        ;
		public static final String NARRATIVE_REPORTERTYPE           =           	"Narrative_ReporterType"        ;
		public static final String REPORTER_CITY 					=           	"Reporter_City"			        ;
		public static final String REPORTER_STATE 					=           	"Reporter_State"		        ;
		public static final String REPORTER_POSTCODE				=           	"Reporter_Postcode"		        ;
		public static final String REPORTER_COUNTRY					=           	"Reporter_Country"		        ;
		public static final String PHONE 							= 				"Sender_Telno"					;
		public static final String FAX								=				"Sender_Faxno"					;
		public static final String EMAIL_ADDRESS					=				"Sender_Emailaddress"			;
		public static final String SALUTATION 						= 				"Receiver_Title"				;
		
	}
	
	public class DiagnosticTabKeys
	{
		public static final String TEST_TEST_NAME                   =       		"Test_TestName"                 ;
		public static final String TEST_TEST_UNIT                   =       		"Test_TestUnit"                 ;
		public static final String TEST_NORMAL_LOW_RANGE            =              	"Test_NormalLowRange"           ;
		public static final String TEST_NORMAL_HIGH_RANGE           =               "Test_NormalHighRange"          ;
		public static final String TEST_TEST_DATE                   =       		"Test_TestDate"                 ;
		public static final String TEST_TEST_RESULT                 =         		"Test_TestResult"               ;
		public static final String TEST_MORE_INFO                   =       		"Test_MoreInfo"					;
	}
	public class ReactionTabKeys
	{
		public static final String REACTION_OUTCOME     			 =               "Reaction_Outcome"   			;
		public static final String NARRATIVE_REACTIONOUTCOME		 =               "Narrative_ReactionOutcome"   	;
		public static final String REACTION_STARTDATE				 =               "Reaction_StartDate"		   	;
		public static final String NARRATIVE_REACTION_START_DATE	 =               "Narrative_ReactionStartDate" 	;
		public static final String REACTION_ENDDATE	 				 =               "Reaction_EndDate"			 	;
		public static final String NARRATIVE_REACTION_STOPDATE	 	 =               "Narrative_ReactionStopDate" 	;
		public static final String REACTION_DURATION			 	 =               "Reaction_Duration"		 	;
		public static final String DAYS								 =				 "Days"							;
	}
	public class NarrativeKeys
	{
		public static final String VERBATIM_ENGLISH_LANGUAGE		=				"Narrative_VerbatimEnglishLanguage"	;
		public static final String PRIMARY_CONSUMER_COUNTRY			=				"Narrative_PrimaryConsumerCountry"	;
		public static final String EVENT_COUNTRY_OCCURED 			= 				"Narrative_EventCountryOccurred" 	;
		public static final String REPORT_TYPE 						= 				"Narrative_ReportType"				;
		public static final String PATIENT_INITIALS 				= 				"Narrative_PatientInitials"			;
		public static final String PATIENT_GENDER 					= 				"Narrative_PatientGender"			;
		public static final String PATIENT_HEIGHT 					= 				"Narrative_PatientHeight"			;
		public static final String PATIENT_WEIGHT 					= 				"Narrative_PatientWeight"			;
		public static final String PATIENT_DOB 						= 				"Narrative_PatientDOB"				;
		public static final String PATIENTS_ALLERGIES_DESC			= 				"Narrative_PatientAllergiesDescription"			;
		public static final String RELEVANT_MEDICAL_HISTORY_DESC    =               "Narrative_RelevantMedicalHistoryDescription"	;
		public static final String RELEVANT_MEDICAL_HISTORY         =               "Narrative_RelevantMedicalHistory"  ;
		public static final String PRODUCT_START_DATE				=				"Narrative_ProductStartDate"        ;
		public static final String PRODUCT_END_DATE					=				"Narrative_ProductStopDate"			;
		public static final String PRODUCT_NAME						=				"Narrative_ProductName"				;
		public static final String REPORTED_INDICATION				=				"Narrative_NarrativeIndication"		;
		public static final String ACTIVITY_AWARENESS_DATE			=				"Narrative_AwarenessDate"			;
		public static final String RECEIPT_DATE						=				"Narrative_ReceiptDate"				;
		public static final String RECEIVE_DATE						=				"Narrative_ReceiveDate"				;
		public static final String NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION=		"Narrative_PatientRouteofAdministration"		;
		public static final String NARRATIVE_PRODUCT_STOP_DATE		=				"Narrative_ProductStopDate"		;
		public static final String NARRATIVE_PRODUCT_START_DATE		=				"Narrative_ProductStartDate"	;
		public static final String NARRATIVE_NARRATIVE_INDICATION	=				"Narrative_NarrativeIndication"	;
		public static final String NARRATIVE_PRODUCT_LOT_NUMBER		=				"Narrative_ProductLotNumber"	;
		public static final String NARRATIVE_PRODUCT_EXPIRY_DATE	=				"Narrative_ProductExpiryDate"	;
		public static final String NARRATIVE_EXTERNAL_SOURCE		=				"Narrative_ExternalSource";
		public static final String NARRATIVE_EXTERNAL_SOURCE_COUNT	=				"Narrative_ExternalSource_Count";
		public static final String NARRATIVE_EXTERNAL_SOURCE_NUMBER	=				"Narrative_ExternalSourceNumber";
		public static final String NARRATIVE_PRODUCTNAME			=				"Narrative_ProductName"			;
		public static final String NARRATIVE_PRODUCT_CODE			=				"Narrative_ProductCode"			;
		public static final String NARRATIVE_DATE_OF_HOSP_ADMISSION=				"Narrative_DateOfHospitalizationAdmission";
		public static final String NARRATIVE_DATE_OF_DISCHARGE		=				"Narrative_DateOfHospitalizationDischarge";
		public static final String NARRATIVE_DEATH					=				"Narrative_Death";
		public static final String NARRATIVE_DATE_OF_DEATH_AND_REPORTED_CAUSE_OF_DEATH
																	=				"Narrative_DateofDeathAndReportedCauseOfDeath";
		public static final String NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
																	=				"Narrative_EventAssociatedWithABirthDefect";
		public static final String NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
																	=				"Narrative_ReportedCauseofBirthDefect";
		public static final String NARRATIVE_ADMITTED_TO_HOSPITAL_RELATED_TO_EVENT
																	=				"Narrative_AdmittedToHospitalRelatedToTheEvent";
		public static final String NARRATIVE_EVENT_ASSOCIATE_WITH_MISSCARRIGE
																	=				"Narrative_IsEventAssociatedWithAMiscarriage";
		public static final String NARRATIVE_DATE_AND_REPORTED_CAUSE_OF_MISSCARRIGE
																	=				"Narrative_DateandReportedCauseofMiscarriage";
		
		public static final String ACTIVITY_AWARENESS_DATE_COUNT			=		"Narrative_AwarenessDate_Count"	;
		public static final String GCC_PLATFORM_CASE_NUMBER			=				"Narrative_GCCPlatformCaseNumber";
		public static final String LINKED_REPORT_NUMBER				=				"LinkedReportNo";
		public static final String CASE_NOTES						=				"Narrative_CaseNotesEnglishLanguage";
		public static final String CASE_NOTES_COUNT					=				"Narrative_CaseNotesEnglishLanguage_Count";
		public static final String NARRATIVE_PRODUCT_REGULATORY_CLASS		=		"Narrative_ProductRegulatoryClass";
	}
	
	public class DownloadSD
	{
		public static final String E2B_DOCUMENT_NAME_VALUE			=				"E2B_PDF_report"				;
		public static final String DOCUMENT_TYPE_VALUE				=				"IRT Supporting"				;
	}
	
	public class Header
	{
		public static final String MESSAGE_NUMBER = "MessageNumber";
		public static final String SENDER_IDENTIFIER = "SenderIdentifier";
	}
	public class FieldLogic
	{
		public static final String COMPANY_UNIT = "PQMS";
		public static final String PRODUCT_REGULATORY_CLASS = "OTC";
		
		public static final String PRODUCT_LIST_APPROVAL_NO = "APPROVAL_NUMBER";
		public static final String PRODUCT_LIST_GENERIC_NAME = "GENERIC_NAME";
		public static final String PRODUCT_LIST_PRODUCT_FORM = "PRODUCT_FORM";
		
	}
	
	public static class Tabs
	{
		public static final String RECEIPT_SUMMARY					=				"Receipt Summary"				;
		public static final String CLASSIFICATION					=				"Classification"				;
		public static final String CASE_DATA						=				"Case Data"						;
		public static final String ACTIVITY_LOG						=				"Activity Log"					;
		public static final String NOTOFICATIONS					=				"Notifications"					;
		public static final String DOCUMENTS						=				"Documents"						;
	}
	
	public static class Fields {
		public static final String TAB								=				"TAB"							;
		public static final String FIELD_COUNT						=				"FIELD_COUNT"					;
		public static final String FIELD_LIST						=				"FIELD_LIST"					;
		public static final String SCIFORMIX_ACTIVITY_ID			=				"SCIFORMIX_ACTIVITY_ID"			;
	}
		
}
