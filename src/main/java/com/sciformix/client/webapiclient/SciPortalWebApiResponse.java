package com.sciformix.client.webapiclient;

public class SciPortalWebApiResponse {

	public static enum STATUS {
		SUCCESS, FAILURE;
	}

	private STATUS status = null;
	private int code = -1;
	private String message = null;
	private Object data = null;

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "SciPortalWebApiResponse [status=" + status + ", code=" + code + ", message=" + message + ", data="
				+ data + "]";
	}

}
