package com.sciformix.client.webapiclient;

import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.webapiclient.utils.SciPortalWebApiRequestCreator;
import com.sciformix.client.webapiclient.utils.WebApiConnnectivityUtil;

public class SciPortalWebApiClient {
	
	private static Logger logger = LoggerFactory.getLogger(SciPortalWebApiClient.class);
	
	private static final String KEY_ZONE_TEST = "sciportal.webapiclient.zone.test";
	private static final String KEY_ZONE_PREPROD = "sciportal.webapiclient.zone.preprod";
	private static final String KEY_ZONE_PROD = "sciportal.webapiclient.zone.prod";
	private static final String KEY_ZONE_DISCONNECTED = "sciportal.webapiclient.zone.disconnected";
	private static final String KEY_JAR_HASH = "sciportal.webapiclient.jar.hash";
	
	public static enum OPERATING_ZONE {
		TEST, PREPRODUCTION, PRODUCTION, DISCONNECTED;
	}

	private static SciPortalWebApiClient s_oInstance = null;

	private String keyId = null;
	private String toolName = null;
	private String toolVersion = null;
	private static Map<String, String> propsMap = null;

	public static Map<String, String> getPropsMap() {
		return propsMap;
	}

	private OPERATING_ZONE m_eCurrentOperatingZone = null;

	/**
	 * Interested keys in the Properties file have the prefix
	 * 'sciportal.webapiclient.'
	 **/
	private SciPortalWebApiClient(String toolName, String toolVersion, String keyId, String zone) {
		this.toolName = toolName;
		this.toolVersion = toolVersion;
		this.keyId = keyId;

		// Client caller has to provide the environment i.e TEST, PREPROD, PROD, DISCONNECTED
		if (zone.equalsIgnoreCase(propsMap.get(KEY_ZONE_TEST))) {
			m_eCurrentOperatingZone = OPERATING_ZONE.TEST;
		} else if (zone.equalsIgnoreCase(propsMap.get(KEY_ZONE_PREPROD))) {
			m_eCurrentOperatingZone = OPERATING_ZONE.PREPRODUCTION;
		} else if (zone.equalsIgnoreCase(propsMap.get(KEY_ZONE_PROD))) {
			m_eCurrentOperatingZone = OPERATING_ZONE.PRODUCTION;
		} else if (zone.equalsIgnoreCase(propsMap.get(KEY_ZONE_DISCONNECTED))) {
			m_eCurrentOperatingZone = OPERATING_ZONE.DISCONNECTED;
		}

	}

	public static SciPortalWebApiClient create(Properties props, String toolName, String toolVersion, String keyId,
			String zone) {
		String jarHash = null;
		if (s_oInstance == null) {
			propsMap = WebClientUtils.loadProperties(props);
			s_oInstance = new SciPortalWebApiClient(toolName, toolVersion, keyId, zone);
			
			// Will work only when the JAR is run and not through normal code run in eclipse.
			jarHash = WebClientUtils.generateHashForClientTool();
			
			// Below IF code only for testing purpose and needs to be removed when deploying to prod.
			if(jarHash == null){
				jarHash = "TEST_HASH";
			}
			propsMap.put(KEY_JAR_HASH, jarHash);
		}
		return s_oInstance;
	}

	public SciPortalWebApiResponse getProjects(String userId, String password) {
		SciPortalWebApiResponse response = new SciPortalWebApiResponse();
		SciPortalWebApiRequest request = null;

		try {
			request = SciPortalWebApiRequestCreator.createRequest(SciPortalWebApiRequestCreator.API_GET_PROJECTS,
					toolName, toolVersion, "", "");
			request.setKeyId(keyId);
			request.setUserId(userId);
			request.setPassword(password);
			request.setClientIqHash(propsMap.get(KEY_JAR_HASH));

			response = WebApiConnnectivityUtil.processRequest(m_eCurrentOperatingZone, request);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return response;
	}

	public SciPortalWebApiResponse registerActivity(String userId, String password, int projectSeqId,
			String activityApp, String activityType, int activityId, String activityCode, String activityDesc,
			Map<String, String> payload, String browserVersionChrome, String browserVersionIE) {
		SciPortalWebApiResponse response = new SciPortalWebApiResponse();
		SciPortalWebApiRequest request = null;

		try {
			request = SciPortalWebApiRequestCreator.createRequest(SciPortalWebApiRequestCreator.API_REGISTER_ACTIVITY,
					toolName, toolVersion, browserVersionChrome, browserVersionIE);
			request.setKeyId(keyId);
			request.setUserId(userId);
			request.setPassword(password);
			request.setClientIqHash(propsMap.get(KEY_JAR_HASH));
			request.setProjectSeqId(projectSeqId);
			request.setActivityApp(activityApp);
			request.setActivityType(activityType);
			request.setActivityId(activityId);
			request.setActivityCode(activityCode);
			request.setActivityDesc(activityDesc);
			request.setPayload(payload);

			response = WebApiConnnectivityUtil.processRequest(m_eCurrentOperatingZone, request);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return response;
	}

	public SciPortalWebApiResponse exportTemplate(String userId, String password, int projectSeqId, int templateId, String browserVersionChrome, String browserVersionIE) {
		SciPortalWebApiResponse response = new SciPortalWebApiResponse();
		SciPortalWebApiRequest request = null;

		try {
			request = SciPortalWebApiRequestCreator.createRequest(SciPortalWebApiRequestCreator.API_EXPORT_TEMPLATE,
					toolName, toolVersion, browserVersionChrome, browserVersionIE);
			request.setKeyId(keyId);
			request.setUserId(userId);
			request.setPassword(password);
			request.setClientIqHash(propsMap.get(KEY_JAR_HASH));
			request.setProjectSeqId(projectSeqId);
			request.setTemplateId(templateId);

			response = WebApiConnnectivityUtil.processRequest(m_eCurrentOperatingZone, request);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return response;
	}

}
