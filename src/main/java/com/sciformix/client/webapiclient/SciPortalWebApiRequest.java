package com.sciformix.client.webapiclient;

import java.io.IOException;
import java.util.Map;

import com.sciformix.client.webapiclient.utils.ClientDetails;
import com.sciformix.client.webapiclient.utils.ClientSystemDetails;
import com.sciformix.client.webapiclient.utils.PublicKeyCrypto;
import com.sciformix.client.webapiclient.utils.SciPortalWebApiRequestCreator.ApiRequest;

public class SciPortalWebApiRequest
{

	private ApiRequest m_oApiRequest = null;
	private String keyId = null;
	private String userId = null;
	private String password = null;
	private int projectSeqId = -1;
	private String activityApp = null;
	private String activityType = null;
	private int activityId = -1;
	private String activityCode = null;
	private String activityDesc = null;
	private String payload = null;
	private int templateId = -1;
	private String clientIqHash = null;

	private ClientDetails clientDetails = null;
	private ClientSystemDetails clientSystemDetails = null;

	
	public SciPortalWebApiRequest(ApiRequest p_oApiRequest)
	{
		m_oApiRequest = p_oApiRequest;
	}
	
	public ApiRequest getApiRequest()
	{
		return m_oApiRequest;
	}
	
	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = encryptPassword(password);
	}
	
	public int getProjectSeqId() {
		return projectSeqId;
	}

	public void setProjectSeqId(int projectSeqId) {
		this.projectSeqId = projectSeqId;
	}

	public String getActivityApp() {
		return activityApp;
	}

	public void setActivityApp(String activityApp) {
		this.activityApp = activityApp;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}

	public String getActivityDesc() {
		return activityDesc;
	}

	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(Map<String, String> payload) throws IOException {
		if(payload!=null)
		{
			this.payload = WebClientUtils.getJsonStringFromMapString(payload);			
		}
	}

	public int getTemplateId() {
		return templateId;
	}

	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public ClientDetails getClientDetails() {
		return clientDetails;
	}

	public void setClientDetails(ClientDetails clientDetails) {
		this.clientDetails = clientDetails;
	}

	public ClientSystemDetails getClientSystemDetails() {
		return clientSystemDetails;
	}

	public void setClientSystemDetails(ClientSystemDetails clientSystemDetails) {
		this.clientSystemDetails = clientSystemDetails;
	}
	
	public String getClientIqHash() {
		return clientIqHash;
	}

	public void setClientIqHash(String clientIqHash) {
		this.clientIqHash = clientIqHash;
	}

	@Override
	public String toString() {
		return "SciPortalWebApiRequest [keyId=" + keyId + ", userId=" + userId 
				+ ", encryptedPassword-length=" + (password!=null?password.length():"0") + ", projectSeqId=" + projectSeqId + ", activityApp="
				+ activityApp + ", activityType=" + activityType + ", activityId=" + activityId + ", activityCode="
				+ activityCode + ", activityDesc=" + activityDesc + ", payload=" + payload + ", templateId="
				+ templateId + ", clientDetails=" + clientDetails + ", clientSystemDetails=" + clientSystemDetails
				+ "]";
	}
	
	private static String encryptPassword(String password) {
		String encryptedPassword = null;
		encryptedPassword = PublicKeyCrypto.encryptText(password);
		return encryptedPassword;
	}

}
