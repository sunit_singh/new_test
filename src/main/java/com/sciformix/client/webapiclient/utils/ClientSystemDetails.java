package com.sciformix.client.webapiclient.utils;

public class ClientSystemDetails {
	private String osName = null;
	private String osVersion = null;
	private String osArchitecture = null;
	private String jdkVersion = null;
	private String machineName = null;
	private String machineDomain = null;
	private String machineLoggedInUser = null;
	private String machineLoggedInUserDomain = null;
	private String ipAddress = null;
	private String browserVersChrome = null;
	private String browserVerIE = null;

	public String getBrowserVersChrome() {
		return browserVersChrome;
	}

	public void setBrowserVersChrome(String browserVersChrome) {
		this.browserVersChrome = browserVersChrome;
	}

	public String getBrowserVerIE() {
		return browserVerIE;
	}

	public void setBrowserVerIE(String browserVerIE) {
		this.browserVerIE = browserVerIE;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getOsArchitecture() {
		return osArchitecture;
	}

	public void setOsArchitecture(String osArchitecture) {
		this.osArchitecture = osArchitecture;
	}

	public String getJdkVersion() {
		return jdkVersion;
	}

	public void setJdkVersion(String jdkVersion) {
		this.jdkVersion = jdkVersion;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getMachineDomain() {
		return machineDomain;
	}

	public void setMachineDomain(String machineDomain) {
		this.machineDomain = machineDomain;
	}

	public String getMachineLoggedInUser() {
		return machineLoggedInUser;
	}

	public void setMachineLoggedInUser(String machineLoggedInUser) {
		this.machineLoggedInUser = machineLoggedInUser;
	}

	public String getMachineLoggedInUserDomain() {
		return machineLoggedInUserDomain;
	}

	public void setMachineLoggedInUserDomain(String machineLoggedInUserDomain) {
		this.machineLoggedInUserDomain = machineLoggedInUserDomain;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Override
	public String toString() {
		return "ClientSystemDetails [osName=" + osName + ", osVersion=" + osVersion + ", osArchitecture="
				+ osArchitecture + ", jdkVersion=" + jdkVersion + ", machineName=" + machineName + ", machineDomain="
				+ machineDomain + ", machineLoggedInUser=" + machineLoggedInUser + ", machineLoggedInUserDomain="
				+ machineLoggedInUserDomain + ", ipAddress=" + ipAddress + ", browserVersChrome=" + browserVersChrome
				+ ", browserVerIE=" + browserVerIE + "]";
	}

}
