package com.sciformix.client.webapiclient.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sciformix.client.webapiclient.SciPortalWebApiClient;
import com.sciformix.client.webapiclient.SciPortalWebApiClient.OPERATING_ZONE;
import com.sciformix.client.webapiclient.SciPortalWebApiRequest;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse.STATUS;
import com.sciformix.client.webapiclient.WebClientUtils;

public class WebApiConnnectivityUtil {
	
	private static Logger logger = LoggerFactory.getLogger(WebApiConnnectivityUtil.class);

	private static final String URL_AVAILABILITY = "/service/checkAvailability";
	private static final String NO_SERVER_AVAILABLE = "No server currently available. Please retry later.";

	public static SciPortalWebApiResponse processRequest(OPERATING_ZONE p_eCurrentOperatingZone, SciPortalWebApiRequest request) {
		SciPortalWebApiResponse response = new SciPortalWebApiResponse();
		String output = null;
		Map<String, Object> tempResponse = null;
		Map<String, Object> tempFailedResponse = null;
		String server = null;
		String jsonRequest = null;
		String serviceUri = null;
		
		logger.info("Request received for service : " + request.getApiRequest().getApiName());
		logger.info("Request Sequence ID : " + request.getClientDetails().getClientReqId());
		
		try 
		{
			serviceUri = request.getApiRequest().getServiceUri();
			jsonRequest = SciPortalWebApiRequestCreator.getServiceRequestJsonString(request);
			logger.info("Request JSON: " + jsonRequest);
			
			if (p_eCurrentOperatingZone != OPERATING_ZONE.DISCONNECTED) 
			{
				server = getAvailableServerAsPerZone(p_eCurrentOperatingZone);
				if (!server.equals("")) 
				{
					logger.info("Server URL being hit: " + server);
					serviceUri = server + serviceUri;
					output = getServiceResponse(serviceUri, jsonRequest);
					tempResponse = WebClientUtils.getMapFromJson(output);
				} 
				else 
				{
					tempFailedResponse = new HashMap<String, Object>();
					tempFailedResponse.put("code", "404");
					tempFailedResponse.put("message", NO_SERVER_AVAILABLE);
					tempResponse = new HashMap<String, Object>();
					tempResponse.put("Response", tempFailedResponse);
				}

				logger.info("Response from server : " + tempResponse);
				response = getFinalResponse(tempResponse, request.getApiRequest().getApiName());
			} 
			else 
			{
				response = WebApiClientDisconnectedModeResponder.getDisconnectedModeResponse(request);
			}
		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
		}
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private static SciPortalWebApiResponse getFinalResponse(Map<String, Object> tempMap, String serviceApi)
			throws UnsupportedEncodingException {
		SciPortalWebApiResponse clientResponse = new SciPortalWebApiResponse();

		Map<String, Object> response = (Map<String, Object>) tempMap.get("Response");
		
		int responseCode = Integer.parseInt(response.get("code").toString());
		if(responseCode!=100000){
			clientResponse.setStatus(STATUS.FAILURE);
		} else {
			clientResponse.setStatus(STATUS.SUCCESS);
		}

		clientResponse.setCode(responseCode);
		clientResponse.setMessage((String) response.get("message"));

		if (serviceApi.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_GET_PROJECTS.getApiName())) {

			clientResponse.setData(tempMap.get("UserProjects") != null ? tempMap.get("UserProjects") : null);

		} else if (serviceApi.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_REGISTER_ACTIVITY.getApiName())) {

			clientResponse.setData(tempMap.get("ActivityId") != null ? tempMap.get("ActivityId") : null);

		} else if (serviceApi.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_EXPORT_TEMPLATE.getApiName())) {

			String encodedTemplate = (String) tempMap.get("TemplateBody");
			if (encodedTemplate != null) {
				byte[] decodedByteArray = DatatypeConverter.parseBase64Binary(encodedTemplate);
				clientResponse.setData(new String(decodedByteArray, "UTF-8"));
			} else {
				clientResponse.setData(null);
			}

		} else if (serviceApi.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_GET_VERSION.getApiName())) {

			clientResponse.setData(tempMap.get("LatestToolVersion") != null ? tempMap.get("LatestToolVersion") : null);

		}

		return clientResponse;
	}

	private static String getAvailableServerAsPerZone(OPERATING_ZONE p_eCurrentOperatingZone) {
		List<String> urlList = null;
		String workingServerUrl = null;
		Map<String, String> propsMap = SciPortalWebApiClient.getPropsMap();
		urlList = WebClientUtils.getZoneSpecificServers(propsMap, p_eCurrentOperatingZone);
		workingServerUrl = checkAvailability(urlList);
		return workingServerUrl;
	}

	private static String checkAvailability(List<String> urlList) {
		Boolean isServiceAvailable = Boolean.FALSE;
		try {

			if (!urlList.isEmpty()) {
				for (String serverUrl : urlList) {
					isServiceAvailable = isReachable(serverUrl + URL_AVAILABILITY);
					if (isServiceAvailable) {
						return serverUrl;
					}
				}
			} else {
				return "";
			}
		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
			return "";
		}

		return "";
	}

	private static boolean isReachable(String serviceUrl) throws IOException {

		Boolean isAvailable = Boolean.FALSE;
		HttpsURLConnection httpsCon = null;

		try {

			httpsCon = SslConnectionProvider.getSecureConnection(serviceUrl);
			httpsCon.setRequestMethod("HEAD");
			if (httpsCon.getResponseCode()!=200) {
				isAvailable = Boolean.FALSE;
			} else {
				isAvailable = Boolean.TRUE;
			}

		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
			isAvailable = Boolean.FALSE;
		}

		return isAvailable;
	}

	private static String getServiceResponse(String serviceUrl, String input)
			throws JsonParseException, JsonMappingException, IOException {
		String output = null;
		Map<String, Object> failedResponse = null;
		Map<String, Object> tempResponse = null;
		HttpsURLConnection httpsCon = null;
		InputStream responseInputStream = null;
		
		try{
			httpsCon = SslConnectionProvider.getSecureConnection(serviceUrl);
			httpsCon.setDoOutput(true);
			httpsCon.setRequestMethod("POST");
			httpsCon.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			
			OutputStream os = httpsCon.getOutputStream();
			os.write(input.getBytes());
			os.flush();
			
			responseInputStream = httpsCon.getInputStream();
			
			if (httpsCon.getResponseCode()==200) {
				output = IOUtils.toString(responseInputStream, "utf-8");
			} else {
				tempResponse = new HashMap<String, Object>();
				tempResponse.put("code", "404");
				tempResponse.put("message", NO_SERVER_AVAILABLE);
				failedResponse = new HashMap<String, Object>();
				failedResponse.put("Response", tempResponse);
				output = WebClientUtils.getJsonStringFromMap(failedResponse);
			}
			
			httpsCon.disconnect();

		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
		} finally {
			try {
				responseInputStream.close();
			} catch (IOException e) {
				logger.error("Error : " + e.getMessage());
			}
		}

		return output;
	}

}
