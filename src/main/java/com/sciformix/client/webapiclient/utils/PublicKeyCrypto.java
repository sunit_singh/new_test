package com.sciformix.client.webapiclient.utils;

import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.webapiclient.WebClientUtils;

public class PublicKeyCrypto {
	
	private static Logger logger = LoggerFactory.getLogger(PublicKeyCrypto.class);
	
	private static final String PROPS_KEY_PUBLICKEY = "sciportal.webapiclient.publickey.filename";

	public static PublicKey getPublic(String filePath) throws Exception {
		InputStream input = PublicKeyCrypto.class.getResourceAsStream(filePath);
		byte[] keyBytes = IOUtils.toByteArray(input);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	public static String encryptText(String plainText) {

		String publickeyFileName = null;
		Cipher cipher = null;
		String encrypt = null;
		try {
			publickeyFileName = WebClientUtils.getKeyValuefromProps(PROPS_KEY_PUBLICKEY);
			cipher = Cipher.getInstance("RSA");
			PublicKey publicKey = getPublic("/" + publickeyFileName);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			encrypt = DatatypeConverter.printBase64Binary(cipher.doFinal(plainText.getBytes("UTF-8")));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return encrypt;
	}

}
