package com.sciformix.client.webapiclient.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import com.sciformix.client.webapiclient.WebClientUtils;

public class SslConnectionProvider {

	private static final String KEY_SELFSIGNED_HOSTS = "sciportal.webapiclient.selfsignedhosts";
	private static final String KEY_KEYSTORE_FILENAME = "sciportal.webapiclient.keystore.filename";
	//private static final String keystorePassword = "sciformix@123";
	private static final String KEY_KEYSTORE_PASSWORD = "sciportal.webapiclient.keystore.password";
	private static String KEYSTORE_TYPE = "JKS";
	private static String PROTOCOL_VERSION = "TLSv1.2";
	
	
	
	static {
		String hosts = WebClientUtils.getKeyValuefromProps(KEY_SELFSIGNED_HOSTS);
		if (!hosts.equals("")) {
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});
		}

	}
	
	public static HttpsURLConnection getSecureConnection(String serviceUrl) {
		HttpsURLConnection httpsCon = null;
		char[] passphrase = null;
		KeyStore keystore = null;
		InputStream keystoreInput = null;
		TrustManagerFactory tmf = null;
		SSLContext context = null;
		TrustManager[] trustManagers = null;
		SSLSocketFactory sf = null;
		URL url = null;

		try {
			String keystorePassword = WebClientUtils.getKeyValuefromProps(KEY_KEYSTORE_PASSWORD);
			passphrase = keystorePassword.toCharArray();

			keystore = KeyStore.getInstance(KEYSTORE_TYPE);

			String keystoreFilename = WebClientUtils.getKeyValuefromProps(KEY_KEYSTORE_FILENAME);
			keystoreInput = SslConnectionProvider.class.getResourceAsStream("/" + keystoreFilename);
			keystore.load(keystoreInput, passphrase);

			tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());

			tmf.init(keystore);

			context = SSLContext.getInstance(PROTOCOL_VERSION);// JDK-7 by default  doesnt support TLSv1.2 until the instance is explicitly got.

			trustManagers = tmf.getTrustManagers();

			context.init(null, trustManagers, null);

			sf = context.getSocketFactory();

			url = new URL(serviceUrl);

			httpsCon = (HttpsURLConnection) url.openConnection();

			httpsCon.setSSLSocketFactory(sf);

		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				keystoreInput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return httpsCon;
	}
	
}
