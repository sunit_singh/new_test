package com.sciformix.client.webapiclient.utils;

public class ClientDetails
{
	private String clientReqId = null;
	private String toolName = null;
	private String toolVersion = null;
	private String timeStamp = null;
	private String timeZone = null;

	public String getClientReqId() {
		return clientReqId;
	}

	public void setClientReqId(String clientReqId) {
		this.clientReqId = clientReqId;
	}

	public String getToolName() {
		return toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	public String getToolVersion() {
		return toolVersion;
	}

	public void setToolVersion(String toolVersion) {
		this.toolVersion = toolVersion;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	@Override
	public String toString() {
		return "ClientPayload [clientReqId=" + clientReqId + ", toolName=" + toolName + ", toolVersion=" + toolVersion
				+ ", timeStamp=" + timeStamp + ", timeZone=" + timeZone + "]";
	}

}
