package com.sciformix.client.webapiclient.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.webapiclient.SciPortalWebApiRequest;
import com.sciformix.client.webapiclient.WebClientUtils;

public class SciPortalWebApiRequestCreator
{
	
	private static Logger logger = LoggerFactory.getLogger(SciPortalWebApiRequestCreator.class);
	
	public static final ApiRequest API_GET_PROJECTS = new ApiRequest("getProjects", "/service/getProjects");
	public static final ApiRequest API_REGISTER_ACTIVITY = new ApiRequest("registerActivity", "/service/registerActivity");
	public static final ApiRequest API_EXPORT_TEMPLATE = new ApiRequest("exportTemplate", "/service/exportTemplate");
	public static final ApiRequest API_GET_VERSION = new ApiRequest("getVersion", "/service/getVersion");
	
	public static class ApiRequest
	{
		private String m_sApiName = null;
		private String m_sApiServiceUri = null;
		
		public ApiRequest(String p_sApiName, String p_sApiServiceUri)
		{
			m_sApiName = p_sApiName;
			m_sApiServiceUri = p_sApiServiceUri;
		}
		
		public String getApiName() {
			return m_sApiName;
		}

		public String getServiceUri()
		{
			return m_sApiServiceUri;
		}
	}
	
	private static final String REQUEST_PARAM_KEYID = "KeyId";
	private static final String REQUEST_PARAM_USERID = "UserId";
	private static final String REQUEST_PARAM_PASSWORD = "Password";
	private static final String REQUEST_PARAM_CLIENTIQHASH = "ClientIQHash";
	private static final String REQUEST_PARAM_PROJECTSEQID = "ProjectSeqId";
	private static final String REQUEST_PARAM_ACTIVITYID = "ActivityId";
	private static final String REQUEST_PARAM_ACTIVITYAPP = "ActivityApp";
	private static final String REQUEST_PARAM_ACTIVITYTYPE = "ActivityType";
	private static final String REQUEST_PARAM_ACTIVITYCODE = "ActivityCode";
	private static final String REQUEST_PARAM_ACTIVITYDESC = "ActivityDesc";
	private static final String REQUEST_PARAM_ACTIVITYPAYLOAD = "Payload";
	private static final String REQUEST_PARAM_TEMPLATEID = "TemplateId";
	
	private static final String REQUEST_PARAM_CLIENT = "Client";
	private static final String REQUEST_PARAM_TOOLNAME = "ToolName";
	private static final String REQUEST_PARAM_TOOLVERSION = "ToolVersion";
	private static final String REQUEST_PARAM_CLIENTREQID = "ClientReqID";
	private static final String REQUEST_PARAM_TIMESTAMP = "Timestamp";
	private static final String REQUEST_PARAM_TIMEZONE = "Timezone";
	
	private static final String REQUEST_PARAM_SYSTEMPAYLOAD = "SystemPayload";
	private static final String REQUEST_PARAM_MACHINENAME = "MachineName";
	private static final String REQUEST_PARAM_MACHINEDOMAIN = "MachineDomain";
	private static final String REQUEST_PARAM_IPADDRESS = "IPAddress";
	private static final String REQUEST_PARAM_MACHINELOGGEDINUSER = "MachineLoggedInUser";
	private static final String REQUEST_PARAM_MACHINELOGGEDINUSERDOMAIN = "MachineLoggedInUserDomain";
	private static final String REQUEST_PARAM_OSNAME = "OSName";
	private static final String REQUEST_PARAM_OSVERSION = "OSVersion";
	private static final String REQUEST_PARAM_OSARCHITECTURE = "OSArchitecture";
	private static final String REQUEST_PARAM_JDKVERSION = "JDKVersion";
	private static final String REQUEST_PARAM_CHROMEVERSION = "ChromeVersion";
	private static final String REQUEST_PARAM_IEVERSION = "IEVersion";

	
	public static SciPortalWebApiRequest createRequest(ApiRequest p_oRequestType, String toolName, String toolVersion, String browserVerChrome, String browserVerIE)
	{
		if(browserVerChrome==null || browserVerChrome.equals(""))
			browserVerChrome = "";
		
		if(browserVerIE==null || browserVerIE.equals(""))
			browserVerIE = "";
			
		SciPortalWebApiRequest request = new SciPortalWebApiRequest(p_oRequestType);
		request.setClientDetails(getClientDetailsParams(toolName, toolVersion));
		request.setClientSystemDetails(getClientSystemDetailsParams(browserVerChrome, browserVerIE));
		return request;
	}
	
	public static String getServiceRequestJsonString(SciPortalWebApiRequest request) throws IOException{
		String jsonRequest = null;
		String apiName = request.getApiRequest().getApiName();
		if(apiName.equalsIgnoreCase("getProjects")){
			jsonRequest = getJsonInputForGetProjectsService(request);
		} else if(apiName.equalsIgnoreCase("registerActivity")) {
			jsonRequest = getJsonInputForRegisterActivityService(request);
		} else if(apiName.equalsIgnoreCase("exportTemplate")) {
			jsonRequest = getJsonInputForExportTemplateService(request);
		}
		return jsonRequest;
	}
	
	private static String getJsonInputForGetProjectsService(SciPortalWebApiRequest request) throws IOException{
		String input = null;
		Map<String, Object> inputParamsMap = new LinkedHashMap<String, Object>();
		
		inputParamsMap.put(REQUEST_PARAM_KEYID, request.getKeyId());
		inputParamsMap.put(REQUEST_PARAM_USERID, request.getUserId());
		inputParamsMap.put(REQUEST_PARAM_PASSWORD, request.getPassword());
		inputParamsMap.put(REQUEST_PARAM_CLIENTIQHASH, request.getClientIqHash());
		inputParamsMap.put(REQUEST_PARAM_CLIENT, getClientDetailsMap(request));
		inputParamsMap.put(REQUEST_PARAM_SYSTEMPAYLOAD, getClientSystemDetailsMap(request));
		
		input = WebClientUtils.getJsonStringFromMap(inputParamsMap);
		return input;
	}
	
	private static String getJsonInputForRegisterActivityService(SciPortalWebApiRequest request) throws IOException{
		String input = null;
		Map<String, Object> inputParamsMap = new LinkedHashMap<String, Object>();
		
		inputParamsMap.put(REQUEST_PARAM_KEYID, request.getKeyId());
		inputParamsMap.put(REQUEST_PARAM_USERID, request.getUserId());
		inputParamsMap.put(REQUEST_PARAM_PASSWORD, request.getPassword());
		inputParamsMap.put(REQUEST_PARAM_CLIENTIQHASH, request.getClientIqHash());
		inputParamsMap.put(REQUEST_PARAM_PROJECTSEQID, "" + request.getProjectSeqId());
		inputParamsMap.put(REQUEST_PARAM_ACTIVITYAPP, request.getActivityApp());
		inputParamsMap.put(REQUEST_PARAM_ACTIVITYTYPE, request.getActivityType());
		inputParamsMap.put(REQUEST_PARAM_ACTIVITYID, "" + request.getActivityId());
		inputParamsMap.put(REQUEST_PARAM_ACTIVITYCODE, request.getActivityCode());
		inputParamsMap.put(REQUEST_PARAM_ACTIVITYDESC, request.getActivityDesc());
		inputParamsMap.put(REQUEST_PARAM_ACTIVITYPAYLOAD, request.getPayload());
		inputParamsMap.put(REQUEST_PARAM_CLIENT, getClientDetailsMap(request));
		inputParamsMap.put(REQUEST_PARAM_SYSTEMPAYLOAD, getClientSystemDetailsMap(request));
		
		input = WebClientUtils.getJsonStringFromMap(inputParamsMap);
		
		return input;
	}
	
	private static String getJsonInputForExportTemplateService(SciPortalWebApiRequest request) throws IOException{
		String input = null;
		Map<String, Object> inputParamsMap = new LinkedHashMap<String, Object>();
		
		inputParamsMap.put(REQUEST_PARAM_KEYID, request.getKeyId());
		inputParamsMap.put(REQUEST_PARAM_USERID, request.getUserId());
		inputParamsMap.put(REQUEST_PARAM_PASSWORD, request.getPassword());
		inputParamsMap.put(REQUEST_PARAM_CLIENTIQHASH, request.getClientIqHash());
		inputParamsMap.put(REQUEST_PARAM_PROJECTSEQID, "" + request.getProjectSeqId());
		inputParamsMap.put(REQUEST_PARAM_TEMPLATEID, "" + request.getProjectSeqId());
		inputParamsMap.put(REQUEST_PARAM_CLIENT, getClientDetailsMap(request));
		inputParamsMap.put(REQUEST_PARAM_SYSTEMPAYLOAD, getClientSystemDetailsMap(request));
		
		input = WebClientUtils.getJsonStringFromMap(inputParamsMap);
		return input;
	}
	
	private static ClientDetails getClientDetailsParams(String toolname, String toolVersion) {
		ClientDetails clientParams = new ClientDetails();

		String clientReqId = null;
		String timeStamp = null;
		String timeZone = null;
		
		SimpleDateFormat sdf = null;

		try {
			
			sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			timeStamp = sdf.format(ts);

			Calendar now = Calendar.getInstance();
			TimeZone tz = now.getTimeZone();
			timeZone = tz.getDisplayName();

			clientReqId = toolname + "_" + toolVersion + "_" + ts.getTime();

			clientParams.setToolName(toolname);
			clientParams.setToolVersion(toolVersion);
			clientParams.setClientReqId(clientReqId);
			clientParams.setTimeStamp(timeStamp);
			clientParams.setTimeZone(timeZone);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} 
		
		return clientParams;
	}
	
	private static ClientSystemDetails getClientSystemDetailsParams(String browserVerChrome, String browserVerIE) {
		ClientSystemDetails systemDetailsParams = new ClientSystemDetails();

		String machineName = null;
		String machineDomain = null;
		String ipAddress = null;
		String machineLoggedInUser = null;
		String machineLoggedInUserDomain = null;
		String osName = null;
		String osVersion = null;
		String jdkVersion = null;
		String osArchitecture = null;

		InetAddress addr = null;
		com.sun.security.auth.module.NTSystem NTSystem = new com.sun.security.auth.module.NTSystem();

		try {
			addr = InetAddress.getLocalHost();
			machineName = addr.getHostName();
			machineDomain = addr.getCanonicalHostName();
			ipAddress = addr.getHostAddress();

			machineLoggedInUser = NTSystem.getName();
			machineLoggedInUserDomain = NTSystem.getDomain();

			osName = System.getProperty("os.name");
			osVersion = System.getProperty("os.version");
			osArchitecture = System.getProperty("os.arch");
			jdkVersion = System.getProperty("java.version");
			
			systemDetailsParams.setMachineName(machineName);
			systemDetailsParams.setMachineDomain(machineDomain);
			systemDetailsParams.setIpAddress(ipAddress);
			systemDetailsParams.setMachineLoggedInUser(machineLoggedInUser);
			systemDetailsParams.setMachineLoggedInUserDomain(machineLoggedInUserDomain);
			systemDetailsParams.setOsName(osName);
			systemDetailsParams.setOsVersion(osVersion);
			systemDetailsParams.setOsArchitecture(osArchitecture);
			systemDetailsParams.setJdkVersion(jdkVersion);
			systemDetailsParams.setBrowserVersChrome(browserVerChrome);
			systemDetailsParams.setBrowserVerIE(browserVerIE);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return systemDetailsParams;
	}
	
	private static Map<String, String> getClientDetailsMap(SciPortalWebApiRequest request){
		Map<String, String> clientSystemDetailsMap = new HashMap<String, String>();
		clientSystemDetailsMap.put(REQUEST_PARAM_TOOLNAME, request.getClientDetails().getToolName());
		clientSystemDetailsMap.put(REQUEST_PARAM_TOOLVERSION, request.getClientDetails().getToolVersion());
		clientSystemDetailsMap.put(REQUEST_PARAM_CLIENTREQID, request.getClientDetails().getClientReqId());
		clientSystemDetailsMap.put(REQUEST_PARAM_TIMESTAMP, request.getClientDetails().getTimeStamp());
		clientSystemDetailsMap.put(REQUEST_PARAM_TIMEZONE, request.getClientDetails().getTimeZone());
		return clientSystemDetailsMap;
	}
	
	private static Map<String, String> getClientSystemDetailsMap(SciPortalWebApiRequest request){
		Map<String, String> clientDetailsMap = new HashMap<String, String>();
		clientDetailsMap.put(REQUEST_PARAM_MACHINENAME, request.getClientSystemDetails().getMachineName());
		clientDetailsMap.put(REQUEST_PARAM_MACHINEDOMAIN, request.getClientSystemDetails().getMachineDomain());
		clientDetailsMap.put(REQUEST_PARAM_IPADDRESS, request.getClientSystemDetails().getIpAddress());
		clientDetailsMap.put(REQUEST_PARAM_MACHINELOGGEDINUSER, request.getClientSystemDetails().getMachineLoggedInUser());
		clientDetailsMap.put(REQUEST_PARAM_MACHINELOGGEDINUSERDOMAIN, request.getClientSystemDetails().getMachineLoggedInUserDomain());
		clientDetailsMap.put(REQUEST_PARAM_OSNAME, request.getClientSystemDetails().getOsName());
		clientDetailsMap.put(REQUEST_PARAM_OSVERSION, request.getClientSystemDetails().getOsVersion());
		clientDetailsMap.put(REQUEST_PARAM_OSARCHITECTURE, request.getClientSystemDetails().getOsArchitecture());
		clientDetailsMap.put(REQUEST_PARAM_JDKVERSION, request.getClientSystemDetails().getJdkVersion());
		clientDetailsMap.put(REQUEST_PARAM_CHROMEVERSION, request.getClientSystemDetails().getBrowserVersChrome());
		clientDetailsMap.put(REQUEST_PARAM_IEVERSION, request.getClientSystemDetails().getBrowserVerIE());
		return clientDetailsMap;
	}
	
}
