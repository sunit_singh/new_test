package com.sciformix.client.webapiclient.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sciformix.client.webapiclient.SciPortalWebApiRequest;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse;
import com.sciformix.client.webapiclient.SciPortalWebApiResponse.STATUS;

public class WebApiClientDisconnectedModeResponder {

	private static final int SUCCESS_CODE = 100000;
	private static final String SUCCESS_MSG = "Success.";

	public static SciPortalWebApiResponse getDisconnectedModeResponse(SciPortalWebApiRequest request) {
		SciPortalWebApiResponse response = new SciPortalWebApiResponse();
		response.setStatus(STATUS.SUCCESS);
		response.setCode(SUCCESS_CODE);
		response.setMessage(SUCCESS_MSG);

		if (request.getApiRequest().getApiName()
				.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_GET_PROJECTS.getApiName())) {

			List<Map<String, Object>> projectList = new ArrayList<Map<String, Object>>();
			Map<String, Object> projMap = new HashMap<String, Object>();
			projMap.put("objectName", "TEST");
			projMap.put("objectSeqId", 1);
			projMap.put("Status", "OK");
			projectList.add(projMap);
			response.setData(projectList);

		} else if (request.getApiRequest().getApiName()
				.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_REGISTER_ACTIVITY.getApiName())) {
			response.setData(150);
		} else if (request.getApiRequest().getApiName()
				.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_EXPORT_TEMPLATE.getApiName())) {

			response.setData("TEST_EXPORT_TEMPLATE");

		} else if (request.getApiRequest().getApiName()
				.equalsIgnoreCase(SciPortalWebApiRequestCreator.API_GET_VERSION.getApiName())) {

			response.setData("TEST_VERSION");

		}

		return response;
	}

}
