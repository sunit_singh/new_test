package com.sciformix.client.webapiclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sciformix.client.webapiclient.SciPortalWebApiClient.OPERATING_ZONE;

public class WebClientUtils
{
	
	private static Logger logger = LoggerFactory.getLogger(WebClientUtils.class);
	
	private static final String WEBAPI_CLIENT_PREFIX = "sciportal.webapiclient";

	private static final String OPERATING_ZONE_TEST_SERVER_COUNT = "sciportal.webapiclient.servers.test.count";
	private static final String OPERATING_ZONE_TEST_PREFIX = "sciportal.webapiclient.servers.test.";
	
	private static final String OPERATING_ZONE_PREPROD_SERVER_COUNT = "sciportal.webapiclient.servers.preprod.count";
	private static final String OPERATING_ZONE_PREPROD_PREFIX = "sciportal.webapiclient.servers.preprod.";
	
	private static final String OPERATING_ZONE_PROD_SERVER_COUNT = "sciportal.webapiclient.servers.prod.count";
	private static final String OPERATING_ZONE_PROD_PREFIX = "sciportal.webapiclient.servers.prod.";
	
	public static Map<String, String> loadProperties(Properties props)
	{
		Map<String, String> propsMap = new HashMap<String, String>();
		InputStream input = null;
		try {
			for (String key : props.stringPropertyNames()) {
				if(key.contains(WEBAPI_CLIENT_PREFIX)){
					String value = props.getProperty(key);
					propsMap.put(key, value);
				}
			}
		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.error("Error : " + e.getMessage());
				}
			}
		}
		return propsMap;
	}
	
	public static String getKeyValuefromProps(String key) {
		String value = null;
		Map<String, String> propsMap = SciPortalWebApiClient.getPropsMap();
		value = propsMap.get(key);
		return value;
	}
	
	public static List<String> getZoneSpecificServers(Map<String, String> propsMap, OPERATING_ZONE zone)
	{
		List<String> serverList = new LinkedList<String>();
		int serverCount = -1;
		try{
			if(zone==OPERATING_ZONE.TEST){
				serverCount = Integer.parseInt(propsMap.get(OPERATING_ZONE_TEST_SERVER_COUNT));
				for(int counter = 1;counter <= serverCount; counter++){
					serverList.add(propsMap.get(OPERATING_ZONE_TEST_PREFIX + counter));//getting all servers for a particular zone
				}
			} else if(zone==OPERATING_ZONE.PREPRODUCTION){
				serverCount = Integer.parseInt(propsMap.get(OPERATING_ZONE_PREPROD_SERVER_COUNT));
				for(int counter = 1;counter <= serverCount; counter++){
					serverList.add(propsMap.get(OPERATING_ZONE_PREPROD_PREFIX + counter));//getting all servers for a particular zone
				}
			} else if(zone==OPERATING_ZONE.PRODUCTION){
				serverCount = Integer.parseInt(propsMap.get(OPERATING_ZONE_PROD_SERVER_COUNT));
				for(int counter = 1;counter <= serverCount; counter++){
					serverList.add(propsMap.get(OPERATING_ZONE_PROD_PREFIX + counter));//getting all servers for a particular zone
				}
			}
			
		} catch (Exception e){
			logger.error("Error : " + e.getMessage());
		}
		return serverList;
	}
	
	public static String getJsonStringFromMap(Map<String, Object> clientParamMap) throws IOException {
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		jsonString = objectMapper.writeValueAsString(clientParamMap);
		Object json = objectMapper.readValue(jsonString, Object.class);
		jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		return jsonString;
	}
	
	public static Map<String, Object> getMapFromJson(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = new LinkedHashMap<String, Object>();
		jsonMap = mapper.readValue(json, new TypeReference<Map<String, Object>>() {
		});
		return jsonMap;
	}
	
	public static String getJsonStringFromMapString(Map<String, String> inputMap) throws IOException {
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		jsonString = objectMapper.writeValueAsString(inputMap);
		Object json = objectMapper.readValue(jsonString, Object.class);
		jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		return jsonString;
	}
	
	public static String generateHashForClientTool() {

		String hash = null;
		String path = null;
		MessageDigest md = null;
		FileInputStream fis = null;
		StringBuilder sb = null;
		String filepath = null;
		try {

			path = URLDecoder.decode(WebClientUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath(),
					"UTF-8");
			File currentJavaJarFile = new File(path);
			filepath = currentJavaJarFile.getAbsolutePath();
			sb = new StringBuilder();

			md = MessageDigest.getInstance("MD5");// MD5
			fis = new FileInputStream(filepath);
			byte[] dataBytes = new byte[1024];
			int nread = 0;

			while ((nread = fis.read(dataBytes)) != -1)
				md.update(dataBytes, 0, nread);

			byte[] mdbytes = md.digest();

			for (int i = 0; i < mdbytes.length; i++) {
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			hash = sb.toString();
			
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error : " + e.getMessage());
		} catch (IOException e) {
			logger.error("Error : " + e.getMessage());
		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				logger.error("Error : " + e.getMessage());
			} catch (Exception e){
				logger.error("Error : " + e.getMessage());
			}
		}
		return hash;
	}
}
