package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class OtherMedicallyImportantCondition implements IFieldBusinessLogic{
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
	
		String	l_fieldvalue	=	CpatToolConstants.FieldsLogicValues.NO;
		
		if(oPdtWrapper.isKeyValueNullEmptyOrMatches(p_sourcedocumentkey, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE)) {
			
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATE_WITH_MISSCARRIGE))
			{
				if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_AND_REPORTED_CAUSE_OF_MISSCARRIGE))
				{
					l_fieldvalue = CpatToolConstants.FieldsLogicValues.NO;
				}
				else
				{
					l_fieldvalue = CpatToolConstants.FieldsLogicValues.YES;
				}
			}
			else
			{
				String value =  oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATE_WITH_MISSCARRIGE);
				if(value.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES))
				{
					l_fieldvalue = CpatToolConstants.FieldsLogicValues.YES;
				}
				else
				{
					if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_AND_REPORTED_CAUSE_OF_MISSCARRIGE))
					{
						l_fieldvalue = CpatToolConstants.FieldsLogicValues.NO;
					} else {
						throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.OTHER_MEDICALLY_IMP_CONDITION);
					}
				}
			}
		}
		else
		{	
			l_fieldvalue = CpatToolConstants.FieldsLogicValues.YES;
		}
		if (l_fieldvalue.equals(CpatToolConstants.FieldsLogicValues.NO)) {
			if ( CpatToolCHelper.isCaseSerious(p_sourcedocumentdata, 
					CpatToolCHelper.propMap.get(IRTConstants.SERIOUS_KEYWORDS) , StringConstants.COMMA)) {
				l_fieldvalue = CpatToolConstants.FieldsLogicValues.YES;
			} else {
				l_fieldvalue = CpatToolConstants.FieldsLogicValues.NO;
			}
		}
		p_dataentrymap.put(p_writerkey, l_fieldvalue);
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
		
	}
}
