package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class PatientId implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;
	
	private static final Logger log = LoggerFactory.getLogger(PatientId.class);

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		String e2b_primarySourceCountry = null;
		String e2b_patientInitials = null;
		String e2b_narrativePatientInitials = null;
		
		e2b_primarySourceCountry = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY);
		if(!StringUtils.isNullOrEmpty(e2b_primarySourceCountry)){
				
			if(CpatToolCHelper.isCountryEMEA(e2b_primarySourceCountry)) {
				p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.PRIVATE);
			} else {
				e2b_patientInitials = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.PatientTabKeys.PATIENT_INITIALS).trim();
				e2b_narrativePatientInitials = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.PATIENT_INITIALS).trim();
				if(!e2b_patientInitials.equalsIgnoreCase(e2b_narrativePatientInitials)){
					p_dataentrymap.put(p_writerkey, e2b_narrativePatientInitials);
				} else {
					p_dataentrymap.put(p_writerkey, e2b_patientInitials);
				}
			}
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		String e2b_primarySourceCountry = null;
		String e2b_patientInitials = null;
		String e2b_narrativePatientInitials = null;
		String sceptre_patientId = null;
		String irt_patientId = null;
		
		e2b_primarySourceCountry = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY);
		if(CpatToolCHelper.isCountryEMEA(e2b_primarySourceCountry)) {
			p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.PRIVATE);
		} else {
			sceptre_patientId = CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.PAT_INITIALS);
			e2b_patientInitials = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.PatientTabKeys.PATIENT_INITIALS).trim();
			e2b_narrativePatientInitials = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.PATIENT_INITIALS).trim();
			if(!e2b_patientInitials.equalsIgnoreCase(e2b_narrativePatientInitials)){
				irt_patientId = e2b_narrativePatientInitials;
			} else {
				irt_patientId = e2b_patientInitials;
			}
			
			if(!sceptre_patientId.equalsIgnoreCase(irt_patientId)){
				p_dataentrymap.put(p_writerkey, sceptre_patientId);
				throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.PATIENT_ID);
			} else {
				p_dataentrymap.put(p_writerkey, sceptre_patientId);
			}
		}
	}
	
}
