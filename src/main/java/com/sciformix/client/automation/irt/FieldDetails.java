package com.sciformix.client.automation.irt;

public class FieldDetails {
	public enum FieldType {
		TEXTBOX, TEXTAREA, SELECT, SELECTSEARCH, CHECKBOX, RADIO, SWITCHER, BUTTON, MULTISELECT, IMAGE, DATETIME;
	}
	
	public enum XPathType {
		ID, NAME, XPATH;
	}
	
	public enum TabName {
		RECEIPT, CLASSIFICATION, CASEDATA;
	}
	
	private final String
		e2bKeyName
	;
	private final IFieldBusinessLogic
		fieldBusinessLogicClass
	;
	private final FieldType
		fieldType
	;	
	
	private final XPathType
		xpathType
	;
	
	private final TabName
		tabName
	;	
	
	public FieldDetails(
		String	p_e2bkeyname
	,	String	p_fieldbusinesslogicclass
	,	String	p_fieldtype
	,	String	p_xpathtype
	,	String	p_tabname
	) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		this.e2bKeyName	=	p_e2bkeyname;
		this.fieldBusinessLogicClass	=	(IFieldBusinessLogic) Class.forName(p_fieldbusinesslogicclass).newInstance();
		this.fieldType	=	FieldType.valueOf(p_fieldtype);
		this.xpathType	=	XPathType.valueOf(p_xpathtype);
		this.tabName	=	TabName.valueOf(p_tabname);
	}

	public String getE2bKeyName() {
		return e2bKeyName;
	}

	public IFieldBusinessLogic getFieldBusinessLogicClass() {
		return fieldBusinessLogicClass;
	}

	public FieldType getFieldType() {
		return fieldType;
	}

	public XPathType getXpathType() {
		return xpathType;
	}	
	
	public TabName getTabName() {
		return tabName;
	}
}
