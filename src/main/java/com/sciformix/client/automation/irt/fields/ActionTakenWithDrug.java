package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class ActionTakenWithDrug implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		if(oPdtWrapper.isKeyValueNullOrEmpty(p_sourcedocumentkey))
		{
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.DrugTabKeys.DRUG_END_DATE))
			{
				if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.PRODUCT_END_DATE))
				{
					p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.UNKNOWN);
				}
				else
				{
					p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.DRUG_DISCONTINUED);
				}	
			}
			else
			{
				p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.DRUG_DISCONTINUED);
			}
		}
		else
		{
			p_dataentrymap.put(p_writerkey,oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey)); 
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
}
