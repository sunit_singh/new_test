package com.sciformix.client.automation.irt.fields;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class InitialReceivedDate implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	private static final Logger log = LoggerFactory.getLogger(InitialReceivedDate.class);
	private static final String EVHUMAN = "EVHUMAN";
	private static final String LOCAL_DATE_FORMAT = "yyyyMMdd";

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		SimpleDateFormat formatter1 = null;
		SimpleDateFormat formatter2 = new SimpleDateFormat(CpatToolCConstants.DATE_FORMAT);
		
		String initialReceivedDate = null;
		String e2bField_senderIdentifier = null;
		String e2bField_messageNumber = null;
		String e2bField_firstReceivedDate = null;
		String e2bField_receiveDate = null;
		String e2bField_receiptDate = null;
		String tempDate = null;
		
		List<String> activityAwarenessDateList = new ArrayList<String>();
		List<Long> tempDateList = null;
		
		Long firstReceivedDate = null;
		Long receiveDate = null;
		Long receiptDate = null;
		
		int count = -1;
		
		try{
			
			//Step 1 : Check if Field M.1.5 contains "EVHUMAN"
			e2bField_senderIdentifier = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.Header.SENDER_IDENTIFIER);
			
			//Step 2 : Fetch the date from Field M.1.4
			e2bField_messageNumber = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.Header.MESSAGE_NUMBER);
			
			if(e2bField_senderIdentifier.contains(EVHUMAN) && !StringUtils.isNullOrEmpty(e2bField_messageNumber)){
				
				tempDate = e2bField_messageNumber.substring(e2bField_messageNumber.indexOf(EVHUMAN)+8,e2bField_messageNumber.indexOf(EVHUMAN)+16);
				formatter1 = new SimpleDateFormat(LOCAL_DATE_FORMAT);
				initialReceivedDate = formatter2.format(formatter1.parse(tempDate));
				
				//Step 3 : Feed the date to the writer map
				p_dataentrymap.put(p_writerkey, initialReceivedDate);
				
			} else {
				
				tempDateList = new ArrayList<Long>();
				
				// Get the respective dates as Strings
				
				if(p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE_COUNT)){
					count = Integer.parseInt(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE_COUNT).trim());
					if(count > 1){
						for(int temp=1; temp<=count; temp++){
							String tempActDate = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE + "_" + temp);
							activityAwarenessDateList.add(tempActDate);
						}
					} else {
						activityAwarenessDateList.add(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE)) ;
					}
				} else if(p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE)){
					activityAwarenessDateList.add(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE)) ;
				}
				
				e2bField_firstReceivedDate = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.CaseIdTabKeys.FIRST_RECEIVED_DATE);
				e2bField_receiveDate = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.RECEIVE_DATE);
				e2bField_receiptDate = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.RECEIPT_DATE);


				// Parse the dates into Long
				
				if(!StringUtils.isNullOrEmpty(e2bField_firstReceivedDate)){
					firstReceivedDate = formatter2.parse(e2bField_firstReceivedDate).getTime();
					tempDateList.add(firstReceivedDate);
				}
				
				if(!StringUtils.isNullOrEmpty(e2bField_receiveDate)){
					receiveDate = formatter2.parse(e2bField_receiveDate).getTime();
					tempDateList.add(receiveDate);
				}
				
				if(!StringUtils.isNullOrEmpty(e2bField_receiptDate)){
					receiptDate = formatter2.parse(e2bField_receiptDate).getTime();
					tempDateList.add(receiptDate);
				}
				
				for(String temp:activityAwarenessDateList) {
					if(!StringUtils.isNullOrEmpty(temp)){
						tempDateList.add(formatter2.parse((String) temp).getTime());
					}
				}
				
				// Sort to get earliest date
				Collections.sort(tempDateList);
				if(!tempDateList.isEmpty()){
					initialReceivedDate = formatter2.format(new Date(tempDateList.get(0)));
				} else {
					initialReceivedDate = SciConstants.StringConstants.EMPTY;
				}
				
				// Feed the date to the writer map
				p_dataentrymap.put(p_writerkey, initialReceivedDate);
			}
			
		} catch (Exception e){
			log.error("FieldName : InitialReceivedDate");
			log.error("Error:",e);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.IRD);
		}
		
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		SimpleDateFormat formatter1 = null;
		SimpleDateFormat formatter2 = new SimpleDateFormat(CpatToolCConstants.DATE_FORMAT);
		
		String initialReceivedDate = null;
		String e2bField_senderIdentifier = null;
		String e2bField_messageNumber = null;
		String e2bField_recentInfoDate = null;
		String e2bField_receiptDate = null;
		String tempDate = null;
		
		List<String> activityAwarenessDateList = new ArrayList<String>();
		List<Long> tempDateList = null;
		
		Long recentInfoDate = null;
		Long receiptDate = null;
		
		int count = -1;
		
		try{
			
			//Step 1 : Check if Field M.1.5 contains "EVHUMAN"
			e2bField_senderIdentifier = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.Header.SENDER_IDENTIFIER);
			
			//Step 2 : Fetch the date from Field M.1.4
			e2bField_messageNumber = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.Header.MESSAGE_NUMBER);
			
			if(e2bField_senderIdentifier.contains(EVHUMAN) && !StringUtils.isNullOrEmpty(e2bField_messageNumber)){
				
				
				tempDate = e2bField_messageNumber.substring(e2bField_messageNumber.indexOf(EVHUMAN)+8,e2bField_messageNumber.indexOf(EVHUMAN)+16);
				formatter1 = new SimpleDateFormat(LOCAL_DATE_FORMAT);
				initialReceivedDate = formatter2.format(formatter1.parse(tempDate));
				
				//Step 3 : Feed the date to the writer map
				p_dataentrymap.put(p_writerkey, initialReceivedDate);
				
			} else {
				
				tempDateList = new ArrayList<Long>();
				
				// Get the respective dates as Strings
				
				if(p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE_COUNT)){
					count = Integer.parseInt((String) p_sourcedocumentdata.getColumnValues(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE_COUNT).get(0).displayValue());
					if(count > 1){
						for(int temp=1; temp<=count; temp++){
							String tempActDate = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE + "_" + temp);
							activityAwarenessDateList.add(tempActDate);
						}
					} else {
						activityAwarenessDateList.add(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE)) ;
					}
				} else if(p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE)){
					activityAwarenessDateList.add(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.ACTIVITY_AWARENESS_DATE)) ;
				}
				
				e2bField_recentInfoDate = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.CaseIdTabKeys.DATE_OF_RECENT_INFO);
				e2bField_receiptDate = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.RECEIPT_DATE);
				
				// Parse the dates into Long
				
				if(!StringUtils.isNullOrEmpty(e2bField_recentInfoDate)){
					recentInfoDate = formatter2.parse(e2bField_recentInfoDate).getTime();
					tempDateList.add(recentInfoDate);
				}
				
				if(!StringUtils.isNullOrEmpty(e2bField_receiptDate)){
					receiptDate = formatter2.parse(e2bField_receiptDate).getTime();
					tempDateList.add(receiptDate);
				}
				
				for(String temp:activityAwarenessDateList) {
					if(!StringUtils.isNullOrEmpty(temp)){
						tempDateList.add(formatter2.parse((String) temp).getTime());
					}
				}
				
				// Sort to get earliest date
				Collections.sort(tempDateList);
				if(!tempDateList.isEmpty()){
					initialReceivedDate = formatter2.format(new Date(tempDateList.get(0)));
				} else {
					initialReceivedDate = SciConstants.StringConstants.EMPTY;
				}
				
				// Feed the date to the writer map
				p_dataentrymap.put(p_writerkey, initialReceivedDate);
			}
			
		} catch (Exception e){
			log.error("FieldName : InitialReceivedDate");
			log.error("Error:",e);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.IRD);
		}
		
	}
	
}
