package com.sciformix.client.automation.irt;

import java.io.File;

public class IRTCaseDocument {
	private String caseFilename;
	private File caseFile;
	
	public String getCaseFilename() {
		return caseFilename;
	}
	public void setCaseFilename(String caseFilename) {
		this.caseFilename = caseFilename;
	}
	public File getCaseFile() {
		return caseFile;
	}
	public void setCaseFile(File caseFile) {
		this.caseFile = caseFile;
	}
}
