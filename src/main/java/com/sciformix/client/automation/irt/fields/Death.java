package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class Death implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		if(oPdtWrapper.isKeyValueNullEmptyOrMatches(p_sourcedocumentkey, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE))
		{
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DEATH))
			{
				if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_DEATH_AND_REPORTED_CAUSE_OF_DEATH))
				{
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.NO);
				}
				else
				{
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
				}
			}
			else
			{
				String value =  oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_DEATH);
				if(value.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES))
				{
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
				}
				else
				{
					if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_DEATH_AND_REPORTED_CAUSE_OF_DEATH))
					{
						p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.NO);
					} else {
					throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.DEATH);
				}
			}
		}
		}
		else
		{	
			p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
}

