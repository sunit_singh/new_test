package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class HospitalizationProlonged implements IFieldBusinessLogic{
	
	ParsedDataTableWrapper oPdtWrapper = null;
	
	/**
	 * This will check below logic 
	 * 	IF (A.1.5) is blank or "No" or "None" then
     * 	check narrative "Admitted to Hospital Related to the Event"
     *     if "Yes" then update the value as "Yes"
     *     else if "Blank" then check the below values
     *          "Date of Hospitalization Admission"
     *          "Date of Hospitalization Discharge"
     *               if values are present then update the value as "Yes"
     *     else if "No" then check the below values
     *          "Date of Hospitalization Admission"
     *          "Date of Hospitalization Discharge"
     *          	if values are present then throw up an error
	 *	else update the value as "Yes"
	 * @param p_dataentrymap The map in which the writer
	 * 		  value need to be set
	 * @param p_sourcedocumentdata Source Document Data
	 * @param p_writerkey The writer key whose value
	 * 		  need to be written
	 */
	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		if(oPdtWrapper.isKeyValueNullEmptyOrMatches(p_sourcedocumentkey, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE)) {
			
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_ADMITTED_TO_HOSPITAL_RELATED_TO_EVENT)) {
				
				if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_HOSP_ADMISSION)
					&& oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_DISCHARGE)) {
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.NO);
				} else {
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
				}
			} else {
				String value =  oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_ADMITTED_TO_HOSPITAL_RELATED_TO_EVENT);
				if(value.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES))
				{
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
				}
				else
				{
					if(!oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_HOSP_ADMISSION) 
						|| !oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_DATE_OF_DISCHARGE))
					{
						throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.HOSPITALIZATION_PROLONGED);
					} 
					else
					{
						p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.NO);
					}
				}
			}
		}
		else
		{	
			p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
}