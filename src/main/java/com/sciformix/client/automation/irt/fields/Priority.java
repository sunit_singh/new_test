package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.commons.utils.ParsedDataTable;

public class Priority implements IFieldBusinessLogic {

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {

		String death = null;
		String hospitalization = null;
		String lifeThreatening = null;
		String incapacitating = null;
		String congenitalDefect = null;
		String otherMedicalConditions = null;
		String reporterSeriousness = null;
		
		death = p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH);
		hospitalization = p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED);
		lifeThreatening = p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING);
		incapacitating = p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY);
		congenitalDefect = p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT);
		otherMedicalConditions = p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION);
		reporterSeriousness = p_dataentrymap.get(IRTConstants.CaseData.SERIOUS_ASSESSMENT_REPORT_SERIOUSNESS);
		
		if (death.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES) || lifeThreatening.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES)) {
			p_dataentrymap.put(p_writerkey, "1");
		} else if (hospitalization.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES) ||
				incapacitating.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES) ||
				congenitalDefect.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES) ||
				otherMedicalConditions.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES)) {
			p_dataentrymap.put(p_writerkey, "2");
		} else if (IRTConstants.SERIOUS.equalsIgnoreCase(reporterSeriousness)){
			p_dataentrymap.put(p_writerkey, "2");
		} else {
			p_dataentrymap.put(p_writerkey, "5");
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}

}
