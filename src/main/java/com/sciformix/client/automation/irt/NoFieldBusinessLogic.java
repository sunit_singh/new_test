package com.sciformix.client.automation.irt;

import java.util.Map;

import com.sciformix.commons.utils.ParsedDataTable;

public class NoFieldBusinessLogic implements IFieldBusinessLogic {

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		return;
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		return;
	}

}
