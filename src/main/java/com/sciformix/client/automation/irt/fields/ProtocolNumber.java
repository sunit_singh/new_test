package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class ProtocolNumber implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	private static final String
		EXT_SOURCE_PROTOCOL_NUMBER	=	"HIVE"
	;	

	@Override
	public void applyBusinessLogicInitial(
		Map<String, String> p_dataentrymap
	, 	ParsedDataTable 	p_sourcedocumentdata
	,	String 				p_writerkey
	, 	String 				p_sourcedocumentkey
	) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String	l_e2bfieldvalue			=	oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey),
				l_narrextsrccountkey	=	null,
				l_narrextsrc			=	null,
				l_protocolnumber		=	StringConstants.EMPTY;
		int		l_narrextsrccountval	=	0;
		
		if (StringUtils.isNullOrEmpty(l_e2bfieldvalue)) {
			
			if (p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_COUNT)) {
				l_narrextsrccountkey	=	oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_COUNT);
			}
			
			l_narrextsrccountval	=	StringUtils.isNullOrEmpty(l_narrextsrccountkey) ? 0 : Integer.parseInt(l_narrextsrccountkey);
			if (l_narrextsrccountval == 0) {
				l_narrextsrc	=	oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE);
				if (EXT_SOURCE_PROTOCOL_NUMBER.equalsIgnoreCase(l_narrextsrc)) {
					l_protocolnumber	=	oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_NUMBER);
				}
			} else {
				for (int l_i=1; l_i <= l_narrextsrccountval; l_i++) {
					l_narrextsrc	=	oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE + "_" + l_i);
					if (EXT_SOURCE_PROTOCOL_NUMBER.equalsIgnoreCase(l_narrextsrc)) {
						l_protocolnumber	=	oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_NUMBER + "_" + l_i);
						break;
					}
				}
			}
		} else {
			l_protocolnumber	=	l_e2bfieldvalue;
		}
		p_dataentrymap.put(p_writerkey, l_protocolnumber);
	}

	@Override
	public void applyBusinessLogicFollowUp(
		Map<String, String> p_dataentrymap
	, 	ParsedDataTable 	p_sourcedocumentdata
	,	String 				p_writerkey
	, 	String 				p_sourcedocumentkey
	) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
}
