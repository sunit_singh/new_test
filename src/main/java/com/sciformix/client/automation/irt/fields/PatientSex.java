package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class PatientSex implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;
	
	private static final String SEX_UNKNOWN = "Unknown";

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String e2b_patientSex = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
		if(!StringUtils.isNullOrEmpty(e2b_patientSex)){
			p_dataentrymap.put(p_writerkey, e2b_patientSex);
		} else {
			p_dataentrymap.put(p_writerkey, SEX_UNKNOWN);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}

}
