package com.sciformix.client.automation.irt;

import java.util.HashMap;
import java.util.Map;

public class IRTConstants {
	
	public static final String IRT_WILDCARD = "%";
	public static final String TBODY_TAG= "tbody";
	public static final String BUTTON_TAG= "button";
	public static final String INITIAL_REVIEW= "Initial Review";
	public static final String DOC_FORMAT= "E2B PDF Report";
	public static final String XPATH_BY_NAME = "name";
	public static final String INPUT_TYPE_INPUT = "input";	
	public static final String INPUT_TYPE_ATTRIBUTE = "a";
	public static final String INPUT_TYPE_IMAGE = "img";	
	public static final String XPATH_BY_ID = "id";	
	public static final String TABLE_BODY = "tbody";
	public static final String DATE_FORMAT = "dd-MMM-yyyy";
	public static final String SELECT_TAG = "select";
	public static final String YES = "yes";
	public static final String EMEA_COUNTRIES = "emea.countries";
	public static final String SERIOUS_KEYWORDS = "serious.keywords";
	public static final String SOURCE_DOC_PREFIX = "E2bPdfReportAgx";
	public static final String SCEPTRE_SEARCH_RESULT_THRESHOLD = "sceptre.search.result.threshold";
	
	public class Login
	{
		public static final String USERNAME_XPATH		=	"login.username";
		public static final String PASSWORD_XPATH		=	"login.password";
		public static final String LOGIN_XPATH			=	"login.submit";
		public static final String LOGIN_CONTINUE_XPATH	=	"login.submit.continue";
		public static final String IRT_LOGOUT	  =	"irt.logout";
	}
	
	public class Wait
	{
		public static final String MIN_WAIT		=	"minWait";
		public static final String COMMON_WAIT = "commonWait2";
		public static final String PASS_WAIT = "minwait10sec";
	}
	
	public class SearchCase
	{
		public static final String INBOUND_XPATH		=	"menu.inbound";
		public static final String INBOUND_ID			=	"menu.inbound.id";
		public static final String ADVANCE_SEARCH_XPATH	=	"menu.inbound.advancesearch";
		public static final String ADVANCE_SEARCH_NAME	=	"menu.inbound.advancesearch.name";
		public static final String ADVANCE_SEARCH_RECPT_NO_XPATH	=	"menu.inbound.advancesearch.receiptno";
		public static final String ADVANCE_SEARCH_BUTTON_XPATH		=	"menu.inbound.advancesearch.search";
		
		public static final String INBOUND_INBOUND_LISTING 			= 	"menu.inbound.inboundlisting";
		public static final String INBOUND_INBOUND_LISTING_SEARCH	=	"menu.inbound.inboundlisting.search";
		public static final String INBOUND_INBOUND_LISTING_SEARCH_BUTTION	=	"menu.inbound.inboundlisting.search.button";
	}
	
	public class CaseProcess
	{
		public static final String DOWNLOAD_IMAGE_ID		= "menu.inbound.advancesearch.search.download";
		public static final String EDIT_IMAGE_ID			= "menu.inbound.advancesearch.search.edit";
		public static final String EDIT_CONFIRM_PASSWORD	= "menu.inbound.advancesearch.search.edit.comfirmpassword";
		public static final String EDIT_CONFIRM_PASSWORD_SUBMIT	= "menu.inbound.advancesearch.search.edit.comfirmpasswordsubmit";
		public static final String EDIT_CASE_TABLE_BODY="menu.inbound.advancesearch.search.tablebody";
		public static final String EDIT_CASE_LOCK="menu.inbound.advancesearch.search.lock";
		public static final String EDIT_CASE_FEOM_MENU="menu.downloadpdf";
		public static final String EDIT_SELECT_REVIEW="menu.inbound.inboundlisting.search.select.label";
		public static final String EDIT_SELECT_REVIEW_INPUT="menu.inbound.inboundlisting.search.select.input";
		public static final String EDIT_SELECT_REVIEW_SELECT="menu.inbound.inboundlisting.search.select.select";

		public static final String CASE_EXIT="case.exit";
	}
	
	public class UnlockCase
	{
		public static final String ADMINISTRATION		=	"menu.administration";
		public static final String ADMINISTRATION_UNLOCK	=	"menu.administration.lockinfo";
		public static final String ADMINISTRATION_UNLOCK_CASE_TABLE	=	"menu.administration.unlock.casetable";
		public static final String ADMINISTRATION_UNLOCK_BUTTON		=	"menu.administration.unlock.button";
	}
	
	public class CaseDocumentDownload
	{
		public static final String DOCUMENT_MENU="menu.dataentry.document";
		public static final String DOWNLOAD_CASE_TABLE_BODY_ID="download.casedoc.tablebodyid";
		public static final String DOWNLOAD_CASE_DOC_PREFIX="download.casedoc.prefix";
		public static final String DOWNLOAD_CASE_DOC_SUFFIX="download.casedoc.suffix";
		public static final String DOWNLOAD_CASE_DOC_SELECT_FORMAT="download.casedoc.selectformat";
		public static final String DOWNLOAD_CASE_DOC_CLOSE="download.casedoc.close";
		public static final String DOWNLOAD_CASE_DOC_EMBEDED="download.casedoc.embededtag";
		
		public static final String DOCUMENT_NAME 					= "Document Name";
		public static final String DOCUMENT_TYPE 					= "Document Type";
		public static final String DOCUMENT_NAME_VALUE				="E2B_PDF_report"				;
		public static final String DOCUMENT_TYPE_VALUE				="IRT Supporting"				;
		public static final String MESA_DOCUMENT_NAME_VALUE 		="PQM"							;
	}
	
	public class ReceiptSummaryForms
	{
		public static final String  GENERAL_INFO ="General Information";
		public static final String  SERIPUSNESS_CRITERIA ="Seriousness Criteria";
		public static final String  ADDITIONAL_REFERENCE ="Additional Reference";
		public static final String  SUSPECT_DRUG ="Suspect Drug";
		public static final String  ADVERSE_EVENT ="Adverse Event";
		public static final String  PATIENT ="Patient";
		public static final String  REPORTER ="Reporter";
	}
	
	public class CaseDataForm
	{
		public static final String  SERIOUS_ASSESMENT ="SeriousAsssesment";
	}
	
	public class DataEntry
	{
		public static final String  RECEIPT_SUMMARY ="receiptSummary.tab";
		
		public static final String  EDITRECEIPT_RECEIPT ="dataentry.editreceipt.receipt";
		public static final String  EDITRECEIPT_MESSAGE ="dataentry.editreceipt.message";
		public static final String  EDITRECEIPT_E2B_ACC_DATE ="dataentry.editreceipt.e2baccdate";
		public static final String  EDITRECEIPT_SENDER ="dataentry.editreceipt.sender";
		public static final String  EDITRECEIPT_RECEIVER ="dataentry.editreceipt.receiver";
		public static final String  EDITRECEIPT_RECOVER_ORG_OWNER_UNIT ="dataentry.editreceipt.receiverorgenizationownerunit";
		public static final String  EDITRECEIPT_OWNER ="dataentry.editreceipt.owner";
		public static final String  EDITRECEIPT_PROTOCOL_NO ="dataentry.editreceipt.protocalno";
		public static final String  EDITRECEIPT_LOC_REC_DATE="dataentry.editreceipt.localreceiveddate";
		public static final String  EDITRECEIPT_REC_DATE ="dataentry.editreceipt.receiveddate";
		public static final String  EDITRECEIPT_PENDING_COMMENT ="dataentry.editreceipt.pendingcomments";

		public static final String  GENINFO_REPORT_TYPE="dataentry.generalinformation.reporttype";
		public static final String  GENINFO_LOC_CASE_ID="dataentry.generalinformation.localcaseid";
		public static final String  GENINFO_INIT_REC_DATE="dataentry.generalinformation.initialreceivedate";
		public static final String  GENINFO_LAST_REC_DATE="dataentry.generalinformation.lastreceivedate";
		public static final String  GENINFO_COUNTRY_OF_OCCUR="dataentry.generalinformation.countryofoccurence";

		public static final String SERUOUSNESS_CRIT_DEATH="dataentry.seriousnesscriteria.death";
		public static final String SERUOUSNESS_CRIT_BIRTH_DEFECT="dataentry.seriousnesscriteria.birthdefect";
		public static final String SERUOUSNESS_CRIT_DISABILITY="dataentry.seriousnesscriteria.disability";
		public static final String SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION="dataentry.seriousnesscriteria.othermedicalimportantcondition";
		public static final String SERUOUSNESS_CRIT_LIFE_THETENING="dataentry.seriousnesscriteria.lifetheatening";
		public static final String SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED="dataentry.seriousnesscriteria.hospitalizationprolonged";

		public static final String ADD_REF_COMP_UNIT="dataentry.additionalreferences.companyunit";
		public static final String ADD_REF_OTHER_INDT_REF="dataentry.additionalreferences.otheridentificationreferrences";
		public static final String ADD_REF_OTHER_INDT_NO="dataentry.additionalreferences.otheridentificationnumber";
		public static final String ADD_REF_ADD_ENTRY="dataentry.additionalreferences.addentry";
		public static final String ADD_REF_ENTRY_COUNT="dataentry.additionalreferences.entrycount";

		public static final String SUSP_DRUG_PRODUCT_FLAG="dataentry.suspectdrugs.productflag";
		public static final String SUSP_DRUG_PREF_PROD_DESC="dataentry.suspectdrugs.preferredproductdescription"; 
		public static final String SUSP_DRUG_PROD_DESC="dataentry.suspectdrugs.productdescription";
		
		public static final String SUSP_DRUG_PROD_DESC_SERACH="dataentry.suspectdrugs.productdescription.search";
		public static final String SUSP_DRUG_PROD_DESC_SEARCH_DRUG="dataentry.suspectdrugs.productdescription.search.drugtype";
		public static final String SUSP_DRUG_PROD_DESC_SEARCH_PREF_PROD_DESC="dataentry.suspectdrugs.productdescription.search.preferedProducDescription";
		public static final String SUSP_DRUG_PROD_DESC_SEARCH_PREF_ACTIVE_INGREDIENT = "dataentry.suspectdrugs.productdescription.search.activeingredient";
		public static final String SUSP_DRUG_PROD_DESC_SEARCH_SEARCHBUTTON="dataentry.suspectdrugs.productdescription.search.searchbutton";
		public static final String SUSP_DRUG_PROD_DESC_SEARCH_TBODY="dataentry.suspectdrugs.productdescription.search.tbody";
		public static final String SUSP_DRUG_PROD_DESC_SEARCH_ACTIVE="dataentry.suspectdrugs.productdescription.search.searchbutton.active";
		
		public static final String SUSP_DRUG_FORMULATION="dataentry.suspectdrugs.formulation";
		public static final String SUSP_DRUG_ACTION_WITH_DRUG="dataentry.suspectdrugs.actionwithdrug";
		public static final String SUSP_DRUG_ADD_ENTRY="dataentry.suspectdrugs.addenrty";
		public static final String SUSP_DRUG_ENTRY_COUNT="dataentry.suspectdrugs.entrycount";

		public static final String DRUG_THERAPY_ROUT_ADMIN="dataentry.drugtherapy.routofadmin";
		public static final String DRUG_THERAPY_ROUT_ADMIN_ADD_ENTRY="dataentry.drugtherapy.routofadmin.addentry";
		public static final String DRUG_THERAPY_ROUT_ADMIN_ENTRY_COUNT="dataentry.drugtherapy.routofadmin.entrycount";

		public static final String ADVERSE_EVENT_REPORTED_ITEM="dataentry.adverseevent.reporteditem";
		public static final String ADVERSE_EVENT_SERIOUS="dataentry.adverseevent.serious";
		public static final String ADVERSE_EVENT_OUTCOME="dataentry.adverseevent.outcome";
		public static final String ADVERSE_EVENT_ADD_ENTRY="dataentry.adverseevent.addentry";
		public static final String ADVERSE_EVENT_ENTRY_COUNT="dataentry.adverseevent.entrycount";

		public static final String PATIENT_PATIENT_ID="dataentry.patient.patientid";
		public static final String PATIENT_PATIENT_UNKNOWN="dataentry.patient.patientidunknown";
		public static final String PATIENT_SEX="dataentry.patient.sex";
		public static final String PATIENT_AGE_GROUP="dataentry.patient.agegroup";
		public static final String PATIENT_DOB="dataentry.patient.dateofbirth";
		public static final String PATIENT_AGE_EVENT_TEXT="dataentry.patient.ageattimeofevent.value";
		public static final String PATIENT_AGE_EVENT_UNIT="dataentry.patient.ageattimeofevent.unit";

		public static final String PROTOCOL_PROTOCOL_NO="dataentry.protocol.protocolno";
		public static final String PROTOCOL_PROTOCOL_NO_SEARCH="dataentry.protocol.protocolno.search";
		public static final String PROTOCOL_PROTOCOL_NO_SEARCH_TBODY="dataentry.protocol.protocolno.search.tbody";
		public static final String PROTOCOL_PROTOCOL_NO_SEARCH_CANCEL="dataentry.protocol.protocolno.search.close";
		public static final String PROTOCOL_STUDY_PATIENT_NO="dataentry.protocol.studypatientno";
		public static final String PROTOCOL_SITE_NO="dataentry.protocol.siteno";

		public static final String REPORTER_QUALIFICATION="dataentry.reporter.qualification";
		public static final String REPORTER_DO_NOT_REPORT_TIME="dataentry.reporter.donotreporttime";
		public static final String REPORTER_FIRST="dataentry.reporter.first";
		public static final String REPORTER_LAST="dataentry.reporter.last";
		public static final String REPORTER_CITY="dataentry.reporter.city";
		public static final String REPORTER_STATE="dataentry.reporter.state";
		public static final String REPORTER_COUNTRY="dataentry.reporter.country";
		public static final String REPORTER_ZIPCODE="dataentry.reporter.zipcode";
		public static final String REPORTER_ADD_ENTRY="dataentry.reporter.addentry";
		public static final String REPORTER_ENTRY_COUNT="dataentry.reporter.entrycount";
		
		public static final String LITERATURE_ART_TITLE="dataentry.literature.articletitle";
		public static final String LITERATURE_JOURNAL_TITLE="dataentry.literature.journaltitle";
		public static final String LITERATURE_ADD_ENTRY="dataentry.literature.addentry";

		public static final String AUTHOR_TITLE="dataentry.author.authortitle";
		public static final String AUTHOR_FIRST="dataentry.author.first";
		public static final String AUTHOR_MI="dataentry.author.mi";
		public static final String AUTHOR_LAST="dataentry.author.last";
		public static final String AUTHOR_ADD_ENTRY="dataentry.author.addentry";

	//	public static final String NARRATIVE_EVENT_DESC="dataentry.narrative.eventdescription";
	}
	
	public class Classification
	{
		public static final String CLASSIFICATION_TAB="classification.tab";
		public static final String DUPLICATE_SEARCH_CRITERIA_SELECT_CRITERIA="classification.duplicatesearchcriteria.selectcriteria";
		public static final String DUPLICATE_SEARCH_CRITERIA_AER_NO="classification.duplicatesearchcriteria.aernumber";
		public static final String DUPLICATE_SEARCH_CRITERIA_LRN="classification.duplicatesearchcriteria.lrn";
		public static final String DUPLICATE_SEARCH_CRITERIA_TRACKING_NO="classification.duplicatesearchcriteria.trackingno";
		public static final String DUPLICATE_SEARCH_CRITERIA_LOCAL_CASE_ID="classification.duplicatesearchcriteria.localcaseid";
		public static final String DUPLICATE_SEARCH_CRITERIA_LOCAL_CASE_ID_ID="classification.duplicatesearchcriteria.localcaseid.id";
		public static final String DUPLICATE_SEARCH_CRITERIA_SAFETY_REPORT_ID="classification.duplicatesearchcriteria.safetyreportid";
		public static final String DUPLICATE_SEARCH_CRITERIA_COMPANY_NO="classification.duplicatesearchcriteria.companyno";
		public static final String DUPLICATE_SEARCH_CRITERIA_AUTHORITY_NO="classification.duplicatesearchcriteria.authorityno";
		public static final String DUPLICATE_SEARCH_CRITERIA_LAST_REC_FIELD_1="classification.duplicatesearchcriteria.lastreceivedatefield1";
		public static final String DUPLICATE_SEARCH_CRITERIA_LAST_REC_FIELD_2="classification.duplicatesearchcriteria.lastreceivedatefield2";
		public static final String DUPLICATE_SEARCH_CRITERIA_COUNTRY_OF_OCCURANCE="classification.duplicatesearchcriteria.countryofoccurance";
		public static final String DUPLICATE_SEARCH_CRITERIA_COUNTRY_OF_OCCURANCE_INPUT="classification.duplicatesearchcriteria.countryofoccuranceinput";
		public static final String DUPLICATE_SEARCH_CRITERIA_COUNTRY_OF_OCCURANCE_SELECT="classification.duplicatesearchcriteria.countryofoccuranceselect";
		
		public static final String DUPLICATE_SEARCH_CRITERIA_REPORT_TYPE="classification.duplicatesearchcriteria.reporttype";
		public static final String DUPLICATE_SEARCH_CRITERIA_REPORT_TYPE_INPUT="classification.duplicatesearchcriteria.reporttypeinput";
		public static final String DUPLICATE_SEARCH_CRITERIA_REPORT_TYPE_SELECT="classification.duplicatesearchcriteria.reporttypeselect";
		
		public static final String DUPLICATE_SEARCH_CRITERIA_PRIMARY_SOURCE_COUNTRY="classification.duplicatesearchcriteria.primarysourcecountry";
		public static final String DUPLICATE_SEARCH_CRITERIA_PRIMARY_SOURCE_COUNTRY_INPUT="classification.duplicatesearchcriteria.primarysourcecountryinput";
		public static final String DUPLICATE_SEARCH_CRITERIA_PRIMARY_SOURCE_COUNTRY_SELECT="classification.duplicatesearchcriteria.primarysourcecountryselect";
		public static final String DUPLICATE_SEARCH_CRITERIA_PRODUCT_DESC="classification.duplicatesearchcriteria.productdescription";
		public static final String DUPLICATE_SEARCH_CRITERIA_SOURCE_ID_NO="classification.duplicatesearchcriteria.sourceidentificationnumber";
		public static final String DUPLICATE_SEARCH_CRITERIA_ONSET_DATA_FIELD_1="classification.duplicatesearchcriteria.onsetdatefield1";
		public static final String DUPLICATE_SEARCH_CRITERIA_ONSET_DATA_FIELD_2="classification.duplicatesearchcriteria.onsetdatefield2";
		public static final String DUPLICATE_SEARCH_CRITERIA_CESSATION_DATA_FIELD_1="classification.duplicatesearchcriteria.cessationdatefiled1";
		public static final String DUPLICATE_SEARCH_CRITERIA_CESSATION_DATA_FIELD_2="classification.duplicatesearchcriteria.cessationdatefiled2";
		public static final String DUPLICATE_SEARCH_CRITERIA_REPORTED_TERM="classification.duplicatesearchcriteria.reportedterm";
		public static final String DUPLICATE_SEARCH_CRITERIA_ARTICLE_TITLE="classification.duplicatesearchcriteria.articletitle";
		public static final String DUPLICATE_SEARCH_CRITERIA_DOB_FIELD_1="classification.duplicatesearchcriteria.dateofbirthfield1";
		public static final String DUPLICATE_SEARCH_CRITERIA_DOB_FIELD_2="classification.duplicatesearchcriteria.dateofbirthfield2";
		public static final String DUPLICATE_SEARCH_CRITERIA_PAT_ID_B_11="classification.duplicatesearchcriteria.patientId.B.11";
		public static final String DUPLICATE_SEARCH_CRITERIA_SEX="classification.duplicatesearchcriteria.sex";
		public static final String DUPLICATE_SEARCH_CRITERIA_SEX_INPUT="classification.duplicatesearchcriteria.sexinput";
		public static final String DUPLICATE_SEARCH_CRITERIA_SEX_SELECT="classification.duplicatesearchcriteria.sexselect";
		
		public static final String DUPLICATE_SEARCH_CRITERIA_PAT_ID="classification.duplicatesearchcriteria.patientid";
		public static final String DUPLICATE_SEARCH_CRITERIA_AGE_OF_DATH_FIELD_1="classification.duplicatesearchcriteria.ageofdeathfield1";
		public static final String DUPLICATE_SEARCH_CRITERIA_AGE_OF_DATH_FIELD_2="classification.duplicatesearchcriteria.ageofdeathfield2";
		public static final String DUPLICATE_SEARCH_CRITERIA_AGE_OF_DEATH_UNIT="classification.duplicatesearchcriteria.ageofdeathunit";
		public static final String DUPLICATE_SEARCH_CRITERIA_AGE_OF_DEATH_UNIT_INPUT="classification.duplicatesearchcriteria.ageofdeathunitinput";
		public static final String DUPLICATE_SEARCH_CRITERIA_AGE_OF_DEATH_UNIT_SELECT="classification.duplicatesearchcriteria.ageofdeathunitselect";
		
		public static final String DUPLICATE_SEARCH_CRITERIA_PRITOCOL_NO="classification.duplicatesearchcriteria.protocolno";
		public static final String DUPLICATE_SEARCH_CRITERIA_SITE_NO="classification.duplicatesearchcriteria.siteno";
		public static final String DUPLICATE_SEARCH_CRITERIA_STUDY_PAT_NO="classification.duplicatesearchcriteria.studypatientno";
		public static final String DUPLICATE_SEARCH_CRITERIA_REPORTER_FIRST_NAME="classification.duplicatesearchcriteria.reporterfirstname";
		public static final String DUPLICATE_SEARCH_CRITERIA_REPORTER_MIDDLE_NAME="classification.duplicatesearchcriteria.reportermiddlename";
		public static final String DUPLICATE_SEARCH_CRITERIA_REPORTER_LAST_NAME="classification.duplicatesearchcriteria.reporterlastname";
		public static final String DUPLICATE_SEARCH_CRITERIA_STATE="classification.duplicatesearchcriteria.state";
		public static final String DUPLICATE_SEARCH_CRITERIA_ZIP_CODE="classification.duplicatesearchcriteria.zipcode";
		public static final String DUPLICATE_SEARCH_CRITERIA_COUNTRY="classification.duplicatesearchcriteria.country";
		public static final String DUPLICATE_SEARCH_CRITERIA_COUNTRY_INPUT="classification.duplicatesearchcriteria.countryinput";
		public static final String DUPLICATE_SEARCH_CRITERIA_COUNTRY_SELECT="classification.duplicatesearchcriteria.countryselect";
		public static final String DUPLICATE_SEARCH_CRITERIA_RECEIDENCE="classification.duplicatesearchcriteria.receidence";
		public static final String DUPLICATE_SEARCH_CRITERIA_CITY="classification.duplicatesearchcriteria.city";
		public static final String DUPLICATE_SEARCH_CRITERIA_HOPITAL_NAME="classification.duplicatesearchcriteria.hospital/orgname";
		public static final String DUPLICATE_SEARCH_CRITERIA_OTHER_ID_NO="classification.duplicatesearchcriteria.otheridentificationo";
		public static final String DUPLICATE_SEARCH_CRITERIA_OTHER_ID_REF="classification.duplicatesearchcriteria.ortheridentificatioreference";
		public static final String DUPLICATE_SEARCH_CRITERIA_OTHER_ID_REF_INPUT="classification.duplicatesearchcriteria.ortheridentificatioreferenceinput";
		public static final String DUPLICATE_SEARCH_CRITERIA_OTHER_ID_REF_SELECT="classification.duplicatesearchcriteria.ortheridentificatioreferenceselect";
		public static final String DUPLICATE_SEARCH_CRITERIA_INCLUDE_PREV_VER_IRT_CHECK="classification.duplicatesearchcriteria.includeallpreviousversionirtcheck";
		public static final String DUPLICATE_SEARCH_CRITERIA_SEARCH="classification.duplicatesearchcriteria.searchbutton";
		public static final String DUPLICATE_SEARCH_CRITERIA_SEARCH_ID="classification.duplicatesearchcriteria.searchbutton.id";
		public static final String DUPLICATE_SEARCH_CRITERIA_SAVE_AND_SEARCH="classification.duplicatesearchcriteria.saveandsearchbutton";
		public static final String DUPLICATE_SEARCH_CRITERIA_RESET="classification.duplicatesearchcriteria.reset";
		public static final String DUPLICATE_SEARCH_CRITERIA_PROCCED="classification.duplicatesearchcriteria.procces";
		public static final String DUPLICATE_SEARCH_CRITERIA_PROCCED_ID="classification.duplicatesearchcriteria.procces.id";
		public static final String DUPLICATE_SEARCH_CRITERIA_PROCCESSING_WINDOW="classification.duplicatesearchcriteria.proccesingwindow";
	}

	public class Duplicate{
		
		public static final String DUPLICATE_TBODY="duplicate.tbody.id";
		public static final String DUPLICATE_TBODY_XPATH="duplicate.tbody.xpath";
		public static final String DUPLICATE_RECORD_RECEIPT_PREFIX = "duplicate.tbody.record.receipt.prefix";
		public static final String DUPLICATE_RECORD_RECEIPT_SUFFIX = "duplicate.tbody.record.receipt.suffix";
		public static final String DUPLICATE_RECORD_LRN_PREFIX = "duplicate.tbody.record.lrn.prefix";
		public static final String DUPLICATE_RECORD_LRN_SUFFIX = "duplicate.tbody.record.lrn.suffix";
	}
	
	public class AssignCase
	{
		public static final String ASSIGN_CASE_SELECT_CLASSIFICATION="assign.case.selectclassification";
		public static final String ASSIGN_CASE_COMMENTS="assign.case.comments";
		public static final String ASSIGN_CASE_VERSION_TYPE="assign.case.versiontype";
		public static final String ASSIGN_CASE_AER="assign.case.aer";
		public static final String ASSIGN_CASE_LRN="assign.case.lrn";
		public static final String ASSIGN_CASE_NEXT="assign.case.next";
		
	}
	
	public static enum CASECLASSIFICATION
	{
		NEW("New"),FOLLOWUP("Follow-up"),DUPLICATE("Duplicate");
		
		private String displayName = null;
		
		CASECLASSIFICATION(String displayName)
		{
			this.displayName = displayName;
		}
		public String displayName()
		{
			return displayName;
		}
	}
	
	public class CaseData
	{	
		public static final String CASEDATA="casedatatab";
		
		public static final String CHOOSEASSESSMENTFORM ="casedata.chooseassessmentform";
		public static final String CHOOSEASSESSMENTFORMINPUT ="casedata.chooseassessmentforminput";
		public static final String CHOOSEASSESSMENTFORMSELECT ="casedata.chooseassessmentformselect";
	
		
		public static final String SERIOUS_ASSESSMENT_REPORT_TYPE="casedata.seriousassessment.reporttype";
		public static final String SERIOUS_ASSESSMENT_REPORT_TYPE_ID="casedata.seriousassessment.reporttype.id";
		public static final String SERIOUS_ASSESSMENT_REPORT_SERIOUSNESS="casedata.seriousassessment.reporterseriousness";
		public static final String SERIOUS_ASSESSMENT_DEATH="casedata.seriousassessment.death";
		public static final String SERIOUS_ASSESSMENT_LIFE_THREATENING="casedata.seriousassessment.lifethreatening";
		public static final String SERIOUS_ASSESSMENT_REGULARITY_RELEVENT="casedata.seriousassessment.regularityrelevent";
		public static final String SERIOUS_ASSESSMENT_PRIORITY="casedata.seriousassessment.priority";
		public static final String SERIOUS_ASSESSMENT_CLASSIFICATION_COMMENTS="casedata.seriousassessment.classificationcomments";
		public static final String SERIOUS_ASSESSMENT_COMMENTS="casedata.seriousassessment.comments";
		public static final String SERIOUS_ASSESSMENT_SCEPTRE_ACTION="casedata.seriousassessment.sceptreaction";
		public static final String SERIOUS_ASSESSMENT_SCEPTER_AER="casedata.seriousassessment.scepteraer";
	}
	
	public class Notification
	{
		public static final String NOTIFICATIONS ="notifications";
		public static final String NOTIFICATIONS_MANUALNOTIFICATIONRULE ="notifications.manualnotificationrule";
		public static final String NOTIFICATIONS_MANUALNOTIFICATIONRULEINPUT ="notifications.manualnotificationruleinput";
		public static final String NOTIFICATIONS_MANUALNOTIFICATIONRULESELECT ="notifications.manualnotificationruleselect";
		public static final String NOTIFICATIONS_MANUALNOTIFICATIONRULEXECUTE ="notifications.manualnotificationrulexecute";
		public static final String NOTIFICATIONS_ADD ="notifications.add";
		public static final String NOTIFICATIONS_NOTIFY ="notifications.notify";
	
		public static final String NOTIFICATIONS_ADDRECEIPENT_ADD ="notifications.addreceipent.add";
		public static final String NOTIFICATIONS_ADDRECEIPENT_ADDSENDERSCONTACT ="notifications.addreceipent.addsenderscontact";
		public static final String NOTIFICATIONS_ADDRECEIPENT_ADDADDITIONALUSER ="notifications.addreceipent.addadditionaluser";
		public static final String NOTIFICATIONS_ADDRECEIPENT_SUBJECT ="notifications.addreceipent.subject";
		public static final String NOTIFICATIONS_ADDRECEIPENTNS_ADDRECEIPENT_ADDBODY ="notifications.addreceipent.addbody";
		public static final String NOTIFICATIONS_ADDRECEIPENT_ADDCOMMENTS ="notifications.addreceipent.addcomments";
		public static final String NOTIFICATIONS_ADDRECEIPENT_SUMMARYSHEETLANGUAGE ="notifications.addreceipent.summarysheetlanguage";
		public static final String NOTIFICATIONS_ADDRECEIPENT_SUMMARYSHEETLANGUAGEINPUT ="notifications.addreceipent.summarysheetlanguageinput";
		public static final String NOTIFICATIONS_ADDRECEIPENT_SUMMARYSHEETLANGUAGESELECT ="notifications.addreceipent.summarysheetlanguageselect";
		public static final String NOTIFICATIONS_ADDRECEIPENT_INCLUDEFILECHECK="notifications.addreceipent.includefilecheck";
		public static final String NOTIFICATIONS_ADDRECEIPEN_ADDBUTTON ="notifications.addreceipent.addbutton";
		public static final String NOTIFICATIONS_ADDRECEIPENT_ADDANDNOTIFYBUTTON ="notifications.addreceipent.addandnotifybutton";
	}
	
	public class Send
	{
		public static final String SELECT_SEND_ACTION ="send.case.select";
		public static final String SEND_CASE_FLAG ="send.case.flag";
	}
	

	public class IrtDuplicateRecord{
		
		public static final String AER_PREFIX = "duplicate.record.aer.prefix";
		public static final String AER_SUFFIX = "duplicate.record.aer.suffix";
		public static final String SFE_PREFIX = "duplicate.record.sfe.prefix";
		public static final String SFE_SUFFIX = "duplicate.record.sfe.suffix";
		public static final String CASE_ID = "duplicate.record.caseid";
		public static final String OCCUR_COUNTRY ="duplicate.record.countryofoccurence";
		public static final String PATIENT_INITIALS = "duplicate.record.patient.initials";
		public static final String PATIENT_GENDER = "duplicate.record.patient.gender";
		public static final String PATIENT_DOB = "duplicate.record.patient.dob";
		public static final String REPORTER_FIRST = "duplicate.record.reporter.first";
		public static final String REPORTER_LAST = "duplicate.record.reporter.last";
		public static final String REPORTER_COUNTRY = "duplicate.record.reporter.country";
		public static final String DRUG_NAME = "duplicate.record.drugname";
		
		public static final String DRUG_NAME_ENTRY_COUNT = "duplicate.record.drugname.entrycount";
		public static final String DRUG_NAME_NEXT = "duplicate.record.drugname.nextvalue.link";

		public static final String REPORTED_REACTION = "duplicate.record.reportedReaction";
		public static final String REPORTED_SERIOUS="duplicate.record.reportedserious";
		public static final String REPORTED_OUTCOME="duplicate.record.reportedoutcome";
		public static final String REACTION_ENTRYCOUNT= "duplicate.record.reaction.entrycount";
		public static final String REACTION_NEXT="duplicate.record.reacion.nextvalue.link";
		
		public static final String WINDOW_CLOSE = "duplicate.record.window.close";

	}
	
	public class ResultSummaryUI
	{	
		
		public static final String CASEDATA_SERIOUS_ASSESSMENT="seriousassessment";
		public static final String CASEDATA_DESPOSITION="sceptre";
		
		public static final String RECEIPT_SUMMARY_GENINFO="dataentry.generalinformation";
		public static final String RECEIPT_SUMMARY_SERUOUSNESS_CRIT="dataentry.seriousnesscriteria";
		public static final String RECEIPT_SUMMARY_ADD_REF="dataentry.additionalreferences";
		public static final String RECEIPT_SUMMARY_SUSP_DRUG="dataentry.suspectdrugs";
		public static final String RECEIPT_SUMMARY_DRUG_THERAPY="dataentry.drugtherapy";
		public static final String RECEIPT_SUMMARY_ADVERSE_EVENT="dataentry.adverseevent";
		public static final String RECEIPT_SUMMARY_PATIENT = "dataentry.patient";
		public static final String RECEIPT_SUMMARY_PROTOCOL="dataentry.protocol";
		public static final String RECEIPT_SUMMARY_REPORTER ="dataentry.reporter";
		public static final String RECEIPT_SUMMARY_LITERATURE = "dataentry.literature";
		
		public static final String CLASSIFICATION	="Classification";
		public static final String FOLLOW_UP_AER	="Follow Up Aer No";
		public static final String VERSION_TYPE	="Version Type";
	}
	
	public class SceptreFieldData
	{
		public static final String LOCAL_CASE_ID="localcaseid";
		public static final String COUNTRY_WHERE_OCCUR="country_occur";
		public static final String PAT_DOB="pat_dob";
		public static final String PAT_GENDER="pat_gender";
		public static final String PAT_INITIALS="pat_initials";
		public static final String PAT_AGE_GROUP="pat_age_group";
		public static final String REPORTER_GIVEN_NAME="reporter_first";
		public static final String REPORTER_FAMILY_NAME="reporter_last";
		public static final String REPORTER_COUNTRY="reporter_country";
		public static final String REPORTER_QUALIFICATION="reporter_qualification";
		
		public static final String REACTION_RANK="rank";
		public static final String REACTION_REPORTED_REACTION="reported_reaction";
		public static final String REACTION_SERIOUSNESS="seriousness";
		public static final String REACTION_OUTCOME="outcome";
	}
	
	public class FieldValue
	{
		public static final String PM="PM";
		public static final String CT="CT";
		public static final String PRODUCT_REGULARITY_CLASS = "OTC";
		public static final String Sceptre_Action = "Create New SCEPTRE version/Follow Up";
	}
	
	public enum ReceiptState
	{
		FOUND, NOT_FOUND, LOCKED_BY_ANOTHER_USER ;
	}
	
	public enum DocumentValidation
	{
		VALID, NO_DOCUMENT,INVALID;
	}
	
	

	public static Map<String, String> initiateFieldNameMap()
	{
		Map<String, String> fieldnameMap = new HashMap<String, String>(); 
		
		//Classification Tab
		fieldnameMap.put(ResultSummaryUI.CLASSIFICATION, ResultSummaryUI.CLASSIFICATION);
		fieldnameMap.put(ResultSummaryUI.FOLLOW_UP_AER,ResultSummaryUI.FOLLOW_UP_AER);
		fieldnameMap.put(ResultSummaryUI.VERSION_TYPE, ResultSummaryUI.VERSION_TYPE);
		
		//Case Data Tab
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_REPORT_TYPE , "Report Type");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_REPORT_SERIOUSNESS , "Reporter Seriousness");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_DEATH , "Death? ");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_LIFE_THREATENING , "Life threatening?");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_REGULARITY_RELEVENT , "Regulatory Relevant");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_PRIORITY , "Priority");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_CLASSIFICATION_COMMENTS , "Classification Comment");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_COMMENTS , "Comments");
		fieldnameMap.put(CaseData.SERIOUS_ASSESSMENT_SCEPTER_AER , "SCEPTRE AER ");
	
		//Receipt Summary Tab
		fieldnameMap.put(DataEntry.GENINFO_REPORT_TYPE , "Report Type");
		fieldnameMap.put(DataEntry.GENINFO_LOC_CASE_ID , "Local Case Id");
		fieldnameMap.put(DataEntry.GENINFO_INIT_REC_DATE , "Initial Receive Date");
		fieldnameMap.put(DataEntry.GENINFO_LAST_REC_DATE , "Latest Receive Date");
		fieldnameMap.put(DataEntry.GENINFO_COUNTRY_OF_OCCUR , "Country of Occurrence");

		fieldnameMap.put(DataEntry.SERUOUSNESS_CRIT_DEATH , "Death?");
		fieldnameMap.put(DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT , "Congenital anomaly/Birth defect?");
		fieldnameMap.put(DataEntry.SERUOUSNESS_CRIT_DISABILITY , "Disability or permanent damage?");
		fieldnameMap.put(DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION , "Other Medically Important Condition");
		fieldnameMap.put(DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING , "Life threatening?");
		fieldnameMap.put(DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED , "Hospitalization prolonged? ");

		fieldnameMap.put(DataEntry.ADD_REF_COMP_UNIT , "Company Unit");
		fieldnameMap.put(DataEntry.ADD_REF_OTHER_INDT_REF , "Other Identification Reference");
		fieldnameMap.put(DataEntry.ADD_REF_OTHER_INDT_NO , "Other Identification Number");

		fieldnameMap.put(DataEntry.SUSP_DRUG_PRODUCT_FLAG , "Product flag");
		fieldnameMap.put(DataEntry.SUSP_DRUG_PREF_PROD_DESC , "Preferred Product Description"); 
		fieldnameMap.put(DataEntry.SUSP_DRUG_PROD_DESC , "Product description (Generic Name)");
		fieldnameMap.put(DataEntry.SUSP_DRUG_FORMULATION , "Formulation");
		fieldnameMap.put(DataEntry.SUSP_DRUG_ACTION_WITH_DRUG , "Action(s) taken with Drug");

		fieldnameMap.put(DataEntry.DRUG_THERAPY_ROUT_ADMIN , "Route of Admin");

		fieldnameMap.put(DataEntry.ADVERSE_EVENT_REPORTED_ITEM , "Reported term");
		fieldnameMap.put(DataEntry.ADVERSE_EVENT_SERIOUS , "Serious");
		fieldnameMap.put(DataEntry.ADVERSE_EVENT_OUTCOME , "Outcome");

		fieldnameMap.put(DataEntry.PATIENT_PATIENT_ID , "Patient ID ");
		fieldnameMap.put(DataEntry.PATIENT_PATIENT_UNKNOWN , "Unknown");
		fieldnameMap.put(DataEntry.PATIENT_SEX , "Patient Sex");
		fieldnameMap.put(DataEntry.PATIENT_AGE_GROUP , "Patient Age group");
		fieldnameMap.put(DataEntry.PATIENT_DOB , "Patient Date of birth");
		fieldnameMap.put(DataEntry.PATIENT_AGE_EVENT_TEXT , "Patient Age at the time of event");
		fieldnameMap.put(DataEntry.PATIENT_AGE_EVENT_UNIT , "Patient Age Unit ");

		fieldnameMap.put(DataEntry.PROTOCOL_PROTOCOL_NO , "Protocol no. (Study no.)");
		fieldnameMap.put(DataEntry.PROTOCOL_STUDY_PATIENT_NO , "Protocol Study Patient Number ");
		fieldnameMap.put(DataEntry.PROTOCOL_SITE_NO , "Protocol Site No");

		fieldnameMap.put(DataEntry.REPORTER_QUALIFICATION , "Reporter Qualification");
		fieldnameMap.put(DataEntry.REPORTER_DO_NOT_REPORT_TIME , "Reporter Do Not Report Name");
		fieldnameMap.put(DataEntry.REPORTER_FIRST , "Reporter First Name");
		fieldnameMap.put(DataEntry.REPORTER_LAST , "Reporter Last Name");
		fieldnameMap.put(DataEntry.REPORTER_CITY , "Reporter City");
		fieldnameMap.put(DataEntry.REPORTER_STATE , "Reporter State");
		fieldnameMap.put(DataEntry.REPORTER_COUNTRY , "Reporter Country");
		fieldnameMap.put(DataEntry.REPORTER_ZIPCODE , "Reporter Zip code");
		
		fieldnameMap.put(DataEntry.LITERATURE_ART_TITLE , " Literature Article title ");
		fieldnameMap.put(DataEntry.LITERATURE_JOURNAL_TITLE , "Literature Journal title");
	
		
		return fieldnameMap;

	}



	public static final String SERIOUS = "Serious";
	public static final String NOT_SERIOUS = "Not Serious";
}
