package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;

public class ReporterSeriousness implements IFieldBusinessLogic {
	
	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {

		if (p_dataentrymap.get(IRTConstants.DataEntry.ADVERSE_EVENT_SERIOUS)
				.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.NO)) {
			p_dataentrymap.put(p_writerkey, IRTConstants.NOT_SERIOUS);
		}
		else {
			p_dataentrymap.put(p_writerkey, IRTConstants.SERIOUS);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		p_dataentrymap.put(p_writerkey, IRTConstants.NOT_SERIOUS);
		String l_serious = p_dataentrymap.get(IRTConstants.DataEntry.ADVERSE_EVENT_SERIOUS);
		
		for(String serious :l_serious.split(StringConstants.TILDE))
		{
			if(serious.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES))
			{
				p_dataentrymap.put(p_writerkey, IRTConstants.SERIOUS);
				break;
			}
		}
		
	}

}
