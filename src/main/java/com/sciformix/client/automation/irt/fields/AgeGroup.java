package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciEnums;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class AgeGroup implements IFieldBusinessLogic {
	
	private static final Logger log = LoggerFactory.getLogger(AgeGroup.class);
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String e2b_ageGroup = null;
		int e2b_onsetAge = -1;
		try{
			e2b_ageGroup = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			if(!StringUtils.isNullOrEmpty(e2b_ageGroup)){
				p_dataentrymap.put(p_writerkey, e2b_ageGroup);
			} else {
				if(!StringUtils.isNullOrEmpty(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.PatientTabKeys.PATIENT_ONSET_AGE).trim())) {
					
					e2b_onsetAge = Integer.parseInt((oPdtWrapper.getKeyValueAsString(CpatToolCConstants.PatientTabKeys.PATIENT_ONSET_AGE)).trim());
					if (e2b_onsetAge < 2) {
						p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.INFANT.displayName());
					} else if(e2b_onsetAge >= 2 && e2b_onsetAge <= 13) {
						p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.CHILD.displayName());
					} else if(e2b_onsetAge >= 14 && e2b_onsetAge <= 18) {
						p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.ADOLESCENT.displayName());
					} else if(e2b_onsetAge >= 19 && e2b_onsetAge <= 65) {
						p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.ADULT.displayName());
					} else if(e2b_onsetAge >= 66) {
						p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.ELDERLY.displayName());
					}
				} else {
					p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.UNKNOWN.displayName());
				}
			}
		} catch(Exception e) {
			log.error("FieldName : AgeGroup");
			log.error("Error : " , e);
			//To support data entry even after the tool has encountered an exception as the value is not known
			p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.UNKNOWN.displayName());
			//throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.AGE_GROUP);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String e2b_ageGroup = null;
		String l_value = null;
		try{
			e2b_ageGroup = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			if(!StringUtils.isNullOrEmpty(e2b_ageGroup)){
				l_value = e2b_ageGroup;
			} else {
				if(CpatToolCHelper.sceptreFieldsMap != null && CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.PAT_AGE_GROUP) != null) {
					l_value = CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.PAT_AGE_GROUP);
				} else {
					l_value	= SciEnums.AgeGroup.UNKNOWN.displayName();	
				}
			}
			
			if (StringUtils.isNullOrEmpty(l_value)) {
				p_dataentrymap.put(p_writerkey, SciEnums.AgeGroup.UNKNOWN.displayName());
			} else {
				p_dataentrymap.put(p_writerkey, l_value);
			}
		} catch(Exception e) {
			log.error("FieldName : AgeGroup");
			log.error("Error : ",e);
			throw new RuntimeException();
		}
		
	}

}
