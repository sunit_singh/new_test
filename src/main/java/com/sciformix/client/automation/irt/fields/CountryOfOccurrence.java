package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class CountryOfOccurrence implements IFieldBusinessLogic {

	private static final Logger log = LoggerFactory.getLogger(CountryOfOccurrence.class);
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		String occurCountry = null;
		String eventCountryOccured = null;
		try {
			occurCountry = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			
			if (StringUtils.isNullOrEmpty((occurCountry))) {
				eventCountryOccured = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.EVENT_COUNTRY_OCCURED);
				if(eventCountryOccured.equalsIgnoreCase("None") || eventCountryOccured.equals("")){
					eventCountryOccured = "";
				}
				p_dataentrymap.put(p_writerkey, eventCountryOccured);
			} else {
				p_dataentrymap.put(p_writerkey, occurCountry);
			}
		} catch (Exception e) {
			log.error("FieldName : CountryOfOccurrence");
			log.error("Error : ",e);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.COUNTRY_OF_OCCURRENCE);
		}

	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String occurCountry = null;
		String eventCountryOccured = null;
		try {
			occurCountry = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			
			if (StringUtils.isNullOrEmpty((occurCountry))) {
				eventCountryOccured = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.EVENT_COUNTRY_OCCURED);
				if(!eventCountryOccured.equalsIgnoreCase("None") && !eventCountryOccured.equals("")){
					p_dataentrymap.put(p_writerkey, eventCountryOccured);
				} else {
					p_dataentrymap.put(p_writerkey, CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.COUNTRY_WHERE_OCCUR));
				}
				
			} else {
				p_dataentrymap.put(p_writerkey, occurCountry);
			}
		} catch (Exception e) {
			log.error("FieldName : CountryOfOccurrence");
			log.error("Error : ",e);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.COUNTRY_OF_OCCURRENCE);
		}

	}

}
