package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class RouteofAdmin implements IFieldBusinessLogic  {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION))
		{
			if(oPdtWrapper.isKeyValueNullOrEmpty(p_sourcedocumentkey))
			{
				p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.UNKNOWN);
			}
			else
			{
				p_dataentrymap.put(p_writerkey, oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey));
			}	
		}
		else
		{
			p_dataentrymap.put(p_writerkey,oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION)); 
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
}
