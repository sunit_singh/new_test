package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.automation.irt.IRTWebApplicationWriter;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class OtherIdentificationNumber implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;
	
	private static final Logger log = LoggerFactory.getLogger(OtherIdentificationNumber.class);
	
	private static final String OPERATING_COMPANY = "Operating Company";
	
	private static final String PRODUCT_COMPLAINT_NUMBER = "Product Complaint Number";
	
	private static final String PQMS = "PQMS";
	
	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_ReceiptSummarymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String safetyCaseId = null;
		String e2b_duplicateNumber = null;
		String e2b_gccPlatformCaseNo = null;
		String e2b_linkedReportNo = null;
		String e2b_otherCaseIdentifiers = null;
		String e2b_narrativePqmsExtSourceNo = null;
		
		String companyUnit = "";
		String otherIdentificationRef = "";
		String otherIdentificationNumber = "";
		
		int count = -1;
		
		try{
			
			safetyCaseId = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.ReportSummaryTabKeys.GENINFO_LOC_CASE_ID);
			if(!StringUtils.isNullOrEmpty(safetyCaseId)){
				companyUnit = getSenderDetails();
				otherIdentificationRef = OPERATING_COMPANY;
				otherIdentificationNumber = safetyCaseId;
			}
			
			e2b_duplicateNumber = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.ReportSummaryTabKeys.DUPLICATE_NUMBER);
			if(!StringUtils.isNullOrEmpty(e2b_duplicateNumber)){
				companyUnit = companyUnit + SciConstants.StringConstants.TILDE + getSenderDetails();
				otherIdentificationRef = otherIdentificationRef + SciConstants.StringConstants.TILDE + OPERATING_COMPANY;
				otherIdentificationNumber = otherIdentificationNumber + SciConstants.StringConstants.TILDE + e2b_duplicateNumber;
			}
			
			e2b_gccPlatformCaseNo = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.GCC_PLATFORM_CASE_NUMBER);
			if(!StringUtils.isNullOrEmpty(e2b_gccPlatformCaseNo) && !e2b_gccPlatformCaseNo.equals(safetyCaseId)){
				companyUnit = companyUnit + SciConstants.StringConstants.TILDE + getSenderDetails();
				otherIdentificationRef = otherIdentificationRef + SciConstants.StringConstants.TILDE + OPERATING_COMPANY;
				otherIdentificationNumber = otherIdentificationNumber + SciConstants.StringConstants.TILDE + e2b_gccPlatformCaseNo;
			}
			
			e2b_linkedReportNo = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.LINKED_REPORT_NUMBER);
			if(!StringUtils.isNullOrEmpty(e2b_linkedReportNo)){
				companyUnit = companyUnit + SciConstants.StringConstants.TILDE + getSenderDetails();
				otherIdentificationRef = otherIdentificationRef + SciConstants.StringConstants.TILDE + OPERATING_COMPANY;
				otherIdentificationNumber = otherIdentificationNumber + SciConstants.StringConstants.TILDE + e2b_linkedReportNo;
			}
			
			e2b_otherCaseIdentifiers = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.ReportSummaryTabKeys.ADD_REF_OTHER_INDT_NO);
			if(!StringUtils.isNullOrEmpty(e2b_otherCaseIdentifiers)){
				companyUnit = companyUnit + SciConstants.StringConstants.TILDE + getSenderDetails();
				otherIdentificationRef = otherIdentificationRef + SciConstants.StringConstants.TILDE + OPERATING_COMPANY;
				otherIdentificationNumber = otherIdentificationNumber + SciConstants.StringConstants.TILDE + e2b_otherCaseIdentifiers;
			}
			
			if(p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_COUNT)){
				count = Integer.parseInt(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_COUNT).trim());
				for(int temp = 1; temp <= count; temp++){
					if(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE + "_" + temp).trim().equalsIgnoreCase(PQMS)){
						e2b_narrativePqmsExtSourceNo = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_NUMBER + "_" + temp);
						if(!StringUtils.isNullOrEmpty(e2b_narrativePqmsExtSourceNo)){
							companyUnit = companyUnit + SciConstants.StringConstants.TILDE + PQMS;
							otherIdentificationRef = otherIdentificationRef + SciConstants.StringConstants.TILDE + PRODUCT_COMPLAINT_NUMBER;
							otherIdentificationNumber = otherIdentificationNumber + SciConstants.StringConstants.TILDE + e2b_narrativePqmsExtSourceNo;
						}
					}
				}
			} else if(p_sourcedocumentdata.isColumnPresent(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE)){
				if(oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE).trim().equalsIgnoreCase(PQMS)){
					e2b_narrativePqmsExtSourceNo = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_NUMBER);
					if(!StringUtils.isNullOrEmpty(e2b_narrativePqmsExtSourceNo)){
						companyUnit = companyUnit + SciConstants.StringConstants.TILDE + PQMS;
						otherIdentificationRef = otherIdentificationRef + SciConstants.StringConstants.TILDE + PRODUCT_COMPLAINT_NUMBER;
						otherIdentificationNumber = otherIdentificationNumber + SciConstants.StringConstants.TILDE + e2b_narrativePqmsExtSourceNo;
					}
				}
			}
			
			if(companyUnit.startsWith("~")) {
				companyUnit = companyUnit.replaceFirst("~", "");
			}
			
			if(otherIdentificationRef.startsWith("~")){
				otherIdentificationRef = otherIdentificationRef.replaceFirst("~", "");
			}
			
			if(otherIdentificationNumber.startsWith("~")){
				otherIdentificationNumber = otherIdentificationNumber.replaceFirst("~", "");
			}
			
			p_ReceiptSummarymap.put(IRTConstants.DataEntry.ADD_REF_COMP_UNIT, companyUnit);
			p_ReceiptSummarymap.put(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF, otherIdentificationRef);
			p_ReceiptSummarymap.put(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO, otherIdentificationNumber);
			
		} catch(Exception e) {
			log.error("FieldName : OtherIdentificationNumber");
			log.error("Error : " , e);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.OTHER_IDENTIFICATION_NUMBER);
		}
		
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_ReceiptSummarymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_ReceiptSummarymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
	
	private String getSenderDetails(){
		return StringUtils.emptyString(IRTWebApplicationWriter.receiptDataMap.get(IRTConstants.DataEntry.EDITRECEIPT_SENDER));
	}
	
}
