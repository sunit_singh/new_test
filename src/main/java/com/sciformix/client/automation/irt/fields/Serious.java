package com.sciformix.client.automation.irt.fields;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class Serious implements IFieldBusinessLogic {
	ParsedDataTableWrapper oPdtWrapper = null;
	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap,ParsedDataTable
			p_sourcedocumentdata,String p_writerkey, String p_sourcedocumentkey) {
		if (checkSeriousnessCriteria(p_dataentrymap, p_sourcedocumentdata)) {
			p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.YES);
		} else {
			p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.NO);
		}
	}
	
	
	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		String l_sceptreeventserious = CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.REACTION_SERIOUSNESS);
		Set<String>	l_seriousset = new HashSet<String>();
		for (String l_serious : l_sceptreeventserious.split(StringConstants.TILDE)) {
			l_seriousset.add(l_serious);
		}
		if (l_seriousset.size() == 1 && l_seriousset.iterator().next().equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.NO)
				&& checkSeriousnessCriteria(p_dataentrymap, p_sourcedocumentdata)) {
			p_dataentrymap.put(p_writerkey, l_sceptreeventserious);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.SERIOUS);
		}
		p_dataentrymap.put(p_writerkey, l_sceptreeventserious);
	}
	
	private boolean checkSeriousnessCriteria (Map<String, String> p_dataentrymap, ParsedDataTable
			p_sourcedocumentdata)
	{
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		boolean	l_isserious	=	false;
		if (p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH).equals(CpatToolConstants.FieldsLogicValues.YES) ||
				p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED).equals(CpatToolConstants.FieldsLogicValues.YES) ||
				p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING).equals(CpatToolConstants.FieldsLogicValues.YES) ||
				p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY).equals(CpatToolConstants.FieldsLogicValues.YES) ||
				p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT).equals(CpatToolConstants.FieldsLogicValues.YES) ||
				p_dataentrymap.get(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION).equals(CpatToolConstants.FieldsLogicValues.YES)
		){
			l_isserious = true;
		} else {
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.
					ReportSummaryTabKeys.ADVERSE_EVENT_SERIOUS))	
			{
				if(oPdtWrapper.isKeyValueNullEmptyOrMatches(CpatToolCConstants.
						NarrativeKeys.NARRATIVE_ADMITTED_TO_HOSPITAL_RELATED_TO_EVENT, CpatToolConstants.FieldsLogicValues.NONE))	
				{
					if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.
						NarrativeKeys.NARRATIVE_DATE_OF_HOSP_ADMISSION) && 
							oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.
							NarrativeKeys.NARRATIVE_DATE_OF_DISCHARGE))
					{
						l_isserious	= false;
					} else {
						l_isserious	= true;
					}
				} else if (oPdtWrapper.doesKeyValueMatch(CpatToolCConstants.
						NarrativeKeys.NARRATIVE_ADMITTED_TO_HOSPITAL_RELATED_TO_EVENT, CpatToolConstants.FieldsLogicValues.YES)) {
					l_isserious	= true;
				}
			}
			if (!l_isserious && CpatToolCHelper.isCaseSerious(p_sourcedocumentdata, 
					CpatToolCHelper.propMap.get(IRTConstants.SERIOUS_KEYWORDS) , StringConstants.COMMA)) {
				l_isserious = true;
			}
		}
		return l_isserious;
	}
}
