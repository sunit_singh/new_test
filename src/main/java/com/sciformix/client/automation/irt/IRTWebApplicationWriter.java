package com.sciformix.client.automation.irt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.BaseWebApplicationWriter;
import com.sciformix.client.automation.WebApplicationWriterUtils;
import com.sciformix.client.automation.irt.FieldDetails.XPathType;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.DuplicateFields;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;

public class IRTWebApplicationWriter extends BaseWebApplicationWriter {
	
private static final Logger log = LoggerFactory.getLogger(IRTWebApplicationWriter.class);
	
	private static String s_sDownloadFolder = null;
	private static String parentWindow = null;
	private static String userDownloadPath= null;
	public static Map<String, String> receiptDataMap = null;
	
	private static String reporterGivenName  = null;
	private static String reporterFamilyName =null;
	private static String patientDOB =null;
	private static String patientInitials =null;
	private static String patientGender =null;
	private static String country =null;
	private static String irtDuplicateSearchWindow = null;
	private static String irtWindow  = null;
	private static String sceptreWindow  = null;
	private static String duplicateWindow  = null;
	private static Map<String, FieldDetails>	l_writere2bfieldmapping;
	public static Map<String, String>	fieldNameMap = null;
	
	public static List<String> editXpathVaList = null;
	private static String downloadedFileName = "E2bPdfReportAgx.pdf";
	public static final String CPAT_CONFIG	= "cpat.properties";
	
	//xpth validations constants
	private static final String _READ_LABLE_FIELD_NAME_PREFIX = ".label";
	private static final String _MATCH_STATUS_NOT_FOUND = "Not Available";
	private static final String _MATCH_STATUS_TRUE = "True";
	private static final String _MATCH_STATUS_FALSE = "False";
	
	private static final String _EDIT_RECEIPT_LABLE_CLASS_FIELD = "labelWid";
	private static final String _EDIT_RECEIPT_CONTROL_CLASS_FIELD = "colWid";
	
	private static final String _DATA_ENTRY_LABLE_CLASS_FIELD = "Label" ;
	private static final String _DATA_ENTRY_CONTROL_CLASS_FIELD = "Ctrl";
	
	private static final String _MAIN_MENU = "Main Menu";
	private static final String DOCUMENT_TAB = "Document";
	private static final String _INBOUND_SEARCH = "Inbound Search";
	private static final String _TABNAME_RECEIPT_SUMMARY = "Receipt Summary";
	private static final String _GROUP_GEN_INFO = "General Information";
	private static final String _GROUP_EDIT_RECEIPT = "Edit Receipt";
	private static final String _GROUP_SERIOUSNESS_CRITERIA = "Seriousness Criteria";
	private static final String _GROUP_ADDITIONAL_REFERENCE = "Additional Reference";
	private static final String _GROUP_SUSPECT_DRUG = "Suspect Drug";
	private static final String _GROUP_DRUG_THERAPY = "Drug Therapy";
	private static final String _GROUP_ADVERSE_EVENT = "Adverse Event";
	private static final String _GROUP_PATIENT = "Patient";
	private static final String _GROUP_PROTOCOL = "Protocol";
	private static final String _GROUP_REPORTER = "Reporter";
	private static final String _GROUP_LITERATURE = "Literature";
	private static final String _GROUP_SERIOUS_ASSESMENT = "Seriousness Assesment";
	
	private static final String _TABNAME_CASE_DATA = "Case Data";
	
	private static final String DOWNLOADFOLDER_SUFFIX = "\\AppData\\Local\\Temp";

	private static final String DOWNLOAD_SUBFOLDER = "\\SCFXDownloadTempFolder";
	
	
	
	static
    {
           //create folder in temp folder to save download files
           if(WebApplicationWriterUtils.createDirectories(System.getenv("USERPROFILE") + DOWNLOADFOLDER_SUFFIX + DOWNLOAD_SUBFOLDER))
           {
                  
                  s_sDownloadFolder = System.getenv("USERPROFILE") + DOWNLOADFOLDER_SUFFIX + DOWNLOAD_SUBFOLDER;
           }
           else
           {
                  s_sDownloadFolder = System.getenv("USERPROFILE") + DOWNLOADFOLDER_SUFFIX ;
                  
           }
          /* s_sDownloadFolder = System.getenv("USERPROFILE") + "\\AppData\\Local\\Temp";
         
          */ //AR//TODO:Handle this for Windows with no env variable AND for n */on-Windows machines
          
           userDownloadPath = System.getenv("USERPROFILE")+"\\Downloads\\";
           fieldNameMap = IRTConstants.initiateFieldNameMap();
           setCPATProperties(CPAT_CONFIG);
    }

	
	public IRTWebApplicationWriter(BROWSER p_eBrowser , Map<String, String> propMap)
	{
		super(p_eBrowser, propMap.get("webdriver.browserdriver"), WebApplicationWriterUtils.getBrowserLocation(propMap.get
				("webdriver.driverlocation")),true , s_sDownloadFolder);
	}
	
	public boolean login(String p_sUserid, Map<String, String> propMap)
	{
		String	sUsernameXpath = null,
			   sContinuLoginXpath = null;
		boolean passwordInputFlag =false;
		int waitCount = 0;
		
		sUsernameXpath = propMap.get(IRTConstants.Login.USERNAME_XPATH);
		sContinuLoginXpath = propMap.get(IRTConstants.Login.LOGIN_CONTINUE_XPATH);
		
		sceptreWindow = getWindowHandle();
		switchWindowHandler(sceptreWindow);
		irtWindow = getWindowHandle();
		
		maximizeWindow();
		findElementByXpathAndSetContent(IRTConstants.XPATH_BY_NAME, sUsernameXpath, p_sUserid);
		
		while(!passwordInputFlag && waitCount <60)
		{
			wait(IRTConstants.Wait.PASS_WAIT, propMap);
		
		if(isElementVisibleByName(sContinuLoginXpath, IRTConstants.INPUT_TYPE_INPUT))
		{
			clickElementByXpath(IRTConstants.INPUT_TYPE_INPUT, IRTConstants.XPATH_BY_NAME, sContinuLoginXpath);
				passwordInputFlag = true;
			}
			else if(isElementVisibleById(propMap.get(IRTConstants.SearchCase.INBOUND_ID),IRTConstants.INPUT_TYPE_ATTRIBUTE))
			{
				passwordInputFlag = true;
				log.info("Login successful");
			}
			waitCount++;
		}
		return passwordInputFlag;
	}
	
	public IRTConstants.ReceiptState searchCase(String caseId ,  Map<String, String> propMap)
	{
		String inboundXpath = null,
			   inboundListing = null,
			   searchTextBox = null,
			   searchXpath = null;
		int trCount = 2;
		IRTConstants.ReceiptState receiptState = null;
		
		inboundXpath = propMap.get(IRTConstants.SearchCase.INBOUND_XPATH);
		inboundListing = propMap.get(IRTConstants.SearchCase.INBOUND_INBOUND_LISTING);
		searchTextBox = propMap.get(IRTConstants.SearchCase.INBOUND_INBOUND_LISTING_SEARCH);
		searchXpath = propMap.get(IRTConstants.SearchCase.INBOUND_INBOUND_LISTING_SEARCH_BUTTION);
		
		clickElementByAbsolutePath(inboundXpath);
		
		clickElementByAbsolutePath(inboundListing);
	
		setLableAsSelectByXpath(propMap.get(IRTConstants.CaseProcess.EDIT_SELECT_REVIEW), 
				propMap.get(IRTConstants.CaseProcess.EDIT_SELECT_REVIEW_INPUT),
				propMap.get(IRTConstants.CaseProcess.EDIT_SELECT_REVIEW_SELECT),
				IRTConstants.INITIAL_REVIEW);
		
		findElementByAbsoluteXpathAndSetContentOverride(searchTextBox, caseId);
		
		clickElementByAbsolutePath(searchXpath);
		
		while(trCount>1)
		{
			wait(IRTConstants.Wait.MIN_WAIT,propMap);
			trCount = getAllRowSizeByTableBodyXpath(propMap.get(IRTConstants.CaseProcess.EDIT_CASE_TABLE_BODY));
		}
		
		if(isTableEmpty(propMap.get(IRTConstants.CaseProcess.EDIT_CASE_TABLE_BODY)))
		{
			receiptState = IRTConstants.ReceiptState.NOT_FOUND;
		}
		else if(isElementVisibleById(propMap.get(IRTConstants.CaseProcess.EDIT_CASE_LOCK), IRTConstants.INPUT_TYPE_IMAGE))
		{
			if(unlockCase(caseId, propMap))
			{
				receiptState = IRTConstants.ReceiptState.FOUND;
				searchCase(caseId, propMap);
			}
			else
			{
				receiptState = IRTConstants.ReceiptState.LOCKED_BY_ANOTHER_USER;
			}
		}else
		{
			receiptState = IRTConstants.ReceiptState.FOUND;
		}
		return receiptState;
	}
	
	public boolean editCasePdf(Map<String, String> propMap)
	{
		String editImgId = null;
		String confirmPasswordField = null;
		String searchWindow = null;
		int waitCount = 0;
		boolean passwordInputFlag = true;
		searchWindow = getWindowHandle();
		
		editImgId = propMap.get(IRTConstants.CaseProcess.EDIT_IMAGE_ID);
		confirmPasswordField =  propMap.get(IRTConstants.CaseProcess.EDIT_CONFIRM_PASSWORD);
	
		clickElementByXpath(IRTConstants.INPUT_TYPE_IMAGE, IRTConstants.XPATH_BY_ID, editImgId);
		
		wait(IRTConstants.Wait.MIN_WAIT,propMap);
		
		if(isElementVisibleByName(confirmPasswordField,IRTConstants.INPUT_TYPE_INPUT))
		{
			passwordInputFlag = false;
			while(!passwordInputFlag && waitCount<12)
			{
				wait(IRTConstants.Wait.PASS_WAIT, propMap);
				if(getWindowHandles().size()<3)
			   {
					passwordInputFlag = true;
			   }
				waitCount++; 
			}
			switchWindow(searchWindow);
		}
		return passwordInputFlag;
	}
	
	public IRTConstants.DocumentValidation downloadCasePdf( Map<String, String> propMap, List<IRTCaseDocument>  caseDocList)
	{
		String documentTabXpath = null;
		String childWindow = null;
		String fileName = null;
		IRTConstants.DocumentValidation documentValidation = null;
		IRTCaseDocument IRTCaseDoc= null; 
		int docCount = 0;
		
		childWindow = getWindowHandle();
		documentTabXpath = propMap.get(IRTConstants.CaseDocumentDownload.DOCUMENT_MENU);
		
		clickElementByAbsolutePath(documentTabXpath);
		
		docCount = getDescendantsTrSizeByTableID(IRTConstants.TABLE_BODY, 
				IRTConstants.XPATH_BY_ID, propMap.get(IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_TABLE_BODY_ID));
		
		
		if(docCount>0)
		{
			documentValidation = IRTConstants.DocumentValidation.VALID;
				
				IRTCaseDoc = new IRTCaseDocument();
				fileName = getTextElementByXpath(IRTConstants.INPUT_TYPE_ATTRIBUTE, IRTConstants.XPATH_BY_ID, 
						propMap.get(IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_DOC_PREFIX)+0+propMap.get(IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_DOC_SUFFIX));
	
				if(!fileName.endsWith(".xml"))
				{
					documentValidation = IRTConstants.DocumentValidation.INVALID;
				}
				IRTCaseDoc.setCaseFilename(fileName);
				
				clickElementByXpath(IRTConstants.INPUT_TYPE_ATTRIBUTE, IRTConstants.XPATH_BY_ID, 
						propMap.get(IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_DOC_PREFIX)+0+propMap.get(IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_DOC_SUFFIX));
				
				//wait("minWait");
				
				switchToNewWindow(getSetOfWindows(3), parentWindow, childWindow);
				wait(IRTConstants.Wait.MIN_WAIT, propMap);
				selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_DOC_SELECT_FORMAT),IRTConstants.DOC_FORMAT);
			
				setDowloadedFileInfo(IRTCaseDoc, propMap);//set downloaded file
				
				
				clickElementByAbsolutePath(propMap.get(IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_DOC_CLOSE));
				switchWindow(childWindow);
				
				caseDocList.add(IRTCaseDoc);
		}
		else
		{
			documentValidation = IRTConstants.DocumentValidation.NO_DOCUMENT;
		}
		return documentValidation;
	}
	
	public boolean unlockCase(String caseId,  Map<String, String> propMap)
	{
		boolean unlockStatus = false;
		
			clickElementByAbsolutePath(propMap.get(IRTConstants.UnlockCase.ADMINISTRATION));
			clickElementByAbsolutePath(propMap.get(IRTConstants.UnlockCase.ADMINISTRATION_UNLOCK));
			
			unlockStatus = unlockCase(propMap.get(IRTConstants.UnlockCase.ADMINISTRATION_UNLOCK_CASE_TABLE), caseId);
			
			clickElementByAbsolutePath(propMap.get(IRTConstants.UnlockCase.ADMINISTRATION_UNLOCK_BUTTON));
			
		return unlockStatus;
	}
	private void setDowloadedFileInfo(IRTCaseDocument p_oCaseDocument,Map<String, String> propMap )
	{
		log.info("Set downloaded file info");
		
		File oFile = null , existFile = null, oDownloadedFile = null;
		
		oFile = new File(s_sDownloadFolder);
		existFile = new File(s_sDownloadFolder+"\\"+downloadedFileName);
		
		while(!existFile.exists())//wait while file download
		{
			wait(IRTConstants.Wait.MIN_WAIT, propMap);
		}	
		oDownloadedFile = getDownloadedPdfFilePath(oFile);

		log.info("Downloaded file path is{}"+oDownloadedFile.getAbsolutePath());
		
		p_oCaseDocument.setCaseFile(oDownloadedFile);
		
		log.info("Set downloaded file is" + oDownloadedFile.getAbsolutePath());
		
	}
	
	public boolean caseClassification(String caseType,  Map<String, String> propMap, ParsedDataTable dataTable, String aer, String versionType)
	{
		boolean dataFeedingFlag = false;
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		
		waitUntillInvibilityOfElementById(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_PROCCESSING_WINDOW));
		if(isElementVisibleById(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_PROCCED_ID), IRTConstants.BUTTON_TAG))
		{	
			clickElementByAbsolutePath(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_PROCCED));
			
			if(caseType.equals(CpatToolCConstants.CaseType.INITIAL))
			{
				selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.AssignCase.ASSIGN_CASE_SELECT_CLASSIFICATION), IRTConstants.CASECLASSIFICATION.NEW.displayName());
				aer = StringConstants.EMPTY;
			}else if(caseType.equals(CpatToolCConstants.CaseType.FOLLOWUP))
			{
				selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.AssignCase.ASSIGN_CASE_SELECT_CLASSIFICATION), IRTConstants.CASECLASSIFICATION.FOLLOWUP.displayName());
				selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.AssignCase.ASSIGN_CASE_VERSION_TYPE), versionType);
				findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.AssignCase.ASSIGN_CASE_AER),aer );
			}	
			clickElementByAbsolutePath(propMap.get(IRTConstants.AssignCase.ASSIGN_CASE_NEXT));
			dataFeedingFlag = true;
		}
		return dataFeedingFlag;
	}
	
	//Irt Fields Writer
	public Map<String, String>  feedReceiptSummaryMenuTab(ParsedDataTable dataTable,  Map<String, String> propMap, Map<String, String>dataEntryMap)
	{
		Map<String, String>  dataFeedMap = null;
		
		waitUntillInvibilityOfElementById(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_PROCCESSING_WINDOW));
		clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.RECEIPT_SUMMARY));
		
		//Feed Data
		
		dataFeedMap =  feedDataEntryForm(dataTable,propMap ,dataEntryMap);
		return dataFeedMap;
	}
	
	private Map<String, String>  feedDataEntryForm(ParsedDataTable dataTable,  Map<String, String> propMap, Map<String, String>dataEntryMap)
	{
		boolean addRefFeedFlag = false,
				suspectDrugFlag=false,
				adverseEventFlag = false;
				
		Map<String, String>  caseDataSummMap= new LinkedHashMap<String, String>();
		
		String fieldValue = null;
	
		IRTE2BFieldsWriterMapper	l_mapper	=	IRTE2BFieldsWriterMapper.getInstance(IRTWebApplicationWriter.class.getClassLoader().getResourceAsStream("IRTWriterE2BMapper.txt"));
		l_writere2bfieldmapping	=	l_mapper.getWriterE2BFieldMapping();
		
		log.info("Receipt Summary Feeding : ");
		for (String l_writerkey : l_writere2bfieldmapping.keySet()) 
		{
			FieldDetails	l_fielddetails	=	l_writere2bfieldmapping.get(l_writerkey);
			
			if(l_fielddetails != null && l_fielddetails.getTabName().equals(FieldDetails.TabName.RECEIPT)
					&& !l_writerkey.equals(IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO) )
			{
				log.info(l_writerkey);
				if(l_writerkey.equals(IRTConstants.DataEntry.GENINFO_REPORT_TYPE))
				{
					
					String protocalNo	=	StringUtils.emptyString(dataEntryMap.get(IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO));
					if(StringUtils.isNullOrEmpty(protocalNo))
					{
						fieldValue =  IRTConstants.FieldValue.PM;
						selectDropdownValueByValueXpathOverride(propMap.get(l_writerkey),fieldValue);
					}
					else
					{
						int trcount = 0;
						findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO),protocalNo);
						clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO_SEARCH));
						
						wait(IRTConstants.Wait.MIN_WAIT, propMap);
						
						switchToNewWindow(getSetOfWindows(4),sceptreWindow,irtWindow,StringUtils.emptyString(duplicateWindow));
						
						trcount = getAllTDSizeByTableBodyXpath(propMap.get(IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO_SEARCH_TBODY));
						
						clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO_SEARCH_CANCEL));
						
						if(trcount>1)
						{	
							switchWindow(irtWindow);
							fieldValue = IRTConstants.FieldValue.CT;
							selectDropdownValueByValueXpathOverride(propMap.get(l_writerkey),fieldValue);
						}
						else
						{
							switchWindow(irtWindow);
							fieldValue = IRTConstants.FieldValue.PM;
							selectDropdownValueByValueXpathOverride(propMap.get(l_writerkey),fieldValue);
						}
						
					}
				}
				else if((l_writerkey.equals(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO) ||
						l_writerkey.equals(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF) ||
						l_writerkey.equals(IRTConstants.DataEntry.ADD_REF_COMP_UNIT))
						)
				{
					if(!addRefFeedFlag) {
						String[] companyUnitArr = StringUtils.emptyString(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT)).split(SciConstants.StringConstants.TILDE);
						String[] idfRefArr =  dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).split(SciConstants.StringConstants.TILDE);
						String[] idfRefNoArr =  dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).split(SciConstants.StringConstants.TILDE);
						
						StringBuffer companyUnitValue = new StringBuffer();
						StringBuffer idfRefValue = new StringBuffer();
						StringBuffer idfRefNoValue = new StringBuffer();
						for(int count = 0; count<companyUnitArr.length; count++)
						{
							findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT),
									companyUnitArr[count]);
							companyUnitValue.append(companyUnitArr[count]);
							
							if(!StringUtils.isNullOrEmpty(idfRefArr[count]))
							{
								if(!selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF),
										idfRefArr[count]))
								{
									idfRefArr[count] = CpatToolConstants.ERROR_STRING +idfRefArr[count];
								}
								
								idfRefValue.append(idfRefArr[count]);
							}
							
							findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO),
									idfRefNoArr[count]);
							idfRefNoValue.append(idfRefNoArr[count]);
							
							if(count<companyUnitArr.length-1)
							{
								companyUnitValue.append(StringConstants.COMMA_WITH_SPACE);
								idfRefValue.append(StringConstants.COMMA_WITH_SPACE);
								idfRefNoValue.append(StringConstants.COMMA_WITH_SPACE);
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.ADD_REF_ADD_ENTRY));
								wait(IRTConstants.Wait.MIN_WAIT,propMap);
							}
						}

						dataEntryMap.put(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO,idfRefNoValue.toString());
						dataEntryMap.put(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF, idfRefValue.toString());
						dataEntryMap.put(IRTConstants.DataEntry.ADD_REF_COMP_UNIT,companyUnitValue.toString());
						addRefFeedFlag = true;
					}
					fieldValue =  StringUtils.emptyString(dataEntryMap.get(l_writerkey));
				}
				else if(l_writerkey.equals(IRTConstants.DataEntry.SUSP_DRUG_PRODUCT_FLAG)||
						l_writerkey.equals(IRTConstants.DataEntry.SUSP_DRUG_PREF_PROD_DESC)||
						l_writerkey.equals(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC) ||
						l_writerkey.equals(IRTConstants.DataEntry.SUSP_DRUG_FORMULATION)||
						l_writerkey.equals(IRTConstants.DataEntry.SUSP_DRUG_ACTION_WITH_DRUG))
				{
					String prodRegClassValue = null;
					if(!suspectDrugFlag) {
						String[] productFlag =  dataEntryMap.get(IRTConstants.DataEntry.SUSP_DRUG_PRODUCT_FLAG).split(SciConstants.StringConstants.TILDE);
						String[] prodDesc =  dataEntryMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC).split(SciConstants.StringConstants.TILDE);
						String[] formulation =  dataEntryMap.get(IRTConstants.DataEntry.SUSP_DRUG_FORMULATION).split(SciConstants.StringConstants.TILDE);
						String[] actionTaken =  dataEntryMap.get(IRTConstants.DataEntry.SUSP_DRUG_ACTION_WITH_DRUG).split(SciConstants.StringConstants.TILDE);
						
						StringBuffer productFlagValues = new StringBuffer();
						StringBuffer productDescValues = new StringBuffer();
						StringBuffer formulationValues = new StringBuffer();
						StringBuffer actionTakenValues = new StringBuffer();
					
						for(int count = 0; count<productFlag.length; count++)
						{
							if(!StringUtils.isNullOrEmpty(productFlag[count]))
							{
								if(!selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PRODUCT_FLAG),
										productFlag[count]))
								{
									productFlag[count] = CpatToolConstants.ERROR_STRING +productFlag[count];
								}
								
								productFlagValues.append(productFlag[count]);
							}
							
							//Product Description
							prodRegClassValue = prodDesc[count];
							if(prodRegClassValue.equals(IRTConstants.FieldValue.PRODUCT_REGULARITY_CLASS)) //Medicinal Product
							{
								int searchCount = 0;
								String prefValue = getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PREF_PROD_DESC));
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SERACH));
								wait(IRTConstants.Wait.MIN_WAIT,propMap);
								
								//switchToNewWindow(getSetOfWindows(3),sceptreWindow,irtWindow);
								
								switchToNewWindow(getSetOfWindows(4),sceptreWindow,irtWindow,StringUtils.emptyString(duplicateWindow));
								
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SEARCH_DRUG));
								
								while(searchCount < 2 && !StringUtils.isNullOrEmpty(fieldValue.trim()))
								{	
									int spacePos = prefValue.lastIndexOf(SciConstants.StringConstants.SPACE);
									
									findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SEARCH_PREF_PROD_DESC),
										SciConstants.StringConstants.PERCENT + prefValue + SciConstants.StringConstants.PERCENT);
									
									clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SEARCH_SEARCHBUTTON));
									
									wait(IRTConstants.Wait.MIN_WAIT,propMap);
									clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SEARCH_ACTIVE));
									searchCount = getAllTDSizeByTableBodyXpath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SEARCH_TBODY));
								
									if(!prefValue.contains(SciConstants.StringConstants.SPACE))
									{
										break;
									}
									if(spacePos>0)
									{
										prefValue = prefValue.substring(0, spacePos - 1);
									}
								}
								
								do{
									JOptionPane.showMessageDialog(null, "Please Select Generic Name and then click on OK");
									wait(IRTConstants.Wait.MIN_WAIT,propMap);
								}
								while(getWindowHandles().size()>3);
								
								switchWindow(irtWindow);
							}
							else //Non Medicinal Product
							{
								/*findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC),
										prodRegClassValue);*/
								productDescValues.append(prodRegClassValue);
								
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SERACH));
								wait(IRTConstants.Wait.MIN_WAIT,propMap);
								
								switchToNewWindow(getSetOfWindows(4),sceptreWindow,irtWindow,StringUtils.emptyString(duplicateWindow));
								
								findElementByAbsoluteXpathAndSetContentOverride(
									propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SEARCH_PREF_ACTIVE_INGREDIENT),
										 prodRegClassValue);
									
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SEARCH_SEARCHBUTTON));
								wait(IRTConstants.Wait.MIN_WAIT,propMap);
								
								do {
									JOptionPane.showMessageDialog(null, "Please Select Generic Name and then click on OK");
									wait(IRTConstants.Wait.MIN_WAIT,propMap);
								} while(getWindowHandles().size()>3);
								
								switchWindow(irtWindow);
									
							}
							
							if(!StringUtils.isNullOrEmpty(formulation[count]))
							{
								//Formulation Reload
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_FORMULATION));
								wait(IRTConstants.Wait.PASS_WAIT, propMap);
								
								if(!selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_FORMULATION),
										formulation[count]))
								{
									formulation[count] = CpatToolConstants.ERROR_STRING +formulation[count];
								}
								
								formulationValues.append(formulation[count]);
							}
							
							wait(IRTConstants.Wait.MIN_WAIT, propMap);
							
							if(!StringUtils.isNullOrEmpty(actionTaken[count]))
							{
								if(!selectDropdownValueByValueXpathOverride(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_ACTION_WITH_DRUG),
										actionTaken[count]))
								{
									actionTaken[count] = CpatToolConstants.ERROR_STRING +actionTaken[count];
								}
								
								actionTakenValues.append(actionTaken[count]);
							}
							if(count<productFlag.length-1)
							{
								actionTakenValues.append(StringConstants.COMMA_WITH_SPACE);
								formulationValues.append(StringConstants.COMMA_WITH_SPACE);
								productFlagValues.append(StringConstants.COMMA_WITH_SPACE);
								
								if(StringUtils.isNullOrEmpty(productDescValues.toString())) 
								{
									productDescValues.append(StringConstants.COMMA_WITH_SPACE);
								}	
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.SUSP_DRUG_ADD_ENTRY));
								wait(IRTConstants.Wait.MIN_WAIT,propMap);
							}
						}
						dataEntryMap.put(IRTConstants.DataEntry.SUSP_DRUG_PRODUCT_FLAG, productFlagValues.toString());
						dataEntryMap.put(IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC, productDescValues.toString());
						dataEntryMap.put(IRTConstants.DataEntry.SUSP_DRUG_FORMULATION, formulationValues.toString());
						dataEntryMap.put(IRTConstants.DataEntry.SUSP_DRUG_ACTION_WITH_DRUG, actionTakenValues.toString());
						
						suspectDrugFlag = true;
					}
					fieldValue =  StringUtils.emptyString(dataEntryMap.get(l_writerkey));
					
				}
				else if(l_writerkey.equals(IRTConstants.DataEntry.ADVERSE_EVENT_REPORTED_ITEM)||
						l_writerkey.equals(IRTConstants.DataEntry.ADVERSE_EVENT_SERIOUS)||
						l_writerkey.equals(IRTConstants.DataEntry.ADVERSE_EVENT_OUTCOME))
				{
					if(!adverseEventFlag) {
						String[] reportedItem =  dataEntryMap.get(IRTConstants.DataEntry.ADVERSE_EVENT_REPORTED_ITEM).split(SciConstants.StringConstants.TILDE);
						String[] serious =  dataEntryMap.get(IRTConstants.DataEntry.ADVERSE_EVENT_SERIOUS).split(SciConstants.StringConstants.TILDE);
						String[] outcome =  dataEntryMap.get(IRTConstants.DataEntry.ADVERSE_EVENT_OUTCOME).split(SciConstants.StringConstants.TILDE);
						
						StringBuffer reportedItemvalues = new StringBuffer();
						StringBuffer seriousalues = new StringBuffer();
						StringBuffer outcomevalues = new StringBuffer();
						
						for(int count = 0; count<reportedItem.length; count++)
						{
							findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.DataEntry.ADVERSE_EVENT_REPORTED_ITEM),
									reportedItem[count]);
							reportedItemvalues.append(reportedItem[count]);
							
							if(!StringUtils.isNullOrEmpty(serious[count]))
							{
								if(!selectDropdownValueByValueByIdentifier(IRTConstants.XPATH_BY_ID, propMap.get(IRTConstants.DataEntry.ADVERSE_EVENT_SERIOUS),serious[count]))
								{
									serious[count] = CpatToolConstants.ERROR_STRING +serious[count];
								}
								seriousalues.append(serious[count]);
							}
							if(!StringUtils.isNullOrEmpty(outcome[count]))
							{
								if(!selectDropdownValueByValueByIdentifier(IRTConstants.XPATH_BY_ID, propMap.get(IRTConstants.DataEntry.ADVERSE_EVENT_OUTCOME),outcome[count]))
								{
									outcome[count]= CpatToolConstants.ERROR_STRING +outcome[count];
								}
								outcomevalues.append(outcome[count]);
							}
							
							if(count<reportedItem.length-1)
							{		
								reportedItemvalues.append(StringConstants.COMMA_WITH_SPACE);
								seriousalues.append(StringConstants.COMMA_WITH_SPACE);
								outcomevalues.append(StringConstants.COMMA_WITH_SPACE);
									
								clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.ADVERSE_EVENT_ADD_ENTRY));
								wait(IRTConstants.Wait.MIN_WAIT,propMap);
							}
						}
						dataEntryMap.put(IRTConstants.DataEntry.ADVERSE_EVENT_REPORTED_ITEM,reportedItemvalues.toString() );
						dataEntryMap.put(IRTConstants.DataEntry.ADVERSE_EVENT_SERIOUS, seriousalues.toString());
						dataEntryMap.put(IRTConstants.DataEntry.ADVERSE_EVENT_OUTCOME, outcomevalues.toString());
						
						adverseEventFlag = true;
					}			
					fieldValue =  StringUtils.emptyString(dataEntryMap.get(l_writerkey));
				}	
				else if(l_fielddetails.getFieldType().equals(FieldDetails.FieldType.TEXTBOX))
				{
					fieldValue =  StringUtils.emptyString(dataEntryMap.get(l_writerkey));
					
					if(l_fielddetails.getXpathType().equals(XPathType.XPATH))
					{
						findElementByAbsoluteXpathAndSetContentOverride(propMap.get(l_writerkey),fieldValue);
					}
					else
					if(l_fielddetails.getXpathType().equals(XPathType.ID))
					{
						findElementByXpathAndSetContent(IRTConstants.XPATH_BY_ID, propMap.get(l_writerkey),fieldValue);
					}
					else
					if(l_fielddetails.getXpathType().equals(XPathType.NAME))
					{
						findElementByXpathAndSetContent(IRTConstants.XPATH_BY_NAME, propMap.get(l_writerkey), fieldValue);
					}
				}
				else if(l_fielddetails.getFieldType().equals(FieldDetails.FieldType.SELECT))
				{
					fieldValue =  StringUtils.emptyString(dataEntryMap.get(l_writerkey));
					if(l_fielddetails.getXpathType().equals(XPathType.XPATH))
					{
						if(!StringUtils.isNullOrEmpty(fieldValue))
						{
							if(!selectDropdownValueByValueXpathOverride(propMap.get(l_writerkey), fieldValue))
							{
								fieldValue = CpatToolConstants.ERROR_STRING +fieldValue;
								
							}
						}
					}
					else
					if(l_fielddetails.getXpathType().equals(XPathType.ID))
					{
						if(!StringUtils.isNullOrEmpty(fieldValue))
						{	
							if(!selectDropdownValueByValueByIdentifier(IRTConstants.XPATH_BY_ID, propMap.get(l_writerkey),fieldValue))
							{
								fieldValue = CpatToolConstants.ERROR_STRING +fieldValue;
							}
						}
					}
					else
					if(l_fielddetails.getXpathType().equals(XPathType.NAME))
					{
						if(!StringUtils.isNullOrEmpty(fieldValue))
						{
							if(!selectDropdownValueByValueByIdentifier(IRTConstants.XPATH_BY_NAME, propMap.get(l_writerkey),fieldValue))
							{
								fieldValue = CpatToolConstants.ERROR_STRING +fieldValue;
							}
						}
					}
				}
			
				//Result Summary Map
				caseDataSummMap.put(l_writerkey, StringUtils.emptyString(fieldValue));

			}
		}
		return caseDataSummMap;
	}
	
	public void readReceiptSummaryFields(Map<String, String> propMap)
	{
		receiptDataMap = new HashMap<String, String>();
		//Read Edit Data Form
		log.info("Read Receipt Summary");
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_RECEIPT, 
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_RECEIPT)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_MESSAGE,
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_MESSAGE)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_E2B_ACC_DATE,
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_E2B_ACC_DATE)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_SENDER, 
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_SENDER)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_RECEIVER, 
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_RECEIVER)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_RECOVER_ORG_OWNER_UNIT, 
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_RECOVER_ORG_OWNER_UNIT)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_OWNER,
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_OWNER)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_PROTOCOL_NO, 
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_PROTOCOL_NO)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_LOC_REC_DATE, 
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_LOC_REC_DATE)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_REC_DATE, 
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_REC_DATE)));
		
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_PENDING_COMMENT,
				getTextOfValueAttributeElementByXpath(propMap.get(IRTConstants.DataEntry.EDITRECEIPT_PENDING_COMMENT)));
		
		log.info( "Read receipt summary edit fields value ");
	}
	public Map<String, String> feedCaseDataTab(ParsedDataTable dataTable,  Map<String, String> propMap, Map<String, String>dataEntryMap, String areNo)
	{
		
		Map<String, String>  caseDataSummMap= new LinkedHashMap<String, String> ();
		
		String value = null;
		
		IRTE2BFieldsWriterMapper	l_mapper	=	IRTE2BFieldsWriterMapper.getInstance(IRTWebApplicationWriter.class.getClassLoader().getResourceAsStream("IRTWriterE2BMapper.txt"));
		l_writere2bfieldmapping	=	l_mapper.getWriterE2BFieldMapping();
		
		clickElementByAbsolutePath(propMap.get(IRTConstants.CaseData.CASEDATA));
		
		log.info("Case Data Feeding : ");
		
		for (String l_writerkey : l_writere2bfieldmapping.keySet()) 
		{
			FieldDetails	l_fielddetails	=	l_writere2bfieldmapping.get(l_writerkey);
			
			if(l_fielddetails != null && l_fielddetails.getTabName().equals(FieldDetails.TabName.CASEDATA))
			{
				log.info(l_writerkey);
				
				value = StringUtils.emptyString(dataEntryMap.get(l_writerkey));
				 
				if(l_writerkey.equals(IRTConstants.CaseData.SERIOUS_ASSESSMENT_SCEPTER_AER))
				{
					value = StringUtils.emptyString(areNo);
					findElementByAbsoluteXpathAndSetContentOverride(propMap.get(l_writerkey),value);
				}
				else
				if(l_fielddetails.getFieldType().equals(FieldDetails.FieldType.TEXTBOX))
				{
					if(l_fielddetails.getXpathType().equals(XPathType.XPATH))
					{
						findElementByAbsoluteXpathAndSetContentOverride(propMap.get(l_writerkey),value);
					}
					else if(l_fielddetails.getXpathType().equals(XPathType.ID))
					{
						findElementByXpathAndSetContent(IRTConstants.XPATH_BY_ID, propMap.get(l_writerkey),value);
					}
					else if(l_fielddetails.getXpathType().equals(XPathType.NAME))
					{
						findElementByXpathAndSetContent(IRTConstants.XPATH_BY_NAME, propMap.get(l_writerkey), value);
					}
				}
				else if(l_fielddetails.getFieldType().equals(FieldDetails.FieldType.SELECT))
				{
					if(l_fielddetails.getXpathType().equals(XPathType.XPATH))
					{
						
						if(!StringUtils.isNullOrEmpty(value))
						{
							if(!selectDropdownValueByValueXpathOverride(propMap.get(l_writerkey), value))
							{
								value = CpatToolConstants.ERROR_STRING +value;
							}
						}
					}
					else if(l_fielddetails.getXpathType().equals(XPathType.ID))
					{

						if(!StringUtils.isNullOrEmpty(value))
						{
							if(!selectDropdownValueByValueByIdentifier(IRTConstants.XPATH_BY_ID, propMap.get(l_writerkey),value))
							{
								value = CpatToolConstants.ERROR_STRING +value;
							}
						}
					}
					else if(l_fielddetails.getXpathType().equals(XPathType.NAME))
					{
						if(!StringUtils.isNullOrEmpty(value))
						{
							if(!selectDropdownValueByValueByIdentifier(IRTConstants.XPATH_BY_NAME, propMap.get(l_writerkey), value))
							{
									value = CpatToolConstants.ERROR_STRING +value;
								
							}
						}
					}
				}
				//Add field in result summary Map
				caseDataSummMap.put(l_writerkey, value);
			}
		}
		return caseDataSummMap;
	}
	
	//Irt Duplicate Search
	public void getIrtDuplicateRecordsAer(ParsedDataTable dataTable, Map<String, String> propMap, 
			List<DuplicateRecordFields> duplicateRecordFieldsList, String recNo)
	{
		int recordCount = 0;
		String duplicateRecordsFieldWindow = null;
		String aerNo = null;
		String sfeNo = null;
		int drugNameEntryCount = 0;
		int reactionEntryCount = 0;
		StringBuffer drugname = null;
		StringBuffer reportedReaction = null;
		StringBuffer seriuousness = null;
		StringBuffer outcome = null;
		
		irtDuplicateSearchWindow = getWindowHandle();
		DuplicateRecordFields duplicateRecodField = null;
		if(!isTableEmpty(propMap.get(IRTConstants.Duplicate.DUPLICATE_TBODY_XPATH)) )
		{
			recordCount = getAllRowSizeByTableBodyXpath( propMap.get(IRTConstants.Duplicate.DUPLICATE_TBODY_XPATH));
			for(int count = 1; count <= recordCount; count++)
			{
				drugNameEntryCount = 0;
				reactionEntryCount = 0;
				
				drugname = new StringBuffer();
				reportedReaction = new StringBuffer();
				seriuousness = new StringBuffer();
				outcome = new StringBuffer();
				
				
				aerNo = StringUtils.emptyString(getTextWithoutVisibilityByXpath(propMap.get
						(IRTConstants.IrtDuplicateRecord.AER_PREFIX)+(count)+propMap.get
						(IRTConstants.IrtDuplicateRecord.AER_SUFFIX)));
				
				sfeNo = getTextWithoutVisibilityByXpath(propMap.get
						(IRTConstants.IrtDuplicateRecord.SFE_PREFIX)+(count-1)+propMap.get
						(IRTConstants.IrtDuplicateRecord.SFE_SUFFIX));
				
				
				
				if((StringUtils.isNullOrEmpty(aerNo.trim())||(!duplicateAerExistInDuplicateList(duplicateRecordFieldsList, aerNo)) && !sfeNo.equalsIgnoreCase(recNo.trim())))
				{
					duplicateRecodField = new DuplicateRecordFields();
					
					duplicateRecodField.setAerNo(aerNo);
					
					clickElementByAbsolutePath(propMap.get
							(IRTConstants.Duplicate.DUPLICATE_RECORD_RECEIPT_PREFIX)+count+propMap.get
							(IRTConstants.Duplicate.DUPLICATE_RECORD_RECEIPT_SUFFIX));
					
					for(String windowHandle  : getSetOfWindows(4))
					{
					    if(!windowHandle.equals(irtWindow)&&!windowHandle.equals(parentWindow)
					    		&&!windowHandle.equals(irtDuplicateSearchWindow))
					   {
					    	duplicateRecordsFieldWindow = windowHandle;
					   }
					}
					switchWindow(duplicateRecordsFieldWindow);
					
					
					duplicateRecodField.setLocalCaseId(getTextOfValueAttributeElementByXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.CASE_ID)));
					duplicateRecodField.setCountryWhereOccure(getSelectedTextByDropDownXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.OCCUR_COUNTRY)));
					duplicateRecodField.setPatientDOB(getTextOfValueAttributeElementByXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.PATIENT_DOB)));
					duplicateRecodField.setPatientInitials(getTextOfValueAttributeElementByXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.PATIENT_INITIALS)));
					duplicateRecodField.setPatientGender(getSelectedTextByDropDownXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.PATIENT_GENDER)));
					duplicateRecodField.setReporterGivenName(getTextOfValueAttributeElementByXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.REPORTER_FIRST)));
					duplicateRecodField.setReporterFamilyName(getTextOfValueAttributeElementByXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.REPORTER_LAST)));
					duplicateRecodField.setReporterCountry(getSelectedTextByDropDownXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.REPORTER_COUNTRY)));
					
					drugNameEntryCount = Integer.parseInt(getTextOfValueAttributeElementByXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.DRUG_NAME_ENTRY_COUNT)));
					
					for(int drugCount= 0; drugCount<drugNameEntryCount; drugCount++ )
					{
						drugname.append(getTextOfValueAttributeElementByXpath(propMap.get
								(IRTConstants.IrtDuplicateRecord.DRUG_NAME)));
						
						if(drugCount < drugNameEntryCount-1)
						{
							drugname.append(SciConstants.StringConstants.COMMA_WITH_SPACE);
							clickElementByAbsolutePath(propMap.get
									(IRTConstants.IrtDuplicateRecord.DRUG_NAME_NEXT));
							wait(IRTConstants.Wait.MIN_WAIT,propMap );
						}
						
					}
					
					duplicateRecodField.setReportedDrugName(drugname.toString());
					
					reactionEntryCount = Integer.parseInt(getTextOfValueAttributeElementByXpath(propMap.get
							(IRTConstants.IrtDuplicateRecord.REACTION_ENTRYCOUNT)));
					
					for(int reactionCount= 0; reactionCount<reactionEntryCount; reactionCount++ )
					{
						reportedReaction.append(getTextOfValueAttributeElementByXpath(propMap.get
								(IRTConstants.IrtDuplicateRecord.REPORTED_REACTION)));
						
						seriuousness.append(getSelectedTextByDropDownXpath(propMap.get
								(IRTConstants.IrtDuplicateRecord.REPORTED_SERIOUS)));
						
						outcome.append(getSelectedTextByDropDownXpath(propMap.get
								(IRTConstants.IrtDuplicateRecord.REPORTED_OUTCOME)));
						
						
						if(reactionCount < reactionEntryCount-1)
						{
							reportedReaction.append(SciConstants.StringConstants.TILDE);
							seriuousness.append(SciConstants.StringConstants.TILDE);
							outcome.append(SciConstants.StringConstants.TILDE);
							clickElementByAbsolutePath(propMap.get
									(IRTConstants.IrtDuplicateRecord.REACTION_NEXT));
							wait(IRTConstants.Wait.MIN_WAIT,propMap );
						}
					}
					
					duplicateRecodField.setReactionRecordOutcome(outcome.toString());
					duplicateRecodField.setReactionRecordReportedReaction(reportedReaction.toString());
					duplicateRecodField.setReactionRecordSeriousness(seriuousness.toString());
					
					clickElementByAbsolutePath(propMap.get
							(IRTConstants.IrtDuplicateRecord.WINDOW_CLOSE));
					
					duplicateRecordFieldsList.add(duplicateRecodField);
					
					switchWindow(irtDuplicateSearchWindow);
				}
			}
		}
	}
	public void irtDuplicateSearchFirstApproch(ParsedDataTable dataTable, Map<String, String> propMap)
	{
		clickElementByAbsolutePath(propMap.get(IRTConstants.Classification.CLASSIFICATION_TAB));
		wait("minWait", propMap);
		
		findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_LOCAL_CASE_ID),
				dataTable.getColumnValues(CpatToolCConstants.ReportSummaryTabKeys.GENINFO_LOC_CASE_ID)
				.get(0).displayValue().toString().trim());
		//TODO: remove below code and uncomment above line
		
		
		clickElementByAbsolutePath(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_SEARCH));
		log.info("Perform Irt duplicate search first approach");
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
	}
	public void irtDuplicateSearchSecondApproch(ParsedDataTable dataTable, Map<String, String> propMap)
	{
		waitUntillInvibilityOfElementById(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_PROCCESSING_WINDOW));
		findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_LOCAL_CASE_ID),
				SciConstants.StringConstants.EMPTY);
		
	/*	findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_OTHER_ID_NO),
				SciConstants.StringConstants.PERCENT+ "049-0073-990003"+SciConstants.StringConstants.PERCENT);
		*/
		 //TODO: remove above code and uncomment below line
		findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_OTHER_ID_NO),
				SciConstants.StringConstants.PERCENT+CpatToolCHelper.getParsedDataFieldValue(dataTable, CpatToolCConstants.ReportSummaryTabKeys.GENINFO_LOC_CASE_ID)
				+SciConstants.StringConstants.PERCENT);
		
		
		clickElementByAbsolutePath(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_SEARCH));
		
		acceptAlert();
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		log.info(" Perform Irt duplicate search second approach");
	}
	public void irtDuplicateSearchThirdApproch(ParsedDataTable dataTable, 
			Map<String, String> propMap, Map<String, Boolean> searchCriteriaMap)
	{
		waitUntillInvibilityOfElementById(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_PROCCESSING_WINDOW));
		findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_OTHER_ID_NO),
				SciConstants.StringConstants.EMPTY);
		
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		List<String> valuesList = new ArrayList<String>();
			
		reporterGivenName  = StringUtils.emptyString(CpatToolCHelper.getParsedDataFieldValue(dataTable ,
				CpatToolCConstants.ReportSummaryTabKeys.REPORTER_FIRST));
		
		reporterFamilyName = StringUtils.emptyString(CpatToolCHelper.getParsedDataFieldValue(dataTable ,
				CpatToolCConstants.ReportSummaryTabKeys.REPORTER_LAST));
		
		patientDOB = StringUtils.emptyString(CpatToolCHelper.getParsedDataFieldValue(dataTable , 
				CpatToolCConstants.ReportSummaryTabKeys.PATIENT_DOB));
		
		patientInitials = StringUtils.emptyString(CpatToolCHelper.getParsedDataFieldValue(dataTable ,
				CpatToolCConstants.ReportSummaryTabKeys.PATIENT_PATIENT_INITIALS));
		
		patientGender = StringUtils.emptyString(CpatToolCHelper.getParsedDataFieldValue(dataTable ,
				CpatToolCConstants.ReportSummaryTabKeys.PATIENT_SEX));
		
		country = StringUtils.emptyString(CpatToolCHelper.getParsedDataFieldValue(dataTable ,
				CpatToolCConstants.ReportSummaryTabKeys.REPORTER_COUNTRY));
		
		DuplicateFields l_dupfielddata = new IRTDuplicateFields(
											!StringUtils.isNullOrEmpty(reporterGivenName)
										, 	!StringUtils.isNullOrEmpty(reporterFamilyName)
										,	!StringUtils.isNullOrEmpty(patientDOB)
										, 	!StringUtils.isNullOrEmpty(patientInitials)
										, 	!StringUtils.isNullOrEmpty(patientGender)
										, 	!StringUtils.isNullOrEmpty(country)
										);
		if (l_dupfielddata.isSearchToBePerformed(searchCriteriaMap)) {
			searchInIRT(propMap);
		}
		
		duplicateWindow = getWindowHandle();
	}
	public boolean duplicateAerExistInDuplicateList(List<DuplicateRecordFields> duplicateRecordList, String aerNo)
	{
		for(DuplicateRecordFields duplField : duplicateRecordList)
		{
			if(duplField.getAerNo().endsWith(aerNo))
			{
				return true;
			}
		}
		return false;
	}
	
	
	//Irt xpath validations
	
	private void getFieldLabel(List<String> receiptDataList, Map<String, String> propMap, String p_sTabName, String p_sGroupName, String p_sXpathFieldId, int anscesterValue, String controlClassName, String labelClassName, boolean editFlag)
	{
		String xpathForControl = propMap.get(p_sXpathFieldId),
				xpathFieldName = propMap.get(p_sXpathFieldId+_READ_LABLE_FIELD_NAME_PREFIX),
				derivedLabel = null,
				matchStatus = _MATCH_STATUS_FALSE;
		
		log.info(xpathFieldName);
		
		 try 
		 {
			derivedLabel = getFieldLableByXpath(xpathForControl, anscesterValue, controlClassName, labelClassName, editFlag);
			
			if(xpathFieldName.trim().equals(derivedLabel.trim()))
			{
				 matchStatus = _MATCH_STATUS_TRUE ; 
			}
			 
		 }
		 catch(Exception ex)
		 {
			 
			 matchStatus =_MATCH_STATUS_NOT_FOUND;
		 }
		
		receiptDataList.add(p_sTabName + ", " + p_sGroupName + ", " +xpathFieldName+","+ p_sXpathFieldId+", "+ xpathForControl+ ", "  + derivedLabel+","+matchStatus);
			
	}
	private void getFieldLabelByFieldId(List<String> receiptDataList, Map<String, String> propMap, String p_sTabName, String p_sGroupName, String p_sXpathFieldId, int anscesterValue,String controlClassName, String labelClassName,boolean editFlag)
	{
		String xpathForControl = propMap.get(p_sXpathFieldId),
				xpathFieldName = propMap.get(p_sXpathFieldId+_READ_LABLE_FIELD_NAME_PREFIX),
				derivedLabel = null,
				matchStatus = _MATCH_STATUS_FALSE;
		
		log.info(xpathFieldName);
		
		 try 
		 {
			derivedLabel = getFieldLableByXpath("//*[@id=\""+xpathForControl+"\"]", anscesterValue, controlClassName, labelClassName, editFlag);
			
			if(xpathFieldName.trim().equals(derivedLabel.trim()))
			{
				 matchStatus = _MATCH_STATUS_TRUE ; 
			}
			 
		 }
		 catch(Exception ex)
		 {
			 
			 matchStatus =_MATCH_STATUS_NOT_FOUND;
		 }
		
		receiptDataList.add(p_sTabName + ", " + p_sGroupName + ", " +xpathFieldName+","+ p_sXpathFieldId+", "+ xpathForControl+ ", "  + derivedLabel+","+matchStatus);
			
	}
	
	private boolean linkPresentByXpath(List<String> receiptDataList, Map<String, String> propMap, String p_sTabName, String p_sGroupName, String p_sXpathFieldId)
	{
		boolean xpathAvailableStatus = true;
		String xpathForControl = propMap.get(p_sXpathFieldId),
				xpathFieldName = propMap.get(p_sXpathFieldId+_READ_LABLE_FIELD_NAME_PREFIX),
				matchStatus = _MATCH_STATUS_FALSE;
		
		log.info(xpathFieldName);
		
		 try 
		 {
			 clickElementByAbsolutePath(xpathForControl);
				 matchStatus = _MATCH_STATUS_TRUE ; 
			 
		 }
		 catch(Exception ex)
		 {
			 matchStatus =_MATCH_STATUS_NOT_FOUND;
			 xpathAvailableStatus = false;
		 }
		receiptDataList.add(p_sTabName + ", " + p_sGroupName + ", " +xpathFieldName+","+ p_sXpathFieldId+", "+ xpathForControl+ ", ,"+matchStatus);
		return xpathAvailableStatus;	
	}

	private boolean linkPresentById(List<String> receiptDataList, Map<String, String> propMap, String p_sTabName, String p_sGroupName, String p_sXpathFieldId, String inputType)
	{
		boolean xpathAvailableStatus = true;
		String xpathForControl = propMap.get(p_sXpathFieldId),
				xpathFieldName = propMap.get(p_sXpathFieldId+_READ_LABLE_FIELD_NAME_PREFIX),
				matchStatus = _MATCH_STATUS_FALSE;
		
		log.info(xpathFieldName);
		
		 try 
		 {
			 clickElementByXpath(inputType, IRTConstants.XPATH_BY_ID, xpathForControl);
				
				 matchStatus = _MATCH_STATUS_TRUE ; 
			 
		 }
		 catch(Exception ex)
		 {
			 matchStatus =_MATCH_STATUS_NOT_FOUND;
			 xpathAvailableStatus = false;
		 }
		receiptDataList.add(p_sTabName + ", " + p_sGroupName + ", " +xpathFieldName+","+ p_sXpathFieldId+", "+ xpathForControl+ ", ,"+matchStatus);
		return xpathAvailableStatus;	
	}

	public boolean searchCaseXpathValidation(String caseId ,  Map<String, String> propMap)
	{
		boolean xpthNotFoundFlag = true;
		String  searchTextBox = null;
		
		editXpathVaList = new ArrayList<>();
		
		searchTextBox = propMap.get(IRTConstants.SearchCase.INBOUND_INBOUND_LISTING_SEARCH);
		
		xpthNotFoundFlag = linkPresentByXpath(editXpathVaList, propMap, _MAIN_MENU,SciConstants.StringConstants.EMPTY,IRTConstants.SearchCase.INBOUND_XPATH);
		
		if(!xpthNotFoundFlag)
		{
			return xpthNotFoundFlag;
		}
		
		
		xpthNotFoundFlag = linkPresentByXpath(editXpathVaList, propMap, _MAIN_MENU,SciConstants.StringConstants.EMPTY,IRTConstants.SearchCase.INBOUND_INBOUND_LISTING);
		if(!xpthNotFoundFlag)
		{
			return xpthNotFoundFlag;
		}
		
		try {
		setLableAsSelectByXpath(propMap.get(IRTConstants.CaseProcess.EDIT_SELECT_REVIEW), 
				propMap.get(IRTConstants.CaseProcess.EDIT_SELECT_REVIEW_INPUT),
				propMap.get(IRTConstants.CaseProcess.EDIT_SELECT_REVIEW_SELECT),
				IRTConstants.INITIAL_REVIEW);
		}
		catch(Exception exc)
		{
			editXpathVaList.add(_INBOUND_SEARCH + ", " +SciConstants.StringConstants.EMPTY  + ", " +"Activity"+",, "+ propMap.get(IRTConstants.CaseProcess.EDIT_SELECT_REVIEW)+ ", ,"+ "true");
			
		}
		
		xpthNotFoundFlag = linkPresentByXpath(editXpathVaList, propMap, _INBOUND_SEARCH,SciConstants.StringConstants.EMPTY,IRTConstants.SearchCase.INBOUND_INBOUND_LISTING_SEARCH);
		if(!xpthNotFoundFlag)
		{
			return xpthNotFoundFlag;
		} 
		findElementByAbsoluteXpathAndSetContentOverride(searchTextBox, caseId);
		
		xpthNotFoundFlag = linkPresentByXpath(editXpathVaList, propMap, _INBOUND_SEARCH,SciConstants.StringConstants.EMPTY,IRTConstants.SearchCase.INBOUND_INBOUND_LISTING_SEARCH_BUTTION);
		if(!xpthNotFoundFlag)
		{
			return xpthNotFoundFlag;
		}
		
		xpthNotFoundFlag = linkPresentByXpath(editXpathVaList, propMap, _INBOUND_SEARCH,SciConstants.StringConstants.EMPTY,IRTConstants.CaseProcess.EDIT_CASE_TABLE_BODY);
		
		if(isTableEmpty(propMap.get(IRTConstants.CaseProcess.EDIT_CASE_TABLE_BODY)))
		{
			return xpthNotFoundFlag;
		}
		else if(isElementVisibleById(propMap.get(IRTConstants.CaseProcess.EDIT_CASE_LOCK), IRTConstants.INPUT_TYPE_IMAGE))
		{
			xpthNotFoundFlag = linkPresentById(editXpathVaList, propMap, _INBOUND_SEARCH, SciConstants.StringConstants.EMPTY, IRTConstants.CaseProcess.EDIT_CASE_LOCK, IRTConstants.INPUT_TYPE_IMAGE);
			
			unlockCase(caseId, propMap);
			searchCase(caseId, propMap);
		}	
		
		xpthNotFoundFlag = linkPresentById(editXpathVaList, propMap, _INBOUND_SEARCH, SciConstants.StringConstants.EMPTY, IRTConstants.CaseProcess.EDIT_IMAGE_ID, IRTConstants.INPUT_TYPE_IMAGE);
		
		return xpthNotFoundFlag;
	}
	public List<String>  downloadCasePdfXpathValidation( Map<String, String> propMap)
	{
		List<String> documentXpathList = null;
		
		documentXpathList = new ArrayList<>();
		
		linkPresentByXpath(documentXpathList, propMap, DOCUMENT_TAB, SciConstants.StringConstants.EMPTY, IRTConstants.CaseDocumentDownload.DOCUMENT_MENU);
		
		linkPresentById(documentXpathList, propMap, DOCUMENT_TAB, SciConstants.StringConstants.EMPTY, IRTConstants.CaseDocumentDownload.DOWNLOAD_CASE_TABLE_BODY_ID, IRTConstants.TABLE_BODY);
		
		return documentXpathList;
	}
	
	public List<String> readReceiptSummaryList(Map<String, String> propMap)
	{
		List<String> receiptDataList = new ArrayList<String>();
		
		//clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.RECEIPT_SUMMARY));
		
		//Read Edit Data Form
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_EDIT_RECEIPT, IRTConstants.DataEntry.EDITRECEIPT_RECEIPT,1,_EDIT_RECEIPT_CONTROL_CLASS_FIELD,_EDIT_RECEIPT_LABLE_CLASS_FIELD,true);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_EDIT_RECEIPT, IRTConstants.DataEntry.EDITRECEIPT_MESSAGE,1,_EDIT_RECEIPT_CONTROL_CLASS_FIELD,_EDIT_RECEIPT_LABLE_CLASS_FIELD,true);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_EDIT_RECEIPT, IRTConstants.DataEntry.EDITRECEIPT_RECOVER_ORG_OWNER_UNIT,1,_EDIT_RECEIPT_CONTROL_CLASS_FIELD,_EDIT_RECEIPT_LABLE_CLASS_FIELD,true);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_EDIT_RECEIPT, IRTConstants.DataEntry.EDITRECEIPT_OWNER,1,_EDIT_RECEIPT_CONTROL_CLASS_FIELD,_EDIT_RECEIPT_LABLE_CLASS_FIELD,true);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_EDIT_RECEIPT, IRTConstants.DataEntry.EDITRECEIPT_PROTOCOL_NO,1,_EDIT_RECEIPT_CONTROL_CLASS_FIELD,_EDIT_RECEIPT_LABLE_CLASS_FIELD,true);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_EDIT_RECEIPT, IRTConstants.DataEntry.EDITRECEIPT_LOC_REC_DATE,1,_EDIT_RECEIPT_CONTROL_CLASS_FIELD,_EDIT_RECEIPT_LABLE_CLASS_FIELD,true);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_EDIT_RECEIPT, IRTConstants.DataEntry.EDITRECEIPT_REC_DATE,1,_EDIT_RECEIPT_CONTROL_CLASS_FIELD,_EDIT_RECEIPT_LABLE_CLASS_FIELD,true);
	
		//Read Data Entry Form
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_GEN_INFO, IRTConstants.DataEntry.GENINFO_REPORT_TYPE,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_GEN_INFO, IRTConstants.DataEntry.GENINFO_LOC_CASE_ID,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_GEN_INFO, IRTConstants.DataEntry.GENINFO_COUNTRY_OF_OCCUR,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_GEN_INFO, IRTConstants.DataEntry.GENINFO_INIT_REC_DATE,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_GEN_INFO, IRTConstants.DataEntry.GENINFO_LAST_REC_DATE,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		
	
		//Fill Data Entry Form > Seriousness Criteria
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SERIOUSNESS_CRITERIA, IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SERIOUSNESS_CRITERIA, IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SERIOUSNESS_CRITERIA, IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SERIOUSNESS_CRITERIA, IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SERIOUSNESS_CRITERIA, IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SERIOUSNESS_CRITERIA, IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
	
		
		// Fill Data Entry Form > Additional Reference
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_ADDITIONAL_REFERENCE, IRTConstants.DataEntry.ADD_REF_COMP_UNIT,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_ADDITIONAL_REFERENCE, IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_ADDITIONAL_REFERENCE,IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
			
		//	clickElementByAbsolutePath(propMap.get(IRTConstants.DataEntry.ADD_REF_ADD_ENTRY));
		
		//Suspect Drug
		
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SUSPECT_DRUG, IRTConstants.DataEntry.SUSP_DRUG_PRODUCT_FLAG,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SUSPECT_DRUG, IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		
		String receiptWindow = getWindowHandle();
		
		linkPresentByXpath(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SUSPECT_DRUG, IRTConstants.DataEntry.SUSP_DRUG_PROD_DESC_SERACH);
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		switchWindow(receiptWindow);
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SUSPECT_DRUG, IRTConstants.DataEntry.SUSP_DRUG_ACTION_WITH_DRUG,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		linkPresentByXpath(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_SUSPECT_DRUG, IRTConstants.DataEntry.SUSP_DRUG_ADD_ENTRY);
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		// Drug Therapy
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_DRUG_THERAPY, IRTConstants.DataEntry.DRUG_THERAPY_ROUT_ADMIN,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
	
		// Adverse Event
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_ADVERSE_EVENT, IRTConstants.DataEntry.ADVERSE_EVENT_REPORTED_ITEM,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		linkPresentByXpath(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_ADVERSE_EVENT, IRTConstants.DataEntry.ADVERSE_EVENT_ADD_ENTRY);
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		//Patient
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PATIENT, IRTConstants.DataEntry.PATIENT_PATIENT_ID,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabelByFieldId(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PATIENT, IRTConstants.DataEntry.PATIENT_PATIENT_UNKNOWN,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabelByFieldId(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PATIENT, IRTConstants.DataEntry.PATIENT_SEX,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabelByFieldId(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PATIENT, IRTConstants.DataEntry.PATIENT_AGE_GROUP,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PATIENT, IRTConstants.DataEntry.PATIENT_DOB,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PATIENT, IRTConstants.DataEntry.PATIENT_AGE_EVENT_TEXT,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		
		// Protocol
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PROTOCOL, IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		
		linkPresentByXpath(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_PROTOCOL, IRTConstants.DataEntry.PROTOCOL_PROTOCOL_NO_SEARCH);
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		switchWindow(receiptWindow);
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		
		//Reporter
		getFieldLabelByFieldId(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_QUALIFICATION,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabelByFieldId(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_DO_NOT_REPORT_TIME,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_FIRST,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_LAST,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_CITY,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_STATE,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabelByFieldId(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_COUNTRY,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_ZIPCODE,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		
		linkPresentByXpath(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_REPORTER, IRTConstants.DataEntry.REPORTER_ADD_ENTRY);
		wait(IRTConstants.Wait.MIN_WAIT, propMap);
		//Literature
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_LITERATURE, IRTConstants.DataEntry.LITERATURE_ART_TITLE,3,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(receiptDataList, propMap, _TABNAME_RECEIPT_SUMMARY, _GROUP_LITERATURE, IRTConstants.DataEntry.LITERATURE_JOURNAL_TITLE,3,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
	
		return receiptDataList;
	}
	public List<String> readCaseDataList(Map<String, String> propMap)
	{
		List<String> casedataList = new ArrayList<String>(); 
		clickElementByAbsolutePath(propMap.get(IRTConstants.CaseData.CASEDATA));
		
		getFieldLabel(casedataList, propMap, _TABNAME_CASE_DATA, _GROUP_SERIOUS_ASSESMENT, IRTConstants.CaseData.SERIOUS_ASSESSMENT_REPORT_SERIOUSNESS,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(casedataList, propMap, _TABNAME_CASE_DATA, _GROUP_SERIOUS_ASSESMENT, IRTConstants.CaseData.SERIOUS_ASSESSMENT_PRIORITY,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
		getFieldLabel(casedataList, propMap, _TABNAME_CASE_DATA, _GROUP_SERIOUS_ASSESMENT, IRTConstants.CaseData.SERIOUS_ASSESSMENT_SCEPTER_AER,2,_DATA_ENTRY_CONTROL_CLASS_FIELD,_DATA_ENTRY_LABLE_CLASS_FIELD,false);
			
		return casedataList;
		}
	public void generateCsvReport(List<String>  receiptFieldList,List<String>  caseDataFieldList, List<String>  documentList )
	{
		FileWriter writer = null;
		 try {
		     writer = new FileWriter(userDownloadPath+"IRTData.csv");
		     writer.append("Tab Name,");
		     writer.append("Group Name,");
		     writer.append("Field Name,");
		     writer.append("ConfigKeyIdForControl,");
		     writer.append("XpathForControlFromConfig,");
		     writer.append("ValueOfLabelAsReadFromBrowser,");
		     writer.append("Match Status");
		     writer.append('\n');
		     
		     if(editXpathVaList !=null)
		     {
			     for(String data : editXpathVaList)
			     {
			    	 writer.append(data);
				     writer.append('\n');
			     }
		     }
		     
		     if(receiptFieldList !=null)
		     {
			     for(String data : receiptFieldList)
			     {
			    	 writer.append(data);
				     writer.append('\n');
			     }
		     }
		     
		     if(caseDataFieldList !=null)
		     {
			     for(String data : caseDataFieldList)
			     {
			    	 writer.append(data);
				     writer.append('\n');
			     }
		     }
		     
		     if(caseDataFieldList !=null)
		     {
			     for(String data : documentList)
			     {
			    	 writer.append(data);
				     writer.append('\n');
			     }
		     }    
		     log.info("CSV file is created...");
		  } 
		 catch (IOException e) {
			 
			 log.error("Error in csv Generation");
		     e.printStackTrace();
		  }
		 finally 
		 	{
		        try 
		        {
		        	writer.flush();
				    writer.close();
		        } 
		        catch (IOException e) 
		        {
		        	log.error("Error in csv Generation");
		        	e.printStackTrace();
		        }
		 	}
	}
	private void wait(String waitKey,  Map<String, String> propMap)
	{
		try
		{
			setWait(propMap.get(waitKey));
		}
		catch (InterruptedException e)
		{
			log.error("Error in wait condition");
		}
	}
	public String readReceiptSummaryField(String key)
	{
		return receiptDataMap.get(key);
	}
	
	public String readFieldNameByWriterKey(String key)
	{
		return StringUtils.emptyString(fieldNameMap.get(key));
	}

	
	
	public void quitWebDriver() {
		deleteAllDownloadedFiles();
		super.quitDriver();
	}
	
	public void deleteAllDownloadedFiles()
	{
		WebApplicationWriterUtils.deleteDirectories(s_sDownloadFolder);
		log.info("Document download folder deleted.");
	}
	private int getCountAvailableValueInList(List<String> valueList)
	{
		int valueCount = 0;
		for(String value : valueList)
		{
			if(!StringUtils.isNullOrEmpty(value))
			{
				valueCount++;
			}
		}
		return valueCount;
	}
	
	private void searchInIRT(Map<String, String> propMap)
	{
		if(!StringUtils.isNullOrEmpty(reporterGivenName))
		{
			findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_REPORTER_FIRST_NAME)
					, SciConstants.StringConstants.PERCENT+reporterGivenName+SciConstants.StringConstants.PERCENT);
		}
		if(!StringUtils.isNullOrEmpty(reporterFamilyName))
		{
			findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_REPORTER_LAST_NAME)
					,SciConstants.StringConstants.PERCENT+reporterFamilyName+SciConstants.StringConstants.PERCENT);
		}
		if(!StringUtils.isNullOrEmpty(patientDOB))
		{
			findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_DOB_FIELD_1)
					,patientDOB);
			findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_DOB_FIELD_2)
					,patientDOB);	
		}
		if(!StringUtils.isNullOrEmpty(patientInitials))
		{
			findElementByAbsoluteXpathAndSetContentOverride(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_PAT_ID)
					, SciConstants.StringConstants.PERCENT+patientInitials+SciConstants.StringConstants.PERCENT);
		}
		if(!StringUtils.isNullOrEmpty(patientGender))
		{
			setLableAsSelectByXpath(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_SEX), 
					propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_SEX_INPUT),
					propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_SEX_SELECT),
					patientGender);
		}
		if(!StringUtils.isNullOrEmpty(country))
		{
			setLableAsSelectByXpath(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_COUNTRY), 
					propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_COUNTRY_INPUT),
					propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_COUNTRY_SELECT),
					country);
		}
		
		clickElementByAbsolutePath(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_SEARCH));
		log.info(" perform Irt duplicate Search third approach");
	}

	public void openUrl(String p_sceptreurl)
	{
		openNewUrl(p_sceptreurl);
	}
	public WebDriver getDriver()
	{
		return getWebDriver();
	}
	public void swithToIrtWindow()
	{
		switchWindow(irtWindow);
	}
	public void swithToSceptreWindow()
	{
		switchWindow(sceptreWindow);
	}
	public void swithToIrtDuplicateWindow()
	{
		switchToNewWindow(getSetOfWindows(3), parentWindow, irtWindow);
	}
	public boolean isCaseAlreadyProccessed(Map<String, String> propMap)
	{
		boolean caseProcessFlag = false;
		clickElementByAbsolutePath(propMap.get(IRTConstants.Classification.CLASSIFICATION_TAB));
		wait("minWait", propMap);
		// check if receipt already processed
		if(!isElementVisibleById(propMap.get(IRTConstants.Classification.DUPLICATE_SEARCH_CRITERIA_LOCAL_CASE_ID_ID), IRTConstants.INPUT_TYPE_INPUT))
		{
			caseProcessFlag = true;
		}
		return caseProcessFlag;
	}

}
