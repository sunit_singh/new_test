package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciEnums;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class ReporterQualification 
	implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;
	
	private static final String	HCP_CONSTANT	=	"HCP";
	
	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String	l_e2bfieldvalue			=	oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
		String	l_value	= SciEnums.ReporterQualification.CONSUMER_OTHERNONHEALTHPROF.getAgXchangeDisplayName();
		if (StringUtils.isNullOrEmpty(l_e2bfieldvalue) || l_e2bfieldvalue.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.NONE)) {
			l_value =  SciEnums.ReporterQualification.CONSUMER_OTHERNONHEALTHPROF.getAgXchangeDisplayName();
		} else if (l_e2bfieldvalue.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.SELF)) {
			l_value =  SciEnums.ReporterQualification.PATIENT.getAgXchangeDisplayName();
		} else if (l_e2bfieldvalue.equalsIgnoreCase(HCP_CONSTANT)) {
			l_value =  SciEnums.ReporterQualification.OTHERHEALTHPROF.getAgXchangeDisplayName();
		}
		p_dataentrymap.put(p_writerkey, l_value);
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		String l_sceptrereporterqualification = CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.REPORTER_QUALIFICATION);
		String	l_value	= SciEnums.ReporterQualification.CONSUMER_OTHERNONHEALTHPROF.getAgXchangeDisplayName();
		if(StringUtils.isNullOrEmpty(l_sceptrereporterqualification)){
			l_value =  SciEnums.ReporterQualification.CONSUMER_OTHERNONHEALTHPROF.getAgXchangeDisplayName();
		} else {
			l_value = SciEnums.ReporterQualification.toEnumFromSceptreValue(l_sceptrereporterqualification).getAgXchangeDisplayName();
		}
		p_dataentrymap.put(p_writerkey, l_value);
	}
}
