package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class Outcome implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		if(CpatToolCHelper.isStringObjEmptyOrNull(p_sourcedocumentdata.
				getColumnValues(p_sourcedocumentkey).get(0).displayValue()))
		{
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.ReactionTabKeys.NARRATIVE_REACTIONOUTCOME))
			{
				p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.UNKNOWN);
			}
			else
			{
				p_dataentrymap.put(p_writerkey, oPdtWrapper.getKeyValueAsString(CpatToolCConstants.ReactionTabKeys.NARRATIVE_REACTIONOUTCOME));
			}
		}
		else
		{
			p_dataentrymap.put(p_writerkey,p_sourcedocumentdata.
					getColumnValues(p_sourcedocumentkey).get(0).displayValue().toString()); 
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		String sceptre_outcomes = CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.REACTION_OUTCOME);
		if(!StringUtils.isNullOrEmpty(sceptre_outcomes)){
			p_dataentrymap.put(p_writerkey, sceptre_outcomes);
		} else {
			p_dataentrymap.put(p_writerkey, StringConstants.EMPTY);
		}
	}

}
