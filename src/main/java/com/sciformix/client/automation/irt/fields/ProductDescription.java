package com.sciformix.client.automation.irt.fields;

import java.util.List;
import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;

public class ProductDescription implements IFieldBusinessLogic  {

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		String productCode =  CpatToolCHelper.getParsedDataFieldValue(p_sourcedocumentdata, CpatToolCConstants.
								NarrativeKeys.NARRATIVE_PRODUCT_CODE);
		String product = StringConstants.EMPTY;
		String productForm = StringConstants.EMPTY;
		
		List<Object> approvalNo = CpatToolCHelper.productListDataTable.getColumnValues(CpatToolCConstants.FieldLogic.PRODUCT_LIST_APPROVAL_NO);
		List<Object> genericName = CpatToolCHelper.productListDataTable.getColumnValues(CpatToolCConstants.FieldLogic.PRODUCT_LIST_GENERIC_NAME);
		List<Object> productFormList = CpatToolCHelper.productListDataTable.getColumnValues(CpatToolCConstants.FieldLogic.PRODUCT_LIST_PRODUCT_FORM);
	
		for(int rowCount = 0;  rowCount<approvalNo.size(); rowCount++) {
			if(!StringUtils.isNullOrEmpty(approvalNo.get(rowCount).toString()) && productCode.startsWith(approvalNo.get(rowCount).toString())) {
				product = genericName.get(rowCount).toString();
				productForm = productFormList.get(rowCount).toString();
				break;
			}
		}
		
		if(CpatToolCHelper.getParsedDataFieldValue(p_sourcedocumentdata, CpatToolCConstants.
				NarrativeKeys.NARRATIVE_PRODUCT_REGULATORY_CLASS ).equals(CpatToolCConstants.FieldLogic.PRODUCT_REGULATORY_CLASS))
		{//Medicinal
			p_dataentrymap.put(p_writerkey, CpatToolCConstants.FieldLogic.PRODUCT_REGULATORY_CLASS);
		}
		else  
		{//Non Medicinal
			//p_dataentrymap.put(p_writerkey, product +SciConstants.StringConstants.SPACE+ productForm);
			p_dataentrymap.put(p_writerkey, product);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);	
	}

}
