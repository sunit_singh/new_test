package com.sciformix.client.automation.irt;

import java.util.Map;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class CommonFieldBusinessLogic implements IFieldBusinessLogic {

	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		p_dataentrymap.put(p_writerkey, oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey));
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		p_dataentrymap.put(p_writerkey, oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey));
	}
}
