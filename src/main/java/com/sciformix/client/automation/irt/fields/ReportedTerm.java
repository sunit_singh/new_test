package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class ReportedTerm implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	/**
	 * This will check below logic
	 * If E2B field (B.2.i.0) is blank or "See Narrative"
	 * then capture "verbatim English language" from case narrative
	 * otherwise populate the value there in E2B Field (B.2.i.0)
	 * @param p_dataentrymap The map in which the writer
	 * 		  value need to be set
	 * @param p_sourcedocumentdata Source Document Data
	 * @param p_writerkey The writer key whose value
	 * 		  need to be written
	 * @param p_sourcedocumentkey The parser key whose
	 *        value need to be extracted from the source
	 *        document
	 */
	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		if(oPdtWrapper.isKeyValueNullEmptyOrMatches(p_sourcedocumentkey, CpatToolCConstants.SEE_NARRATIVE_CONSTANT))
		{
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.VERBATIM_ENGLISH_LANGUAGE))
			{
				p_dataentrymap.put(p_writerkey, StringConstants.EMPTY);
			}
			else
			{
				p_dataentrymap.put(p_writerkey, oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.VERBATIM_ENGLISH_LANGUAGE));
			}	
		}
		else
		{
			p_dataentrymap.put(p_writerkey,oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey)); 
		}
	}

	/**
	 * This will check below logic
	 * Go to Reaction tab in sceptre.
	 * Click on List All button. Copy the Reported Reaction
	 * @param p_dataentrymap The map in which the writer
	 * 		  value need to be set
	 * @param p_sourcedocumentdata Source Document Data
	 * @param p_writerkey The writer key whose value
	 * 		  need to be written
	 * @param p_sourcedocumentkey The parser key whose
	 *        value need to be extracted from the source
	 *        document
	 */
	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		String sceptre_reportedReactions = CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.REACTION_REPORTED_REACTION);
		if(!StringUtils.isNullOrEmpty(sceptre_reportedReactions)){
			p_dataentrymap.put(p_writerkey, sceptre_reportedReactions);
		} else {
			p_dataentrymap.put(p_writerkey, StringConstants.EMPTY);
		}
	}
}
