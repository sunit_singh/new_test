package com.sciformix.client.automation.irt;

import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;

public class DuplicateRecordFields {
	
	private String aerNo = null;
	private String localCaseId = null;
	private String patientInitials = null;
	private String patientGender = null;
	private String patientDOB = null;
	private String patientAgeGroup = null;
	private String reporterFamilyName = null;
	private String reporterGivenName = null;
	private String reporterCountry = null;
	private String reporterQualification = null;
	private String countryWhereOccure = null;
	private String reactionRecordRank = null;
	private String reactionRecordReportedReaction = null;
	private String reactionRecordSeriousness = null;
	private String reactionRecordOutcome = null;
	private String reportedDrugName = null;
	
	
	
	public String getReportedDrugName() {
		return reportedDrugName;
	}
	public void setReportedDrugName(String reportedDrugName) {
		this.reportedDrugName = reportedDrugName;
	}
	public String getReporterQualification() {
		return reporterQualification;
	}
	public void setReporterQualification(String reporterQualification) {
		this.reporterQualification = reporterQualification;
	}
	public String getCountryWhereOccure() {
		return countryWhereOccure;
	}
	public void setCountryWhereOccure(String countryWhereOccure) {
		
		this.countryWhereOccure = CpatToolHome.updateDropDownAsBlank(countryWhereOccure, CpatToolConstants.DROPDOWN_VAL);
	}
	public String getPatientAgeGroup() {
		return patientAgeGroup;
	}
	public void setPatientAgeGroup(String patientAgeGroup) {
		this.patientAgeGroup = patientAgeGroup;
	}
	public String getAerNo() {
		return aerNo;
	}
	public void setAerNo(String aerNo) {
		this.aerNo = aerNo;
	}
	public String getLocalCaseId() {
		return localCaseId;
	}
	public void setLocalCaseId(String localCaseId) {
		this.localCaseId = localCaseId;
	}
	public String getPatientInitials() {
		return patientInitials;
	}
	public void setPatientInitials(String patientInitials) {
		this.patientInitials = patientInitials;
	}
	public String getPatientGender() {
		return patientGender;
	}
	public void setPatientGender(String patientGender) {
		this.patientGender = CpatToolHome.updateDropDownAsBlank(patientGender, CpatToolConstants.DROPDOWN_VAL);
	}
	public String getPatientDOB() {
		return patientDOB;
	}
	public void setPatientDOB(String patientDOB) {
		this.patientDOB = patientDOB;
	}
	public String getReporterFamilyName() {
		return reporterFamilyName;
	}
	public void setReporterFamilyName(String reporterFamilyName) {
		this.reporterFamilyName = reporterFamilyName;
	}
	public String getReporterGivenName() {
		return reporterGivenName;
	}
	public void setReporterGivenName(String reporterGivenName) {
		this.reporterGivenName = reporterGivenName;
	}
	public String getReporterCountry() {
		return reporterCountry;
	}
	public void setReporterCountry(String reporterCountry) {
		this.reporterCountry = CpatToolHome.updateDropDownAsBlank(reporterCountry, CpatToolConstants.DROPDOWN_VAL);
	}
	public String getReactionRecordRank() {
		return reactionRecordRank;
	}
	public void setReactionRecordRank(String reactionRecordRank) {
		this.reactionRecordRank = reactionRecordRank;
	}
	public String getReactionRecordReportedReaction() {
		return reactionRecordReportedReaction;
	}
	public void setReactionRecordReportedReaction(String reactionRecordReportedReaction) {
		this.reactionRecordReportedReaction = reactionRecordReportedReaction;
	}
	public String getReactionRecordSeriousness() {
		return reactionRecordSeriousness;
	}
	public void setReactionRecordSeriousness(String reactionRecordSeriousness) {
		this.reactionRecordSeriousness = reactionRecordSeriousness;
	}
	public String getReactionRecordOutcome() {
		return reactionRecordOutcome;
	}
	public void setReactionRecordOutcome(String reactionRecordOutcome) {
		this.reactionRecordOutcome = reactionRecordOutcome;
	}
	

}
