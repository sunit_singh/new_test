package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class ReporterLast implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		String e2b_primarySourceCountry = null;
		String e2b_reporterFamilyName = null;
		String e2b_narrativePatientInitials = null;
		e2b_primarySourceCountry = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY);	
		if(CpatToolCHelper.isCountryEMEA(e2b_primarySourceCountry)){
			p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.PRIVATE);
		} else {
			e2b_reporterFamilyName = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.ReporterTabKeys.REPORTER_FAMILY_NAME).trim();
			e2b_narrativePatientInitials = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.PATIENT_INITIALS).trim();
			if(!StringUtils.isNullOrEmpty(e2b_narrativePatientInitials) && !e2b_reporterFamilyName.equalsIgnoreCase(e2b_narrativePatientInitials)){
				p_dataentrymap.put(p_writerkey, e2b_narrativePatientInitials);
			} else {
				p_dataentrymap.put(p_writerkey, e2b_reporterFamilyName);
			}
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}

}
