package com.sciformix.client.automation.irt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sciformix.commons.SciConstants;

public class IRTE2BFieldsWriterMapper {
	
	private static InputStream
		fileMapper
	;
	
	private final Map<String, FieldDetails> 
		writerE2BFieldMapping
	;	
	
	private IRTE2BFieldsWriterMapper(InputStream p_mapperfile) {
		String			l_temp			=	null;
		String[]		l_linedata		=	null;
		FieldDetails	l_fielddetails	=	null;
		writerE2BFieldMapping	=	new LinkedHashMap<String, FieldDetails>();
		try (BufferedReader l_br =	new BufferedReader(new InputStreamReader(p_mapperfile))){
			l_temp	=	l_br.readLine();
			while (l_temp != null) {
				l_linedata		=	l_temp.split(SciConstants.StringConstants.COMMA);
				if (l_linedata.length != 6) {
					throw new InstantiationException();
				}
				l_temp			=	l_br.readLine();
				l_fielddetails	=	null;
				l_fielddetails	=	new FieldDetails(
										l_linedata[1]
									,	l_linedata[2]
									,	l_linedata[3]
									,	l_linedata[4]
									,	l_linedata[5]	
									);
				writerE2BFieldMapping.put(l_linedata[0], l_fielddetails);
			}
		} catch (IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			/**
			 * Runtime exception is thrown because
			 * we cannot handle the same in the calling
			 * code of the inner class
			 */
			throw new RuntimeException("Error reading propties file");
		}
	}
	
	private static class IRTE2BFieldsWriterMapperHelper {
		private static final IRTE2BFieldsWriterMapper INSTANCE	=	new IRTE2BFieldsWriterMapper(fileMapper);
	}
	
	public static IRTE2BFieldsWriterMapper getInstance(InputStream p_mapperfile) {
		fileMapper	=	p_mapperfile;
		return IRTE2BFieldsWriterMapperHelper.INSTANCE;
	}
	
	public Map<String, FieldDetails> getWriterE2BFieldMapping() {
		Map<String, FieldDetails>	l_writere2bfieldmapping	=	new LinkedHashMap<>();
		for (String l_writer: writerE2BFieldMapping.keySet()) {
			l_writere2bfieldmapping.put(l_writer, writerE2BFieldMapping.get(l_writer));
		}
		return l_writere2bfieldmapping;
	}
}
