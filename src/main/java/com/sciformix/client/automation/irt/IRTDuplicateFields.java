package com.sciformix.client.automation.irt;

import java.util.Map;

import com.sciformix.client.pat.commons.DuplicateFields;
import com.sciformix.commons.SciConstants.StringConstants;

public final class 
	IRTDuplicateFields
extends
	DuplicateFields
{
	
	public IRTDuplicateFields(boolean reporterGivenName, boolean reporterFamilyName, boolean patientDOB,
			boolean patientInitials, boolean patientGender, boolean country) {
		super(reporterGivenName, reporterFamilyName, patientDOB, patientInitials, patientGender, country);
	}

	@Override
	public boolean isSearchToBePerformed(Map<String, Boolean> p_dupsearchcriteriamap) {
		if (p_dupsearchcriteriamap == null) {
			throw new InstantiationError("The search criteria map is null");
		} else {
			return p_dupsearchcriteriamap.get(createSearchCriteria()) == null ? false : p_dupsearchcriteriamap.get(createSearchCriteria());
		}
	}

	@Override
	public String createSearchCriteria() {
		StringBuilder l_searchparameter = new StringBuilder();
		l_searchparameter.append(
			reporterGivenName + StringConstants.TILDE +
			reporterFamilyName + StringConstants.TILDE +
			patientDOB + StringConstants.TILDE +
			patientInitials + StringConstants.TILDE +
			patientGender + StringConstants.TILDE +
			country + StringConstants.TILDE
		);
		return l_searchparameter.toString();
	}
}
