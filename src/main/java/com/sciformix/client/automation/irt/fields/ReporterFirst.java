package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class ReporterFirst implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		String e2b_primarySourceCountry = null;
		//String e2b_reporterGivenName = null;
		e2b_primarySourceCountry = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.CaseIdTabKeys.PRIMARY_SOURCE_COUNTRY).toLowerCase();
		if(CpatToolCHelper.isCountryEMEA(e2b_primarySourceCountry)){
			p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.PRIVATE);
		} else {
			//e2b_reporterGivenName = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.ReporterTabKeys.REPORTER_GIVEN_NAME);
			p_dataentrymap.put(p_writerkey, SciConstants.StringConstants.EMPTY);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}

}
