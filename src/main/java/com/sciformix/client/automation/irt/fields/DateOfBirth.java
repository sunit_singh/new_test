package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class DateOfBirth implements IFieldBusinessLogic {
	
	private static final Logger log = LoggerFactory.getLogger(DateOfBirth.class);
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		String e2b_Dob = null;
		String e2b_narrativePatientDob = null; 
		try{
			e2b_Dob = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			if(StringUtils.isNullOrEmpty(e2b_Dob)){
				e2b_narrativePatientDob = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.PATIENT_DOB);
				p_dataentrymap.put(p_writerkey, e2b_narrativePatientDob);
			} else {
				p_dataentrymap.put(p_writerkey, e2b_Dob);
			}
			
		} catch(Exception e){
			log.error("FieldName : DateOfBirth");
			log.error("Error : " , e);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.DATE_OF_BIRTH);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
		
		String e2b_Dob = null;
		String e2b_narrativePatientDob = null; 
		try{
			e2b_Dob = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			if(StringUtils.isNullOrEmpty(e2b_Dob)){
				e2b_narrativePatientDob = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.PATIENT_DOB);
				if(StringUtils.isNullOrEmpty(e2b_narrativePatientDob)){
					p_dataentrymap.put(p_writerkey, CpatToolCHelper.sceptreFieldsMap.get(IRTConstants.SceptreFieldData.PAT_DOB));
				} else {
					p_dataentrymap.put(p_writerkey, e2b_narrativePatientDob);
				}
			} else {
				p_dataentrymap.put(p_writerkey, e2b_Dob);
			}
			
		} catch(Exception e){
			log.error("FieldName : DateOfBirth");
			log.error("Error : " , e);
			throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.DATE_OF_BIRTH);
		}
		
	}

}