package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;

public class Unknown implements IFieldBusinessLogic {

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		if(StringUtils.isNullOrEmpty(p_dataentrymap.get(IRTConstants.DataEntry.PATIENT_PATIENT_ID)))
		{
			p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.UNKNOWN);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
}	
