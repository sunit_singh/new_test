package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class SeriousnessCriteriaFields implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);
			
		if(oPdtWrapper.isKeyValueNullOrEmpty(p_sourcedocumentkey))
		{
			p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.NO);
		}
		else
		{
			String value =  oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			if(value.equalsIgnoreCase("y") || value.equalsIgnoreCase(CpatToolConstants.FieldsLogicValues.YES))
			{
				p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
			}
			else
			{
				p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.NO);
			}
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}

}
