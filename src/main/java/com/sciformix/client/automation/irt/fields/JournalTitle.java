package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;
import com.sciformix.commons.utils.StringUtils;

public class JournalTitle implements IFieldBusinessLogic {
	
	ParsedDataTableWrapper oPdtWrapper = null;
	
	private static final Logger log = LoggerFactory.getLogger(JournalTitle.class);

	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		String e2b_literatureReferences = null;
		String e2b_narrativeCaseNotes = null;
		try{
			e2b_literatureReferences = oPdtWrapper.getKeyValueAsString(p_sourcedocumentkey);
			if(StringUtils.isNullOrEmpty(e2b_literatureReferences)){
				e2b_narrativeCaseNotes = oPdtWrapper.getKeyValueAsString(CpatToolCConstants.NarrativeKeys.CASE_NOTES);
				p_dataentrymap.put(p_writerkey, e2b_narrativeCaseNotes);
			} else {
				p_dataentrymap.put(p_writerkey, e2b_literatureReferences);
			}
		}catch(Exception e){
			log.error("FieldName : JournalTitle");
			log.error("Error : " , e);
			throw new RuntimeException();
		}
		
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}

}
