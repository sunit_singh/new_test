package com.sciformix.client.automation.irt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import com.sciformix.commons.SciConstants.StringConstants;

public class 
	IRTDuplicateSearchCriteria
{
	private final Map<String, Boolean>
		searchCriteriaMap
	;	
	private static InputStream
		fileMapper
	;	

	private IRTDuplicateSearchCriteria(InputStream p_mapperfile) {
		String	 l_temp		=	null;
		String[] l_linedata	=	null;
		boolean[] l_fielddata = null;
		
		BufferedReader	l_br = new BufferedReader(new InputStreamReader(p_mapperfile));
		try {
			searchCriteriaMap = new HashMap<>();
			l_temp	= l_br.readLine();
			while (l_temp != null) {
				IRTDuplicateFields	l_irtdupfields = null;
				l_linedata	=	l_temp.split(StringConstants.TILDE);
				l_fielddata = new boolean [l_linedata.length];
				for (int l_i=0; l_i < l_linedata.length; l_i++) {
					l_fielddata[l_i] = Boolean.valueOf(l_linedata[l_i]);
				}
				l_irtdupfields	=	new IRTDuplicateFields(
										l_fielddata[0]
									, 	l_fielddata[1]
									, 	l_fielddata[2]
									, 	l_fielddata[3]
									, 	l_fielddata[4]
									, 	l_fielddata[5]
									);
				searchCriteriaMap.put(l_irtdupfields.createSearchCriteria(), l_fielddata[6]);
				l_temp	= l_br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error reading propties file");
		}
	}
	
	private static class IRTDuplicateSearchCriteriaHelper {
		private static IRTDuplicateSearchCriteria INSTANCE = new IRTDuplicateSearchCriteria(fileMapper);
	}
	
	public static IRTDuplicateSearchCriteria getInstance(InputStream p_mapperfile) {
		fileMapper	=	p_mapperfile;
		return IRTDuplicateSearchCriteriaHelper.INSTANCE;
	}
	
	public Map<String, Boolean> getDuplicateSearchCriteriaMap() {
		Map<String, Boolean> l_dupsearchcriteriamap = new HashMap<>();
		for (String l_searchkey: searchCriteriaMap.keySet()) {
			l_dupsearchcriteriamap.put(l_searchkey, searchCriteriaMap.get(l_searchkey));
		}
		return l_dupsearchcriteriamap;
	}
}
