package com.sciformix.client.automation.irt.fields;

import java.util.Map;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCErrorMessages;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTableWrapper;

public class BirthDefect  implements IFieldBusinessLogic{

	ParsedDataTableWrapper oPdtWrapper = null;
	@Override
	public void applyBusinessLogicInitial(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
	
		oPdtWrapper = new ParsedDataTableWrapper(p_sourcedocumentdata);

		if (oPdtWrapper.isKeyValueNullEmptyOrMatches(p_sourcedocumentkey, CpatToolConstants.FieldsLogicValues.NO, CpatToolConstants.FieldsLogicValues.NONE))
		{
			if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT))
			{
				if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT))
				{
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.NO);
				}
				else
				{
					p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
				}
			}
			else
			{
				if (oPdtWrapper.doesKeyValueMatch(CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT, CpatToolConstants.FieldsLogicValues.YES))
				{
					p_dataentrymap.put(p_writerkey, CpatToolConstants.FieldsLogicValues.YES);
				}
				else
				{
					if(oPdtWrapper.isKeyValueNullOrEmpty(CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT))
					{
						p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.NO);
					} else {
					throw new RuntimeException(CpatToolCErrorMessages.DataEntryFailure.BIRTH_DEFECT);
				}
			}
		}
		}
		else
		{	
			p_dataentrymap.put(p_writerkey,CpatToolConstants.FieldsLogicValues.YES);
		}
	}

	@Override
	public void applyBusinessLogicFollowUp(Map<String, String> p_dataentrymap, ParsedDataTable p_sourcedocumentdata,
			String p_writerkey, String p_sourcedocumentkey) {
		applyBusinessLogicInitial(p_dataentrymap, p_sourcedocumentdata, p_writerkey, p_sourcedocumentkey);
	}
}
