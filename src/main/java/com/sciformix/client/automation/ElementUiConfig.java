package com.sciformix.client.automation;

public class ElementUiConfig
{
	public static enum TYPE
	{
		FIELD_DIRECLTY_MAPPED, DATE_TRIPLET_MAPPED_FIELD , VARY_XPATH_MAPPED_FIELD;
	}
	
		private String m_sAccessMethod = null;
		private String[] m_sAccessDetails = null;
		private String m_sAccessAtribute = null;
	
		public TYPE m_nFieldType = null;
		
		private ElementUiConfig(TYPE p_nFieldType, String sAccessMethod, String[] sAccessDetails , String sAccessAtribute)
		{
			m_nFieldType = p_nFieldType;
			m_sAccessMethod = sAccessMethod;
			m_sAccessDetails = sAccessDetails;
			m_sAccessAtribute = sAccessAtribute;
		}
		
		public static ElementUiConfig createField(String sAccessMethod, String sAccessDetails , String sAccessAtribute)
		{
			String [] sarrAccesDetails = null;
			sarrAccesDetails = new String[1];
			sarrAccesDetails[0] = sAccessDetails;
			return new ElementUiConfig(TYPE.FIELD_DIRECLTY_MAPPED, sAccessMethod, sarrAccesDetails, sAccessAtribute);
		}
		
		public static ElementUiConfig createDateField(String sAccessMethod, String[] sarrAccessDetails, String sAccessAtribute)
		{
			return new ElementUiConfig(TYPE.DATE_TRIPLET_MAPPED_FIELD, sAccessMethod, sarrAccessDetails, sAccessAtribute);
		}
		
		public static ElementUiConfig createVaryXpathField(String sAccessMethod, String[] sarrAccessDetails , String sAccessAtribute)
		{
			return new ElementUiConfig(TYPE.VARY_XPATH_MAPPED_FIELD, sAccessMethod, sarrAccessDetails, sAccessAtribute);
		}

		public String getAccessMethod() {
			return m_sAccessMethod;
		}

		public void setAccessMethod(String m_sAccessMethod) {
			this.m_sAccessMethod = m_sAccessMethod;
		}

		public String[] getAccessDetails() {
			return m_sAccessDetails;
		}

		public void setAccessDetails(String[] m_sAccessDetails) {
			this.m_sAccessDetails = m_sAccessDetails;
		}
		
		public String getAccessAtribute() {
			return m_sAccessAtribute;
		}

		public void setAccessAtribute(String m_sAccessAtribute) {
			this.m_sAccessAtribute = m_sAccessAtribute;
		}
	
}
