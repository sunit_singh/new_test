package com.sciformix.client.automation.argus;

public class ArgusConstants
{
	
	public static class ArgusSearchFields
	{
		public static final ArgusSearchFields AERNUMBER = new ArgusSearchFields("AERNUMBER");
		public static final ArgusSearchFields PATIENTAGE = new ArgusSearchFields("PATIENTAGE");
		public static final ArgusSearchFields PATIENTGENDER = new ArgusSearchFields("PATIENTGENDER");
		public static final ArgusSearchFields COUNTRY = new ArgusSearchFields("COUNTRY");
		public static final ArgusSearchFields PRODUCT = new ArgusSearchFields("PRODUCT");
		public static final ArgusSearchFields EVENT = new ArgusSearchFields("EVENT");
		public static final ArgusSearchFields DATEOFBIRTH = new ArgusSearchFields("DATEOFBIRTH");
		
		
		private String m_sInternalName = null;
		
		private ArgusSearchFields(String p_sInternalName)
		{
			m_sInternalName = p_sInternalName;
		}
		
		public String internalName()
		{
			return m_sInternalName;
		}
		
		public boolean equals(ArgusSearchFields other)
		{
			return this.m_sInternalName.equalsIgnoreCase(other.m_sInternalName);
		}
	}
	
	public static class ArgusSearchResultColumns
	{
		public static final String CASEID = "CASEID";
		public static final String INITIAL_RECEIPT_DATE = "Initial Receipt Date";
		public static final String PRODUCTS = "Product";
		public static final String REPORT_TYPE = "Report Type";
		public static final String PROJECT_ID_STUDY_ID = "Project ID-Study ID";
		public static final String REPORTER = "Reporter";
		public static final String COUNTRY = "Country";
		public static final String EVENTS = "Events";
		public static final String PATIENT_INITIALS = "Pat Initials";
		public static final String PATIENT_ID = "Patient ID";
	}
	
	public static enum FETCH_SCOPE
	{
		//CRITICAL_FIELDS, BASIC_FIELDS, ALL;
		CRITICAL_FIELDS, ALL;
	}
	
}