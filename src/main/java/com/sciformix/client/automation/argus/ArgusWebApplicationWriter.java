package com.sciformix.client.automation.argus;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.BaseWebApplicationWriter;
import com.sciformix.client.automation.Case;
import com.sciformix.client.automation.ElementUiConfig;
import com.sciformix.client.automation.Products;
import com.sciformix.client.automation.SearchResults;
import com.sciformix.client.automation.WebApplicationWriterUtils;
import com.sciformix.client.automation.argus.ArgusConstants.ArgusSearchFields;
import com.sciformix.client.automation.argus.ArgusConstants.FETCH_SCOPE;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.PreviewCaseDoc;
import com.sciformix.client.pat.insertvalues.ICaseOperations;
import com.sciformix.client.pat.toola.CaseStatus;
import com.sciformix.client.pat.toola.CaseStatusUpdate;
import com.sciformix.client.pat.toola.CpatToolAHelper.PROPOSED_ACTION;
import com.sciformix.client.pat.toola.Product;
import com.sciformix.client.pat.toola.ProductUtils;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;
//import com.sciformix.commons.utils.Products;
import com.sciformix.commons.utils.ParsedDataTable;

public class ArgusWebApplicationWriter extends BaseWebApplicationWriter
{
	private static final Logger log = LoggerFactory.getLogger(ArgusWebApplicationWriter.class);
	
	private static Map<String, String> propMap = new HashMap<String, String>();
	
	private static final String HEALTH_CANADA_FILE_TYPE = "HealthCanada-AERReport-PDF";
	
	public static Map<ArgusSearchFields, ElementUiConfig> mapSearchConfiguration = new HashMap<ArgusSearchFields, ElementUiConfig>();
	
	private static final String CONFIG_FOR_INSERT_PROPERTIES = "ConfigForInsert.properties";
	
	private static String m_sBrowserDriverLocation = null;
	
	public PROPOSED_ACTION eProposedAction = null;
	public String sDuplicateCaseId = null;
	
	public String sMatchingDuplicateCaseId = null;
	
	static
	{
		propMap = BaseWebApplicationWriter.loadProperties(CONFIG_FOR_INSERT_PROPERTIES);
		
		m_sBrowserDriverLocation = WebApplicationWriterUtils.getBrowserLocation(propMap.get("webdriver.driverlocation"));
		
		mapSearchConfiguration.put(ArgusSearchFields.AERNUMBER, ElementUiConfig.createField("xpath-name", propMap.get("Search.AER") , propMap.get("Attribute.Name")));
		mapSearchConfiguration.put(ArgusSearchFields.PATIENTAGE, ElementUiConfig.createField("xpath-name", propMap.get("Search.Patient.Age"), propMap.get("Attribute.Name")));
		mapSearchConfiguration.put(ArgusSearchFields.PATIENTGENDER, ElementUiConfig.createField("xpath-name", propMap.get("Search.Patient.Gender"), propMap.get("Attribute.Name")));
		mapSearchConfiguration.put(ArgusSearchFields.COUNTRY, ElementUiConfig.createField("xpath-name", propMap.get("CreateCase.Country"), propMap.get("Attribute.Name")));
		mapSearchConfiguration.put(ArgusSearchFields.PRODUCT, ElementUiConfig.createField("xpath-name", propMap.get("server.product.searchProductName"), propMap.get("Attribute.Name")));
		mapSearchConfiguration.put(ArgusSearchFields.EVENT, ElementUiConfig.createField("xpath-name", propMap.get("CreateCase.DescriptionAsReported"), propMap.get("Attribute.Name")));
		mapSearchConfiguration.put(ArgusSearchFields.DATEOFBIRTH, ElementUiConfig.createField("xpath-name", propMap.get("Search.Patient.DOB"), propMap.get("Attribute.Name")));
	}
	
	
	public ArgusWebApplicationWriter()
	{
		//TODO:Put download folder
		super(BROWSER.IE, propMap.get("webbrowserdriver") , m_sBrowserDriverLocation , true , "");
	}
	
	
	public void login(String p_sUserid, String p_sPassword)throws InterruptedException
	{
		String 	uIdentifier = null
		,		uValue = null;
		
		String 	pIdentifier = null
		,		pValue = null;
		
		String 	liType = null
		,		liIdentifier = null
		,		liValue = null
		,		parentFrameName=null
		,		childFrameName=null;
		
		uValue = propMap.get("server.loginform.userid.value");
		uIdentifier = propMap.get("server.loginform.userid.identifier");
		
		pValue = propMap.get("server.loginform.password.value");
		pIdentifier = propMap.get("server.loginform.password.identifier");
		
		liType = propMap.get("server.loginbtn.type");
		liIdentifier = propMap.get("server.loginbtn.identifier");
		liValue = propMap.get("server.loginbtn.value");
		parentFrameName = propMap.get("server.loginform.parentframe");
		childFrameName =  propMap.get("server.loginform.childframe");
		
		
		switchToFrame(parentFrameName);
		
		setWait(propMap.get("minWait"));
		findElementByXpathAndSetContent(uIdentifier, uValue, p_sUserid);
		findElementByXpathAndSetContent(pIdentifier, pValue, p_sPassword);
		enterClickByXpath(liType, liIdentifier, liValue);
		setWait(propMap.get("minWait"));
		switchParentToChildFrame(parentFrameName, childFrameName);
		
	}

	
	public SearchResults search(Map<ArgusConstants.ArgusSearchFields, Object> oCriteria,Map<String,String> propMap) throws InterruptedException
	{
		ElementUiConfig elementUiConfig = null;
		SearchResults oResults = null;
		int nNumberOfSearchResultRows = 0 , nNumberOfSearchResultRowsPerPage = 0;
		ArgusSearchFields oSearchField = null;
		boolean l_bNoCaseFound=false;
		Set<Entry<ArgusSearchFields, Object>> iterKeys=null;
		Products l_oProduct = null;
		
		String 	sValue = null , sElementXpath = null, sElementAttribute = null
				, sResultCaseId = null;
		
		String 	sInitialReceiptDate = null , sReporter = null , sCountry = null, sProduct = null, sEvent = null
				, sReportType= null, sPatientInitials = null, sProjectID = null, sPatientID = null , m_sWindow = null
				, selectProduct = null;
		
		//UI Constants read from Properties file
		sResultCaseId = propMap.get("CreateCase.SeachResult.RowsId");
		sInitialReceiptDate = propMap.get("GetRowValue.InitialReceiptDate");
		sReporter = propMap.get("GetRowValue.ReporterName");
		sCountry = propMap.get("GetRowValue.Country");
		sProduct = propMap.get("GetRowValue.Product");
		sEvent = propMap.get("GetRowValue.Event");
		sReportType = propMap.get("GetRowValue.ReportType");
		sPatientInitials = propMap.get("GetRowValue.PatientInitials");
		sProjectID = propMap.get("GetRowValue.ProjectID");
		sPatientID = propMap.get("GetRowValue.PatientID");
		
		selectProduct = propMap.get("CreateCase.SelectProduct");
		
		iterKeys = oCriteria.entrySet();
		
		for(Entry<ArgusSearchFields,Object> entry : iterKeys)
		{
			oSearchField = entry.getKey();
			
			if (oSearchField.equals(ArgusSearchFields.PRODUCT))
			{
				m_sWindow = getWindowHandle();
				
				l_oProduct = (Products) entry.getValue();
				
				enterClickByXpath("INPUT","name",selectProduct);
				
				selectProduct(m_sWindow, l_oProduct);
				
			}
			else
			{
				//Simple Search Input fields
				
				//Else SET the value in the Argus Search screen
				elementUiConfig = mapSearchConfiguration.get(oSearchField);
				
				sValue=entry.getValue().toString();
				
				sElementXpath = elementUiConfig.getAccessDetails()[0];
				
				sElementAttribute=elementUiConfig.getAccessAtribute();
				
				findElementByXpathAndSetContent(sElementAttribute, sElementXpath, sValue);
			}
		}
		
		//Execute the search now by clicking on the button 'Search'
		clickSearchButton();
		
		oResults = new SearchResults();
		nNumberOfSearchResultRows = getTotalNumberOfCaseResult();
		
		nNumberOfSearchResultRowsPerPage = getAllRowSizeByTableID("div","id", propMap.get("Search.TableId"));
		
		if(nNumberOfSearchResultRows == 1)
		{
			l_bNoCaseFound=isElementContainsText("div","id", propMap.get("Search.TableId"), "No case found");
		}
		
		
		if(l_bNoCaseFound)
		{
			
		}
		else
		{
			for(int nRowCounter = 1 ; nRowCounter <= nNumberOfSearchResultRows; nRowCounter++)
			{
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.CASEID, getTextByElement(getElement("tr", "id", sResultCaseId+nRowCounter)));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.INITIAL_RECEIPT_DATE, getTextOfValueAttributeElementByXpath("name", sInitialReceiptDate+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.PRODUCTS,  getTextOfValueAttributeElementByXpath("name", sProduct+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.REPORT_TYPE,  getTextOfValueAttributeElementByXpath("name", sReportType+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.PROJECT_ID_STUDY_ID,  getTextOfValueAttributeElementByXpath("name", sProjectID+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.REPORTER,  getTextOfValueAttributeElementByXpath("name", sReporter+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.COUNTRY,  getTextOfValueAttributeElementByXpath("name", sCountry+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.EVENTS,  getTextOfValueAttributeElementByXpath("name", sEvent+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.PATIENT_INITIALS,  getTextOfValueAttributeElementByXpath("name", sPatientInitials+nRowCounter));
				oResults.insertValue(ArgusConstants.ArgusSearchResultColumns.PATIENT_ID,  getTextOfValueAttributeElementByXpath("name", sPatientID+nRowCounter));
			
				if(nNumberOfSearchResultRowsPerPage == nRowCounter)
				{
					
					nextResultPage();
					
					setWait(propMap.get("commonWait2"));
					
					nRowCounter = 0;
					
					nNumberOfSearchResultRows=nNumberOfSearchResultRows - nNumberOfSearchResultRowsPerPage;
				}
			}
			
		}
		
		
		
		return oResults;
	}
	
	
	public Case retrieveCase(String p_sCaseId, FETCH_SCOPE p_eFetchScope) throws InterruptedException
	{
		Case oCase = null;
		Map<String ,Products> l_mapAllProduct = null;
		
		searchPage();
		
		enterIntoCase(p_sCaseId);
		
		oCase = new Case(p_sCaseId);
		
		if(p_eFetchScope == FETCH_SCOPE.ALL)
		{
			
			oCase.storeValue("ReporterType",  getTextOfValueAttributeElementByXpath("name", propMap.get("server.Reporter.Type")));
			
			executeJavaScript(propMap.get("server.ProductTab"));
			
			setWait(propMap.get("minWait"));
			
			isProductTab();
			
			setWait(propMap.get("minWait"));
			
			l_mapAllProduct = fetchALLProduct();
			
			oCase.storeValue("Products",  l_mapAllProduct);
		}
		else if(p_eFetchScope == FETCH_SCOPE.CRITICAL_FIELDS)
		{
			oCase.storeValue("ReporterType",  getTextOfValueAttributeElementByXpath("name", propMap.get("server.Reporter.Type")));
		}
		
		return oCase;
	}
	
	
	public void prepareForSearch() throws InterruptedException
	{
		String caseOptionsearch = null , clearButton = null;
		caseOptionsearch = propMap.get("caseaction.dropdown.valueForSearch");
		clearButton=propMap.get("Search.Clear");
		
		setWait(propMap.get("minWait"));
		
		executeJavaScript(caseOptionsearch);
		
		setWait(propMap.get("waitForSearchCase"));
		
		enterClickByXpath("INPUT","name",clearButton);
		
		setWait(propMap.get("minWait"));
		
		
//		String clearButton=propMap.get("Search.Clear");
//		enterClickByXpath("INPUT","name",clearButton);
	}
	
	
	public boolean checkAllEvents(DataTable oDataTableNew,Map<String,Object>existingDataFromArgusMap){
		int eventCount=0,newEventCount=0,matchesCount=0;
		String descReportedAdd=null,newDescReportedAdd=null;
		boolean matchMorethanOne=false;
		
		if(oDataTableNew.isColumnPresent("Event Count"))
				newEventCount = (int) oDataTableNew.getColumnValues("Event Count").get(0);
			
			eventCount=(int) existingDataFromArgusMap.get("Event Count");
			
			
			for(int j=1;j<=newEventCount;j++){
			
				if(oDataTableNew.isColumnPresent("DescriptionasReported"+j))
					newDescReportedAdd = (String) oDataTableNew.getColumnValues("DescriptionasReported"+j).get(0);
			
				for(int i=1;i<=eventCount;i++){
					
					if(existingDataFromArgusMap.containsKey("DescriptionasReported"+i))
						descReportedAdd=(String) existingDataFromArgusMap.get("DescriptionasReported"+i);
					
					if(newDescReportedAdd.equals(descReportedAdd)){
						matchesCount++;
						break;
					}
				}
			}
			if(matchesCount>=2){
				matchMorethanOne=true;
			}
		return matchMorethanOne;
	}
	
	
	private void selectProduct(String p_sParentWindow,Products p_oProduct)throws  InterruptedException{
		
		String productSelectButton=null,searchProductName=null,l_sProductTableName=null;
		
		searchProductName=propMap.get("server.product.searchProductName");
		productSelectButton=propMap.get("CreateCase.SelectButton");
	
		l_sProductTableName=propMap.get("server.product.SearchId");
		
		getSetOfWindows(2);
			
		switchWindowHandler(p_sParentWindow);
		
		findElementByXpathAndSetContent("name", searchProductName, p_oProduct.getProductName());
		
		enterClickByXpath("INPUT","name",propMap.get("server.Product.searchButton"));
		
		clickOnTextByTableId(l_sProductTableName, p_oProduct.getProductName());
		
		clickOnTextByTableIdSpanText(propMap.get("server.Product.DivAttrForSelectDosageForm") , p_oProduct.getDosageForm());		
		
		enterClickByXpath("INPUT","name",productSelectButton);
		
		setWait(propMap.get("minWait"));
		
		switchWindow(p_sParentWindow);
		
		switchParentToChildFrame(propMap.get("server.loginform.parentframe"), propMap.get("server.loginform.childframe"));
		
		
	}
	
	
	public Map<String,Object> getTextOfAllTabs()throws InterruptedException{
		
		boolean checkReporterHcpPage = false, checkProductPage=false,checkEventPage=false,checkPatientFeedingPage=false;
		String repoPrefix=null,initRecpDate=null,centralrecpDate=null,generalTab=null,descReported=null,descToBeCoded=null
				,productTab=null, productName=null, pcmDose=null, pcmFrequency=null,eventTab=null,patientTab=null
				,patFirstName=null,patLastName=null,patAddress=null,newProductTabTable=null,newProductTDID=null;
		int productCount=0;
		Map<String,Products>getProductMapFromArgus=null;
		Products product=null;
		Map<String,Object>fetchAllDataByGetText=null;
		
		fetchAllDataByGetText=new HashMap<>();
		productTab = propMap.get("server.ProductTab");
		repoPrefix = propMap.get("server.Reporter.prefix");
		initRecpDate= propMap.get("server.DateOfAwareness");
		centralrecpDate=propMap.get("server.centralRecptDate");
		generalTab = propMap.get("server.GeneralTab");
		productName=propMap.get("server.Product.Name");
		pcmDose = propMap.get("server.product.Dose");
		pcmFrequency = propMap.get("server.product.DoseFrequency");
		descReported=propMap.get("server.Event.DescReported");
		descToBeCoded=propMap.get("server.Event.DescCoded");
		eventTab = propMap.get("server.EventTab");
		patientTab = propMap.get("server.PatientTab");
		patFirstName = propMap.get("server.Patient.FirstName");
		patLastName = propMap.get("server.Patient.LastName");
		patAddress = propMap.get("server.Patient.Address");
		newProductTabTable=propMap.get("server.Product.NewProductTableName");
		newProductTDID=propMap.get("server.Product.NewProductTDID");
		
		executeJavaScript( generalTab);
		
		setWait(propMap.get("minWait"));
		
		//check page absents
		checkReporterHcpPage=findMultipleElements( new String[]{repoPrefix,initRecpDate,centralrecpDate});
		if(!checkReporterHcpPage)
		{
			log.error("General Tab page not found,Redirecting to general tab");
			executeJavaScript( generalTab);
		}
		
		fetchAllDataByGetText=fetchReporterDetails(fetchAllDataByGetText, false);
		fetchAllDataByGetText=fetchReporterDetails(fetchAllDataByGetText, true);
		
		setWait(propMap.get("minWait"));
		
		executeJavaScript( patientTab);
		
		setWait(propMap.get("commonWait"));
		
		checkPatientFeedingPage=findMultipleElements( new String[]{patFirstName,patLastName,patAddress});
		if(!checkPatientFeedingPage)
		{
			log.error("patien tab not found,Redirecting to patient tab");
			executeJavaScript( patientTab);
		}
		
		fetchAllDataByGetText=fetchPatientDetails(fetchAllDataByGetText);
		
		setWait(propMap.get("commonWait"));
		
		executeJavaScript( productTab);
		
		setWait(propMap.get("commonWait"));
		
		checkProductPage=findMultipleElements( new String[]{productName,pcmDose,pcmFrequency});
		if(!checkProductPage){
			log.error("product tab not found,Redirecting to product tab");
			executeJavaScript( productTab);
		}
		
		getProductMapFromArgus=new HashMap<>();
		
		//get total count of product
		productCount=getTDCountByTableId(	newProductTabTable,	newProductTDID);
		
		for(int i=1;i<=productCount-2;i++){
			
			executeScriptByTitle( newProductTabTable, newProductTDID+i);
			
			setWait(propMap.get("minWait"));
			
			//fetch product details by get text of argus tab
			product=fetchProductDetails();
			
			//add product details into map
			getProductMapFromArgus.put("Product"+i, product);
		}
		
		//put all products
		fetchAllDataByGetText.put("Products", getProductMapFromArgus);
		
		setWait(propMap.get("waitForEventTab"));
		
		executeJavaScript(eventTab);
		
		setWait(propMap.get("commonWait"));
		
		checkEventPage=findMultipleElements( new String[]{descReported,descToBeCoded});
		if(!checkEventPage){
			log.error("Event tab not found,Redirecting to Event tab");
			executeJavaScript( eventTab);
		}
		
		//get all events
		fetchAllDataByGetText=fetchEventDetails(fetchAllDataByGetText);
		
		return fetchAllDataByGetText;
	}
	
	
	public Map<String,Object> fetchReporterDetails(Map<String,Object>mapOfReporterFromGetText,boolean HCP){
		
		String centralRcptDate=null,intRecieptDate=null,repoCountry=null,occupation=null
				,institution=null,reportertype=null,reportType=null,scriptAttribute=null,newHCPTableName=null;
		
		centralRcptDate= propMap.get("server.centralRecptDate");
		intRecieptDate= propMap.get("server.DateOfAwareness");
		repoCountry = propMap.get("server.Reporter.Country");
		institution=propMap.get("server.Reporter.Institution");
		reportertype=propMap.get("server.Reporter.Type");
		reportType=propMap.get("server.Report.Type");
		occupation=propMap.get("server.Reporter.Occupation");
		
		scriptAttribute = propMap.get("server.Script.Attribute");
		newHCPTableName=propMap.get("server.Reporter.NewHCPTableName");
		
		if(!HCP)
		{
			mapOfReporterFromGetText.put("Occupation", getTextOfValueAttributeElementByXpath("name", occupation));
			mapOfReporterFromGetText.put("CRD", getTextOfValueAttributeElementByXpath("name", centralRcptDate));
			mapOfReporterFromGetText.put("Date of Awareness", getTextOfValueAttributeElementByXpath("name", intRecieptDate));
			mapOfReporterFromGetText.put("Country", getTextOfValueAttributeElementByXpath("name", repoCountry));
			mapOfReporterFromGetText.put("PrimaryReporter", getTextOfValueAttributeElementByXpath("name", reportertype));
			mapOfReporterFromGetText.put("ReportType", getTextOfValueAttributeElementByXpath("name", reportType));
			mapOfReporterFromGetText.put("Institution", getTextOfValueAttributeElementByXpath("name", institution));
		}
		else
		{
			dynamicExecuteJavaScriptByText(newHCPTableName,scriptAttribute,"(Consumer)");
			mapOfReporterFromGetText.put("HCP:Occupation", getTextOfValueAttributeElementByXpath("name", occupation));
			mapOfReporterFromGetText.put("HCP:Country", getTextOfValueAttributeElementByXpath("name", repoCountry));
			mapOfReporterFromGetText.put("HCP:PrimaryReporter", getTextOfValueAttributeElementByXpath("name", reportertype));
			mapOfReporterFromGetText.put("HCP:Institution", getTextOfValueAttributeElementByXpath("name", institution));
		}
	 
		return mapOfReporterFromGetText;
	}
	
	
	public Map<String,Object> fetchPatientDetails(Map<String,Object>mapOfPatientFromGetText){
		String patInitials=null,patCountry=null,patAge=null,patAgeUnits=null
				,patHeight=null,patWeight=null,patGender=null;
		
		patInitials=propMap.get("server.Patient.Initials");
		patCountry=propMap.get("server.Patient.Country");
		patAge=propMap.get("server.Patient.Age");
		patAgeUnits=propMap.get("server.Patient.AgeUnits");
		patHeight=propMap.get("server.Patient.height");
		patWeight=propMap.get("server.Patient.weight");
		patGender=propMap.get("server.Patient.Gender");
		
		mapOfPatientFromGetText.put("Patient Details.Initial", getTextOfValueAttributeElementByXpath("name", patInitials));
		mapOfPatientFromGetText.put("Country", getTextOfValueAttributeElementByXpath("name", patCountry));
		mapOfPatientFromGetText.put("Patient Details.Age", getTextOfValueAttributeElementByXpath("name", patAge));
		mapOfPatientFromGetText.put("Patient Details.Age Units", getTextOfValueAttributeElementByXpath("name", patAgeUnits));
		mapOfPatientFromGetText.put("Patient Details.Height", getTextOfValueAttributeElementByXpath("name", patHeight));
		mapOfPatientFromGetText.put("Patient Details.Weight", getTextOfValueAttributeElementByXpath("name", patWeight));
		mapOfPatientFromGetText.put("Patient Details.Gender", getTextOfValueAttributeElementByXpath("name", patGender));
	
		return mapOfPatientFromGetText;
	}
	
	
	public Products fetchProductDetails(){
		
		String dosageForm=null,patRouteOfAdmin=null
				,prodName=null,hpRole=null , prodIndicationPrefix = null , prodIndicationSuffix = null ,l_sDoseTableID = null
				, l_sDoseSectionTitleTDID = null;
		Products product=null;
		String[] l_saIndication = null , l_saDose=null , l_saDoseUnit=null , l_saDoseFrequency=null;
		int l_nIndicationCount = 0 , l_nDoseCount = 0;
		
		prodName=getTextOfValueAttributeElementByXpath("name", propMap.get("server.Product.Name"));
		prodIndicationPrefix = propMap.get("server.Product.IndicationPrefix");
		prodIndicationSuffix= propMap.get("server.Product.IndicationSuffix");
		l_sDoseTableID = propMap.get("server.Product.DoseCountTableID");
		l_sDoseSectionTitleTDID = propMap.get("server.Product.DoseSectionTitleTDID");

		if(prodName != null && !prodName.equals(""))
		{
			product=new Products();
			
			dosageForm=getTextOfValueAttributeElementByXpath("name", propMap.get("server.Product.Formulation"));
			patRouteOfAdmin=getTextOfValueAttributeElementByXpath("name", propMap.get("server.Product.PatRouteOfAdmin"));
			hpRole=isRadioButtonCheckedAndGetValue( propMap.get("server.Drug.DrugType"));
			
			l_nIndicationCount = getCountByXpath("//table[@id='" + propMap.get("server.Product.IndicationCountTableID") + "']" , "RowPosition");
			
			l_saIndication = new String[l_nIndicationCount];
			
			for(int i = 0; i < l_nIndicationCount ; i++)
			{
				l_saIndication[i]=getTextOfValueAttributeElementByXpath("name", prodIndicationPrefix + i + prodIndicationSuffix);
			}
			
			l_nDoseCount = getDosesCount();
			l_saDose = new String[l_nDoseCount]; 
			l_saDoseUnit = new String[l_nDoseCount];
			l_saDoseFrequency = new String[l_nDoseCount];
			
			for(int i = 0; i < l_nDoseCount ; i++)
			{
				
				executeScriptByTitle(l_sDoseTableID, l_sDoseSectionTitleTDID+(i+1));
				
				l_saDose[i]=getTextOfValueAttributeElementByXpath("name", propMap.get("server.product.Dose"));
				l_saDoseUnit[i]=getTextOfValueAttributeElementByXpath("name", propMap.get("server.product.DoseUnit"));
				l_saDoseFrequency[i]=getTextOfValueAttributeElementByXpath("name", propMap.get("server.product.DoseFrequency"));
				
			}
			if(hpRole.equals("1")){
				hpRole="Suspect";
			}else{
				hpRole="Concomitant";
			}
		}
		
		
		if(prodName != null && !prodName.equals("")){
			if(dosageForm != null)
				product.setDosageForm(dosageForm);
			if(l_saDose != null)
				product.setDose(l_saDose);
			if(l_saDoseUnit != null)
				product.setDoseUnit(l_saDoseUnit);
			if(l_saDoseFrequency != null)
				product.setFrequency(l_saDoseFrequency);
			if(l_saIndication != null)
				product.setIndication(l_saIndication);
			if(patRouteOfAdmin != null)
				product.setRoutOfAdmin(patRouteOfAdmin);
			if(prodName != null)
				product.setProductName(prodName);
			if(hpRole != null)
				product.setHPRole(hpRole);
		}
		return product;
	}
	
	
	public Map<String,Products> fetchALLProduct() throws InterruptedException
	{
		
		Map<String,Products>getProductMapFromArgus = null;
		int l_nProductCount = 0;
		Products product = null;
		String l_sNewProductTabTableID = null , l_sNewProductTDID = null;
		
		
		l_sNewProductTabTableID=propMap.get("server.Product.NewProductTableName");
		l_sNewProductTDID=propMap.get("server.Product.NewProductTDID");
		
		getProductMapFromArgus=new HashMap<>();
	
		l_nProductCount = getTDCountByTableId(l_sNewProductTabTableID,	l_sNewProductTDID);
		
		for(int i = 1 ; i <= l_nProductCount-1 ; i++)
		{
			executeScriptByTitle(l_sNewProductTabTableID, l_sNewProductTDID+i);
			
			
			setWait(propMap.get("minWait"));
			
			//fetch product details by get text of argus tab
			product = fetchProductDetails();
			
			if(product != null)
			{
				//add product details into map
				getProductMapFromArgus.put("Product"+i, product);
				
			}
		}
		
		return getProductMapFromArgus;
	}
	
	
	public int getTotalNumberOfCaseResult()
	{
		String l_sText = null;
		//getElement("tr", "id", propMap.get("GetTotalRowCount.TRID"));
		
		l_sText = getTextByElement(getElement("span", "id", propMap.get("GetTotalRowCount.SPANID")));
		
		if(l_sText != null)
		{
			l_sText=l_sText.replaceAll("[()]", "");
		}
		return l_sText != null ? Integer.parseInt(l_sText) : 0;
			
	}
	
	
	public Map<String,Object> fetchEventDetails(Map<String,Object>mapOfEventFromGetText)throws InterruptedException{
		
		String descReported=null,descCoded=null,outCome=null;
		int eventCount=0;
		descReported=propMap.get("server.Event.DescReported");
		descCoded=propMap.get("server.Event.DescCoded");
		outCome=propMap.get("server.Event.Outcome");

		eventCount=getTDCountByTableId("table_nav_CF_EVT_id","td_CF_EVT_");
		for(int i=1;i<=eventCount-2;i++){
			
			setWait(propMap.get("minWait"));
			executeScriptByTitle( "table_nav_CF_EVT_id", "td_CF_EVT_"+i);
			mapOfEventFromGetText.put("DescriptionasReported"+i, getTextOfValueAttributeElementByXpath("name", descReported));
			mapOfEventFromGetText.put("DescriptiontobeCoded"+i, getTextOfValueAttributeElementByXpath("name", descCoded));
			mapOfEventFromGetText.put("OutcomeofEvent"+i, getTextOfValueAttributeElementByXpath("name", outCome));
		}
		
		mapOfEventFromGetText.put("Event Count", (eventCount-2));
		
		return mapOfEventFromGetText;
	}

	
	public void enterIntoCase(String p_sCaseId) throws InterruptedException
	{
		setWait(propMap.get("minWait"));
		
		clickLinkByText(p_sCaseId);
		
		setWait(propMap.get("minWait"));
	}
	
	
	public void redirectToCaseSearchPage()throws InterruptedException{
		
		String caseOptionsearch=null, searchRepType=null, searchPatCountry=null, searchInitRepDate= null;
		boolean checkDuplicateCasePage=false;
		
		searchRepType = propMap.get("Search.General.ReportType");
		searchPatCountry = propMap.get("Search.Patient.Country");
		searchInitRepDate = propMap.get("Search.InitRecpDate");
		caseOptionsearch = propMap.get("caseaction.dropdown.valueForSearch");
		
		executeJavaScript(caseOptionsearch);
		setWait(propMap.get("waitForSearchCase"));
			
		checkDuplicateCasePage = findMultipleElements(new String[] { searchRepType, searchInitRepDate, searchPatCountry });
		
		if (!checkDuplicateCasePage) {
			log.error("duplicate search case page not found");
			executeJavaScript(caseOptionsearch);
		}
			
	}
	
	
	public void nextResultPage()
	{
		String l_sNextPageButton = null;
		
		l_sNextPageButton = propMap.get("CreateCase.nextSearchResultPage");
		
		enterClickByXpathIfElementEnabled("INPUT", "name", l_sNextPageButton);
		
	}
	
	
	private int getDosesCount()
	{
		String l_sAttributeValue = null;
		String l_sDoseTableID = null , l_sDoseTableTDID = null , l_sTextOfLastSection = null;
		int l_nDoseCount = 0;
		
		
		l_sDoseTableTDID = propMap.get("server.Product.DoseCountTDID");
		l_sDoseTableID = propMap.get("server.Product.DoseCountTableID");
		
		l_sAttributeValue =	getElement("Table", "id", l_sDoseTableID).getAttribute("num_sections");
		
		l_nDoseCount = l_sAttributeValue != null ? Integer.parseInt(l_sAttributeValue) : 0;
		
		
		l_sTextOfLastSection =	getTextElementByXpath("td", "id", l_sDoseTableTDID + l_nDoseCount);
		
		if(l_sTextOfLastSection != null)
		{
			if(l_sTextOfLastSection.equals("(New)"))
			{
				l_nDoseCount = l_nDoseCount -1;
			}

		}
		
		return l_nDoseCount;
	}
	
	
	public void searchPage() throws InterruptedException{
		
		String caseOptionsearch = null ;
		caseOptionsearch = propMap.get("caseaction.dropdown.valueForSearch");
		
		setWait(propMap.get("minWait"));
		
		executeJavaScript(caseOptionsearch);
		
		setWait(propMap.get("waitForSearchCase"));
		
		
	}

	
	public boolean isHomePage()
	{
		return super.isPageCorrect(propMap.get("Argus_Home_Page"));
	}
	
	
	private void clickSearchButton() throws InterruptedException
	{
		enterClickByXpath("INPUT","name", propMap.get("Search.Button"));
		super.setWait(propMap.get("minWait"));
	}

	
	public void isProductTab(){
		
		String  l_sProductName = null , l_sPcmDose = null , l_sPcmFrequency = null , l_sProductTab = null;
		boolean l_bCheckProductPage = false;
		
		l_sProductName=propMap.get("server.Product.Name");
		l_sPcmDose = propMap.get("server.product.Dose");
		l_sPcmFrequency = propMap.get("server.product.DoseFrequency");
		l_sProductTab = propMap.get("server.ProductTab");

		l_bCheckProductPage=findMultipleElements(new String[]{ l_sProductName , l_sPcmDose , l_sPcmFrequency });
		
		if(!l_bCheckProductPage)
		{
			log.error("product feeding page not found,Redirecting to product tab");
			executeJavaScript(l_sProductTab);
		}
	}
	public boolean checkDuplicateByReportTypeAndProduct(Map<String,String> propMap,ParsedDataTable oDataTable,Map<String,Object>exstingDataFromArgusMap){
		boolean duplicateCase=false,prodForBooking=false;
		String reporterType=null,reportrTypeAdd=null,prodPath=null;
		Map<String,Map<String,Product>>allProductMap=null;
		Map<String,Product>getProductMapFromArgus=null;
		DataTable ProductExcelData=null;
		ProductUtils productUtils=null;
		try{
			productUtils=new ProductUtils();
			log.debug("wait to click general tab");
			prodPath = propMap.get("productCfgPath");
				
			//check page absents
			reporterType=(String)exstingDataFromArgusMap.get("Occupation");
			
			if(oDataTable.isColumnPresent("PrimaryReporter"))
				reportrTypeAdd = (String) oDataTable.getColumnValues("PrimaryReporter").get(0).displayValue().toString();

			if(reportrTypeAdd.equals("") || reportrTypeAdd.equals("Other Health Professional")){
				reportrTypeAdd="Other Health Care Professional";
			}
		 
			if(reporterType.equals(reportrTypeAdd)){
				exstingDataFromArgusMap.put("ReporterSame", true);
				//get product excel data for verifying
				ProductExcelData= productUtils.getDataFromProductExcel(prodPath);
				
				//get all product from DataTable 
				allProductMap=productUtils.PDFVerification(oDataTable,ProductExcelData,prodForBooking);
				//get existing Product details to check with new SD
				getProductMapFromArgus=(Map<String, Product>) exstingDataFromArgusMap.get("Products");
				
				//check existing product with new product list
				duplicateCase=checkDuplicateByProduct( propMap, getProductMapFromArgus, allProductMap);
			
			}else{
				exstingDataFromArgusMap.put("ReporterSame", false);
				duplicateCase=false;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return duplicateCase;
	}

	public boolean checkDuplicateByProduct(Map<String,String>propMap,Map<String,Product> allProductMap,Map<String,Map<String,Product>> newAllProductMap){
		boolean checkProductExistence=true;
		Product product=null,newProduct=null;
		Map<String,Product>newVerifiedProductMap=null,newMissingProductMap=null,mergedMap=null;
		int count=1;
		
		String newProductIndication=null,newProductRouteAdmin=null,toBreakLoop=null,argusProductDose=null;
		try{
			mergedMap=new HashMap<>();
			newVerifiedProductMap= newAllProductMap.get("verifiedProductMap");
			newMissingProductMap=newAllProductMap.get("missingProductMap");
			
			
			mergedMap=mergeMap(newVerifiedProductMap, newMissingProductMap);
			
			for(int j=1;j<=mergedMap.size();j++){
				
				if ((mergedMap.get("Product"+j)) != null)
					newProduct=mergedMap.get("Product"+j);
				checkProductExistence=true;
				
				newProductIndication=newProduct.getIndication();
				newProductRouteAdmin=newProduct.getRoutOfAdmin();
				
				if(newProductIndication.equals("")){
					newProductIndication="product used for unknown indication";
				}
				
				if(newProductRouteAdmin.equals("")){
					newProductRouteAdmin="unknown";
				}
				for(int i=1;i<=allProductMap.size();i++){
				
					//check wedr product is same or different
					if ((allProductMap.get("Product"+i)) != null)
						product=allProductMap.get("Product"+i);
					
					argusProductDose=product.getDose();
					if(!argusProductDose.contains(".") && !argusProductDose.equals("")){
						argusProductDose=argusProductDose+".0";
					}
					if(newProduct.getProductName().equals(product.getProductName())){
						toBreakLoop="break";
						
						if(!(newProduct.getDosageForm().equalsIgnoreCase(product.getDosageForm()))){
							
							//if new product property is empty or unknown then don't increment
							/*if(!newProduct.getDosageForm().equals("") && !newProduct.getDosageForm().equalsIgnoreCase("uknown")){
								checkProductExistence=false;
							}*/
						
						} if(!(newProduct.getDose().equalsIgnoreCase(argusProductDose))){
							
							if(!newProduct.getDose().equals("") && !newProduct.getDose().equalsIgnoreCase("uknown")){
								checkProductExistence=false;
							}
							
						} if(!(newProductIndication.equalsIgnoreCase(product.getIndication()))){
							
							checkProductExistence=false;
							
						} if(!(newProduct.getDoseUnit().equalsIgnoreCase(product.getDoseUnit()))){
							
							if(!newProduct.getDoseUnit().equals("") && !newProduct.getDoseUnit().equalsIgnoreCase("uknown")){
								checkProductExistence=false;
							}
							
						}/* if(!(newProduct.getFrequency().equalsIgnoreCase(product.getFrequency()))){
							
							if(!newProduct.getFrequency().equals("") && !newProduct.getFrequency().equalsIgnoreCase("uknown")){
								checkProductExistence=false;
							}
							
						}*/ if(!(newProduct.getHPRole().equalsIgnoreCase(product.getHPRole()))){
							
							if(!newProduct.getHPRole().equals("") && !newProduct.getHPRole().equalsIgnoreCase("uknown")){
								checkProductExistence=false;
							}
							
						} if(!(newProductRouteAdmin.equalsIgnoreCase(product.getRoutOfAdmin()))){
							
							if(!newProduct.getRoutOfAdmin().equalsIgnoreCase("uknown")){
								checkProductExistence=false;
							}
							
						}
						
					//if new product and old product name is not same then add increment 	
					}else{
						checkProductExistence=false;
					}
				
					if(toBreakLoop!=null && toBreakLoop.equals("break")){
						toBreakLoop="";
						break;
					}
				}
				if(!checkProductExistence){
					count++;
				}
				
			}//for-loop for comp. btwn NewMissing Product an allExisting Product 
				
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(count==mergedMap.size()){
			return true;
		}else{
			return false;
		}
		
	}
	
	public Product fetchProductDetails(Map<String,String> propMap){
		String dosageForm=null,dose=null,doseUnit=null,doseFrequency=null,indication=null,patRouteOfAdmin=null
				,prodName=null,hpRole=null;
		Product product=null;
		
		try {
			
			product=new Product();
			prodName=getTextOfValueAttributeElementByXpath("name", propMap.get("server.Product.Name"));
			dosageForm=getTextOfValueAttributeElementByXpath("name", propMap.get("server.Product.Formulation"));
			dose=getTextOfValueAttributeElementByXpath("name", propMap.get("server.product.Dose"));
			doseUnit=getTextOfValueAttributeElementByXpath("name", propMap.get("server.product.DoseUnit"));
			doseFrequency=getTextOfValueAttributeElementByXpath("name", propMap.get("server.product.DoseFrequency"));
			patRouteOfAdmin=getTextOfValueAttributeElementByXpath("name", propMap.get("server.Product.PatRouteOfAdmin"));
			indication=getTextOfValueAttributeElementByXpath("name", propMap.get("server.Product.Indication"));
			hpRole=isRadioButtonCheckedAndGetValue( propMap.get("server.Drug.DrugType"));
			
			if(hpRole.equals("1")){
				hpRole="Suspect";
			}else{
				hpRole="Concomitant";
			}
			
			if(dosageForm != null)
				product.setDosageForm(dosageForm);
			if(dose != null)
				product.setDose(dose);
			if(doseUnit != null)
				product.setDoseUnit(doseUnit);
			if(doseFrequency != null)
				product.setFrequency(doseFrequency);
			if(indication != null)
				product.setIndication(indication);
			if(patRouteOfAdmin != null)
				product.setRoutOfAdmin(patRouteOfAdmin);
			if(prodName != null)
				product.setProductName(prodName);
			if(hpRole != null)
				product.setHPRole(hpRole);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return product;
	}

	public Map<String,Object> fetchReporterDetails(Map<String,String> propMap,Map<String,Object>mapOfReporterFromGetText,boolean HCP){
		String centralRcptDate=null,intRecieptDate=null,repoCountry=null,occupation=null
				,institution=null,reportertype=null,reportType=null,scriptAttribute=null,newHCPTableName=null;
		
		try {
			
			centralRcptDate= propMap.get("server.centralRecptDate");
			intRecieptDate= propMap.get("server.DateOfAwareness");
			repoCountry = propMap.get("server.Reporter.Country");
			institution=propMap.get("server.Reporter.Institution");
			reportertype=propMap.get("server.Reporter.Type");
			reportType=propMap.get("server.Report.Type");
			occupation=propMap.get("server.Reporter.Occupation");
			
			scriptAttribute = propMap.get("server.Script.Attribute");
			newHCPTableName=propMap.get("server.Reporter.NewHCPTableName");
			
			if(!HCP){
				mapOfReporterFromGetText.put("Occupation", getTextOfValueAttributeElementByXpath("name", occupation));
				mapOfReporterFromGetText.put("CRD", getTextOfValueAttributeElementByXpath("name", centralRcptDate));
				mapOfReporterFromGetText.put("Date of Awareness", getTextOfValueAttributeElementByXpath("name", intRecieptDate));
				mapOfReporterFromGetText.put("Country", getTextOfValueAttributeElementByXpath("name", repoCountry));
				mapOfReporterFromGetText.put("PrimaryReporter", getTextOfValueAttributeElementByXpath("name", reportertype));
				mapOfReporterFromGetText.put("ReportType", getTextOfValueAttributeElementByXpath("name", reportType));
				mapOfReporterFromGetText.put("Institution", getTextOfValueAttributeElementByXpath("name", institution));
			}else{
				dynamicExecuteJavaScriptByText(newHCPTableName,scriptAttribute,"(Consumer)");
				mapOfReporterFromGetText.put("HCP:Occupation", getTextOfValueAttributeElementByXpath("name", occupation));
				mapOfReporterFromGetText.put("HCP:Country", getTextOfValueAttributeElementByXpath("name", repoCountry));
				mapOfReporterFromGetText.put("HCP:PrimaryReporter", getTextOfValueAttributeElementByXpath("name", reportertype));
				mapOfReporterFromGetText.put("HCP:Institution", getTextOfValueAttributeElementByXpath("name", institution));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapOfReporterFromGetText;
	}
	
	public Map<String,Object> fetchPatientDetails(Map<String,String> propMap,Map<String,Object>mapOfPatientFromGetText){
		String patInitials=null,patCountry=null,patAge=null,patAgeUnits=null
				,patHeight=null,patWeight=null,patGender=null;
		
		try {
			
			patInitials=propMap.get("server.Patient.Initials");
			patCountry=propMap.get("server.Patient.Country");
			patAge=propMap.get("server.Patient.Age");
			patAgeUnits=propMap.get("server.Patient.AgeUnits");
			patHeight=propMap.get("server.Patient.height");
			patWeight=propMap.get("server.Patient.weight");
			patGender=propMap.get("server.Patient.Gender");
			
			mapOfPatientFromGetText.put("Patient Details.Initial", getTextOfValueAttributeElementByXpath("name", patInitials));
			mapOfPatientFromGetText.put("Country", getTextOfValueAttributeElementByXpath("name", patCountry));
			mapOfPatientFromGetText.put("Patient Details.Age", getTextOfValueAttributeElementByXpath("name", patAge));
			mapOfPatientFromGetText.put("Patient Details.Age Units", getTextOfValueAttributeElementByXpath("name", patAgeUnits));
			mapOfPatientFromGetText.put("Patient Details.Height", getTextOfValueAttributeElementByXpath("name", patHeight));
			mapOfPatientFromGetText.put("Patient Details.Weight", getTextOfValueAttributeElementByXpath("name", patWeight));
			mapOfPatientFromGetText.put("Patient Details.Gender", getTextOfValueAttributeElementByXpath("name", patGender));
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapOfPatientFromGetText;
	}

	public Map<String,Object> fetchEventDetails(Map<String,String> propMap,Map<String,Object>mapOfEventFromGetText){
		String descReported=null,descCoded=null,outCome=null;
		int eventCount=0;
		
		try {
			
			descReported=propMap.get("server.Event.DescReported");
			descCoded=propMap.get("server.Event.DescCoded");
			outCome=propMap.get("server.Event.Outcome");

			eventCount=getTDCountByTableId("table_nav_CF_EVT_id","td_CF_EVT_");
			for(int i=1;i<=eventCount-2;i++){
				Thread.sleep(2000);
				executeScriptByTitle( "table_nav_CF_EVT_id", "td_CF_EVT_"+i);
				Thread.sleep(3000);
				mapOfEventFromGetText.put("DescriptionasReported"+i, getTextOfValueAttributeElementByXpath("name", descReported));
				mapOfEventFromGetText.put("DescriptiontobeCoded"+i, getTextOfValueAttributeElementByXpath("name", descCoded));
				mapOfEventFromGetText.put("OutcomeofEvent"+i, getTextOfValueAttributeElementByXpath("name", outCome));
			}
			
			mapOfEventFromGetText.put("Event Count", (eventCount-2));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapOfEventFromGetText;
	}

	public Map<String,Object> getTextOfAllTabs(Map<String,String> propMap){
		boolean checkReporterHcpPage = false, checkProductPage=false,checkEventPage=false,checkPatientFeedingPage=false;
		String repoPrefix=null,initRecpDate=null,centralrecpDate=null,generalTab=null,descReported=null,descToBeCoded=null
				,productTab=null, productName=null, pcmDose=null, pcmFrequency=null,eventTab=null,patientTab=null
				,patFirstName=null,patLastName=null,patAddress=null,newProductTabTable=null,newProductTDID=null;
		int productCount=0;
		Map<String,Product>getProductMapFromArgus=null;
		Product product=null;
		Map<String,Object>fetchAllDataByGetText=null;
		
		try{
			
			fetchAllDataByGetText=new HashMap<>();
			productTab = propMap.get("server.ProductTab");
			repoPrefix = propMap.get("server.Reporter.prefix");
			initRecpDate= propMap.get("server.DateOfAwareness");
			centralrecpDate=propMap.get("server.centralRecptDate");
			generalTab = propMap.get("server.GeneralTab");
			productName=propMap.get("server.Product.Name");
			pcmDose = propMap.get("server.product.Dose");
			pcmFrequency = propMap.get("server.product.DoseFrequency");
			descReported=propMap.get("server.Event.DescReported");
			descToBeCoded=propMap.get("server.Event.DescCoded");
			eventTab = propMap.get("server.EventTab");
			patientTab = propMap.get("server.PatientTab");
			patFirstName = propMap.get("server.Patient.FirstName");
			patLastName = propMap.get("server.Patient.LastName");
			patAddress = propMap.get("server.Patient.Address");
			newProductTabTable=propMap.get("server.Product.NewProductTableName");
			newProductTDID=propMap.get("server.Product.NewProductTDID");
			
			log.debug("wait to click general tab");
			executeJavaScript( generalTab);
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			//check page absents
			checkReporterHcpPage=findMultipleElements( new String[]{repoPrefix,initRecpDate,centralrecpDate});
			if(!checkReporterHcpPage)
			{
				log.debug("General Tab page not found,Redirecting to general tab");
				executeJavaScript( generalTab);
			}
			fetchAllDataByGetText=fetchReporterDetails( propMap, fetchAllDataByGetText, false);
			fetchAllDataByGetText=fetchReporterDetails( propMap, fetchAllDataByGetText, true);
			
			Thread.sleep(2000);
			executeJavaScript( patientTab);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			checkPatientFeedingPage=findMultipleElements( new String[]{patFirstName,patLastName,patAddress});
			if(!checkPatientFeedingPage)
			{
				log.debug("patient feeding page not found,Redirecting to patient tab");
				executeJavaScript( patientTab);
			}
			
			fetchAllDataByGetText=fetchPatientDetails( propMap, fetchAllDataByGetText);
			
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			executeJavaScript( productTab);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			checkProductPage=findMultipleElements( new String[]{productName,pcmDose,pcmFrequency});
			if(!checkProductPage){
				log.debug("product feeding page not found,Redirecting to product tab");
				executeJavaScript( productTab);
			}
			log.debug("product data feeding started");
			
			getProductMapFromArgus=new HashMap<>();
			//get total count of product
			productCount=getTDCountByTableId(	newProductTabTable,	newProductTDID);
			for(int i=1;i<=productCount-2;i++){
				executeScriptByTitle( newProductTabTable, newProductTDID+i);
				Thread.sleep(3000);
				//fetch product details by get text of argus tab
				product=fetchProductDetails( propMap);
				//add product details into map
				getProductMapFromArgus.put("Product"+i, product);
			}
			//put all products
			fetchAllDataByGetText.put("Products", getProductMapFromArgus);
			Thread.sleep(Integer.parseInt(propMap.get("waitForEventTab")));
			executeJavaScript( eventTab);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			checkEventPage=findMultipleElements( new String[]{descReported,descToBeCoded});
			if(!checkEventPage){
				log.debug("Event feeding page not found,Redirecting to Event tab");
				executeJavaScript( eventTab);
			}
			//get all events
			fetchAllDataByGetText=fetchEventDetails( propMap, fetchAllDataByGetText);
				
		}catch(Exception e){
			e.printStackTrace();
		}
		return fetchAllDataByGetText;
	}
	public  void feedAdditionInfo( Map<String,String> propMap,ParsedDataTable oDataTable)throws SciException{
		String sDClass=null,sDKeyword=null,sDNotes=null,iNTClass=null,iNTKeyword=null,iNTNotes=null,caseId=null,text=null,type=null,sdPath=null,inTakeMailPath=null
				,window=null,clickAttachment=null,MAHNum=null,type1=null,refferNumber=null,note2=null;
		String[] splitted=null;
		
		try{
			
			sDClass=propMap.get("server.AddInfo.SDClass");
			sDKeyword=propMap.get("server.AddInfo.SDKeyword");
			sDNotes=propMap.get("server.AddInfo.SDNotes");
			iNTClass=propMap.get("server.AddInfo.INTClass");
			iNTKeyword=propMap.get("server.AddInfo.INTKeyword");
			iNTNotes=propMap.get("server.AddInfo.INTNotes");
			caseId=propMap.get("Search.CaseID");
			type=propMap.get("server.AddInfo.Type");
			sdPath = propMap.get("sdpath");
			inTakeMailPath = propMap.get("intakeMailPath");
			clickAttachment=propMap.get("server.AddInfo.ClickAttach");
			type1=propMap.get("server.Addinfo.TypeId1");
			refferNumber=propMap.get("server.Addinfo.RefferNumber");
			note2=propMap.get("server.Addinfo.Notes2");
			
			window=getWindowHandle();
			if(oDataTable.isColumnPresent("RegulatoryRef"))
				MAHNum =  (String) oDataTable.getColumnValues("RegulatoryRef").get(0).displayValue().toString();
			
			findElementByXpathAndSetContent("name", sDClass, "Source Document");
			enterClickByXpath("INPUT","name",clickAttachment);
			uploadFile( propMap, window, sdPath);
			text=getTextFromXpath( caseId);
			splitted=text.split("-");
			if(splitted[1].contains("\"")){
				splitted=splitted[1].split("\"");
				splitted[1]=splitted[0];
			}
			findElementByXpathAndSetContent("name", sDKeyword, "SD");
			findElementByAbsoluteXpathAndSetContent( sDNotes, "A01_"+splitted[1].trim()+"_SD");
			findElementByXpathAndSetContent("name", iNTClass, "Source Document");
			enterClickByXpath("INPUT","name",clickAttachment);
			uploadFile( propMap, window, inTakeMailPath);
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			findElementByXpathAndSetContent("name", iNTKeyword, "Intake mail");
			findElementByAbsoluteXpathAndSetContent( iNTNotes, "A02_"+splitted[1].trim()+"_Intake Mail");
			findElementByXpathAndSetContent("name", type, "E2B Report Duplicate");
			findElementByXpathAndSetContent("name", propMap.get("server.Addinfo.Notes1"), "Canada Vigilance");
			findElementByXpathAndSetContent("name", type1, "Regulatory Ref");
			findElementByXpathAndSetContent("name", refferNumber, MAHNum);
			findElementByXpathAndSetContent("name", note2, "Market Authorization Holder AER Number");
			
		}catch(Exception e){
			log.error("Error while feeding edditional info tab < "+e.getMessage()+" >");
			e.printStackTrace();
		}
	}

	public  void uploadFile(Map<String,String> propMap,String p_sWindow,String p_sFilePathToUpload)throws SciException{
		
		String uploadFile=null;
		Robot robot=null;
		StringSelection stringSelection=null;
		Clipboard clipboard =null;
		
		try{
			
			robot=new Robot();
			uploadFile=propMap.get("CreateCase.uploadFile");
		
			Thread.sleep(2000);
			switchWindowHandler(p_sWindow);
			doubleClickOnElementById( uploadFile);
			getAlert();
			Thread.sleep(2000);
			File file=new File(p_sFilePathToUpload);
			String text =file.getAbsolutePath();
			stringSelection = new StringSelection(text);
			clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(stringSelection, stringSelection);
			
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(3000);
			enterClickByXpath("INPUT","name",propMap.get("server.AddInfo.uploadOkButton"));
			Thread.sleep(3000);
			Set<String> windows=getSetOfWindows(2);
				for(String wind:windows){
					if(wind.equals(p_sWindow))
						switchWindow(wind);
				}
			switchParentToChildFrame(propMap.get("server.loginform.parentframe"), propMap.get("server.loginform.childframe"));
			
		}catch(Exception e){
			log.error("Error while uploading file < "+e.getMessage()+" >");
		}
	}

	public  void selectProduct(Map<String,String>propMap,ParsedDataTable oDataTable,String p_sParentWindow,int p_nProductCount,Map<String,Product>productMap)throws SciException{
		String productSelectButton=null,searchProductName=null,p_sProductTableName=null;
		
		Product product=null;
		
		try{
			
			searchProductName=propMap.get("server.product.searchProductName");
			productSelectButton=propMap.get("CreateCase.SelectButton");
			
			if ((productMap.get("Product"+p_nProductCount)) != null)
				product=productMap.get("Product"+p_nProductCount);
		
			p_sProductTableName=propMap.get("server.product.SearchId");
			
			getSetOfWindows( 2);
			switchWindowHandler(p_sParentWindow);
			findElementByXpathAndSetContent("name", searchProductName, product.getProductName());
			enterClickByXpath("INPUT","name",propMap.get("server.Product.searchButton"));
			clickOnTextByTableId( p_sProductTableName, product.getProductName());
			clickOnTextByTableIdSpanText(propMap.get("server.Product.DivAttrForSelectDosageForm") , product.getDosageForm());		
			enterClickByXpath("INPUT","name",productSelectButton);
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			switchWindow(p_sParentWindow);
			switchParentToChildFrame(propMap.get("server.loginform.parentframe"), propMap.get("server.loginform.childframe"));
		
		}catch(Exception e){
			log.error("Error while selecting product < "+e.getMessage()+" >");
		}
	}
	
	public  void encodeProduct(Map<String,String>propMap,ParsedDataTable oDataTable,String p_sParentWindow,int p_nProductCount,Map<String,Product>productMap)throws SciException{
		String productSelectButton=null,searchDrugName=null;
		
		Product product=null;
		
		try{
			
			searchDrugName=propMap.get("server.Drug.SearchDrugName");
			productSelectButton=propMap.get("CreateCase.SelectButton");
			
			if ((productMap.get("Product"+p_nProductCount)) != null)
				product=productMap.get("Product"+p_nProductCount);
		
			getSetOfWindows( 2);
			switchWindowHandler(p_sParentWindow);
			findElementByXpathAndSetContent("name", searchDrugName, product.getProductName());
			enterClickByXpath("INPUT","name",propMap.get("server.Product.searchButton"));
			enterClickByXpath("INPUT","name",productSelectButton);
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			switchWindow(p_sParentWindow);
			switchParentToChildFrame(propMap.get("server.loginform.parentframe"), propMap.get("server.loginform.childframe"));
		
		}catch(Exception e){
			log.error("Error while encoding product < "+e.getMessage()+" >");
		}
	}

	public  void feedProductData( Map<String,String> propMap,ParsedDataTable oDataTable,int p_nProductCount,Map<String, Product> productMap)throws SciException{
		
		String 	pcmDose=null,pcmUnit=null,pcmFrequency=null,pcmDoseAdd=null,pcmUnitAdd=null,pcmFrequencyAdd=null,prodIndication=null,prodIndicationAdd=null,
				prodIndicationAddButton=null,batchLot=null,batchLotAdd=null,patRouteOfAdmin=null,hpRole=null;
		String actionTaken=null,patRouteOfAdminAdd=null,doseDescription=null;
		Product product=null;
		
		try{
			
			doseDescription=propMap.get("server.Product.doseDescription");
			actionTaken=propMap.get("server.Product.ActionTaken");
		
			if ((productMap.get("Product"+p_nProductCount)) != null)
				product=productMap.get("Product"+p_nProductCount);
		
			patRouteOfAdmin= propMap.get("server.Product.PatRouteOfAdmin");
			patRouteOfAdminAdd = product.getRoutOfAdmin();
			if(patRouteOfAdminAdd.equals(""))
				patRouteOfAdminAdd="Unknown";
			
			pcmDose = propMap.get("server.product.Dose");
			pcmDoseAdd =product.getDose();
			
			pcmFrequency = propMap.get("server.product.DoseFrequency");
			pcmFrequencyAdd =product.getFrequency();
			
			pcmUnit = propMap.get("server.product.DoseUnit");
			pcmUnitAdd = product.getDoseUnit();
		
			prodIndication = propMap.get("server.Product.Indication");
			prodIndicationAdd=product.getIndication();
			
			hpRole=product.getHPRole();
			prodIndicationAddButton= propMap.get("server.Product.AddButtonForIndication");
			
			batchLot= propMap.get("server.product.BatchLot");
			if(oDataTable.isColumnPresent("Lot/Batch Numbers"))
				batchLotAdd=  (String) oDataTable.getColumnValues("Lot/Batch Numbers").get(0).displayValue().toString();
			
			findElementByXpathAndSetContent("name", patRouteOfAdmin, patRouteOfAdminAdd);
			enterClickByXpath("INPUT","name",prodIndicationAddButton);
			if(prodIndicationAdd.equals("")){
				findElementByXpathAndSetContent("name", prodIndication, "product used for unknown indication");
			}else if(prodIndicationAdd.equals("Product used\rfor unknown\rindication")){
				findElementByXpathAndSetContent("name", prodIndication, "product used for unknown indication");
			}else{
				//for demo
				//findElementByXpathAndSetContent("name", prodIndication, "product used for unknown indication");
				//enable this line for actual indication
				findElementByXpathAndSetContent("name", prodIndication, prodIndicationAdd);
			}
			findElementByXpathAndSetContent("name", actionTaken, "Unknown");
			if((!pcmDoseAdd.equals(""))||(!pcmUnitAdd.equals(""))||(!pcmFrequencyAdd.equals(""))){
				findElementByXpathAndSetContent("name", doseDescription, pcmDoseAdd+" "+pcmUnitAdd+" "+pcmFrequencyAdd);
			}
			findElementByXpathAndSetContent("name", pcmDose, pcmDoseAdd);
			findElementByXpathAndSetContent("name", pcmUnit, pcmUnitAdd);
			findElementByXpathAndSetContent("name", pcmFrequency, pcmFrequencyAdd);
			findElementByXpathAndSetContent("name", batchLot, batchLotAdd);
			
			if(hpRole.equalsIgnoreCase("Suspect")){
				clickCheckboxByValueAttribute( propMap.get("server.Drug.DrugType"), "1");
			}else if(hpRole.equalsIgnoreCase("Concomitant")){
				clickCheckboxByValueAttribute( propMap.get("server.Drug.DrugType"), "2");
			}else{
				clickCheckboxByValueAttribute( propMap.get("server.Drug.DrugType"), "3");
			}
			
			
		}catch(Exception e)
		{
			log.error("Error while feeding Product tab < "+e.getMessage()+" >");
			
		}
	}
	
	public  void feedPatientData( Map<String,String> propMap,ParsedDataTable oDataTable)throws SciException{
		
		String 	patGender=null,patGenderAdd=null,patAge=null,patAgeAdd=null,patAgeUnits=null,patAgeUnitsAdd=null
				,patHeight=null,patHeightAdd=null,patWeight=null,patWeightAdd=null,patInitials=null,patCountry=null
				,patCountryAdd=null;
		
		try{
			
			
			patInitials=propMap.get("server.Patient.Initials");
			patCountry=propMap.get("server.Patient.Country");
			if(oDataTable.isColumnPresent("Country"))
				patCountryAdd = (String) oDataTable.getColumnValues("Country").get(0).displayValue().toString();
			
			patAge=propMap.get("server.Patient.Age");
			if(oDataTable.isColumnPresent("Patient Details.Age"))
				patAgeAdd = (String) oDataTable.getColumnValues("Patient Details.Age").get(0).displayValue().toString();
			
			patAgeUnits=propMap.get("server.Patient.AgeUnits");
			if(oDataTable.isColumnPresent("Patient Details.Age Units"))
				patAgeUnitsAdd = (String) oDataTable.getColumnValues("Patient Details.Age Units").get(0).displayValue().toString();
			
			patHeight=propMap.get("server.Patient.height");
			if(oDataTable.isColumnPresent("Patient Details.Height"))
				patHeightAdd = (String) oDataTable.getColumnValues("Patient Details.Height").get(0).displayValue().toString();
			
			patWeight=propMap.get("server.Patient.weight");
			if(oDataTable.isColumnPresent("Patient Details.Weight"))
				patWeightAdd = (String) oDataTable.getColumnValues("Patient Details.Weight").get(0).displayValue().toString();
			
					patGender=propMap.get("server.Patient.Gender");
			if( oDataTable.isColumnPresent("Patient Details.Gender"))
				patGenderAdd= (String) oDataTable.getColumnValues("Patient Details.Gender").get(0).displayValue().toString();
				
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			findElementByXpathAndSetContent("name", patInitials, "Unknown");
			findElementByXpathAndSetContent("name", patCountry, patCountryAdd);
			findElementByXpathAndSetContent("name", patAge, patAgeAdd);
			findElementByXpathAndSetContent("name", patAgeUnits, patAgeUnitsAdd);
			findElementByXpathAndSetContent("name", patGender, patGenderAdd.trim());
			findElementByXpathAndSetContent("name", patHeight,patHeightAdd);
			findElementByXpathAndSetContent("name", patWeight, patWeightAdd);
			
		}catch(Exception e)
		{
			log.error("Error in Feeding data for patient < "+e.getMessage()+" >");
		}	
	}

	public  void feedReporterHCPData( Map<String,String> propMap,ParsedDataTable oDataTable,boolean hcp)throws SciException{
		
		String  intRecieptDate=null,intRecieptDateAdd=null,centralRcptDate=null;
		String 	repoCountry = null, repoCountryAdd = null,repoSentByRegAuth=null;
		String 	hcpDropDown=null,newHCPTableName=null,institution=null;
		String scriptAttribute=null,newButtonName=null;
		String repoTypeAdd=null,reptype=null,protectedConfidentially=null,dateTobeParsed=null;
		
		String[] splittedDate=null;
		try{
			
			repoSentByRegAuth= propMap.get("server.Reporter.RepSentByregAuth");
			centralRcptDate= propMap.get("server.centralRecptDate");
			intRecieptDate= propMap.get("server.DateOfAwareness");
			repoCountry = propMap.get("server.Reporter.Country");
			scriptAttribute = propMap.get("server.Script.Attribute");
			newButtonName= propMap.get("server.Button.Name");
			newHCPTableName=propMap.get("server.Reporter.NewHCPTableName");
			hcpDropDown= propMap.get("server.Reporter.HCP");
			institution=propMap.get("server.Reporter.Institution");
			reptype=propMap.get("server.Reporter.Type");
			protectedConfidentially=propMap.get("server.Reporter.ProtectedConfidentially");
			if(oDataTable.isColumnPresent("Date of Awareness"))
				intRecieptDateAdd = (String) oDataTable.getColumnValues("Date of Awareness").get(0).displayValue().toString();
			
			if(oDataTable.isColumnPresent("Country"))
				repoCountryAdd = (String) oDataTable.getColumnValues("Country").get(0).displayValue().toString();
			
			if(oDataTable.isColumnPresent("PrimaryReporter"))
				repoTypeAdd = (String) oDataTable.getColumnValues("PrimaryReporter").get(0).displayValue().toString();
			
			if(repoTypeAdd.equals("Other Health Professional")){
				repoTypeAdd="Other Health Care Professional";
			}else if(repoTypeAdd.equals("")){
				
			}
			if(hcp){
					dynamicExecuteJavaScriptByText(newHCPTableName,scriptAttribute,newButtonName);
					
					selectDropdownValueByIndex( hcpDropDown, 2);
					findElementByXpathAndSetContent("name", repoCountry, repoCountryAdd);
					deSelectElementWithSpaceKey( protectedConfidentially);	
					findElementByXpathAndSetContent("name", institution, "Health Canada");
					findElementByXpathAndSetContent("name",	propMap.get("server.Reporter.Occupation"), "Consumer");
					findElementByXpathAndSetContent("name", reptype, "Consumer");
			}else{
					splittedDate=intRecieptDateAdd.split(" ");
					dateTobeParsed=splittedDate[0];
					findElementByXpathAndSetContent( "name", intRecieptDate, dateTobeParsed);
					findElementByXpathAndSetContent( "name", centralRcptDate, dateTobeParsed);
					selectElementWithSpaceKey( propMap.get("Server.Reporter.FollowUp"));
					if(!(oDataTable.isColumnPresent("HCP Details"))){
						selectDropdownValueByIndex( hcpDropDown, 1);	
					}
					selectDropdownValueByIndex( repoSentByRegAuth, 1);
					deSelectElementWithSpaceKey( protectedConfidentially);	
					if(!repoTypeAdd.equals("")){
						selectElementWithSpaceKey( propMap.get("server.Reporter.PrimaryReport"));	
					}
					findElementByXpathAndSetContent("name",	propMap.get("server.Reporter.Occupation"), repoTypeAdd);
					findElementByXpathAndSetContent("name", repoCountry, repoCountryAdd);
					findElementByXpathAndSetContent("name", reptype, repoTypeAdd);
				}
		}catch(Exception e)
		{
			log.error("Error in feeding data for reporter or hcp < "+e.getMessage()+" >");
			e.printStackTrace();
		}
		
	}

	public  void feedEventData( Map<String,String> propMap,ParsedDataTable oDataTable,int p_nEventCount)throws SciException{
		String 	descReported=null,descReportedAdd=null,descCoded=null,descCodedAdd=null,outCome=null
				,outComeAdd=null,reactionDuration=null,rectionDurationAdd=null;
		
		try{
			
			descReported=propMap.get("server.Event.DescReported");
			descCoded=propMap.get("server.Event.DescCoded");
			outCome=propMap.get("server.Event.Outcome");
			rectionDurationAdd=propMap.get("server.Activities.Duration");
			
			if(oDataTable.isColumnPresent("DescriptionasReported"+p_nEventCount))
				descReportedAdd = (String) oDataTable.getColumnValues("DescriptionasReported"+p_nEventCount).get(0).displayValue().toString();
			if(oDataTable.isColumnPresent("DescriptiontobeCoded"+p_nEventCount))
				descCodedAdd = (String) oDataTable.getColumnValues("DescriptiontobeCoded"+p_nEventCount).get(0).displayValue().toString();
			if(oDataTable.isColumnPresent("OutcomeofEvent"))
				outComeAdd = (String) oDataTable.getColumnValues("OutcomeofEvent").get(0).displayValue().toString();
			if(oDataTable.isColumnPresent("Reaction Duration"+p_nEventCount))
				reactionDuration = (String) oDataTable.getColumnValues("Reaction Duration"+p_nEventCount).get(0).displayValue().toString();
			
			
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			findElementByXpathAndSetContent("name",descReported, descReportedAdd);
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			findElementByXpathAndSetContent("name",descCoded, descCodedAdd);
			findElementByXpathAndSetContent("name", outCome, outComeAdd);
			findElementByXpathAndSetContent("name", rectionDurationAdd, reactionDuration);
			checkSeriousCriteria( propMap, oDataTable);
		
		}catch(Exception e)
		{
			log.error("Error in feeding data for event < "+e.getStackTrace()+" >");
			
		}
		
	}
	
	public  void checkSeriousCriteria( Map<String,String> propMap,ParsedDataTable oDataTable)throws SciException{
		String 	death = null,congenital = null, disable = null, medicallySignificant = null,
				hospitalize = null, threatening = null,nonSerious = null,hospitalizeEvent=null
				,hospitOk=null,descReportedAdd=null,deathEventAddButton=null,window=null;
		int eventCount=0,idOfDeathEventCount=0;
		
		try{
			
			deathEventAddButton=propMap.get("CreateCase.Death.DescAdd");
			death = propMap.get("CreateCase.Death");
			congenital = propMap.get("CreateCase.Congenital");
			disable = propMap.get("CreateCase.Disable");
			medicallySignificant = propMap.get("CreateCase.MedicallySignificant");
			hospitalize = propMap.get("CreateCase.Hospitalize");
			threatening = propMap.get("CreateCase.Threatening");
			nonSerious = propMap.get("CreateCase.NonSerious");
	
			hospitalize = propMap.get("CreateCase.Hospitalize");
			nonSerious = propMap.get("CreateCase.NonSerious");
			hospitalizeEvent=propMap.get("Hospitalization.Event");
			hospitOk=propMap.get("Hospitalization.OK");
			deathEventAddButton=propMap.get("CreateCase.Death.DescAdd");
			window = getWindowHandle();
	
			if(oDataTable.isColumnPresent("Event Count"))
				eventCount = (int) oDataTable.getColumnValues("Event Count").get(0).displayValue();
			
			if (oDataTable.getColumnValues("SeriousnessCriteriaDetails").get(0).equals("Yes")) {
				
				if (oDataTable.getColumnValues("Death").get(0).equals("Yes")) {
					
					clickElementWithSpaceKeyByValueAttribute( death, "1");
					for(int i=1;i<=eventCount;i++){
						if(oDataTable.isColumnPresent("DescriptionasReported"+i))
							descReportedAdd = (String) oDataTable.getColumnValues("DescriptionasReported"+i).get(0).displayValue().toString();
					
						findElementByXpathAndSetContent("id","CSDD_"+idOfDeathEventCount+"_cause_reptd_id", descReportedAdd);
						if(i>=2&&i!=eventCount){
							enterClickByXpath("INPUT","name",deathEventAddButton);
						}
						idOfDeathEventCount++;
					}
				}
				if (oDataTable.getColumnValues("Congenital Anomaly").get(0).equals("Yes")) {
					clickElementWithSpaceKeyByValueAttribute( congenital, "1");
				}
				if (oDataTable.getColumnValues("Disability").get(0).equals("Yes")) {
					clickElementWithSpaceKeyByValueAttribute( disable, "1");
				}
				if (oDataTable.getColumnValues("Other Medically Important Conditions").get(0).equals("Yes")) {
					clickElementWithSpaceKeyByValueAttribute( medicallySignificant, "1");
				}
				if (oDataTable.getColumnValues("Life Threatening").get(0).equals("Yes")) {
					clickElementWithSpaceKeyByValueAttribute( threatening, "1");
				}
				if (oDataTable.getColumnValues("Hospitalization").get(0).equals("Yes")) {
					clickElementWithSpaceKeyByValueAttribute( hospitalize, "1");
					Thread.sleep(2000);
					switchWindowHandler( window);
					Thread.sleep(2000);
					clickElementWithSpaceKeyByValueAttribute( hospitalizeEvent, "1");
					Thread.sleep(2000);
					enterClickByXpath("INPUT","name",hospitOk);
					Thread.sleep(2000);
					Set<String> windows = getSetOfWindows(2);
					for (String wind : windows) {
						if (wind.equals(window))
							switchWindow(wind);
					}
					switchParentToChildFrame( propMap.get("server.loginform.parentframe"),
							propMap.get("server.loginform.childframe"));

				}
				
			} else {
				clickElementWithSpaceKeyByValueAttribute( nonSerious, "1");
			}
				
		}catch(Exception e)
		{
			log.error("Error in feeding data for event < "+e.getMessage()+" >");
			
		}
		
	}
	
	public  void feedEventDataForFollowUp( Map<String,String> propMap,ParsedDataTable oDataTable,Map<String,String>newEventsToAdd,int p_nEventCount)throws SciException{
		String 	descReported=null,descReportedAdd=null,descCoded=null,descCodedAdd=null,outCome=null
				,outComeAdd=null,desc=null;
		int eventCount=0;
		
		try{
			
			descReported=propMap.get("server.Event.DescReported");
			descCoded=propMap.get("server.Event.DescCoded");
			outCome=propMap.get("server.Event.Outcome");
			
			desc=newEventsToAdd.get("DescriptionasReported"+p_nEventCount);
			
			if(oDataTable.isColumnPresent("Event Count"))
				eventCount = (int) oDataTable.getColumnValues("Event Count").get(0).displayValue();
			
			for(int i=0;i<=eventCount;i++){
				
				if(oDataTable.isColumnPresent("DescriptionasReported"+p_nEventCount))
					descReportedAdd = (String) oDataTable.getColumnValues("DescriptionasReported"+p_nEventCount).get(0).displayValue().toString();
				
				if(desc.equals(descReportedAdd)){
					
					if(oDataTable.isColumnPresent("DescriptiontobeCoded"+p_nEventCount))
						descCodedAdd = (String) oDataTable.getColumnValues("DescriptiontobeCoded"+p_nEventCount).get(0).displayValue().toString();
					
					if(oDataTable.isColumnPresent("OutcomeofEvent"))
						outComeAdd = (String) oDataTable.getColumnValues("OutcomeofEvent").get(0).displayValue().toString();
					
					Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
					findElementByXpathAndSetContent("name",descReported, descReportedAdd);
					Thread.sleep(Integer.parseInt(propMap.get("minWait")));
					findElementByXpathAndSetContent("name",descCoded, descCodedAdd);
					Thread.sleep(Integer.parseInt(propMap.get("minWait")));
					findElementByXpathAndSetContent("name", outCome, outComeAdd);
					break;
				}
			}
							
		}catch(Exception e)
		{
			log.error("Error in feeding data for event < "+e.getMessage()+" >");
			
		}
		
	}
	
	public  void feedActivitiesData( Map<String,String> propMap,ParsedDataTable oDataTable)throws SciException{
		String  emailIn=null,
				emailInCodeTitle=null,
				emailInCode=null,
				actionCode=null,
				emailContaint=null,day=null,month=null,year=null,caseFollowUp=null,emailBodyPath=null,grpuser=null
				,endDate=null;
		Map<String,String> mapOfFile=null,mapOfMonthYearAndDate=null;
		
		try{
			emailBodyPath=propMap.get("contactLogPath");
			//mapOfFile=readTextFile("D:\\result.txt");
			mapOfFile=readTextFile(emailBodyPath);
			emailIn=propMap.get("server.Activities.EmailIn");
			emailInCodeTitle=propMap.get("server.Activities.EmailInCodeTitle");
			emailInCode=propMap.get("server.Activities.EmailInCodeTextArea");
			caseFollowUp=propMap.get("server.Activities.CaseRequireFollowUp");
			actionCode=propMap.get("server.Activities.ActionCode");
			grpuser=propMap.get("server.Activities.GrpUser1");
			endDate=propMap.get("server.Activities.EndDate");
			
			mapOfMonthYearAndDate=getDateBySplittingFullDateAndTime(mapOfFile.get("InDate"));
			emailContaint=mapOfFile.get("fileContent");
			day=mapOfMonthYearAndDate.get("day");
			month=mapOfMonthYearAndDate.get("month");
			year=mapOfMonthYearAndDate.get("year");
			
			findElementByXpathAndSetContent("name", emailIn, day+month+year);
			findElementByXpathAndSetContent("name", emailInCodeTitle, "Email In");
			findElementByAbsoluteXpathAndSetContent( emailInCode, "Email In\n"+emailContaint);
			findElementByXpathAndSetContent("name", actionCode, "Follow-up Required");
			findElementByAbsoluteXpathAndSetContent( caseFollowUp, "No follow up required as this is RA Case");
			findElementByXpathAndSetContent("name", grpuser, "Apo-Data Entry");
			findElementByXpathAndSetContent("name", endDate, "=");
			
		}catch(Exception e){
			log.error("Error in Feeding data for Activities tab < "+e.getMessage()+" >");
		}
	}

	public  void feedAdditionInfoForFollowUp( Map<String,String> propMap,ParsedDataTable oDataTable)throws SciException{
		
		String caseId=null,text=null,type=null,sdPath=null,inTakeMailPath=null,AERAdd=null
				,window=null,clickAttachment=null,getText=null,oldChar=null,lastAlphabet=null,AERBtn=null;
		String[] splitted=null;
		StringBuilder builder=null;
		int size=0,AERCount=0;
		
		try{
			
			builder=new StringBuilder();
			AERBtn=propMap.get("server.Addinfo.AddAER");
			caseId=propMap.get("Search.CaseID");
			type=propMap.get("server.AddInfo.Type");
			sdPath = propMap.get("sdpath");
			inTakeMailPath = propMap.get("intakeMailPath");
			clickAttachment=propMap.get("server.AddInfo.ClickAttach");
			window=getWindowHandle();
			
			if(oDataTable.isColumnPresent("AERNumber"))
				AERAdd =  (String) oDataTable.getColumnValues("AERNumber").get(0).displayValue().toString();
			
			size=getCountByXpath( "//table[@id='TableNotesAttach']","RowPosition");
			for(int i=1;i<=size-2;i++){
				Thread.sleep(3000);
				if(i==1){
					getText=getTextFromXpath( "//table[@id='TableNotesAttach']/tbody/tr/td[3]/table/tbody/tr[2]/td/textarea");
				}else{
					getText=getTextFromXpath( "//table[@id='TableNotesAttach']/tbody/tr["+i+"]/td[3]/table/tbody/tr[2]/td/textarea");
				}
				System.out.println(getText);
				if(getText!=null && !getText.equals("")){
					getText=getText.substring(0,1);
				}
				if(!getText.equals(oldChar))
					builder.append(getText);
				oldChar=getText;
			}
			lastAlphabet=getLastAlphabetCharacter(builder.toString());
			findElementByXpathAndSetContent("name", "TXT_TableNotesAttach_"+(size-2)+"_classification", "Source Document");
			enterClickByXpath("INPUT","name",clickAttachment);
			uploadFile( propMap, window, sdPath);
			text=getTextFromXpath( caseId);
			splitted=text.split("-");
			if(splitted[1].contains("\"")){
				splitted=splitted[1].split("\"");
				splitted[1]=splitted[0];
			}
			Thread.sleep(2000);
			findElementByXpathAndSetContent("name", "TableNotesAttach_"+(size-2)+"_keywords", "SD");
			Thread.sleep(2000);
			findElementByAbsoluteXpathAndSetContent( "//table[@id='TableNotesAttach']/tbody/tr["+(size-1)+"]/td[3]/table/tbody/tr[2]/td/textarea", lastAlphabet+"01_"+splitted[1].trim()+"_SD");
			findElementByXpathAndSetContent("name", "TXT_TableNotesAttach_"+(size-1)+"_classification", "Source Document");
			enterClickByXpath("INPUT","name",clickAttachment);
			uploadFile( propMap, window, inTakeMailPath);
			Thread.sleep(3000);
			findElementByXpathAndSetContent("name", "TableNotesAttach_"+(size-1)+"_keywords", "Intake mail");
			Thread.sleep(2000);
			findElementByAbsoluteXpathAndSetContent( "//table[@id='TableNotesAttach']/tbody/tr["+size+"]/td[3]/table/tbody/tr[2]/td/textarea", lastAlphabet+"02_"+splitted[1].trim()+"_Intake Mail");
			AERCount=getAERCount("TableReference_","_ref_no");
			if(AERCount>0){
				enterClickByXpath("INPUT","name",AERBtn);
				findElementByXpathAndSetContent("name", "TableReference_"+AERCount+"_ref_no", AERAdd);
			}
			findElementByXpathAndSetContent("name", type, "Health Canada File Number");
		}catch(Exception e){
			log.error("Error in Feeding data for Additional info tab for follou-up case< "+e.getMessage()+" >");
		}
	}

	public  boolean checkAllEvents(Map<String,String>propMap,ParsedDataTable oDataTableNew,Map<String,Object>existingDataFromArgusMap)throws SciException{
		int eventCount=0,newEventCount=0,matchesCount=0;
		String descReportedAdd=null,newDescReportedAdd=null;
		boolean matchMorethanOne=false;
		try{
			
			if(oDataTableNew.isColumnPresent("Event Count"))
				newEventCount = (int) oDataTableNew.getColumnValues("Event Count").get(0).displayValue();
			
			eventCount=(int) existingDataFromArgusMap.get("Event Count");
			
			
			for(int j=1;j<=newEventCount;j++){
			
				if(oDataTableNew.isColumnPresent("DescriptionasReported"+j))
					newDescReportedAdd = (String) oDataTableNew.getColumnValues("DescriptionasReported"+j).get(0).displayValue().toString();
			
				for(int i=1;i<=eventCount;i++){
					
					if(existingDataFromArgusMap.containsKey("DescriptionasReported"+i))
						descReportedAdd=(String) existingDataFromArgusMap.get("DescriptionasReported"+i);
					
					if(newDescReportedAdd.equals(descReportedAdd)){
						matchesCount++;
						break;
					}
				}
			}
			if(matchesCount>=2){
				matchMorethanOne=true;
			}
		}catch(Exception e){
			log.error("Error in check events to conform follow-up < "+e.getMessage()+" >");
		}
				return matchMorethanOne;
	}
	
	public  Map<String,String> getNewEventMapToAddInFollowUp(Map<String,String>propMap,ParsedDataTable oDataTableNew,Map<String,Object>existingArgusDataMap)throws SciException{
		int eventCount=0,newEventCount=0,count=1;
		String descReportedAdd=null,newDescReportedAdd=null;
		boolean checkNewEvent=false;
		Map<String,String>newEventsToAdd=null;
		DataTable eventDataTable=null;
		try{
			newEventsToAdd=new HashMap<>();
			eventDataTable=new DataTable();
			
			if(existingArgusDataMap.containsKey("Event Count"))
				eventCount=(int)existingArgusDataMap.get("Event Count");
			
			if(oDataTableNew.isColumnPresent("Event Count"))
				newEventCount = (int) oDataTableNew.getColumnValues("Event Count").get(0).displayValue();
			
			
			for(int j=1;j<=newEventCount;j++){
			
				if(oDataTableNew.isColumnPresent("DescriptionasReported"+j))
					newDescReportedAdd = (String) oDataTableNew.getColumnValues("DescriptionasReported"+j).get(0).displayValue().toString();
			
				for(int i=1;i<=eventCount;i++){
					
					if(existingArgusDataMap.containsKey("DescriptionasReported"+i))
						descReportedAdd=(String)existingArgusDataMap.get("DescriptionasReported"+i);
					
					if(newDescReportedAdd.equals(descReportedAdd)){
						checkNewEvent=true;
						break;
					}
				}
				if(!checkNewEvent){
					newEventsToAdd.put("DescriptionasReported"+count, newDescReportedAdd);
					eventDataTable.addColumn("DescriptionasReported"+count, newDescReportedAdd);
					count++;
					
				}
			}
		}catch(Exception e){
			log.error("Error in getting Map of events to add in follow-up case< "+e.getMessage()+" >");
		}
		return newEventsToAdd;
	}
	
	public  Map<String,Map<String,Product>> getNewProductMapToAddInFollowUp(Map<String,String>propMap,ParsedDataTable oDataTableNew,Map<String,Product>existingProductFromArgusMap)throws SciException{
		
		DataTable ProductExcelData=null;
		String prodPath=null;
		boolean prodForBooking=false,checkProductExistence=true;
		Map<String,Map<String,Product>>newAllProductMap=null,newAllProductTobeAdded=null;
		Map<String,Product>newVerifiedProductMap=null,newMissingProductMap=null,mergerdMap=null;
		Product product=null,newProduct=null;
		Map<String,Product>verifiedProductMapTobeAdd=null,missingProductMapTobeAdd=null,toAddInExistingProductMap=null;
		int vcount=1,mcount=1,editCount=1;
		ProductUtils productUtils=null;
		try{
			productUtils=new ProductUtils();
			newAllProductTobeAdded=new HashMap<>();
			prodPath = propMap.get("productCfgPath");
			
			ProductExcelData= productUtils.getDataFromProductExcel(prodPath);
			prodForBooking=false;
			newAllProductMap=productUtils.PDFVerification(oDataTableNew,ProductExcelData,prodForBooking);
			newVerifiedProductMap= newAllProductMap.get("verifiedProductMap");
			newMissingProductMap=newAllProductMap.get("missingProductMap");
			
			mergerdMap=mergeMap(newVerifiedProductMap, newMissingProductMap);
			verifiedProductMapTobeAdd=new HashMap<>();
			missingProductMapTobeAdd=new HashMap<>();
			toAddInExistingProductMap=new HashMap<>();
			for(int j=1;j<=mergerdMap.size();j++){
				
				if ((mergerdMap.get("Product"+j)) != null)
					newProduct=mergerdMap.get("Product"+j);
				checkProductExistence=true;
				
				for(int i=1;i<=existingProductFromArgusMap.size();i++){
				
					//check wedr product is same or different
					if ((existingProductFromArgusMap.get("Product"+i)) != null)
						product=existingProductFromArgusMap.get("Product"+i);
					
					if(newProduct.getProductName().equals(product.getProductName())){
						
						if(!(newProduct.getDosageForm().equalsIgnoreCase(product.getDosageForm()))){
							
							//if both product's properties are not empty and not unknown then add new product to follow-up 
							if(!newProduct.getDosageForm().equals("") && !newProduct.getDosageForm().equalsIgnoreCase("uknown")
									&& !product.getDosageForm().equals("") && !product.getDosageForm().equalsIgnoreCase("unknown")){
								checkProductExistence=false;
							}
							//if new product property is empty or unknown then don't add new product
							else if(newProduct.getDosageForm().equals("") || newProduct.getDosageForm().equalsIgnoreCase("uknown")){
								checkProductExistence=true;
							}
							//else edit new information to existing product
							else{
								checkProductExistence=true;
								toAddInExistingProductMap.put("Product"+editCount, newProduct);
							}
						
						} if(!(newProduct.getDose().equalsIgnoreCase(product.getDose()))){
							
							if(!newProduct.getDose().equals("") && !newProduct.getDose().equalsIgnoreCase("uknown")
									&& !product.getDose().equals("") && !product.getDose().equalsIgnoreCase("unknown")){
								checkProductExistence=false;
							}
							else if(newProduct.getDose().equals("") || newProduct.getDose().equalsIgnoreCase("uknown")){
								checkProductExistence=true;
							}
							else{
								checkProductExistence=true;
								toAddInExistingProductMap.put("Product"+editCount, newProduct);
							}
							
						} if(!(newProduct.getIndication().equalsIgnoreCase(product.getIndication()))){
							
							if(!newProduct.getIndication().equals("") && !newProduct.getIndication().equalsIgnoreCase("uknown")
									&& !product.getIndication().equals("") && !product.getIndication().equalsIgnoreCase("unknown")){
								checkProductExistence=false;
							}
							else if(newProduct.getIndication().equals("") || newProduct.getIndication().equalsIgnoreCase("uknown")){
								checkProductExistence=true;
							}
							else{
								checkProductExistence=true;
								toAddInExistingProductMap.put("Product"+editCount, newProduct);
							}
							
						} if(!(newProduct.getDoseUnit().equalsIgnoreCase(product.getDoseUnit()))){
							
							if(!newProduct.getDoseUnit().equals("") && !newProduct.getDoseUnit().equalsIgnoreCase("uknown")
									&& !product.getDoseUnit().equals("") && !product.getDoseUnit().equalsIgnoreCase("unknown")){
								checkProductExistence=false;
							}
							else if(newProduct.getDoseUnit().equals("") || newProduct.getDoseUnit().equalsIgnoreCase("uknown")){
								checkProductExistence=true;
							}
							else{
								checkProductExistence=true;
								toAddInExistingProductMap.put("Product"+editCount, newProduct);
							}
							
						} if(!(newProduct.getFrequency().equalsIgnoreCase(product.getFrequency()))){
							
							if(!newProduct.getFrequency().equals("") && !newProduct.getFrequency().equalsIgnoreCase("uknown")
									&& !product.getFrequency().equals("") && !product.getFrequency().equalsIgnoreCase("unknown")){
								checkProductExistence=false;
							}
							else if(newProduct.getFrequency().equals("") || newProduct.getFrequency().equalsIgnoreCase("uknown")){
								checkProductExistence=true;
							}
							else{
								checkProductExistence=true;
								toAddInExistingProductMap.put("Product"+editCount, newProduct);
							}
							
						} if(!(newProduct.getHPRole().equalsIgnoreCase(product.getHPRole()))){
							
							if(!newProduct.getHPRole().equals("") && !newProduct.getHPRole().equalsIgnoreCase("uknown")
									&& !product.getHPRole().equals("") && !product.getHPRole().equalsIgnoreCase("unknown")){
								checkProductExistence=false;
							}
							else if(newProduct.getHPRole().equals("") || newProduct.getHPRole().equalsIgnoreCase("uknown")){
								checkProductExistence=true;
							}
							else{
								checkProductExistence=true;
								toAddInExistingProductMap.put("Product"+editCount, newProduct);
							}
							
						} if(!(newProduct.getRoutOfAdmin().equalsIgnoreCase(product.getRoutOfAdmin()))){
							
							if(!newProduct.getRoutOfAdmin().equals("") && !newProduct.getRoutOfAdmin().equalsIgnoreCase("uknown")
									&& !product.getRoutOfAdmin().equals("") && !product.getRoutOfAdmin().equalsIgnoreCase("unknown")){
								checkProductExistence=false;
							}
							else if(newProduct.getRoutOfAdmin().equals("") || newProduct.getRoutOfAdmin().equalsIgnoreCase("uknown")){
								checkProductExistence=true;
							}
							else{
								checkProductExistence=true;
								toAddInExistingProductMap.put("Product"+editCount, newProduct);
							}
							
						}
						
						
					}else{
						checkProductExistence=false;
					}
					if(checkProductExistence){
						break;
						
					}
				}
				if(!checkProductExistence){
					
					if(newProduct.isVerifiedFlag()){
						verifiedProductMapTobeAdd.put("Product"+vcount, newProduct);
						vcount++;
					}else{
						missingProductMapTobeAdd.put("Product"+mcount, newProduct);
						mcount++;
					}
					
				}
			}
			newAllProductTobeAdded.put("verifiedProductMap", verifiedProductMapTobeAdd);
			newAllProductTobeAdded.put("missingProductMap", missingProductMapTobeAdd);
			newAllProductTobeAdded.put("toAddInExistingProductMap", toAddInExistingProductMap);	
		}catch(Exception e){
			log.error("Error in getting new products to add in follow-up case< "+e.getMessage()+" >");		
		}
				return newAllProductTobeAdded;
	}
	 
	public boolean duplicateCaseByAERAndMAHAndLikedNumber(Map<String,String>propMap,ParsedDataTable oDataTable)throws SciException{
		String AER=null,AERAdd=null,searchRepType=null,searchPatCountry=null,searchInitRepDate=null,caseOptionsearch=null
				, caseResultSearchTableID=null,searchButton=null,clearButton=null,MAHNum=null,linkedNumber=null;
		boolean checkDuplicateCasePage=false,dup=false;
		int rowSize=0;
		
		try{
			
			searchRepType = propMap.get("Search.General.ReportType");
			searchPatCountry = propMap.get("Search.Patient.Country");
			searchInitRepDate = propMap.get("Search.InitRecpDate");
			caseOptionsearch = propMap.get("caseaction.dropdown.valueForSearch");
			caseResultSearchTableID=propMap.get("Search.TableId");
			searchButton=propMap.get("Search.Button");
			clearButton=propMap.get("Search.Clear");
			
			executeJavaScript( caseOptionsearch);
			Thread.sleep(Integer.parseInt(propMap.get("waitForSearchCase")));
			checkDuplicateCasePage = findMultipleElements(
					new String[] { searchRepType, searchInitRepDate, searchPatCountry });
			if (!checkDuplicateCasePage) {
				log.debug("duplicate search case page not found");
				executeJavaScript( caseOptionsearch);
			}
			AER=propMap.get("Search.AER");
			if(oDataTable.isColumnPresent("AERNumber"))
				AERAdd =  (String) oDataTable.getColumnValues("AERNumber").get(0).displayValue().toString();
			
			if(oDataTable.isColumnPresent("RegulatoryRef"))
				MAHNum =  (String) oDataTable.getColumnValues("RegulatoryRef").get(0).displayValue().toString();
			
		/*	if(oDataTable.isColumnPresent("linkDupRecordCount"))
				 linkedNumber=  (String) oDataTable.getColumnValues("linkDupRecordCount").get(0).displayValue().toString();
		*/	
			enterClickByXpath("INPUT","name",clearButton);
			findElementByXpathAndSetContent("name", AER, AERAdd);
			enterClickByXpath("INPUT","name",searchButton);
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			rowSize=getAllRowSizeByTableID("div","id", caseResultSearchTableID);
			if(rowSize>=1){
				dup=true;
			}else if(!MAHNum.equals("") && MAHNum!=null){
				enterClickByXpath("INPUT","name",clearButton);
				findElementByXpathAndSetContent("name", AER, MAHNum);
				enterClickByXpath("INPUT","name",searchButton);
				Thread.sleep(Integer.parseInt(propMap.get("minWait")));
				rowSize=getAllRowSizeByTableID("div","id", caseResultSearchTableID);
				if(rowSize>=1){
					dup=true;
				}/*else if(!linkedNumber.endsWith("") && linkedNumber!=null){
					enterClickByXpath("INPUT","name",clearButton);
					findElementByXpathAndSetContent("name", AER, linkedNumber);
					enterClickByXpath("INPUT","name",searchButton);
					Thread.sleep(Integer.parseInt(propMap.get("minWait")));
					rowSize=getAllRowSizeByTableID("div","id", caseResultSearchTableID);
					if(rowSize>=1){
						dup=true;
					}
				}
*/			}
			
		}catch(Exception e){
			log.error("Error in check duplicate number by AER number and MAH number< "+e.getMessage()+" >");	
		}
		return dup;
	}

	public boolean duplicateCaseByPatInfoProductAndEvents( Map<String,String>propMap, ParsedDataTable oDataTable)throws SciException{
		String gender=null,genderAdd=null
				,age=null,ageAdd=null,countryValue=null,country=null,clearButton=null,selectProduct=null,caseResultSearchTableID=null
				,searchButton=null,prodPath=null,descReported=null,window=null,descReportedAdd=null,resultRowsId=null;
		boolean matchMorethanOneEvent=false;
		
		DataTable ProductExcelData=null;
		Map<String,Map<String,Product>> allProductMap=null;
		Map<String,Product>verifiedProductMap=null;
		int eventCount=0,rowSize=0;
		ProductUtils productUtils=null;
		
		try{
			productUtils=new ProductUtils();
					
			resultRowsId=propMap.get("CreateCase.SeachResult.RowsId");
			clearButton=propMap.get("Search.Clear");
			Map<String,Object>existingDataFromArgusMap=null;
			
			selectProduct = propMap.get("CreateCase.SelectProduct");
			caseResultSearchTableID=propMap.get("Search.TableId");
			searchButton=propMap.get("Search.Button");
			prodPath = propMap.get("productCfgPath");
			descReported=propMap.get("CreateCase.DescriptionAsReported");
			ProductExcelData= productUtils.getDataFromProductExcel(prodPath);
			allProductMap=productUtils.PDFVerification(oDataTable,ProductExcelData,false);
			verifiedProductMap=allProductMap.get("verifiedProductMap");
			
			if(oDataTable.isColumnPresent("Event Count"))
				eventCount = (int) oDataTable.getColumnValues("Event Count").get(0).displayValue();
		
			gender=propMap.get("Search.Patient.Gender");
			if(oDataTable.isColumnPresent("Patient Details.Gender"))
				genderAdd =  (String) oDataTable.getColumnValues("Patient Details.Gender").get(0).displayValue().toString();
			
			age=propMap.get("Search.Patient.Age");
			if(oDataTable.isColumnPresent("Patient Details.Age"))
				ageAdd = (String) oDataTable.getColumnValues("Patient Details.Age").get(0).displayValue().toString();
			
			country = propMap.get("CreateCase.Country");
			if (oDataTable.isColumnPresent("Country"))
				countryValue = (String) oDataTable.getColumnValues("Country").get(0).displayValue().toString();
		
			redirectToCaseSearchPage( propMap);
			
			enterClickByXpath("INPUT","name",clearButton);
			findElementByXpathAndSetContent("name", age, ageAdd);
			findElementByXpathAndSetContent("name", gender, genderAdd.trim());
			findElementByXpathAndSetContent("name", country, countryValue);
		
			//check with product
			for(int i=1;i<=verifiedProductMap.size();i++){
				
				if(i>1)
					redirectToCaseSearchPage( propMap);
				
				Thread.sleep(Integer.parseInt(propMap.get("minWait")));
				window=getWindowHandle();
				enterClickByXpath("INPUT","name",selectProduct);
				selectProduct( propMap, oDataTable, window, i,verifiedProductMap);
					
				enterClickByXpath("INPUT","name",searchButton);
				Thread.sleep(3000);
				rowSize=getAllRowSizeByTableID("div","id", caseResultSearchTableID);
				
				//if any case found By Age gender then go for check all events
				if(rowSize>=1){
					
					//check with events
					for(int j=1;j<=eventCount;j++){
						Thread.sleep(Integer.parseInt(propMap.get("minWait")));
						if(j>1)
							redirectToCaseSearchPage( propMap);
						
						if(oDataTable.isColumnPresent("DescriptionasReported"+j))
							descReportedAdd = (String) oDataTable.getColumnValues("DescriptionasReported"+j).get(0).displayValue().toString();
						findElementByXpathAndSetContent("name",descReported, descReportedAdd);
						
						enterClickByXpath("INPUT","name",searchButton);
						Thread.sleep(3000);
						rowSize=getAllRowSizeByTableID("div","id", caseResultSearchTableID);
						
						//if any case found By Age gender then go for check all events
						if(rowSize>=1){
							
							for(int k=1 ; k <= rowSize ; k++){
								
								if(k>1)
									redirectToCaseSearchPage( propMap);
								
								//click on first case id to open case 
								clickfirstLink(resultRowsId+k);
								
								//get text from argus and make Map
								existingDataFromArgusMap=getTextOfAllTabs( propMap);
								
								//check if similar events founds or not
								matchMorethanOneEvent=checkAllEvents( propMap, oDataTable,existingDataFromArgusMap);
							
								if(matchMorethanOneEvent)
									break;
							}
							
						}//if
						
						if(matchMorethanOneEvent)
							break;
						
					}//for j
				}
				
				if(matchMorethanOneEvent)
					break;
				
			}//for i
			
		}catch(Exception e){
			log.error("Error in searching duplicate by patient info and product info< "+e.getMessage()+" >");	
		}
		
		return matchMorethanOneEvent;
		
	}

	
	public  void feedReporterForFollowUp( Map<String,String> propMap,ParsedDataTable oDataTable,Map<String,Object> existingDataFromArgusMap)throws SciException{
		String repoSentByRegAuth=null,repoCountry=null,scriptAttribute=null,newButtonName=null
				,newHCPTableName=null,hcpDropDown=null,reptype=null,repoCountryAdd=null,repoTypeAdd=null
				,protectedConfidentially=null;
		
		try{
			
			repoSentByRegAuth= propMap.get("server.Reporter.RepSentByregAuth");
			repoCountry = propMap.get("server.Reporter.Country");
			scriptAttribute = propMap.get("server.Script.Attribute");
			newButtonName= propMap.get("server.Button.Name");
			newHCPTableName=propMap.get("server.Reporter.NewHCPTableName");
			hcpDropDown= propMap.get("server.Reporter.HCP");
			reptype=propMap.get("server.Reporter.Type");
			protectedConfidentially=propMap.get("server.Reporter.ProtectedConfidentially");
			
			if(oDataTable.isColumnPresent("Country"))
				repoCountryAdd = (String) oDataTable.getColumnValues("Country").get(0).displayValue().toString();
			
			if(oDataTable.isColumnPresent("PrimaryReporter"))
				repoTypeAdd = (String) oDataTable.getColumnValues("PrimaryReporter").get(0).displayValue().toString();
			
			if(repoTypeAdd.equals("")){
				repoTypeAdd="Other Health Care Professional";
			}
			dynamicExecuteJavaScriptByText(newHCPTableName,scriptAttribute,newButtonName);
			
			selectDropdownValueByIndex( hcpDropDown, 1);
			findElementByXpathAndSetContent("name",propMap.get("server.Reporter.Occupation"), repoTypeAdd);
			selectDropdownValueByIndex( repoSentByRegAuth, 1);
			deSelectElementWithSpaceKey( protectedConfidentially);	
			if(!repoTypeAdd.equalsIgnoreCase("Consumer or other non health professional")){
				selectElementWithSpaceKey( propMap.get("server.Reporter.PrimaryReport"));
			}
			findElementByXpathAndSetContent("name", repoCountry, repoCountryAdd);
			findElementByXpathAndSetContent("name", reptype, repoTypeAdd);
			
		}catch(Exception e){
			log.error("Error in feeding reporter for follow-up case< "+e.getMessage()+" >");
		}
	}

	public  void editProductInFollowUp( Map<String,String> propMap,ParsedDataTable oDataTable,Product product)throws SciException{
		
		String 	pcmDose=null,pcmUnit=null,pcmFrequency=null,pcmDoseAdd=null,pcmUnitAdd=null,pcmFrequencyAdd=null,prodIndication=null
				,prodIndicationAdd=null,patRouteOfAdmin=null;
		String patRouteOfAdminAdd=null,doseDescription=null;
		
		try{
			
			doseDescription=propMap.get("server.Product.doseDescription");
			
			patRouteOfAdmin= propMap.get("server.Product.PatRouteOfAdmin");
			patRouteOfAdminAdd = product.getRoutOfAdmin();
			
			pcmDose = propMap.get("server.product.Dose");
			pcmDoseAdd =product.getDose();
			
			pcmFrequency = propMap.get("server.product.DoseFrequency");
			pcmFrequencyAdd =product.getFrequency();
			
			pcmUnit = propMap.get("server.product.DoseUnit");
			pcmUnitAdd = product.getDoseUnit();
		
			prodIndication = propMap.get("server.Product.Indication");
			prodIndicationAdd=product.getIndication();
			
			if(!patRouteOfAdminAdd.equals("") && !patRouteOfAdminAdd.equalsIgnoreCase("unknown"))
				findElementByXpathAndSetContent("name", patRouteOfAdmin, patRouteOfAdminAdd);
			
			if(!prodIndicationAdd.equals("") && !prodIndicationAdd.equalsIgnoreCase("unknown"))
				//for demo
				findElementByXpathAndSetContent("name", prodIndication, "product used for unknown indication");
				//enable this line for actual indication
				//findElementByXpathAndSetContent("name", prodIndication, prodIndicationAdd);
			
			if((!pcmDoseAdd.equals(""))||(!pcmUnitAdd.equals(""))||(!pcmFrequencyAdd.equals(""))){
				findElementByXpathAndSetContent("name", doseDescription, pcmDoseAdd+" "+pcmUnitAdd+" "+pcmFrequencyAdd);
			}
			if(!pcmDoseAdd.equals("") || !pcmDoseAdd.equalsIgnoreCase("unknown"))
				findElementByXpathAndSetContent("name", pcmDose, pcmDoseAdd);
			
			if(!pcmUnitAdd.equals("") || !pcmUnitAdd.equalsIgnoreCase("unknown"))
				findElementByXpathAndSetContent("name", pcmUnit, pcmUnitAdd);
			
			if(!pcmFrequencyAdd.equals("") || !pcmFrequencyAdd.equalsIgnoreCase("unknown"))
				findElementByXpathAndSetContent("name", pcmFrequency, pcmFrequencyAdd);
			
		}catch(Exception e)
		{
			log.error("Error in editing existing product while performing follow-up< "+e.getMessage()+" >");
			
		}
		
	}

	public  Map<String,String> readTextFile(String sFilePath) {

		StringBuilder builder=null;
		String sCurrentLine=null;
		Map<String,String> map=null;
		String[] splittedDate=null;
		try (BufferedReader br = new BufferedReader(new FileReader(sFilePath))) {
			builder=new StringBuilder();
			map=new HashMap<>();
			while ((sCurrentLine = br.readLine()) != null) {
				builder.append(sCurrentLine);
				builder.append("\n");
				if(sCurrentLine.contains(":")){
					splittedDate=sCurrentLine.split(":");
					if(splittedDate[0].equalsIgnoreCase("sent")){
						map.put("InDate", splittedDate[1]);
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		map.put("fileContent", builder.toString());
		return map;

	}
	
	public  String getLastAlphabetCharacter(String arr){
		
		char[] charArr=arr.toCharArray();
		int i=0;
		String alphabetStr=null;
		String charArrStr=null;
		String toAdd=null;
		for (char alphabet = 'A'; alphabet <= 'Z'; alphabet++) {
			alphabetStr=Character.toString(alphabet);
			charArrStr=Character.toString(charArr[i]);
			if(!charArrStr.equals(alphabetStr)){
				toAdd=Character.toString(alphabet);
				break;
			}
			if(i<charArr.length-1)
				i++;
		}
		return toAdd;
	}
	
	public  Map<String, Product> mergeMap(Map<String, Product> firstMap ,Map<String, Product> secondMap){
		Product product=null;
		int size=secondMap.size();
		Map<String,Product> tmp = new HashMap<String, Product>(firstMap);
		//tmp.keySet().removeAll(firstMap.keySet());
		for(String key:firstMap.keySet()){
			if(!secondMap.containsKey(key)){
				product=tmp.get(key);
				secondMap.put(key, product);
			}else{
				product=tmp.get(key);
				secondMap.put("Product"+(size+1), product);
				size++;
			}
			
		}
		
		return secondMap;
	}
	
	public Map<String,String> getDateBySplittingFullDateAndTime(String fullDate) {
		Map<String,String> map=null;
		String month=null,year=null,monthupTo3Characters=null,yearupTo4Characters=null,day;
		String[] splittedDate=null,splittedDay=null;
		try{
			map=new HashMap<>();
			splittedDate=fullDate.split(",");
			month=splittedDate[1].trim();
			year=splittedDate[2].trim();
			monthupTo3Characters = month.substring(0, Math.min(month.length(), 3));
			yearupTo4Characters = year.substring(0, Math.min(month.length(), 4));
			splittedDay=month.split(" ");
			day=splittedDay[1];
			map.put("day", day);
			map.put("month", monthupTo3Characters);
			map.put("year", yearupTo4Characters);
		}catch(Exception  e)
		{
			e.printStackTrace();
		}
		return map;
	}

	public int getAERCount( String p_sSubString1, String p_sSubString2) throws SciException {
		String value=null;
		int count=0;
		WebElement baseTable=null;
		for(int i=0;i<=count;i++){
			baseTable=getElement( "INPUT", "name", p_sSubString1+i+p_sSubString2);
			value=baseTable.getAttribute("value");
			if(value!=null&&!value.equals("")){
				count++;
			}
		}
		return count;

	}

	public void redirectToCaseSearchPage(Map<String,String> propMap)throws SciException{
		
		
		String caseOptionsearch=null, searchRepType=null, searchPatCountry=null, searchInitRepDate= null;
		searchRepType = propMap.get("Search.General.ReportType");
		searchPatCountry = propMap.get("Search.Patient.Country");
		searchInitRepDate = propMap.get("Search.InitRecpDate");
		boolean checkDuplicateCasePage=false;
		try{
			
			caseOptionsearch = propMap.get("caseaction.dropdown.valueForSearch");
			executeJavaScript( caseOptionsearch);
			Thread.sleep(Integer.parseInt(propMap.get("waitForSearchCase")));
			checkDuplicateCasePage = findMultipleElements(
					new String[] { searchRepType, searchInitRepDate, searchPatCountry });
			if (!checkDuplicateCasePage) {
				log.debug("duplicate search case page not found");
				executeJavaScript( caseOptionsearch);
			}
		}
		catch(Exception e)
		{
			log.error("Error while redirecting to search page");
		}
	}

	
	public void updateCase( Map<String, String> map, ParsedDataTable oDataTable, String caseId)throws SciException {
		String additionalInformationTab=null,dateOfAttachment=null,pspId=null,caseSaveButton=null;
		String productTab=null,productName,pcmDose,pcmFrequency,patientTab=null,patFirstName=null,patLastName=null,patAddress=null
				,selectProduct=null,selectDrug=null;
		String window=null,generalTab=null,eventTab=null, newProductTabTableName=null,scriptAttributeForNewProductTab=null,newProductTabName=null;
		String initRecpDate=null,repoPrefix=null,centralrecpDate=null,newEventTabTableName=null;
		String descReported=null,descToBeCoded=null,activitiesTab=null,emailIn=null,emailInCodeTitle=null,prodPath=null;
		int eventCount=0;
		boolean checkSDDownloadPage=false,checkProductPage=false,checkPatientFeedingPage=false,checkReporterHcpPage=false,hcp=false
				, checkEventPage=false,checkActivitiTab=false,prodForBooking=false;
		Map<String, Product>  verifiedProductMap=null,missingProductMap=null;
		Map<String,Map<String,Product>> allProductMap=null;
		
		
		ProductUtils productUtils=null;
		try{
			productUtils=new ProductUtils();
			
			repoPrefix = map.get("server.Reporter.prefix");
			initRecpDate= map.get("server.DateOfAwareness");
			centralrecpDate=map.get("server.centralRecptDate");
			generalTab = map.get("server.GeneralTab");
			eventTab = map.get("server.EventTab");
			selectProduct=map.get("server.Product.SelectProduct");
			dateOfAttachment=map.get("server.AddInfo.DateOfAttach");
			pspId=map.get("server.AddInfo.PSPID");
			caseSaveButton = map.get("Server.Save.button");
			productTab = map.get("server.ProductTab");
			additionalInformationTab = map.get("server.AdditionalInformationTab");
			activitiesTab = map.get("server.ActivitiesTab");
			
			productName=map.get("server.Product.Name");
			pcmDose = map.get("server.product.Dose");
			pcmFrequency = map.get("server.product.DoseFrequency");
			
			patientTab = map.get("server.PatientTab");
			patFirstName = map.get("server.Patient.FirstName");
			patLastName = map.get("server.Patient.LastName");
			patAddress = map.get("server.Patient.Address");
			
			descReported=map.get("server.Event.DescReported");
			descToBeCoded=map.get("server.Event.DescCoded");
			emailIn=map.get("server.Activities.EmailIn");
			emailInCodeTitle=map.get("server.Activities.EmailInCodeTitle");
			selectDrug=map.get("server.Product.SelectDrug");
			prodPath = map.get("productCfgPath");
			newProductTabTableName=map.get("server.Product.NewProductTableName");
			scriptAttributeForNewProductTab= map.get("server.Script.Attribute");
			newProductTabName=map.get("server.Button.Name");
			newEventTabTableName=map.get("server.Event.NewEventTableName");
			//user ApotexFeedOperation
			
			
			DataTable ProductExcelData= productUtils.getDataFromProductExcel(prodPath);
			log.debug("wait to click general tab");
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			//check page absents
			checkReporterHcpPage=findMultipleElements( new String[]{repoPrefix,initRecpDate,centralrecpDate});
			if(!checkReporterHcpPage)
			{
				log.debug("General Tab page not found,Redirecting to general tab");
				executeJavaScript( generalTab);
			}
			
			log.debug("Reporter data feeding started");
			feedReporterHCPData(map,oDataTable,hcp);			
			log.debug("Reporter data feeding completed\n");
			hcp=true;
			log.debug("HCP data feeding started");
			feedReporterHCPData(map,oDataTable,hcp);	
			log.debug("HCP data feeding completed\n");
			
			executeJavaScript( patientTab);
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			checkPatientFeedingPage=findMultipleElements( new String[]{patFirstName,patLastName,patAddress});
			if(!checkPatientFeedingPage)
			{
				log.debug("patient feeding page not found,Redirecting to patient tab");
				executeJavaScript( patientTab);
			}
			log.debug("patient data feeding started");
			feedPatientData(map,oDataTable);
			log.debug("patient data feeding completed\n");
			
			//get all product from data table to differentiate verified product
			allProductMap=(Map<String,Map<String,Product>>)productUtils.PDFVerification(oDataTable,ProductExcelData,prodForBooking);
			//to select operation
			verifiedProductMap=(Map<String,Product>) allProductMap.get("verifiedProductMap");
			//to encode operation
			missingProductMap=(Map<String,Product>) allProductMap.get("missingProductMap");
			
			executeJavaScript( productTab);
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			checkProductPage=findMultipleElements( new String[]{productName,pcmDose,pcmFrequency});
			if(!checkProductPage){
				log.debug("product feeding page not found,Redirecting to product tab");
				executeJavaScript( productTab);
			}
			log.debug("product data feeding started");
			feedProductData( map, oDataTable,1,verifiedProductMap);
		
			//for select product
			for(int i=2;i<=verifiedProductMap.size();i++){
				dynamicExecuteJavaScriptByText( newProductTabTableName, scriptAttributeForNewProductTab, newProductTabName);
				Thread.sleep(Integer.parseInt(map.get("commonWait")));
				window=getWindowHandle();
				enterClickByXpath( "INPUT", "name", selectProduct);
				selectProduct( map, oDataTable,window,i,verifiedProductMap);
				feedProductData( map, oDataTable,i,verifiedProductMap);
			}
			//for encoding
			for(int i=1;i<=missingProductMap.size();i++){
				dynamicExecuteJavaScriptByText( newProductTabTableName, scriptAttributeForNewProductTab, newProductTabName);
				Thread.sleep(Integer.parseInt(map.get("commonWait")));
				window=getWindowHandle();
				enterClickByXpath( "INPUT", "name", selectDrug);
				encodeProduct( map, oDataTable,window,i,missingProductMap);
				feedProductData( map, oDataTable,i,missingProductMap);
			}
			
			Thread.sleep(Integer.parseInt(map.get("waitForEventTab")));
			executeJavaScript( eventTab);
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			checkEventPage=findMultipleElements( new String[]{descReported,descToBeCoded});
			if(!checkEventPage){
				log.error("Event feeding page not found,Redirecting to Event tab");
				executeJavaScript( eventTab);
			}
			log.debug("Event data feeding started");
			feedEventData( map, oDataTable,1);
			if(oDataTable.isColumnPresent("Event Count"))
				eventCount = (int) oDataTable.getColumnValues("Event Count").get(0).displayValue();
			
			for(int i=2;i<=eventCount;i++){
				Thread.sleep(Integer.parseInt(map.get("minWait")));
				dynamicExecuteJavaScriptByText( newEventTabTableName, scriptAttributeForNewProductTab, newProductTabName);
				feedEventData( map, oDataTable,i);
			}
			log.debug("Event data feeding completed\n");
			//feed activities data
			Thread.sleep(Integer.parseInt(map.get("minWait")));
			executeJavaScript( activitiesTab);
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			checkActivitiTab=findMultipleElements( new String[]{emailIn,emailInCodeTitle});
			if(!checkActivitiTab){
				executeJavaScript( activitiesTab);
			}
			Thread.sleep(Integer.parseInt(map.get("minWait")));
			feedActivitiesData( map, oDataTable);
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			executeJavaScript( additionalInformationTab);
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			checkSDDownloadPage=findMultipleElements( new String[]{dateOfAttachment,pspId});
			if(!checkSDDownloadPage)
			{
				log.debug("Attachment page not found,Redirecting to additional info tab");
				executeJavaScript( additionalInformationTab);
			}
			Thread.sleep(Integer.parseInt(map.get("minWait")));
			feedAdditionInfo( map, oDataTable);
			Thread.sleep(Integer.parseInt(map.get("waitForSaveCase")));
			enterClickByXpath( "INPUT", "name", caseSaveButton);
			Thread.sleep(Integer.parseInt(map.get("commonWait")));
			
		}catch(Exception e){
			log.error("Error in update case <"+e.getMessage()+">");
			e.printStackTrace();
		}
	}

	public File locateCaseAndDownloadSD( Map<String, String> map, String caseId) throws SciException {
		String additionalInformationTab = null;
		
		String downloadPdf=null ,pdfTableId=null;
		boolean checkSDDownloadPage=false,checkDownloadFile=false,checkReporterHcpPage=false;
		String dateOfAwareness=null,dateOfAttachment=null,pspId=null,repoPrefix=null,initRecpDate=null,centralrecpDate=null,generalTab=null;
		Robot robot=null;
		String downloadPath=null;
		StringSelection stringSelection=null;
		Clipboard clipboard =null;
		
		try 
		{
			
			dateOfAwareness= map.get("Date of Awareness");
			downloadPdf = map.get("server.downloadPDF");
			pdfTableId = map.get("server.PDFTableID");
			dateOfAttachment=map.get("server.AddInfo.DateOfAttach");
			pspId=map.get("server.AddInfo.PSPID");
			additionalInformationTab = map.get("server.AdditionalInformationTab");
			repoPrefix = map.get("server.Reporter.prefix");
			initRecpDate= map.get("server.DateOfAwareness");
			centralrecpDate=map.get("server.centralRecptDate");
			generalTab = map.get("server.GeneralTab");
			
			Thread.sleep(2000);
			checkReporterHcpPage=findMultipleElements( new String[]{repoPrefix,initRecpDate,centralrecpDate});
			if(!checkReporterHcpPage)
			{
				log.error("General Tab page not found");
				log.error("Redirecting to general tab");
				executeJavaScript( generalTab);
			}
			executeJavaScript( additionalInformationTab);
			Thread.sleep(2000);
			checkSDDownloadPage=findMultipleElements( new String[]{dateOfAttachment,pspId});
			if(!checkSDDownloadPage)
			{
				log.error("Attachment page not found");
				log.error("Redirecting to additional info tab");
				executeJavaScript( additionalInformationTab);
			}

			Thread.sleep(Integer.parseInt(map.get("minWait")));
			downloadPath = System.getProperty("user.dir")+"\\"+map.get("PdfName")+"_"+System.currentTimeMillis()+".pdf";
			log.debug("Starting pdf downlaod");
			checkDownloadFile=downloadPdf(pdfTableId,downloadPdf,dateOfAwareness);
			
			if(checkDownloadFile){
		
				log.debug("download link clicked");	
				getSetOfWindows(2); 
				Thread.sleep(Integer.parseInt(map.get("waitForRobot")));
				robot=new Robot();
				stringSelection = new StringSelection(downloadPath);
				clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(stringSelection, stringSelection);
				
				robot.keyPress(KeyEvent.VK_TAB);
				Thread.sleep(Integer.parseInt(map.get("waitForSaveAsOperation")));
				robot.keyPress(KeyEvent.VK_TAB);
				Thread.sleep(Integer.parseInt(map.get("waitForSaveAsOperation")));
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.keyRelease(KeyEvent.VK_DOWN);
				Thread.sleep(Integer.parseInt(map.get("waitForSaveAsOperation")));
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.keyRelease(KeyEvent.VK_DOWN);
				Thread.sleep(Integer.parseInt(map.get("waitForSaveAsOperation")));
				robot.keyPress(KeyEvent.VK_ENTER);
				Thread.sleep(Integer.parseInt(map.get("commonWait2")));
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_V);
				Thread.sleep(Integer.parseInt(map.get("waitForSaveAsOperation")));
				robot.keyRelease(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_CONTROL);
				Thread.sleep(Integer.parseInt(map.get("waitForSaveAsOperation")));
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
			}

		}catch(Exception e)
		{
			log.error("Download failed < "+e.getMessage()+" >");
			// log.error("Error: in locating caseid or downloading sd file ");
		}
		
		return new File(downloadPath);
	

	}

	public void followUpCase(Map<String,String>propMap,ParsedDataTable oDataTableNew,Map<String,Object>existingDataFromArgusMap)throws SciException {
		
		String 	productTab=null,productName=null,pcmDose=null,pcmFrequency=null,window=null,selectProduct=null
				,selectDrug=null,eventTab=null,descReported=null,descToBeCoded=null,dateOfAttachment=null,pspId=null
				,additionalInformationTab=null,caseSaveButton=null,generalTab=null,repoPrefix=null,initRecpDate=null,centralrecpDate=null;
		boolean checkProductPage=false,checkEventPage=false,checkSDDownloadPage=false,checkReporterHcpPage=false,pReporter=false;
		Map<String,Map<String,Product>>newAllProductMapToAdd=null;
		Map<String,Product>verifiedProductMap=null,missingProductMap=null,existingProductMapFromArgusMap=null,toEditInExistingProduct=null;
		Map<String,String> newEventsToAdd=null;
		
		
		try{
			
			caseSaveButton = propMap.get("Server.Save.button");
			additionalInformationTab = propMap.get("server.AdditionalInformationTab");
			dateOfAttachment=propMap.get("server.AddInfo.DateOfAttach");
			pspId=propMap.get("server.AddInfo.PSPID");
			descReported=propMap.get("server.Event.DescReported");
			descToBeCoded=propMap.get("server.Event.DescCoded");
			eventTab = propMap.get("server.EventTab");
			selectDrug=propMap.get("server.Product.SelectDrug");
			selectProduct=propMap.get("server.Product.SelectProduct");
			productName=propMap.get("server.Product.Name");
			pcmDose = propMap.get("server.product.Dose");
			pcmFrequency = propMap.get("server.product.DoseFrequency");
			productTab = propMap.get("server.ProductTab");
			generalTab = propMap.get("server.GeneralTab");
			repoPrefix = propMap.get("server.Reporter.prefix");
			initRecpDate= propMap.get("server.DateOfAwareness");
			centralrecpDate=propMap.get("server.centralRecptDate");
			
			//use ApotexFeedOperation
			
			//get existing Product Details
			existingProductMapFromArgusMap=(Map<String, Product>) existingDataFromArgusMap.get("Products");
			//get follow-up added product to be add in case
			newAllProductMapToAdd=getNewProductMapToAddInFollowUp( propMap, oDataTableNew,existingProductMapFromArgusMap);
			//get verified an missing product
			verifiedProductMap=newAllProductMapToAdd.get("verifiedProductMap");
			missingProductMap=newAllProductMapToAdd.get("missingProductMap");
			toEditInExistingProduct=newAllProductMapToAdd.get("toAddInExistingProductMap");
			
			System.out.println(verifiedProductMap.size());
			System.out.println(missingProductMap.size());
			
			executeJavaScript( generalTab);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			checkReporterHcpPage=findMultipleElements( new String[]{repoPrefix,initRecpDate,centralrecpDate});
			if(!checkReporterHcpPage)
			{
				log.error("General Tab page not found");
				log.error("Redirecting to general tab");
				executeJavaScript( generalTab);
			}
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			pReporter=(boolean) existingDataFromArgusMap.get("ReporterSame");
			if(!pReporter){
				feedReporterForFollowUp( propMap, oDataTableNew,existingDataFromArgusMap);
			}
			executeJavaScript( productTab);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			checkProductPage=findMultipleElements( new String[]{productName,pcmDose,pcmFrequency});
			if(!checkProductPage){
				log.error("product feeding page not found");
				log.error("Redirecting to product tab");
				executeJavaScript( productTab);
			}
			log.debug("product data feeding started");
			System.out.println("Parent window for product tab::"+getWindowHandle());
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			
			for ( Map.Entry<String, Product> entry : toEditInExistingProduct.entrySet()) {
			    String key = entry.getKey();
			    Product product = entry.getValue();
			    dynamicExecuteJavaScriptByText( "table_nav_CF_PRD_id", "code", key);
			    editProductInFollowUp( propMap, oDataTableNew, product);
			}
			
			for(int i=1;i<=verifiedProductMap.size();i++){
				dynamicExecuteJavaScriptByText( "table_nav_CF_PRD_id", "code", "(New)");
				Thread.sleep(4000);
				window=getWindowHandle();
				enterClickByXpath( "INPUT", "name", selectProduct);
				selectProduct( propMap, oDataTableNew,window,i,verifiedProductMap);
				feedProductData( propMap, oDataTableNew,i,verifiedProductMap);
			}
			
			for(int i=1;i<=missingProductMap.size();i++){
				dynamicExecuteJavaScriptByText( "table_nav_CF_PRD_id", "code", "(New)");
				Thread.sleep(4000);
				window=getWindowHandle();
				enterClickByXpath( "INPUT", "name", selectDrug);
				encodeProduct( propMap, oDataTableNew,window,i,missingProductMap);
				feedProductData( propMap, oDataTableNew,i,missingProductMap);
			
			}
			
			Thread.sleep(Integer.parseInt(propMap.get("waitForEventTab")));
			executeJavaScript( eventTab);
			Thread.sleep(Integer.parseInt(propMap.get("waitForEventTab")));
			checkEventPage=findMultipleElements( new String[]{descReported,descToBeCoded});
			if(!checkEventPage){
				log.error("Event feeding page not found");
				log.error("Redirecting to Event tab");
				executeJavaScript( eventTab);
			}
			log.debug("Event data feeding started");
			//get New Events To In follow-up
			newEventsToAdd=getNewEventMapToAddInFollowUp( propMap,oDataTableNew,existingDataFromArgusMap);
			for(int i=1;i<=newEventsToAdd.size();i++){
				Thread.sleep(2000);
				dynamicExecuteJavaScriptByText( "table_nav_CF_EVT_id", "code", "(New)");
				feedEventDataForFollowUp( propMap, oDataTableNew,newEventsToAdd, i);
			}
			
			log.debug("Event data feeding completed\n");
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			executeJavaScript( additionalInformationTab);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			checkSDDownloadPage=findMultipleElements( new String[]{dateOfAttachment,pspId});
			if(!checkSDDownloadPage)
			{
				log.error("Attachment page not found");
				log.error("Redirecting to additional info tab");
				executeJavaScript( additionalInformationTab);
			}
			Thread.sleep(Integer.parseInt(propMap.get("minWait")));
			feedAdditionInfoForFollowUp( propMap, oDataTableNew);
			Thread.sleep(Integer.parseInt(propMap.get("waitForSaveCase")));
			enterClickByXpath( "INPUT", "name", caseSaveButton);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
		
		}catch(Exception e){
			log.error("error in follow-up case<"+e.getMessage()+">");
			e.printStackTrace();
		}
	}

	public boolean isAgeGenderAvailable(ParsedDataTable oDataTableNew)
	{
		String 	ageAdd=null,genderAdd=null;
		
		if(oDataTableNew.isColumnPresent("Patient Details.Age"))
		{
			ageAdd=(String) oDataTableNew.getColumnValues("Patient Details.Age").get(0).displayValue().toString();
		}
		
		if(oDataTableNew.isColumnPresent("Patient Details.Age"))
		{
			genderAdd=(String) oDataTableNew.getColumnValues("Patient Details.Gender").get(0).displayValue().toString();
		}
		//if age gender are not provided then create new case
		if(ageAdd.equals("")||genderAdd.equals(""))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public boolean isAerNumAndMahAerNumberDuplicateFound(  Map<String,String> propMap,ParsedDataTable oDataTableNew)
			throws SciException
	{
		String 	caseResultSearchTableID=null;
		String 	resultRowsId=null;
		
		Map<String,Object>existingDataFromArgusMap=null;
		boolean bAerNumAndMahAerNumberDuplicateFound = false;
		int nAerNumAndMahAerNumberSearchResultCount = 0;
		
	
		
		resultRowsId=propMap.get("CreateCase.SeachResult.RowsId");
		caseResultSearchTableID=propMap.get("Search.TableId");
		
		nAerNumAndMahAerNumberSearchResultCount = getAllRowSizeByTableID( "div", "id", caseResultSearchTableID);
		bAerNumAndMahAerNumberDuplicateFound = false;
		
		for(int i=1 ; i <= nAerNumAndMahAerNumberSearchResultCount; i++)
		{
			//click on first case id to open case 
			clickfirstLink(resultRowsId+i);
			
			//get text from argus and make Map
			existingDataFromArgusMap = getTextOfAllTabs( propMap);
			
			//check whether case is duplicate or not by Report Type and Product Details
			bAerNumAndMahAerNumberDuplicateFound = checkDuplicateByReportTypeAndProduct( propMap, oDataTableNew,existingDataFromArgusMap);
		
			if(bAerNumAndMahAerNumberDuplicateFound)
			{
				break;
			}
		}
		return bAerNumAndMahAerNumberDuplicateFound;
	}
	
	public String caseDuplicateOrFollowUpPerform(Map<String,String> propMap,ParsedDataTable oDataTableNew)throws SciException{
		
    	
    	boolean followUpByProAndEvent=false;
		String sAERNumber=null,caseId=null,returnMessage=null;
		
		boolean bIsAgeGenderPresent = false;
		boolean bSomeAerNumAndMahAerNumberMatchResults = false;
		 
		boolean bAerNumAndMahAerNumberDuplicateFound = false;
		
		try{
			
			
			if(oDataTableNew.isColumnPresent("AERNumber"))
				sAERNumber =  (String) oDataTableNew.getColumnValues("AERNumber").get(0).displayValue().toString();
			
			//check duplicate by AER number
			bSomeAerNumAndMahAerNumberMatchResults = duplicateCaseByAERAndMAHAndLikedNumber( propMap, oDataTableNew);
			
			//if any case found then go for get text of argus tab and check data
			if(bSomeAerNumAndMahAerNumberMatchResults)
			{
				bAerNumAndMahAerNumberDuplicateFound = isAerNumAndMahAerNumberDuplicateFound( propMap, oDataTableNew);
			
				caseId=getCaseId( propMap);
				
				if(bAerNumAndMahAerNumberDuplicateFound)
				{
					eProposedAction=PROPOSED_ACTION.TREAT_AS_DUPLICATE;
					returnMessage="Search on AER and MAH AER identified incoming AER "+sAERNumber+" as DUPLICATE of existing Case Id "+caseId.trim();
				}
				else
				{
					eProposedAction=PROPOSED_ACTION.TREAT_AS_FOLLOWUP;
					returnMessage="Search on AER and MAH AER identified incoming AER "+sAERNumber+" as a FOLLOW UP of existing Case Id "+caseId.trim();
					//apotexCaseOperation.followUpCase( propMap, oDataTableNew,existingDataFromArgusMap);
				}
			}
			else
			{
				bIsAgeGenderPresent = isAgeGenderAvailable(oDataTableNew);
				
				if(!bIsAgeGenderPresent)
				{
					//if age and gender is not provided
					eProposedAction=PROPOSED_ACTION.CREATE_NEW_CASE;
					returnMessage="Age/Gender not available � incoming AER "+sAERNumber+" is a NEW CASE";
					//createCase( propMap, oDataTableNew);				
				}
				else
				{
					//check whether case is duplicate or not by patient info and product info and Events info
					followUpByProAndEvent = duplicateCaseByPatInfoProductAndEvents( propMap, oDataTableNew);
					
					//if similar events found then go for follow-up
					if(followUpByProAndEvent){
						caseId=getCaseId( propMap);
						eProposedAction=PROPOSED_ACTION.TREAT_AS_FOLLOWUP;
						returnMessage="Incoming AER " + sAERNumber + " is a FOLLOW-UP of existing Case " + caseId.trim();
						//apotexCaseOperation.followUpCase( propMap, oDataTableNew, existingDataFromArgusMap);
					}else{
						//if no similar events found then go for create new case
						//createCase( propMap, oDataTableNew);
						eProposedAction=PROPOSED_ACTION.CREATE_NEW_CASE;
						returnMessage="No duplicates identified basis AER Number/MAH AER Number and Patient Info/Product/Event\nNo existing cases identified basis AER Number/MAH AER Number and Patient Info/Product/Event\nTherefore, incoming AER " + sAERNumber + "  is a NEW CASE";
					}
				}
					
			}
			
		}catch(Exception e){
			log.error("Error in case duplicate or follow-up<"+e.getMessage()+">");
			e.printStackTrace();
		}
		return returnMessage;
	}

	public  void createCase(CpatToolBase oToolBase, IAction oCallbackAction, Map<String, String> propMap, ParsedDataTable dataTable)throws SciException{
		String initRecptDate = null, country = null, descAsReported = null,
				caseOptionsearch = null, continueButton = null;
		String searchRepType = null, searchPatCountry = null, searchInitRepDate = null, 
				descAsReportedValue = null, selectProduct = null;
		String initRecpDateValue = null, countryValue = null,
				window = null,AER=null,AERAdd=null,prodPath=null,dateTobeParsed=null,reportTypeAdd=null;
		boolean checkDuplicateCasePage = false,prodForBooking=true;
		Map<String, Product>  verifiedProductMap=null;
		Map<String,Map<String,Product>> allProductMap=null;
		ICaseOperations acseOp = null;
		String[] splittedDate=null;
		ProductUtils productUtils=null;
		try {
			productUtils=new ProductUtils();
			
			continueButton = propMap.get("CreateCase.Continue");
			caseOptionsearch = propMap.get("caseaction.dropdown.valueForSearch");
			initRecptDate = propMap.get("CreateCase.initialRecptDate");
			country = propMap.get("CreateCase.Country");
			descAsReported = propMap.get("CreateCase.DescriptionAsReported");
			
			searchRepType = propMap.get("Search.General.ReportType");
			searchPatCountry = propMap.get("Search.Patient.Country");
			searchInitRepDate = propMap.get("Search.InitRecpDate");
			selectProduct = propMap.get("CreateCase.SelectProduct");
			AER=propMap.get("Search.AER");
			reportTypeAdd=propMap.get("HealthCanada.ReporterType");
			
			
			
			
			executeJavaScript( caseOptionsearch);
			prodPath = propMap.get("productCfgPath");
			DataTable ProductExcelData= productUtils.getDataFromProductExcel(prodPath);
			
			//get all product from data table to separate verified product list
			allProductMap=productUtils.PDFVerification(dataTable,ProductExcelData,prodForBooking);
			
			verifiedProductMap=(Map<String,Product>) allProductMap.get("verifiedProductMap");
			
			checkDuplicateCasePage = findMultipleElements(
					new String[] { searchRepType, searchInitRepDate, searchPatCountry });
			if (!checkDuplicateCasePage) {
				log.debug("duplicate search case page not found");
				executeJavaScript( caseOptionsearch);
			}

			if (dataTable.isColumnPresent("Date of Awareness"))
				initRecpDateValue = (String) dataTable.getColumnValues("Date of Awareness").get(0).displayValue().toString();

			if (dataTable.isColumnPresent("Country"))
				countryValue = (String) dataTable.getColumnValues("Country").get(0).displayValue().toString();

			if (dataTable.isColumnPresent("DescriptionasReported1"))
				descAsReportedValue = (String) dataTable.getColumnValues("DescriptionasReported1").get(0).displayValue().toString();
			
			if(dataTable.isColumnPresent("AERNumber"))
				AERAdd =  (String) dataTable.getColumnValues("AERNumber").get(0).displayValue().toString();
			
			//feed initial receive date
			splittedDate=initRecpDateValue.split(" ");
			dateTobeParsed=splittedDate[0];
			findElementByXpathAndSetContent( "name", initRecptDate, dateTobeParsed);
			findElementByXpathAndSetContent("name", country, countryValue);
			//All health canada case reporter type is Regulatory Authority
			findElementByXpathAndSetContent("name", searchRepType, reportTypeAdd);
			
			window = getWindowHandle();
			enterClickByXpath( "INPUT", "name", selectProduct);
			//clickWithEnterKeyElementByName( selectProduct);
			selectProduct( propMap, dataTable, window, 1,verifiedProductMap);
			Thread.sleep(2000);
			findElementByXpathAndSetContent("name", descAsReported, descAsReportedValue);
			findElementByXpathAndSetContent("name", AER, AERAdd);
			
			enterClickByXpath( "INPUT", "name", continueButton);
			Thread.sleep(2000);
			//for checking seriousness,if report is serious or not perform accordingly 
			checkSeriousCriteria( propMap, dataTable);
			window = getWindowHandle();
			enterClickByXpath( "INPUT", "name", propMap.get("CreateCase.BookIn"));
			
			getSetOfWindows( 2);
			Thread.sleep(Integer.parseInt(propMap.get("commonWait")));
			//click yes on pop-up to open case
			yesOrNoOrCancleBtnOnPopup( propMap.get("saveornot.idValue"), window);
			switchParentToChildFrame(propMap.get("server.loginform.parentframe"), propMap.get("server.loginform.childframe"));
			//after create case feed data to that case
			updateCase( propMap, dataTable, "");
			
			nextCase(oToolBase,oCallbackAction, propMap, dataTable,"Case saved successfully");

		} catch (Exception e) {
			log.error("Error in bookIn case<"+e.getMessage()+">");
			e.printStackTrace();
		}
	}
	public void nextCase(CpatToolBase oToolBase, IAction oCallbackAction,Map<String,String> propMap,ParsedDataTable dataTable,String returnMessage) throws SciException{
		
		String ARRNumber=null;
		String sdPath=null;
		CaseStatus caseData=null;
		
		try{
			
			if(dataTable.isColumnPresent("E2BReportDuplicate"))
			 ARRNumber=(String)dataTable.getColumnValues("E2BReportDuplicate").get(0).displayValue().toString();
			String caseStatus="success";
			
			caseData=new CaseStatus();
			caseData.setARRNumber(ARRNumber);
			caseData.setCaseId("DemoCase001");
			caseData.setReportInitial_FU("Initial");
			caseData.setReportSerious("non-Serious");
			caseData.setStatus(caseStatus);
			propMap.put("firstTimeLogin", "false");
			CaseStatusUpdate.updateCsvFile(caseData);
			//processing next case 
			System.out.println("done with updateCase method");
			sdPath = propMap.get("sdpath");
			System.out.println("this is the current sd path which we are deleting" + sdPath);
			String folder = sdPath.substring(0, sdPath.lastIndexOf("\\"));
			File[] fileList = new File(folder).listFiles();
			int nextFileIndex = 0;
			
			String tempPath = sdPath.substring(sdPath.lastIndexOf("\\") + 1, sdPath.length());
			for (int index = 0; index < fileList.length; index++) {
				if (fileList[index].getAbsolutePath().endsWith(tempPath)) {
					nextFileIndex = index + 1;
					if (nextFileIndex < fileList.length) {
						sdPath = fileList[nextFileIndex].getAbsolutePath();
						System.out.println("this is the next SD to be processed after previous one deleted" + sdPath);
						propMap.put("sdpath", sdPath);
						System.out.println(propMap.get("sdpath"));
						int count=Integer.parseInt(propMap.get("count"));
						count++;
						String sCount=""+count;
						propMap.put("count", sCount);
					    continueToNextCase(oToolBase,oCallbackAction, sdPath,returnMessage,dataTable,propMap);
						break;
					}
	
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	public String getCaseId(Map<String,String>propMap){
		String caseId=null, text=null;
		String[] splitted = null;
		caseId=propMap.get("Search.CaseID");
		text=getTextFromXpath( caseId);
		splitted=text.split("-");
		if(splitted[1].contains("\"")){
			splitted=splitted[1].split("\"");
			splitted[1]=splitted[0];
		}
		return splitted[1];
	}

	private final static JButton abortCase() {
		JButton abortButton = new JButton("Abort Case");
		int pad = 20;
		abortButton.setMargin(new Insets(pad, pad, pad, pad));
		abortButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		abortButton.setBackground(new Color(128, 128, 128));
		abortButton.createToolTip();
		abortButton.setToolTipText("Duplicate or Followup Found. Skip the current case and Proceed to the next case");
		abortButton.setForeground(new Color(0, 0, 0));
		abortButton.setBounds(290, 230, 140, 40);
		return abortButton;
	}

	private final static JButton proceedToNextCase() {
		JButton abortButton = new JButton("Proceed to Next Case");
		int pad = 20;
		abortButton.setMargin(new Insets(pad, pad, pad, pad));
		abortButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		abortButton.setBackground(new Color(128, 128, 128));
		abortButton.createToolTip();
		abortButton.setToolTipText("Duplicate or Followup Found. Skip the current case and Proceed to the next case");
		abortButton.setForeground(new Color(0, 0, 0));
		abortButton.setBounds(70, 230, 190, 40);
		return abortButton;
	}
	public void continueToNextCase(final CpatToolBase oToolBase, final IAction oCallbackAction, final String sPdfFilePath, final String parsedHTMLdata,
			final ParsedDataTable oDataTablePageContents, final Map<String, String> userInputMap)
	{
		
		final JFrame frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 300);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		JEditorPane jep;
		String content=null;
		Element body=null;
		String imgsrc=null;
		HTMLEditorKit hek=null;
		HTMLDocument htdoc=null;
		JButton continueButton =null,abortBUtton=null;
		List<PreviewCaseDoc> listCaseDocs = null;
		File file = null;
		
		try 
		{
			listCaseDocs = new ArrayList<>();
			file = new File(sPdfFilePath);
			
			listCaseDocs.add(new PreviewCaseDoc(file, oDataTablePageContents, null));
			
			final List<PreviewCaseDoc> listPreviewCaseDocs = listCaseDocs;
			
			JPanel duplicateCasePanel = new JPanel();
			JButton skipButton = proceedToNextCase();
			JButton abortButton = abortCase();
			
			InputStream initialStream=ClassLoader.getSystemResourceAsStream("sciformix_icon.png");
			try {
				frame.setIconImage(ImageIO.read(initialStream));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			JLabel jlabel = new JLabel(
					"<html>"+parsedHTMLdata+"</html>");
			jlabel.setFont(new Font("Serif", Font.ROMAN_BASELINE, 15));
			jlabel.setForeground(new Color(128, 0, 64));
			jlabel.setBounds(50, 70, 450, 50);
			jlabel.setBorder(BorderFactory.createTitledBorder(""));
			duplicateCasePanel.setLayout(null);
			duplicateCasePanel.add(jlabel);
			duplicateCasePanel.add(skipButton);
			duplicateCasePanel.add(abortButton);
		

			frame.add(duplicateCasePanel);
			frame.setTitle("Sciformix Case Processor Assist Tool (CPAT)");
			frame.setSize(550, 350);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);	
			skipButton.addActionListener(new ActionListener()
			 {
			      public void actionPerformed(ActionEvent ae) 
			      {
			    	 frame.dispose();
			    	 Map<String, Object> params = new HashMap<>();
			    	 try {
			    		// PDFUtilsMain.listofFilesinDir( propMap, sCaseId);
			    		params.put(CpatToolConstants.ContentParams.CASE_DOC_LIST, listPreviewCaseDocs);
			 			params.put(CpatToolConstants.ContentParams.CASE_SUMMARY_DATATABLE, listPreviewCaseDocs.get(0).getDataTable());
			 			params.put(CpatToolConstants.ContentParams.CASE_FILE_TYPE, HEALTH_CANADA_FILE_TYPE);
			 			
			    		CpatToolHome.showPreviewScreen(oToolBase, oCallbackAction, params);
			    		
			    	 } catch (Exception e) {
						e.printStackTrace();
					}finally{
						
					}
			      }
			    });
			abortButton.addActionListener(new ActionListener()
			 {
				 public void actionPerformed(ActionEvent ae) 
			      {
			    	 
			    	frame.dispose();
			    	
			    	switchParentToChildFrame(userInputMap.get("server.loginform.parentframe"), userInputMap.get("server.loginform.childframe2"));
					log.debug("log out process");
					try {
						Thread.sleep(Integer.parseInt(userInputMap.get("waitForLogOut")));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					try {
						clickElementByXpath("INPUT","name", userInputMap.get("page.logoutbtn.value"));
						switchWindowHandler(getWindowHandle());
						enterClickByXpath("INPUT","name", userInputMap.get("saveornot.idValue"));
					} catch (Exception e) {
						log.error("Error in logout"+e.getStackTrace());
						e.printStackTrace();
					}
//			    	getLogin();
			      }
			    });
		    
		} catch (Exception e) 
		{
			log.error("Error in duplicate case page due to <"+e.getMessage()+" >");
		}
	
	}



}
