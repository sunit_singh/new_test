package com.sciformix.client.automation;


import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
// JAXP packages
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Entity;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class BasicXMLParser {

    /** Output goes here */
//    private PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out), true);
    private PrintWriter out = new PrintWriter(new NullOutput(), true);

    /** Indent level */
    private int indent = 0;

    /** Indentation will be in multiples of basicIndent  */
    private final String basicIndent = "  ";

	private Node RootNode;

	private static final char Ch = '.';
	private static final char SeqStartCh = '[';
	private static final char SeqEndCh = ']';

	private boolean DebugMode = false;

    static String DTDPath = null;
    static EntityResolver er = null;
    static String DTDUrlPath = null;
    static boolean isURL = false;

    public static void setDTDUrlPath(String urlPath)
    {
      DTDUrlPath = urlPath;
      if( !DTDUrlPath.trim().equals("") ) {
        er = new EntityResolver(DTDUrlPath);
        isURL = true;
      }
    }

    public static void setDTDPath(String Path)
    {
      DTDPath = Path;
      if( !DTDPath.trim().equals("") ) {
        er = new EntityResolver(DTDPath);
        isURL = false;
      }
    }

	public void setDebugMode(java.io.OutputStream os)
	{
		if( ! DebugMode )
		{
			out = new PrintWriter(new OutputStreamWriter(os), true);
			DebugMode = true;
		}
	}

    public BasicXMLParser(Node n)
    {
		RootNode = n;
	}

    public BasicXMLParser(java.io.Reader reader) throws IOException, SAXException {

        InputSource is = new InputSource(reader);
        basicXMLParser(is);
        reader.close();
    }

    public BasicXMLParser(java.io.InputStream ios) throws IOException, SAXException {

        InputSource is = new InputSource(ios);
        basicXMLParser(is);
        ios.close();
    }

//    public BasicXMLParser(String DTDPath,String systemId) throws IOException, SAXException {
//
//      EntityResolver er =  new EntityResolver(DTDPath);
//      InputSource is = er.resolveEntity("",systemId);
//      basicXMLParser(is);
//    }

    public Node getRootNode ()
    {
      return (RootNode);
    }

    private void basicXMLParser(InputSource is) throws IOException, SAXException {

        DocumentBuilderFactory dbf =
            DocumentBuilderFactory.newInstance();

        // Optional: set various configuration options
        dbf.setValidating(false);
        dbf.setIgnoringComments(true);
        dbf.setIgnoringElementContentWhitespace(false);
        dbf.setCoalescing(false);
        // The opposite of creating entity ref nodes is expanding them inline
        dbf.setExpandEntityReferences(true);

        // At this point the DocumentBuilderFactory instance can be saved
        // and reused to create any number of DocumentBuilder instances
        // with the same configuration options.

        // Step 2: create a DocumentBuilder that satisfies the constraints
        // specified by the DocumentBuilderFactory
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
            if( er != null ){
              db.setEntityResolver(er);
            }
        } catch (ParserConfigurationException pce) {
            System.err.println(pce);
            System.exit(1);
        }

/*
        // Set an ErrorHandler before parsing
        OutputStreamWriter errorWriter =
            new OutputStreamWriter(System.err, outputEncoding);
        db.setErrorHandler(
            new MyErrorHandler(new PrintWriter(errorWriter, true)));
*/
        // Step 3: parse the input file
        Document doc = null;
        try {
            doc = db.parse(is);
        } catch (SAXException se) {
            System.err.println(se.getMessage());
            throw se;
          //System.exit(1);
        }
//        is.close();
        RootNode = doc;
    }



    public BasicXMLParser(String Filename) throws IOException, SAXException
    {
      this(new FileInputStream(Filename));
    }
    public BasicXMLParser(byte[] buffer) throws IOException, SAXException
    {
      this(new ByteArrayInputStream(buffer));
    }

    /**
     * Echo common attributes of a DOM2 Node and terminate output with an
     * EOL character.
     */
    private void printlnCommon(Node n) {
        out.print(" nodeName=\"" + n.getNodeName() + "\"");

        String val = n.getNamespaceURI();
        if (val != null) {
            out.print(" uri=\"" + val + "\"");
        }

        val = n.getPrefix();
        if (val != null) {
            out.print(" pre=\"" + val + "\"");
        }

        val = n.getLocalName();
        if (val != null) {
            out.print(" local=\"" + val + "\"");
        }

        val = n.getNodeValue();
        if (val != null) {
            out.print(" nodeValue=");
            if (val.trim().equals("")) {
                // Whitespace
                out.print("[WS]");
            } else {
                out.print("\"" + n.getNodeValue() + "\"");
            }
        }
        out.println();
    }

    /**
     * Indent to the current level in multiples of basicIndent
     */
    private void outputIndentation() {
        for (int i = 0; i < indent; i++) {
            out.print(basicIndent);
        }
    }


	public String getNodeValue(String Path) {

/*
		out.println( "Printing Root node." );
		echo(RootNode);
		out.println( "Printing Over." );
*/
		out.println( "Getting node <" + Path + ">..." );
		Node n = getNode(Path, RootNode);
        return getValueForNode(n);
	}

	private String getValueForNode(Node n) {

		if( n != null )
		{
			n = n.getFirstChild();
//			out.println( "Node type <" + n.getNodeType() + ">" );

			if( n != null )
				return n.getNodeValue();
		}

		return null;
	}

	public Node getNode(String Path) {
		out.println( "Getting node <" + Path + ">..." );
		return getNode(Path, RootNode);
	}


	private Node getNode(String Path, Node n)
	{
        int type = n.getNodeType();
		String RemainingPath = null;
		String tmp = null;
		int Index = 0;


		if( Path == null )
		{
			out.println( "Node type <" + type + ">" );
			return n;
		}

		int Idx = Path.indexOf(Ch);
		if( Idx == -1 )
		{
			tmp = Path;
			Idx = Path.length();
		}
		else
		{
			RemainingPath = Path.substring( Idx+1, Path.length() );
		}

		tmp = Path.substring( 0, Idx );

/*      Search for [ ] and if its present then get the index */
		int SeqStartIdx = tmp.indexOf(SeqStartCh);
		if( SeqStartIdx != -1 )
		{
			int SeqEndIdx = tmp.indexOf(SeqEndCh);
			if( SeqEndIdx == -1 )
			{
				out.println( "End of seq block ']' not found for node <" + tmp + ">" );
				return null;
			}

			Index = Integer.parseInt(tmp.substring(SeqStartIdx+1, SeqEndIdx));
			out.println( "Index <" + Index + ">" );
			tmp = tmp.substring(0,SeqStartIdx);
		}

		out.println( "Rem path <" + RemainingPath + ">" );
		out.println( "tmp path <" + tmp + ">" );

		int Found = 0;
        for (Node child = n.getFirstChild(); child != null;
             child = child.getNextSibling()) {
			if( child.getNodeName().compareToIgnoreCase(tmp) == 0 )
			{
				if( Found == Index )
				{
					out.println( "Found node <" + tmp + ">" );
					return getNode(RemainingPath, child);
				}
				Found++;
			}
		}

		out.println( "No node with name <" + tmp + ">" );
		return null;
	}

/* Reads a node with the following structure

    <PROPERTIES>
        <Property>
            <KEY> </KEY>
            <VALUE></VALUE>
        <Property>
        <Property>
            <KEY> </KEY>
            <VALUE></VALUE>
        <Property>
    </PROPERTIES>

    Returns null if there is a problem in the structure
*/
    public Properties createProperties(Node node)
    {
        Properties p = new Properties();
        String Key, Value;
/*
        node = getNode("Properties", node);
        if(node == null)
            return null;
*/
        Node n = null;
        Node keynode = null;
        Node valuenode = null;

        for(int i=0;  ; i++ )
        {

            n = getNode("Property[" + i + "]", node);
            if( n == null )
            {
                if( i == 0 )
                {
                    return null;
                }

                return p;
            }

            keynode = getNode("Key", n);
            if( keynode == null )
                return null;

            Key = getValueForNode(keynode);
            if( Key == null )
                return null;

            valuenode = getNode("Value", n);
            if( valuenode == null )
                return null;

            Value = getValueForNode(valuenode);
            if( Value == null )
                return null;
            p.setProperty(Key,Value);
        }

//        return null;
    }

    /**
     * Recursive routine to print out DOM tree nodes
     */
    private void echo(Node n) {
        // Indent to the current level before printing anything
        outputIndentation();

        int type = n.getNodeType();
		out.print(type);
        switch (type) {
        case Node.ATTRIBUTE_NODE:
            out.print("ATTR:");
            printlnCommon(n);
            break;
        case Node.CDATA_SECTION_NODE:
            out.print("CDATA:");
            printlnCommon(n);
            break;
        case Node.COMMENT_NODE:
            out.print("COMM:");
            printlnCommon(n);
            break;
        case Node.DOCUMENT_FRAGMENT_NODE:
            out.print("DOC_FRAG:");
            printlnCommon(n);
            break;
        case Node.DOCUMENT_NODE:
            out.print("DOC:");
            printlnCommon(n);
            break;
        case Node.DOCUMENT_TYPE_NODE:
            out.print("DOC_TYPE:");
            printlnCommon(n);

            // Print entities if any
            NamedNodeMap nodeMap = ((DocumentType)n).getEntities();
            indent += 2;
            for (int i = 0; i < nodeMap.getLength(); i++) {
                Entity entity = (Entity)nodeMap.item(i);
                echo(entity);
            }
            indent -= 2;
            break;
        case Node.ELEMENT_NODE:
            out.print("ELEM:");
            printlnCommon(n);

            // Print attributes if any.  Note: element attributes are not
            // children of ELEMENT_NODEs but are properties of their
            // associated ELEMENT_NODE.  For this reason, they are printed
            // with 2x the indent level to indicate this.
            NamedNodeMap atts = n.getAttributes();
            indent += 2;
            for (int i = 0; i < atts.getLength(); i++) {
                Node att = atts.item(i);
                echo(att);
            }
            indent -= 2;
            break;
        case Node.ENTITY_NODE:
            out.print("ENT:");
            printlnCommon(n);
            break;
        case Node.ENTITY_REFERENCE_NODE:
            out.print("ENT_REF:");
            printlnCommon(n);
            break;
        case Node.NOTATION_NODE:
            out.print("NOTATION:");
            printlnCommon(n);
            break;
        case Node.PROCESSING_INSTRUCTION_NODE:
            out.print("PROC_INST:");
            printlnCommon(n);
            break;
        case Node.TEXT_NODE:
            out.print("TEXT:");
            printlnCommon(n);
            break;
        default:
            out.print("UNSUPPORTED NODE: " + type);
            printlnCommon(n);
            break;
        }

        // Print children if any
        indent++;
        for (Node child = n.getFirstChild(); child != null;
             child = child.getNextSibling()) {
            echo(child);
        }
        indent--;
    }

    private static void usage() {
        System.err.println("Usage: BasicXMLParser <FileName> [Node Path] <Path>");
        System.exit(1);
    }

//START : Added for reading attribute from xml tag on 200611115
    public boolean hasAttributes(Node N)
    {
        return (N.hasAttributes());
    }

    public int getAttributeCount(Node N)
    {
        int Length = 0;

        NamedNodeMap NNM = N.getAttributes();

        if(NNM != null)
        {
            Length = NNM.getLength();
        }

        return Length;
    }

    public Node getAttributeNode(Node N, String AttributeName)
    {
        Node AttributeNode = null;

        if((N.hasAttributes()) == true)
        {
            NamedNodeMap NNM = N.getAttributes();

            if(NNM != null)
            {
                AttributeNode = NNM.getNamedItem(AttributeName);
            }
        }

        return AttributeNode;
    }

    public Node getAttributeNode(Node N, int Position)
    {
        Node AttributeNode = null;

        if((N.hasAttributes()) == true)
        {
            NamedNodeMap NNM = N.getAttributes();

            if(NNM != null)
            {
                int Length = NNM.getLength();

                if(Position < Length)
                {
                    AttributeNode = NNM.item(Position);
                }
            }
        }

        return AttributeNode;
    }
//END : Added for reading attribute from xml tag on 200611115

    public static void main(String[] args) throws Exception {
        String filename = null;

		filename = args[0];
		String Path = null;
		String Path1 = null;
		if( args.length > 1 )
			Path = args[1];

		if( args.length > 2 )
			Path1 = args[2];

        if (filename == null ) {
            usage();
        }


		BasicXMLParser dm =  new BasicXMLParser(filename);
		if( Path1 == null )
		{
			System.out.println( "Node value <" + dm.getNode(Path) + ">" );
		}
		else
		{
			System.out.println( "Trying to get the node <" + Path + ">" );
			Node n = dm.getNode(Path);
			if( n == null )
			{
				System.out.println( "No node found for <" + Path + ">" );
				System.exit(1);
			}
			BasicXMLParser dm1 =  new BasicXMLParser(n);
			dm1.setDebugMode(System.out);
			System.out.println( "Node value <" + dm1.getNodeValue(Path1) + ">" );
		}

    }

}

final class NullOutput extends java.io.OutputStream
{
	public void close() { }
	public void flush() { }
	public void write(byte[] b) { }
	public void write(byte[] b, int off, int len) { }
	public void write(int b) { }
}

class EntityResolver implements org.xml.sax.EntityResolver
{
  public String DTDPath         = "";

  public EntityResolver(String DTDPath)
  {
    this.DTDPath = DTDPath;
  }

  public InputSource resolveEntity(String publicId, String systemId) throws SAXException
  {
    java.net.URL url;
    InputStream is = null;
    String file = "";

    System.out.println("Public ID <"+ publicId + "> System ID <" + systemId + ">");

    if( BasicXMLParser.isURL == false )
    {
      if ( systemId != null )
      {
        file = DTDPath + System.getProperty("file.separator") + systemId;
      }else if( publicId != null ){
         file = DTDPath + System.getProperty("file.separator") + publicId;
      }

      try {
        is = new BufferedInputStream(new FileInputStream(file));
      }
      catch (IOException ioe) {
        ioe.printStackTrace();
      }

      if (is != null)
        return new InputSource(is);

      throw new SAXException("XML configuration DTD not found<FILE mode>: " + file);
    }
    else
    {
      if ( systemId != null )
      {
        file = DTDPath + "/" + systemId;
      }else if( publicId != null ){
        file = DTDPath + "/" + publicId;
      }

      try{
        url = new java.net.URL(file);
      }catch(java.net.MalformedURLException mfe){
        throw new SAXException("URL is not correct: " + DTDPath);
      }

      try{
        is = url.openStream();
      }catch (IOException ioe) {
        ioe.printStackTrace();
      }

      if (is != null)
        return new InputSource(is);
      throw new SAXException("XML configuration DTD not found<URL mode>: " +DTDPath);
    }

//    throw new SAXException("External entites are not permitted in XML configuration files");
  }
}

