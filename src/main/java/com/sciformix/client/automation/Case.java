package com.sciformix.client.automation;

import java.util.HashMap;
import java.util.Map;

public class Case
{
	private String m_sCaseId = null;
	Map<String, Object> m_mapCaseData = null;
	
	public Case(String p_sCaseId)
	{
		m_sCaseId = p_sCaseId;
		m_mapCaseData = new HashMap<String, Object>();
	}
	
	public void storeValue(String p_sAttributeName, Object p_oValue)
	{
		m_mapCaseData.put(p_sAttributeName, p_oValue);
	}
	
	public String caseId()
	{
		return m_sCaseId;
	}
	
	public Object getAttributeValue(String p_sAttributeName)
	{
		return m_mapCaseData.get(p_sAttributeName);
	}
}
