package com.sciformix.client.automation;

import java.io.File;
import java.util.Map;

public class CaseDocument 
{
	private File oCaseFile;
	private Map<String , Object> oAttributeMap ;
	
	public File getPath() {
		return oCaseFile;
	}
	public void setPath(File oFile) {
		this.oCaseFile = oFile;
	}
	public Map<String, Object> getoAttributeMap() {
		return oAttributeMap;
	}
	public void setoAttributeMap(Map<String, Object> oAttributeMap) {
		this.oAttributeMap = oAttributeMap;
	}
	
}
