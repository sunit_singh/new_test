package com.sciformix.client.automation;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.rulesengine.Criterion;
import com.sciformix.commons.rulesengine.Criterion.Conjunction;
import com.sciformix.commons.rulesengine.Rule;


public class WebApplicationWriterUtils 
{
	private static final Logger log 			= LoggerFactory.getLogger(WebApplicationWriterUtils.class);

	public static String getBrowserLocation(String p_sBrowserDriverLocation)
	{
		String sDriverLocation = null;
		
		File oBrowserDriverFile = null;
		
		oBrowserDriverFile = getFileByInputStream(p_sBrowserDriverLocation);
		
		sDriverLocation = oBrowserDriverFile.getAbsolutePath();
		
		return sDriverLocation;
	}
	
	private static File getFileByInputStream(String p_sBrowserDriverLocation)
	{
		
		InputStream m_oInputStreamFordriver = null;
		File oDriverFile = null;
		
		try 
		{
			m_oInputStreamFordriver = ClassLoader.getSystemResourceAsStream(p_sBrowserDriverLocation);
			oDriverFile = convertingInputStreamToFile(m_oInputStreamFordriver , p_sBrowserDriverLocation);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return oDriverFile;
	}

	public static File convertingInputStreamToFile(InputStream initialStream,String p_sFileName) throws IOException 
	{
		File targetFile=null;
		String m_sExtension = null;
		String m_sFileName = null;
		try
		{
			m_sFileName = p_sFileName.split("\\.")[0];
			m_sExtension = p_sFileName.split("\\.")[1];
			
			targetFile = File.createTempFile(m_sFileName, "."+m_sExtension);
			
			targetFile.deleteOnExit();
			
			FileUtils.copyInputStreamToFile(initialStream, targetFile);
		
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return targetFile;
	}
	public int getAge(String p_sFormat , String p_sInput) throws ParseException 
	{
		
		SimpleDateFormat sdf = null;
		Calendar dob = null, today = null;
		int curYear = 0, dobYear = 0, age = 0, curMonth = 0, dobMonth = 0, curDay = 0, dobDay = 0;

		sdf = new SimpleDateFormat(p_sFormat);
		dob = Calendar.getInstance();
		dob.setTime(sdf.parse(p_sInput));
		today = Calendar.getInstance();
		curYear = today.get(Calendar.YEAR);
		dobYear = dob.get(Calendar.YEAR);
		
		age = curYear - dobYear;
		
		// if dob is month or day is behind today's month or day
		curMonth = today.get(Calendar.MONTH);
		dobMonth = dob.get(Calendar.MONTH);
		
		if (dobMonth > curMonth) 
		{ 
			// this year can't be counted!
			age--;
		}
		else if (dobMonth == curMonth) 
		{
			// same month? check for day
			curDay = today.get(Calendar.DAY_OF_MONTH);
			dobDay = dob.get(Calendar.DAY_OF_MONTH);
			if (dobDay > curDay) { // this year can't be counted!
				age--;

			}
		}
		return age;
    }

	public static boolean evaluateCriteria(List<Criterion> listCriteria, Map<String, Object> mapRuntimeValues) throws SciException
	{
		Rule oRule = null;
		
		oRule = new Rule(null);
		for (int nCriteriaCounter = 0; nCriteriaCounter < listCriteria.size(); nCriteriaCounter++)
		{
			if (nCriteriaCounter == 0)
			{
				oRule.add(listCriteria.get(nCriteriaCounter));
			}
			else
			{
				oRule.add(Conjunction.AND, listCriteria.get(nCriteriaCounter));
			}
		}
		return oRule.evalute(mapRuntimeValues);
	}
	
	public static boolean deleteDirectories(String p_sFilePath)
	{
		Path path = null;
		path = Paths.get(p_sFilePath);
		boolean bDeleted = false;
        
        if (Files.exists(path)) 
        {
			File dir = new File(p_sFilePath);
			
			if(dir.isDirectory() == false)
			{
				log.info("Not a directory. Do nothing");
				return bDeleted;
			}
			
			File[] listFiles = dir.listFiles();
			
			for(File file : listFiles)
			{
				log.info("Deleting "+file.getName());
				file.delete();
				bDeleted = true;
			}
        }
		return bDeleted;
	}
	
	
	public static boolean createDirectories(String sPath)
	{
		Path path = null;
		path = Paths.get(sPath);
        boolean bDirectoryCreated = false;
		
        
        try
        {
			//if directory exists?
	        if (Files.exists(path)) 
	        {
	        	deleteDirectories(sPath);
	        }
	        
        	Files.createDirectories(path);
        	bDirectoryCreated = true;
        }
        catch (IOException e)
        {
        	//fail to create directory
        	e.printStackTrace();
        	log.error("Fail to create directory "+sPath);
        	
        }
        
        return bDirectoryCreated;
	}

	public static Set<String> getLatestOccuredWindow(Set<String> p_oOldSetOfWindow, Set<String> p_oNewSetOfWindows) 
	{
	    Set<String> latestSetOfWindows = new TreeSet<String>();
	    
	    for(String sCurrentWindowFromNewSet: p_oNewSetOfWindows)
	    {
		    if(!p_oOldSetOfWindow.contains(sCurrentWindowFromNewSet))
		    {
		    	latestSetOfWindows.add(sCurrentWindowFromNewSet);
		    }
	    }
	    
	    return latestSetOfWindows;
	}
}
