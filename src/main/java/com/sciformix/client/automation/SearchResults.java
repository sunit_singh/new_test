package com.sciformix.client.automation;

import com.sciformix.commons.utils.DataTable;

public class SearchResults
{

	public DataTable m_oDataTable = null;
	
	public SearchResults() 
	{
		m_oDataTable = new DataTable();
	}
	
	public boolean resultsFound()
	{
		return (resultCount() > 0);
	}

	public int resultCount()
	{
		if(m_oDataTable != null)
		{
			
			if (m_oDataTable.colCount() > 0)
			{
				return m_oDataTable.getColumnValues(0).size();
			}
			else
			{
				//TODO:Error handling
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}

	public Object getValue(int p_nRowNumber, String p_sColumnName)
	{
		//TODO:CHeck for valid inputs
		return m_oDataTable.getColumnValues(p_sColumnName).get(p_nRowNumber);
	}
	
	public DataTable getDataTable()
	{
		return m_oDataTable;
	}
	
	public void insertValue(String p_sKey, String p_sValue){
		
		if(m_oDataTable.isColumnPresent(p_sKey))
		{
			m_oDataTable.appendColumnValue(p_sKey, p_sValue);
		}
		else
		{
			m_oDataTable.addColumn(p_sKey, p_sValue);
		}
	}
}
