package com.sciformix.client.automation;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.commons.utils.StringUtils;


public class BaseWebApplicationWriter
{
	private static final Logger log 			= LoggerFactory.getLogger(BaseWebApplicationWriter.class);
	
	private static Map<String,String> cpatProperties = new HashMap<>();
	
	public static enum BROWSER
	{
		IE, CHROME;
	}
	
	private BROWSER m_eBrowser = null;
	private String m_sBrowserDriver = null;
	private String m_sBrowserDriverLocation = null;
	private boolean m_bUseCapabilityFlag = false;
	private String m_sDownloadsFolder = null;
	private WebDriver driver = null;
	
	private static String STALE_REF_ATTEMPT_COUNT = "stale.reference.attempt";
	private static String DRIVER_TIMEOUT = "webdriver.timeout";
	private static String DRIVER_VISIBILITY_TIMEOUT = "webdriver.visibility.timeout";
	
	private static int driverVisibilityTimeOut = 0;
	private static int driverTimeout = 0;
	private static int attemptCount= 0;

	
	protected BaseWebApplicationWriter(BROWSER p_eBrowser , String p_sBrowserPortName, String p_sBrowserDriverLocation , String p_sDownloadsFolder)
	{
		this(p_eBrowser, p_sBrowserPortName, p_sBrowserDriverLocation, true , p_sDownloadsFolder);
	}
	
	protected BaseWebApplicationWriter(BROWSER p_eBrowser, String p_sBrowserDriver, String p_sBrowserDriverLocation, boolean p_bUseCapabilityFlag , String p_sDownloadsFolder)
	{
		
		m_eBrowser = p_eBrowser;
		
		if (p_sBrowserDriver != null && p_sBrowserDriverLocation != null)
		{
			m_sBrowserDriver = p_sBrowserDriver;
			m_sBrowserDriverLocation = p_sBrowserDriverLocation;
			m_bUseCapabilityFlag = p_bUseCapabilityFlag;
			m_sDownloadsFolder = p_sDownloadsFolder;
		}
		else
		{
			m_sBrowserDriver = "p_sBrowserDriver";
			m_sBrowserDriverLocation = "p_sBrowserDriverLocation";
			m_sDownloadsFolder = "p_sDownloadsFolder;";
			m_bUseCapabilityFlag = false;
		}
		
		driverTimeout = Integer.parseInt(cpatProperties.get(DRIVER_TIMEOUT));
		attemptCount =  Integer.parseInt(cpatProperties.get(STALE_REF_ATTEMPT_COUNT));
		driverVisibilityTimeOut = Integer.parseInt(cpatProperties.get(DRIVER_VISIBILITY_TIMEOUT));
	}
	
	protected BaseWebApplicationWriter(WebDriver p_sBrowserDriver)
	{
		driver = p_sBrowserDriver;
	}
	
	public BaseWebApplicationWriter() {
		// TODO Auto-generated constructor stub
	}

	protected static Map<String, String> loadProperties(String p_sPropertiesFileName)
	{
		Map<String, String> propMap = null;
		Properties configProp = null;
		
		propMap = new HashMap<String, String>();
		InputStream inputStream = BaseWebApplicationWriter.class.getClassLoader().getResourceAsStream(p_sPropertiesFileName);
        try 
        {
        	configProp = new Properties();
            configProp.load(inputStream);
            
            for (final String name: configProp.stringPropertyNames())
			    propMap.put(name, configProp.getProperty(name));
        
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
        
        return propMap;
	}

	public WebDriver launchBrowser(String p_sDesiredUrl)
	{
		driver = launchBrowser();
		navigateToURL(p_sDesiredUrl);
		
		return driver;
	}
	
	public WebDriver launchBrowser()
	{
		DesiredCapabilities oDesiredBrowserCapabilities = null;
		ChromeOptions oBrowserOptions = null;
		HashMap<String, Object> l_mChromePrefs = null;
		HashMap<String, Object> plugin = null;
		
		System.setProperty(m_sBrowserDriver, m_sBrowserDriverLocation);
		
		if (m_eBrowser == BROWSER.IE)
		{
			oDesiredBrowserCapabilities = DesiredCapabilities.internetExplorer();
			oDesiredBrowserCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, m_bUseCapabilityFlag);
			
			driver = new InternetExplorerDriver(oDesiredBrowserCapabilities);

			driver.manage().window().maximize();
			
		}
		else if (m_eBrowser == BROWSER.CHROME)
		{
			
			plugin = new HashMap<String, Object>();
			plugin.put("enabled", false);
			plugin.put("name", "Chrome PDF Viewer");
			
			l_mChromePrefs = new HashMap<String, Object>();
			l_mChromePrefs.put("profile.default_content_settings.popups", 0);
			l_mChromePrefs.put("download.default_directory", m_sDownloadsFolder);
			l_mChromePrefs.put("plugins.plugins_list", Arrays.asList(plugin));
			
			oDesiredBrowserCapabilities = DesiredCapabilities.chrome();
			oBrowserOptions = new ChromeOptions();
			oBrowserOptions.addArguments("test-type", "start-maximized", "no-default-browser-check");  
			oBrowserOptions.addArguments("test-type");
			oBrowserOptions.addArguments("start-maximized");
			oBrowserOptions.addArguments("disable-infobars");
			oBrowserOptions.addArguments("--disable-extensions"); 
			
			oBrowserOptions.setExperimentalOption("prefs", l_mChromePrefs);
			
			if (m_bUseCapabilityFlag)
			{
				oDesiredBrowserCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				oDesiredBrowserCapabilities.setCapability(ChromeOptions.CAPABILITY, oBrowserOptions);
			}
			
			driver = new ChromeDriver(oDesiredBrowserCapabilities);
			
		}
		else
		{
			//Incorrect value
		}
		return driver;
	}
	
	public Map<String , String> getBrowserdetails()
	{
		Capabilities capabilities = null;
		String browserName = null , browserVersion = null;
		Map<String , String> browserdetailsMap = null;

		browserdetailsMap = new HashMap<>();
		
		capabilities = ((RemoteWebDriver) driver).getCapabilities();
		browserName = capabilities.getBrowserName();
		browserVersion = capabilities.getVersion();
		
		browserdetailsMap.put(CpatToolConstants.BrowserDetails.BROWSERVERSION , browserVersion);
		browserdetailsMap.put(CpatToolConstants.BrowserDetails.BROWSERNAME, browserName);
		
		return browserdetailsMap;
	}
	
	public void navigateToURL(String sUrl) 
	{
		driver.get(sUrl);
		log.info("navigate url success");
	}	
	
	protected void findElementByXpathAndSetContent(String p_sIdentifier, String p_sValue, String name)
	{
		int attempt=0;
		WebDriverWait wait =null;
		WebElement element=null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//INPUT[@" + p_sIdentifier + "=" + "'" + (p_sValue!=null?p_sValue:"") + "'" + "]"))));
				element.clear();
				element.sendKeys(name);
				element.sendKeys(Keys.TAB);
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element with Xpath: " + p_sValue, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
			
		}
		while(attempt<attemptCount);
	}
	
	protected void findElementByAbsoluteXpathAndSetContent(String p_sXPath,String p_sText) {
		
		int attempt = 0;
		WebDriverWait wait=null;
		WebElement element=null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p_sXPath)));
				element.sendKeys(p_sText);
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element with Xpath: " + p_sXPath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}	
	
	protected boolean findElements(String name){

		boolean elementFound=false;
		WebDriverWait wait =null;
		WebElement element=null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		element=wait.until(ExpectedConditions
			.visibilityOfElementLocated((By.xpath("//INPUT[@name='" + name + "']"))));
		
		if(element!=null)
			elementFound = true;
		
		return elementFound;
	}
	
	protected WebElement getElement(String type,String identifier, String name){

		WebDriverWait wait =null;
		
		WebElement element=null;
		wait = new WebDriverWait(driver, driverTimeout);
		element=wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//" + type + "[@" + identifier + "='" + name + "']"))));
		
		return element;
	}

	protected String getTextByElement(WebElement p_oElement){

		String text=null;
		
		text=p_oElement.getText();
		
		return text;
	}
	
	protected String getTextOfValueAttributeElementByXpath(String p_sIdentifier, String xpathByName){
		int attempt = 0;
		String text=null;
		
		WebDriverWait wait =null;
		WebElement element =null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try 
			{
				element= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//INPUT[@" +p_sIdentifier+ "='" + xpathByName + "']")));
				
				if(element != null)
					text=element.getAttribute("value");
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element with Xpath: " + xpathByName, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
			
		}
		while(attempt<attemptCount);
		
		return text;
	}
	
	protected String getTextElementByXpath(String p_sType,String p_sIdentifier, String xpathByName){
		
		int attempt = 0;
		String text=null;
		
		WebDriverWait wait =null;
		WebElement element =null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try 
			{
				element= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//" + p_sType + "[@" +p_sIdentifier+ "='" + xpathByName + "']")));
				
				if(element != null)
					text=element.getText();
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element with Xpath: " + xpathByName, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		
		return text;
	}
	
	protected String getTextWithoutVisibilityByXpath(String p_sXpath){
		int attempt = 0;
		String text=null;
		WebElement element =null;
		
		do
		{
			try 
			{
				element= driver.findElement(By.xpath(p_sXpath));
				if(element != null)
					text=element.getText();
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element with Xpath: "+ p_sXpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		
		return text;
	}
	
	protected void enterClickByXpath(String type, String identifier, String value) {
		int attempt = 0;
		WebDriverWait wait =null;
		WebElement element =null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						(By.xpath("//" + type + "[@" + identifier + "=" + "'" + value + "'" + "]"))));
				element.sendKeys(Keys.ENTER);
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element with Xpath: "+ value, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	protected void enterClickByAbsoluteXpath(String p_xPath) {
		int attempt= 0;
		WebDriverWait wait =null;
		WebElement element =null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						(By.xpath(p_xPath))));
				element.sendKeys(Keys.ENTER);
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element with Xpath: "+ p_xPath, excp );
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	protected void enterClickByXpathIfElementEnabled(String type, String identifier, String value) {
		int attempt= 0;
		WebDriverWait wait =null;
		WebElement element =null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(
						(By.xpath("//" + type + "[@" + identifier + "=" + "'" + value + "'" + "]"))));

				if(element.isEnabled())
					element.sendKeys(Keys.ENTER);
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ value, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	protected void doubleClickOnElementById(String p_sId)throws InterruptedException{
		int attempt= 0;
		Actions action =null;
		WebDriverWait wait =null;
		WebElement element=null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		action = new Actions(driver);
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id(p_sId)));
				action.moveToElement(element).build().perform();
				action.doubleClick().perform();
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}	
	
	protected void clickElementByXpath(String type,String p_sIdentifier, String p_sValue) {
		int attempt = 0;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try 
			{
				WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//" + type + "[@" + p_sIdentifier + "=" + "'" + p_sValue + "'" + "]")));
				element.click();
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sValue, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	protected void clickElementByAbsolutePath(String p_sXPath) {
		int attempt = 0;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		WebElement element = null;
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p_sXPath)));
				element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(p_sXPath)));

				element.click();
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sXPath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	protected void clickElementWithSpaceKeyByValueAttribute(String name,String p_sValueAttribute) {
		int attempt = 0;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		List<WebElement> elements = null;
		do
		{
			try 
			{
				elements =  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name(name)));
				
				for(WebElement element :elements)
				{
					if(element.getAttribute("value").equalsIgnoreCase(p_sValueAttribute)){
						if(!(element.isSelected())){
							element.sendKeys(Keys.SPACE);
						}
					} 
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ name, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	

	protected void clickCheckboxByValueAttribute(String value, String p_sValueAttribute) {
		int attempt = 0;
		List<WebElement> elements = null;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		do
		{
			try 
			{
				elements = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//INPUT[@name='" + value + "']")));
				for(WebElement element:elements){
					if(element.getAttribute("value").equalsIgnoreCase(p_sValueAttribute)){
						if(!(element.isSelected())){
							element.sendKeys(Keys.SPACE);
						}
					}
				}
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ value, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	protected void selectElementWithSpaceKey(String name) {
		int attempt = 0;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
		
		do
		{
			try 
			{
				if(!(element.isSelected())){
					element.sendKeys(Keys.SPACE);
				}
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ name, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);

	}
	
	
	protected void deSelectElementWithSpaceKey(String name) {
		
		int attempt = 0;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);

		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
		do
		{
			try 
			{
				if(element.isSelected()){
					element.sendKeys(Keys.SPACE);
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ name, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	
	protected int getAllRowSizeByTableID(String p_sType, String p_sIdentifier, String p_sTableId) {
		
		List<WebElement>rows = null , sBaseElement = null ;
		
		WebDriverWait wait=null;
		
		wait=new WebDriverWait(driver, 10);
		
		sBaseElement =  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//" + p_sType + "[@" + p_sIdentifier + " = '" + p_sTableId + "']")));
		
		for(WebElement element : sBaseElement)
		{
			wait.until(ExpectedConditions.visibilityOf(element));
			rows = element.findElements(By.tagName("tr"));
		}
		
		return rows.size();
	}	
	
	protected int getAllRowSizeByTableBodyXpath(String p_sTableBodyXpath) 
	{
		List<WebElement> sBaseElement = null;
		WebDriverWait wait = null;
		
		int size = 0, attempt = 0;
		
		wait = new WebDriverWait(driver, 10);
		
		do
		{
			try 
			{
				sBaseElement = wait.until(
						ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(p_sTableBodyXpath + "/tr")));
				size = sBaseElement.size();
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableBodyXpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt < attemptCount);
		
		return size;
	}
	
	protected int getDescendantsTrSizeByTableID(String p_sType, String p_sIdentifier, String p_sTableId) {
		
		int baseElementSize = 0,
					attempt = 0;
		List<WebElement> sBaseElement = null ;
		WebDriverWait wait=null;
		
		wait=new WebDriverWait(driver, 10);
		do
		{
			try 
			{
				sBaseElement =  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//" + p_sType + "[@" + p_sIdentifier + " = '" + p_sTableId + "']/tr")));
				baseElementSize =  sBaseElement.size();
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		
		return baseElementSize;
	}	
	
	protected boolean isElementContainsText(String p_sType, String p_sIdentifier, String p_sTableId, String p_sText)
	{
		String l_sElemenText = null;
		WebDriverWait wait=null;
		WebElement l_oElement = null;
		wait=new WebDriverWait(driver, 10);
		
		l_oElement=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//" + p_sType + "[@" + p_sIdentifier + " = '" + p_sTableId + "']")));
		
		l_sElemenText = l_oElement.getText();
			
		if(l_sElemenText != null  &&  l_sElemenText.equals(p_sText))
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	protected void executeJavaScript(String script) {
		JavascriptExecutor js = null;

		if (script != null) {
			js = (JavascriptExecutor) driver;
			js.executeScript(script);
			log.info("script executed");
		}
	
	}
	
	
	protected void dynamicExecuteJavaScriptByText(String p_sTableId, String p_sScriptAttribute,String p_sTitleAttribute) {
		
		int attempt = 0;
		List<WebElement> tableRows = null; 
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		WebElement baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(p_sTableId)));

		do{
			try 
			{
				tableRows= baseTable.findElements(By.tagName("tr"));
				if (tableRows != null) {

					for (WebElement element : tableRows) {

						// get list of td's from td
						List<WebElement> s = element.findElements(By.tagName("td"));
						
						for (WebElement e : s) {
							
							System.out.println(e.getAttribute("title").toString());
							if(e.getAttribute("title").toString().trim().equals(p_sTitleAttribute.trim())){
								executeJavaScript(e.getAttribute(p_sScriptAttribute).toString());
								//e.click();
								break;
							}
						}
						break;
					}
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);

	}	
	
	
	protected boolean downloadPdf(String p_sTableId,String p_sImageNameAttribute,String p_sDateOfAwareness) {
		int attempt = 0;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		WebElement baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(p_sTableId)));
		List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
		
		do
		{
			try 
			{
				if (tableRows != null) {
					for (WebElement element : tableRows) {
						List<WebElement> s = element.findElements(By.tagName("image"));
							for(WebElement e:s){
								if(e.getAttribute("name").equals(p_sImageNameAttribute)){
									e.click();
									break;
								}
							}
						break;
					}
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return true;
	}	
	
	protected void selectDropdownValueByValue(String DropdownElementName, String Value) {
		Select selectElement = null;
		boolean isAppearing = false;
		List<WebElement> m_lAllOptions = null;
		
		isAppearing = driver.findElement(By.xpath("//SELECT[@name='" + DropdownElementName + "']")).isDisplayed();
		
		
		if(isAppearing)
		{
			selectElement = new Select(
					driver.findElement(By.xpath("//SELECT[@name='" + DropdownElementName + "']")));

			m_lAllOptions = selectElement.getOptions();
			
			for(WebElement element : m_lAllOptions) 
			{
				if(element.getText().equalsIgnoreCase(Value)) 
				{
					selectElement.selectByVisibleText(element.getText());
					break;
				}
			}
		}
	}
	
	protected boolean isDropDownContainValue(String p_sDropdownElementName , String p_sFindText)
	{
		Boolean bFound = false , isAppearing  = false;
		List<WebElement> m_lAllOptions = null;
		
		Select selectElement = null;
		
		isAppearing = driver.findElement(By.xpath("//SELECT[@name='" + p_sDropdownElementName + "']")).isDisplayed();
		
		
		if(isAppearing)
		{
			selectElement = new Select(
					driver.findElement(By.xpath("//SELECT[@name='" + p_sDropdownElementName + "']")));
			
			m_lAllOptions = selectElement.getOptions();
			
			for(WebElement element : m_lAllOptions) 
			{
				if(element.getText().equalsIgnoreCase(p_sFindText)) 
				{
					bFound = true;
					break;
				}
			}
		}
		return bFound;
	}
	
	protected String getSelectedTextByDropDownName(String p_sDropDownElementName)
	{
		Select selectElement = null;
		WebElement option = null;
		String defaultItem = null;
		boolean isAppearing  = false;
		
		isAppearing = driver.findElement(By.xpath("//SELECT[@name='" + p_sDropDownElementName + "']")).isDisplayed();
		
		
		if(isAppearing)
		{
			
			selectElement = new Select(
					driver.findElement(By.xpath("//SELECT[@name='" + p_sDropDownElementName + "']")));
			
			option = selectElement.getFirstSelectedOption();
			
			if(option != null)
				defaultItem = option.getText();
		}
		
		return defaultItem;
	}

	protected List<String> getAllDropDownList(String p_sDropdownElementName)
	{
		List<WebElement> slAllOptions = null;
		List<String> allText = null;
		Select sarrSelectElement = null;
		boolean	isAppearing = false;
		
		isAppearing = isElementPresent(By.xpath("//SELECT[@name='" + p_sDropdownElementName + "']"));

		allText = new ArrayList<>();
	
		if(isAppearing)
		{
			sarrSelectElement = new Select(
					driver.findElement(By.xpath("//SELECT[@name='" + p_sDropdownElementName + "']")));
			
			slAllOptions = sarrSelectElement.getOptions();
			
			
			if(slAllOptions != null)
			{
				for(int i = 0; i < slAllOptions.size() ; i++)
				{
					allText.add(slAllOptions.get(i).getText());
				}
			}
		}
		return allText;
	}

	
	protected void selectDropdownValueByIndex(String dropdownElementName, int index) {
		int attempt = 0;
		Select selectElement = null;
		
		do
		{
			try 
			{
				selectElement = new Select(
						driver.findElement(By.xpath("//SELECT[@name='" + dropdownElementName + "']")));
				selectElement.selectByIndex(index);
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ dropdownElementName, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	
	protected void clickLinkByText(String p_sHref) {
		
		List<WebElement> anchors = null;
		Iterator<WebElement> iterator = null;
		
		// take list of anchor tag
		anchors = driver.findElements(By.tagName("a"));
		// iterate each anchor
		iterator = anchors.iterator();

		while (iterator.hasNext()) 
		{
			WebElement anchor = iterator.next();
			
			if (null != anchor.getAttribute("href")) 
			{
				
				if (anchor.getText().equals(p_sHref)) 
				{
				
					anchor.sendKeys(Keys.RETURN);
					System.out.println("link clicked");
					break;
				}
			}
		}
	}
	
	protected void clickfirstLink(String resultRowsId){
		WebElement element=null;
		element=getElement("tr", "id", resultRowsId);
		// take list of anchor tag
		List<WebElement> anchors = element.findElements(By.tagName("a"));
		// iterate each anchor
		Iterator<WebElement> i = anchors.iterator();

		while (i.hasNext()) {
			WebElement anchor = i.next();
			if (null != anchor.getAttribute("href")) {
				anchor.sendKeys(Keys.RETURN);
				System.out.println("link clicked");
				break;
			}
		}
	}	
	
	protected void switchToFrame(String p_sFrameName){
		WebDriverWait wait = new WebDriverWait(driver,driverTimeout);
	 	wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(p_sFrameName));
		log.info("Frame switched{}"+p_sFrameName);
	}
	
	
	protected void switchParentToChildFrame(String parentFrame, String childFrame) {

		WebDriverWait wait = null;
		wait = new WebDriverWait(driver,driverTimeout);
		driver.switchTo().defaultContent();
		log.info("Navigated back to webpage from frame");
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(parentFrame));
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(childFrame));
		log.info("frame changed to " + childFrame + " inside parent frame " + parentFrame);
		
	}
	
	
	protected Set<String> getSetOfWindows(int p_nWindowSizw) {
		
		WebDriverWait wait=null;
		Set<String> handles=null;
		int attempt = 0;
		
		wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try
			{
				wait.until(ExpectedConditions.numberOfWindowsToBe(p_nWindowSizw));
				handles = driver.getWindowHandles(); // get all window
				
				break;
			}
			catch(TimeoutException timeoutExcp)
			{
				log.error("Timeout Exception - Expected windows:" + p_nWindowSizw, timeoutExcp);
				attempt++;
				
				if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
			}
		}
		while(attempt < attemptCount);
			
		return handles;
	}
	
	public void switchWindow(String window) {
		
		driver.switchTo().window(window);
		
	}
	
	protected void switchWindowHandler(String parentWindow) {
			
		
		Set<String> handles = getSetOfWindows(2); // get all window handles
	
		for(String windowHandle  : handles)
		{
		    if(!windowHandle.equals(parentWindow))
		   {
		    	switchWindow(windowHandle);
		   }
		}
	}		
	
	protected boolean yesOrNoOrCancleBtnOnPopup(String p_sBtnNameAttribute, String p_sWindow) {
		
		switchWindowHandler(p_sWindow);
		List<WebElement>elements=driver.findElements(By.tagName("input"));
		
			for(WebElement ele:elements){
				if(ele!=null && !(ele.getAttribute("name")).toString().isEmpty()){
					if(ele.getAttribute("name").toString().equals(p_sBtnNameAttribute)){
						enterClickByXpath("INPUT","name", p_sBtnNameAttribute);
						break;
					}
				}
			}	
		
		driver.switchTo().window(p_sWindow);
	
		return true;
	}	
	

	protected void clickOnTextByTableId(String p_sTableId,String p_sProductName) {
		int attempt = 0;
		WebDriverWait wait=null;
		WebElement baseTable=null;
		List<WebElement> tableRows=null,tds=null;
		boolean clicked=false;
		wait = new WebDriverWait(driver, driverTimeout);
		baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(p_sTableId)));

		do{
			try 
			{
				tableRows = baseTable.findElements(By.tagName("tr"));
				
				if (tableRows != null) {
			
					for (WebElement element : tableRows) {
			
						// get list of td's from td
						 tds = element.findElements(By.tagName("td"));
						
						for (WebElement e : tds) {
							if(e.getText().contains(p_sProductName)){
								e.click();
								clicked=true;
								break;
							}
						}
						if(clicked){
							break;
						}
					}
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	
	protected void clickOnTextByTableIdSpanText(String p_sTableId,String p_sDosageForm) {
		
		int attempt = 0;
		WebElement span = null, baseTable = null;
		List<WebElement> tableRows = null, tds = null;
		WebDriverWait wait = null;
		boolean clicked = false;
		int size = 0, count = 1;

		wait = new WebDriverWait(driver, driverTimeout);
		
		do{
			try 
			{
				baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(p_sTableId)));
				tableRows = baseTable.findElements(By.tagName("tr"));

				if (tableRows != null) 
				{
					size = tableRows.size();
					for (WebElement element : tableRows)
					{
						// get list of td's from td
						tds = element.findElements(By.tagName("td"));

						for (WebElement td : tds) 
						{
							span = td.findElement(By.tagName("Span"));
							if (!p_sDosageForm.toLowerCase().equals("")) 
							{
								if (span.getText().toLowerCase().contains(p_sDosageForm.toLowerCase())) 
								{
									span.click();
									clicked = true;
									break;
								} 
								else if (span.getText().contains("Unknown1")) 
								{
									span.click();
									clicked = true;
									break;
								}
								else if (count == size) 
								{
									span.click();
									clicked = true;
									break;
								}
								count++;
							} 
							else if (span.getText().contains("Unknown")) 
							{
								span.click();
								clicked = true;
								break;
							} 
						}
						if (clicked)
						{
							break;
						}
					}
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}	
	
	protected File getDownloadedPdfFilePath(File path) {

		String p_sSupportedExtn = "pdf";
		String[] sarrSupportedExtn = p_sSupportedExtn.split(",");

		List<File> lstFiles = null;
		File[] fileArrayList = null;
		
		try 
		{
			Thread.sleep(7000);
			
			lstFiles = (List<File>) FileUtils.listFiles(path, sarrSupportedExtn, false);

			fileArrayList = lstFiles.toArray(new File[lstFiles.size()]);

			if (fileArrayList.length == 0) 
			{
				System.out.println("ListFile length is 0");
			}
			else
			{
				
				Arrays.sort(fileArrayList, new Comparator<File>() 
				{
					public int compare(File o1, File o2)
					{
						return new Long(o2.lastModified()).compareTo(o1.lastModified());
					}
				});
				
			}

		} 
		catch (Exception e) 
		{
			log.error("Error fetching downloaded file ::" + e);
			System.out.println("Error fetching downloaded file ::" + e);
			e.printStackTrace();
		}
		
		return fileArrayList[0];
	}
	
	public void quitDriver(){
		try{
			driver.quit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error in closing driver");
		}
	}	
	
	protected String getDOM(){
		String pageSource=null;
		pageSource=driver.getPageSource();
		return pageSource;
	}	
	
	protected boolean findMultipleElements(String[] names){
		boolean check=false;
		for(String name:names){
			check=findElements(name);
			if(check==false){
				break;
			}
		}
		return check;
	}
	
	
	protected String getTextFromXpath(String p_sXpath){
		int attempt = 0;
		WebElement element=null;
		WebDriverWait wait =null;
		String text=null;
		
		wait = new WebDriverWait(driver, driverTimeout);

		do{
			try 
			{
				element= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p_sXpath)));
				text=element.getText();
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sXpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		
		return text;
	}
	
	
	protected String getTextFromXpathById(String id){
		int attempt = 0;
		WebElement element=null;
		WebDriverWait wait =null;
		String text=null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		
		do{
			try 
			{
				element= wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
				text=element.getText();
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ id, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return text;
	}
	
	protected boolean isElementPresentInXpath(String p_sXpath){
		
		boolean bElement = false;
		
		bElement= isElementPresent(By.xpath(p_sXpath));
		
		return bElement;
		
	}
	
	protected boolean isElementPresentByName(String p_sXpath){
		
		boolean bElement = false;
		
		bElement= isElementPresent(By.xpath("//*[@name='" + p_sXpath + "']"));
		
		return bElement;
		
	}
	
	public boolean isElementPresent(By locatorKey) {
		try {
	    	driver.findElement(locatorKey);
	        return true;
	    } catch (NoSuchElementException e) {
	        return false;
	    }
	}
	public boolean isElementPresent(By locatorKey , int nTimeout) {
		WebDriverWait waitForVisibility = new WebDriverWait(driver, nTimeout);
		try {
	    	waitForVisibility.until(ExpectedConditions.visibilityOfElementLocated(locatorKey));
	        return true;
	    } catch (TimeoutException e) {
	        return false;
	    }
	}
	public boolean isElementNotPresent(By locatorKey , int nTimeout) {
		WebDriverWait waitForVisibility = new WebDriverWait(driver, nTimeout);
		try {
	    	waitForVisibility.until(ExpectedConditions.invisibilityOfElementLocated(locatorKey));
	        return true;
	    } catch (TimeoutException e) {
	        return false;
	    }
	}
	public String getAlert()
    {
		Alert alert = null;
		String str = null;

		alert = driver.switchTo().alert();

		str = alert.getText();

		alert.accept();
		
		return str;
    }
	
	public boolean isAlertPresent() {
		try 
		{
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) 
		{
			return false;
		}
	}

	protected int getCountByXpath(String p_sXpath, String p_sAttributeName){
		
		int attempt = 0;
		WebElement element=null;
		List<WebElement> elements=null;
		int count=0;
		
		do
		{
			try 
			{
				element=driver.findElement(By.xpath(p_sXpath));
				elements=element.findElements(By.tagName("tr"));

				for(WebElement ele:elements){
					if(ele.getAttribute(p_sAttributeName)!=null){
						count=Integer.parseInt(ele.getAttribute(p_sAttributeName));
					}
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sXpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return count;
	}
	
	
	protected int getTDCountByTableId(String p_sTableId,String p_sTDID) {
		int attempt = 0;
		List<WebElement> tableRows = null;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		WebElement baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(p_sTableId)));
		int count=1;
		boolean oneFor=false;
		
		do{
			try 
			{
				tableRows = baseTable.findElements(By.tagName("tr"));

					if (tableRows != null) {

						for (WebElement element : tableRows) {

							// get list of td's from td
							List<WebElement> s = element.findElements(By.tagName("td"));
				
							for (WebElement e : s) {
								if(e.getAttribute("id").equals(p_sTDID + count)){
									count++;
									oneFor=true;
								}
							}
							if(oneFor){
								break;
							}
						}
					}
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		
		return count;
	}
	
	
	protected void executeScriptByTitle(String p_sTableId,String p_sTDID) {
		
		int attempt = 0;
		WebDriverWait wait = new WebDriverWait(driver, driverTimeout);
		WebElement baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(p_sTableId)));
		boolean script=false;
		List<WebElement> tableRows = null;
		
		do{ 
			try 
			{
				tableRows = baseTable.findElements(By.tagName("tr"));
					
					if (tableRows != null) {

						for (WebElement element : tableRows) {

							// get list of td's from td
							List<WebElement> s = element.findElements(By.tagName("td"));
				
							for (WebElement e : s) {
								if(e.getAttribute("id").equals(p_sTDID)){
									executeJavaScript(e.getAttribute("code").toString());
									script=true;
									break;
								}
							}
							if(script)
								break;
						}
					}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
	}
	
	
	protected String isRadioButtonCheckedAndGetValue(String xPathByname) {

		int attempt = 0;
		boolean checked=false;
		String title=null;
		WebDriverWait wait = null;
		List<WebElement> elements = null;
		wait = new WebDriverWait(driver, driverTimeout);
		
		do{
			try 
			{
				elements = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//INPUT[@name='" + xPathByname + "']")));
				
				for(WebElement element:elements)
				{
					String str = element.getAttribute("checked");
					
					if (str!=null && str.equalsIgnoreCase("true"))
					{
					   title=element.getAttribute("value");
					   checked=true;
					}
					else
					{
						title = "Unspecified";
					}
				
					if(checked)
						break;
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ xPathByname, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return title;
	}
	
	protected String isCheckBoxSelectedAndGetValue(String xPathByname) {

		int attempt = 0;
		String title=null;
		WebDriverWait wait = null;
		WebElement element = null;
		wait = new WebDriverWait(driver, driverTimeout);
		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//INPUT[@name='" + xPathByname + "']")));
				
				if (element.isSelected())
				{
				   title = "Y";
				}
				else
				{
					title = "N";
				}
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ xPathByname, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return title;
	}
	
	protected boolean isPageCorrect(String p_sPageName)
	{
		
		return findElements(p_sPageName);

	}
	
	public String getWindowHandle()
	{
		return driver.getWindowHandle();
	}
	
	public Set<String> getWindowHandles()
	{
		return driver.getWindowHandles();
	}
	
	protected void setWait(String p_sSleepDuration) throws InterruptedException
	{
		int nSleep = 0;
		
		nSleep = (p_sSleepDuration != null) ? Integer.parseInt(p_sSleepDuration) : 3000;
		
		Thread.sleep(nSleep);
	}

	public void highLightElement(String p_sXpath) 
	{
		WebElement element = null;

		element = driver.findElement(By.xpath(p_sXpath));

		if (driver instanceof JavascriptExecutor) 
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", element);
		}

	}
	
	public void scrollPageToElement(String p_sXpath) 
	{
		WebElement element = null;

		element = driver.findElement(By.xpath(p_sXpath));
		
		if (driver instanceof JavascriptExecutor) 
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		}

		
    }
	
	public boolean isElementEnabled(String p_sElementByName)
	{
		int attempt = 0;
		boolean eleEnable = false;
		WebDriverWait wait = null;
		WebElement element = null;
		wait = new WebDriverWait(driver, driverTimeout);

		do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//INPUT[@name='" + p_sElementByName + "']")));
				eleEnable =  element.isEnabled();
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sElementByName, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return eleEnable;
	}
	
	//Code By Girish
    public boolean isElementVisibleByName(String name, String tagName)
    {
        boolean visibilityFlag = false;
    	try {
       
        List<WebElement>elements=driver.findElements(By.tagName(tagName));
        
        for(WebElement ele:elements){
            if(ele!=null && !(ele.getAttribute("name")).toString().isEmpty()){
                if(ele.getAttribute("name").toString().equals(name)){
                    visibilityFlag = true;
                }
            }
        }
    	}
        catch(StaleElementReferenceException exc)
    	{
        	visibilityFlag =isElementVisibleByName(name, tagName);
    	}
        return visibilityFlag;
    }
    
    public boolean isTableEmpty(String tbodyXpath)
    {
        WebElement tableBodyElement= driver.findElement(By.xpath(tbodyXpath));
        List<WebElement>elements=tableBodyElement.findElements(By.tagName("a"));
        return elements.isEmpty();
    }
    
    public boolean isElementVisibleById(String id, String tagName)
    {

    	boolean visibilityFlag = false;
    	List<WebElement> elements = null;
    	int attempt = 0;
    	
    	do
    	{
    		try
    		{
    			elements = driver.findElements(By.tagName(tagName));
    	        
    	        for(WebElement ele:elements)
    	        {
    	            if(ele!=null && !(ele.getAttribute("id")).toString().isEmpty())
    	            {
    	                if(ele.getAttribute("id").toString().equals(id))
    	                {
    	                    visibilityFlag = true;
    	                    break;
    	                }
    	            }
    	        }
    	        
    	        break;
    		}
    		catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ id, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
    	}
    	while(attempt < attemptCount);
    	
    	return visibilityFlag;
    
    }
   
    //Code By Girish
    protected boolean selectDropdownValueByValueXpathOverride(String dropdownXpath, String value) {
    	int attempt = 0;
    	String selectedValue = null;
    	boolean selectSuccessFlag = true;
    	
    	WebDriverWait wait = new WebDriverWait(driver, driverTimeout); 
        Select selectElement = null;
        List<WebElement> m_lAllOptions = null;
        WebElement element = null;
        
		do
		{
			try 
			{

				element = driver.findElement(By.xpath(dropdownXpath));
				element =wait.until(ExpectedConditions.elementToBeClickable(By.xpath(dropdownXpath)));
		        selectElement=new Select(element);
		        
		        m_lAllOptions = selectElement.getOptions();
				
		        for(WebElement ele : m_lAllOptions) 
				{
					if(ele.getText().equalsIgnoreCase(value.trim())) 
					{
						selectedValue = ele.getText();
						break;
					}
				}
		        
		        if(StringUtils.isNullOrEmpty(selectedValue))
		        {
		        	log.info("Bussiness logic value does not match with select options for xpath:"+ dropdownXpath +"  Value : "+value);
		        	selectSuccessFlag = false;
		        }
		        else
		        {
		        	selectElement.selectByVisibleText(selectedValue);
		        }
		        
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ dropdownXpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
        return selectSuccessFlag;
    }
    //Code By Girish
    protected boolean selectDropdownValueByValueXpath(String dropdownXpath, String value) {
    	
    	int attempt = 0;
    	String selectedValue = null;
        List<WebElement> m_lAllOptions = null;
        boolean selectSuccessFlag = true;
        
    	Select selectElement = null;
         
    	do
		{
			try 
			{

				 selectElement=new Select(
		                    driver.findElement(By.xpath(dropdownXpath)));
		        
		        m_lAllOptions = selectElement.getOptions();
				
		        for(WebElement ele : m_lAllOptions) 
				{
					if(ele.getText().equalsIgnoreCase(value.trim())) 
					{
						selectedValue = ele.getText();
						break;
					}
				}
		        
		        if(StringUtils.isNullOrEmpty(selectedValue))
		        {
		        	log.info("Bussiness logic value does not match with select options for xpath:"+ dropdownXpath);
		        	selectSuccessFlag = false;
		        }
		        else
		        {
		        	  if(StringUtils.isNullOrEmpty(selectElement.getFirstSelectedOption().getText()))
		        	        selectElement.selectByVisibleText(selectedValue);
		        }
		        
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ dropdownXpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
        
        return selectSuccessFlag;
    }
    //Code By Girish
    protected boolean selectDropdownValueByValueByIdentifier(String p_sIdentifier, String xpathId, String value) {
       
    	int attempt = 0;
    	String selectedValue = null;
        List<WebElement> m_lAllOptions = null;
        boolean selectSuccessFlag = true;
        
    	Select selectElement = null;
        
    	do
		{
			try 
			{

		     selectElement=new Select(
		                driver.findElement(By.xpath("//*[@" + p_sIdentifier + "=" + "'" + (xpathId!=null ? xpathId:"") + "'" + "]")));
				
		        m_lAllOptions = selectElement.getOptions();
				
		        for(WebElement ele : m_lAllOptions) 
				{
					if(ele.getText().equalsIgnoreCase(value.trim())) 
					{
						selectedValue = ele.getText();
						break;
					}
				}
		        
		        if(StringUtils.isNullOrEmpty(selectedValue))
		        {
		        	log.info("Bussiness logic value does not match with select options for xpath Id:"+ xpathId +"  Value : "+value);
		        	selectSuccessFlag = false;
		        }
		        else
		        {
		        	selectElement.selectByVisibleText(selectedValue);
		        }
		        
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ xpathId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
        
        return selectSuccessFlag;
    }
    
    
    protected void findElementByAbsoluteXpathAndSetContentIfEmpty(String p_sXPath,String p_sText) {
          
    	int attempt = 0; 
        WebDriverWait wait=null;
        WebElement element=null;
            
        wait = new WebDriverWait(driver, driverTimeout);
        
        do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p_sXPath)));
		        if(StringUtils.isNullOrEmpty(element.getText()))
		        element.sendKeys(p_sText);
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sXPath, excp);
	    		attempt++;
	    	}
		}
		while(attempt<attemptCount);
    }
    
    //Code By Girish
    protected void findElementByAbsoluteXpathAndSetContentOverride(String p_sXPath,String p_sText) {
	  
    	int attempt = 0;
        WebDriverWait wait=null;
        WebElement element=null;
            
        wait = new WebDriverWait(driver, driverTimeout);
            
        do
		{
			try 
			{
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p_sXPath)));
			    element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(p_sXPath)));
				    	
			    element.clear();
			    element.sendKeys(p_sText);
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sXPath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
    }
    
    
    //Code By Girish
    public void closeWebDriver()
    {
        driver.close();
    }
    //Code By Girish
    protected void setLableAsSelectByXpath(String selectXpath, String inputXpath,  String selectlistXpath,String input) {
        clickElementByAbsolutePath(selectXpath);
        findElementByAbsoluteXpathAndSetContentOverride(inputXpath, input);
        clickElementByAbsolutePath(selectlistXpath);
    }
    //Code By Girish
    protected boolean unlockCase(String tableId, String caseId)
    {
    	boolean unlockStatus = false;
        List<WebElement>elements=driver.findElements(By.id(tableId));
        
        for(WebElement ele:elements){
            if(ele!=null){
                List<WebElement> rowelements = ele.findElements(By.tagName("tr"));
                
                for(WebElement row:rowelements){
                    if(row!=null)
                    {
                        List<WebElement> cellElements = row.findElements(By.tagName("td"));
                        for(WebElement cell:cellElements) {
                            if(cell.getText().toString().replace("(Receipt#)", "").trim().equals(caseId)){
                                cell.click();
                                unlockStatus = true;
                            }
                        }
                    }
                }
            }
        }
        return unlockStatus;
    }
    
    protected void switchToNewWindow(Set<String> handles, String... window)
    {
    	String childWindow1= null;
    	for(String windowHandle  : handles)
		{
    		for(String win : window)
    		{	
			    if(!windowHandle.equals(win))
		   {
		    	childWindow1 = windowHandle;
		   }
		}
		}
		switchWindow(childWindow1);
    }
    
    protected void downloadPdfInEmbededTag(String tagXpath)
    {
    	switchToFrame("ifrm");
    	 WebElement element=driver.findElement(By.xpath(tagXpath));
    	 driver.get(element.getAttribute("src"));
    }
    
    
    protected boolean isTableDataEmpty(String p_sTableId) {
		
    	int attempt = 0;
		boolean dataPresent = false;
		WebElement baseTable = null;
		List<WebElement> tableRows = null, tds = null;
		WebDriverWait wait = null;
		int size = 0;
		wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try 
			{
				baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(p_sTableId)));
				tableRows = baseTable.findElements(By.tagName("tr"));

				if (tableRows != null) 
				{
					size = tableRows.size();
					
					if(size >1)
					{
						dataPresent = true;
					}
					else
					{
						for (WebElement element : tableRows)
						{
							// get list of td's from td
							tds = element.findElements(By.tagName("td"));
							
							if(tds.size()>1)
							{
								dataPresent = true;
							}
						}
					}
				}	
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableId, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return dataPresent	;	
	}
    
  //Code By Girish
  	protected String getSelectedTextByDropDownXpath(String xpath)
  	{
  		Select selectElement = null;
  		WebElement option = null;
  		String defaultItem = null;
  		
  		int attempt = 0;
  		
  		do
  		{
			try 
			{
				selectElement = new Select(driver.findElement(By.xpath(xpath)));
				option = selectElement.getFirstSelectedOption();

				if (option != null)
				{
					defaultItem = option.getText();					
				}
				
				break;
			} 
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ xpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
  		}
  		while(attempt < attemptCount);
  		
  		return defaultItem;
  	}

  	protected String getSelectedTextByDropDownById(String id)
  	{

  		Select selectElement = null;
  		WebElement option = null;
  		String defaultItem = null;
  		
  		int attempt = 0;
  		
  		do
  		{
			try 
			{
				selectElement = new Select(driver.findElement(By.xpath("//SELECT[@id='" + id + "']")));
				option = selectElement.getFirstSelectedOption();
				
				if (option != null)
				{
					defaultItem = option.getText();					
				}
				
				break;
			} 
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ id, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
  	  			
  		}
  		while(attempt < attemptCount);	
  		
  		return defaultItem;
  	}
  	
  
  	protected String getTextOfValueAttributeElementByXpath(String xpathByName){
		
  		int attempt = 0;
  		String text=null;
		
		WebDriverWait wait =null;
		WebElement element =null;
		
		wait = new WebDriverWait(driver, driverTimeout);
		
		do
		{
			try 
			{
				element= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathByName)));
				if(element != null)
					text=element.getAttribute("value");
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ xpathByName, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		
		return text;
	}
  	public void maximizeWindow() {
  		driver.manage().window().maximize();
  	}
  	
  	protected void acceptAlert()
  	{
  		try {
  		driver.switchTo().alert().accept();
  		}
  		catch(NoAlertPresentException noElertExcp)
  		{
  			log.error("No alert generate");
  		}
  	}
  	
  	protected void closeWindow()
  	{
  		driver.close();
  	}
  	
  	public void openUrlInNewTab(String url)
  	{
  		 driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");

  	}
  	
  	public void openNewUrl(String url)
  	{
  		 driver.get(url);

  	}
  	
  	protected WebDriver getWebDriver()
  	{
  		return driver;
  	}
  	
	protected int getAllTDSizeByTableBodyXpath(String p_sTableBodyXpath) {
		
		int rowCount = 0,
			attempt = 0;	
		List<WebElement> sBaseElement = null ;
		
		WebDriverWait wait=null;
		
		wait=new WebDriverWait(driver, 10);
		do
		{
			try 
			{
				sBaseElement =  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(p_sTableBodyXpath+"/tr/td")));
				
				rowCount =  sBaseElement.size();
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ p_sTableBodyXpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return rowCount;
	}
  	
	//get field atribute
	protected String getFieldAtributeByXpath(String xpath, String attribute)
	{
		int attempt = 0;
		String fieldAttributeValue = null;
		WebElement element = null;
		WebDriverWait wait=null;
		
		wait=new WebDriverWait(driver, 10);
		
		do
		{
			try 
			{
				element =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
				
				//get xpath  dynamically
				fieldAttributeValue = element.getAttribute(attribute);
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ xpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		
		return fieldAttributeValue;
	}
	
	protected String getFieldLableByXpath(String xpath, int ancesterValue, String controlClassString, String lableClassString,boolean editFlag)
	{
		int attempt = 0;
		String  dynamicXpath = null, fieldClassName = null, labelXpath = null;
		WebElement element = null ,lableElement = null;
		WebDriverWait wait=null;
		dynamicXpath = xpath+"//ancestor::td["+ancesterValue+"]";
		
		do
		{
			try 
			{
				fieldClassName = getFieldAtributeByXpath(dynamicXpath, "class");
				
				fieldClassName = fieldClassName.replace(controlClassString, lableClassString);
				
				labelXpath = xpath+"//preceding::td[@class='"+fieldClassName+"'][1]";
				
				//button[contains(.,'Arcade Reader')]/preceding-sibling::button[@name='settings']
				
				wait=new WebDriverWait(driver, 10);
				
				element =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(labelXpath)));
				
				if(editFlag)
				{
					lableElement = element.findElement(By.tagName("span"));
					
				}
				else
				{
					lableElement = element.findElement(By.tagName("label"));
				}
				
				break;
			}
			catch(StaleElementReferenceException | TimeoutException excp)
	    	{
	    		log.error("Error accessing element: "+ xpath, excp);
	    		attempt++;
	    		
	    		if(attempt == attemptCount)
	    		{
	    			throw new RuntimeException();
	    		}
	    	}
		}
		while(attempt<attemptCount);
		return lableElement.getText();
	}
	
	protected static void setCPATProperties(String p_sPropertiesFileName) 
	{
		Map<String, String> propMap = null;
		Properties configProp = null;
		InputStream inputStream = null;
		propMap = new HashMap<String, String>();
		
		inputStream = BaseWebApplicationWriter.class.getClassLoader().getResourceAsStream(p_sPropertiesFileName);
        
		try 
        {
        	configProp = new Properties();
            configProp.load(inputStream);
            
            for (final String name: configProp.stringPropertyNames())
			    propMap.put(name, configProp.getProperty(name));
        
            cpatProperties = propMap;

        } 
        catch (IOException e) 
        {
            log.error("Error while common properties file reading");
            throw new RuntimeException();
        }
    }
	
	protected void waitUntillInvibilityOfElementById(String p_sID) {

		if(isElementPresent(By.id(p_sID), driverVisibilityTimeOut))
		{
			isElementNotPresent(By.id(p_sID), driverTimeout);
		}
	}
	
	protected void closeWindow(String p_sWindowToClose , String p_sParentWidow)
	{
		switchWindow(p_sWindowToClose);
		
		closeWebDriver();
		
		switchWindow(p_sParentWidow);
	}
}
