package com.sciformix.client.automation.sceptre;

public class SceptreConstants
{
	public static final String YES = "Yes";
	public static final String YES_VALUE = "Y";
	public static final String NO = "No";
	public static final String NO_VALUE = "N";
	public static final String UNDEFINED = "U";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_ID = "id";
	public static final String DAY = ".day";
	public static final String MONTH = ".month";
	public static final String YEAR = ".year";
	public static final String NAME_IDENTIFIER = "name";
	public static final String INPUT_TAG = "input";
	public static final String TBODY_TAG= "tbody";
	public static final String TABLE_TAG= "table";
	public static final String ID_IDENTIFIER = "id";
	public static final String CASENOTE = "Sceptre.CaseNote";
	public static final String CASENOTE_ADDNEW = "Sceptre.CaseNote.AddNew";
	public static final String CASENOTE_STANDARDNOTE = "Sceptre.CaseNote.StandardNote";
	public static final String CASENOTE_CUSTOMNOTE = "Sceptre.CaseNote.CustomNote";
	public static final String CASENOTE_SAVE = "Sceptre.CaseNote.Save";
	public static final String CASENOTE_NOTIFYUSER = "Sceptre.CaseNote.NotifyUserRadio";
	public static final String MINWAIT = "minWait";
	
	public static class HomeScreen
	{
		public static final String MENU_PANEL = "HomeScreen.MenuPanel";
	}
	
	public static class SceptreSearchFields
	{
		public static final SceptreSearchFields LOCAL_CASE_ID = new SceptreSearchFields("localcaseid");
		public static final SceptreSearchFields AER_NUMBER = new SceptreSearchFields("aernumber");
		public static final SceptreSearchFields OTHER_ID = new SceptreSearchFields("otherid");
		public static final SceptreSearchFields PATIENT_INITIALS = new SceptreSearchFields("patientinitials");

		public static final SceptreSearchFields PATIENT_AGEOPERATOR = new SceptreSearchFields("ageoperator");
		public static final SceptreSearchFields PATIENT_AGEUNIT = new SceptreSearchFields("ageunit");
		public static final SceptreSearchFields PATIENT_AGE1 = new SceptreSearchFields("patientage1");
		public static final SceptreSearchFields PATIENT_AGE2 = new SceptreSearchFields("patientage2");
		public static final SceptreSearchFields PATIENT_GENDER = new SceptreSearchFields("patientgender");

		public static final SceptreSearchFields PATIENT_DOB1 = new SceptreSearchFields("dob1");
		public static final SceptreSearchFields PATIENT_DOBOPERATOR = new SceptreSearchFields("doboperator");
		public static final SceptreSearchFields PATIENT_DOB1DAY = new SceptreSearchFields("dob1day");
		public static final SceptreSearchFields PATIENT_DOB1MONTH = new SceptreSearchFields("dob1month");
		public static final SceptreSearchFields PATIENT_DOB1YEAR = new SceptreSearchFields("dob1year");
		
		public static final SceptreSearchFields PATIENT_DOB2 = new SceptreSearchFields("dob2");
		public static final SceptreSearchFields PATIENT_DOB2DAY = new SceptreSearchFields("dob2day");
		public static final SceptreSearchFields PATIENT_DOB2MONTH = new SceptreSearchFields("dob2month");
		public static final SceptreSearchFields PATIENT_DOB2YEAR = new SceptreSearchFields("dob2year");

		public static final SceptreSearchFields PATIENT_DOD1 = new SceptreSearchFields("dod1");
		public static final SceptreSearchFields PATIENT_DODOPERATOR = new SceptreSearchFields("dodoperator");
		public static final SceptreSearchFields PATIENT_DOD1DAY = new SceptreSearchFields("dod1day");
		public static final SceptreSearchFields PATIENT_DOD1MONTH = new SceptreSearchFields("dod1month");
		public static final SceptreSearchFields PATIENT_DOD1YEAR = new SceptreSearchFields("dod1year");
		
		public static final SceptreSearchFields PATIENT_DOD2 = new SceptreSearchFields("dod2");
		public static final SceptreSearchFields PATIENT_DOD2DAY = new SceptreSearchFields("dod2day");
		public static final SceptreSearchFields PATIENT_DOD2MONTH = new SceptreSearchFields("dod2month");
		public static final SceptreSearchFields PATIENT_DOD2YEAR = new SceptreSearchFields("dod2year");

		public static final SceptreSearchFields REPORTED_REACTION = new SceptreSearchFields("reportedreaction");
		public static final SceptreSearchFields REPORTER_GIVENNAME = new SceptreSearchFields("reportergivenname");
		public static final SceptreSearchFields REPORTER_FAMILYNAME = new SceptreSearchFields("reporterfamilyname");
		public static final SceptreSearchFields REPORTER_ORGANIZATION = new SceptreSearchFields("reporterorganization");
		public static final SceptreSearchFields REPORTER_CITY = new SceptreSearchFields("reportercity");
		public static final SceptreSearchFields REPORTER_STATE = new SceptreSearchFields("reporterstate");
		public static final SceptreSearchFields PROTOCOL = new SceptreSearchFields("protocol");
		public static final SceptreSearchFields SITE_NUMBER = new SceptreSearchFields("sitenumber");
		public static final SceptreSearchFields PATIENT_MEDICATION_NUMBER = new SceptreSearchFields("patientmedicationnumber");
		public static final SceptreSearchFields COUNTRY = new SceptreSearchFields("country");
		public static final SceptreSearchFields GENERICDRUG = new SceptreSearchFields("genericdrug");
		public static final SceptreSearchFields SHOW_DELETED_CASES= new SceptreSearchFields("ShowDeletedCases");
		
		public String m_sInternalName = null;
		
		private SceptreSearchFields(String p_sInternalName)
		{
			m_sInternalName = p_sInternalName;
		}
		
		public String internalName()
		{
			return m_sInternalName;
		}
		
		public boolean equals(SceptreSearchFields other)
		{
			return this.m_sInternalName.equalsIgnoreCase(other.m_sInternalName);
		}
	}
	
	public static class SceptreSearchResultColumns
	{
		public static final String AER = "AER";
		public static final String VERSION = "VERSION";
		public static final String QUEUE = "QUEUE";
		public static final String CASEOWNER = "CASEOWNER";
		public static final String REACTION = "REACTION";
		public static final String REACTIONDATE = "REACTIONDATE";
		public static final String SUSPECTPRODUCT = "SUSPECTPRODUCT";
		public static final String SERIOUSNESS = "SERIOUSNESS";
		public static final String REPORTER = "REPORTER";
		
	}
	
	public static enum FETCH_SCOPE
	{
		//CRITICAL_FIELDS, BASIC_FIELDS, ALL;
		CRITICAL_FIELDS, ALL;
	}
	
	public static class SearchFieldXpath
	{
		public static final String AER_NUMBER 							= "DupSearch.AERNumber"				;
		public static final String LOCAL_CASE_ID 						= "DupSearch.LocalCaseID"			;
		public static final String OTHER_ID 							= "DupSearch.OtherID"				;
		public static final String SEARCHBUTTON 						= "DupSearch.SearchButton"			;
		public static final String PATIENT_INITIALS 					= "DupSearch.Patient.Initials"		;
	
		public static final String PATIENT_AGEOPERATOR 					= "DupSearch.Patient.AgeOperator"	;
		public static final String PATIENT_AGEUNIT 						= "DupSearch.Patient.AgeUnit"		;
		public static final String PATIENT_AGE1 						= "DupSearch.Patient.Age1"			;
		public static final String PATIENT_AGE2 						= "DupSearch.Patient.Age2"			;
		public static final String PATIENT_GENDER 						= "DupSearch.Patient.Gender"		;
		
		public static final String PATIENT_DOB1 						= "DupSearch.Patient.DOB1"			;
		public static final String PATIENT_DOBOPERATOR 					= "DupSearch.Patient.DOBOperator"	;
		public static final String PATIENT_DOB1DAY 						= "DupSearch.Patient.DOB1Day"		;
		public static final String PATIENT_DOB1MONTH 					= "DupSearch.Patient.DOB1Month"		;	
		public static final String PATIENT_DOB1YEAR 					= "DupSearch.Patient.DOB1Year"		;
		
		public static final String PATIENT_DOB2 						= "DupSearch.Patient.DOB2"			;
		public static final String PATIENT_DOB2DAY 						= "DupSearch.Patient.DOB2Day"		;
		public static final String PATIENT_DOB2MONTH 					= "DupSearch.Patient.DOB2Month"		;
		public static final String PATIENT_DOB2YEAR 					= "DupSearch.Patient.DOB2Year"		;
		
		public static final String PATIENT_DOD1 						= "DupSearch.Patient.DOD1"			;
		public static final String PATIENT_DODOPERATOR 					= "DupSearch.Patient.DODOperator"	;
		public static final String PATIENT_DOD1DAY 						= "DupSearch.Patient.DOD1Day"		;
		public static final String PATIENT_DOD1MONTH 					= "DupSearch.Patient.DOD1Month"		;
		public static final String PATIENT_DOD1YEAR 					= "DupSearch.Patient.DOD1Year"		;
		
		public static final String PATIENT_DOD2 						= "DupSearch.Patient.DOD2"			;
		public static final String PATIENT_DOD2DAY 						= "DupSearch.Patient.DOD2Day"		;
		public static final String PATIENT_DOD2MONTH 					= "DupSearch.Patient.DOD2Month"		;
		public static final String PATIENT_DOD2YEAR 					= "DupSearch.Patient.DOD2Year"		;

		
		public static final String REPORTED_REACTION 					= "DupSearch.ReportedReaction"		;
		public static final String REPORTER_GIVENNAME 					= "DupSearch.ReportedGivenName"		;
		public static final String REPORTER_FAMILYNAME 					= "DupSearch.ReportedFamilyName"	;
		public static final String REPORTER_ORGANIZATION 				= "DupSearch.ReporterOrganization"	;
		public static final String REPORTER_CITY 						= "DupSearch.ReporterCity"			;	
		public static final String REPORTER_STATE 						= "DupSearch.ReporterState"			;
		public static final String PROTOCOL 							= "DupSearch.Protocol"				;
		public static final String SITE_NUMBER 							= "DupSearch.SiteNumber"			;
		public static final String PATIENT_MEDICATION_NUMBER 			= "DupSearch.PatientMedicationNumber";
		public static final String COUNTRY 								= "DupSearch.Country"				;
		public static final String GENERICDRUGLOOKUP 					= "DupSearch.GenericDrugLookUp"		;
		public static final String GENERICDRUG 							= "DupSearch.GenericName"			;
		public static final String GENERICDRUGCODE 						= "DupSearch.GenericDrugCode"		;
		public static final String GENERICDRUGSEARCH 					= "DupSearch.GenericDrugSearch"		;
		public static final String GENERICDRUGCLICK 					= "DupSearch.GenericDrugClick"		;
		public static final String SHOW_DELETED_CASES 					= "DupSearch.ShowDeletedCases"		;

		public static final String CLEAR 								= "DupSearch.Clear"					;
		public static final String VERIFY_DUPLICATE_SEARCH_PAGE 		= "Tab.VerifyDuplicateSearchPage"	;
	}
	
	public static class ResultFieldsXpath
	{
		public static final String AER_PREFIX = "Result.CaseID.Prefix";
		public static final String AER_POSTFIX = "Result.CaseID.Postfix";
		public static final String VERSION_PREFIX = "Result.Version.Prefix";
		public static final String VERSION_POSTFIX = "Result.Version.Postfix";
		public static final String QUEUE_PREFIX = "Result.Queue.Prefix";
		public static final String QUEUE_POSTFIX = "Result.Queue.Postfix";
		public static final String CASEOWNER_PREFIX = "Result.CaseOwner.Prefix";
		public static final String CASEOWNER_POSTFIX = "Result.CaseOwner.Postfix";
		public static final String REACTION_PREFIX = "Result.Reaction.Prefix";
		public static final String REACTION_POSTFIX = "Result.Reaction.Postfix";
		public static final String REACTIONDATE_PREFIX = "Result.ReactionDate.Prefix";
		public static final String REACTIONDATE_POSTFIX = "Result.ReactionDate.Postfix";
		public static final String SUSPECTPRODUCT_PREFIX = "Result.SuspectProduct.Prefix";
		public static final String SUSPECTPRODUCT_POSTFIX = "Result.SuspectProduct.Postfix";
		public static final String SERIOUSNESS_PREFIX = "Result.Seriousness.Prefix";
		public static final String SERIOUSNESS_POSTFIX = "Result.Seriousness.Postfix";
		public static final String REPORTER_PREFIX = "Result.Reporter.Prefix";
		public static final String REPORTER_POSTFIX = "Result.Reporter.Postfix";
		public static final String AERSEARCH_CASEOWNER_PREFIX = "AERSearch.CaseOwner.Prefix";
		public static final String AERSEARCH_CASEOWNER_POSTFIX = "AERSearch.CaseOwner.Postfix";
		public static final String AERSEARCH_AER_PREFIX = "AERSearch.AER.Prefix";
		public static final String AERSEARCH_AER_POSTFIX = "AERSearch.AER.Postfix";
		
		
	} 
		
	public static class ReporterTabKeys{
		
		public static final String QUALIFICATION = "Reporter.Qualification";
		public static final String TITLE = "Reporter.Title";
		public static final String GIVEN_NAME = "Reporter.GivenName";
		public static final String GIVEN_NAME_LABLE= "Reporter.GivenName.Lable";
		public static final String MIDDLE_NAME_LABLE = "Reporter.MiddleName.Lable";
		public static final String MIDDLE_NAME = "Reporter.MiddleName";
		public static final String FAMILY_NAME_LABLE = "Reporter.FamilyName.Lable";
		public static final String FAMILY_NAME = "Reporter.FamilyName";
		public static final String DEGREE = "Reporter.Degree";
		public static final String ANONYMOU_REFUSED = "Reporter.Annonymous";
		public static final String ADMINISTERED_VACCINE = "Reporter.Administered"; 

		public static final String ORGANIZATION = "Reporter.Organizataion";
		public static final String DEPARTMENT = "Reporter.Department";
		public static final String ADDRESS1_LABLE = "Reporter.Address1.Lable";
		public static final String ADDRESS1 = "Reporter.Address1";
		public static final String ADDRESS2_LABEL = "Reporter.Address2.Label";
		public static final String ADDRESS2 = "Reporter.Address2";
		public static final String ADDRESS3_LABEL = "Reporter.Address3.Lable";
		public static final String ADDRESS3 = "Reporter.Address3";
		public static final String CITY_LABEL = "Reporter.City.Lable";
		public static final String CITY = "Reporter.City";
		public static final String PROVINCE_STATE = "Reporter.State";
		public static final String POSTAL_CODE_LABEL = "Reporter.PostalCode.Lable";
		public static final String POSTAL_CODE = "Reporter.PostalCode";
		public static final String COUNTRY = "Reporter.Country";

		public static final String PHONE1_CODE_LABLE = "Reporter.Phone1Code.Lable";
		public static final String PHONE1_NUMBER_LABLE = "Reporter.Phone1Number.Lable";
		public static final String PHONE1_EXT_LABLE = "Reporter.Phone1Ext.Lable";
		public static final String PHONE2_CODE_LABLE = "Reporter.Phone2Code.Lable";
		public static final String PHONE2_NUMBER_LABLE = "Reporter.Phone2Number.Lable";
		public static final String PHONE2_EXT_LABLE = "Reporter.Phone2Ext.Lable";

		public static final String PHONE1_CODE = "Reporter.Phone1Code";
		public static final String PHONE1_NUMBER = "Reporter.Phone1Number";
		public static final String PHONE1_EXT = "Reporter.Phone1Ext";
		public static final String PHONE2_CODE = "Reporter.Phone2Code";
		public static final String PHONE2_NUMBER = "Reporter.Phone2Number";
		public static final String PHONE2_EXT = "Reporter.Phone2Ext";
		public static final String FAX_CODE = "Reporter.FaxCode";
		public static final String FAX_NUMBER = "Reporter.FaxNumber";
		public static final String FAX_EXT = "Reporter.FaxExt";
		public static final String E_MAIL_ADDRESS_LABLE = "Reporter.Email.Lable";
		public static final String E_MAIL_ADDRESS = "Reporter.Email";
		public static final String CONTACT_METHOD = "Reporter.ContactMethod";

		public static final String COMPANY_TITLE = "Reporter.Company.Title";
		public static final String COMPANY_GIVEN_NAME = "Reporter.Company.GivenName";
		public static final String COMPANY_FAMILY_NAME = "Reporter.Company.FamilyName";
		public static final String COMPANY_REPORTER = "Reporter.Company.Reporter";
		public static final String COMPANY_QUALIFICATION = "Reporter.Company.Qalification";

		public static final String SCHEDULE = "Reporter.Schedule";
		public static final String RESPONSE_RECEIVED = "Reporter.ResponseRecDay";
		public static final String RESPONSE_RECEIVED_DAY = "Reporter.ResponseRecDay";
		public static final String RESPONSE_RECEIVED_MONTH = "Reporter.ResponseRecMonth";
		public static final String RESPONSE_RECEIVED_YEAR = "Reporter.ResponseRecYear";
		public static final String SUFFICENT_DETAIL = "Reporter.SufficientDetails";
		public static final String ADDITIONAL_LETTER_TEXT = "Reporter.AdditionalText";
		public static final String REPORTER_SAVE = "Reporter.Save";
		public static final String REPORTER_TAB = "Tab.Reporter";
		public static final String REPORTER_ADDNEW = "Reporter.AddNew";

		public static final String STATUS = "";
		public static final String ACTION = "";
	}
	
	public static class PatientTabKeys{
		
		public static final String PATIENT_TAB = "Tab.Patient";
		public static final String PATIENT_UNKNOWN = "Patient.Unknown";
		public static final String PATIENT_ID = "Patient.Id";
		public static final String PATIENT_ID_LABLE = "Patient.Id.Lable";
		
		public static final String PATIENT_SEX = "Patient.Gender";
		public static final String POSTAL_CODE = "Patient.PostalCode";
		public static final String POSTAL_CODE_LABLE = "Patient.PostalCode.Lable";
	
		public static final String DATE_OF_BIRTH = "Patient.Birth";
		public static final String DATE_OF_BIRTH_DAY = "Patient.BirthDay";
		public static final String DATE_OF_BIRTH_MONTH = "Patient.BirthMonth";
		public static final String DATE_OF_BIRTH_YEAR = "Patient.BirthYear";
	
		public static final String AGE = "Patient.Age";
		public static final String AGE_UNIT = "Patient.AgeUnit";
		public static final String AGE_GROUP = "Patient.AgeGroup";
		
		public static final String HEIGHT = "Patient.Height";
		public static final String HEIGHT_UNIT = "Patient.HeightUnit";
		public static final String WEIGHT = "Patient.Weight";
		public static final String WEIGHT_UNIT = "Patient.WeightUnit";
		
		public static final String CONVERTED_HEIGHT ="CaseID.ConvertedHeight";
		public static final String CONVERTED_WEIGHT ="CaseID.ConvertedWeight";
		public static final String DATE_OF_LAST_MENSTRUAL_PERIOD = "Patient.LastMenst";
		public static final String DATE_OF_LAST_MENSTRUAL_PERIOD_DAY = "Patient.LastMenstDay";
		public static final String DATE_OF_LAST_MENSTRUAL_PERIOD_MONTH = "Patient.LastMenstMonth";
		public static final String DATE_OF_LAST_MENSTRUAL_PERIOD_YEAR = "Patient.LastMenstYear";

		public static final String PHYSICIAN_AWARE_OF_ADVERSE_EVENT = "Patient.PhyAwareOfAdverseEvent";
		public static final String PERMISSION_TO_CONTACT_PHYSICIAN = "Patient.PermissionToContactPhy";

		public static final String DATE_OF_DEATH = "Patient.DeathDay";
		public static final String DATE_OF_DEATH_DAY = "Patient.DeathDay";
		public static final String DATE_OF_DEATH_MONTH = "Patient.DeathMonth";
		public static final String DATE_OF_DEATH_YEAR = "Patient.DeathYear";
		
		public static final String WAS_AUTOPSY_DONE = "Patient.AutoSpyDone";
		public static final String DATE_OF_AUTOPSY = "Patient.AutospyDate";
		public static final String DATE_OF_AUTOPSY_DAY = "Patient.AutospyDateDay";
		public static final String DATE_OF_AUTOPSY_MONTH = "Patient.AutospyDateMonth";
		public static final String DATE_OF_AUTOPSY_YEAR = "Patient.AutospyDateYear";

		public static final String GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER = "Patient.GenPractitionalMedRecNo";
		public static final String SPECIALIST_RECORD_NUMBER = "Patient.SpecialistRecNo";
		public static final String HOSPITAL_RECORD_NUMBER = "Patient.HospitalRecordNo";

		public static final String NUMBER_OF_CIGARETTES_PER_DAY = "Patient.NoOfCiggPerDay";
		public static final String NUMBER_OF_YEARS_SMOKED = "Patient.NoOfYearSmoke";
		public static final String NUMBER_OF_DRINKS_PER_DAY = "Patient.NoOfDrinkPerDay";
		public static final String TYPE_OF_ALCOHOL = "Patient.TypeOfAlcohol";
		public static final String PHYSICAL_ACTIVITY = "Patient.PhysicalActivity";
		public static final String PATIENT_SAVE = "Patient.Save";
		
		public static final String OTHER_HISTORY = "Patient.OtherHistory";
		public static final String OTHER_HISTORY_RELEVENT_TEXTAREA = "Patient.OtherHistoryReleventTextArea";
		public static final String OTHER_HISTORY_SAVE = "OtherHistory.Save";
	}
	
	public static class CaseIdTabKeys{
		
		public static final String INITIATING_COMPANY_UNIT = "CaseID.CompanyUnit";
		public static final String REPORTED_TO_REGULATOR_BY_SOURCE = "CaseID.ReportedToRegulatorBySource";
		public static final String LOCAL_CASE_ID = "CaseID.LocalCaseID";
		public static final String INITIATING_JNJ_UNIT = "CaseID.InitialJ_JUnit";
		public static final String CASE_PROCESSING_FUNCTION = "CaseID.CaseProcessingFunction";
		public static final String COUNTRY_OF_PRIMARY_SOURCE = "CaseID.CountryOfPrimarySource";
		public static final String COUNTRY_WHERE_AE_OCCURRED = "CaseID.CountryWhereAEOccoured";

		public static final String COUNTRY_OF_PRIMARY_SOURCE_FOLLOWUP= "CaseID.CountryOfPrimarySourceFollowup";
		public static final String COUNTRY_WHEREAE_OCCOURED_FOLLOWUP = "CaseID.CountryWhereAEOccouredFollowup";
				
		public static final String CASE_PRIORITY = "CaseID.CasePriority";
		
		
		public static final String VERSION_INITIALLY_RECEIVED = "CaseID.VersionInitiallyRecieved";
		public static final String VERSION_INITIALLY_RECEIVED_DAY = "CaseID.VersionInitiallyRecievedDay";
		public static final String VERSION_INITIALLY_RECEIVED_MONTH = "CaseID.VersionInitiallyRecievedMonth";
		public static final String VERSION_INITIALLY_RECEIVED_YEAR = "CaseID.VersionInitiallyRecievedYear";
		
		public static final String ICH_EXPEDITED_START = "CaseID.ICHExpeditedStarts";
		public static final String ICH_EXPEDITED_START_DAY = "CaseID.ICHExpeditedStartsDay";
		public static final String ICH_EXPEDITED_START_MONTH = "CaseID.ICHExpeditedStartsMonth";
		public static final String ICH_EXPEDITED_START_YEAR = "CaseID.ICHExpeditedStartsYear";
		
		public static final String LATEST_INFORMATION_RECEIVED = "CaseID.LetestInformationRecieved";
		public static final String LATEST_INFORMATION_RECEIVED_DAY = "CaseID.LetestInformationRecievedDay";
		public static final String LATEST_INFORMATION_RECEIVED_MONTH = "CaseID.LetestInformationRecievedMonth";
		public static final String LATEST_INFORMATION_RECEIVED_YEAR = "CaseID.LetestInformationRecievedYear";
		
		public static final String RECEIVED_BY_LOCAL_SAFETY_UNIT = "CaseID.RecievedByLocalSafetyUnit";
		public static final String RECEIVED_BY_LOCAL_SAFETY_UNIT_DAY = "CaseID.RecievedByLocalSafetyUnitDay";
		public static final String RECEIVED_BY_LOCAL_SAFETY_UNIT_MONTH = "CaseID.RecievedByLocalSafetyUnitMonth";
		public static final String RECEIVED_BY_LOCAL_SAFETY_UNIT_YEAR = "CaseID.RecievedByLocalSafetyUnitYear";

		public static final String RECEIVED_BY_CASE_PROCESSING_CENTER = "CaseID.RecievedByCaseProcessingCenter";
		public static final String RECEIVED_BY_CASE_PROCESSING_CENTER_DAY = "CaseID.RecievedByCaseProcessingCenterDay";
		public static final String RECEIVED_BY_CASE_PROCESSING_CENTER_MONTH = "CaseID.RecievedByCaseProcessingCenterMonth";
		public static final String RECEIVED_BY_CASE_PROCESSING_CENTER_YEAR = "CaseID.RecievedByCaseProcessingCenterYear";
		
		public static final String REPORT_TYPE = "CaseID.ReportType";
		public static final String REPORT_TYPE_FOLLOWUP = "CaseID.ReportTypeFollowup";

		public static final String ADVERSEEVENT = "CaseID.Classification.AdverseEvent";
		public static final String PREGNANCY = "CaseID.Classification.Pregnancy";
		public static final String LITEARTURE = "CaseID.Classification.Litearture";
		public static final String PARENTCHILD = "CaseID.Classification.ParentChild";
		public static final String PRODUCTCOMPLAINT = "CaseID.Classification.ProductComplaint";
		public static final String INVALIDCASE = "CaseID.Classification.InvalidCase";
		
		public static final String NATURE_OF_COMPLAINT = "CaseID.NatureOfComplaint";
		public static final String OTHER_COMPLAINT = "CaseID.OtherComplaint";
		public static final String REASON_FOR_INVALID_CASE = "CaseID.ReasonForInvalidCase";
		
		public static final String CASE_CHARACTERISTICS_TEXTAREA = "CaseID.CaseCharactristics.TextArea";
		public static final String CASE_CHARACTERISTICS_LOOKUP = "CaseID.CaseCharactristics.LookUp";
		public static final String CASE_CHARACTERISTICS_AVAILABLE = "CaseID.CaseCharactristics.Available";
		public static final String CASE_CHARACTERISTICS_SELECTED  = "CaseID.CaseCharactristics.Selected";
		public static final String CASE_CHARACTERISTICS_ADDCC = "CaseID.AddCC";
		public static final String CASE_CHARACTERISTICS_REMOVECC = "CaseID.RemoveCC";
		public static final String CASE_CHARACTERISTICS_SAVECC = "CaseID.SaveCC";
		
		public static final String MEDICALLY_CONFIRMED = "CaseID.MedicallyConfirmed";
		public static final String REGULATORY_RELEVANT_UPDATE="CaseID.RegulatoryReleventUpdate";
		public static final String ADDITIONAL_DOCUMENTS_AVAILABLE = "CaseID.AdditionDocumentsAvailable";
		public static final String DOCUMENTS_HELD_BY_SENDER = "CaseID.DocumentHeldBySenderTextArea";
		
		public static final String SERIOUS = "CaseID.Seriousness";
		public static final String LIFE_THREATENING = "CaseID.LifeThreatening";
		public static final String DISABLING_INCAPACITATING = "CaseID.Disabling";
		public static final String RESULTS_IN_DEATH = "CaseID.Death";
		public static final String CAUSED_PROLONGED_HOSPITALIZATION = "CaseID.Hospitalization";
		public static final String CONGENITAL_ANOMALY_BIRTH_DEFECT = "CaseID.Congenital";
		public static final String OTHER_MEDICALLY_IMPORTANT_CONDITION = "CaseID.Other";
		public static final String CASE_SAVE = "CaseID.CaseIDSave";
		public static final String CASEID_TAB = "Tab.CaseId";

		public static final String INVALID_CASE_EXEMPTION_LOOKUP_BUTTON = "CaseID.InvalidExemptionsButton";
		public static final String INVALID_CASE_EXEMPTION_TEXTAREA = "CaseID.InvalidExemptionsTextArea";
		public static final String INVALID_CASE_EXEMPTION_AVAILABLE = "CaseID.InvalidExcemptionAvailable";
		public static final String INVALID_CASE_EXEMPTION_ADD = "CaseID.AddIE";
		public static final String INVALID_CASE_EXEMPTION_SAVE = "CaseID.SaveIE";
		public static final String PATIENT_REGISTRY = "";
		public static final String LINKED_CASES = "DupCheck.LinkedCase";
		
		public static final String COMPANYCODE_LOOKUP="CaseId.LookUp";
		public static final String COMPANYUNITCODE_LOOKUP="CaseID.CompanyUnitCodeLookUp";
		public static final String ORGANIZATION_LOOKUP="CaseID.OrganizationLookUp";
		public static final String COMPANYUNITCODE_LOOKUP_SEARCHBUTTON="CaseID.CompanyUnitCodeLookUp.SearchButton";
		public static final String CASEIDSAVE="CaseID.CaseIDSave";
		public static final String LOOKUP_CLICKONRESULT="CaseID.LookUp.ClickOnResult";
		
		public static final String LOOKUP_RESULT_TABLE="CaseID.LookUpResultTable";
		public static final String LOOKUP_CANCLE="CaseID.LookUpCancle";

		public static final String LOOKUP_CCSELECT ="CaseID.CaseCharactristicsSelect";
		public static final String LOOKUP_ICESELECT ="CaseID.InvalidCaseException";

		
		public static final String CASEID_OTHER_IDENTIFICATION_NUMBER_ADDNEW	= 			"CaseID.OtherIdentificationNumber";
		public static final String OTHER_IDENTIFICATION_NUMBER_SOURCE			=			"CaseID.OtherIdentificationNumber.Source";
		public static final String OTHER_IDENTIFICATION_NUMBER_CASEIDNUMBER		=			"CaseID.OtherIdentificationNumber.CaseIDNumber";
		public static final String OTHER_IDENTIFICATION_NUMBER_SAVE				=			"CaseID.OtherIdentificationNumber.Save";

		public static final String OTHER_IDENTIFICATION_NUMBER_LOOKUPBUTTON		=			"CaseID.OtherIdentificationNumber.LookUpButton";
		public static final String OTHER_IDENTIFICATION_NUMBER_TBODYID			=			"CaseID.OtherIdentificationNumber.TBodyID";
		public static final String OTHER_IDENTIFICATION_NUMBER_OTHERID_PREFIX	=			"CaseID.OtherIdentificationNumber.OtherIDPrefix";
		public static final String OTHER_IDENTIFICATION_NUMBER_OTHERID_POSTFIX	=			"CaseID.OtherIdentificationNumber.OtherIDPostfix";

		public static final String LINKEDCASES_TBODYID							=			"CaseID.LinkedCases.TBodyID";
		public static final String LINKEDCASES_LINKTYPE_PREFIX					=			"CaseID.LinkedCases.LinkTypePrefix";
		public static final String LINKEDCASES_LINKTYPE_POSTFIX					=			"CaseID.LinkedCases.LinkTypePostFix";
		public static final String LINKEDCASES_AERNUMBER_PREFIX					=			"CaseID.LinkedCases.AERNumberPrefix";
		public static final String LINKEDCASES_AERNUMBER_POSTFIX				=			"CaseID.LinkedCases.AERNumberPostfix";

		public static final String OTHER_IDENTIFICATION_NUMBER_SOURCEPREFIX		=			"CaseID.OtherIdentificationNumber.SourcePrefix";
		public static final String OTHER_IDENTIFICATION_NUMBER_SOURCEPOSTFIX	=			"CaseID.OtherIdentificationNumber.SourcePostfix";

		public static final String CASEID_OTHER_IDENTIFICATION_NUMBER			=			"OtherIdentificationNumber";
	}

	public static class DiagnosticTabKeys
	{
		public static final String REPORTEDTEST = "Diagnostic.ReportedTest";
		public static final String LOWERLEVELTERM = "Diagnostic.LowerLevelTerm";
		public static final String PREFFEREDTERM = "Diagnostic.PrefferedTerm";
		public static final String UNIT = "Diagnostic.Unit";
		public static final String NORMALRANGELOW = "Diagnostic.NormalRangeLow";
		public static final String NORMALRANGEHIGH = "Diagnostic.NormalRangeHigh";
		public static final String TEST = "Diagnostic.Test";
		public static final String TESTDAY = "Diagnostic.TestDay";
		public static final String TESTMONTH = "Diagnostic.TestMonth";
		public static final String TESTYEAR = "Diagnostic.TestYear";
		public static final String RESULT = "Diagnostic.Result";
		public static final String OUTOFRANGE = "Diagnostic.OutOfRange";
		public static final String INVESTIGATIONPATIENT = "Diagnostic.InvestigationPatient";
		public static final String MOREINFOAVAILABLE = "Diagnostic.MoreInfoAvailable";
		public static final String DIAGNOSTIC_SAVE = "Diagnostic.Save";
		public static final String DIAGNOSTIC_TAB = "Tab.Diagnostic";
	}
	
	public static class DrugTabKeys{
		
		
		public static final String ADD_NEWDRUG = "Drug.AddNewDrug";
		public static final String MADEBY = "Drug.MadeBy";
		public static final String GENERICDRUGNAME = "Drug.GenericDrugName";
		public static final String REPORTED_DRUGNAME = "Drug.ReportedDrugName";
		public static final String CHARACTERIZATION = "Drug.Characterization";
		public static final String ACTIONTAKEN = "Drug.ActionTaken";
		public static final String DOSAGE_FORM = "Drug.DosageForm";
		public static final String ROUTEOFADMIN = "Drug.RouteOfAdmin";
		public static final String ROUTEOFADMIN_DROPDOWN = "Drug.RouteOfAdminDropDown";
		public static final String PARENT_ROUTE_OF_ADMIN = "Drug.ParentRouteOfAdmin";
		public static final String PARENT_DOSING = "Drug.ParentDosing";
		public static final String START_DRUG = "Drug.StartDrug";
		public static final String START_DRUGDAY = "Drug.StartDrugDay";
		public static final String START_DRUGMONTH = "Drug.StartDrugMonth";
		public static final String START_DRUGYEAR = "Drug.StartDrugYear";
		public static final String LAST_ADMIN = "Drug.LastAdmin";
		public static final String LAST_ADMINDAY = "Drug.LastAdminDay";
		public static final String LAST_ADMINMONTH = "Drug.LastAdminMonth";
		public static final String LAST_ADMINYEAR = "Drug.LastAdminYear";
		public static final String DRUG_DURATION = "Drug.DrugDuration";
		public static final String DRUG_DURATION_UNIT = "Drug.DrugDurationUnit";
		public static final String GESTATION_PERIOD_ATTIMEOF_EXPOSURE = "Drug.GestationPeriodAtTimeOfExposure";
		public static final String GESTATION_PERIOD_ATTIMEOF_EXPOSURE_UNIT = "Drug.GestationPeriodAtTimeOfExposureUnit";
		public static final String ADDITIONAL_INFO = "Drug.AdditionalInfo";
		public static final String SAMPLE_AVAILABLE = "Drug.SampleAvailable";
		public static final String DOSAGE_STATUS = "Drug.DosageStatus";
		public static final String DOSAGE_RANK = "Drug.DosageRank";
		public static final String DOSE = "Drug.Dose";
		public static final String DOSE_UNIT = "Drug.DosageUnit";
		public static final String NUM_DOSAGES = "Drug.Dosages";
		public static final String INTERVAL = "Drug.Interval";
		public static final String INTERVAL_FREQ = "Drug.IntervalFreq";
		public static final String DOSAGE_START = "Drug.DosageStartDay";
		public static final String DOSAGE_STARTDAY = "Drug.DosageStartDay";
		public static final String DOSAGE_STARTMONTH = "Drug.DosageStartMonth";
		public static final String DOSAGE_STARTYEAR = "Drug.DosageStartYear";
		public static final String DOSAGE_END = "Drug.DosageEndDay";
		public static final String DOSAGE_ENDDAY = "Drug.DosageEndDay";
		public static final String DOSAGE_ENDMONTH = "Drug.DosageEndMonth";
		public static final String DOSAGE_ENDYEAR = "Drug.DosageEndYear";
		public static final String FIRST_REACTION = "Drug.FirstReaction";
		public static final String INDICATION_RANK = "Drug.IndicationRank";
		public static final String REPORTED_INDICATION = "Drug.ReportedIndication";
		public static final String LOW_LEVEL_TERM = "Drug.LowLevelTerm";
		public static final String PREFFERED_TERM = "Drug.PrefferedTerm";
		public static final String CODING_STATUS = "Drug.CodingStatus";
		public static final String BATCHLOT = "Drug.BatchLot";
		public static final String EXPIRATION = "Drug.Expiration";
		public static final String EXPIRATION_MONTH = "Drug.ExpirationMonth";
		public static final String EXPIRATION_YEAR = "Drug.ExpirationYear";
		public static final String PRODUCT_QUALITY = "Drug.ProductQuality";
		public static final String REPORTED_DEFECT = "Drug.ReportedDefect";
		public static final String SENT_TO_QAYN = "Drug.SentToQAYN";
		public static final String SENT_TO_QA = "Drug.SentToQADay";
		public static final String SENT_TO_QADAY = "Drug.SentToQADay";
		public static final String SENT_TO_QAMONTH = "Drug.SentToQAMonth";
		public static final String SENT_TO_QAYEAR = "Drug.SentToQAYear";
		public static final String INVALID_LOT = "Drug.InvalidLot";
		public static final String INVESTIGATION = "Drug.Investigation";
		public static final String INVESTIGATION_CONSLUSION = "Drug.InvestigationConslusion";
		public static final String DESCRIPTION = "Drug.Description";
		public static final String START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION = "Drug.FirstAdminToFirstReaction";
		public static final String START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION_UNIT = "Drug.FirstAdminToFirstReactionUnit";
		public static final String LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION = "Drug.LastDoseToFirstReaction";
		public static final String LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION_UNIT = "Drug.LastDoseToFirstReactionUnit";
		public static final String COUNTRY_WHERE_DRUG_OBTAIN = "Drug.CountryWhereDrugObtain";
		public static final String COUNTRY_WHERE_DRUG_OBTAIN_DROPDOWN = "Drug.CountryWhereDrugObtainDropDown";
		public static final String WHERE_DRUG_PURCHASE = "Drug.WhereDrugPurchase";
		public static final String PURCHASE_LOCATION = "Drug.PurchaseLocation";
		public static final String PHONE_NUMBER = "Drug.PhoneNumber";
		public static final String ADDRESS = "Drug.Address";
		public static final String MANUFACTURER = "Drug.ManuFacturer";
		public static final String DEPARTMENT = "Drug.Department";
		public static final String NON_COMAPNY_ADDRESS = "Drug.NonComapnyAddress";
		public static final String CITY = "Drug.City";
		public static final String STATE = "Drug.State";
		public static final String POSTAL_CODE = "Drug.PostalCode";
		public static final String COUNTRY = "Drug.Country";
		public static final String COUNTRYDROPDOWN = "Drug.CountryDropDown";
		public static final String VACCINE_STATE = "Drug.VaccineState";
		public static final String VACCINE_COUNTRY = "Drug.VaccineCountry";
		public static final String VACCINE_DAY = "Drug.VaccineDay";
		public static final String VACCINE_MONTH = "Drug.VaccineMonth";
		public static final String VACCINE_YEAR = "Drug.VaccineYear";
		public static final String VACCINE_HOUR = "Drug.VaccineHour";
		public static final String VACCINE_MINUTE = "Drug.VaccineMinute";
		public static final String VACCINE_AT = "Drug.VaccineAt";
		public static final String VACINE_PURCHASE_WITH = "Drug.VacinePurchaseWith";
		public static final String EVENT_REPORTED_PREVIOUSLY_NO = "Drug.EventReportedPreviously_NO";
		public static final String EVENT_REPORTED_PREVIOUSLY_TO_DOCTOR = "Drug.EventReportedPreviously_ToDoctor";
		public static final String EVENT_REPORTED_PREVIOUSLY_TO_MANUFACTURER = "Drug.EventReportedPreviously_ToManufacturer";
		public static final String EVENT_REPORTED_PREVIOUSLY_HEALTH_DEPARTMENT = "Drug.EventReportedPreviously_healthDepartment";
		public static final String REPORTEDDRUG_LOOKUP_BUTTON = "Drug.ReportedDrugLookUpButton";
		public static final String BATCH_RANK = "Drug.BatchRank";
		public static final String SAVE = "Drug.Save";
		public static final String RELATEDNESS = "Drug.Relatedness";
		public static final String RELATEDNESS_TBODYID = "Relatedness.TbodyID";
		public static final String RELATENESS_SAVE = "Relateness.Save";
		public static final String REPORTEDCASUALITY_PREFIX = "Relatedness.ReportedCasualityPrefix";
		public static final String REPORTEDCASUALITY_POSTFIX = "Relatedness.ReportedCasualityPostfix";
		public static final String COMPANYCASUALITY_PREFIX = "Relatedness.CompanyCasualityPrefix";
		public static final String COMPANYCASUALITY_POSTFIX = "Relatedness.CompanyCasualityPostfix";
		public static final String COUNTRY_SPECIFIC_LABELING = "Drug.CountrySpecificLabeling";
		public static final String COUNTRY_SPECIFIC_LABELING_TBODY_ID = "CountrySpecificLabeling.TbodyID";
		public static final String MAH_LABELING_POSTFIX = "CountrySpecificLabeling.MAHLabelingPostfix";		
		public static final String MAH_LABELING_PREFIX = "CountrySpecificLabeling.MAHLabelingPrefix";		
		public static final String COUNTRYSPECIFICLABELING_SAVE = "CountrySpecificLabeling.Save";		
		public static final String CORE_LABELING = "Drug.CoreLabeling";
		public static final String CORELABEL_TBODYID = "CoreLabeling.CoreLabelTbodyID";
		public static final String CORELABELPREFIX = "CoreLabeling.CoreLabelPrefix";
		public static final String CORELABELPOSTFIX = "CoreLabeling.CoreLabelPostfix";
		public static final String CORE_LABELING_SAVE = "CoreLabeling.Save";
		public static final String MAH_LOOKUPBUTTON = "Drug.MAHLookupButton";
		public static final String REPORTED_DRUG_LOOKUP_GENERICNAME = "Drug.ReportedDrugLookUp.GenericName";
		public static final String REPORTED_DRUG_LOOKUP_SEARCH_BUTTON = "Drug.ReportedDrugLookUp.Search";
		public static final String REPORTED_DRUG_LOOKUP_RESULT_TBODY_ID = "Drug.ReportedDrugLookUp.ResultTBodyID";
		public static final String REPORTED_DRUG_LOOKUP_RESULTFORM_PREFIX = "Drug.ReportedDrugLookUp.ResultFormPrefix";
		public static final String REPORTED_DRUG_LOOKUP_RESULTFORM_POSTFIX = "Drug.ReportedDrugLookUp.ResultFormPostfix";
		public static final String REPORTED_DRUG_LOOKUP_RESULTCHARACTERISTICS_PREFIX = "Drug.ReportedDrugLookUp.ResultCharacteristicsPrefix";
		public static final String REPORTED_DRUG_LOOKUP_RESULTCHARACTERISTICS_POSTFIX = "Drug.ReportedDrugLookUp.ResultCharacteristicsPostfix";
		public static final String REPORTED_DRUG_LOOKUP_RESULTFORM = "Drug.ReportedDrugLookUp.ResultForm";
		public static final String REPORTED_DRUG_LOOKUP_RESULTCHARACTERISTICS= "Drug.ReportedDrugLookUp.ResultCharacteristics";
		public static final String REPORTED_DRUG_LOOKUP_RESULTGENERICNAME= "Drug.ReportedDrugLookUp.ResultGenericName";
		public static final String REPORTED_DRUG_LOOKUP_RESULTGENERICNAME_PREFIX= "Drug.ReportedDrugLookUp.ResultGenericNamePrefix";
		public static final String REPORTED_DRUG_LOOKUP_RESULTGENERICNAME_POSTFIX= "Drug.ReportedDrugLookUp.ResultGenericNamePostfix";
		public static final String REPORTED_DRUG_LOOKUP_CLOSE= "Drug.ReportedDrugLookUp.close";
		public static final String TAB= "Tab.Drug";
		public static final String DRUG_COUNTRY_SPECIFIC_APPROVAL= "Drug.CountrySpecificApproval";
		public static final String COUNTRY_SPECIFIC_APPROVAL_ADDNEW= "CountrySpecificApproval.AddNew";
		public static final String COUNTRY_SPECIFIC_APPROVAL_TBODYID= "CountrySpecificApproval.TbodyID";
		public static final String COUNTRY_SPECIFIC_APPROVAL_SELECT_PRODUCTCODE_PREFIX= "CountrySpecificApproval.SelectProductCode.Prefix";
		public static final String COUNTRY_SPECIFIC_APPROVAL_SELECT_PRODUCTCODE_POSTFIX= "CountrySpecificApproval.SelectProductCode.Postfix";
		public static final String DELETE_DRUG_TBODYID = "Drug.DeleteDrug.TbodyID";
		public static final String DELETE_DRUG_PRODUCTNAME_PREFIX = "Drug.DeleteDrug.ProductName.Prefix";
		public static final String DELETE_DRUG_PRODUCTNAME_POSTFIX = "Drug.DeleteDrug.ProductName.Postfix";
		public static final String DELETE_DRUG_DELETESIGN_PREFIX = "Drug.DeleteDrug.DeleteSign.Prefix";
		public static final String DELETE_DRUG_DELETESIGN_POSTFIX  = "Drug.DeleteDrug.DeleteSign.Postfix";

		public static final String ERROR_MESSAGE_DETAILS_TBODYID  			= 	"Drug.ErrorMessageDetails.TBodyID";
		public static final String ERROR_MESSAGE_DETAILS  					= 	"Drug.ErrorMessageDetails";
		public static final String ERROR_MESSAGE_DETAILS_PREFIX  			= 	"Drug.ErrorMessageDetails.Prefix";
		public static final String ERROR_MESSAGE_DETAILS_POSTFIX  			= 	"Drug.ErrorMessageDetails.Postfix";

		public static final String GET_APPROVAL_NUMBERS			  			= 	"CountrySpecificApproval";
		public static final String GETAPPROVAL_TBODYID						= 	"CountrySpecificApproval.GetApprovalTbodyId";
		public static final String GET_APPROVAL_NUMBERS_GET_TEXT_PREFIX		=	"GetApprovalNumbers.GetTextPrefix";
		public static final String GET_APPROVAL_NUMBERS_GET_TEXT_POSTFIX	=	"GetApprovalNumbers.GetTextPostfix";

		public static final String DRUG_DOSE_ADDITIONAL_INFO				=	"Drug.DoseAdditionalInfo";
		public static final String DRUG_DOSE_ADDITIONAL_INFO_TEXT			=	"Drug.DoseAdditionalInfoText";
		public static final String DRUG_DOSE_ADDITIONAL_INFO_CONTINUE		=	"Drug.DoseAdditionalInfoContinue";
		public static final String DOSAGE_TEXT								= 	"Dosage Text";
		public static final String REPORTED_CASUALLITY						= 	"Reported Casuallity";
		public static final String COMPANY_CAUSALITY						= 	"Company Causality";
		public static final String COUNTRY_SPECIFIC_LABELING_LABLE			= 	"Country Specific Labeling";
		public static final String CORE_LABELING_LABLE						= 	"Core Labeling";

		public static final String CC_SUSPECT								= 	"Suspect";
		public static final String CC_SUSPECT_DRUG_INTERACTING				= 	"Suspect Drug Interacting";
		public static final String CC_CONCOMITANT							= 	"Concomitant";
	}
	 
	public static class Name
	{
		public static final String PRODUCTCODE								= 	"ProductCode";
		public static final String ICU										= 	"ICUName";
		public static final String LINK_TYPE								= 	"Link Type";
		public static final String AER_NUMBER								= 	"AER Number";
		public static final String OTHER_ID_NUMBER							= 	"Other Id Number";
		public static final String LINKED_CASES_REPORT_NO					= 	"Linked Cases Report No";
		public static final String MEDICALLY_IMPORTANT						= 	"Medically Important";
		public static final String DOSE										= 	"Dose";
		public static final String DOSAGE									= 	"Dosage";
		public static final String INTERVAL									= 	"Interval";
		public static final String LISTED									= 	"Listed";
		public static final String UNLISTED									= 	"Unlisted";
		public static final String AUTO										= 	"Auto";
		public static final String COUNTRYLABELLING_LISTED					= 	"14900";
		public static final String COUNTRYLABELLING_UNLISTED				= 	"14901";
		public static final String COUNTRYLABELLING_AUTO					= 	"14902";
		public static final String LABELED									= 	"Labeled";
		public static final String UNLABELED								= 	"Unlabeled";
		public static final String UNKNOWN									= 	"Unknown";

		public static final String REACTION_RANK							=	"Rank";
		public static final String REACTION_REPORTEDREACTION				=	"Reportedreaction";
		public static final String REACTION_IME								=	"Ime";
		public static final String REACTION_SERIOUS							=	"Serious";
		public static final String REACTION_OUTCOME							=	"Outcome";

	}
	
	public static class CaseType
	{
		public static final String INITIAL									= 	"Initial";
		public static final String FOLLOWUP									= 	"Followup";
		
	}
	
	public static class DrugHistoryTabKeys{
		
		public static final String  REPORTEDDRUGNAME = "DrugHistory.ReportedDrugName";
		public static final String  DRUGNAME = "DrugHistory.DrugName";
		public static final String  CODINGSTATUS1 = "ReportedDrugName.CodingStatus";
		
		public static final String  START = "DrugHistory.Start";
		public static final String  STARTDAY = "DrugHistory.StartDay";
		public static final String  STARTMONTH = "DrugHistory.StartMonth";
		public static final String  STARTYEAR = "DrugHistory.StartYear";

		public static final String  END = "DrugHistory.End";
		public static final String  ENDDAY = "DrugHistory.EndDay";
		public static final String  ENDMONTH = "DrugHistory.EndMonth";
		public static final String  ENDYEAR = "DrugHistory.EndYear";
		public static final String  ROUTEOFADMIN = "DrugHistory.RouteOfAdmin";
		public static final String  DRUGABUSE = "DrugHistory.DrugAbuse";
		public static final String  REPORTEDINDICATION = "DrugHistory.ReportedIndication";
		public static final String  LOWLEVELTERM1 = "DrugHistory.LowLevelTerm1";
		public static final String  CODDINGSTATUS2 = "DrugHistory.CoddingStatus2";
		public static final String  PREFFEREDTERM1 = "DrugHistory.PrefferedTerm1";
		public static final String  REPORTEDREACTION = "DrugHistory.ReportedReaction";
		public static final String  LOWLEVELTERM2 = "DrugHistory.LowLevelTerm2";
		public static final String  CODDINGSTATUS3 = "DrugHistory.CoddingStatus3";
		public static final String  PREFFEREDTERM2 = "DrugHistory.PrefferedTerm2";
		public static final String  SAVE = "DrugHistory.Save";
		public static final String  TAB = "Tab.DrugHistory";
		public static final String  DRUGHISTORY_ADDNEW = "DrugHistory.AddNew";

	}
	
	public static class MedicalHistoryTabKeys{
		
		public static final String REPORTEDDISEASE = "Medical.ReportedDisease";
		public static final String LOWLEVELTERM = "Medical.LowLevelTerm";
		public static final String PREFFEREDTERM = "Medical.PrefferedTerm";
		public static final String CODINGSTATUS = "ReportedDisease.CodingStatus";
		
		public static final String STARTDATE = "Medical.StartDate";
		public static final String STARTDATEDAY = "Medical.StartDateDay";
		public static final String STARTDATEMONTH = "Medical.StartDateMonth";
		public static final String STARTDATEYEAR = "Medical.StartDateYear";
		public static final String CONTINUING = "Medical.Continuing";
		
		public static final String ENDDATE = "Medical.EndDate";
		public static final String ENDDATEDAY = "Medical.EndDateDay";
		public static final String ENDDATEMONTH = "Medical.EndDateMonth";
		public static final String ENDDATEYEAR = "Medical.EndDateYear";
		public static final String MEDICALHISTORYCOMMENT = "Medical.MedialHistoryComment";
		public static final String SAVE = "Medical.Save";
		public static final String TAB = "Tab.MedicalHistory";

		public static final String MEDICAL_ADDNEW = "Medical.AddNew";
		
	}
	
	public static class NarrativeTabKeys
	{
		public static final String  APPLY_TO = "Narrative.ApplyTo";
		public static final String  BRM_NARRATIVE = "Narrative.BRMNarrative";
		public static final String  INITIAL_COMPANY_NARRATIVE_ENGLISH = "Narrative.InitialCompanyNarrative.English";
		public static final String  INITIAL_COMPANY_NARRATIVE_LOCALLANGUAGE = "Narrative.InitialCompanyNarrative.LocalLanguage";
		public static final String  TEMPLATE_TBODY_ID = "Narrative.TemplateSelectTbodyID";

		public static final String  TEMPLATESELECT = "Narrative.TemplateSelect";
		public static final String 	TEMPLATESELECTTBODYID = "Narrative.TemplateSelectTbodyID";
		public static final String  TEMPLATETEXTPREFFIX = "Narrative.TemplateTextPreffix";
		public static final String 	TEMPLATETEXTPOSTFIX = "Narrative.TemplateTextPostfix";
		public static final String 	TEMPLATE_APPLY = "Narrative.TemplateApply";
		public static final String 	SAVE = "Narrative.Save";
		public static final String 	NARRATIVE_TAB = "Tab.Narrative";
		
		public static final String SUA = "Narrative.SUA";
		public static final String SUA_TEMPLATELIST_TBODYID = "Narrative.SUATemplateListTbodyID";
		public static final String SUA_TEMPLATE_SELECT_BUTTON = "Narrative.SUATemplateSelectButton";
		public static final String SUA_TEMPLATENAME_SELECT_PREFIX = "Narrative.SUATemplateNameSelectPrefix";
		public static final String SUA_TEMPLATENAME_SELECT_POSTFIX = "Narrative.SUATemplateNameSelectPostfix";
		public static final String SUA_SUMMARY_TEXTAREA = "Narrative.SUASummaryTextarea";
		public static final String SUA_TEMPLATE_APPLY = "Narrative.SUATemplateApply";
		public static final String SUA_SAVE = "Narrative.SUASave";
		
		public static final String  OTHER_NARRATIVE = "Narrative.OtherNarrative";
		public static final String  PSUR_COMMENTS_TEXTAREA  = "OtherNarrative.PSURCommentsTextarea";
		public static final String  REPORTERS_COMMENTS_TEXTAREA  = "OtherNarrative.ReportersCommentsTextarea";
		public static final String  SENDERS_COMMENTS_TEXTAREA  = "OtherNarrative.SendersCommentsTextarea";
		public static final String  MEDICAL_ASSESMENT_TEXTAREA  = "OtherNarrative.MedicalAssesmentTextarea";
		public static final String  OTHERNARRATIVE_SAVE  = "OtherNarrative.Save";

	}
	
	public static class ReactionTabKeys{
		
		public static final String REPORTED_REACTION = "Reaction.ReportedReaction";
		public static final String LOWLEVEL_TERM = "Reaction.LowLevelTerm";
		public static final String CODDING_STATUS = "Reaction.CoddingStatus";
		public static final String PREFFERED_TERM = "Reaction.PrefferedTerm";
		public static final String VERSION_WHEN_CODED = "Reaction.VersionWhenCoded";
		public static final String SIREOUS_RADIO = "Reaction.SireousRadio";
		public static final String REASON_FOR_NON_IME = "Reaction.ReasonForNonIME";
		public static final String RATIONAL_FOR_NONIME = "Reaction.RationalForNonIME";
		public static final String OUTCOME = "Reaction.OutCome";
		public static final String TERM_HIGHLIGHTEDBY_REPORTER = "Reaction.TermHighlightedByReporter";
		public static final String REPORTED_CAUSE_OF_DEATH = "Reaction.ReportedCauseOfDeath";
		public static final String AUTOSPY_CAUSE_OF_DEATH = "Reaction.AutospyCauseOfDeath";
		public static final String REACTION_STARTED = "Reaction.ReactionStarted";
		public static final String REACTION_STARTED_DAY = "Reaction.ReactionStartedDay";
		public static final String REACTION_STARTED_MONTH = "Reaction.ReactionStartedMonth";
		public static final String REACTION_STARTED_YEAR = "Reaction.ReactionStartedYear";
		public static final String REACTION_STARTED_HOUR = "Reaction.ReactionStartedHour";
		public static final String REACTION_STARTED_MINUTE = "Reaction.ReactionStartedMinute";
		public static final String REACTION_END = "Reaction.ReactionEnded";
		public static final String REACTION_END_DAY = "Reaction.ReactionEndedDay";
		public static final String REACTION_END_MONTH = "Reaction.ReactionEndedMonth";
		public static final String REACTION_END_YEAR = "Reaction.ReactionEndYear";
		public static final String REACTION_END_HOUR = "Reaction.ReactionEndHour";
		public static final String REACTION_END_MINUTE = "Reaction.ReactionEndMinute";
		public static final String REACTION_DURATION = "Reaction.ReactionDuration";
		public static final String REACTION_DURATION_UNIT = "Reaction.ReactionDurationUnit";
		public static final String DRUG_ADMIN_TO_START_OF_THIS_REACTION = "Reaction.DrugAdminToStartOfThisReaction";
		public static final String DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT = "Reaction.DrugAdminToStartOfThisReactionUnit";
		public static final String LAST_DOSE_TO_START_OF_THIS_REACTION = "Reaction.LastDoseToStartOfThisReaction";
		public static final String LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT = "Reaction.LastDoseToStartOfThisReactionUnit";
		public static final String SAVE = "Reaction.Save";
		public static final String TAB = "Tab.Reaction";
		public static final String REACTION_ADDNEW = "Reaction.AddNew";
		
		
		public static final String REACTION_LISTALL_BUTTON						="Reaction.Listall.Button";
		public static final String REACTION_LISTALL_TBODYID						="Reaction.Listall.TBodyID";
		public static final String REACTION_LISTALL_RANK_PREFIX					="Reaction.Listall.Rank.Prefix";
		public static final String REACTION_LISTALL_RANK_SUFFIX					="Reaction.Listall.Rank.Suffix";
		public static final String REACTION_LISTALL_REPORTEDREACTION_PREFIX		="Reaction.Listall.Reportedreaction.Prefix";
		public static final String REACTION_LISTALL_REPORTEDREACTION_SUFFIX		="Reaction.Listall.Reportedreaction.Suffix";
		public static final String REACTION_LISTALL_IME_PREFIX					="Reaction.Listall.Ime.Prefix";
		public static final String REACTION_LISTALL_IME_SUFFIX					="Reaction.Listall.Ime.Suffix";
		public static final String REACTION_LISTALL_SERIOUS_PREFIX				="Reaction.Listall.Serious.Prefix";
		public static final String REACTION_LISTALL_SERIOUS_SUFFIX				="Reaction.Listall.Serious.Suffix";
		public static final String REACTION_LISTALL_OUTCOME_PREFIX				="Reaction.Listall.Outcome.Prefix";
		public static final String REACTION_LISTALL_OUTCOME_SUFFIX				="Reaction.Listall.Outcome.Suffix";

	}
		
	public static class DownloadSD
	{
		public static final String GETTEXT_PREFIX = "Download.getText.Prefix";
		public static final String GETTEXT_MIDDLE = "Download.getText.Middle";
		public static final String GETTEXT_SUFFIX = "Download.getText.Suffix";

		public static final String GETTEXTCVV_PREFIX = "Download.getTextCVV.Prefix";
		public static final String GETTEXTCVV_SUFFIX = "Download.getTextCVV.Suffix";
		
		public static final String DOCUMENT_NAME = "Document Name";
		public static final String DOCUMENT_TYPE = "Document Type";
		public static final String DATE_OF_RECEIPT = "Date of Receipt";
		public static final String CASE_VERSION = "Case Version";
		public static final String DATE_ATTACHED = "Date Attached";
		public static final String DATE_SENT_TO_CCV = "Date Sent to CCV";
		public static final String SEND_TO_CCV = "Send to CCV";
	}
	
	public static class Logout
	{
		public static final String LOGOUT = "Sceptre.Logout";
	}
		
	public static class BrowserDetails
	{
		public static final String BROWSERVERSION 	= "BrowserVersion";
		public static final String BROWSERNAME 	= "BrowserName";

	}

	public static class CheckElementPresence
	{
		public static final String DOWNLOAD_SD_TBODY_PRESENT					= "Download.TBodyIsPresent"					;
		public static final String RELATEDNESS_TBODYID_PRESENT					= "Relatedness.TbodyIDPresent"				;
		public static final String COUNTRY_SPECIFIC_LABELING_TBODYID_PRESENT	= "CountrySpecificLabeling.TbodyIDPresent"	;
		public static final String CORELABELING_TBODYID_PRESENT					= "CoreLabeling.TbodyIDPresent"				;
		public static final String COUNTRY_SPECIFIC_APPROVAL_TBODYID_PRESENT	= "CountrySpecificApproval.TbodyIdPresent"	;
		public static final String OTHER_IDENTIFICATION_NUMBER_PRESENT			= "CaseID.OtherIdentificationNumber.Present";
		public static final String LINKEDCASES_PRESENT							= "CaseID.LinkedCases.Present"				;
		public static final String COUNTRY_SPECIFIC_APPROVAL_PRESENT			= "CountrySpecificApproval.Present"				;
		public static final String REACTION_LISTALL_TBODYID_PRESENT			= "Reaction.Listall.TBodyID.Present"				;
		
	}
}