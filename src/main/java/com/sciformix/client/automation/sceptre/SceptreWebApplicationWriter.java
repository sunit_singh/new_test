package com.sciformix.client.automation.sceptre;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.client.automation.BaseWebApplicationWriter;
import com.sciformix.client.automation.Case;
import com.sciformix.client.automation.CaseDocument;
import com.sciformix.client.automation.ElementUiConfig;
import com.sciformix.client.automation.SearchResults;
import com.sciformix.client.automation.WebApplicationWriterUtils;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.automation.sceptre.SceptreConstants.FETCH_SCOPE;
import com.sciformix.client.automation.sceptre.SceptreConstants.ResultFieldsXpath;
import com.sciformix.client.automation.sceptre.SceptreConstants.SceptreSearchFields;
import com.sciformix.client.automation.sceptre.SceptreConstants.SearchFieldXpath;
import com.sciformix.commons.SciException;
import com.sciformix.commons.rulesengine.Criterion;
import com.sciformix.commons.utils.StringUtils;



public class SceptreWebApplicationWriter extends BaseWebApplicationWriter {

	private static final Logger log = LoggerFactory.getLogger(SceptreWebApplicationWriter.class);
	
	private static Map<String, String> propMap = new HashMap<String, String>();

	private static final String CONFIG_FOR_INSERT_PROPERTIES = "ConfigForSceptre.properties";

	private static String s_sBrowserDriverLocation = null;

	private static String s_sDownloadFolder = null;
	
	private static Map<String , String> m_mMonthMap = null;
	
	private static Map<SceptreSearchFields, ElementUiConfig> s_mapSearchConfiguration = new HashMap<SceptreSearchFields, ElementUiConfig>();
	
	private static Map<String, List<String>> s_mapTabwiseFields = new HashMap<String, List<String>>();
	
	private static Map<String, ElementUiConfig> s_mapFieldConfiguration = new HashMap<String, ElementUiConfig>();
	
	private static final String Day = "Day";
	
	private static final String Month = "Month";
	
	private static final String Year = "Year";

	private static final String DOWNLOADFOLDER_SUFFIX = "\\AppData\\Local\\Temp";

	public static boolean isThresholdToBeChecked = false;
	
	public static int thresholdSearchResultCount = -1;
	
	private static final String DOWNLOAD_SUBFOLDER = "\\DownloadTempFolder";
	
	static
	{
		propMap = BaseWebApplicationWriter.loadProperties(CONFIG_FOR_INSERT_PROPERTIES);

		//create folder in temp folder to save download files
		if(WebApplicationWriterUtils.createDirectories(System.getenv("USERPROFILE") + DOWNLOADFOLDER_SUFFIX + DOWNLOAD_SUBFOLDER))
		{
			
			s_sDownloadFolder = System.getenv("USERPROFILE") + DOWNLOADFOLDER_SUFFIX + DOWNLOAD_SUBFOLDER;
		}
		else
		{
			s_sDownloadFolder = System.getenv("USERPROFILE") + DOWNLOADFOLDER_SUFFIX ;
			
		}
		
		m_mMonthMap = getMonthMap();
		
		log.info("properties file loaded");

		s_sBrowserDriverLocation = WebApplicationWriterUtils.getBrowserLocation(propMap.get("webdriver.driverlocation"));
		
		mapSearchFields();
				
		listFieldTabwiseForCaseIDTab();
		
		mapFieldForCaseIDTab();
		
		listFieldTabwiseForPatientTab();
		
		mapFieldForPatientTab();
		
		listFieldTabwiseForReporterTab();
		
		listFieldTabwiseForAddNewReporterTab();
		
		mapFieldForReporterTab();
		
		listFieldTabwiseForDrugTab();
		
		listFieldTabwiseForAddNewDrug();
		
		mapFieldForDrugTab();
		
		listFieldTabwiseForMedicalHistoryTab();
		
		listFieldTabwiseForAddNewMedicalHistoryTab();
		
		mapFieldForMedicalHistoryTab();
		
		listFieldTabwiseForDrugHistoryTab();
		
		listFieldTabwiseForAddNewDrugHistoryTab();
		
		mapFieldForDrugHistoryTab();
		
		listFieldTabwiseForReactionTab();
		
		listFieldTabwiseForAddNewReactionTab();
		
		mapFieldForReactionTab();
		
		listFieldTabwiseForDiagnosticTab();
		
		mapFieldForDiagnosticTab();
		
		listFieldTabwiseForNarrativeTab();
		
		mapFieldForNarrativeTab();
		
	/****************************for reading**************************/
		
		listFieldForReadingDrugTab();
	}

	
	private static List<String> getFieldsToBePopulated(String sTabName)
	{
		return s_mapTabwiseFields.get(sTabName);
	}
	
	
	public SceptreWebApplicationWriter(BROWSER p_eBrowser)
	{
		super(p_eBrowser, propMap.get("webdriver.browserdriver"), s_sBrowserDriverLocation, true , s_sDownloadFolder);
	}
	
	public SceptreWebApplicationWriter(WebDriver driver)
	{
		super(driver);
	}
	
	
	private static void mapSearchFields()
	{
		s_mapSearchConfiguration.put(SceptreSearchFields.LOCAL_CASE_ID, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.LOCAL_CASE_ID), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.AER_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.AER_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.COUNTRY, ElementUiConfig.createField("xpath-dropdown", propMap.get(SearchFieldXpath.COUNTRY), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.OTHER_ID, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.OTHER_ID), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_INITIALS, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.PATIENT_INITIALS), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_AGEOPERATOR, ElementUiConfig.createField("xpath-dropdown", propMap.get(SearchFieldXpath.PATIENT_AGEOPERATOR), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_AGEUNIT, ElementUiConfig.createField("xpath-dropdown", propMap.get(SearchFieldXpath.PATIENT_AGEUNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_AGE1, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.PATIENT_AGE1), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_AGE2, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.PATIENT_AGE2), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_GENDER, ElementUiConfig.createField("xpath-dropdown", propMap.get(SearchFieldXpath.PATIENT_GENDER), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_DOBOPERATOR, ElementUiConfig.createField("xpath-dropdown", propMap.get(SearchFieldXpath.PATIENT_DOBOPERATOR), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_DODOPERATOR, ElementUiConfig.createField("xpath-dropdown", propMap.get(SearchFieldXpath.PATIENT_DODOPERATOR), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_DOB1, ElementUiConfig.createDateField("xpath-name", new String[] { propMap.get(SearchFieldXpath.PATIENT_DOB1)}, SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_DOB2, ElementUiConfig.createDateField("xpath-name", new String[] {  propMap.get(SearchFieldXpath.PATIENT_DOB2) }, SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_DOD1, ElementUiConfig.createDateField("xpath-name", new String[] { propMap.get(SearchFieldXpath.PATIENT_DOD1) }, SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_DOD2, ElementUiConfig.createDateField("xpath-name", new String[] { propMap.get(SearchFieldXpath.PATIENT_DOD2) }, SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PATIENT_MEDICATION_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.PATIENT_MEDICATION_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.PROTOCOL, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.PROTOCOL), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.REPORTED_REACTION, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.REPORTED_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.REPORTER_CITY, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.REPORTER_CITY), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.REPORTER_FAMILYNAME, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.REPORTER_FAMILYNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.REPORTER_GIVENNAME, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.REPORTER_GIVENNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.REPORTER_ORGANIZATION, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.REPORTER_ORGANIZATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.REPORTER_STATE, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.REPORTER_STATE), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.SITE_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.SITE_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.GENERICDRUG, ElementUiConfig.createField("xpath-name", propMap.get(SearchFieldXpath.GENERICDRUG), SceptreConstants.NAME_IDENTIFIER));
		s_mapSearchConfiguration.put(SceptreSearchFields.SHOW_DELETED_CASES, ElementUiConfig.createField("xpath-value", propMap.get(SearchFieldXpath.SHOW_DELETED_CASES), SceptreConstants.NAME_IDENTIFIER));
	
	}
	
	
	private static void listFieldTabwiseForCaseIDTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START);

		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REPORTED_TO_REGULATOR_BY_SOURCE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REPORT_TYPE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REPORT_TYPE_FOLLOWUP);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.ADVERSEEVENT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.MEDICALLY_CONFIRMED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE);
		
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.CAUSED_PROLONGED_HOSPITALIZATION);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.DISABLING_INCAPACITATING);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.DOCUMENTS_HELD_BY_SENDER);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.COUNTRY_WHEREAE_OCCOURED_FOLLOWUP);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE_FOLLOWUP);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.INITIATING_COMPANY_UNIT);
//		
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY);
//	
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.CONGENITAL_ANOMALY_BIRTH_DEFECT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.ADDITIONAL_DOCUMENTS_AVAILABLE);
		//listTabwiseFields.s_mapFieldConfiguration.put(aseIdTabKeys.CASE_PROCESSING_FUNCTION);
		//listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.INITIATING_JNJ_UNIT);
		//listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.INVALIDCASE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LIFE_THREATENING);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LITEARTURE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.NATURE_OF_COMPLAINT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.OTHER_COMPLAINT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.OTHER_MEDICALLY_IMPORTANT_CONDITION);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.PARENTCHILD);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.PREGNANCY);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.PRODUCTCOMPLAINT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REASON_FOR_INVALID_CASE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.RESULTS_IN_DEATH);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.SERIOUS);
		
		s_mapTabwiseFields.put("CASEID", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForPatientTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PATIENT_ID);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PATIENT_SEX);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.POSTAL_CODE);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.AGE);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.AGE_UNIT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.AGE_GROUP);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.HEIGHT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.HEIGHT_UNIT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.WEIGHT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.WEIGHT_UNIT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PHYSICIAN_AWARE_OF_ADVERSE_EVENT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PERMISSION_TO_CONTACT_PHYSICIAN);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_DEATH);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.WAS_AUTOPSY_DONE);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.NUMBER_OF_CIGARETTES_PER_DAY);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.NUMBER_OF_YEARS_SMOKED);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.NUMBER_OF_DRINKS_PER_DAY);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.TYPE_OF_ALCOHOL);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PHYSICAL_ACTIVITY);
		
		s_mapTabwiseFields.put("PATIENT", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForReporterTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.QUALIFICATION);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.TITLE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ANONYMOU_REFUSED);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.MIDDLE_NAME);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADMINISTERED_VACCINE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.DEGREE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ORGANIZATION);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.DEPARTMENT);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDRESS1);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDRESS2);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDRESS3);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.CITY);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PROVINCE_STATE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.POSTAL_CODE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COUNTRY);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE1_CODE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE1_NUMBER);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE1_EXT);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE2_CODE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE2_NUMBER);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE2_EXT);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAX_CODE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAX_EXT);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAX_NUMBER);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.CONTACT_METHOD);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_TITLE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_GIVEN_NAME);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_FAMILY_NAME);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.SCHEDULE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.RESPONSE_RECEIVED);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.SUFFICENT_DETAIL);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDITIONAL_LETTER_TEXT);
		
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_QUALIFICATION);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_REPORTER);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.STATUS);

		s_mapTabwiseFields.put("REPORTER", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForAddNewReporterTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.QUALIFICATION);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.TITLE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.GIVEN_NAME);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ANONYMOU_REFUSED);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.MIDDLE_NAME);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAMILY_NAME);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADMINISTERED_VACCINE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.DEGREE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ORGANIZATION);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.DEPARTMENT);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDRESS1);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDRESS2);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDRESS3);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.CITY);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PROVINCE_STATE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.POSTAL_CODE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COUNTRY);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE1_CODE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE1_NUMBER);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE1_EXT);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE2_CODE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE2_NUMBER);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.PHONE2_EXT);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAX_CODE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAX_EXT);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.FAX_NUMBER);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.CONTACT_METHOD);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_TITLE);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_GIVEN_NAME);
		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_FAMILY_NAME);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.SCHEDULE);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.RESPONSE_RECEIVED);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.SUFFICENT_DETAIL);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.ADDITIONAL_LETTER_TEXT);
		
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_QUALIFICATION);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.COMPANY_REPORTER);
//		listTabwiseFields.add(SceptreConstants.ReporterTabKeys.STATUS);

		s_mapTabwiseFields.put("ADDNEWREPORTER", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForDrugTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.MADEBY);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CHARACTERIZATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ACTIONTAKEN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_FORM);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN_DROPDOWN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PARENT_DOSING);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DRUG_DURATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE_UNIT);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ADDITIONAL_INFO);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SAMPLE_AVAILABLE);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_RANK);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_STATUS);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_START);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_END);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSE_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.NUM_DOSAGES);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INTERVAL);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INTERVAL_FREQ);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.FIRST_REACTION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_TEXT);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INDICATION_RANK);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_INDICATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LOW_LEVEL_TERM);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PREFFERED_TERM);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CODING_STATUS);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.BATCH_RANK);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.BATCHLOT);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EXPIRATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PRODUCT_QUALITY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_DEFECT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SENT_TO_QAYN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVALID_LOT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVESTIGATION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVESTIGATION_CONSLUSION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DESCRIPTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_ADMIN_TO_FIRST_REACTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_ADMIN_TO_FIRST_REACTION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_DOSE_TO_FIRST_REACTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_DOSE_TO_FIRST_REACTION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN_DROPDOWN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.WHERE_DRUG_PURCHASE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PURCHASE_LOCATION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PHONE_NUMBER);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ADDRESS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.MANUFACTURER);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DEPARTMENT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.NON_COMAPNY_ADDRESS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CITY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.STATE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.POSTAL_CODE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRYDROPDOWN);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACCINE_COUNTRY);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACCINE_AT);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACINE_PURCHASE_WITH);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_NO);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_DOCTOR);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_MANUFACTURER);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_HEALTH_DEPARTMENT);
//
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_DRUG);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_ADMIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SENT_TO_QA);
//	
		
		s_mapTabwiseFields.put("DRUG", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForAddNewDrug()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.DrugTabKeys.MADEBY);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CHARACTERIZATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ACTIONTAKEN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_FORM);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN_DROPDOWN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PARENT_DOSING);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DRUG_DURATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ADDITIONAL_INFO);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SAMPLE_AVAILABLE);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_RANK);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_STATUS);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_START);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_END);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSE_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.NUM_DOSAGES);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INTERVAL);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INTERVAL_FREQ);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.FIRST_REACTION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INDICATION_RANK);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_INDICATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LOW_LEVEL_TERM);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PREFFERED_TERM);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CODING_STATUS);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.BATCH_RANK);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.BATCHLOT);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EXPIRATION);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PRODUCT_QUALITY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_DEFECT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SENT_TO_QAYN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVALID_LOT);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVESTIGATION);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVESTIGATION_CONSLUSION);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DESCRIPTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_ADMIN_TO_FIRST_REACTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_ADMIN_TO_FIRST_REACTION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_DOSE_TO_FIRST_REACTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_DOSE_TO_FIRST_REACTION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN_DROPDOWN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.WHERE_DRUG_PURCHASE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PURCHASE_LOCATION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PHONE_NUMBER);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ADDRESS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.MANUFACTURER);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DEPARTMENT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.NON_COMAPNY_ADDRESS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CITY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.STATE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.POSTAL_CODE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRYDROPDOWN);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACCINE_COUNTRY);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACCINE_AT);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACINE_PURCHASE_WITH);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_NO);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_DOCTOR);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_MANUFACTURER);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_HEALTH_DEPARTMENT);
//
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_DRUG);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_ADMIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SENT_TO_QA);
//	
		
		s_mapTabwiseFields.put("ADDNEWDRUG", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForMedicalHistoryTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.LOWLEVELTERM);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.PREFFEREDTERM);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.STARTDATE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.CONTINUING);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.ENDDATE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT);
		
		s_mapTabwiseFields.put("MEDICALHISTORY", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForAddNewMedicalHistoryTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.LOWLEVELTERM);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.PREFFEREDTERM);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.STARTDATE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.CONTINUING);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.ENDDATE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT);
		
		s_mapTabwiseFields.put("ADDNEWMEDICALHISTORY", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForDrugHistoryTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.DRUGNAME);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.START);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.END);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.ROUTEOFADMIN);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.DRUGABUSE);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS2);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM2);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS3);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM2);
		
		s_mapTabwiseFields.put("DRUGHISTORY", listTabwiseFields);
	}
	

	private static void listFieldTabwiseForAddNewDrugHistoryTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.DRUGNAME);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.START);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.END);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.ROUTEOFADMIN);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.DRUGABUSE);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM1);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS2);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM2);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS3);
//		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM2);
		
		s_mapTabwiseFields.put("ADDNEWDRUGHISTORY", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForReactionTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REPORTED_REACTION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LOWLEVEL_TERM);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.CODDING_STATUS);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.PREFFERED_TERM);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.VERSION_WHEN_CODED);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.SIREOUS_RADIO);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REASON_FOR_NON_IME);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.RATIONAL_FOR_NONIME);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.OUTCOME);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.TERM_HIGHLIGHTEDBY_REPORTER);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REPORTED_CAUSE_OF_DEATH);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.AUTOSPY_CAUSE_OF_DEATH);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED_HOUR);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MINUTE);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END_HOUR);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END_MINUTE);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT);
		
		s_mapTabwiseFields.put("REACTION", listTabwiseFields);
	}

	
	private static void listFieldTabwiseForDiagnosticTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.REPORTEDTEST);
/*		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.LOWERLEVELTERM);
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.PREFFEREDTERM);*/
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.UNIT);
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.NORMALRANGELOW);
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.NORMALRANGEHIGH);
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.TEST);
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.RESULT);
//		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.OUTOFRANGE);
		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.MOREINFOAVAILABLE);
//		listTabwiseFields.add(SceptreConstants.DiagnosticTabKeys.INVESTIGATIONPATIENT);

		s_mapTabwiseFields.put("DIAGNOSTIC", listTabwiseFields);
	}
	
	
	private static void listFieldTabwiseForNarrativeTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		
		/*listTabwiseFields.add(SceptreConstants.NarrativeTabKeys.APPLY_TO);
		listTabwiseFields.add(SceptreConstants.NarrativeTabKeys.BRM_NARRATIVE);
		listTabwiseFields.add(SceptreConstants.NarrativeTabKeys.INITIAL_COMPANY_NARRATIVE_ENGLISH);
		listTabwiseFields.add(SceptreConstants.NarrativeTabKeys.INITIAL_COMPANY_NARRATIVE_LOCALLANGUAGE);*/
		
		
		s_mapTabwiseFields.put("NARRATIVE", listTabwiseFields);
	}
	
	
	private static void mapFieldForCaseIDTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CASE_PROCESSING_FUNCTION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.CaseIdTabKeys.CASE_PROCESSING_FUNCTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.INITIATING_COMPANY_UNIT, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.CompanyUnit"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION_TEXTAREA, ElementUiConfig.createField("xpath-button", propMap.get(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION_TEXTAREA), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.INITIATING_JNJ_UNIT, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.InitialJ_JUnit"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_TEXTAREA, ElementUiConfig.createField("xpath-button", propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_TEXTAREA), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REPORTED_TO_REGULATOR_BY_SOURCE, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.ReportedToRegulatorBySource"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.LocalCaseID"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE, ElementUiConfig.createVaryXpathField("xpath-select", new String[] {propMap.get("CaseID.CountryOfPrimarySource") , propMap.get("CaseID.CountryOfPrimarySourceFollowup")}, SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED, ElementUiConfig.createVaryXpathField("xpath-select", new String[] {propMap.get("CaseID.CountryWhereAEOccoured") , propMap.get("CaseID.CountryWhereAEOccouredFollowup")}, SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE_FOLLOWUP, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.CountryOfPrimarySourceFollowup"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.COUNTRY_WHEREAE_OCCOURED_FOLLOWUP, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.CountryWhereAEOccouredFollowup"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.CasePriority"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REPORT_TYPE, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.ReportType"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REPORT_TYPE_FOLLOWUP, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.ReportTypeFollowup"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.ADVERSEEVENT, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.AdverseEvent"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.PREGNANCY, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.Pregnancy"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LITEARTURE, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.Litearture"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.PARENTCHILD, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.ParentChild"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.PRODUCTCOMPLAINT, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.ProductComplaint"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.INVALIDCASE, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.InvalidCase"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.NATURE_OF_COMPLAINT, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.NatureOfComplaint"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.OTHER_COMPLAINT, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.OtherComplaint"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REASON_FOR_INVALID_CASE, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.ReasonForInvalidCase"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.MEDICALLY_CONFIRMED, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.MedicallyConfirmed"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.RegulatoryReleventUpdate"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.ADDITIONAL_DOCUMENTS_AVAILABLE, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.AdditionDocumentsAvailable"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.DOCUMENTS_HELD_BY_SENDER, ElementUiConfig.createField("xpath-textarea", propMap.get("CaseID.DocumentHeldBySenderTextArea"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.SERIOUS, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.Seriousness"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LIFE_THREATENING, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.LifeThreatening"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CAUSED_PROLONGED_HOSPITALIZATION, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Hospitalization"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.DISABLING_INCAPACITATING, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Disabling"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CONGENITAL_ANOMALY_BIRTH_DEFECT, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Congenital"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.RESULTS_IN_DEATH, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Death"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.OTHER_MEDICALLY_IMPORTANT_CONDITION, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Other"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LOOKUP_CCSELECT, ElementUiConfig.createField("xpath-lookup", propMap.get("CaseID.CaseCharactristicsSelect"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LOOKUP_ICESELECT, ElementUiConfig.createField("xpath-lookup", propMap.get("CaseID.InvalidCaseException"), SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));

		
	}
	
	
	private static void mapFieldForPatientTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.AGE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.AGE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.AGE_GROUP, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.AGE_GROUP), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.AGE_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.AGE_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.HEIGHT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.HEIGHT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.HEIGHT_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.HEIGHT_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.NUMBER_OF_CIGARETTES_PER_DAY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.NUMBER_OF_CIGARETTES_PER_DAY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.NUMBER_OF_DRINKS_PER_DAY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.NUMBER_OF_DRINKS_PER_DAY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.NUMBER_OF_YEARS_SMOKED, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.NUMBER_OF_YEARS_SMOKED), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PATIENT_ID, ElementUiConfig.createVaryXpathField("xpath-name", new String[] {propMap.get(SceptreConstants.PatientTabKeys.PATIENT_ID) , propMap.get(SceptreConstants.PatientTabKeys.PATIENT_ID_LABLE)}, SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PATIENT_SEX, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.PATIENT_SEX), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PERMISSION_TO_CONTACT_PHYSICIAN, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.PatientTabKeys.PERMISSION_TO_CONTACT_PHYSICIAN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PHYSICAL_ACTIVITY, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.PHYSICAL_ACTIVITY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PHYSICIAN_AWARE_OF_ADVERSE_EVENT, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.PatientTabKeys.PHYSICIAN_AWARE_OF_ADVERSE_EVENT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.POSTAL_CODE, ElementUiConfig.createVaryXpathField("xpath-name",  new String[] {propMap.get(SceptreConstants.PatientTabKeys.POSTAL_CODE) , propMap.get(SceptreConstants.PatientTabKeys.POSTAL_CODE_LABLE)}, SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.TYPE_OF_ALCOHOL, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.TYPE_OF_ALCOHOL), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.WAS_AUTOPSY_DONE, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.PatientTabKeys.WAS_AUTOPSY_DONE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.WEIGHT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.WEIGHT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.WEIGHT_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.WEIGHT_UNIT), SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_DEATH,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_DEATH_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_DEATH_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_DEATH_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}

	
	private static void mapFieldForReporterTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.ADDITIONAL_LETTER_TEXT , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.ReporterTabKeys.ADDITIONAL_LETTER_TEXT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.ADDRESS1 , ElementUiConfig.createVaryXpathField("xpath-name",new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.ADDRESS1) , propMap.get(SceptreConstants.ReporterTabKeys.ADDRESS1_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.ADDRESS2, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.ADDRESS2) , propMap.get(SceptreConstants.ReporterTabKeys.ADDRESS2_LABEL)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.ADDRESS3, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.ADDRESS3) , propMap.get(SceptreConstants.ReporterTabKeys.ADDRESS2_LABEL)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.ADMINISTERED_VACCINE, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReporterTabKeys.ADMINISTERED_VACCINE),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.ANONYMOU_REFUSED, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReporterTabKeys.ANONYMOU_REFUSED),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.CITY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.CITY),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.COMPANY_FAMILY_NAME, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.COMPANY_FAMILY_NAME),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.COMPANY_GIVEN_NAME, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.COMPANY_GIVEN_NAME),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.COMPANY_QUALIFICATION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.COMPANY_QUALIFICATION),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.COMPANY_REPORTER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.COMPANY_REPORTER),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.COMPANY_TITLE, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReporterTabKeys.COMPANY_TITLE),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.CONTACT_METHOD, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReporterTabKeys.CONTACT_METHOD),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.COUNTRY, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReporterTabKeys.COUNTRY),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.DEGREE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.DEGREE),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.DEPARTMENT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.DEPARTMENT),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS) , propMap.get(SceptreConstants.ReporterTabKeys.E_MAIL_ADDRESS_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.FAMILY_NAME, ElementUiConfig.createVaryXpathField("xpath-name", new String[] { propMap.get(SceptreConstants.ReporterTabKeys.FAMILY_NAME) , propMap.get(SceptreConstants.ReporterTabKeys.FAMILY_NAME_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.FAX_CODE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.FAX_CODE),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.FAX_EXT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.FAX_EXT),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.FAX_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.FAX_NUMBER),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.GIVEN_NAME, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.GIVEN_NAME) , propMap.get(SceptreConstants.ReporterTabKeys.GIVEN_NAME_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.MIDDLE_NAME, ElementUiConfig.createVaryXpathField("xpath-name",new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.MIDDLE_NAME) , propMap.get(SceptreConstants.ReporterTabKeys.MIDDLE_NAME_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.ORGANIZATION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.ORGANIZATION),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.PHONE1_CODE, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.PHONE1_CODE) , propMap.get(SceptreConstants.ReporterTabKeys.PHONE1_CODE_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.PHONE1_EXT, ElementUiConfig.createVaryXpathField("xpath-name", new String[] { propMap.get(SceptreConstants.ReporterTabKeys.PHONE1_EXT) , propMap.get(SceptreConstants.ReporterTabKeys.PHONE1_EXT_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.PHONE1_NUMBER, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.PHONE1_NUMBER) , propMap.get(SceptreConstants.ReporterTabKeys.PHONE1_NUMBER_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.PHONE2_CODE, ElementUiConfig.createVaryXpathField("xpath-name", new String[] { propMap.get(SceptreConstants.ReporterTabKeys.PHONE2_CODE) , propMap.get(SceptreConstants.ReporterTabKeys.PHONE2_CODE_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.PHONE2_EXT, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{propMap.get(SceptreConstants.ReporterTabKeys.PHONE2_EXT) ,propMap.get(SceptreConstants.ReporterTabKeys.PHONE2_EXT_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.PHONE2_NUMBER, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.PHONE2_NUMBER) , propMap.get(SceptreConstants.ReporterTabKeys.PHONE2_NUMBER_LABLE)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.POSTAL_CODE, ElementUiConfig.createVaryXpathField("xpath-name", new String[]{ propMap.get(SceptreConstants.ReporterTabKeys.POSTAL_CODE) , propMap.get(SceptreConstants.ReporterTabKeys.POSTAL_CODE_LABEL)},SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.PROVINCE_STATE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReporterTabKeys.PROVINCE_STATE),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.QUALIFICATION, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReporterTabKeys.QUALIFICATION),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.SCHEDULE, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReporterTabKeys.SCHEDULE),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.SUFFICENT_DETAIL, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReporterTabKeys.SUFFICENT_DETAIL),SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.TITLE, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReporterTabKeys.TITLE),SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.ReporterTabKeys.RESPONSE_RECEIVED ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.ReporterTabKeys.RESPONSE_RECEIVED_DAY),
								propMap.get(SceptreConstants.ReporterTabKeys.RESPONSE_RECEIVED_MONTH),
								propMap.get(SceptreConstants.ReporterTabKeys.RESPONSE_RECEIVED_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}
	
	
	private static void mapFieldForDrugTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.MADEBY , ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.DrugTabKeys.MADEBY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.GENERICDRUGNAME, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.GENERICDRUGNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.CHARACTERIZATION, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.DrugTabKeys.CHARACTERIZATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.ACTIONTAKEN, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.ACTIONTAKEN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSAGE_FORM, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_FORM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.ROUTEOFADMIN, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.ROUTEOFADMIN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.ROUTEOFADMIN_DROPDOWN, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.ROUTEOFADMIN_DROPDOWN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.PARENT_DOSING, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.PARENT_DOSING), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DRUG_DURATION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.DRUG_DURATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.ADDITIONAL_INFO, ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.DrugTabKeys.ADDITIONAL_INFO), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.SAMPLE_AVAILABLE, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.DrugTabKeys.SAMPLE_AVAILABLE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSAGE_RANK, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_RANK), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSAGE_STATUS, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_STATUS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSE, ElementUiConfig.createField("xpath-Addinfo", propMap.get(SceptreConstants.DrugTabKeys.DOSE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSE_UNIT, ElementUiConfig.createField("xpath-Addinfo", propMap.get(SceptreConstants.DrugTabKeys.DOSE_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.NUM_DOSAGES, ElementUiConfig.createField("xpath-Addinfo", propMap.get(SceptreConstants.DrugTabKeys.NUM_DOSAGES), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.INTERVAL, ElementUiConfig.createField("xpath-Addinfo", propMap.get(SceptreConstants.DrugTabKeys.INTERVAL), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.INTERVAL_FREQ, ElementUiConfig.createField("xpath-Addinfo", propMap.get(SceptreConstants.DrugTabKeys.INTERVAL_FREQ), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSAGE_TEXT, ElementUiConfig.createField("xpath-Addinfo", propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_TEXT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.FIRST_REACTION, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.FIRST_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.INDICATION_RANK, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.INDICATION_RANK), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.REPORTED_INDICATION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.REPORTED_INDICATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.LOW_LEVEL_TERM, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.LOW_LEVEL_TERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.PREFFERED_TERM, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.PREFFERED_TERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.CODING_STATUS, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.CODING_STATUS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.BATCH_RANK, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.BATCH_RANK), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.BATCHLOT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.BATCHLOT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.PRODUCT_QUALITY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.PRODUCT_QUALITY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.REPORTED_DEFECT, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DEFECT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.SENT_TO_QAYN, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.SENT_TO_QA), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.INVALID_LOT, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.INVALID_LOT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.INVESTIGATION, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.INVESTIGATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.INVESTIGATION_CONSLUSION, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.INVESTIGATION_CONSLUSION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DESCRIPTION, ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.DrugTabKeys.DESCRIPTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.START_OF_ADMINISTRATION_TO_START_OF_FIRST_REACTION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.LAST_DOSE_OF_DRUG_TO_START_OF_FIRST_REACTION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN_DROPDOWN, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN_DROPDOWN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.WHERE_DRUG_PURCHASE, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.WHERE_DRUG_PURCHASE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.PURCHASE_LOCATION, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.PURCHASE_LOCATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.PHONE_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.PHONE_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.ADDRESS, ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.DrugTabKeys.ADDRESS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.MANUFACTURER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.MANUFACTURER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DEPARTMENT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.DEPARTMENT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.NON_COMAPNY_ADDRESS, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.NON_COMAPNY_ADDRESS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.CITY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.CITY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.STATE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.STATE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.POSTAL_CODE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.POSTAL_CODE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.COUNTRY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.COUNTRY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.COUNTRYDROPDOWN, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.COUNTRYDROPDOWN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.VACCINE_COUNTRY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugTabKeys.VACCINE_COUNTRY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.VACCINE_AT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.VACCINE_AT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.VACINE_PURCHASE_WITH, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugTabKeys.VACINE_PURCHASE_WITH), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_NO, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_NO), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_DOCTOR, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_DOCTOR), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_MANUFACTURER, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_MANUFACTURER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_HEALTH_DEPARTMENT, ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_HEALTH_DEPARTMENT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.RELATEDNESS, ElementUiConfig.createField("xpath-subtab", propMap.get(SceptreConstants.DrugTabKeys.RELATEDNESS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_LABELING, ElementUiConfig.createField("xpath-subtab", propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_LABELING), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.CORE_LABELING, ElementUiConfig.createField("xpath-subtab", propMap.get(SceptreConstants.DrugTabKeys.CORE_LABELING), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO , ElementUiConfig.createField("xpath-lookup", propMap.get(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO), SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.START_DRUG ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugTabKeys.START_DRUGDAY),
								propMap.get(SceptreConstants.DrugTabKeys.START_DRUGMONTH),
								propMap.get(SceptreConstants.DrugTabKeys.START_DRUGYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.LAST_ADMIN ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugTabKeys.LAST_ADMINDAY),
								propMap.get(SceptreConstants.DrugTabKeys.LAST_ADMINMONTH),
								propMap.get(SceptreConstants.DrugTabKeys.LAST_ADMINYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSAGE_START ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_STARTDAY),
								propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_STARTMONTH),
								propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_STARTYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.DOSAGE_END,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_ENDDAY),
								propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_ENDMONTH),
								propMap.get(SceptreConstants.DrugTabKeys.DOSAGE_ENDYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.SENT_TO_QA,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugTabKeys.SENT_TO_QADAY),
								propMap.get(SceptreConstants.DrugTabKeys.SENT_TO_QAMONTH),
								propMap.get(SceptreConstants.DrugTabKeys.SENT_TO_QAYEAR) },
						SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.DrugTabKeys.EXPIRATION,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugTabKeys.EXPIRATION_MONTH),propMap.get(SceptreConstants.DrugTabKeys.EXPIRATION_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}
	
	
	private static void mapFieldForMedicalHistoryTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.CONTINUING , ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.MedicalHistoryTabKeys.CONTINUING), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.LOWLEVELTERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.LOWLEVELTERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.PREFFEREDTERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.PREFFEREDTERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE), SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.STARTDATE ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.MedicalHistoryTabKeys.STARTDATEDAY),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.STARTDATEMONTH),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.STARTDATEYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.ENDDATE ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.MedicalHistoryTabKeys.ENDDATEDAY),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.ENDDATEMONTH),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.ENDDATEYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}
	
	
	private static void mapFieldForDrugHistoryTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS2 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS2), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS3 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS3), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.DRUGABUSE , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugHistoryTabKeys.DRUGABUSE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.DRUGNAME , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.DRUGNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM1 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM1), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM2 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM2), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM1 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM1), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM2 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM2), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.ROUTEOFADMIN , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugHistoryTabKeys.ROUTEOFADMIN), SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.START,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugHistoryTabKeys.STARTDAY),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.STARTMONTH),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.STARTYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.END,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugHistoryTabKeys.ENDDAY),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.ENDMONTH),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.ENDYEAR) },
						SceptreConstants.NAME_IDENTIFIER));

	}
	
	
	private static void mapFieldForReactionTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.AUTOSPY_CAUSE_OF_DEATH , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReactionTabKeys.AUTOSPY_CAUSE_OF_DEATH), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.CODDING_STATUS , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.CODDING_STATUS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.LOWLEVEL_TERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.LOWLEVEL_TERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.OUTCOME , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.OUTCOME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.PREFFERED_TERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.PREFFERED_TERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.RATIONAL_FOR_NONIME , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.ReactionTabKeys.RATIONAL_FOR_NONIME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_DURATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_END_HOUR , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_HOUR), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_END_MINUTE , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_MINUTE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED_HOUR , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_HOUR), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MINUTE , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MINUTE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REASON_FOR_NON_IME , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.REASON_FOR_NON_IME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REPORTED_CAUSE_OF_DEATH , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReactionTabKeys.REPORTED_CAUSE_OF_DEATH), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REPORTED_REACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REPORTED_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.SIREOUS_RADIO , ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.ReactionTabKeys.SIREOUS_RADIO), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.TERM_HIGHLIGHTEDBY_REPORTER , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReactionTabKeys.TERM_HIGHLIGHTEDBY_REPORTER), SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_DAY),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MONTH),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_END,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_DAY),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_MONTH),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}
	
	
	private static void mapFieldForDiagnosticTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.INVESTIGATIONPATIENT , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.DiagnosticTabKeys.INVESTIGATIONPATIENT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.LOWERLEVELTERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DiagnosticTabKeys.LOWERLEVELTERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.MOREINFOAVAILABLE , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DiagnosticTabKeys.MOREINFOAVAILABLE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.NORMALRANGEHIGH , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DiagnosticTabKeys.NORMALRANGEHIGH), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.NORMALRANGELOW , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DiagnosticTabKeys.NORMALRANGELOW), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.OUTOFRANGE , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DiagnosticTabKeys.OUTOFRANGE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.PREFFEREDTERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DiagnosticTabKeys.PREFFEREDTERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.REPORTEDTEST , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DiagnosticTabKeys.REPORTEDTEST), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.RESULT , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DiagnosticTabKeys.RESULT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.UNIT , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DiagnosticTabKeys.UNIT), SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.DiagnosticTabKeys.TEST,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DiagnosticTabKeys.TESTDAY),
								propMap.get(SceptreConstants.DiagnosticTabKeys.TESTMONTH),
								propMap.get(SceptreConstants.DiagnosticTabKeys.TESTYEAR) },
						SceptreConstants.NAME_IDENTIFIER));

	}
	
	
	private static void mapFieldForNarrativeTab()
	{
		/*s_mapFieldConfiguration.put(SceptreConstants.NarrativeTabKeys.APPLY_TO , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.NarrativeTabKeys.APPLY_TO), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.NarrativeTabKeys.BRM_NARRATIVE , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.NarrativeTabKeys.BRM_NARRATIVE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.NarrativeTabKeys.INITIAL_COMPANY_NARRATIVE_ENGLISH , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.NarrativeTabKeys.INITIAL_COMPANY_NARRATIVE_ENGLISH), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.NarrativeTabKeys.INITIAL_COMPANY_NARRATIVE_LOCALLANGUAGE , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.NarrativeTabKeys.INITIAL_COMPANY_NARRATIVE_LOCALLANGUAGE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.NarrativeTabKeys.TEMPLATESELECT , ElementUiConfig.createField("xpath-button", propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATESELECT), SceptreConstants.NAME_IDENTIFIER));
		*/	
	}
	
	
	public void login(String p_sUserid, String p_sPassword)
	{
		String sUsername = null, sPassword = null, sSubmit = null;

		sUsername = propMap.get("Login.Username");
		sPassword = propMap.get("Login.Password");
		sSubmit = propMap.get("Login.Submit");

		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER, sUsername, p_sUserid);

		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER, sPassword, p_sPassword);

		clickElementByAbsolutePath(sSubmit);

		log.info("Login successful");
	}
	
	public boolean login(String p_sUserid)
	{
		String sUsername = null;
		int waitCount = 0;
		boolean passwordInputFlag = false;

		sUsername = propMap.get("Login.Username");
		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER, sUsername, p_sUserid);
		
		while(!passwordInputFlag && waitCount < 100)
		{
			wait(propMap.get(SceptreConstants.MINWAIT), propMap);
			
			if(isElementVisibleById(propMap.get(SceptreConstants.HomeScreen.MENU_PANEL), SceptreConstants.TABLE_TAG))
			{
				passwordInputFlag = true;
			}
			waitCount++;
		}
		log.info("Login successful");
		return passwordInputFlag;
	}

	
	public SearchResults search(Map<SceptreConstants.SceptreSearchFields, Object> oCriteria)
	{
		log.info("search process begin");
		
		Set<Entry<SceptreSearchFields, Object>> iterKeys=null;
		SceptreSearchFields oSearchField = null;
		SearchResults oResults = null;
		iterKeys = oCriteria.entrySet();
		ElementUiConfig elementUiConfig = null;
		String sValue = null , sElementXpath = null , sElementAttribute = null , sSearchButton = null;
		Map<String , String> splitDateMap = null;
		
		sSearchButton = propMap.get(SearchFieldXpath.SEARCHBUTTON);

		for(Entry<SceptreSearchFields,Object> entry : iterKeys)
		{
			oSearchField = entry.getKey();
			
			sValue=entry.getValue().toString();
			
			elementUiConfig = s_mapSearchConfiguration.get(oSearchField);
			
			sElementXpath = elementUiConfig.getAccessDetails()[0];
			
			sElementAttribute=elementUiConfig.getAccessAtribute();
			
			if (elementUiConfig.m_nFieldType == ElementUiConfig.TYPE.DATE_TRIPLET_MAPPED_FIELD)
			{
				
				/*
				 * If Date fields comes from search screen
				 */
				
				splitDateMap = new HashMap<>();
				
				splitDateMap.put(oSearchField.internalName(), sValue);
				
				splitDateByRegex(splitDateMap, oSearchField.internalName());
				
				findElementByXpathAndSetContent(sElementAttribute, sElementXpath + Day, splitDateMap.get(oSearchField.internalName() + Day));
				
//				selectDropdownValueByValue(sElementXpath + Month, splitDateMap.get(oSearchField.internalName() + Month));
				
				selectDropdownValueByIndex(sElementXpath + Month, Integer.parseInt(splitDateMap.get(oSearchField.internalName() + Month)));
				
				findElementByXpathAndSetContent(sElementAttribute, sElementXpath + Year, splitDateMap.get(oSearchField.internalName() + Year));
			}
			else 
			{
				if (elementUiConfig.getAccessMethod().equals("xpath-dropdown"))
				{
					
					/*
					 * if dropdown comes from search screen
					 */
					
					selectDropdownValueByValue(	sElementXpath , sValue);
				}
				else if(elementUiConfig.getAccessMethod().equals("xpath-value"))
				{
					if(sValue.equalsIgnoreCase("Yes"))
						selectElementWithSpaceKey(elementUiConfig.getAccessDetails()[0]);
					else if(sValue.equalsIgnoreCase("No"))
						deSelectElementWithSpaceKey(elementUiConfig.getAccessDetails()[0]);
				}
				else if(oSearchField.equals(SceptreConstants.SceptreSearchFields.GENERICDRUG))
				{
					/*
					 * if look up fields comes from search screen
					 */
					lookUpGenericCode(sValue);
				}
				else
				{
					
					/*
					 * if common text field comes from search screen
					 */
					findElementByXpathAndSetContent(sElementAttribute, sElementXpath, sValue);
				}
			}
		}
		
		clickElementByAbsolutePath(sSearchButton);
		
		oResults = insertValuestoResult();
		
		log.info("search process finished");
		
		return oResults;
	}

	
	public String getLocalCaseId(String p_sAERNumber , String p_sVersionNumber, boolean p_bTakeOwnership)
	{
		log.info("getting local case id for AER number :" + p_sAERNumber + " And Case version :" + p_sVersionNumber);
		
		String sLocalCaseId = null;
		
		takeCaseOwnerShip(p_sAERNumber , p_sVersionNumber, p_bTakeOwnership);
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB)); 
		
		sLocalCaseId = getTextOfValueAttributeElementByXpath(SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID));
		
		log.info("Local case id for AER number :" + p_sAERNumber + " is " + sLocalCaseId);
		
		return sLocalCaseId;
	}

	
	private boolean takeCaseOwnerShip(String p_sAERNumber , String  p_sVersionNumber, boolean p_bTakeOwnership)
	{
		log.info("take owner ship for AER Number :" + p_sAERNumber);
		
		int nNumberOfSearchResultRows = 0;
		String sAERNumber = null , sVersionNumber = null;
		boolean bIsAlertSwitched = false;
		
		nNumberOfSearchResultRows = getTotalNumberOfCaseResult();
		
		for(int nRowCounter = 1 ; nRowCounter <= nNumberOfSearchResultRows ; nRowCounter++)
		{
			sAERNumber = getTextFromXpath(propMap.get(ResultFieldsXpath.AER_PREFIX) + nRowCounter + propMap.get(ResultFieldsXpath.AER_POSTFIX));
			
			sVersionNumber = getTextFromXpath(propMap.get(ResultFieldsXpath.VERSION_PREFIX) + nRowCounter + propMap.get(ResultFieldsXpath.VERSION_POSTFIX));
			
			log.info("fetched AER is:" + sAERNumber + " and Case Version is :" + sVersionNumber + " By record numeber :" +nRowCounter);

			log.info("requested AER is:" + p_sAERNumber + " and Case Version is :" + p_sVersionNumber + " By record numeber :" +nRowCounter);

			if(sAERNumber.equalsIgnoreCase(p_sAERNumber) && sVersionNumber.equalsIgnoreCase(p_sVersionNumber))
			{
				log.info("AER matches same ? :-" + sAERNumber.equalsIgnoreCase(p_sAERNumber));
			
				if(p_bTakeOwnership)
				{
					enterClickByAbsoluteXpath(propMap.get(ResultFieldsXpath.AERSEARCH_CASEOWNER_PREFIX) + nRowCounter + propMap.get(ResultFieldsXpath.AERSEARCH_CASEOWNER_POSTFIX));
				}
				else
				{
					enterClickByAbsoluteXpath(propMap.get(ResultFieldsXpath.AERSEARCH_AER_PREFIX) + nRowCounter + propMap.get(ResultFieldsXpath.AERSEARCH_AER_POSTFIX));
					break;
				}
				
				if(isAlertPresent())
				{
					log.info("Alert is present for take ownership");
					
					getAlert();
					
					bIsAlertSwitched = true;
					
					log.info("Alert accepted");
				}
			}
		}
		return bIsAlertSwitched;
	}
	
	
	
	private SearchResults insertValuestoResult()
	{
		SearchResults oResults = null;
		int nNumberOfSearchResultRows = 0 ;
		
		oResults = new SearchResults();
	
		nNumberOfSearchResultRows = getTotalNumberOfCaseResult();
		
		if (isThresholdToBeChecked) {
			isThresholdToBeChecked = false;
			if (nNumberOfSearchResultRows > thresholdSearchResultCount) {
				log.info("Threshold value for Sceptre Search Result" + thresholdSearchResultCount);
				thresholdSearchResultCount = -1;
				//Explicit Null to be handled. Please do not changege the below value
				return null;
			}
		}
		
		log.info("Found results for AER search is :" + nNumberOfSearchResultRows);
		
		if(nNumberOfSearchResultRows > 0)
		{
			for(int nRowCounter = 1 ; nRowCounter <= nNumberOfSearchResultRows ; nRowCounter++)
			{
				log.info("getting found results for row :-" + nRowCounter);
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.AER,
						getTextFromXpath(propMap.get(ResultFieldsXpath.AER_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.AER_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.VERSION,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.VERSION_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.VERSION_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.QUEUE,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.QUEUE_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.QUEUE_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.CASEOWNER,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.CASEOWNER_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.CASEOWNER_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.SUSPECTPRODUCT,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.SUSPECTPRODUCT_PREFIX)
								+ nRowCounter + propMap.get(ResultFieldsXpath.SUSPECTPRODUCT_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.REACTION,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.REACTION_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.REACTION_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.REACTIONDATE,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.REACTIONDATE_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.REACTIONDATE_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.SERIOUSNESS,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.SERIOUSNESS_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.SERIOUSNESS_POSTFIX)));
				
				oResults.insertValue(SceptreConstants.SceptreSearchResultColumns.REPORTER,
						getTextWithoutVisibilityByXpath(propMap.get(ResultFieldsXpath.REPORTER_PREFIX) + nRowCounter
								+ propMap.get(ResultFieldsXpath.REPORTER_POSTFIX)));
				
				log.info("getting found results for row :-" + nRowCounter + "is done");
			}
		}
		
		return oResults;
	}
	
	
	
	private void lookUpGenericCode(String p_sGenericName)
	{
		String l_sParentwindow = null;
		
		l_sParentwindow = getWindowHandle();

		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SearchFieldXpath.GENERICDRUGLOOKUP));
		
		getSetOfWindows(2);
		
		switchWindowHandler(l_sParentwindow);
		
		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER , propMap.get(SearchFieldXpath.GENERICDRUG) , p_sGenericName);
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SearchFieldXpath.GENERICDRUGSEARCH));
		
		clickElementByAbsolutePath(propMap.get(SearchFieldXpath.GENERICDRUGCLICK));
		
		switchWindow(l_sParentwindow);
		
	}
	
	
	
	public boolean isLinkedCasePresent(String caseID)
	{
		String sLinkedCaseNotPresentText = null , sLinkedCaseXpath = null;
		
		boolean bIsCaseLinked = false , bLinkedCaseNotPresent = false;
		
		sLinkedCaseXpath = propMap.get(SceptreConstants.CaseIdTabKeys.LINKED_CASES);
		
		clickLinkByText(caseID);
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB));
		
		bLinkedCaseNotPresent = isElementPresentInXpath(sLinkedCaseXpath);

		if(bLinkedCaseNotPresent)
		{
			sLinkedCaseNotPresentText = getTextFromXpath(sLinkedCaseXpath);

			if(sLinkedCaseNotPresentText.equalsIgnoreCase("There are no Linked Cases for this case."))
			{
				scrollPageToElement(sLinkedCaseXpath);
				
				highLightElement(sLinkedCaseXpath); 
				
				bIsCaseLinked = false;
			}
			
		}
		else
		{
			bIsCaseLinked = true;
		}
		
		
		return bIsCaseLinked;
	}
	
	
	
	public boolean isLinkedTypeDuplicate()
	{
		String sLinkTypeText = null;
		
		sLinkTypeText = getTextFromXpath(propMap.get("CaseID.LinkedCaseTypeDuplicate"));
		
		if(sLinkTypeText.equalsIgnoreCase("Duplicate"))
		{
			scrollPageToElement(propMap.get("CaseID.LinkedCaseTypeDuplicate"));

			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	
	public int getTotalNumberOfCaseResult()
	{
		int nRowCount = 0;
		boolean bResultTablePresent = false;
		
		bResultTablePresent = isElementPresentInXpath("//tbody[@id = '" + propMap.get("DupSearch.CaseResult") + "']/tr");
		
		if(bResultTablePresent)
		{
			nRowCount = getDescendantsTrSizeByTableID("tbody", "id", propMap.get("DupSearch.CaseResult"));
		}
		
		return nRowCount;
	}
	
	
	public List<CaseDocument> downloadSourceDocument(List<Criterion> listCriterion) throws SciException 
	{
		log.info("download source documents ");
		
		int nRowCount = 0;
		CaseDocument oCaseDocument = null;
		List<CaseDocument> listCaseDocuments = null;

		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get("Download.CaseDocuments"));

		nRowCount = getCaseDocumentsCount();
		
		log.info("Number of case document count is" + nRowCount);

		listCaseDocuments = new ArrayList<>();
		
		for(int i = 1; i <= nRowCount ; i++)
		{
			log.info("Process gathering case document details for row" + i);

			oCaseDocument = getCaseDocumentDetails(i);
			
			log.info("Criteria for download case document is matches or not : " + WebApplicationWriterUtils.evaluateCriteria(listCriterion, oCaseDocument.getoAttributeMap()));
			
			if(listCriterion.size() == 0 || WebApplicationWriterUtils.evaluateCriteria(listCriterion, oCaseDocument.getoAttributeMap()))
			{
				clickElementByAbsolutePath(propMap.get("Download.ClickOnSD.Prefix") + i + propMap.get("Download.ClickOnSD.Suffix"));

				setDowloadedFileInfo(oCaseDocument);
				
				listCaseDocuments.add(oCaseDocument);

				closeWindow();
			}
		}
		return listCaseDocuments;
	}
	
	public List<CaseDocument> getAllDocumentsDetails()
	{
		int nRowCount = 0;
		
		CaseDocument oCaseDocument = null;
		
		List<CaseDocument> listCaseDocuments = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get("Download.CaseDocuments"));
		
		nRowCount = getCaseDocumentsCount();
		
		log.info("Number of case document count is" + nRowCount);

		listCaseDocuments = new ArrayList<>();
		
		for(int i = 1; i <= nRowCount ; i++)
		{
			log.info("Process gathering case document details for row" + i);

			oCaseDocument = getCaseDocumentDetails(i);
			
			listCaseDocuments.add(oCaseDocument);
		}
		return listCaseDocuments;
	}
	
	private int getCaseDocumentsCount()
	{
		log.info("get case documents counts");
		 
		String sCaseDocumentTableId = null;
		int nRowCount = 0;
		 
		sCaseDocumentTableId = propMap.get("Download.CaseDocuments.Count");
		 
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.DOWNLOAD_SD_TBODY_PRESENT)))
		{
			nRowCount = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, sCaseDocumentTableId);
		}
	
		log.info("case documents counts is" + nRowCount);
	 
		return nRowCount;
	}
	
	
	
	private CaseDocument getCaseDocumentDetails(int p_nRowCount)
	{
		log.info("get case documents details");
		
		Map<String , Object> mAttributeMap = null;
		CaseDocument oCaseDocument = null;
		
		oCaseDocument = new CaseDocument();

		mAttributeMap = getCaseDocumentAttributeMap(p_nRowCount);
		
		oCaseDocument.setoAttributeMap(mAttributeMap);
		
		log.info("get case documents details done for row " + p_nRowCount);
		
		return oCaseDocument;
	}
	
	
	
	private CaseDocument setDowloadedFileInfo(CaseDocument p_oCaseDocument)
	{
		log.info("Set downloaded file info");
		
		File oFile = null , oDownloadedFile = null;
		
		oFile = new File(s_sDownloadFolder);
		
		oDownloadedFile = getDownloadedPdfFilePath(oFile);

		log.info("Downloaded file path is{}"+oDownloadedFile.getAbsolutePath());
		
		p_oCaseDocument.setPath(oDownloadedFile);
		
		log.info("Set downloaded file is" + oDownloadedFile.getAbsolutePath());
		
		return p_oCaseDocument;
	}
	

	
	private Map<String , Object> getCaseDocumentAttributeMap(int p_nRowCount)
	{
		log.info("get Case Document Attribute Map for row " + p_nRowCount);
		
		Map<String , Object> mAttributeMap = null;
		
		mAttributeMap = new HashMap<>();
		
		mAttributeMap.put(SceptreConstants.DownloadSD.DOCUMENT_NAME,
				getTextFromXpath(propMap.get(SceptreConstants.DownloadSD.GETTEXT_PREFIX) + p_nRowCount
						+ propMap.get(SceptreConstants.DownloadSD.GETTEXT_MIDDLE) + 1 + propMap.get(SceptreConstants.DownloadSD.GETTEXT_SUFFIX)));
		
		mAttributeMap.put(SceptreConstants.DownloadSD.DOCUMENT_TYPE,
				getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.DownloadSD.GETTEXT_PREFIX) + p_nRowCount
						+ propMap.get(SceptreConstants.DownloadSD.GETTEXT_MIDDLE) + 2 + propMap.get(SceptreConstants.DownloadSD.GETTEXT_SUFFIX)));
		
		mAttributeMap.put(SceptreConstants.DownloadSD.DATE_OF_RECEIPT,
				getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.DownloadSD.GETTEXT_PREFIX) + p_nRowCount
						+ propMap.get(SceptreConstants.DownloadSD.GETTEXT_MIDDLE) + 3 + propMap.get(SceptreConstants.DownloadSD.GETTEXT_SUFFIX)));
		
		mAttributeMap.put(SceptreConstants.DownloadSD.CASE_VERSION,
				getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.DownloadSD.GETTEXT_PREFIX) + p_nRowCount
						+ propMap.get(SceptreConstants.DownloadSD.GETTEXT_MIDDLE) + 4 + propMap.get(SceptreConstants.DownloadSD.GETTEXT_SUFFIX)));
		
		mAttributeMap.put(SceptreConstants.DownloadSD.DATE_ATTACHED,
				getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.DownloadSD.GETTEXT_PREFIX) + p_nRowCount
						+ propMap.get(SceptreConstants.DownloadSD.GETTEXT_MIDDLE) + 5 + propMap.get(SceptreConstants.DownloadSD.GETTEXT_SUFFIX)));
		
		mAttributeMap.put(SceptreConstants.DownloadSD.DATE_SENT_TO_CCV, getTextWithoutVisibilityByXpath(
				propMap.get(SceptreConstants.DownloadSD.GETTEXTCVV_PREFIX) + 7 + propMap.get(SceptreConstants.DownloadSD.GETTEXTCVV_SUFFIX)));
		
		mAttributeMap.put(SceptreConstants.DownloadSD.SEND_TO_CCV, getTextWithoutVisibilityByXpath(
				propMap.get(SceptreConstants.DownloadSD.GETTEXTCVV_PREFIX) + 8 + propMap.get(SceptreConstants.DownloadSD.GETTEXTCVV_SUFFIX)));
	
		log.info("Done , Case Document Attribute Map for row " + p_nRowCount);
		
		return mAttributeMap;
	}


	private static void listFieldTabwiseForAddNewReactionTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

////		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REPORTED_REACTION);
////		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LOWLEVEL_TERM);
////		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.CODDING_STATUS);
////		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.PREFFERED_TERM);
////		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.VERSION_WHEN_CODED);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.SIREOUS_RADIO);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REASON_FOR_NON_IME);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.RATIONAL_FOR_NONIME);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.OUTCOME);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.TERM_HIGHLIGHTEDBY_REPORTER);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REPORTED_CAUSE_OF_DEATH);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.AUTOSPY_CAUSE_OF_DEATH);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED_HOUR);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MINUTE);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END_HOUR);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END_MINUTE);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION);
//		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT);
		
		s_mapTabwiseFields.put("ADDNEWREACTION", listTabwiseFields);
	}

	
	/*

	private void invalidCaseExcemption(String[] p_sSelectValue)
	{
		boolean ifFound = false;
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION_LOOKUP_BUTTON));
		
		for(int i=0 ; i < p_sSelectValue.length ; i++ )
		{
			ifFound = isDropDownContainValue(propMap.get(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION_AVAILABLE) , p_sSelectValue[i]);
			
			if(ifFound)
			{
				selectDropdownValueByValue(propMap.get(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION_AVAILABLE), p_sSelectValue[i]);
				
				clickElementByAbsolutePath(propMap.get(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION_ADD));
			}
		}
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.INVALID_CASE_EXEMPTION_SAVE));
	}
	
	private void splitExpirationDate(Map<String , String> p_mSceptreMap , String p_sKey)
	{
		String l_sMonth = null , l_sYear = null , l_sKeyValue = null;
		
		
		l_sKeyValue = p_mSceptreMap.get(p_sKey);
		
		
		l_sYear = l_sKeyValue.split("-")[1];
		l_sMonth = l_sKeyValue.split("-")[0];
		
		
		p_mSceptreMap.put(p_sKey + "Month", m_mMonthMap.get(l_sMonth));
		p_mSceptreMap.put(p_sKey + "Year", l_sYear);                 
		
	}
	
	private void clickOnCaseDocument(int p_sRowCount)
	{
		String sSDXpath = null, sCaseDoumentName = null;
	
		for(int i = 1; i <= p_sRowCount ; i++)
		{
			sSDXpath  = propMap.get("Download.ClickOnSD.Prefix") + i + propMap.get("Download.ClickOnSD.Suffix");

			sCaseDoumentName = getTextFromXpath(sSDXpath);
			
			if(sCaseDoumentName.replaceAll("[-_]", "").toLowerCase().contains(SOURCE_DOCUMENT_NAME))
			{
				clickElementByAbsolutePath(sSDXpath);
				break;
			}
			
		}
	} 
	 
	public File downloadSourceDocument(List<Criterion> listCriteria) throws IOException 
	{
		
		List<com.sciformix.sciportal.rulesengine.Criterion> listCriteria = null;
		listCriteria = new ArrayList<Criterion>();
		listCriteria.add(Criterion.create(Operator.ENDS_WITH, "Document Name", "E2B_PDF_Report"));
		listCriteria.add(Criterion.create(Operator.MATCHES, "Document Type", "IRT Supporting"));
		
		//sceptrePdfFile =  oAppWriter.downloadSourceDocument(listCriteria); 

		listCriteria.
		
		File oFile = null, oDownloadedFile = null;
		String sClickOnCaseDocument = null, sCaseIDTab = null;
		int nRowCount = 0;
		
		sClickOnCaseDocument = propMap.get("Download.CaseDocuments");
		sCaseIDTab = propMap.get("Tab.CaseId");

		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, sCaseIDTab); 

		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, sClickOnCaseDocument);

		nRowCount = getCaseDocumentsCount();
		
		clickCaseDocument(nRowCount);

		log.error("SD attachment clicked");
		
		oFile = new File(s_sDownloadFolder);
		
		oDownloadedFile = getDownloadedPdfFilePath(oFile);

		log.error("Downloaded file path is{}"+oDownloadedFile.getAbsolutePath());
		
		return oDownloadedFile;
	
	}

	
	private int getCaseDocumentsCount()
	{
		String sCaseDocumentTableId = null;
		int nRowCount = 0;
		
		sCaseDocumentTableId = propMap.get("Download.CaseDocuments.Count");
		
		nRowCount = getDescendantsTrSizeByTableID("tbody", "id", sCaseDocumentTableId);
		
		return nRowCount;
	}
	
	
	private void clickCaseDocument(int p_sRowCount)
	{
		String sSDXpath = null;
		
		for(int i = 1; i <= p_sRowCount ; i++)
		{
			sSDXpath  = propMap.get("Download.ClickOnSD.Prefix") + i + propMap.get("Download.ClickOnSD.Suffix");

			sCaseDoumentName = getTextFromXpath(sSDXpath);
			
			if(sCaseDoumentName.replaceAll("[-_]", "").toLowerCase().contains(SOURCE_DOCUMENT_NAME))
			{
				clickElementByAbsolutePath(sSDXpath);
				break;
			}
			
			sSDXpath  = propMap.get("Download.ClickOnSD.Prefix") + i + propMap.get("Download.ClickOnSD.Suffix");

			clickElementByAbsolutePath(sSDXpath);
			
		}
	}
	*/
	
	
	public SearchResults locateCase(String p_sCaseId)
	{
		log.info("locate case for case id " + p_sCaseId);
		
		SearchResults oResult = null;
		
		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.SearchFieldXpath.AER_NUMBER), p_sCaseId);

		clickElementByAbsolutePath(propMap.get(SceptreConstants.SearchFieldXpath.SEARCHBUTTON));

		oResult = insertValuestoResult();
		
		return oResult;
	}
	
	
	public void prepareForSearch() 
	{
		clickElementByAbsolutePath(propMap.get("HomeScreen.DupSearchLink"));

		log.info("clicked on search screen to open");
	}
	
	
	public void refreshForSearch()
	{
		clickElementByAbsolutePath(propMap.get("HomeScreen.DupSearchLinkForRefresh"));

		log.info("clicked on search screen to Refresh duplicate search");
	}
	

 	private void splitDateByRegex(Map<String , String> p_mSceptreMap , String p_sKey)
	{
		String l_sDay = null , l_sMonth = null , l_sYear = null , l_sKeyValue = null;
		
		
		l_sKeyValue = p_mSceptreMap.get(p_sKey);
		
		
		l_sDay = l_sKeyValue.split("-")[0];
		l_sMonth = l_sKeyValue.split("-")[1].toUpperCase();
		l_sYear = l_sKeyValue.split("-")[2];
	
		p_mSceptreMap.put(p_sKey + "Day", l_sDay);
		p_mSceptreMap.put(p_sKey + "Month", m_mMonthMap.get(l_sMonth));
		p_mSceptreMap.put(p_sKey + "Year", l_sYear);                 
		
	}
	
	
	protected static Map<String ,String> getMonthMap()
	{
		Map<String , String> m_MonthMap = null;
		
		m_MonthMap = new HashMap<>();
		
		m_MonthMap.put("JAN" , "1");
		m_MonthMap.put("FEB" , "2");
		m_MonthMap.put("MAR" , "3");
		m_MonthMap.put("APR" , "4");
		m_MonthMap.put("MAY" , "5");
		m_MonthMap.put("JUN" , "6");
		m_MonthMap.put("JUL" , "7");
		m_MonthMap.put("AUG" , "8");
		m_MonthMap.put("SEP" , "9");
		m_MonthMap.put("OCT" , "10");
		m_MonthMap.put("NOV" , "11");
		m_MonthMap.put("DEC" , "12");
		
		return m_MonthMap;
	}
	
	
	public List<String> feedCaseIdTab(Map<String, String> sceptreMap)
	{
				
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<>();

		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB));
		
		listFieldsToPopulate = getFieldsToBePopulated("CASEID");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.CASE_SAVE));
	
		//String sParentWindow = null;

		//sParentWindow = getWindowHandle();
		
		//clickElementByAbsolutePath(propMap.get(SceptreConstants.CaseIdTabKeys.COMPANYCODE_LOOKUP));
		
		//companyUnitLookUp(sParentWindow, "JNJCONMCH", "JNJCONMCH");
		
		addNewOtherIdentification(sceptreMap);
		
		return listFieldsActuallyPopulated;
	}

	
	private void companyUnitLookUp(String p_sParentWindow , String p_sCompanyUnit)
	{
		boolean bIsLookupResultFound = false;
		
		getSetOfWindows(2);

		switchWindowHandler(p_sParentWindow);

		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.COMPANYUNITCODE_LOOKUP), p_sCompanyUnit);//i.e p_sCompanyUnitCode=JNJCONMH
		
//		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER,  propMap.get(SceptreConstants.CaseIdTabKeys.ORGANIZATION_LOOKUP), p_sOrganizeCode);
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, "id", propMap.get(SceptreConstants.CaseIdTabKeys.COMPANYUNITCODE_LOOKUP_SEARCHBUTTON));
		
		bIsLookupResultFound = isElementPresentInXpath(propMap.get(SceptreConstants.CaseIdTabKeys.LOOKUP_RESULT_TABLE));

		if(!bIsLookupResultFound)
		{
			clickElementByAbsolutePath(propMap.get(SceptreConstants.CaseIdTabKeys.LOOKUP_CLICKONRESULT));
		}
		else
		{
			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.LOOKUP_CANCLE));
		}
		
		switchWindow(p_sParentWindow);
	}
	
	
	
	private void addNewOtherIdentification(Map<String , String> sceptreMap)
	{

		String sParentWindow = null;

		sParentWindow = getWindowHandle();
		
		if(!StringUtils.isNullOrEmpty(sceptreMap.get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_CASEIDNUMBER)))
		{
			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_OTHER_IDENTIFICATION_NUMBER_ADDNEW));

			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_LOOKUPBUTTON));
			
			companyUnitLookUp(sParentWindow, sceptreMap.get(SceptreConstants.Name.ICU));
			
			selectDropdownValueByValue(propMap.get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_SOURCE), sceptreMap.get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_SOURCE));
			
			findElementByXpathAndSetContent(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_CASEIDNUMBER);
			
			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_SAVE);
		}
	}
	
	
	public void caseCharacteristicsLookUp(String p_sDropDownname , String p_sCaseType)
	{
		boolean ifFound = false;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB));
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_LOOKUP));

		if(p_sCaseType.equalsIgnoreCase(SceptreConstants.CaseType.INITIAL))
		{
			ifFound = isDropDownContainValue(propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_AVAILABLE), p_sDropDownname);

			if(ifFound)
			{
				selectDropdownValueByValue(propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_AVAILABLE), p_sDropDownname);
				
				clickElementByAbsolutePath(propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_ADDCC));
			}
		}
		else
		{
			ifFound = isDropDownContainValue(propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_SELECTED), p_sDropDownname);

			if(ifFound)
			{
				selectDropdownValueByValue(propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_SELECTED), p_sDropDownname);
				
				clickElementByAbsolutePath(propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_REMOVECC));
			}

		}
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.CASE_CHARACTERISTICS_SAVECC));
		
	}
	
	
	public List<String> feedPatientTab(Map<String, String> sceptreMap)
	{
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.PatientTabKeys.PATIENT_TAB));
		
		listFieldsToPopulate = getFieldsToBePopulated("PATIENT");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
		 		
		enterClickByXpath( SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.PatientTabKeys.PATIENT_SAVE));
		
		//patientOtherHistorySubTab(sceptreMap);
		
		return listFieldsActuallyPopulated;
	}

	
	/*private void patientOtherHistorySubTab(Map<String, String> sceptreMap)
	{
		clickElementByAbsolutePath(propMap.get(SceptreConstants.PatientTabKeys.OTHER_HISTORY));
		
		findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.PatientTabKeys.OTHER_HISTORY_RELEVENT_TEXTAREA) ,sceptreMap.get(SceptreConstants.PatientTabKeys.OTHER_HISTORY_RELEVENT_TEXTAREA));
		
		enterClickByXpath( SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.PatientTabKeys.OTHER_HISTORY_SAVE));
	}

	*/
	

	public List<String> feedMedicalHistoryTab(Map<String, String> sceptreMap)
	{
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		enterClickByXpath(SceptreConstants.INPUT_TAG , SceptreConstants.NAME_IDENTIFIER , propMap.get(SceptreConstants.MedicalHistoryTabKeys.TAB));
		
		listFieldsToPopulate = getFieldsToBePopulated("MEDICALHISTORY");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
		
		clickElementByXpath( SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.MedicalHistoryTabKeys.SAVE));
		
		return listFieldsActuallyPopulated;
	}
	
	
	public List<String> addNewMedicalHistory(Map<String, String> sceptreMap)
	{
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();

		enterClickByXpath(SceptreConstants.INPUT_TAG , SceptreConstants.NAME_IDENTIFIER , propMap.get(SceptreConstants.MedicalHistoryTabKeys.MEDICAL_ADDNEW));
		
		listFieldsToPopulate = getFieldsToBePopulated("ADDNEWMEDICALHISTORY");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
		
		clickElementByXpath( SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.MedicalHistoryTabKeys.SAVE));
		
		return listFieldsActuallyPopulated;
	}
	
	
	public List<String> feedReactionTab(Map<String, String> sceptreMap) 
	{
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.ReactionTabKeys.TAB));
		
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		listFieldsToPopulate = getFieldsToBePopulated("REACTION");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.ReactionTabKeys.SAVE));
		
		return listFieldsActuallyPopulated;
	}
	
	
	public List<String> addNewReactionTab(Map<String, String> sceptreMap) 
	{
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.ReactionTabKeys.REACTION_ADDNEW));
		
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		listFieldsToPopulate = getFieldsToBePopulated("ADDNEWREACTION");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.ReactionTabKeys.SAVE));
		
		return listFieldsActuallyPopulated;
	}
	
	
	public List<String> feedDrugTab(Map<String, String> sceptreMap)
	{
		List<String> listFieldsToPopulate = null;
		
		List<String> listFieldsActuallyPopulated = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.TAB));

		listFieldsToPopulate = getFieldsToBePopulated("DRUG");
		
		listFieldsActuallyPopulated = feedDrugTabCommon(listFieldsToPopulate, sceptreMap);
		
		return listFieldsActuallyPopulated;
	}

	
	public List<String> addNewDrug(Map<String, String> sceptreMap)
	{
		List<String> listFieldsToPopulate = null;
		
		List<String> listFieldsActuallyPopulated = null;
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER , propMap.get(SceptreConstants.DrugTabKeys.ADD_NEWDRUG));
		
		listFieldsToPopulate = getFieldsToBePopulated("ADDNEWDRUG");
		
		clickElementWithSpaceKeyByValueAttribute(propMap.get(SceptreConstants.DrugTabKeys.MADEBY), "Y");
		
		reportedDrugLookUp(sceptreMap);
		
		listFieldsActuallyPopulated = feedDrugTabCommon(listFieldsToPopulate, sceptreMap);
		
		return listFieldsActuallyPopulated;
	}

	
	private boolean reportedDrugLookUp(Map<String, String> sceptreMap)
	{
		String sParentWindow = null , sForm = null , sCharactristics = null;
		int nResultRowSize = 0;
		boolean bResultFound = false;
		
		sParentWindow = getWindowHandle();

		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.REPORTEDDRUG_LOOKUP_BUTTON));

		getSetOfWindows(2);

		switchWindowHandler(sParentWindow);
		
		findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_GENERICNAME), sceptreMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_GENERICNAME));
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_SEARCH_BUTTON));
		
		nResultRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULT_TBODY_ID));
		
		log.info("Reported drug lookup result size is :" + nResultRowSize);
		
		if(nResultRowSize != 0)
		{
			for(int nCurrentRow = 1 ; nCurrentRow <= nResultRowSize ; nCurrentRow++)
			{
				sForm = getTextFromXpath(propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTFORM_PREFIX) + nCurrentRow + propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTFORM_POSTFIX));
				
				log.info("Form name for row " + nCurrentRow + "is " + sForm + " In sceptre");
				
				sCharactristics = getTextFromXpath(propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTCHARACTERISTICS_PREFIX) + nCurrentRow + propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTCHARACTERISTICS_POSTFIX));
				
				log.info("Characteristic name for row " + nCurrentRow + "is " + sCharactristics + " In sceptre");
				
				if (sForm.equals(sceptreMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTFORM)) && sCharactristics
						.equals(sceptreMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTCHARACTERISTICS))) 
				{
					log.info("Form value and characteristic matches for row " + nCurrentRow + " are " + sForm + "And " +sCharactristics);
					
					clickElementByAbsolutePath(propMap
							.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTGENERICNAME_PREFIX) + nCurrentRow
							+ propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_RESULTGENERICNAME_POSTFIX));
					
					bResultFound = true;
					break;
				}
			}
			
			if(!bResultFound)
			{
				log.info("No matches found :" + sceptreMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_GENERICNAME));
				
				clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_CLOSE));
				
			}
		}
		else
		{
			log.info("No result found for generic name :" + sceptreMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_GENERICNAME));
			
			clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.REPORTED_DRUG_LOOKUP_CLOSE));
			
			bResultFound = false;
		}
		
		switchWindow(sParentWindow);
		
		return bResultFound;
	}
	
	
	private List<String> feedDrugTabCommon(List<String> listFieldsToPopulate , Map<String, String> sceptreMap)
	{
		String sValueToPopulate = null;
		
		String sDosageText = null;
		
		ElementUiConfig oFieldElementUiConfig = null;
		
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		
		for (String sFieldToPopulate : listFieldsToPopulate)
		{
			if (!sceptreMap.containsKey(sFieldToPopulate))
			{
				continue;
			}
			sValueToPopulate = sceptreMap.get(sFieldToPopulate);
			
			oFieldElementUiConfig = s_mapFieldConfiguration.get(sFieldToPopulate);
			
			listFieldsActuallyPopulated.add(sFieldToPopulate);
			//System.out.println(sFieldToPopulate);
			
			if (oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.FIELD_DIRECLTY_MAPPED || oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.VARY_XPATH_MAPPED_FIELD)
			{
				if (oFieldElementUiConfig.getAccessMethod().equals("xpath-name"))
				{
					findElementByXpathAndSetContent(oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[0], sValueToPopulate);
				}
				else if (oFieldElementUiConfig.getAccessMethod().equals("xpath-select"))
				{
					selectDropdownValueByValue(oFieldElementUiConfig.getAccessDetails()[0], sValueToPopulate);
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-button"))
				{
					clickElementByXpath(SceptreConstants.INPUT_TAG, oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[0]);
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-rbutton"))
				{
					if(sFieldToPopulate.equalsIgnoreCase(SceptreConstants.DrugTabKeys.CHARACTERIZATION))
					{
						if(sValueToPopulate.equalsIgnoreCase(SceptreConstants.DrugTabKeys.CC_SUSPECT))
						{
							clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "Y");
						}
						else if(sValueToPopulate.equalsIgnoreCase(SceptreConstants.DrugTabKeys.CC_SUSPECT_DRUG_INTERACTING))
						{
							clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "N");
						}
						else
						{
							clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "U");
						}
					}
					else
					{
						if(sValueToPopulate.equalsIgnoreCase("Yes"))
						{
							clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "Y");
						}
						else if(sValueToPopulate.equalsIgnoreCase("No"))
						{
							clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "N");
						}
						else
						{
							clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "U");
						}
					}
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-textarea"))
				{
					findElementByAbsoluteXpathAndSetContent(oFieldElementUiConfig.getAccessDetails()[0] , sValueToPopulate);
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-value"))
				{
					if(sValueToPopulate.equalsIgnoreCase("Yes"))
						selectElementWithSpaceKey(oFieldElementUiConfig.getAccessDetails()[0]);
					else if(sValueToPopulate.equalsIgnoreCase("No"))
						deSelectElementWithSpaceKey(oFieldElementUiConfig.getAccessDetails()[0]);
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-Addinfo"))
				{
					if(!StringUtils.isNullOrEmpty(sValueToPopulate))
					{
						sDosageText	=	sValueToPopulate;
					}
				}
			}
			else if (oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.DATE_TRIPLET_MAPPED_FIELD)
			{
				splitDateByRegex(sceptreMap, sFieldToPopulate);
			
				if(sFieldToPopulate.equals(SceptreConstants.DrugTabKeys.EXPIRATION))
				{
					selectDropdownValueByIndex(oFieldElementUiConfig.getAccessDetails()[0], Integer.parseInt(sceptreMap.get(sFieldToPopulate + Month)));
					
					findElementByXpathAndSetContent(oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[1], sceptreMap.get(sFieldToPopulate + Year));
				}
				else
				{
					findElementByXpathAndSetContent(oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[0], sceptreMap.get(sFieldToPopulate + Day));
					
					selectDropdownValueByIndex(oFieldElementUiConfig.getAccessDetails()[1], Integer.parseInt(sceptreMap.get(sFieldToPopulate + Month)));
					
					findElementByXpathAndSetContent(oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[2], sceptreMap.get(sFieldToPopulate + Year));
				}
				
			}
			else
			{
				
			}
		
		}
		
		if(!StringUtils.isNullOrEmpty(sDosageText))
		{
			updateDoseInformation(sDosageText);
		}
		
//		sParentWindow = getWindowHandle();
		
//		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.MAH_LOOKUPBUTTON));
//		
//		companyUnitLookUp(sParentWindow, "JNJCONMCH", "JNJCONMCH");

		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.SAVE));
		
		return listFieldsActuallyPopulated;
		
	}

	public void performDrugSubTab()
	{
		drugRelatednessSubTab();
		
		drugCountrySpecificLabelingSubTab(); 
		
		drugCountryCoreLabelingSubTab();
	}
	
	private void updateDoseInformation(String p_sNotes)
	{
		String sParentWindow = null;
		
		sParentWindow = getWindowHandle();
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO));
		
		switchWindowHandler(sParentWindow);
		
		findElementByAbsoluteXpathAndSetContentOverride(propMap.get(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO_TEXT) , p_sNotes);

		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO_CONTINUE));
	
		switchWindow(sParentWindow);
	}
	
	
	private void drugRelatednessSubTab()
	{
		int nRelatednessRowSize = 0;
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.RELATEDNESS));
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.RELATEDNESS_TBODYID_PRESENT)))
		{
			nRelatednessRowSize = getDescendantsTrSizeByTableID("tbody", "id", propMap.get(SceptreConstants.DrugTabKeys.RELATEDNESS_TBODYID));
			
			for(int i = 0 ; i < nRelatednessRowSize ; i++)
			{
				selectDropdownValueByValue(propMap.get(SceptreConstants.DrugTabKeys.REPORTEDCASUALITY_PREFIX) + i + propMap.get(SceptreConstants.DrugTabKeys.REPORTEDCASUALITY_POSTFIX), "Possible");
				selectDropdownValueByValue(propMap.get(SceptreConstants.DrugTabKeys.COMPANYCASUALITY_PREFIX) + i + propMap.get(SceptreConstants.DrugTabKeys.COMPANYCASUALITY_POSTFIX), "Possible");
			}
			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.RELATENESS_SAVE));
		}
		
	} 
	
	
	private void drugCountrySpecificLabelingSubTab()
	{
		int nRelatednessRowSize = 0;

		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_LABELING));
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.COUNTRY_SPECIFIC_LABELING_TBODYID_PRESENT)))
		{
			nRelatednessRowSize = getDescendantsTrSizeByTableID("tbody", "id", propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_LABELING_TBODY_ID));
			
			for(int i = 0 ; i < nRelatednessRowSize ; i++)
			{
				clickElementWithSpaceKeyByValueAttribute(propMap.get(SceptreConstants.DrugTabKeys.MAH_LABELING_PREFIX) + i + propMap.get(SceptreConstants.DrugTabKeys.MAH_LABELING_POSTFIX), "N");
			}
			
			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.COUNTRYSPECIFICLABELING_SAVE));
		}
	} 
	
	
	private void drugCountryCoreLabelingSubTab()
	{

		int nRelatednessRowSize = 0;

		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.CORE_LABELING));
		
		nRelatednessRowSize = getDescendantsTrSizeByTableID("tbody", "id", propMap.get(SceptreConstants.DrugTabKeys.CORELABEL_TBODYID));
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.CORELABELING_TBODYID_PRESENT)))
		{
			for(int i = 0 ; i < nRelatednessRowSize ; i++)
			{
				clickElementWithSpaceKeyByValueAttribute(propMap.get(SceptreConstants.DrugTabKeys.CORELABELPREFIX) + i + propMap.get(SceptreConstants.DrugTabKeys.CORELABELPOSTFIX), "14901");
			}
			
			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.CORE_LABELING_SAVE));
		}
		
	}

	
	public void drugCountrySpecificApproval(String p_sProductCode)
	{
		int nProductCodeRowSize = 0;

		List<String> listOfApprovalNumbers = null;
		String sProductCode  = null , sParentWindow  = null;
		
		//clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.DRUG_COUNTRY_SPECIFIC_APPROVAL));
		
		sParentWindow = getWindowHandle();

		listOfApprovalNumbers = fetchApprovalNumbers();
		
		if(!listOfApprovalNumbers.contains(p_sProductCode))
		{
			clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_APPROVAL_ADDNEW));
			
			getSetOfWindows(2);
			
			switchWindowHandler(sParentWindow);
			
			nProductCodeRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_APPROVAL_TBODYID));
			
			for(int i = 0 ; i < nProductCodeRowSize ; i++)
			{
				sProductCode = getTextOfValueAttributeElementByXpath(SceptreConstants.NAME_IDENTIFIER,
						propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_APPROVAL_SELECT_PRODUCTCODE_PREFIX)
						+ i + propMap.get(
								SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_APPROVAL_SELECT_PRODUCTCODE_POSTFIX));
				
				if(sProductCode.equals(p_sProductCode))
				{
					clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap
							.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_APPROVAL_SELECT_PRODUCTCODE_PREFIX) + i
							+ propMap.get(
									SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_APPROVAL_SELECT_PRODUCTCODE_POSTFIX));
					break;
				}
			}
			switchWindow(sParentWindow);
		}
		
	}
	
	
	public boolean isErrorContainsKeyWord(String sKeyword)
	{
		int nErrorMessage = 0;
		boolean bIsKeyWordThere = false;
		String sErrorMessage = null;
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.DrugTabKeys.ERROR_MESSAGE_DETAILS)))
		{
			nErrorMessage = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.ERROR_MESSAGE_DETAILS_TBODYID));
		}
		
		for(int i = 1 ; i <= nErrorMessage ; i++)
		{
			sErrorMessage = getTextFromXpath(propMap.get(SceptreConstants.DrugTabKeys.ERROR_MESSAGE_DETAILS_PREFIX) + i
					+ propMap.get(SceptreConstants.DrugTabKeys.ERROR_MESSAGE_DETAILS_POSTFIX));
			
			if(sErrorMessage.toUpperCase().contains(sKeyword.toUpperCase()))
			{
				bIsKeyWordThere = true;
				break;
			}
		}
		
		return bIsKeyWordThere;
	}
	
	
	public void modifyIndication(String sIndicationValue)
	{
		findElementByXpathAndSetContent(SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.REPORTED_INDICATION), sIndicationValue);
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.SAVE));
	} 
		
	public void deletedrug(String p_sProductName)
	{
		int nProductCodeRowSize = 0;

		String sProductCode  = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.TAB));
		
		nProductCodeRowSize = getDescendantsTrSizeByTableID("tbody", "id", propMap.get(SceptreConstants.DrugTabKeys.DELETE_DRUG_TBODYID));
		
		log.info("Reported drug count is " + nProductCodeRowSize);
		
		for(int i = 0 ; i < nProductCodeRowSize ; i++)
		{
			sProductCode = getTextFromXpath(propMap.get(SceptreConstants.DrugTabKeys.DELETE_DRUG_PRODUCTNAME_PREFIX)
					+ i + propMap.get(SceptreConstants.DrugTabKeys.DELETE_DRUG_PRODUCTNAME_POSTFIX));
			
			if(sProductCode.equalsIgnoreCase(p_sProductName))
			{
				clickElementByAbsolutePath(
						propMap.get(SceptreConstants.DrugTabKeys.DELETE_DRUG_DELETESIGN_PREFIX) + i
								+ propMap.get(SceptreConstants.DrugTabKeys.DELETE_DRUG_DELETESIGN_POSTFIX));
				break;
			}
		}
		
		if(isAlertPresent())
		{
			log.info("Alert is present for delete drug");
			
			getAlert();
			
			log.info("Alert accepted");
		}
		
	}

	
	public List<String> feedReporterTab(Map<String, String> sceptreMap){
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.ReporterTabKeys.REPORTER_TAB));
		
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		listFieldsToPopulate = getFieldsToBePopulated("REPORTER");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
	
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.ReporterTabKeys.REPORTER_SAVE));
		
		return listFieldsActuallyPopulated;
	}

	
	public List<String> addNewReporterTab(Map<String, String> sceptreMap){	
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.ReporterTabKeys.REPORTER_ADDNEW));
		
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		listFieldsToPopulate = getFieldsToBePopulated("ADDNEWREPORTER");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
	
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.ReporterTabKeys.REPORTER_SAVE));
		
		return listFieldsActuallyPopulated;
	}
	
	
	public List<String> feedDrugHistory(Map<String, String> sceptreMap)
	{
		enterClickByXpath(SceptreConstants.INPUT_TAG , SceptreConstants.NAME_IDENTIFIER , propMap.get(SceptreConstants.DrugHistoryTabKeys.TAB));
		
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		listFieldsToPopulate = getFieldsToBePopulated("DRUGHISTORY");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
	
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugHistoryTabKeys.SAVE));
	
		return listFieldsActuallyPopulated;
	}

	
	public List<String> addNewDrugHistory(Map<String, String> sceptreMap)
	{
		enterClickByXpath(SceptreConstants.INPUT_TAG , SceptreConstants.NAME_IDENTIFIER , propMap.get(SceptreConstants.DrugHistoryTabKeys.DRUGHISTORY_ADDNEW));
		
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		listFieldsToPopulate = getFieldsToBePopulated("ADDNEWDRUGHISTORY");
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
	
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugHistoryTabKeys.SAVE));
		
		return listFieldsActuallyPopulated;
	}
	
	
	public List<String> feedDiagnostictab(Map<String, String> sceptreMap)
	{
		
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		listFieldsToPopulate = getFieldsToBePopulated("DIAGNOSTIC");
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DiagnosticTabKeys.DIAGNOSTIC_TAB));

		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
	
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DiagnosticTabKeys.DIAGNOSTIC_SAVE));
	
		return listFieldsActuallyPopulated;
	}

	
	public List<String> feedNarrativeTab(Map<String, String> sceptreMap)
	{
		List<String> listFieldsToPopulate = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		listFieldsToPopulate = getFieldsToBePopulated("NARRATIVE");
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.NARRATIVE_TAB));
		
		listFieldsActuallyPopulated = feedCommonTab(sceptreMap, listFieldsToPopulate);
		
		selectTemplateForNarrative("Narrative Spontaneous");
	
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.SAVE));
		
//		narrativeSUASubTab(sceptreMap);
		
//		oTherNarrativeSubTab(sceptreMap);
		
		return listFieldsActuallyPopulated;
	}
	
	
	private void narrativeSUASubTab(Map<String, String> sceptreMap)
	{
		clickElementByAbsolutePath(propMap.get(SceptreConstants.NarrativeTabKeys.SUA));
		
		selectTemplateForSUA("SUA Summary");
		
		findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.NarrativeTabKeys.SUA_SUMMARY_TEXTAREA) , sceptreMap.get(SceptreConstants.NarrativeTabKeys.SUA_SUMMARY_TEXTAREA));
		
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.SUA_SAVE));
	}

	
	private void oTherNarrativeSubTab(Map<String, String> sceptreMap)
	{
		clickElementByAbsolutePath(propMap.get(SceptreConstants.NarrativeTabKeys.OTHER_NARRATIVE));
		
		findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.NarrativeTabKeys.PSUR_COMMENTS_TEXTAREA) , sceptreMap.get(SceptreConstants.NarrativeTabKeys.PSUR_COMMENTS_TEXTAREA));
		
		findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.NarrativeTabKeys.REPORTERS_COMMENTS_TEXTAREA) , sceptreMap.get(SceptreConstants.NarrativeTabKeys.REPORTERS_COMMENTS_TEXTAREA));
		
		findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.NarrativeTabKeys.SENDERS_COMMENTS_TEXTAREA) , sceptreMap.get(SceptreConstants.NarrativeTabKeys.SENDERS_COMMENTS_TEXTAREA));
		
		findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.NarrativeTabKeys.MEDICAL_ASSESMENT_TEXTAREA) , sceptreMap.get(SceptreConstants.NarrativeTabKeys.MEDICAL_ASSESMENT_TEXTAREA));
	
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.OTHERNARRATIVE_SAVE));
	}
	
	
	private void selectTemplateForNarrative(String p_sTemplateName)
	{
		int nTemplateRowSize = 0;
		String sTemplateText = null , sParentwindow = null;
		
		sParentwindow = getWindowHandle();
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATESELECT));
		
		getSetOfWindows(2);
		
		switchWindowHandler(sParentwindow);
		
		nTemplateRowSize = getDescendantsTrSizeByTableID("tbody", "id", propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATE_TBODY_ID));
		
		for(int i=1 ; i <= nTemplateRowSize ; i++)
		{
			sTemplateText = getTextFromXpath(propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATETEXTPREFFIX) + i + propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATETEXTPOSTFIX));
			
			if(sTemplateText.equalsIgnoreCase(p_sTemplateName))
			{
				clickElementByAbsolutePath(propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATETEXTPREFFIX) + i + propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATETEXTPOSTFIX));
			}
		}
		
		switchWindow(sParentwindow);
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.TEMPLATE_APPLY));
	} 
	
	
	private void selectTemplateForSUA(String p_sTemplateName)
	{
		int nTemplateRowSize = 0;
		String sTemplateText = null , sParentwindow = null;
		
		sParentwindow = getWindowHandle();
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.SUA_TEMPLATE_SELECT_BUTTON));
		
		getSetOfWindows(2);
		
		switchWindowHandler(sParentwindow);
		
		nTemplateRowSize = getDescendantsTrSizeByTableID("tbody", "id", propMap.get(SceptreConstants.NarrativeTabKeys.SUA_TEMPLATELIST_TBODYID));
		
		for(int i=1 ; i <= nTemplateRowSize ; i++)
		{
			sTemplateText = getTextFromXpath(propMap.get(SceptreConstants.NarrativeTabKeys.SUA_TEMPLATENAME_SELECT_PREFIX) + i + propMap.get(SceptreConstants.NarrativeTabKeys.SUA_TEMPLATENAME_SELECT_POSTFIX));
			
			if(sTemplateText.equalsIgnoreCase(p_sTemplateName))
			{
				clickElementByAbsolutePath(propMap.get(SceptreConstants.NarrativeTabKeys.SUA_TEMPLATENAME_SELECT_PREFIX) + i + propMap.get(SceptreConstants.NarrativeTabKeys.SUA_TEMPLATENAME_SELECT_POSTFIX));
			}
		}
		
		switchWindow(sParentwindow);
		
		//clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.NarrativeTabKeys.SUA_TEMPLATE_APPLY));
	} 


	private List<String> feedCommonTab(Map<String, String> sceptreMap , List<String> listFieldsToPopulate)
	{
		String sValueToPopulate = null;
		ElementUiConfig oFieldElementUiConfig = null;
		List<String> listFieldsActuallyPopulated = null;
		
		listFieldsActuallyPopulated = new ArrayList<String>();
		
		for (String sFieldToPopulate : listFieldsToPopulate)
		{
			if (!sceptreMap.containsKey(sFieldToPopulate))
			{
				continue;
			}
			sValueToPopulate = sceptreMap.get(sFieldToPopulate);
			
			oFieldElementUiConfig = s_mapFieldConfiguration.get(sFieldToPopulate);
			
			listFieldsActuallyPopulated.add(sFieldToPopulate);
			
			log.info("Current key to feed "+sFieldToPopulate);
			
			if (oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.FIELD_DIRECLTY_MAPPED || oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.VARY_XPATH_MAPPED_FIELD)
			{
				if (oFieldElementUiConfig.getAccessMethod().equals("xpath-name"))
				{
					findElementByXpathAndSetContent(oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[0], sValueToPopulate);
				}
				else if (oFieldElementUiConfig.getAccessMethod().equals("xpath-select"))
				{
					selectDropdownValueByValue(oFieldElementUiConfig.getAccessDetails()[0], sValueToPopulate);
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-button"))
				{
					clickElementByXpath(SceptreConstants.INPUT_TAG, oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[0]);
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-rbutton"))
				{
					if(sValueToPopulate.equalsIgnoreCase("Yes"))
					{
						clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "Y");
					}
					else if(sValueToPopulate.equalsIgnoreCase("No"))
					{
						clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "N");
					}
					else
					{
						clickElementWithSpaceKeyByValueAttribute(oFieldElementUiConfig.getAccessDetails()[0], "U");
					}
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-textarea"))
				{
					findElementByAbsoluteXpathAndSetContent(oFieldElementUiConfig.getAccessDetails()[0] , sValueToPopulate);
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-value"))
				{
					if(sValueToPopulate.equalsIgnoreCase("Yes"))
						selectElementWithSpaceKey(oFieldElementUiConfig.getAccessDetails()[0]);
					else if(sValueToPopulate.equalsIgnoreCase("No"))
						deSelectElementWithSpaceKey(oFieldElementUiConfig.getAccessDetails()[0]);
				}
			}
			else if (oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.DATE_TRIPLET_MAPPED_FIELD)
			{
				if(!StringUtils.isNullOrEmpty(sValueToPopulate))
				{
					splitDateByRegex(sceptreMap, sFieldToPopulate);
					
					findElementByXpathAndSetContent(oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[0], sceptreMap.get(sFieldToPopulate + Day));
					
					selectDropdownValueByIndex(oFieldElementUiConfig.getAccessDetails()[1], Integer.parseInt(sceptreMap.get(sFieldToPopulate + Month)));
					
					findElementByXpathAndSetContent(oFieldElementUiConfig.getAccessAtribute(), oFieldElementUiConfig.getAccessDetails()[2], sceptreMap.get(sFieldToPopulate + Year));
				}
			}
			else
			{
				
			}
		
		}
		return listFieldsActuallyPopulated;
	}
	
	
	public Case retrieveCase(String p_sCaseId, FETCH_SCOPE p_eFetchScope) 
	{
		Case oCase = null;

		enterIntoCase(p_sCaseId);

		oCase = new Case(p_sCaseId);

		if (p_eFetchScope == FETCH_SCOPE.ALL) 
		{
			oCase.storeValue("", "");
		} else if (p_eFetchScope == FETCH_SCOPE.CRITICAL_FIELDS) 
		{

		}

		return oCase;
	}
	
	
	public void enterIntoCase(String p_sCaseId)
	{
		clickLinkByText(p_sCaseId);
	}

	
	public void logout()
	{
		clickElementByAbsolutePath(propMap.get(SceptreConstants.Logout.LOGOUT));
	}
	
	public void clearSearch()
	{
		clickElementByAbsolutePath(propMap.get(SceptreConstants.SearchFieldXpath.CLEAR));
	}

	
	public void gotoCaseIDTab()
	{
		enterClickByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB));
	}
	
	
	public void updateCaseNote(Map<String, String> sceptreMap)
	{
		clickElementByAbsolutePath(propMap.get(SceptreConstants.CASENOTE));
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CASENOTE_ADDNEW));
		
		if(sceptreMap.containsKey(SceptreConstants.CASENOTE_STANDARDNOTE))
		{
			selectDropdownValueByValue(propMap.get(SceptreConstants.CASENOTE_STANDARDNOTE), sceptreMap.get(SceptreConstants.CASENOTE_STANDARDNOTE));
		}
		if(sceptreMap.containsKey(SceptreConstants.CASENOTE_CUSTOMNOTE))
		{
			findElementByAbsoluteXpathAndSetContent(propMap.get(SceptreConstants.CASENOTE_CUSTOMNOTE), sceptreMap.get(SceptreConstants.CASENOTE_CUSTOMNOTE));
		}
		if(sceptreMap.containsKey(SceptreConstants.CASENOTE_NOTIFYUSER))
		{
			clickElementWithSpaceKeyByValueAttribute(propMap.get(SceptreConstants.CASENOTE_NOTIFYUSER), sceptreMap.get(SceptreConstants.CASENOTE_NOTIFYUSER));
		}
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.CASENOTE_SAVE));
		
	}
	

	public Map<String , Map<String , Object>> getFieldValues(Map<String , List<String>> p_MapOfListCriterion)
	{
		Set<Entry<String , List<String>>> iterKeys = null;
		
		Map<String , Map<String , Object>> mapOfFieldValuesTabWise = null;
		
		Map<String , Object> mapOfFieldsForCurrentTab = null;
		
		String sKey = null ;
		
		List<String> listCriteria = null;
		
		mapOfFieldValuesTabWise = new HashMap<>();
		
		mapOfFieldsForCurrentTab = new HashMap<>();
		
		if(p_MapOfListCriterion.size() == 0 )
		{
			return null;
		}
		else
		{
			iterKeys = p_MapOfListCriterion.entrySet();
			
			for(Entry<String , List<String>> entry : iterKeys)
			{
				sKey = entry.getKey();
				
				listCriteria=entry.getValue();
			
				mapOfFieldsForCurrentTab = getValuesOfListFields(sKey, listCriteria);
				
				mapOfFieldValuesTabWise.put(sKey, mapOfFieldsForCurrentTab);
			}
		}
		return mapOfFieldValuesTabWise;
	}

	
	public Map<String, Object> getValuesOfListFields(String p_sTabKey , List<String> p_ListValues)
	{
		Map<String, Object> p_mapSceptre = null;
		
		p_mapSceptre = new HashMap<>();
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER, propMap.get(p_sTabKey));
		
		readData(p_mapSceptre, p_ListValues);
		
		if(p_sTabKey.equals(SceptreConstants.DrugTabKeys.TAB))
		{
			for (String sFieldToFetch : p_ListValues)
			{
				if(sFieldToFetch.equals(SceptreConstants.DrugTabKeys.GET_APPROVAL_NUMBERS))
				{
					p_mapSceptre.put(SceptreConstants.DrugTabKeys.GET_APPROVAL_NUMBERS, fetchApprovalNumbers());
					
					break;
				}
			}//for sub tab
		}
		
		return p_mapSceptre;
	}
	
	
	public boolean isDuplicateSearchPage()
	{
		if(isElementPresentInXpath(propMap.get(SceptreConstants.SearchFieldXpath.VERIFY_DUPLICATE_SEARCH_PAGE)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	public void closeWindow()
	{
		String sParentWindow = null;
		
		sParentWindow = getWindowHandle();
		
		getSetOfWindows(2);
		
		switchWindowHandler(sParentWindow);
		
		closeWebDriver();
		
		switchWindow(sParentWindow);
	}
	
	//------------------------------------------------Reader Part -------------------------------------------------//
	
	
	private static List<String> getFieldsToBeFetch(String sTabName)
	{
		return s_mapTabwiseFields.get(sTabName);
	}
	

	private static void listFieldForReadingCaseIDTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.INITIATING_COMPANY_UNIT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.INITIATING_JNJ_UNIT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.CASE_PROCESSING_FUNCTION);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.CAUSED_PROLONGED_HOSPITALIZATION);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.DISABLING_INCAPACITATING);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.DOCUMENTS_HELD_BY_SENDER);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.CONGENITAL_ANOMALY_BIRTH_DEFECT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.ADDITIONAL_DOCUMENTS_AVAILABLE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.ADVERSEEVENT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.INVALIDCASE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LIFE_THREATENING);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LITEARTURE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.MEDICALLY_CONFIRMED);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.NATURE_OF_COMPLAINT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.OTHER_COMPLAINT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.OTHER_MEDICALLY_IMPORTANT_CONDITION);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.PARENTCHILD);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.PREGNANCY);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.PRODUCTCOMPLAINT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REASON_FOR_INVALID_CASE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REPORT_TYPE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.REPORTED_TO_REGULATOR_BY_SOURCE);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.RESULTS_IN_DEATH);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.SERIOUS);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LOOKUP_CCSELECT);
		listTabwiseFields.add(SceptreConstants.CaseIdTabKeys.LOOKUP_ICESELECT);
		 		
		s_mapTabwiseFields.put("CASEID", listTabwiseFields);
	}
	
	private static void mapFieldForReadingCaseIDTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.INITIATING_COMPANY_UNIT, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.CompanyUnit"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CASE_PROCESSING_FUNCTION, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.CaseProcessingFunction"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.INITIATING_JNJ_UNIT, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.InitialJ_JUnit"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REPORTED_TO_REGULATOR_BY_SOURCE, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.ReportedToRegulatorBySource"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LOCAL_CASE_ID, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.LocalCaseID"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.COUNTRY_OF_PRIMARY_SOURCE, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.CountryOfPrimarySource"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.COUNTRY_WHERE_AE_OCCURRED, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.CountryWhereAEOccoured"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CASE_PRIORITY, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.CasePriority"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REPORT_TYPE, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.ReportType"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.ADVERSEEVENT, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.AdverseEvent"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.PREGNANCY, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.Pregnancy"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LITEARTURE, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.Litearture"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.PARENTCHILD, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.ParentChild"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.PRODUCTCOMPLAINT, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.ProductComplaint"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.INVALIDCASE, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Classification.InvalidCase"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.NATURE_OF_COMPLAINT, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.NatureOfComplaint"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.OTHER_COMPLAINT, ElementUiConfig.createField("xpath-name", propMap.get("CaseID.OtherComplaint"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REASON_FOR_INVALID_CASE, ElementUiConfig.createField("xpath-select", propMap.get("CaseID.ReasonForInvalidCase"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.MEDICALLY_CONFIRMED, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.MedicallyConfirmed"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.REGULATORY_RELEVANT_UPDATE, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.RegulatoryReleventUpdate"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.ADDITIONAL_DOCUMENTS_AVAILABLE, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.AdditionDocumentsAvailable"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.DOCUMENTS_HELD_BY_SENDER, ElementUiConfig.createField("xpath-textarea", propMap.get("CaseID.DocumentHeldBySenderTextArea"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.SERIOUS, ElementUiConfig.createField("xpath-rbutton", propMap.get("CaseID.Seriousness"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LIFE_THREATENING, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.LifeThreatening"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CAUSED_PROLONGED_HOSPITALIZATION, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Hospitalization"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.DISABLING_INCAPACITATING, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Disabling"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.CONGENITAL_ANOMALY_BIRTH_DEFECT, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Congenital"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.RESULTS_IN_DEATH, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Death"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.OTHER_MEDICALLY_IMPORTANT_CONDITION, ElementUiConfig.createField("xpath-value", propMap.get("CaseID.Other"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LOOKUP_CCSELECT, ElementUiConfig.createField("xpath-lookup", propMap.get("CaseID.CaseCharactristicsSelect"), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LOOKUP_ICESELECT, ElementUiConfig.createField("xpath-lookup", propMap.get("CaseID.InvalidCaseException"), SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.VERSION_INITIALLY_RECEIVED_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.ICH_EXPEDITED_START_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.LATEST_INFORMATION_RECEIVED_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_CASE_PROCESSING_CENTER_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT_DAY),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT_MONTH),
								propMap.get(SceptreConstants.CaseIdTabKeys.RECEIVED_BY_LOCAL_SAFETY_UNIT_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));

		
	}
	
	private static void listFieldForReadingPatientTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();
		
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PATIENT_ID);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PATIENT_SEX);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.POSTAL_CODE);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.AGE);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.AGE_UNIT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.AGE_GROUP);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.HEIGHT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.HEIGHT_UNIT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.WEIGHT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.WEIGHT_UNIT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.CONVERTED_HEIGHT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.CONVERTED_WEIGHT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PHYSICIAN_AWARE_OF_ADVERSE_EVENT);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PERMISSION_TO_CONTACT_PHYSICIAN);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_DEATH);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.WAS_AUTOPSY_DONE);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.NUMBER_OF_CIGARETTES_PER_DAY);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.NUMBER_OF_YEARS_SMOKED);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.NUMBER_OF_DRINKS_PER_DAY);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.TYPE_OF_ALCOHOL);
		listTabwiseFields.add(SceptreConstants.PatientTabKeys.PHYSICAL_ACTIVITY);
		
		s_mapTabwiseFields.put("PATIENT", listTabwiseFields);
	}
	
	private static void mapFieldForReadingPatientTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.AGE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.AGE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.AGE_GROUP, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.AGE_GROUP), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.AGE_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.AGE_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.GENERAL_PRACTITIONER_MEDICAL_RECORD_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.HEIGHT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.HEIGHT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.HEIGHT_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.HEIGHT_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.CONVERTED_HEIGHT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.CONVERTED_HEIGHT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.CONVERTED_WEIGHT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.CONVERTED_WEIGHT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.HOSPITAL_RECORD_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.NUMBER_OF_CIGARETTES_PER_DAY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.NUMBER_OF_CIGARETTES_PER_DAY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.NUMBER_OF_DRINKS_PER_DAY, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.NUMBER_OF_DRINKS_PER_DAY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.NUMBER_OF_YEARS_SMOKED, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.NUMBER_OF_YEARS_SMOKED), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PATIENT_ID, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.PATIENT_ID), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PATIENT_SEX, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.PATIENT_SEX), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PERMISSION_TO_CONTACT_PHYSICIAN, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.PatientTabKeys.PERMISSION_TO_CONTACT_PHYSICIAN), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PHYSICAL_ACTIVITY, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.PHYSICAL_ACTIVITY), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.PHYSICIAN_AWARE_OF_ADVERSE_EVENT, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.PatientTabKeys.PHYSICIAN_AWARE_OF_ADVERSE_EVENT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.POSTAL_CODE, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.POSTAL_CODE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.TYPE_OF_ALCOHOL, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.TYPE_OF_ALCOHOL), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.SPECIALIST_RECORD_NUMBER), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.WAS_AUTOPSY_DONE, ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.PatientTabKeys.WAS_AUTOPSY_DONE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.WEIGHT, ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.PatientTabKeys.WEIGHT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.WEIGHT_UNIT, ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.PatientTabKeys.WEIGHT_UNIT), SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_BIRTH_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_DEATH,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_DEATH_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_DEATH_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_DEATH_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_AUTOPSY_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD_DAY),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD_MONTH),
								propMap.get(SceptreConstants.PatientTabKeys.DATE_OF_LAST_MENSTRUAL_PERIOD_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}
	
	private static void listFieldForReadingMedicalHistoryTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.LOWLEVELTERM);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.PREFFEREDTERM);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.STARTDATE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.CONTINUING);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.ENDDATE);
		listTabwiseFields.add(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT);
		
		s_mapTabwiseFields.put("MEDICALHISTORY", listTabwiseFields);
	}
	
	private static void mapFieldForReadingMedicalHistoryTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.CODINGSTATUS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.CONTINUING , ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.MedicalHistoryTabKeys.CONTINUING), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.LOWLEVELTERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.LOWLEVELTERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.MedicalHistoryTabKeys.MEDICALHISTORYCOMMENT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.PREFFEREDTERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.PREFFEREDTERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.MedicalHistoryTabKeys.REPORTEDDISEASE), SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.STARTDATE ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.MedicalHistoryTabKeys.STARTDATEDAY),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.STARTDATEMONTH),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.STARTDATEYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.MedicalHistoryTabKeys.ENDDATE ,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.MedicalHistoryTabKeys.ENDDATEDAY),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.ENDDATEMONTH),
								propMap.get(SceptreConstants.MedicalHistoryTabKeys.ENDDATEYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}
	
	private static void listFieldForReadingDrugHistoryTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.DRUGNAME);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.START);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.END);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.ROUTEOFADMIN);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.DRUGABUSE);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS2);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM1);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM2);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS3);
		listTabwiseFields.add(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM2);
		
		s_mapTabwiseFields.put("DRUGHISTORY", listTabwiseFields);
	}

	private static void mapFieldForReadingDrugHistoryTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS2 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS2), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS3 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.CODDINGSTATUS3), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.CODINGSTATUS1), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.DRUGABUSE , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.DrugHistoryTabKeys.DRUGABUSE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.DRUGNAME , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.DRUGNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM1 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM1), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM2 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.LOWLEVELTERM2), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM1 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM1), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM2 , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.PREFFEREDTERM2), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.REPORTEDDRUGNAME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.REPORTEDINDICATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.DrugHistoryTabKeys.REPORTEDREACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.ROUTEOFADMIN , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.DrugHistoryTabKeys.ROUTEOFADMIN), SceptreConstants.NAME_IDENTIFIER));
		
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.START,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugHistoryTabKeys.STARTDAY),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.STARTMONTH),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.STARTYEAR) },
						SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.DrugHistoryTabKeys.END,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.DrugHistoryTabKeys.ENDDAY),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.ENDMONTH),
								propMap.get(SceptreConstants.DrugHistoryTabKeys.ENDYEAR) },
						SceptreConstants.NAME_IDENTIFIER));

	}
	
	private static void listFieldForReadingReactionTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REPORTED_REACTION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LOWLEVEL_TERM);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.CODDING_STATUS);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.PREFFERED_TERM);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.VERSION_WHEN_CODED);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.SIREOUS_RADIO);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REASON_FOR_NON_IME);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.RATIONAL_FOR_NONIME);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.OUTCOME);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.TERM_HIGHLIGHTEDBY_REPORTER);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REPORTED_CAUSE_OF_DEATH);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.AUTOSPY_CAUSE_OF_DEATH);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED_HOUR);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MINUTE);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END_HOUR);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_END_MINUTE);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION);
		listTabwiseFields.add(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT);
		
		s_mapTabwiseFields.put("REACTION", listTabwiseFields);
	}

	private static void mapFieldForReadingReactionTab()
	{
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.AUTOSPY_CAUSE_OF_DEATH , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReactionTabKeys.AUTOSPY_CAUSE_OF_DEATH), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.VERSION_WHEN_CODED , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.VERSION_WHEN_CODED), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.CODDING_STATUS , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.CODDING_STATUS), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.DRUG_ADMIN_TO_START_OF_THIS_REACTION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.LAST_DOSE_TO_START_OF_THIS_REACTION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.LOWLEVEL_TERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.LOWLEVEL_TERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.OUTCOME , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.OUTCOME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.PREFFERED_TERM , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.PREFFERED_TERM), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.RATIONAL_FOR_NONIME , ElementUiConfig.createField("xpath-textarea", propMap.get(SceptreConstants.ReactionTabKeys.RATIONAL_FOR_NONIME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_DURATION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_DURATION_UNIT), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_END_HOUR , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_HOUR), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_END_MINUTE , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_MINUTE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED_HOUR , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_HOUR), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MINUTE , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MINUTE), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REASON_FOR_NON_IME , ElementUiConfig.createField("xpath-select", propMap.get(SceptreConstants.ReactionTabKeys.REASON_FOR_NON_IME), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REPORTED_CAUSE_OF_DEATH , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReactionTabKeys.REPORTED_CAUSE_OF_DEATH), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REPORTED_REACTION , ElementUiConfig.createField("xpath-name", propMap.get(SceptreConstants.ReactionTabKeys.REPORTED_REACTION), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.SIREOUS_RADIO , ElementUiConfig.createField("xpath-rbutton", propMap.get(SceptreConstants.ReactionTabKeys.SIREOUS_RADIO), SceptreConstants.NAME_IDENTIFIER));
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.TERM_HIGHLIGHTEDBY_REPORTER , ElementUiConfig.createField("xpath-value", propMap.get(SceptreConstants.ReactionTabKeys.TERM_HIGHLIGHTEDBY_REPORTER), SceptreConstants.NAME_IDENTIFIER));
	
		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_STARTED,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_DAY),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_MONTH),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_STARTED_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));

		s_mapFieldConfiguration.put(SceptreConstants.ReactionTabKeys.REACTION_END,
				ElementUiConfig.createDateField("xpath-date",
						new String[] { propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_DAY),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_MONTH),
								propMap.get(SceptreConstants.ReactionTabKeys.REACTION_END_YEAR) },
						SceptreConstants.NAME_IDENTIFIER));
	}
	
	private static void listFieldForReadingDrugTab()
	{
		List<String> listTabwiseFields = null;
		listTabwiseFields = new ArrayList<String>();

//		//listTabwiseFields.add(SceptreConstants.DrugTabKeys.MADEBY);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_DRUGNAME);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.GENERICDRUGNAME);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CHARACTERIZATION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ACTIONTAKEN);
		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_FORM);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ROUTEOFADMIN_DROPDOWN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PARENT_DOSING);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DRUG_DURATION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DRUG_DURATION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.GESTATION_PERIOD_ATTIMEOF_EXPOSURE_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ADDITIONAL_INFO);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SAMPLE_AVAILABLE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_RANK);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_STATUS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSE_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.NUM_DOSAGES);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INTERVAL);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INTERVAL_FREQ);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.FIRST_REACTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INDICATION_RANK);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_INDICATION);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LOW_LEVEL_TERM);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PREFFERED_TERM);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CODING_STATUS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.BATCH_RANK);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.BATCHLOT);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PRODUCT_QUALITY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.REPORTED_DEFECT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SENT_TO_QAYN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVALID_LOT);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVESTIGATION);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.INVESTIGATION_CONSLUSION);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DESCRIPTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_ADMIN_TO_FIRST_REACTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_ADMIN_TO_FIRST_REACTION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_DOSE_TO_FIRST_REACTION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_DOSE_TO_FIRST_REACTION_UNIT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY_WHERE_DRUG_OBTAIN_DROPDOWN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.WHERE_DRUG_PURCHASE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PURCHASE_LOCATION);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.PHONE_NUMBER);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.ADDRESS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.MANUFACTURER);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DEPARTMENT);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.NON_COMAPNY_ADDRESS);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.CITY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.STATE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.POSTAL_CODE);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRY);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.COUNTRYDROPDOWN);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACCINE_COUNTRY);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACCINE_AT);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.VACINE_PURCHASE_WITH);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_NO);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_DOCTOR);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_TO_MANUFACTURER);
////		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EVENT_REPORTED_PREVIOUSLY_HEALTH_DEPARTMENT);
//
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.START_DRUG);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.LAST_ADMIN);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_START);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.DOSAGE_END);
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.SENT_TO_QA);
//	
//		listTabwiseFields.add(SceptreConstants.DrugTabKeys.EXPIRATION);
		
		s_mapTabwiseFields.put("READDRUG", listTabwiseFields);
	}

	
	public Map<String, Object> readCaseIdTab(Map<String, Object> sceptreMap)
	{
				
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("READCASEID");
		
		if(sceptreMap == null)
		{
			sceptreMap = new HashMap<>();
		}
		
		readData(sceptreMap, listFieldsToFetch);		
		
		return sceptreMap;
		
	}//caseId
	

	public Map<String, Object> readPatientTab(Map<String, Object> sceptreMap)
	{
				
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.PatientTabKeys.PATIENT_TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("READPATIENT");
		
		readData(sceptreMap, listFieldsToFetch);
		
		return sceptreMap;
		
	}//patient
	

	public Map<String, Object> readMedicalHistoryTab(Map<String, Object> sceptreMap)
	{
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.MedicalHistoryTabKeys.TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("READMEDICALHISTORY");
		
		readData(sceptreMap, listFieldsToFetch);
		
		return sceptreMap;
		
	}//medical
	
	public Map<String, Object> readDrugHistoryTab(Map<String, Object> sceptreMap)
	{
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.DrugHistoryTabKeys.TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("DRUGHISTORY");
		
		readData(sceptreMap, listFieldsToFetch);
		
		return sceptreMap;
		
	}//drug

	
	public Map<String, Object> readReactionTab(Map<String, Object> sceptreMap)
	{
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.ReactionTabKeys.TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("REACTION");
		
		readData(sceptreMap, listFieldsToFetch);
		
		return sceptreMap;
		
	}//reaction

	public Map<String, Object> readDiagnosticTab(Map<String, Object> sceptreMap)
	{
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.ReactionTabKeys.TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("DIAGNOSTIC");
		
		readData(sceptreMap, listFieldsToFetch);
		
		return sceptreMap;
		
	}//

	public Map<String, Object> readDrugTab(Map<String, Object> sceptreMap)
	{
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.DrugTabKeys.TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("READDRUG");
		
		readData(sceptreMap, listFieldsToFetch);
		
		return sceptreMap;
		
	}//
	
	public Map<String, Object> readTab2(Map<String, Object> sceptreMap)
	{
		List<String> listFieldsToFetch = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.ReactionTabKeys.TAB));
		
		listFieldsToFetch = getFieldsToBeFetch("REACTION");
		
		readData(sceptreMap, listFieldsToFetch);
		
		return sceptreMap;
		
	}//
	
	public void readData(Map<String, Object> p_mapSceptre, List<String> p_listFieldsToFetch)
	{
		ElementUiConfig oFieldElementUiConfig = null;
		
		for (String sFieldToFetch : p_listFieldsToFetch)
		{
			System.out.println(sFieldToFetch);
			if (!s_mapFieldConfiguration.containsKey(sFieldToFetch))
			{
				continue;
			}
			oFieldElementUiConfig = s_mapFieldConfiguration.get(sFieldToFetch);
			
			if (oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.FIELD_DIRECLTY_MAPPED)
			{
				if (oFieldElementUiConfig.getAccessMethod().equals("xpath-name"))
				{
					p_mapSceptre.put(sFieldToFetch , getTextOfValueAttributeElementByXpath(oFieldElementUiConfig.getAccessAtribute() , oFieldElementUiConfig.getAccessDetails()[0]));
				}
				else if (oFieldElementUiConfig.getAccessMethod().equals("xpath-select"))
				{
					String sSelectedValue = null;
					sSelectedValue = getSelectedTextByDropDownName(oFieldElementUiConfig.getAccessDetails()[0]);
					if(!StringUtils.isNullOrEmpty(sSelectedValue))
					{
						p_mapSceptre.put(sFieldToFetch , sSelectedValue);
					}
					else
					{
						p_mapSceptre.put(sFieldToFetch , "");
						
					}
					
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-rbutton"))
				{
					String sRadioButtonValue = null;
					
					sRadioButtonValue = isRadioButtonCheckedAndGetValue(oFieldElementUiConfig.getAccessDetails()[0]);

					if(sRadioButtonValue.equalsIgnoreCase("y"))
					{
						p_mapSceptre.put(sFieldToFetch ,"Yes");
					}
					else if(sRadioButtonValue.equalsIgnoreCase("n"))
					{
						p_mapSceptre.put(sFieldToFetch ,"No");
					}
					else if(sRadioButtonValue.equalsIgnoreCase("u"))
					{
						p_mapSceptre.put(sFieldToFetch ,"Unknown");
					}
					else if(sFieldToFetch.equalsIgnoreCase(SceptreConstants.DrugTabKeys.CHARACTERIZATION))
					{
						String sCharacterization = null;
						
						if(sRadioButtonValue.equalsIgnoreCase("31"))
						{
							sCharacterization = SceptreConstants.DrugTabKeys.CC_SUSPECT;
							p_mapSceptre.put(sFieldToFetch , sCharacterization);
						}
						else if(sRadioButtonValue.equalsIgnoreCase("32"))
						{
							sCharacterization = SceptreConstants.DrugTabKeys.CC_SUSPECT_DRUG_INTERACTING;
							p_mapSceptre.put(sFieldToFetch , sCharacterization);
						}
						else
						{
							sCharacterization = SceptreConstants.DrugTabKeys.CC_CONCOMITANT;
							p_mapSceptre.put(sFieldToFetch , sCharacterization);
						}
					}
					else
					{
						p_mapSceptre.put(sFieldToFetch , sRadioButtonValue);
					}
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-textarea"))
				{
					if(getTextFromXpath(oFieldElementUiConfig.getAccessDetails()[0]) != null)
					{
						p_mapSceptre.put(sFieldToFetch , getTextFromXpath(oFieldElementUiConfig.getAccessDetails()[0]));
					}
					else
					{
						p_mapSceptre.put(sFieldToFetch , "");
					}
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-value"))
				{
					String sSelectedValue = null;
					
					sSelectedValue = isCheckBoxSelectedAndGetValue(oFieldElementUiConfig.getAccessDetails()[0]);
					
					if(sSelectedValue.equalsIgnoreCase("Y"))
					{
						p_mapSceptre.put(sFieldToFetch , "Yes");
					}
					else if(sSelectedValue.equalsIgnoreCase("n"))
					{
						p_mapSceptre.put(sFieldToFetch , "No");
					}
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-lookup"))
				{
					p_mapSceptre.put(sFieldToFetch , getAllDropDownList(oFieldElementUiConfig.getAccessDetails()[0]));
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-Addinfo"))
				{
					if(sFieldToFetch.equalsIgnoreCase(SceptreConstants.DrugTabKeys.DOSAGE_TEXT))
					{
						p_mapSceptre.put(sFieldToFetch , fetchDoseInformation());
					}
					
				}
				else if(oFieldElementUiConfig.getAccessMethod().equals("xpath-subtab"))
				{
					if(sFieldToFetch.equalsIgnoreCase(SceptreConstants.DrugTabKeys.RELATEDNESS))
					{
						p_mapSceptre.put(sFieldToFetch ,fetchDrugRelatednessSubTab());
					}
					else if(sFieldToFetch.equalsIgnoreCase(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_LABELING))
					{
						p_mapSceptre.put(sFieldToFetch ,fetchDrugCountrySpecificLabelingSubTab());
					}
					else if(sFieldToFetch.equalsIgnoreCase(SceptreConstants.DrugTabKeys.CORE_LABELING))
					{
						p_mapSceptre.put(sFieldToFetch ,fetchDrugCoreLabelingSubTab());
					}
					
				}
			}
			else if (oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.DATE_TRIPLET_MAPPED_FIELD)
			{
				String sDay = null , sMonth = null , sYear = null;
				
				if(sFieldToFetch.equals(SceptreConstants.DrugTabKeys.EXPIRATION))
				{
					sMonth = getSelectedTextByDropDownName(oFieldElementUiConfig.getAccessDetails()[0]);
					
					sYear = getTextOfValueAttributeElementByXpath(oFieldElementUiConfig.getAccessAtribute() , oFieldElementUiConfig.getAccessDetails()[1]);
				}
				else
				{
					sDay = getTextOfValueAttributeElementByXpath(oFieldElementUiConfig.getAccessAtribute() , oFieldElementUiConfig.getAccessDetails()[0]);
					
					sMonth = getSelectedTextByDropDownName(oFieldElementUiConfig.getAccessDetails()[1]);
					
					sYear = getTextOfValueAttributeElementByXpath(oFieldElementUiConfig.getAccessAtribute() , oFieldElementUiConfig.getAccessDetails()[2]);
				}
				
				if(StringUtils.isNullOrEmpty(sDay) && StringUtils.isNullOrEmpty(sMonth) && StringUtils.isNullOrEmpty(sYear))
				{
					p_mapSceptre.put(sFieldToFetch , "");
				}
				else
				{
					if(StringUtils.isNullOrEmpty(sDay))
					{
						sDay = "";
					}
					if(StringUtils.isNullOrEmpty(sMonth))
					{
						sMonth = "";
					}
					if(StringUtils.isNullOrEmpty(sYear))
					{
						sYear = "";
					}
						
					p_mapSceptre.put(sFieldToFetch , sDay + "-" + sMonth + "-" + sYear);
				}
			}
			else if (oFieldElementUiConfig.m_nFieldType == ElementUiConfig.TYPE.VARY_XPATH_MAPPED_FIELD)
			{
				if (oFieldElementUiConfig.getAccessMethod().equals("xpath-name"))
				{
					if(isElementPresentByName(oFieldElementUiConfig.getAccessDetails()[0]))
					{
						p_mapSceptre.put(sFieldToFetch , getTextOfValueAttributeElementByXpath(oFieldElementUiConfig.getAccessAtribute() , oFieldElementUiConfig.getAccessDetails()[0]));
					}
					else
					{
						p_mapSceptre.put(sFieldToFetch , getTextOfValueAttributeElementByXpath(oFieldElementUiConfig.getAccessAtribute() , oFieldElementUiConfig.getAccessDetails()[1]));
					}
				}
				else if (oFieldElementUiConfig.getAccessMethod().equals("xpath-select"))
				{
					String sSelectedValue = null;
					
					if(isElementPresentByName(oFieldElementUiConfig.getAccessDetails()[0]))
					{
						sSelectedValue = getSelectedTextByDropDownName(oFieldElementUiConfig.getAccessDetails()[0]);
					}
					else
					{
						sSelectedValue = getSelectedTextByDropDownName(oFieldElementUiConfig.getAccessDetails()[1]);
					}

					if(!StringUtils.isNullOrEmpty(sSelectedValue))
					{
						p_mapSceptre.put(sFieldToFetch , sSelectedValue);
					}
					else
					{
						p_mapSceptre.put(sFieldToFetch , "");
						
					}
					
				}
				
			}
			
			
		}//for
		
	}
	
	private List<String> fetchApprovalNumbers()
	{
		int nApprovalRowSize = 0;

		List<String> srrApprovalNumbers = null;
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.DRUG_COUNTRY_SPECIFIC_APPROVAL));
		
		srrApprovalNumbers = new ArrayList<>();

		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.COUNTRY_SPECIFIC_APPROVAL_PRESENT)))
		{
			nApprovalRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.GETAPPROVAL_TBODYID));
			
			log.info("List Approvals size is " + nApprovalRowSize);
			
			
			for(int nCurrentRow = 1 ; nCurrentRow <= nApprovalRowSize ; nCurrentRow ++)
			{
				srrApprovalNumbers.add(getTextFromXpath(
						propMap.get(SceptreConstants.DrugTabKeys.GET_APPROVAL_NUMBERS_GET_TEXT_PREFIX) + nCurrentRow
						+ propMap.get(SceptreConstants.DrugTabKeys.GET_APPROVAL_NUMBERS_GET_TEXT_POSTFIX)));
			}
		}
		
		return srrApprovalNumbers;
	}
	
	public Set<String> fetchOtherIDNumbers()
	{
		int nOtherIDNumberRowSize = 0;
		
		Set<String> listOfOtherCaseIdNumber = null;
		
		listOfOtherCaseIdNumber = new HashSet<>();

		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB));
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.OTHER_IDENTIFICATION_NUMBER_PRESENT)))
		{
			
			nOtherIDNumberRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_TBODYID));
			
			for (int nCurrentRow = 1; nCurrentRow <= nOtherIDNumberRowSize; nCurrentRow++) 
			{
				listOfOtherCaseIdNumber.add(getTextFromXpath(propMap.get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_OTHERID_PREFIX)
						+ nCurrentRow
						+ propMap.get(SceptreConstants.CaseIdTabKeys.OTHER_IDENTIFICATION_NUMBER_OTHERID_POSTFIX)));

			}
		}
		return listOfOtherCaseIdNumber;
	}
	
	public SearchResults fetchLinkedNumber()
	{
		int nLinkedCasesRowSize = 0;
		
		SearchResults oSearchResult = null;
		
		enterClickByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.CaseIdTabKeys.CASEID_TAB));

		oSearchResult = new SearchResults();
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.LINKEDCASES_PRESENT)))
		{
			
			nLinkedCasesRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.CaseIdTabKeys.LINKEDCASES_TBODYID));
			
			for (int nCurrentRow = 1; nCurrentRow <= nLinkedCasesRowSize; nCurrentRow++) 
			{
				oSearchResult.insertValue(SceptreConstants.Name.LINK_TYPE , getTextFromXpath(propMap.get(SceptreConstants.CaseIdTabKeys.LINKEDCASES_LINKTYPE_PREFIX)
						+ nCurrentRow
						+ propMap.get(SceptreConstants.CaseIdTabKeys.LINKEDCASES_LINKTYPE_POSTFIX)));


				oSearchResult.insertValue(SceptreConstants.Name.AER_NUMBER , getTextFromXpath(propMap.get(SceptreConstants.CaseIdTabKeys.LINKEDCASES_AERNUMBER_PREFIX)
						+ nCurrentRow
						+ propMap.get(SceptreConstants.CaseIdTabKeys.LINKEDCASES_AERNUMBER_POSTFIX)));
			}
		}
		return oSearchResult;
		
	}
	
	
	
	private SearchResults fetchDrugRelatednessSubTab()
	{
		int nRelatednessRowSize = 0;
		
		SearchResults oSearchResult = null;
		
		oSearchResult = new SearchResults();
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.RELATEDNESS));
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.RELATEDNESS_TBODYID_PRESENT)))
		{
			nRelatednessRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.RELATEDNESS_TBODYID));
			
			for(int i = 0 ; i < nRelatednessRowSize ; i++)
			{
				
				oSearchResult.insertValue(SceptreConstants.DrugTabKeys.REPORTED_CASUALLITY,
						getSelectedTextByDropDownName(propMap.get(SceptreConstants.DrugTabKeys.REPORTEDCASUALITY_PREFIX)
								+ i + propMap.get(SceptreConstants.DrugTabKeys.REPORTEDCASUALITY_POSTFIX)));

				oSearchResult.insertValue(SceptreConstants.DrugTabKeys.COMPANY_CAUSALITY,
						getSelectedTextByDropDownName(propMap.get(SceptreConstants.DrugTabKeys.COMPANYCASUALITY_PREFIX)
								+ i + propMap.get(SceptreConstants.DrugTabKeys.COMPANYCASUALITY_POSTFIX)));
			}
		}
		return oSearchResult;
	} 
	
	
	private List<String>  fetchDrugCountrySpecificLabelingSubTab()
	{
		int nRelatednessRowSize = 0;
		
		List<String> listLabelling = null;
		
		listLabelling = new ArrayList<>();
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_LABELING));
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.COUNTRY_SPECIFIC_LABELING_TBODYID_PRESENT)))
		{
			nRelatednessRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.COUNTRY_SPECIFIC_LABELING_TBODY_ID));
			
			for(int i = 0 ; i < nRelatednessRowSize ; i++)
			{
				listLabelling.add(getRadioButtonValueForCountryLabelling(propMap.get(SceptreConstants.DrugTabKeys.MAH_LABELING_PREFIX) + i + propMap.get(SceptreConstants.DrugTabKeys.MAH_LABELING_POSTFIX)));
			}
			
		}
		return listLabelling;
	} 
	
	
	private List<String> fetchDrugCoreLabelingSubTab()
	{

		int nCoreLebellingRowSize = 0;

		List<String> listLabelling = null;
		
		listLabelling = new ArrayList<>();
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.CORE_LABELING));
		
		nCoreLebellingRowSize = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.CORELABEL_TBODYID));
		
		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.CORELABELING_TBODYID_PRESENT)))
		{
			for(int i = 0 ; i < nCoreLebellingRowSize ; i++)
			{
				listLabelling.add(getRadioButtonValueForCoreLabelling(propMap.get(SceptreConstants.DrugTabKeys.CORELABELPREFIX) + i + propMap.get(SceptreConstants.DrugTabKeys.CORELABELPOSTFIX)));
			}
			
		}
		return listLabelling;
	}

	
	
	private String getRadioButtonValueForCountryLabelling(String xPathValue)
	{
		String sRadioButtonValue = null , sReturnValue = null;
		
		sRadioButtonValue = isRadioButtonCheckedAndGetValue(xPathValue);

		if(sRadioButtonValue.equalsIgnoreCase(SceptreConstants.YES_VALUE))
		{
			sReturnValue = SceptreConstants.Name.LABELED;
			return sReturnValue;
		}
		else if(sRadioButtonValue.equalsIgnoreCase(SceptreConstants.NO_VALUE))
		{
			sReturnValue = SceptreConstants.Name.UNLABELED;
			return sReturnValue;
		}
		else if(sRadioButtonValue.equalsIgnoreCase(SceptreConstants.UNDEFINED))
		{
			sReturnValue = SceptreConstants.Name.UNKNOWN;
			return sRadioButtonValue;
		}
		else
		{
			return sRadioButtonValue;
		}
	}
	

	private String getRadioButtonValueForCoreLabelling(String xPathValue)
	{
		String sRadioButtonValue = null , sReturnValue = null;
		
		sRadioButtonValue = isRadioButtonCheckedAndGetValue(xPathValue);

		if(sRadioButtonValue.equalsIgnoreCase(SceptreConstants.Name.COUNTRYLABELLING_LISTED))
		{
			sReturnValue = SceptreConstants.Name.LISTED;
			return sReturnValue;
		}
		else if(sRadioButtonValue.equalsIgnoreCase(SceptreConstants.Name.COUNTRYLABELLING_UNLISTED))
		{
			sReturnValue = SceptreConstants.Name.UNLISTED;
			return sReturnValue;
		}
		else if(sRadioButtonValue.equalsIgnoreCase(SceptreConstants.Name.COUNTRYLABELLING_AUTO))
		{
			sReturnValue = SceptreConstants.Name.AUTO;
			return sReturnValue;
		}
		else
		{
			return sRadioButtonValue;
		}
	}
	
	private String fetchDoseInformation()
	{
		String sParentWindow = null , sDoseInfo = null;
		
		sParentWindow = getWindowHandle();
		
		clickElementByAbsolutePath(propMap.get(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO));
		
		switchWindowHandler(sParentWindow);
		
		sDoseInfo =	getTextFromXpath(propMap.get(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO_TEXT));
		
		clickElementByXpath(SceptreConstants.INPUT_TAG, SceptreConstants.NAME_IDENTIFIER, propMap.get(SceptreConstants.DrugTabKeys.DRUG_DOSE_ADDITIONAL_INFO_CONTINUE));
	
		switchWindow(sParentWindow);
		
		return sDoseInfo;
	}
	
	

	public SearchResults getReactionListAll()
	{
		int nListAllReaction  = 0;

		SearchResults oSearchResult = null;
		
		clickElementByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.ReactionTabKeys.TAB));
		
		clickElementByXpath(SceptreConstants.INPUT_TAG,SceptreConstants.NAME_IDENTIFIER,propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_BUTTON));
		
		oSearchResult = new SearchResults();

		if(isElementPresentInXpath(propMap.get(SceptreConstants.CheckElementPresence.REACTION_LISTALL_TBODYID_PRESENT)))
		{
			nListAllReaction = getDescendantsTrSizeByTableID(SceptreConstants.TBODY_TAG, SceptreConstants.ID_IDENTIFIER, propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_TBODYID));
			
			for(int nCurrentRow = 1 ; nCurrentRow <= nListAllReaction ; nCurrentRow++)
			{
				
				oSearchResult.insertValue(SceptreConstants.Name.REACTION_RANK , getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_RANK_PREFIX)
						+ nCurrentRow + propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_RANK_SUFFIX)));
				oSearchResult.insertValue(SceptreConstants.Name.REACTION_IME , getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_IME_PREFIX)
						+ nCurrentRow + propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_IME_SUFFIX)));
				oSearchResult.insertValue(SceptreConstants.Name.REACTION_OUTCOME , getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_OUTCOME_PREFIX)
						+ nCurrentRow + propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_OUTCOME_SUFFIX)));
				oSearchResult.insertValue(SceptreConstants.Name.REACTION_REPORTEDREACTION , getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_REPORTEDREACTION_PREFIX)
						+ nCurrentRow + propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_REPORTEDREACTION_SUFFIX)));
				oSearchResult.insertValue(SceptreConstants.Name.REACTION_SERIOUS , getTextWithoutVisibilityByXpath(propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_SERIOUS_PREFIX)
						+ nCurrentRow + propMap.get(SceptreConstants.ReactionTabKeys.REACTION_LISTALL_SERIOUS_SUFFIX)));
				
			}
			
		}
		
		return oSearchResult;
	}
	private void wait(String waitKey,  Map<String, String> propMap)
	{
		try
		{
			setWait(propMap.get(waitKey));
		}
		catch (InterruptedException e)
		{
			log.error("Error in wait condition");
		}
	}
	
	public void closeCaseNotesWindowIfAvailable(Set<String> oSetOfWindowBeforeGotoCase , Set<String> oSetOfWindowAfterGotoCase, String p_sWindowToFocus)
	{
		List<String> newWindowList = null;
		
		newWindowList = new ArrayList<>(WebApplicationWriterUtils.getLatestOccuredWindow(oSetOfWindowBeforeGotoCase, oSetOfWindowAfterGotoCase));
		
		if(newWindowList.size() > 0)
		{
			closeWindow(newWindowList.get(0) , p_sWindowToFocus);
		}
	}
	
	public Set<String> getSetOfWindow()
	{
		return getWindowHandles();
	}
	
	public String getCurrentWindow()
	{
		return getWindowHandle();
	}
}
