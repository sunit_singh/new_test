package com.sciformix.commons;

public class CommonAuditHelper {
	public static class AuditAttributes
	{
		public static final String TEMPLATE_ID 		=	"Template Id"; 
		public static final String TEMPLATE_STATE	=	"Template State";
		public static final String TEMPLATE_NAME	=	"Template Name";
		
		public static final String CASE_VALIDATION_RULESET_ID 		=	"Validation Ruleset Id"; 
		public static final String CASE_VALIDATION_RULESET_STATE	=	"Validation Ruleset State";
		public static final String CASE_VALIDATION_RULESET_NAME	=	"Validation Ruleset Name"; 
	}
		
	private CommonAuditHelper()
	{
		//Nothing to do
	}
}
