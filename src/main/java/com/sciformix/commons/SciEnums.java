package com.sciformix.commons;

public class SciEnums {

	public enum RecordState
	{
		ACTIVE(1, "Active"), UNDER_CONFIGURATION(100, "Under Configuration"), DEACTIVATED(900, "Deactivated");
		
		private int value = -1;
		private String displayName = null;
		
		RecordState(int value, String displayName)
		{
			this.value = value;
			this.displayName = displayName;
		}
		
		public int value()
		{
			return this.value;
		}
		
		public String displayName()
		{
			return displayName;
		}
		
		public static RecordState toEnum(int value)
		{
			RecordState[] values = RecordState.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (value == values[counter].value())
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + value);
		}
		
		public static boolean isCorrectWorkflowState(RecordState p_eCurrentState, RecordState p_eProposedState)
		{
			switch (p_eCurrentState)
			{
			case UNDER_CONFIGURATION:
				return (p_eProposedState == RecordState.ACTIVE || p_eProposedState == DEACTIVATED);
			case ACTIVE:
				return (p_eProposedState == DEACTIVATED);
			case DEACTIVATED:
				return false;
			default:
				return false;
			}
		}
	}
	
	public enum ConfigTplObjType {
		QRT("QRT","Quality Review Template", "com.sciformix.sciportal.apps.qr.QRTemplate"),
		SCF ("SCF","Safety DB Case File Template", "com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate"),
		EMAIL("EMAIL", "Email Template", "com.sciformix.sciportal.mail.MailTemplate"),
		CDVR("CDVR","Case Data Validation RuleSet","com.sciformix.sciportal.apps.ngv.CaseDataValidationRuleset"),
		MBT("MBT", "Mail Box Template", "com.sciformix.sciportal.mb.MailBox");
		
		
		private String code;
		private String displayName;
		private String className;
		
		ConfigTplObjType(String code, String displayName, String className) {
			this.code 			=	code;
			this.displayName 	=	displayName;
			this.className 		= 	className;
		}

		public String getCode() {
			return code;
		}

		public String getDisplayName() {
			return displayName;
		}
		
		public String getClassName()
		{
			return className;
		}
		
		public static ConfigTplObjType toEnum(String code)
		{	
			for (ConfigTplObjType l_configtplobjtype : ConfigTplObjType.values())
			{
				if (code.equals(l_configtplobjtype.getCode()))
				{
					return l_configtplobjtype;
				}
			}
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + code);
		}
	}
	
	public enum CreationMode {
		IMPORT("I","Import"), SCRATCH("S","Scratch");
		
		private String code;
		private String displayName;
		
		CreationMode (String code, String displayName) {
			this.code 			=	code;
			this.displayName 	=	displayName;
		}

		public String getCode() {
			return code;
		}

		public String getDisplayName() {
			return displayName;
		}
		
		public static CreationMode toEnum(String code)
		{	
			for (CreationMode l_creationmode : CreationMode.values())
			{
				if (code.equals(l_creationmode.getCode()))
				{
					return l_creationmode;
				}
			}
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + code);
		}
	}
	
	public enum EmailSendSetting {
		SUCCESS("S","Success Cases"),FAILURE("F","Failure Cases"),ALL("A","All Cases"),NONE("N","No Cases");
		
		private String
			sendCode
		;	
		private String
			displayName
		;	
		EmailSendSetting (String p_code, String p_displayname) {
			this.sendCode		=	p_code;
			this.displayName	=	p_displayname;
		}
		
		public String getSendCode () {
			return sendCode;
		}
		public String getDisplayName () {
			return displayName;
		}
		
		public static EmailSendSetting toEnum(String p_code)
		{
			EmailSendSetting[] values = EmailSendSetting.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (p_code.equals(values[counter].getSendCode()))
				{
					return values[counter];
				}
			}
			//TODO: Discuss about throwing the SciException rather than IllegalArgumentException
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + p_code);
		}
		
	}
	/**
	 * This enum is created to store the
	 * data type that is allowed for
	 * SciPortal File upload mapping.
	 * <ul>
	 * 	<li>S - String and Character</li>
	 *  <li>D - Date without timestamp</li>
	 *  <li>T - Date with timestamp</li>
	 *  <li>I - Integer without decimals</li>
	 *  <li>F - Integer with decimals</li>
	 * </ul>
	 * @author gmishra
	 *
	 */
	public enum DataType {
		STRING,
		DATE,
		TIMESTAMP,
		INTEGER,
		FLOAT
	}
	
	public enum MailBoxType {
		USER_PRIVATE("UP","User Private"), TEAM_SHARED("TS","Team Shared");
		
		private String code;
		private String displayName;
		
		MailBoxType(String code, String displayName) {
			this.code 			=	code;
			this.displayName 	=	displayName;
		}

		public String getCode() {
			return code;
		}

		public String getDisplayName() {
			return displayName;
		}
		
		public static MailBoxType toEnum(String code)
		{	
			for (MailBoxType l_mailboxtype : MailBoxType.values())
			{
				if (code.equals(l_mailboxtype.getCode()))
				{
					return l_mailboxtype;
				}
			}
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + code);
		}
	}
	
	public enum ModeOfOperation {
		PORATAL_UI("PU","Portal UI"), WEB_API("WA","Web API");
		
		private String code;
		private String displayName;
		
		ModeOfOperation(String code, String displayName) {
			this.code 			=	code;
			this.displayName 	=	displayName;
		}

		public String getCode() {
			return code;
		}

		public String getDisplayName() {
			return displayName;
		}
		public static ModeOfOperation toEnum(String code)
		{	
			for (ModeOfOperation l_modeofoperation : ModeOfOperation.values())
			{
				if (code.equals(l_modeofoperation.getCode()))
				{
					return l_modeofoperation;
				}
			}
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + code);
		}
	}
		/**
	 * CPAT ACTION
	 */
	
	public static enum CPAT_ACTION
	{
		INITIAL,
		TOOL_CLOSED, 
		LOGIN_SUCCESSFUL, 
		USER_INPUTS_COLLECTED,
		SPLIT_PARSE_SUCCESSFUL,
		VERIFICATION_SUCCESSFUL,
		REVIEW_SUCCESSFUL,
		
		// CPAT3
		COLLECT_USER_INPUTS,
		LOCATE_CASE,
		DOWNLOAD_SD,
		PARSE,
		PREVIEW,
		DATA_ENTRY,
		NG, 
		FOLLOWUP,BOOKCASE, LOGOUT,
		READIRT,
		
		//CPAT IRT
		DUPLICATE_SUMMARY,
		DUPLICATE_SEARCH,DUPLICATE,
		CASECLASSIFICATION,
		RESULT_SUMMARY;
		
		
		
		; 
	}

	/**
	 * This enum is created to declare all the 
	 * purpose for which an application sequence
	 * can be created. The DB will store the
	 * enum code in the column "SEQPURPOSE" of
	 * Application Sequence table.
	 * @author gmishra
	 */
	public static enum ApplicationSequencePurpose {
		MAILBOX("MAILBOX", "Mailbox"),ACTIVITY("ACTIVITY","Activity");
		
		ApplicationSequencePurpose(String code, String displayName) {
			this.code = code;
			this.displayName = displayName;
		}
		
		String code;
		String displayName;
		
		public String getCode() {
			return code;
		}
		
		public String displayName() {
			return displayName;
		}
		
		public static ApplicationSequencePurpose toEnum(String code) {	
			for (ApplicationSequencePurpose l_appseqpurpose : ApplicationSequencePurpose.values()) {
				if (code.equals(l_appseqpurpose.getCode())) {
					return l_appseqpurpose;
				}
			}
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + code);
		}
	}

	public static enum AgeGroup {
		INFANT("Infant"),CHILD("Child"), ADOLESCENT("Adolescent"), ADULT("Adult"), ELDERLY("Elderly"), UNKNOWN("Unknown");
		
		String	displayName;
		
		AgeGroup(String p_groupname) {
			this.displayName = p_groupname;
		}
		
		public String displayName() {
			return displayName;
		}
	}

	public static enum ReporterQualification {
		CONSUMER_OTHERNONHEALTHPROF("Consumer/other non health professional", "Consumer or Other Non Health Professional")
		, PATIENT("Patient", "Patient")
		, OTHERHEALTHPROF("Other health professional", "Other Health Professional")
		, PHARMACIST("Pharmacist", "Pharmacist")
		, CONTACT("Contact", "Contact")
		, LAWYER("Lawyer", "Lawyer")
		, PHYSICIAN("Physician", "Physician")
		, COMPANY_REPRESENTATIVE("Company Representative", "Company Representative");
		
		String	agXchangeDisplayName,
				sceptreDisplayName;
		
		ReporterQualification(String p_agxchangedisplayname, String p_sceptredisplayname) {
			this.agXchangeDisplayName = p_agxchangedisplayname;
			this.sceptreDisplayName = p_sceptredisplayname;
		}
		
		public String getAgXchangeDisplayName() {
			return agXchangeDisplayName;
		}
		public String getSceptreDisplayName() {
			return sceptreDisplayName;
		}
		
		public static ReporterQualification toEnumFromSceptreValue (String p_sceptredisplayname) {
			for (ReporterQualification l_reporterqual : ReporterQualification.values()) {
				if (p_sceptredisplayname.equals(l_reporterqual.getSceptreDisplayName())) {
					return l_reporterqual;
				}
		}
			throw new IllegalArgumentException("ReporterQualification: Illegal enum value - " + p_sceptredisplayname);
		}
		
	}
}
