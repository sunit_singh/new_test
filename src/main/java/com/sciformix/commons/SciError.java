package com.sciformix.commons;


public class SciError
{
	private ErrorCode m_errorCode = null;
	private String m_displayMessage = null;
	
	
	public SciError(WebErrorCodes.WebErrorCode p_errorCode, String p_errorMessage)
	{
		this((ErrorCode)p_errorCode, p_errorMessage);
	}

	public SciError(WebErrorCodes.SimpleWebErrorCode p_errorCode, String p_errorMessage)
	{
		this((ErrorCode)p_errorCode, p_errorMessage);
	}
	
	public SciError(ServiceErrorCodes.SimpleServiceErrorCode p_errorCode, String p_errorMessage)
	{
		this((ErrorCode)p_errorCode, p_errorMessage);
	}
	
	private SciError(ErrorCode p_errorCode, String p_errorMessage)
	{
		m_errorCode = p_errorCode;
		m_displayMessage = p_errorMessage;
	}
	
	public ErrorCode errorCode()
	{
		return m_errorCode;
	}
	
	public String errorMessage()
	{
		return m_displayMessage;
	}
	
	public String toString()
	{
		return m_errorCode.toString();
	}
}
