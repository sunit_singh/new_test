package com.sciformix.commons.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;

public class ParsedDataTable
{
	private static final Logger log = LoggerFactory.getLogger(ParsedDataTable.class);
	
	public static enum DataType
	{
		TEXT, NUMBER, DATE;
	}
	
	public static enum DataState
	{
		ASIS, CANONICALIZED, WORDWRAP, PHRASEWRAP;
	}
	
	public static enum ValidationState
	{
		UNVALIDATED(1, "Unvalidated"), 
		VALIDATION_NOT_APPLICABLE(2, "Validation Not Applicable"), 
		VALIDATED(3, "Validated"), 
		REQUIRED_REVIEW(4, "Validation Failed"),
		VALIDATED_WITH_WARNINGS(5, "Validation Warnings");
		
		private int value = -1;
		private String displayName = null;
		
		ValidationState(int value, String displayName)
		{
			this.value = value;
			this.displayName = displayName;
		}
		
		public int value()
		{
			return this.value;
		}
		
		public String displayName()
		{
			return displayName;
		}
		
		public static ValidationState toEnum(int value)
		{
			ValidationState[] values = ValidationState.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (value == values[counter].value())
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + value);
		}
	}
	
	public static abstract class ParsedDataValue
	{
		protected Object m_objValueRaw = null;
		protected Object m_objValueDisplay = null;
		protected DataState m_eDataState = null;
		protected ValidationState m_eValidationState = null;
		//Unvalidated, Validation not applicable, Validated, Requires Review
		
		protected ParsedDataValue(Object p_objValueRaw, Object p_objValueDisplay, DataState p_eDataState, ValidationState p_eValidationState)
		{
			m_objValueRaw = p_objValueRaw;
			m_objValueDisplay = p_objValueDisplay;
			m_eDataState = p_eDataState;
			m_eValidationState = p_eValidationState;
		}
		
		public Object rawValue()
		{
			return m_objValueRaw;
		}
		
		public Object displayValue()
		{
			return m_objValueDisplay;
		}
		
		public DataState dataState()
		{
			return m_eDataState;
		}
		
		public ValidationState validationState()
		{
			return m_eValidationState;
		}
	}
	
	public static class StringParsedDataValue extends ParsedDataValue
	{
		public StringParsedDataValue(String p_sValueRaw, String p_sValueDisplay, DataState p_eDataState, ValidationState  p_eValidationState)
		{
			super(p_sValueRaw, p_sValueDisplay, p_eDataState, p_eValidationState);
		}
	}
	
	public static class ParsedData
	{
		protected String m_sMnemonic = null;
		protected String m_sLabel = null;
		protected DataType m_eDataType = null;
		private List<ParsedDataValue> m_listValues = null;
		
		
		public ParsedData(String p_sMnemonic, String p_sLabel, ParsedDataValue... oarrValues)
		{
			m_sMnemonic = p_sMnemonic;
			m_sLabel = p_sLabel;
			m_listValues = new ArrayList<ParsedDataValue>(oarrValues.length);
			for (int index = 0; index < oarrValues.length; index++)
			{
				m_listValues.add(oarrValues[index]);
			}
		}
		
		public void addValue(ParsedDataValue p_oValue)
		{
			m_listValues.add(p_oValue);
		}
		
		public String mnemonic()
		{
			return m_sMnemonic;
		}
		
		public List<ParsedDataValue> values()
		{
			return m_listValues;
		}
	}
	
	
	

	
	private List<ParsedData> m_listDataTable = null;
	
	private int m_nReviewRequiredCounter = 0;
	
	public ParsedDataTable()
	{
		m_listDataTable = new ArrayList<ParsedData>();
		m_nReviewRequiredCounter = 0;
	}
	
	public void incrementReviewRequiredCount()
	{
		m_nReviewRequiredCounter++;
	}
	
	public int getReviewRequiredCounter()
	{
		return m_nReviewRequiredCounter;
	}
	
	public boolean isColumnPresent(String p_sMnemonicName)
	{
		if(p_sMnemonicName!=null)
		for (int nLookupColIndex = 0; nLookupColIndex < m_listDataTable.size(); nLookupColIndex++)
		{
			if (m_listDataTable.get(nLookupColIndex).m_sMnemonic!=null && m_listDataTable.get(nLookupColIndex).m_sMnemonic.equals(p_sMnemonicName))
			{
				return true;
			}
		}
		return false;
	}
	
	public List<ParsedDataValue> getColumnValues(String p_sMnemonicName)
	{
		for (int nLookupColIndex = 0; nLookupColIndex < m_listDataTable.size(); nLookupColIndex++)
		{
			if (m_listDataTable.get(nLookupColIndex).m_sMnemonic!=null && m_listDataTable.get(nLookupColIndex).m_sMnemonic.equals(p_sMnemonicName))
			{
				return m_listDataTable.get(nLookupColIndex).values();
			}
		}
		return null;
	}

	public String getMnemonic(int p_nColumnIndex)
	{
		return m_listDataTable.get(p_nColumnIndex).m_sMnemonic;
	}
	public String getLabel(int p_nColumnIndex)
	{
		return m_listDataTable.get(p_nColumnIndex).m_sLabel;
	}
	
	public List<ParsedDataValue> getColumnValues(int p_nColumnIndex)
	{
		return m_listDataTable.get(p_nColumnIndex).values();
	}
	public void appendMnemonicColumnValue(String p_sColumnName, StringParsedDataValue p_oValue)
	//public void appendMnemonicColumnValue(String p_sColumnName, String p_sValueRaw, String p_sValueDisplay, DataState  p_eDataState)
	{
		//StringParsedDataValue p_oValue=new StringParsedDataValue(p_sValueRaw, p_sValueDisplay, p_eDataState);
		
		for (int nLookupColIndex = 0; nLookupColIndex < m_listDataTable.size(); nLookupColIndex++)
		{
			 if(m_listDataTable.get(nLookupColIndex).m_sMnemonic!=null && m_listDataTable.get(nLookupColIndex).m_sMnemonic.equals(p_sColumnName))
			{
					m_listDataTable.get(nLookupColIndex).addValue(p_oValue);
			}
		}
	}
	
	public void appendColumnValue(int p_nColumnIndex, ParsedDataValue p_oValue)
	{
		m_listDataTable.get(p_nColumnIndex).addValue(p_oValue);
	}
//	public void appendColumnValue(int p_nColumnIndex, Object p_oValue)
//	{
//		m_listDataTable.get(p_nColumnIndex).addValue(p_oValue);
//	}
	public void addColumn(String p_sMnemonicName,String p_sLabel, ParsedDataValue p_oValue)
	{
			m_listDataTable.add(new ParsedData(p_sMnemonicName,p_sLabel, p_oValue));
	}
	
	public void addColumn(ParsedData oData)
	{
		m_listDataTable.add(oData);
	}
	
	public int colCount()
	{
		return m_listDataTable.size();
	}
	
	public int rowCount()
	{
		//For row count, take the size of the first column
		if (m_listDataTable.size() > 0)
		{
			return m_listDataTable.get(0).values().size();
		}
		else
		{
			return 0;
		}
	}
	
    public void dumpContents()
    {
    	
    	if(log.isTraceEnabled())
    	{
    		StringBuffer buffer = null;
    		List<String>list = null;
    		log.trace("--- BEGIN: Dumping contents of data-table ---");
            for (ParsedData oData : m_listDataTable)
            {
            	buffer = new StringBuffer();
            	buffer.append(oData.m_sMnemonic+"=");
            	list = new ArrayList<>();
            //	   log.trace("\t" + oData.m_sHeader + ": ");
                   for (Object oValue : oData.m_listValues)
                   {
                	   list.add((String) oValue);
                	  // buffer.append(oValue);
                	  // 	log.trace(String.valueOf(oValue));
                	   //	log.trace(", ");
                   }
                   buffer.append(list);
                   log.trace(buffer.toString());     
            }
            log.trace("--- END: Dumping contents of data-table ---");
    	}	
    }

	public JSONArray createJsonRepresentation(String p_sColumnsToCollapse) throws SciServiceException
	{
		List<String> listColumnNames = null;
		String[] arrColumnNames = null;
		JSONArray jsonArray = null;
		OrderedJSONObject jsonObject = null;
		List<ParsedDataValue> listDataTableValuesForColumn = null;
		jsonArray = new JSONArray();
		String sColumnValues = "";
		jsonObject = new OrderedJSONObject();
		Integer nlistDataTableValuesSize = null;
		String sTemp = null;
		try
		{
			arrColumnNames = p_sColumnsToCollapse == null ? new String[0] : p_sColumnsToCollapse.split(",");
			listColumnNames = new ArrayList<String>(Arrays.asList(arrColumnNames)); 
			for (ParsedData oData : m_listDataTable)
			{
				listDataTableValuesForColumn = oData.m_listValues;
				 if (oData.m_sMnemonic!=null && !listColumnNames.contains(oData.m_sMnemonic) && listDataTableValuesForColumn.size()>1 && listDataTableValuesForColumn.get(0).m_objValueDisplay=="")
				 {
					 listDataTableValuesForColumn.remove(0);
				 }
				if(listDataTableValuesForColumn == null)
				{
					nlistDataTableValuesSize = 0;
				}
				else
				{
					nlistDataTableValuesSize = listDataTableValuesForColumn.size();
				}
				if(oData.m_sMnemonic!=null)
				{
					switch(nlistDataTableValuesSize)
					{
						case 0: jsonObject.put(oData.m_sMnemonic, "");
								break;
						case 1:	jsonObject.put(oData.m_sMnemonic, listDataTableValuesForColumn.get(0).m_objValueDisplay==null?"":listDataTableValuesForColumn.get(0).m_objValueDisplay);
								break;	
								//TODO:Relook at the Json logic
						case 2: 
								/*
						 		 sTemp = (String) listDataTableValuesForColumn.get(1);
						 		 if(sTemp != null && !sTemp.equals(""))
						 		{*/
						 			jsonObject.put(oData.m_sMnemonic, listDataTableValuesForColumn.get(0)+" : "+listDataTableValuesForColumn.get(1).m_objValueDisplay);
						 		/*}else
						 		{
						 			jsonObject.put(oData.m_sHeader, listDataTableValuesForColumn.get(0)==null?"":listDataTableValuesForColumn.get(0));
						 		}*/
								break;
						default:
								sColumnValues = "";
							    for(int nLookupValIndex=0; nLookupValIndex<nlistDataTableValuesSize-1;nLookupValIndex++)
							    {
							    	if(nLookupValIndex+1 == nlistDataTableValuesSize-1)
							    	{
							    		sColumnValues = sColumnValues+(String) listDataTableValuesForColumn.get(nLookupValIndex).m_objValueDisplay;
							    	}else
							    	{
							    		sColumnValues = sColumnValues+(String) listDataTableValuesForColumn.get(nLookupValIndex).m_objValueDisplay+":";
							    	}	
							    }
							   	jsonObject.put(oData.m_sMnemonic, sColumnValues+" : "+listDataTableValuesForColumn.get(nlistDataTableValuesSize-1));
							    break;
					}
				}
			}
			jsonArray.put(jsonObject);
		}
		catch(JSONException excep)
		{
			log.error("Error while converting dataTable into JSON", excep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_PARSING_WORKBOOK_FILE, excep);
		}
		
		if(log.isTraceEnabled())
		{
			log.trace(jsonArray.toString());
		}
		
		return jsonArray;
	}
}
