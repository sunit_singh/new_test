package com.sciformix.commons.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;

public class DataTable
{
	private static final Logger log = LoggerFactory.getLogger(DataTable.class);
	
	public static class Data
	{
		private String m_sHeader = null;
		private List<Object> m_listValues = null;
		
		public Data(String p_sHeader, Object... oarrValues)
		{
			m_sHeader = p_sHeader;
			m_listValues = new ArrayList<Object>(oarrValues.length);
			for (int index = 0; index < oarrValues.length; index++)
			{
						m_listValues.add(oarrValues[index]);
			}
		}
		
		public void addValue(Object p_oValue)
		{
			m_listValues.add(p_oValue);
		}
		
		public String header()
		{
			return m_sHeader;
		}
		
		public List<Object> values()
		{
			return m_listValues;
		}
	}

	
	private List<Data> m_listDataTable = null;
	
	public DataTable()
	{
		m_listDataTable = new ArrayList<Data>();
	}
	
	public boolean isColumnPresent(String p_sColumnName)
	{
		if(p_sColumnName!=null)
		for (int nLookupColIndex = 0; nLookupColIndex < m_listDataTable.size(); nLookupColIndex++)
		{
			if (m_listDataTable.get(nLookupColIndex).header()!=null && m_listDataTable.get(nLookupColIndex).header().equals(p_sColumnName))
			{
				return true;
			}
		}
		return false;
	}
	
	public List<Object> getColumnValues(String p_sColumnName)
	{
		for (int nLookupColIndex = 0; nLookupColIndex < m_listDataTable.size(); nLookupColIndex++)
		{
			if (m_listDataTable.get(nLookupColIndex).header()!=null && m_listDataTable.get(nLookupColIndex).header().equals(p_sColumnName))
			{
				return m_listDataTable.get(nLookupColIndex).values();
			}
		}
		return null;
	}
	
	public String getColumnHeader(int p_nColumnIndex)
	{
		return m_listDataTable.get(p_nColumnIndex).header();
	}
	
	public List<Object> getColumnValues(int p_nColumnIndex)
	{
		return m_listDataTable.get(p_nColumnIndex).values();
	}
	
	public void appendColumnValue(String p_sColumnName, Object p_oValue)
	{
		for (int nLookupColIndex = 0; nLookupColIndex < m_listDataTable.size(); nLookupColIndex++)
		{
			 if(m_listDataTable.get(nLookupColIndex).header()!=null && m_listDataTable.get(nLookupColIndex).header().equals(p_sColumnName))
			{
					m_listDataTable.get(nLookupColIndex).addValue(p_oValue);
			}
		}
	}
	
	public void appendColumnValue(int p_nColumnIndex, Object p_oValue)
	{
		m_listDataTable.get(p_nColumnIndex).addValue(p_oValue);
	}
	
	public void addColumn(String p_sHeader, Object p_oValue)
	{
			m_listDataTable.add(new Data(p_sHeader, p_oValue));
	}
	
	public void addColumn(Data oData)
	{
		m_listDataTable.add(oData);
	}
	
	public int colCount()
	{
		return m_listDataTable.size();
	}
	
	public int rowCount()
	{
		//For row count, take the size of the first column
		if (m_listDataTable.size() > 0)
		{
			return m_listDataTable.get(0).values().size();
		}
		else
		{
			return 0;
		}
	}
	
    public void dumpContents()
    {
    	
    	if(log.isTraceEnabled())
    	{
    		StringBuffer buffer = null;
    		List<String>list = null;
    		log.trace("--- BEGIN: Dumping contents of data-table ---");
            for (Data oData : m_listDataTable)
            {
            	buffer = new StringBuffer();
            	buffer.append(oData.m_sHeader+"=");
            	list = new ArrayList<>();
            //	   log.trace("\t" + oData.m_sHeader + ": ");
                   for (Object oValue : oData.m_listValues)
                   {
                	   list.add((String) oValue);
                	  // buffer.append(oValue);
                	  // 	log.trace(String.valueOf(oValue));
                	   //	log.trace(", ");
                   }
                   buffer.append(list);
                   log.trace(buffer.toString());     
            }
            log.trace("--- END: Dumping contents of data-table ---");
    	}	
    }

	public JSONArray createJsonRepresentation(String p_sColumnsToCollapse) throws SciServiceException
	{
		List<String> listColumnNames = null;
		String[] arrColumnNames = null;
		JSONArray jsonArray = null;
		OrderedJSONObject jsonObject = null;
		List<Object> listDataTableValuesForColumn = null;
		jsonArray = new JSONArray();
		String sColumnValues = "";
		jsonObject = new OrderedJSONObject();
		Integer nlistDataTableValuesSize = null;
		String sTemp = null;
		try
		{
			arrColumnNames = p_sColumnsToCollapse == null ? new String[0] : p_sColumnsToCollapse.split(",");
			listColumnNames = new ArrayList<String>(Arrays.asList(arrColumnNames)); 
			for (Data oData : m_listDataTable)
			{
				listDataTableValuesForColumn = oData.m_listValues;
				 if (oData.m_sHeader!=null && !listColumnNames.contains(oData.m_sHeader) && listDataTableValuesForColumn.size()>1 && listDataTableValuesForColumn.get(0)=="")
				 {
					 listDataTableValuesForColumn.remove(0);
				 }
				if(listDataTableValuesForColumn == null)
				{
					nlistDataTableValuesSize = 0;
				}
				else
				{
					nlistDataTableValuesSize = listDataTableValuesForColumn.size();
				}
				if(oData.m_sHeader!=null)
				{
					switch(nlistDataTableValuesSize)
					{
						case 0: jsonObject.put(oData.m_sHeader, "");
								break;
						case 1:	jsonObject.put(oData.m_sHeader, listDataTableValuesForColumn.get(0)==null?"":listDataTableValuesForColumn.get(0));
								break;	
								//TODO:Relook at the Json logic
						case 2: 
								/*
						 		 sTemp = (String) listDataTableValuesForColumn.get(1);
						 		 if(sTemp != null && !sTemp.equals(""))
						 		{*/
						 			//jsonObject.put(oData.m_sHeader, listDataTableValuesForColumn.get(0)+" : "+listDataTableValuesForColumn.get(1));
						 			jsonObject.put(oData.m_sHeader, listDataTableValuesForColumn.get(0)+" ~~~ "+listDataTableValuesForColumn.get(1));
						 		/*}else
						 		{
						 			jsonObject.put(oData.m_sHeader, listDataTableValuesForColumn.get(0)==null?"":listDataTableValuesForColumn.get(0));
						 		}*/
								break;
						default:
								sColumnValues = "";
							    for(int nLookupValIndex=0; nLookupValIndex<nlistDataTableValuesSize-1;nLookupValIndex++)
							    {
							    	if(nLookupValIndex+1 == nlistDataTableValuesSize-1)
							    	{
							    		sColumnValues = sColumnValues+(String) listDataTableValuesForColumn.get(nLookupValIndex);
							    	}else
							    	{
							    		//sColumnValues = sColumnValues+(String) listDataTableValuesForColumn.get(nLookupValIndex)+":";
							    		sColumnValues = sColumnValues+(String) listDataTableValuesForColumn.get(nLookupValIndex)+"~~~";
							    	}	
							    }
							   	//jsonObject.put(oData.m_sHeader, sColumnValues+" : "+listDataTableValuesForColumn.get(nlistDataTableValuesSize-1));
							   	jsonObject.put(oData.m_sHeader, sColumnValues+" ~~~ "+listDataTableValuesForColumn.get(nlistDataTableValuesSize-1));
							    break;
					}
				}
			}
			jsonArray.put(jsonObject);
		}
		catch(JSONException excep)
		{
			log.error("Error while converting dataTable into JSON", excep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_PARSING_WORKBOOK_FILE, excep);
		}
		
		if(log.isTraceEnabled())
		{
			log.trace(jsonArray.toString());
		}
		
		return jsonArray;
	}
}
