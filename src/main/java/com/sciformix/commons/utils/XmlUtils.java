package com.sciformix.commons.utils;

import java.nio.charset.StandardCharsets;

import org.apache.commons.codec.binary.Base64;

public class XmlUtils
{
	private static final String _TAG_STARTING = "<%s>";
	private static final String _TAG_CLOSING = "</%s>";
	private static final String _TAG_NULL = "<%s />";
	
	private static final String _TAGPREFIX_CDATA = "<![CDATA[";
	private static final String _TAGSUFFIX_CDATA = "]]>";
	
	private XmlUtils()
	{
		//Nothing to do
	}
	
	public static String createElementStartingTag(String p_sElementName)
	{
		return String.format(_TAG_STARTING, p_sElementName);
	}
	
	public static String createElementClosingTag(String p_sElementName)
	{
		return String.format(_TAG_CLOSING, p_sElementName);
	}
	
	public static String createElementStartingTagWithCdata(String p_sElementName)
	{
		return String.format(_TAG_STARTING, p_sElementName) + _TAGPREFIX_CDATA;
	}
	
	public static String createElementClosingTagWithCdata(String p_sElementName)
	{
		return _TAGSUFFIX_CDATA + String.format(_TAG_CLOSING, p_sElementName);
	}
	
	public static String createElementTag(String p_sElementName, String p_sValue)
	{
		return createElementTag(p_sElementName, p_sValue, false);
	}
	
	public static String createElementTag(String p_sElementName, String p_sValue, boolean p_bBase64EncodeData)
	{
		if (p_sValue != null && p_sValue.trim().length() > 0)
		{
			if (!p_bBase64EncodeData)
			{
				return createElementStartingTag(p_sElementName) + p_sValue + createElementClosingTag(p_sElementName);
			}
			else
			{
				String sEncodedData = null;
				
				//Perform Base64 encoding
				sEncodedData = Base64.encodeBase64String(p_sValue.getBytes(StandardCharsets.UTF_8));
				
				//Add CDATA marker
				return createElementStartingTagWithCdata(p_sElementName) + sEncodedData + createElementClosingTagWithCdata(p_sElementName);
			}
		}
		else
		{
			return createElementNullTag(p_sElementName);
		}
	}
	
	public static String createElementNullTag(String p_sElementName)
	{
		return String.format(_TAG_NULL, p_sElementName);
	}
	
}
