package com.sciformix.commons.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;

public class FileUtils
{
	private static final Logger log = LoggerFactory.getLogger(FileUtils.class);
	
	public static boolean isPdfFile(String p_sTemplateFilePath)
	{
		return (p_sTemplateFilePath.toUpperCase().endsWith(".PDF"));
	}
	
	public static boolean isTxtFile(String p_sTemplateFilePath)
	{
		return (p_sTemplateFilePath.toUpperCase().endsWith(".TXT"));
	}
	
	public static String getFileText(String p_sTemplateFilePath) throws SciException
	{
		String sFileText  = null;
		File file = null;
		
		file = new File(p_sTemplateFilePath);
		try 
		{
			sFileText = org.apache.commons.io.FileUtils.readFileToString(file,"utf-8");
		} 
		catch (IOException ioExcep) 
		{
			log.error("Error reading contents of file - {} ", p_sTemplateFilePath, ioExcep);
			throw new SciException("Error reading contents of file", ioExcep);
		}
		
		return sFileText;
	}
	
	public static void createDirectory(String p_sFolderName) throws IOException
	{
		org.apache.commons.io.FileUtils.forceMkdir(new File(p_sFolderName));
	}
	
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();
	}
}
