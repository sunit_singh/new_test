package com.sciformix.commons.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataValidationUtils
{
	private final static String REGEX_PATTERN_VALID_OBJECT_NAME = "[a-zA-Z0-9][a-zA-Z0-9\\s_-]{1,62}[a-zA-Z0-9]";
	
	private final static String REGEX_PATTERN_VALID_MNEMONIC = "[a-zA-Z0-9_]{1,32}";
	
	private DataValidationUtils()
	{
		//Nothing to do
	}
	
	public static boolean isValidObjectName(String p_sInputValue)
	{
		if (StringUtils.isNullOrEmpty(p_sInputValue) || !p_sInputValue.matches(REGEX_PATTERN_VALID_OBJECT_NAME))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public static boolean isCaseIdValid(String sInputCaseId, String p_sExpectedPattern) 
	{
		Pattern oPattern = null;
		Matcher oMatcher = null;
		
		oPattern = Pattern.compile(p_sExpectedPattern);
		oMatcher = oPattern.matcher(sInputCaseId);
		if (oMatcher.find()) 
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public static boolean isMnemonicValid(String sInputMnemonic) {
		return validateData(sInputMnemonic, REGEX_PATTERN_VALID_MNEMONIC);
	}
	
	public static boolean validateData (
		String	p_data
	, 	String	p_regex
	) {
		Pattern	l_pattern	=	Pattern.compile(p_regex);
		Matcher	l_matcher	=	l_pattern.matcher(p_data);
		return l_matcher.matches();
	}
}
