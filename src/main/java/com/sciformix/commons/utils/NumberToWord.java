package com.sciformix.commons.utils;

public class NumberToWord  
{
   private static final String[] specialNames = 
	   {
       "",
       " thousand",
       " million",
       " billion",
       " trillion",
       " quadrillion",
       " quintillion"
   };
   
   private static final String[] tensNames = {
       "",
       " ten",
       " twenty",
       " thirty",
       " forty",
       " fifty",
       " sixty",
       " seventy",
       " eighty",
       " ninety"
   };
   
   private static final String[] numNames = {
       "",
       " one",
       " two",
       " three",
       " four",
       " five",
       " six",
       " seven",
       " eight",
       " nine",
       " ten",
       " eleven",
       " twelve",
       " thirteen",
       " fourteen",
       " fifteen",
       " sixteen",
       " seventeen",
       " eighteen",
       " nineteen"
   };
   
   private static String convertLessThanOneThousand(int iNumber) {
       String sCurrent;
       
       if (iNumber % 100 < 20){
           sCurrent = numNames[iNumber % 100];
           iNumber /= 100;
       }
       else {
           sCurrent = numNames[iNumber % 10];
           iNumber /= 10;
           
           sCurrent = tensNames[iNumber % 10] + sCurrent;
           iNumber /= 10;
       }
       if (iNumber == 0) return sCurrent;
       return numNames[iNumber] + " hundred" + sCurrent;
   }
   
   public static String convert(int iNumber) 
   {

       if (iNumber == 0) { return "zero"; }
       
       String sPrefix = "";
       
       if (iNumber < 0) {
           iNumber = -iNumber;
           sPrefix = "negative";
       }
       
       String sCurrent = "";
       int place = 0;
       
       do {
           int n = iNumber % 1000;
           if (n != 0){
               String s = convertLessThanOneThousand(n);
               sCurrent = s + specialNames[place] + sCurrent;
           }
           place++;
           iNumber /= 1000;
       } while (iNumber > 0);
       
       return (sPrefix + sCurrent).trim();
   }
}
