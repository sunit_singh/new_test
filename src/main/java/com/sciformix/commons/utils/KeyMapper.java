package com.sciformix.commons.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sciformix.commons.SciConstants.StringConstants;

public class KeyMapper {
	
	private static final String HEALTHCANADA_PROPERTY_FILE = "ApotexCanadaKeyMapping.properties";

	private static final String HEALTH_CANADA_FILETYPE_VALUE = "HealthCanada-AERReport-PDF";
	
	static List<String> permanentKeyList=new ArrayList<String>();
	
	public static DataTable mapKey(String fileType, DataTable oDataTable)
	{
		 DataTable pDataTable=new DataTable();
		 String key=null;
		 String value=null;
		 String propKeyaAsValue=null;
		 String keyBeforeMap=null;
		 String keyAfterMap=null;
		 String sKey=null, sValue=null;
		InputStream keyStream=null;
		 if(fileType.equals("NL"))
		 {
			 try {
					String path=System.getProperty("user.dir");
					keyStream = new FileInputStream(path+"\\"+"NLDKeyMapping.properties");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 Properties nldKeys=new Properties();
			 try {
				nldKeys.load(keyStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//	 createPermanentKeyList();
		 pDataTable=new DataTable();
		 
		 for (int nIndex = 0; nIndex < oDataTable.colCount() ; nIndex++)
         {
			 
			
			 for(int listIndex=0; listIndex<permanentKeyList.size();listIndex++)
			 {
			 if(oDataTable.isColumnPresent((String)permanentKeyList.get(listIndex)))
				{	
    			 sKey = oDataTable.getColumnHeader(nIndex);
    			 
    			 sValue = ""+oDataTable.getColumnValues(nIndex).get(oDataTable.getColumnValues(nIndex).size()-1);
    			 if(nldKeys.containsKey(sKey))
    			 {
    				 System.out.println("key found in NLD property file"+sKey);
    				 sKey=nldKeys.getProperty(sKey);
    				 
    				 if(pDataTable.isColumnPresent(sKey))
    				 {
    					 pDataTable.appendColumnValue(sKey.trim(), sValue);
    				 }
    				 else
						{
							//sValue="";
							pDataTable.addColumn(sKey.trim(), sValue);
						}
    				 
    			 }
				 
				else
				{
					System.out.println("key not present in NLD property file:"+sKey);
				}	
			 }
			 
			 
			 
			 String sddValue = ""+oDataTable.getColumnValues(nIndex).get(0);
			 System.out.println(sKey+"="+sValue+"="+sddValue);
			// prop.setProperty(sKey, sValue.trim());
         }
		 String tempK=null, tempV=null;
		 for (int index = 0; index < pDataTable.colCount() ; index++)
         {
			 tempK = pDataTable.getColumnHeader(nIndex);;
			 tempV = ""+pDataTable.getColumnValues(nIndex).get(0);
			// String sddValue = ""+pDataTable.getColumnValues(nIndex).get(0);
			 System.out.println("Mapping started-------------------------------------------------------------------------");
			 System.out.println(tempK+"="+tempV);
			 System.out.println("Mapping Done-------------------------------------------------------------------------");
			// prop.setProperty(sKey, sValue.trim());
         }
		 
		 }
		 }
		 if(fileType.equals(HEALTH_CANADA_FILETYPE_VALUE))
		 {
			 try {
					String path=System.getProperty("user.dir");
					keyStream = new FileInputStream(path+"\\"+HEALTHCANADA_PROPERTY_FILE);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
    			 Properties apotexCKeys=new Properties();
    			 try {
    				 apotexCKeys.load(keyStream);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		//	 createPermanentKeyList();
    	//	 DataTable pDataTable=new DataTable();
    		 Iterator<Object> apotexIterator=apotexCKeys.keySet().iterator();
    		 int productCount=(int)oDataTable.getColumnValues("Product Count").get(0);
    		 int eventCount=(int)oDataTable.getColumnValues("Event Count").get(0);
    		 int linkDupRecordCount=(int)oDataTable.getColumnValues("LinkDupRecord Count").get(0);
    		 pDataTable.addColumn("Product Count", productCount);
    		 pDataTable.addColumn("Event Count", eventCount);
    		 pDataTable.addColumn("LinkDupRecord Count", linkDupRecordCount);
    		 while(apotexIterator.hasNext())
    		 {
    			 	 key=(String) apotexIterator.next();
    				 propKeyaAsValue=apotexCKeys.getProperty(key);
    			 
    			 if(key.equals("Product") || key.equals("AER Description.Indication") || key.equals("PCM Description.Dose")
    					 || key.equals("PCM Description.Unit") || key.equals("PCM Description.Frequency")
    					 || key.equals("PCM Description.Dosage Form") || key.equals("PCM Description.Health Product Role")
    					 || key.equals("PatientRouteofAdministration"))
    			 {
    				 createMultipleKey(key, propKeyaAsValue, oDataTable, pDataTable, productCount);
    			 }
    			 else if(key.equals("DescriptionasReported") || key.equals("DescriptiontobeCoded") || key.equals("Reaction Duration") || key.equals("MedDRAVersion"))
    			 {
    				
    				 createMultipleKey(key, propKeyaAsValue, oDataTable, pDataTable, eventCount);
    			 }
    			 else if(key.equals("RecordType") || key.equals("LinkedAERNumber"))
    			 {
    				 createMultipleKey(key, propKeyaAsValue, oDataTable, pDataTable, linkDupRecordCount);
    			 }
    			 else if(key.contains("Date of Awareness") || key.contains("SRD") || key.contains("Date"))
    			 {
    				
    				 
    				 if(oDataTable.isColumnPresent(propKeyaAsValue))
    				 {
    						 
        				 value=(String) oDataTable.getColumnValues(propKeyaAsValue).get(0);
        			
        				
        					 
        				 Date preDate=null;
        				 String date=null;
        				 SimpleDateFormat preFormat=null;
						try {
							if(value.contains("-"))
							{
							 if(value.split("-")[1].length()==2)
								 preFormat=new SimpleDateFormat("yyyy-MM-dd");
							 else
								 preFormat=new SimpleDateFormat("dd-MMM-yyyy");

							}
							else if(value.contains("/"))
							{
								 if(value.split("/")[1].length()==2)
									 preFormat=new SimpleDateFormat("yyyy/MM/dd");
								 else
									 preFormat=new SimpleDateFormat("dd/MMM/yyyy");

								}
							
							//format = new SimpleDateFormat("yyyy-MM-dd")
							preDate =preFormat.parse(value);
							SimpleDateFormat postFormat=new SimpleDateFormat("dd-MMM-yyyy");
							date=postFormat.format(preDate);
							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
        				if(pDataTable.isColumnPresent(key))
        				 {
        					pDataTable.appendColumnValue(key, date);
        				 }
        				 else
        				 {
        					 pDataTable.addColumn(key, date);
        					 
        				 }
    				 }
    			 }
    			 else
    			 {
    			/* if(oDataTable.isColumnPresent(propKeyaAsValue))
				 {
    				 value=(String) oDataTable.getColumnValues(propKeyaAsValue).get(0);
    				// if(value.equals(""))
					 //pDataTable.appendColumnValue(key, value);
    				 pDataTable.addColumn(key, value);
					 
				 }*/
    			 if(oDataTable.isColumnPresent(propKeyaAsValue))
				 {
						 
    				 value=(String) oDataTable.getColumnValues(propKeyaAsValue).get(0).toString();
    				if(pDataTable.isColumnPresent(key))
    				 {
    					pDataTable.appendColumnValue(key, value);
    				 }
    				 else
    				 {
    					 pDataTable.addColumn(key, value);
    					 
    				 }
				 }
				 else
					{
						//List sValue=new ArrayList();
						//pDataTable.addColumn(key, sValue);
					}
    			 } 
    		 }
    		 System.out.println("-----------------Key mapper started--------------------------------------------------");
    		 
    		 for (int nIndex = 0; nIndex < pDataTable.colCount() ; nIndex++)
             {
    			 
    			 String sTempKey = pDataTable.getColumnHeader(nIndex);
    			 String sTempValue = ""+pDataTable.getColumnValues(nIndex).get(pDataTable.getColumnValues(nIndex).size()-1);
    			// String sddValue = ""+pDataTable.getColumnValues(nIndex).get(0);
    			 System.out.println(sTempKey+"="+sTempValue);
    			// prop.setProperty(sKey, sValue.trim());
    		
             }
    		 System.out.println("-----------------Key mapper Ended--------------------------------------------------");
    		 
		 }
		 return pDataTable;
	}
	private static void createMultipleKey(String key,String propKeyAsValue,DataTable oDataTable, DataTable pDataTable, int count)
	{
		String keyBeforeMap=null, keyAfterMap=null;
		String value=null;	
		 String[] keyArray=propKeyAsValue.split("\\.");
		 for(int index=1;index<=count;index++)
		 {
			 
			 
			 if(keyArray.length==3)
			 {
			 keyBeforeMap=keyArray[0]+"."+keyArray[1]+index+"."+keyArray[2];
			 keyAfterMap=key+index;
			 }
			 else
			 {
				 keyBeforeMap=StringConstants.EMPTY;
				 keyAfterMap=StringConstants.EMPTY;
			 }
			 if(oDataTable.isColumnPresent(keyBeforeMap))
			 {
					 
				 value=(String) oDataTable.getColumnValues(keyBeforeMap).get(0);
				if(pDataTable.isColumnPresent(keyAfterMap))
				 {
					pDataTable.appendColumnValue(keyAfterMap, value);
				 
				 }
				 else
				 {
					 pDataTable.addColumn(keyAfterMap, value);
				 }
			 }
		 
		 }
	}
	
	
}
