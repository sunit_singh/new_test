package com.sciformix.commons.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.ParseException;

import com.sciformix.commons.SciConstants.StringConstants;

public class DataUtils
{
	private DataUtils()
	{
		//Nothing to do
	}
	
	public static boolean hasChanged(String p_sOldValue, String p_sNewValue)
	{
		if (p_sOldValue == null && p_sNewValue == null)
		{
			//Both are null - hence, no change
			return false;
		}
		else if (p_sOldValue == null || p_sNewValue == null)
		{
			//At least one is null and one is not - hence, something has changed
			return true;
		}
		else
		{
			//Both are not null - check for equality
			return (!p_sOldValue.equals(p_sNewValue));
		}
	}
	public static boolean hasChanged(int p_nOldValue, int p_nNewValue)
	{
		return (p_nOldValue != p_nNewValue);
	}
	
	public static String formatForJson(Object p_oValue)
	{
		if (p_oValue != null)
		{
			//TODO:Special handling required for datatypes like Date, etc.
			return p_oValue.toString();
		}
		else
		{
			return "";
		}
	}
	
	public static Set<String> extractUniqueValues(List<String> p_sListValues)
	{
		Set<String> setUniqueValues = null;
		
		setUniqueValues = new java.util.HashSet<String>();
		for (String sValue : p_sListValues)
		{
			setUniqueValues.add(sValue);
		}
		
		return setUniqueValues;
	}
	
	public static String extractUniqueStringValue(List<String> p_sListValues)
	{
		String sUniqueValue = null;
		
		for (String sValue : p_sListValues)
		{
			if (sUniqueValue == null)
			{
				sUniqueValue = sValue;
			}
			else if (sUniqueValue.equals(sValue))
			{
				continue;
			}
			else
			{
				return null;
			}
		}
		
		return sUniqueValue;
	}
	
	public static Object extractUniqueValue(List<Object> p_oListValues)
	{
		Object sUniqueValue = null;
		
		for (Object sValue : p_oListValues)
		{
			if (sUniqueValue == null)
			{
				sUniqueValue = sValue;
			}
			else if (sUniqueValue.equals(sValue))
			{
				continue;
			}
			else
			{
				return null;
			}
		}
		
		return sUniqueValue;
	}
	
    public static List<Float> parseFloatList(String option) throws ParseException
    {
        String[] f = option.split(",");
        List<Float> listValues = new ArrayList<Float>();
        try
        {
            for (int i = 0; i < f.length; i++) {
                listValues.add(Float.parseFloat(f[i]));
            }
            return listValues;
        }
        catch (NumberFormatException e)
        {
            throw new ParseException("Wrong number syntax");
        }
    }

	//TODO: Make this 4000
	private static final int DB_MAX_COL_SIZE = 3500;
	
	public static String[] splitForDb(String p_sLargeValue)
	{
		String[] sarrValues = null;
		int nNumberOfFragments = 0;
		
		if (p_sLargeValue == null || p_sLargeValue.length() == 0)
		{
			sarrValues = new String[0];
		}
		else
		{
			//DEV-NOTE: This is INTEGER Modulus and INTEGER Division in use here
			if ((p_sLargeValue.length() % DB_MAX_COL_SIZE) == 0)
			{
				nNumberOfFragments = (p_sLargeValue.length() / DB_MAX_COL_SIZE);
			}
			else
			{
				nNumberOfFragments = (p_sLargeValue.length() / DB_MAX_COL_SIZE) + 1;
			}
			sarrValues = new String[nNumberOfFragments];
			for (int nFragmentIndex = 0; nFragmentIndex < nNumberOfFragments; nFragmentIndex++)
			{
				if ((DB_MAX_COL_SIZE*(nFragmentIndex+1)) < p_sLargeValue.length())
				{
					sarrValues[nFragmentIndex] = p_sLargeValue.substring((DB_MAX_COL_SIZE*nFragmentIndex), (DB_MAX_COL_SIZE*(nFragmentIndex+1)) );
				}
				else
				{
					sarrValues[nFragmentIndex] = p_sLargeValue.substring((DB_MAX_COL_SIZE*nFragmentIndex), p_sLargeValue.length() );
				}
			}
		}
		
		return sarrValues;
	}
	public static String combineSplitFragments(String... parts)
	{
		String sCombinedString = null;
		
		sCombinedString = StringConstants.EMPTY;
		for (String part : parts)
		{
			if (part != null && part.length() > 0)
			{
				sCombinedString += part;
			}
		}
		
		return sCombinedString;
	}
}
