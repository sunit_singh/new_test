/**
 * 
 */
package com.sciformix.commons.utils;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;

/**
 * @author sshetty1
 *
 */
public class CryptoUtils 
{
	private static final Logger log = LoggerFactory.getLogger(CryptoUtils.class);
	
	private static final String CIPHER = "AES/CBC/PKCS5Padding";
	
	private static final String ENCRYPTION_ALGO = "AES";
	
	private static final String CHARSET = "UTF-8";
	
	public static String encryptToString(byte[] p_bBytesToBeEncrypted, String p_sEncryptedKey) throws SciException 
	{ 
		//return Base64.getEncoder().encodeToString(encrypt(p_bBytesToBeEncrypted, p_sEncryptedKey)); 
		return Base64.encodeBase64String(encrypt(p_bBytesToBeEncrypted, p_sEncryptedKey));
	}
	
	public static byte[] encrypt(byte[] p_bBytesToBeEncrypted, String p_sKey) throws SciException 
	{
		try 
		{
			return encrypt(p_bBytesToBeEncrypted, p_sKey.getBytes(CHARSET));
		}
		catch (Exception excep) 
		{
			log.error("Error in encrypt", excep);
			throw new SciException("Error in encrypt", excep);
		}
	}
	
	public static byte[] encrypt(byte[] p_bBytesToBeEncrypted, byte[] p_barrEncryptedKey) throws SciException 
	{
		SecureRandom oSecureRandom = null;
	    IvParameterSpec oIvParameterSpec = null;
	    SecretKeySpec oSecretKeySpec = null;
	    Cipher oCipher = null;
	    
	    byte[] barrEncryptedStringData = null; 
	    byte[] barrIVBytes = null;
	    byte[] barrTempIVBytes = null; 
	    byte[] barrCombinedData = null; 
	    
	    try
	    {
	    	//Generate Random IV
		    oSecureRandom = new SecureRandom(); 
		    barrIVBytes = new byte[16]; 
		    oSecureRandom.nextBytes(barrIVBytes); 
		    
		    oIvParameterSpec = new IvParameterSpec(barrIVBytes); 
		    oSecretKeySpec = new SecretKeySpec(p_barrEncryptedKey, ENCRYPTION_ALGO);
	    	oCipher = Cipher.getInstance(CIPHER);
	    	oCipher.init(Cipher.ENCRYPT_MODE, oSecretKeySpec, oIvParameterSpec);
	    	barrEncryptedStringData = oCipher.doFinal(p_bBytesToBeEncrypted);
	    
	    	//Prepending the IV array to the encrypted array. This is being done to retrieve the IV during decryption 
		    barrTempIVBytes = oIvParameterSpec.getIV(); 
		    barrCombinedData = new byte[barrTempIVBytes.length + barrEncryptedStringData.length]; 
		 
		    System.arraycopy(barrTempIVBytes, 0, barrCombinedData, 0, barrTempIVBytes.length); 
		    System.arraycopy(barrEncryptedStringData, 0, barrCombinedData, barrTempIVBytes.length, barrEncryptedStringData.length); 
		}
	    catch(Exception excep)
	    {
	    	log.error("SciCrypto.encrypt(): Exception during encryption", excep);
		}
	   
	    return barrCombinedData; 
	}
	
	public static byte[] encrypt(String p_sStringToBeEncrypted, byte[] p_barrEncryptedKey) throws SciException 
	{
		try 
		{
			return encrypt(p_sStringToBeEncrypted.getBytes(CHARSET), p_barrEncryptedKey);
		}
		catch (Exception excep) 
		{
			log.error("Error in encrypt", excep);
			throw new SciException("Error in encrypt", excep);
		}
	}
	
	public static String encryptToString(String p_sStringToBeEncrypted, byte[] p_barrKey) throws SciException 
	{ 
		//return Base64.getEncoder().encodeToString(encrypt(p_sStringToBeEncrypted, p_barrKey));
		return Base64.encodeBase64String(encrypt(p_sStringToBeEncrypted, p_barrKey));
	}
	
	public static String decryptToString(String p_sStringToDecrypt, byte[] p_barrKey) throws SciException 
	{ 
		return new String(decrypt(p_sStringToDecrypt, p_barrKey)); 
	}
	
	public static byte[] decrypt(String p_sStringToDecrypt, String p_sKey) throws SciException 
	{ 
		try 
		{
			return decrypt(p_sStringToDecrypt, p_sKey.getBytes(CHARSET));
		}
		catch (Exception excep) 
		{
			log.error("Error in decrypt", excep);
			throw new SciException("Error in decrypt", excep);
		}
	} 
	
	public static byte[] decrypt(String p_sStringToDecrypt, byte[] p_barrKey) throws SciException 
	{ 
		SecretKeySpec oSecretKeySpec = null;
		Cipher oCipher = null;
		
	    byte[] barrDecryptedData = null; 
	    byte[] barrInputData = Base64.decodeBase64(p_sStringToDecrypt); 
	    byte[] barrIVBytes = new byte[16]; 
	    byte[] barrActualCipherText = new byte[barrInputData.length - barrIVBytes.length]; 
	 
	    try
	    {
	    	//Separating the IV and the cipher 
		    System.arraycopy(barrInputData, 0, barrIVBytes, 0, 16); 
		    System.arraycopy(barrInputData, 16, barrActualCipherText, 0, barrActualCipherText.length); 
		 
		    oSecretKeySpec = new SecretKeySpec(p_barrKey, ENCRYPTION_ALGO);
	 		oCipher = Cipher.getInstance(CIPHER);
	 		oCipher.init(Cipher.DECRYPT_MODE, oSecretKeySpec, new IvParameterSpec(barrIVBytes));
	 		barrDecryptedData = oCipher.doFinal(barrActualCipherText);
	 	}
	    catch(Exception excep)
	    {
	    	log.error("SciCrypto.decrypt(): Exception during decryption", excep);
		}
	    
	    return barrDecryptedData;
	} 
	
	public static byte[] decrypt(byte[] barrToBeDecrypted, byte[] p_barrKey) throws SciException 
	{ 
		SecretKeySpec oSecretKeySpec = null;
		Cipher oCipher = null;
		
	    byte[] barrDecryptedData = null; 
	    byte[] barrInputData = Base64.decodeBase64(barrToBeDecrypted); 
	    byte[] barrIVBytes = new byte[16]; 
	    byte[] barrActualCipherText = new byte[barrInputData.length - barrIVBytes.length]; 
	 
	    try
	    {
	    	//Separating the IV and the cipher 
		    System.arraycopy(barrInputData, 0, barrIVBytes, 0, 16); 
		    System.arraycopy(barrInputData, 16, barrActualCipherText, 0, barrActualCipherText.length); 
		 
		    oSecretKeySpec = new SecretKeySpec(p_barrKey, ENCRYPTION_ALGO);
	 		oCipher = Cipher.getInstance(CIPHER);
	 		oCipher.init(Cipher.DECRYPT_MODE, oSecretKeySpec, new IvParameterSpec(barrIVBytes));
	 		barrDecryptedData = oCipher.doFinal(barrActualCipherText);
	 	}
	    catch(Exception excep)
	    {
	    	log.error("SciCrypto.decrypt(): Exception during decryption", excep);
		}
	    
	    return barrDecryptedData;
	}
	
	public static byte[] generateKey() throws SciException
	{
		byte[] barrKey = null;
		
		try
		{
			KeyGenerator oKeyGenerator  = KeyGenerator.getInstance(ENCRYPTION_ALGO);
			oKeyGenerator.init(128);   
			barrKey = oKeyGenerator.generateKey().getEncoded();
		}
		catch(Exception excep)
		{
			log.error("Error generating key", excep);
			throw new SciException("Error generating key", excep);
		}
		return barrKey;
	}
	
	/*Public Key Encryption Methods: Added By Jason : START*/

	

	public static String getHash(String textToHash) {
		String generatedHash = DigestUtils.sha512Hex(textToHash);
		return generatedHash;
	}

	/*Public Key Encryption Methods: Added By Jason : END*/
}
