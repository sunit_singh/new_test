package com.sciformix.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;

public class OfficeUtils
{

	
	private static final Logger log = LoggerFactory.getLogger(OfficeUtils.class);
	
	private static final int EMPTY_RECORD_SIZE = 1;
	
	
	public static boolean checkIfWorkbookFile(String filePath) throws SciException
	{
		FileInputStream oInputFileStream = null;
		try
		{
			oInputFileStream = new FileInputStream(filePath);
			
			Workbook workbook = WorkbookFactory.create(oInputFileStream);
			if (workbook.getNumberOfSheets() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch (FileNotFoundException e)
		{
			log.error("checkIfWorkbookFile: Error locating file " + filePath, e);
		}
		catch (EncryptedDocumentException e)
		{
			log.error("checkIfWorkbookFile: Error opening encrypted file", e);
		}
		catch (InvalidFormatException e)
		{
			log.error("checkIfWorkbookFile: Invalid file format", e);
		}
		catch (IOException e)
		{
			log.error("checkIfWorkbookFile: Error opening file", e);
		}
		catch (IllegalArgumentException e)
		{
			log.error("checkIfWorkbookFile: Error opening file", e);
		}
		finally
		{
			if (oInputFileStream != null)
			{
				try
				{
					oInputFileStream.close();
				}
				 catch (IOException e)
				{
					 log.error("checkIfWorkbookFile: Error closing file", e);
				}
			}
		}
		return false;
	}
	
	public static boolean checkIfCsvFile(String filePath) throws SciException
	{
		if (!filePath.toLowerCase().endsWith("csv"))
		{
			return false;
		}
		
		FileInputStream oInputFileStream = null;
		try
		{
			oInputFileStream = new FileInputStream(filePath);
			
			return true;
		}
		catch (FileNotFoundException excep)
		{
			log.error("Error checking file type - {}", filePath, excep);
			throw new SciException("Error checking file type", excep);
		}
		finally
		{
			if (oInputFileStream != null)
			{
				try
				{
					oInputFileStream.close();
				}
				catch (IOException excep)
				{
					//Log and continue
					log.error("checkIfWorkbookFile: Error closing file", excep);
				}
			}
		}
	}
	
	/*public static WorkbookWorkSheetWrapper parseWorkbook(String p_sStagedFileName, boolean p_bIsHeaderPresent, int p_nSheetIndex) throws SciException
	{
		WorkbookWorkSheetWrapper oWorksheetWrapper = null;
		int nNumberOfColumns = 0;
		int nNumberOfDataRows = 0;
		Workbook oWorkbook = null;
		Sheet oSheet = null;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;
		
		List<Object> listTableRow = null;
		List<Object> listHeaderRow = null;
		List<List<Object>> listDataTable = null;
		File oWorkbookFile = null;
		try
		{
			oWorkbookFile = new File(p_sStagedFileName);
			oWorkbook = WorkbookFactory.create(oWorkbookFile);
			
			oSheet = oWorkbook.getSheetAt(p_nSheetIndex);
			
			listDataTable = new ArrayList<List<Object>>();
			
			oIteratorWorkbookRows = oSheet.rowIterator();
			if (p_bIsHeaderPresent)
			{
				if (oIteratorWorkbookRows.hasNext())
				{
					oSingleWorkbookRow = oIteratorWorkbookRows.next();
					listHeaderRow = new ArrayList<Object>();
					
					if(oSingleWorkbookRow!=null)
					{
						// Iterate through the cells
						for (int nRowCellIndex = 0; nRowCellIndex < oSingleWorkbookRow.getLastCellNum(); nRowCellIndex++)
						{
							oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
							listHeaderRow.add(getWorkbookCellContent(oSingleWorkbookCell));
							
							nNumberOfColumns++;
						}
					}
				}
			}
			else
			{
				// throw new Exception("Header row not present use case not supported as yet");
				//listErrors.add(new SciError(p_sErrorKey));
				//Ignore and continue
			}
			
			//NOTE: Iterator might have moved one workbook row if an header was expected and found 
			while (oIteratorWorkbookRows.hasNext())
			{
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				listTableRow = new ArrayList<Object>(nNumberOfColumns);
				
				// Iterate through the cells.
				if(oSingleWorkbookRow!=null)
				{
					// Iterate through the cells.
					for (int nRowCellIndex = 0; nRowCellIndex < nNumberOfColumns; nRowCellIndex++)
					{
						oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
						listTableRow.add(getWorkbookCellContent(oSingleWorkbookCell));
					}
					listDataTable.add(listTableRow);
					nNumberOfDataRows++;
				}
			}
			
			oWorksheetWrapper = new WorkbookWorkSheetWrapper(oSheet.getSheetName(), p_nSheetIndex, p_bIsHeaderPresent, nNumberOfColumns, nNumberOfDataRows, 
					listHeaderRow, listDataTable);
			
		}
		catch (FileNotFoundException fnfExcep)
		{
			log.error("Error parsing file as file not found", fnfExcep);
			throw new SciException("Error parsing file as file not found", fnfExcep);
		}
		catch (InvalidFormatException invExcep)
		{
			log.error("Error parsing file as file format incorrect", invExcep);
			throw new SciException("Error parsing file as file format incorrect", invExcep);
		}
		catch (IOException ioExcep)
		{
			log.error("Error parsing file as file", ioExcep);
			throw new SciException("Error parsing file", ioExcep);
		}
		finally
		{
			try
				{
					oWorkbook.close();
				}
				catch (Exception ioExcep)
				{
					//Ignore and continue
					log.error("Error closing workbook file", ioExcep);
				}
		}
		return oWorksheetWrapper;
	}*/
	/**
	 * This method is overloaded to pass the input date format
	 * and the output date format.
	 * @param p_sStagedFileName
	 * @param p_bIsHeaderPresent
	 * @param p_nSheetIndex
	 * @param p_inputdateformatter
	 * @param p_outputdateformatter
	 * @return
	 * @throws SciException
	 */
	public static WorkbookWorkSheetWrapper parseWorkbook(
		String p_sStagedFileName, boolean p_bIsHeaderPresent, int p_nSheetIndex, String p_inputdateformatter, String p_outputdateformatter) throws SciException
	{
		WorkbookWorkSheetWrapper oWorksheetWrapper = null;
		int nNumberOfColumns = 0;
		int nNumberOfDataRows = 0;
		Workbook oWorkbook = null;
		Sheet oSheet = null;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;
		
		List<Object> listTableRow = null;
		List<Object> listHeaderRow = null;
		List<List<Object>> listDataTable = null;
		File oWorkbookFile = null;
		try
		{
			oWorkbookFile = new File(p_sStagedFileName);
			oWorkbook = WorkbookFactory.create(oWorkbookFile);
			
			oSheet = oWorkbook.getSheetAt(p_nSheetIndex);
			
			listDataTable = new ArrayList<List<Object>>();
			
			oIteratorWorkbookRows = oSheet.rowIterator();
			if (p_bIsHeaderPresent)
			{
				if (oIteratorWorkbookRows.hasNext())
				{
					oSingleWorkbookRow = oIteratorWorkbookRows.next();
					listHeaderRow = new ArrayList<Object>();
					
					if(oSingleWorkbookRow!=null)
					{
						// Iterate through the cells
						for (int nRowCellIndex = 0; nRowCellIndex < oSingleWorkbookRow.getLastCellNum(); nRowCellIndex++)
						{
							oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
							listHeaderRow.add(getWorkbookCellContent(oSingleWorkbookCell, p_inputdateformatter, p_outputdateformatter));
							
							nNumberOfColumns++;
						}
					}
				}
			}
			else
			{
				// throw new Exception("Header row not present use case not supported as yet");
				//listErrors.add(new SciError(p_sErrorKey));
				//Ignore and continue
			}
			
			//NOTE: Iterator might have moved one workbook row if an header was expected and found 
			while (oIteratorWorkbookRows.hasNext())
			{
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				listTableRow = new ArrayList<Object>(nNumberOfColumns);
				
				// Iterate through the cells.
				if(oSingleWorkbookRow!=null)
				{
					// Iterate through the cells.
					for (int nRowCellIndex = 0; nRowCellIndex < nNumberOfColumns; nRowCellIndex++)
					{
						oSingleWorkbookCell = oSingleWorkbookRow.getCell(nRowCellIndex);
						listTableRow.add(getWorkbookCellContent(oSingleWorkbookCell, p_inputdateformatter, p_outputdateformatter));
					}
					listDataTable.add(listTableRow);
					nNumberOfDataRows++;
				}
			}
			
			oWorksheetWrapper = new WorkbookWorkSheetWrapper(oSheet.getSheetName(), p_nSheetIndex, p_bIsHeaderPresent, nNumberOfColumns, nNumberOfDataRows, 
					listHeaderRow, listDataTable);
			
		}
		catch (FileNotFoundException fnfExcep)
		{
			log.error("Error parsing file as file not found", fnfExcep);
			throw new SciException("Error parsing file as file not found", fnfExcep);
		}
		catch (InvalidFormatException invExcep)
		{
			log.error("Error parsing file as file format incorrect", invExcep);
			throw new SciException("Error parsing file as file format incorrect", invExcep);
		}
		catch (IOException ioExcep)
		{
			log.error("Error parsing file as file", ioExcep);
			throw new SciException("Error parsing file", ioExcep);
		}
		finally
		{
			try
				{
					oWorkbook.close();
				}
				catch (Exception ioExcep)
				{
					//Ignore and continue
					log.error("Error closing workbook file", ioExcep);
				}
		}
		return oWorksheetWrapper;
	}
	
	public static void parseCsvFile(String p_sStagedFileName, DataTable p_oDataTable, boolean p_bIsHeaderPresent) throws SciException
	{
		File file = null;
		Object oColumnValue = null;
		Map<String, Object> m_mapHeaderContent = new HashMap<>();
		String sTemp = null;
		CSVParser oCsvParser = null;
		if (!p_bIsHeaderPresent)
		{
			log.error("Error parsing CSV file - Header row not present use case not supported as yet");
			throw new SciException("Error parsing CSV file - Header row not present use case not supported as yet");
		}
		
		file = new File(p_sStagedFileName);
		try
		{
			 oCsvParser = CSVParser.parse(file, Charset.forName("UTF8"),  CSVFormat.RFC4180);
			if (p_oDataTable == null)
			{
				p_oDataTable = new DataTable();
			}
			int nRecordCounter = 0;
			List<String> listHeaderRow = null;
			
			listHeaderRow = new ArrayList<String>();
			
			for (CSVRecord oRecord : oCsvParser)
			{
				if(oRecord.size()!=EMPTY_RECORD_SIZE){
					if (nRecordCounter == 0)
					{
						for (int nColIndex = 0; nColIndex < oRecord.size(); nColIndex++)
						{
							listHeaderRow.add(oRecord.get(nColIndex));
						}
					}
					else
					{
						for (int nColIndex = 0; nColIndex < oRecord.size(); nColIndex++)
						{
							sTemp = listHeaderRow.get(nColIndex);
							oColumnValue = new Object();
							oColumnValue = oRecord.get(nColIndex).trim();
							
							if(m_mapHeaderContent.containsKey(sTemp))
							{
								p_oDataTable.appendColumnValue(sTemp, oColumnValue);
							}else
							{
								p_oDataTable.addColumn(sTemp, oColumnValue);
								m_mapHeaderContent.put(sTemp,  oColumnValue);
							}
						//	p_oDataTable.addColumn(listHeaderRow.get(nColIndex), oRecord.get(nColIndex));//TODO:Handle different row sizes
						}
					}
					nRecordCounter++;
				}
			}
		}
		catch (IOException ioExcep)
		{
			log.error("Error parsing CSV file ", ioExcep);
			throw new SciException("Error parsing CSV file", ioExcep);
		}
		finally
		{
			try
			{
				oCsvParser.close();
			}
			catch (Exception ioExcep)
			{
				//Ignore and continue
				log.error("Error closing csv file", ioExcep);
			}
		}
	}
	
//	public static boolean validateMandatoryColumns(WorkbookWorkSheetWrapper oWorksheetWrapper, String[] p_sarrMandatoryColumnHeaders, List<SciError> listErrors, String p_sErrorKey)
//	{
//		//TODO:Compute bMandatoryColumnMissing
//		boolean bMandatoryColumnMissing = false;
//		List<Object> m_listHeaderRowContent = null;
//		m_listHeaderRowContent = oWorksheetWrapper.getHeaderRowContent();
//		
//		// this is for if sheet is blank 
//		if(m_listHeaderRowContent != null)
//		{
//		
//			for (int i = 0; i < p_sarrMandatoryColumnHeaders.length; i++) {
//				if (!m_listHeaderRowContent.contains(p_sarrMandatoryColumnHeaders[i])) {
//					bMandatoryColumnMissing = true;
//				}
//			}
//
//			if (bMandatoryColumnMissing) {
//				// ....
//				if (listErrors != null) {
//					listErrors.add(new SciError(p_sErrorKey));
//				}
//				return true;
//			}
//		}
//		else
//		{
//			if (listErrors != null) {
//				listErrors.add(new SciError(p_sErrorKey));
//			}
//			return true;
//		}
//		//else Do nothing
//		return false;
//	}
	
	public static boolean isMinDataRowCountBreached(DataTable p_oDataTable, int p_sMinExpectedDataRowCount)
	{
		if (p_oDataTable.rowCount() < p_sMinExpectedDataRowCount)
		{
			return true;
		}
		return false;
	}
	
	public static boolean isMaxDataRowCountBreached(DataTable p_oDataTable, int p_sMaxExpectedDataRowCount)
	{
		if ((p_sMaxExpectedDataRowCount > 0) && (p_oDataTable.rowCount() > p_sMaxExpectedDataRowCount))
		{
			return true;
		}
		return false;
	}
	
	public static DataTable convertToDatatable(WorkbookWorkSheetWrapper p_oWorksheetWrapper, String p_sColumnsToCollapse)
	{
		DataTable oDataTable = null;

		oDataTable = new DataTable();
		
		p_oWorksheetWrapper.appendDataIntoDataTable(oDataTable, false, p_sColumnsToCollapse);
		
		return oDataTable;
	}

	
	@SuppressWarnings("deprecation")
	/*private static Object getWorkbookCellContent(Cell oSingleWorkbookCell)
	{
		boolean bCheckIfCellContainsDateValue = false;
		DataFormatter df = new DataFormatter(true);
	
		if (oSingleWorkbookCell!=null)
		{
			switch (oSingleWorkbookCell.getCellType()) 
			{
				case Cell.CELL_TYPE_BLANK:
					return oSingleWorkbookCell.getStringCellValue();
				case Cell.CELL_TYPE_STRING:
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_NUMERIC:
					bCheckIfCellContainsDateValue = checkIfCellContainsDateValue(df.formatCellValue(oSingleWorkbookCell));
					if(bCheckIfCellContainsDateValue)
					{
						System.out.println(df.formatCellValue(oSingleWorkbookCell));
						return getActualFormattedDateValue(df.formatCellValue(oSingleWorkbookCell));
					}
					else
					{
						oSingleWorkbookCell.setCellType(Cell.CELL_TYPE_STRING);
					}
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_BOOLEAN:
					return oSingleWorkbookCell.getBooleanCellValue();
				case Cell.CELL_TYPE_FORMULA:
					return oSingleWorkbookCell.getCellFormula();
				case Cell.CELL_TYPE_ERROR:
					return null;
				default:
					return new String("");
			}
		}
		else
		{
			return null;
		}
	}*/
	/**
	 * This method is overloaded to pass the input date format
	 * and the output date format.
	 * @param oSingleWorkbookCell
	 * @param p_inputdateformatter
	 * @param p_outputdateformatter
	 * @return
	 */
	private static Object getWorkbookCellContent(Cell oSingleWorkbookCell, String p_inputdateformatter, String p_outputdateformatter)
	{
		boolean bCheckIfCellContainsDateValue = false;
	
		if (oSingleWorkbookCell!=null)
		{
			switch (oSingleWorkbookCell.getCellType()) 
			{
				case Cell.CELL_TYPE_BLANK:
					return oSingleWorkbookCell.getStringCellValue();
				case Cell.CELL_TYPE_STRING:
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_NUMERIC:
					if (DateUtil.isCellDateFormatted(oSingleWorkbookCell) && !StringUtils.isNullOrEmpty(p_inputdateformatter)) {
						bCheckIfCellContainsDateValue = checkIfCellContainsDateValue(oSingleWorkbookCell.toString(), p_inputdateformatter);
					}
					
					if(bCheckIfCellContainsDateValue)
					{
						return getActualFormattedDateValue(oSingleWorkbookCell.toString(), p_inputdateformatter, p_outputdateformatter);
					}
					else
					{
						oSingleWorkbookCell.setCellType(Cell.CELL_TYPE_STRING);
					}
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_BOOLEAN:
					return oSingleWorkbookCell.getBooleanCellValue();
				case Cell.CELL_TYPE_FORMULA:
					return oSingleWorkbookCell.getCellFormula();
				case Cell.CELL_TYPE_ERROR:
					return null;
				default:
					return new String("");
			}
		}
		else
		{
			return null;
		}
	}
	
	

	/*private  static String getActualFormattedDateValue(String p_sDateValue) 
	{
		String sIncommingDateFormat = null;
		String sFormattedDate = null;
		String sDateFormat = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		SimpleDateFormat formatter = null;
		
		if(p_sDateValue!=null)
		{
			sIncommingDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString());
			sDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT.toString());
			//TODO: Check this
			try
			{
				incomingformatter = new SimpleDateFormat(sIncommingDateFormat);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
			}
			catch(ParseException parseExcep)
			{
				log.error("Date formatting failed for: Date/Format - " + p_sDateValue +  "/" + sIncommingDateFormat, parseExcep);
				return "";
			}
			
			formatter = new SimpleDateFormat(sDateFormat);
			sFormattedDate = formatter.format(oFormattedDate);
		}
		else
		{
			return "";
		}
		return sFormattedDate;
	}*/
	/**
	 * This method is overloaded to pass the input date format
	 * and the output date format.
	 * @param p_sDateValue
	 * @param p_inputdateformatter
	 * @param p_outputdateformatter
	 * @return
	 */
	private  static String getActualFormattedDateValue(String p_sDateValue, String p_inputdateformatter, String p_outputdateformatter) 
	{
		String sIncommingDateFormat = null;
		String sFormattedDate = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		SimpleDateFormat formatter = null;
		
		if(p_sDateValue!=null)
		{
			//TODO: Check this
			try
			{
				incomingformatter = new SimpleDateFormat(p_inputdateformatter);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
			}
			catch(ParseException parseExcep)
			{
				log.error("Date formatting failed for: Date/Format - " + p_sDateValue +  "/" + sIncommingDateFormat, parseExcep);
				return "";
			}
			
			formatter = new SimpleDateFormat(p_outputdateformatter);
			sFormattedDate = formatter.format(oFormattedDate);
		}
		else
		{
			return "";
		}
		return sFormattedDate;
	}
	
	private static boolean checkIfCellContainsDateValue(String p_sColumnValue, String p_inputdateformatter) 
	{
		
		try
		{
			//TODO: Check this
			SimpleDateFormat formatter = new SimpleDateFormat(p_inputdateformatter);
			formatter.parse(p_sColumnValue);
		}
		catch(Exception excep)
		{
			return false;
		}
		return true;
	}
	
	/*private static boolean checkIfCellContainsDateValue(String p_sColumnValue) 
	{
		String sIncomingDateFormat = null;
		
		try
		{
			sIncomingDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString());
			
			@SuppressWarnings("deprecation")
			//TODO: Check this
			SimpleDateFormat formatter = new SimpleDateFormat(sIncomingDateFormat);
			formatter.parse(p_sColumnValue);
		}
		catch(Exception excep)
		{
			return false;
		}
		return true;
	}*/
	
	public static void appendDataIntoDataTable(DataTable p_oDataTableMain, DataTable p_oDataTableNew, boolean p_bCollapseMultivaluedCells, String p_sColumnsToCollapse)
	{
		List<String> listColumnNames = null;
		String[] arrColumnNames = null;
		List<Object> oListSingleCol = null;
		String sHeaderName = null;
		Object oElementValue = null;
		boolean bColumnPresentInDataTable = false;
		boolean bNewColumnValueAlreadyPresent = false;
		List<Object> listDataTableValuesForColumn = null;
		
		arrColumnNames = p_sColumnsToCollapse == null ? new String[0] : p_sColumnsToCollapse.split(",");
		listColumnNames = new ArrayList<String>(Arrays.asList(arrColumnNames));
		
		for (int nColumnIndex = 0; nColumnIndex < p_oDataTableNew.colCount(); nColumnIndex++) 
		{
			sHeaderName = p_oDataTableNew.getColumnHeader(nColumnIndex);
			oListSingleCol = p_oDataTableNew.getColumnValues(nColumnIndex);
			if (oListSingleCol == null || oListSingleCol.size() == 0)
			{
				log.debug("Working with col# {} NOT PRESENT in datatable; adding it with NULL value", nColumnIndex);
				p_oDataTableMain.addColumn(sHeaderName, null);
			} 
			for (int nRowIndex = 0; nRowIndex < oListSingleCol.size(); nRowIndex++)
			{
				oElementValue = oListSingleCol.get(nRowIndex);
				
				bColumnPresentInDataTable = p_oDataTableMain.isColumnPresent(sHeaderName);
				if (bColumnPresentInDataTable)
				{
					if (p_bCollapseMultivaluedCells)
					{
						bNewColumnValueAlreadyPresent = false;
						listDataTableValuesForColumn = p_oDataTableMain.getColumnValues(sHeaderName);
						
						for (Object oDataTableColumnValue : listDataTableValuesForColumn)
						{
							if (oDataTableColumnValue != null && oDataTableColumnValue.equals(oElementValue) && !listColumnNames.contains(sHeaderName))
							{
								//Do not add this
								bNewColumnValueAlreadyPresent = true;
							}
							else if (oDataTableColumnValue == null && oElementValue == null)
							{
								//Do not add this
								bNewColumnValueAlreadyPresent = true;
							}
						}
						
						if (!bNewColumnValueAlreadyPresent)
						{
							if((oElementValue==null || String.valueOf(oElementValue).equals("")) && listColumnNames.contains(sHeaderName))
							{
								p_oDataTableMain.appendColumnValue(sHeaderName, "");
							}else if(oElementValue!=null && !String.valueOf(oElementValue).equals(""))
							{	
								p_oDataTableMain.appendColumnValue(sHeaderName, oElementValue);
							}
							//p_oDataTable.dumpContents();
						}
						else
						{
							//Skip and continue
						}
					}
					else
					{
						if((oElementValue==null || String.valueOf(oElementValue).equals("")) && listColumnNames.contains(sHeaderName))
						{
							p_oDataTableMain.appendColumnValue(sHeaderName, "");
						}else if(oElementValue!=null && !String.valueOf(oElementValue).equals(""))
						{
							p_oDataTableMain.appendColumnValue(sHeaderName, oElementValue);
						}
					}
				}
				else
				{
					//Column itself is new to the DataTable
					if((oElementValue==null || String.valueOf(oElementValue).equals("")) && listColumnNames.contains(sHeaderName))
					{
						p_oDataTableMain.addColumn(sHeaderName, "");
					}else if(oElementValue==null || String.valueOf(oElementValue).equals(""))
					{
						p_oDataTableMain.addColumn(sHeaderName, "");
					}else{
						p_oDataTableMain.addColumn(sHeaderName, oElementValue);
					}
					//p_oDataTable.dumpContents();
				}
			}
		}
	}
}
