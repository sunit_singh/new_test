package com.sciformix.commons.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sciformix.commons.SciConstants;

public class ValidationUtils {
	
	private static String sStricterRegex = SciConstants.RegExConstants.OBJECT_NAME;
	
	public static boolean validate(String fieldValue, String type)
	{
		boolean validationFlag = true;
		
		if(StringUtils.isNullOrEmpty(type)) 
		{	
			if(!validateField(fieldValue, sStricterRegex))
			{
				validationFlag = false;
			}
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_EMAIL)) //Email Validation
		{	
			if(!validateField(fieldValue, SciConstants.RegExConstants.EMAIL))
			{
				validationFlag = false;
			}
		}
		else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_FTL)) //FTL tags
		{
			if(validateScriptTag(fieldValue, SciConstants.RegExConstants.FTL))
			{
				validationFlag = false;
			}
		}
		else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_INTEGER)) //Number field
		{
			if(!validateField(fieldValue, SciConstants.RegExConstants.INTEGER))
			{
				validationFlag = false;
			}
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_NUMBER)) //data with special characters
		{
			if(!validateField(fieldValue, SciConstants.RegExConstants.NUMBER))
			{
				validationFlag = false;
			}
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_CONFIGSETTINGS)) //data with special characters
		{
			if(validateScriptTag(fieldValue, SciConstants.RegExConstants.CONFIGSETTINGS))
			{
				validationFlag = false;
			}
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_USERID)) //data with special characters
		{
			if(!validateField(fieldValue, SciConstants.RegExConstants.USERID))
			{
				validationFlag = false;
			}
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_PASSWORD)) //data with special characters
		{
			/*if(!validateField(fieldValue, SciConstants.RegExConstants.PASSWORD))
			{
				validationFlag = false;
				fieldNameList.add(sTemp);
				mapValidationErrorFieldDetails.put(prepareKey(contextId, sFieldName), oValidationFieldDetails);
			}*/
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_RULECRITERION)) //data with special characters
		{
			if(!validateField(fieldValue, SciConstants.RegExConstants.RULECRITERION))
			{
				validationFlag = false;
			}
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_CASEID)) //data with special characters
		{
			if(!validateField(fieldValue, SciConstants.RegExConstants.CASEID))
			{
				validationFlag = false;
			}
		}else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_CASEVERSION)) //data with special characters
		{
			if(!validateField(fieldValue, SciConstants.RegExConstants.CASEVERSION))
			{
				validationFlag = false;
			}
		}
		else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_DATE)) //data with special characters
		{
			if(!validateField(fieldValue.toUpperCase(),  SciConstants.RegExConstants.DATE))
			{
				validationFlag = false;
			}
		}
		else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_DESCRIPTION)) //data with special characters
		{
			if(validateScriptTag(fieldValue, SciConstants.RegExConstants.DESCRIPTION))
			{
				validationFlag = false;
			}
		}
		else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_RESTRICTEDDESCRIPTION)) //data with special characters
		{
			if(validateScriptTag(fieldValue, SciConstants.RegExConstants.RESTRICTEDDESCRIPTION))
			{
				validationFlag = false;
			}
		}
		else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_ADVANCE)) //data with special characters
		{
			/*if(!validateField(fieldValue, oValidationFieldDetails.getsRegEx()))
			{
				validationFlag = false;
			}*/
		}
		else if(type.equals(SciConstants.ValidationTypeConstants.TYPE_FILE)) //data with special characters
		{
			if(!validateField(fieldValue, SciConstants.RegExConstants.FILE))
			{
				validationFlag = false;
			}
		}
		
		return validationFlag;
	}

	public static boolean validateField(String p_sFieldValue, String p_sFieldRegex) 
	{
			Pattern	oPattern	=	Pattern.compile(p_sFieldRegex);
			Matcher	oMatcher	=	oPattern.matcher(p_sFieldValue);
			return oMatcher.matches();
	}
	
	private static boolean validateScriptTag(String fieldvalue, String regex)
	{
		Pattern	oPattern	=	Pattern.compile(regex);
		Matcher	oMatcher	=	oPattern.matcher(fieldvalue.toLowerCase());
		return oMatcher.find();
	}
}
