package com.sciformix.commons.utils;

import com.sciformix.commons.SciConstants;

public class ParsedDataTableWrapper
{
	private ParsedDataTable m_oParsedDataTable = null;
	
	public ParsedDataTableWrapper(ParsedDataTable p_oParsedDataTable)
	{
		m_oParsedDataTable = p_oParsedDataTable;
	}
	
	public ParsedDataTable getParsedDataTable()
	{
		return m_oParsedDataTable;
	}
	
	public Object getKeyValue(String p_sKey)
	{
		return m_oParsedDataTable.getColumnValues(p_sKey).get(0).displayValue();
	}
	
	public String getKeyValueAsString(String p_sKey)
	{
		if(m_oParsedDataTable.isColumnPresent(p_sKey) && m_oParsedDataTable.getColumnValues(p_sKey).get(0)!=null) {
			return m_oParsedDataTable.getColumnValues(p_sKey).get(0).displayValue().toString().trim();
		} else {
			return SciConstants.StringConstants.EMPTY;
		}
	}
	
	public boolean isKeyValueNullEmptyOrMatches(String p_sKey, String... literalStrings)
	{
		return (isKeyValueNullOrEmpty(p_sKey) || doesKeyValueMatch(p_sKey, literalStrings));
	}
	
	public boolean isKeyValueNullOrEmpty(String p_sKey)
	{
		Object obj = null;
		obj = getKeyValueAsString(p_sKey);
		
		if(obj != null && !StringUtils.isNullOrEmpty(obj.toString()))
		{
			return false;
		}
		return true;
	}
	
	public boolean doesKeyValueMatch(String p_sKey, String... literalStrings)
	{
		String sValue = null;
		sValue = getKeyValueAsString(p_sKey);
		
		for (int nCounter = 0; nCounter < literalStrings.length; nCounter++)
		{
			if (sValue.equalsIgnoreCase(literalStrings[nCounter]))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean doesValueMatch(String p_sValue, String... literalStrings)
	{
		
		for (int nCounter = 0; nCounter < literalStrings.length; nCounter++)
		{
			if (p_sValue.equalsIgnoreCase(literalStrings[nCounter]))
			{
				return true;
			}
		}
		return false;
	}
	
	
}
