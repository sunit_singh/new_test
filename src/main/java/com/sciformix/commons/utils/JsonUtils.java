package com.sciformix.commons.utils;

import java.util.HashMap;

import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.JSONObject;
import org.apache.wink.json4j.OrderedJSONObject;

public class JsonUtils
{
	
	public static String serialize(String key, String value)
	{
		return doubleQuote(key) + ":" + doubleQuote(value);
	}
	
	public static String serializePrefix(String key)
	{
		return doubleQuote(key) + ": {";
	}
	
	public static String serializeSuffix()
	{
		return "}";
	}
	
	public static String doubleQuote(String value)
	{
		return "\"" + (value!=null?value:"") + "\"";
	}
	
	public static void addAttribute(OrderedJSONObject jsonRow, String key, String value) throws JSONException
	{
		jsonRow.put(key, value);
	}
	
	public static String getJsonErrorString()
	{
		return doubleQuote("ERROR-GENERATING-JSON") + ":\"\""; 
	}
	
	@SuppressWarnings("unchecked")
	public static HashMap<String,String> parseJSONObject (String p_jsondata) throws JSONException
	{
		JSONObject jObject  = 	new JSONObject(p_jsondata);
		return jObject;
	}
}
