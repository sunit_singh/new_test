package com.sciformix.commons.utils;

import java.util.ArrayList;
import java.util.List;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciConstants.StringConstants;

public class StringUtils
{
	
	public static boolean isNullOrEmpty(String p_sString)
	{
		return (p_sString == null || p_sString.trim().length() == 0);
	}
	
	
	public static String[] split(String p_sValue, String p_sDelimiter)
	{
		return split(p_sValue, p_sDelimiter, false);
	}
	
	private static String[] split(String p_sValue, String p_sDelimiter, boolean p_bCombineConsecutiveDelimiters)
	{
		if (p_bCombineConsecutiveDelimiters)
		{
			return p_sValue.split(p_sDelimiter + "+");
		}
		else
		{
			return p_sValue.split(p_sDelimiter);
		}
	}
	
	public static String[] splitOnWhitespace(String p_sValue, boolean p_bCombineConsecutiveSpaces)
	{
		return split(p_sValue, StringConstants.SPACE, p_bCombineConsecutiveSpaces);
	}
	
	public static String[] splitOnComma(String p_sValue)
	{
		return split(p_sValue, StringConstants.COMMA);
	}
	
	public static boolean containsSpace(String p_sString)
	{
		return (p_sString.indexOf(StringConstants.SPACE) != -1);
	}
	
	public static String rightPad(String p_sString, int p_nSize, char p_cPaddingChar)
	{
		return org.apache.commons.lang3.StringUtils.rightPad(p_sString, p_nSize, p_cPaddingChar);
	}

	public static String leftPad(String p_sString, int p_nSize, char p_cPaddingChar)
	{
		return org.apache.commons.lang3.StringUtils.leftPad(p_sString, p_nSize, p_cPaddingChar);
	}

	public static String leftPad(int p_nNumber, int p_nSize)
	{
		return org.apache.commons.lang3.StringUtils.leftPad(""+p_nNumber, p_nSize, '0');
	}
	
	public static String concatenateOverflowFields(String...strings)
	{
		StringBuffer sReturn = null;
		
		sReturn = new StringBuffer();
		
		for (String sValue : strings)
		{
			if (sValue != null)
			{
				sReturn.append(sValue);
			}
		}
		
		return sReturn.toString();
	}
	
	public static String serialize(List<String> p_listValues, String p_sDelimiter)
	{
		return org.apache.commons.lang3.StringUtils.join(p_listValues, p_sDelimiter);
	}
	
	public static String serializeNames(List<ObjectIdPair<Integer,String>> p_listValues, String p_sDelimiter)
	{
		List<String> listNames = null;
		
		listNames = new ArrayList<String>(p_listValues.size());
		for (ObjectIdPair<Integer,String> oObject : p_listValues)
		{
			listNames.add(oObject.getObjectName());
		}
		return org.apache.commons.lang3.StringUtils.join(listNames, p_sDelimiter);
	}
	
	/**
	 * This utility method will let users replace characters easily for the passed data.
	 * @param p_data			The data whose character need to be replaced.
	 * @param p_chartoreplace	The array of characters to be replaced.
	 * @param p_charwillreplace The array of characters that will replace the characters
	 * 							mentioned in the above parameter.
	 * @return					The updated data where all the characters are replaced.
	 */
	
	public static String replaceChar (
		String		p_data
	,	String[]	p_chartoreplace
	,	String[]	p_charwillreplace
	) {
		if (p_chartoreplace.length != p_charwillreplace.length) {
			return null;
		} else {
			for (int l_i=0; l_i<p_chartoreplace.length; l_i++) {
				p_data	=	p_data.replace(p_chartoreplace[l_i], p_charwillreplace[l_i]);
			}
			return p_data;
		}
	}
	
	/**
	 * This utility method will let users replace characters easily for the passed data.
	 * @param p_data			The data whose character need to be replaced.
	 * @return					The updated data where all the characters are replaced.
	 */
	
	public static String replaceCharLoginUserid (
		String		p_data
	) {
		String[]	l_chartoreplace		=	null,
					l_charwillreplace	=	null;
		
		l_chartoreplace		=	new String [2];
		l_charwillreplace	=	new String [2];
		
		l_chartoreplace[0]	=	"<";
		l_chartoreplace[1]	=	">";
		
		l_charwillreplace[0]	=	"&lt;";
		l_charwillreplace[1]	=	"&gt;";		
		
		return	replaceChar(
					p_data
				, 	l_chartoreplace
				, 	l_charwillreplace
				);
	}
	
	public static String emptyString(String p_sInputValue)
	{
		return (p_sInputValue != null ? p_sInputValue : StringConstants.EMPTY);
	}
}
