package com.sciformix.commons.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.ParseException;
import org.apache.pdfbox.pdmodel.PDDocument;

import com.sciformix.commons.SciException;

import technology.tabula.ObjectExtractor;
import technology.tabula.Page;
import technology.tabula.PageIterator;
import technology.tabula.Rectangle;
import technology.tabula.RectangularTextContainer;
import technology.tabula.Table;
import technology.tabula.extractors.BasicExtractionAlgorithm;
import technology.tabula.extractors.SpreadsheetExtractionAlgorithm;

/**
 * 
 * @author mpatil1
 *
 */
class TabulaPdfProcessorUtils
{
	/*
	 * DEV-NOTE: This class is based on the Class CommandLineApp available within the Tabula library.
	 * 
	 * Changes done in reference to that file:
	 * 	* Support for auto-Extraction method 'DECIDE' dropped
	 *  * Output format support is restricted to 'CSV'
	 *  * Support for directory-based file processing removed
	 *  * Support for redirecting the output to a file has been removed
	 *  * Support for password protected PDFs removed
	 *  * During extraction, 'Guess', 'UseLineReturns', 'VerticalRulingPositions' are not used
	 * 
	 */
    private enum OutputFormat {
        CSV,
        TSV,
        JSON;
    }
    
	public static List<List<String>> extractData(String p_sFile, PdfUtils.ExtractionMethod p_eExtractionMethod, 
			int p_nStartPageNumber, int p_nEndPageNumber, int p_nNumberOfPages, Map<String, String> p_mapParameters) throws SciException
	{
		List<List<String>> oRowsOfAllTables = null;
		Rectangle pageArea;
		List<Integer> pages;
		
        pageArea = TabulaPdfProcessorUtils.determinePageArea(p_mapParameters);
        pages = TabulaPdfProcessorUtils.createPageNumberList(p_nStartPageNumber, p_nEndPageNumber, p_nNumberOfPages);
        
        oRowsOfAllTables = extractFile(p_sFile, pageArea, pages, OutputFormat.CSV, p_eExtractionMethod);
        
        return oRowsOfAllTables;
	}
	
    private static Rectangle determinePageArea(Map<String, String> p_mapParameters) throws SciException
    {
        if (p_mapParameters == null || !p_mapParameters.containsKey("area")) {
            return null;
        }
        List<Float> listFloatValues = null;
        
        try
        {
	        listFloatValues = DataUtils.parseFloatList(p_mapParameters.get("area").toString());
        }
        catch (ParseException e)
        {
        	throw new SciException(e.getMessage(), e);
        }
        
        if (listFloatValues.size() != 4)
        {
            throw new SciException("area parameters must be top,left,bottom,right");
        }
        return new Rectangle(listFloatValues.get(0), listFloatValues.get(1), listFloatValues.get(3) - listFloatValues.get(1), listFloatValues.get(2) - listFloatValues.get(0));
    }
    
    private static List<Integer> createPageNumberList(int p_nStartPageNumber, int p_nEndPageNumber, int p_nNumberOfPages)
    {
    	//Note: If number-of-pages not provided, take the complete file
    	if (p_nNumberOfPages <= 0)
    	{
    		return null;
    	}
    	
    	 List<Integer> listRequiredPages = new ArrayList<Integer>();
    	 int nStartPageNo = 0;
    	 int nEndPageNo = 0;
    	 
    	 nStartPageNo = (p_nStartPageNumber <= 0) ? 1 : p_nStartPageNumber;
    	 nEndPageNo = (p_nEndPageNumber > 0 && p_nEndPageNumber >= p_nStartPageNumber) ? p_nEndPageNumber : p_nNumberOfPages;
    	 nEndPageNo = (nEndPageNo > p_nNumberOfPages) ? p_nNumberOfPages : nEndPageNo;
    	 
    	 listRequiredPages = new ArrayList<Integer>();
     	for (int nCounter = nStartPageNo; nCounter <= nEndPageNo; nCounter++)
     	{
     		listRequiredPages.add(nCounter);
     	}
     	
     	return listRequiredPages;
    }
	
	@SuppressWarnings("rawtypes")
	private static List<List<String>> extractFile(String p_sFile, Rectangle pageArea, List<Integer> pages, 
			OutputFormat outputFormat, PdfUtils.ExtractionMethod extractionMethod) throws SciException
	{
		PDDocument pdfDocument = null;
        List<List<String>> oRowsOfAllTables = null;
        List<Table> tables = null;
        ObjectExtractor extractor = null;
        PageIterator pageIterator = null;
        Page page = null;
        List<String> listCellsOfARow = null;
        
        tables = new ArrayList<Table>();
        oRowsOfAllTables = new ArrayList<List<String>>();
        
        try
        {
            pdfDocument = PDDocument.load(new File(p_sFile));
            extractor = new ObjectExtractor(pdfDocument);
            
            if (pages == null)
            {
            	pageIterator = extractor.extract();
            }
            else
            {
            	pageIterator = extractor.extract(pages);
            }

            while (pageIterator.hasNext())
            {
                page = pageIterator.next();
                if (pageArea != null)
                {
                    page = page.getArea(pageArea);
                }
                
                tables.addAll(extractTables(page, extractionMethod));
            }
            
            for (Table table : tables)
            {
            	 for (List<RectangularTextContainer> oCurrentRow: table.getRows())
            	 {
                     listCellsOfARow = new ArrayList<String>(oCurrentRow.size());
                     for (RectangularTextContainer tc: oCurrentRow)
                     {
                         listCellsOfARow.add(tc.getText());
                     }
                     oRowsOfAllTables.add(listCellsOfARow);
            	 }
            }
            
        }
        catch (IOException e)
        {
            throw new SciException(e.getMessage(), e);
        }
        finally
        {
            try {
                if (pdfDocument != null) {
                    pdfDocument.close();
                }
            } catch (IOException e) {
                throw new SciException("Error closing PDF file", e);
            }
        }
        return oRowsOfAllTables;
    }
	
	@SuppressWarnings("unchecked")
	private static List<Table> extractTables(Page page, PdfUtils.ExtractionMethod extractionMethod)
	{
	    BasicExtractionAlgorithm basicExtractor = null;
	    SpreadsheetExtractionAlgorithm spreadsheetExtractor = null;
	    
		if (extractionMethod == PdfUtils.ExtractionMethod.SPREADSHEET)
		{
	        spreadsheetExtractor = new SpreadsheetExtractionAlgorithm();
	        return (List<Table>) spreadsheetExtractor.extract(page);
		}
		else
		{
			/*if(spreadsheetExtractor.isTabular(page))
			{*/
				
			/*}*/
			//else default to Basic Extractor
			basicExtractor = new BasicExtractionAlgorithm();
			return basicExtractor.extract(page);
		}
		
		//Default Extractor
		
	}
	
}
