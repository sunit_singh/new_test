package com.sciformix.commons.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;


public class PdfUtils 
{
	public enum ExtractionMethod {
        BASIC,
        SPREADSHEET;
    }

	private static final Logger log = LoggerFactory.getLogger(PdfUtils.class);
	
	public static String getFileText(String p_sStagedFileName) throws SciException
	{
		File file = null;
		PDDocument  oPdfReader = null;
		int nStartPageNumber = 0;
		int nEndPageNumber = 0;
		
		//DEV-NOTE: Page counter is 1-based
		nStartPageNumber = 1;
		try
		{
			file = new File(p_sStagedFileName);
			oPdfReader = PDDocument.load(file);
			nEndPageNumber = oPdfReader.getNumberOfPages();
			oPdfReader.close();
		}
		catch (IOException ioExcep)
		{
			log.error("Error extracting text from pdf file ", ioExcep);
			throw new SciException("Error extracting text from pdf file", ioExcep);
		}
		
		return getFileText(p_sStagedFileName, nStartPageNumber, nEndPageNumber);
	}
	
	public static String getFileText(String p_sStagedFileName, int p_nStartPageNumber, int p_nEndPageNumber) throws SciException
	{
		BufferedReader oBufferedReader = null;
		String sInputFileText = null;
		PDDocument oPdfReader = null;
		File file = null;
		PDFTextStripper pdfStripper = null;
		try 
		{
			file = new File(p_sStagedFileName);
			pdfStripper = new PDFTextStripper();
			oPdfReader = PDDocument.load(file);
			pdfStripper.setStartPage(p_nStartPageNumber);
			pdfStripper.setEndPage(p_nEndPageNumber);
			sInputFileText = pdfStripper.getText(oPdfReader);
			oPdfReader.close();
		}
		catch (IOException ioExcep)
		{
			log.error("Error extracting text from pdf file ", ioExcep);
			throw new SciException("Error extracting text from pdf file", ioExcep);
		}
		finally 
		{
			try
			{
				if(oBufferedReader!=null)
				{
					oBufferedReader.close();
				}
			}
			catch (Exception ioExcep)
			{
				log.error("Error closing pdf file", ioExcep);
			}
		}
		
		log.debug("Dumping file=>"+p_sStagedFileName);
		return sInputFileText;
	}

	public static boolean checkIfPdfFile(String p_sStagedFileName) 
	{
		if(p_sStagedFileName.endsWith(".pdf"))
			return true;
		return false;
	}
	
	public static List<PDDocument> splitPdfIntoPages(String p_sStagedFileName) throws SciException
	{
		PDDocument oCompositePdfDocument = null;
		Splitter oPdfDocSplitter = null;
		List<PDDocument> listCompositeDocPages = null;
		
		try
		{
	    	oCompositePdfDocument = PDDocument.load(new File(p_sStagedFileName)); 
			
	    	oPdfDocSplitter = new Splitter();
			listCompositeDocPages = oPdfDocSplitter.split(oCompositePdfDocument);
			
//			 if (oCompositePdfDocument != null)
//			 {
//				 oCompositePdfDocument.close();
//	    	 }
			return listCompositeDocPages;
		}
		catch (IOException ioExcep)
		{
			log.error("Error extracting pages from pdf file ", ioExcep);
			throw new SciException("Error extracting pages from pdf file", ioExcep);
		}
	}
	
	public static List<List<String>> extractData(String p_sFile, ExtractionMethod p_eExtractionMethod, 
			int p_nStartPageNumber, int p_nEndPageNumber, int p_nNumberOfPages, Map<String, String> p_mapParameters) throws SciException
	{
		return TabulaPdfProcessorUtils.extractData(p_sFile, p_eExtractionMethod, 
				p_nStartPageNumber, p_nEndPageNumber, p_nNumberOfPages, p_mapParameters);

	}
	public static int getNumberOfPages(String filePath)
	{
		File file=null;
		int nEndPageNumber=0;
		PDDocument oPdfReader = null;
		
		file=new File(filePath);
		try {
			oPdfReader=PDDocument.load(file);
		} catch (IOException e) {
			log.error("Error while loading file. < "+e.getMessage()+" >");
		}
		finally
		{
			if(oPdfReader!=null)
			{
				try {
					oPdfReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
		nEndPageNumber = oPdfReader.getNumberOfPages();
		return nEndPageNumber;
	}
}
