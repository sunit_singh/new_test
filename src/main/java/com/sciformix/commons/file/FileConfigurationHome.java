package com.sciformix.commons.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;

public class FileConfigurationHome
{
	private static BasicXMLParser basicXmlParser = null;
	
	private static final String XML_FILE_NAME_CONF = "_Configuration";
	private static Map<String, FileParsingConfiguration> s_mapXmlFileConfig = null;
	
	private static final String PROPERTY_FILE_NAME_CONF = "_SDKeyMapping";
	private static Map<String, Properties> s_mapKeyPropertiesConfig = null;
	
	public static boolean init(SciStartupLogger oStartupLogger, String sParserFileConfigurationIds)
	{
		boolean bSuccessful = false;
		String[] xmlFileNameArray = null;
		
		xmlFileNameArray = sParserFileConfigurationIds.split(SciConstants.StringConstants.COMMA);
		
		s_mapXmlFileConfig = new HashMap<>();
		
		try{
			for(String xmlName : xmlFileNameArray){
				System.out.println("Input Stream : "+FileConfigurationHome.class.getClassLoader().getResourceAsStream(xmlName.trim()
						+XML_FILE_NAME_CONF+SciConstants.XMLParseConstant.XML_EXT));
				
				basicXmlParser = new BasicXMLParser();
				s_mapXmlFileConfig.put(xmlName.trim()+XML_FILE_NAME_CONF, 
						basicXmlParser.parseXMLDocument(FileConfigurationHome.class.getClassLoader().getResourceAsStream(xmlName.trim()
						+XML_FILE_NAME_CONF+SciConstants.XMLParseConstant.XML_EXT)))  ;
			}
		}
		catch(SciException scExc) {
			oStartupLogger.logError("Error parsing xml " +scExc.getMessage());
		}
		
		String[] propertiesFileNameArray = null;
		Properties propertyKeys = null;
		
		propertiesFileNameArray = sParserFileConfigurationIds.split(SciConstants.StringConstants.COMMA);
		s_mapKeyPropertiesConfig = new HashMap<>();
		try
		{
			for(String keyPropName : propertiesFileNameArray)
			{
				propertyKeys =	getKeyProp(
									FileConfigurationHome.class.getClassLoader().getResourceAsStream(
										URLDecoder.decode(
											keyPropName.trim()+PROPERTY_FILE_NAME_CONF+SciConstants.XMLParseConstant.PROPERTIES_EXT
										, 	"UTF-8"
										)
									)	
								);
				s_mapKeyPropertiesConfig.put(keyPropName.trim()+ PROPERTY_FILE_NAME_CONF, propertyKeys)  ;
			}
		}
		catch(UnsupportedEncodingException | SciException scExc)
		{
			oStartupLogger.logError("Error parsing xml " +scExc.getMessage());
		}
		bSuccessful = true;
		return bSuccessful;
	}
	
	public static Map<String, FileParsingConfiguration> getXmlFileConfigMap()
	{
		return s_mapXmlFileConfig;	
	}
	
	public static FileParsingConfiguration getFileConfig(String key)
	{
		return s_mapXmlFileConfig.get(key);	
	}
	
	private static Properties getKeyProp(InputStream p_is) throws SciException
	{
		//InputStream keyStream=null;
		Properties keyProp = null;
		try
		{
			//keyStream = new FileInputStream(p_is);
			keyProp = new Properties();
			keyProp.load(p_is);
		} catch (IOException ioExc) {
			throw new SciException("Error Loading key properties", ioExc);
			}
		return keyProp;	
	}
	
	public static Properties getParsePropertiesByKey(String propKey)
	{
		return s_mapKeyPropertiesConfig.get(propKey);
	}
}
