package com.sciformix.commons.file.annotator;


import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;




public class PDFAnnotator {
	//private static final String HIGHLIGHTER_PATTERN = "[\\d]+AP[\\d]+,Deviation\\sPR\\s[\\d]+";



  public Map<String, Object> annotate(File file,String userPath, String pattern) throws Exception {
	 String outputFilePath=null;
	 PDDocument pdDoc = null;
	 RandomAccessRead source=null;
	 PDFParser parser=null;
	 PDFAnnotatorHelper pdfAnnotator=null;
	 File outputFolder=null;
	 String keyForFoundList=null;
	 String fileName = null;
	 
	 Map<String, Object> valueToReturn=new HashMap<String, Object>();
	 Map<String, List<String>> annotedMap=new HashMap<String, List<String>>();
	 Map<String, Map> stringMap=new HashMap<String, Map>();
	 if(file.getName().contains("_file_1_"))
		{
		 fileName = file.getName().substring(file.getName().indexOf("_file_1_")+8);
			
		}
	 outputFolder=new File(userPath+"\\highlighter\\"+fileName.split("\\.pdf")[0]+System.currentTimeMillis());
	 if(!outputFolder.exists())
		 outputFolder.mkdirs();
	 outputFilePath=outputFolder+"\\"+fileName.replace(".pdf", "_highlighted_"+System.currentTimeMillis()+".pdf");

	 FileInputStream f_Is = new FileInputStream(file);
    source = new RandomAccessBuffer(f_Is);
    parser=new PDFParser(source);
    parser.parse();
    pdDoc = new PDDocument(parser.getDocument());
    
    pdfAnnotator = new PDFAnnotatorHelper("UTF-8"); // create new annotator
    pdfAnnotator.setLineSeparator(" "); // kinda depends on what you want to match
    pdfAnnotator.initialize(pdDoc);
    String[] patternArray=pattern.split(",");
    
    
    PDDocumentOutline mostOutline = new PDDocumentOutline();
    pdDoc.getDocumentCatalog().setDocumentOutline(mostOutline);
    for(String pattern1: patternArray)
    {
    	if(pattern1.contains("AP"))
    		keyForFoundList="Case Id";
    	else
    		keyForFoundList="Deviation PR";
    	annotedMap=pdfAnnotator.highlight(mostOutline,pdDoc, pattern1);
    	stringMap.put(keyForFoundList, annotedMap);
    }
//saving annotated document
    pdDoc.save(outputFilePath);
    try {
      if (parser.getDocument() != null) {
        parser.getDocument().close();
      }
      if (pdDoc != null) {
        pdDoc.close();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    finally
    {
    	if(source!= null)
    	source.close();
    	if(f_Is != null)
    		f_Is.close();
    }
    valueToReturn.put("outputFilePath", outputFilePath);
    valueToReturn.put("annotedTextMap", stringMap);
    return valueToReturn;
  }

  public Map<String, Object> setHighlighter(String inputFilePath, String userPath, String pattern) throws Exception
  {
	  File inputFile=null;
	  String outputPath=null;
	  Map<String, Object> keyMap=null;
	  PDFAnnotator oHighlighter=new PDFAnnotator();
	  inputFile = new File(inputFilePath);
	  keyMap=oHighlighter.annotate(inputFile,userPath, pattern);
	  return keyMap;
  }

}




