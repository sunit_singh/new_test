package com.sciformix.commons.file;


public interface IFileSplitter
{
	public String splitFile(String path, String[] headerValues, String userPath)  throws Exception;
	
}
