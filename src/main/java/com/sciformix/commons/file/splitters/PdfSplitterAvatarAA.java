package com.sciformix.commons.file.splitters;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.file.IFileParser;
import com.sciformix.commons.file.IFileSplitter;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarBA;
import com.sciformix.commons.utils.FileUtils;
import com.sciformix.commons.utils.ParsedDataTable;


/**
 * 
 * @author gkorde
 * This class read multiple report pdf file and seperate it by individual report by its AER_Number
 */
public class PdfSplitterAvatarAA implements IFileSplitter
{
	public static Logger log = LoggerFactory.getLogger(PdfSplitterAvatarAA.class);
	
	
	public String splitFile(String p_sStagedFileName, String[] p_sarrHeaderPageIdentifierValues,String userPath) throws Exception
	{
		String sFilesTemporaryStagingFolder = null;
		File oFile = null;
		List<PDDocument> listPagesOfCompositeDoc = null;
		PDDocument oCurrentPageDoc = null;
		PDDocument oChildDoc = null;
		String sChildDocName = null;
		ParsedDataTable oDataTablePageContents = null;
  	  	boolean bHeaderPresent = true;
		PDDocument oCompositePdfDocument = null;
		Splitter oPdfDocSplitter = null;
		IFileParser oParser = null;
  	  	String zipFileName  =null;
		oFile = new File(p_sStagedFileName);
		
		if (!oFile.isFile())
		{
			log.error("Provided file not found {}", oFile.getName());
			throw new SciException("provided file not found"+oFile.getName());
		}
		else if (!FileUtils.isPdfFile(p_sStagedFileName))
		{
			  log.error("Illegal file type detected - {}", oFile.getName());
			  throw new SciException("provided file is  not pdf file"+oFile.getName());
		}
		
		
		try
		{
			if(oFile.getName().contains("_file_1_"))
			{
				zipFileName = oFile.getName().substring(oFile.getName().indexOf("_file_1_")+8);
				
				zipFileName = zipFileName.replace(".pdf", "");
			}
			sFilesTemporaryStagingFolder = userPath + "\\splitFolder\\" +zipFileName+"_"+ System.currentTimeMillis();
			FileUtils.createDirectory(sFilesTemporaryStagingFolder);
			
			oCompositePdfDocument = PDDocument.load(new File(p_sStagedFileName)); 
			
			oPdfDocSplitter = new Splitter();
			listPagesOfCompositeDoc = oPdfDocSplitter.split(oCompositePdfDocument);
			
			
			oChildDoc = null;
			for (int nPageCounter = 0; nPageCounter < listPagesOfCompositeDoc.size(); nPageCounter++)
			{
				oCurrentPageDoc = listPagesOfCompositeDoc.get(nPageCounter);
				  
				//DEV-NOTE: Extraction API required '1-based' page numbering while the page-counter is '0-based'
				oParser = new PdfDocParserAvatarBA(p_sStagedFileName, (nPageCounter+1), (nPageCounter+1), listPagesOfCompositeDoc.size());
				oDataTablePageContents = oParser.parse();
  
				/*
				oRowsOfAllTables = PdfUtils.extractData(p_sStagedFileName, 
						  PdfUtils.ExtractionMethod.SPREADSHEET, 
						  (nPageCounter+1), (nPageCounter+1), listPagesOfCompositeDoc.size(), null);
				  
				oDataTablePageContents = convertRowContentsToDataTable(oRowsOfAllTables);
				*/
  
				bHeaderPresent = true;
				for(String sRequiredHeaderText : p_sarrHeaderPageIdentifierValues)
				{
					 if(!oDataTablePageContents.isColumnPresent(sRequiredHeaderText))
					 {
						 bHeaderPresent=false;
						 break;
					 }
				}
				if (bHeaderPresent)
				{
					//Current Page is start of new Case
					oChildDoc = oCurrentPageDoc;
					 
					sChildDocName = oDataTablePageContents.getColumnValues(PdfDocParserAvatarBA.ARRN_KEY).get(0).displayValue().toString();
					oChildDoc.save(sFilesTemporaryStagingFolder+"\\"+(sChildDocName)+".pdf");
				}
				else
				{
					//Current Page is part of previous Case
					if (oChildDoc != null)
					{
						 for (PDPage oPageToAdd : oCurrentPageDoc.getPages())
						 {
							 oChildDoc.addPage(oPageToAdd);
						 }
						 oChildDoc.save(sFilesTemporaryStagingFolder+"\\"+(sChildDocName)+".pdf");
			    	 }
			     }
			}
		}
		catch(SciException excep)
		{
			excep.printStackTrace();
			log.error("Error in parsing pdf file << "+p_sStagedFileName+" >>");
			throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_PARSING_FILE, excep);
		}
		finally 
		{
			  try
			  {
				  if(oCurrentPageDoc!=null)
		    	  {
		    		  oCurrentPageDoc.close();
		    	  }
		    	  if(oChildDoc!=null)
		    	  {
		    		  oChildDoc.close();
		    	  }
		    	  if (oCompositePdfDocument != null)
		    	  {
					 oCompositePdfDocument.close();
		    	  }
			  }
			  catch(IOException excep)
			  {
				  log.error("Error in parsing pdf file << "+p_sStagedFileName+" >>");
		    	  throw new SciException(ServiceErrorCodes.SourceDocUtil.ERROR_DOWNLOADING_FILE.toString(), excep);
			  }
		 }
		return sFilesTemporaryStagingFolder;
	}
}
