package com.sciformix.commons.file;

import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.utils.ParsedDataTable;

public interface IFileParser
{
	public String getFtlTemplateFileName();
	public Map<String, List<String>> compareMultipleDataTable(List<ParsedDataTable> m_DdataTableList);
	public void setKeyHeaderList(String[] keyHeaderList);
	public ParsedDataTable parse() throws SciException, SciServiceException;
	public ParsedDataTable MapAutomatedKeysToMnemonics(ParsedDataTable m_ParsedDataTable);
}
