package com.sciformix.commons.file;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sciformix.commons.SciException;
import com.sciformix.commons.file.FileParsingConfiguration.DataType;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;

public class BasicXMLParser extends DefaultHandler 
{
	private static final Logger log = LoggerFactory.getLogger(BasicXMLParser.class);
	
	String sXmlFileToBeParsed = null;
	FileParsingConfiguration fileParsingConfig = new FileParsingConfiguration();
	List<FileParsingField> fieldList = null;
	FileParsingField oFileParsingField = null;
	String tmpValue = null;
	String sMnemonic = null;
	String sLabel = null;
	String sGroup = null;
	String sSubGroup = null;
	String collationgroup = null;

	public BasicXMLParser(String p_sxmlFileToParse) throws SciException
	{
		this.sXmlFileToBeParsed = p_sxmlFileToParse;
		parseXMLDocument();
	}
	
	public BasicXMLParser() throws SciException
	{
		//parseXMLDocument(p_sxmlFileToParse);
	}

	public FileParsingConfiguration parseXMLDocument() throws SciException
	{
		SAXParserFactory saxFactory = null;
		SAXParser saxParser = null;

		try 
		{
			saxFactory = SAXParserFactory.newInstance();
			saxParser = saxFactory.newSAXParser();
			saxParser.parse(sXmlFileToBeParsed, this);
		} 
		catch (ParserConfigurationException pce) 
		{
			log.error("Error in XML Parsing" , pce);
			throw new SciException("Error in XML Parsing");
		} catch (SAXException sxe) 
		{
			log.error("Error in XML Parsing" , sxe);
			throw new SciException("Error in XML Parsing");
		} catch (IOException ioe) 
		{
			log.error("Error in XML Parsing" , ioe);
			throw new SciException("Error in XML Parsing");
		}
		return fileParsingConfig;
	}
	
	public FileParsingConfiguration parseXMLDocument(InputStream p_is) throws SciException
	{
		SAXParserFactory saxFactory = null;
		SAXParser saxParser = null;

		try 
		{
			saxFactory = SAXParserFactory.newInstance();
			saxParser = saxFactory.newSAXParser();
			saxParser.parse(p_is, this);
		} 
		catch (ParserConfigurationException pce) 
		{
			log.error("Error in XML Parsing" , pce);
			throw new SciException("Error in XML Parsing");
		} catch (SAXException sxe) 
		{
			log.error("Error in XML Parsing" , sxe);
			throw new SciException("Error in XML Parsing");
		} catch (IOException ioe) 
		{
			log.error("Error in XML Parsing" , ioe);
			throw new SciException("Error in XML Parsing");
		}
		return fileParsingConfig;
	}

	@Override
	public void startElement(String uri, String localName, String elementName, Attributes attributes) 
	{
		if (elementName.equalsIgnoreCase("Field")) {
			FileParsingConfiguration fileParsingConfig = new FileParsingConfiguration();
			oFileParsingField = fileParsingConfig.new FileParsingField();
			sMnemonic = attributes.getValue("sdMnemonic");
			sLabel = attributes.getValue("label");
			sGroup = attributes.getValue("group");
			sSubGroup = attributes.getValue("subgroup");
			collationgroup = attributes.getValue("collationgroup");
			oFileParsingField.setMnemonic(sMnemonic);
			oFileParsingField.setFieldLabel(sLabel);
			oFileParsingField.setM_sFieldGroup(sGroup);
			oFileParsingField.setM_sFieldSubGroup(sSubGroup);
			oFileParsingField.setCollationgroup(collationgroup);
		}

	}

	@Override
	public void endElement(String uri, String localName, String elementName) 
	{
		String sDataType = null;
		DataType oDataType = null;
		String sSingleValued = null;
		boolean bSingleValued = false;
		String sMandatory = null;
		boolean bMandatory = false;
		String sCritical = null;
		boolean bCritical = false;
		String sDisplayOnScreen = null;
		boolean bDisplayOnScreen = false;
		String sValidationRule = null;
		String sLegalValues = null;
		String errorType = null;
		String comment = null;
		String[] sArrLegalValues = null;
		boolean deMandatory = false;
		String sDeMandatory = "";
		String deGroup = null;

		if (elementName.equalsIgnoreCase("Field")) 
		{

			fileParsingConfig.addField(oFileParsingField);

		}
		if (elementName.equalsIgnoreCase("DataType")) 
		{
			sDataType = tmpValue;
			if (sDataType != null && sDataType.equalsIgnoreCase("TEXT")) 
			{
				oDataType = DataType.TEXT;
			} else if (sDataType != null && sDataType.equalsIgnoreCase("NUMERIC"))
			{
				oDataType = DataType.NUMERIC;
			} else if (sDataType != null && sDataType.equalsIgnoreCase("DATE")) 
			{
				oDataType = DataType.DATE;
			}
			oFileParsingField.setDataType(oDataType);
		}
		if (elementName.equalsIgnoreCase("SingleValued")) 
		{
			sSingleValued = tmpValue;

			if (sSingleValued != null && sSingleValued.equalsIgnoreCase("YES") && !sSingleValued.equalsIgnoreCase("")) 
			{
				bSingleValued = true;
			} else {
				bSingleValued = false;
			}
			oFileParsingField.setMultivalued(bSingleValued);
		}
		if (elementName.equalsIgnoreCase("Mandatory")) 
		{
			sMandatory = tmpValue;

			if (sMandatory != null && sMandatory.equalsIgnoreCase("YES") && !sMandatory.equalsIgnoreCase(""))
			{
				bMandatory = true;
			} else {
				bMandatory = false;
			}
			oFileParsingField.setMandatory(bMandatory);
		}
		if (elementName.equalsIgnoreCase("Critical")) 
		{
			sCritical = tmpValue;

			if (sCritical != null && sCritical.equalsIgnoreCase("YES") && !sCritical.equalsIgnoreCase(""))
			{
				bCritical = true;
			} else {
				bCritical = false;
			}
			oFileParsingField.setCritical(bCritical);
		}
		if (elementName.equalsIgnoreCase("DisplayOnScreen")) 
		{
			sDisplayOnScreen = tmpValue;

			if (sDisplayOnScreen != null && sDisplayOnScreen.equalsIgnoreCase("YES") && !sDisplayOnScreen.equalsIgnoreCase(""))
			{
				bDisplayOnScreen = true;
			} else {
				bDisplayOnScreen = false;
			}
			oFileParsingField.setDisplayOnScreen(bDisplayOnScreen);
		}
		if (elementName.equalsIgnoreCase("ValidationRule"))
		{
			sValidationRule = tmpValue;
			oFileParsingField.setValidationRule(sValidationRule);
		}
		if (elementName.equalsIgnoreCase("ErrorType"))
		{
			errorType = tmpValue;
			oFileParsingField.setErrorType(errorType);
		}
		if (elementName.equalsIgnoreCase("Comments"))
		{
			comment = tmpValue;
			oFileParsingField.setComment(comment);
		}
		if (elementName.equalsIgnoreCase("LegalValues"))
		{
			sLegalValues = tmpValue;
			sArrLegalValues = sLegalValues.split(",");
			oFileParsingField.setLegalValues(sArrLegalValues);
		}
		if (elementName.equalsIgnoreCase("DEGroup"))
		{
			deGroup = tmpValue;
			oFileParsingField.setDEGroup(deGroup);
		}
		if (elementName.equalsIgnoreCase("DEMandatory"))
		{
			sDeMandatory = tmpValue;
			if(sDeMandatory != null && sDeMandatory.equalsIgnoreCase("YES"))
			{
				deMandatory=true;
			}
			else if(sDeMandatory == null || sDeMandatory.equalsIgnoreCase("NO"))
				deMandatory=false;
			else
				deMandatory=false;
			
			oFileParsingField.setDEMandatory(deMandatory);
		}
		

	}

	public void characters(char[] ac, int i, int j) 
	{
		tmpValue = new String(ac, i, j);
	}

	/*public static void main(String args[]) 
	{
		BasicXMLParser bxmlParser = new BasicXMLParser("C:\\Users\\rramchandran\\Desktop\\parsing.xml");
	}*/
}
