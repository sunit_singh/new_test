package com.sciformix.commons.file;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FileParsingConfiguration
{
	private Map<String, FileParsingField> m_mapFields = null;
	
	public static enum DataType
	{
		TEXT, NUMERIC, DATE;
	}
	
	public class FileParsingField
	{
		private String m_sFieldGroup = null;
		private String m_sFieldSubGroup = null;
		private String m_sFieldLabel = null;
		private String m_collationgroup = null;
		
		private String m_sFieldMnemonic = null;
		private DataType m_eDataType = null;
		private boolean m_bMandatory = false;
		private boolean m_bCritical = false;
		private boolean m_bDisplayOnScreen = false;
		private boolean m_bMultivalued = false;
		private String m_sValidationRule = null;
		private String errorType = null;
		private String comment = null;
		private String[] m_sarrLegalValues = null;
		private String m_deGroup = null;
		private boolean m_deMandatory = false;
		
		public FileParsingField(String p_sMnemonic, DataType p_eDataType,boolean p_sSingleValued, boolean p_bMandatory, boolean p_bCritical, boolean p_bDisplayOnScreen, String p_sValidationRule,String [] p_sLegalValues,String p_deGroup, boolean p_deMandatory)
		{
			this.m_sFieldMnemonic = p_sMnemonic;
			this.m_eDataType = p_eDataType;
			this.m_bMultivalued = p_sSingleValued;
			this.m_bMandatory =p_bMandatory;
			this.m_bCritical =p_bCritical;
			this.m_bDisplayOnScreen = p_bDisplayOnScreen;
			this.m_sValidationRule = p_sValidationRule;
			this.m_sarrLegalValues = p_sLegalValues;
			this.m_deGroup = p_deGroup;
			this.m_deMandatory = p_deMandatory;
		}
		
		public FileParsingField() {
			
			//nothing to do

		}
		public DataType getDataType()
		{
			return m_eDataType ;
		}
		public void setDataType(DataType m_eDataType)
		{
			this.m_eDataType = m_eDataType;
		}
		
		public String getMnemonic()
		{
			return m_sFieldMnemonic;
		}
		
		public void setMnemonic(String m_sFieldMnemonic)
		{
		this.m_sFieldMnemonic = m_sFieldMnemonic;	
		}
		
		public boolean isMandatory()
		{
			return m_bMandatory;
		}
		
		public boolean isCritical()
		{
			return m_bCritical;
		}
		
		public void setCritical(boolean m_bCritical)
		{
			this.m_bCritical = m_bCritical;
		}
		public boolean isDisplayOnScreen()
		{
			return m_bDisplayOnScreen;
		}
		
		public void setDisplayOnScreen(boolean m_bDisplayOnScreen)
		{
			this.m_bDisplayOnScreen = m_bDisplayOnScreen;
		}
		
		public void setMandatory(boolean m_bMandatory)
		{
			this.m_bMandatory = m_bMandatory;
		}
		
		public String getValidationRule()
		{
			return m_sValidationRule;
		}

		public void setValidationRule(String m_sValidationRule)
		{
			this.m_sValidationRule = m_sValidationRule;
		}
		
		public String getM_sFieldGroup() 
		{
			return m_sFieldGroup;
		}

		public void setM_sFieldGroup(String m_sFieldGroup) 
		{
			this.m_sFieldGroup = m_sFieldGroup;
		}
		
		public String getM_sFieldSubGroup() 
		{
			return m_sFieldSubGroup;
		}

		public void setM_sFieldSubGroup(String m_sFieldSubGroup) 
		{
			this.m_sFieldSubGroup = m_sFieldSubGroup;
		}
		public String getCollationgroup() {
			return m_collationgroup;
		}

		public void setCollationgroup(String m_collationgroup) {
			this.m_collationgroup = m_collationgroup;
		}

		public String getFieldLabel() 
		{
			return m_sFieldLabel;
		}

		public void setFieldLabel(String m_sFieldLabel) 
		{
			this.m_sFieldLabel = m_sFieldLabel;
		}

		public boolean isMultivalued() 
		{
			return m_bMultivalued;
		}

		public void setMultivalued(boolean m_bMultivalued) 
		{
			this.m_bMultivalued = m_bMultivalued;
		}

		public String[] getLegalValues() 
		{
			return m_sarrLegalValues;
		}
		public String getErrorType() {
			return errorType;
		}

		public void setErrorType(String errorType) {
			this.errorType = errorType;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}
		public void setLegalValues(String[] m_sarrLegalValues) 
		{
			this.m_sarrLegalValues = m_sarrLegalValues;
		}
		
		public void setDEGroup(String m_deGroup) {
			this.m_deGroup = m_deGroup;
	}
		
		public String getDEGroup() {
			return m_deGroup;
		}
		
		public void setDEMandatory(boolean m_DEMandatory)
		{
			this.m_deMandatory = m_DEMandatory;
		}
		
		public boolean isDEMandatory()
		{
			return m_deMandatory;
		}
	}
	
	
	public FileParsingConfiguration()
	{
		m_mapFields = new LinkedHashMap<String, FileParsingField>();
	}
	
	public void addField(FileParsingField p_oField)
	{
		m_mapFields.put(p_oField.getMnemonic(), p_oField);
	}
	/*public void addField(FileParsingField p_oField)
	{
		m_mapFields.put(p_oField.getM_sFieldLabel(), p_oField);
	}*/
	
	
	public FileParsingField getField(String p_sMnemonic)
	{
		return m_mapFields.get(p_sMnemonic);
	}
	
	public Map<String, FileParsingField> getFieldMap()
	{
		return m_mapFields;
	}
	
	public List<String> getMnemonicListByHeader(String Header)
	{
		List<String> mnemonicList = new ArrayList<String>();
		
		for(String key : m_mapFields.keySet())
		{
			if(m_mapFields.get(key).m_sFieldGroup.equals(Header))
			{
				mnemonicList.add(m_mapFields.get(key).m_sFieldMnemonic);
			}
		}
		return mnemonicList;
	}
	public List<String> getFieldGroupList()
	{
		Set<String> groupSet = new LinkedHashSet<String>();
	
		for(String key : m_mapFields.keySet())
		{
			groupSet.add(m_mapFields.get(key).m_sFieldGroup);
		}
		List<String> groupList = new ArrayList<String>(groupSet);
		return groupList;
	}
	public List<String> getCollationGroupList()
	{
		Set<String> collationGroupSet = new LinkedHashSet<String>();
		
		for(String key : m_mapFields.keySet())
		{
			collationGroupSet.add(m_mapFields.get(key).m_collationgroup);
		}
		List<String> collationGroupList = new ArrayList<String>(collationGroupSet);
		return collationGroupList;
	}
	public List<String> getGrouplistByCollationGroup(String collationGroup)
	{
		Set<String> collationGroupSet = new LinkedHashSet<String>();
		
		for(String key : m_mapFields.keySet())
		{
			if(m_mapFields.get(key).m_collationgroup.equalsIgnoreCase(collationGroup))
			{
				collationGroupSet.add(m_mapFields.get(key).m_sFieldGroup);
			}
		}
		List<String> collationGroupList = new ArrayList<String>(collationGroupSet);
		return collationGroupList;
	}
	public List<FileParsingConfiguration.FileParsingField> getFieldListByHeader(String Header)
	{
		List<FileParsingConfiguration.FileParsingField> mnemonicList = new ArrayList<>();
		
		for(String key : m_mapFields.keySet())
		{
			if(m_mapFields.get(key).m_sFieldGroup.equals(Header))
			{
				mnemonicList.add(m_mapFields.get(key));
			}
		}
		return mnemonicList;
	}
	public List<String> getsubGroupListByGroupName(String subGroup)
	{
		Set<String> subGroupSet = new LinkedHashSet<String>();
		
		for(String key : m_mapFields.keySet())
		{
			if(m_mapFields.get(key).getM_sFieldGroup().equalsIgnoreCase(subGroup))
			{
				subGroupSet.add(m_mapFields.get(key).m_sFieldSubGroup);
			}
		}
		List<String> subGroupList = new ArrayList<String>(subGroupSet);
		return subGroupList;
	}
	public List<FileParsingConfiguration.FileParsingField> getFieldListBySubGroup(String subgroup)
	{
		List<FileParsingConfiguration.FileParsingField> mnemonicList = new ArrayList<>();
		
		for(String key : m_mapFields.keySet())
		{
			if(m_mapFields.get(key).m_sFieldSubGroup.equals(subgroup))
			{
				mnemonicList.add(m_mapFields.get(key));
			}
		}
		return mnemonicList;
	}
	
	public List<FileParsingConfiguration.FileParsingField> getFieldListByCollationGroup(String collationGroup)
	{
		List<FileParsingConfiguration.FileParsingField> fieldList = new ArrayList<>();
		
		for(String key : m_mapFields.keySet())
		{
			if(m_mapFields.get(key).getCollationgroup().equals(collationGroup))
			{
				fieldList.add(m_mapFields.get(key));
			}
		}
		return fieldList;
	}
	
	public List<FileParsingConfiguration.FileParsingField> getCriticalFields()
	{
		List<FileParsingConfiguration.FileParsingField> fieldList = new ArrayList<>();
		
		for(String key : m_mapFields.keySet())
		{
			if(m_mapFields.get(key).isCritical())
			{
				fieldList.add(m_mapFields.get(key));
			}
		}
		return fieldList;
	}
	
}
