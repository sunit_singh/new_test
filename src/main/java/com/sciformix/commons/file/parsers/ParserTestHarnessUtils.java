package com.sciformix.commons.file.parsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class ParserTestHarnessUtils
{
	
	private final static String _NEW_LINE_HEADER = "{{NEW_LINE_HEADER}}";
	private final static String _INTERNAL_CRLF_ESCAPE_SEQUENCE = "{{CRLF}}";
	private final static String _INTERNAL_CR_ESCAPE_SEQUENCE = "{{CR}}";
	private final static String _NULL_LINE_INDICATOR = "{{null}}";
	
	private static final String FRAGMENT_START_PREFIX = "{{FragmentStart-";
	private static final String FRAGMENT_END_PREFIX = "{{FragmentEnd-";
	private static final String FRAGMENT_SUFFIX = "}}";
	
	private static final String _NEW_FIELD_DELIMITER = "!>!";
	private static final String _BOX_BRACKET_START = "[";
	private static final String _BOX_BRACKET_CLOSE = "]";
	
	private static final Logger log = LoggerFactory.getLogger(ParserTestHarnessUtils.class);
	
	private ParserTestHarnessUtils()
	{
		//Nothing to do
	}
	
	public static void printExtractedData(List<List<String>> p_listExtractedData, String p_sFilePath, int p_sFragmentNumber)
	{
		StringBuilder sb = null;
		
		sb = new StringBuilder();
		for (List<String> listExtractedDataLine : p_listExtractedData)
		{
			sb.append(_NEW_LINE_HEADER).append(StringConstants.CRLF);
			for (String sExtractedDataField : listExtractedDataLine)
			{
				sb.append(formatFieldValueForPrint(sExtractedDataField)).append(StringConstants.CRLF);
			}
		}
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriterWithEncoding(p_sFilePath, StringConstants.FILE_CHARSET_UTF8, true)))
		{
			bw.write(FRAGMENT_START_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX + StringConstants.CRLF);
			bw.write(sb.toString());
			bw.write(StringConstants.CRLF + FRAGMENT_END_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX + StringConstants.CRLF);

		} catch (IOException e) {

			log.error("Error in writing the uploaded file(s)", e.getMessage());

		}
	}
	
	public static void printExtractedDataTable(ParsedDataTable p_oDataTablePageContents, String p_sFilePath)
	{
		StringBuilder sb = null;
		
		sb = new StringBuilder();
		for(int colCount=0 ;colCount<p_oDataTablePageContents.colCount(); colCount++ )
		{
			sb.append(p_oDataTablePageContents.getMnemonic(colCount));
			sb.append(_BOX_BRACKET_START).append(p_oDataTablePageContents.getLabel(colCount)).append(_BOX_BRACKET_CLOSE);
			sb.append(_NEW_FIELD_DELIMITER).append(p_oDataTablePageContents.getColumnValues(colCount).get(0).displayValue()).append(StringConstants.CRLF);
		}
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriterWithEncoding(p_sFilePath, StringConstants.FILE_CHARSET_UTF8, true)))
		{
			bw.write(sb.toString());
		} catch (IOException e) {

			log.error("Error in writing the uploaded file(s)", e.getMessage());

		}
	}
	
	public static void printExtractedData(String p_sExtractedData, String p_sFilePath, int p_sFragmentNumber)
	{
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(p_sFilePath, true))) {

			bw.write(FRAGMENT_START_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX + StringConstants.CRLF);
			bw.write(p_sExtractedData != null ? formatFieldValueForPrint(p_sExtractedData) : StringConstants.EMPTY);
			bw.write(StringConstants.CRLF + FRAGMENT_END_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX + StringConstants.CRLF);

		} catch (IOException e) {

			log.error("Error in writing the uploaded file(s)", e.getMessage());
		}
	}
	
	public static List<List<String>> readExtractedData(String p_sFileName, int p_sFragmentNumber) throws SciException
	{
		List<List<String>> listInputExtractedData = null;
		List<String> listSingleLine = null;
		List<String> listRawInputLines =  new ArrayList<>();
		boolean bRelevantFragment = false;
		
		listInputExtractedData = new ArrayList<>();
		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(p_sFileName));
			String line = reader.readLine();
			while (line != null) {
				if (!bRelevantFragment)
				{
					if (line.equals(FRAGMENT_START_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX))
					{
						bRelevantFragment = true;
					}
				}
				else
				{
					if (line.equals(FRAGMENT_END_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX))
					{
						bRelevantFragment = false;
					}
					else
					{
						listRawInputLines.add(line);
					}
				}
				
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			throw new SciException("Error reading file", e);
		}
		
		for (String sRawInputLine : listRawInputLines)
		{
			if (sRawInputLine.indexOf(_NEW_LINE_HEADER) != -1)
			{
				if (listSingleLine  == null)
				{
					listSingleLine  = new ArrayList<>();
				}
				else
				{
					listInputExtractedData.add(listSingleLine);
					listSingleLine = new ArrayList<>();
				}
			}
			else
			{
				listSingleLine.add(retrieveFieldRawValueFromFormattedValue(sRawInputLine));
			}
		}
		
		return listInputExtractedData;
	}
	public static ParsedDataTable readExtractedDataTable(String p_sFileName) throws SciException
	{
		ParsedDataTable oInputExtractedDataTable = null;
		
		oInputExtractedDataTable = new ParsedDataTable();
		
		BufferedReader reader;
		int nIndexOfDelim = -1;
		int nIndexOfBracket = -1;
		String sTemp = null;
		String sKey = null;
		String sValue = null;
		try
		{
			reader = new BufferedReader(new FileReader(p_sFileName));
			String line = reader.readLine();
			while (line != null)
			{
				nIndexOfDelim = line.indexOf(_NEW_FIELD_DELIMITER);
				if (nIndexOfDelim > -1)
				{
					sTemp = line.substring(0, nIndexOfDelim);
					nIndexOfBracket = sTemp.indexOf(_BOX_BRACKET_START);
					if (nIndexOfBracket > -1)
					{
						sKey = sTemp.substring(0, nIndexOfBracket);
					}
					else
					{
						sKey = sTemp;
					}
					sValue = line.substring(nIndexOfDelim + _NEW_FIELD_DELIMITER.length());
				}
				
				oInputExtractedDataTable.addColumn(new ParsedDataTable.ParsedData(sKey, sKey, 
							new ParsedDataTable.StringParsedDataValue(sValue, sValue, DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE)));
				
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			throw new SciException("Error reading file", e);
		}
		
		return oInputExtractedDataTable;
	}
	
	public static String readExtractedStringData(String p_sFileName, int p_sFragmentNumber) throws SciException
	{
		StringBuilder sb = null;
		boolean bRelevantFragment = false;
		sb = new StringBuilder();
		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(p_sFileName));
			String line = reader.readLine();
			while (line != null)
			{
				if (!bRelevantFragment)
				{
					if (line.equals(FRAGMENT_START_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX))
					{
						bRelevantFragment = true;
					}
				}
				else
				{
					if (line.equals(FRAGMENT_END_PREFIX + p_sFragmentNumber + FRAGMENT_SUFFIX))
					{
						bRelevantFragment = false;
					}
					else
					{
						sb.append(retrieveFieldRawValueFromFormattedValue(line));
					}
				}
				
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			throw new SciException("Error reading file", e);
		}
		
		return sb.toString();
	}
	
	private static String formatFieldValueForPrint(String p_sRawValue)
	{
		String sFormattedValue = null;
		
		if (p_sRawValue == null)
		{
			sFormattedValue = _NULL_LINE_INDICATOR;
		}
		else
		{
			sFormattedValue = p_sRawValue;
			while (sFormattedValue.indexOf(StringConstants.CRLF) != -1)
			{
				sFormattedValue = sFormattedValue.substring(0, sFormattedValue.indexOf(StringConstants.CRLF))
									+ _INTERNAL_CRLF_ESCAPE_SEQUENCE
									+ sFormattedValue.substring(sFormattedValue.indexOf(StringConstants.CRLF)+StringConstants.CRLF.length());
			}
			while (sFormattedValue.indexOf(StringConstants.CARRIAGERETURN) != -1)
			{
				sFormattedValue = sFormattedValue.substring(0, sFormattedValue.indexOf(StringConstants.CARRIAGERETURN))
									+ _INTERNAL_CR_ESCAPE_SEQUENCE
									+ sFormattedValue.substring(sFormattedValue.indexOf(StringConstants.CARRIAGERETURN)+StringConstants.CARRIAGERETURN.length());
			}
		}
		return sFormattedValue;
	}
	
	private static String retrieveFieldRawValueFromFormattedValue(String p_sFormattedValue)
	{
		String sRawValue = null;
		
		if (p_sFormattedValue == null)
		{
			sRawValue = null;
		}
		if (p_sFormattedValue != null && p_sFormattedValue.equals(_NULL_LINE_INDICATOR))
		{
			sRawValue = null;
		}
		else
		{
			sRawValue = p_sFormattedValue;
			while (sRawValue.indexOf(_INTERNAL_CRLF_ESCAPE_SEQUENCE) != -1)
			{
				sRawValue = sRawValue.substring(0, sRawValue.indexOf(_INTERNAL_CRLF_ESCAPE_SEQUENCE))
						+ StringConstants.CRLF
						+ sRawValue.substring(sRawValue.indexOf(_INTERNAL_CRLF_ESCAPE_SEQUENCE)+_INTERNAL_CRLF_ESCAPE_SEQUENCE.length());
			}
			while (sRawValue.indexOf(_INTERNAL_CR_ESCAPE_SEQUENCE) != -1)
			{
				sRawValue = sRawValue.substring(0, sRawValue.indexOf(_INTERNAL_CR_ESCAPE_SEQUENCE))
						+ StringConstants.CARRIAGERETURN
						+ sRawValue.substring(sRawValue.indexOf(_INTERNAL_CR_ESCAPE_SEQUENCE)+_INTERNAL_CR_ESCAPE_SEQUENCE.length());
			}
		}
		return sRawValue;
	}
}
