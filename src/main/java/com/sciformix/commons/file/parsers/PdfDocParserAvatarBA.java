package com.sciformix.commons.file.parsers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.file.BaseFileParser;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;
import com.sciformix.commons.utils.PdfUtils;
import com.sciformix.commons.utils.ValidationUtils;




public class PdfDocParserAvatarBA extends BaseFileParser
{
	private static final String LINK_DUP_RECORD_COUNT = "LinkDupRecord Count";

	private static final String HEALTH_CANADA_FILETYPE_VALUE = "HealthCanada-AERReport-PDF";

	private static final String FILE_TYPE_KEY = "fileType";

	private static final String AGEKEY_REPORT1_AGE = "Report1.Age";

	private static final String PATIENT_AGE_UNIT = "Patient Age Unit";

	private static final String PATIENT_AGE = "Patient Age";

	private static final String EVENT_COUNT = "Event Count";

	private static final String PRODUCT_COUNT = "Product Count";

	private static final String OTHER_MEDICALLY_IMPORTANT_CONDITIONS = "Other Medically Important Conditions";

	private static final String HOSPITALIZATION = "Hospitalization";

	private static final String LIFE_THREATENING = "Life Threatening";

	private static final String CONGENITAL_ANOMALY = "Congenital Anomaly";

	private static final String DISABILITY = "Disability";

	private static final String DEATH = "Death";

	private static final String TABLECOLUMNHEADERKEY_DEATH = "Death:";

	private static final String COUNTRY_KEY = "Country";

	private static final String REPORT_RUNTIME_KEY = "Report Runtime";

	private static final String REPORTRUNTIME_FETCH_SUFFIX_REPORT_S = "Report(s)";

	private static final String REPORTRUNTIME_FETCH_PREFIX_TOTAL_NUMBER_OF_REPORTS = "Total Number of Reports:";

	private static final String TABLEHEADER_LINK_DUPLICATE_RECORD = "Link_Duplicate_Record";

	private static final String PRODUCTKEYPREFIX_PRODUCT = "product";

	private static final String EVENTKEYPREFIX_EVENT = "event";

	private static final String KEYPREFIX_REPORT1_DOT = "Report1.";

	private static final String TABLECOLUMNHEADER_DOSE_PREFIX_REPORT1_PRODUCT = "Report1.product";

	private static final String TABLECOLUMNHEADER_ADVERSE_REACTION_TERM_S = "Adverse Reaction Term(s)";

	private static final String TABLECOLUMNHEADER_DOSE = "Dose";

	private static final String TABLECOLUMNHEADER_PRODUCT_DESCRIPTION = "Product Description";

	private static final String TABLENAME_ADVERSE_REACTION_TERM_INFORMATION = "Adverse Reaction Term Information";

	private static final String TABLECOLUMNHEADER_RECORD_TYPE = "Record Type";

	private static final String TABLECOLUMNHEADER_AGE = "Age";

	private static final String TABLECOLUMNHEADER_SERIOUS_REPORT = "Serious report?";

	private static final String TABLECOLUMNHEADER_ADVERSE_REACTION_REPORT_NUMBER = "Adverse Reaction Report Number";

	private static final String TABLENAME_LINK_DUPLICATE_REPORT_INFORMATION = "Link / Duplicate Report Information";

	private static final String TABLENAME_PATIENT_INFORMATION = "Patient Information";

	private static final String TABLENAME_PRODUCT_INFORMATION = "Product Information";

	private static final String TABLENAME_REPORT_INFORMATION = "Report Information";

	private static final Logger log = LoggerFactory.getLogger(PdfDocParserAvatarBA.class);
	
	public static final String ARRN_KEY = "Report1.Adverse Reaction Report Number";
	
	private int m_nStartPageNumber = 0;
	private int m_nEndPageNumber = 0;
	private int m_NumberOfPages = 0;
	
	public PdfDocParserAvatarBA(String p_sDocumentLocalPath)
	{
		
		this(p_sDocumentLocalPath, 0 ,0, PdfUtils.getNumberOfPages(p_sDocumentLocalPath));
	}
	
	public PdfDocParserAvatarBA(String p_sDocumentLocalPath, int p_nStartPageNumber, int p_nEndPageNumber, int p_NumberOfPages)
	{
		super(p_sDocumentLocalPath, "PdfDocParser_AvatarBA.ftl");
		
		super.m_oParsingConfiguration = new FileParsingConfiguration();
		loadValidationFile();
		m_nStartPageNumber = p_nStartPageNumber;
		m_nEndPageNumber = p_nEndPageNumber;
		m_NumberOfPages = p_NumberOfPages;
	}
	public FileParsingConfiguration loadValidationFile()
	{
		FileParsingField field = null;
		FileParsingConfiguration filePassConfig = null;

		filePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_BA_KEY);
	
	    for(String key :  filePassConfig.getFieldMap().keySet())

	    {

	           field = filePassConfig.getField(key);
	           m_oParsingConfiguration.addField(field);
	    

	    }
	    
	    return filePassConfig;
	}

	@Override
	public ParsedDataTable  parseDocument() throws SciException
	{
		ParsedDataTable oDataTablePageContents = null;
  	  	List<List<String>> oRowsOfAllTables = null;
  	  	
		oDataTablePageContents = new ParsedDataTable();
		  
		//DEV-NOTE: Extraction API required '1-based' page numbering
		oRowsOfAllTables = PdfUtils.extractData(super.m_sDocumentLocalPath, PdfUtils.ExtractionMethod.SPREADSHEET, 
									m_nStartPageNumber, m_nEndPageNumber, m_NumberOfPages, null);
		
		
		oDataTablePageContents = convertRowContentsToDataTable(super.m_sDocumentLocalPath, oRowsOfAllTables);
		
		return oDataTablePageContents;
	}

	@Override
	protected boolean canDocumentBeParsed() throws SciServiceException
	{
			String fileText = null;
			List<String> keyList = null;
			boolean keyPresent= true;
			//handle the file validation wether file is of same type or not
	  	  	try {
				fileText = PdfUtils.getFileText(super.m_sDocumentLocalPath);
			} catch (SciException e) {
				log.error("Error while extracting text from PDF"+e.getMessage());
			return false;
		}
	  	  	fileText = fileText.replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE);
	  	    fileText = fileText.replaceAll(StringConstants.CRLF, StringConstants.SPACE);
	  	    fileText = fileText.replaceAll(StringConstants.NEWLINE, StringConstants.SPACE);
	  	  	
	  	    keyList = this.keyHeaderList;
	  	  	for(String key:keyList)
		{
	  	  		if(!fileText.contains(key))
	  	  		{
	  	  			keyPresent = false;
	  	  			break;
	  	  		}
		}
		
	  	  if(!keyPresent)
		  	{
		  	throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_FILE_TYPE_AND_FILE_DOES_NOT_MATCH);
		  	}
	  	  	
	  	  		
			return keyPresent;
	}
	
	
	@Override
	protected boolean validateParsedDocumentData()
	{
		return true;
	}

	

	private ParsedDataTable convertRowContentsToDataTable(String filePath, List<List<String>> p_listRowsOfAllTables) throws SciException
	{
		ParsedDataTable oDataTable = null;
		
		Set<String> setHeaderContent = new HashSet<String>();
		String sTemp = null;
		int prodCount=0;
		int eventCount=0;
		int linkDupRecordCount=0;
		int nTableRowCounter = 0;
		List<String> listTableHeaderRow = null;
		String sCurrentRowKey = null;
		
		oDataTable = new ParsedDataTable();
		listTableHeaderRow = new ArrayList<String>();
		
		if(!validateFileParse(filePath))
		{
			log.error("this page is not parsable");
		}
		else
		{
		try
		{
			addNonTabularKeysToDataTable(filePath, oDataTable, setHeaderContent);
			
			listTableHeaderRow.clear();
			nTableRowCounter = 0;
			prodCount = 0;
			eventCount = 0;
			linkDupRecordCount = 0;

				for (List<String> oCurrentRecord : p_listRowsOfAllTables)
				{
					if (oCurrentRecord.size() < 1)
					{
						log.error("No data at current Row");
						continue;
					}
					
					sCurrentRowKey = ReplaceNewLineChar(oCurrentRecord, 0);
					
					//These are Table Sections with EXPLICIT Table Names
					if (sCurrentRowKey.equalsIgnoreCase(TABLENAME_REPORT_INFORMATION) 
							|| sCurrentRowKey.equalsIgnoreCase(TABLENAME_PRODUCT_INFORMATION)
							|| sCurrentRowKey.equalsIgnoreCase(TABLENAME_PATIENT_INFORMATION)
							|| sCurrentRowKey.equalsIgnoreCase(TABLENAME_LINK_DUPLICATE_REPORT_INFORMATION) 
							|| (sCurrentRowKey.contains(TABLENAME_ADVERSE_REACTION_TERM_INFORMATION)))
					{
						sTemp = createColumnKey(sCurrentRowKey);
						addColumnValue(oDataTable, sTemp, StringConstants.EMPTY, setHeaderContent);
						
						//Prepare for first table within this Table Section
						listTableHeaderRow.clear();
						nTableRowCounter = 0;
						
						continue;
					}
					else if(sCurrentRowKey.equalsIgnoreCase(TABLECOLUMNHEADER_PRODUCT_DESCRIPTION) || sCurrentRowKey.equalsIgnoreCase(TABLECOLUMNHEADER_ADVERSE_REACTION_TERM_S))
					{
						listTableHeaderRow.clear();
						nTableRowCounter=0;
					}
					
					if (nTableRowCounter == 0)
					{
						//DEV-NOTE: This represents Header row for a Table with the EXCEPTION for table containing 'Death' info
						
						for (int nColIndex = 0; nColIndex < oCurrentRecord.size(); nColIndex++)
						{
							listTableHeaderRow.add(ReplaceNewLineChar(oCurrentRecord, nColIndex));
						}
						nTableRowCounter++;
					}
					else
					{
						//DEV-NOTE: This represents Data row for a Table with the EXCEPTION for table containing 'Death' info
						
						if (listTableHeaderRow.get(0).equalsIgnoreCase(TABLECOLUMNHEADER_ADVERSE_REACTION_REPORT_NUMBER)
								|| listTableHeaderRow.get(0).equalsIgnoreCase(TABLECOLUMNHEADER_SERIOUS_REPORT)
								|| listTableHeaderRow.get(0).equalsIgnoreCase(TABLECOLUMNHEADER_AGE))
						{
							copyAllColumnValues(oCurrentRecord, listTableHeaderRow, oDataTable, setHeaderContent);
							
							
							
							//Table processing complete, reset for next table
							listTableHeaderRow.clear();
							nTableRowCounter = 0;
							
							continue;
						}
						else if(listTableHeaderRow.get(0).equalsIgnoreCase(TABLECOLUMNHEADERKEY_DEATH))
						{
							List<String> listRefurbishedHeaderRow = new ArrayList<String>();
							List<String> listRefurbishedDataRow = new ArrayList<String>();
							
							listRefurbishedHeaderRow.add(DEATH);
							listRefurbishedHeaderRow.add(DISABILITY);
							listRefurbishedHeaderRow.add(CONGENITAL_ANOMALY);
							listRefurbishedHeaderRow.add(LIFE_THREATENING);
							listRefurbishedHeaderRow.add(HOSPITALIZATION);
							listRefurbishedHeaderRow.add(OTHER_MEDICALLY_IMPORTANT_CONDITIONS);
							
							if(listTableHeaderRow.size()==6 && oCurrentRecord.size()==6)
							{
							listRefurbishedDataRow.add(getColValueForCurrentRecord(listTableHeaderRow, 1));	//Value for 'Death'
							listRefurbishedDataRow.add(getColValueForCurrentRecord(listTableHeaderRow, 3));	//Value for 'Disability'
							listRefurbishedDataRow.add(getColValueForCurrentRecord(listTableHeaderRow, 5));	//Value for 'Congenital Anomaly'
							listRefurbishedDataRow.add(getColValueForCurrentRecord(oCurrentRecord, 1));		//Value for 'Life Threatening'
							listRefurbishedDataRow.add(getColValueForCurrentRecord(oCurrentRecord, 3));		//Value for 'Hospitalization'
							listRefurbishedDataRow.add(getColValueForCurrentRecord(oCurrentRecord, 5));		//Value for 'Other Medically Important Conditions'
							}
							else
							{
								log.error("Insufficient Values for Death Table");
								throw new SciException("Insufficient values for Death Table");
							}
							copyAllColumnValues(listRefurbishedDataRow, listRefurbishedHeaderRow, oDataTable, setHeaderContent);
							
							//Table processing complete, reset for next table
							listTableHeaderRow.clear();
							nTableRowCounter = 0;
							
							continue;
						}
						else if (listTableHeaderRow.get(0).equalsIgnoreCase(TABLECOLUMNHEADER_RECORD_TYPE))
						{
							linkDupRecordCount++;
							
							sTemp = TABLEHEADER_LINK_DUPLICATE_RECORD + linkDupRecordCount;
							addColumnValue(oDataTable, sTemp, StringConstants.EMPTY, setHeaderContent);
							
							copyAllColumnValues(TABLEHEADER_LINK_DUPLICATE_RECORD, linkDupRecordCount, oCurrentRecord, 
									listTableHeaderRow, oDataTable, setHeaderContent);
							
						}
						else
						{
							if(listTableHeaderRow.get(0).equalsIgnoreCase(TABLECOLUMNHEADER_PRODUCT_DESCRIPTION))
							{
								prodCount++;
								sTemp=PRODUCTKEYPREFIX_PRODUCT+prodCount;
								addColumnValue(oDataTable, sTemp, StringConstants.EMPTY, setHeaderContent);
								copyAllColumnValues(PRODUCTKEYPREFIX_PRODUCT, prodCount, oCurrentRecord, listTableHeaderRow, oDataTable, setHeaderContent);
							}
							else if(listTableHeaderRow.get(0).equalsIgnoreCase(TABLECOLUMNHEADER_ADVERSE_REACTION_TERM_S))
							{
								eventCount++;
								sTemp=EVENTKEYPREFIX_EVENT+eventCount;
								addColumnValue(oDataTable, sTemp, StringConstants.EMPTY, setHeaderContent);
								copyAllColumnValues(EVENTKEYPREFIX_EVENT, eventCount, oCurrentRecord, listTableHeaderRow, oDataTable, setHeaderContent);
								
							}
							
							else
							{
							}
							
						}
						
						
					}
					
				}//for loop
				
				//This section deals with POST-EXTRACTION logic
				
				
				if(oDataTable.isColumnPresent(AGEKEY_REPORT1_AGE))
				{
					String age=null;
					String ageUnit=null;
					List<ParsedDataValue> ageValues=oDataTable.getColumnValues(AGEKEY_REPORT1_AGE);
					String[] ageArray=((String) ageValues.get(ageValues.size()-1).rawValue()).split(" ");
					if(ageArray.length==2)
					{
						age=ageArray[0];
						ageUnit=ageArray[1];
					}
					else
					{
						age=StringConstants.EMPTY;
						ageUnit=StringConstants.EMPTY;
					}
					addColumnValue(oDataTable, PATIENT_AGE, age, setHeaderContent);
					addColumnValue(oDataTable, PATIENT_AGE_UNIT, ageUnit, setHeaderContent);
				}
				
				addColumnValue(oDataTable, PRODUCT_COUNT, prodCount, setHeaderContent);
				addColumnValue(oDataTable, EVENT_COUNT, eventCount, setHeaderContent);
				addColumnValue(oDataTable, LINK_DUP_RECORD_COUNT, linkDupRecordCount, setHeaderContent);
			
		}
		catch (Exception ioExcep)
		{
			log.error("Error while creating DataTable ", ioExcep);
			throw new SciException("Error while creating DataTable", ioExcep);
		}
		finally
		{
			
		}
		}
		
		
		
		return oDataTable;
	}
	
	private void ReformAndAddDose(ParsedDataTable oDataTable,String sTemp, Object oColumnValue,Set<String> setHeaderContent)
	{	
		String drug=null;
		String drugUnit=null;
		String drugValues=(String)oColumnValue;
		if(drugValues.trim()!=null && !drugValues.trim().equals(""))
		{
			String[] drugArray=drugValues.split(" ");
			if(drugArray.length==2)
		{
		drug=drugArray[0];
		drugUnit=drugArray[1];
		
		}
		else if(drugArray.length>2)
		{
			drug=drugArray[0];
			drugUnit=drugArray[1]+drugArray[2];
			
		}
		}
		else
		{
			drug=StringConstants.EMPTY;
			drugUnit=StringConstants.EMPTY;
			
		}
	//	addColumnValue(oDataTable, sTemp, drug, setHeaderContent);
	//	addColumnValue(oDataTable, sTemp+" Unit", drugUnit, setHeaderContent);
		addColumnValue(oDataTable, sTemp, drugValues, setHeaderContent);
		
	}
	private String createColumnKey(String sKey)
	{
		return createColumnKey(sKey, null, 0);
	}
	
	private String createColumnKey(String sKey, String sAdditionalKeyPrefix, int nAdditionalKeyPrefixValue)
	{
		if (sAdditionalKeyPrefix != null)
		{
			return KEYPREFIX_REPORT1_DOT + sAdditionalKeyPrefix + nAdditionalKeyPrefixValue + StringConstants.PERIOD + sKey.replace(StringConstants.CARRIAGERETURN, StringConstants.EMPTY);
		}
		else
		{
			return KEYPREFIX_REPORT1_DOT + sKey.replace(StringConstants.CARRIAGERETURN, StringConstants.EMPTY);
		}
	}
	
	private String getColValueForCurrentRecord(List<String> oCurrentRecord, int nColIndex)
	{
		//need to commment this new line character replacement
		return oCurrentRecord.get(nColIndex).trim();//.replace(StringConstants.CARRIAGERETURN, StringConstants.SPACE);
	}
	private String ReplaceNewLineChar(List<String> oCurrentRecord, int nColIndex)
	{
		return oCurrentRecord.get(nColIndex).trim().replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE).trim();
	}
	private void copyAllColumnValues(List<String> oCurrentRecord, List<String> listTableHeaderRow, 
			ParsedDataTable oDataTable, Set<String> setHeaderContent)
	{
		copyAllColumnValues(null, 0, oCurrentRecord, listTableHeaderRow, oDataTable, setHeaderContent);
	}
	
	private void copyAllColumnValues(String sAdditionalKeyPrefix, int nAdditionalKeyPrefixValue, List<String> oCurrentRecord, 
			List<String> listTableHeaderRow, 
			ParsedDataTable oDataTable, Set<String> setHeaderContent)
	{
		String sTemp = null;
		Object oColumnValue = null;
		for (int nColIndex = 0; nColIndex < oCurrentRecord.size(); nColIndex++)
		{
			sTemp = createColumnKey(listTableHeaderRow.get(nColIndex), sAdditionalKeyPrefix, nAdditionalKeyPrefixValue);
			
			oColumnValue = getColValueForCurrentRecord(oCurrentRecord, nColIndex);
			if(sTemp.contains(TABLECOLUMNHEADER_DOSE_PREFIX_REPORT1_PRODUCT) && sTemp.contains(TABLECOLUMNHEADER_DOSE))
			{
				ReformAndAddDose(oDataTable, sTemp, oColumnValue, setHeaderContent);

			}
			else
			{
				addColumnValue(oDataTable, sTemp, oColumnValue, setHeaderContent);
			}

		}
	}
	
	
	private boolean validateFileParse(String filePath)
	{
		String sFileContent=null;
		try {
			
		if(m_nStartPageNumber==0 && m_nEndPageNumber==0)
		{
			
			sFileContent = PdfUtils.getFileText(filePath,(m_nStartPageNumber+1),m_NumberOfPages);
		}
		else
		{
			sFileContent = PdfUtils.getFileText(filePath,m_nStartPageNumber,m_NumberOfPages);
		}
		} catch (SciException e) {
			log.error("Error While Parsing"+e.getErrorMessage());
		}
		
		if((sFileContent.contains(TABLECOLUMNHEADER_ADVERSE_REACTION_REPORT_NUMBER) && 
				sFileContent.contains(TABLECOLUMNHEADER_SERIOUS_REPORT) )||
				sFileContent.contains(TABLECOLUMNHEADER_PRODUCT_DESCRIPTION) ||
				sFileContent.contains(TABLECOLUMNHEADER_ADVERSE_REACTION_TERM_S))
		{
			
			return true;
		}
		else
		{
			return false;
		}
	}
	private void addNonTabularKeysToDataTable(String filePath,ParsedDataTable oDataTable, Set<String> setHeaderContent)
	{
		String sFileContent=null;
		String reportRuntime=null;
		
		try {
			sFileContent = PdfUtils.getFileText(filePath);
		} catch (SciException e) {
			log.error("Error [parsing file"+e.getErrorMessage());
		}
		
		reportRuntime=sFileContent.substring(sFileContent.indexOf(REPORTRUNTIME_FETCH_PREFIX_TOTAL_NUMBER_OF_REPORTS)+25,sFileContent.indexOf(REPORTRUNTIME_FETCH_SUFFIX_REPORT_S)+1);
		reportRuntime=reportRuntime.substring(0,reportRuntime.lastIndexOf(" - ")).trim();
		
		addColumnValue(oDataTable, REPORT_RUNTIME_KEY, reportRuntime, setHeaderContent);
		addColumnValue(oDataTable, COUNTRY_KEY, "Canada", setHeaderContent);
		addColumnValue(oDataTable, FILE_TYPE_KEY, HEALTH_CANADA_FILETYPE_VALUE, setHeaderContent);
	
		
	}
	
	public ParsedDataTable MapAutomatedKeysToMnemonics(ParsedDataTable oDataTable)
	{
		 ParsedDataTable pDataTable=new ParsedDataTable();
			 String value=null;
			 String propKeyaAsValue=null;
			Properties oKeyProp=new Properties();
			Set<String> setHeaderContent = new HashSet<String>();
			oKeyProp = FileConfigurationHome.getParsePropertiesByKey(SciConstants.XMLParseConstant.PARSER_PROPERTIES_BA_KEY);
			
	    		 int productCount=Integer.parseInt(oDataTable.getColumnValues("Product Count").get(0).displayValue().toString());
	    		 int eventCount=Integer.parseInt(oDataTable.getColumnValues("Event Count").get(0).displayValue().toString());
	    		 int linkDupRecordCount=Integer.parseInt(oDataTable.getColumnValues("LinkDupRecord Count").get(0).displayValue().toString());
	    		
	    		 
	    		 for(String key : m_oParsingConfiguration.getFieldMap().keySet())
	    			{
	    				
	    			 propKeyaAsValue = oKeyProp.getProperty(key);
	    		 if(propKeyaAsValue == null)
	    		 {
	    			 addColumnValue(pDataTable, key, StringConstants.EMPTY, setHeaderContent);
	    		 }
	    		 else
	    		 {
	  /*  		 while(apotexIterator.hasNext())
	    		 {
	    			 	 key=(String) apotexIterator.next();
	    				 propKeyaAsValue=oKeyProp.getProperty(key);*/
	    			 
	    			 if(key.equals("ProductDescription") || key.equals("HealthProductRole") || key.equals("DosageForm")
	    					 || key.equals("RouteOfAdministration") || key.equals("Dose")
	    					 || key.equals("Frequency") || key.equals("TherapyDuration")
	    					 || key.equals("Indications"))
	    			 {
	    				 createMultipleKey(key, propKeyaAsValue, oDataTable, pDataTable, productCount, setHeaderContent);
	    			 }
	    			 else if(key.equals("AdverseReactionTerms") || key.equals("MedDRAVersion") || key.equals("ReactionDuration"))
	    			 {
	    				
	    				 createMultipleKey(key, propKeyaAsValue, oDataTable, pDataTable, eventCount, setHeaderContent);
	    			 }
	    			 else if(key.equals("RecordType") || key.equals("LinkAERNumber"))
	    			 {
	    				 createMultipleKey(key, propKeyaAsValue, oDataTable, pDataTable, linkDupRecordCount, setHeaderContent);
	    			 }
	    			 else if(key.contains("ReportRuntime") || key.contains("InitialReceivedDate") || key.contains("LatestReceivedDate"))
	    			 {
	    				
	    				 
	    				 if(oDataTable.isColumnPresent(propKeyaAsValue))
	    				 {
	    						 
	        				 value=(String) oDataTable.getColumnValues(propKeyaAsValue).get(0).displayValue();
	        			
	        				
	        					 
	        				 Date preDate=null;
	        				 String date=null;
	        				 SimpleDateFormat preFormat=null;
							try {
								if(value.contains("-"))
								{
								 if(value.split("-")[1].length()==2)
									 preFormat=new SimpleDateFormat("yyyy-MM-dd");
								 else
									 preFormat=new SimpleDateFormat("dd-MMM-yyyy");

								}
								else if(value.contains("/"))
								{
									 if(value.split("/")[1].length()==2)
										 preFormat=new SimpleDateFormat("yyyy/MM/dd");
									 else
										 preFormat=new SimpleDateFormat("dd/MMM/yyyy");

									}
								
								//format = new SimpleDateFormat("yyyy-MM-dd")
								preDate =preFormat.parse(value);
								SimpleDateFormat postFormat=new SimpleDateFormat("dd-MMM-yyyy");
								date=postFormat.format(preDate);
								
							} catch (ParseException e) {
								log.error("Error Parsing file"+e.getMessage());
	        				 }
	        					 
							addColumnValue(pDataTable, key, date, setHeaderContent);
	    				 }
	    			 }
	    			 else
	    			 {
	    			 if(oDataTable.isColumnPresent(propKeyaAsValue))
					 {
							 
	    				 value=oDataTable.getColumnValues(propKeyaAsValue).get(0).displayValue().toString();
	    				 addColumnValue(pDataTable, key, value, setHeaderContent);
					 }
					 else
						{
						 addColumnValue(pDataTable, key, StringConstants.EMPTY, setHeaderContent);
						}
	    			 } 
	    		 }
	    		}
			 
			 return pDataTable;
	}
	

	@Override
	public ValidationState applyValidationRules(String p_sMnemonicName, String displayValue)
	{
		FileParsingField mfield = null;
		ValidationState vState = null;
		String validationRule = null;
		String[] legalValues = null;
	    String editedMnemonicPrefix = null;
	    String editedMnemonicSuffix = null;
	    boolean	matchedFlag = false;
	    String regex = null;
	    ValidationState vFailedStatusType;
	    boolean isMandatory = false;
	    
	    String pattern = "[0-9]*";
	    
		if(p_sMnemonicName.contains("_"))
		{
			editedMnemonicPrefix = p_sMnemonicName.substring(0,p_sMnemonicName.lastIndexOf("_"));
			editedMnemonicSuffix = p_sMnemonicName.substring(p_sMnemonicName.lastIndexOf("_"));
			if(editedMnemonicSuffix.length()>=2)
			{
				matchedFlag	=	Pattern.compile(pattern).matcher(editedMnemonicSuffix.substring(1)).matches();
			}
		}
		if(matchedFlag)
		{
			mfield = m_oParsingConfiguration.getField(editedMnemonicPrefix);
		}
		else
		{
			mfield = m_oParsingConfiguration.getField(p_sMnemonicName);
		}
	    if(mfield == null || displayValue == null || displayValue.equalsIgnoreCase(""))
	    {
	    	vState = ValidationState.UNVALIDATED;
	    }
	    else
	    {
		validationRule = mfield.getValidationRule();
		
		isMandatory = mfield.isMandatory();
		legalValues = mfield.getLegalValues();
		if(mfield.getErrorType().equals("WARN"))
			vFailedStatusType = ValidationState.VALIDATED_WITH_WARNINGS;
		else
			vFailedStatusType = ValidationState.REQUIRED_REVIEW;
		
		if(isMandatory && displayValue.equalsIgnoreCase(""))
		{
			vState = vFailedStatusType;
			
		}
		else
		{
		if(validationRule.equalsIgnoreCase("DATE"))
		{
			if(ValidationUtils.validateField(displayValue.toUpperCase(), SciConstants.RegExConstants.DATE))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("ALPHABETS"))
		{
			if(ValidationUtils.validateField(displayValue, "^[A-Za-z ]*"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("NUMBER"))
		{
			if(ValidationUtils.validateField(displayValue, SciConstants.RegExConstants.INTEGER))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("GENERAL TEXT"))
		{
			vState = ValidationState.VALIDATION_NOT_APPLICABLE;
			
		}
		else if(validationRule.equalsIgnoreCase("ALPHANUMERIC"))
		{
			if(ValidationUtils.validateField(displayValue, "^[A-Za-z0-9 ]*$"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
			
		}
		else if(validationRule.equalsIgnoreCase("DATETIME"))
		{//have to change the regex for datetime
			if(ValidationUtils.validateField(displayValue, "^(([0-3]{1}[0-9]{1})?\\-?[a-z,A-Z]{3}?\\-[0-9]{4}?\\ [0-9]{2}?\\:[0-9]{2}?\\:[0-9]{2}?)+$"))
				vState = ValidationState.VALIDATED;
		else
				vState = vFailedStatusType;
	
	    }
		else if(validationRule.equalsIgnoreCase("DATETIMEMILLISEC"))
		{//have to change the regex for datetime
			if(ValidationUtils.validateField(displayValue, "^(([0-3]{1}[0-9]{1})?\\-?[a-z,A-Z]{3}?\\-[0-9]{4}?\\ [0-9]{2}?\\:[0-9]{2}?\\:[0-9]{2}?\\.[0-9]{3}?)+$"))
				vState = ValidationState.VALIDATED;
    		 else
				vState = vFailedStatusType;
    				
    			 }
		else if(validationRule.equalsIgnoreCase("LEGAL VALUES"))
    				 {
			//legalValues = mfield.getM_sarrLegalValues();
			//errorType = mfield.getErrorType();
			if(legalValues.length==1 && legalValues[0].contains("\n "))
							{
				legalValues[0] = legalValues[0].replace("\n", "").trim();

							}
			for(int index =0; index<legalValues.length; index++)
							{
				legalValues[index]=legalValues[index].toUpperCase();
								}
			if(legalValues!= null && (!legalValues[0].equals("")) && (!displayValue.equalsIgnoreCase("")) &&(!Arrays.asList(legalValues).contains( displayValue.toUpperCase())))
        				 {
				vState = vFailedStatusType;
        				 }
			else if(legalValues!= null && (!legalValues[0].equals("")) && (!displayValue.equalsIgnoreCase("")) &&(Arrays.asList(legalValues).contains( displayValue.toUpperCase())))
				vState = ValidationState.VALIDATED;
        				 else
				vState = ValidationState.UNVALIDATED;
    			 }
		else if(validationRule.equalsIgnoreCase("CUSTOMIZE"))
    			 {
			legalValues = mfield.getLegalValues();
			if(legalValues!= null)
			regex = legalValues[0];
			else
				regex = "";
			matchedFlag = Pattern.compile(regex).matcher(displayValue).matches();
			if(displayValue.equalsIgnoreCase(""))
				 {
				vState = ValidationState.UNVALIDATED;
				 }
			else if(matchedFlag)
				 {
				vState = ValidationState.VALIDATED;
    				 }
			else if((!displayValue.equalsIgnoreCase("")) && !matchedFlag)
				vState = vFailedStatusType;
    					 
    				 }
				 else
					{
			vState = ValidationState.UNVALIDATED;
    		 }
    		}
    		
             }
	   return vState;
	}
		 
	private void createMultipleKey(String key,String propKeyAsValue,ParsedDataTable oDataTable, ParsedDataTable pDataTable, int count, Set<String> setHeaderContent)
	{
		String keyBeforeMap=null, keyAfterMap=null;
		String value=null;	
		 String[] keyArray=propKeyAsValue.split("\\.");
		 for(int index=1;index<=count;index++)
		 {
			 
			 
			 if(keyArray.length==3)
			 {
			 keyBeforeMap=keyArray[0]+"."+keyArray[1].substring(0, keyArray[1].length()-1)+index+"."+keyArray[2];
			 keyAfterMap=key+"_"+index;
			 }
			 else
			 {
				 keyBeforeMap=StringConstants.EMPTY;
				 keyAfterMap=StringConstants.EMPTY;
			 }
			 if(oDataTable.isColumnPresent(keyBeforeMap))
			 {
					 
				 value= oDataTable.getColumnValues(keyBeforeMap).get(0).rawValue().toString();
				/*if(pDataTable.isColumnPresent(keyAfterMap))
				 {
					pDataTable.appendColumnValue(keyAfterMap, value);
				 
				 }
				 else
				 {
					 pDataTable.addColumn(keyAfterMap, value);
				 }*/
				addColumnValue(pDataTable, keyAfterMap, value, setHeaderContent); 
			 }
			 else
			 {
				 addColumnValue(pDataTable, keyAfterMap, StringConstants.EMPTY, setHeaderContent);
			 }
		 
		 }
		 
		 addColumnValue(pDataTable, key+"_Count", count, setHeaderContent);
	}

	@Override
	public Map<String, List<String>> compareMultipleDataTable(List<ParsedDataTable> m_DdataTableList) {
		
			String key = null;
			List<String> valueList = null;
			Map<String, List<String>> keyMap = new HashMap<String, List<String>>();
			
			for(String mnemonic : m_oParsingConfiguration.getFieldMap().keySet())
			{
				key = mnemonic;
				//valueList = new ArrayList<String>();
				int maxCount = 1;
				int keyCount = 1;
				if(!m_oParsingConfiguration.getField(mnemonic).isMultivalued())
				{
					//valueList = new ArrayList<String>();
					for(ParsedDataTable oDataTable: m_DdataTableList)
					{
						if(oDataTable.isColumnPresent(mnemonic+"_Count"))
						{
							keyCount =Integer.parseInt(oDataTable.getColumnValues(mnemonic+"_Count").get(0).displayValue().toString());
							if(maxCount < keyCount)
							{
								maxCount = keyCount;
							}
						}
						else
						{
							//multivalued but count field is not present
						}
					}
	/*				if(maxCount > 1)
					{
	*/					for(int index =1; index<= maxCount; index++)
						{
							valueList = new ArrayList<String>();
							if(maxCount==1)
								key = mnemonic;
							else
							key = mnemonic+"_"+index;
							for(ParsedDataTable oDataTable: m_DdataTableList)
							{
								if(oDataTable.isColumnPresent(mnemonic+"_"+index))
								{
									valueList.add(oDataTable.getColumnValues(mnemonic+"_"+index).get(0).displayValue().toString());
									
								}
								else
								{
									if(index == 1)
									{
										if(oDataTable.isColumnPresent(mnemonic))
										{
											valueList.add(oDataTable.getColumnValues(mnemonic).get(0).displayValue().toString());
											
										}	
										else
											valueList.add(StringConstants.EMPTY);
											
									}
									else
									{
										valueList.add(StringConstants.EMPTY);
									}
									//multivalued but count field is not present
								}
							}
							keyMap.put(key, valueList);
						}
					//}
				}
				else
				{
					valueList = new ArrayList<String>();
					for(ParsedDataTable oDataTable: m_DdataTableList)
					{
						if(oDataTable.isColumnPresent(mnemonic))
						{
							valueList.add(oDataTable.getColumnValues(mnemonic).get(0).displayValue().toString());
						}
						else
						{
							valueList.add(StringConstants.EMPTY);
						}
							
					}
					keyMap.put(key, valueList);
				}
				
				
			}
			//addingFileNames to keyMap
			valueList = new ArrayList<String>();
			for(ParsedDataTable oDataTable: m_DdataTableList)
			{
				if(oDataTable.isColumnPresent("FileName"))
				{
					valueList.add(oDataTable.getColumnValues("FileName").get(0).displayValue().toString());
				}
				else
				{
					valueList.add(StringConstants.EMPTY);
				}
					
			}
			keyMap.put("FileName", valueList);
			
			return keyMap;
		}

	
	
}
