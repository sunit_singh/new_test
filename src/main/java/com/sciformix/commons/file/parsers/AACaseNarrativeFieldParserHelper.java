package com.sciformix.commons.file.parsers;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

class AACaseNarrativeFieldParserHelper
{
	
	private static final String CASE_NOTES_ENGLISH_LANGUAGE_KEY = "Case Notes English Language";
	private static final String VERBATIM_ENGLISH_LANGUAGE_KEY = "Verbatim English Language";

	public static class IdentifiedKey
	{
		private String sKey = null;
		private int nKeyInstanceNumber = 0;
		private int nKeyStartLocation = 0;
		private int nKeyEndLocation = 0;
		
		public IdentifiedKey(String sKey, int nKeyInstance, int nKeyStartLocation)
		{
			this.sKey = sKey;
			this.nKeyInstanceNumber = nKeyInstance;
			this.nKeyStartLocation = nKeyStartLocation;
			this.nKeyEndLocation = nKeyStartLocation + sKey.length();
		}
		
		public String key()
		{
			return sKey;
		}
		
		public int instanceNumber()
		{
			return nKeyInstanceNumber;
		}
		
		public int startLocation()
		{
			return nKeyStartLocation;
		}
		
		public int endLocation()
		{
			return nKeyEndLocation;
		}
		
		public String toString()
		{
			return sKey + "(" + nKeyInstanceNumber + ":" + nKeyStartLocation + "-" + nKeyEndLocation + ")";
		}
	}
	
	public static boolean doesThisKeyOverlapExistingKey(SortedMap<Integer, IdentifiedKey> listOfFindings, String sCurrentKey, int nIdentifiedLocation)
	{
		boolean bOverlappingKey = false;
		
		for (IdentifiedKey oExistingKey : listOfFindings.values())
		{
			if (oExistingKey.endLocation() < nIdentifiedLocation)
			{
				//Existing key 'ends' before current key 'starts'
				//Continue looping forward
				continue;
			}
			else
			{
				if (oExistingKey.startLocation() > (nIdentifiedLocation+sCurrentKey.length()))
				{
					//Existing key 'starts' after current key 'ends'
					//Break as no overlap possible
					break;
				}
				else
				{
					//Existing key and current key overlap
					//Break as overlap detected
					bOverlappingKey = true;
					break;
				}
			}
			
		}
		
		return bOverlappingKey;
	}
	public static SortedMap<Integer, IdentifiedKey> removeOverlappedKey(SortedMap<Integer, IdentifiedKey> listOfFindings)
	{
		IdentifiedKey oExistingKey = null;
		String sCurrentKey = null;
		boolean overLapped = true;
		Integer nIdentifiedLocation = null;
		SortedMap<Integer, IdentifiedKey> modifiedListOfFinding = new TreeMap<Integer, IdentifiedKey>();
		
		for (IdentifiedKey sKey : listOfFindings.values())
		{
			overLapped = false;
			sCurrentKey = sKey.sKey;
			nIdentifiedLocation = sKey.startLocation(); 
		
		for (Entry<Integer, IdentifiedKey>  entry: listOfFindings.entrySet())
		{
			oExistingKey = entry.getValue();
			if ((!oExistingKey.sKey.equalsIgnoreCase(sCurrentKey)) && oExistingKey.startLocation() <= nIdentifiedLocation && nIdentifiedLocation < oExistingKey.endLocation())
			{
				//currentKey is in between existing key
				//hence discard
				overLapped = true;
				break;
			}
			/*else if(oExistingKey.sKey.equalsIgnoreCase(sCurrentKey) && nIdentifiedLocation != sStartLocation)
			{
				overLapped = false;
			}*/
			else
			{
				overLapped = false;
			}
			
		}
		
		if(!overLapped)
		{
			modifiedListOfFinding.put(nIdentifiedLocation, sKey);
		}
		
		}
		
		return modifiedListOfFinding;
	}

	
	/*public static Boolean doesThisKeyOverlapWithEnglishText(String textToParse, String sCurrentKey, int nIdentifiedLocation)
	{
		boolean overlapFlag = false;
		int startIndexOfverbatimEngLangKey = 0;
		int startIndexOfCaseNoteEngLangKey = 0;
		int endIndexOfverbatimEngLangText = 0;
		int endIndexOfCaseNoteEngLangText = 0;
		String caseNoteEngLangKey = null;
		String verbatimEngLangKey = null;
		List<String> keyList = null;
		
		keyList = getKeyListForEnglishTextIdentification();
		verbatimEngLangKey = VERBATIM_ENGLISH_LANGUAGE_KEY;
		startIndexOfverbatimEngLangKey = textToParse.indexOf(verbatimEngLangKey+":");
		endIndexOfverbatimEngLangText = getEndIndexForGivenKey(textToParse, keyList);
		keyList.clear();
		
		keyList = getKeyListForCaseNoteEnglish();
		caseNoteEngLangKey = CASE_NOTES_ENGLISH_LANGUAGE_KEY;
		startIndexOfCaseNoteEngLangKey = textToParse.indexOf(caseNoteEngLangKey+":");
		endIndexOfCaseNoteEngLangText = getEndIndexForGivenKey(textToParse, keyList);
		keyList.clear();
		
		if(!sCurrentKey.equals(VERBATIM_ENGLISH_LANGUAGE_KEY) && !sCurrentKey.equals(CASE_NOTES_ENGLISH_LANGUAGE_KEY))
		{
			if(startIndexOfverbatimEngLangKey>0 && startIndexOfverbatimEngLangKey < endIndexOfverbatimEngLangText 
					&& nIdentifiedLocation>=startIndexOfverbatimEngLangKey && nIdentifiedLocation<=endIndexOfverbatimEngLangText)
			{
				// if in between verbatim english Language Text start and end location, ignore it
				overlapFlag = true;
				return overlapFlag;
			}
			else if(startIndexOfCaseNoteEngLangKey>0 && startIndexOfCaseNoteEngLangKey < endIndexOfCaseNoteEngLangText && 
					nIdentifiedLocation>= startIndexOfCaseNoteEngLangKey && nIdentifiedLocation<=endIndexOfCaseNoteEngLangText)
			{
				// if in between case note english Language Text start and end location, ignore it
				overlapFlag = true;
				return overlapFlag;
			}
			else
			{
				//when location is not overlapping
			}
		}
		
		return overlapFlag;
	}
	
	public static List<String> getKeyListForCaseNoteEnglish()
	{
		List<String> keyList = new ArrayList<String>();
		keyList.add("External Source");
		keyList.add("External Source Number");
		return keyList;
	}
	public static int getEndIndexForGivenKey(String textToParse, List<String> keyList)
	{
		int endIndex = 0;
		
		for(String key:keyList)
		{
			if(textToParse.indexOf(key+":")>0)
			{
				endIndex = textToParse.indexOf(key+":");
				break;
			}
		}
		if(endIndex < 0)
		{
			endIndex = textToParse.length()-1;
		}
		
		return endIndex;
	}
	
	
	public static List<String> getKeyListForEnglishTextIdentification()
	{
		List<String> keyListForvarbatimEnglishTextIdentification = new ArrayList<String>();
		keyListForvarbatimEnglishTextIdentification.add("Type of Communication");
		keyListForvarbatimEnglishTextIdentification.add("Source");
		keyListForvarbatimEnglishTextIdentification.add("Sub source");
		keyListForvarbatimEnglishTextIdentification.add("Phone Call Source");
		keyListForvarbatimEnglishTextIdentification.add("Other Source Sub source");
		keyListForvarbatimEnglishTextIdentification.add("Product Name");
		keyListForvarbatimEnglishTextIdentification.add("Product Code");
		keyListForvarbatimEnglishTextIdentification.add("Product Article Number");
		keyListForvarbatimEnglishTextIdentification.add("Product Formula");
		keyListForvarbatimEnglishTextIdentification.add("Product Regulatory Class");
		keyListForvarbatimEnglishTextIdentification.add("Product Local Market Status");
		keyListForvarbatimEnglishTextIdentification.add("Product Same/Similar");
		keyListForvarbatimEnglishTextIdentification.add("Product Country Marketed");
		keyListForvarbatimEnglishTextIdentification.add("Product Quantity");
		keyListForvarbatimEnglishTextIdentification.add("Product Lot Number");
		keyListForvarbatimEnglishTextIdentification.add("Product Lot Unavailability Reason");
		keyListForvarbatimEnglishTextIdentification.add("Product Expiry Date");
		keyListForvarbatimEnglishTextIdentification.add("Product Country Purchased");
		keyListForvarbatimEnglishTextIdentification.add("Consumer Initials");
		keyListForvarbatimEnglishTextIdentification.add("Consumer Number");
		keyListForvarbatimEnglishTextIdentification.add("Consumer Level");
		keyListForvarbatimEnglishTextIdentification.add("Customer Type");
		keyListForvarbatimEnglishTextIdentification.add("Consumer Complaint Reporting Role");
		keyListForvarbatimEnglishTextIdentification.add("Consumer Country");
		keyListForvarbatimEnglishTextIdentification.add("Primary Consumer Country");
		keyListForvarbatimEnglishTextIdentification.add("Case Notes English Language");
		keyListForvarbatimEnglishTextIdentification.add("External Source");
		keyListForvarbatimEnglishTextIdentification.add("External Source Number");
		
		return keyListForvarbatimEnglishTextIdentification;
	}
	
	*/

	
/*	private static void loadNarrativeKeys(List<String> listOfNarrativeKeys)
	{
		
		addKey(listOfNarrativeKeys, "Consumer Available");
		addKey(listOfNarrativeKeys, "Reason for Unavailability");
		addKey(listOfNarrativeKeys, "Activity Number");
		addKey(listOfNarrativeKeys, "Activity Type");
		addKey(listOfNarrativeKeys, "Activity Category");
		addKey(listOfNarrativeKeys, "Activity Awareness Date");
		addKey(listOfNarrativeKeys, "Activity Last Modified Date");
		addKey(listOfNarrativeKeys, "Report Type");
		addKey(listOfNarrativeKeys, "PQC");
		addKey(listOfNarrativeKeys, "Reporter Type");
		addKey(listOfNarrativeKeys, "Relationship to Patient");
		addKey(listOfNarrativeKeys, "Patient's Initials");
		addKey(listOfNarrativeKeys, "Patient's Gender");
		addKey(listOfNarrativeKeys, "Route of Administration");
		addKey(listOfNarrativeKeys, "Event Country Occurred");
		addKey(listOfNarrativeKeys, "Permission to Contact");
		addKey(listOfNarrativeKeys, "Patient's DOB");
		addKey(listOfNarrativeKeys, "Patient's Weight");
		addKey(listOfNarrativeKeys, "Patient's Height");
		addKey(listOfNarrativeKeys, "Indication");
		addKey(listOfNarrativeKeys, "First Time Product Used");
		addKey(listOfNarrativeKeys, "Product Start Date");
		addKey(listOfNarrativeKeys, "Product Use Frequency");
		addKey(listOfNarrativeKeys, "Product Dose or Quantity");
		addKey(listOfNarrativeKeys, "Product Stop Date");
		addKey(listOfNarrativeKeys, "Reaction Start Date");
		addKey(listOfNarrativeKeys, "Symptoms Abate After Stop Using");
		addKey(listOfNarrativeKeys, "Product Re-use After Symptoms Abated");
		addKey(listOfNarrativeKeys, "If yes, did the event or symptoms reappear after the patient reuse");
		addKey(listOfNarrativeKeys, "Patient Treatment");
		addKey(listOfNarrativeKeys, "Reaction Outcome");
		addKey(listOfNarrativeKeys, "Reaction Outcome Description");
		addKey(listOfNarrativeKeys, "Reaction Stop Date");
		addKey(listOfNarrativeKeys, "Patient Experienced Similar Events Before");
		addKey(listOfNarrativeKeys, "HCP Contacted");
		addKey(listOfNarrativeKeys, "HCP Treatment");
		addKey(listOfNarrativeKeys, "HCP Contact Information");
		addKey(listOfNarrativeKeys, "Admitted to Hospital Related to the Event");
		addKey(listOfNarrativeKeys, "Date of Hospitalization Admission");
		addKey(listOfNarrativeKeys, "Date of Hospitalization Discharge");
		addKey(listOfNarrativeKeys, "Medical or Lab Tests");
		addKey(listOfNarrativeKeys, "Patient's allergies");
		addKey(listOfNarrativeKeys, "Patient's Allergies Description");
		addKey(listOfNarrativeKeys, "Patient's Medications");
		addKey(listOfNarrativeKeys, "Patient's Medications Indication, Start Date, Dose, Frequency and Last Used Date");
		addKey(listOfNarrativeKeys, "Other Products Applied to Affected Area in Last 4 Days");
		addKey(listOfNarrativeKeys, "Relevant Medical History");
		addKey(listOfNarrativeKeys, "Relevant Medical History Description");
		addKey(listOfNarrativeKeys, "Pregnant");
		addKey(listOfNarrativeKeys, "Pregnancy Due Date");
		addKey(listOfNarrativeKeys, "Was Patient Breastfeeding");
		addKey(listOfNarrativeKeys, "Did Child Have Unfavorable Event");
		addKey(listOfNarrativeKeys, "Is Event Associated with a Miscarriage");
		addKey(listOfNarrativeKeys, "Date and Reported Cause of Miscarriage");
		addKey(listOfNarrativeKeys, "Event Associated with a Birth Defect");
		addKey(listOfNarrativeKeys, "Reported Cause of Birth Defect");
		addKey(listOfNarrativeKeys, "Death");
		addKey(listOfNarrativeKeys, "Date of Death and Reported Cause of Death");
		addKey(listOfNarrativeKeys, "High Level Reason for Delayed Awareness");
		addKey(listOfNarrativeKeys, "Message Date");
		addKey(listOfNarrativeKeys, "Detailed Reason for Delayed Awareness");
		addKey(listOfNarrativeKeys, "GCC Platform Case Number");
		addKey(listOfNarrativeKeys, "Receive Date");
		addKey(listOfNarrativeKeys, "Receipt Date");
		addKey(listOfNarrativeKeys, VERBATIM_ENGLISH_LANGUAGE_KEY);
		addKey(listOfNarrativeKeys, "Type of Communication");
		addKey(listOfNarrativeKeys, "Source");
		addKey(listOfNarrativeKeys, "Sub source");
		addKey(listOfNarrativeKeys, "Phone Call Source");
		addKey(listOfNarrativeKeys, "Other Source Sub source");
		addKey(listOfNarrativeKeys, "Product Name");
		addKey(listOfNarrativeKeys, "Product Code");
		addKey(listOfNarrativeKeys, "Product Article Number");
		addKey(listOfNarrativeKeys, "Product Formula");
		addKey(listOfNarrativeKeys, "Product Regulatory Class");
		addKey(listOfNarrativeKeys, "Product Local Market Status");
		addKey(listOfNarrativeKeys, "Product Same/Similar");
		addKey(listOfNarrativeKeys, "Product Country Marketed");
		addKey(listOfNarrativeKeys, "Product Quantity");
		addKey(listOfNarrativeKeys, "Product Lot Number");
		addKey(listOfNarrativeKeys, "Product Lot Unavailability Reason");
		addKey(listOfNarrativeKeys, "Product Expiry Date");
		addKey(listOfNarrativeKeys, "Product Country Purchased");
		addKey(listOfNarrativeKeys, "Consumer Initials");
		addKey(listOfNarrativeKeys, "Consumer Number");
		addKey(listOfNarrativeKeys, "Consumer Level");
		addKey(listOfNarrativeKeys, "Customer Type");
		addKey(listOfNarrativeKeys, "Consumer Complaint Reporting Role");
		addKey(listOfNarrativeKeys, "Consumer Country");
		addKey(listOfNarrativeKeys, "Primary Consumer Country");
		addKey(listOfNarrativeKeys, CASE_NOTES_ENGLISH_LANGUAGE_KEY);
		addKey(listOfNarrativeKeys, "External Source");
		addKey(listOfNarrativeKeys, "External Source Number");
	}
*/	
	
	/*public static class KeyComparator implements Comparator<String>
	{
		public int compare(String str1, String str2)
		{
			if (str1.length() > str2.length())
			{
				return -1;
			}
			else if (str1.length() < str2.length())
			{
				return 1;
			}
			else
			{
				return str1.compareTo(str2);
			}
		}
	}*/
	
}
