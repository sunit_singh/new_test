package com.sciformix.commons.file.parsers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.parsers.AACaseNarrativeFieldParserHelper.IdentifiedKey;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class ABCaseNarrativeFieldParser {
	private List<String> s_listOfNarrativeKeys = null;
	public ABCaseNarrativeFieldParser(FileParsingConfiguration m_oFileParsingConf)
	{
		this.s_listOfNarrativeKeys = new ArrayList<String>();
		loadNarrativeList(m_oFileParsingConf);
		
		
	}
	//private static List<String> s_listOfNarrativeKeys = E2BCaseNarrativeFieldParserHelper.getListOfNarrativeKeys();
	private void loadNarrativeList(FileParsingConfiguration m_oFileParsingConf)
	{
		/*Properties keyListProp =ConfigurationPropertiesHome.getParsePropertiesByKey(SciConstants.XMLParseConstant.PARSER_PROPERTIES_AA_KEY);
		
		List<String> keyList = m_oFileParsingConf.getMnemonicListByHeader("Narrative");
		String narrativeKeyBeforeHeaderRemoved = null;
		for(String key: keyList)
		{
			if(keyListProp.containsKey(key))
			{
				narrativeKeyBeforeHeaderRemoved = keyListProp.getProperty(key);
				if(narrativeKeyBeforeHeaderRemoved.contains("Narrative."))
				this.s_listOfNarrativeKeys.add(narrativeKeyBeforeHeaderRemoved.replace("B.5.1 Case narrative.",""));
			}*/
		
		this.s_listOfNarrativeKeys.add("Is the event associated with a death?");
		this.s_listOfNarrativeKeys.add("Did the Patient treat themselves?");
		this.s_listOfNarrativeKeys.add("Patient Ethnicity:");
		this.s_listOfNarrativeKeys.add("Height At AE Onset:");
		this.s_listOfNarrativeKeys.add("Known Non Drug Allergies?:");
		this.s_listOfNarrativeKeys.add("Did the Patient treat themselves?");
		this.s_listOfNarrativeKeys.add("Seen in ER?");
		this.s_listOfNarrativeKeys.add("Patient Nursing?");
		this.s_listOfNarrativeKeys.add("Associated Birth Defect?");
		this.s_listOfNarrativeKeys.add("Did event reappear after reintroduction of product?");
		this.s_listOfNarrativeKeys.add("Is the event associated with a miscarriage?");
		this.s_listOfNarrativeKeys.add("Has the patient ever had a similar experience?");
		this.s_listOfNarrativeKeys.add("Previous experience:");
		this.s_listOfNarrativeKeys.add("Diagnostic Data Obtained?::");
		this.s_listOfNarrativeKeys.add("Permission to contact Physician?:");
		this.s_listOfNarrativeKeys.add("Physician Aware of Event:");
		this.s_listOfNarrativeKeys.add("Product Manufactured Country:");
		this.s_listOfNarrativeKeys.add("Was the product packaging recently opened?");
		this.s_listOfNarrativeKeys.add("Did Patient re-introduce product use?");
		this.s_listOfNarrativeKeys.add("Is the patient Physician aware of the product use?");
		this.s_listOfNarrativeKeys.add("First time Patient used Product?:");
			
		
		
		Collections.sort(this.s_listOfNarrativeKeys, new KeyComparator());
	}
	
	public Map<String, StringParsedDataValue> parse(String p_sTextToParse) {
		Map<String, StringParsedDataValue> narrativeKeyMap = null;
		int nKeyStartLocation = -1;
		new TreeMap<Integer, IdentifiedKey>();
		new HashMap<String, Integer>();
		narrativeKeyMap = new TreeMap<String, StringParsedDataValue>();
		String keyValue = null;
		
		for (String sKey : s_listOfNarrativeKeys)
		{
			nKeyStartLocation = p_sTextToParse.indexOf(sKey);
			
			 if(nKeyStartLocation>=0 && p_sTextToParse.indexOf("\r",nKeyStartLocation+sKey.length())>=0 && nKeyStartLocation+sKey.length()<p_sTextToParse.indexOf("\r",nKeyStartLocation+sKey.length()))
			{
				keyValue = p_sTextToParse.substring(nKeyStartLocation+sKey.length(),p_sTextToParse.indexOf("\r",nKeyStartLocation+sKey.length())).trim();
				narrativeKeyMap.put("Narrative."+sKey, new StringParsedDataValue(keyValue,keyValue,DataState.ASIS,ValidationState.UNVALIDATED));
			}
		}
			//find 'CR'(\r) and take substring from 'nKeyStartLocation' to 'end of line' and trim the value to get the key-value 
			//add to 'listOfFindings'/mapKeyInstanceCounters
			

		
		return narrativeKeyMap;
	}
	
	

	
	public static class KeyComparator implements Comparator<String>
	{
		public int compare(String str1, String str2)
		{
			if (str1.length() > str2.length())
			{
				return -1;
			}
			else if (str1.length() < str2.length())
			{
				return 1;
			}
			else
			{
				return str1.compareTo(str2);
			}
		}
	}
}
