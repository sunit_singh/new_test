package com.sciformix.commons.file.parsers; 

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.parsers.AACaseNarrativeFieldParserHelper.IdentifiedKey;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;


public class AACaseNarrativeFieldParser
{
	private List<String> s_listOfNarrativeKeys = null;
	public AACaseNarrativeFieldParser(FileParsingConfiguration m_oFileParsingConf)
	{
		this.s_listOfNarrativeKeys = new ArrayList<String>();
		loadNarrativeList(m_oFileParsingConf);
		
		
	}
	//private static List<String> s_listOfNarrativeKeys = E2BCaseNarrativeFieldParserHelper.getListOfNarrativeKeys();
	private void loadNarrativeList(FileParsingConfiguration m_oFileParsingConf)
	{
		Properties keyListProp = FileConfigurationHome.getParsePropertiesByKey(SciConstants.XMLParseConstant.PARSER_PROPERTIES_AA_KEY);
	
		List<String> keyList = m_oFileParsingConf.getMnemonicListByHeader("Narrative");
		String narrativeKeyBeforeHeaderRemoved = null;
		for(String key: keyList)
		{
			if(keyListProp.containsKey(key))
			{
				narrativeKeyBeforeHeaderRemoved = keyListProp.getProperty(key);
				if(narrativeKeyBeforeHeaderRemoved.contains("B.5.1 Case narrative."))
				this.s_listOfNarrativeKeys.add(narrativeKeyBeforeHeaderRemoved.replace("B.5.1 Case narrative.",""));
			}
			
		}
		
		Collections.sort(this.s_listOfNarrativeKeys, new KeyComparator());
	}
	
	public Map<String, StringParsedDataValue> parse(String p_sTextToParse) {
		Map<String, StringParsedDataValue> narrativeKeyMap = null;
		int nKeyStartLocation = -1;
		SortedMap<Integer, IdentifiedKey> listOfFindings = null;
		Map<String, Integer> mapKeyInstanceCounters = null;
		int nKeyExistingInstanceCount = -1;
		Iterator<Integer> iterKeysFound = null;
		boolean bFirstKeyFound = false;
		int nPreviousKeyEndLocation = -1;
		IdentifiedKey oPreviousKey = null;
		String sKeyValue = null;
		String key = null;
		String value = null;
		ValidationState vState = null;
 		String narrativeJunkText = null;
 		int endIndexOfNarrativeJunkText = 0;
		listOfFindings = new TreeMap<Integer, IdentifiedKey>();
		mapKeyInstanceCounters = new HashMap<String, Integer>();
		narrativeKeyMap = new TreeMap<String, StringParsedDataValue>();
		narrativeJunkText = "B.5.1 Case narrative";
		endIndexOfNarrativeJunkText = p_sTextToParse.indexOf(narrativeJunkText)+narrativeJunkText.length();
		for (String sKey : s_listOfNarrativeKeys)
		{
			nKeyStartLocation = p_sTextToParse.indexOf(sKey + ":");
			
			if(endIndexOfNarrativeJunkText==nKeyStartLocation)
			{
				//do nothing as it is first key hence dont need to prefix with space
			}
			else if(nKeyStartLocation>0)
			{
				nKeyStartLocation = p_sTextToParse.indexOf(" "+sKey + ":")+1;
			}
			
			while (nKeyStartLocation >= 0)
			{
				//Is this location part of another key
				if (AACaseNarrativeFieldParserHelper.doesThisKeyOverlapExistingKey(listOfFindings, sKey, nKeyStartLocation))
				{
					//Ignore this finding
				}
				/*else if(E2BCaseNarrativeFieldParserHelper.doesThisKeyOverlapWithEnglishText(p_sTextToParse,sKey, nKeyStartLocation))
				{
					//ignore if key location is in between the varbatim english Language or Case Note english Language
				}*/
				else
				{
					if (!mapKeyInstanceCounters.containsKey(sKey))
					{
						nKeyExistingInstanceCount = 0;
					}
					else
					{
						nKeyExistingInstanceCount = mapKeyInstanceCounters.get(sKey).intValue();
					}
					listOfFindings.put(new Integer(nKeyStartLocation), new IdentifiedKey(sKey, (nKeyExistingInstanceCount+1), nKeyStartLocation));
					mapKeyInstanceCounters.put(sKey, (nKeyExistingInstanceCount+1));
				}
				
				//Look for the next key instance
				nKeyStartLocation = p_sTextToParse.indexOf(" "+sKey + ":", nKeyStartLocation+sKey.length());
				if(nKeyStartLocation>=0)
					nKeyStartLocation = nKeyStartLocation+1;
				
				/*if(endIndexOfNarrativeJunkText==nKeyStartLocation)
				{
					//do nothing as it is first key hence dont need to prefix with space
				}
				else
				{
					nKeyStartLocation = p_sTextToParse.indexOf(" "+sKey + ":", nKeyStartLocation+sKey.length());
				}*/
			}
		}
		
		//need to handle the overlapping key issue saparately after inserting all keys with its location
		
			
		//listOfFindings = E2BCaseNarrativeFieldParserHelper.removeOverlappedKey(listOfFindings);
		
		iterKeysFound = listOfFindings.keySet().iterator();
		while (iterKeysFound.hasNext())
		{
			nKeyStartLocation = iterKeysFound.next().intValue();
			
			if (!bFirstKeyFound)
			{
				if (nKeyStartLocation > 0)
				{
					//Everything to the left of this has to be DISCARDED
					System.out.println("DISCARDING: '" + p_sTextToParse.substring(0, nKeyStartLocation));
				}
				else
				{
					//Nothing to do
				}
				oPreviousKey = listOfFindings.get(new Integer(nKeyStartLocation));
				nPreviousKeyEndLocation = nKeyStartLocation + (oPreviousKey.key().length()+1);
				bFirstKeyFound = true;
			}
			else
			{
				sKeyValue = p_sTextToParse.substring(nPreviousKeyEndLocation, nKeyStartLocation);
				if (sKeyValue != null)
				{
					sKeyValue = sKeyValue.trim();
				}
				if (mapKeyInstanceCounters.containsKey(oPreviousKey.key()) && mapKeyInstanceCounters.get(oPreviousKey.key()).intValue() > 1)
				{
					key = oPreviousKey.key()+"_"+oPreviousKey.instanceNumber();
					value = sKeyValue;
					vState = ValidationState.UNVALIDATED;
				}
				else
				{
					key = oPreviousKey.key();
					value = sKeyValue;
					vState = ValidationState.UNVALIDATED;
				}
				if(doesKeyValueRequireReview(sKeyValue))
				{
					vState = ValidationState.REQUIRED_REVIEW;
				}
				
				narrativeKeyMap.put(key, new StringParsedDataValue(value, value, DataState.ASIS,vState));
				oPreviousKey = listOfFindings.get(new Integer(nKeyStartLocation));
				nPreviousKeyEndLocation = nKeyStartLocation + (oPreviousKey.key().length()+1);
			}
		}
		
		if (nPreviousKeyEndLocation != -1)
		{
			//Handle the last key
			sKeyValue = p_sTextToParse.substring(nPreviousKeyEndLocation, p_sTextToParse.length());
			if (sKeyValue != null)
			{
				sKeyValue = sKeyValue.trim();
			}
			if (mapKeyInstanceCounters.containsKey(oPreviousKey.key()) && mapKeyInstanceCounters.get(oPreviousKey.key()).intValue() > 1)
			{
				key = oPreviousKey.key()+"_"+oPreviousKey.instanceNumber();
				value = sKeyValue;
				vState = ValidationState.UNVALIDATED;
			//	System.out.println("Key:" + oPreviousKey.key() + "[" + oPreviousKey.instanceNumber() + "]" + "; Value is '" + sKeyValue + "'");
			}
			else
			{
				key = oPreviousKey.key();
				value = sKeyValue;
				vState = ValidationState.UNVALIDATED;
			//	System.out.println("Key:" + oPreviousKey.key() + "; Value is '" + sKeyValue + "'");
			}
			if(doesKeyValueRequireReview(sKeyValue))
			{
				vState = ValidationState.REQUIRED_REVIEW;
			//	System.out.println("\t\tRequires Review");
			}
			narrativeKeyMap.put(key, new StringParsedDataValue(value, value, DataState.ASIS,vState));
		}
		//inserting instance count for the multivalue keys
		for(Entry<String, Integer> entry:mapKeyInstanceCounters.entrySet())
		{
			if(entry.getValue()>1)
			{
				narrativeKeyMap.put(entry.getKey()+"_Count",new StringParsedDataValue(entry.getValue().toString(), entry.getValue().toString(), DataState.ASIS,ValidationState.UNVALIDATED) );
			}
		}
		
		return narrativeKeyMap;
	}
	
	
	//2017-12-11T13:28:22.000Z
	private static String sTimestampRegex ="^([^\\:])*([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z)*([^\\:]*)$"; 
			//"^([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[.][0-9]{3}Z)*([^\\:])*$"; 
//"^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z";
	private static boolean doesKeyValueRequireReview(String sKeyValue)
	{
		boolean bRequiresReview = false;
		
		 if (sKeyValue.indexOf(":") != -1)
		 {
			 if (!sKeyValue.matches(sTimestampRegex))
			 {
				 bRequiresReview = true;
			 }
		 }
		 
		return bRequiresReview;
	}
	
	/*protected void addColumnValue(ParsedDataTable oDataTable, String p_sMnemonicName, Object p_ColumnValue, Set<String> setHeaderContent)
	{
		FileParsingField mfield=null;
		String m_sLabel = null;
		String m_nColumnValue = null;
		DataState dataState= null;
		//TODO: validation rules
		If this is NOT A REGISTERED MNEMONIC, throw it out
		Do inline validation
		mfield = m_oParsingConfiguration.getField(sColumnHeader);
		mfield.getValidationRule();
		
		//assigning currently existing mnemonic value untill validation done
		m_sLabel=p_sMnemonicName;
		m_nColumnValue = p_ColumnValue.toString();
		
		
		if(p_ColumnValue.toString().contains("\r"))
		{
			m_nColumnValue=p_ColumnValue.toString().replaceAll("\r", " ").trim();
			dataState=DataState.PHRASEWRAP;
		}
		else
		{
			m_nColumnValue=p_ColumnValue.toString().trim();
			dataState=DataState.ASIS;
		}
		if(p_sMnemonicName== null || p_sMnemonicName.equals(""))
		{
			
		}
		else
		{
		if (setHeaderContent.contains(p_sMnemonicName))
		{
		//	oDataTable.appendMnemonicColumnValue(p_sMnemonicName, p_ColumnValue.toString(),m_nColumnValue.toString(), DataState.ASIS);
			oDataTable.appendMnemonicColumnValue(p_sMnemonicName, new StringParsedDataValue(p_ColumnValue.toString(), m_nColumnValue.toString(), dataState, ValidationState.UNVALIDATED));
		}
		else
		{
			oDataTable.addColumn(p_sMnemonicName, m_sLabel, new StringParsedDataValue(p_ColumnValue.toString(), m_nColumnValue.toString(), dataState, ValidationState.UNVALIDATED) );
			setHeaderContent.add(p_sMnemonicName);
		}
		}
	}*/
	
	public static class KeyComparator implements Comparator<String>
	{
		public int compare(String str1, String str2)
		{
			if (str1.length() > str2.length())
			{
				return -1;
			}
			else if (str1.length() < str2.length())
			{
				return 1;
			}
			else
			{
				return str1.compareTo(str2);
			}
		}
	}
}
