package com.sciformix.commons.file.parsers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.collections4.map.ListOrderedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.file.BaseFileParser;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;
import com.sciformix.commons.utils.PdfUtils;
import com.sciformix.commons.utils.ValidationUtils;

public class PdfDocParserAvatarAB extends BaseFileParser {
	
	private static final String GENERATED_ON_KEY = "General.Header.Generated On";

	private static final String NARRATIVE_KEY_EVENT_DESCRIPTION_B_5_1_H_1 = "Narrative.Event Description (B.5.1/H.1)";

	private static final String KEY_DATE_OF_AUTOSPY = "Date of Autopsy";

	private static final String EVENT_DESCRIPTION_B_5_1_H_1 = "Event Description (B.5.1/H.1)";

	private static final String DRUG_REACT_RELATEDNESS = "DrugReactRelatedness(";

	private static final String DIAGNOSTIC_RESULTS = "Diagnostic Results(";

	private static final String DRUG_DOSAGES = "Drug Dosages(";

	private static final String DEVICE = "Device(";

	private static final String ADDITIONAL_REFERENCE = "Additional Reference(";

	private static final String OTHER_DIAGNOSTIC_DATA = "Other Diagnostic Data";

	private static final String DIAGNOSTIC_TESTS = "Diagnostic Tests(";

	private static final String MEDICAL_HISTORY2 = "Medical History(";

	private static final String DRUG_HISTORY = "Drug History(";

	private static final String DRUG_ALLERGIES_HISTORY = "Drug Allergies/ History";

	private static final String AUTHORS = "Authors(";

	private static final String NARRATIVE = "Narrative";

	private static final String LITERATURE2 = "Literature(";

	private static final String CURRENT_PREGNANCY_OUTCOMES = "Current Pregnancy Outcomes(";

	private static final String CURRENT_PREGNANCY = "Current Pregnancy";

	private static final String PREVIOUS_PREGNANCY = "Previous Pregnancy";

	private static final String PREGNANCY = "Pregnancy";

	private static final String STUDY = "Study";

	private static final String CAUSALITY_DE_RE_CHALLENGE = "Causality/ DeReChallenge(";

	private static final String STANDARD_INTAKE_QUERIES = "Standard Intake Queries";

	private static final String PROTOCOL = "Protocol";

	private static final String ADVERSE_EVENT = "Adverse Event(";

	private static final String DRUG = "Drug(";

	private static final String PATIENT = "Patient";

	private static final String REPORTER = "Reporter(";

	private static final String SERIOUSNESS_CRITERIA = "Seriousness Criteria";

	private static final String LOT_NUMBERS = "Lot Numbers(";

	private static final String INDICATIONS_FOR_USE = "Indications for Use(";

	private static final String GENERAL = "General";

	private static final Logger log = LoggerFactory.getLogger(PdfDocParserAvatarAB.class);

	private int m_nStartPageNumber = 0;
	private int m_nEndPageNumber = 0;
	private int m_NumberOfPages = 0;
	
	public PdfDocParserAvatarAB(String p_sDocumentLocalPath) {
		this(p_sDocumentLocalPath, 0 ,0, -1, false);
	}
	
	public PdfDocParserAvatarAB(String p_sDocumentLocalPath, boolean p_bTestMode) {

		this(p_sDocumentLocalPath, 0 ,0, -1, p_bTestMode);
		
	}

	public PdfDocParserAvatarAB(String p_sDocumentLocalPath, int p_nStartPageNumber, int p_nEndPageNumber, int p_NumberOfPages) {
		this(p_sDocumentLocalPath, p_nStartPageNumber, p_nEndPageNumber, p_NumberOfPages, false);
	}

	public PdfDocParserAvatarAB(String p_sDocumentLocalPath, int p_nStartPageNumber, int p_nEndPageNumber, int p_NumberOfPages, boolean p_bTestMode) {
		
		super(p_sDocumentLocalPath,"", p_bTestMode);
		
		super.m_oParsingConfiguration = new FileParsingConfiguration();
		loadValidationFile();
		m_nStartPageNumber = p_nStartPageNumber;
		if (p_nEndPageNumber == -1)
		{
			if (super.m_bTestMode && super.m_sDocumentLocalPath.endsWith(TEST_FILE_EXTENSION_INPUT))
			{
				try
				{
		  	  		String pageCountText = ParserTestHarnessUtils.readExtractedStringData(super.m_sDocumentLocalPath, 1);
		  	  		p_nEndPageNumber = Integer.parseInt(pageCountText);
				}
				catch (SciException e)
				{
					//TODO:Nothing for now
				}
			}
			else
			{
				p_nEndPageNumber = PdfUtils.getNumberOfPages(p_sDocumentLocalPath);
			}
		}
		m_nEndPageNumber = p_nEndPageNumber;
		m_NumberOfPages = p_NumberOfPages;
		
	}
	
	@Override
	protected boolean canDocumentBeParsed()
	{
		if (super.m_bTestMode)
		{
			if (super.m_sDocumentLocalPath != null && super.m_sDocumentLocalPath.endsWith(TEST_FILE_EXTENSION_INPUT))
			{
				return true;
			}
		}
		
			String fileText = null;
			List<String> keyList = null;
			boolean keyPresent= true;
			//handle the file validation wether file is of same type or not
	  	  	try {
				fileText = PdfUtils.getFileText(super.m_sDocumentLocalPath);
			} catch (SciException e) {
				log.error("Error while extracting text from PDF"+e.getMessage());
				return false;
			}
	  	  	fileText = fileText.replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE);
	  	    fileText = fileText.replaceAll(StringConstants.CRLF, StringConstants.SPACE);
	  	    fileText = fileText.replaceAll(StringConstants.NEWLINE, StringConstants.SPACE);
	  	  	
	  	    keyList = this.keyHeaderList;
	  	  	for(String key:keyList)
	  	  	{
	  	  		if(key.equalsIgnoreCase("General Information"))
	  	  		{}
	  	  		else
	  	  		{
	  	  		if(!fileText.contains(key))
	  	  		{
	  	  			keyPresent = false;
	  	  			break;
	  	  		}
	  	  		}
	  	  	}
	  	  	
	  	  	
	  	  		
			return keyPresent;
		}

	@Override
	protected ParsedDataTable parseDocument() throws SciException
	{
		

			ParsedDataTable oDataTablePageContents = null;
	  	  	List<List<String>> oRowsOfAllTables = null;
	  	  	Set<String> setHeaderContent= new HashSet<String>();
	  	  	String fileText = null;
			oDataTablePageContents = new ParsedDataTable();
			
	  	  	if (super.m_bTestMode && super.m_sDocumentLocalPath.endsWith(TEST_FILE_EXTENSION_INPUT))
			{
	  	  		fileText = ParserTestHarnessUtils.readExtractedStringData(super.m_sDocumentLocalPath, 3);
				if(fileText.contains("Seriousness Assessment Summary Sheet"))
					m_nStartPageNumber=2;
				else
					m_nStartPageNumber=1;
	  	  		
	  	  		oRowsOfAllTables = ParserTestHarnessUtils.readExtractedData(super.m_sDocumentLocalPath, 2);
			}
			else
			{
				fileText = PdfUtils.getFileText(super.m_sDocumentLocalPath,1,1);
				if(fileText.contains("Seriousness Assessment Summary Sheet"))
					m_nStartPageNumber=2;
				else
					m_nStartPageNumber=1;
				
				oRowsOfAllTables = PdfUtils.extractData(super.m_sDocumentLocalPath, PdfUtils.ExtractionMethod.SPREADSHEET, 
						m_nStartPageNumber, m_nEndPageNumber, m_NumberOfPages, null);
				
				
				if (m_bTestMode)
				{
		  			ParserTestHarnessUtils.printExtractedData(Integer.toString(m_nEndPageNumber), super.m_sDocumentLocalPath + TEST_FILE_EXTENSION_OUTPUT, 1);
		  			
		  			ParserTestHarnessUtils.printExtractedData(oRowsOfAllTables, super.m_sDocumentLocalPath + TEST_FILE_EXTENSION_OUTPUT, 2);
		  			
		  			ParserTestHarnessUtils.printExtractedData(fileText, super.m_sDocumentLocalPath + TEST_FILE_EXTENSION_OUTPUT, 3);
				}
			}
	  	  	
			//DEV-NOTE: Extraction API required '1-based' page numbering
/*			oRowsOfAllTables = PdfUtils.extractData(super.m_sDocumentLocalPath, PdfUtils.ExtractionMethod.SPREADSHEET, 
										2, m_nEndPageNumber, m_NumberOfPages, null);
*/
			//for new Mesa file there is no header page hence parsing start from 1
				
	  	  	fileText = PdfUtils.getFileText(super.m_sDocumentLocalPath);
	  	  
			oDataTablePageContents = convertRowContentsToDataTable(super.m_sDocumentLocalPath, oRowsOfAllTables);
			
			extractNarrativeData(fileText, oDataTablePageContents, setHeaderContent);
			
			/*System.out.println("Initial Parsing Data Start-----------------------------------------------------------------------");
			String key = null;
			ParsedDataValue parsedDataValue = null;
			for(int index=0; index<oDataTablePageContents.colCount(); index++)
			{
				key = oDataTablePageContents.getMnemonic(index);
				parsedDataValue = oDataTablePageContents.getColumnValues(index).get(0);
				System.out.println(key+":"+parsedDataValue.rawValue()+":"+parsedDataValue.displayValue()+":"+parsedDataValue.dataState()+":"+parsedDataValue.validationState());
			}
			System.out.println("After Initial Parsing End-----------------------------------------------------------------------");
			*/
			oDataTablePageContents = MapAutomatedKeysToMnemonics(oDataTablePageContents);
			
			return oDataTablePageContents;
			
			
		
	}
	
	@Override
	protected boolean validateParsedDocumentData() {
		// TODO Auto-generated method stub
		return true;
	}
	
	private void extractNarrativeData(String fileContent, ParsedDataTable oDataTable, Set<String> setHeaderContent) throws SciException
	{
		String lastHeader = null;
		String dateValue = null;
		String junkHeaderContent = null;
		StringBuffer narrativeString = null;
		String sKey = null;
		StringParsedDataValue value= null;
	//	lastHeader = CASE_NARRATIVE_SECTION_IDENTIFIER;
		
		fileContent = fileContent.substring(fileContent.indexOf("Event Description (B.5.1/H.1)"),fileContent.indexOf("STATEMENT TEXT:"));//.replaceAll("\r", "");
		/*if(fileContent.contains(PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT))
		{
			junkHeaderContent = fileContent.substring(fileContent.indexOf(PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT)+PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT.length());
			junkHeaderContent = junkHeaderContent.substring(0, junkHeaderContent.indexOf(StringConstants.CRLF));
			junkHeaderContent = PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT + junkHeaderContent;
			fileContent = fileContent.replace(junkHeaderContent, StringConstants.EMPTY);
		}*/
	//	fileContent = fileContent.replaceAll(StringConstants.CRLF, StringConstants.EMPTY);
		
		narrativeString = new StringBuffer();
		narrativeString.append(fileContent);
		ABCaseNarrativeFieldParser oNarrativeParser = new ABCaseNarrativeFieldParser(m_oParsingConfiguration);
		Map<String, StringParsedDataValue> narrativeKeyMap = oNarrativeParser.parse(narrativeString.toString());
		
		for(Entry<String, StringParsedDataValue> entry: narrativeKeyMap.entrySet())
		{
			//sKey=createColumnKeyWithHeader(entry.getKey(), lastHeader);
			sKey=entry.getKey();
			/*if(sKey.contains("Date"))
			{
				
					value = entry.getValue();
					dateValue = value.rawValue().toString();
					dateValue = convertDateForNarrative(dateValue);
					addColumnValue(oDataTable, sKey,sKey, new StringParsedDataValue(value.rawValue().toString(),dateValue,DataState.CANONICALIZED,ValidationState.UNVALIDATED), setHeaderContent,true);
				
			}
			else
			{*/
					value = entry.getValue();
					addColumnValue(oDataTable, sKey,sKey, value, setHeaderContent,true);
			//}
		}
	}
	
	private ParsedDataTable convertRowContentsToDataTable(String filePath, List<List<String>> p_listRowsOfAllTables) throws SciException
	{
		
		ParsedDataTable p_oParsedDataTable = null;
		
		Set<String> setHeaderContent = new HashSet<String>();
		String key = null;
		new ParsedDataTable();
		new ArrayList<String>();

		Object oColumnValue = null;
		new HashMap<>();
		List<List<String>> oCsvParser = p_listRowsOfAllTables;
//		CSVParser oCsvParser = null;
		String lastHeader=null;
		String repeatSectionMainHeader=null;
		StringBuffer narrativeTextBuffer = new StringBuffer();
		String narrativeKey = null;
		new File(filePath);
		String repeatKeyInsert = null;
		String keyCount = null;
		String fileText = null;
		String narrativeText = null;
		String inputDate = null;
		String afterParsedDate = null;
		try
		{
			 if (p_oParsedDataTable == null)
				{
				 p_oParsedDataTable = new ParsedDataTable();
				}
				new ArrayList<String>();
				
				
				fileText = PdfUtils.getFileText(super.m_sDocumentLocalPath,1,1);
				if(fileText.contains("Generated On:") && fileText.contains("Submitted By:"))
				{
					String generatedOnValue= fileText.substring(fileText.indexOf("Generated On:")+13,fileText.indexOf("Submitted By:")).trim();
					
					addColumnValue(p_oParsedDataTable, GENERATED_ON_KEY, generatedOnValue, setHeaderContent);
				}
				
				for (List<String> oRecord : oCsvParser)
				{
					
					if(oRecord.size()==1)
						continue;
					
					if(oRecord.get(0).equals(GENERAL) || oRecord.get(0).equals(SERIOUSNESS_CRITERIA)
						|| oRecord.get(0).contains(REPORTER) || oRecord.get(0).equals(PATIENT) 
						|| (oRecord.get(0).startsWith(DRUG) && oRecord.get(0).endsWith(")")) 
						|| oRecord.get(0).contains(ADVERSE_EVENT) || oRecord.get(0).equals(PROTOCOL) 
						|| oRecord.get(0).equals(STANDARD_INTAKE_QUERIES) || oRecord.get(0).contains(CAUSALITY_DE_RE_CHALLENGE)
						|| oRecord.get(0).equals(STUDY) || oRecord.get(0).equals(PREGNANCY)|| oRecord.get(0).equals(PREVIOUS_PREGNANCY) 
						|| oRecord.get(0).equals(CURRENT_PREGNANCY) || oRecord.get(0).contains(CURRENT_PREGNANCY_OUTCOMES)
						|| oRecord.get(0).contains(LITERATURE2) || (oRecord.get(0).equals(NARRATIVE) ) 
						|| oRecord.get(0).contains(DRUG_HISTORY) || oRecord.get(0).contains(MEDICAL_HISTORY2)
						|| oRecord.get(0).contains(DIAGNOSTIC_TESTS) || oRecord.get(0).equals(OTHER_DIAGNOSTIC_DATA)
						|| oRecord.get(0).contains(ADDITIONAL_REFERENCE) || oRecord.get(0).startsWith(DEVICE) )
						
					{ 
						
						//To add narrative key to dataTable
						if(oRecord.get(0).equalsIgnoreCase(DRUG_ALLERGIES_HISTORY) && narrativeKey.equalsIgnoreCase(NARRATIVE_KEY_EVENT_DESCRIPTION_B_5_1_H_1))
						{
							addColumnValue(p_oParsedDataTable,narrativeKey,narrativeTextBuffer.toString(),setHeaderContent);
						}
						
						lastHeader=oRecord.get(0);
						
						//to handle the subKeyHeader and header while creating Key we use lastTopHeader as MainHeader
						if(oRecord.get(0).contains("Reporter(") || (oRecord.get(0).contains(DRUG) && oRecord.get(0).trim().length()==12) 
								|| oRecord.get(0).contains("Adverse Event(") ||  oRecord.get(0).contains(CAUSALITY_DE_RE_CHALLENGE) 
								|| oRecord.get(0).contains("Current Pregnancy Outcomes(") ||oRecord.get(0).contains(LITERATURE2)
								|| oRecord.get(0).contains("Drug History(") ||oRecord.get(0).contains("Medical History(")
								|| oRecord.get(0).contains(DIAGNOSTIC_TESTS)|| oRecord.get(0).contains("Additional Reference(") 
								|| oRecord.get(0).startsWith(DEVICE))
						{
							repeatSectionMainHeader=oRecord.get(0);
							keyCount = repeatSectionMainHeader.substring(repeatSectionMainHeader.lastIndexOf(" ")+1,repeatSectionMainHeader.lastIndexOf(")"));
							repeatKeyInsert = repeatSectionMainHeader.substring(0,repeatSectionMainHeader.lastIndexOf("("));
							repeatSectionMainHeader = repeatSectionMainHeader.substring(0,repeatSectionMainHeader.lastIndexOf(" of ")).replace("(", "");
							 lastHeader = repeatSectionMainHeader;
							addColumnValue(p_oParsedDataTable,repeatKeyInsert+"_Count",keyCount,setHeaderContent);
						}
						
						addColumnValue(p_oParsedDataTable,lastHeader,StringConstants.EMPTY,setHeaderContent);
						continue;
					}
					else if(oRecord.get(1).contains(DRUG_DOSAGES) || (oRecord.get(1).contains(INDICATIONS_FOR_USE) && oRecord.get(1).endsWith(")"))
							|| (oRecord.get(1).contains(DRUG_REACT_RELATEDNESS)) || (oRecord.get(1).contains(LOT_NUMBERS) && oRecord.get(1).endsWith(")"))
							|| (oRecord.get(1).contains(AUTHORS)) || (oRecord.get(1).contains(DIAGNOSTIC_RESULTS))
							//|| (oRecord.get(1).contains(DRUG_REACT_RELATEDNESS)) || (oRecord.get(1).contains(DEVICE2))
							)
					{
						
						lastHeader=createColumnKey(repeatSectionMainHeader,oRecord.get(1));
						
						keyCount = lastHeader.substring(lastHeader.lastIndexOf(" ")+1,lastHeader.lastIndexOf(")"));
						repeatKeyInsert = lastHeader.substring(0,lastHeader.lastIndexOf("("));
						lastHeader = lastHeader.substring(0,lastHeader.lastIndexOf(" of ")).replace("(", "");
						 
						addColumnValue(p_oParsedDataTable,repeatKeyInsert+"_Count",keyCount,setHeaderContent);

						addColumnValue(p_oParsedDataTable,lastHeader,StringConstants.EMPTY,setHeaderContent);
						continue;
					}
					else if(oRecord.get(0).contains(DRUG_DOSAGES) || (oRecord.get(0).contains(INDICATIONS_FOR_USE) && oRecord.get(0).endsWith(")") ) 
							|| (oRecord.get(0).contains(DRUG_REACT_RELATEDNESS)) || (oRecord.get(0).contains(LOT_NUMBERS))
							|| (oRecord.get(0).contains(AUTHORS)) || (oRecord.get(0).contains(DIAGNOSTIC_RESULTS))
							//|| (oRecord.get(0).contains(DRUG_REACT_RELATEDNESS)) || (oRecord.get(0).contains(DEVICE2))
							)
					{
						lastHeader = createColumnKey(repeatSectionMainHeader,oRecord.get(0));
						
						keyCount = lastHeader.substring(lastHeader.lastIndexOf(" ")+1,lastHeader.lastIndexOf(")"));
						repeatKeyInsert = lastHeader.substring(0,lastHeader.lastIndexOf("("));
						lastHeader = lastHeader.substring(0,lastHeader.lastIndexOf(" of ")).replace("(", "");
						 
						addColumnValue(p_oParsedDataTable,repeatKeyInsert+"_Count",keyCount,setHeaderContent);
						
						addColumnValue(p_oParsedDataTable,lastHeader,StringConstants.EMPTY,setHeaderContent);
						continue;
					}
					else if(lastHeader.equals(GENERAL) || lastHeader.equals(SERIOUSNESS_CRITERIA)
							|| lastHeader.contains("Reporter") || lastHeader.equals(PATIENT) 
							|| (lastHeader.startsWith("Drug") && (!lastHeader.contains("Drug Dosage"))) 
							|| lastHeader.contains("Adverse Event") || lastHeader.equals(PROTOCOL) 
							|| lastHeader.equals(STANDARD_INTAKE_QUERIES) || lastHeader.contains("Causality/ DeReChallenge")
							|| lastHeader.equals(STUDY) || lastHeader.equals(PREGNANCY)|| lastHeader.equals(PREVIOUS_PREGNANCY) 
							|| lastHeader.equals(CURRENT_PREGNANCY) || lastHeader.contains("Current Pregnancy Outcomes")
							|| lastHeader.contains("Literature") /*|| (lastHeader.equals(NARRATIVE) ) */
							|| lastHeader.contains("Drug History") || lastHeader.contains("Medical History")
							|| lastHeader.contains("Diagnostic Tests") || lastHeader.equals(OTHER_DIAGNOSTIC_DATA)
							|| lastHeader.contains("Additional Reference") || lastHeader.contains("Device") 
							)
					{
						//to skip the junk keys
						if(lastHeader.contains("Reporter") && createColumnKey(oRecord.get(0)).equalsIgnoreCase(KEY_DATE_OF_AUTOSPY))
						{
							continue;
						}
						for(int i=0;i<oRecord.size();i++)
						{
							if(oRecord.get(0).equals(StringConstants.EMPTY) && i==0)
							{
								i++;
							}
							if(i >= oRecord.size())
							{
								continue;
							}
							key = createColumnKey(lastHeader,oRecord.get(i));
							i++;
							if(key.equals(lastHeader+StringConstants.PERIOD) || key.equals(lastHeader) || i>=oRecord.size())
							{
								continue;
							}
							oColumnValue=oRecord.get(i);
							addColumnValue(p_oParsedDataTable,key,oColumnValue,setHeaderContent);
						
						}
							
			
					}
					else if(lastHeader.equals(NARRATIVE))
					{
						if(oRecord.get(0).trim().equalsIgnoreCase("Drug Allergies/ History"))
						{
							while(narrativeTextBuffer.toString().contains("<a href=") && narrativeTextBuffer.toString().contains("</a>"))
							{
								narrativeTextBuffer.replace(narrativeTextBuffer.indexOf("<a href="),narrativeTextBuffer.indexOf(">",narrativeTextBuffer.indexOf("<a href="))+1,"");
								narrativeTextBuffer.replace(narrativeTextBuffer.indexOf("</a>"),narrativeTextBuffer.indexOf("</a>")+5,"");
							
							}
							narrativeText = narrativeTextBuffer.toString().replace("\"", "");
							/*while(narrativeTextBuffer.toString().contains("<img src"))
							{
								narrativeTextBuffer.replace(narrativeTextBuffer.indexOf("<img src"),narrativeTextBuffer.indexOf("<img src")+7, "");
								
							}*/
							addColumnValue(p_oParsedDataTable,narrativeKey,narrativeText,setHeaderContent);
							continue;
						}
						else
						{
							for(int i=0;i<oRecord.size();i++)
							{
								if(oRecord.get(i).equals(EVENT_DESCRIPTION_B_5_1_H_1))
								{
									narrativeKey =	createColumnKey(NARRATIVE,oRecord.get(i));
								}
								else
								{
									narrativeTextBuffer.append(oRecord.get(i));
								}
							if(i==oRecord.size()-1)
							{
								if(!narrativeTextBuffer.toString().equals(StringConstants.EMPTY))
								{
									narrativeTextBuffer.append(StringConstants.CARRIAGERETURN);
								}
							continue;
							}
						}
						
					}
					}
					
					else if(lastHeader.contains("Drug Dosages") || (lastHeader.contains("Indications for Use") /*&& lastHeader.endsWith(")")*/ ) 
							|| (lastHeader.contains("DrugReactRelatedness")) || (lastHeader.contains("Lot Numbers"))
							|| (lastHeader.contains("Authors")) || (lastHeader.contains("Diagnostic Results"))
							//|| (oRecord.get(0).contains(DRUG_REACT_RELATEDNESS)) || (oRecord.get(0).contains(DEVICE2))
							)
					{
						for(int i=0;i<oRecord.size();i++)
						{
							if(oRecord.get(0).equals(StringConstants.EMPTY) && i==0)
							{
								i++;
							}
							if(i >= oRecord.size())
							{
								continue;
							}
							key = createColumnKey(lastHeader,oRecord.get(i));
							i++;
							if(key.equals(lastHeader+StringConstants.PERIOD) || key.equals(lastHeader) || i>=oRecord.size())
							{
								continue;
							}
							oColumnValue=oRecord.get(i);
							addColumnValue(p_oParsedDataTable,key,oColumnValue,setHeaderContent);
						}
						
					}
					
				}
				
				
		}
		catch (Exception ioExcep)
		{
			log.error("Error parsing file ", ioExcep);
			throw new SciException("Error parsing file", ioExcep);
		}
		finally
		{
			try
			{
//				oCsvParser.close();
			}
			catch (Exception ioExcep)
			{
				
			}
		}
		
		return p_oParsedDataTable;
		
	}
	
	private String createColumnKey(String sKey)
	{
		return sKey.replace(StringConstants.CARRIAGERETURN, StringConstants.SPACE);
	}
	
	private String createColumnKey(String p_header, String p_recordvalue)
	{
		return p_header + StringConstants.PERIOD + p_recordvalue.replace(StringConstants.CARRIAGERETURN, StringConstants.SPACE);
	}

	@Override
	protected ValidationState applyValidationRules(String p_sMnemonicName, String displayValue) {
		FileParsingField mfield = null;
		ValidationState vState = null;
		String validationRule = null;
		String[] legalValues = null;
	    String editedMnemonicPrefix = null;
	    String editedMnemonicSuffix = null;
	    boolean	matchedFlag = false;
	    String regex = null;
	    ValidationState vFailedStatusType;
	    boolean isMandatory = false;
	    List<String> tempGroupList = m_oParsingConfiguration.getFieldGroupList();
	    List<String> groupList = new ArrayList<String>();
	    String foundGroup = null;
	    String tempSubGroup = null;
	    List<String> tempSubGroupList = null;
	    List<String> subGroupList = null;
	    String foundSubGroup = null;
	    
	    for(String group: tempGroupList)
	    {
	    	group = group.replace(" ", "");
	    	group = group.replace("/", "");
	    	groupList.add(group);
	    }
		if(p_sMnemonicName.contains("_"))
		{
			editedMnemonicPrefix = p_sMnemonicName.substring(0,p_sMnemonicName.lastIndexOf("_"));
			editedMnemonicSuffix = p_sMnemonicName.substring(p_sMnemonicName.lastIndexOf("_"));
			if(editedMnemonicPrefix.contains("_"))
			{
				tempSubGroup = editedMnemonicPrefix.split("_")[1];
				editedMnemonicPrefix = editedMnemonicPrefix.substring(0,editedMnemonicPrefix.indexOf("_"));
				
				
				if(!groupList.contains(editedMnemonicPrefix))
				{
					for(int index=1;index<editedMnemonicPrefix.length();index++)
					{
						if(groupList.contains(editedMnemonicPrefix.substring(0,editedMnemonicPrefix.length()-index)))
						{
							foundGroup = editedMnemonicPrefix.substring(0,editedMnemonicPrefix.length()-index);
							break;
			}
		}
				}
				else//present in the group list
				{
					foundGroup = editedMnemonicPrefix;
				}
				
				//for subgroup
				if(foundGroup!= null)
				tempSubGroupList = m_oParsingConfiguration.getsubGroupListByGroupName(foundGroup);
				subGroupList = new ArrayList<String>();
				for(String subGroup: tempSubGroupList)
			    {
					subGroup = subGroup.replace(" ", "");
					subGroup = subGroup.replace("/", "");
					subGroupList.add(subGroup);
			    }
				
				
				if(!subGroupList.contains(tempSubGroup))
				{
					for(int index=1;index<tempSubGroup.length();index++)
					{
						if(subGroupList.contains(tempSubGroup.substring(0,tempSubGroup.length()-index)))
						{
							foundSubGroup = tempSubGroup.substring(0,tempSubGroup.length()-index);
							break;
						}
					}
				}
				else//present in the group list
		{
					foundSubGroup = tempSubGroup;
				}
				if(foundGroup!=null && foundSubGroup!= null && !foundGroup.equalsIgnoreCase(foundSubGroup))
				{
					p_sMnemonicName= foundGroup+"_"+foundSubGroup+editedMnemonicSuffix;
				}
				//else if()
		}
		else
		{
			if(!groupList.contains(editedMnemonicPrefix))
			{
				
				for(int index=1;index<editedMnemonicPrefix.length();index++)
				{
					if(groupList.contains(editedMnemonicPrefix.substring(0,editedMnemonicPrefix.length()-index)))
					{
						foundGroup = editedMnemonicPrefix.substring(0,editedMnemonicPrefix.length()-index);
						break;
					}
				}
			}
			else//present in the group list
			{
				foundGroup = editedMnemonicPrefix;
			}
			if(foundGroup!= null)
			{
				p_sMnemonicName= foundGroup+editedMnemonicSuffix;
			}
			}
		}
		
		p_sMnemonicName =p_sMnemonicName.replace(" ", "");
		p_sMnemonicName=p_sMnemonicName.replace("/", "");
			mfield = m_oParsingConfiguration.getField(p_sMnemonicName);
		
	    if(mfield == null || displayValue == null || displayValue.equalsIgnoreCase(""))
	    {
	    	vState = ValidationState.UNVALIDATED;
	    }
	    else
	    {
		validationRule = mfield.getValidationRule();
		isMandatory = mfield.isMandatory();
		legalValues = mfield.getLegalValues();
		if(mfield.getErrorType().equals("WARN"))
			vFailedStatusType = ValidationState.VALIDATED_WITH_WARNINGS;
		else
			vFailedStatusType = ValidationState.REQUIRED_REVIEW;
		
		if(isMandatory && displayValue.equalsIgnoreCase(""))
		{
			vState = vFailedStatusType;
		
		}
		else
		{
		if(validationRule.equalsIgnoreCase("DATE"))
		{
			if(ValidationUtils.validateField(displayValue.toUpperCase(), SciConstants.RegExConstants.DATE))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("ALPHABETS"))
		{
			if(ValidationUtils.validateField(displayValue, "^[A-Za-z ]*"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("NUMBER"))
		{
			if(ValidationUtils.validateField(displayValue, SciConstants.RegExConstants.INTEGER))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("GENERAL TEXT"))
		{
			vState = ValidationState.VALIDATION_NOT_APPLICABLE;
			
		}
		else if(validationRule.equalsIgnoreCase("ALPHANUMERIC"))
		{
			if(ValidationUtils.validateField(displayValue, "^[A-Za-z0-9 ]*$"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
			
		}
		else if(validationRule.equalsIgnoreCase("DATETIME"))
		{//have to change the regex for datetime
			if(ValidationUtils.validateField(displayValue, "^(([0-3]{1}[0-9]{1})?\\-?[a-z,A-Z]{3}?\\-[0-9]{4}?\\ [0-9]{2}?\\:[0-9]{2}?\\:[0-9]{2}?)+$"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
			
		}
		else if(validationRule.equalsIgnoreCase("DATETIMEMILLISEC"))
		{//have to change the regex for datetime
			if(ValidationUtils.validateField(displayValue, "^(([0-3]{1}[0-9]{1})?\\-?[a-z,A-Z]{3}?\\-[0-9]{4}?\\ [0-9]{2}?\\:[0-9]{2}?\\:[0-9]{2}?\\.[0-9]{3}?)+$"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
			
		}
		else if(validationRule.equalsIgnoreCase("LEGAL VALUES"))
		{
			//legalValues = mfield.getM_sarrLegalValues();
			//errorType = mfield.getErrorType();
			if(legalValues.length==1 && legalValues[0].contains("\n "))
			{
				legalValues[0] = legalValues[0].replace("\n", "").trim();
				
			}
			for(int index =0; index<legalValues.length; index++)
			{
				legalValues[index]=legalValues[index].toUpperCase();
			}
			if(legalValues!= null && (!legalValues[0].equals("")) && (!displayValue.equalsIgnoreCase("")) &&(!Arrays.asList(legalValues).contains( displayValue.toUpperCase())))
			{
				vState = vFailedStatusType;
			}
			else if(legalValues!= null && (!legalValues[0].equals("")) && (!displayValue.equalsIgnoreCase("")) &&(Arrays.asList(legalValues).contains( displayValue.toUpperCase())))
				vState = ValidationState.VALIDATED;
			else
				vState = ValidationState.UNVALIDATED;
		}
		else if(validationRule.equalsIgnoreCase("CUSTOMIZE"))
		{
			legalValues = mfield.getLegalValues();
			if(legalValues!= null)
			regex = legalValues[0];
			else
				regex = "";
			matchedFlag = Pattern.compile(regex).matcher(displayValue).matches();
			if(displayValue.equalsIgnoreCase(""))
			{
				vState = ValidationState.UNVALIDATED;
			}
			else if(matchedFlag)
			{
				vState = ValidationState.VALIDATED;
			}
			else if((!displayValue.equalsIgnoreCase("")) && !matchedFlag)
				vState = vFailedStatusType;
				
		}
		else
		{
			vState = ValidationState.UNVALIDATED;
		}
		}
	
	    }
	   return vState;
	}

	public void addKey(Map<String, List<String>> keyMap,String key, String value)
	{
		if(keyMap.containsKey(key))
			keyMap.get(key).add(value);
		else
		{
			List<String> valueList= new ArrayList<String>();
			valueList.add(value);
			keyMap.put(key,valueList);
		}
	}
			
	public int getMaxCountForKey(List<ParsedDataTable> m_DdataTableList, String key)
	{
		int keyCount=1;
		int maxCount=1;
			
				for(ParsedDataTable oDataTable: m_DdataTableList)
				{
			if(oDataTable.isColumnPresent(key+"_Count"))
					{
				keyCount =Integer.parseInt(oDataTable.getColumnValues(key+"_Count").get(0).displayValue().toString());
						if(maxCount < keyCount)
						{
							maxCount = keyCount;
						}
				
					}
					}
		return maxCount;
		
				}
	@Override
	public Map<String, List<String>> compareMultipleDataTable(List<ParsedDataTable> m_DdataTableList)
				{
		String mnemonic = null;
		StringParsedDataValue keyValue = null;
		String groupNamewithCount = null;
		String subGroupNamewithCount = null;
		String keyName = null;
		String displayValue = null;
		int maxGroupCount =1, maxSubGroupCount=1;
		ParsedDataValue keyCountValue= null;
		Map<String, List<String>> keyMap = new ListOrderedMap<String, List<String>>();
		
				
		//pasted code starts here
		for(String groupName: m_oParsingConfiguration.getFieldGroupList())
		{
			
			maxGroupCount = getMaxCountForKey(m_DdataTableList, groupName);
			
			
			for(ParsedDataTable oDataTable: m_DdataTableList)
			{
				
			if(oDataTable.isColumnPresent(groupName+"_Count"))
					{
						 
				keyCountValue = oDataTable.getColumnValues(groupName+"_Count").get(0);
				Integer.valueOf(keyCountValue.displayValue().toString());
				for(int groupIndex=1; groupIndex<= maxGroupCount; groupIndex++)
						{
					groupNamewithCount = groupName+groupIndex;
					for(String subgroup : m_oParsingConfiguration.getsubGroupListByGroupName(groupName))
					{
						if(subgroup.equalsIgnoreCase(groupName))
						{
							for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
							{
								
								keyName = tField.getMnemonic();
								displayValue = tField.getFieldLabel();
								displayValue = groupNamewithCount+"_"+displayValue;
								mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
								mnemonic =mnemonic.replace(" ", "");
								mnemonic =mnemonic.replace("/", "");
							if(oDataTable.isColumnPresent(mnemonic))
							keyValue = (StringParsedDataValue) oDataTable.getColumnValues(mnemonic).get(0);
						else
						{
								//when it does not found the key
								//it should never come to this block
								keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
							}
							addKey(keyMap, displayValue, keyValue.displayValue().toString());
								
						}
						}
						else  //subgroup is different than group
						{
							if(oDataTable.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
							{
								maxSubGroupCount = getMaxCountForKey(m_DdataTableList, groupNamewithCount+"."+subgroup);
								
								keyCountValue = oDataTable.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
								Integer.valueOf(keyCountValue.displayValue().toString());
								for(int subgroupIndex=1; subgroupIndex<= maxSubGroupCount; subgroupIndex++)
							{
									subGroupNamewithCount = subgroup+subgroupIndex;
									for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
								{
										
										keyName = tField.getMnemonic();
										displayValue = tField.getFieldLabel();
										displayValue = groupNamewithCount+"_"+subGroupNamewithCount+"_"+displayValue;
										mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
										mnemonic =mnemonic.replace(" ", "");
										mnemonic =mnemonic.replace("/", "");
									if(oDataTable.isColumnPresent(mnemonic))
									keyValue = (StringParsedDataValue) oDataTable.getColumnValues(mnemonic).get(0);
									else
									{
										//when it does not found the key
										//it should never come to this block
										keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
									}
									addKey(keyMap, displayValue, keyValue.displayValue().toString());
										
									}	
										
								}
							}
							else //if subgroup count is not present in the dataTable
							{
								for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
								{
									
									keyName = tField.getMnemonic();
									displayValue = tField.getFieldLabel();
									displayValue = groupNamewithCount+"_"+subgroup+"_"+displayValue;
									mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
									mnemonic =mnemonic.replace(" ", "");
									mnemonic =mnemonic.replace("/", "");
								if(oDataTable.isColumnPresent(mnemonic))
								keyValue = (StringParsedDataValue) oDataTable.getColumnValues(mnemonic).get(0);
								else
								{
									//when it does not found the key
									//it should never come to this block
									keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
								}
								addKey(keyMap, displayValue, keyValue.displayValue().toString());
									
							}

							}
							
						}
					}
			}
				
				
				
			}//if group count is not present in datatable
			else
			{
						groupNamewithCount = groupName;
						for(String subgroup : m_oParsingConfiguration.getsubGroupListByGroupName(groupName))
						{
							if(subgroup.equalsIgnoreCase(groupName))
							{
								for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
				{
									
									keyName = tField.getMnemonic();
									displayValue = tField.getFieldLabel();
									displayValue = groupNamewithCount+"_"+displayValue;
									mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
									mnemonic =mnemonic.replace(" ", "");
									mnemonic =mnemonic.replace("/", "");
					if(oDataTable.isColumnPresent(mnemonic))
								keyValue = (StringParsedDataValue) oDataTable.getColumnValues(mnemonic).get(0);
								else
					{
									//when it does not found the key
									//it should never come to this block
									keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
								}
								addKey(keyMap, displayValue, keyValue.displayValue().toString());
									
					}
							}
							else  //subgroup is different than group
							{
								if(oDataTable.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
								{
									maxSubGroupCount = getMaxCountForKey(m_DdataTableList, groupNamewithCount+"."+subgroup);
									keyCountValue = oDataTable.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
									Integer.valueOf(keyCountValue.displayValue().toString());
									for(int subgroupIndex=1; subgroupIndex<= maxSubGroupCount; subgroupIndex++)
									{
										subGroupNamewithCount = subgroup+subgroupIndex;
										for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
										{
											
											keyName = tField.getMnemonic();
											displayValue = tField.getFieldLabel();
											displayValue = groupNamewithCount+"_"+subGroupNamewithCount+"_"+displayValue;
											mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
											mnemonic =mnemonic.replace(" ", "");
											mnemonic =mnemonic.replace("/", "");
										if(oDataTable.isColumnPresent(mnemonic))
										keyValue = (StringParsedDataValue) oDataTable.getColumnValues(mnemonic).get(0);
					else
					{
											//when it does not found the key
											//it should never come to this block
											keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
										}
										addKey(keyMap, displayValue, keyValue.displayValue().toString());
											
										}
										
									}
					}
								else //if subgroup count is not present in the dataTable
								{
									for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
									{
						
										keyName = tField.getMnemonic();
										displayValue = tField.getFieldLabel();
										displayValue = groupNamewithCount+"_"+subgroup+"_"+displayValue;
										mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
										mnemonic =mnemonic.replace(" ", "");
										mnemonic =mnemonic.replace("/", "");
									if(oDataTable.isColumnPresent(mnemonic))
									keyValue = (StringParsedDataValue) oDataTable.getColumnValues(mnemonic).get(0);
									else
									{
										//when it does not found the key
										//it should never come to this block
										keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
				}
									addKey(keyMap, displayValue, keyValue.displayValue().toString());
										
			}
			
								}
			
		}
						}
			}
		}
		}
	
		//addingFileNames to keyMap
		List<String> fileNameList = new ArrayList<String>();
		List<String> fileTypeList = new ArrayList<String>();
		for(ParsedDataTable oDataTable: m_DdataTableList)
		{
			if(oDataTable.isColumnPresent("FileName"))
			{
				fileNameList.add(oDataTable.getColumnValues("FileName").get(0).displayValue().toString());
			}
			else
			{
				fileNameList.add(StringConstants.EMPTY);
			}
			
			if(oDataTable.isColumnPresent("fileType"))
			{
				fileTypeList.add(oDataTable.getColumnValues("fileType").get(0).displayValue().toString());
			}
			else
			{
				fileTypeList.add(StringConstants.EMPTY);
			}
				
		}
		keyMap.put("FileName", fileNameList);
		keyMap.put("fileType", fileTypeList);
		List<String> values=null;
		String key= null;
		
		return keyMap;
	}
	
	public FileParsingConfiguration loadValidationFile()
	{
		FileParsingField field = null;
		FileParsingConfiguration filePassConfig = null;

		filePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AB_KEY);
	
	    for(String key :  filePassConfig.getFieldMap().keySet())

	    {

	           field = filePassConfig.getField(key);
	           m_oParsingConfiguration.addField(field);
	    

	    }
	    
	    return filePassConfig;
	}
	
	public ParsedDataTable MapAutomatedKeysToMnemonics(ParsedDataTable oDataTablePageContents)
	{
		ParsedDataTable nDataTable = new ParsedDataTable();
		String m_sLabel = null;
		String mnemonic = null;
		Properties oKeyProp = null;
		String propValue = null;
		StringParsedDataValue keyValue = null;
		int repeatCountForGroup = 0;
		int repeatCountForsubGroup = 0;
		String groupNamewithCount = null;
		String subGroupNamewithCount = null;
		String keyName = null;
	//	String propKey = null;
		ParsedDataValue keyCountValue= null; 
		Set<String> setHeaderContent = new HashSet<String>();
		oKeyProp = FileConfigurationHome.getParsePropertiesByKey(SciConstants.XMLParseConstant.PARSER_PROPERTIES_AB_KEY);
		
		for(String groupName: m_oParsingConfiguration.getFieldGroupList())
		{
			if(oDataTablePageContents.isColumnPresent(groupName+"_Count"))
			{
				keyCountValue = oDataTablePageContents.getColumnValues(groupName+"_Count").get(0);
				repeatCountForGroup = Integer.valueOf(keyCountValue.displayValue().toString());
				
				addColumnValue(nDataTable, groupName+"_Count",m_sLabel, (StringParsedDataValue)keyCountValue, setHeaderContent, true);
				//tempKey = propValue;
				for(int groupIndex=1; groupIndex<= repeatCountForGroup; groupIndex++)
				{
					groupNamewithCount = groupName+groupIndex;
					for(String subgroup : m_oParsingConfiguration.getsubGroupListByGroupName(groupName))
					{
						if(subgroup.equalsIgnoreCase(groupName))
						{
							for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
							{
								
								keyName = tField.getMnemonic();
								propValue = oKeyProp.getProperty(keyName);
								//propValue = groupNamewithCount+"."+propValue;
								propValue = propValue.replaceFirst(groupName, groupNamewithCount);
								m_sLabel = tField.getFieldLabel();
								mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
								mnemonic =mnemonic.replace(" ", "");
								mnemonic =mnemonic.replace("/", "");
						//		m_sLabel = m_sLabel+"("+index+" of "+repeatCount+")";
						//		propValue = tempKey + "("+index+" of "+repeatCount+")";
							if(oDataTablePageContents.isColumnPresent(propValue))
							keyValue = (StringParsedDataValue) oDataTablePageContents.getColumnValues(propValue).get(0);
							else
							{
								//when it does not found the key
								//it should never come to this block
								keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
							}
							addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);

								
							}
						}
						else  //subgroup is different than group
						{
							if(oDataTablePageContents.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
							{
								keyCountValue = oDataTablePageContents.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
								repeatCountForsubGroup = Integer.valueOf(keyCountValue.displayValue().toString());
								//adding count to dataTable
								addColumnValue(nDataTable, groupNamewithCount+"."+subgroup+"_Count",m_sLabel, (StringParsedDataValue)keyCountValue, setHeaderContent, true);
								for(int subgroupIndex=1; subgroupIndex<= repeatCountForsubGroup; subgroupIndex++)
								{
									subGroupNamewithCount = subgroup+subgroupIndex;
									for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
									{
										
										keyName = tField.getMnemonic();
										propValue = oKeyProp.getProperty(keyName);
										//propValue = groupNamewithCount+"."+subGroupNamewithCount+"."+propValue;
										propValue = propValue.replaceFirst(groupName+"."+subgroup, groupNamewithCount+"."+subGroupNamewithCount);
										m_sLabel = tField.getFieldLabel();
										mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
										mnemonic =mnemonic.replace(" ", "");
										mnemonic =mnemonic.replace("/", "");
								//		m_sLabel = m_sLabel+"("+index+" of "+repeatCount+")";
								//		propValue = tempKey + "("+index+" of "+repeatCount+")";
									if(oDataTablePageContents.isColumnPresent(propValue))
									keyValue = (StringParsedDataValue) oDataTablePageContents.getColumnValues(propValue).get(0);
									else
									{
										//when it does not found the key
										//it should never come to this block
										keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
									}
									addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);

										
									}
									
								}
							}
							else //if subgroup count is not present in the dataTable
							{
								for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
								{
									
									keyName = tField.getMnemonic();
									propValue = oKeyProp.getProperty(keyName);
									propValue = propValue.replaceFirst(groupName+".", groupNamewithCount+".");
									
									m_sLabel = tField.getFieldLabel();
									mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
									mnemonic =mnemonic.replace(" ", "");
									mnemonic =mnemonic.replace("/", "");
							//		m_sLabel = m_sLabel+"("+index+" of "+repeatCount+")";
							//		propValue = tempKey + "("+index+" of "+repeatCount+")";
								if(oDataTablePageContents.isColumnPresent(propValue))
								keyValue = (StringParsedDataValue) oDataTablePageContents.getColumnValues(propValue).get(0);
								else
								{
									//when it does not found the key
									//it should never come to this block
									keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
								}
								addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);

									
								}

							}
							
						}
					}
				}
				
				
				
			}//if group count is not present in datatable
			else
			{
				if(oDataTablePageContents.isColumnPresent(groupName))
				{
					
						groupNamewithCount = groupName;
						for(String subgroup : m_oParsingConfiguration.getsubGroupListByGroupName(groupName))
						{
							if(subgroup.equalsIgnoreCase(groupName))
							{
								for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
								{
									
									keyName = tField.getMnemonic();
									propValue = oKeyProp.getProperty(keyName);
								//	propValue = groupNamewithCount+"."+propValue;
									
									m_sLabel = tField.getFieldLabel();
									mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
									mnemonic =mnemonic.replace(" ", "");
									mnemonic =mnemonic.replace("/", "");
							//		m_sLabel = m_sLabel+"("+index+" of "+repeatCount+")";
							//		propValue = tempKey + "("+index+" of "+repeatCount+")";
								if(oDataTablePageContents.isColumnPresent(propValue))
								keyValue = (StringParsedDataValue) oDataTablePageContents.getColumnValues(propValue).get(0);
								else
								{
									//when it does not found the key
									//it should never come to this block
									keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
								}
								addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);

									
								}
							}
							else  //subgroup is different than group
							{
								if(oDataTablePageContents.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
								{
									keyCountValue = oDataTablePageContents.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
									repeatCountForsubGroup = Integer.valueOf(keyCountValue.displayValue().toString());
									//adding count to dataTable
									addColumnValue(nDataTable, groupNamewithCount+"."+subgroup+"_Count",m_sLabel, (StringParsedDataValue)keyCountValue, setHeaderContent, true);
									
									for(int subgroupIndex=1; subgroupIndex<= repeatCountForsubGroup; subgroupIndex++)
									{
										subGroupNamewithCount = subgroup+subgroupIndex;
										for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
										{
											
											keyName = tField.getMnemonic();
											propValue = oKeyProp.getProperty(keyName);
											//propValue = groupNamewithCount+"."+subGroupNamewithCount+"."+propValue;
											propValue = propValue.replaceFirst(groupName+"."+subgroup, groupNamewithCount+"."+subGroupNamewithCount);
											m_sLabel = tField.getFieldLabel();
											mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
											mnemonic =mnemonic.replace(" ", "");
											mnemonic =mnemonic.replace("/", "");
									//		m_sLabel = m_sLabel+"("+index+" of "+repeatCount+")";
									//		propValue = tempKey + "("+index+" of "+repeatCount+")";
										if(oDataTablePageContents.isColumnPresent(propValue))
										keyValue = (StringParsedDataValue) oDataTablePageContents.getColumnValues(propValue).get(0);
										else
										{
											//when it does not found the key
											//it should never come to this block
											keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
										}
										addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);

											
										}
										
									}
								}
								else //if subgroup count is not present in the dataTable
								{
									for(FileParsingField tField: m_oParsingConfiguration.getFieldListBySubGroup(subgroup))
									{
										
										keyName = tField.getMnemonic();
										propValue = oKeyProp.getProperty(keyName);
										propValue = propValue.replaceFirst(groupName+".", groupNamewithCount+".");
										
										m_sLabel = tField.getFieldLabel();
										mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
										mnemonic =mnemonic.replace(" ", "");
										mnemonic =mnemonic.replace("/", "");
								//		m_sLabel = m_sLabel+"("+index+" of "+repeatCount+")";
								//		propValue = tempKey + "("+index+" of "+repeatCount+")";
									if(oDataTablePageContents.isColumnPresent(propValue))
									keyValue = (StringParsedDataValue) oDataTablePageContents.getColumnValues(propValue).get(0);
									else
									{
										//when it does not found the key
										//it should never come to this block
										keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
									}
									addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);

										
									}

								}
								
							}
						}
					}
			}
			//propValue = oKeyProp.getProperty(propKey);
			
		
		}
	
					
		
		return nDataTable;
	}
}
