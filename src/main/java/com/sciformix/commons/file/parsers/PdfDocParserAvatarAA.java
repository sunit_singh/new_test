package com.sciformix.commons.file.parsers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.collections4.map.ListOrderedMap;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.file.BaseFileParser;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;
import com.sciformix.commons.utils.PdfUtils;
import com.sciformix.commons.utils.ValidationUtils;

public class PdfDocParserAvatarAA extends BaseFileParser
{

	private static final String MANDATORY_FIELD_SAFETY_REPORT_I_D = "A.1 Safety Report.A.1.0.1 Safety report I.D.";
	private static final String MANDATORY_FIELD_PATIENT_INITIAL = "B.1 Patient.B.1.1 Patient initial";
	private static final String MANDATORY_FIELD_RECEIVER_IDENTIFIER = "M.1.6 ?Receiver Identifier";
	private static final String MANDATORY_FIELD_SENDER_IDENTIFIER = "M.1.5 ?Sender Identifier";
	private static final String MANDATORY_FIELD_MESSAGE_NUMBER = "M.1.4 ?Message Number";
	private static final String MANDATORY_FIELD_MESSAGE_DATE = "M.1.7b ?Message Date";
	private static final String MANDATORY_FIELD_FORMAT_RELEASE = "M.1.3 ?Format Release";
	private static final String MANDATORY_FIELD_FORMAT_VERSION = "M.1.2 ?Format Version";
	private static final String FIELD_HEADER_DUPLICATE_NUMBER = "Duplicate Number";
	private static final String FIELD_HEADER_DUPLICATE_SOURCE = "Duplicate Source";
	private static final String SOURCE_IDENTIFIER = "Duplicate SourceDuplicate Number";
	private static final String FIELD_HEADER_A_1_11_1_SOURCE_S_A_1_11_2 = "A.1.11.1 Source(s) /A.1.11.2";
	private static final String WEIGHT_UNIT_KG = "(kg)";
	private static final String HEIGHT_UNIT_CM = "(cm)";
	private static final String HEIGHT_HEADER_IDENTIFIER_2 = "height (cm)";
	private static final String WEIGHT_HEADER_IDENTIFIER_2 = "weight (kg)";
	private static final String HEIGHT_HEADER_IDENTIFIER = "Height (cm)";
	private static final String WEIGHT_HEADER_IDENTIFIER = "Weight (kg)";
	private static final String SPILLOVER_HEADER_B_2_I_2_A_MED_DRA_VERSION_FOR_PREFIX = "B.2.i.2.a MedDRA version for";
	private static final String SPILLOVER_HEADER_PROFESSIONAL_SUFFIX = "professional?";
	private static final String SPILLOVER_HEADER_REPORTER_SUFFIX = "reporter?";
	private static final String SPILLOVER_HEADER_TERM_HIGHLIGHTED_BY_THE_PREFIX = "B.2.i.3 Term highlighted by the";
	private static final String SPILLOVER_HEADER_MED_DRA_VER_SUFFIX_HISTORY = "history";
	private static final String SPILLOVER_HEADER_MEDICALLY_CONFIRMED_OR_FROM_HEALTH_PREFIX = "A.1.14 Medically confirmed or from health";
	private static final String SPLILLOVER_HEADER_SUFFIX_REACTION_EVENT_TERM_PT = "reaction/event term PT";
	private static final String SPLILLOVER_HEADER_REACTION_EVENT_TERM_LLT_SUFFIX = "reaction/event term LLT";
	private static final String SPILLOVER_HEADER_B_2_I_1_A_MED_DRA_VERSION_FOR_PREFIX = "B.2.i.1.a MedDRA version for";
	private static final String SPLILLOVER_HEADER_MED_DRA_VERSION_FOR_MEDICAL = "B.1.10.7.1a.1 MedDRA version for medical";
	private static final String NUMBER_SECTION_IDENTIFIER_SUFFFIX = "no.";
	private static final String FAX_SECTION_IDENTIFIER = "Fax";
	private static final String TELEPHONE_NUMBER_SECTION_IDENTIFIER = "Tel";
	private static final String SECTION_HEADER_DRUG_INFO = "B.4.k.5 Structured dosage info";
	private static final String DATE_FORMAT_IDENTIFIER = "YY";
	private static final String FIELD_HEADER_MESSAGE_DATE = MANDATORY_FIELD_MESSAGE_DATE;
	private static final String EMPTY_ROW_IDENTIFIER_FOR_SERIOUS = "Resulted in";
	private static final String SERIOUS_ROW_IDENTIFIER_5 = "birth defect?";
	private static final String SERIOUS_ROW_IDENTIFIER_4 = "Threatening?";
	private static final String SERIOUS_ROW_IDENTIFIER_3 = "Incapacitating?";
	private static final String SERIOUS_ROW_IDENTIFIER_2 = "Death?";
	private static final String SERIOUS_ROW_IDENTIFIER_1 = "Hospitalization?";
	private static final String SERIOUS_FIELD_HEADER_CONGENITAL_ANOMALY_BIRTH_DEFECT = "Congenital anomaly/birth defect?";
	private static final String SERIOUS_FIELD_HEADER_LIFE_THREATENING = "Life Threatening?";
	private static final String SERIOUS_FIELD_HEADER_DISABLING_INCAPACITATING = "Disabling/Incapacitating?";
	private static final String SERIOUS_FIELD_HEADER_RESULTED_IN_DEATH = "Resulted In Death?";
	private static final String FIELD_HEADER_CAUSED_PROLONGED_HOSPITALIZATION = "Caused/Prolonged Hospitalization?";
	private static final String HEADER_SECTION_SERIOUS_IDENTIFIER2 = "Serious?";
	private static final String HEADER_SECTION_SERIOUS_IDENTIFIER1 = "A.1.5 Seriousness";
	private static final String FIELD_HEADER_HOLDER_AND_AUTHORIZATION_APPLICATION_NO_OF_DRUG = "B.4.k.4 Holder and authorization/application no. of drug";
	private static final String FIELD_HEADER_STRUCTURED_DOSAGE_INFO = "B.4.k.5 Structured dosage info";
	private static final String FIELD_KEY_FAX_NO_WITH_DOT = "Fax. no.";
	private static final String FIELD_KEY_FAX_NO = "Fax no.";
	private static final String FIELD_KEY_TEL_NO_WITH_DOT = "Tel. no.";
	private static final String FIELD_KEY_EXT_WITH_DOT = "ext.";
	private static final String FIELD_KEY_EXT = "ext";
	private static final String FIELD_KEY_TEL_NO = "Tel no.";
	private static final String SECTION_HEADER_DRUG_APPLICATION_INFO = "Holder and authorization_application Info";
	private static final String REPORT_HEADER_IDENTIFIER_TEXT_ICHE2B = "ICHE2BADVERSEEVENTFORM";
	private static final String PAGE_HEADER_IDENTIFIER_TEXT_ICHE2B = "ICHE2B";
	private static final String PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT = "B.1.1 Patient initial A.1.0.1 Safety report I.D. (Ver.)\r\n";
	
	private static final String SECTION_HEADING_A_1_SAFETYREPORT = "A.1 Safety Report";
	private static final String SECTION_HEADING_B_1_PATIENT = "B.1 Patient";
	private static final String SECTION_HEADING_B_2_REACTION = "B.2 Reaction";
	private static final String FIELD_HEADING_B_5_1_CASE_NARRATIVE = "B.5.1 Case narrative";
	private static final String FIELD_HEADING_B_5_2_REPORTERS_COMMENTS = "B.5.2 Reporter's comments";
	
	private static final String CASE_NARRATIVE_SECTION_IDENTIFIER = "B.5.1 Case narrative";
	private static final String CASE_NARRATIVE_STRING_START_DELIMITER_TEXT = FIELD_HEADING_B_5_1_CASE_NARRATIVE;
	private static final String CASE_NARRATIVE_STRING_END_DELIMITER_TEXT = FIELD_HEADING_B_5_2_REPORTERS_COMMENTS;
	
	
	private static final String SECTION_HEADING_B_1_8_PAST_DRUG_THERAPY = "B.1.8 Past drug therapy";
	private static final String SECTION_HEADING_B_1_9_2_PATIENT_DEATH_CAUSE = "B.1.9.2 Patient death cause";
	private static final String SECTION_HEADING_B_1_10_PARENT = "B.1.10 Parent";
	private static final String SECTION_HEADING_B_1_10_8_PARENT_DRUG_THERAPY = "B.1.10.8 Parent drug therapy";
	private static final String SECTION_HEADING_B_4_K_18_DRUG_REACTION_RELATEDNESS = "B.4.k.18 Drug Reaction relatedness";
	private static final String SECTION_HEADING_B_3_TEST = "B.3 Test";
	private static final String SECTION_HEADING_B_4_K_2_2_ACTIVE_SUBSTANCE = "B.4.k.2.2 Active Substance";
	private static final String SECTION_HEADING_B_5_SUMMARY = "B.5 Summary";
	private static final String SECTION_HEADING_B_4_K_17_DRUG_RECURRENCE = "B.4.k.17 Drug Recurrence";
	private static final String SECTION_HEADING_B_4_DRUG = "B.4 Drug";
	private static final String SECTION_HEADING_B_1_10_7_PARENT_MEDICAL_HISTORY = "B.1.10.7 Parent medical history";
	private static final String SECTION_HEADING_B_1_9_4_PATIENT_AUTOPSY = "B.1.9.4 Patient autopsy";
	private static final String SECTION_HEADING_B_1_9_PATIENT_DEATH = "B.1.9 Patient death";
	private static final String SECTION_HEADING_B_1_7_PATIENT_MEDICAL_HISTORY = "B.1.7 Patient medical history";
	private static final String SECTION_HEADING_A_3_2_RECEIVER = "A.3.2 Receiver";
	private static final String SECTION_HEADING_A_3_1_SENDER = "A.3.1 Sender";
	private static final String SECTION_HEADING_A_2_PRIMARY_SOURCE = "A.2 Primary Source";
	
	private static final Logger log = LoggerFactory.getLogger(PdfDocParserAvatarAA.class);
	
	private int m_nStartPageNumber = 0;
	private int m_nEndPageNumber = 0;
	private int m_NumberOfPages = 0;
	
	
	public PdfDocParserAvatarAA(String p_sDocumentLocalPath)
	{
		this(p_sDocumentLocalPath, 0, 0, 0, false);
	}

	public PdfDocParserAvatarAA(String p_sDocumentLocalPath, boolean p_bTestMode)
	{
		this(p_sDocumentLocalPath, 0, 0, 0, p_bTestMode);
	}

	public PdfDocParserAvatarAA(String p_sDocumentLocalPath, int p_nStartPageNumber, int p_nEndPageNumber, int p_NumberOfPages, boolean p_bTestMode)
	{
		super(p_sDocumentLocalPath, "PdfDocParser_AvatarBA.ftl", p_bTestMode);
		
		super.m_oParsingConfiguration = new FileParsingConfiguration();
		loadValidationFile();
		m_nStartPageNumber = p_nStartPageNumber;
		m_nEndPageNumber = p_nEndPageNumber;
		m_NumberOfPages = p_NumberOfPages;
	}
	
	public void checkPdfProducer() throws SciException, SciServiceException
	{
		PDDocument oPdfReader = null;
			File file = null;
			String contentType = null;
			String pdfApplication = null;
			file=new File(super.m_sDocumentLocalPath);
			
			
			
			try {
				
				contentType = Files.probeContentType(Paths.get(file.toURI()));
				if(contentType!= null && !contentType.equalsIgnoreCase("application/pdf"))
				{
					throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_FILETYPE_CONTENT);
				}
				oPdfReader=PDDocument.load(file);
				pdfApplication = oPdfReader.getDocumentInformation().getCustomMetadataValue("Producer");
				if(pdfApplication!= null && !pdfApplication.equalsIgnoreCase("Apache FOP Version 1.1"))
				{
					//not getting proper value for producer sometimes different name appears need to review 
					//throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_PDF_PRODUCER);
				}
			} catch (IOException e) {
				log.error("Error while loading file into PDDocument. < "+e.getMessage()+" >");
				throw new SciException(e.getMessage());
			}	
			finally
			{
				if(oPdfReader!= null)
					try {
						oPdfReader.close();
					} catch (IOException e) {
						log.error("Error while closing PDDocument object < "+e.getMessage()+" >");
						throw new SciException(e.getMessage());
					}
			}
	}
	@Override
	protected boolean canDocumentBeParsed() throws SciServiceException
	{
		if (super.m_bTestMode)
		{
			if (super.m_sDocumentLocalPath != null && (super.m_sDocumentLocalPath.endsWith(TEST_FILE_EXTENSION_INPUT) || 
					super.m_sDocumentLocalPath.endsWith(TEST_FILE_EXTENSION_PARSEDDATATABLE)))
			{
				return true;
			}
		}
		
		String fileText = null;
		List<String> keyList = null;
		boolean keyPresent= true;
		String sCurrentRowKey = null;
		int numberOfPages = 0;
		boolean isFirstPageHeaderIdentifierPresent = false;
		List<List<String>> oRowsOfAllTables = null;
		
		
		//handle the file validation whether file is of same type or not
  	  	try
  	  	{
  	  	
  	  	//
  	  		
  	  	checkPdfProducer();
  			
  	  	fileText = PdfUtils.getFileText(super.m_sDocumentLocalPath);
  	  	
  	  	fileText = fileText.replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE);
	    fileText = fileText.replaceAll(StringConstants.CRLF, StringConstants.SPACE);
	    fileText = fileText.replaceAll(StringConstants.NEWLINE, StringConstants.SPACE);
	    
	  	keyList = this.keyHeaderList;
	  	for(String key:keyList)
	  	{
	  		if(!fileText.contains(key))
	  		{
	  			keyPresent = false;
	  			break;
	  		}
	  	}
	  	
	  	if(!keyPresent)
	  	{
	  	throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_FILE_TYPE_AND_FILE_DOES_NOT_MATCH);
	  	}
	  	
	  	
  	  	
  	  		//no of Pages validation 4 to 6
  	  		numberOfPages = PdfUtils.getNumberOfPages(super.m_sDocumentLocalPath);
  	  		
  	  		if(numberOfPages<4 || numberOfPages>6)
  	  		{
  	  			throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_NO_OF_PAGES);
  	  		}
  	  		
			
			
			
			for(int pageIndex=1; pageIndex<=numberOfPages;pageIndex++)
			{
				fileText = PdfUtils.getFileText(super.m_sDocumentLocalPath,pageIndex,pageIndex);
				
				if(pageIndex==1)
				{
					//first page footer validation
					if(!fileText.contains("ICH DTD Version: 2.1, Release: 2.0"))
					{
						
						throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_FIRST_PAGE_FOOTER_MISSING);
					}
				}
				else
				{
					//Page Header Validation 'ICHE2B'
					if(!fileText.contains("ICHE2B"))
					{
						throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_SUBSEQUENT_PAGE_FOOTER_MISSING);
					}
					//validation page number footer format 'Page 2 of 5'
					if(!fileText.contains("Page "+pageIndex+" of "+numberOfPages))
					{
						throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_PAGE_NO_IN_FOOTER);
					}
					if(!(fileText.contains("B.1.1 Patient initial") && fileText.contains("A.1.0.1 Safety report I.D. (Ver.)")))
					{
						throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_SUBSEQUENT_PAGE_HEADER__KEYS_MISSING);
					}
				}
	}

			// checking first page identifier text //‘ICHE2BADVERSEEVENT’
			 oRowsOfAllTables = PdfUtils.extractData(super.m_sDocumentLocalPath, PdfUtils.ExtractionMethod.SPREADSHEET, 
						1, 1, 1, null);
			 for(List<String> oCurrentRecord : oRowsOfAllTables)
				{
					if (oCurrentRecord.size() < 1)
					{
						//log.error("No data at current Row");
						continue;
					}
					
					sCurrentRowKey = oCurrentRecord.get(0).replaceAll(StringConstants.SPACE, StringConstants.EMPTY).replace(StringConstants.CARRIAGERETURN,StringConstants.EMPTY);
					if(sCurrentRowKey.equalsIgnoreCase(REPORT_HEADER_IDENTIFIER_TEXT_ICHE2B))
					{
						isFirstPageHeaderIdentifierPresent = true;
					}
				}
			 
			 if(!isFirstPageHeaderIdentifierPresent)
			 {
				 throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_FIRST_PAGE_HEADER);
			 }
			
			 
			 
		} catch (SciException e) {
			log.error("Error while extracting text from PDF"+e.getMessage());
			throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_PARSING_FILE);
			//return false;
		}
  		
  	  	
  	  		
		return keyPresent;
	}

	
	
	@Override
	protected ParsedDataTable parseDocument() throws SciException, SciServiceException
	{
		ParsedDataTable oDataTablePageContents = null;
  	  	List<List<String>> oRowsOfAllTables = null;
  	  	List<List<String>> oRowsOfTablesForSeriousness = null;
  	    Set<String> setHeaderContent = new HashSet<String>();
  	    String fileContentForNarrative = null;
  	  
  	  	oDataTablePageContents = new ParsedDataTable();
		
  	  	if (super.m_bTestMode && super.m_sDocumentLocalPath.endsWith(TEST_FILE_EXTENSION_PARSEDDATATABLE))
  	  	{
			oDataTablePageContents = ParserTestHarnessUtils.readExtractedDataTable(super.m_sDocumentLocalPath);
  	  	}
  	  	else
  	  	{
  	  	if (super.m_bTestMode && super.m_sDocumentLocalPath.endsWith(TEST_FILE_EXTENSION_INPUT))
  	  	{
  	  		oRowsOfAllTables = ParserTestHarnessUtils.readExtractedData(super.m_sDocumentLocalPath, 1);
  			
  	  		oRowsOfTablesForSeriousness = ParserTestHarnessUtils.readExtractedData(super.m_sDocumentLocalPath, 2);
  	  		
  	  		fileContentForNarrative = ParserTestHarnessUtils.readExtractedStringData(super.m_sDocumentLocalPath, 3);
  	  	}
  	  	else
  	  	{
			//DEV-NOTE: Extraction API required '1-based' page numbering
			oRowsOfAllTables = PdfUtils.extractData(super.m_sDocumentLocalPath, PdfUtils.ExtractionMethod.SPREADSHEET, 
										m_nStartPageNumber, m_nEndPageNumber, m_NumberOfPages, null);
			
			//DEV-NOTE: This extraction via an alternate method is ONLY for the 'Seriousness' fields
			oRowsOfTablesForSeriousness = PdfUtils.extractData(super.m_sDocumentLocalPath, PdfUtils.ExtractionMethod.BASIC, 
										1, 1, 1, null);
			
			fileContentForNarrative = PdfUtils.getFileText(super.m_sDocumentLocalPath);
			
			if (super.m_bTestMode)
			{
	  			ParserTestHarnessUtils.printExtractedData(oRowsOfAllTables, super.m_sDocumentLocalPath + TEST_FILE_EXTENSION_OUTPUT, 1);
	  			
	  			ParserTestHarnessUtils.printExtractedData(oRowsOfTablesForSeriousness, super.m_sDocumentLocalPath + TEST_FILE_EXTENSION_OUTPUT, 2);
	  			
	  			ParserTestHarnessUtils.printExtractedData(fileContentForNarrative, super.m_sDocumentLocalPath + TEST_FILE_EXTENSION_OUTPUT, 3);
			}
  	  	}
		
		//convertGeneral
		oDataTablePageContents = convertRowContentsToDataTable(setHeaderContent, oRowsOfAllTables);
				
		//checking mandatory keys and its values
		checkMandatoryKeysForFirstPage(oDataTablePageContents);
		
		//Convert Seriousness
		extractSeriousData(oRowsOfTablesForSeriousness, oDataTablePageContents, setHeaderContent);
		
		//Convert narrative
		extractNarrativeData(fileContentForNarrative, oDataTablePageContents, setHeaderContent);
		
		oDataTablePageContents = MapAutomatedKeysToMnemonics(oDataTablePageContents);
  	  	}
  	  	
		if (super.m_bTestMode)
		{
			ParserTestHarnessUtils.printExtractedDataTable(oDataTablePageContents, super.m_sDocumentLocalPath + TEST_FILE_EXTENSION_PARSEDDATATABLE);
		}
		
		return oDataTablePageContents;
	}

	@Override
	protected boolean validateParsedDocumentData() {
		return true;
	}
	public ParsedDataTable MapAutomatedKeysToMnemonics(ParsedDataTable oDataTablePageContents)
	{
		ParsedDataTable nDataTable = new ParsedDataTable();
		String m_sLabel = null;
		String mnemonic = null;
		Properties oKeyProp = null;
		String propValue = null;
		StringParsedDataValue keyValue = null;
		List<ParsedDataValue> valueList = null;
		int repeatCount = 0;
		ParsedDataValue keyCountValue= null; 
		Set<String> setHeaderContent = new HashSet<String>();
		String tempKey = null;
		oKeyProp = FileConfigurationHome.getParsePropertiesByKey(SciConstants.XMLParseConstant.PARSER_PROPERTIES_AA_KEY);
		
	
		for(String propKey : m_oParsingConfiguration.getFieldMap().keySet())
		{
			
			propValue = oKeyProp.getProperty(propKey);
			m_sLabel = m_oParsingConfiguration.getField(propKey).getFieldLabel();
			if(propValue != null)
			{
				if(oDataTablePageContents.isColumnPresent(propValue+"_Count"))
				{
					keyCountValue = oDataTablePageContents.getColumnValues(propValue+"_Count").get(0);
					repeatCount = Integer.valueOf(keyCountValue.displayValue().toString());
					tempKey = propValue;
					for(int index=1; index<= repeatCount; index++)
					{
						
						m_sLabel = m_oParsingConfiguration.getField(propKey).getFieldLabel();
							mnemonic = propKey+"_"+index;
							m_sLabel = m_sLabel+" "+index;
							propValue = tempKey + "_" +index;
						if(oDataTablePageContents.isColumnPresent(propValue))
						keyValue = (StringParsedDataValue) oDataTablePageContents.getColumnValues(propValue).get(0);
						else
						{
							//when it does not found the key
							//it should never come to this block
							keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
						}
						addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);
					}
					
					addColumnValue(nDataTable, propKey+"_Count",m_sLabel, (StringParsedDataValue)keyCountValue, setHeaderContent, true);
					
					
				}
				else if(oDataTablePageContents.isColumnPresent(propValue))
				{
					mnemonic = propKey;
					m_sLabel = m_oParsingConfiguration.getField(propKey).getFieldLabel();
					valueList = oDataTablePageContents.getColumnValues(propValue);
					
						keyValue = (StringParsedDataValue)valueList.get(0);
						addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);
					
					
				}
				else
				{
					mnemonic = propKey;
					m_sLabel = m_oParsingConfiguration.getField(propKey).getFieldLabel();
					//if Mnemonic is not present in dataTable.
					keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
					addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);
					
				}
		}
			else
			{
				// if key does not present in property file. add Key with Empty value 
				mnemonic = propKey;
				m_sLabel = m_oParsingConfiguration.getField(propKey).getFieldLabel();
				keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
				addColumnValue(nDataTable, mnemonic,m_sLabel, keyValue, setHeaderContent, true);
			}
		}
		
		return nDataTable;
	}
	
	private ParsedDataTable convertRowContentsToDataTable(Set<String> setHeaderContent, List<List<String>> p_listRowsOfAllTables) throws SciException, SciServiceException
	{
		ParsedDataTable oDataTable = null;
		String lastHeader=null;
		String sCurrentRowKey = null;
		oDataTable = new ParsedDataTable();
		
		
		for(List<String> oCurrentRecord : p_listRowsOfAllTables)
		{
			if (oCurrentRecord.size() < 1)
			{
				log.error("No data at current Row");
				continue;
			}
			
			sCurrentRowKey = oCurrentRecord.get(0).trim();
			
			if (sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_A_1_SAFETYREPORT) 
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_A_2_PRIMARY_SOURCE) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_A_3_1_SENDER)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_A_3_2_RECEIVER) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_PATIENT)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_7_PATIENT_MEDICAL_HISTORY) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_8_PAST_DRUG_THERAPY)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_9_PATIENT_DEATH) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_9_2_PATIENT_DEATH_CAUSE)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_9_4_PATIENT_AUTOPSY) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_10_PARENT)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_10_7_PARENT_MEDICAL_HISTORY) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_1_10_8_PARENT_DRUG_THERAPY)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_2_REACTION) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_3_TEST)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_4_DRUG) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_4_K_2_2_ACTIVE_SUBSTANCE)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_4_K_17_DRUG_RECURRENCE) || sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_4_K_18_DRUG_REACTION_RELATEDNESS)
					|| sCurrentRowKey.equalsIgnoreCase(SECTION_HEADING_B_5_SUMMARY) )
			{
				//DEV-NOTE: This represents various Sections identified by the Section Headers such as 'A.x' and 'B.x'; need to extract info from here
				lastHeader = createColumnKey(sCurrentRowKey);
				addColumnValue(oDataTable, lastHeader, StringConstants.EMPTY, setHeaderContent);
				continue;
			}
			else if(sCurrentRowKey.startsWith(PAGE_HEADER_IDENTIFIER_TEXT_ICHE2B))
			{
				sCurrentRowKey = sCurrentRowKey.replaceAll(StringConstants.SPACE, StringConstants.EMPTY).replace(StringConstants.CARRIAGERETURN,StringConstants.EMPTY);
				if(sCurrentRowKey.equalsIgnoreCase(REPORT_HEADER_IDENTIFIER_TEXT_ICHE2B))
				{
					//DEV-NOTE: This is the first page with the Report Header section; need to extract info from here
					//First Page Identifier Flag
					lastHeader = REPORT_HEADER_IDENTIFIER_TEXT_ICHE2B;
					extractContentFromReportHeader(null, oCurrentRecord, oDataTable, setHeaderContent,1);
				}
				else if(sCurrentRowKey.equalsIgnoreCase(PAGE_HEADER_IDENTIFIER_TEXT_ICHE2B))
				{
					//DEV-NOTE: This is a subsequent page header; skip this
					continue;
				}
				else
				{
					//Not expected to come here...
				}
			}
			else if(sCurrentRowKey.startsWith(CASE_NARRATIVE_SECTION_IDENTIFIER) )
			{
				lastHeader = CASE_NARRATIVE_SECTION_IDENTIFIER;
				
			}
			else
			{
				if(lastHeader != null && lastHeader.equalsIgnoreCase(CASE_NARRATIVE_SECTION_IDENTIFIER) && !oCurrentRecord.get(0).startsWith("B.5."))
				{
					//to skip the spill over narrative text
					continue;
				}
				else if(lastHeader != null && lastHeader.equalsIgnoreCase(REPORT_HEADER_IDENTIFIER_TEXT_ICHE2B))
				{
					//DEV-NOTE: Report Header; need to extract info from here
					extractContentFromReportHeader(null, oCurrentRecord, oDataTable, setHeaderContent, 0);
				}
				else 
				{
					//DEV-NOTE:General Case of data extraction for 'A.x.x' and 'B.x.x'; need to extract info from here
					copyAllColumnValues(lastHeader, oCurrentRecord, oDataTable, setHeaderContent);
				}
			}
		}
		
	
		return oDataTable;
	}
	
	public void checkMandatoryKeysForFirstPage(ParsedDataTable oDataTable) throws SciServiceException
	{
		List<String> mKeyList = new ArrayList<String>();
		String displayValue = null;
		mKeyList.add(MANDATORY_FIELD_FORMAT_VERSION);
		mKeyList.add(MANDATORY_FIELD_FORMAT_RELEASE);
		mKeyList.add(MANDATORY_FIELD_MESSAGE_DATE);
		mKeyList.add(MANDATORY_FIELD_MESSAGE_NUMBER);
		mKeyList.add(MANDATORY_FIELD_SENDER_IDENTIFIER);
		mKeyList.add(MANDATORY_FIELD_RECEIVER_IDENTIFIER);
		//mKeyList.add("A.1 Safety Report.A.1.0.1 Safety report I.D.");
		
		for(String mKey: mKeyList)
		{
			if(oDataTable.isColumnPresent(mKey))
			{
				displayValue = oDataTable.getColumnValues(mKey).get(0).displayValue().toString();
				if(mKey.equalsIgnoreCase(MANDATORY_FIELD_FORMAT_VERSION) && !displayValue.equalsIgnoreCase("2.1"))
				{
					log.error("ERROR_VALIDATION_FORMAT_VERSION_VALUE: '" + mKey + "'");
					throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_FORMAT_VERSION_VALUE);
				}
				else if(mKey.equalsIgnoreCase(MANDATORY_FIELD_FORMAT_RELEASE) && !displayValue.equalsIgnoreCase("2.0"))
				{
					log.error("ERROR_VALIDATION_FORMAT_RELEASE_VALUE: '" + mKey + "'");
					throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_FORMAT_RELEASE_VALUE);
				}
				if(displayValue == null || displayValue.trim().equalsIgnoreCase(""))
				{
					log.error("ERROR_VALIDATION_HEADER_MANDATORY_FIELD_VALUE: '" + mKey + "'");
					throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_HEADER_MANDATORY_FIELD_VALUE);
				}
			}
			else
			{
				log.error("ERROR_VALIDATION_HEADER_MANDATORY_FIELD_KEY: '" + mKey + "'");
				throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_HEADER_MANDATORY_FIELD_KEY);
			}
			
			
		}
		
		//check only for key
		if(!oDataTable.isColumnPresent(MANDATORY_FIELD_PATIENT_INITIAL))
		{
			throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_PATIENT_INITIAL_MISSING);
		}
		
		if(oDataTable.isColumnPresent(MANDATORY_FIELD_SAFETY_REPORT_I_D))
		{
			displayValue = oDataTable.getColumnValues(MANDATORY_FIELD_SAFETY_REPORT_I_D).get(0).displayValue().toString();
			if(displayValue == null || displayValue.equalsIgnoreCase(""))
			{
			throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_SAFETY_REPOR_ID_MISSING);
			}
		}
		else
		{
			throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_SAFETY_REPOR_ID_MISSING);
		}
		
	}
	
	private static String createColumnKey(String sKey)
	{
		return createColumnKeyWithHeader(sKey, null);
	}
	
	private static String createColumnKeyWithHeader(String sKey, String sAdditionalKeyPrefix)
	{
		if (sAdditionalKeyPrefix != null && !sKey.trim().equals(StringConstants.EMPTY))
		{
			return sAdditionalKeyPrefix + StringConstants.PERIOD + sKey.replace(StringConstants.CARRIAGERETURN, StringConstants.EMPTY);
		}
		else
		{
			return sKey.replace(StringConstants.CARRIAGERETURN, StringConstants.EMPTY);
		}
	}
	
	
	
	private void copyDosagInfoValues(String cellValue,ParsedDataTable oDataTable,Set<String> setHeaderContent )
	{
		List<String> keyList=getKeysForDrugInfo();
		String columnValue=null;
		String sKey=null;
		StringBuffer text=new StringBuffer(cellValue);
		String lastHeader=SECTION_HEADER_DRUG_INFO;
		for(int index=0;index<keyList.size()-1;index++)
		{
			
				
			columnValue = extractValueFromKeys(keyList.get(index), keyList.get(index+1), text);
			sKey=createColumnKeyWithHeader(keyList.get(index), lastHeader);
			addColumnValue(oDataTable, sKey, columnValue, setHeaderContent);
			
			if(index==keyList.size()-2)
			{
				columnValue = extractValueFromKeys(keyList.get(index+1), null, text);
			sKey=createColumnKeyWithHeader(keyList.get(index+1), lastHeader);
			addColumnValue(oDataTable, sKey, columnValue, setHeaderContent);
			}	
			
		}
	}
	private void copyDrugAppInfoValues(String cellValue,ParsedDataTable oDataTable,Set<String> setHeaderContent )
	{
		List<String> keyList=getKeysForDrugAppInfo();
		String columnValue=null;
		String sKey=null;
		StringBuffer text=new StringBuffer(cellValue);
		String lastHeader=SECTION_HEADER_DRUG_APPLICATION_INFO;
		for(int index=0;index<keyList.size()-1;index++)
		{
			
				
			columnValue = extractValueFromKeys(keyList.get(index), keyList.get(index+1), text);
			sKey=createColumnKeyWithHeader(keyList.get(index), lastHeader);
			addColumnValue(oDataTable, sKey, columnValue, setHeaderContent);
			
			if(index==keyList.size()-2)
			{
				columnValue = extractValueFromKeys(keyList.get(index+1), null, text);
			sKey=createColumnKeyWithHeader(keyList.get(index+1), lastHeader);
			addColumnValue(oDataTable, sKey, columnValue, setHeaderContent);
			}	
			
		}
	}
	
	private void copyTelephoneFaxNoValues(String cellValue,String lastHeader, ParsedDataTable oDataTable,Set<String> setHeaderContent )
	{
	//	List<String> keyList=getKeysForDrugAppInfo();
		String sKey = null;
		String NoValue = null, extValue = null;
		StringBuffer text=new StringBuffer(cellValue);
		lastHeader=lastHeader+StringConstants.PERIOD;
		NoValue = StringConstants.EMPTY;
		if(text.toString().contains(FIELD_KEY_TEL_NO) && text.toString().contains(FIELD_KEY_EXT))
		{
			sKey=lastHeader+FIELD_KEY_TEL_NO;
			NoValue = text.substring(text.indexOf(FIELD_KEY_TEL_NO)+FIELD_KEY_TEL_NO.length(), text.indexOf(FIELD_KEY_EXT));
		}
		else if(text.toString().contains(FIELD_KEY_TEL_NO_WITH_DOT) && text.toString().contains(FIELD_KEY_EXT))
		{
			sKey=lastHeader+FIELD_KEY_TEL_NO_WITH_DOT;
			NoValue = text.substring(text.indexOf(FIELD_KEY_TEL_NO_WITH_DOT)+FIELD_KEY_TEL_NO_WITH_DOT.length(), text.indexOf(FIELD_KEY_EXT));
		}
		else if(text.toString().contains(FIELD_KEY_FAX_NO) && text.toString().contains(FIELD_KEY_EXT_WITH_DOT))
		{
			sKey=lastHeader+FIELD_KEY_FAX_NO;
			NoValue = text.substring(text.indexOf(FIELD_KEY_FAX_NO)+FIELD_KEY_FAX_NO.length(), text.indexOf(FIELD_KEY_EXT_WITH_DOT));
		}
		else if(text.toString().contains(FIELD_KEY_FAX_NO_WITH_DOT) && text.toString().contains(FIELD_KEY_EXT_WITH_DOT))
		{
			sKey=lastHeader+FIELD_KEY_FAX_NO_WITH_DOT;
			NoValue = text.substring(text.indexOf(FIELD_KEY_FAX_NO_WITH_DOT)+FIELD_KEY_FAX_NO_WITH_DOT.length(), text.indexOf(FIELD_KEY_EXT_WITH_DOT));
		}
		//addColumnValue(oDataTable, sKey, NoValue, setHeaderContent);
		
		if(text.toString().contains(FIELD_KEY_EXT_WITH_DOT))
		{
			if(text.toString().trim().endsWith(FIELD_KEY_EXT_WITH_DOT))
			{
				//sKey = sKey+" Ext.";
				extValue = StringConstants.EMPTY;
			}
			else
			{
				//sKey = sKey+" Ext.";
				extValue = text.substring(text.indexOf(FIELD_KEY_EXT_WITH_DOT)+FIELD_KEY_EXT_WITH_DOT.length()).trim();
			}
		//	addColumnValue(oDataTable, sKey, extValue, setHeaderContent);
			addColumnValue(oDataTable, sKey, NoValue+StringConstants.SPACE+extValue, setHeaderContent);
		}
	}
	
	private void copyAllColumnValues(String header, List<String> oCurrentRecord, 
			ParsedDataTable oDataTable, Set<String> setHeaderContent)
	{
		String cellValue = null;
		String key=null;
		Object oColumnValue = null;
		String safetyReportVersionValue = null;
		String[] cellValueArray=null;
		String[] splitVer = null;
		oColumnValue="";
		for (int nColIndex = 0; nColIndex < oCurrentRecord.size(); nColIndex++)
		{
			cellValueArray=null;
			
		//	cellValue = getTrimmedColValueForCurrentRecord(oCurrentRecord.get(nColIndex));
			cellValue = oCurrentRecord.get(nColIndex);
			if(cellValue.startsWith(FIELD_HEADER_STRUCTURED_DOSAGE_INFO))
			{
				//copyDosagInfoValues(cellValue.replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE), oDataTable, setHeaderContent);
				copyDosagInfoValues(cellValue, oDataTable, setHeaderContent);
			}
			else if(cellValue.startsWith(FIELD_HEADER_HOLDER_AND_AUTHORIZATION_APPLICATION_NO_OF_DRUG))
			{
				copyDrugAppInfoValues(cellValue, oDataTable, setHeaderContent);
			}
			else if((cellValue.contains(TELEPHONE_NUMBER_SECTION_IDENTIFIER) || cellValue.contains(FAX_SECTION_IDENTIFIER)) && cellValue.contains(NUMBER_SECTION_IDENTIFIER_SUFFFIX) )
			{
				//copyDrugAppInfoValues(cellValue.replaceAll("\r", " "), oDataTable, setHeaderContent);
				copyTelephoneFaxNoValues(cellValue, header, oDataTable, setHeaderContent);
			}
			
			else if(cellValue.startsWith(FIELD_HEADER_A_1_11_1_SOURCE_S_A_1_11_2) )
			{
				
				String[] cellA=cellValue.split(StringConstants.CARRIAGERETURN);
				
				if(cellA.length > 1 && cellA[1].equalsIgnoreCase(SOURCE_IDENTIFIER))
				{
					key=createColumnKeyWithHeader(FIELD_HEADER_A_1_11_1_SOURCE_S_A_1_11_2+StringConstants.PERIOD+FIELD_HEADER_DUPLICATE_SOURCE, header);
					addColumnValue(oDataTable, key, StringConstants.EMPTY, setHeaderContent);
					key=createColumnKeyWithHeader(FIELD_HEADER_A_1_11_1_SOURCE_S_A_1_11_2+StringConstants.PERIOD+FIELD_HEADER_DUPLICATE_NUMBER, header);
					addColumnValue(oDataTable, key, StringConstants.EMPTY, setHeaderContent);
				}
				else
				{
					log.error(">>> Possible Value ERROR <<< " + cellA.toString() + "cellA.size=" + (cellA!=null ? cellA.length : 0) + "cellA[0]=" + (cellA.length>0 ? cellA[0] : "(not-available)"));
					//not handle for SFE type of files as it is always blank
				}
			}
			else if(cellValue.startsWith(HEADER_SECTION_SERIOUS_IDENTIFIER1) && cellValue.contains(HEADER_SECTION_SERIOUS_IDENTIFIER2))
			{
				//skip serious section here. it has been handled separately outside the method
			}
			else
			{
				if(cellValue.contains("MedDRA version for\rsender's diagnosis"))
					cellValue=cellValue.replaceAll("MedDRA version for\rsender's diagnosis", "MedDRA version for sender's diagnosis");
				
			if(cellValue.contains(StringConstants.CARRIAGERETURN))
			{
				cellValueArray = cellValue.split(StringConstants.CARRIAGERETURN);
			}
			else
			{
				cellValueArray=new String[1];
				cellValueArray[0]=cellValue;
			}
			if(cellValueArray.length==1)
			{
				key=cellValueArray[0];
				key=createColumnKeyWithHeader(key, header);
				oColumnValue=StringConstants.EMPTY;
				addColumnValue(oDataTable, key, oColumnValue, setHeaderContent);
			}
			else if(cellValueArray.length>=2 && ( (cellValueArray[0].equalsIgnoreCase(SPLILLOVER_HEADER_MED_DRA_VERSION_FOR_MEDICAL) && cellValueArray[1].contains(SPILLOVER_HEADER_MED_DRA_VER_SUFFIX_HISTORY))
					|| (cellValueArray[0].equalsIgnoreCase(SPILLOVER_HEADER_B_2_I_1_A_MED_DRA_VERSION_FOR_PREFIX) && cellValueArray[1].contains(SPLILLOVER_HEADER_REACTION_EVENT_TERM_LLT_SUFFIX))
					|| (cellValueArray[0].equalsIgnoreCase(SPILLOVER_HEADER_B_2_I_2_A_MED_DRA_VERSION_FOR_PREFIX) && cellValueArray[1].contains(SPLILLOVER_HEADER_SUFFIX_REACTION_EVENT_TERM_PT))
					|| (cellValueArray[0].equalsIgnoreCase(SPILLOVER_HEADER_MEDICALLY_CONFIRMED_OR_FROM_HEALTH_PREFIX) && cellValueArray[1].contains(SPILLOVER_HEADER_PROFESSIONAL_SUFFIX))
					|| (cellValueArray[0].equalsIgnoreCase(SPILLOVER_HEADER_TERM_HIGHLIGHTED_BY_THE_PREFIX) && cellValueArray[1].contains(SPILLOVER_HEADER_REPORTER_SUFFIX))))
			{
				createKeyValuesForSpillOver(cellValueArray, oDataTable, setHeaderContent, header);
			}
			
			
			else if(cellValueArray.length==3 && cellValueArray[2].contains("DD") && cellValueArray[2].contains(DATE_FORMAT_IDENTIFIER))
			{
				if(cellValueArray[2].equalsIgnoreCase("CCYYMMDD"))
				{
					//oColumnValue=keyArray[0];
					key = cellValueArray[0];
					key=createColumnKeyWithHeader(key, header);
					oColumnValue = convertDateFormat(cellValueArray[2],cellValueArray[1]);
					addColumnValue(oDataTable, key,key, new StringParsedDataValue(cellValueArray[1],oColumnValue.toString(),DataState.CANONICALIZED,ValidationState.UNVALIDATED), setHeaderContent,true);
					
				}
				else
				{
					key=cellValueArray[0];
					key=createColumnKeyWithHeader(key, header);
					oColumnValue=cellValueArray[1];
					//addColumnValue(oDataTable, key, oColumnValue, setHeaderContent);
					
					addColumnValue(oDataTable, key,key, new StringParsedDataValue(cellValueArray[1],oColumnValue.toString(),DataState.ASIS,ValidationState.REQUIRED_REVIEW), setHeaderContent,true);
					
				/*	key=cellValueArray[0]+DATE_FORMAT_SUFFIX;
					key=createColumnKeyWithHeader(key, header);
					oColumnValue=cellValueArray[2];
					addColumnValue(oDataTable, key, oColumnValue, setHeaderContent);*/
				}
			}
			else if(cellValueArray.length>=2)
			{
				key=cellValueArray[0];
				key=createColumnKeyWithHeader(key, header);
				oColumnValue="";
				for(int index=1;index<cellValueArray.length;index++)
				oColumnValue=oColumnValue+cellValueArray[index].trim();
				if((key.contains(WEIGHT_HEADER_IDENTIFIER) || key.contains(HEIGHT_HEADER_IDENTIFIER) ||
						key.contains(WEIGHT_HEADER_IDENTIFIER_2) || key.contains(HEIGHT_HEADER_IDENTIFIER_2))
						&&
						(oColumnValue.toString().equalsIgnoreCase(HEIGHT_UNIT_CM) || oColumnValue.toString().equalsIgnoreCase(WEIGHT_UNIT_KG)))
				{
					oColumnValue = StringConstants.EMPTY;
				}
				 if(key.endsWith("A.1.0.1 Safety report I.D. (Ver.)"))
				{
					 splitVer=key.split("\\(");
					 key = splitVer[0].trim();
					if(oColumnValue.toString().contains(" ("))
					{
						safetyReportVersionValue = oColumnValue.toString().split(" \\(")[1];
						safetyReportVersionValue = safetyReportVersionValue.replace(")", "");
						oColumnValue =oColumnValue.toString().split(" \\(")[0];
					}
					else
					{
						safetyReportVersionValue = "";
					}
					addColumnValue(oDataTable, key,key, new StringParsedDataValue(oColumnValue.toString(),oColumnValue.toString(),DataState.ASIS,ValidationState.REQUIRED_REVIEW), setHeaderContent,true);
					
					addColumnValue(oDataTable, key+" (Ver.)",key+" (Ver.)", new StringParsedDataValue(safetyReportVersionValue,safetyReportVersionValue,DataState.ASIS,ValidationState.REQUIRED_REVIEW), setHeaderContent,true);
					
				}
				else
				{	
				addColumnValue(oDataTable, key, oColumnValue, setHeaderContent);
			}
			}
			
			}
		}
	}
	
	private void extractContentFromReportHeader(String sAdditionalKeyPrefix, List<String> oCurrentRecord,
			ParsedDataTable oDataTable, Set<String> setHeaderContent,int startIndex) throws SciServiceException
	{
		String reformedKey = null;
		String key=null;
		String[] keyArray=null;
		Object oColumnValue = null;
		for (int nColIndex = startIndex; nColIndex < oCurrentRecord.size(); nColIndex++)
		{
		//	key=getTrimmedColValueForCurrentRecord(oCurrentRecord.get(nColIndex));
			key=oCurrentRecord.get(nColIndex);
			reformedKey = createColumnKeyWithHeader(key, null);
			nColIndex++;
			if(oCurrentRecord.size()>nColIndex)
				oColumnValue = oCurrentRecord.get(nColIndex).trim();
			else
				oColumnValue=StringConstants.EMPTY;
		if(key.contains(FIELD_HEADER_MESSAGE_DATE))
		{
			if(oColumnValue.toString().contains(StringConstants.CARRIAGERETURN))
		{
			keyArray=oColumnValue.toString().split(StringConstants.CARRIAGERETURN);
		
			if( keyArray.length==2 && keyArray[1].contains(DATE_FORMAT_IDENTIFIER))
			{
				if(keyArray[1].equalsIgnoreCase("CCYYMMDDHHMMSS"))
				{
					//oColumnValue=keyArray[0];
					if(keyArray[0].trim().contains(" ") || keyArray[0].trim().contains("-") || keyArray[0].trim().contains("\\"))
					{
						throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_MESSAGE_DATE);
					}
					oColumnValue = convertDateFormat(keyArray[1],keyArray[0]);
					addColumnValue(oDataTable, key,key, new StringParsedDataValue(keyArray[0],oColumnValue.toString(),DataState.CANONICALIZED,ValidationState.UNVALIDATED), setHeaderContent,true);
					
				}
				else
				{
				
					throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_MESSAGE_DATE);
				/*oColumnValue=keyArray[0];
				addColumnValue(oDataTable, key,key, new StringParsedDataValue(keyArray[0],oColumnValue.toString(),DataState.ASIS,ValidationState.REQUIRED_REVIEW), setHeaderContent,true);*/
				}
				}
			else
			{
				throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_MESSAGE_DATE);
			}
		}
		
		else
		{
			throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_MESSAGE_DATE);
		}
		}
		else
		{
			//throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_VALIDATION_MESSAGE_DATE);
			addColumnValue(oDataTable, reformedKey, oColumnValue, setHeaderContent);
		}
	}
	}
	
	
private static String convertDateFormat(String inputFormat, String dateValue)
	{
		String outputDate = null;
		String day = null, year = null, month = null;
		if(inputFormat.equalsIgnoreCase("CCYYMMDD") && dateValue.length()==8)
		{
			year = dateValue.substring(0, 4);
			month = dateValue.substring(4, 6);
			day = dateValue.substring(6);
			outputDate = day+"-"+getMonth(month)+"-"+year;
		}
		else if(inputFormat.equalsIgnoreCase("CCYYMMDDHHMMSS") && dateValue.length()==14)
		{
			String afterParsedDate = null;
			Date inputDate = null;
			try {
				inputDate = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).parse(dateValue);
			} catch (ParseException e) {
				
				log.error("Failed to Parsed Message Date"+ e.getMessage());
		}
			afterParsedDate = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH).format(inputDate);
			outputDate = afterParsedDate;
		}
		else
		{
			outputDate = dateValue;
		}
	//	outputDate = day+"-"+getMonth(month)+"-"+year;
	//	outputDate = afterParsedDate;
		/*if(outputDate.length()!=11)
			return StringConstants.EMPTY;
		else*/
			return outputDate;
	}
private static String getMonth(String monthNumber)
{
	Map<String, String> monthList = new HashMap<String, String>();
	String month = null;
	monthList.put("01","JAN");
	monthList.put("02","FEB");
	monthList.put("03","MAR");
	monthList.put("04","APR");
	monthList.put("05","MAY");
	monthList.put("06","JUN");
	monthList.put("07","JUL");
	monthList.put("08","AUG");
	monthList.put("09","SEP");
	monthList.put("10","OCT");
	monthList.put("11","NOV");
	monthList.put("12","DEC");
	
	month = monthList.get(monthNumber);
	if(month == null)
		return StringConstants.EMPTY;
	else
		return month;
}
	
	private String extractValueFromKeys(String firstKey,String secondKey,StringBuffer sCurrentRowKey)
	{
		String firstKeyValue=null;
		int firstKeyIndex=0; 
		int	secondKeyIndex=0;
		firstKeyIndex=sCurrentRowKey.indexOf(firstKey);
		if(secondKey==null)
		{
			secondKeyIndex=-2;
		}
		else
		{
			secondKeyIndex=sCurrentRowKey.indexOf(secondKey);
		}
		
		if(firstKeyIndex!=-1 && secondKeyIndex!=-1 && firstKeyIndex<=secondKeyIndex)
		{
			if((firstKeyIndex+firstKey.length()+1)<=sCurrentRowKey.length()-1 && firstKeyIndex+firstKey.length()+1<=secondKeyIndex)
			{
				firstKeyValue=sCurrentRowKey.substring(firstKeyIndex+firstKey.length()+1,secondKeyIndex).trim();
			}
			else
			{
				firstKeyValue=sCurrentRowKey.substring(firstKeyIndex+firstKey.length(),secondKeyIndex).trim();
			}
			//sCurrentRowKey.replace(firstKeyIndex, secondKeyIndex-1, "");
			return firstKeyValue;	
		}
		else if(firstKeyIndex!=-1 && secondKeyIndex==-2 )
		{
			if((firstKeyIndex+firstKey.length()+1)<=sCurrentRowKey.length()-1)
			{
				firstKeyValue=sCurrentRowKey.substring(firstKeyIndex+firstKey.length()+1).trim();
			}
			else
			{
				firstKeyValue=sCurrentRowKey.substring(firstKeyIndex+firstKey.length()).trim();
			}
			
			return firstKeyValue;	
		}
		else
			return StringConstants.EMPTY;
	}
	private List<String> getKeysForDrugInfo()
	{
		List<String> keyList= null;
		String predefinedKeys="Dose(no.),Dose ,No.of separate dosages,No.of units in the interval,Definition of the interval,"
				+ "Cumulative dose to first reaction(no.),Cumulative dose to first reaction ";
				
				
		keyList = Arrays.asList(predefinedKeys.split(","));
		
		return keyList;
	}
	
	private List<String> getKeysForDrugAppInfo()
	{
		List<String> keyList= null;
		String predefinedKeys="Authorization/Application No.,Country of authorization/application,Name of holder/applicant";
				
		keyList = Arrays.asList(predefinedKeys.split(","));
		
		return keyList;
	}
	private void extractSeriousData(List<List<String>> p_listRowsOfAllTablesForSeriousness, ParsedDataTable oDataTable, Set<String> setHeaderContent)
	{
		String headerForSerious = null;
		String seriousValue = null;
		String hospitalizationValue = null;
		headerForSerious=StringConstants.EMPTY;
			
			
			for(List<String> oCurrentRecord2 : p_listRowsOfAllTablesForSeriousness)
			{
				if(oCurrentRecord2.size()>=5 && (oCurrentRecord2.get(4).equals(SERIOUS_ROW_IDENTIFIER_1) || (oCurrentRecord2.get(3).equals(SERIOUS_ROW_IDENTIFIER_2) && oCurrentRecord2.get(4).equals(SERIOUS_ROW_IDENTIFIER_3))
						|| (oCurrentRecord2.get(3).equals(SERIOUS_ROW_IDENTIFIER_4) && oCurrentRecord2.get(4).equals(SERIOUS_ROW_IDENTIFIER_5))))
				{
					headerForSerious=oCurrentRecord2.get(4);
					continue;

					
				}
				if(headerForSerious.equalsIgnoreCase(SERIOUS_ROW_IDENTIFIER_1))
				{
					if(oCurrentRecord2.get(3).equalsIgnoreCase(EMPTY_ROW_IDENTIFIER_FOR_SERIOUS))
					{
						seriousValue = StringConstants.EMPTY;
						hospitalizationValue = StringConstants.EMPTY;
					}
					else
					{
						seriousValue = oCurrentRecord2.get(3).trim();
						hospitalizationValue = oCurrentRecord2.get(4).trim();
					}
					addColumnValue(oDataTable, HEADER_SECTION_SERIOUS_IDENTIFIER2, seriousValue, setHeaderContent);
					addColumnValue(oDataTable, FIELD_HEADER_CAUSED_PROLONGED_HOSPITALIZATION, hospitalizationValue, setHeaderContent);
					headerForSerious=StringConstants.EMPTY;
				}
				else if(headerForSerious.equalsIgnoreCase(SERIOUS_ROW_IDENTIFIER_3))
				{
					addColumnValue(oDataTable, SERIOUS_FIELD_HEADER_RESULTED_IN_DEATH, oCurrentRecord2.get(3).trim(), setHeaderContent);
					addColumnValue(oDataTable, SERIOUS_FIELD_HEADER_DISABLING_INCAPACITATING, oCurrentRecord2.get(4).trim(), setHeaderContent);
					headerForSerious=StringConstants.EMPTY;
				}
				else if(headerForSerious.equalsIgnoreCase(SERIOUS_ROW_IDENTIFIER_5))
				{
					addColumnValue(oDataTable, SERIOUS_FIELD_HEADER_LIFE_THREATENING, oCurrentRecord2.get(3).trim(), setHeaderContent);
					addColumnValue(oDataTable, SERIOUS_FIELD_HEADER_CONGENITAL_ANOMALY_BIRTH_DEFECT, oCurrentRecord2.get(4).trim(), setHeaderContent);
					headerForSerious=StringConstants.EMPTY;
				}
			}
		
	}
	private void extractNarrativeData(String fileContent, ParsedDataTable oDataTable, Set<String> setHeaderContent) throws SciException
	{
		String lastHeader = null;
		String dateValue = null;
		String junkHeaderContent = null;
		StringBuffer narrativeString = null;
		String sKey = null;
		StringParsedDataValue value= null;
		lastHeader = CASE_NARRATIVE_SECTION_IDENTIFIER;
		
		fileContent = fileContent.substring(fileContent.indexOf(CASE_NARRATIVE_STRING_START_DELIMITER_TEXT), 
							fileContent.indexOf(CASE_NARRATIVE_STRING_END_DELIMITER_TEXT));//.replaceAll("\r", "");
		if(fileContent.contains(PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT))
		{
			junkHeaderContent = fileContent.substring(fileContent.indexOf(PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT)+PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT.length());
			junkHeaderContent = junkHeaderContent.substring(0, junkHeaderContent.indexOf(StringConstants.CRLF));
			junkHeaderContent = PAGE_HEADER_SUBSEQUENTPAGE_IDENTIFIER_TEXT + junkHeaderContent;
			fileContent = fileContent.replace(junkHeaderContent, StringConstants.EMPTY);
		}
		fileContent = fileContent.replaceAll(StringConstants.CRLF, StringConstants.EMPTY);
		
		narrativeString = new StringBuffer();
		narrativeString.append(fileContent);
		AACaseNarrativeFieldParser oNarrativeParser = new AACaseNarrativeFieldParser(m_oParsingConfiguration);
		Map<String, StringParsedDataValue> narrativeKeyMap = oNarrativeParser.parse(narrativeString.toString());
		
		for(Entry<String, StringParsedDataValue> entry: narrativeKeyMap.entrySet())
		{
			sKey=createColumnKeyWithHeader(entry.getKey(), lastHeader);
			if(sKey.contains("Date"))
			{
				
					value = entry.getValue();
					dateValue = value.rawValue().toString();
					dateValue = convertDateForNarrative(dateValue);
					addColumnValue(oDataTable, sKey,sKey, new StringParsedDataValue(value.rawValue().toString(),dateValue,DataState.CANONICALIZED,ValidationState.UNVALIDATED), setHeaderContent,true);
				
			}
			else
			{
					value = entry.getValue();
					addColumnValue(oDataTable, sKey,sKey, value, setHeaderContent,true);
			}
		}
	}
	private String convertDateForNarrative(String dateValue)
	{
		String[] dateValueArray = null;
		String day = null;
		String month = null;
		String year = null;
		String outputDate = null;
		day = StringConstants.EMPTY;
		month = StringConstants.EMPTY;
		year = StringConstants.EMPTY;
		if(dateValue.length()>0 && dateValue.contains("-") && dateValue.length()<=11)
		{
			dateValueArray = dateValue.split("-");
			if(dateValueArray.length==3)
			{
				if(dateValueArray[0].length()==2 && dateValueArray[2].length()==4)
				{
					if(dateValueArray[1].length()==2)
						month = getMonth(dateValueArray[1]);
					else
						month = dateValueArray[1].toUpperCase();
					day = dateValueArray[0];
					year = dateValueArray[2];
				}
				else if(dateValueArray[0].length()==4 && dateValueArray[2].length()==2)
				{
					if(dateValueArray[1].length()==2)
						month = getMonth(dateValueArray[1]);
					else
						month = dateValueArray[1];
					day = dateValueArray[2];
					year = dateValueArray[0];
				}
				else if(dateValueArray[0].length()==4 && dateValueArray[2].length()>2 && dateValueArray[2].contains(":"))
				{
					if(dateValueArray[1].length()==2)
						month = getMonth(dateValueArray[1]);
					else
						month = dateValueArray[1];
					day = dateValueArray[2].substring(0, 2);
					year = dateValueArray[0];
				}
				else
				{
						
				}
			}
			else
			{
				
			}
			outputDate = day+"-"+month+"-"+year;
		}
		else if(dateValue.length()==14 && (!dateValue.contains("-")) && (!dateValue.contains("\\")) )
		{
		
			//tring trDate="20120106";    
			Date inputDate = null;
			try {
				inputDate = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).parse(dateValue);
			} catch (ParseException e) {
				log.error("Failed to Parsed Message Date"+ e.getMessage());
			}
			String afterParsedDate = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH).format(inputDate);
			
			outputDate = afterParsedDate;
		}
		else if(dateValue.contains("T") && dateValue.contains("Z") && dateValue.length()==24)
		{
			//inputDate="2017-12-21T08:56:00.000Z";
			dateValue = dateValue.replace("T", "");
			dateValue = dateValue.replace("Z", "");
			Date inputDate = null;
			try {
				inputDate = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss.SSS", Locale.ENGLISH).parse(dateValue);
			} catch (ParseException e) {
				log.error("Failed to Parsed Message Date"+ e.getMessage());
			}
			String afterParsedDate = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS", Locale.ENGLISH).format(inputDate);
			outputDate = afterParsedDate;
		}
		else
		{
			outputDate = dateValue;
		}
		
		if(outputDate.equals("--"))
			outputDate = StringConstants.EMPTY;
		
		return outputDate;
	}
	private void createKeyValuesForSpillOver(String[] cellValueArray, ParsedDataTable oDataTable,Set<String> setHeaderContent, String header)
	{
		String key = null;
		String oColumnValue = null ;
		oColumnValue= StringConstants.EMPTY;
		key=cellValueArray[0]+StringConstants.SPACE+cellValueArray[1];
			key=createColumnKeyWithHeader(key, header);
		if(cellValueArray.length>2)
			{
				for(int cellIndex=2; cellIndex<cellValueArray.length; cellIndex++)
				{
					if(cellIndex == 2)
					oColumnValue = cellValueArray[cellIndex];
					else
					{
						oColumnValue = oColumnValue + StringConstants.CARRIAGERETURN + cellValueArray[cellIndex];
					}
				}	
			}
		addColumnValue(oDataTable, key,key, new StringParsedDataValue(oColumnValue, oColumnValue, DataState.ASIS, ValidationState.UNVALIDATED), setHeaderContent,true);
		
	}
	
	public FileParsingConfiguration loadValidationFile()
	{
		FileParsingField field = null;
		FileParsingConfiguration filePassConfig = null;

		filePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AA_KEY);
	
	    for(String key :  filePassConfig.getFieldMap().keySet())

	    {

	           field = filePassConfig.getField(key);
	           m_oParsingConfiguration.addField(field);
	    

	    }
	    
	    return filePassConfig;
	}
	public ValidationState applyValidationRules(String p_sMnemonicName, String displayValue)
	{
		FileParsingField mfield = null;
		ValidationState vState = null;
		String validationRule = null;
		String[] legalValues = null;
	    String editedMnemonicPrefix = null;
	    String editedMnemonicSuffix = null;
	    boolean	matchedFlag = false;
	    String regex = null;
	    ValidationState vFailedStatusType;
	    boolean isMandatory = false;
	    
	    String pattern = "[0-9]*";
	    
		if(p_sMnemonicName.contains("_"))
		{
			editedMnemonicPrefix = p_sMnemonicName.substring(0,p_sMnemonicName.lastIndexOf("_"));
			editedMnemonicSuffix = p_sMnemonicName.substring(p_sMnemonicName.lastIndexOf("_"));
			if(editedMnemonicSuffix.length()>=2)
			{
				matchedFlag	=	Pattern.compile(pattern).matcher(editedMnemonicSuffix.substring(1)).matches();
			}
		}
		if(matchedFlag)
		{
			mfield = m_oParsingConfiguration.getField(editedMnemonicPrefix);
		}
		else
		{
			mfield = m_oParsingConfiguration.getField(p_sMnemonicName);
		}
	    if(mfield == null || displayValue == null || displayValue.equalsIgnoreCase(""))
	    {
	    	vState = ValidationState.UNVALIDATED;
	    }
	    else
	    {
		validationRule = mfield.getValidationRule();
		isMandatory = mfield.isMandatory();
		legalValues = mfield.getLegalValues();
		if(mfield.getErrorType().equals("WARN"))
			vFailedStatusType = ValidationState.VALIDATED_WITH_WARNINGS;
		else
			vFailedStatusType = ValidationState.REQUIRED_REVIEW;
		
		if(isMandatory && displayValue.equalsIgnoreCase(""))
		{
			vState = vFailedStatusType;
		
		}
		else
		{
		if(validationRule.equalsIgnoreCase("DATE"))
		{
			if(ValidationUtils.validateField(displayValue.toUpperCase(), SciConstants.RegExConstants.DATE))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("ALPHABETS"))
		{
			if(ValidationUtils.validateField(displayValue, "^[A-Za-z ]*"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("NUMBER"))
		{
			if(ValidationUtils.validateField(displayValue, SciConstants.RegExConstants.INTEGER))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
		}
		else if(validationRule.equalsIgnoreCase("GENERAL TEXT"))
		{
			vState = ValidationState.VALIDATION_NOT_APPLICABLE;
			
		}
		else if(validationRule.equalsIgnoreCase("ALPHANUMERIC"))
		{
			if(ValidationUtils.validateField(displayValue, "^[A-Za-z0-9 ]*$"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
			
		}
		else if(validationRule.equalsIgnoreCase("DATETIME"))
		{//have to change the regex for datetime
			if(ValidationUtils.validateField(displayValue, "^(([0-3]{1}[0-9]{1})?\\-?[a-z,A-Z]{3}?\\-[0-9]{4}?\\ [0-9]{2}?\\:[0-9]{2}?\\:[0-9]{2}?)+$"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
			
		}
		else if(validationRule.equalsIgnoreCase("DATETIMEMILLISEC"))
		{//have to change the regex for datetime
			if(ValidationUtils.validateField(displayValue, "^(([0-3]{1}[0-9]{1})?\\-?[a-z,A-Z]{3}?\\-[0-9]{4}?\\ [0-9]{2}?\\:[0-9]{2}?\\:[0-9]{2}?\\.[0-9]{3}?)+$"))
				vState = ValidationState.VALIDATED;
			else
				vState = vFailedStatusType;
			
		}
		else if(validationRule.equalsIgnoreCase("LEGAL VALUES"))
		{
			//legalValues = mfield.getM_sarrLegalValues();
			//errorType = mfield.getErrorType();
			if(legalValues.length==1 && legalValues[0].contains("\n "))
			{
				legalValues[0] = legalValues[0].replace("\n", "").trim();
				
			}
			for(int index =0; index<legalValues.length; index++)
			{
				legalValues[index]=legalValues[index].toUpperCase();
			}
			if(legalValues!= null && (!legalValues[0].equals("")) && (!displayValue.equalsIgnoreCase("")) &&(!Arrays.asList(legalValues).contains( displayValue.toUpperCase())))
			{
				vState = vFailedStatusType;
			}
			else if(legalValues!= null && (!legalValues[0].equals("")) && (!displayValue.equalsIgnoreCase("")) &&(Arrays.asList(legalValues).contains( displayValue.toUpperCase())))
				vState = ValidationState.VALIDATED;
			else
				vState = ValidationState.UNVALIDATED;
		}
		else if(validationRule.equalsIgnoreCase("CUSTOMIZE"))
		{
			legalValues = mfield.getLegalValues();
			if(legalValues!= null)
			regex = legalValues[0];
			else
				regex = "";
			matchedFlag = Pattern.compile(regex).matcher(displayValue).matches();
			if(displayValue.equalsIgnoreCase(""))
			{
				vState = ValidationState.UNVALIDATED;
			}
			else if(matchedFlag)
			{
				vState = ValidationState.VALIDATED;
			}
			else if((!displayValue.equalsIgnoreCase("")) && !matchedFlag)
				vState = vFailedStatusType;
			
		}
		else
		{
			vState = ValidationState.UNVALIDATED;
		}
		}
	
	    }
	   return vState;
	}
	@Override
	public Map<String, List<String>> compareMultipleDataTable(List<ParsedDataTable> m_DdataTableList)
	{
		String displayName = null;
		String nDisplayName = null;
		List<String> valueList = null;
		String subGroup = null;
	//	Map<String, List<String>> keyMap = new HashMap<String, List<String>>();
		Map<String, List<String>> keyMap = new ListOrderedMap<String, List<String>>();
		for(String mnemonic : m_oParsingConfiguration.getFieldMap().keySet())
		{
			subGroup = m_oParsingConfiguration.getField(mnemonic).getM_sFieldSubGroup();
			displayName = m_oParsingConfiguration.getField(mnemonic).getFieldLabel().toString();
			//valueList = new ArrayList<String>();
			int maxCount = 1;
			int keyCount = 1;
			if(!m_oParsingConfiguration.getField(mnemonic).isMultivalued())
			{
				//valueList = new ArrayList<String>();
				for(ParsedDataTable oDataTable: m_DdataTableList)
				{
					if(oDataTable.isColumnPresent(mnemonic+"_Count"))
					{
						keyCount =Integer.parseInt(oDataTable.getColumnValues(mnemonic+"_Count").get(0).displayValue().toString());
						if(maxCount < keyCount)
						{
							maxCount = keyCount;
						}
					}
					else
					{
						//multivalued but count field is not present
					}
				}
/*				if(maxCount > 1)
				{
				
*/				nDisplayName = displayName;
				for(int index =1; index<= maxCount; index++)
					{
						 
						valueList = new ArrayList<String>();
						if(maxCount==1)
						{
						}
						else
						{
						displayName = nDisplayName+" "+index;
						}
						for(ParsedDataTable oDataTable: m_DdataTableList)
						{
							if(oDataTable.isColumnPresent(mnemonic+"_"+index))
							{
								valueList.add(oDataTable.getColumnValues(mnemonic+"_"+index).get(0).displayValue().toString());
								
							}
							else
							{
								if(index == 1)
								{
									if(oDataTable.isColumnPresent(mnemonic))
									{
										valueList.add(oDataTable.getColumnValues(mnemonic).get(0).displayValue().toString());
										
									}	
									else
										valueList.add(StringConstants.EMPTY);
										
								}
								else
								{
									valueList.add(StringConstants.EMPTY);
								}
								//multivalued but count field is not present
							}
						}
						keyMap.put(subGroup+"##"+displayName, valueList);
					}
				//}
			}
			else
			{
				valueList = new ArrayList<String>();
				for(ParsedDataTable oDataTable: m_DdataTableList)
				{
					if(oDataTable.isColumnPresent(mnemonic))
					{
						valueList.add(oDataTable.getColumnValues(mnemonic).get(0).displayValue().toString());
					}
					else
					{
						valueList.add(StringConstants.EMPTY);
					}
						
				}
				keyMap.put(subGroup+"##"+displayName, valueList);
			}
			
			
		}
		//addingFileNames to keyMap
		List<String> fileNameList = new ArrayList<String>();
		List<String> fileTypeList = new ArrayList<String>();
		for(ParsedDataTable oDataTable: m_DdataTableList)
		{
			if(oDataTable.isColumnPresent("FileName"))
			{
				fileNameList.add(oDataTable.getColumnValues("FileName").get(0).displayValue().toString());
			}
			else
			{
				fileNameList.add(StringConstants.EMPTY);
			}
			
			if(oDataTable.isColumnPresent("fileType"))
			{
				fileTypeList.add(oDataTable.getColumnValues("fileType").get(0).displayValue().toString());
			}
			else
			{
				fileTypeList.add(StringConstants.EMPTY);
			}
				
		}
		keyMap.put("FileName", fileNameList);
		keyMap.put("fileType", fileTypeList);
		
		return keyMap;
	}
	
}
