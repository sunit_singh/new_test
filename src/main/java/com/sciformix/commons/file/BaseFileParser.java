package com.sciformix.commons.file;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public abstract class BaseFileParser implements IFileParser
{
	private static final Logger log = LoggerFactory.getLogger(BaseFileParser.class);
	protected final static String TEST_FILE_EXTENSION_INPUT = ".test";
	protected final static String TEST_FILE_EXTENSION_OUTPUT = ".testout";
	protected final static String TEST_FILE_EXTENSION_PARSEDDATATABLE = ".pdt";
	
	
	protected String m_sDocumentLocalPath = null;
	protected String m_sFtlTemplateFileName = null;
	protected boolean m_bTestMode = false;
	
	public List<String> keyHeaderList = null;
	protected FileParsingConfiguration m_oParsingConfiguration = null;
	
	/*protected BaseFileParser(String p_sDocumentLocalPath)
	{
		m_sDocumentLocalPath = p_sDocumentLocalPath;
//		m_sFtlTemplateFileName = p_sFtlTemplateFileName;
	}*/
	
	public abstract Map<String, List<String>> compareMultipleDataTable(List<ParsedDataTable> m_DdataTableList);
	
	
	protected BaseFileParser(String p_sDocumentLocalPath, String p_sFtlTemplateFileName)
	{
		this(p_sDocumentLocalPath,  p_sFtlTemplateFileName, false);
	}
	
	protected BaseFileParser(String p_sDocumentLocalPath, String p_sFtlTemplateFileName, boolean p_bTestMode)
	{
		m_sDocumentLocalPath = p_sDocumentLocalPath;
		m_sFtlTemplateFileName = p_sFtlTemplateFileName;
		m_bTestMode = p_bTestMode;
		keyHeaderList = new ArrayList<String>();
	}
	
	public void setKeyHeaderList(String[] p_KeyHeaderList)
	{
		this.keyHeaderList = Arrays.asList(p_KeyHeaderList);
	}
	
	
	public String getFtlTemplateFileName()
	{
		return m_sFtlTemplateFileName;
	}
	
	@Override
	public final ParsedDataTable parse() throws SciException, SciServiceException
	{
		ParsedDataTable oDataTable = null;
	
		if (!canDocumentBeParsed())
		{
			//log.error("File Type and File does not match");
			//throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_FILE_TYPE_AND_FILE_DOES_NOT_MATCH);
		}
		
		oDataTable=parseDocument();
		
		if (!validateParsedDocumentData())
		{
			//Do something else here
		}
		
		return oDataTable;
	}

	//check filetype,size,filenotfound,path,
	protected abstract boolean canDocumentBeParsed() throws SciServiceException;
	
	public ParsedDataTable MapAutomatedKeysToMnemonics(ParsedDataTable m_ParsedDataTable)
	{
		return m_ParsedDataTable;
	}
	
	protected abstract ParsedDataTable parseDocument() throws SciException, SciServiceException;
	
	protected abstract boolean validateParsedDocumentData();
	
	protected abstract ValidationState applyValidationRules(String p_sMnemonicName, String displayValue);
	
	protected void addColumnValue(ParsedDataTable oDataTable, String p_sMnemonicName, Object p_ColumnValue, Set<String> setHeaderContent)
	{
		String m_sLabel = null;
		DataState dataState= null;
		String displayValue = null;
		ValidationState vState = null;
		
		if(p_sMnemonicName.toString().contains(StringConstants.CARRIAGERETURN))
		{
			p_sMnemonicName = p_sMnemonicName.replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE).trim();
		}
		
		m_sLabel=p_sMnemonicName;
		
		
		
		if(p_ColumnValue.toString().contains(StringConstants.CARRIAGERETURN))
		{
			displayValue=p_ColumnValue.toString().replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE).trim();
			dataState=DataState.PHRASEWRAP;
		}
		else
		{
			displayValue=p_ColumnValue.toString().trim();
			dataState=DataState.ASIS;
		}
		if(p_sMnemonicName== null || p_sMnemonicName.equals(StringConstants.EMPTY))
		{
			
		}
		else
		{//validation done here
			
		vState = applyValidationRules(p_sMnemonicName, displayValue);

		if (setHeaderContent.contains(p_sMnemonicName))
		{
			oDataTable.appendMnemonicColumnValue(p_sMnemonicName, new StringParsedDataValue(p_ColumnValue.toString(), displayValue, dataState, vState));
		}
		else
		{
			oDataTable.addColumn(p_sMnemonicName, m_sLabel, new StringParsedDataValue(p_ColumnValue.toString(), displayValue, dataState, vState) );
			setHeaderContent.add(p_sMnemonicName);
		}
		}
	}
	

	
	protected void addColumnValue(ParsedDataTable oDataTable, String p_sMnemonicName,String p_Label, StringParsedDataValue p_ColumnValue, Set<String> setHeaderContent, boolean booleanFlag)
	{
		
		String m_sLabel = null;
		String m_nColumnValue = null;
		DataState dataState= null;
		ValidationState vState = null;
		String displayValue = null;
		
		
		if(p_sMnemonicName.toString().contains(StringConstants.CARRIAGERETURN))
		{
			p_sMnemonicName = p_sMnemonicName.replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE).trim();
		}
		//assigning currently existing mnemonic value untill validation done
		//m_oParsingConfiguration.getField(p_sMnemonic)
		m_sLabel=p_Label;
		m_nColumnValue = p_ColumnValue.rawValue().toString();
		vState = p_ColumnValue.validationState();
		if(p_ColumnValue.dataState().equals(DataState.CANONICALIZED))
		{
			displayValue = p_ColumnValue.displayValue().toString();
			dataState = DataState.CANONICALIZED;
		}
		else
		{
		if(m_nColumnValue.contains(StringConstants.CARRIAGERETURN))
		{
			displayValue=m_nColumnValue.replaceAll(StringConstants.CARRIAGERETURN, StringConstants.SPACE).trim();
			dataState=DataState.PHRASEWRAP;
		}
		else
		{
			displayValue=m_nColumnValue.trim();
			dataState=DataState.ASIS;
		}
		}
		if(p_sMnemonicName== null || p_sMnemonicName.equals(StringConstants.EMPTY))
		{
			
		}
		else
		{//validation done here
			vState = applyValidationRules(p_sMnemonicName, displayValue);
		if (setHeaderContent.contains(p_sMnemonicName))
		{
		//	oDataTable.appendMnemonicColumnValue(p_sMnemonicName, p_ColumnValue.toString(),m_nColumnValue.toString(), DataState.ASIS);
			oDataTable.appendMnemonicColumnValue(p_sMnemonicName, new StringParsedDataValue(m_nColumnValue, displayValue, dataState, vState));
			if (vState == ValidationState.REQUIRED_REVIEW)
			{
				oDataTable.incrementReviewRequiredCount();
			}
		}
		else
		{
			oDataTable.addColumn(p_sMnemonicName, m_sLabel, new StringParsedDataValue(m_nColumnValue,displayValue, dataState, vState) );
			if (vState == ValidationState.REQUIRED_REVIEW)
			{
				oDataTable.incrementReviewRequiredCount();
			}
			setHeaderContent.add(p_sMnemonicName);
		}
		}
	}
}
