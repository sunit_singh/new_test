/**
 * 
 */
package com.sciformix.commons;

/**
 * @author sshetty1
 *
 */
public class WebErrorCodes 
{
	public static class SimpleWebErrorCode extends ErrorCode
	{
		private SimpleWebErrorCode(String p_sNamespace, int p_errorcode)
		{
			super(p_sNamespace, p_errorcode);
		}
	}
	
	public static class WebErrorCode extends ErrorCode
	{
		private WebErrorCode(String p_sNamespace, int p_errorcode)
		{
			super(p_sNamespace, p_errorcode);
		}
	}
	
	public static class SourceDocUtil
	{
		private static final String NAMESPACE = "sourcedocutil";
		public static final SimpleWebErrorCode ERROR_IN_FILE_PARSING = new SimpleWebErrorCode(NAMESPACE, 801);
		public static final SimpleWebErrorCode ERROR_IN_FILE_DOWNLOADING = new SimpleWebErrorCode(NAMESPACE, 802);
		public static final SimpleWebErrorCode ERROR_FILE_NOT_AVAILABLE = new SimpleWebErrorCode(NAMESPACE, 803);
		public static final SimpleWebErrorCode ERROR_FILE_ALREADY_DOWNLOADED = new SimpleWebErrorCode(NAMESPACE, 804);
		public static final SimpleWebErrorCode ERROR_FILE_NOT_UPLOADED = new SimpleWebErrorCode(NAMESPACE, 805);
        public static final SimpleWebErrorCode ERROR_EXPORT_AS_TEXT = new SimpleWebErrorCode(NAMESPACE, 806);
        public static final SimpleWebErrorCode ERROR_EXPORT_AS_EXEL = new SimpleWebErrorCode(NAMESPACE, 807);
        public static final SimpleWebErrorCode ERROR_EXPORT_AS_CSV = new SimpleWebErrorCode(NAMESPACE, 808);
        public static final SimpleWebErrorCode ERROR_FILE_TYPE_FILE_DIFFERENT = new SimpleWebErrorCode(NAMESPACE, 809);
        public static final SimpleWebErrorCode ERROR_FILE_SPLITTING = new SimpleWebErrorCode(NAMESPACE, 810);
        

	}
	public static class System
	{
		private static final String NAMESPACE = "system";
		
		public static final SimpleWebErrorCode SYSTEM_NOT_BOOTSTRAPPED = new SimpleWebErrorCode(NAMESPACE, 1);
		public static final SimpleWebErrorCode SESSION_INVALID = new SimpleWebErrorCode(NAMESPACE, 2);
		public static final SimpleWebErrorCode LOGIN_CREDENTIALS_NOT_SUPPLIED = new SimpleWebErrorCode(NAMESPACE, 3);
		public static final SimpleWebErrorCode ERROR_SAVING_PREFERENCE = new SimpleWebErrorCode(NAMESPACE, 4);
		public static final SimpleWebErrorCode SYSTEM_HAS_ENCOUNTERED_AN_ISSUE = new SimpleWebErrorCode(NAMESPACE, 5);
		
		public static final WebErrorCode PORTAL_APP_NOT_AVAILABLE = new WebErrorCode(NAMESPACE, 50);
		public static final WebErrorCode PORTAL_ACTION_NOT_AVAILABLE = new WebErrorCode(NAMESPACE, 51);
		public static final WebErrorCode LOGIN_CREDENTIALS_INCORRECT = new WebErrorCode(NAMESPACE, 52);
		public static final WebErrorCode APPLICATION_NOT_ENABLED_FOR_USER = new WebErrorCode(NAMESPACE, 53);
		public static final WebErrorCode USER_NOT_REGISTERED = new WebErrorCode(NAMESPACE, 54);
		public static final WebErrorCode INVALID_ACTION = new WebErrorCode(NAMESPACE, 55);
		public static final WebErrorCode INVALID_USERTYPE = new WebErrorCode(NAMESPACE, 56);
		
		public static final WebErrorCode APPLICATION_REQUIRES_PROJECT_ASSOCIATION = new WebErrorCode(NAMESPACE, 61);
		public static final WebErrorCode APPLICATION_REQUIRES_MAX_ONE_PROJECT_ASSOCIATION = new WebErrorCode(NAMESPACE, 62);
		public static final WebErrorCode INVALID_REQUEST_PARAMETER = new WebErrorCode(NAMESPACE, 63);
	}
	
	public static class Ngv
	{
		private static final String NAMESPACE = "ngv";
		/**
		 * Ranges - 
		 * 
		 * Simple: 101 - 130
		 * Standard: 131 - 150
		 */
		public static final SimpleWebErrorCode CASEID_EMPTY = new SimpleWebErrorCode(NAMESPACE, 101);
		public static final SimpleWebErrorCode SAFETYDB_EMPTY = new SimpleWebErrorCode(NAMESPACE,102);
		public static final SimpleWebErrorCode CASE_FILE_NOT_UPLOADED = new SimpleWebErrorCode(NAMESPACE,103);
		public static final SimpleWebErrorCode RULESET_IMPORT_FILE_NOT_UPLOADED = new SimpleWebErrorCode(NAMESPACE,104);
		public static final SimpleWebErrorCode CASE_FILE_PARSING_FAILED = new SimpleWebErrorCode(NAMESPACE,105);
		public static final SimpleWebErrorCode CASE_FILE_NOT_AVAILABLE = new SimpleWebErrorCode(NAMESPACE,106);
		public static final SimpleWebErrorCode NARRATIVE_TEMPLATE_NOT_CONFIGURED = new SimpleWebErrorCode(NAMESPACE,107);
		public static final SimpleWebErrorCode EMPTY_TEMPLATE_RULESET = new SimpleWebErrorCode(NAMESPACE,108);		
		public static final SimpleWebErrorCode ERROR_EXPORTING_FILE = new SimpleWebErrorCode(NAMESPACE,109);	
		public static final SimpleWebErrorCode DEFAULT_TEMPLATE_NOT_SELECTED = new SimpleWebErrorCode(NAMESPACE,110); 
		public static final SimpleWebErrorCode ERROR_SAVING_PARSING_FILE = new SimpleWebErrorCode(NAMESPACE,111); 
		public static final SimpleWebErrorCode TEMPLATE_NOT_CONFIGURED_IN_USERPREFS = new SimpleWebErrorCode(NAMESPACE,112); 
		public static final SimpleWebErrorCode TEMPLATE_IS_DEACTIVATED = new SimpleWebErrorCode(NAMESPACE,113); 
		public static final WebErrorCode MULTIPLE_FILE_ALLOWED_FLAG = new WebErrorCode(NAMESPACE,114);
		public static final WebErrorCode MULTIPLE_FILE_MAX_COUNT = new WebErrorCode(NAMESPACE,115);
		
		public static final WebErrorCode CASEID_INVALID_PATTERN = new WebErrorCode(NAMESPACE,131);
		public static final WebErrorCode MANDATORY_COLUMNS_ABSENT = new WebErrorCode(NAMESPACE,132);
		public static final WebErrorCode RULESET_CANNOT_BE_ACTIVATED = new WebErrorCode(NAMESPACE,133); //Ruleset configuration not complete, it cannot be activated
		public static final WebErrorCode RULESET_CANNOT_BE_DEACTIVATED = new WebErrorCode(NAMESPACE,134); //Ruleset cannot be de-activated
		public static final WebErrorCode ERROR_UPDATING_RULESET_STATE = new WebErrorCode(NAMESPACE,135); //Ruleset state cannot be updated
		public static final WebErrorCode INVALID_TEMPLATE_RULESET_NAME = new WebErrorCode(NAMESPACE,136); 
		public static final WebErrorCode TEMPLATE_RULESET_NAME_NOT_UNIQUE = new WebErrorCode(NAMESPACE,137); 
		public static final WebErrorCode INVALID_TEMPLATE_NAME = new WebErrorCode(NAMESPACE,138); 
		public static final WebErrorCode TEMPLATE_NAME_NOT_UNIQUE = new WebErrorCode(NAMESPACE,139); 
		public static final WebErrorCode INVALID_TEMPLATE_RULE_CRITERIA = new WebErrorCode(NAMESPACE,140); 
		public static final WebErrorCode TEMPLATE_RULE_CRITERIA_NOT_UNIQUE = new WebErrorCode(NAMESPACE,141); 
		public static final WebErrorCode CASE_VALIDATION_FAILED_FOR_VALID_CASE = new WebErrorCode(NAMESPACE, 142);
		public static final WebErrorCode CASE_VALIDATION_FAILED_FOR_ERROR_CASE = new WebErrorCode(NAMESPACE, 143);
		public static final WebErrorCode CASE_VALIDATION_FAILED_FOR_WARNING_CASE = new WebErrorCode(NAMESPACE, 144);
		public static final WebErrorCode CASE_VALIDATION_RULESET_CANNOT_BE_ACTIVATED = new WebErrorCode(NAMESPACE,145); //Ruleset configuration not complete, it cannot be activated
		public static final WebErrorCode CASE_VALIDATION_RULESET_CANNOT_BE_DEACTIVATED = new WebErrorCode(NAMESPACE,146);
		public static final WebErrorCode INVALID_RULE_CRITERIA = new WebErrorCode(NAMESPACE,147); 
		public static final WebErrorCode INVALID_RULE_APPLICABILITY = new WebErrorCode(NAMESPACE,148); 
		public static final WebErrorCode INVALID_RULE_NAME = new WebErrorCode(NAMESPACE,149); 
		public static final WebErrorCode RULE_NAME_NOT_UNIQUE = new WebErrorCode(NAMESPACE,150); 
		public static final WebErrorCode INVALID_VALIDATION_RULESET_NAME = new WebErrorCode(NAMESPACE,151);
		public static final WebErrorCode VALIDATION_RULESET_NAME_NOT_UNIQUE = new WebErrorCode(NAMESPACE,152); 
		public static final WebErrorCode VALIDATION_RULES_NOT_CONFIGURED = new WebErrorCode(NAMESPACE,153);
		public static final WebErrorCode COLUMNS_MISSING_IN_CASE_FILE = new WebErrorCode(NAMESPACE,154);
		public static final WebErrorCode ERROR_ENCODING_FILE = new WebErrorCode(NAMESPACE,155);
		public static final WebErrorCode ERROR_DECODING_FILE = new WebErrorCode(NAMESPACE,156);
		
	}
	
	public static class Audit
	{
		private static final String NAMESPACE = "audit";
		
		public static final WebErrorCode ERROR_FETCHING_USER_INFO_FOR_AUDIT = new WebErrorCode(NAMESPACE, 151);
	
		
	}
	
	public static class QR {
		private static final String NAMESPACE =	"qr";
		public static final SimpleWebErrorCode ERROR_UPLOADING_MASTER_FILE = new SimpleWebErrorCode(NAMESPACE, 201);
		public static final SimpleWebErrorCode SAFETY_DATABASE_NOT_AVAILABLE = new SimpleWebErrorCode(NAMESPACE, 202);
		public static final SimpleWebErrorCode REVIEW_TYPE_NOT_AVAILABLE = new SimpleWebErrorCode(NAMESPACE, 203);
		public static final SimpleWebErrorCode ERROR_PARSING_INIT_RECVD_DATE = new SimpleWebErrorCode(NAMESPACE, 204);	
		public static final SimpleWebErrorCode QUALITY_REVIEW_TYPE_NOT_DEFINED = new SimpleWebErrorCode(NAMESPACE,205);
		public static final SimpleWebErrorCode ERROR_SAFETYCASEFILE_TEMPLATE_NOTACTIVATED =	new SimpleWebErrorCode(NAMESPACE,206);
		public static final SimpleWebErrorCode ERROR_INVALID_CASE_ASSOCIATE_ID	= new SimpleWebErrorCode(NAMESPACE, 207);
		public static final SimpleWebErrorCode ERROR_INVALID_CASE_SERIOUS	= new SimpleWebErrorCode(NAMESPACE, 208);
		public static final SimpleWebErrorCode ERROR_INVALID_SCFT_SEQID	= new SimpleWebErrorCode(NAMESPACE, 209);
		public static final SimpleWebErrorCode ERROR_FIELDS_REVIEWED_COUNT	= new SimpleWebErrorCode(NAMESPACE, 210);
		public static final SimpleWebErrorCode ERROR_FIELDS_COUNT_MISMATCH	= new SimpleWebErrorCode(NAMESPACE, 211);
		public static final SimpleWebErrorCode ERROR_SAVING_QRL_DATA = new SimpleWebErrorCode(NAMESPACE, 213);
		public static final SimpleWebErrorCode ERROR_SEQUENCE_ID = new SimpleWebErrorCode(NAMESPACE, 214);
		public static final SimpleWebErrorCode ERROR_GENERATING_REPORT = new SimpleWebErrorCode(NAMESPACE, 215);
		public static final SimpleWebErrorCode ERROR_PARSING_FIELD = new SimpleWebErrorCode(NAMESPACE, 216);
		public static final SimpleWebErrorCode ERROR_INVALID_FILE	= new SimpleWebErrorCode(NAMESPACE, 217);
		public static final SimpleWebErrorCode ERROR_UPLOADING_FILE	= new SimpleWebErrorCode(NAMESPACE, 218);
		public static final SimpleWebErrorCode ERROR_FETCH_CASE_RECORDS	= new SimpleWebErrorCode(NAMESPACE, 219);
		public static final SimpleWebErrorCode ERROR_EMPTY_CASE_RECORDS	= new SimpleWebErrorCode(NAMESPACE, 220);
		public static final SimpleWebErrorCode ERROR_MISSING_ERROR_TYPE	= new SimpleWebErrorCode(NAMESPACE, 221);
		public static final SimpleWebErrorCode REVIEW_TYPE_LIST_NOT_AVAILABLE = new SimpleWebErrorCode(NAMESPACE, 222);
		public static final SimpleWebErrorCode ERROR_PROJECT_NOT_CONFIGURED		=	new SimpleWebErrorCode(NAMESPACE, 223);
		public static final SimpleWebErrorCode ERROR_PREVIOUS_REVIEW_TYPE_NOT_COMPLETED	=	new SimpleWebErrorCode(NAMESPACE, 224);
		public static final SimpleWebErrorCode ERROR_CREATING_ERROR_DASHBOARD	= new SimpleWebErrorCode(NAMESPACE, 225);
		public static final SimpleWebErrorCode ERROR_CASE_MASTER_NOTEXIST	= new SimpleWebErrorCode(NAMESPACE, 226);
		
		
		public static final WebErrorCode ERROR_INVLAID_CASEID	= new WebErrorCode(NAMESPACE, 251);
		public static final WebErrorCode ERROR_INVLAID_CASE_VERSION	= new WebErrorCode(NAMESPACE, 252);
		public static final WebErrorCode ERROR_BLANK_CASEID	= new WebErrorCode(NAMESPACE, 253);
		public static final WebErrorCode ERROR_FILE_NOT_SELECTED	= new WebErrorCode(NAMESPACE, 254);
		public static final WebErrorCode ERROR_CASEID_EMPTY	=	 new WebErrorCode(NAMESPACE, 255);
		public static final WebErrorCode ERROR_CASE_VERSION_EMPTY	= new WebErrorCode(NAMESPACE, 256);
		public static final WebErrorCode ERROR_SERIOUSNESS_EMPTY	= new WebErrorCode(NAMESPACE, 257);
		public static final WebErrorCode ERROR_IRD_EMPTY	= new WebErrorCode(NAMESPACE, 258);
		public static final WebErrorCode ERROR_INVALID_IRD	= new WebErrorCode(NAMESPACE, 259);
		public static final WebErrorCode ERROR_UDF_FIELD_MAPPING	= new WebErrorCode(NAMESPACE, 260);
		
	}
		
	public static class ProjectSetting {
		private static final String NAMESPACE =	"prjset";
		public static final SimpleWebErrorCode ERROR_TEMPLATE_NAME_EMPTY = new SimpleWebErrorCode(NAMESPACE, 301);
		public static final SimpleWebErrorCode ERROR_TEMPLATE_NOT_FOUND	= new SimpleWebErrorCode(NAMESPACE, 302);	
		public static final SimpleWebErrorCode ERROR_UPLOADING_FILE	= new SimpleWebErrorCode(NAMESPACE, 303);
		public static final WebErrorCode ERROR_TEMPLATE_NAME_INVALID = new WebErrorCode(NAMESPACE, 351);
		public static final WebErrorCode ERROR_TEMPLATE_NAME_NOT_UNIQUE = new WebErrorCode(NAMESPACE,352); 
	}
	
	public static class MailSettings {
		private static final String NAMESPACE =	"mailset";
		public static final SimpleWebErrorCode ERROR_MAIL_TEMPLATE_NAME_EMPTY = new SimpleWebErrorCode(NAMESPACE, 401);
		public static final SimpleWebErrorCode ERROR_MAIL_TEMPLATE_BODY_EMPTY	= new SimpleWebErrorCode(NAMESPACE, 404);	
		public static final SimpleWebErrorCode ERROR_MAIL_TEMPLATE_SUBJECT_EMPTY	= new SimpleWebErrorCode(NAMESPACE, 403);
		public static final SimpleWebErrorCode ERROR_MAIL_TEMPLATE_PURPOSE_EMPTY	= new SimpleWebErrorCode(NAMESPACE, 402);
		public static final SimpleWebErrorCode ERROR_MAIL_TEMPLATE_NAME_INVALID = new SimpleWebErrorCode(NAMESPACE, 405);
		public static final SimpleWebErrorCode ERROR_MAIL_TEMPLATE_NAME_NOT_UNIQUE = new SimpleWebErrorCode(NAMESPACE,406);
		public static final SimpleWebErrorCode ERROR_UPDATED_VALUES_SAME_AS_EXISTING = new SimpleWebErrorCode(NAMESPACE,407);
	}
	
	public static class ProjectUserManagement {
		private static final String NAMESPACE =	"projectusermanagement";
		public static final SimpleWebErrorCode ERROR_USER_NOT_SELECTED = new SimpleWebErrorCode(NAMESPACE, 501);
		public static final SimpleWebErrorCode ERROR_USER_CLIENT_NAME_ERROR = new SimpleWebErrorCode(NAMESPACE, 502);
		public static final SimpleWebErrorCode ERROR_USER_CLIENT_EMAIID_ERROR = new SimpleWebErrorCode(NAMESPACE, 503);
		public static final SimpleWebErrorCode ERROR_USER_CLIENT_SAFETYDBID_ERROR = new SimpleWebErrorCode(NAMESPACE, 504);
		
	}
	
	public static class ProjectManagement {
		private static final String NAMESPACE =	"projectmanagement";
		public static final SimpleWebErrorCode ERROR_PROJECT_NAME_EMPTY = new SimpleWebErrorCode(NAMESPACE, 601);
		public static final SimpleWebErrorCode ERROR_PROJECT_NAME_INVALID = new SimpleWebErrorCode(NAMESPACE, 602);
		public static final SimpleWebErrorCode ERROR_PROJECT_DESCRIPTION_EMPTY = new SimpleWebErrorCode(NAMESPACE, 603);
		public static final SimpleWebErrorCode ERROR_PROJECT_NAME_ALREADY_EXIST = new SimpleWebErrorCode(NAMESPACE, 604);
		public static final SimpleWebErrorCode ERROR_UPDATE_VALUES_SAME_AS_EXISTING = new SimpleWebErrorCode(NAMESPACE, 605);
	
	}
	public static class UserManagement {
		private static final String NAMESPACE =	"usermanagement";
		public static final SimpleWebErrorCode ERROR_USER_ID_EMPTY = new SimpleWebErrorCode(NAMESPACE, 901);
		public static final SimpleWebErrorCode ERROR_USER_NAME_EMPTY = new SimpleWebErrorCode(NAMESPACE, 902);
		public static final SimpleWebErrorCode ERROR_USER_EMAIL_EMPTY = new SimpleWebErrorCode(NAMESPACE, 903);
		public static final SimpleWebErrorCode ERROR_USER_AUTH_TYPE = new SimpleWebErrorCode(NAMESPACE, 904);
		public static final SimpleWebErrorCode ERROR_USER_ID_EXIST = new SimpleWebErrorCode(NAMESPACE, 905);	
	
	
	}

	/*Added by Jason for Web Service Messages : 18122017 : START*/
	public static class WebServices{
		private static final String NAMESPACE =	"webservices";
		
		// Success Messages
		public static final SimpleWebErrorCode SUCCESS = new SimpleWebErrorCode(NAMESPACE, 100000);
		
		// Failure Messages
		public static final SimpleWebErrorCode INVALID_USERID_PWD = new SimpleWebErrorCode(NAMESPACE, 110000);
		public static final SimpleWebErrorCode USERID_INVALID_CHARS = new SimpleWebErrorCode(NAMESPACE, 110001);
		public static final SimpleWebErrorCode USERID_NOT_REGISTERED = new SimpleWebErrorCode(NAMESPACE, 110002);
		public static final SimpleWebErrorCode PWD_NOT_MATCH = new SimpleWebErrorCode(NAMESPACE, 110003);
		public static final SimpleWebErrorCode SOMETHING_WENT_WRONG = new SimpleWebErrorCode(NAMESPACE, 110004);
		public static final SimpleWebErrorCode INVALID_ACTIVITY_CODE = new SimpleWebErrorCode(NAMESPACE, 110005);
		public static final SimpleWebErrorCode ACT_REGISTER_FAILED = new SimpleWebErrorCode(NAMESPACE, 110006);
		public static final SimpleWebErrorCode ACT_REGISTER_FAILED_ADMIN = new SimpleWebErrorCode(NAMESPACE, 110007);
		public static final SimpleWebErrorCode INVALID_TEMPLATEID_BLANK = new SimpleWebErrorCode(NAMESPACE, 110008);
		public static final SimpleWebErrorCode TEMPLATEID_NOT_EXIST = new SimpleWebErrorCode(NAMESPACE, 110009);
		public static final SimpleWebErrorCode TEMPLATE_EXPORT_FAILED_ADMIN = new SimpleWebErrorCode(NAMESPACE, 110010);
		public static final SimpleWebErrorCode INVALID_TEMPLATEID = new SimpleWebErrorCode(NAMESPACE, 110011);
		public static final SimpleWebErrorCode TEMPLATE_EXPORT_FAILED = new SimpleWebErrorCode(NAMESPACE, 110012);
		public static final SimpleWebErrorCode NO_SUCH_ACTIVITY = new SimpleWebErrorCode(NAMESPACE, 110013);
		public static final SimpleWebErrorCode NON_NUMERIC_ACTIVITY_ID = new SimpleWebErrorCode(NAMESPACE, 110014);
		public static final SimpleWebErrorCode NON_NUMERIC_PROJECT_ID = new SimpleWebErrorCode(NAMESPACE, 110015);
		public static final SimpleWebErrorCode INVALID_ACTIVITY_TYPE = new SimpleWebErrorCode(NAMESPACE, 110016);
		public static final SimpleWebErrorCode AUTH_NOT_DONE = new SimpleWebErrorCode(NAMESPACE, 110017);
		public static final SimpleWebErrorCode NON_NUMERIC_TEMPLATE_ID = new SimpleWebErrorCode(NAMESPACE, 110018);
		public static final SimpleWebErrorCode INVALID_KEYID = new SimpleWebErrorCode(NAMESPACE, 110019);
		public static final SimpleWebErrorCode AUTH_FAILED_ADMIN = new SimpleWebErrorCode(NAMESPACE, 110020);
		public static final SimpleWebErrorCode INVALID_ACTIVITY_APP = new SimpleWebErrorCode(NAMESPACE, 110021);
		public static final SimpleWebErrorCode INVALID_DATETIME = new SimpleWebErrorCode(NAMESPACE, 110022);
		public static final SimpleWebErrorCode WEB_APIS_DISABLED = new SimpleWebErrorCode(NAMESPACE, 110023);
		public static final SimpleWebErrorCode INVALID_JSON_STRUCTURE = new SimpleWebErrorCode(NAMESPACE, 110024);
		public static final SimpleWebErrorCode INCORRECT_KEYID = new SimpleWebErrorCode(NAMESPACE, 110025);
		public static final SimpleWebErrorCode TOOL_NOT_UPDATED = new SimpleWebErrorCode(NAMESPACE, 110026);
		public static final SimpleWebErrorCode INVALID_TIMEZONE = new SimpleWebErrorCode(NAMESPACE, 110027);
		public static final SimpleWebErrorCode INVALID_TIMESTAMP = new SimpleWebErrorCode(NAMESPACE, 110028);
		public static final SimpleWebErrorCode INVALID_TOOLNAME = new SimpleWebErrorCode(NAMESPACE, 110029);
		public static final SimpleWebErrorCode INVALID_TOOLVERSION = new SimpleWebErrorCode(NAMESPACE, 110030);
		public static final SimpleWebErrorCode INVALID_CLIENTREQID = new SimpleWebErrorCode(NAMESPACE, 110031);
		public static final SimpleWebErrorCode INVALID_MACHINENAME = new SimpleWebErrorCode(NAMESPACE, 110032);
		public static final SimpleWebErrorCode INVALID_MACHINEDOMAIN = new SimpleWebErrorCode(NAMESPACE, 110033);
		public static final SimpleWebErrorCode INVALID_IPADDRESS = new SimpleWebErrorCode(NAMESPACE, 110034);
		public static final SimpleWebErrorCode INVALID_OSNAME = new SimpleWebErrorCode(NAMESPACE, 110035);
		public static final SimpleWebErrorCode INVALID_OSVERSION = new SimpleWebErrorCode(NAMESPACE, 110036);
		public static final SimpleWebErrorCode INVALID_OSARCHITECTURE = new SimpleWebErrorCode(NAMESPACE, 110037);
		public static final SimpleWebErrorCode INVALID_JDK = new SimpleWebErrorCode(NAMESPACE, 110038);
		public static final SimpleWebErrorCode INVALID_MACHINELOGGEDINUSER = new SimpleWebErrorCode(NAMESPACE, 110039);
		public static final SimpleWebErrorCode INVALID_MACHINELOGGEDINUSERDOMAIN = new SimpleWebErrorCode(NAMESPACE, 110040);
		public static final SimpleWebErrorCode UNALLOWED_TOOLNAME = new SimpleWebErrorCode(NAMESPACE, 110041);
		public static final SimpleWebErrorCode UNALLOWED_TOOLVERSION = new SimpleWebErrorCode(NAMESPACE, 110042);
		public static final SimpleWebErrorCode RUNTIME_IQ_FAILED_OS = new SimpleWebErrorCode(NAMESPACE, 110043);
		public static final SimpleWebErrorCode RUNTIME_IQ_FAILED_JAVA = new SimpleWebErrorCode(NAMESPACE, 110044);
		public static final SimpleWebErrorCode INVALID_CLIENT_IQHASH = new SimpleWebErrorCode(NAMESPACE, 110045);
		public static final SimpleWebErrorCode UNALLOWED_CLIENT_IQHASH = new SimpleWebErrorCode(NAMESPACE, 110046);
		public static final SimpleWebErrorCode INVALID_DATE_FORMAT = new SimpleWebErrorCode(NAMESPACE, 110047);
		public static final SimpleWebErrorCode PROJECT_NOT_EXISTS = new SimpleWebErrorCode(NAMESPACE, 110048);
		public static final SimpleWebErrorCode BROWSER_CHROME_DISALLOWED = new SimpleWebErrorCode(NAMESPACE, 110049);
		public static final SimpleWebErrorCode BROWSER_IE_DISALLOWED = new SimpleWebErrorCode(NAMESPACE, 110050);
		public static final SimpleWebErrorCode BROWSER_CHROME_NO_SUPPORT = new SimpleWebErrorCode(NAMESPACE, 110051);
		public static final SimpleWebErrorCode BROWSER_IE_NO_SUPPORT = new SimpleWebErrorCode(NAMESPACE, 110052);
		public static final SimpleWebErrorCode GENERIC_INVALID_INPUT = new SimpleWebErrorCode(NAMESPACE, 110053);
		
	}
	/*Added by Jason for Web Service Messages : 18122017 : END*/
	
	public static class SystemConfig {
		private static final String NAMESPACE = "systemconfig";
		public static final SimpleWebErrorCode ERROR_KEY_ID_EMPTY = new SimpleWebErrorCode(NAMESPACE, 701);
		public static final SimpleWebErrorCode ERROR_KEY_PURPOSE_EMPTY = new SimpleWebErrorCode(NAMESPACE, 702);
		public static final SimpleWebErrorCode ERROR_KEY_TYPE_EMPTY = new SimpleWebErrorCode(NAMESPACE, 703);
		public static final SimpleWebErrorCode ERROR_INVALID_KEY_ID = new SimpleWebErrorCode(NAMESPACE, 704);
		public static final SimpleWebErrorCode ERROR_KEY_ID_EXIST = new SimpleWebErrorCode(NAMESPACE, 705);
		public static final SimpleWebErrorCode ERROR_EXPORT_KEY = new SimpleWebErrorCode(NAMESPACE, 706);
	}

	public static class Activity {
		private static final String NAMESPACE = "activity";
		public static final WebErrorCode ERROR_FETCHING_USER_INFO_FOR_ACTIVITY = new WebErrorCode(NAMESPACE, 1001);
		public static final WebErrorCode ERROR_FETCHING_PROJECT_INFO_FOR_ACTIVITY = new WebErrorCode(NAMESPACE, 1002);
	}
	public static class MailBox {
		private static final String NAMESPACE =	"mailbox";
		public static final SimpleWebErrorCode ERROR_MAIL_BOX_NAME_EMPTY = new SimpleWebErrorCode(NAMESPACE, 1101);
		public static final SimpleWebErrorCode ERROR_MAIL_BOX_NAME_INVALID	= new SimpleWebErrorCode(NAMESPACE, 1102);	
		public static final SimpleWebErrorCode ERROR_MAIL_BOX_EMAILID_EMPTY	= new SimpleWebErrorCode(NAMESPACE, 1103);
		public static final SimpleWebErrorCode ERROR_MAIL_BOX_DESCRIPTION_EMPTY	= new SimpleWebErrorCode(NAMESPACE, 1104);
		public static final SimpleWebErrorCode ERROR_MAIL_BOX_MNEMONICS_EMPTY = new SimpleWebErrorCode(NAMESPACE, 1105);
		public static final SimpleWebErrorCode ERROR_MAIL_BOX_NAME_NOT_UNIQUE = new SimpleWebErrorCode(NAMESPACE,1106);
		public static final SimpleWebErrorCode ERROR_UPDATED_VALUES_SAME_AS_EXISTING = new SimpleWebErrorCode(NAMESPACE,1107);
		public static final SimpleWebErrorCode ERROR_MAIL_BOX_MNEMONICS_INVALID	= new SimpleWebErrorCode(NAMESPACE, 1108);
		public static final SimpleWebErrorCode ERROR_UPLOAD_MSG_FILE    = new SimpleWebErrorCode(NAMESPACE, 1109);
		public static final SimpleWebErrorCode ERROR_GENERATING_REPORT = new SimpleWebErrorCode(NAMESPACE, 1110);
	}
}
