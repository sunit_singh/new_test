package com.sciformix.commons;

public class ServiceErrorCodes
{
	public static class SimpleServiceErrorCode extends ErrorCode
	{
		private SimpleServiceErrorCode(String p_sNamespace, int p_errorcode)
		{
			super(p_sNamespace, p_errorcode);
		}
	}
	public static class SourceDocUtil
	{
		private static final String NAMESPACE = "sourcedocutil";
		public static final SimpleServiceErrorCode ERROR_DOWNLOADING_FILE = new SimpleServiceErrorCode(NAMESPACE, 11601);	
		public static final SimpleServiceErrorCode ERROR_PARSING_FILE = new SimpleServiceErrorCode(NAMESPACE, 11602);	
		public static final SimpleServiceErrorCode ERROR_FILE_TYPE_AND_FILE_DOES_NOT_MATCH = new SimpleServiceErrorCode(NAMESPACE, 11603);
		
		public static final SimpleServiceErrorCode ERROR_VALIDATION_NO_OF_PAGES = new SimpleServiceErrorCode(NAMESPACE, 11604);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_FIRST_PAGE_FOOTER_MISSING = new SimpleServiceErrorCode(NAMESPACE, 11605);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_SUBSEQUENT_PAGE_FOOTER_MISSING = new SimpleServiceErrorCode(NAMESPACE, 11606);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_PAGE_NO_IN_FOOTER = new SimpleServiceErrorCode(NAMESPACE, 11607);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_FIRST_PAGE_HEADER = new SimpleServiceErrorCode(NAMESPACE, 11608);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_FORMAT_VERSION_VALUE = new SimpleServiceErrorCode(NAMESPACE, 11609);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_FORMAT_RELEASE_VALUE = new SimpleServiceErrorCode(NAMESPACE, 11610);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_HEADER_MANDATORY_FIELD_VALUE = new SimpleServiceErrorCode(NAMESPACE, 11611);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_HEADER_MANDATORY_FIELD_KEY = new SimpleServiceErrorCode(NAMESPACE, 11612);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_SUBSEQUENT_PAGE_HEADER__KEYS_MISSING = new SimpleServiceErrorCode(NAMESPACE, 11613);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_FILETYPE_CONTENT = new SimpleServiceErrorCode(NAMESPACE, 11614);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_PDF_PRODUCER = new SimpleServiceErrorCode(NAMESPACE, 11615);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_SAFETY_REPOR_ID_MISSING = new SimpleServiceErrorCode(NAMESPACE, 11616);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_PATIENT_INITIAL_MISSING = new SimpleServiceErrorCode(NAMESPACE, 11617);
		public static final SimpleServiceErrorCode ERROR_VALIDATION_MESSAGE_DATE = new SimpleServiceErrorCode(NAMESPACE, 11618);
	}
	public static class System
	{
		private static final String NAMESPACE = "system";
		
		public static final SimpleServiceErrorCode ERROR_SAVING_LOGIN_AUDIT = new SimpleServiceErrorCode(NAMESPACE, 10001);		
		public static final SimpleServiceErrorCode ERROR_SAVING_LOGIN_ATTEMPT_AUDIT = new SimpleServiceErrorCode(NAMESPACE, 10002);		
		public static final SimpleServiceErrorCode ERROR_SAVING_LOGOUT_AUDIT = new SimpleServiceErrorCode(NAMESPACE, 10003);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_LIST_OF_USERS = new SimpleServiceErrorCode(NAMESPACE, 10004);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_USER_INFO = new SimpleServiceErrorCode(NAMESPACE, 10005);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_USER_PREFS = new SimpleServiceErrorCode(NAMESPACE, 10006);		
		public static final SimpleServiceErrorCode ERROR_SAVING_USER_PREFS = new SimpleServiceErrorCode(NAMESPACE, 10007);		
		public static final SimpleServiceErrorCode ERROR_UPDATING_CONFIG = new SimpleServiceErrorCode(NAMESPACE, 10008);		
		public static final SimpleServiceErrorCode ERROR_UPDATING_PREFERENCES = new SimpleServiceErrorCode(NAMESPACE, 10009);		
		public static final SimpleServiceErrorCode ERROR_REFRESHING_PREFERENCES = new SimpleServiceErrorCode(NAMESPACE, 10010);		
		public static final SimpleServiceErrorCode ERROR_CREATING_WEB_USERSESSION = new SimpleServiceErrorCode(NAMESPACE, 10011);		
		public static final SimpleServiceErrorCode ERROR_REMOVE_USER_PREF = new SimpleServiceErrorCode(NAMESPACE, 10012);		
		public static final SimpleServiceErrorCode ERROR_CREATING_USER = new SimpleServiceErrorCode(NAMESPACE, 10013);
		public static final SimpleServiceErrorCode ERROR_UPDATING_USER = new SimpleServiceErrorCode(NAMESPACE, 10014);
		public static final SimpleServiceErrorCode ERROR_REFRESHING_SYS_CONFIG = new SimpleServiceErrorCode(NAMESPACE, 10015);		
		
		
	}

	public static class Ngv
	{
		private static final String NAMESPACE = "ngv";
		
		public static final SimpleServiceErrorCode NARRATIVE_TEMPLATE_NOT_CONFIGURED = new SimpleServiceErrorCode(NAMESPACE, 11101);		
		public static final SimpleServiceErrorCode NARRATIVE_TEMPLATE_RULESET_NOT_AVAILABLE = new SimpleServiceErrorCode(NAMESPACE, 11102);		
		public static final SimpleServiceErrorCode NARRATIVE_TEMPLATE_NOT_AVAILABLE = new SimpleServiceErrorCode(NAMESPACE, 11103);		
		public static final SimpleServiceErrorCode NARRATIVE_GENERATION_FAILED = new SimpleServiceErrorCode(NAMESPACE, 11104);		
		public static final SimpleServiceErrorCode TEMPLATE_RULESET_FETCH_FAILED = new SimpleServiceErrorCode(NAMESPACE, 11105);		
		public static final SimpleServiceErrorCode ERROR_UPDATING_TEMPLATERULESET_STATE = new SimpleServiceErrorCode(NAMESPACE, 11106);		
		public static final SimpleServiceErrorCode ERROR_CHECKING_TEMPLATE_RULESET_NAME_UNIQUENESS = new SimpleServiceErrorCode(NAMESPACE, 11107);		
		public static final SimpleServiceErrorCode ERROR_SAVING_TEMPLATE_RULESET = new SimpleServiceErrorCode(NAMESPACE, 11108);
		public static final SimpleServiceErrorCode ERROR_SAVING_TEMPLATE = new SimpleServiceErrorCode(NAMESPACE, 11109);
		public static final SimpleServiceErrorCode ERROR_FETCHING_TEMPLATE_RULESET = new SimpleServiceErrorCode(NAMESPACE, 11110);
		public static final SimpleServiceErrorCode ERROR_SAVING_TEMPLATE_HEADER = new SimpleServiceErrorCode(NAMESPACE, 11111);
		public static final SimpleServiceErrorCode ERROR_SAVING_DEFAULT_TEMPLATE = new SimpleServiceErrorCode(NAMESPACE, 11112);
		public static final SimpleServiceErrorCode ERROR_IMPORTING_TEMPLATE = new SimpleServiceErrorCode(NAMESPACE, 11113);
		public static final SimpleServiceErrorCode FILE_SIZE_EXCEEDS_SYSTEM_SUPPORTED = new SimpleServiceErrorCode(NAMESPACE, 11114);
		public static final SimpleServiceErrorCode ILLEGAL_FILE_TYPE_DETECTED = new SimpleServiceErrorCode(NAMESPACE, 11115);
		public static final SimpleServiceErrorCode ERROR_PARSING_WORKBOOK_FILE = new SimpleServiceErrorCode(NAMESPACE, 11116);
		public static final SimpleServiceErrorCode UPLOADED_WORKBOOK_FILE_DOES_NOT_EXIST = new SimpleServiceErrorCode(NAMESPACE, 11117);
		public static final SimpleServiceErrorCode WORKBOOK_VALIDATION_FAILED_MIN_ROW_COUNT= new SimpleServiceErrorCode(NAMESPACE, 11118);
		public static final SimpleServiceErrorCode WORKBOOK_VALIDATION_FAILED_MAX_ROW_COUNT = new SimpleServiceErrorCode(NAMESPACE, 11119);
		public static final SimpleServiceErrorCode WORKBOOK_VALIDATION_FAILED_CASEID_EMPTY = new SimpleServiceErrorCode(NAMESPACE, 11120);
		public static final SimpleServiceErrorCode WORKBOOK_VALIDATION_FAILED_CASEID_MISMATCH = new SimpleServiceErrorCode(NAMESPACE, 11121);
		public static final SimpleServiceErrorCode SAFETY_DATABASE_NOT_AVAILABLE = new SimpleServiceErrorCode(NAMESPACE, 11122);
		public static final SimpleServiceErrorCode REPLCAING_NARRATIVE_TEXT_SPECIAL_CHARS = new SimpleServiceErrorCode(NAMESPACE, 11123);
		public static final SimpleServiceErrorCode CHECKING_REGEX_FOR_UPDATE_MARKES = new SimpleServiceErrorCode(NAMESPACE, 11124);
		public static final SimpleServiceErrorCode CASE_VALIDATION_FAILED_DUE_TO_COLUMN_MISSING = new SimpleServiceErrorCode(NAMESPACE, 11125);
		public static final SimpleServiceErrorCode ERROR_FETCHING_LIST_OF_RULES_FROM_DB = new SimpleServiceErrorCode(NAMESPACE, 11126);
		public static final SimpleServiceErrorCode VALIDATION_RULESET_FETCH_FAILED = new SimpleServiceErrorCode(NAMESPACE, 11127);
		
		public static final SimpleServiceErrorCode PDFXML_VALIDATION_FAILED_CASEID_MISMATCH = new SimpleServiceErrorCode(NAMESPACE, 11128);
		public static final SimpleServiceErrorCode ERROR_PARSING_PDFXML_FILE = new SimpleServiceErrorCode(NAMESPACE, 11129);
	}
	
	public static class Audit
	{
		private static final String NAMESPACE = "audit";
		
		public static final SimpleServiceErrorCode ERROR_FETCHING_USER_AUDIT_TRAIL = new SimpleServiceErrorCode(NAMESPACE, 11151);		
		public static final SimpleServiceErrorCode NARRATIVE_TEMPLATE_RULESET_NOT_AVAILABLE = new SimpleServiceErrorCode(NAMESPACE, 11152);		
		public static final SimpleServiceErrorCode NARRATIVE_TEMPLATE_NOT_AVAILABLE = new SimpleServiceErrorCode(NAMESPACE, 11153);		
	}
	
	public static class QR
	{
		private static final String NAMESPACE	=	"qr";
		public static final SimpleServiceErrorCode SAFETY_DATABASE_NOT_AVAILABLE	=	new SimpleServiceErrorCode(NAMESPACE, 11201);
		public static final SimpleServiceErrorCode REVIEW_TYPE_NOT_AVAILABLE		=	new SimpleServiceErrorCode(NAMESPACE, 11202);
		public static final SimpleServiceErrorCode ERROR_SAVING_REVIEW_DATA			=	new SimpleServiceErrorCode(NAMESPACE, 11203);
		public static final SimpleServiceErrorCode ERROR_FETCH_ALL_QR_DATA			=	new SimpleServiceErrorCode(NAMESPACE, 11204);	
		public static final SimpleServiceErrorCode ERROR_FETCH_CASE_QR_DATA			=	new SimpleServiceErrorCode(NAMESPACE, 11205);
		public static final SimpleServiceErrorCode ERROR_SAVING_MASTER_DATA			=	new SimpleServiceErrorCode(NAMESPACE, 11206);
		public static final SimpleServiceErrorCode ERROR_LOADING_MASTER_DATA		=	new SimpleServiceErrorCode(NAMESPACE, 11207);	
		public static final SimpleServiceErrorCode LAYOUT_TYPE_NOT_AVAILABLE		=	new SimpleServiceErrorCode(NAMESPACE, 11208);
		public static final SimpleServiceErrorCode INVALID_QUALITY_REVIEW_TYPE		=	new SimpleServiceErrorCode(NAMESPACE, 11209);
		public static final SimpleServiceErrorCode ERROR_SENDING_EMAIL				=	new SimpleServiceErrorCode(NAMESPACE, 11210);
		public static final SimpleServiceErrorCode ERROR_UPLOAD_CASE_MASTER_FIle	=	new SimpleServiceErrorCode(NAMESPACE, 11211);
		public static final SimpleServiceErrorCode ERROR_FETCH_CASEMASTER_RECORDS	=	new SimpleServiceErrorCode(NAMESPACE, 11212);
		public static final SimpleServiceErrorCode ERROR_PARSING_CASEMASTER_RECORDS	=	new SimpleServiceErrorCode(NAMESPACE, 11213);
		public static final SimpleServiceErrorCode ERROR_CASEMASTER_FILE_EMPLY		=	new SimpleServiceErrorCode(NAMESPACE, 11214);
		public static final SimpleServiceErrorCode ERROR_INCORRECT_DATE_FORMAT		=	new SimpleServiceErrorCode(NAMESPACE, 11215);
		public static final SimpleServiceErrorCode ERROR_SAVING_CASEMASTER_DATA		=	new SimpleServiceErrorCode(NAMESPACE, 11216);
		public static final SimpleServiceErrorCode ERROR_UPDATING_CASEMASTER_DATA	=	new SimpleServiceErrorCode(NAMESPACE, 11217);
		public static final SimpleServiceErrorCode ERROR_FETCHING_CASE_HISTORY		=	new SimpleServiceErrorCode(NAMESPACE, 11218);
		public static final SimpleServiceErrorCode ERROR_PARSING_PRJ_START_DATE		=	new SimpleServiceErrorCode(NAMESPACE, 11219);
		public static final SimpleServiceErrorCode ERROR_REVIEW_FIELD_NOT_CHECKED	=	new SimpleServiceErrorCode(NAMESPACE, 11220);
		public static final SimpleServiceErrorCode ERROR_PARSING_DWNLD_SHEET_NUM	=	new SimpleServiceErrorCode(NAMESPACE, 11221);
		public static final SimpleServiceErrorCode REVIEW_TYPE_LIST_NOT_AVAILABLE 	=	new SimpleServiceErrorCode(NAMESPACE, 11222);
		public static final SimpleServiceErrorCode ERROR_CAT_THRESHOLD_NOT_DEFINED 	=	new SimpleServiceErrorCode(NAMESPACE, 11223);
		public static final SimpleServiceErrorCode ERROR_CAT_FIELD_NOT_SELECTED 	=	new SimpleServiceErrorCode(NAMESPACE, 11224);
		public static final SimpleServiceErrorCode ERROR_CAT_THRESHOLD_WRONG_VALUE 	=	new SimpleServiceErrorCode(NAMESPACE, 11225);
		public static final SimpleServiceErrorCode ERROR_CREATING_CP_DASHBOARD 		=	new SimpleServiceErrorCode(NAMESPACE, 11226);
		public static final SimpleServiceErrorCode ERROR_EXPORTING_CP_DASHBOARD 	=	new SimpleServiceErrorCode(NAMESPACE, 11227);
	}
	
	public static class Project
	{
		private static final String NAMESPACE = "project";
		
		public static final SimpleServiceErrorCode ERROR_FETCHING_PROJECT_INFO = new SimpleServiceErrorCode(NAMESPACE, 11301);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_PROJECT_INFO_LIST = new SimpleServiceErrorCode(NAMESPACE, 11302);		
		public static final SimpleServiceErrorCode ERROR_SAVING_PROJECT = new SimpleServiceErrorCode(NAMESPACE, 11303);		
	}
	
	public static class Email
	{
		private static final String NAMESPACE = "email";
		
		public static final SimpleServiceErrorCode ERROR_FETCHING_PROJECT_INFO = new SimpleServiceErrorCode(NAMESPACE, 11401);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_EMAIL_TEMPLATE_LIST = new SimpleServiceErrorCode(NAMESPACE, 11402);		
		public static final SimpleServiceErrorCode ERROR_SAVING_EMAIL = new SimpleServiceErrorCode(NAMESPACE, 11403);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_ACTIVE_MAIL_TEMPLATE = new SimpleServiceErrorCode(NAMESPACE, 11404);		
		
	}
	
	public static class ProjectUser
	{
		private static final String NAMESPACE = "projectuser";
		
		public static final SimpleServiceErrorCode ERROR_SAVING_PROJECT_USER_INFO = new SimpleServiceErrorCode(NAMESPACE, 11501);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_USER_LIST = new SimpleServiceErrorCode(NAMESPACE, 11502);		
		public static final SimpleServiceErrorCode ERROR_FETCHING_PROJECT_USER = new SimpleServiceErrorCode(NAMESPACE, 11503);
		public static final SimpleServiceErrorCode ERROR_REMOVING_PROJECT_USER = new SimpleServiceErrorCode(NAMESPACE, 11504);
		public static final SimpleServiceErrorCode ERROR_SAVING_PROJECT = new SimpleServiceErrorCode(NAMESPACE, 11505);
	}

	public static class ProjectSetting
	{
		private static final String NAMESPACE =	"prjset";
		public static final SimpleServiceErrorCode ERROR_CHECKING_TEMPLATE_NAME_UNIQUE	=	new SimpleServiceErrorCode(NAMESPACE, 11601);
		public static final SimpleServiceErrorCode ERROR_FETCHING_TEMPLATES			=	new SimpleServiceErrorCode(NAMESPACE, 11602);
		public static final SimpleServiceErrorCode ERROR_FETCHING_TEMPLATE_DETAILS	=	new SimpleServiceErrorCode(NAMESPACE, 11603);
		public static final SimpleServiceErrorCode ERROR_FETCHING_TEMPLATE_CHILD_DETAILS	=	new SimpleServiceErrorCode(NAMESPACE, 11604);
		public static final SimpleServiceErrorCode ERROR_UPDATING_TEMPLATE_STATE	=	new SimpleServiceErrorCode(NAMESPACE, 11605);
		public static final SimpleServiceErrorCode ERROR_CREATING_TEMPLATE			=	new SimpleServiceErrorCode(NAMESPACE, 11606);
		public static final SimpleServiceErrorCode ERROR_UPDATING_PROJECTCONFIG			=	new SimpleServiceErrorCode(NAMESPACE, 11607);
	}
	
	public static class Common
	{
		private static final String NAMESPACE =	"common";
		public static final SimpleServiceErrorCode ERROR_MENMONIC	=	new SimpleServiceErrorCode(NAMESPACE, 11701);
		public static final SimpleServiceErrorCode ERROR_FILE_EMPTY	=	new SimpleServiceErrorCode(NAMESPACE, 11702);
		public static final SimpleServiceErrorCode ERROR_ACTIVE_TEMPLATE_NOT_FOUND	=	new SimpleServiceErrorCode(NAMESPACE, 11703);
		public static final SimpleServiceErrorCode ERROR_PARSING_FILE	=	new SimpleServiceErrorCode(NAMESPACE, 11704);
		public static final SimpleServiceErrorCode ERROR_PARSING_COLUMN	=	new SimpleServiceErrorCode(NAMESPACE, 11705);
		public static final SimpleServiceErrorCode MAPPING_COLUMN_DATA_NULL	=	new SimpleServiceErrorCode(NAMESPACE, 11706);
		public static final SimpleServiceErrorCode ERROR_SQL_QUERY_NOT_FOUND =	new SimpleServiceErrorCode(NAMESPACE, 11707);
		public static final SimpleServiceErrorCode FILE_FORMAT_NOT_MAINTAINED =	new SimpleServiceErrorCode(NAMESPACE, 11708);
	}
	public static class Job
	{
		private static final String NAMESPACE =	"job";
		public static final SimpleServiceErrorCode ERROR_RUN_JOB	=	new SimpleServiceErrorCode(NAMESPACE, 11801);
		
	}
	public static class ServerConfig
	{
		private static final String NAMESPACE =	"serverconfig";
		public static final SimpleServiceErrorCode ERROR_KEY_PAIR_GENERATION	=	new SimpleServiceErrorCode(NAMESPACE, 11901);
		public static final SimpleServiceErrorCode ERROR_KEY_DECRYPTION	=	new SimpleServiceErrorCode(NAMESPACE, 11902);
		public static final SimpleServiceErrorCode ERROR_KEY_ENCRYPTION	=	new SimpleServiceErrorCode(NAMESPACE, 11903);
	}
	public static class Activity
	{
		private static final String NAMESPACE = "activity";
		
		public static final SimpleServiceErrorCode ERROR_FETCHING_USER_ACTIVITY_TRAIL = new SimpleServiceErrorCode(NAMESPACE, 111001);		
		
	}
	public static class MB
	{
		private static final String NAMESPACE = "mailbox";
		
		public static final SimpleServiceErrorCode ERROR_SAVING_EMAIL_BOX = new SimpleServiceErrorCode(NAMESPACE, 12101);
		public static final SimpleServiceErrorCode ERROR_FETCHING_MAIL_BOX = new SimpleServiceErrorCode(NAMESPACE, 12102);	
		public static final SimpleServiceErrorCode ERROR_FETCHING_MAIL_LIST = new SimpleServiceErrorCode(NAMESPACE, 12103);
		public static final SimpleServiceErrorCode ERROR_PARSING_MSG_FILE = new SimpleServiceErrorCode(NAMESPACE, 12104);
		public static final SimpleServiceErrorCode ERROR_PROJECT_DETAILS_NOT_FOUND = new SimpleServiceErrorCode(NAMESPACE, 12105);
		public static final SimpleServiceErrorCode ERROR_PARSING_MAIL_FIELDS =  new SimpleServiceErrorCode(NAMESPACE, 12106);
		public static final SimpleServiceErrorCode ERROR_FETCHING_JSON_ARRAY_VALUES =  new SimpleServiceErrorCode(NAMESPACE, 12107);

	}
	public static class ApplicationSequence
	{
		private static final String NAMESPACE = "appseq";
		public static final SimpleServiceErrorCode ERROR_REGISTERING_SEQUENCE = new SimpleServiceErrorCode(NAMESPACE, 12001);
		public static final SimpleServiceErrorCode ERROR_UNIQUE_SEQUENCE = new SimpleServiceErrorCode(NAMESPACE, 12002);
		public static final SimpleServiceErrorCode ERROR_RETRIEVING_SEQUENCE = new SimpleServiceErrorCode(NAMESPACE, 12003);
		public static final SimpleServiceErrorCode ERROR_MAX_SEQ_NUMBER = new SimpleServiceErrorCode(NAMESPACE, 12004);
		
	}
}
