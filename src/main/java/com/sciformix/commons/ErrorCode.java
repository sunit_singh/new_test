/**
 * 
 */
package com.sciformix.commons;

/**
 * @author sshetty1
 *
 */
public abstract class ErrorCode 
{
	private static final String
		NAMESPACE	=	"Namespace:"
	;
	private static final String
		ERRORCODE	=	"ErrorCode:"
	;	
	private static final String
		SEPARATOR	=	";"
	;
	private int m_nErrorCode = -1;
	
	private String m_sNamespace = null;
	
	public ErrorCode(String p_sNamespace, int p_nErrorCode)
	{
		m_nErrorCode = p_nErrorCode;
		m_sNamespace = p_sNamespace;
	}
	
	public int errorCode()
	{
		return m_nErrorCode;
	}
	
	public String namespace()
	{
		return m_sNamespace;
	}
	
	public String errorMessage()
	{
		//TODO:Get the string from display properties
		return Integer.toString(m_nErrorCode);
	}
	
	public String toString () {
		return NAMESPACE+m_sNamespace+SEPARATOR+ERRORCODE+m_nErrorCode;
	}
}
