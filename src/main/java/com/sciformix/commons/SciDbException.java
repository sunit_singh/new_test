package com.sciformix.commons;

import java.sql.SQLException;

public class SciDbException extends Exception
{
	private String m_sErrorMessage = null;
	private SQLException m_exceptionRoot = null;
	
	private static final long serialVersionUID = -5708247261444507601L;

	public SciDbException(String p_sErrorMessage)
	{
		this(p_sErrorMessage, null);
	}
	
	public SciDbException(String p_sErrorMessage, SQLException p_exceptionRoot)
	{
		m_sErrorMessage = p_sErrorMessage;
		m_exceptionRoot = p_exceptionRoot;
	}
	
	public String getErrorMessage()
	{
		return m_sErrorMessage;
	}
	
	public Exception getRootException()
	{
		return m_exceptionRoot;
	}

}
