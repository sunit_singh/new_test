package com.sciformix.commons;

public class SciConstants
{
	public static class CharacterConstants
	{
		public static final char SPACE = ' ';
	}
	
	public static class StringConstants
	{
		public static final String EMPTY = "";
		public static final String COLON = ":";
		public static final String SEMICOLON = ";";
		public static final String COMMA = ",";
		public static final String COMMA_WITH_SPACE = ", ";
		public static final String QUESTIONMARK = "?";
		public static final String PARENTHESIS_OPEN = "(";
		public static final String PARENTHESIS_CLOSE = ")";
		public static final String SPACE = " ";
		public static final String DOLLAR = "$";
		public static final String CARRIAGERETURN = "\r";
		public static final String NEWLINE = "\n";
		public static final String CRLF = "\r\n";
		public static final String TILDE = "~";
		public static final String CARET = "^";
		public static final String CURLY_OPEN = "{";
		public static final String CURLY_CLOSE = "}";
		public static final String LESSTHAN = "<";
		public static final String GREATERTHAN = ">";
		public static final String PIPE = "|";
		public static final String HASH = "#";
		public static final String PERIOD = ".";
		public static final String NOTAPPLICABLE = "NA";
		public static final String AMPERSAND = "&";
		public static final String EQUAL = "=";
		public static final String FILE_CHARSET_UTF8 = "UTF-8";
		public static final String QUESTIONMARK_WITH_COMMA = "?,";
		public static final String ESCAPE_CSV = "\"";
		public static final String HYPHEN = "-";
		public static final String PERCENT = "%";
		public static final String ASTERISK = "*";
	}
	
	public static class RegExConstants
	{
		public static final String OBJECT_NAME = "^[A-Za-z0-9 _.-]*$";
		public static final String EMAIL = "^[a-zA-Z0-9]+[\\.]?[a-zA-Z0-9]+@[a-zA-Z0-9]+([\\.]+[a-zA-Z]{2,4})$";
		public static final String FTL = "<{1}( *\\/*)*(script)|(script)( *\\/*)*>{1}";
		public static final String INTEGER = "^\\-?\\d*$";
		public static final String FILE = "^[\\w]?\\:?[\\w\\s-\\\\,\\.\\]\\[\\~\\@\\#\\$\\%\\^\\&\\(\\)\\+\\=\\.\\`]+\\.[A-Za-z]{2,4}$";
		
		public static final String NUMBER = "^[0-9]\\d*(\\.\\d+)?$";
		public static final String CONFIGSETTINGS = "<{1}( *\\/*)*(script)|(script)( *\\/*)*>{1}";
		public static final String USERID = "^[A-Za-z0-9.@]{1,50}$";
		public static final String PASSWORD = "";
		public static final String RULECRITERION = "^[A-Za-z0-9 ()$_.-]*$";
		public static final String CASEID = "^[A-Za-z0-9 _.-]*$";
		public static final String CASEVERSION = "^[A-Za-z0-9 :/]*$";
		public static final String DATE = "(([0]?[1-9])|([1-2][0-9])|([3][0-1]))\\-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)\\-([1-2]{1}[0-9]{3})$";
		public static final String DESCRIPTION = "<{1}( *\\/*)*(script)|(script)( *\\/*)*>{1}";
		public static final String RESTRICTEDDESCRIPTION = "<{1}( *\\/*)*(script)|(script)( *\\/*)*>{1}|\\#|\\,|\\~|\\;|\\:|\\(|\\)";
	}
	
	public static class ValidationTypeConstants
	{
		public static final String TYPE_EMAIL = "EMAIL";
		public static final String TYPE_FTL = "FTL";
		public static final String TYPE_INTEGER = "INTEGER";
		public static final String TYPE_FILE = "FILE";
		
		public static final String TYPE_NUMBER = "NUMBER";
		public static final String TYPE_CONFIGSETTINGS = "CONFIGSETTINGS";
		public static final String TYPE_USERID = "USERID";
		public static final String TYPE_PASSWORD = "PASSWORD";
		public static final String TYPE_RULECRITERION = "RULECRITERION";
		public static final String TYPE_CASEID = "CASEID";
		public static final String TYPE_CASEVERSION = "CASEVERSION";
		public static final String TYPE_DATE = "DATE";
		public static final String TYPE_DESCRIPTION = "DESCRIPTION";
		public static final String TYPE_RESTRICTEDDESCRIPTION = "RESTRICTEDDESCRIPTION";
		public static final String TYPE_ADVANCE = "ADVANCE";
		
	}
	
	public static class XMLParseConstant
	{
		public static final String XML_PARSER_AA_KEY = "AA_Parser_Configuration";
		public static final String XML_PARSER_AB_KEY = "AB_Parser_Configuration";
		public static final String XML_PARSER_BA_KEY = "BA_Parser_Configuration";
		public static final String PARSER_PROPERTIES_AA_KEY = "AA_Parser_SDKeyMapping";
		public static final String PARSER_PROPERTIES_BA_KEY = "BA_Parser_SDKeyMapping";
		public static final String PARSER_PROPERTIES_AB_KEY = "AB_Parser_SDKeyMapping";
		public static final String XML_EXT = ".xml";
		public static final String PROPERTIES_EXT = ".properties";
		
	}
	public static final int FILE_MIN_THRESHOLD_SIZE = 1024 * 1024 * 3;//TODO: 
	public static final int FILE_MAX_SIZE= 1024 * 1024 * 40;//TODO:
	public static final int HTTP_REQUEST_MAX_SIZE= 1024 * 1024 * 50;//TODO:
	
	
	public static final int MAX_COLUMN_SIZE_OF_FILE = 30;//TODO: 
	public static final int DEFAULT_GRID_SIZE= 10;//TODO:
	public static final int SEARCH_SHEET_NO= 2;//TODO:
	
	public static final String TRACKING_NO= "tracking_no";//TODO:
	//public static final String FILE_UPLOAD_PATH = "D:\\";//TODO:
	
	
	public static final int SIZE_100KB = 100 * 1024; 
	public static final int SIZE_50KB = 50 * 1024; 
	public static final int SIZE_25KB = 25 * 1024; 
	
	public static final String ENCODING_UTF8 = "UTF-8";
}
