package com.sciformix.commons;

public class ObjectIdPair<X,Y>
{
	private X m_nObjectSeqId	=	null;
	private Y m_sObjectName		=	null;
	
	public ObjectIdPair(X p_nObjectSeqId, Y p_sObjectName)
	{
		m_nObjectSeqId = p_nObjectSeqId;
		m_sObjectName = p_sObjectName;
	}
	
	public X getObjectSeqId()
	{
		return m_nObjectSeqId;
	}

	public Y getObjectName()
	{
		return m_sObjectName;
	}
	
	//TODO: Equals should be overridden here
}
