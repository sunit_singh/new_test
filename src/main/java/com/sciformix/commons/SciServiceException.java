package com.sciformix.commons;

public class SciServiceException extends Exception
{
	
	private ErrorCode m_errorCode = null;
	private String m_payload = null;
	private Exception m_exceptionRoot = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5708247261444507601L;

	public SciServiceException(ServiceErrorCodes.SimpleServiceErrorCode p_errorCode)
	{
		this(p_errorCode, null, null);
	}
	
	public SciServiceException(ServiceErrorCodes.SimpleServiceErrorCode p_errorCode, Exception p_exceptionRoot)
	{
		this(p_errorCode, null, p_exceptionRoot);
	}
	
	public SciServiceException(ServiceErrorCodes.SimpleServiceErrorCode p_errorCode, String p_payload, Exception p_exceptionRoot)
	{
		m_errorCode = p_errorCode;
		m_exceptionRoot = p_exceptionRoot;
		m_payload = p_payload;
	}

	public ErrorCode errorCode()
	{
		return m_errorCode;
	}
	
	public String payload()
	{
		return m_payload;
	}
	
	public Exception getRootException()
	{
		return m_exceptionRoot;
	}
}

