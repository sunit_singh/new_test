package com.sciformix.commons.rulesengine;

import java.util.List;
import java.util.Map;

import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.JsonUtils;

public class Criterion
{
	private static final Logger log = LoggerFactory.getLogger(Criterion.class);

	public enum Conjunction
	{
		AND, OR;
	}
	
	public enum OperatorDatatype
	{
		STRING,
		NUMERIC;
	}
	
	public enum OperatorType
	{
		/*
		 * Common Notes:
		 * 	<operator> represents one of the valid enum values of the class Criterion.Operator as identified
		 * 					by the operator name (there is no short-hand notation like '==')
		 * 
		 * 	<operand-variable> is supposed to be a variable that is a string with no spaces and needs to start 
		 * 					with a '$'
		 * 
		 * 	<operand-value> is supposed to be value/literal that could have spaces (if of data type string)
		 */
		/*
		 * Format of usage for UNARY operators:
		 * 		(<operand-variable> <operator>)
		 */
		UNARY,
		/*
		 * Format of usage for BINARY operators:
		 * 		(<operand-variable> <operator> <operand-value>)
		 */
		BINARY,
		/*
		 * Format of usage for TERNARY operators:
		 * 		(<operand-variable> <operator> <operand-value> AND <operand-value>)
		 */
		TERNARY;
	}
	

	
	public enum Operator
	{
		//String Binary Operators
		MATCHES (OperatorDatatype.STRING, OperatorType.BINARY),
		DOES_NOT_MATCH (OperatorDatatype.STRING, OperatorType.BINARY), 
		STARTS_WITH (OperatorDatatype.STRING, OperatorType.BINARY),
		DOES_NOT_START_WITH (OperatorDatatype.STRING, OperatorType.BINARY), 
		ENDS_WITH (OperatorDatatype.STRING, OperatorType.BINARY),
		DOES_NOT_END_WITH (OperatorDatatype.STRING, OperatorType.BINARY), 
		CONTAINS (OperatorDatatype.STRING, OperatorType.BINARY),
		DOES_NOT_CONTAIN (OperatorDatatype.STRING, OperatorType.BINARY), 
		IN_CSV_LIST (OperatorDatatype.STRING, OperatorType.BINARY), 
		NOT_IN_CSV_LIST (OperatorDatatype.STRING, OperatorType.BINARY), 
		
		//String Unary Operators
		IS_EMPTY (OperatorDatatype.STRING, OperatorType.UNARY), 
		IS_NOT_EMPTY (OperatorDatatype.STRING, OperatorType.UNARY), 
		IS_UNSPECIFIED (OperatorDatatype.STRING, OperatorType.UNARY),
		
		//Numeric Binary Operators
		EQUALS (OperatorDatatype.NUMERIC, OperatorType.BINARY),
		NOT_EQUALS (OperatorDatatype.NUMERIC, OperatorType.BINARY), 
		GREATER_THAN (OperatorDatatype.NUMERIC, OperatorType.BINARY), 
		LESSER_THAN (OperatorDatatype.NUMERIC, OperatorType.BINARY), 
		GREATER_THAN_EQUAL_TO (OperatorDatatype.NUMERIC, OperatorType.BINARY), 
		LESSER_THAN_EQUAL_TO (OperatorDatatype.NUMERIC, OperatorType.BINARY),
		
		//Numeric Ternary Operators
		BETWEEN (OperatorDatatype.NUMERIC, OperatorType.TERNARY),
		NOT_BETWEEN (OperatorDatatype.NUMERIC, OperatorType.TERNARY);
		
		//General Operators
		//COUNT (OperatorDatatype.GENERAL, OperatorType.UNARY);
		
		private OperatorDatatype dataType;
		private OperatorType type;
		
		Operator(OperatorDatatype dataType, OperatorType type)
		{
			this.dataType = dataType;
			this.type = type;
		}
		
		public OperatorDatatype dataType()
		{
			return this.dataType;
		}
		
		public OperatorType type()
		{
			return this.type;
		}
	
		public static boolean isListDataOperator(Operator p_eOperator)
		{
			return (p_eOperator == Operator.IN_CSV_LIST || p_eOperator == Operator.NOT_IN_CSV_LIST);
		}
	}
	
	public static Criterion create(Operator p_eOperator, String p_sLhsOperand)
	{
		if (p_eOperator.type() != OperatorType.UNARY)
		{
			throw new IllegalArgumentException("Incorrect method usage");
		}
		return new Criterion(p_eOperator, p_sLhsOperand, null, null);
	}
	
	public static Criterion create(Operator p_eOperator, String p_sLhsOperand, String p_sRhsOperand)
	{
		if (p_eOperator.type() != OperatorType.BINARY)
		{
			throw new IllegalArgumentException("Incorrect method usage");
		}
		return new Criterion(p_eOperator, p_sLhsOperand, p_sRhsOperand, null);
	}
	
	public static Criterion create(Operator p_eOperator, String p_sLhsOperand, String p_sRhsOperand, String p_sRhsOperand2)
	{
		if (p_eOperator.type() != OperatorType.TERNARY)
		{
			throw new IllegalArgumentException("Incorrect method usage");
		}
		return new Criterion(p_eOperator, p_sLhsOperand, p_sRhsOperand, p_sRhsOperand2);
	}
	
	private Operator m_eOperator = null;
	private String m_sLhsOperand = null;
	private String m_sRhsOperand = null;
	private String m_sRhsOperand2 = null;
	
	
	private Criterion(Operator p_eOperator, String p_sLhsOperand, String p_sRhsOperand, String p_sRhsOperand2)
	{
		m_eOperator = p_eOperator;
		m_sLhsOperand = p_sLhsOperand;
		m_sRhsOperand = p_sRhsOperand;
		m_sRhsOperand2 = p_sRhsOperand2;
	}
	
	private String getLhsOperandField()
	{
		//DEV-NOTE: Strip off the '$'
		return (isLhsOperandVariable() ? m_sLhsOperand.substring(1) : m_sLhsOperand);
	}
	
	private boolean isLhsOperandVariable()
	{
		return m_sLhsOperand.startsWith("$");
	}
	
	public String print()
	{
		return "(" + m_sLhsOperand + " " + m_eOperator + " " + m_sRhsOperand + ")";
	}
	
	public OrderedJSONObject convertToJson() throws JSONException
	{
		OrderedJSONObject oJsonCriterion = null;
		
		oJsonCriterion = new OrderedJSONObject();
		
		JsonUtils.addAttribute(oJsonCriterion, "LHS", m_sLhsOperand);
		JsonUtils.addAttribute(oJsonCriterion, "RHS", m_sRhsOperand);
		JsonUtils.addAttribute(oJsonCriterion, "OPERATOR", m_eOperator.name());
		
		return oJsonCriterion;
	}
	
	public boolean evalute(Map<String, Object> mapRuntimeValues) throws SciException
	{
		boolean bEvalResult = false;
		String sLhsOperandValue = null;
		
	    sLhsOperandValue = getLhsOperandValue(mapRuntimeValues);
		if(sLhsOperandValue !=null)
		{	
			if (m_eOperator.dataType() == OperatorDatatype.STRING)
			{
				String sRhsOperandValue = null;
				sRhsOperandValue = m_sRhsOperand;
				
				bEvalResult = OperatorEvalImpl.evaluateStringOperator(m_eOperator, sLhsOperandValue, sRhsOperandValue);
			}
			else if (m_eOperator.dataType() == OperatorDatatype.NUMERIC)
			{
				String sRhsOperandValue = null;
				String sRhsOperand2Value = null;
				
				sRhsOperandValue = m_sRhsOperand;
				sRhsOperand2Value = m_sRhsOperand2;
				
				bEvalResult = OperatorEvalImpl.evaluateNumericOperator(m_eOperator, sLhsOperandValue, sRhsOperandValue, sRhsOperand2Value);
			}
			else
			{
				log.error("Operator not supported - {} ", m_eOperator.dataType().name());
				throw new SciException("Operator not supported yet", null);
			}
		}
		return bEvalResult;
	}

	@SuppressWarnings("rawtypes")
	private String getLhsOperandValue(Map<String, Object> mapRuntimeValues)
	{
		String sLhsOperandField = null;
		Object oLhsOperandValue = null;
		sLhsOperandField = getLhsOperandField();
		if(sLhsOperandField!=null)
		{
			oLhsOperandValue = mapRuntimeValues.get(sLhsOperandField);
			if(oLhsOperandValue!=null && oLhsOperandValue instanceof String)
			{
				return oLhsOperandValue.toString();
			}
			else if(oLhsOperandValue!=null && oLhsOperandValue instanceof List)
			{
				for (Object oValue : ((List)oLhsOperandValue))
				{
					return oValue.toString();
				}
			}
			else
			{
				return null;
			}
		}
		return null;
	}
	
	//Added by Rohini
	public String getLhsOperandValueForCaseValidation(Map<String, Object> mapRuntimeValues) throws SciServiceException
	{
		String sLhsOperandField = null;
		boolean oLhsOperandValue = false;
		sLhsOperandField = getLhsOperandField();
		if(sLhsOperandField!=null)
		{
			oLhsOperandValue = mapRuntimeValues.containsKey(sLhsOperandField);
			if(oLhsOperandValue)
			{
				return null;
			}
			else
			{

				log.error("Column mentioned in Criteria "+sLhsOperandField+" is not present in the Case File . It is missing...Hence cannot proceed");
//				throw new SciServiceException("Column mentioned in Criteria "+sLhsOperandField+" is not present in the Case File . It is missing...Hence cannot proceed", null);
				throw new SciServiceException(ServiceErrorCodes.Ngv.CASE_VALIDATION_FAILED_DUE_TO_COLUMN_MISSING,sLhsOperandField,null);
			}
		}
		else
		{
			log.error("Column mentioned in Criteria "+sLhsOperandField+" is not present in the Case File . It is missing...Hence cannot proceed");
			throw new SciServiceException(ServiceErrorCodes.Ngv.CASE_VALIDATION_FAILED_DUE_TO_COLUMN_MISSING,sLhsOperandField,null);
		}
	}
	//Ended by Rohini
	
}
