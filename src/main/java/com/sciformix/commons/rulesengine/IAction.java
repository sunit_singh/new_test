package com.sciformix.commons.rulesengine;

public interface IAction
{
	
	
	public String convertToString();

	public String convertToJson();
}
