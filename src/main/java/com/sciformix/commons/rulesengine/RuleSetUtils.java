package com.sciformix.commons.rulesengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.CharacterConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.rulesengine.Criterion.Conjunction;
import com.sciformix.commons.rulesengine.Criterion.OperatorType;
import com.sciformix.commons.utils.StringUtils;

public class RuleSetUtils
{
	private static final Logger log = LoggerFactory.getLogger(RuleSetUtils.class);

	private RuleSetUtils()
	{
		//Nothing to do
	}
	
	public static RuleSet createRuleSetFromCriteria(String p_sCriteria) throws SciException
	{
		if (!isValidRuleCriteria(p_sCriteria))
		{
			return null;
		}
		
		RuleSet oRuleSet = null;
		Rule oRule = null;
		String sCriteriaToParse = null;
		String sSingleCriterion = null;
		String[] sarrSingleCriterionElements = null;
		Criterion.Operator eOperator = null;
		String sLhsOperandVariable = null;
		String sRhsOperandValue = null;
		String sRhsOperand2Value = null;
		int indexParanthesesOpen = -1;
		int indexParanthesesClose = -1;
		boolean bCriteriaAdded = false;
		String sConjuction = null;
		Conjunction eConjunction = null;
		String sTemp = null;
		String[] sarrTemp = null;
		oRuleSet = new RuleSet();
		
		sCriteriaToParse = p_sCriteria;
		
		oRule = new Rule(null);
		
		while (true)
		{
			indexParanthesesOpen = sCriteriaToParse.indexOf(StringConstants.PARENTHESIS_OPEN);
			if (indexParanthesesOpen == -1)
			{
				return null;
			}
			
			if (bCriteriaAdded)
			{
				sConjuction = sCriteriaToParse.substring(0, indexParanthesesOpen);
				eConjunction = (sConjuction != null ? Conjunction.valueOf(sConjuction.trim()) : null);
				if (eConjunction == null)
				{
					//Error
					return null;
				}
				//else all good
			}
			
			indexParanthesesClose = sCriteriaToParse .indexOf(StringConstants.PARENTHESIS_CLOSE, indexParanthesesOpen);
			if (indexParanthesesClose == -1)
			{
				return null;
			}
			
			
			sSingleCriterion = sCriteriaToParse.substring(indexParanthesesOpen + 1, indexParanthesesClose);
			sarrSingleCriterionElements = StringUtils.splitOnWhitespace(sSingleCriterion, true);
			
			if (sarrSingleCriterionElements.length == 2)
			{
				eOperator = convertToOperator(sarrSingleCriterionElements[1]);
				sLhsOperandVariable = sarrSingleCriterionElements[0];
				
				if (!bCriteriaAdded)
				{
					oRule.add(Criterion.create(eOperator, sLhsOperandVariable));
				}
				else
				{
					oRule.add(eConjunction, Criterion.create(eOperator, sLhsOperandVariable));
				}
			}
			else if (sarrSingleCriterionElements.length > 2)
			{
				eOperator = convertToOperator(sarrSingleCriterionElements[1]);
				if (eOperator.type() == Criterion.OperatorType.BINARY)
				{
					sLhsOperandVariable = sarrSingleCriterionElements[0];
					sRhsOperandValue = extractRhsOperand(sSingleCriterion);
					
					if (!bCriteriaAdded)
					{
						oRule.add(Criterion.create(eOperator, sLhsOperandVariable, sRhsOperandValue));
					}
					else
					{
						oRule.add(eConjunction, Criterion.create(eOperator, sLhsOperandVariable, sRhsOperandValue));
					}
				}
				else if (eOperator.type() == Criterion.OperatorType.TERNARY)
				{
					sLhsOperandVariable = sarrSingleCriterionElements[0];
					sTemp = null;
					sTemp =  sarrSingleCriterionElements[2];
					sarrTemp = sTemp!=null?sTemp.split(","):null;
					sRhsOperandValue = sarrTemp[0];
					sRhsOperand2Value = sarrTemp[1];
					
					if (!bCriteriaAdded)
					{
						oRule.add(Criterion.create(eOperator, sLhsOperandVariable, sRhsOperandValue, sRhsOperand2Value));
					}
					else
					{
						oRule.add(eConjunction, Criterion.create(eOperator, sLhsOperandVariable, sRhsOperandValue, sRhsOperand2Value));
					}
				}
				else
				{
					log.error("Operator not supported - {}", eOperator.type().name());
					throw new SciException("Operator not supported", null);
				}
			}
			else
			{
				//TODO:Error
				log.error("Invalid criterion length");
				throw new SciException("Invalid criterion length", null);
			}
			bCriteriaAdded = true;
			
			sCriteriaToParse = sCriteriaToParse.substring(indexParanthesesClose + 1);
			if (sCriteriaToParse.trim().length() == 0)
			{
				break;
			}
		}
		oRuleSet.add(oRule);
		
		return oRuleSet;
	}
	
	public static boolean isValidRuleCriteria(String p_sInputValue)
	{
		return isValidRuleCriteria(p_sInputValue, true, true);
	}
	
	public static boolean isValidRuleCriteria(String p_sInputValue, boolean p_bMixedConjunctionSupported, boolean p_bEnforceAndOrSequence)
	{
		String sCriteriaToParse = null;
		int indexParanthesesOpen = -1;
		int indexParanthesesClose = -1;
		String sSingleCriterion = null;
		String[] sarrSingleCriterionElements = null;
		Criterion.Operator eOperator = null;
		String sLhsOperandVariable = null;
		String sRhsOperandValue = null;
		String sRhsOperand2Value = null;
		String sConjuction = null;
		boolean bCriteriaFound = false;
		Conjunction eConjunction = null;
		Conjunction eEarlierConjunction = null;
		String[] sarrTemp = null;
		sCriteriaToParse = p_sInputValue.trim();
		String sTemp = null;
		while (true)
		{
			indexParanthesesOpen = sCriteriaToParse.indexOf(StringConstants.PARENTHESIS_OPEN);
			if (indexParanthesesOpen == -1)
			{
				return false;
			}
			
			if (bCriteriaFound)
			{
				sConjuction = sCriteriaToParse.substring(0, indexParanthesesOpen);
				eConjunction = (sConjuction != null ? Conjunction.valueOf(sConjuction.trim()) : null);
				if (eConjunction == null)
				{
					return false;
				}
				
				eEarlierConjunction = ((eEarlierConjunction == null) ? eConjunction : eEarlierConjunction);
				if (!p_bMixedConjunctionSupported)
				{
					if (eEarlierConjunction != eConjunction)
					{
						return false;
					}
				}
				
				if (p_bEnforceAndOrSequence)
				{
					if (eConjunction == Conjunction.AND && eEarlierConjunction == Conjunction.OR)
					{
						return false;
					}
				}
				//else all good
			}
			
			indexParanthesesClose = sCriteriaToParse .indexOf(StringConstants.PARENTHESIS_CLOSE, indexParanthesesOpen);
			if (indexParanthesesClose == -1)
			{
				return false;
			}
			
			sSingleCriterion = sCriteriaToParse.substring(indexParanthesesOpen + 1, indexParanthesesClose);
			sarrSingleCriterionElements = sSingleCriterion.split(StringConstants.SPACE);
			
			if (sarrSingleCriterionElements.length == 2)
			{
				eOperator = convertToOperator(sarrSingleCriterionElements[1]);
				if (eOperator == null || eOperator.type() != OperatorType.UNARY)
				{
					return false;
				}
				
				sLhsOperandVariable = sarrSingleCriterionElements[0];
				if (!isValidVariableOperand(sLhsOperandVariable))
				{
					return false;
				}
				
				//Continue to check for other criteria
			}
			else if (sarrSingleCriterionElements.length > 2)
			{
				eOperator = convertToOperator(sarrSingleCriterionElements[1]);
				if (eOperator == null)
				{
					return false;
				}
				
				if (eOperator.type() == OperatorType.BINARY)
				{
					sLhsOperandVariable = sarrSingleCriterionElements[0];
					if (!isValidVariableOperand(sLhsOperandVariable))
					{
						return false;
					}
					
					sRhsOperandValue = extractRhsOperand(sSingleCriterion);
					if (!isValidValueOperand(eOperator, sRhsOperandValue))
					{
						return false;
					}
				}
				else if (eOperator.type() == OperatorType.TERNARY)
				{
					sLhsOperandVariable = sarrSingleCriterionElements[0];
					if (!isValidVariableOperand(sLhsOperandVariable))
					{
						return false;
					}
					
					/*if (sarrSingleCriterionElements.length != 5)
					{
						return false;
					}
					if (!sarrSingleCriterionElements[3].equalsIgnoreCase(OperatorEvalImpl._SPECIAL_STRING_AND))
					{
						return false;
					}*/
					sTemp = null;
					sTemp =  sarrSingleCriterionElements[2];
					sarrTemp = sTemp!=null?sTemp.split(","):null;
					if(sarrTemp==null || sarrTemp.length != 2)
					{
						return false;
					}
					
					sRhsOperandValue = sarrTemp[0];
					sRhsOperand2Value = sarrTemp[1];
					if (!isValidValueOperand(eOperator, sRhsOperandValue))
					{
						return false;
					}
					if (!isValidValueOperand(eOperator, sRhsOperand2Value))
					{
						return false;
					}
					
					//Continue to check for other criteria
				}
				else
				{
					//ERROR
					log.error("Operator not supported - " + eOperator.type().name());
					return false;
				}
				
				//Continue to check for other criteria
			}
			else
			{
				log.error("Invalid criterion length");
				return false;
			}
			bCriteriaFound = true;
			
			sCriteriaToParse = sCriteriaToParse.substring(indexParanthesesClose + 1);
			if (sCriteriaToParse.trim().length() == 0)
			{
				//DEV-NOTE: This is to terminate the loop
				break;
			}
		}
		return true;
	}
	
	private static boolean isValidVariableOperand(String p_sOperandString)
	{
		String sOperand = null;
		
		sOperand = p_sOperandString.trim();
		if (StringUtils.isNullOrEmpty(p_sOperandString) || StringUtils.containsSpace(p_sOperandString))
		{
			return false;
		}
		if (!sOperand.startsWith(StringConstants.DOLLAR))
		{
			return false;
		}
		return true;
	}
	
	private static Criterion.Operator convertToOperator(String p_sOperatorString)
	{
		try
		{
			return Criterion.Operator.valueOf(p_sOperatorString);
		}
		catch (IllegalArgumentException excep)
		{
			log.error("Invalid argument during operator conversion - " + p_sOperatorString, excep);
			return null;
		}
		catch (NullPointerException excep)
		{
			log.error("Null Operator provided");
			return null;
		}
	}
	
	private static String extractRhsOperand(String p_sSingleCriterion)
	{
		String sCriterionString = null;
		int nIndex = -1;
		String sRhsOperand = null;
		
		//DEV-NOTE: In order to handle spaces that are allowed in the Rhs operand, 'direct splitting' on space 
		//would not work
		sCriterionString = p_sSingleCriterion.trim();
		
		//First index terminates after the operand-variable
		nIndex = sCriterionString.indexOf(StringConstants.SPACE);
		if (nIndex == -1)
		{
			log.error("Invalid RHS Operand");
			return null;
		}
		//Skip over any consecutive white spaces
		while (sCriterionString.charAt(nIndex) == CharacterConstants.SPACE)
		{
			nIndex++;
		}
		
		//Second index terminates after the operator
		nIndex = sCriterionString.indexOf(StringConstants.SPACE, (nIndex+1));
		if (nIndex == -1)
		{
			log.error("Invalid RHS Operand");
			return null;
		}
		//Skip over any consecutive white spaces
		while (sCriterionString.charAt(nIndex) == CharacterConstants.SPACE)
		{
			nIndex++;
		}
		//it is returning emale instead Female or ale instead to Male so changed nIndex+1 to index
		sRhsOperand = sCriterionString.substring(nIndex);
		
		return sRhsOperand;
	}
	
	private static boolean isValidValueOperand(Criterion.Operator p_eOperator, String p_sValueOperand)
	{
		if (p_eOperator.dataType() == Criterion.OperatorDatatype.STRING)
		{
			if (StringUtils.isNullOrEmpty(p_sValueOperand))
			{
				return false;
			}
			
			/*if (Criterion.Operator.isListDataOperator(p_eOperator))
			{
				//TODO: No support available
				log.error("List Operator currently not supported");
				return false;
			}*/
			
			return true;
		}
		else if (p_eOperator.dataType() == Criterion.OperatorDatatype.NUMERIC)
		{
			if (StringUtils.isNullOrEmpty(p_sValueOperand))
			{
				return false;
			}
			try
			{
				Integer.parseInt(p_sValueOperand);
			}
			catch (NumberFormatException nfExcep)
			{
				log.error("Exception during parsing numeric value - {}", p_sValueOperand, nfExcep);
				return false;
			}
			
			/*if (p_eOperator == Criterion.Operator.BETWEEN || p_eOperator == Criterion.Operator.NOT_BETWEEN)
			{
				//TODO:No support available
				log.error("Operator BETWEEN and NOT BETWEEN currently not supported");
				return false;
			}*/
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	static int convertToIntOperandValue(String p_sOperandString)
	{
		if (StringUtils.isNullOrEmpty(p_sOperandString))
		{
			return -1;
		}
		
		try
		{
			return Integer.parseInt(p_sOperandString);
		}
		catch (NumberFormatException nfExcep)
		{
			log.error("Exception during parsing numeric operand - {}", p_sOperandString, nfExcep);
			return -1;
		}
	}

	public static boolean isValidRuleExpression(String p_sInputValue)
	{
		if (StringUtils.isNullOrEmpty(p_sInputValue) || !isValidRuleCriteria(p_sInputValue))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
}
