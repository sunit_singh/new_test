package com.sciformix.commons.rulesengine;

import org.apache.commons.lang3.StringUtils;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.rulesengine.Criterion.Conjunction;
import com.sciformix.commons.rulesengine.Criterion.Operator;

class OperatorEvalImpl
{
	static final String _SPECIAL_STRING_UNSPECIFIED = "UNSPECIFIED";
	static final String _SPECIAL_STRING_AND = "AND";

	private OperatorEvalImpl()
	{
		//Nothing to do
	}
	
	public static boolean evaluateStringOperator(Operator p_eOperator, String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		switch(p_eOperator)
		{
		case MATCHES:
			return matches(p_sLhsOperandValue, p_sRhsOperandValue);
		case DOES_NOT_MATCH:
			return !(matches(p_sLhsOperandValue, p_sRhsOperandValue));
		case STARTS_WITH:
			return startsWith(p_sLhsOperandValue, p_sRhsOperandValue);
		case DOES_NOT_START_WITH:
			return !(startsWith(p_sLhsOperandValue, p_sRhsOperandValue));
		case ENDS_WITH:
			return endsWith(p_sLhsOperandValue, p_sRhsOperandValue);
		case DOES_NOT_END_WITH:
			return !(endsWith(p_sLhsOperandValue, p_sRhsOperandValue));
		case CONTAINS:
			return contains(p_sLhsOperandValue, p_sRhsOperandValue);
		case DOES_NOT_CONTAIN:
			return !(contains(p_sLhsOperandValue, p_sRhsOperandValue));
		case IN_CSV_LIST:
			return inCsvList(p_sLhsOperandValue, p_sRhsOperandValue);
		case NOT_IN_CSV_LIST:
			return !(inCsvList(p_sLhsOperandValue, p_sRhsOperandValue));
		case IS_EMPTY:
			return isEmpty(p_sLhsOperandValue);
		case IS_NOT_EMPTY:
			return !(isEmpty(p_sLhsOperandValue));
		case IS_UNSPECIFIED:
			return matches(p_sLhsOperandValue, _SPECIAL_STRING_UNSPECIFIED);
		default:
			//ERROR
		}
		return false;
	}
	
	//TODO:Change the incoming parameter data-type to int
	public static boolean evaluateNumericOperator(Operator p_eOperator, String p_sLhsOperandValue, String p_sRhsOperandValue, String p_sRhsOperand2Value)
	{
		switch(p_eOperator)
		{
		case EQUALS:
			return equals(p_sLhsOperandValue, p_sRhsOperandValue);
		case NOT_EQUALS:
			return !(equals(p_sLhsOperandValue, p_sRhsOperandValue));
		case GREATER_THAN:
			return greaterThan(p_sLhsOperandValue, p_sRhsOperandValue);
		case LESSER_THAN_EQUAL_TO:
			return !(greaterThan(p_sLhsOperandValue, p_sRhsOperandValue));
		case LESSER_THAN:
			return lesserThan(p_sLhsOperandValue, p_sRhsOperandValue);
		case GREATER_THAN_EQUAL_TO:
			return !(lesserThan(p_sLhsOperandValue, p_sRhsOperandValue));
		case BETWEEN:
			return between(p_eOperator, p_sLhsOperandValue, p_sRhsOperandValue, p_sRhsOperand2Value);
		case NOT_BETWEEN:
			return !(between(p_eOperator, p_sLhsOperandValue, p_sRhsOperandValue, p_sRhsOperand2Value));
		default:
			//ERROR
		}
		return false;
	}
	
	public static boolean getInitialResultValue(Conjunction p_eConjunction)
	{
		if (p_eConjunction == Conjunction.AND)
		{
			return true;
		}
		else if (p_eConjunction == Conjunction.OR)
		{
			return false;
		}
		else
		{
			//TODO:Error
			throw new UnsupportedOperationException("Operator not supported yet");
		}
	}

	public static boolean combineResultValue(Conjunction p_eConjunction, boolean p_bExistingResult, boolean p_bAdditionalResult)
	{
		if (p_eConjunction == Conjunction.AND)
		{
			return (p_bExistingResult && p_bAdditionalResult);
		}
		else if (p_eConjunction == Conjunction.OR)
		{
			return (p_bExistingResult || p_bAdditionalResult);
		}
		else
		{
			//TODO:Error
			throw new UnsupportedOperationException("Operator not supported yet");
		}
	}
	
	private static String cleanStringOperandValue(String p_sOperandValue)
	{
		return (p_sOperandValue != null ? p_sOperandValue.trim().toUpperCase() : null); 
	}
	
	private static boolean isEmpty(String p_sLhsOperandValue)
	{
		return (p_sLhsOperandValue!=null ? (p_sLhsOperandValue.trim().length() == 0) : true);
	}
	
	private static boolean matches(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		String sLhsOperandValue = null;
		String sRhsOperandValue = null;
		
		sLhsOperandValue = cleanStringOperandValue(p_sLhsOperandValue);
		sRhsOperandValue = cleanStringOperandValue(p_sRhsOperandValue);
		
		return (sLhsOperandValue!=null ? sLhsOperandValue.equalsIgnoreCase(sRhsOperandValue) : (sRhsOperandValue==null));
	}
	
	private static boolean startsWith(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		String sLhsOperandValue = null;
		String sRhsOperandValue = null;
		
		sLhsOperandValue = cleanStringOperandValue(p_sLhsOperandValue);
		sRhsOperandValue = cleanStringOperandValue(p_sRhsOperandValue);
		
		return (sLhsOperandValue != null ? sLhsOperandValue.startsWith(sRhsOperandValue) : (sRhsOperandValue==null));
	}
	
	private static boolean endsWith(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		String sLhsOperandValue = null;
		String sRhsOperandValue = null;
		
		sLhsOperandValue = cleanStringOperandValue(p_sLhsOperandValue);
		sRhsOperandValue = cleanStringOperandValue(p_sRhsOperandValue);
		
		return (sLhsOperandValue != null ? sLhsOperandValue.endsWith(sRhsOperandValue) : (sRhsOperandValue==null));
	}
	
	private static boolean contains(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		String sLhsOperandValue = null;
		String sRhsOperandValue = null;
		
		sLhsOperandValue = cleanStringOperandValue(p_sLhsOperandValue);
		sRhsOperandValue = cleanStringOperandValue(p_sRhsOperandValue);
		
		return (sLhsOperandValue!=null ? sLhsOperandValue.contains(sRhsOperandValue) : (sRhsOperandValue==null));
	}
	
	private static boolean inCsvList(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		String[] sarrRhsOperandValues = null;
		String sLhsOperandValue = null;
		
		sLhsOperandValue = cleanStringOperandValue(p_sLhsOperandValue);
		sarrRhsOperandValues = StringUtils.split(p_sRhsOperandValue, StringConstants.COMMA);
		
		for (String sSingleRhsValue : sarrRhsOperandValues)
		{
			sSingleRhsValue = cleanStringOperandValue(sSingleRhsValue);
			
			if (sLhsOperandValue!=null ? sLhsOperandValue.equalsIgnoreCase(sSingleRhsValue) : (sSingleRhsValue==null))
			{
				return true;
			}
		}
		return false;
		//throw new UnsupportedOperationException("Operator not supported yet");
	}
	
	private static int cleanNumber(String p_sValue)
	{
		return RuleSetUtils.convertToIntOperandValue(p_sValue);
	}
	
	private static boolean equals(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		int sLhsOperandValue = -1;
		int sRhsOperandValue = -1;
		
		sLhsOperandValue = cleanNumber(p_sLhsOperandValue);
		sRhsOperandValue = cleanNumber(p_sRhsOperandValue);
		
		return (sLhsOperandValue == sRhsOperandValue);
	}
	
	private static boolean greaterThan(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		int sLhsOperandValue = -1;
		int sRhsOperandValue = -1;
		
		sLhsOperandValue = cleanNumber(p_sLhsOperandValue);
		sRhsOperandValue = cleanNumber(p_sRhsOperandValue);
		
		return (sLhsOperandValue > sRhsOperandValue);
	}
	
	private static boolean lesserThan(String p_sLhsOperandValue, String p_sRhsOperandValue)
	{
		int sLhsOperandValue = -1;
		int sRhsOperandValue = -1;
		
		sLhsOperandValue = cleanNumber(p_sLhsOperandValue);
		sRhsOperandValue = cleanNumber(p_sRhsOperandValue);
		
		return (sLhsOperandValue < sRhsOperandValue);
	}
	
	private static boolean between(Operator p_eOperator, String p_sLhsOperandValue, String p_sRhsOperandValue, String p_sRhsOperand2Value)
	{
		int nLhsOperandValue = -1;
		int nRhsOperandValue = -1;
		int nRhsOperand2Value = -1;
		nLhsOperandValue = cleanNumber(p_sLhsOperandValue);
		nRhsOperandValue = cleanNumber(p_sRhsOperandValue);
		nRhsOperand2Value = cleanNumber(p_sRhsOperand2Value);
		if(nLhsOperandValue == -1 && p_eOperator.equals(Operator.NOT_BETWEEN))
		{
			return true;
		}
		if(nLhsOperandValue == -1)
		{
			return false;
		}
		return ((nLhsOperandValue == nRhsOperandValue || nLhsOperandValue == nRhsOperand2Value) || (nLhsOperandValue > nRhsOperandValue && nLhsOperandValue < nRhsOperand2Value));
	}
}
