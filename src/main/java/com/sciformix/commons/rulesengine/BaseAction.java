package com.sciformix.commons.rulesengine;

public abstract class BaseAction implements IAction
{
	
	public BaseAction()
	{
		//Nothing to do
	}
	
	
	
	public String convertToString()
	{
		return this.getClass().getCanonicalName();
	}
	
	public String convertToJson()
	{
		return this.getClass().getCanonicalName();
	}
}
