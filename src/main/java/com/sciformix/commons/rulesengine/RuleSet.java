package com.sciformix.commons.rulesengine;

import java.util.ArrayList;

import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;

public class RuleSet extends ArrayList<Rule>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 127110307800552233L;
	
	
	public RuleSet()
	{
		super();
	}
	
	public RuleSet(int size)
	{
		super(size);
	}
	
	public boolean add(Rule p_oRule)
	{
		return super.add(p_oRule);
	}
	
	public Rule get(int index)
	{
		return (Rule)super.get(index);
	}
	
	public String print()
	{
		StringBuffer sb = null;
		int nRuleIndex = 0;
		
		sb = new StringBuffer();
		
		for (Rule oRule : this)
		{
			if (nRuleIndex > 0)
			{
				sb.append("\r\n");
			}
			sb.append("Rule #").append((nRuleIndex+1)).append("> ").append(oRule.print());
			nRuleIndex++;
		}
		
		return sb.toString();
	}
	
	public OrderedJSONObject convertToJson() throws JSONException
	{
		OrderedJSONObject oJsonRuleSet = null;
		JSONArray oJsonRuleArray = null;
		OrderedJSONObject oJsonRule = null;
		
		oJsonRuleSet = new OrderedJSONObject();
		
		oJsonRuleArray = new JSONArray();
		
		for (Rule oRule : this)
		{
			oJsonRule = oRule.convertToJson();
			oJsonRuleArray.add(oJsonRule);
		}
		oJsonRuleSet.put("RULES", oJsonRuleArray);
		
		return oJsonRuleSet;
	}
}
