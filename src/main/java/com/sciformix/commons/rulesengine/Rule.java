package com.sciformix.commons.rulesengine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.rulesengine.Criterion.Conjunction;
import com.sciformix.commons.utils.JsonUtils;

public class Rule
{
	private List<Conjunction> m_listConjunctions = null;
	private List<Criterion> m_listCriteria = null;
	private IAction m_oAction = null;
	
	public Rule(IAction p_oAction)
	{
		m_oAction = p_oAction;
		
		m_listCriteria = new ArrayList<>();
		m_listConjunctions = new ArrayList<>();
	}
	
	public boolean add(Criterion p_oCriteria)
	{
		if (m_listCriteria.size() == 0)
		{
			return m_listCriteria.add(p_oCriteria);
		}
		else
		{
			return false;
		}
	}
	
	public boolean add(Conjunction p_eConjunction, Criterion p_oCriteria)
	{
		m_listConjunctions.add(p_eConjunction);
		return m_listCriteria.add(p_oCriteria);
	}
	
	public IAction getAction()
	{
		return m_oAction;
	}
	
	public String print()
	{
		StringBuffer sb = null;
		int nCriteriaAdditionCounter = 0;
		
		sb = new StringBuffer();
		
		sb.append("Action:");
		if (m_oAction != null)
		{
			sb.append(m_oAction.convertToString());
		}
		sb.append(" - ");

		sb.append("Criteria: ");
		nCriteriaAdditionCounter = 0;
		for (Criterion oCriteria : m_listCriteria)
		{
			if (nCriteriaAdditionCounter > 0)
			{
				sb.append(" ").append(m_listConjunctions.get(nCriteriaAdditionCounter-1).toString()).append(" ");
			}
			else
			{
				//Nothing to do
			}
			sb.append(oCriteria.print());
			nCriteriaAdditionCounter++;
		}
		return sb.toString();
	}
	
	public OrderedJSONObject convertToJson() throws JSONException
	{
		OrderedJSONObject oJsonRule = null;
		JSONArray oJsonCriteriaArray = null;
		OrderedJSONObject oJsonCriterion = null;
		JSONArray oJsonConjunctionArray = null;
		OrderedJSONObject oJsonConjunction = new OrderedJSONObject();
		
		oJsonCriteriaArray = new JSONArray();
		oJsonConjunctionArray = new JSONArray();
		
		oJsonRule = new OrderedJSONObject();
		
		oJsonRule.put("ACTION", m_oAction.convertToString());
		
		
		//oJsonRule.put("CONJUNCTION", m_eConjunction.toString());
		for (Conjunction eConjunction : m_listConjunctions)
		{
			JsonUtils.addAttribute(oJsonConjunction, "CONJUNCTION", eConjunction.toString());
			oJsonConjunctionArray.add(oJsonConjunction);
		}
		oJsonRule.put("CONJUNCTIONS", oJsonConjunctionArray);
		
		for (Criterion oCriterion : m_listCriteria)
		{
			oJsonCriterion = oCriterion.convertToJson();
			oJsonCriteriaArray.add(oJsonCriterion);
		}
		oJsonRule.put("CRITERIA", oJsonCriteriaArray);
		
		return oJsonRule;
	}
	
	public boolean evalute(Map<String, Object> mapRuntimeValues) throws SciException
	{
		boolean bEvalResult = false;
		
		boolean bSingleCriterionEvalResult = false;
		int nCriteriaAdditionCounter = 0;
		
		for (Criterion oCriterion : m_listCriteria)
		{
			bSingleCriterionEvalResult = oCriterion.evalute(mapRuntimeValues);
			
			if (nCriteriaAdditionCounter > 0)
			{
				bEvalResult = OperatorEvalImpl.combineResultValue(m_listConjunctions.get(nCriteriaAdditionCounter-1), bEvalResult, bSingleCriterionEvalResult);
			}
			else
			{
				bEvalResult = bSingleCriterionEvalResult;
			}
			
			//*** Early Exit Logic ***
			// If no next conjunction, return current value
			// If next conjunction is AND the the result so far is FALSE, return FALSE
			// If next conjunction is AND and the result so far is TRUE, continue evaluation
			// If next conjunction is OR and the result so far is TRUE, return TRUE
			// If next conjunction is OR and the result so far is FALSE, continue evaluation
			if (nCriteriaAdditionCounter == m_listConjunctions.size())
			{
				//No more conjunctions to be added, we would return by default since the for loop would exit
			}
			else
			{
				if (m_listConjunctions.get(nCriteriaAdditionCounter) == Conjunction.AND && bEvalResult == false)
				{
					//Do early exit with current value (false)
					break;
				}
				else if (m_listConjunctions.get(nCriteriaAdditionCounter) == Conjunction.OR && bEvalResult == true)
				{
					//Do early exit with current value (true)
					break;
				}
			}
			
			nCriteriaAdditionCounter++;
		}
		
		return bEvalResult;
	}	
	
	public boolean checkForColumn(Map<String, Object> mapRuntimeValues) throws SciServiceException
	{
		boolean bEvalResult = false;
		
		for (Criterion oCriterion : m_listCriteria)
		{
			String LHSOperand = oCriterion.getLhsOperandValueForCaseValidation(mapRuntimeValues);
			if(LHSOperand!=null)
			{
				bEvalResult = false;
			}
			else
			{
				bEvalResult = true;
			}
		}
		return bEvalResult;
	}
	
}
