package com.sciformix.commons;

public class SciStartupLogger
{
	private static SciStartupLogger _logger = null;
	public static boolean ENABLE_TRACE_LOGGING = false;
	
	private SciStartupLogger()
	{
		//Nothing to do
	}
	
	public static SciStartupLogger getLogger()
	{
		if (_logger == null)
		{
			_logger = new SciStartupLogger();
		}
		return _logger;
	}
	
	public void log(String p_sMessage)
	{
		System.out.println(p_sMessage);
	}
	
	public void fatal(String p_sMessage)
	{
		log(p_sMessage);
	}
	
	public void trace(String p_sMessage)
	{
		if (!ENABLE_TRACE_LOGGING)
		{
			return;
		}
		log(p_sMessage);
	}
	
	public void logError(String p_sMessage)
	{
		System.out.println(p_sMessage);
	}

}
