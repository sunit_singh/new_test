package com.sciformix.commons;

import com.sciformix.commons.WebErrorCodes.SimpleWebErrorCode;
import com.sciformix.commons.WebErrorCodes.WebErrorCode;

public class SciWebException extends Exception
{
	private static final long serialVersionUID = -7283056801089977124L;

	private ErrorCode m_errorCode = null;
	private String m_payload = null;
	private Exception m_exceptionRoot = null;
	
	public SciWebException(WebErrorCode p_errorCode, String p_payload)
	{
		this(p_errorCode, p_payload, null);
	}
	
	public SciWebException(SimpleWebErrorCode p_errorCode)
	{
		this(p_errorCode, null, null);
	}
	
	public SciWebException(SimpleWebErrorCode p_errorCode, Exception p_exceptionRoot)
	{
		this(p_errorCode, null, p_exceptionRoot);
	}
	
	public SciWebException(ErrorCode p_errorCode, String p_payload, Exception p_exceptionRoot)
	{
		m_errorCode = p_errorCode;
		m_exceptionRoot = p_exceptionRoot;
		m_payload = p_payload;
	}

	public SciWebException(Exception p_exceptionRoot)
	{
		this(null, null, p_exceptionRoot);
	}

	
	public ErrorCode getErrorCode()
	{
		return m_errorCode;
	}
	
	public String payload()
	{
		return m_payload;
	}
	
	public Exception getRootException()
	{
		return m_exceptionRoot;
	}

}
