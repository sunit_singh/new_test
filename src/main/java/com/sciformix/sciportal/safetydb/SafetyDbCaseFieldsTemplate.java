package com.sciformix.sciportal.safetydb;

import java.util.List;

import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.config.ConfigTplObj;

public class SafetyDbCaseFieldsTemplate extends ConfigTplObj {
	
	
	private static final int UDFINDEX_SAFETYDB_NAME = 0;
	private static final int UDFINDEX_SAFETYDB_VERSION = 1;
	private static final int UDFINDEX_CASEID_ALIAS = 2;
	private static final int UDFINDEX_CASEVERSION_ALIAS = 3;
	
	public SafetyDbCaseFieldsTemplate(
		String	p_safteydbname
	, 	String	p_version
	,	String	p_caseidalias
	,	String	p_caseversionalias
	) throws SciException
	{
		this();
		setUdf(UDFINDEX_SAFETYDB_NAME, p_safteydbname);
		setUdf(UDFINDEX_SAFETYDB_VERSION, p_version);
		setUdf(UDFINDEX_CASEID_ALIAS, p_caseidalias);
		setUdf(UDFINDEX_CASEVERSION_ALIAS, p_caseversionalias);
	}
	
	private List<SafetyDbCaseFields>
		caseFieldList
	;
	
	public SafetyDbCaseFieldsTemplate()
	{
		super(ConfigTplObjType.SCF);
	}
	
	public List<SafetyDbCaseFields> getCaseFieldList() {
		return caseFieldList;
	}
	
	public void setCaseFieldList(List<SafetyDbCaseFields> caseFieldList) {
		this.caseFieldList = caseFieldList;
	}
	
	public String getSafetyDbName() throws SciException
	{
		return getUdf(UDFINDEX_SAFETYDB_NAME);
	}
	
	public String getSafetyDbVersion() throws SciException
	{
		return getUdf(UDFINDEX_SAFETYDB_VERSION);
	}
	public String getCaseIdAlias() throws SciException 
	{
		return getUdf(UDFINDEX_CASEID_ALIAS);
	}
	public String getCaseVersionAlias () throws SciException
	{
		return getUdf(UDFINDEX_CASEVERSION_ALIAS);
	}
	
}
