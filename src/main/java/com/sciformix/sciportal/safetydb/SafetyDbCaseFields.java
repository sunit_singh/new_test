package com.sciformix.sciportal.safetydb;

public class SafetyDbCaseFields {
	/**
	 * This will store the sequence id
	 * of the field.
	 */
	private int
		caseFieldSeqId
	;	
	
	/**
	 * This will store the name of the 
	 * case field.
	 */
	private String
		caseFieldName
	;
	/**
	 * This will store the type of the 
	 * case field.
	 */
	private SafetyDBCaseFieldCategoryType
		caseFieldType
	;
	/**
	 * This will store the group or tab
	 * name of the case field.
	 */
	private String
		caseFieldTab
	;
	/**
	 * This will store the hint associated 
	 * with the case field.
	 */
	private String
		caseFieldHint
	;
	/**
	 * This is constant for the same
	 * field name and group combination.
	 */
	private String
		caseFieldMnemonic
	;	
	
	/**
	 * Constructor to create object with all
	 * the parameters.
	 * @param fieldName	Case field name.
	 * @param fieldType	Case field type.
	 * @param fieldTab	Case field tab name.
	 * @param fieldHint Case field hint.
	 */
	public SafetyDbCaseFields(
		String 					caseFieldName
	, 	SafetyDBCaseFieldCategoryType casFieldType
	, 	String 					caseFieldTab
	,	String 					caseFieldHint
	,	String					caseFieldMnemonic
	) {
		super();
		this.caseFieldName	=	caseFieldName;
		this.caseFieldType	=	casFieldType;
		this.caseFieldTab	=	caseFieldTab;
		this.caseFieldHint	=	caseFieldHint;
		this.caseFieldMnemonic	=	caseFieldMnemonic;
	}
	
	
	/**
	 * Constructor to create object with all
	 * the parameters.
	 * @param fieldName	Case field name.
	 * @param fieldType	Case field type.
	 * @param fieldTab	Case field tab name.
	 * @param fieldHint Case field hint.
	 */
	public SafetyDbCaseFields(
		int						caseFieldSeqId	
	,	String 					caseFieldName
	, 	SafetyDBCaseFieldCategoryType casFieldType
	, 	String 					caseFieldTab
	,	String 					caseFieldHint
	,	String					caseFieldMnemonic
	) {
		super();
		this.caseFieldSeqId	=	caseFieldSeqId;
		this.caseFieldName	=	caseFieldName;
		this.caseFieldType	=	casFieldType;
		this.caseFieldTab	=	caseFieldTab;
		this.caseFieldHint	=	caseFieldHint;
		this.caseFieldMnemonic	=	caseFieldMnemonic;
	}
	
	public String getCaseFieldName() {
		return caseFieldName;
	}
	public SafetyDBCaseFieldCategoryType getCaseFieldType() {
		return caseFieldType;
	}
	public String getCaseFieldTab() {
		return caseFieldTab;
	}
	public String getCaseFieldHint() {
		return caseFieldHint;
	}
	public int getCaseFieldSeqId() {
		return caseFieldSeqId;
	}
	public String getCaseFieldMnemonic() {
		return caseFieldMnemonic;
	}
}
