package com.sciformix.sciportal.safetydb;

public enum SafetyDBCaseFieldCategoryType {
	CAT1(1), CAT2(2), CAT3(3), NOCAT(0);
	
	private int catCode;
	
	SafetyDBCaseFieldCategoryType (
		int p_catcode
	) {
		this.catCode	=	p_catcode;
	}
	
	public int getCatCode () {
		return catCode;
	}
	
	public static SafetyDBCaseFieldCategoryType toEnum(int p_catcode)
	{	
		for (SafetyDBCaseFieldCategoryType l_casefieldcattype : SafetyDBCaseFieldCategoryType.values())
		{
			if (p_catcode == l_casefieldcattype.getCatCode())
			{
				return l_casefieldcattype;
			}
		}
		throw new IllegalArgumentException("RecordState: Illegal enum value - " + p_catcode);
	}
}
