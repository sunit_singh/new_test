package com.sciformix.sciportal.safetydb;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.CommonAuditHelper;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.DataValidationUtils;
import com.sciformix.commons.utils.OfficeUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.commons.utils.WorkbookWorkSheetWrapper;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.config.ConfigTplObj;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class SafetyDbConfigurationHelper {
	
	private static final Logger
		log	=	LoggerFactory.getLogger(SafetyDbConfigurationHelper.class)
	;
	
	public static boolean isObjectNameUnique (
		int		p_projectid
	,	String	p_proposedctoname
	) throws SciServiceException {
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			return	ConfigTplObjHomeImpl.isConfigTplObjNameUnique(
						connection
					, 	p_projectid
					, 	ConfigTplObjType.SCF
					, 	p_proposedctoname
					);
		}
		catch (SciException sciExcep)
		{
			log.error("Error checking config template object name uniqueness", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_CHECKING_TEMPLATE_NAME_UNIQUE, sciExcep);
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error checking config template object name uniqueness", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_CHECKING_TEMPLATE_NAME_UNIQUE, dbExcep);
		}
	}
	
	/**
	 * 
	 * @param p_uploadedfile
	 * @param p_projectid
	 * @param p_templatename
	 * @param p_usersession
	 * @param p_safetydbname
	 * @param p_safetydbversion
	 * @param p_caseidalias
	 * @param p_caseversionalias
	 * @return
	 * @throws SciServiceException
	 */
	public static SafetyDbCaseFieldsTemplate saveSafetyCaseFileData (
		UploadedFileWrapper	p_uploadedfile
	,	int					p_projectid
	,	String				p_templatename
	,	UserSession			p_usersession
	,	String				p_safetydbname
	,	String				p_safetydbversion
	,	String				p_caseidalias
	,	String				p_caseversionalias
	) throws SciServiceException {
		WorkbookWorkSheetWrapper		l_wbwrapper					=	null;
		List<SafetyDbCaseFields>		l_safetydbcasefieldlist		=	null;
		String 							l_tabname					=	null,
										l_temptabname				=	null,
										l_fieldname					=	null,
										l_fieldhint					=	null,
										l_fieldmnemonic				=	null;
		SafetyDBCaseFieldCategoryType	l_fieldtype					=	null;
		SafetyDbCaseFields				l_safetydbcasefield			=	null;
		SafetyDbCaseFieldsTemplate		l_safetydbcasefieldtemplate	=	null;
		AuditInfoDb						l_auditinfo					=	null;
		
		try {
			l_wbwrapper		=	OfficeUtils.parseWorkbook(
									p_uploadedfile.getStagedFilePath()
								, 	true
								, 	0
								,	null
								,	null
								);
			try (DbConnection connection = DbConnectionManager.getConnection()) {
				try {
					connection.startTransaction();
					l_safetydbcasefieldtemplate	=	new SafetyDbCaseFieldsTemplate(
														p_safetydbname
													,	p_safetydbversion
													,	p_caseidalias
													,	p_caseversionalias
													);
					l_safetydbcasefieldtemplate.setProjectId(p_projectid);
					l_safetydbcasefieldtemplate.setTemplateName(p_templatename);
					l_safetydbcasefieldtemplate.setState(RecordState.UNDER_CONFIGURATION);
					l_safetydbcasefieldtemplate.setCreationMode(CreationMode.SCRATCH);
					
					if (l_wbwrapper.getNumberOfDataRows() > 0) {
						l_safetydbcasefieldlist	=	new ArrayList<>();
						for (int l_i = 0; l_i < l_wbwrapper.getNumberOfDataRows(); l_i++) {
							l_tabname	=	(String) l_wbwrapper.getCellValue("Field Group(Tabs)",l_i);
							if (l_i == 0 && StringUtils.isNullOrEmpty(l_tabname)) {
								//TODO: throw exception
							}
							if (!StringUtils.isNullOrEmpty(l_tabname)) {
								l_temptabname	=	l_tabname;
							} 
							l_fieldname		=	(String) l_wbwrapper.getCellValue("Exact Field Display Name",l_i);
							l_fieldmnemonic	=	(String) l_wbwrapper.getCellValue("Field Mnemonic",l_i);
							if (StringUtils.isNullOrEmpty(l_fieldmnemonic)) {
								log.error(
									"Mnemonic error"
								);
								throw new SciServiceException(
									ServiceErrorCodes.Common.ERROR_MENMONIC
								);
							}
							if (!DataValidationUtils.isMnemonicValid(l_fieldmnemonic)) {
								log.error(
									"Mnemonic error"
								);
								throw new SciServiceException(
									ServiceErrorCodes.Common.ERROR_MENMONIC
								);
							}
							l_fieldtype		=	SafetyDBCaseFieldCategoryType.toEnum(Integer.parseInt((String) l_wbwrapper.getCellValue("Field Type",l_i)));
							l_fieldhint		=	(String) l_wbwrapper.getCellValue("Field Hint",l_i);
							 
							l_safetydbcasefield	=	new SafetyDbCaseFields (
														l_fieldname
													,	l_fieldtype
													,	l_temptabname
													,	l_fieldhint
													,	l_fieldmnemonic.toUpperCase()
													);
							l_safetydbcasefieldlist.add(l_safetydbcasefield);
						}
					} else {
						log.error(
							"Empty file uploaded"
						);
						throw new SciServiceException(
							ServiceErrorCodes.Common.ERROR_FILE_EMPTY
						);
					}
					ConfigTplObjHomeImpl.saveConfigTplObj(
						connection
					, 	l_safetydbcasefieldtemplate
					,	p_usersession.getUserInfo()
					);
					SafetyDbConfigurationHomeImpl.saveSafetyCaseFileData(
						connection
					, 	l_safetydbcasefieldlist
					, 	l_safetydbcasefieldtemplate.getSequenceId()
					);
					l_safetydbcasefieldtemplate.setCaseFieldList(
						l_safetydbcasefieldlist
					);	
					l_auditinfo	=	AuditHome.createRecordInsertStub(
										p_usersession
									, 	"APPSETTINGS-PORTAL-APP"
									, 	"Template Created"
									,	p_projectid
									);
					l_auditinfo.addPayload(
						CommonAuditHelper.AuditAttributes.TEMPLATE_ID
					, 	l_safetydbcasefieldtemplate.getSequenceId()
					);
					l_auditinfo.addPayload(
						CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
					, 	l_safetydbcasefieldtemplate.getTemplateName()
					);
					AuditHome.savePopulatedStub(connection, l_auditinfo);
					connection.commitTransaction();
				} catch (SciServiceException servExcep) {
					connection.rollbackTransaction();
					log.error(
						"Error Saving master data of qr "
					,	servExcep
					);
					throw servExcep;
				} catch(SciException sciExcep) {
					connection.rollbackTransaction();
					log.error(
						"Error Saving master data of qr "
					,	sciExcep
					);
					throw new SciServiceException(
						ServiceErrorCodes.QR.ERROR_SAVING_MASTER_DATA
					, 	sciExcep
					);
				} 	
			} catch (SciDbException dbExcep) {
				log.error(
					"Error Saving master data of qr "
				,	dbExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_SAVING_MASTER_DATA
				, 	dbExcep
				);
			}
		} catch (SciException sciExcep) {
			log.error(
				"Saving master data of qr "
			,	sciExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_SAVING_MASTER_DATA
			, 	sciExcep
			);
		}
		return l_safetydbcasefieldtemplate;
	}
	
	/**
	 * This will fetch you all the templates that
	 * belong to the passed project id and template
	 * type.
	 * @param p_projectid	The id of the project
	 * 						to which user belongs to.
	 * @param p_ctotype		The config template object
	 * 						type to which the template
	 * 						needs to be fetched.
	 * @return				The list of templates.
	 * @throws SciServiceException
	 */
	public static List<SafetyDbCaseFieldsTemplate> getTemplates (
		int	p_projectid
	) throws SciServiceException {
		
		List<SafetyDbCaseFieldsTemplate>	l_safetydbcasefieldtemplatelist	=	null;
		List<ConfigTplObj>					l_configtplobjlist				=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_configtplobjlist	=	ConfigTplObjHomeImpl.getListOfTemplates(
											connection
										, 	p_projectid
										, 	ConfigTplObjType.SCF
										);
				if (l_configtplobjlist.size() > 0) {
					l_safetydbcasefieldtemplatelist	=	new ArrayList<> ();
					for (ConfigTplObj l_template : l_configtplobjlist) {
						l_safetydbcasefieldtemplatelist.add((SafetyDbCaseFieldsTemplate)l_template);
					}
				}
				
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error fetching templates"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATES
				, 	sciExcep
				);
			} 	
		} catch (SciDbException dbExcep) {
			log.error(
				"Error fetching templates"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATES
			, 	dbExcep
			);
		}	
		return l_safetydbcasefieldtemplatelist;
	}
	
	public static SafetyDbCaseFieldsTemplate getTemplateDetails (
			int p_templateseqid
		) throws SciServiceException {
		SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate	=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			l_safetydbcasefieldtemplate = getTemplateDetails(connection, p_templateseqid);
		}
		catch (SciDbException dbExcep)
		{
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
			, 	dbExcep
			);
		}	
		return l_safetydbcasefieldtemplate;
	}
	
	/**
	 * 
	 * @param p_templateseqid
	 * @return
	 * @throws SciServiceException
	 */
	public static SafetyDbCaseFieldsTemplate getTemplateDetails (
			DbConnection connection,
		int p_templateseqid
	) throws SciDbException, SciServiceException {
		SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate	=	null;
		
		try {
			connection.startTransaction();
			l_safetydbcasefieldtemplate	=	(SafetyDbCaseFieldsTemplate) ConfigTplObjHomeImpl.getTemplateDetails(
												connection
											, 	p_templateseqid
											);
			if (l_safetydbcasefieldtemplate != null) {
				l_safetydbcasefieldtemplate.setCaseFieldList(
					SafetyDbConfigurationHelper.getSafetyDbCaseFileData(
							connection,
						l_safetydbcasefieldtemplate.getSequenceId()
					)
				);
			}
			connection.commitTransaction();
		} catch(SciException sciExcep) {
			connection.rollbackTransaction();
			log.error(
				"Error in fetching template details"
			,	sciExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
			, 	sciExcep
			);
		} catch(SciServiceException servExcep) {
			connection.rollbackTransaction();
			log.error(
				"Error in fetching template details"
			,	servExcep
			);
			//Swallowed the exception thrown in the above code to set error as fetching template details
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
			, 	servExcep
			);
		}	
		return l_safetydbcasefieldtemplate;
	}
	/**
	 * 
	 * @param p_projectid
	 * @return
	 * @throws SciServiceException
	 */
	public static SafetyDbCaseFieldsTemplate getActiveTemplateDetails (
		int	p_projectid	
	) throws SciServiceException {
		SafetyDbCaseFieldsTemplate	l_safetydbcasefieldtemplate	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_safetydbcasefieldtemplate	=	(SafetyDbCaseFieldsTemplate)ConfigTplObjHomeImpl.getActiveTemplateDetails(
													connection
												, 	ConfigTplObjType.SCF
												,	p_projectid
												);
				if (l_safetydbcasefieldtemplate != null) {
					l_safetydbcasefieldtemplate.setCaseFieldList(
						SafetyDbConfigurationHelper.getSafetyDbCaseFileData(
								connection,
							l_safetydbcasefieldtemplate.getSequenceId()
						)
					);
				} 
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error in fetching template details"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
				, 	sciExcep
				);
			} 	
		} catch (SciDbException dbExcep) {
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
			, 	dbExcep
			);
		}	
		return l_safetydbcasefieldtemplate;
	}
	/**
	 * 
	 * @param p_templateseqid
	 * @return
	 * @throws SciServiceException
	 */
	public static List<SafetyDbCaseFields> getSafetyDbCaseFileData (
			DbConnection connection,
		int	p_templateseqid
	) throws SciServiceException {
		List<SafetyDbCaseFields>	l_safetydbcasefieldlist	=	null;
		try 
		{
			l_safetydbcasefieldlist	=	SafetyDbConfigurationHomeImpl.getSafetyCaseFileData(connection, p_templateseqid);
		}
		catch (SciException sciExcep)
		{
			log.error("Error fetching the template child detail from DB", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_CHILD_DETAILS, sciExcep);
		}
		return l_safetydbcasefieldlist;
	}
	
	/**
	 * This is used to update the state of the template from under
	 * configuration state to active state.
	 * @param p_usersession			Session information of the user
	 * @param p_eNewState			The state to which it needs to
	 * 								be updated.
	 * @param p_configtemplateobj	The config template object whose
	 * 								state needs to be updated.
	 * @throws SciServiceException
	 */
	public static SafetyDbCaseFieldsTemplate updateTemplateState(
		UserSession	p_usersession	
	,	RecordState p_eNewState
	,	int			p_templateseqid
	,	int			p_projectid
	) throws SciServiceException {
		AuditInfoDb					l_auditinfo			=	null,
									l_auditinfoactive	=	null;
		SafetyDbCaseFieldsTemplate	l_scft				=	null,
									l_scftactive		=	null;
		
		l_scft	=	getTemplateDetails(
						p_templateseqid
					);
		if (l_scft.getState() == p_eNewState) {
			return l_scft;
		}
		if (!RecordState.isCorrectWorkflowState(l_scft.getState(), p_eNewState)) {
			return l_scft;
		}
		l_scftactive	=	getActiveTemplateDetails(p_projectid);
		if (l_scftactive != null) {
			l_auditinfoactive	=	AuditHome.createRecordUpdateStub(
										p_usersession
									, 	"APPSETTINGS-PORTAL-APP"
									, 	"Template Ruleset state changed"
									,	p_projectid
									);
			l_auditinfoactive.addPayload(
				CommonAuditHelper.AuditAttributes.TEMPLATE_ID
			, 	l_scftactive.getSequenceId()
			);
			l_auditinfoactive.addPayload(
				CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
			, 	l_scftactive.getTemplateName()
			);
			l_auditinfoactive.addDetails(
				CommonAuditHelper.AuditAttributes.TEMPLATE_STATE
			, 	l_scftactive.getState().displayName()
			, 	RecordState.DEACTIVATED.displayName()
			);
		}
		
		l_auditinfo	=	AuditHome.createRecordUpdateStub(
							p_usersession
						, 	"APPSETTINGS-PORTAL-APP"
						, 	"Template Ruleset state changed"
						,	p_projectid
						);
		l_auditinfo.addPayload(
			CommonAuditHelper.AuditAttributes.TEMPLATE_ID
		, 	l_scft.getSequenceId()
		);
		l_auditinfo.addPayload(
			CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
		, 	l_scft.getTemplateName()
		);
		l_auditinfo.addDetails(
			CommonAuditHelper.AuditAttributes.TEMPLATE_STATE
		, 	l_scft.getState().displayName()
		, 	p_eNewState.displayName()
		);
		l_scft.setState(p_eNewState);
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				
				if (l_scftactive != null) {
					ConfigTplObjHomeImpl.updateConfigTplObjState(
						connection
					, 	l_scftactive.getSequenceId()
					, 	RecordState.DEACTIVATED
					, 	p_usersession.getUserInfo()
					);
				}
				
				if (l_scftactive != null) {
					AuditHome.savePopulatedStub(connection, l_auditinfoactive);
				}
				
				ConfigTplObjHomeImpl.updateConfigTplObjState(
					connection
				,	l_scft.getSequenceId()
				, 	p_eNewState
				, 	p_usersession.getUserInfo()
				);
				AuditHome.savePopulatedStub(connection, l_auditinfo);
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error updating template ruleset state", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_UPDATING_TEMPLATE_STATE, l_scft.getTemplateName(), sciExcep);
			}
		} catch (SciDbException dbExcep){
			log.error("Error updating template ruleset state", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_UPDATING_TEMPLATE_STATE, dbExcep);
		}
		return l_scft;
	}
}
