package com.sciformix.sciportal.safetydb;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

public class SafetyDbConfigurationHomeImpl {
	private static final Logger 
		log	=	LoggerFactory.getLogger(SafetyDbConfigurationHomeImpl.class)
	;
	
	/**
	 * 
	 * @param p_connection
	 * @param p_casefieldlist
	 * @param p_parentseqid
	 * @throws SciException
	 */
	public static void saveSafetyCaseFileData (
		DbConnection				p_connection
	,	List<SafetyDbCaseFields>	p_casefieldlist
	,	int							p_parentseqid
	) throws SciException {
		
		DbQueryInsert	l_insertquery	=	null;
		try {
			p_connection.startTransaction();
			for (SafetyDbCaseFields l_casefield : p_casefieldlist) {
				l_insertquery = DbQueryUtils.createInsertQuery(SafetyDbConfigurationDbSchema.SAFETYCASEFIELD);
				l_insertquery.addInsertParameter(
					DbQueryHome.generateSequenceValueForTablePk(
						SafetyDbConfigurationDbSchema.SAFETYCASEFIELD
					)
				);
				l_insertquery.addInsertParameter(p_parentseqid);
				l_insertquery.addInsertParameter(l_casefield.getCaseFieldName());
				l_insertquery.addInsertParameter(l_casefield.getCaseFieldHint());
				l_insertquery.addInsertParameter(l_casefield.getCaseFieldType().getCatCode());
				l_insertquery.addInsertParameter(l_casefield.getCaseFieldTab());
				l_insertquery.addInsertParameter(l_casefield.getCaseFieldMnemonic());
				p_connection.executeQuery(l_insertquery);
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving master data of qr into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error saving master data of qr into DB"
			,	dbExcep
			);
		}
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_templateseqid
	 * @return
	 * @throws SciException
	 */
	public static List<SafetyDbCaseFields> getSafetyCaseFileData (
		DbConnection	p_connection
	, 	int				p_templateseqid
	) throws SciException {
		DbQuerySelect				l_selectquery	=	null;
		List<SafetyDbCaseFields>	l_casefieldlist	=	null;
		try {
			p_connection.startTransaction();
			
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									SafetyDbConfigurationDbSchema.SAFETYCASEFIELD
								, 	SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.CTOSEQID.bindClause()
								,	SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.FIELDSEQID.name()
								);
			
			l_selectquery.addWhereClauseParameter(p_templateseqid);
			
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_casefieldlist	=	new ArrayList<>();
					while (l_rs.next()) {
						l_casefieldlist.add(
							new SafetyDbCaseFields(
								l_rs.readInt(SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.FIELDSEQID)	
							,	l_rs.readString(SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.FIELDNAME)
							, 	SafetyDBCaseFieldCategoryType.toEnum(l_rs.readInt(SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.FIELDTYPE))
							, 	l_rs.readString(SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.FIELDGROUP)
							, 	l_rs.readString(SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.FIELDHINT)
							, 	l_rs.readString(SafetyDbConfigurationDbSchema.SAFETYCASEFIELD.FIELDMNEMONIC)
							)
						);
					}
				} else {
					
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching the template child detail from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching the template child detail from DB"
			,	dbExcep
			);
		}	
		return l_casefieldlist;
	}
}
