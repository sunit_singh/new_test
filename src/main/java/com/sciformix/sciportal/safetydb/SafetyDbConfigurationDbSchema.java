package com.sciformix.sciportal.safetydb;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class SafetyDbConfigurationDbSchema {
	public static final SafetyCaseField	
		SAFETYCASEFIELD	=	new SafetyCaseField();

	public static class SafetyCaseField extends DbTable {
		
		public DbColumn	FIELDSEQID			=	null;
		public DbColumn	CTOSEQID			=	null;
		public DbColumn	FIELDNAME			=	null;
		public DbColumn	FIELDHINT			=	null;
		public DbColumn	FIELDTYPE			=	null;
		public DbColumn	FIELDGROUP      	=	null;
		public DbColumn	FIELDMNEMONIC		=	null;
	
		public SafetyCaseField() {
			super("CMN_SAFETYDBCASEFIELDDATA");
			setPkSequenceName("CMN_SAFETYDBCASEFIELDDATA_SEQ");
			
			FIELDSEQID			=	addColumn("FIELDSEQID");
			CTOSEQID			=	addColumn("CTOSEQID");
			FIELDNAME			=	addColumn("FIELDNAME");
			FIELDHINT			=	addColumn("FIELDHINT");
			FIELDTYPE        	=	addColumn("FIELDTYPE");
			FIELDGROUP			=	addColumn("FIELDGROUP");
			FIELDMNEMONIC		=	addColumn("FIELDMNEMONIC");
		}
	}
}
