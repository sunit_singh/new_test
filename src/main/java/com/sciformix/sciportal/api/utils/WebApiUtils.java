package com.sciformix.sciportal.api.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.api.model.SciApiResponse;
import com.sciformix.sciportal.api.model.ServiceApiContext;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.system.DisplayMessageHelper;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;

public class WebApiUtils {

	private static final Logger log = LoggerFactory.getLogger(WebApiUtils.class);
	
	private static final String KEY_PROJ_TOOLNAME = "WEBAPI-CLIENT-TOOL.tool.CPAT.permissibletoolnames";
	private static final String KEY_PROJ_TOOLVERSION = "WEBAPI-CLIENT-TOOL.tool.CPAT.permissibletoolversions";
	private static final String KEY_PROJ_OSVERSION = "WEBAPI-CLIENT-TOOL.tool.CPAT.permissibleosversions";
	private static final String KEY_PROJ_JAVAVERSION = "WEBAPI-CLIENT-TOOL.tool.CPAT.permissiblejavaversions";
	private static final String KEY_PROJ_CLIENTIQHASH = "WEBAPI-CLIENT-TOOL.tool.CPAT.clienthash";
	
	public static boolean areWebApisEnabled() {
		String webApiEnabled = ConfigHome.getSysConfigSetting("WEBAPIS.enabled");
		if (webApiEnabled.equalsIgnoreCase("false") || webApiEnabled==null) {
			return false;
		} else {
			return true;
		}
	}

	public static String getRequestParameter(JSONObject jsonRequest, String p_sParameterName) {
		return jsonRequest.optString(p_sParameterName, StringConstants.EMPTY);
	}
	
	public static JSONArray getRequestArrayParameter(JSONObject jsonRequest, String p_sParameterName) throws JSONException{
		return jsonRequest.getJSONArray(p_sParameterName);
	}

	public static Date getRequestDateParameter(JSONObject jsonRequest, String p_sParameterName) throws ParseException {
		return getFormattedDate(jsonRequest.optString(p_sParameterName, StringConstants.EMPTY));
	}

	public static int getRequestIntParameter(JSONObject jsonRequest, String p_sParameterName)
			throws NumberFormatException {
		return Integer.parseInt(jsonRequest.optString(p_sParameterName, "-1"));
	}
	
	public static int getRequestProjSeqId(JSONObject jsonRequest, String p_sParameterName)
			throws NumberFormatException {
		return Integer.parseInt(jsonRequest.optString(p_sParameterName, ""));
	}

	public static Date getFormattedDate(String dateToFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		Date formattedDate = null;
		try {
			formattedDate = formatter.parse(dateToFormat);
		} catch (ParseException e) {
			log.error("Error parsing date.");
			return null;
		}
		return formattedDate;
	}
	
	public static String getSystemDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		String timestamp = null;
		sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		timestamp = sdf.format(ts);
		return timestamp;
	}

	public static void displayLogsForClientParams(Map<String, String> clientParamsMap,
			Map<String, String> systemPayloadMap) {
		log.info("ClientParams - " + clientParamsMap.toString() + " SystemPayloadParams - " + systemPayloadMap.toString());
	}
	
	public static byte[] getBytesFromFile(File file) throws IOException{
		byte[] convertedFileToByteArr = FileUtils.readFileToByteArray(file);
		return convertedFileToByteArr;
	}
	
	public static String getEncodedStringFromBytearr(byte [] byteArr){
		Base64.Encoder encoder = Base64.getEncoder();
		String encodedString = encoder.encodeToString(byteArr);
		return encodedString;
	}
	
	public static byte[] getByteFromEncodedString(String encodedText) {
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] decodedByteArray = decoder.decode(encodedText);
		return decodedByteArray;
	}
	
	public static String getJsonStringFromMap(Map<String, String> inputMap) throws IOException {
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		jsonString = objectMapper.writeValueAsString(inputMap);
		Object json = objectMapper.readValue(jsonString, Object.class);
		jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		return jsonString;
	}
	
	public static boolean performToolValidation(SciApiResponse serviceResponse, int projectSeqId, String toolName, String toolVersion){
		boolean isToolNameValid = Boolean.FALSE;
		boolean isToolVersionValid = Boolean.FALSE;
		
		String [] permissibleToolNameValues = getValueArr(KEY_PROJ_TOOLNAME, projectSeqId);
		String [] permissibleToolVersionValues = getValueArr(KEY_PROJ_TOOLVERSION, projectSeqId);
		
		// ToolName Check
		if(permissibleToolNameValues!=null){
			for(String value:permissibleToolNameValues){
	    		if(toolName.equalsIgnoreCase(value)){
	    			isToolNameValid = Boolean.TRUE;
	    		}
	    	}
		}
		
		if(!isToolNameValid){
			serviceResponse.setCode(WebErrorCodes.WebServices.UNALLOWED_TOOLNAME.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.UNALLOWED_TOOLNAME));
			return false;
		}
		
		// Tool Version Check
		if(permissibleToolVersionValues!=null){
			for(String value:permissibleToolVersionValues){
	    		if(toolVersion.equalsIgnoreCase(value)){
	    			isToolVersionValid = Boolean.TRUE;
	    		}
	    	}
		}
		
		if(!isToolVersionValid){
			serviceResponse.setCode(WebErrorCodes.WebServices.UNALLOWED_TOOLVERSION.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.UNALLOWED_TOOLVERSION));
			return false;
		}
		
		return true;
	}
	
	public static boolean verifyRuntimeIQParams(SciApiResponse serviceResponse, int projectSeqId, Map<String, String> systemPayloadMap){
		boolean isOsVerified = Boolean.FALSE;
		boolean isJdkVerified = Boolean.FALSE;
		
		String osVersion = systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_OSVERSION);
		String jdkVersion = systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_JDKVERSION);
		
		String [] permissibleOsValues = getValueArr(KEY_PROJ_OSVERSION, projectSeqId);
		String [] permissibleJdkValues = getValueArr(KEY_PROJ_JAVAVERSION, projectSeqId);
		
		// OS Check
		if(permissibleOsValues!=null){
			for(String value:permissibleOsValues){
	    		if(osVersion.equalsIgnoreCase(value)){
	    			isOsVerified = Boolean.TRUE;
	    		}
	    	}
		}
		
		if(!isOsVerified){
			serviceResponse.setCode(WebErrorCodes.WebServices.RUNTIME_IQ_FAILED_OS.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.RUNTIME_IQ_FAILED_OS));
			return false;
		}
		
		// JAVA Check
		if(permissibleJdkValues!=null){
			for(String value:permissibleJdkValues){
	    		if(jdkVersion.startsWith(value)){
	    			isJdkVerified = Boolean.TRUE;
	    		}
	    	}
		}
		
		if(!isJdkVerified){
			serviceResponse.setCode(WebErrorCodes.WebServices.RUNTIME_IQ_FAILED_JAVA.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.RUNTIME_IQ_FAILED_JAVA));
			return false;
		}
		
		return true;
	}
	
	private static String[] getValueArr(String key, int projectSeqId){
		String[] valueArr = null;
		String value = null;
		
		value = ConfigHome.getPrjConfigSettings(key, projectSeqId);
		if(value != null){
			valueArr = StringUtils.splitOnComma(value);
		}
		
		return valueArr;
	}
	
	public static boolean verifyClientIQ(SciApiResponse serviceResponse, int projectSeqId, String requestClientIqHash){
		String projClientIqHash = null;
		
		projClientIqHash = ConfigHome.getPrjConfigSettings(KEY_PROJ_CLIENTIQHASH, projectSeqId);
		
		if(projClientIqHash == null || projClientIqHash.equals(requestClientIqHash)){
			return true;
		} else {
			serviceResponse.setCode(WebErrorCodes.WebServices.UNALLOWED_CLIENT_IQHASH.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.UNALLOWED_CLIENT_IQHASH));
			return false;
		}
		
	}
	
	public static boolean checkProjectAgainstProjectId(HttpServletRequest request, SciApiResponse serviceResponse,
			String userId, int projectSeqId) throws SciServiceException {

		UserInfo oUserInfo = null;
		UserSession oUserSession = null;
		List<ObjectIdPair<Integer, String>> listProjectsSession = null;
		try {
			oUserInfo = getUserInfo(userId);

		} catch (SciServiceException e) {
			log.error("Error getting User Info : " + e.getMessage());
		}
		oUserSession = WebUtils.createWebUserSession(request, oUserInfo);

		listProjectsSession = oUserSession.getAuthorizedProjectIds();

		if (!listProjectsSession.isEmpty()) {
			for (ObjectIdPair<Integer, String> oProject : listProjectsSession) {
				if (oProject.getObjectSeqId() == projectSeqId) {
					return true;
				}
			}
		}

		//Project allocation not present
		serviceResponse.setCode(WebErrorCodes.WebServices.PROJECT_NOT_EXISTS.errorCode());
		serviceResponse
				.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.PROJECT_NOT_EXISTS));

		return false;
	}
	
	private static UserInfo getUserInfo(String userId) throws SciServiceException {
		UserInfo oUserInfo = UserHome.retrieveUser(userId, UserHome.UserPurpose.DATA_SEARCH);
		return oUserInfo;
	}
	
	public static boolean validateScriptTag(String fieldvalue, String regex)
	{
		Pattern	oPattern	=	Pattern.compile(regex);
		Matcher	oMatcher	=	oPattern.matcher(fieldvalue.toLowerCase());
		return oMatcher.find();
	}
	
}
