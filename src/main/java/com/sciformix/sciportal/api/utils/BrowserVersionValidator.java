package com.sciformix.sciportal.api.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.WebErrorCodes;
import com.sciformix.sciportal.api.model.SciApiResponse;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.system.DisplayMessageHelper;

public class BrowserVersionValidator {
	
	private static Logger logger = LoggerFactory.getLogger(BrowserVersionValidator.class);

	private static String ASTERIX = "*";

	private static String KEY_CHROME_ALLOWED = "WEBAPI-CLIENT-TOOL.tool.CPAT.allowedchrome";
	private static String KEY_CHROME_DISALLOWED = "WEBAPI-CLIENT-TOOL.tool.CPAT.disallowedchrome";
	private static String KEY_IE_ALLOWED = "WEBAPI-CLIENT-TOOL.tool.CPAT.allowedie";
	private static String KEY_IE_DISALLOWED = "WEBAPI-CLIENT-TOOL.tool.CPAT.disallowedie";

	public static String BROWSER_CHROME = "CHROME";
	public static String BROWSER_IE = "IE";

	public static enum STATUS {
		ALLOWED, DISALLOWED, NOSUPPORT;
	}

	public static boolean checkBrowserVersionChrome(SciApiResponse serviceResponse, String incomingVersion,
			int projectSeqId) {
		STATUS eStatus = null;
		String serverAllowedVersions = null;
		String serverDisallowedVersions = null;

		serverAllowedVersions = ConfigHome.getPrjConfigSettings(KEY_CHROME_ALLOWED, projectSeqId);
		serverDisallowedVersions = ConfigHome.getPrjConfigSettings(KEY_CHROME_DISALLOWED, projectSeqId);

		if (!incomingVersion.equals("")) {
			eStatus = validateBrowserVersion(incomingVersion, serverAllowedVersions, serverDisallowedVersions);
		}

		if (eStatus != null) {
			if (eStatus.equals(STATUS.DISALLOWED)) {
				serviceResponse.setCode(WebErrorCodes.WebServices.BROWSER_CHROME_DISALLOWED.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.BROWSER_CHROME_DISALLOWED));
				return false;
			} else if (eStatus.equals(STATUS.NOSUPPORT)) {
				serviceResponse.setCode(WebErrorCodes.WebServices.BROWSER_CHROME_NO_SUPPORT.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.BROWSER_CHROME_NO_SUPPORT));
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}

	}

	public static boolean checkBrowserVersionIE(SciApiResponse serviceResponse, String incomingVersion,
			int projectSeqId) {
		STATUS eStatus = null;
		String serverAllowedVersions = null;
		String serverDisallowedVersions = null;

		serverAllowedVersions = ConfigHome.getPrjConfigSettings(KEY_IE_ALLOWED, projectSeqId);
		serverDisallowedVersions = ConfigHome.getPrjConfigSettings(KEY_IE_DISALLOWED, projectSeqId);

		if (!incomingVersion.equals("")) {
			eStatus = validateBrowserVersion(incomingVersion, serverAllowedVersions, serverDisallowedVersions);
		}

		if (eStatus != null) {
			if (eStatus.equals(STATUS.DISALLOWED)) {
				serviceResponse.setCode(WebErrorCodes.WebServices.BROWSER_IE_DISALLOWED.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.BROWSER_IE_DISALLOWED));
				return false;
			} else if (eStatus.equals(STATUS.NOSUPPORT)) {
				serviceResponse.setCode(WebErrorCodes.WebServices.BROWSER_IE_NO_SUPPORT.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.BROWSER_IE_NO_SUPPORT));
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}

	}

	private static STATUS validateBrowserVersion(String p_sIncomingVersion, String p_sServerAllowedVersions,
			String p_sServerDisallowedVersions) {
		Boolean bVersionIsAllowed = Boolean.FALSE;
		Boolean bVersionIsExplicitlyDisallowed = Boolean.FALSE;
		STATUS eStatus = null;

		String[] sarrAllowedVersions = null;
		String[] sarrDisallowedVersions = null;

		if (p_sServerAllowedVersions != null) {
			sarrAllowedVersions = p_sServerAllowedVersions.split("\\,");
		}

		if (p_sServerDisallowedVersions != null) {
			sarrDisallowedVersions = p_sServerDisallowedVersions.split("\\,");
		}

		// Allowed Checks
		if (sarrAllowedVersions != null) {
			for (String sAllowedVersion : sarrAllowedVersions) {
				if (sAllowedVersion.trim().length() == 0) {
					continue;
				}

				if (allowedCheck(p_sIncomingVersion, sAllowedVersion)) {
					bVersionIsAllowed = Boolean.TRUE;
					break;
				}
			}
		} else {
			bVersionIsAllowed = Boolean.TRUE;
		}

		// Disallowed Checks
		if (sarrDisallowedVersions != null) {
			for (String sDisallowedVersion : sarrDisallowedVersions) {
				if (sDisallowedVersion.trim().length() == 0) {
					continue;
				}

				if (disAllowedCheck(p_sIncomingVersion, sDisallowedVersion)) {
					bVersionIsExplicitlyDisallowed = true;
				}
			}
		} else {
			bVersionIsExplicitlyDisallowed = false;
		}

		if (bVersionIsAllowed && !bVersionIsExplicitlyDisallowed) {
			eStatus = STATUS.ALLOWED;
		} else if (bVersionIsExplicitlyDisallowed) {
			eStatus = STATUS.DISALLOWED;
		} else {
			eStatus = STATUS.NOSUPPORT;
		}

		return eStatus;
	}

	private static String[] getVersionComponents(String p_sVersionString) {
		return p_sVersionString.split("\\.");
	}

	private static boolean allowedCheck(String p_sIncomingVersion, String p_sAllowedVersion) {
		if (p_sIncomingVersion.equalsIgnoreCase(p_sAllowedVersion)) {
			return true;
		}

		String[] sarrIncomingVersionComponents = getVersionComponents(p_sIncomingVersion);
		String[] allowedVerArr = getVersionComponents(p_sAllowedVersion);

		int position = allowedVerArr.length;
		for (int i = 0; i < allowedVerArr.length; i++) {
			if (allowedVerArr[i].equals(ASTERIX)) {
				position = i;
				break;
			}
		}

		try{
			for (int i = 0; i < position; i++) {
				if (Integer.parseInt(sarrIncomingVersionComponents[i]) == Integer.parseInt(allowedVerArr[i])) {
					continue;
				} else if (Integer.parseInt(sarrIncomingVersionComponents[i]) > Integer.parseInt(allowedVerArr[i])) {
					break;
				} else {
					return false;
				}
			}
		} catch(Exception e){
			logger.error("Error : " + e.getMessage());
			return false;
		}
		
		return true;
	}

	private static boolean disAllowedCheck(String p_sIncomingVersion, String p_sDisallowedVersion) {
		if (p_sIncomingVersion.equalsIgnoreCase(p_sDisallowedVersion)) {
			return true;
		}

		String[] sarrIncomingVersionComponents = getVersionComponents(p_sIncomingVersion);
		String[] disAllowedVerArr = getVersionComponents(p_sDisallowedVersion);

		int position = disAllowedVerArr.length;
		for (int i = 0; i < disAllowedVerArr.length; i++) {
			if (disAllowedVerArr[i].equals(ASTERIX)) {
				position = i;
				break;
			}
		}

		try{
			for (int i = 0; i < position; i++) {
				if (Integer.parseInt(sarrIncomingVersionComponents[i]) == Integer.parseInt(disAllowedVerArr[i])) {
					continue;
				} else {
					return false;
				}
			}
		} catch(Exception e){
			logger.error("Error : " + e.getMessage());
			return false;
		}
		
		return true;
	}
}
