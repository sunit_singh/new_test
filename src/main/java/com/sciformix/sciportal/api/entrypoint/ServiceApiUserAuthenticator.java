package com.sciformix.sciportal.api.entrypoint;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Map;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.CryptoUtils;
import com.sciformix.sciportal.api.entrypoint.UserPasswordCache.HASH;
import com.sciformix.sciportal.api.model.SciApiResponse;
import com.sciformix.sciportal.api.model.ServiceApiContext;
import com.sciformix.sciportal.apps.sys.SysConfigPortalApp;
import com.sciformix.sciportal.audit.AuditWebManager;
import com.sciformix.sciportal.system.DisplayMessageHelper;
import com.sciformix.sciportal.user.UserAuthHome;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;

class ServiceApiUserAuthenticator {

	private static final Logger log = LoggerFactory.getLogger(ServiceApiUserAuthenticator.class);

	private static UserPasswordCache userCache = new UserPasswordCache();

	public static SciApiResponse authenticateUser(HttpServletRequest request, String keyId, String userId,
			String p_sIncomingPassword, Map<String, String> systemPayloadMap) throws SciException, SciServiceException {
		SciApiResponse tempResponse = new SciApiResponse();
		String decrPassword = null;
		PrivateKey privateKey = null;
		
		try {
			// Code to get the private key from db with respective keyId.
			privateKey = SysConfigPortalApp.getPrivateKey(keyId);
			if(privateKey == null){
				tempResponse.setCode(WebErrorCodes.WebServices.INCORRECT_KEYID.errorCode());
				tempResponse.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.INCORRECT_KEYID));
				return tempResponse;
			}
			decrPassword = decryptText(keyId, p_sIncomingPassword);
			if(decrPassword == null){
				tempResponse.setCode(WebErrorCodes.WebServices.PWD_NOT_MATCH.errorCode());
				tempResponse
						.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.PWD_NOT_MATCH));
				log.error("Authenticate Request rejected for {} as Password does not match.", userId);
				return tempResponse;
			}
		} catch (SciException e) {
			log.error("Error while decrypting password. Error : " + e.getMessage());
		}
		
		String sIncomingPasswordHash = CryptoUtils.getHash(decrPassword);
		HASH hashCheck = userCache.checkHash(userId, sIncomingPasswordHash);

		if (hashCheck == HASH.HASH_NOT_EQUAL || hashCheck == HASH.HASH_NOT_EXISTS) {
			log.error("Hash not valid. Proceeding to authenticate and re-cache hash if authentication is successful.");
			tempResponse = forceAuthenticate(request, keyId, userId, decrPassword, systemPayloadMap);
			if (tempResponse.getCode() == WebErrorCodes.WebServices.SUCCESS.errorCode()) {
				userCache.addToCache(userId, sIncomingPasswordHash);
				log.info("Auhentication successful and added to cache.");
			} else {
				log.error("Auhentication failed. Cannot allow to proceed further.");
			}
		} else if (hashCheck == HASH.HASH_EQUAL) {
			log.info("Auhentication successful.");
			tempResponse.setCode(WebErrorCodes.WebServices.SUCCESS.errorCode());
			tempResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.SUCCESS));
		}
		return tempResponse;
	}

	private static SciApiResponse forceAuthenticate(HttpServletRequest request, String keyId, String userId,
			String decrPassword, Map<String, String> systemPayloadMap) throws SciServiceException {

		SciApiResponse tempResponse = new SciApiResponse();
		UserInfo oUserInfo = null;
		UserSession oUserSession = null;

		try {
			if (!WebUtils.validateUserid(userId)) {
				tempResponse.setCode(WebErrorCodes.WebServices.USERID_INVALID_CHARS.errorCode());
				tempResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.USERID_INVALID_CHARS));
				log.error("Authentication denied for {} as Username contains invalid characters", userId);

				try {

					AuditWebManager.auditLoginAttempt(-1, userId, "Invalid user id",
							WebUtils.getSessionServerIp(request),
							systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_IPADDRESS),
							systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINENAME));

				} catch (SciServiceException serExcep) {
					log.error("Error auditing failed Authentication attempt - {}. Marking as invalid user id.", userId,
							serExcep);
				}

				return tempResponse;

			} else {
				oUserInfo = getUserInfo(userId);
			}

		} catch (SciServiceException serExcep) {
			log.error("Authentication denied for user - {}. Marking as incorrect user credentials.", userId, serExcep);
		}

		if (oUserInfo == null) {
			tempResponse.setCode(WebErrorCodes.WebServices.USERID_NOT_REGISTERED.errorCode());
			tempResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.USERID_NOT_REGISTERED));
			log.error("Authentication denied for {} as User is not registered", userId);
			try {
				AuditWebManager.auditLoginAttempt(-1, userId, "User not registered",
						WebUtils.getSessionServerIp(request),
						systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_IPADDRESS),
						systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINENAME));
			} catch (SciServiceException serExcep) {
				log.error("Error auditing failed Authentication attempt - {}. Marking as incorrect user credentials.",
						userId, serExcep);
			}
		} else {

			boolean bAuthenticationSuccessful = false;

			try {
				bAuthenticationSuccessful = UserAuthHome.authenticateUserForLogin(oUserInfo, decrPassword);
			} catch (SciWebException webExcep) {
				log.error("Error during authentication of user - {}", userId, webExcep);
			}

			if (bAuthenticationSuccessful) {
				tempResponse.setCode(WebErrorCodes.WebServices.SUCCESS.errorCode());
				tempResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.SUCCESS));
				log.debug("User authentication successful for {}; ", userId);
				try {
					oUserSession = WebUtils.createWebUserSession(request, oUserInfo);
					AuditWebManager.auditLogin(oUserSession);
				} catch (SciServiceException serExcep) {
					log.error("Error auditing Authentication attempt - {}. Marking as incorrect user credentials.",
							userId, serExcep);
				}

			} else {
				tempResponse.setCode(WebErrorCodes.WebServices.PWD_NOT_MATCH.errorCode());
				tempResponse
						.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.PWD_NOT_MATCH));
				log.error("Authenticate Request rejected for {} as Password does not match.", userId);
				try {
					AuditWebManager.auditLoginAttempt(oUserInfo.getUserSeqId(), oUserInfo.getUserId(),
							"Incorrect user credentials", WebUtils.getSessionServerIp(request),
							systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_IPADDRESS),
							systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINENAME));
				} catch (SciServiceException serExcep) {
					log.error(
							"Error auditing failed Authentication attempt - {}. Marking as incorrect user credentials.",
							userId, serExcep);
				}
			}
		}

		return tempResponse;
	}

	public static UserInfo getUserInfo(String userId) throws SciServiceException {
		UserInfo oUserInfo = UserHome.retrieveUser(userId, UserHome.UserPurpose.DATA_SEARCH);
		return oUserInfo;
	}
	
	public static String encryptText(String keyId, String textToEncrypt) throws SciException {

		Cipher cipher = null;
		String encrypt = null;

		try {
			cipher = Cipher.getInstance("RSA");
			PublicKey publicKey = SysConfigPortalApp.getPublicKey(keyId);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			encrypt = Base64.getEncoder().encodeToString(cipher.doFinal(textToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			log.error("Error while decrypting : ", e.getMessage());
		}

		return encrypt;
	}

	public static String decryptText(String keyId, String textToDecrypt) throws SciException {

		Cipher cipher = null;
		String decrypt = null;

		try {
			cipher = Cipher.getInstance("RSA");
			PrivateKey privateKey = SysConfigPortalApp.getPrivateKey(keyId);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			decrypt = new String(cipher.doFinal(Base64.getDecoder().decode(textToDecrypt)));
		} catch (Exception e) {
			log.error("Error while decrypting : ", e.getMessage());
		}

		return decrypt;
	}
}
