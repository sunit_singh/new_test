package com.sciformix.sciportal.api.entrypoint;

import java.util.HashMap;
import java.util.Map;

class UserPasswordCache
{
	public static enum HASH
	{
		HASH_NOT_EXISTS, HASH_NOT_EQUAL, HASH_EQUAL;
	}
	
	public static class CacheItem
	{
		String sHashedPassword = null;
		long lEntryCreationTimestamp  = 0L;
		long lEntryLastTouchTimestamp = 0L;
		
		public CacheItem(String p_sHashedPassowrd)
		{
			sHashedPassword = p_sHashedPassowrd;
			lEntryCreationTimestamp = System.currentTimeMillis();
			lEntryLastTouchTimestamp = lEntryCreationTimestamp; 
		}
	}
	
	private Map<String, CacheItem> m_mapUserCache = null;

	public UserPasswordCache()
	{
		m_mapUserCache = new HashMap<String, CacheItem>();
	}
	
	public String getHashedPassword(String p_sUserId)
	{
		return m_mapUserCache.get(p_sUserId).sHashedPassword;
	}

	public void addToCache(String p_sUserId, String p_sHashedPassword)
	{
		m_mapUserCache.put(p_sUserId, new CacheItem(p_sHashedPassword));
	}
	
	public HASH checkHash(String userId, String p_UserPasswordHash)
	{
		String currentHash = "";
		if (m_mapUserCache.containsKey(userId))
		{
			if (m_mapUserCache.get(userId).lEntryLastTouchTimestamp < (30*60*1000))
			{
				m_mapUserCache.remove(userId);
				return HASH.HASH_NOT_EXISTS;
			}
			currentHash = m_mapUserCache.get(userId).sHashedPassword;
			m_mapUserCache.get(userId).lEntryLastTouchTimestamp = System.currentTimeMillis();
			if (currentHash.equalsIgnoreCase(p_UserPasswordHash))
			{
				return HASH.HASH_EQUAL;
			} else {
				return HASH.HASH_NOT_EQUAL;
			}
		}
		else
		{
			return HASH.HASH_NOT_EXISTS;
		}
		
	}
}
