package com.sciformix.sciportal.api.entrypoint;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.JsonParseException;

import com.sciformix.commons.WebErrorCodes;
import com.sciformix.sciportal.api.model.SciApiResponse;
import com.sciformix.sciportal.system.DisplayMessageHelper;

@Provider
public class JsonParseExceptionMapper implements ExceptionMapper<JsonParseException> {

	@Override
	public Response toResponse(JsonParseException exception) {
		Map<String, Object> sciApiResponseMap = new LinkedHashMap<String, Object>();
		SciApiResponse serviceResponse = new SciApiResponse();
		serviceResponse.setCode(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE.errorCode());
		serviceResponse.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE));
		sciApiResponseMap.put("Response", serviceResponse);
		return Response.status(Response.Status.BAD_REQUEST).entity(sciApiResponseMap).type(MediaType.APPLICATION_JSON).build();
	}

}