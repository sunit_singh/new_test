package com.sciformix.sciportal.api.entrypoint;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FilenameUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.WebErrorCodes.SimpleWebErrorCode;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.api.model.SciApiResponse;
import com.sciformix.sciportal.api.model.ServiceApiContext;
import com.sciformix.sciportal.api.utils.BrowserVersionValidator;
import com.sciformix.sciportal.api.utils.WebApiUtils;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.activity.ActivityPortalApp;
import com.sciformix.sciportal.apps.mb.MBPortalApp;
import com.sciformix.sciportal.apps.ngv.NgvPortalApp;
import com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset;
import com.sciformix.sciportal.system.DisplayMessageHelper;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.SystemUtils;
import com.sciformix.sciportal.utils.WebUtils;

@Path(value = "/")
public class SciWebApiEntryPoint {

	private static final Logger log = LoggerFactory.getLogger(SciWebApiEntryPoint.class);

	private static final String NGV_PORTALAPP_ID = "NGV-PORTAL-APP";

	private static final String ACTIVITY_PORTALAPP_ID = "ACTIVITY-PORTAL-APP";

	private static final String ACTIVITY_SUCCESS = "Activity registered successfully.";
	private static final String ACTIVITY_TRAIL_SUCCESS = "Activity added successfully to current trail.";
	private static final String TEMPLATE_EXPORT = "Template exported successfully.";
	private static final String NO_PROJECTS = "User not allocated to any project.";
	

	private static final String JSON_KEY_KEYID = "KeyId";
	private static final String JSON_KEY_USERID = "UserId";
	private static final String JSON_KEY_PASSWORD = "Password";
	private static final String JSON_KEY_CLIENTIQHASH = "ClientIQHash";
	private static final String JSON_KEY_PROJECTSEQID = "ProjectSeqId";
	private static final String JSON_KEY_ACTIVITYID = "ActivityId";
	private static final String JSON_KEY_ACTIVITYAPP = "ActivityApp";
	private static final String JSON_KEY_ACTIVITYTYPE = "ActivityType";
	private static final String JSON_KEY_ACTIVITYCODE = "ActivityCode";
	private static final String JSON_KEY_ACTIVITYDESC = "ActivityDesc";
	private static final String JSON_KEY_ACTIVITYPAYLOAD = "Payload";
	private static final String JSON_KEY_TEMPLATEID = "TemplateId";
	
	private static final String JSON_KEY_JDKVERSION = "JdkVersion";
	private static final String JSON_KEY_MAILBOXID = "mailbox_name";
	private static final String JSON_KEY_PROJNAME = "project_name";
	private static final String JSON_KEY_MAILARRAY = "mails";

	private static final String ACTIVITY_TYPE_GET_PROJECTS = "GET_PROJECTS";
	private static final String ACTIVITY_TYPE_EXPORT_TEMPLATE = "EXPORT_TEMPLATE";
	
	public static final String API_SCRIPT = "<{1}( *\\/*)*(script)|(script)( *\\/*)*>{1}";

	private SciApiResponse serviceResponse = null;

	private Map<String, String> clientParamsMap = null;
	private Map<String, String> systemPayloadMap = null;

	@HEAD
	@Path(value = "checkAvailability")
	@Produces(MediaType.TEXT_PLAIN)
	public Response checkAvailability() {
		Response r = null;
		try {
			r = Response.status(Status.OK).build();
		} catch (Exception e) {
			log.error("Error : " + e.getMessage());
		}
		return r;
	}

	@POST
	@Path(value = "getProjects")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> getProjects(@Context HttpServletRequest request, JSONObject json) {

		Map<String, Object> sciApiResponseMap = new LinkedHashMap<>();
		List<ObjectIdPair<Integer,String>> listProjectsSession = null;
		//List<ObjectIdPair<Integer,String>> listProjectsFinal = null;
		
		serviceResponse = new SciApiResponse();
		 
		IPortalApp oPortalApp = null;
		ActivityPortalApp oAuditActivityPortalApp = null;
		oPortalApp = (IPortalApp) AppRegistry.getApp(ACTIVITY_PORTALAPP_ID);
		oAuditActivityPortalApp = (ActivityPortalApp) oPortalApp;

		int respParentActivityId = -1;

		boolean areApisEnabled = WebApiUtils.areWebApisEnabled();
		if (!areApisEnabled) {
			log.error("Web APIs crrently disabled.");
			serviceResponse.setCode(WebErrorCodes.WebServices.WEB_APIS_DISABLED.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.WEB_APIS_DISABLED));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		try {

			String keyId = WebApiUtils.getRequestParameter(json, JSON_KEY_KEYID);
			String userId = WebApiUtils.getRequestParameter(json, JSON_KEY_USERID);
			String password = WebApiUtils.getRequestParameter(json, JSON_KEY_PASSWORD);
			String clientIqHash = WebApiUtils.getRequestParameter(json, JSON_KEY_CLIENTIQHASH);

			extractContextParamsFromRequest(json);
			WebApiUtils.displayLogsForClientParams(clientParamsMap, systemPayloadMap);

			/*VAPT : Fix added for Script Tag and not echoing the value back : START*/
			if (WebApiUtils.validateScriptTag(clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID), API_SCRIPT)){
				sciApiResponseMap.put(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID, StringConstants.EMPTY);
			} else {
				sciApiResponseMap.put(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID,
						clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID));
			}
			/*VAPT : Fix added for Script Tag and not echoing the value back : END*/

			UserInfo oUserInfo = null;
			UserSession oUserSession = null;

			if (!validateInputParameter(serviceResponse, keyId, WebErrorCodes.WebServices.INVALID_KEYID, JSON_KEY_KEYID)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, userId, WebErrorCodes.WebServices.INVALID_USERID_PWD, JSON_KEY_USERID)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, password,
					WebErrorCodes.WebServices.INVALID_USERID_PWD, JSON_KEY_PASSWORD)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, clientIqHash,
					WebErrorCodes.WebServices.INVALID_CLIENT_IQHASH, JSON_KEY_CLIENTIQHASH)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}

			if (!validateServiceCommonParams(serviceResponse, clientParamsMap, systemPayloadMap)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			log.debug("Login request received for user - {}", userId);

			serviceResponse = ServiceApiUserAuthenticator.authenticateUser(request, keyId, userId, password,
					systemPayloadMap);
			sciApiResponseMap.put("Response", serviceResponse);
			if (serviceResponse.getCode() == WebErrorCodes.WebServices.SUCCESS.errorCode()) {
				SciApiResponse tempServiceResponse = new SciApiResponse();
				try {
					oUserInfo = ServiceApiUserAuthenticator.getUserInfo(userId);

				} catch (SciServiceException e) {
					log.error("Error getting User Info : " + e.getMessage());
				}
				oUserSession = WebUtils.createWebUserSession(request, oUserInfo);
				
				listProjectsSession = oUserSession.getAuthorizedProjectIds();
				if(listProjectsSession.isEmpty()){
					sciApiResponseMap.put("UserProjects", NO_PROJECTS);
					return sciApiResponseMap;
				}
				
				List<Map<String, Object>> projectList = new ArrayList<Map<String, Object>>();
				Map<String, Object> projectsMap = new LinkedHashMap<String, Object>();
				
				for (ObjectIdPair<Integer, String> oProject : listProjectsSession)
				{
					projectsMap = getProjectsParamsValidator(tempServiceResponse, oProject, clientParamsMap, systemPayloadMap, clientIqHash);
					projectList.add(projectsMap);
				}
				
				sciApiResponseMap.put("UserProjects", listProjectsSession.isEmpty() ? null
						: projectList);

				// Creation of activity entry
				try {
					respParentActivityId = oAuditActivityPortalApp.registerNewActivity(oUserInfo, -1,
							WebUtils.getSessionServerIp(request),
							clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLNAME),
							ACTIVITY_TYPE_GET_PROJECTS, "01", null, clientParamsMap, systemPayloadMap);
					log.debug("Activity registered successfully. Activity ID : " + respParentActivityId);
				} catch (ParseException | IOException e) {
					log.error("Error in logging activity : " + e.getMessage());
				}
			}

		} catch (SciException e) {
			log.error("Error Authenticating User : " + e.getMessage());
			serviceResponse.setCode(WebErrorCodes.WebServices.AUTH_FAILED_ADMIN.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.AUTH_FAILED_ADMIN));
			sciApiResponseMap.put("Response", serviceResponse);
		} catch (SciServiceException e) {
			log.error("Error Authenticating User : " + e.getMessage());
			serviceResponse.setCode(WebErrorCodes.WebServices.AUTH_FAILED_ADMIN.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.AUTH_FAILED_ADMIN));
			sciApiResponseMap.put("Response", serviceResponse);
		} catch (JSONException e1) {
			log.error("Invalid Json Structure.");
			serviceResponse.setCode(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		log.info("Response : " + sciApiResponseMap.toString());
		return sciApiResponseMap;
	}

	@POST
	@Path(value = "registerActivity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> registerActivity(@Context HttpServletRequest request, JSONObject json) {
		Map<String, Object> sciApiResponseMap = new LinkedHashMap<>();
		serviceResponse = new SciApiResponse();

		boolean areApisEnabled = WebApiUtils.areWebApisEnabled();
		if (!areApisEnabled) {
			log.error("Web APIs crrently disabled.");
			serviceResponse.setCode(WebErrorCodes.WebServices.WEB_APIS_DISABLED.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.WEB_APIS_DISABLED));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		String keyId = WebApiUtils.getRequestParameter(json, JSON_KEY_KEYID);
		String userId = WebApiUtils.getRequestParameter(json, JSON_KEY_USERID);
		String password = WebApiUtils.getRequestParameter(json, JSON_KEY_PASSWORD);
		String clientIqHash = WebApiUtils.getRequestParameter(json, JSON_KEY_CLIENTIQHASH);

		int projectSeqId = -1;
		int currParentActivityId = -1;
		String activityApp = WebApiUtils.getRequestParameter(json, JSON_KEY_ACTIVITYAPP);
		String activityType = WebApiUtils.getRequestParameter(json, JSON_KEY_ACTIVITYTYPE);
		String activityCode = WebApiUtils.getRequestParameter(json, JSON_KEY_ACTIVITYCODE);
		String activityDesc = WebApiUtils.getRequestParameter(json, JSON_KEY_ACTIVITYDESC);
		String payload = WebApiUtils.getRequestParameter(json, JSON_KEY_ACTIVITYPAYLOAD);

		try {
			extractContextParamsFromRequest(json);
		} catch (JSONException e2) {
			log.error("Invalid Json Structure.");
			serviceResponse.setCode(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		WebApiUtils.displayLogsForClientParams(clientParamsMap, systemPayloadMap);

		/*VAPT : Fix added for Script Tag and not echoing the value back : START*/
		if (WebApiUtils.validateScriptTag(clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID), API_SCRIPT)){
			sciApiResponseMap.put(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID, StringConstants.EMPTY);
		} else {
			sciApiResponseMap.put(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID,
					clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID));
		}
		/*VAPT : Fix added for Script Tag and not echoing the value back : END*/

		UserInfo oUserInfo = null;
		IPortalApp oPortalApp = null;
		ActivityPortalApp oAuditActivityPortalApp = null;
		oPortalApp = (IPortalApp) AppRegistry.getApp(ACTIVITY_PORTALAPP_ID);
		oAuditActivityPortalApp = (ActivityPortalApp) oPortalApp;

		int respParentActivityId = -1;
		Date activityDate = null;

		try {

			if (!validateInputParameter(serviceResponse, keyId, WebErrorCodes.WebServices.INVALID_KEYID, JSON_KEY_KEYID)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, userId, WebErrorCodes.WebServices.INVALID_USERID_PWD, JSON_KEY_USERID)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, password,
					WebErrorCodes.WebServices.INVALID_USERID_PWD, JSON_KEY_PASSWORD)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, clientIqHash,
					WebErrorCodes.WebServices.INVALID_CLIENT_IQHASH, JSON_KEY_CLIENTIQHASH)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, activityApp,
					WebErrorCodes.WebServices.INVALID_ACTIVITY_APP, JSON_KEY_ACTIVITYAPP)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, activityType,
					WebErrorCodes.WebServices.INVALID_ACTIVITY_TYPE, JSON_KEY_ACTIVITYTYPE)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, activityCode,
					WebErrorCodes.WebServices.INVALID_ACTIVITY_CODE, JSON_KEY_ACTIVITYCODE)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			/*VAPT : Fix added for activity description and payload : START*/
			if (!validateNonMandatoryInputsForScript(serviceResponse,
					activityDesc, WebErrorCodes.WebServices.GENERIC_INVALID_INPUT, JSON_KEY_ACTIVITYDESC)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateNonMandatoryInputsForScript(serviceResponse,
					payload, WebErrorCodes.WebServices.GENERIC_INVALID_INPUT, JSON_KEY_ACTIVITYPAYLOAD)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			/*VAPT : Fix added for activity description and payload : END*/

			activityDate = WebApiUtils
					.getFormattedDate(clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TIMESTAMP));
			if (activityDate == null) {
				log.debug("Invalid Input. Timestamp not valid." + activityDate);
				serviceResponse.setCode(WebErrorCodes.WebServices.INVALID_DATETIME.errorCode());
				serviceResponse
						.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.INVALID_DATETIME));
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}

			try {
				currParentActivityId = WebApiUtils.getRequestIntParameter(json, JSON_KEY_ACTIVITYID);
			} catch (NumberFormatException e) {
				log.debug("Invalid Input. ActivityID needs to be numeric.");
				serviceResponse.setCode(WebErrorCodes.WebServices.NON_NUMERIC_ACTIVITY_ID.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.NON_NUMERIC_ACTIVITY_ID));
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}

			try {
				projectSeqId = WebApiUtils.getRequestProjSeqId(json, JSON_KEY_PROJECTSEQID);
			} catch (NumberFormatException e) {
				log.debug("Invalid Input. ActivityID needs to be numeric.");
				serviceResponse.setCode(WebErrorCodes.WebServices.NON_NUMERIC_PROJECT_ID.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.NON_NUMERIC_PROJECT_ID));
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			boolean isProjectValidForUser = WebApiUtils.checkProjectAgainstProjectId(request, serviceResponse, userId, projectSeqId);
			if(!isProjectValidForUser){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			if (!validateServiceCommonParams(serviceResponse, clientParamsMap, systemPayloadMap)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			// Client Jar Checksum
			boolean isClientIqValid = WebApiUtils.verifyClientIQ(serviceResponse, projectSeqId, clientIqHash);
			if(!isClientIqValid){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//Tool Name and Version check
			boolean isToolAllowed = WebApiUtils.performToolValidation(serviceResponse, projectSeqId, clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLNAME), clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLVERSION));
			if(!isToolAllowed){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//OsName and JDK check
			boolean isServerIQValid = WebApiUtils.verifyRuntimeIQParams(serviceResponse, projectSeqId, systemPayloadMap);
			if(!isServerIQValid){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//Validate Browser Versions : CHROME
			if (!BrowserVersionValidator.checkBrowserVersionChrome(serviceResponse, systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_CHROME), projectSeqId)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//Validate Browser Versions : IE
			if (!BrowserVersionValidator.checkBrowserVersionIE(serviceResponse, systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_IE), projectSeqId)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			serviceResponse = ServiceApiUserAuthenticator.authenticateUser(request, keyId, userId, password,
					systemPayloadMap);

			if (serviceResponse.getCode() == WebErrorCodes.WebServices.SUCCESS.errorCode()) {

				oUserInfo = ServiceApiUserAuthenticator.getUserInfo(userId);
				if (currParentActivityId <= 0) {

					// Insert Activity with new parentActivityId
					respParentActivityId = oAuditActivityPortalApp.registerNewActivity(oUserInfo, projectSeqId,
							WebUtils.getSessionServerIp(request), activityApp, activityType, activityCode, payload,
							clientParamsMap, systemPayloadMap);

					if (respParentActivityId != -1) {
						serviceResponse.setCode(WebErrorCodes.WebServices.SUCCESS.errorCode());
						serviceResponse.setMessage(ACTIVITY_SUCCESS);
						sciApiResponseMap.put("Response", serviceResponse);
						sciApiResponseMap.put("ActivityId", respParentActivityId);
					} else {
						serviceResponse.setCode(WebErrorCodes.WebServices.ACT_REGISTER_FAILED_ADMIN.errorCode());
						serviceResponse.setMessage(DisplayMessageHelper
								.getDisplayMessage(WebErrorCodes.WebServices.ACT_REGISTER_FAILED_ADMIN));
						sciApiResponseMap.put("Response", serviceResponse);
						sciApiResponseMap.put("ActivityId", "");
					}

				} else {

					// Insert Activity Detail with existing parentActivity
					respParentActivityId = oAuditActivityPortalApp.registerIntoExistingActivityTrail(
							currParentActivityId, activityCode, activityDesc, payload, clientParamsMap);

					if (respParentActivityId == WebErrorCodes.WebServices.NO_SUCH_ACTIVITY.errorCode()) {
						serviceResponse.setCode(WebErrorCodes.WebServices.NO_SUCH_ACTIVITY.errorCode());
						serviceResponse.setMessage(
								DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.NO_SUCH_ACTIVITY));
						sciApiResponseMap.put("Response", serviceResponse);
						sciApiResponseMap.put("ActivityId", "");
					} else if (respParentActivityId == -1) {
						serviceResponse.setCode(WebErrorCodes.WebServices.ACT_REGISTER_FAILED_ADMIN.errorCode());
						serviceResponse.setMessage(DisplayMessageHelper
								.getDisplayMessage(WebErrorCodes.WebServices.ACT_REGISTER_FAILED_ADMIN));
						sciApiResponseMap.put("Response", serviceResponse);
						sciApiResponseMap.put("ActivityId", "");
					} else {
						// Success
						serviceResponse.setCode(WebErrorCodes.WebServices.SUCCESS.errorCode());
						serviceResponse.setMessage(ACTIVITY_TRAIL_SUCCESS);
						sciApiResponseMap.put("Response", serviceResponse);
						sciApiResponseMap.put("ActivityId", respParentActivityId);
					}

				}
			} else {
				sciApiResponseMap.put("Response", serviceResponse);
			}

		} catch (SciServiceException | SciException | ParseException | IOException e) {
			log.error("Activity Registration failed : " + e);
			serviceResponse.setCode(WebErrorCodes.WebServices.ACT_REGISTER_FAILED_ADMIN.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.ACT_REGISTER_FAILED_ADMIN));
			sciApiResponseMap.put("Response", serviceResponse);
			sciApiResponseMap.put("ActivityId", "");
		}

		log.info("Response : " + sciApiResponseMap.toString());
		return sciApiResponseMap;
	}

	@POST
	//@Path(value = "exportTemplate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> exportTemplateFromId(@Context HttpServletRequest request, JSONObject json) {
		Map<String, Object> sciApiResponseMap = new LinkedHashMap<>();
		serviceResponse = new SciApiResponse();

		ActivityPortalApp oAuditActivityPortalApp = null;
		UserInfo oUserInfo = null;

		int respParentActivityId = -1;

		boolean areApisEnabled = WebApiUtils.areWebApisEnabled();
		if (!areApisEnabled) {
			log.error("Web APIs crrently disabled.");
			serviceResponse.setCode(WebErrorCodes.WebServices.WEB_APIS_DISABLED.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.WEB_APIS_DISABLED));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		String keyId = WebApiUtils.getRequestParameter(json, JSON_KEY_KEYID);
		String userId = WebApiUtils.getRequestParameter(json, JSON_KEY_USERID);
		String password = WebApiUtils.getRequestParameter(json, JSON_KEY_PASSWORD);
		String clientIqHash = WebApiUtils.getRequestParameter(json, JSON_KEY_CLIENTIQHASH);
		int projectSeqId = -1;
		int templateId = -1;

		try {
			extractContextParamsFromRequest(json);
		} catch (JSONException e1) {
			log.error("Invalid Json Structure.");
			serviceResponse.setCode(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.INVALID_JSON_STRUCTURE));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}
		WebApiUtils.displayLogsForClientParams(clientParamsMap, systemPayloadMap);

		if (WebApiUtils.validateScriptTag(clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID), API_SCRIPT)){
			sciApiResponseMap.put(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID, StringConstants.EMPTY);
		} else {
			sciApiResponseMap.put(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID,
					clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID));
		}

		IPortalApp oPortalApp = null;
		NgvPortalApp oNgvPortalApp = null;
		oPortalApp = (IPortalApp) AppRegistry.getApp(NGV_PORTALAPP_ID);
		oNgvPortalApp = (NgvPortalApp) oPortalApp;
		NgvTemplateRuleset ruleSet = null;
		String sTemplateXml = null;
		FileWriter oFileWriter = null;

		File file = null;

		try {

			if (!validateInputParameter(serviceResponse, keyId, WebErrorCodes.WebServices.INVALID_KEYID, JSON_KEY_KEYID)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, userId, WebErrorCodes.WebServices.INVALID_USERID_PWD, JSON_KEY_USERID)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, password,
					WebErrorCodes.WebServices.INVALID_USERID_PWD, JSON_KEY_PASSWORD)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, clientIqHash,
					WebErrorCodes.WebServices.INVALID_CLIENT_IQHASH, JSON_KEY_CLIENTIQHASH)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			try {
				projectSeqId = WebApiUtils.getRequestProjSeqId(json, JSON_KEY_PROJECTSEQID);
			} catch (NumberFormatException e) {
				log.debug("Invalid Input. ActivityID needs to be numeric.");
				serviceResponse.setCode(WebErrorCodes.WebServices.NON_NUMERIC_PROJECT_ID.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.NON_NUMERIC_PROJECT_ID));
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			boolean isProjectValidForUser = WebApiUtils.checkProjectAgainstProjectId(request, serviceResponse, userId, projectSeqId);
			if(!isProjectValidForUser){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}

			try {
				templateId = WebApiUtils.getRequestIntParameter(json, JSON_KEY_TEMPLATEID);
			} catch (NumberFormatException e) {
				log.debug("Invalid Input. TemplateId needs to be numeric.");
				serviceResponse.setCode(WebErrorCodes.WebServices.NON_NUMERIC_TEMPLATE_ID.errorCode());
				serviceResponse.setMessage(
						DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.NON_NUMERIC_TEMPLATE_ID));
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}

			if (!validateServiceCommonParams(serviceResponse, clientParamsMap, systemPayloadMap)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			// Client Jar Checksum
			boolean isClientIqValid = WebApiUtils.verifyClientIQ(serviceResponse, projectSeqId, clientIqHash);
			if(!isClientIqValid){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//Tool Name and Version check
			boolean isToolAllowed = WebApiUtils.performToolValidation(serviceResponse, projectSeqId, clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLNAME), clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLVERSION));
			if(!isToolAllowed){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//OsName and JDK check
			boolean isServerIQValid = WebApiUtils.verifyRuntimeIQParams(serviceResponse, projectSeqId, systemPayloadMap);
			if(!isServerIQValid){
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//Validate Browser Versions : CHROME
			if (!BrowserVersionValidator.checkBrowserVersionChrome(serviceResponse, systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_CHROME), projectSeqId)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			//Validate Browser Versions : IE
			if (!BrowserVersionValidator.checkBrowserVersionIE(serviceResponse, systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_IE), projectSeqId)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			serviceResponse = ServiceApiUserAuthenticator.authenticateUser(request, keyId, userId, password,
					systemPayloadMap);
			if (serviceResponse.getCode() == WebErrorCodes.WebServices.SUCCESS.errorCode()) {

				try {
					ruleSet = oNgvPortalApp.getTemplateRuleset(templateId);
				} catch (Exception excep) {
					log.error("Error while getting templateRuleset : {}", excep);
					serviceResponse.setCode(WebErrorCodes.WebServices.TEMPLATEID_NOT_EXIST.errorCode());
					serviceResponse.setMessage(
							DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.TEMPLATEID_NOT_EXIST));
				}

				if (ruleSet != null) {

					sTemplateXml = ruleSet.createXml();
					String sTempFolderPath = "";
					try {
						sTempFolderPath = SystemUtils.createUserTempFolder("APITemplateExports");
					} catch (Exception e) {
						log.error("Error during template export", e);
						serviceResponse.setCode(WebErrorCodes.WebServices.TEMPLATE_EXPORT_FAILED_ADMIN.errorCode());
						serviceResponse.setMessage(DisplayMessageHelper
								.getDisplayMessage(WebErrorCodes.WebServices.TEMPLATE_EXPORT_FAILED_ADMIN));
					}

					String filePath = FilenameUtils.concat(sTempFolderPath,
							Calendar.getInstance().getTimeInMillis() + "_" + "template.xml");
					file = new File(filePath);
					oFileWriter = new FileWriter(file);

					file.createNewFile();

					oFileWriter.write(sTemplateXml);
					oFileWriter.close();

					byte[] convertedFileToByteArr = WebApiUtils.getBytesFromFile(file);

					String encoded = WebApiUtils.getEncodedStringFromBytearr(convertedFileToByteArr);

					serviceResponse.setCode(WebErrorCodes.WebServices.SUCCESS.errorCode());
					serviceResponse.setMessage(TEMPLATE_EXPORT);
					sciApiResponseMap.put("Response", serviceResponse);
					sciApiResponseMap.put("TemplateBody", encoded);

				} else {
					log.debug("No template found for template ID :" + templateId);
					serviceResponse.setCode(WebErrorCodes.WebServices.TEMPLATEID_NOT_EXIST.errorCode());
					serviceResponse.setMessage(
							DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.TEMPLATEID_NOT_EXIST));
					sciApiResponseMap.put("Response", serviceResponse);
				}

				// Creation of activity entry
				oPortalApp = (IPortalApp) AppRegistry.getApp(ACTIVITY_PORTALAPP_ID);
				oAuditActivityPortalApp = (ActivityPortalApp) oPortalApp;
				try {
					oUserInfo = ServiceApiUserAuthenticator.getUserInfo(userId);
					respParentActivityId = oAuditActivityPortalApp.registerNewActivity(oUserInfo, projectSeqId,
							WebUtils.getSessionServerIp(request),
							clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLNAME),
							ACTIVITY_TYPE_EXPORT_TEMPLATE, "CODE", null, clientParamsMap, systemPayloadMap);
					log.debug("Activity registered successfully. Activity ID : " + respParentActivityId);
				} catch (ParseException | IOException e) {
					log.error("Error in logging activity : " + e.getMessage());
				}

			} else {
				sciApiResponseMap.put("Response", serviceResponse);
			}

		} catch (IOException e) {
			log.error("Error while getting templateRuleset : {}", e);
			serviceResponse.setCode(WebErrorCodes.WebServices.TEMPLATE_EXPORT_FAILED_ADMIN.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.TEMPLATE_EXPORT_FAILED_ADMIN));
			sciApiResponseMap.put("Response", serviceResponse);
		} catch (Exception e) {
			log.error("Error while getting templateRuleset : {}", e);
			serviceResponse.setCode(WebErrorCodes.WebServices.TEMPLATE_EXPORT_FAILED_ADMIN.errorCode());
			serviceResponse.setMessage(
					DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.TEMPLATE_EXPORT_FAILED_ADMIN));
			sciApiResponseMap.put("Response", serviceResponse);
		}

		log.info("Response : " + sciApiResponseMap.toString());
		return sciApiResponseMap;
	}

	/*@POST
	@Path(value = "getVersion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> getVersion(@Context HttpServletRequest request, JSONObject json) {
		Map<String, Object> sciApiResponseMap = new LinkedHashMap<>();
		serviceResponse = new SciApiResponse();
		String latestToolVersion = null;

		boolean areApisEnabled = WebApiUtils.areWebApisEnabled();
		if (!areApisEnabled) {
			log.error("Web APIs crrently disabled.");
			serviceResponse.setCode(WebErrorCodes.WebServices.WEB_APIS_DISABLED.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.WEB_APIS_DISABLED));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		try {

			String keyId = WebApiUtils.getRequestParameter(json, JSON_KEY_KEYID);
			String userId = WebApiUtils.getRequestParameter(json, JSON_KEY_USERID);
			String password = WebApiUtils.getRequestParameter(json, JSON_KEY_PASSWORD);

			extractContextParamsFromRequest(json);
			WebApiUtils.displayLogsForClientParams(clientParamsMap, systemPayloadMap);

			sciApiResponseMap.put(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID,
					clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID));

			if (!validateInputParameter(serviceResponse, keyId, WebErrorCodes.WebServices.INVALID_KEYID)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, userId, WebErrorCodes.WebServices.INVALID_USERID_PWD)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			} else if (!validateInputParameter(serviceResponse, password,
					WebErrorCodes.WebServices.INVALID_USERID_PWD)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}

			if (!validateServiceCommonParams(serviceResponse, clientParamsMap, systemPayloadMap)) {
				sciApiResponseMap.put("Response", serviceResponse);
				return sciApiResponseMap;
			}
			
			serviceResponse = ServiceApiUserAuthenticator.authenticateUser(request, keyId, userId, password,
					systemPayloadMap);
			if (serviceResponse.getCode() == WebErrorCodes.WebServices.SUCCESS.errorCode()) {
				latestToolVersion = WebApiUtils.getCurrentCpatVersion();
				serviceResponse.setCode(WebErrorCodes.WebServices.SUCCESS.errorCode());
				serviceResponse.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.SUCCESS));
				
				sciApiResponseMap.put("Response", serviceResponse);
				sciApiResponseMap.put("LatestToolVersion", latestToolVersion);
			} else {
				sciApiResponseMap.put("Response", serviceResponse);
			}

		} catch (Exception e) {
			log.error("Something went wrong: " + e);
			serviceResponse.setCode(WebErrorCodes.WebServices.SOMETHING_WENT_WRONG.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.SOMETHING_WENT_WRONG));
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		return sciApiResponseMap;
	}*/

	private void extractContextParamsFromRequest(JSONObject jsonRequest) throws JSONException {
		clientParamsMap = new LinkedHashMap<String, String>();
		systemPayloadMap = new LinkedHashMap<String, String>();
		ServiceApiContext oContext = new ServiceApiContext();
		clientParamsMap = oContext.getClientContextParameters(jsonRequest);
		systemPayloadMap = oContext.getSystemPayloadContextParameters(jsonRequest);
	}

	private boolean validateInputParameter(SciApiResponse response, String p_sParameterValueToValidate,
			SimpleWebErrorCode p_eErrorCode, String p_sParameterKey) {
		if (StringUtils.isNullOrEmpty(p_sParameterValueToValidate)) {
			log.debug(p_eErrorCode.errorMessage());
			response.setCode(p_eErrorCode.errorCode());
			response.setMessage(DisplayMessageHelper.getDisplayMessage(p_eErrorCode));
			return false;
		} else if (WebApiUtils.validateScriptTag(p_sParameterValueToValidate, API_SCRIPT)){
			log.debug(WebErrorCodes.WebServices.GENERIC_INVALID_INPUT.errorMessage() + p_sParameterKey);
			response.setCode(WebErrorCodes.WebServices.GENERIC_INVALID_INPUT.errorCode());
			response.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.GENERIC_INVALID_INPUT) + p_sParameterKey);
			return false;
		} else {
			return true;
		}
	}
	
	/*VAPT Fix for Script tag in request : 23-04-2018*/
	private boolean validateNonMandatoryInputsForScript(SciApiResponse response, String p_sParameterValueToValidate,
			SimpleWebErrorCode p_eErrorCode, String p_sParameterKey){
		if (!StringUtils.isNullOrEmpty(p_sParameterValueToValidate) && WebApiUtils.validateScriptTag(p_sParameterValueToValidate, API_SCRIPT)) {
			log.debug(p_eErrorCode.errorMessage() + p_sParameterKey);
			response.setCode(p_eErrorCode.errorCode());
			response.setMessage(DisplayMessageHelper.getDisplayMessage(p_eErrorCode) + p_sParameterKey);
			return false;
		} else {
			return true;
		}
	}
	/*VAPT Fix for Script tag in request : 23-04-2018*/

	private boolean validateDateFormat(SciApiResponse response, String p_sParameterValueToValidate,
			SimpleWebErrorCode p_eErrorCode){
		if(WebApiUtils.getFormattedDate(p_sParameterValueToValidate)==null){
			log.debug(p_eErrorCode.errorMessage());
			response.setCode(p_eErrorCode.errorCode());
			response.setMessage(DisplayMessageHelper.getDisplayMessage(p_eErrorCode));
			return false;
		} else {
			return true;
		}
	}
	
	private boolean validateServiceCommonParams(SciApiResponse response, Map<String, String> clientParamsMap,
			Map<String, String> systemPayloadMap) {

		if (!validateInputParameter(serviceResponse, clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLNAME),
				WebErrorCodes.WebServices.INVALID_TOOLNAME, ServiceApiContext.CONTEXT_VARIABLE_TOOLNAME)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLVERSION),
				WebErrorCodes.WebServices.INVALID_TOOLVERSION, ServiceApiContext.CONTEXT_VARIABLE_TOOLVERSION)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TIMEZONE),
				WebErrorCodes.WebServices.INVALID_TIMEZONE, ServiceApiContext.CONTEXT_VARIABLE_TIMEZONE)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TIMESTAMP),
				WebErrorCodes.WebServices.INVALID_TIMESTAMP, ServiceApiContext.CONTEXT_VARIABLE_TIMESTAMP)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID),
				WebErrorCodes.WebServices.INVALID_CLIENTREQID, ServiceApiContext.CONTEXT_VARIABLE_CLIENTREQID)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINENAME),
				WebErrorCodes.WebServices.INVALID_MACHINENAME, ServiceApiContext.CONTEXT_VARIABLE_MACHINENAME)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINEDOMAIN),
				WebErrorCodes.WebServices.INVALID_MACHINEDOMAIN, ServiceApiContext.CONTEXT_VARIABLE_MACHINEDOMAIN)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_IPADDRESS),
				WebErrorCodes.WebServices.INVALID_IPADDRESS, ServiceApiContext.CONTEXT_VARIABLE_IPADDRESS)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINELOGGEDINUSER),
				WebErrorCodes.WebServices.INVALID_MACHINELOGGEDINUSER, ServiceApiContext.CONTEXT_VARIABLE_MACHINELOGGEDINUSER)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINELOGGEDINUSERDOMAIN),
				WebErrorCodes.WebServices.INVALID_MACHINELOGGEDINUSERDOMAIN, ServiceApiContext.CONTEXT_VARIABLE_MACHINELOGGEDINUSERDOMAIN)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_OSNAME),
				WebErrorCodes.WebServices.INVALID_OSNAME, ServiceApiContext.CONTEXT_VARIABLE_OSNAME)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_OSVERSION),
				WebErrorCodes.WebServices.INVALID_OSVERSION, ServiceApiContext.CONTEXT_VARIABLE_OSVERSION)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_OSARCHITECTURE),
				WebErrorCodes.WebServices.INVALID_OSARCHITECTURE, ServiceApiContext.CONTEXT_VARIABLE_OSARCHITECTURE)) {
			return false;
		} else if (!validateInputParameter(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_JDKVERSION),
				WebErrorCodes.WebServices.INVALID_JDK, ServiceApiContext.CONTEXT_VARIABLE_JDKVERSION)) {
			return false;
		} else if (!validateDateFormat(serviceResponse,
				clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TIMESTAMP),
				WebErrorCodes.WebServices.INVALID_DATE_FORMAT)) {
			return false;
		} else if (!validateNonMandatoryInputsForScript(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_CHROME),
				WebErrorCodes.WebServices.GENERIC_INVALID_INPUT, ServiceApiContext.CONTEXT_VARIABLE_BROWSER_CHROME)) {
			return false;
		} else if (!validateNonMandatoryInputsForScript(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_IE),
				WebErrorCodes.WebServices.GENERIC_INVALID_INPUT, ServiceApiContext.CONTEXT_VARIABLE_BROWSER_IE)) {
			return false;
		} else {
			return true;
		}

	}
	
	private Map<String, Object> getProjectsParamsValidator(SciApiResponse serviceResponse,
			ObjectIdPair<Integer, String> project, Map<String, String> clientParamsMap,
			Map<String, String> systemPayloadMap, String clientIqHash) {
		Map<String, Object> tempProjectsMap = new LinkedHashMap<String, Object>();
		String status = null;
		
		boolean isClientIqValid = WebApiUtils.verifyClientIQ(serviceResponse, project.getObjectSeqId(), clientIqHash);
		if (!isClientIqValid) {
			tempProjectsMap.put("objectName", project.getObjectName());
			tempProjectsMap.put("objectSeqId", project.getObjectSeqId());
			status = serviceResponse.getCode() + ": " + serviceResponse.getMessage();
			tempProjectsMap.put("Status", status);
			return tempProjectsMap;
		}

		boolean isToolAllowed = WebApiUtils.performToolValidation(serviceResponse, project.getObjectSeqId(),
				clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLNAME),
				clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TOOLVERSION));
		if (!isToolAllowed) {
			tempProjectsMap.put("objectName", project.getObjectName());
			tempProjectsMap.put("objectSeqId", project.getObjectSeqId());
			status = serviceResponse.getCode() + ": " + serviceResponse.getMessage();
			tempProjectsMap.put("Status", status);
			return tempProjectsMap;
		}

		boolean isServerIQValid = WebApiUtils.verifyRuntimeIQParams(serviceResponse, project.getObjectSeqId(),
				systemPayloadMap);
		if (!isServerIQValid) {
			tempProjectsMap.put("objectName", project.getObjectName());
			tempProjectsMap.put("objectSeqId", project.getObjectSeqId());
			status = serviceResponse.getCode() + ": " + serviceResponse.getMessage();
			tempProjectsMap.put("Status", status);
			return tempProjectsMap;
		}

		// Validate Browser Versions : CHROME
		boolean isChromeVersionAllowed = BrowserVersionValidator.checkBrowserVersionChrome(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_CHROME), project.getObjectSeqId());
		if (!isChromeVersionAllowed) {
			tempProjectsMap.put("objectName", project.getObjectName());
			tempProjectsMap.put("objectSeqId", project.getObjectSeqId());
			status = serviceResponse.getCode() + ": " + serviceResponse.getMessage();
			tempProjectsMap.put("Status", status);
			return tempProjectsMap;
		}

		// Validate Browser Versions : IE
		boolean isIEVersionAllowed = BrowserVersionValidator.checkBrowserVersionIE(serviceResponse,
				systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_BROWSER_IE), project.getObjectSeqId());
		if (!isIEVersionAllowed) {
			tempProjectsMap.put("objectName", project.getObjectName());
			tempProjectsMap.put("objectSeqId", project.getObjectSeqId());
			status = serviceResponse.getCode() + ": " + serviceResponse.getMessage();
			tempProjectsMap.put("Status", status);
			return tempProjectsMap;
		}

		tempProjectsMap.put("objectName", project.getObjectName());
		tempProjectsMap.put("objectSeqId", project.getObjectSeqId());
		tempProjectsMap.put("Status", "OK");
		return tempProjectsMap;

	}
	
	@POST
	@Path(value = "importOutlookMail")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> importOutlookMail(@Context HttpServletRequest request, JSONObject jsonObj) {
		JSONArray jsonArray= null;
		Object mailStatusArray =null;
		Map<String, Object> sciApiResponseMap = new LinkedHashMap<>();
		serviceResponse = new SciApiResponse();
		
		boolean areApisEnabled = WebApiUtils.areWebApisEnabled();
		if (!areApisEnabled) {
			log.error("Web APIs crrently disabled.");
			serviceResponse.setCode(WebErrorCodes.WebServices.WEB_APIS_DISABLED.errorCode());
			serviceResponse
					.setMessage(DisplayMessageHelper.getDisplayMessage(WebErrorCodes.WebServices.WEB_APIS_DISABLED));
			
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}
		
		
		String keyId = WebApiUtils.getRequestParameter(jsonObj, JSON_KEY_KEYID);
		String userId = WebApiUtils.getRequestParameter(jsonObj, JSON_KEY_USERID);
		String password = WebApiUtils.getRequestParameter(jsonObj, JSON_KEY_PASSWORD);
		String clientIqHash = WebApiUtils.getRequestParameter(jsonObj, JSON_KEY_CLIENTIQHASH);
		String jdkVersion = WebApiUtils.getRequestParameter(jsonObj, JSON_KEY_JDKVERSION);
		String MailBoxId = WebApiUtils.getRequestParameter(jsonObj, JSON_KEY_MAILBOXID);
		String l_projectname = WebApiUtils.getRequestParameter(jsonObj, JSON_KEY_PROJNAME);
		
		try {
			jsonArray = WebApiUtils.getRequestArrayParameter(jsonObj, JSON_KEY_MAILARRAY);
			mailStatusArray = MBPortalApp.saveMailByApi(jsonArray, MailBoxId, userId, l_projectname);
		}
		catch(JSONException | SciServiceException jsonExcp)
		{
			log.error("Error getting mail list array");
			serviceResponse.setMessage("Error Generating Mail");
			sciApiResponseMap.put("Response", serviceResponse);
			return sciApiResponseMap;
		}

		sciApiResponseMap.put("Response", mailStatusArray);
		return sciApiResponseMap;
	}
	
}
