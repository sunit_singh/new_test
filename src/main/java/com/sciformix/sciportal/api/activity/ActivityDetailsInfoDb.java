package com.sciformix.sciportal.api.activity;

import java.util.Date;

public class ActivityDetailsInfoDb {

	private int activitySeqId = -1;
	private Date activityDateTime = null;
	private String activityStepCode = null;
	private String activityStepDesc = null;
	private String activityPayload = null;

	public int getActivitySeqId() {
		return activitySeqId;
	}

	public void setActivitySeqId(int activitySeqId) {
		this.activitySeqId = activitySeqId;
	}

	public Date getActivityDateTime() {
		return activityDateTime;
	}

	public void setActivityDateTime(Date activityDateTime) {
		this.activityDateTime = activityDateTime;
	}

	public String getActivityStepCode() {
		return activityStepCode;
	}

	public void setActivityStepCode(String activityStepCode) {
		this.activityStepCode = activityStepCode;
	}

	public String getActivityStepDesc() {
		return activityStepDesc;
	}

	public void setActivityStepDesc(String activityStepDesc) {
		this.activityStepDesc = activityStepDesc;
	}

	public String getActivityPayload() {
		return activityPayload;
	}

	public void setActivityPayload(String activityPayload) {
		this.activityPayload = activityPayload;
	}

	@Override
	public String toString() {
		return "ActivityDetailsInfoDb [activitySeqId=" + activitySeqId + ", activityDateTime=" + activityDateTime
				+ ", activityStepCode=" + activityStepCode + ", activityStepDesc=" + activityStepDesc
				+ ", activityPayload=" + activityPayload + "]";
	}
	
	public ActivityDetailsInfoDb(int p_iActivitySeqId, Date p_dActivityDateTime, String p_sActivityStepCode, String p_sActivityStepDesc,
			String p_sActivityPayload1, String p_sActivityPayload2) {
		
		activitySeqId = p_iActivitySeqId;
		activityDateTime = p_dActivityDateTime;
		activityStepCode = p_sActivityStepCode;
		activityStepDesc = p_sActivityStepDesc;
		activityPayload = p_sActivityPayload1 +" "+ p_sActivityPayload2;
		
	}

	public ActivityDetailsInfoDb() {
		// TODO Auto-generated constructor stub
	}

}
