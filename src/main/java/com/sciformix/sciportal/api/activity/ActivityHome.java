package com.sciformix.sciportal.api.activity;

import java.util.Calendar;
import java.util.List;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;

public class ActivityHome {
	
	private ActivityHome()
	{
		//Nothing to do
	}
	
	public static List<ActivityInfoDb> getUserActivityTrailForUser(DbConnection connection, int p_nUserSeqId, Calendar p_oStartDate, Calendar p_oEndDate,
			boolean p_bFetchAuditDetails, int p_nUserProjectId) throws SciException
	{

		List<ActivityInfoDb> listActivityTrail = null;
		
		listActivityTrail =  ActivityHomeImpl.getUserActivityTrailForUser(connection, p_nUserSeqId, p_oStartDate, null, p_bFetchAuditDetails,p_nUserProjectId);
		
		return listActivityTrail;
		
	}

}
