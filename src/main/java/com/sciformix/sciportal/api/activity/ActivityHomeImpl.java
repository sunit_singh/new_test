package com.sciformix.sciportal.api.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbOperator;

public class ActivityHomeImpl {

	private static final Logger log = LoggerFactory.getLogger(ActivityHomeImpl.class);

	public static int saveActivity(DbConnection connection, ActivityInfoDb activityInfo) throws SciServiceException, SciException {

		int nReturnValue = -1;
		int nRowCount = -1;
		
		DbQueryInsert oActivityInsertQuery = null;
				
		int nActivitySeqId = -1;
		nActivitySeqId = DbQueryHome.generateSequenceValueForTablePk(ActivityDbSchema.ACTIVITY);
		
		oActivityInsertQuery = getActivityInsertQuery(nActivitySeqId, activityInfo);
				
		try{
			
			connection.startTransaction();
			nRowCount = connection.executeQuery(oActivityInsertQuery);
			connection.commitTransaction();
			
		} catch (SciDbException excp){
			connection.rollbackTransaction();
			log.error("Error saving audit object", excp);
			throw new SciException("Error saving audit object", excp);
		}
		
		if(nRowCount > 0){
			nReturnValue = nActivitySeqId;
		}
		return nReturnValue;
	}
	
	public static int saveActivityDetails(DbConnection connection, ActivityDetailsInfoDb activityDetails,
			int currParentActivityId) throws SciException {
		int nReturnValue = -1;
		int nRowCount = -1;
		DbQueryInsert oActivityDetailsInsertQuery = null;

		String sPayload1 = null;
		String sPayload2 = null;
		String[] sarrPayloadFragments = null;

		sarrPayloadFragments = DataUtils.splitForDb(activityDetails.getActivityPayload());
		if (sarrPayloadFragments.length != 0) {
			for (int i = 0; i < sarrPayloadFragments.length; i++) {
				sPayload1 = sarrPayloadFragments[i];
				i++;
				sPayload2 = (sarrPayloadFragments.length > i ? sarrPayloadFragments[i] : null);
			}
		}

		oActivityDetailsInsertQuery = getActivityDetailsInsertQuery(currParentActivityId, activityDetails, sPayload1,
				sPayload2);

		try {

			connection.startTransaction();
			nRowCount = connection.executeQuery(oActivityDetailsInsertQuery);
			connection.commitTransaction();

		} catch (SciDbException excp) {
			connection.rollbackTransaction();
			log.error("Error saving audit object", excp);
			throw new SciException("Error saving audit object", excp);
		}
		
		if(nRowCount > 0){
			nReturnValue = currParentActivityId;
		}

		return nReturnValue;
	}
	
	private static DbQueryInsert getActivityInsertQuery(int activitySeqId, ActivityInfoDb activityInfo){
		DbQueryInsert oActivityInsertQuery = DbQueryUtils.createInsertQuery(ActivityDbSchema.ACTIVITY);
		
		oActivityInsertQuery.addInsertParameter(activitySeqId);
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityDateTime());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityUserSeqId());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityUserId());
		oActivityInsertQuery.addInsertParameter(activityInfo.getProjectSeqId());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityServer());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityApp());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityType());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityCode());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityClientIp());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityClientHost());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityClientHostDomain());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityClientDateTime());
		oActivityInsertQuery.addInsertParameter(activityInfo.getActivityClientTimeZone());
		oActivityInsertQuery.addInsertParameter(activityInfo.getMachineLoginUser());
		oActivityInsertQuery.addInsertParameter(activityInfo.getMachineLoginUserDomain());
		oActivityInsertQuery.addInsertParameter(activityInfo.getSystemPayload());
		
		return oActivityInsertQuery;
	}
	
	private static DbQueryInsert getActivityDetailsInsertQuery(int activitySeqId, ActivityDetailsInfoDb activityDetail,
			String payload1, String payload2) {
		DbQueryInsert oActivityDetailsInsertQuery = DbQueryUtils.createInsertQuery(ActivityDbSchema.ACTIVITYDETAILS);

		oActivityDetailsInsertQuery.addInsertParameter(activitySeqId);
		oActivityDetailsInsertQuery.addInsertParameter(activityDetail.getActivityDateTime());
		oActivityDetailsInsertQuery.addInsertParameter(activityDetail.getActivityStepCode());
		oActivityDetailsInsertQuery.addInsertParameter(activityDetail.getActivityStepDesc());
		oActivityDetailsInsertQuery.addInsertParameter(payload1);
		oActivityDetailsInsertQuery.addInsertParameter(payload2);

		return oActivityDetailsInsertQuery;
	}
	
	public static List<ActivityInfoDb> getActivityListByActivitySeqId(DbConnection connection,
			int activitySeqId) throws SciServiceException {

		List<ActivityInfoDb> activityList = new ArrayList<ActivityInfoDb>();
		
		DbQuerySelect oSelectQuery = null;
		String sWhereClause = " ACTIVITYSEQID = ?";
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ActivityDbSchema.ACTIVITY, sWhereClause);
		oSelectQuery.addWhereClauseParameter(activitySeqId);

		try (DbResultSet resultSet = connection.executeQuery(oSelectQuery)) {
			while (resultSet.next()) {
				activityList.add(readActivityObject(resultSet));
			}
		} catch (SciDbException dbExcep) {
			connection.rollbackTransaction();
			log.error("Error fetching Activity details :", dbExcep);
		}
		
		return activityList;
		
	}
	
	private static ActivityInfoDb readActivityObject(DbResultSet resultset) throws SciDbException {
		ActivityInfoDb activity = new ActivityInfoDb();
		
		activity.setActivitySeqId(resultset.readInt(ActivityDbSchema.ACTIVITY.ACTIVITYSEQID));
		activity.setActivityDateTime(resultset.readDate(ActivityDbSchema.ACTIVITY.ACTIVITYDATETIME));
		activity.setActivityUserSeqId(resultset.readInt(ActivityDbSchema.ACTIVITY.ACTIVITYUSERSEQID));
		activity.setActivityUserId(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYUSERID));
		activity.setProjectSeqId(resultset.readInt(ActivityDbSchema.ACTIVITY.PROJECTSEQID));
		activity.setActivityServer(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYSERVER));
		activity.setActivityApp(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYAPP));
		activity.setActivityType(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYTYPE));
		activity.setActivityCode(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYCODE));
		activity.setActivityClientIp(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYCLIENTIP));
		activity.setActivityClientHost(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYCLIENTHOST));
		activity.setActivityClientHost(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYCLIENTHOSTDOMAIN));
		activity.setActivityClientDateTime(resultset.readDate(ActivityDbSchema.ACTIVITY.ACTIVITYCLIENTDATETIME));
		activity.setActivityClientTimeZone(resultset.readString(ActivityDbSchema.ACTIVITY.ACTIVITYCLIENTTIMEZONE));
		activity.setMachineLoginUser(resultset.readString(ActivityDbSchema.ACTIVITY.MACHINELOGINUSER));
		activity.setMachineLoginUserDomain(resultset.readString(ActivityDbSchema.ACTIVITY.MACHINELOGINUSERDOMAIN));
		activity.setSystemPayload(resultset.readString(ActivityDbSchema.ACTIVITY.SYSTEMPAYLOAD));
		
		return activity;
	}
	
	/**
	 * This method is used for getting Activity trail for User
	 * @param connection
	 * @param p_nUserSeqId
	 * @param p_oStartDate
	 * @param p_oEndDate
	 * @param p_bFetchActivityDetails
	 * @param p_nUserProjectId
	 * @return  List AuditInfoDb
	 * @throws SciException
	 */	
	public static List<ActivityInfoDb> getUserActivityTrailForUser(DbConnection connection, int p_nUserSeqId, Calendar p_oStartDate, Calendar p_oEndDate,
			boolean p_bFetchActivityDetails, int p_nUserProjectId) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		
		if(p_oEndDate != null)
		{
				oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ActivityDbSchema.ACTIVITY,
							DbQueryUtils.bindWhereClauseWithAnd(ActivityDbSchema.ACTIVITY.PROJECTSEQID.bindClause() ,
							ActivityDbSchema.ACTIVITY.ACTIVITYUSERSEQID.bindClause() ,
							ActivityDbSchema.ACTIVITY.ACTIVITYDATETIME.bindClause(DbOperator.GREATER_THAN_EQUAL_TO),
							ActivityDbSchema.ACTIVITY.ACTIVITYDATETIME.bindClause(DbOperator.LESS_THAN_EQUAL_TO)
					),"ACTIVITYDATETIME DESC");
				oSelectQuery.addWhereClauseParameter(p_nUserProjectId);
				oSelectQuery.addWhereClauseParameter(p_nUserSeqId);
				oSelectQuery.addWhereClauseParameter(p_oStartDate.getTime());
				oSelectQuery.addWhereClauseParameter(p_oEndDate.getTime());
		}
			
		else
		{
				oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ActivityDbSchema.ACTIVITY,
							DbQueryUtils.bindWhereClauseWithAnd(ActivityDbSchema.ACTIVITY.PROJECTSEQID.bindClause() ,
							ActivityDbSchema.ACTIVITY.ACTIVITYUSERSEQID.bindClause() ,
							ActivityDbSchema.ACTIVITY.ACTIVITYDATETIME.bindClause(DbOperator.GREATER_THAN_EQUAL_TO)
					),"ACTIVITYDATETIME DESC, ACTIVITYSEQID DESC");
				oSelectQuery.addWhereClauseParameter(p_nUserProjectId);
				oSelectQuery.addWhereClauseParameter(p_nUserSeqId);
				oSelectQuery.addWhereClauseParameter(p_oStartDate.getTime());
			
		}	

		
		return getActivityTrail(connection, oSelectQuery, p_bFetchActivityDetails);
	}
	
	private static List<ActivityInfoDb> getActivityTrail(DbConnection connection, DbQuerySelect oSelectQuery, boolean p_bFetchActivityDetails) throws SciException
	{
		List<ActivityInfoDb> listActivityTrail = null;
		DbQuerySelect oSelectQueryDetails = null;
		
		listActivityTrail = fetchUserActivityTrail(connection, oSelectQuery);
		
		if (p_bFetchActivityDetails)
		{
			int nHighestActivitySeqId = -1;
			int nLowestActivitySeqId = -1;
			String sWhereClauseDetails = null;
			
			if (listActivityTrail.size() > 0)
			{
				nHighestActivitySeqId = listActivityTrail.get(0).getActivitySeqId();
				nLowestActivitySeqId = ((listActivityTrail.size() > 1) ? listActivityTrail.get(listActivityTrail.size()-1).getActivitySeqId() : nHighestActivitySeqId); 
				
				sWhereClauseDetails = " ACTIVITYSEQID BETWEEN ? AND ? ";
				
				oSelectQueryDetails = DbQueryUtils.createSelectAllColumnsQuery(ActivityDbSchema.ACTIVITYDETAILS, sWhereClauseDetails, "ACTIVITYSEQID, ACTIVITYDATETIME ASC");
				oSelectQueryDetails.addWhereClauseParameter(nLowestActivitySeqId);
				oSelectQueryDetails.addWhereClauseParameter(nHighestActivitySeqId);
				
				TreeMap<Integer, ActivityInfoDb> smapActivityTrail = new TreeMap<Integer, ActivityInfoDb>();
				for (ActivityInfoDb oActivityInfoDb : listActivityTrail)
				{
					smapActivityTrail.put(oActivityInfoDb.getActivitySeqId(), oActivityInfoDb);
				}
				
				
				fetchAndFillUserActivityDetails(connection, smapActivityTrail, oSelectQueryDetails);
				
//				listActivityTrail = (List<ActivityInfoDb>)  smapActivityTrail.values();
			}
		}
		
		return listActivityTrail;
	}
	
	private static List<ActivityInfoDb> fetchUserActivityTrail(DbConnection connection, DbQuerySelect p_oSelectQuery) throws SciException
	{
		 List<ActivityInfoDb> listActivityTrail = null;
		 
		 ActivityInfoDb oActivityInfo = null;
		 
		 listActivityTrail = new ArrayList<ActivityInfoDb>();
		 
		 try
		 {
			 connection.startTransaction();
		 
			 try (DbResultSet resultSet = connection.executeQuery(p_oSelectQuery))
			 {
				 while (resultSet.next())
				 {
					 oActivityInfo = readActivityObject(resultSet);
					 
					 listActivityTrail.add(oActivityInfo);
				 }
			 }
			 
			 connection.commitTransaction();
		 }
		 catch (SciDbException dbExcep)
		 {
			 connection.rollbackTransaction();
			 log.error("Error fetching user activity trail", dbExcep);
			 throw new SciException("Error fetching activity object", dbExcep);
		 }
		 
		 return listActivityTrail;
	}
	
	private static void fetchAndFillUserActivityDetails(DbConnection connection, SortedMap<Integer, ActivityInfoDb> p_smapActivityTrail, DbQuerySelect p_oSelectQuery) throws SciException
	{
		 ActivityDetailsInfoDb oActivityDetailsInfo = null;
		 int nActivitySeqId = -1;
		 int nActivityTrailListNavigatorIndex = -1;
		 int nTempIndex = -1;
		 
		 try
		 {
			 connection.startTransaction();
			 
			 nActivityTrailListNavigatorIndex = 0;
			 
			 try (DbResultSet resultSet = connection.executeQuery(p_oSelectQuery))
			 {
				 while (resultSet.next())
				 {
					 nActivitySeqId = resultSet.readInt(ActivityDbSchema.ACTIVITYDETAILS.ACTIVITYSEQID);
					 oActivityDetailsInfo = readActivityDetailsObject(resultSet);
					 
					 if (p_smapActivityTrail.containsKey(nActivitySeqId))
					 {
						 p_smapActivityTrail.get(nActivitySeqId).addDetails(oActivityDetailsInfo);
					 }
					 
					//findActivityObjectInList(p_smapActivityTrail, nActivitySeqId, nActivityTrailListNavigatorIndex);
//					 if (nTempIndex != -1)
//					 {
//						 nActivityTrailListNavigatorIndex = nTempIndex;
//						 p_listActivityTrail.get(nActivityTrailListNavigatorIndex).addDetails(oActivityDetailsInfo);
//					 }
				 }
			 }
			 
			 connection.commitTransaction();
		 }
		 catch (SciDbException dbExcep)
		 {
			 connection.rollbackTransaction();
			 log.error("Error fetching and filling user Activity trail", dbExcep);
			 throw new SciException("Error fetching and filling user Activity trail", dbExcep);
		}
	}
	
	private static int findActivityObjectInList(List<ActivityInfoDb> p_listActivityTrail, int p_nActivitySeqId, int p_nActivityTrailListNavigatorIndex)
	{
		for (int nTempCounter = p_nActivityTrailListNavigatorIndex; nTempCounter < p_listActivityTrail.size(); nTempCounter++)
//			for (int nTempCounter = 0; nTempCounter < p_listActivityTrail.size(); nTempCounter++)
		{
			if (p_listActivityTrail.get(nTempCounter).getActivitySeqId() == p_nActivitySeqId)
			{
				return nTempCounter;
			}
			else if (p_listActivityTrail.get(nTempCounter).getActivitySeqId() < p_nActivitySeqId)
			{
				//Record not found
				return -1;
			}
		}
		return -1;
	}
	
	private static ActivityDetailsInfoDb readActivityDetailsObject(DbResultSet resultset) throws SciDbException
	{
		ActivityDetailsInfoDb oActivityDetailsInfo = null;
		
		oActivityDetailsInfo = new ActivityDetailsInfoDb();
		oActivityDetailsInfo.setActivityDateTime(resultset.readDate(ActivityDbSchema.ACTIVITYDETAILS.ACTIVITYDATETIME));
		oActivityDetailsInfo.setActivitySeqId(resultset.readInt(ActivityDbSchema.ACTIVITYDETAILS.ACTIVITYSEQID));
		oActivityDetailsInfo.setActivityStepCode(resultset.readString(ActivityDbSchema.ACTIVITYDETAILS.ACTIVITYSTEPCODE));
		oActivityDetailsInfo.setActivityStepDesc(resultset.readString(ActivityDbSchema.ACTIVITYDETAILS.ACTIVITYSTEPDESC));
		oActivityDetailsInfo.setActivityPayload(resultset.readString(ActivityDbSchema.ACTIVITYDETAILS.ACTIVITYPAYLOAD1));		
		
		return oActivityDetailsInfo;
	}

}
