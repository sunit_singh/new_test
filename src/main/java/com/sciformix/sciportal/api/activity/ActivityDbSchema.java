package com.sciformix.sciportal.api.activity;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class ActivityDbSchema {

	public static Activity ACTIVITY = new Activity();
	public static ActivityDetails ACTIVITYDETAILS = new ActivityDetails();

	public static class Activity extends DbTable {

		public DbColumn ACTIVITYSEQID = null;
		public DbColumn ACTIVITYDATETIME = null;
		public DbColumn ACTIVITYUSERSEQID = null;
		public DbColumn ACTIVITYUSERID = null;
		public DbColumn PROJECTSEQID = null;
		public DbColumn ACTIVITYSERVER = null;
		public DbColumn ACTIVITYAPP = null;
		public DbColumn ACTIVITYTYPE = null;
		public DbColumn ACTIVITYCODE = null;
		public DbColumn ACTIVITYCLIENTIP = null;
		public DbColumn ACTIVITYCLIENTHOST = null;
		public DbColumn ACTIVITYCLIENTHOSTDOMAIN = null;
		public DbColumn ACTIVITYCLIENTDATETIME = null;
		public DbColumn ACTIVITYCLIENTTIMEZONE = null;
		public DbColumn MACHINELOGINUSER = null;
		public DbColumn MACHINELOGINUSERDOMAIN = null;
		public DbColumn SYSTEMPAYLOAD = null;

		public Activity() {
			super("ADT_ACTIVITY");
			setPkSequenceName("ADT_ACTIVITYSEQID_SEQ");

			ACTIVITYSEQID = addColumn("ACTIVITYSEQID");
			ACTIVITYDATETIME = addColumn("ACTIVITYDATETIME");
			ACTIVITYUSERSEQID = addColumn("ACTIVITYUSERSEQID");
			ACTIVITYUSERID = addColumn("ACTIVITYUSERID");
			PROJECTSEQID = addColumn("PROJECTSEQID");
			ACTIVITYSERVER = addColumn("ACTIVITYSERVER");
			ACTIVITYAPP = addColumn("ACTIVITYAPP");
			ACTIVITYTYPE = addColumn("ACTIVITYTYPE");
			ACTIVITYCODE = addColumn("ACTIVITYCODE");
			ACTIVITYCLIENTIP = addColumn("ACTIVITYCLIENTIP");
			ACTIVITYCLIENTHOST = addColumn("ACTIVITYCLIENTHOST");
			ACTIVITYCLIENTHOSTDOMAIN = addColumn("ACTIVITYCLIENTHOSTDOMAIN");
			ACTIVITYCLIENTDATETIME = addColumn("ACTIVITYCLIENTDATETIME");
			ACTIVITYCLIENTTIMEZONE = addColumn("ACTIVITYCLIENTTIMEZONE");
			MACHINELOGINUSER = addColumn("MACHINELOGINUSER");
			MACHINELOGINUSERDOMAIN = addColumn("MACHINELOGINUSERDOMAIN");
			SYSTEMPAYLOAD = addColumn("SYSTEMPAYLOAD");
			
		}
	}

	public static class ActivityDetails extends DbTable {

		public DbColumn ACTIVITYSEQID = null;
		public DbColumn ACTIVITYDATETIME = null;
		public DbColumn ACTIVITYSTEPCODE = null;
		public DbColumn ACTIVITYSTEPDESC = null;
		public DbColumn ACTIVITYPAYLOAD1 = null;
		public DbColumn ACTIVITYPAYLOAD2 = null;

		public ActivityDetails() {
			super("ADT_ACTIVITY_DETAILS");
			setPkSequenceName(null);

			ACTIVITYSEQID = addColumn("ACTIVITYSEQID");
			ACTIVITYDATETIME = addColumn("ACTIVITYDATETIME");
			ACTIVITYSTEPCODE = addColumn("ACTIVITYSTEPCODE");
			ACTIVITYSTEPDESC = addColumn("ACTIVITYSTEPDESC");
			ACTIVITYPAYLOAD1 = addColumn("ACTIVITYPAYLOAD1");
			ACTIVITYPAYLOAD2 = addColumn("ACTIVITYPAYLOAD2");
		}

	}

}
