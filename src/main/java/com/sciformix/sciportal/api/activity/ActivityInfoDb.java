package com.sciformix.sciportal.api.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityInfoDb {

	private int activitySeqId = -1;
	private Date activityDateTime = null;
	private int activityUserSeqId = -1;
	private String activityUserId = null;
	private int projectSeqId = -1;
	private String activityServer = null;
	private String activityApp = null;
	private String activityType = null;
	private String activityCode = null;
	private String activityClientIp = null;
	private String activityClientHost = null;
	private String activityClientHostDomain = null;
	private Date activityClientDateTime = null;
	private String activityClientTimeZone = null;
	private String machineLoginUser = null;
	private String machineLoginUserDomain = null;
	private String systemPayload = null;


	private List<ActivityDetailsInfoDb> m_listactivityDetails = null;

	public ActivityInfoDb() {
		m_listactivityDetails = new ArrayList<ActivityDetailsInfoDb>();
	}

	public int getActivitySeqId() {
		return activitySeqId;
	}

	public void setActivitySeqId(int activitySeqId) {
		this.activitySeqId = activitySeqId;
	}

	public Date getActivityDateTime() {
		return activityDateTime;
	}

	public void setActivityDateTime(Date activityDateTime) {
		this.activityDateTime = activityDateTime;
	}

	public String getActivityClientTimeZone() {
		return activityClientTimeZone;
	}

	public void setActivityClientTimeZone(String activityClientTimeZone) {
		this.activityClientTimeZone = activityClientTimeZone;
	}

	public int getActivityUserSeqId() {
		return activityUserSeqId;
	}

	public void setActivityUserSeqId(int activityUserSeqId) {
		this.activityUserSeqId = activityUserSeqId;
	}

	public String getActivityUserId() {
		return activityUserId;
	}

	public void setActivityUserId(String activityUserId) {
		this.activityUserId = activityUserId;
	}

	public int getProjectSeqId() {
		return projectSeqId;
	}

	public void setProjectSeqId(int projectSeqId) {
		this.projectSeqId = projectSeqId;
	}

	public String getActivityServer() {
		return activityServer;
	}

	public void setActivityServer(String activityServer) {
		this.activityServer = activityServer;
	}

	public String getActivityApp() {
		return activityApp;
	}

	public void setActivityApp(String activityApp) {
		this.activityApp = activityApp;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}

	public String getActivityClientIp() {
		return activityClientIp;
	}

	public void setActivityClientIp(String activityClientIp) {
		this.activityClientIp = activityClientIp;
	}

	public String getActivityClientHost() {
		return activityClientHost;
	}

	public void setActivityClientHost(String activityClientHost) {
		this.activityClientHost = activityClientHost;
	}

	public String getActivityClientHostDomain() {
		return activityClientHostDomain;
	}

	public void setActivityClientHostDomain(String activityClientHostDomain) {
		this.activityClientHostDomain = activityClientHostDomain;
	}

	public String getMachineLoginUser() {
		return machineLoginUser;
	}

	public void setMachineLoginUser(String machineLoginUser) {
		this.machineLoginUser = machineLoginUser;
	}

	public String getMachineLoginUserDomain() {
		return machineLoginUserDomain;
	}

	public void setMachineLoginUserDomain(String machineLoginUserDomain) {
		this.machineLoginUserDomain = machineLoginUserDomain;
	}

	public String getSystemPayload() {
		return systemPayload;
	}

	public void setSystemPayload(String systemPayload) {
		this.systemPayload = systemPayload;
	}

	public void addDetails(int p_iActivitySeqId, Date p_dActivityDateTime, String p_sActivityStepCode,
			String p_sActivityStepDesc, String p_sActivityPayload1, String p_sActivityPayload2) {
		m_listactivityDetails.add(new ActivityDetailsInfoDb(p_iActivitySeqId, p_dActivityDateTime, p_sActivityStepCode,
				p_sActivityStepDesc, p_sActivityPayload1, p_sActivityPayload2));
	}

	void addDetails(ActivityDetailsInfoDb p_oActivityDetails) {
		m_listactivityDetails.add(p_oActivityDetails);
	}

	public List<ActivityDetailsInfoDb> getDetails() {
		return m_listactivityDetails;
	}
	
	public Date getActivityClientDateTime() {
		return activityClientDateTime;
	}

	public void setActivityClientDateTime(Date activityClientDateTime) {
		this.activityClientDateTime = activityClientDateTime;
	}

	@Override
	public String toString() {
		return "ActivityInfoDb [activitySeqId=" + activitySeqId + ", activityDateTime=" + activityDateTime
				+ ", activityUserSeqId=" + activityUserSeqId + ", activityUserId=" + activityUserId + ", projectSeqId="
				+ projectSeqId + ", activityServer=" + activityServer + ", activityApp=" + activityApp
				+ ", activityType=" + activityType + ", activityCode=" + activityCode + ", activityClientIp="
				+ activityClientIp + ", activityClientHost=" + activityClientHost + ", activityClientHostDomain="
				+ activityClientHostDomain + ", activityClientDateTime=" + activityClientDateTime
				+ ", activityClientTimeZone=" + activityClientTimeZone + ", machineLoginUser=" + machineLoginUser
				+ ", machineLoginUserDomain=" + machineLoginUserDomain + ", systemPayload=" + systemPayload
				+ ", m_listactivityDetails=" + m_listactivityDetails + "]";
	}
	
}
