package com.sciformix.sciportal.api.model;

public class SciApiResponse
{

	private int code = -1;
	private String message = "";

	public SciApiResponse()
	{
		//Nothing to do
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "SciApiResponse [code=" + code + ", message=" + message + "]";
	}

}
