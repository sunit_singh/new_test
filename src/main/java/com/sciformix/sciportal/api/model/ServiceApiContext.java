package com.sciformix.sciportal.api.model;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class ServiceApiContext
{
	public static final String CONTEXT_VARIABLE_CLIENT = "Client";
	public static final String CONTEXT_VARIABLE_TOOLNAME = "ToolName";
	public static final String CONTEXT_VARIABLE_TOOLVERSION = "ToolVersion";
	public static final String CONTEXT_VARIABLE_CLIENTREQID = "ClientReqID";
	public static final String CONTEXT_VARIABLE_TIMESTAMP = "Timestamp";
	public static final String CONTEXT_VARIABLE_TIMEZONE = "Timezone";
	
	public static final String CONTEXT_VARIABLE_SYSTEMPAYLOAD = "SystemPayload";
	public static final String CONTEXT_VARIABLE_MACHINENAME = "MachineName";
	public static final String CONTEXT_VARIABLE_MACHINEDOMAIN = "MachineDomain";
	public static final String CONTEXT_VARIABLE_IPADDRESS = "IPAddress";
	public static final String CONTEXT_VARIABLE_MACHINELOGGEDINUSER = "MachineLoggedInUser";
	public static final String CONTEXT_VARIABLE_MACHINELOGGEDINUSERDOMAIN = "MachineLoggedInUserDomain";
	public static final String CONTEXT_VARIABLE_OSNAME = "OSName";
	public static final String CONTEXT_VARIABLE_OSVERSION = "OSVersion";
	public static final String CONTEXT_VARIABLE_OSARCHITECTURE = "OSArchitecture";
	public static final String CONTEXT_VARIABLE_JDKVERSION = "JDKVersion";
	public static final String CONTEXT_VARIABLE_BROWSER_CHROME = "ChromeVersion";
	public static final String CONTEXT_VARIABLE_BROWSER_IE = "IEVersion";
	
	
	private Map<String, String> m_mapClientParameters = null;
	private Map<String, String> m_mapSystemPayload = null;
	
	
	public Map<String, String> getClientContextParameters(JSONObject requestJson) throws JSONException{
		m_mapClientParameters = new HashMap<String, String>();
		JSONObject clientJson = requestJson.getJSONObject(CONTEXT_VARIABLE_CLIENT);
		
		String toolName = clientJson.optString(CONTEXT_VARIABLE_TOOLNAME, "");
		String toolversion = clientJson.optString(CONTEXT_VARIABLE_TOOLVERSION, "");
		String clientReqId = clientJson.optString(CONTEXT_VARIABLE_CLIENTREQID, "");
		String timeStamp = clientJson.optString(CONTEXT_VARIABLE_TIMESTAMP, "");
		String timeZone = clientJson.optString(CONTEXT_VARIABLE_TIMEZONE, "");

		m_mapClientParameters.put(CONTEXT_VARIABLE_TOOLNAME, toolName);
		m_mapClientParameters.put(CONTEXT_VARIABLE_TOOLVERSION, toolversion);
		m_mapClientParameters.put(CONTEXT_VARIABLE_CLIENTREQID, clientReqId);
		m_mapClientParameters.put(CONTEXT_VARIABLE_TIMESTAMP, timeStamp);
		m_mapClientParameters.put(CONTEXT_VARIABLE_TIMEZONE, timeZone);
		
		return m_mapClientParameters;
	}
	
	public Map<String, String> getSystemPayloadContextParameters(JSONObject requestJson) throws JSONException{
		m_mapSystemPayload = new HashMap<String, String>();
		JSONObject sysPayloadJson = requestJson.getJSONObject(CONTEXT_VARIABLE_SYSTEMPAYLOAD);
		
		String machineName = sysPayloadJson.optString(CONTEXT_VARIABLE_MACHINENAME, "");
		String machineDomain = sysPayloadJson.optString(CONTEXT_VARIABLE_MACHINEDOMAIN, "");
		String ipAddress = sysPayloadJson.optString(CONTEXT_VARIABLE_IPADDRESS, "");
		String machineloggedInUser = sysPayloadJson.optString(CONTEXT_VARIABLE_MACHINELOGGEDINUSER, "");
		String machineloggedInUserDomain = sysPayloadJson.optString(CONTEXT_VARIABLE_MACHINELOGGEDINUSERDOMAIN, "");
		String osName = sysPayloadJson.optString(CONTEXT_VARIABLE_OSNAME, "");
		String osVersion = sysPayloadJson.optString(CONTEXT_VARIABLE_OSVERSION, "");
		String osArchitecture = sysPayloadJson.optString(CONTEXT_VARIABLE_OSARCHITECTURE, "");
		String jdkVersion = sysPayloadJson.optString(CONTEXT_VARIABLE_JDKVERSION, "");
		String chromeVersion = sysPayloadJson.optString(CONTEXT_VARIABLE_BROWSER_CHROME, "");
		String ieVersion = sysPayloadJson.optString(CONTEXT_VARIABLE_BROWSER_IE, "");
		
		m_mapSystemPayload.put(CONTEXT_VARIABLE_MACHINENAME, machineName);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_MACHINEDOMAIN, machineDomain);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_IPADDRESS, ipAddress);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_MACHINELOGGEDINUSER, machineloggedInUser);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_MACHINELOGGEDINUSERDOMAIN, machineloggedInUserDomain);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_OSNAME, osName);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_OSVERSION, osVersion);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_OSARCHITECTURE, osArchitecture);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_JDKVERSION, jdkVersion);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_BROWSER_CHROME, chromeVersion);
		m_mapSystemPayload.put(CONTEXT_VARIABLE_BROWSER_IE, ieVersion);
		
		return m_mapSystemPayload;
	}
	
}
