/**
 * 
 */
package com.sciformix.sciportal.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.CryptoUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.SciPortal;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.user.UserSession;
/**
 * @author sshetty1
 *
 */
public class SystemUtils 
{
	private static final Logger log = LoggerFactory.getLogger(SystemUtils.class);
	
	public enum SYSTEM_CONFIG
	{
		SYSTEM_BOOTSTRAPPED("sciportal.system.bootstrapped"),
		SYSTEM_ENVIRONMENT_TYPE("sciportal.system.environment"),
		LOGIN_USERID_REGEX("SYSTEM.sciportal.login.userid.regex"),
		EMAIL_RETRY_COUNT("SYSTEM.email.retry.count"),
		EMAIL_THREAD_SLEEP_TIME("SYSTEM.email.thread.sleep"),
		CMN_FIELD_VALIDATION_REGEX("SYSTEM.common.validation.regex");
		private String value;
		
		SYSTEM_CONFIG(String value)
		{
			this.value = value;
		}
		
		public String toString()
		{
			return this.value;
		}
		
		public static SYSTEM_CONFIG toEnum(int value)
		{
			SYSTEM_CONFIG[] values = SYSTEM_CONFIG.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (values[counter].toString().equals(value))
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("SYSTEM_CONFIG: Illegal enum value - " + value);
		}
	}
	
	private static final String _PROPERTY_SCIPORTAL_DATABASE_HOST = "sciportal.database.host";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_PORTNO = "sciportal.database.portno";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_INSTANCE = "sciportal.database.instance";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_NAME = "sciportal.database.connectuser.name";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_PASSWORD = "sciportal.database.connectuser.password";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_SCHEMAOWNER_NAME = "sciportal.database.schemaowner.name";
	
	private static String DEFAULT_SCHEMA_OWNER = "SCFX_SO";
	
	public static final String SYS_CONFIG_KEYCATEGORY = "SYSTEM";
	
	public static final String PRIMARY_PROPERTIES_FILE_NAME = "sciportal.properties";
	
	public static final String EXTENDED_PROPERTIES_FILE_NAME = "sciportal-ext.properties";
	
	/*Added by Jason : START*/
	public static final String TOOLS_PROPERTIES_FILE_NAME = "sciportal-webapi.properties";
	/*Added by Jason : END*/
	
	public static final String ENVIRONMENT_VARIABLE_INSTALL_PATH = "SCIPORTAL_INSTALL_DIR";
	
	private static final String PROP_ENCR_KEY_DB = "sciportal.bootstrap.encr.key";
	
	private static final String INSTALL_SUBFOLDER_CONFIGURATION = "conf";
	private static final String INSTALL_SUBFOLDER_TEMP = "temp";
	private static final String INSTALL_SUBFOLDER_LOGS = "logs";
	
	private static final String ENCR_KEY_SYSTEM = "(@&WS:>9HB`#_=~0";

	private static final String ENCR_KEY_DB = "2$%#$^Gd)!";

	private static final String SYS_CONFIG_KEYNAME = "sciportal.system.encr.key";
	
	private static String m_sInstallPath = null;
	private static String m_sConfigurationFolderPath = null;
	private static String m_sTempFolderPath = null;
	private static String m_sLogFolderPath = null;
	
	
	public static String getInstallPath()
	{
		return m_sInstallPath;
		}
		
	/**
	 * This method will set the install path for SciPortal.
	 * SciPortal Directory contains the configuration and
	 * log files location.
	 * @param p_sSciPortalDir : SciPortal Directory
	 */
	public static void setInstallPath (String p_sSciPortalDir) {
		m_sInstallPath	=	p_sSciPortalDir;
	}
	
	public static String createUserTempFolder(String p_sUserIdentifier) throws IOException
	{
		String sUserTempFolder = null;
		
		sUserTempFolder = FilenameUtils.concat(SystemUtils.getTempFolderPath(), p_sUserIdentifier);
		com.sciformix.commons.utils.FileUtils.createDirectory(sUserTempFolder);
		
		return sUserTempFolder;
	}
	
	
	public static boolean createExtendedPropertyFile(String p_sDBHost, String p_sDBPort, String p_sDBSID, 
										String p_sDBUsername, String p_sDBPassword, String p_sDBSchemaOwner) throws SciException
	{
		String sPropFilePath = null;
		boolean bFileCreated = false;
		FileOutputStream oPropFileOutputStream = null;
		Properties oProps = null;
		byte[] barrKey = null;
		
		sPropFilePath = getExtendedPropFilePath();
		try 
		{
			barrKey = CryptoUtils.generateKey();
			
			oProps = new Properties();
			oProps.setProperty(_PROPERTY_SCIPORTAL_DATABASE_HOST, p_sDBHost);
			oProps.setProperty(_PROPERTY_SCIPORTAL_DATABASE_PORTNO, p_sDBPort);
			oProps.setProperty(_PROPERTY_SCIPORTAL_DATABASE_INSTANCE, p_sDBSID);
			oProps.setProperty(_PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_NAME, p_sDBUsername);
			oProps.setProperty(_PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_PASSWORD, CryptoUtils.encryptToString(p_sDBPassword, barrKey));
			oProps.setProperty(PROP_ENCR_KEY_DB, CryptoUtils.encryptToString(barrKey, getDBKey(p_sDBHost)));
			oProps.setProperty(_PROPERTY_SCIPORTAL_DATABASE_SCHEMAOWNER_NAME, p_sDBSchemaOwner);
			
			
			oPropFileOutputStream = new FileOutputStream(sPropFilePath);
			oProps.store(oPropFileOutputStream, null);
			bFileCreated = true;
		}
		catch (IOException ioExcep) 
		{
			//Delete the file which might have been created. System state should revert to pending Bootstrap
			log.error("Error creating extended prop file", ioExcep);
			throw new SciException("Error creating extended prop file", ioExcep);
		}
		finally
		{
			if(oPropFileOutputStream != null)
			{
				try
				{
					oPropFileOutputStream.close();
				}
				catch (IOException ioExcep) 
				{
					log.error("Error closing file - ignore and continue", ioExcep);
				}	
			}
			else
			{
				//Do nothing
			}
		}
      	return bFileCreated;
	}
	
	public static boolean deleteExtendedPropertyFile() throws SciException
	{
		return FileUtils.deleteQuietly(new File(getExtendedPropFilePath()));
	}
	
	public static String getExtendedPropFilePath()
	{
		return FilenameUtils.concat(getConfigFolderPath(), EXTENDED_PROPERTIES_FILE_NAME);
	}
	
	public static String getConfigFolderPath()
	{
		if(m_sConfigurationFolderPath == null)
		{
			m_sConfigurationFolderPath = FilenameUtils.concat(getInstallPath(), INSTALL_SUBFOLDER_CONFIGURATION);
		}
		
		return m_sConfigurationFolderPath;
	}	
	
	public static String getTempFolderPath()
	{
		if(m_sTempFolderPath == null)
		{
			m_sTempFolderPath = FilenameUtils.concat(getInstallPath(), INSTALL_SUBFOLDER_TEMP);
		}
		
		return m_sTempFolderPath;
	}
	
	public static String getLogFolderPath()
	{
		if(m_sLogFolderPath == null)
		{
			m_sLogFolderPath = FilenameUtils.concat(getInstallPath(), INSTALL_SUBFOLDER_LOGS);
		}
		return m_sLogFolderPath;
	}
	
	
	public static void storeSystemEncryptionKey(DbConnection p_oConnection, UserSession p_oUserSession, byte[] p_barrKey) throws SciException
	{
		ConfigHome.updateSysConfig(p_oConnection, p_oUserSession, SYS_CONFIG_KEYCATEGORY, SYS_CONFIG_KEYNAME, CryptoUtils.encryptToString(p_barrKey, ENCR_KEY_SYSTEM));
	}
	
	public static byte[] getSystemEncryptionKey() throws SciException
	{
		try 
		{
			return CryptoUtils.decrypt(ConfigHome.getSysConfigSetting(SYS_CONFIG_KEYCATEGORY, SYS_CONFIG_KEYNAME).getBytes("UTF-8"), 
										ENCR_KEY_SYSTEM.getBytes("UTF-8"));
		} 
		catch (UnsupportedEncodingException ueeExcep) 
		{
			log.error("Error fetching key", ueeExcep);
			throw new SciException("Error fetching key", ueeExcep);
		}
	}
	
	private static byte[] getDBEncryptionKey(String p_sHost) throws SciException
	{
		return CryptoUtils.decrypt(SystemUtils.getPortalProperty(PROP_ENCR_KEY_DB), getDBKey(p_sHost));
	}

	private static String getDBKey(String p_sHostName)
	{
		String sCombined = null;
		String sKey = null;
		
		sCombined = ENCR_KEY_DB + p_sHostName;
		
		if(sCombined.length() <= 16)
		{
			sKey = StringUtils.rightPad(sCombined, 16, '@');
		}
		else 
		{
			sKey = sCombined.substring(0, 16);
		}
		
		return sKey;
	}
	
	public static String getDBUserPassword(String p_sEncryptedPassword, String p_sHost)
	{
		try 
		{
			return  CryptoUtils.decryptToString(p_sEncryptedPassword, getDBEncryptionKey(p_sHost));
		} 
		catch (SciException sciExcep) 
		{
			log.error("Error getting db user details", sciExcep);
		}
		
		return null;
	}
	
	public static boolean doesSystemKeyExist() throws SciException
	{
		return ConfigHome.isSysConfigSettingPresent(SYS_CONFIG_KEYNAME);
	}

	public static boolean isLocalRequest(HttpServletRequest p_oRequest)
	{
		String sClientIP = null;
		
		sClientIP = WebUtils.getSessionClientIp(p_oRequest);
		
		if(sClientIP != null)
		{
			if(sClientIP.equalsIgnoreCase("0:0:0:0:0:0:0:1") || sClientIP.equalsIgnoreCase("127.0.0.1"))
			{
				return true;
			}
		}
		return false;
	}
	
	public static boolean areDBPropertiesConfigured(String p_sPropFilePath)
	{
		Properties oProps = null;
		InputStream oInputStream = null;
		
		oProps = new Properties();
		
		try
		{
			oInputStream = new FileInputStream(p_sPropFilePath);
			
			if (oInputStream != null)
			{
				oProps.load(oInputStream);
			}
			
			if(oProps.containsKey(_PROPERTY_SCIPORTAL_DATABASE_HOST) && oProps.containsKey(_PROPERTY_SCIPORTAL_DATABASE_PORTNO) 
					&& oProps.containsKey(_PROPERTY_SCIPORTAL_DATABASE_INSTANCE) && oProps.containsKey(_PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_NAME)
						&& oProps.containsKey(_PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_PASSWORD) && oProps.containsKey(_PROPERTY_SCIPORTAL_DATABASE_SCHEMAOWNER_NAME))
			{
				return true;
			}
		}
		catch (FileNotFoundException fnfExcep)
		{
			log.error("Property file not found", fnfExcep);
		}
		catch (IOException ioExcep)
		{
			log.error("Property file not found", ioExcep);
		}
		finally
		{
			if(oInputStream != null)
			{
				try
				{
					oInputStream.close();
				}
				catch(IOException ioExcep)
				{
					log.error("Error closing extended prop file stream. Ignore and continue ", ioExcep.getMessage());
				}
			}
		}
		return false;
	}
	
	public static String getDBSchemaOwnerName()
	{
		String sSchemaOwnerFromProp = null;
				
		sSchemaOwnerFromProp = SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_SCHEMAOWNER_NAME);
		
		if(!StringUtils.isNullOrEmpty(sSchemaOwnerFromProp))
		{
			return sSchemaOwnerFromProp;
		}
		else
		{
			return DEFAULT_SCHEMA_OWNER;
		}
	
	}
	
	public static boolean isProdOrPreProdEnvironment () {
		String	p_sSysEnvType	=	ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, SystemUtils.SYSTEM_CONFIG.SYSTEM_ENVIRONMENT_TYPE.toString());
		if (StringUtils.isNullOrEmpty(p_sSysEnvType)) {
			return true;
		} else {
			if (p_sSysEnvType.equals("PROD") || p_sSysEnvType.equals("PREPROD")) {
				return true;
			} else {
				return false;
			}
		}
		
	}
	
	public static String getLoginUseridValidationRegex () {
		String	l_sLoginUseridRegex	=	ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, SystemUtils.SYSTEM_CONFIG.LOGIN_USERID_REGEX.toString());
		return l_sLoginUseridRegex;
	}
	
	public static String getCommonFieldValidationRegex () {
		String	sCommonFieldValidationRegex	=	ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, SystemUtils.SYSTEM_CONFIG.CMN_FIELD_VALIDATION_REGEX.toString());
		return sCommonFieldValidationRegex;
	}
	/**
	 * It will store the number of times e-mail
	 * should be tried to be sent in case of failure.
	 * @return
	 */
	public static int getEmailRetryCount () {
		String	l_sEmailRetryCount	=	ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, SystemUtils.SYSTEM_CONFIG.EMAIL_RETRY_COUNT.toString());
		if (StringUtils.isNullOrEmpty(l_sEmailRetryCount)) {
			return 0;
		} else {
			try {
				return Integer.parseInt(l_sEmailRetryCount);
			} catch (NumberFormatException numExcep) {
				log.error("Error parsing Email Retry Count", numExcep);
				return 0;
			}
			
		}
	}
	
	/**
	 * This is used to get the sleep time for the
	 * e-mail thread. The time mentioned is in
	 * milliseconds.
	 * @return
	 */
	public static int getEmailThreadSleepTime () {
		String	l_sEmailThreadSleepTime	=	ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, SystemUtils.SYSTEM_CONFIG.EMAIL_THREAD_SLEEP_TIME.toString());
		if (StringUtils.isNullOrEmpty(l_sEmailThreadSleepTime)) {
			return 0;
		} else {
			try {
				return Integer.parseInt(l_sEmailThreadSleepTime);
			} catch (NumberFormatException numExcep) {
				log.error("Error parsing Email Retry Count", numExcep);
				return 0;
			}
			
		}
	}

	public static String getPortalProperty(String p_sPropertyKey)
	{
		return (String)SciPortal.getProperties().get(p_sPropertyKey);
	}

	public static int getPortalIntProperty(String p_sPropertyKey)
	{
		return Integer.parseInt((String)SciPortal.getProperties().get(p_sPropertyKey));
	}

	public static boolean getPortalBooleanProperty(String p_sPropertyKey)
	{
		return Boolean.valueOf((String)SciPortal.getProperties().get(p_sPropertyKey));
	}

	public static boolean isPropertyDefined(String p_sPropertyKey)
	{
		return (SciPortal.getProperties().get(p_sPropertyKey) != null && ((String)SciPortal.getProperties().get(p_sPropertyKey)).trim().length() > 0);
	}
	
	
	
}
