package com.sciformix.sciportal.utils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.utils.CryptoUtils;
import com.sciformix.commons.utils.FileUtils;
import com.sciformix.sciportal.SciPortal;
import com.sciformix.sciportal.authenticator.LDAPAuthenticator;
import com.sciformix.sciportal.bootstrap.BootstrapInfo;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;

public class BootstrapUtils
{
	private static final Logger log = LoggerFactory.getLogger(BootstrapUtils.class);
	
	private static final String JDBC_DRIVER_NAME_ORACLE = "oracle.jdbc.driver.OracleDriver";
	
	public static boolean initializeWorkingFolders() throws SciException
	{
		try
		{
			FileUtils.createDirectory(SystemUtils.getConfigFolderPath());
			FileUtils.createDirectory(SystemUtils.getTempFolderPath());
			FileUtils.createDirectory(SystemUtils.getLogFolderPath());
		}
		catch (java.io.IOException ioExcep)
		{
			log.error("Error creating working folder", ioExcep);
			throw new SciException("error creating working folders", ioExcep);
		}
		return true;
	}
	
	public static boolean testAndSaveDBConnection(HttpServletRequest request, BootstrapInfo oBootstrapInfo) throws SciException
	{
		boolean bDBTestSuccessful = false;
		Connection oConnection = null;
		
		try
		{
			Class.forName(JDBC_DRIVER_NAME_ORACLE);
			oConnection = DriverManager.getConnection(oBootstrapInfo.getDBConnectionURL(), oBootstrapInfo.getDBUsername(), oBootstrapInfo.getDBPassword());
			
			SystemUtils.createExtendedPropertyFile(oBootstrapInfo.getDBHost(), oBootstrapInfo.getDBPort(), oBootstrapInfo.getDBSID(), 
					oBootstrapInfo.getDBUsername(), oBootstrapInfo.getDBPassword(), oBootstrapInfo.getDBSchemaOwner());
	
			SciPortal.init();
			
			bDBTestSuccessful = true;
		}
		catch(Exception excep)
		{
			SystemUtils.deleteExtendedPropertyFile();
			log.error("Error saving DB details", excep);
		}
		finally
		{
			if(oConnection != null)
			{
				try
				{
					oConnection.close();
				}
				catch(Exception excep)
				{
					log.error("Error closing connection", excep);
				}
			}
		}
		return bDBTestSuccessful;
	}

	public static boolean populateLDAPValuesInDB(HttpServletRequest p_oRequest, BootstrapInfo p_oBootstrapInfo)
	{
		String sPassword = null;
		boolean bDataPopulated = false;
		byte[] barrKey = null;
		UserSession oUserSession = null;
		boolean bKeyExists = false;
		
		try (DbConnection oDBConnection = DbConnectionManager.getBootstrapDBConnection())
		{
			try
			{
				oDBConnection.startTransaction();
				
				bKeyExists = SystemUtils.doesSystemKeyExist();
				
				if(!bKeyExists)
				{
					barrKey = CryptoUtils.generateKey();
					
				}
				else
				{
					barrKey = SystemUtils.getSystemEncryptionKey();
				}
				
				sPassword = CryptoUtils.encryptToString(p_oBootstrapInfo.getLDAPPassword(), barrKey);
				
				oUserSession = createBootstrapUserSession(p_oRequest);
				
				if(oUserSession == null)
				{
					return false;
				}
				
				LDAPUtils.updateLDAPConfig(oDBConnection, oUserSession, p_oBootstrapInfo.getLDAPDomain(), p_oBootstrapInfo.getLDAPUrl(), 
											p_oBootstrapInfo.getLDAPUsername(), sPassword);
				
				updateSystemBootstrapFlag(oDBConnection, oUserSession, true);
				
				if(!bKeyExists)
				{
					SystemUtils.storeSystemEncryptionKey(oDBConnection, oUserSession, barrKey);
				}
				else
				{
					//Do NOT overwrite the existing key
				}
				bDataPopulated = true;
				oDBConnection.commitTransaction();
			}
			catch(Exception oExcep)
			{
				oDBConnection.rollbackTransaction();
				log.error("Error creating extended prop file", oExcep);
				//TODO
			}
		}
		catch (Exception excep)
		{
			log.error("Error during LDAP values population in DB", excep);
		}
		return bDataPopulated;
	}

	public static boolean isInstanceBootstrapped()
	{
		File oPropFile = null;
		String sPropFilePath = null;
		boolean bIsInstanceBootstrapped = false;
		
		if (SystemUtils.getInstallPath() != null)
		{
			sPropFilePath = SystemUtils.getExtendedPropFilePath();
			oPropFile = new File (sPropFilePath);
			if (!oPropFile.isFile())
			{
				return bIsInstanceBootstrapped;
			}
			else
			{
				bIsInstanceBootstrapped = SystemUtils.areDBPropertiesConfigured(sPropFilePath);
			}
		}
		else
		{
			log.error("Error during bootstrap check. Environmental variable not provided - {}", SystemUtils.ENVIRONMENT_VARIABLE_INSTALL_PATH);
		}
		
		return bIsInstanceBootstrapped;
	}

	public static boolean testAndSaveLDAPConnection(HttpServletRequest request, BootstrapInfo oBootstrapInfo)
	{
		boolean bLDAPTestSuccessful = false;
		boolean bSaveSuccessful = false;
	
		try
		{
			bLDAPTestSuccessful = LDAPAuthenticator.authenticateAdmin(oBootstrapInfo.getLDAPUsername(), oBootstrapInfo.getLDAPPassword(), 
					oBootstrapInfo.getLDAPDomain(), oBootstrapInfo.getLDAPUrl());
		
			if(bLDAPTestSuccessful)
			{
				try
				{
					bSaveSuccessful = populateLDAPValuesInDB(request, oBootstrapInfo);
				}
				catch(Exception excep)
				{
					log.error("Error populating LDAP values in DB", excep);
				}
			}
		}
		catch(Exception excep)
		{
			log.error("Error populating LDAP values in DB", excep);
		}
		
		return bSaveSuccessful;
	}

	public static boolean fetchLDAPConnection(HttpServletRequest request, BootstrapInfo oBootstrapInfo) throws SciException
	{
		boolean bLDAPDetailsExist = false;
		
		try (DbConnection connection = DbConnectionManager.getBootstrapDBConnection())
		{
			bLDAPDetailsExist = isSystemBootstrapped();
		}
		catch(Exception excep)
		{
			log.error("Error fetching LDAP values", excep);
		}
		
		return bLDAPDetailsExist;
	}

	private static UserSession createBootstrapUserSession(HttpServletRequest p_oRequest) throws SciServiceException
	{
		UserInfo oUserInfo = null;
		String sClientIp = null;
		String sClientHost = null;
		String sServerIp = null;
		
		if(!SystemUtils.isLocalRequest(p_oRequest))
		{
			log.error("Bootstrap session cannot be created from a remote location");
			return null;
		}
		
		sClientIp = WebUtils.getSessionClientIp(p_oRequest);
		sClientHost = WebUtils.getSessionClientHost(p_oRequest);
		sServerIp = WebUtils.getSessionServerIp(p_oRequest);
		
		oUserInfo = UserHome.retrieveUser(UserHome.SYSTEM_BOOTSTRAP_USERID, UserHome.UserPurpose.BOOTSTRAP_MODE);
	
		return UserSession.createWebUserSession(oUserInfo, sClientIp, sClientHost, sServerIp);
	}

	private static void updateSystemBootstrapFlag(DbConnection p_oConnection, UserSession p_oUserSession, boolean p_bValue) throws SciException
	{
		//TODO: Bulk insert and update
		try
		{
			ConfigHome.updateSysConfig(p_oConnection, p_oUserSession, SystemUtils.SYS_CONFIG_KEYCATEGORY, SystemUtils.SYSTEM_CONFIG.SYSTEM_BOOTSTRAPPED.toString(), Boolean.toString(p_bValue));
		}
		catch(SciException sciExcep)
		{
			log.error("Error updating bootstrap status", sciExcep);
			throw sciExcep;
		}
	}

	public static boolean isSystemBootstrapped() throws SciException
	{
		return Boolean.parseBoolean(ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, SystemUtils.SYSTEM_CONFIG.SYSTEM_BOOTSTRAPPED.toString()));
	}
	

}
