package com.sciformix.sciportal.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;

public class PropertiesUtil
{
	public static void loadProperties(Properties oProperties, SciStartupLogger oStartupLogger) throws SciException
	{
		InputStream oInputStream = null;
		
		try
		{
			oInputStream = PropertiesUtil.class.getClassLoader().getResourceAsStream("resources/" + SystemUtils.PRIMARY_PROPERTIES_FILE_NAME);
			
			if (oInputStream != null)
			{
				oProperties.load(oInputStream);
			}
		}
		catch (FileNotFoundException fnfExcep)
		{
			oStartupLogger.logError("Property file not found - " + fnfExcep.getMessage());
			throw new SciException("Property file not found",fnfExcep);
		}
		catch (IOException ioExcep)
		{
			oStartupLogger.logError("Property file not found - " + ioExcep.getMessage());
			throw new SciException("Property file not found", ioExcep);
		}
		finally
		{
			if(oInputStream != null)
			{
				try
				{
					oInputStream.close();
				}
				catch(IOException ioExcep)
				{
					oStartupLogger.logError("Error closing prop file stream. Ignore and continue " + ioExcep.getMessage());
				}
			}
		}
	}
	
	public static void loadExtendedProperties(Properties p_oProperties, SciStartupLogger oStartupLogger) throws SciException
	{
		InputStream oInputStream = null;
		
		try
		{
			oInputStream = new FileInputStream(SystemUtils.getExtendedPropFilePath());
			
			if (oInputStream != null)
			{
				p_oProperties.load(oInputStream);
			}
		}
		catch (FileNotFoundException fnfExcep)
		{
			oStartupLogger.logError("Property file not found - " + fnfExcep.getMessage());
			throw new SciException("Extended Property file not found",fnfExcep);
		}
		catch (IOException ioExcep)
		{
			oStartupLogger.logError("Property file not found - " + ioExcep.getMessage());
			throw new SciException("Extended Property file not found", ioExcep);
		}
		finally
		{
			if(oInputStream != null)
			{
				try
				{
					oInputStream.close();
				}
				catch(IOException ioExcep)
				{
					oStartupLogger.logError("Error closing extended prop file stream. Ignore and continue " + ioExcep.getMessage());
				}
			}
		}
	}
	
	public static void dumpContentForDevTrace(Properties oProperties, SciStartupLogger oStartupLogger)
	{
		@SuppressWarnings("rawtypes")
		Enumeration eProperties = oProperties.propertyNames();
		String sProperty = null;
		
		oStartupLogger.trace("Dumping Properties read");
		while (eProperties.hasMoreElements())
		{
			sProperty = (String) eProperties.nextElement();
			oStartupLogger.trace("\t" + sProperty + ": " + oProperties.getProperty(sProperty));
		}
		oStartupLogger.trace("Dumping Properties completed");
	}
	
	public static String getProperty(Properties properties, String p_sKey)
	{
		return properties.getProperty(p_sKey);
	}
	
	public static String getProperty(Properties properties, String p_sKey, String p_sDefaultValue)
	{
		return properties.getProperty(p_sKey, p_sDefaultValue);
	}
	
	public static String[] getMultivalueProperty(Properties properties, String p_sKey)
	{
		String sValue = null;
		
		sValue = properties.getProperty(p_sKey, StringConstants.EMPTY);
		return sValue.split(StringConstants.COMMA);
	}
	
}