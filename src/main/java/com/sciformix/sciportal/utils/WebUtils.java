package com.sciformix.sciportal.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.commons.utils.ValidationUtils;
import com.sciformix.sciportal.project.ProjectHomeImpl;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.user.UserUtils;
import com.sciformix.sciportal.validation.ValidationFieldDetails;
import com.sciformix.sciportal.validation.ValidationHome;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.WebConstants;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class WebUtils
{
	private static final Logger log = LoggerFactory.getLogger(WebUtils.class);
	private static String FORMAT_VALIDATION_FIELD_MAP_KEY_ = "%s.%s";
	public static class TimeAgo 
	{
		public static final List<Long> times = Arrays.asList(
		        TimeUnit.DAYS.toMillis(365),
		        TimeUnit.DAYS.toMillis(30),
		        TimeUnit.DAYS.toMillis(1),
		        TimeUnit.HOURS.toMillis(1),
		        TimeUnit.MINUTES.toMillis(1),
		        TimeUnit.SECONDS.toMillis(1) );
		public static final List<String> timesString = Arrays.asList("year","month","day","hour","minute","second");

		public static String toDuration(long duration) {

		    StringBuffer res = new StringBuffer();
		    for(int i=0;i< TimeAgo.times.size(); i++) {
		        Long current = TimeAgo.times.get(i);
		        long temp = duration/current;
		        if(temp>0) {
		            res.append(temp).append(" ").append( TimeAgo.timesString.get(i) ).append(temp != 1 ? "s" : "").append(" ago");
		            break;
		        }
		    }
		    if("".equals(res.toString()))
		        return "0 seconds ago";
		    else
		        return res.toString();
		}
		public static String toRelativeTime(Date p_date) {

			SimpleDateFormat sdf = null;
			Calendar calNow = Calendar.getInstance();
			Calendar calEvent = Calendar.getInstance();
			calEvent.setTime(p_date);
			
			if (calEvent.get(Calendar.YEAR) == calNow.get(Calendar.YEAR) 
					&& calEvent.get(Calendar.MONTH) == calNow.get(Calendar.MONTH)
					&& calEvent.get(Calendar.DAY_OF_MONTH) == calNow.get(Calendar.DAY_OF_MONTH))
			{
				sdf = new SimpleDateFormat("hh:mm:ss a");
				//sdf = new SimpleDateFormat("hh:MM:ss.sss Z");
			}
			else if (calEvent.get(Calendar.YEAR) == calNow.get(Calendar.YEAR) 
					&& calEvent.get(Calendar.MONTH) == calNow.get(Calendar.MONTH))
			{
				sdf = new SimpleDateFormat("dd MMM hh:mm:ss a");
			}

			else
			{
				sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
			}
			
			return sdf.format(p_date);
			
//		    StringBuffer res = new StringBuffer();
//		    for(int i=0;i< TimeAgo.times.size(); i++) {
//		        Long current = TimeAgo.times.get(i);
//		        long temp = duration/current;
//		        if(temp>0) {
//		            res.append(temp).append(" ").append( TimeAgo.timesString.get(i) ).append(temp != 1 ? "s" : "").append(" ago");
//		            break;
//		        }
//		    }
//		    if("".equals(res.toString()))
//		        return "0 seconds ago";
//		    else
//		        return res.toString();
		}
		
		public static String formatDate (
			Date	p_date
		,	String	p_format	
		) {
			SimpleDateFormat	l_sdf	=	null;
			l_sdf	=	new SimpleDateFormat(p_format);
			try {
				return	l_sdf.format(p_date);
			} catch (Exception e) {
				l_sdf	=	null;
				l_sdf	=	new SimpleDateFormat("dd MMM yyyy hh:mm:ss a Z");
				return	l_sdf.format(p_date);
			}
			
		}
		
		public static String formatDate (
			Date	p_date
		) {
			return	formatDate(
						p_date
					,	"dd MMM yyyy hh:mm:ss a Z"
					);
		}
	}
	
	private static DiskFileItemFactory s_oFileUploadServletDiskFileItemFactory = null;
	
	private WebUtils()
	{
		//Private Ctor
	}
	
	public static String getLoginPageUri()
	{
		return "loginPage.jsp";
	}
	
	public static String getLandingPageUri()
	{
		return "landingPage.jsp";
	}
	
	public static String getLoginRedirectPageUri()
	{
		return "loginRedirectPage.jsp";
	}
	
	public static String getSessionExpiredPageUri()
	{
		return "sessionExpiredPage.jsp";
	}
	
	public static String getSessionClientIp(HttpServletRequest request)
	{
		//TODO: check this
		String sIPAddress = request.getHeader("X-Forwarded-For");
		return (sIPAddress == null) ? request.getRemoteAddr():sIPAddress;
	}
	
	public static String getSessionClientHost(HttpServletRequest request)
	{
		return request.getRemoteHost();
	}
	
	public static String getSessionServerIp(HttpServletRequest request)
	{
		return request.getLocalAddr();
	}
	
	public static void forwardRequestToSessionExpiration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		forwardRequest(request, response, getSessionExpiredPageUri());
	}
	
	public static void forwardRequestToLandingPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		forwardRequest(request, response, getLandingPageUri());
	}
	
	public static void forwardRequestToLoginRedirectPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		forwardRequest(request, response, getLoginRedirectPageUri());
	}
	
	public static void forwardRequestToLoginPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		forwardRequest(request, response, getLoginPageUri());
	}
	
	public static void forwardRequest(HttpServletRequest request, HttpServletResponse response, String p_sForwardUri) throws ServletException, IOException
	{
		request.getRequestDispatcher(p_sForwardUri).forward(request, response);
		
	}

	public static void invalidateSession(HttpServletRequest request)
	{
		HttpSession	l_session	=	request.getSession(false);
		if (l_session != null) {
			l_session.invalidate();
		}
	}
	
	public static void changeSessionId(HttpServletRequest request)
	{
		HttpSession	l_session	=	request.getSession(false);
		if (l_session != null) {
			l_session.invalidate();
			request.getSession(true);
		}
	}
	
	public static void setRequestAttribute(HttpServletRequest request, String p_sAttributeName, Object p_oValue)
	{
		request.setAttribute(p_sAttributeName, p_oValue);
	}
	
	public static void setSessionAttribute(HttpServletRequest request, String p_sAttributeName, Object p_oValue)
	{
		request.getSession().setAttribute(p_sAttributeName, p_oValue);
	}
	
	public static Object getSessionAttribute(HttpServletRequest request, String p_sParameterName)
	{
		return request.getSession().getAttribute(p_sParameterName);
	}
	
	public static UserSession getUserSession(HttpServletRequest request)
	{
		return (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
	}
	
	public static void setUserSession(HttpServletRequest request, UserSession oUserSession)
	{
		WebUtils.setSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION, oUserSession);
	}
	
	public static String getSessionUserId(HttpServletRequest request)
	{
		UserSession oUserSession = getUserSession(request);
		return (oUserSession != null ? oUserSession.getSessionId() : null);
	}
	
	public static String getAuthorizedProject(HttpServletRequest request)
	{
		UserSession oUserSession = getUserSession(request);
		String sCurrentSelectionProjectname = null;
		int sCurrentSelectionProjectSeqId = -1;
		
		try {
			sCurrentSelectionProjectSeqId = UserUtils.getCurrentlySelectedUserProject(oUserSession.getUserInfo());
			if(sCurrentSelectionProjectSeqId != -1)
			{
				sCurrentSelectionProjectname= ProjectHomeImpl.getProjectGivenProjectId(sCurrentSelectionProjectSeqId).getProjectName();
			}
		}
		catch(SciException SciEx)
		{
			log.error("Error Fetching current selected project",SciEx );
			return "(Error! Error Fetching current selected project)";
		}
		if (sCurrentSelectionProjectname != null)
		{
			return sCurrentSelectionProjectname;
		}
		else
		{
			//User not part of Project team
			return "(Error! User not part of Project team)";
		}
		
//		if (listAuthorizedProjects != null && listAuthorizedProjects.size() == 1)
//		{
//			return listAuthorizedProjects.get(0);
//		}
//		else if (listAuthorizedProjects != null && listAuthorizedProjects.size() > 1)
//		{
//			//User part of multiple Project teams
//			return "(Error! User part of multiple Project teams)";
//		}
//		
	}
	
	
	public static int getAuthorizedProjectId(HttpServletRequest request)
	{
		UserSession oUserSession = getUserSession(request);
		
		try {
			return UserUtils.getCurrentlySelectedUserProject(oUserSession.getUserInfo());
		}
		catch(SciException SciEx)
		{
			log.error("Error Fetching current selected project",SciEx );
			return -1;
		}
		/*if (listAuthorizedProjectIds != null && listAuthorizedProjectIds.size() == 1)
		{
			return listAuthorizedProjectIds.get(0).getObjectSeqId();
		}
		else if (listAuthorizedProjectIds != null && listAuthorizedProjectIds.size() > 1)
		{
			//User part of multiple Project teams
			return -1;
		}
		else
		{
			//User not part of Project team
			return -1;
		}*/
	}
	
	
//	public static String getRequestParameter(HttpServletRequest request, List<FileItem> items, String p_sParameterName)
//	{
//		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
//		if (isMultipart)
//		{
//			try
//			{
//			List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
//	        for (FileItem item : items)
//	        {
//	            if (item.isFormField() && item.getFieldName().equalsIgnoreCase(p_sParameterName))
//	            {
//	                return item.getString();
//	            }
//	            else
//	            {
////	                // Process form file field (input type="file").
////	                String fieldname = item.getFieldName();
////	                String filename = FilenameUtils.getName(item.getName());
////	                InputStream filecontent = item.getInputStream();
////	                // ... (do your job here)
//	            }
//	        }
//			}
//			catch(FileUploadException fueExcep)
//			{
//				//TODO:
//			}
//	        return null;
//		}
//		else
//		{
//			return request.getParameter(p_sParameterName);
//		}
//	}
	
	@SuppressWarnings("deprecation")
	public static boolean isMultipartContentRequest(HttpServletRequest request)
	{
		return ServletFileUpload.isMultipartContent(request);
	}
	
	@SuppressWarnings("unchecked")
	public static MultipartContentRequestWrapper createMultipartContentRequestWrapper(HttpServletRequest request) throws SciWebException
	{
		List<FileItem> listFileitemsMultipartContent = null;
		Collection<Part> collectionOfFileParts = null;
		
		if (isMultipartContentRequest(request))
		{
			try
			{
				if(s_oFileUploadServletDiskFileItemFactory == null)
				{
					File oFileUploadServletDiskFactoryTempRepository = new File(SystemUtils.getTempFolderPath());
					s_oFileUploadServletDiskFileItemFactory = new DiskFileItemFactory(WebConstants.FILE_UPLOAD_SERVLET_DISK_FACTORY_MEMORY_THRESHOLD, oFileUploadServletDiskFactoryTempRepository);
				}
					
				listFileitemsMultipartContent = (List<FileItem>)(new ServletFileUpload(s_oFileUploadServletDiskFileItemFactory).parseRequest(request));
				
				try
				{
					collectionOfFileParts = request.getParts();
				}
				catch(Exception excep)
				{
					excep.printStackTrace();
				}
			}
			catch(FileUploadException fueExcep)
			{
				log.error("Error parsing multipart content ", fueExcep);
				throw new SciWebException(fueExcep);
			}
		}
		return (listFileitemsMultipartContent != null || collectionOfFileParts != null) ? new MultipartContentRequestWrapper(listFileitemsMultipartContent, collectionOfFileParts) : null;
	}
	
	public static String getRequestParameter(HttpServletRequest request, String p_sParameterName)
	{
		return getRequestParameter(request, p_sParameterName, null);
	}
	
	public static String getRequestParameter(HttpServletRequest request, String p_sParameterName, MultipartContentRequestWrapper p_oMultipartContentRequest)
	{
		if (p_oMultipartContentRequest != null && p_oMultipartContentRequest.fileItemsAvailable())
		{
	        for (FileItem item : p_oMultipartContentRequest.getFileItemList())
	        {
	            if (item.isFormField() && item.getFieldName().equalsIgnoreCase(p_sParameterName))
	            {
	                return item.getString().trim();
	            }
	        }
	        return null;
		}
		else
		{
			if(!StringUtils.isNullOrEmpty(request.getParameter(p_sParameterName)))
			{
				return request.getParameter(p_sParameterName).trim();
			}
			else
			{
				 return request.getParameter(p_sParameterName);
			}
		}
	}
	
	public static Enumeration<String> getRequestParameterAll (HttpServletRequest request) {
		return getRequestParameterAll(request, null);
	}
	
	public static Enumeration<String> getRequestParameterAll (HttpServletRequest request, MultipartContentRequestWrapper p_oMultipartContentRequest) {
		if (p_oMultipartContentRequest != null && p_oMultipartContentRequest.fileItemsAvailable())
		{
			List<String>	l_paramlist	=	new ArrayList<>();
	        for (FileItem item : p_oMultipartContentRequest.getFileItemList())
	        {
	           l_paramlist.add(item.getFieldName());
	        }
	        return Collections.enumeration(l_paramlist);
		}
		else
		{
			return request.getParameterNames();
		}
	}
	
	public static boolean isRequestParameterAvailable (HttpServletRequest request, String p_sParameterName, MultipartContentRequestWrapper p_oMultipartContentRequest) {
		String sValue = getRequestParameter(request, p_sParameterName, p_oMultipartContentRequest);
		
		if (StringUtils.isNullOrEmpty(sValue))
		{
			return false;
		} else {
			return true;
		}
	}
	
	public static int getRequestIntParameter(HttpServletRequest request, String p_sParameterName, MultipartContentRequestWrapper p_oMultipartContentRequest)
	{
		String sValue = getRequestParameter(request, p_sParameterName, p_oMultipartContentRequest);
		
		if (StringUtils.isNullOrEmpty(sValue))
		{
			return -1;
		}
		try
		{
			return Integer.parseInt(sValue);
		}
		catch (NumberFormatException nfException)
		{
			return -1;
		}
	}
	
	public static UploadedFileWrapper stageRequestParameterFile(HttpServletRequest request, String p_sParameterName, 
			MultipartContentRequestWrapper p_oMultipartContentRequest) throws SciWebException
	{
		UploadedFileWrapper oUploadedFileObject = null;
		String sStagingFileName = null;
		String sStagingFileCompletePath = null;
		File oStagingFile = null;
		
		if (p_oMultipartContentRequest != null && p_oMultipartContentRequest.fileItemsAvailable())
		{
	        for (FileItem item : p_oMultipartContentRequest.getFileItemList())
	        {
	        	if (!item.isFormField() && item.getFieldName().equalsIgnoreCase(p_sParameterName))
	        	{
					if (!StringUtils.isNullOrEmpty(item.getName()))
					{
					    try
					    {
					    	sStagingFileName = item.getName();
						    sStagingFileCompletePath = createStagingFile(getUserSession(request), sStagingFileName);
						    oStagingFile = new File(sStagingFileCompletePath);
				   
							item.write(oStagingFile);
							oUploadedFileObject = new UploadedFileWrapper(sStagingFileCompletePath, sStagingFileName, sStagingFileName);
						}
						catch (IOException ioExcep)
						{
							log.error("error saving uploaded file", ioExcep);
							throw new SciWebException(ioExcep);
						}
						catch (Exception excep)
						{
							log.error("error saving uploaded file", excep);
							throw new SciWebException(excep);
						}
					}
					break;
	        	}
	        	//else continue looping through the item list
	        }
		}
		else if (p_oMultipartContentRequest.getParts() != null)
		{
			for (Part part : p_oMultipartContentRequest.getParts())
	        {
	        	if (part.getContentType() != null && 
	        			!StringUtils.isNullOrEmpty(part.getName()) && 
	        				part.getName().equalsIgnoreCase(p_sParameterName))
	        	{
	        		try
				    {
			    		sStagingFileName = getFileNameFromPartObject(part);
					    sStagingFileCompletePath = createStagingFile(getUserSession(request), sStagingFileName);
					    
						part.write(sStagingFileCompletePath);
						oUploadedFileObject = new UploadedFileWrapper(sStagingFileCompletePath, sStagingFileName, sStagingFileName);
					}
					catch (IOException ioExcep)
					{
						log.error("error saving uploaded file", ioExcep);
						throw new SciWebException(ioExcep);
					}
				    break;
	        	}
	        }
		}
		else
		{
			log.error("Error saving uploaded file as null multipart content type received");
		}
		return oUploadedFileObject;
	}
	
	public static String getFileNameFromPartObject(Part p_oPart)
	{
		String sFileName = null;
		String sHeader = null;
		
		if(p_oPart != null)
		{
			sHeader = p_oPart.getHeader("content-disposition");
			for (String content : sHeader.split(";")) 
			{
				if (content.contains("filename")) 
	            {
					sFileName = content.trim().substring(content.indexOf('=') + 1).trim().replace("\"","");
	            	break;
			    }
	        }
		 }
		return sFileName;
	}
	
	public static String createStagingFile(UserSession p_oUserSession, String p_sFileName) throws IOException
	{
		String sTempFolderPath = SystemUtils.createUserTempFolder("u" + p_oUserSession.getUserInfo().getUserSeqId());
		
		String filePath =  FilenameUtils.concat(sTempFolderPath,  Calendar.getInstance().getTimeInMillis() + "_" + p_sFileName);
		
		return filePath;
	}
	
	public static String createStagingFileForTools(UserSession p_oUserSession, String p_sFileName, String fileExt) throws IOException
	{
		String sTempFolderPath = SystemUtils.createUserTempFolder("u" + p_oUserSession.getUserInfo().getUserSeqId());
		
		String filePath =  FilenameUtils.concat(sTempFolderPath,  p_sFileName+ "_" + Calendar.getInstance().getTimeInMillis()+fileExt);
		
		return filePath;
	}
	
	public static void cleanUserTempFolder(UserSession p_oUserSession)
	{
		String sTempFolderPath = null;
		File oDirectory = null;
		
		try
		{
			sTempFolderPath = SystemUtils.createUserTempFolder("u" + p_oUserSession.getUserInfo().getUserSeqId());
			oDirectory = new File(sTempFolderPath);
			if(oDirectory.isDirectory())
			{
				FileUtils.cleanDirectory(oDirectory);
			}
		}
		catch(IOException ioExcep)
		{
			log.error("Error cleaning user temp folder. Ignore and continue", ioExcep);
		}
		
	}
	
	public static boolean cleanTempFolder()
	{
		File oDirectory = null;
		
		try
		{
			oDirectory = new File(SystemUtils.getTempFolderPath());
			if(oDirectory.isDirectory())
			{
				FileUtils.cleanDirectory(oDirectory);
				return true;
			}
		}
		catch(IOException ioExcep)
		{
			log.error("Error cleaning user temp folder. Ignore and continue", ioExcep);
		}
		return false;
	}
	
	public static List<UploadedFileWrapper> stageRequestParameterFiles(HttpServletRequest request, String p_sParameterName, 
			MultipartContentRequestWrapper p_oMultipartContentRequest) throws SciWebException
	{
		List<UploadedFileWrapper> listUploadedFileObject = null;
		UploadedFileWrapper oUploadedFileObject =null;
	    int nFileCounter = 0;
	    String sStagingFileName = null;
	    String sStagingFileCompletePath = null;
	    File oStagingFile = null;
	    String sOriginalFileName = null;
	   
	    listUploadedFileObject = new ArrayList<>();
		if (p_oMultipartContentRequest != null && p_oMultipartContentRequest.fileItemsAvailable())
		{
	        for (FileItem item : p_oMultipartContentRequest.getFileItemList())
	        {
	        	if (item.getFieldName().equalsIgnoreCase(p_sParameterName))
	        	{
		            if (!item.isFormField() && !StringUtils.isNullOrEmpty(item.getName()))
		            {
		            	//DEV-NOTE:Increment it for next loop as well as make it 1-based
		            	nFileCounter++;
		            	
						try
						{
							sOriginalFileName = item.getName();
							sStagingFileName = new File("file_" + nFileCounter + "_" + sOriginalFileName).getName();
							sStagingFileCompletePath = createStagingFile(WebUtils.getUserSession(request), sStagingFileName);
							oStagingFile = new File(sStagingFileCompletePath);
							item.write(oStagingFile);
							
							oUploadedFileObject = new UploadedFileWrapper(sStagingFileCompletePath, sStagingFileName, sOriginalFileName); 
							listUploadedFileObject.add(oUploadedFileObject);	
						}
						catch (IOException ioExcep)
						{
							log.error("error saving uploaded file", ioExcep);
							throw new SciWebException(ioExcep);
						}
						catch (Exception excep)
						{
							log.error("error saving uploaded file", excep);
							throw new SciWebException(excep);
						}
		            }
		            else
		            {
		            	//ERROR
		            	//return null;
		            	log.error("error saving uploaded file");
						throw new SciWebException(WebErrorCodes.Ngv.ERROR_SAVING_PARSING_FILE);
					
		            }
	        	}
	        	//else continue looping through the item list
	        }
	        return listUploadedFileObject;
		}
		else if (p_oMultipartContentRequest.getParts() != null)
		{
			for (Part part : p_oMultipartContentRequest.getParts())
	        {
	        	if (part.getContentType() != null && 
	        				!StringUtils.isNullOrEmpty(part.getName()) && 
	        					part.getName().equalsIgnoreCase(p_sParameterName))
	        	{
	        		nFileCounter++;
	            	
				    try
				    {
				    	sOriginalFileName = sStagingFileName = getFileNameFromPartObject(part);
					    sStagingFileName = new File("file_" + nFileCounter + "_" + sOriginalFileName).getName();
					    sStagingFileCompletePath = createStagingFile(getUserSession(request), sStagingFileName);
					    
						part.write(sStagingFileCompletePath);
						oUploadedFileObject = new UploadedFileWrapper(sStagingFileCompletePath, sStagingFileName, sOriginalFileName);
					
						listUploadedFileObject.add(oUploadedFileObject);	
					}
					catch (IOException ioExcep)
					{
						log.error("error saving uploaded file", ioExcep);
						throw new SciWebException(ioExcep);
					}
				}
	        	//else continue looping through the item list
	        }
			return listUploadedFileObject;
		}
		else
		{
			//ERROR
			return null;
		}
	}
	
	public static final String generateSessionId(String p_sUserId)
	{
		return p_sUserId + "_" + System.currentTimeMillis();
	}

	public static UserSession createWebUserSession(HttpServletRequest request, UserInfo p_oUserInfo) throws SciServiceException
	{
		String sClientIp = null;
		String sClientHost = null;
		String sServerIp = null;
		
		sClientIp = getSessionClientIp(request);
		sClientHost = getSessionClientHost(request);
		sServerIp = getSessionServerIp(request);
		
		return UserSession.createWebUserSession(p_oUserInfo, sClientIp, sClientHost, sServerIp);
	}	

	public static String formatForDisplayDuration(Date p_dtValue)
	{
		return TimeAgo.toDuration(System.currentTimeMillis() - p_dtValue.getTime());
	}
	
	public static String formatForDisplay(Date p_dtValue)
	{
		return TimeAgo.toRelativeTime(p_dtValue);
	}
	
	public static String formatForDisplayDate (Date	p_dtValue) {
		return TimeAgo.formatDate(p_dtValue);
	}
	
	public static String formatForDisplayDate (Date	p_dtValue, String	p_format) {
		return TimeAgo.formatDate(p_dtValue,p_format);
	}
	
	public static String formatForDisplay(String p_sText)
	{
		return (p_sText != null ? p_sText : StringConstants.EMPTY);
	}
	
	public static String formatForDisplay(String p_sText, String p_sDefaultText)
	{
		return (p_sText != null ? p_sText : p_sDefaultText);
	}
	
	public static String formatForDisplay(String p_sText, int p_nMaxDisplaySize)
	{
		return formatForDisplay(p_sText, StringConstants.EMPTY, p_nMaxDisplaySize, false);
	}
	
	public static String formatForDisplay(String p_sText, String p_sDefaultText, int p_nMaxDisplaySize)
	{
		return formatForDisplay(p_sText, p_sDefaultText, p_nMaxDisplaySize, false);
	}
	
	public static String formatForDisplay(String p_sText, String p_sDefaultText, int p_nMaxDisplaySize, boolean p_bDisplaySize)
	{
		String sTextToDisplay = null;
		int nTextLength = -1;
		
		sTextToDisplay = formatForDisplay(p_sText, p_sDefaultText);
		nTextLength = sTextToDisplay.length();
		
		if (p_nMaxDisplaySize > 0 && nTextLength >  p_nMaxDisplaySize)
		{
			sTextToDisplay = sTextToDisplay.substring(0, p_nMaxDisplaySize) + "...";
			if (p_bDisplaySize)
			{
				sTextToDisplay = sTextToDisplay + "(total: " + nTextLength + " characters)"; 
			}
		}
		
		return sTextToDisplay;
	}
	
	public static boolean isPresent(String p_sText)
	{
		return (p_sText != null && p_sText.trim().length() > 0);
	}
	
	/**
	 * This method will validate the user id passed from the login page.
	 * @param p_userid	The user id which needs to be validated.
	 * @return			true if the regex passes otherwise false.
	 */
	public static boolean validateUserid(
		String	p_userid
	) {
		Pattern	l_pattern	=	Pattern.compile(
									SystemUtils.getLoginUseridValidationRegex()
								);
		Matcher	l_matcher	=	l_pattern.matcher(
									p_userid
								);
		return l_matcher.matches();
	}
	
	public static List<Object> validateRequestParameters(
		String p_sActionId, String p_sAppId, HttpServletRequest request, MultipartContentRequestWrapper oMultipartContentRequest
	) throws SciServiceException 
	{
		Enumeration<String> parameterList = null;
		String sFieldValue = null;
		String sTemp = null;
		boolean bValidationFlag = true;
		List<String> fieldNameList = null;
		List<Object> resultList = null;
		ValidationFieldDetails oValidationFieldDetails = null;
		String validationMapKey = "";
		String sStricterRegex = null;
		String contextId = null;
		Map<String, Map<String, ValidationFieldDetails>> mapValidationFieldDetails = null;
		Map<String, Map<String, ValidationFieldDetails>> mapWildCardValidationFieldDetails = null;
		Map<String, ValidationFieldDetails> mapValidationErrorFieldDetails = null;
		
		parameterList = getRequestParameterAll(request ,oMultipartContentRequest);
		sStricterRegex = SciConstants.RegExConstants.OBJECT_NAME;
		fieldNameList = new ArrayList<>();
		
		mapValidationFieldDetails = ValidationHome.getValidationFieldDetailsMap();
		mapWildCardValidationFieldDetails = ValidationHome.getWildCardalidationFieldDetailsMap();
		mapValidationErrorFieldDetails = new HashMap<String, ValidationFieldDetails>();
		contextId = getRequestParameter(request, "_jsp_jspPageObject", oMultipartContentRequest);
		
		resultList = new ArrayList<>();
		while(parameterList.hasMoreElements())
		{
			sTemp = parameterList.nextElement();
			sFieldValue	=	getRequestParameter(request, sTemp, oMultipartContentRequest);
			if(!StringUtils.isNullOrEmpty(sFieldValue))
			{
				validationMapKey = prepareKey(contextId, sTemp);
				if(sTemp.equals("portal_appid") || sTemp.equals("portal_action")|| sTemp.equals("_token_"))
				{
					if(!ValidationUtils.validateField(sFieldValue, sStricterRegex))
					{
						bValidationFlag = false;
						fieldNameList.add(sTemp);
						mapValidationErrorFieldDetails.put(prepareKey(contextId, sTemp), oValidationFieldDetails);
					}
				}
				else
				{
					if(sTemp.startsWith("__"))
					{
						boolean keyPresflag = true;
						if(mapWildCardValidationFieldDetails.containsKey(p_sAppId))
						{
							for(String key : mapWildCardValidationFieldDetails.get(p_sAppId).keySet() )
							{
								String [] keyArray = key.split("\\.");
								if(sTemp.startsWith(keyArray[1]))
								{	
									keyPresflag = false;
									oValidationFieldDetails = mapWildCardValidationFieldDetails.get(p_sAppId).get(key);
									bValidationFlag = ValidationUtils.validate(sFieldValue, oValidationFieldDetails.getsDataType());
									if (!bValidationFlag)
									{
										fieldNameList.add(sTemp);
										mapValidationErrorFieldDetails.put(prepareKey(contextId, sTemp), oValidationFieldDetails);
									}
								}
							}
							if(keyPresflag){
								if(!ValidationUtils.validateField(sFieldValue, sStricterRegex))
								{
									bValidationFlag = false;
									fieldNameList.add(sTemp);
									mapValidationErrorFieldDetails.put(prepareKey(contextId, sTemp), oValidationFieldDetails);
								}
							}
						}
					}
					else if(mapValidationFieldDetails != null && mapValidationFieldDetails.containsKey(p_sAppId))
					{
						if(mapValidationFieldDetails.get(p_sAppId).containsKey(validationMapKey))
						{
							oValidationFieldDetails = mapValidationFieldDetails.get(p_sAppId).get(validationMapKey);
							
							bValidationFlag = ValidationUtils.validate(sFieldValue, oValidationFieldDetails.getsDataType());
							if (! bValidationFlag)
							{
								fieldNameList.add(sTemp);
								mapValidationErrorFieldDetails.put(prepareKey(contextId, sTemp), oValidationFieldDetails);
							}
						} else {
							if(!ValidationUtils.validateField(sFieldValue, sStricterRegex))
							{
								bValidationFlag = false;
								fieldNameList.add(sTemp);
								mapValidationErrorFieldDetails.put(prepareKey(contextId, sTemp), oValidationFieldDetails);
							}
						}
					}
					else
					{
						if(!ValidationUtils.validateField(sFieldValue, sStricterRegex))
						{
							bValidationFlag = false;
							fieldNameList.add(sTemp);
							mapValidationErrorFieldDetails.put(prepareKey(contextId, sTemp), oValidationFieldDetails);
						}
					}
				}
			}
		}
		setRequestAttribute(request, "r_validationMap", mapValidationErrorFieldDetails);
		resultList.add(bValidationFlag);
		resultList.add(fieldNameList);
		return resultList ;
	}
	
	private static String prepareKey(String contextId, String p_sFieldName)
	{
		return String.format(FORMAT_VALIDATION_FIELD_MAP_KEY_,contextId, p_sFieldName);
	}
}
