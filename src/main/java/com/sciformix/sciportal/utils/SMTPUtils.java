package com.sciformix.sciportal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.CryptoUtils;
import com.sciformix.sciportal.config.ConfigHome;

public class SMTPUtils {
	
private static final Logger log = LoggerFactory.getLogger(SMTPUtils.class);
	
	private static final String SMTP_CONFIG_CATEGORY = "APPSETTINGS-PORTAL-APP";
	
	//private final static String REGEX_ALPHANUMERIC = "^[a-zA-Z0-9]*$";
	
	public enum SMTP_CONFIG
	{
		PRIMARY_SMTP_HOST("APPSETTINGS-PORTAL-APP.sciportal.smtp.1.host"),
		PRIMARY_SMTP_PORT("APPSETTINGS-PORTAL-APP.sciportal.smtp.1.port"),
		PRIMARY_SMTP_BINDING_MAIL("APPSETTINGS-PORTAL-APP.sciportal.smtp.1.fromuser.email"),
		PRIMARY_SMTP_BINDING_PASSWORD("APPSETTINGS-PORTAL-APP.sciportal.smtp.1.connectuser.password"),
		PRIMARY_SMTP_SENDER_MAIL("APPSETTINGS-PORTAL-APP.sciportal.smtp.1.connectuser.email");
		
		private String value;
		
		SMTP_CONFIG(String value)
		{
			this.value = value;
		}
		
		public String toString()
		{
			return this.value;
		}
		
		public static SMTP_CONFIG toEnum(int value)
		{
			SMTP_CONFIG[] values = SMTP_CONFIG.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (values[counter].toString().equals(value))
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("AppConfig: Illegal enum value - " + value);
		}
	}
	
	public static String encrypt(String p_sStringToBeEncrypted) throws SciException
	{
		return CryptoUtils.encryptToString(p_sStringToBeEncrypted, SystemUtils.getSystemEncryptionKey());
	}

	public static String decrypt(String p_sStringToBeEncrypted) throws SciException
	{
		return CryptoUtils.decryptToString(p_sStringToBeEncrypted, SystemUtils.getSystemEncryptionKey());
	}

	
	public static String getSMTPHost() throws SciException
	{
		return ConfigHome.getAppConfigSetting(SMTP_CONFIG_CATEGORY, SMTP_CONFIG.PRIMARY_SMTP_HOST.toString());
	}

	public static String getSMTPPort() throws SciException
	{
		return ConfigHome.getAppConfigSetting(SMTP_CONFIG_CATEGORY, SMTP_CONFIG.PRIMARY_SMTP_PORT.toString());
	}
	
	public static String getSMTPBindingMail() throws SciException
	{
		return ConfigHome.getAppConfigSetting(SMTP_CONFIG_CATEGORY, SMTP_CONFIG.PRIMARY_SMTP_BINDING_MAIL.toString());
	}		
	
	public static String getSMTPBindingPassworrd() throws SciException
	{
		return ConfigHome.getAppConfigSetting(SMTP_CONFIG_CATEGORY, SMTP_CONFIG.PRIMARY_SMTP_BINDING_PASSWORD.toString());
	}
	
	public static String getSMTPSenderMail() throws SciException
	{
		return ConfigHome.getAppConfigSetting(SMTP_CONFIG_CATEGORY, SMTP_CONFIG.PRIMARY_SMTP_SENDER_MAIL.toString());
	}
}
