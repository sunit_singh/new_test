/**
 * 
 */
package com.sciformix.sciportal.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.CryptoUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.user.UserSession;

/**
 * @author sshetty1
 *
 */
public class LDAPUtils 
{
	private static final Logger log = LoggerFactory.getLogger(LDAPUtils.class);
	
	private static final String LDAP_CONFIG_CATEGORY = "LDAP.PRIMARY.CONFIG";
	
	private final static String REGEX_ALPHANUMERIC = "^[a-zA-Z0-9]*$";
	
	public enum LDAP_CONFIG
	{
		PRIMARY_LDAP_DOMAIN("sciportal.ldap.primary.domain"),
		PRIMARY_LDAP_URL("sciportal.ldap.primary.url"),
		PRIMARY_LDAP_BINDING_USERNAME("sciportal.ldap.primary.binding.username"),
		PRIMARY_LDAP_BINDING_PASSWORD("sciportal.ldap.primary.binding.password");
	
		private String value;
		
		LDAP_CONFIG(String value)
		{
			this.value = value;
		}
		
		public String toString()
		{
			return this.value;
		}
		
		public static LDAP_CONFIG toEnum(int value)
		{
			LDAP_CONFIG[] values = LDAP_CONFIG.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (values[counter].toString().equals(value))
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("AppConfig: Illegal enum value - " + value);
		}
	}
	
	public static String encrypt(String p_sStringToBeEncrypted) throws SciException
	{
		return CryptoUtils.encryptToString(p_sStringToBeEncrypted, SystemUtils.getSystemEncryptionKey());
	}

	public static String decrypt(String p_sStringToBeEncrypted) throws SciException
	{
		return CryptoUtils.decryptToString(p_sStringToBeEncrypted, SystemUtils.getSystemEncryptionKey());
	}

	
	public static String getLDAPPrimaryDomain() throws SciException
	{
		return ConfigHome.getSysConfigSetting(LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_DOMAIN.toString());
	}

	public static String getLDAPPrimaryURL() throws SciException
	{
		return ConfigHome.getSysConfigSetting(LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_URL.toString());
	}
	
	public static String getLDAPPrimaryUsername() throws SciException
	{
		return ConfigHome.getSysConfigSetting(LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_BINDING_USERNAME.toString());
	}
	
	public static String getLDAPPrimaryPassword() throws SciException
	{
		return ConfigHome.getSysConfigSetting(LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_BINDING_PASSWORD.toString());
	}
	
	public static void updateLDAPConfig(DbConnection p_oConnection, UserSession p_oUserSession, String p_sLDAPDomain, 
			String p_sLDAPUrl, String p_sBindingUserName, String p_sBindingPassword) throws SciException
	{
		//TODO: Bulk insert and update
		try
		{
			ConfigHome.updateSysConfig(p_oConnection, p_oUserSession, LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_DOMAIN.toString(), p_sLDAPDomain);
			ConfigHome.updateSysConfig(p_oConnection, p_oUserSession, LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_URL.toString(), p_sLDAPUrl);
			ConfigHome.updateSysConfig(p_oConnection, p_oUserSession, LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_BINDING_USERNAME.toString(), p_sBindingUserName);
			ConfigHome.updateSysConfig(p_oConnection, p_oUserSession, LDAP_CONFIG_CATEGORY, LDAP_CONFIG.PRIMARY_LDAP_BINDING_PASSWORD.toString(), p_sBindingPassword);
		}
		catch(SciException sciExcep)
		{
			log.error("Error updating LDAP Config", sciExcep);
			throw sciExcep;
		}
	}
	
	public String encodeFilter(String p_sFilter) 
	{
		StringBuilder oStringBuilder = null;
		
		if( p_sFilter == null ) 
		{
	    	return null;	
	    }
		
	    oStringBuilder = new StringBuilder();
		for (int i = 0; i < p_sFilter.length(); i++) 
		{
			char c = p_sFilter.charAt(i);
			switch (c) {
			case '\\':
				oStringBuilder.append("\\5c");
				break;
			case '*':
				oStringBuilder.append("\\2a");
				break;
			case '(':
				oStringBuilder.append("\\28");
				break;
			case ')':
				oStringBuilder.append("\\29");
				break;
			case '\0':
				oStringBuilder.append("\\00");
				break;
			default:
				oStringBuilder.append(c);
			}
		}
		return oStringBuilder.toString();
	}

	public String encodeDN(String p_sDN) 
	{
		StringBuilder oStringBuilder = null;
			
		if( p_sDN == null ) 
	    {
	    	return null;	
	    }
		
		oStringBuilder = new StringBuilder();
		
	    //Adding leading '\\' if required
		if ((p_sDN.length() > 0) && ((p_sDN.charAt(0) == ' ') || (p_sDN.charAt(0) == '#'))) 
		{
			oStringBuilder.append('\\'); 
		}
		
		//Replace chars
	    for (int i = 0; i < p_sDN.length(); i++) 
		{
			char c = p_sDN.charAt(i);
			switch (c) 
			{
			case '\\':
				oStringBuilder.append("\\\\");
				break;
			case ',':
				oStringBuilder.append("\\,");
				break;
			case '+':
				oStringBuilder.append("\\+");
				break;
			case '"':
				oStringBuilder.append("\\\"");
				break;
			case '<':
				oStringBuilder.append("\\<");
				break;
			case '>':
				oStringBuilder.append("\\>");
				break;
			case ';':
				oStringBuilder.append("\\;");
				break;
			default:
				oStringBuilder.append(c);
			}
		}
		
	    //Adding trailing '\\' if required
		if ((p_sDN.length() > 1) && (p_sDN.charAt(p_sDN.length() - 1) == ' ')) 
		{
			oStringBuilder.insert(oStringBuilder.length() - 1, '\\');
		}
		
		return oStringBuilder.toString();
	}
	
	public static boolean isValid(String p_sInput)
	{
		if (StringUtils.isNullOrEmpty(p_sInput) || !p_sInput.matches(REGEX_ALPHANUMERIC))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
}
