package com.sciformix.sciportal.config;

class ConfigObj
{
	public enum ConfigType
	{
		COMMON, SYSTEM, PROJECT;
	}
	
	private ConfigType m_eConfigType = null;
	//TODO:Add Project Seq Id and leave it as '-1' for non-Project Config Types
	private int  prjSeq_Id=-1;
	private String m_sAppName = null;
	private String m_sKeyName = null;
	private String m_sSerializedValue = null;
	
	public static ConfigObj createCommonConfig()
	{
		return new ConfigObj(ConfigType.COMMON);
	}
	
	public static ConfigObj createSysConfig()
	{
		return new ConfigObj(ConfigType.SYSTEM);
	}

	public static ConfigObj createPrjConfig()
	{
		return new ConfigObj(ConfigType.PROJECT);
	}
	
	public ConfigObj(ConfigType p_eConfigType)
	{
		m_eConfigType = p_eConfigType;
	}
	
	public ConfigType getConfigType() {
		return m_eConfigType;
	}
	
	public String getAppName() {
		return m_sAppName;
	}

	public void setAppName(String p_sAppName) {
		this.m_sAppName = p_sAppName;
	}

	public String getKeyName() {
		return m_sKeyName;
	}

	public void setKeyName(String p_sKeyName) {
		this.m_sKeyName = p_sKeyName;
	}

	public String getSerializedValue() {
		return m_sSerializedValue;
	}

	public void setSerializedValue(String p_sSerializedValue) {
		this.m_sSerializedValue = p_sSerializedValue;
	}

	public int getPrjSeq_Id() {
		return prjSeq_Id;
	}

	public void setPrjSeq_Id(int prjSeq_Id) {
		this.prjSeq_Id = prjSeq_Id;
	}
}
