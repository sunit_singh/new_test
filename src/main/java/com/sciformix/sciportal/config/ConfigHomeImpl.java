package com.sciformix.sciportal.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.config.ConfigObj.ConfigType;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUpdate;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHomeImpl;
import com.sciformix.sciportal.user.UserInfo;

class ConfigHomeImpl
{
	private static final Logger log = LoggerFactory.getLogger(ConfigHomeImpl.class);
	
	private ConfigHomeImpl()
	{
		//Nothing to do
	}
	
	public static List<ConfigMetadata> loadSysConfigMetadata(DbConnection connection) throws SciException
	{
		return loadConfigMetadata(connection, ConfigType.SYSTEM);
	}
	
	public static List<ConfigMetadata> loadCommonConfigMetadata(DbConnection connection) throws SciException
	{
		return loadConfigMetadata(connection, ConfigType.COMMON);
	}
	public static List<ConfigMetadata> loadPrjConfigMetadata(DbConnection connection) throws SciException
	{
		return loadConfigMetadata(connection, ConfigType.PROJECT);
	}
	
	private static List<ConfigMetadata> loadConfigMetadata(DbConnection connection, ConfigType p_eConfigType) throws SciException
	{
		ConfigMetadata oSysConfigMdDb = null;
		List<ConfigMetadata> listAppConfig = null;
		DbQuerySelect oSelectQuery = null;
		
		listAppConfig = new ArrayList<ConfigMetadata>();

		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigDbSchema.CONFIG_MD, ConfigDbSchema.CONFIG_MD.CFGTYPE.bindClause(), 
				DbQueryUtils.createOrderbyClause(ConfigDbSchema.CONFIG_MD.CFGCATEGORY, ConfigDbSchema.CONFIG_MD.SERIALNO));
		oSelectQuery.addWhereClauseParameter(p_eConfigType.name());
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					oSysConfigMdDb = new ConfigMetadata();
					
					oSysConfigMdDb.setCategory(resultSet.readString(ConfigDbSchema.CONFIG_MD.CFGCATEGORY));
					oSysConfigMdDb.setSerialNumber(resultSet.readInt(ConfigDbSchema.CONFIG_MD.SERIALNO));
					oSysConfigMdDb.setKey(resultSet.readString(ConfigDbSchema.CONFIG_MD.CFGKEY));
					oSysConfigMdDb.setDisplayName(resultSet.readString(ConfigDbSchema.CONFIG_MD.CFGDISPLAYNAME));
					oSysConfigMdDb.setValueDatatype(resultSet.readString(ConfigDbSchema.CONFIG_MD.CFGVALUETYPE));
					oSysConfigMdDb.setTooltip(resultSet.readString(ConfigDbSchema.CONFIG_MD.CFGTOOLTIP));
					oSysConfigMdDb.setDefaultValue(resultSet.readString(ConfigDbSchema.CONFIG_MD.CFGDEFAULTVALUE));
					oSysConfigMdDb.setVisibility(resultSet.readString(ConfigDbSchema.CONFIG_MD.CFGVISIBILITY));
					
					listAppConfig.add(oSysConfigMdDb);
				}
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading App Configuration Metadata from Db", dbExcep);
			throw new SciException("Error reading App Configuration Metadata from Db", dbExcep);
		}
		
		return listAppConfig;
	}
	
	public static void updateSysConfig(DbConnection connection, UserInfo p_oUserInfo, String p_sCategory, String p_sKey, String p_sNewValue, 
								boolean p_bEditOperation) throws SciException
	{
		updateConfig(connection, p_oUserInfo, ConfigType.SYSTEM, p_sCategory, p_sKey, p_sNewValue, p_bEditOperation);
	}
	
	public static void updateCommonConfig(DbConnection connection, UserInfo p_oUserInfo, String p_sCategory, String p_sKey, String p_sNewValue, 
							boolean p_bEditOperation) throws SciException
	{
		updateConfig(connection, p_oUserInfo, ConfigType.COMMON, p_sCategory, p_sKey, p_sNewValue, p_bEditOperation);
	}
	public static void updateProjectConfig(DbConnection connection, UserInfo p_oUserInfo, String p_sCategory, String p_sKey, String p_sNewValue, 
			boolean p_bEditOperation, int projId) throws SciException
	{
		updateConfig(connection, p_oUserInfo, ConfigType.PROJECT, p_sCategory, p_sKey, p_sNewValue, p_bEditOperation,projId);
	}
	
	
	
	private static void updateConfig(DbConnection connection, UserInfo p_oUserInfo, ConfigType p_eConfigType, 
			String p_sCategory, String p_sKey, String p_sNewValue, boolean p_bEditOperation) throws SciException
	{
		
		if (p_bEditOperation)
		{
			DbQueryUpdate oUpdateQuery = null;
			
			if (p_eConfigType == ConfigType.SYSTEM)
			{
				oUpdateQuery = DbQueryUtils.createUpdateQuery(ConfigDbSchema.SYS_CONFIG, 
					DbQueryUtils.bindUpdateClause(ConfigDbSchema.SYS_CONFIG.CFGVALUE, ConfigDbSchema.SYS_CONFIG.DTLASTUPDATED, ConfigDbSchema.SYS_CONFIG.USRLASTUPDATED), 
					ConfigDbSchema.SYS_CONFIG.CFGKEY.bindClause());
			}
			else if (p_eConfigType == ConfigType.COMMON)
			{
				oUpdateQuery = DbQueryUtils.createUpdateQuery(ConfigDbSchema.CMN_CONFIG, 
					DbQueryUtils.bindUpdateClause(ConfigDbSchema.CMN_CONFIG.CFGVALUE, ConfigDbSchema.CMN_CONFIG.DTLASTUPDATED, ConfigDbSchema.CMN_CONFIG.USRLASTUPDATED), 
					ConfigDbSchema.CMN_CONFIG.CFGKEY.bindClause());
			}else if (p_eConfigType == ConfigType.PROJECT)
			{
				oUpdateQuery = DbQueryUtils.createUpdateQuery(ConfigDbSchema.PRJ_CONFIG, 
					DbQueryUtils.bindUpdateClause(ConfigDbSchema.PRJ_CONFIG.CFGVALUE, ConfigDbSchema.PRJ_CONFIG.DTLASTUPDATED, ConfigDbSchema.PRJ_CONFIG.USRLASTUPDATED), 
					DbQueryUtils.bindWhereClauseWithAnd(ConfigDbSchema.PRJ_CONFIG.CFGKEY.bindClause(),ConfigDbSchema.PRJ_CONFIG.PRJSEQID.bindClause()));
			}
			else
			{
				log.error("Config type not supported - {}", p_eConfigType.toString());
				return;
			}
			oUpdateQuery.addUpdateParameter(p_sNewValue);
			oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());
			
			oUpdateQuery.addWhereClauseParameter(p_sKey);
			
			if(p_eConfigType == ConfigType.PROJECT){
			oUpdateQuery.addWhereClauseParameter(ProjectUserManagementHomeImpl.getProjectIdByUserSeqId(connection, p_oUserInfo.getUserSeqId()));
			}
			try
			{
				connection.startTransaction();
				
				connection.executeQuery(oUpdateQuery);
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating config", dbExcep);
				throw new SciException("Error updating config", dbExcep);
			}
		}
		else
		{
			DbQueryInsert oInsertQuery = null;
			
			if (p_eConfigType == ConfigType.SYSTEM)
			{
				oInsertQuery = DbQueryUtils.createInsertQuery(ConfigDbSchema.SYS_CONFIG);
			}
			else if (p_eConfigType == ConfigType.COMMON)
			{
				oInsertQuery = DbQueryUtils.createInsertQuery(ConfigDbSchema.CMN_CONFIG);
			}else if (p_eConfigType == ConfigType.PROJECT)
			{
				oInsertQuery = DbQueryUtils.createInsertQuery(ConfigDbSchema.PRJ_CONFIG);
			}
			else
			{
				log.error("Config type not supported - {}", p_eConfigType.toString());
				return;
			}
			
			oInsertQuery.addInsertParameter(p_sCategory);
			oInsertQuery.addInsertParameter(p_sKey);
			oInsertQuery.addInsertParameter(p_sNewValue);
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
			
			try
			{
				connection.startTransaction();
				
				connection.executeQuery(oInsertQuery);
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating config", dbExcep);
				throw new SciException("Error updating config", dbExcep);
			}
		}
	}
	
	private static void updateConfig(DbConnection connection, UserInfo p_oUserInfo, ConfigType p_eConfigType, 
			String p_sCategory, String p_sKey, String p_sNewValue, boolean p_bEditOperation, int prjId) throws SciException
	{
		
		if (p_bEditOperation)
		{
			DbQueryUpdate oUpdateQuery = null;
			
			
				oUpdateQuery = DbQueryUtils.createUpdateQuery(ConfigDbSchema.PRJ_CONFIG, 
					DbQueryUtils.bindUpdateClause(ConfigDbSchema.PRJ_CONFIG.CFGVALUE, ConfigDbSchema.PRJ_CONFIG.DTLASTUPDATED, ConfigDbSchema.PRJ_CONFIG.USRLASTUPDATED), 
					DbQueryUtils.bindWhereClauseWithAnd(ConfigDbSchema.PRJ_CONFIG.CFGKEY.bindClause(),ConfigDbSchema.PRJ_CONFIG.PRJSEQID.bindClause()));
			
			oUpdateQuery.addUpdateParameter(p_sNewValue);
			oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());
			
			oUpdateQuery.addWhereClauseParameter(p_sKey);
			oUpdateQuery.addWhereClauseParameter(prjId);
			try
			{
				connection.startTransaction();
				
				connection.executeQuery(oUpdateQuery);
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating config", dbExcep);
				throw new SciException("Error updating config", dbExcep);
			}
		}
		else
		{
			DbQueryInsert oInsertQuery = null;
			
			oInsertQuery = DbQueryUtils.createInsertQuery(ConfigDbSchema.PRJ_CONFIG);
			
			oInsertQuery.addInsertParameter(p_sCategory);
			oInsertQuery.addInsertParameter(p_sKey);
			oInsertQuery.addInsertParameter(p_sNewValue);
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
			
			try
			{
				connection.startTransaction();
				
				connection.executeQuery(oInsertQuery);
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating config", dbExcep);
				throw new SciException("Error updating config", dbExcep);
			}
		}
	}
	
	public static List<ConfigObj> loadSysConfig(DbConnection connection) throws SciException
	{
		return loadConfig(connection, ConfigType.SYSTEM,-1);
	}
	
	public static List<ConfigObj> loadCommonConfig(DbConnection connection) throws SciException
	{
		return loadConfig(connection, ConfigType.COMMON,-1);
	}
	public static List<ConfigObj> loadPrjConfig(DbConnection connection, int nProjectSeqId) throws SciException
	{
		return loadConfig(connection, ConfigType.PROJECT, nProjectSeqId);
	}
	
	private static List<ConfigObj> loadConfig(DbConnection connection, ConfigType p_eConfigType,int nProjectSeqId) throws SciException
	{
		ConfigObj oConfig = null;
		List<ConfigObj> listConfig = null;
		DbQuerySelect oSelectQuery = null;
		
		listConfig = new ArrayList<ConfigObj>();
		
		if (p_eConfigType == ConfigType.SYSTEM)
		{
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigDbSchema.SYS_CONFIG, null);
		}
		else if (p_eConfigType == ConfigType.COMMON)
		{
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigDbSchema.CMN_CONFIG, null);
		}
		else if (p_eConfigType == ConfigType.PROJECT)
		{
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigDbSchema.PRJ_CONFIG,ConfigDbSchema.PRJ_CONFIG.PRJSEQID.bindClause(), ConfigDbSchema.PRJ_CONFIG.CFGKEY.name());
			oSelectQuery.addWhereClauseParameter(nProjectSeqId);
		}
		else
		{
			log.error("Config type not supported - {}", p_eConfigType.toString());
			return null;
		}
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					oConfig = new ConfigObj(p_eConfigType);
					if (p_eConfigType == ConfigType.PROJECT)
					{
						oConfig.setPrjSeq_Id(resultSet.readInt(ConfigDbSchema.PRJ_CONFIG.PRJSEQID));
					}
					oConfig.setAppName(resultSet.readString(ConfigDbSchema.SYS_CONFIG.CFGCATEGORY));
					oConfig.setKeyName(resultSet.readString(ConfigDbSchema.SYS_CONFIG.CFGKEY));
					oConfig.setSerializedValue(resultSet.readString(ConfigDbSchema.SYS_CONFIG.CFGVALUE));
					
					listConfig.add(oConfig);
				}
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading Sys Configuration from Db", dbExcep);
			throw new SciException("Error reading Sys Configuration from Db", dbExcep);
		}
		
		return listConfig;
	}
	}
