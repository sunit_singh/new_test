package com.sciformix.sciportal.config;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class ConfigDbSchema
{
	public static Config SYS_CONFIG = new Config("SYS_CONFIG");
	public static Config CMN_CONFIG = new Config("CMN_CONFIG");
	public static ProjectConfig PRJ_CONFIG = new ProjectConfig();
	public static ConfigMetadata CONFIG_MD = new ConfigMetadata();
	
	public static class Config extends DbTable
	{
		public DbColumn CFGCATEGORY = null;
		public DbColumn CFGKEY = null;
		public DbColumn CFGVALUE = null;
		public DbColumn DTLASTUPDATED = null;
		public DbColumn USRLASTUPDATED = null;
		
		public Config(String p_sTableName)
		{
			super(p_sTableName);
			setPkSequenceName(null);
			
			CFGCATEGORY = addColumn("CFGCATEGORY");
			CFGKEY = addColumn("CFGKEY");
			CFGVALUE = addColumn("CFGVALUE");
			DTLASTUPDATED = addColumn("DTLASTUPDATED");
			USRLASTUPDATED = addColumn("USRLASTUPDATED");
		}
	}
	
	
	public static class ConfigMetadata extends DbTable
	{
		public DbColumn CFGTYPE = null;
		public DbColumn CFGCATEGORY = null;
		public DbColumn SERIALNO = null;
		public DbColumn CFGKEY = null;
		public DbColumn CFGDISPLAYNAME = null;
		public DbColumn CFGVALUETYPE = null;
		public DbColumn CFGTOOLTIP = null;
		public DbColumn CFGDEFAULTVALUE = null;
		public DbColumn CFGVISIBILITY = null;
		
		public ConfigMetadata()
		{
			super("CMN_CONFIG_MD");
			setPkSequenceName(null);
			
			CFGTYPE = addColumn("CFGTYPE");
			CFGCATEGORY = addColumn("CFGCATEGORY");
			SERIALNO = addColumn("SERIALNO");
			CFGKEY = addColumn("CFGKEY");
			CFGDISPLAYNAME = addColumn("CFGDISPLAYNAME");
			CFGVALUETYPE = addColumn("CFGVALUETYPE");
			CFGTOOLTIP = addColumn("CFGTOOLTIP");
			CFGDEFAULTVALUE = addColumn("CFGDEFAULTVALUE");
			CFGVISIBILITY = addColumn("CFGVISIBILITY");
		}
	}
	
	public static class ProjectConfig extends DbTable
	{
		public DbColumn PRJSEQID=null;
		public DbColumn CFGCATEGORY = null;
		public DbColumn CFGKEY = null;
		public DbColumn CFGVALUE = null;
		public DbColumn DTLASTUPDATED = null;
		public DbColumn USRLASTUPDATED = null;
		
		public ProjectConfig()
		{
			super("PRJ_CONFIG");
			setPkSequenceName("PRJ_CONFIG_PRJSEQID_SEQ");
			
			PRJSEQID=addColumn("PRJSEQID");
			CFGCATEGORY = addColumn("CFGCATEGORY");
			CFGKEY = addColumn("CFGKEY");
			CFGVALUE = addColumn("CFGVALUE");
			DTLASTUPDATED = addColumn("DTLASTUPDATED");
			USRLASTUPDATED = addColumn("USRLASTUPDATED");
		}
	}
}
