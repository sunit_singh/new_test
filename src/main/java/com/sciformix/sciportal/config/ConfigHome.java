package com.sciformix.sciportal.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.project.ProjectHome;
import com.sciformix.sciportal.project.ProjectInfo;
import com.sciformix.sciportal.user.UserSession;

public class ConfigHome
{
	private static final Logger log = LoggerFactory.getLogger(ConfigHome.class);
	
	private ConfigHome()
	{
		//Nothing to do
	}
	
	private static Map<String, String> s_mapAllSysConfig = null;
	private static Map<String, Map<String, String>> s_mapSysCategorySpecificConfig = null;

	private static Map<String, String> s_mapAllAppConfig = null;
	private static Map<String, Map<String, String>> s_mapAppSpecificConfig = null;
	
	private static Map<String, ConfigMetadata> s_mapSysConfigMetadata = null;
	private static Map<String, ConfigMetadata> s_mapAppConfigMetadata = null;
	private static Map<String, ConfigMetadata> s_mapPrjConfigMetadata = null;
	
	private static Map<String, Map<String, String>> mapPrjCategorySpecificConfig = null;
	private static Map<String,Map<String,String>> mapPrjConfigIdWise=null;
	
	private static Map<Integer,Map<String,Map<String,String>>> mapPrjConfigIdAndCategoryWise=null;
	private static Map<String, Map<String, String>> mapCategorySpecificConfig = null;
	
	
	public static boolean init(DbConnection connection, SciStartupLogger oStartupLogger) throws SciException
	{
		List<ConfigObj> listConfig = null;
		List<ConfigMetadata> listConfigMetadata = null;
		
		s_mapAllSysConfig = new HashMap<String, String>();
		s_mapSysCategorySpecificConfig = new HashMap<String, Map<String, String>>();
		
		listConfig = ConfigHomeImpl.loadSysConfig(connection);
		for (ConfigObj oSysConfig : listConfig)
		{
			if (s_mapSysCategorySpecificConfig.get(oSysConfig.getAppName()) == null)
			{
				s_mapSysCategorySpecificConfig.put(oSysConfig.getAppName(), new HashMap<String, String>());
			}
			
			//DEV-NOTE: It is okay for any existing value of the config to be overwritten as there is no support for revisioning 
			s_mapSysCategorySpecificConfig.get(oSysConfig.getAppName()).put(oSysConfig.getKeyName(), oSysConfig.getSerializedValue());
			
			s_mapAllSysConfig.put(oSysConfig.getKeyName(), oSysConfig.getSerializedValue());
		}
		
		s_mapAllAppConfig = new HashMap<String, String>();
		s_mapAppSpecificConfig = new HashMap<String, Map<String, String>>();
		
		listConfig = ConfigHomeImpl.loadCommonConfig(connection);
		
		for (ConfigObj oAppConfig : listConfig)
		{
			if (s_mapAppSpecificConfig.get(oAppConfig.getAppName()) == null)
			{
				s_mapAppSpecificConfig.put(oAppConfig.getAppName(), new HashMap<String, String>());
			}
			
			//DEV-NOTE: It is okay for any existing value of the config to be overwritten as there is no support for revisioning 
			s_mapAppSpecificConfig.get(oAppConfig.getAppName()).put(oAppConfig.getKeyName(), oAppConfig.getSerializedValue());
			
			s_mapAllAppConfig.put(oAppConfig.getKeyName(), oAppConfig.getSerializedValue());
		}
	
		List<ProjectInfo> listProjects=null;
		Map<String,String> mapProjectIdWise=null;
		mapPrjConfigIdWise=new HashMap<>();
		
		mapPrjCategorySpecificConfig = new HashMap<String, Map<String, String>>();
		mapPrjConfigIdAndCategoryWise = new HashMap<>();
	
		//TODO:Call 'loadPrjConfig' in a loop passing ProjectId
		try {
			listProjects=ProjectHome.getAllProjects();
		} catch (SciServiceException e) {
			log.error("Error retrieve projects", e);
			throw new SciException("Error retrieve project", e);
		}
		for (ProjectInfo projectInfo:listProjects)
		{
			mapProjectIdWise=new HashMap<>();
			mapCategorySpecificConfig	=	new LinkedHashMap<>();
			listConfig = ConfigHomeImpl.loadPrjConfig(connection, projectInfo.getProjectSequenceId());
			for (ConfigObj oPrjConfig : listConfig)
			{
				if (mapPrjCategorySpecificConfig.get(oPrjConfig.getAppName()) == null)
				{
					mapPrjCategorySpecificConfig.put(oPrjConfig.getAppName(), new LinkedHashMap<String, String>());
					
				}
				mapPrjCategorySpecificConfig.get(oPrjConfig.getAppName()).put(StringConstants.HASH+oPrjConfig.getPrjSeq_Id()+StringConstants.HASH+oPrjConfig.getKeyName(), oPrjConfig.getSerializedValue());
				if(mapCategorySpecificConfig.get(oPrjConfig.getAppName()) == null)
				{
					mapCategorySpecificConfig.put(oPrjConfig.getAppName(), new LinkedHashMap<String, String>());
				}
				mapProjectIdWise.put(oPrjConfig.getKeyName(), oPrjConfig.getSerializedValue());
//				mapCategorySpecificConfig.get(oPrjConfig.getAppName()).put(oPrjConfig.getKeyName(), oPrjConfig.getSerializedValue());
//				mapCategorySpecificConfig.get(oPrjConfig.getAppName()).putAll(mapProjectIdWise); --- commented by prakash
				mapCategorySpecificConfig.get(oPrjConfig.getAppName()).put(oPrjConfig.getKeyName(), oPrjConfig.getSerializedValue());
				
			}
			mapPrjConfigIdWise.put(StringConstants.HASH+projectInfo.getProjectSequenceId(), mapProjectIdWise);
			mapPrjConfigIdAndCategoryWise.put(projectInfo.getProjectSequenceId(), mapCategorySpecificConfig);
		}
		
		/* changes for prj-config starts here*/
		
		try {
			listProjects=ProjectHome.getAllProjects();
		} catch (SciServiceException e) {
			log.error("Error retrieve projects", e);
			throw new SciException("Error retrieve project", e);
		}
		
		
		s_mapSysConfigMetadata = new HashMap<String, ConfigMetadata>();
		listConfigMetadata= ConfigHomeImpl.loadSysConfigMetadata(connection);
		for (ConfigMetadata oSysConfigMedata : listConfigMetadata)
		{
			s_mapSysConfigMetadata.put(oSysConfigMedata.getKey(), oSysConfigMedata);
		}
		
		s_mapAppConfigMetadata = new HashMap<String, ConfigMetadata>();
		
		listConfigMetadata= ConfigHomeImpl.loadCommonConfigMetadata(connection);
		for (ConfigMetadata oAppConfigMedata : listConfigMetadata)
		{
			s_mapAppConfigMetadata.put(oAppConfigMedata.getKey(), oAppConfigMedata);
		}
		
		return true;
	}
	
	public static String getPrimaryDbInfo()
	{
		return DbConnectionManager.getPrimaryDbInfo();
	}
	
	public static ConfigMetadata getSysConfigMetadata(String p_sKey)
	{
		return s_mapSysConfigMetadata.get(p_sKey);
	}
	
	public static Map<String, String> getSysConfigSettings(String p_sCategoryId)
	{
		return s_mapSysCategorySpecificConfig.get(p_sCategoryId);
	}
	
	public static String getSysConfigSetting(String p_sCategoryId, String p_sKey)
	{
		if (s_mapSysCategorySpecificConfig.get(p_sCategoryId) != null)
		{
			return s_mapSysCategorySpecificConfig.get(p_sCategoryId).get(p_sKey);
		}
		else
		{
			//Invalid Category or Category with no settings
			return null;
		}
	}
	
	public static String getSysConfigSetting(String p_sKey)
	{
		return s_mapAllSysConfig.get(p_sKey);
	}
	
	public static boolean isSysConfigSettingPresent(String p_sKey)
	{
		return s_mapAllSysConfig.containsKey(p_sKey);
	}
	
	public static void updateSysConfig(DbConnection connection, UserSession p_oUserSession, String p_sCategory, String p_sKey, String p_sNewValue) throws SciException
	{
		AuditInfoDb oAuditInfo = null;
		String sCurrentValue = null;
		boolean bSettingPresent = false;
		
		bSettingPresent = isSysConfigSettingPresent(p_sKey);
		sCurrentValue = getSysConfigSetting(p_sKey);
		
		if (bSettingPresent)
		{
			oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, "SYS-CONFIG", "System Config updated");
		}
		else
		{
			oAuditInfo = AuditHome.createRecordInsertStub(p_oUserSession, "SYS-CONFIG", "System Config created");
		}
		oAuditInfo.addPayload("Config Key", p_sKey);
		oAuditInfo.addDetails("Config Value", sCurrentValue, p_sNewValue);
		
		try
		{
			connection.startTransaction();
			
			ConfigHomeImpl.updateSysConfig(connection, p_oUserSession.getUserInfo(), p_sCategory, p_sKey, p_sNewValue, bSettingPresent);
			
			AuditHome.savePopulatedStub(connection, oAuditInfo);
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			 connection.rollbackTransaction();
			 log.error("Error updating system config", dbExcep);
			 throw new SciException("Error updating system config", dbExcep);
		}
		catch (SciException sciExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating system config", sciExcep);
			throw sciExcep;
		}
	}
	
	public static void updateAppConfig(DbConnection connection, UserSession p_oUserSession, 
			String p_sAppId, String p_sKey, String p_sNewValue) throws SciException
	{
		AuditInfoDb oAuditInfo = null;
		String sCurrentValue = null;
		boolean bSettingPresent = false;
		
		bSettingPresent = isAppConfigSettingPresent(p_sKey);
		sCurrentValue = getAppConfigSetting(p_sKey);
		
		if (bSettingPresent)
		{
			oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, "CMN-CONFIG", "Config updated");
		}
		else
		{
			oAuditInfo = AuditHome.createRecordInsertStub(p_oUserSession, "CMN-CONFIG", "Config created");
		}
		oAuditInfo.addPayload("Config Key", p_sKey);
		oAuditInfo.addDetails("Config Value", sCurrentValue, p_sNewValue);
		
		try
		{
			connection.startTransaction();
			
			ConfigHomeImpl.updateCommonConfig(connection, p_oUserSession.getUserInfo(), p_sAppId, p_sKey, p_sNewValue, bSettingPresent);
			
			AuditHome.savePopulatedStub(connection, oAuditInfo);
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating app config", dbExcep);
			throw new SciException("Error updating app config", dbExcep);
		}
		catch (SciException sciExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating app config", sciExcep);
			throw sciExcep;
		}
	}
	
	public static ConfigMetadata getAppConfigMetadata(String p_sKey)
	{
		return s_mapAppConfigMetadata.get(p_sKey);
	}
	
	public static Map<String, String> getAppConfigSettings(String p_sAppId)
	{
		return s_mapAppSpecificConfig.get(p_sAppId);
	}
	
	public static String getAppConfigSetting(String p_sAppId, String p_sKey)
	{
		if (s_mapAppSpecificConfig.get(p_sAppId) != null)
		{
			return s_mapAppSpecificConfig.get(p_sAppId).get(p_sKey);
		}
		else
		{
			//Invalid App or App with no settings
			return null;
		}
	}
	
	public static String getAppConfigSetting(String p_sKey)
	{
		return s_mapAllAppConfig.get(p_sKey);
	}
	
	public static boolean isAppConfigSettingPresent(String p_sKey)
	{
		return s_mapAllAppConfig.containsKey(p_sKey);
	}
	
	public static String getPrjConfigSetting(String p_sKey,String appId,int p_nPrjId)
	{
		if (mapPrjCategorySpecificConfig.get(appId)!= null)
		{
			return mapPrjCategorySpecificConfig.get(appId).get(StringConstants.HASH+p_nPrjId+StringConstants.HASH+p_sKey);
		}
		else
		{
			//Invalid Category or Category with no settings
			return null;
		}
		
	}
	
	
	public static String getPrjConfigSettings(String p_sKey,int p_nPrjId) 
	{
		if(mapPrjConfigIdWise != null)
		{
			if(mapPrjConfigIdWise.get(StringConstants.HASH+p_nPrjId) != null)
			{
				return mapPrjConfigIdWise.get(StringConstants.HASH+p_nPrjId).get(p_sKey);
				
			}
		}
		return null;
	}
	
	public static boolean isPrjConfigSettingPresent(String p_sKey,int p_nPrjId)
	{
		return mapPrjConfigIdWise.get(StringConstants.HASH+p_nPrjId).containsKey(p_sKey);
		
	}
	//update project config setting
	public static void updatePrjConfig(DbConnection connection, UserSession p_oUserSession, 
				String p_sAppId, String p_sKey, String p_sNewValue, int p_nPrjId) throws SciException
		{
			AuditInfoDb oAuditInfo = null;
			String sCurrentValue = null;
			boolean bSettingPresent = false;
			ConfigMetadata oAppConfigMetadata = null;
			
			bSettingPresent = isPrjConfigSettingPresent(p_sKey,p_nPrjId);
			sCurrentValue = getPrjConfigSettings(p_sKey, p_nPrjId);
			
			if(DataUtils.hasChanged(StringUtils.emptyString(sCurrentValue), StringUtils.emptyString(p_sNewValue))){			
			
			if (bSettingPresent)
			{
				oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, "PRJ-CONFIG", "Config updated", p_nPrjId);
			}
			else
			{
				oAuditInfo = AuditHome.createRecordInsertStub(p_oUserSession, "PRJ-CONFIG", "Config created", p_nPrjId);
			}
			oAppConfigMetadata = AppRegistry.getAppConfigMetadata(p_sKey);
			oAuditInfo.addPayload("Project Config Key", oAppConfigMetadata.getDisplayName());
			oAuditInfo.addDetails("Project Config Value", sCurrentValue, p_sNewValue);
			
			try
			{
				connection.startTransaction();
				
				ConfigHomeImpl.updateProjectConfig(connection, p_oUserSession.getUserInfo(), p_sAppId, p_sKey, p_sNewValue, bSettingPresent, p_nPrjId);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating project config", dbExcep);
				throw new SciException("Error updating project config", dbExcep);
			}
			
			}
			
		}
	
	public static List<ConfigObj> loadProjConfig(DbConnection connection,int nProjectSeqId) throws SciException{
		List<ConfigObj> listOfConfigObj=null;
		try
		{
			connection.startTransaction();
			
			listOfConfigObj=ConfigHomeImpl.loadPrjConfig(connection, nProjectSeqId);
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error retrieve project config", dbExcep);
			throw new SciException("Error retrieve project config", dbExcep);
		}
	return listOfConfigObj;
	}
	
	public static Map<String,Map<String,String>>getPrjConfigSetting(int p_nPrjId)
	{
		if (mapPrjConfigIdAndCategoryWise.get(p_nPrjId)!= null)
		{
			return mapPrjConfigIdAndCategoryWise.get(p_nPrjId);
		}
		else
		{
			//Invalid Category or Category with no settings
			return null;
		}
		
	}
}
