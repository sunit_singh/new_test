package com.sciformix.sciportal.config;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class 
	ConfigTplObjDbSchema
{
	public static final ConfigObject	
		CONFIGOBJECT	=	new ConfigObject();
	
	public static class ConfigObject extends DbTable {
		public DbColumn	CTOSEQID			=	null;
		public DbColumn	PRJSEQID			=	null;
		public DbColumn	CTOTYPE				=	null;
		public DbColumn	CTONAME				=	null;
		public DbColumn	CTOSTATE         	=	null;
		public DbColumn	CREATIONMODE		=	null;
		public DbColumn	DTCREATED			=	null;
		public DbColumn	USRCREATED			=	null;
		public DbColumn	DTLASTUPDATEDREC	=	null;
		public DbColumn	USRLASTUPDATEDREC	=	null;
		public DbColumn	DTLASTUPDATEDGRP	=	null;
		public DbColumn	USRLASTUPDATEDGRP	=	null;
		public DbColumn	CTOUDF01			=	null;
		public DbColumn	CTOUDF02			=	null;
		public DbColumn	CTOUDF03			=	null;
		public DbColumn	CTOUDF04			=	null;
		public DbColumn	CTOUDF05			=	null;
		public DbColumn	CTOUDF06			=	null;
		public DbColumn	CTOUDF07			=	null;
		public DbColumn	CTOUDF08			=	null;
		public DbColumn	CTOUDF09			=	null;
		public DbColumn	CTOUDF10			=	null;
		
		public ConfigObject() {
			super("CMN_CONFIGTPLOBJ");
			setPkSequenceName("CMN_CONFIGTPLOBJ_SEQ");
			
			CTOSEQID			=	addColumn("CTOSEQID");
			PRJSEQID			=	addColumn("PRJSEQID");
			CTOTYPE				=	addColumn("CTOTYPE");
			CTONAME				=	addColumn("CTONAME");
			CTOSTATE        	=	addColumn("CTOSTATE");
			CREATIONMODE		=	addColumn("CREATIONMODE");
			DTCREATED			=	addColumn("DTCREATED");
			USRCREATED			=	addColumn("USRCREATED");
			DTLASTUPDATEDREC	=	addColumn("DTLASTUPDATEDREC");
			USRLASTUPDATEDREC	=	addColumn("USRLASTUPDATEDREC");
			DTLASTUPDATEDGRP	=	addColumn("DTLASTUPDATEDGRP");
			USRLASTUPDATEDGRP	=	addColumn("USRLASTUPDATEDGRP");
			CTOUDF01			=	addColumn("CTOUDF01");
			CTOUDF02			=	addColumn("CTOUDF02");
			CTOUDF03			=	addColumn("CTOUDF03");
			CTOUDF04			=	addColumn("CTOUDF04");
			CTOUDF05			=	addColumn("CTOUDF05");
			CTOUDF06			=	addColumn("CTOUDF06");
			CTOUDF07			=	addColumn("CTOUDF07");
			CTOUDF08			=	addColumn("CTOUDF08");
			CTOUDF09			=	addColumn("CTOUDF09");
			CTOUDF10			=	addColumn("CTOUDF10");
			
		}
		
	}
}