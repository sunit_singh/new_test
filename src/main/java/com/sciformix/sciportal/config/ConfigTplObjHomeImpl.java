package com.sciformix.sciportal.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUpdate;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.user.UserInfo;


public class ConfigTplObjHomeImpl {
	
	private static final Logger
		log	=	LoggerFactory.getLogger(ConfigTplObjHomeImpl.class)
	;
	
	/**
	 * This will list all the templates for the given project and 
	 * config template object type. If the config template object
	 * type is null then it will fetch all the templates that
	 * belongs to a project.
	 * @param p_connection	DB Connection.
	 * @param p_projectid	The project id to which user belongs.
	 * @param p_cotype		The config template object type.
	 * @return				List of templates.
	 * @throws SciException
	 */
	public static List<ConfigTplObj> getListOfTemplates (
		DbConnection		p_connection	
	,	int					p_projectid
	,	ConfigTplObjType	p_ctotype
	) throws SciException {
		List<ConfigTplObj>	l_configtplobjlist	=	null;
		DbQuerySelect		l_selectquery		=	null;
		boolean				l_fetchalltemplate	=	false;
		
		if (p_ctotype != null) {
			l_fetchalltemplate	=	false;
		} else {
			l_fetchalltemplate	=	true;
		}
		
		if (l_fetchalltemplate) {
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									ConfigTplObjDbSchema.CONFIGOBJECT
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.PRJSEQID.bindClause()
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.name()
								);
			l_selectquery.addWhereClauseParameter(p_projectid);
		} else {
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									ConfigTplObjDbSchema.CONFIGOBJECT
								, 	DbQueryUtils.bindWhereClauseWithAnd(
										ConfigTplObjDbSchema.CONFIGOBJECT.PRJSEQID
									, 	ConfigTplObjDbSchema.CONFIGOBJECT.CTOTYPE
									)
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.name()
								);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(p_ctotype.getCode());
		}
		
		l_configtplobjlist	=	fetchConfigTplObjs(p_connection, l_selectquery);
		
		
		return l_configtplobjlist;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_templateseqid
	 * @return
	 * @throws SciException
	 */
	public static ConfigTplObj getTemplateDetails (
		DbConnection		p_connection	
	,	int					p_templateseqid
	) throws SciException {
		ConfigTplObj	l_configtplobjlist	=	null;
		DbQuerySelect	l_selectquery		=	null;
		
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								ConfigTplObjDbSchema.CONFIGOBJECT
							, 	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.bindClause()
							,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.name()
							);
		l_selectquery.addWhereClauseParameter(p_templateseqid);
		
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				while (l_rs.next()) {
					l_configtplobjlist	=	readConfigTplObj(l_rs);
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching list of templates", dbExcep);
			throw new SciException("Error fetching list of templates", dbExcep);
		}
		return l_configtplobjlist;
	}
	
	public static ConfigTplObj getTemplateDetailsByName (
		DbConnection		p_connection
	,	ConfigTplObjType	p_ctotype
	,	int					p_projectid
	,	String				p_templatename
	) throws SciException {
		ConfigTplObj	l_configtplobjlist	=	null;
		DbQuerySelect	l_selectquery		=	null;
		
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								ConfigTplObjDbSchema.CONFIGOBJECT
							, 	DbQueryUtils.bindWhereClauseWithAnd(
									ConfigTplObjDbSchema.CONFIGOBJECT.CTOTYPE
								,	ConfigTplObjDbSchema.CONFIGOBJECT.PRJSEQID
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME
								)
							);
		l_selectquery.addWhereClauseParameter(p_ctotype.getCode());
		l_selectquery.addWhereClauseParameter(p_projectid);
		l_selectquery.addWhereClauseParameter(p_templatename);
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				while (l_rs.next()) {
					l_configtplobjlist	=	readConfigTplObj(l_rs);
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching list of templates", dbExcep);
			throw new SciException("Error fetching list of templates", dbExcep);
		}
		return l_configtplobjlist;
	}
	
	public static ConfigTplObj getActiveTemplateDetails (
		DbConnection		p_connection
	,	ConfigTplObjType	p_ctotype
	,	int					p_projectid
	) throws SciException {
		ConfigTplObj	l_configtplobjlist	=	null;
		DbQuerySelect	l_selectquery		=	null;
		
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								ConfigTplObjDbSchema.CONFIGOBJECT
							, 	DbQueryUtils.bindWhereClauseWithAnd(
									ConfigTplObjDbSchema.CONFIGOBJECT.CTOSTATE
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOTYPE
								,	ConfigTplObjDbSchema.CONFIGOBJECT.PRJSEQID
								)
							);
		l_selectquery.addWhereClauseParameter(RecordState.ACTIVE.value());
		l_selectquery.addWhereClauseParameter(p_ctotype.getCode()); //TODO: if it is null give error
		l_selectquery.addWhereClauseParameter(p_projectid);
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				while (l_rs.next()) {
					l_configtplobjlist	=	readConfigTplObj(l_rs);
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching list of templates", dbExcep);
			throw new SciException("Error fetching list of templates", dbExcep);
		}
		return l_configtplobjlist;
	}
	
	/**
	 * This method is used internally to fetch result based on the query
	 * passed.
	 * @param p_connection	DB Connection.
	 * @param p_selectquery	Query which needs to be executed.
	 * @return				List of all the templates
	 * @throws SciException
	 */
	private static List<ConfigTplObj> fetchConfigTplObjs (
		DbConnection		p_connection
	,	DbQuerySelect		p_selectquery
	) throws SciException {
		List<ConfigTplObj>	l_configtplobjlist		=	null;
		ConfigTplObj		l_configtemplateobj	=	null;
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(p_selectquery)) {
				if (l_rs != null) {
					l_configtplobjlist	=	new ArrayList<>();
					while (l_rs.next()) {
						l_configtemplateobj	=	readConfigTplObj (l_rs);
						l_configtplobjlist.add(l_configtemplateobj);
					}
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching list of templates", dbExcep);
			throw new SciException("Error fetching list of templates", dbExcep);
		}	
		return l_configtplobjlist;
	}
	
	/**
	 * This method is used internally to read the resultset and 
	 * create ConfigTplObj from it.
	 * @param p_rs	Data stored in the database.
	 * @return		ConfigTplObj 
	 * @throws SciDbException
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	private static ConfigTplObj readConfigTplObj(
		DbResultSet			p_rs
	) throws SciDbException, SciException {
		Class				l_class				=	null;
		ConfigTplObj	l_configtplobj	=	null;
		ConfigTplObjType l_configObjectType = null;
		
		l_configObjectType = ConfigTplObjType.toEnum(p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOTYPE));
		
		if (StringUtils.isNullOrEmpty(l_configObjectType.getClassName()))
		{
			l_configtplobj	=	new ConfigTplObj(l_configObjectType);
		}
		//TODO:Change to use reflection
		else
		{
			try {
				l_class			=	Class.forName(l_configObjectType.getClassName());
			} catch (ClassNotFoundException e) {
				log.error("Error fetching template details", e);
				throw new SciException("Error fetching template details", e);
			}
			try {
				l_configtplobj	=	(ConfigTplObj) l_class.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				log.error("Error fetching template details", e);
				throw new SciException("Error fetching template details", e);
		}
		}
		
		l_configtplobj.setSequenceId(p_rs.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID));
		
		l_configtplobj.setProjectId(p_rs.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.PRJSEQID));
		l_configtplobj.setTemplateName(p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME));
		l_configtplobj.setState(RecordState.toEnum(p_rs.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSTATE)));
		l_configtplobj.setCreationMode(CreationMode.toEnum(p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CREATIONMODE)));
		
		l_configtplobj.setCreatedBy(p_rs.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.USRCREATED));
		l_configtplobj.setCreatedOn(p_rs.readDate(ConfigTplObjDbSchema.CONFIGOBJECT.DTCREATED));
		l_configtplobj.setLastUpdatedBy(p_rs.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.USRLASTUPDATEDREC));
		l_configtplobj.setLastUpdatedOn(p_rs.readDate(ConfigTplObjDbSchema.CONFIGOBJECT.DTLASTUPDATEDREC));
		l_configtplobj.setLastUpdatedGrpBy(p_rs.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.USRLASTUPDATEDGRP));
		l_configtplobj.setLastUpdatedGrpOn(p_rs.readDate(ConfigTplObjDbSchema.CONFIGOBJECT.DTLASTUPDATEDGRP));
		
		l_configtplobj.setUdf(0, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF01));
		l_configtplobj.setUdf(1, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF02));
		l_configtplobj.setUdf(2, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF03));
		l_configtplobj.setUdf(3, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF04));
		l_configtplobj.setUdf(4, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF05));
		l_configtplobj.setUdf(5, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF06));
		l_configtplobj.setUdf(6, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF07));
		l_configtplobj.setUdf(7, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF08));
		l_configtplobj.setUdf(8, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF09));
		l_configtplobj.setUdf(9, p_rs.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF10));
		
		return l_configtplobj;
	}
	
	/**
	 * This method can be used to save the template config object data.
	 * @param p_connection			DB Connection.
	 * @param p_configtplobj	The object whose data need to be persisted.
	 * @return						Will update the object being passed to it.
	 * @throws SciException
	 */
	public static ConfigTplObj saveConfigTplObj (
		DbConnection	p_connection		
	,	ConfigTplObj	p_configtplobj
	,	UserInfo		p_userinfo
	) throws SciException {
		DbQueryInsert			l_insertquery		=	null;
		int						l_sequencid			=	0;
		
		try {
			p_connection.startTransaction();
			l_sequencid	=	DbQueryHome.generateSequenceValueForTablePk(ConfigTplObjDbSchema.CONFIGOBJECT);
			
//				p_coobjtemplatedata.setCreatedOn(System.currentTimeMillis());
//				p_coobjtemplatedata.setLastUpdatedOn(System.currentTimeMillis());
			
			l_insertquery = DbQueryUtils.createInsertQuery(ConfigTplObjDbSchema.CONFIGOBJECT);
			l_insertquery.addInsertParameter(l_sequencid);
			l_insertquery.addInsertParameter(p_configtplobj.getProjectId());
			l_insertquery.addInsertParameter(p_configtplobj.getConfigObjectType().getCode());
			l_insertquery.addInsertParameter(p_configtplobj.getTemplateName());
			l_insertquery.addInsertParameter(p_configtplobj.getState().value());
			l_insertquery.addInsertParameter(p_configtplobj.getCreationMode().getCode());
			l_insertquery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			l_insertquery.addInsertParameter(p_userinfo.getUserSeqId());
			l_insertquery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			l_insertquery.addInsertParameter(p_userinfo.getUserSeqId());
			l_insertquery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			l_insertquery.addInsertParameter(p_userinfo.getUserSeqId());
			
			for (int l_i=0; l_i<ConfigTplObj.TOTAL_NUMBER_OF_UDF; l_i++) {
				l_insertquery.addInsertParameter(p_configtplobj.getUdf(l_i));
			}
			p_connection.executeQuery(l_insertquery);
			p_connection.commitTransaction();
			
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving config object data into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error saving config object data into DB"
			,	dbExcep	
			);
		}
		p_configtplobj.setSequenceId(l_sequencid);
		return p_configtplobj;
	}
	
	/**
	 * This can be used to update the state of the template.
	 * @param p_connection	DVB Connection
	 * @param p_sequenceid	The template sequence id whose state
							need to be updated.
	 * @param p_state		The state to which it to be updated.
	 * @param p_userinfo	The information about the logged in user
	  						who is performing the action.
	 * @return				No. of rows updated.
	 * @throws SciException
	 */
	public static int updateConfigTplObjState (
		DbConnection	p_connection
	,	int				p_sequenceid
	,	RecordState		p_state
	,	UserInfo		p_userinfo
	) throws SciException {
		int				l_updaterowcount	=	-1;
		DbQueryUpdate 	l_updatequery 		= 	null;
		
		l_updatequery	=	DbQueryUtils.createUpdateQuery(
								ConfigTplObjDbSchema.CONFIGOBJECT
							,	DbQueryUtils.bindUpdateClause(
								 	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSTATE
								,	ConfigTplObjDbSchema.CONFIGOBJECT.DTLASTUPDATEDREC
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.USRLASTUPDATEDREC
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.DTLASTUPDATEDGRP
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.USRLASTUPDATEDGRP
								)
							,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.bindClause()
							);
		
		l_updatequery.addUpdateParameter(p_state.value());
		l_updatequery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		l_updatequery.addUpdateParameter(p_userinfo.getUserSeqId());
		l_updatequery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		l_updatequery.addUpdateParameter(p_userinfo.getUserSeqId());
		
		l_updatequery.addWhereClauseParameter(p_sequenceid);
		
		try {
			p_connection.startTransaction();
			l_updaterowcount	=	p_connection.executeQuery(l_updatequery);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error updating state of template into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error updating state of template into DB"
			,	dbExcep	
			);
		}
		return l_updaterowcount;
	}
	
	/**
	 * This can be used to update either name or udf values
	 * of config template object saved in DB.
	 * @param p_connection			DB Connection.
	 * @param p_configtplobj	Config template object data that 
	 								needs to be updated.
	 * @param p_userinfo			The information about the logged in
	  								user who is performing the action.
	 * @return						No. of rows updated.
	 * @throws SciException
	 */
	public static int updateConfigTplObjAttributes (
		DbConnection	p_connection
	,	ConfigTplObj	p_configtplobj	
	,	UserInfo		p_userinfo
	) throws SciException {
		int				l_updaterowcount	=	-1;
		DbQueryUpdate 	l_updatequery 		= 	null;
		
		if (p_configtplobj.getState() == RecordState.ACTIVE || p_configtplobj.getState() == RecordState.DEACTIVATED) {
			log.error(
				"Activated or Deactivated templates cannot be modified"
			);
			throw new SciException(
				"Activated or Deactivated templates cannot be modified"	
			);
		}
		
		l_updatequery	=	DbQueryUtils.createUpdateQuery(
								ConfigTplObjDbSchema.CONFIGOBJECT
							,	DbQueryUtils.bindUpdateClause(
								 	ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME
								,	ConfigTplObjDbSchema.CONFIGOBJECT.DTLASTUPDATEDREC
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.USRLASTUPDATEDREC
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.DTLASTUPDATEDGRP
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.USRLASTUPDATEDGRP
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF01
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF02
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF03
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF04
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF05
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF06
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF07
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF08
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF09
								,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF10
								)
							,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.bindClause()
							);
		
		l_updatequery.addUpdateParameter(p_configtplobj.getTemplateName());
		l_updatequery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		l_updatequery.addUpdateParameter(p_userinfo.getUserSeqId());
		l_updatequery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		l_updatequery.addUpdateParameter(p_userinfo.getUserSeqId());
		for (int l_i=0; l_i<ConfigTplObj.TOTAL_NUMBER_OF_UDF; l_i++) {
			l_updatequery.addUpdateParameter(p_configtplobj.getUdf(l_i));
		}
		l_updatequery.addWhereClauseParameter(p_configtplobj.getSequenceId());
		
		try {
			p_connection.startTransaction();
			l_updaterowcount	=	p_connection.executeQuery(l_updatequery);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error updating template attribute into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error updating template attribute into DB"
			,	dbExcep	
			);
		}
		return l_updaterowcount;
	}
	
	/**
	 * This can be used to update the group attributes of template
	 * if the child record is created or modified.
	 * @param p_connection	DB Connection
	 * @param p_sequenceid	The template sequence id whose state
							need to be updated.
	 * @param p_userinfo	The information about the logged in
	  						user who is performing the action.
	 * @return				No. of rows updated.
	 * @throws SciException
	 */
	public static int updateConfigTplObjGroup (
		DbConnection	p_connection
	,	int				p_sequenceid
	,	UserInfo		p_userinfo
	) throws SciException{
		int				l_updaterowcount	=	-1;
		DbQueryUpdate 	l_updatequery 		= 	null;
		
		l_updatequery	=	DbQueryUtils.createUpdateQuery(
								ConfigTplObjDbSchema.CONFIGOBJECT
							,	DbQueryUtils.bindUpdateClause(
								 	ConfigTplObjDbSchema.CONFIGOBJECT.DTLASTUPDATEDGRP
								, 	ConfigTplObjDbSchema.CONFIGOBJECT.USRLASTUPDATEDGRP
								)
							,	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.bindClause()
							);
		
		l_updatequery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		l_updatequery.addUpdateParameter(p_userinfo.getUserSeqId());
		
		l_updatequery.addWhereClauseParameter(p_sequenceid);
		
		try {
			p_connection.startTransaction();
			l_updaterowcount	=	p_connection.executeQuery(l_updatequery);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error updating group attribute of template into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error updating group attribute of template into DB"
			,	dbExcep	
			);
		}
		return l_updaterowcount;
	}
	
	public static boolean isConfigTplObjNameUnique (
		DbConnection		p_connection
	,	int					p_projectid
	,	ConfigTplObjType	p_ctotype
	,	String				p_proposedctoname
	) throws SciException {
		boolean	l_isctonameunique	=	false;
		l_isctonameunique	=	DbQueryHome.isObjectNameUnique(
									p_connection
								, 	"Config Template Object Name"
								,	ConfigTplObjDbSchema.CONFIGOBJECT
								,  	p_proposedctoname
								,  	ConfigTplObjDbSchema.CONFIGOBJECT.PRJSEQID.bindClause() + " and " + ConfigTplObjDbSchema.CONFIGOBJECT.CTOTYPE.bindClause() + " and " + ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME.bindClause()
								, 	DbQueryParam.createParameter(p_projectid)
								,	DbQueryParam.createParameter(p_ctotype.getCode())
								,	DbQueryParam.createParameter(p_proposedctoname)
								);
		return l_isctonameunique;
	}
}
