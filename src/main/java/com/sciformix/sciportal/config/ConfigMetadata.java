package com.sciformix.sciportal.config;

public class ConfigMetadata
{
	
	private String m_sCategory = null;
	private int m_nSerialNumber = -1;
	private String m_sKey = null;
	private String m_sDisplayName = null;
	private String m_sValueDatatype = null;
	private String m_sTooltip = null;
	private String m_sDefaultValue = null;
	private String m_sVisibility = null;
	
	public ConfigMetadata()
	{
		//Nothing to do
	}
	
	
	public String getCategory() {
		return m_sCategory;
	}
	public void setCategory(String p_sCategory) {
		this.m_sCategory = p_sCategory;
	}
	public int getSerialNumber() {
		return m_nSerialNumber;
	}
	public void setSerialNumber(int p_nSerialNumber) {
		this.m_nSerialNumber = p_nSerialNumber;
	}
	public String getKey() {
		return m_sKey;
	}
	public void setKey(String p_sKey) {
		this.m_sKey = p_sKey;
	}
	public String getDisplayName() {
		return m_sDisplayName;
	}
	public void setDisplayName(String p_sDisplayName) {
		this.m_sDisplayName = p_sDisplayName;
	}
	public String getValueDatatype() {
		return m_sValueDatatype;
	}
	public void setValueDatatype(String p_sValueDatatype) {
		this.m_sValueDatatype = p_sValueDatatype;
	}
	public String getTooltip() {
		return m_sTooltip;
	}
	public void setTooltip(String p_sTooltip) {
		this.m_sTooltip = p_sTooltip;
	}
	public String getDefaultValue() {
		return m_sDefaultValue;
	}
	public void setDefaultValue(String p_sDefaultValue) {
		this.m_sDefaultValue = p_sDefaultValue;
	}
	public String getVisibility()
	{
		return m_sVisibility;
	}
	public void setVisibility(String p_sVisibility){
		m_sVisibility = p_sVisibility;
	}

}
