package com.sciformix.sciportal.config;

import java.util.Date;

import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;

public class ConfigTplObj {
	/**
	 * The number of udf fields available in db.
	 */
	final static int
		TOTAL_NUMBER_OF_UDF	=	10;
	;

	/**
	 * This will store the sequence
	 * id of the template.
	 */
	private int
		sequenceId
	;
	/** 
	 * This is the name of the project to
	 *  which this template belongs.
	 */
	private int
		projectId
	;
	/**
	 * This is used to hold information about
	 * the type to which this template belongs.
	 */
	private ConfigTplObjType
		configObjectType
	;
	/**
	 * This is used to store the name of the template.
	 */
	private String
		templateName
	;
	/**
	 * This will hold the state of the config object
	 * created.
	 */
	private RecordState
		state
	;
	/**
	 * This is used to store the mode of template
	 * creation.
	 */
	private CreationMode
		creationMode
	;	
	/**
	 * This is used to store the date on which it
	 * is created.
	 */
	private Date
		createdOn
	;
	/**
	 * This is used to store the userid of the user
	 * who has created the template.
	 */
	private int
		createdBy
	;
	/**
	 * This is used to store the date on which it
	 * is modified.
	 */
	private Date
		lastUpdatedOn
	;
	/**
	 * This is used to store the userid of the user
	 * who has modified the template.
	 */
	private int
		lastUpdatedBy
	;
	/**
	 * This is used to store the date on which the
	 * template group is modified.
	 */
	private Date
		lastUpdatedGrpOn
	;
	/**
	 * This is used to store the userid of the user
	 * who has modified the template group.
	 */
	private int
		lastUpdatedGrpBy
	;
	/**
	 * This is used to store all the udf values stored
	 * in db.	
	 */
	private String []
		configTplObjUdf
	;
	
	{
		configTplObjUdf	=	new String [TOTAL_NUMBER_OF_UDF];
	}
	
	public ConfigTplObj(
			ConfigTplObjType 	p_configObjectType)
	{
		this.configObjectType 	= 	p_configObjectType;
	}
	
	/**
	 * This is used to call when inserting the data into db.
	 * @param p_projectId			The project id to which
	 *  							the template belongs.
	 * @param p_configObjectType	It is the type of template.
	 * @param p_templateName		Name of the template.
	 * @param p_state				State of the template.
	 * @param p_creationMode		Creation mode of the
	 * 								template.
	 */
	protected ConfigTplObj(
	 	int 				p_projectId
	, 	ConfigTplObjType 	p_configObjectType
	, 	String 				p_templateName
	,	RecordState 		p_state
	,	CreationMode		p_creationMode
	) {
		super();
		this.projectId			=	p_projectId;
		this.configObjectType	=	p_configObjectType;
		this.templateName 		=	p_templateName;
		this.state 				=	p_state;
		this.creationMode		=	p_creationMode;
	}
	
	/**
	 * This is used to call when fetching the data from db.
	 * @param p_sequenceId			Sequence Id of the template.
	 * @param p_projectId			Project Id to which the
	 * 								template belongs.
	 * @param p_configObjectType	It is the type of template.
	 * @param p_templateName		Name of the template.
	 * @param p_state				State of the template.
	 * @param p_creationMode		Creation mode of the
	 * 								template.
	 * @param p_createdOn			Date on which template
	 * 								got created.	
	 * @param p_createdBy			The user who has created
	 * 								the template.
	 * @param p_lastUpdatedOn		Date on which template
	 * 								got modified last.
	 * @param p_lastUpdatedBy		The user who has modified
	 * 								the template last.
	 * @param p_lastUpdatedGrpOn	Date on which template or
	 * 								it's child got modified last.
	 * @param p_lastUpdatedGrpBy	The user who has modified
	 * 								the template or it's child
	 *  							last.
	 * @param p_configTplObjUdf		The set of udf fields.
	 */
	protected ConfigTplObj(
		int 				p_sequenceId
	, 	int 				p_projectId
	, 	ConfigTplObjType 	p_configObjectType
	, 	String 				p_templateName
	,	RecordState 		p_state
	,	CreationMode		p_creationMode
	, 	Date 				p_createdOn
	, 	int 				p_createdBy
	, 	Date 				p_lastUpdatedOn
	,	int 				p_lastUpdatedBy
	, 	Date 				p_lastUpdatedGrpOn
	, 	int 				p_lastUpdatedGrpBy
	,	String []			p_configTplObjUdf
	) {
		super();
		this.sequenceId			=	p_sequenceId; 
		this.projectId			=	p_projectId;
		this.configObjectType	=	p_configObjectType;
		this.templateName 		=	p_templateName;
		this.state 				=	p_state;
		this.creationMode		=	p_creationMode;
		this.createdOn 			=	p_createdOn;
		this.createdBy 			=	p_createdBy;
		this.lastUpdatedOn 		=	p_lastUpdatedOn;
		this.lastUpdatedBy 		=	p_lastUpdatedBy;
		this.lastUpdatedGrpOn 	=	p_lastUpdatedGrpOn;
		this.lastUpdatedGrpBy 	=	p_lastUpdatedGrpBy;
		this.configTplObjUdf	=	p_configTplObjUdf;
		
	}
	public int getSequenceId() {
		return sequenceId;
	}
	
	public int getProjectId() {
		return projectId;
	}
	
	public ConfigTplObjType getConfigObjectType() {
		return configObjectType;
	}
	
	public String getTemplateName() {
		return templateName;
	}
	
	public RecordState getState() {
		return state;
	}
	
	public CreationMode getCreationMode() {
		return creationMode;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	
	public int getCreatedBy() {
		return createdBy;
	}
	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}
	
	public int getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public Date getLastUpdatedGrpOn() {
		return lastUpdatedGrpOn;
	}
	
	public int getLastUpdatedGrpBy() {
		return lastUpdatedGrpBy;
	}	
	
	public void setSequenceId(
		int sequenceId
	) {
		this.sequenceId = sequenceId;
	}
	
	public void setCreationMode(
		CreationMode creationMode
	) {
		this.creationMode = creationMode;
	}
	
	public void setCreatedOn(
		Date createdOn
	) {
		this.createdOn = createdOn;
	}

	public void setCreatedBy(
		int createdBy
	) {
		this.createdBy = createdBy;
	}
	
	public void setProjectId(
		int projectId
	) {
		this.projectId = projectId;
	}

	public void setTemplateName(
		String templateName
	) {
		this.templateName = templateName;
	}

	public void setState(
		RecordState state
	) {
		this.state = state;
	}

	public void setLastUpdatedOn(
		Date lastUpdatedOn
	) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public void setLastUpdatedBy(
		int lastUpdatedBy
	) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public void setLastUpdatedGrpOn(
		Date lastUpdatedGrpOn
	) {
		this.lastUpdatedGrpOn = lastUpdatedGrpOn;
	}

	public void setLastUpdatedGrpBy(
		int lastUpdatedGrpBy
	) {
		this.lastUpdatedGrpBy = lastUpdatedGrpBy;
	}
	
	/**
	 * This method is used to set the udf value of the index passed
	 * by the user.
	 * @param p_index	The index whose value needs to be set.
	 * @return			The value of the udf stored at index passed.
	 * @throws SciException
	 */
	protected String getUdf (
		int p_index
	) throws SciException {
		if (p_index >=0 && (p_index <= (TOTAL_NUMBER_OF_UDF-1))) {
			return this.configTplObjUdf[p_index];
		} else {
			throw new SciException("Index out of bound");
		}
	}
	
	/**
	 * This method is used to set the udf value of the index passed
	 * by the user.
	 * @param p_index	The index whose value needs to be set.
	 * @param p_value	Value of the udf.
	 * @throws SciException
	 */
	protected void setUdf (
		int p_index
	,	String	p_value
	) throws SciException {
		if (p_index >=0 && (p_index <= (TOTAL_NUMBER_OF_UDF-1))) {
			configTplObjUdf[p_index]	=	p_value;
		} else {
			throw new SciException("Index out of bound");
		}
	}
}
