package com.sciformix.sciportal.mail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.user.UserInfo;

public class MailTemplateHome
{
	
	private static final Logger log = LoggerFactory.getLogger(MailTemplateHome.class);
	
	public static MailTemplate saveEmailTemplate(MailTemplate m_oMailTemplate,UserInfo u_oUserInfo, boolean p_bInsert) throws SciServiceException
	{
		try
		{
			return MailTemplateHomeImpl.saveMailTemplate(m_oMailTemplate,u_oUserInfo,  p_bInsert);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while saving email template", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Email.ERROR_SAVING_EMAIL, sciExcep);
		}
	}
	
	public static List<MailTemplate> getMailTemplates(int p_nProjectId) throws SciServiceException
	{
		try
		{
			return MailTemplateHomeImpl.getMailTemplates(p_nProjectId);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving project list", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Email.ERROR_FETCHING_EMAIL_TEMPLATE_LIST, sciExcep);
		}
	}
	
	public static boolean changeMailTemplateStatus(int p_projectId, int p_sequenceid,RecordState p_state, UserInfo u_oUserInfo) throws SciException
	{
		MailTemplateHomeImpl.changeMailTemplateStatus(p_projectId, p_sequenceid, p_state, u_oUserInfo);
		return true;
	}
	
	public static MailTemplate getMailTempalateBySeqId(int m_sequenceid) throws SciException
	{
		return MailTemplateHomeImpl.getmailTemplateBySeqId(m_sequenceid);
		
	}
}
