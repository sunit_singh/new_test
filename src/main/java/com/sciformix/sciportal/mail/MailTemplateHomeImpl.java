package com.sciformix.sciportal.mail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.config.ConfigTplObj;
import com.sciformix.sciportal.config.ConfigTplObjDbSchema;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.content.BinaryContent;
import com.sciformix.sciportal.content.BinaryContentHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.user.UserInfo;

class MailTemplateHomeImpl
{
	
	private static final Logger log = LoggerFactory.getLogger(MailTemplateHomeImpl.class);
	
	public static List<MailTemplate> getMailTemplates(int p_nProjectId) throws SciException
	{
		List<MailTemplate> listTemplates = null;
		List<ConfigTplObj> listTemplateStubs = null;
		MailTemplate o_mailtemplate = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				listTemplateStubs = ConfigTplObjHomeImpl.getListOfTemplates(connection, p_nProjectId, ConfigTplObjType.EMAIL);
				listTemplates = new ArrayList<MailTemplate>(listTemplateStubs.size());
				for (ConfigTplObj oConfigObject : listTemplateStubs)
				{
					o_mailtemplate =(MailTemplate)oConfigObject;
					o_mailtemplate.setBodyTemplate(BinaryContentHomeImpl.readContent(connection, Integer.parseInt(o_mailtemplate.getBodyTemplate())).getContent());
					listTemplates.add(o_mailtemplate);
				}
			} catch (SciException sciExcep) {
				log.error("Error loading mail template List - {}",  sciExcep);
				throw sciExcep;
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading mail template List - {}",  dbExcep);
			throw new SciException("Error loading mail template List", dbExcep);
		}
		return listTemplates;
	}
	
	public static MailTemplate saveMailTemplate(MailTemplate m_oMailTemplate,UserInfo u_oUserInfo, boolean p_bInsert) throws SciException
	{
		BinaryContent l_bincontent = null;
		MailTemplate m_oMailTemp= new MailTemplate();
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			if (p_bInsert)
			{	
				m_oMailTemplate.setCreatedOn(new Date());
				m_oMailTemplate.setCreatedBy(u_oUserInfo.getUserSeqId());
				m_oMailTemplate.setLastUpdatedGrpOn(new Date());
				m_oMailTemplate.setLastUpdatedGrpBy(u_oUserInfo.getUserSeqId());
				m_oMailTemplate.setLastUpdatedOn(new Date());
				m_oMailTemplate.setLastUpdatedBy(u_oUserInfo.getUserSeqId());
				try {
					connection.startTransaction();
					l_bincontent	=	BinaryContent.createNew("mailtemplatebodycontent", "", "", m_oMailTemplate.getBodyTemplate());
					BinaryContentHomeImpl.saveContent(connection, u_oUserInfo, l_bincontent);
					m_oMailTemplate.setBodyTemplate(Integer.toString(l_bincontent.getContentSeqId()));
					m_oMailTemp=(MailTemplate)ConfigTplObjHomeImpl.saveConfigTplObj(connection,m_oMailTemplate,u_oUserInfo);
					connection.commitTransaction();
				} catch (SciException sciExcep) {
					connection.rollbackTransaction();
					log.error("Error saving email Template - {}", sciExcep);
					throw new SciException("Error saving email template", sciExcep);
				}
			} 
			else
			{	
				try {
					connection.startTransaction();
					l_bincontent	=	BinaryContent.createNew("mailtemplatebodycontent", "", "", m_oMailTemplate.getBodyTemplate());
					BinaryContentHomeImpl.saveContent(connection, u_oUserInfo, l_bincontent);
					m_oMailTemplate.setBodyTemplate(Integer.toString(l_bincontent.getContentSeqId()));
					ConfigTplObjHomeImpl.updateConfigTplObjAttributes(connection, m_oMailTemplate, u_oUserInfo);
					connection.commitTransaction();
				} catch (SciException sciExcep) {
					connection.rollbackTransaction();
					log.error("Error saving email Template - {}", sciExcep);
					throw new SciException("Error saving email template", sciExcep);
				}
			}
		} 
		catch(SciDbException dbExcep)
		{
			log.error("Error saving email Template - {}", dbExcep);
			throw new SciException("Error saving email template", dbExcep);
		}
		return m_oMailTemp;
	}
	
	public static boolean changeMailTemplateStatus(int p_projectId, int p_sequenceid, RecordState p_state, UserInfo u_oUserInfo ) throws SciException
	{
		MailTemplate m_MailTemplateBySeq = null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			if(p_state.equals(RecordState.ACTIVE))
			{
				m_MailTemplateBySeq = getmailTemplateBySeqId( p_sequenceid);
				for (MailTemplate m_oMailTemaplate: getMailTemplates(p_projectId))
				{
					if(m_oMailTemaplate.getState().name().equals(RecordState.ACTIVE.name())&&m_MailTemplateBySeq.getPurpose().equalsIgnoreCase(m_oMailTemaplate.getPurpose()))
					{
						ConfigTplObjHomeImpl.updateConfigTplObjState(connection, m_oMailTemaplate.getSequenceId(), RecordState.DEACTIVATED, u_oUserInfo);
					}
				}
			}
			
			
		ConfigTplObjHomeImpl.updateConfigTplObjState(connection, p_sequenceid, p_state, u_oUserInfo);
		return true;
		}
		catch(SciDbException dbExcep)
		{
			dbExcep.printStackTrace();
		return false;
		}
	}
	
	public static MailTemplate getmailTemplateBySeqId(int m_sequenceid ) throws SciException
	{
		MailTemplate mailtemplate = new MailTemplate();
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				mailtemplate=(MailTemplate)ConfigTplObjHomeImpl.getTemplateDetails(connection, m_sequenceid);
				
				mailtemplate.setBodyTemplate(BinaryContentHomeImpl.readContent(connection, Integer.parseInt(mailtemplate.getBodyTemplate())).getContent());
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				throw sciExcep;
			}
			
		} catch(SciDbException dbExcep)
		{
			dbExcep.printStackTrace();
			throw new SciException("Error getting email template", dbExcep);
		}
		
		return mailtemplate;
	}
	
	public static MailTemplate getActiveMailTemplatesByProjId(int p_nProjectId , String p_sPurpose) throws SciException
	{
		MailTemplate mailtemplate = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigTplObjDbSchema.CONFIGOBJECT,DbQueryUtils.bindWhereClauseWithAnd(ConfigTplObjDbSchema.CONFIGOBJECT.PRJSEQID,
				ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF01,ConfigTplObjDbSchema.CONFIGOBJECT.CTOTYPE,ConfigTplObjDbSchema.CONFIGOBJECT.CTOSTATE),	ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.name());
		oSelectQuery.addWhereClauseParameter(p_nProjectId);
		oSelectQuery.addWhereClauseParameter(p_sPurpose);
		oSelectQuery.addWhereClauseParameter(SciEnums.ConfigTplObjType.EMAIL.getCode());
		oSelectQuery.addWhereClauseParameter(SciEnums.RecordState.ACTIVE.value());
		

		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					if(resultSet.next())
					{
						mailtemplate	=	new MailTemplate();
						mailtemplate.setSequenceId(resultSet.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID));
						mailtemplate.setTemplateName(resultSet.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME));
						mailtemplate.setPurpose(resultSet.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF01));
						mailtemplate.setSequenceId(resultSet.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID));
						mailtemplate.setSubjectTemplate(resultSet.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF02));
						mailtemplate.setBodyTemplate(BinaryContentHomeImpl.readContent(connection, Integer.parseInt(resultSet.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTOUDF03))).getContent());
						//mailtemplate.(resultSet.read(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID));
					}
				}
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching mail template- {}", p_nProjectId , sciExcep);
				throw new SciException("Error fetching mail template", sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching mail template - {}",p_nProjectId, dbExcep);
			throw new SciException("Error fetching mail template", dbExcep);
		}
		return mailtemplate;
	}
	
	public static MailTemplate getSMTPServer(int p_nProjectId , String p_sPurpose) throws SciException
	{
		return null;
	}
	
}
