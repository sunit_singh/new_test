package com.sciformix.sciportal.mail;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.apps.ngv.FreemarkerUtils;
import com.sciformix.sciportal.utils.SMTPUtils;
import com.sciformix.sciportal.utils.SystemUtils;

public class MailRuntimeHelper
{
	

	private static final Logger log = LoggerFactory.getLogger(MailRuntimeHelper.class);
	
	private MailRuntimeHelper()
	{
		//Nothing to do
	}

	public static boolean relayEmail(int p_nProjectId, String p_sPurpose, 
			MailRuntimeContents p_oMailRuntimeContents) throws SciServiceException
	{
		String sBodyText = null;
		String sSubjectText = null;
		List<Object> listReturnValue = null;
		
		//Step 1: Retrieve 'MailTemplate' for the given Project and Purpose
		//	assuming there is EXACTLY ONE Active MailTemplate for each Project and Purpose
	
		MailTemplate m_oMailTemplate=MailRuntimeHelper.getActiveMailTemplateByProjectId(p_nProjectId, p_sPurpose);
		MimeMessage message = null; 
		
		//Step 2: Replace the name-value pair data into the Subject and Body templates using
		//	runtime values provided
		try
		{
			if (p_oMailRuntimeContents.getRuntimeValues().containsKey("subjectKey"))
			{
				sSubjectText = (String)p_oMailRuntimeContents.getRuntimeValues().get("subjectKey");
			}
			else
			{
				listReturnValue = FreemarkerUtils.replaceFreemarkerTemplate(m_oMailTemplate.getSubjectTemplate(), (Map<String,Object>)p_oMailRuntimeContents.getRuntimeValues());
				if (listReturnValue != null && listReturnValue.size() > 0)
				{
					sSubjectText = (String)listReturnValue.get(0);
				}
				else
				{
					sBodyText = StringConstants.EMPTY;
				}
			}
			
			if (p_oMailRuntimeContents.getRuntimeValues().containsKey("bodyKey"))
			{
				sBodyText = (String)p_oMailRuntimeContents.getRuntimeValues().get("bodyKey");
			}
			else
			{
				listReturnValue = FreemarkerUtils.replaceFreemarkerTemplate(m_oMailTemplate.getBodyTemplate(), (Map<String,Object>)p_oMailRuntimeContents.getRuntimeValues());
				if (listReturnValue != null && listReturnValue.size() > 0)
				{
					sBodyText = (String)listReturnValue.get(0);
				}
				else
				{
					sBodyText = StringConstants.EMPTY;
				}
			}
		Properties props = new Properties();    
		props.put("mail.smtp.host", SMTPUtils.getSMTPHost());  
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		props.put("mail.smtp.auth", true);    
		props.put("mail.smtp.port", SMTPUtils.getSMTPPort()); 
		
		String mailId= SMTPUtils.getSMTPBindingMail();
		String password= SMTPUtils.decrypt(SMTPUtils.getSMTPBindingPassworrd());
			Session session	=	Session.getDefaultInstance(
									props,    
				 new javax.mail.Authenticator() {    
				 protected PasswordAuthentication getPasswordAuthentication() {    
				return new PasswordAuthentication(mailId,password);  
				 }    
									}
								);    
			 
			message	=	new MimeMessage(session); 
			 message.setFrom(new InternetAddress(SMTPUtils.getSMTPSenderMail()));
				 for(String mailToRecipt:p_oMailRuntimeContents.getToAddresses())
				 {
					 message.addRecipient(Message.RecipientType.TO,new InternetAddress(mailToRecipt));    
				 }
				 for(String mailCCRecipt:p_oMailRuntimeContents.getCcAddresses())
				 {
					 message.addRecipient(Message.RecipientType.CC,new InternetAddress(mailCCRecipt));    
				 }
			 message.setSubject(sSubjectText);
			 message.setContent(sBodyText, "text/html");
			} 
			catch (MessagingException exMail) 
			{
			log.error("Error Creating Email",exMail);
				throw new RuntimeException();
			} 
			catch (SciException exSci) 
			{
			log.error("Error Creating Email",exSci);
			}  
		int nPendingRepeatCount =	SystemUtils.getEmailRetryCount();
		int nThreadSleepTime	=	SystemUtils.getEmailThreadSleepTime();
		log.error("Email Retry Count is:"+nPendingRepeatCount);
		log.error("Email Thread Sleep Time is:"+nThreadSleepTime);
		do
		{
			try
			{
				nPendingRepeatCount--;
				log.error("FYI: Initiating Sending/Relaying Email #" + nPendingRepeatCount);
				Transport.send(message);
				nPendingRepeatCount = 0;
				log.error("FYI: Email sent successfully");
			} 
			catch (MessagingException exMail) 
			{
				if (nPendingRepeatCount > 0)
				{
					log.error("Error Sending/Relaying Email - attempt #" + nPendingRepeatCount, exMail);
					try {
						if (nThreadSleepTime > 0) {
							Thread.sleep(nThreadSleepTime);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				else
				{
					log.error("Error Sending/Relaying Email for {" + sSubjectText + "}", exMail);
					throw new RuntimeException();
				}
			} 
		} while(nPendingRepeatCount > 0);
		return false;
	}
	
	public static MailTemplate getActiveMailTemplateByProjectId(int p_oProjjSeqId, String p_sPurpose) throws SciServiceException
	{
		MailTemplate	l_mailtemplate	=	null;
		try{
			l_mailtemplate	=	MailTemplateHomeImpl.getActiveMailTemplatesByProjId(p_oProjjSeqId, p_sPurpose);
			if (l_mailtemplate == null) {
				log.error("No active email template found for project:" + p_oProjjSeqId + " and purpose:"+p_sPurpose);
				throw new SciServiceException(ServiceErrorCodes.Email.ERROR_FETCHING_ACTIVE_MAIL_TEMPLATE);
			}
		}
		catch(SciException sciExcep)
		{
			log.error("Error retriving active mail template for project: " + p_oProjjSeqId);
			throw new SciServiceException(ServiceErrorCodes.Email.ERROR_FETCHING_ACTIVE_MAIL_TEMPLATE, sciExcep);
		}
		return l_mailtemplate;
	}
	
}