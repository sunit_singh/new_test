package com.sciformix.sciportal.mail;

import java.util.List;
import java.util.Map;

public class MailRuntimeContents
{
	private List<String> toAddresses;
	private List<String> ccAddresses;
	private Map<String, Object> runtimeValues = null;
	
	public List<String> getToAddresses() {
		return toAddresses;
	}
	public void setToAddresses(List<String> toAddresses) {
		this.toAddresses = toAddresses;
	}
	public List<String> getCcAddresses() {
		return ccAddresses;
	}
	public void setCcAddresses(List<String> ccAddresses) {
		this.ccAddresses = ccAddresses;
	}
	public Map<String, Object> getRuntimeValues() {
		return runtimeValues;
	}
	public void setRuntimeValues(Map<String, Object> runtimeValues) {
		this.runtimeValues = runtimeValues;
	}

}
