package com.sciformix.sciportal.mail;

public class MailAuditHelper {
	
	public static class MailAttributes
	{
		public static final String MAIL_SEQUENCE_ID = "Mail Template Sequence Id"; 
		public static final String MAIL_TEMPLATE_NAME = "Mail Template Name";
		public static final String OLD_MAIL_SUBJECT = "Old Mail Subject";
		public static final String MAIL_SUBJECT = "Mail Subject";
		public static final String OLD_MAIL_BODY = "Old Mail Template Body";
		public static final String MAIL_BODY = "Mail Template Body";
		public static final String OLD_MAIL_STATUS = "Old Mail Template Status";
		public static final String MAIL_STATUS = "Mail Template Status";
		
	}
	
	private MailAuditHelper()
	{
		//Nothing to do
	}

}
