package com.sciformix.sciportal.mail;

import java.util.Date;

import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.config.ConfigTplObj;

public class MailTemplate extends ConfigTplObj
{
	private static final int UDFINDEX_PURPOSE = 0;
	private static final int UDFINDEX_SUBJECT_BINCONTENTREF = 1;
	private static final int UDFINDEX_BODY_BINCONTENTREF = 2;
	
	public MailTemplate()
	{
		super(ConfigTplObjType.EMAIL);
	}
			
	public MailTemplate(int p_projectId,  String p_templateName,
			RecordState p_state, CreationMode p_creationMode, Date p_createdOn, int p_createdBy)
	{
		super(p_projectId, ConfigTplObjType.EMAIL, p_templateName, p_state, p_creationMode);
	}
	
	public String getPurpose() throws SciException
	{
		return super.getUdf(UDFINDEX_PURPOSE);
	}
	
	public String getSubjectTemplate() throws SciException
	{
		return super.getUdf(UDFINDEX_SUBJECT_BINCONTENTREF);
	}
	
	public String getBodyTemplate() throws SciException
	{
		return super.getUdf(UDFINDEX_BODY_BINCONTENTREF);
	}
	
	public void setPurpose(String p_sPurpose) throws SciException
	{
		super.setUdf(UDFINDEX_PURPOSE, p_sPurpose);
	}
	
	public void setSubjectTemplate(String p_nBinContentRef) throws SciException
	{
		super.setUdf(UDFINDEX_SUBJECT_BINCONTENTREF, p_nBinContentRef);
	}
	
	public void setBodyTemplate(String p_nBinContentRef) throws SciException
	{
		super.setUdf(UDFINDEX_BODY_BINCONTENTREF, p_nBinContentRef);
	}
	
}
