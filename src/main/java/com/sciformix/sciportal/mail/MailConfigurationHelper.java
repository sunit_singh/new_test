package com.sciformix.sciportal.mail;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.sciportal.user.UserInfo;

public class MailConfigurationHelper 
{
	
	private static final Logger log = LoggerFactory.getLogger(MailConfigurationHelper.class);

	
	private MailConfigurationHelper()
	{
		//Nothing to do
	}
	
	public static List<MailTemplate> getAllEmailTemplates(int p_nProjectId)
	{
		try {
			return MailTemplateHome.getMailTemplates(p_nProjectId);
		}
		catch(SciServiceException sciExcep)
		{
			log.error("Error retriving list of Email Templates", sciExcep);
			return null;
		}
	}
	
	public static MailTemplate saveMailTemplate(int p_nProjectId, MailTemplate m_oMailTemplate,UserInfo u_oUserInfo, boolean p_bInsert) 
	{
		MailTemplate mailTemp= null;
		try{
		return MailTemplateHome.saveEmailTemplate(m_oMailTemplate, u_oUserInfo, p_bInsert);
		}
		catch(SciServiceException scEx)
		{
			log.error("Error saving email template", scEx);
			return mailTemp;
		}
	}
	
	public static MailTemplate updateMailTemplate(int p_nProjectId, String m_Subject, String m_Body,
			 UserInfo u_oUserInfo, int m_SeqId ) 
	{
		//MailTemplate mailTemp= null;
		List<MailTemplate> mailTemplateList= getAllEmailTemplates(p_nProjectId);
		MailTemplate m_oMailTemplate = null;
		
		for(MailTemplate mailTemplate: mailTemplateList)
		{	
			if(mailTemplate.getSequenceId()==m_SeqId)
			{
				m_oMailTemplate= mailTemplate;
			}
		}
		try{
			
			m_oMailTemplate.setSubjectTemplate(m_Subject);
			m_oMailTemplate.setBodyTemplate(m_Body);
			MailTemplateHome.saveEmailTemplate(m_oMailTemplate, u_oUserInfo, false);
		 return m_oMailTemplate;
		}
		catch(SciException sciEx)
		{
			log.error("Error updating mail template", sciEx);
			return null;
		}
		catch(SciServiceException sciEx)
		{
			log.error("Error updating mail template", sciEx);
			return null;
		}
	}
	
	public static boolean changeMailTemplateStatus(int p_ProjectId,int p_sequenceid,RecordState p_state, UserInfo u_oUserInfo) 
	{
	 try {
			MailTemplateHome.changeMailTemplateStatus(p_ProjectId, p_sequenceid, p_state, u_oUserInfo);
			return true;
	 	}
	 catch(SciException scExp)
	 {
		 log.error("Error updating mail status", scExp);
			return false;
	 }
	}
	
	public static MailTemplate getMailTemplateBySeqId(int m_sequenceid) 
	{
	 try {
			return MailTemplateHome.getMailTempalateBySeqId(m_sequenceid);
	 	}
	 catch(SciException scExp)
	 {
		 log.error("Error updating mail status", scExp);
		return null;	
	 }
	}
	
	
}
