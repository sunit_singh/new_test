package com.sciformix.sciportal.project;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.config.ConfigDbSchema;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUpdate;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.user.UserInfo;

public class ProjectHomeImpl
{
	
	private static final Logger log = LoggerFactory.getLogger(ProjectHomeImpl.class);
	
	/**
	 * This method is used for getting project Info
	 * @param p_nProjectId
	 * @return ProjectInfo
	 * @throws SciException
	 */	
	public static ProjectInfo getProjectGivenProjectId(int p_nProjectSeqId) throws SciException
	{
		ProjectInfo project = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectDbSchema.PROJECTS,ProjectDbSchema.PROJECTS.PRJSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_nProjectSeqId);
		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						project = readProjectInfoObjFromDb(resultSet);
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching Project - {}", p_nProjectSeqId, dbExcep);
				throw new SciException("Error fetching project", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching Project - {}",p_nProjectSeqId, dbExcep);
			new SciException("Error fetching project", dbExcep);
		}
	
		return project;
	}
	
	/**
	 * This method is used for getting Project Info by Passing Project Name
	 * @param p_sProjectName
	 * @return ProjectInfo
	 * @throws SciException
	 */	
	public static ProjectInfo getProjectGivenProjectName(String p_sProjectName) throws SciException
	{
		ProjectInfo project = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectDbSchema.PROJECTS,ProjectDbSchema.PROJECTS.PRJNAME.bindClause());
		oSelectQuery.addWhereClauseParameter(p_sProjectName);
		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						project = readProjectInfoObjFromDb(resultSet);
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching Project - {}", p_sProjectName, dbExcep);
				throw new SciException("Error fetching project", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching Project - {}",p_sProjectName, dbExcep);
			new SciException("Error fetching project", dbExcep);
		}
	
		return project;
	}
	
	
	/**
	 * This method is used for getting list of all projects
	 * @return List ProjectInfo
	 * @throws SciException
	 */	
	public static List<ProjectInfo> getAllProjects() throws SciException
	{
		ProjectInfo projInfo = null;
		List<ProjectInfo> allProjectsList = null;
		DbQuerySelect oSelectQuery = null;
		
		allProjectsList = new ArrayList<>();
		
		//TODO:Order by Project Name
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectDbSchema.PROJECTS, null, ProjectDbSchema.PROJECTS.PRJNAME.name());
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while (resultSet.next())
					{
						projInfo = readProjectInfoObjFromDb(resultSet);
						allProjectsList.add(projInfo);
					}				
				}
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error loading project List - {}",  dbExcep);
				throw new SciException("Error loading project list", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading project List - {}",  dbExcep);
			throw new SciException("Error loading project list", dbExcep);
		}
		
		return allProjectsList;
	}
	
	private static ProjectInfo readProjectInfoObjFromDb(DbResultSet resultSet) throws SciDbException
	{
		ProjectInfo projectInfo = null;
		ProjectInfoDb projectInfoDb = new ProjectInfoDb();
		
		projectInfoDb.setProjectSequenceId(resultSet.readInt(ProjectDbSchema.PROJECTS.PRJSEQID));
		projectInfoDb.setProjectName(resultSet.readString(ProjectDbSchema.PROJECTS.PRJNAME));
		projectInfoDb.setProjectDescription(resultSet.readString(ProjectDbSchema.PROJECTS.PRJDESC));
		projectInfoDb.setProjectState(resultSet.readInt(ProjectDbSchema.PROJECTS.PRJSTATE));
		
		projectInfo = new ProjectInfo(projectInfoDb);
		
		return projectInfo;
	}
	
	/**
	 * This method is used for saving project
	 * @param p_oProjectInfo
	 * @param p_oUserInfo
	 * @param p_bInsert
	 * @return boolean
	 * @throws SciException
	 */	
	public static boolean saveProject(ProjectInfo p_oProjectInfo, UserInfo p_oUserInfo, boolean p_bInsert) throws SciException
	{
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				if (p_bInsert)
				{	
					DbQueryInsert oInsertQuery = null;
					
					int nProjectSeqId = DbQueryHome.generateSequenceValueForTablePk(ProjectDbSchema.PROJECTS);
					
					oInsertQuery = DbQueryUtils.createInsertQuery(ProjectDbSchema.PROJECTS);
					oInsertQuery.addInsertParameter(nProjectSeqId);
					oInsertQuery.addInsertParameter(p_oProjectInfo.getProjectName());
					oInsertQuery.addInsertParameter(p_oProjectInfo.getProjectDescription());
					oInsertQuery.addInsertParameter(p_oProjectInfo.getProjectState().value());
					oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
					oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
					oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
					oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
					oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
					oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
					
					connection.executeQuery(oInsertQuery);
					
					p_oProjectInfo.setProjectSequenceId(nProjectSeqId);
				}
				else
				{
					
					DbQueryUpdate oUpdateQuery = null;
					
					oUpdateQuery = DbQueryUtils.createUpdateQuery(ProjectDbSchema.PROJECTS, 
							DbQueryUtils.bindUpdateClause(ProjectDbSchema.PROJECTS.PRJNAME,
										ProjectDbSchema.PROJECTS.PRJDESC, ProjectDbSchema.PROJECTS.PRJSTATE),
							DbQueryUtils.bindWhereClauseWithAnd(ProjectDbSchema.PROJECTS.PRJSEQID));
					
					oUpdateQuery.addUpdateParameter(p_oProjectInfo.getProjectName());
					oUpdateQuery.addUpdateParameter(p_oProjectInfo.getProjectDescription());
					oUpdateQuery.addUpdateParameter(p_oProjectInfo.getProjectState().value());
					
					oUpdateQuery.addWhereClauseParameter(p_oProjectInfo.getProjectSequenceId());
					
					connection.executeQuery(oUpdateQuery);
				}
				connection.commitTransaction();
			}
			catch(SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving project - {}", p_oProjectInfo.getProjectSequenceId(), dbExcep);
				throw new SciException("Error saving project", dbExcep);
			}
		}catch(SciDbException dbExcep)
		{
			log.error("Error saving project - {}", p_oProjectInfo.getProjectSequenceId(), dbExcep);
			throw new SciException("Error saving project", dbExcep);
		}
	
		return true;
	}
	
	/**
	 * This method is used for saving project config
	 * @param p_oProjectInfo
	 * @param p_oUserInfo
	 * @return boolean
	 * @throws SciException
	 */	
	public static boolean saveProjectConfig(ProjectInfo p_oProjectInfo, UserInfo p_oUserInfo) throws SciException
	{
		
		DbQueryInsert oInsertQuery = null;
		DbQuerySelect oSelectQuery = null;
		
		int p_nProjectSeqId = -1;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigDbSchema.PRJ_CONFIG, ConfigDbSchema.PRJ_CONFIG.PRJSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_nProjectSeqId);
		
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
					
				
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						oInsertQuery = DbQueryUtils.createInsertQuery(ConfigDbSchema.PRJ_CONFIG);
						oInsertQuery.addInsertParameter(p_oProjectInfo.getProjectSequenceId());
						oInsertQuery.addInsertParameter(resultSet.readString(2));
						oInsertQuery.addInsertParameter(resultSet.readString(3));
						oInsertQuery.addInsertParameter(resultSet.readString(4));
						oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
						oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
						connection.executeQuery(oInsertQuery);
					}
				}
				
				connection.commitTransaction();
			}
			catch(SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving projConfig - {}", p_oProjectInfo.getProjectSequenceId(), dbExcep);
				throw new SciException("Error saving projConfig", dbExcep);
			}
		}catch(SciDbException dbExcep)
		{
			log.error("Error saving projConfig - {}", p_oProjectInfo.getProjectSequenceId(), dbExcep);
			throw new SciException("Error saving projConfig", dbExcep);
		}
	
		return true;
	}
	
	/**
	 * This method is used for getting max project SeqId
	 * @return int Project Id
	 * @throws SciException
	 */	
	public static int getMaxProjectSeqId() throws SciException
	{
		int maxProjectSeqId = 0;
		DbQuerySelect oSelectQuery = null;
		oSelectQuery = DbQueryUtils.createSelectQuery(ProjectDbSchema.PROJECTS,"max(PRJSEQID)",null);
			
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						maxProjectSeqId = resultSet.readInt(1);
					}
				}
				connection.commitTransaction();
				
			}catch(SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error getting max project seq Id",  dbExcep);
				throw new SciException("Error getting max project seq Id ", dbExcep);
			}
		}catch(SciDbException dbExcep)
		{
			log.error("Error getting max project seq Id",  dbExcep);
			throw new SciException("Error getting max project seq Id ", dbExcep);
		}
		
		return maxProjectSeqId;
	}
			
			

}
