package com.sciformix.sciportal.project;

public class ProjectInfoDb
{
	
	private int m_nProjectSequenceId;
	private String m_sProjectName;
	private String m_sProjectDescription;
	private int m_nProjectState;
	
	public ProjectInfoDb()
	{
		//Nothing to do
	}
	
	public int getProjectSequenceId() {
		return m_nProjectSequenceId;
	}
	public void setProjectSequenceId(int p_nProjectSequenceId) {
		this.m_nProjectSequenceId = p_nProjectSequenceId;
	}
	public String getProjectName() {
		return m_sProjectName;
	}
	public void setProjectName(String p_sProjectName) {
		this.m_sProjectName = p_sProjectName;
	}
	public String getProjectDescription() {
		return m_sProjectDescription;
	}
	public void setProjectDescription(String p_sProjectDescription) {
		this.m_sProjectDescription = p_sProjectDescription;
	}
	public int getProjectState() {
		return m_nProjectState;
	}
	public void setProjectState(int p_nProjectState) {
		this.m_nProjectState = p_nProjectState;
	}
}
