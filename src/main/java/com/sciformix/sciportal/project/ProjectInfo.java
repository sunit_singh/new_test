package com.sciformix.sciportal.project;

public class ProjectInfo {
	
	public enum ProjectState
	{
		ACTIVE(1);
		private int value;
		
		ProjectState(int value)
		{
			this.value=value;
		}
		public int value()
		{
			return this.value;
		}
		
		public static ProjectState toEnum( int value)
		{
			ProjectState[] values = ProjectState.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (value == values[counter].value())
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("ProjectState: Illegal enum value - " + value);
		}
	}
	
	private int m_nProjectSequenceId;
	private String m_sProjectName;
	private String m_sProjectDescription;
	private ProjectState m_nProjectState;
	
	public ProjectInfo(ProjectInfoDb oProjectDb)
	{
		m_nProjectSequenceId = oProjectDb.getProjectSequenceId();
		m_sProjectName= oProjectDb.getProjectName();
		m_sProjectDescription = oProjectDb.getProjectDescription();
		m_nProjectState=ProjectState.toEnum(oProjectDb.getProjectState());
		
	}
	
	public int getProjectSequenceId() {
		return m_nProjectSequenceId;
	}
	public void setProjectSequenceId(int p_nProjectSequenceId) {
		this.m_nProjectSequenceId = p_nProjectSequenceId;
	}
	public String getProjectName() {
		return m_sProjectName;
	}
	public void setProjectName(String p_sProjectName) {
		this.m_sProjectName = p_sProjectName;
	}
	public String getProjectDescription() {
		return m_sProjectDescription;
	}
	public void setProjectDescription(String p_sProjectDescription) {
		this.m_sProjectDescription = p_sProjectDescription;
	}
	public ProjectState getProjectState() {
		return m_nProjectState;
	}
	public void setProjectState(ProjectState p_nProjectState) {
		this.m_nProjectState = p_nProjectState;
	}
	
}
