package com.sciformix.sciportal.project;

public class ProjectAuditHelper {
	
	public static class ProjectAttributes
	{
		public static final String PROJECT_SEQUENCE_ID = "Project Sequence Id"; 
		public static final String PROJECT_NAME = "Project Name";
		public static final String OLD_PROJECT_NAME = "Old Project Name";
		public static final String OLD_PROJECT_DESCRIPTION = "Old Project Description";
		public static final String PROJECT_DESCRIPTION = "Project Description";
	}
	
	private ProjectAuditHelper()
	{
		//Nothing to do
	}
}
