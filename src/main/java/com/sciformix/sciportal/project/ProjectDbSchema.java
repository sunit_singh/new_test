package com.sciformix.sciportal.project;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class ProjectDbSchema
{
	
	public static Projects PROJECTS= new Projects();
			
	public static class Projects extends DbTable
	{
		public DbColumn PRJSEQID= null;
		public DbColumn PRJNAME= null;
		public DbColumn PRJDESC= null;
		public DbColumn PRJSTATE= null;
		public DbColumn DTCREATED= null;
		public DbColumn USRCREATED= null;
		public DbColumn DTLASTUPDATEDREC= null;
		public DbColumn USRLASTUPDATEDREC= null;
		public DbColumn DTLASTUPDATEDGRP= null;
		public DbColumn USRLASTUPDATEDGRP= null;
		
		public Projects()
		{
			super("PRJ_PROJECTS");
			setPkSequenceName("PRJ_PROJECTS_PRJSEQID_SEQ");
			
			PRJSEQID = addColumn("PRJSEQID");
			PRJNAME= addColumn("PRJNAME");
			PRJDESC= addColumn("PRJDESC");
			PRJSTATE= addColumn("STATE");
			DTCREATED= addColumn("DTCREATED");
			USRCREATED= addColumn("USRCREATED");
			DTLASTUPDATEDREC= addColumn("DTLASTUPDATEDREC");
			USRLASTUPDATEDREC= addColumn("USRLASTUPDATEDREC");
			DTLASTUPDATEDGRP= addColumn("DTLASTUPDATEDGRP");
			USRLASTUPDATEDGRP= addColumn("USRLASTUPDATEDGRP");
			
		}
	}
}
