package com.sciformix.sciportal.project;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.user.UserInfo;

public class ProjectHome
{
	private static final Logger log = LoggerFactory.getLogger(ProjectHome.class);
	
	/**
	 * This method is used for getting project Info
	 * @param p_nProjectId
	 * @return ProjectInfo
	 * @throws SciServiceException
	 */	
	public static ProjectInfo getProject(int p_nProjectId) throws SciServiceException
	{
		ProjectInfo projectInfo = null;
		
		try
		{
			projectInfo = ProjectHomeImpl.getProjectGivenProjectId(p_nProjectId);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving project - {}", p_nProjectId, sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Project.ERROR_FETCHING_PROJECT_INFO, sciExcep);
		}
		
		return projectInfo;
	}
	
	/**
	 * This method is used for getting Project Info by Passing Project Name
	 * @param p_sProjectName
	 * @return ProjectInfo
	 * @throws SciServiceException
	 */	
	public static ProjectInfo getProjectGivenProjectName(String p_sProjectName) throws SciServiceException
	{
		ProjectInfo projectInfo = null;
		
		try
		{
			projectInfo = ProjectHomeImpl.getProjectGivenProjectName(p_sProjectName);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving project - {}", p_sProjectName, sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Project.ERROR_FETCHING_PROJECT_INFO, sciExcep);
		}
		
		return projectInfo;
	}
	
	/**
	 * This method is used for getting list of all projects
	 * @return List ProjectInfo
	 * @throws SciServiceException
	 */	
	public static List<ProjectInfo> getAllProjects() throws SciServiceException
	{
		try
		{
			return ProjectHomeImpl.getAllProjects();
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving project list", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Project.ERROR_FETCHING_PROJECT_INFO_LIST, sciExcep);
		}
	}
	
	/**
	 * This method is used for saving project
	 * @param p_oProjectInfo
	 * @param p_oUserInfo
	 * @param p_bInsert
	 * @return boolean
	 * @throws SciServiceException
	 */	
	public static boolean saveProject(ProjectInfo p_oProjectInfo, UserInfo p_oUserInfo, boolean p_bInsert) throws SciServiceException
	{
		try
		{
			return ProjectHomeImpl.saveProject(p_oProjectInfo,p_oUserInfo, p_bInsert);
			 
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while saving ProjectInfo to db for Project - {}", p_oProjectInfo.getProjectSequenceId(), sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Project.ERROR_SAVING_PROJECT, sciExcep);
		}
	}
	
	/**
	 * This method is used for saving project config
	 * @param p_oProjectInfo
	 * @param p_oUserInfo
	 * @return boolean
	 * @throws SciServiceException
	 */	
	public static boolean saveProjectConfig(ProjectInfo p_oProjectInfo, UserInfo p_oUserInfo) throws SciServiceException
	{
		try
		{
			return ProjectHomeImpl.saveProjectConfig(p_oProjectInfo,p_oUserInfo);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while saving ProjectConfigInfo to db for ProjectConfig - {}", p_oProjectInfo.getProjectSequenceId(), sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Project.ERROR_SAVING_PROJECT, sciExcep);
		}
	}
	
	/**
	 * This method is used for getting max project SeqId
	 * @return int Project Id
	 * @throws SciServiceException
	 */	
	public static int getMaxProjectSeqId() throws SciServiceException
	{
		try
		{
			return ProjectHomeImpl.getMaxProjectSeqId();
		}
		catch(SciException sciExcep)
		{
			log.error("Exception fetching max project id ", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Project.ERROR_FETCHING_PROJECT_INFO, sciExcep);
		}
		
	}
	
}
