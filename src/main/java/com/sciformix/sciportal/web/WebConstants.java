package com.sciformix.sciportal.web;

import com.sciformix.commons.SciConstants;

public class WebConstants
{
	public static final String RESPONSE_ERROR_LIST = "s_errorList";

	private WebConstants()
	{
		//Nothing to do
	}
	
	public static final String SESSION_PARAMETER_SESSION = "session";
	public static final String REQUEST_PARAMETER_USERNAME = "userName";
	public static final String REQUEST_PARAMETER_PASSWORD = "password";
	
	public static final String REQUEST_PARAMETER_LOGREQUEST = "logoutRequest";
	
	public static final String REQUEST_PARAMETER_CONTENT_PAGE_NAME = "content-page-name";
	public static final String REQUEST_PARAMETER_MENU = "menu";
	public static final String REQUEST_PARAMETER_PORTALAPPID = "portal_appid";
	public static final String REQUEST_PARAMETER_PORTALACTION = "portal_action";
	public static final String REQUEST_PARAMETER_MISSINGMARKER = "missing_Marker";
	public static final String TEMP_FLAG = "true";
	public static final int FILE_UPLOAD_SERVLET_DISK_FACTORY_MEMORY_THRESHOLD = SciConstants.SIZE_100KB;
	
}
