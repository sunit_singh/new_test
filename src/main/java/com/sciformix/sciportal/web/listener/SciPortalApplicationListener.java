package com.sciformix.sciportal.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.SciPortal;
import com.sciformix.sciportal.utils.BootstrapUtils;
import com.sciformix.sciportal.utils.SystemUtils;

public class SciPortalApplicationListener implements ServletContextListener
{

	private ServletContext sc = null;

	public void contextInitialized(ServletContextEvent arg0)
	{
	    this.sc = arg0.getServletContext();
	    boolean bInitSuccessful = false;
	    String sSciPortalDir	= null;
	    
	    sSciPortalDir	=	sc.getInitParameter(SystemUtils.ENVIRONMENT_VARIABLE_INSTALL_PATH);
	    if (StringUtils.isNullOrEmpty(sSciPortalDir)) {
	    	System.out.println("SciPortal Application not Initialized as SciPortal install folder is not maintained in app.xml file");
	    	return;   
	    } else {
	    	System.setProperty(SystemUtils.ENVIRONMENT_VARIABLE_INSTALL_PATH, sSciPortalDir);
	    	SystemUtils.setInstallPath(sSciPortalDir);
	    }
	    
	    //Check if system is bootstrapped; if not, print to log and return
	    if(!BootstrapUtils.isInstanceBootstrapped())
	    {
	    	System.out.println("SciPortal Application not Initialized as system is not bootstrapped");
	    	return;    	
	    }
	    
	    System.out.println("SciPortal Application starting...");
	    
		// initialization code
		bInitSuccessful = SciPortal.init();
		if (!bInitSuccessful)
		{
			throw new IllegalStateException("Initialization of one or more of SciPortal modules failed - contact System Administrator");
		}
	    System.out.println("SciPortal Application start completed");
	}

	public void contextDestroyed(ServletContextEvent arg0)
	{
		// shutdown code
		this.sc = null;
	    System.out.println("SciPortal Application shutdown completed");
	}


}
