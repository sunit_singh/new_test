package com.sciformix.sciportal.web.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sciformix.commons.SciStartupLogger;
import com.sciformix.sciportal.utils.WebUtils;

/**
 * Servlet Filter implementation class TimeOutFilter
 */
@WebFilter("/SciPortalTimeOutFilter")
public class SciPortalTimeOutFilter implements Filter 
{
	/**
     * Default constructor. 
     */
	public SciPortalTimeOutFilter() 
    {
    }

	private ArrayList<String> listFilterAvoidUrls;
    
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest httpRequest=(HttpServletRequest) request;
		HttpServletResponse	l_httpresponse	=	(HttpServletResponse) response;
		
		l_httpresponse.addHeader("x-frame-options", "SAMEORIGIN");
		l_httpresponse.addHeader("X-XSS-Protection", "1; mode=block");
		l_httpresponse.addHeader("X-Content-Type-Options", "nosniff");
		
		String sRequestedUri = ((HttpServletRequest) request).getServletPath();
		
		if (listFilterAvoidUrls.contains(sRequestedUri))
		{
			chain.doFilter(request, l_httpresponse);
		}
		else
		{
			HttpSession session = httpRequest.getSession(false);		
			if (session == null)
			{
				//log.info("Expired Session detected");			
				request.getRequestDispatcher(WebUtils.getSessionExpiredPageUri()).forward(request, l_httpresponse);
				
				//DEV-NOTE: It is CRITICAL to prevent further execution of the request
				return;
			}
			else
			{
				chain.doFilter(request, l_httpresponse);
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig config) throws ServletException
	{
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		
		oStartupLogger.log("SciPortalTimeOutFilter - Initialization started");
		
		String sConfiguredAvoidUrls = config.getInitParameter("avoid-urls");
		oStartupLogger.trace("SciPortalTimeOutFilter - AvoidUrls:" + sConfiguredAvoidUrls);
		
		if(sConfiguredAvoidUrls!=null)
		{
			StringTokenizer token = new StringTokenizer(sConfiguredAvoidUrls, ",");
			listFilterAvoidUrls = new ArrayList<String>();

			while (token.hasMoreTokens())
			{
				listFilterAvoidUrls.add(token.nextToken());
			}
		}
		oStartupLogger.log("SciPortalTimeOutFilter - Initialization completed");
	}

}
