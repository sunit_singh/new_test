package com.sciformix.sciportal.web;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;

public class MultipartContentRequestWrapper
{
	private List<FileItem> listFileitemsMultipartContent = null;
	private Collection<Part> collectionOfFileParts = null;
	private boolean bFileItemsAvailable = false;
	
	public MultipartContentRequestWrapper(List<FileItem> p_listFileitemsMultipartContent, Collection<Part> p_collParts)
	{
		listFileitemsMultipartContent = p_listFileitemsMultipartContent;
		collectionOfFileParts = p_collParts;
		
		if(listFileitemsMultipartContent != null && !listFileitemsMultipartContent.isEmpty())
		{
			bFileItemsAvailable = true;
		}
		else
		{
			bFileItemsAvailable = false;
		}
	}
	
	public List<FileItem> getFileItemList()
	{
		return listFileitemsMultipartContent;
	}
	
	public Collection<Part> getParts()
	{
		return collectionOfFileParts;
	}
	
	public boolean  fileItemsAvailable()
	{
		return bFileItemsAvailable;
	}
}
