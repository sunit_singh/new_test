package com.sciformix.sciportal.web.apps.ngv;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.ngv.CaseDataValidationRule;
import com.sciformix.sciportal.apps.ngv.CaseDataValidationRuleset;
import com.sciformix.sciportal.apps.ngv.NgvPortalApp;
import com.sciformix.sciportal.apps.ngv.NgvTemplate;
import com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;

class NgvPortalWebAppUtils
{
	private static final Logger log = LoggerFactory.getLogger(NgvPortalWebAppUtils.class);
	private static final String QR_REQUEST_PARAMETER_PROJECT_ID			=	"fldprojectid";

	public static String prepareForDisplayOfSingleTemplateRuleset(HttpServletRequest request,
			HttpServletResponse response, MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId,
			UserSession p_oUserSession, List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp,
			NgvTemplateRuleset oTemplateRuleset) {
		List<NgvTemplate> listTemplates = null;
		
		if (oTemplateRuleset != null)
		{
			listTemplates = oTemplateRuleset.getTemplateList();
			oTemplateRuleset.setTemplateList(listTemplates);
			WebUtils.setRequestAttribute(request, "ngv_listTemplates", listTemplates);
		}
		WebUtils.setSessionAttribute(request, "ngv_template_ruleset", oTemplateRuleset);
		
		return "ngv_tplrs_viewedit";
	}
	
	//Added by Rohini
	public static String prepareForDisplayOfSingleValidationRuleset(HttpServletRequest request,
			HttpServletResponse response, MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId,
			UserSession p_oUserSession, List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp,
			CaseDataValidationRuleset oValidationRuleset) {
		List<CaseDataValidationRule> listRules = null;

		if (oValidationRuleset != null) {
			listRules = oValidationRuleset.getValidationRules();
			oValidationRuleset.setValidationRules(listRules);
			WebUtils.setRequestAttribute(request, "ngv_list_of_Rules", listRules);
		}
		WebUtils.setSessionAttribute(request, "ngv_validate_ruleset", oValidationRuleset);

		return "ngv_valrs_viewedit";
	}
	//Ended by Rohini
	public static String prepareForDisplayOfRulesets(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, NgvPortalApp oMyPortalApp, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) throws SciServiceException
	{
		List<NgvTemplateRuleset> listRulesets = null;
		
		try
		{
			listRulesets = oMyPortalApp.getTemplateRulesets(null);
			
			WebUtils.setRequestAttribute(request, "ngv_listRulesets", listRulesets);
		}
		catch(SciServiceException sciExcep)
		{
			log.error("Error fetching list of rulesets", sciExcep);
			throw sciExcep;
		}
		
		return null;
	}
	//Added by Rohini
	public static String prepareForDisplayOfValidationRulesets(HttpServletRequest request, HttpServletResponse response,
			MultipartContentRequestWrapper oMultipartContentRequest, NgvPortalApp oMyPortalApp, String p_sActionId,
			UserSession p_oUserSession, List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp)
			throws SciServiceException {
		List<CaseDataValidationRuleset> listRulesets = null;

		try {
//			int l_projectid			=	WebUtils.getRequestIntParameter(request	, 	QR_REQUEST_PARAMETER_PROJECT_ID	, 	oMultipartContentRequest);
			int l_projectid = 0;
			listRulesets = oMyPortalApp.getCaseDataValidationRulesets(l_projectid);

			WebUtils.setRequestAttribute(request, "ngv_listRulesets", listRulesets);
		} catch (SciServiceException sciExcep) {
			log.error("Error fetching list of rulesets", sciExcep);
			throw sciExcep;
		}

		return null;
	}
	//Ended by Rohini
}
