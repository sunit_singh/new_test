package com.sciformix.sciportal.web.apps.qr;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.DataValidationUtils;
import com.sciformix.commons.utils.OfficeUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.commons.utils.WorkbookWorkSheetWrapper;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.qr.QRConstant;
import com.sciformix.sciportal.apps.qr.QRDailySummaryReport;
import com.sciformix.sciportal.apps.qr.QRErrorDashboardData;
import com.sciformix.sciportal.apps.qr.QRPortalApp;
import com.sciformix.sciportal.apps.qr.QRReviewData;
import com.sciformix.sciportal.apps.qr.QRTemplate;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.config.ConfigTplObj;
import com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHome;
import com.sciformix.sciportal.pvcp.PvcpCaseMaster;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFields;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate;
import com.sciformix.sciportal.safetydb.SafetyDbConfigurationHelper;
import com.sciformix.sciportal.transform.FileDataMapperUtil;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class 
	QRPortalWebApp
extends
	BasePortalWebApp{
	
	private static final String	
		QR_REQUEST_PARAMETER_TEMPLATE_NAME		=	"fldtemplatename"
	;
	private static final String
		QR_REQUEST_PARAMETER_TEMPLATE_SEQID		=	"fldtemplateseqid"
	;
	private static final String
		QR_REQUEST_PARAMETER_TEMPLATE_NEWSTATE	=	"fldtemplatenewstate"
	;
	private static final String
		QR_REQUEST_PARAMETER_MODE				=	"fldmode"
	;	
	private static final String
		QR_REQUEST_RVWALLOWEDTOCORRECT			=	"fldcorrectionallowed"
	;
	private static final String 
		QR_REQUEST_CASEQUALITYTHRESHOLD 		=	"fldoverallcriteria"
	;
	private static final String 
		QR_REQUEST_CAT1QUALITYTHRESHOLD 		=	"fldcat1criteria"
	;
	private static final String 
		QR_REQUEST_CAT2QUALITYTHRESHOLD 		=	"fldcat2criteria"
	;
	private static final String 
		QR_REQUEST_CAT3QUALITYTHRESHOLD 		=	"fldcat3criteria"
	;
	private static final String 	
		QR_REQUEST_CASECLASSIFICATIONOVERALL 	=	"fldoverallclassification"
	;
	private static final String 
		QR_REQUEST_CASECLASSIFICATIONCAT1 		=	"fldcat1classification"
	;
	private static final String 
		QR_REQUEST_CASECLASSIFICATIONCAT2 		=	"fldcat2classification"
	;
	private static final String 
		QR_REQUEST_CASECLASSIFICATIONCAT3 		=	"fldcat3classification"
	;
	private static final String
		QR_REQUEST_CASEFIELDMAPPING				=	"fldcasefieldmapping"
	;	
	private static final String
		QR_REQUEST_CASEID						=	"fldcaseid"
	;	
	private static final String
		QR_REQUEST_CASEID_SEARCH				=	"fldsearchedcaseid"
	;	
	private static final String
		QR_REQUEST_SEARCH_TYPE					=	"fldsearchtype"
	;	
	private static final String
		QR_REQUEST_STARTDATE_COUNT				=	"fldstartdatecount"
	;
	private static final String
		QR_GRAPH_SERIOUS_CASE_THRESHOLD			=	"seriouscasethreshold"
	;
	private static final String
		QR_GRAPH_NON_SERIOUS_CASE_THRESHOLD		=	"nonseriouscasethreshold"
	;
	private static final String
		QR_REQUEST_ENDDATE_COUNT				=	"fldenddatecount"
	;
	private static final String
		QR_REQUEST_CASEVERSION					=	"fldcaseversion"
	;	
	private static final String
		QR_REQUEST_REVIEWTYPE					=	"fldreviewtype"
	;	
	private static final String
		QR_REQUEST_FLDFILETYPE					=	"fldfiletype"
	;
	private static final String
		QR_REQUEST_FLDFILEFORMAT				=	"fldfileformat"
	;
	private static final String 
		QR_REVIEW_DWNLD_TMP_FILE 				=	"fldcasereviewdwnldtmp"
	;
	private static final String
		QR_HISTORY_CASE_VERSION					=	"fldhistroycaseversion"
	;	
	private static final String
		DSR_CSV_HEADER_1						=	",Serious,,,,Non-Serious,,,"
	;	
	private static final String
		DSR_CSV_HEADER_2						=	"Date,Total Reviewed,Passed,Failed,Quality(%),Total Reviewed,Passed,Failed,Quality(%)"
	;		
	private static final Logger 
		log	=	LoggerFactory.getLogger(QRPortalWebApp.class)
	;
	public QRPortalWebApp() {
		super("QR-PORTAL-APP", "ilqrlanding.jsp", ProjectApplicability.SINGLE_PROJECT);
	}
	
	@Override
	protected PortalAppResponse processAction(
		HttpServletRequest 				p_request
	, 	HttpServletResponse 			p_response
	,	MultipartContentRequestWrapper	p_multipartcontentrequest
	, 	String 							p_actionid
	, 	UserSession 					p_usersession
	, 	List<UserPreference> 			p_listappspecificuserpreferences
	, 	IPortalApp 						p_portalapp
	) {
		String 										l_layouttype				=	null,
													l_reporttype				=	null,
													l_caseid					=	null,
													l_caseversion				=	null,
													l_mode						=	null,
													l_reviewtype				=	null,
													l_internalid				=	null,
													l_casereviewsumm			=	null,
													l_internalcaseid			=	null,
													l_searchedcaseid			=	null,
													l_regclass					=	null,
													l_inputdateformat			=	null,
													l_outputdateformat			=	null,
													l_filetype					=	null,
													l_fileformat				=	null,
													l_temp						=	null;
		QRPortalApp									l_qrtportalapp				=	(QRPortalApp)	p_portalapp;
		QRReviewData 								l_reviewdata				= 	null;
		PortalAppResponse							l_appresponse				=	null,
													l_appresponseerror			=	null;
		Map<String, ArrayList<SafetyDbCaseFields>>	l_mapqualityreviewtabdata	=	null;
		int											l_projectid					=	-1,
													l_startdate					=	0,
													l_enddate					=	0,
													l_searchtype				=	1;	//Setting the value to make default search on date 
		List<UserInfo>								l_listuser					=	null;
		SafetyDbCaseFieldsTemplate					l_safetydbcasefieldtemplate	=	null;
		boolean										l_iserror					=	false,
													l_isreviewdatepassed		=	true,
													l_iserrordatapresent		=	true;
		File										l_exportfile				=	null;
		FileWriter									l_fwr						=	null;
		PrintWriter									l_pwr						=	null;
		FileInputStream								l_fis						=	null;
		List<QRDailySummaryReport>					l_dailysummaryreportlist	=	null;
		List<QRReviewData>							l_qrreviewdatalist			=	null;
		StringBuilder								l_fullcontent				=	null;
		QRTemplate									l_qrtemplate				=	null;
		UploadedFileWrapper							l_uploadedfile				=	null;
		PvcpCaseMaster 								l_pvcpcasemaster 			=	null;
		DataTable 									l_origdatatable 			= 	null,
													l_transtable				=	null;
		WorkbookWorkSheetWrapper 					l_workbooksheet 			= 	null; 
		Map<String,String>							l_errortypes				=	null;
		QRErrorDashboardData[]						l_errordbdata				=	null;
		List<ObjectIdPair<String,String>>			l_reviewtypelist			=	null;
		List<Object> 								listTemp 					=	null;
		Map<String,String>							l_fileformats				=	null;
		Map<String,List<String>>					enableUdfList				=	null;

		l_appresponse			=	new PortalAppResponse(getDefaultPageUri());
		l_projectid				=	WebUtils.getAuthorizedProjectId(p_request);
		l_startdate				=	WebUtils.isRequestParameterAvailable(p_request, "fldstartdatecount", p_multipartcontentrequest) ? 
									WebUtils.getRequestIntParameter(p_request, "fldstartdatecount", p_multipartcontentrequest) : 0;
		l_enddate				=	WebUtils.isRequestParameterAvailable(p_request, "fldenddatecount", p_multipartcontentrequest) ? 
									WebUtils.getRequestIntParameter(p_request, "fldenddatecount", p_multipartcontentrequest) : 0;
		l_isreviewdatepassed	=	WebUtils.isRequestParameterAvailable(p_request, "fldreviewdate", p_multipartcontentrequest);
		
		enableUdfList 			=	 l_qrtportalapp.getEnableUdfList(l_projectid);
		WebUtils.setRequestAttribute(p_request, "enableUdfList",enableUdfList);
		if (l_isreviewdatepassed) {
			WebUtils.setRequestAttribute(
				p_request
			, 	"fldreviewdate"
			,	WebUtils.getRequestParameter(p_request, "fldreviewdate", p_multipartcontentrequest)
			);
		}
		if (p_actionid == null ) {
			l_appresponse	=	null;
			l_appresponse	=	new PortalAppResponse("ilqrlanding.jsp");
		} else if (p_actionid.equalsIgnoreCase("qualityreview")) {
			l_appresponse		=	null;
			l_appresponse		=	new PortalAppResponse("qualityreview.jsp");
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			if (!prepareQRSearchPage(p_request, l_qrtportalapp, l_projectid,l_appresponseerror,p_usersession)) {
				return l_appresponseerror;
			}
		} else if ("search".equals(p_actionid)) {
			l_appresponse		=	null;
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("qualityreview.jsp");
			l_reviewtype		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_REVIEWTYPE, p_multipartcontentrequest);
			l_caseid			=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEID, p_multipartcontentrequest).trim();
			l_caseversion		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEVERSION, p_multipartcontentrequest).trim();
			l_internalid 		= 	l_caseid + "_" + l_caseversion;
			
			return	prepareQRLPage(
				p_request
			, 	l_qrtportalapp
			, 	l_projectid
			, 	p_usersession
			, 	l_caseid
			, 	l_caseversion
			, 	l_reviewtype
			, 	l_appresponseerror
			, 	"create", false
			);
			
		} else if (p_actionid.equalsIgnoreCase("startnewmrround")) {
			l_appresponse		=	null;
			l_appresponseerror	=	null;
			List<QRReviewData> 							l_reviewdatalist			= 	null;
			l_appresponseerror	=	new PortalAppResponse("qrlhistorydata.jsp");
			l_reviewtype		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_REVIEWTYPE, p_multipartcontentrequest);
			l_caseid			=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEID, p_multipartcontentrequest).trim();
			l_caseversion		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEVERSION, p_multipartcontentrequest).trim();
			
			WebUtils.setRequestAttribute(
				p_request
			, 	"loggedinuser"
			, 	p_usersession.getUserInfo()
			);
			
			try {
				l_internalid		=	l_caseid + "_" + l_caseversion;
				l_pvcpcasemaster	=	l_qrtportalapp.getPVCPCaseMasterByInternalCaseId(l_internalid , l_projectid);
			} catch (SciServiceException serExcep) {
				log.error("Exception retrieving pcvp case details" , serExcep	);
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			
			if (l_pvcpcasemaster !=null) {
				
				try {
					l_reviewdatalist	=	l_qrtportalapp.getQRLReviewTypeRecordDetails(l_pvcpcasemaster, l_reviewtype);
				} catch(SciServiceException sciserexp) {
					log.error("Error fetching quality review data by case master");
					l_appresponseerror.logError(sciserexp);
					return l_appresponseerror;
				}
			}
			try {
				l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
				if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewtypelist"
					, 	l_reviewtypelist
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			
			try {
				l_qrtemplate	=	l_qrtportalapp.getActiveQRLTemplateDetails(l_projectid, l_reviewtype, p_usersession);
			} catch (SciServiceException serExcep) {
				log.error("Exception retrieving review type from db" , serExcep);
				l_appresponseerror.logError(serExcep);
				WebUtils.setRequestAttribute(p_request, "qrdatalist", l_reviewdatalist);
				return l_appresponseerror;
			}
			
			if (!StringUtils.isNullOrEmpty(l_caseid)) {
				l_caseid = l_caseid.trim();
			}
			
			if (!StringUtils.isNullOrEmpty(l_caseversion)) {
				l_caseversion = l_caseversion.trim();
			}
			l_internalid 		= 	l_caseid + "_" + l_caseversion;
			
			return	prepareQRLPage(
				p_request
			, 	l_qrtportalapp
			, 	l_projectid
			, 	p_usersession
			, 	l_caseid
			, 	l_caseversion
			, 	l_reviewtype
			, 	l_appresponseerror
			, 	"create"
			,	true
			);
		} else if (p_actionid.equalsIgnoreCase("editqrldetailsblank")) {
			
			QRReviewData	l_ilqrreviewdata	=	null;
			l_appresponse	=	null;
			l_appresponse	=	new PortalAppResponse("qreditcasedata.jsp");
			l_searchtype 	= 	WebUtils.getRequestIntParameter(p_request, QR_REQUEST_SEARCH_TYPE,p_multipartcontentrequest);
			try {
				l_layouttype	=	l_qrtportalapp.getUserLayoutType(
										p_usersession.getUserInfo(),
										l_projectid
									);
				WebUtils.setRequestAttribute(
					p_request
				, 	"UserLayoutType"
				, 	l_layouttype
				);
			} catch (SciServiceException serExcep) {
				log.error(
					"Exception retrieving review type from db" , serExcep
				);
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			try {
				l_errortypes	=	l_qrtportalapp.getErrorTypes(l_projectid);
				if (l_errortypes != null && l_errortypes.size()>0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"errortypes"
					, 	l_errortypes
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(
					WebErrorCodes.QR.ERROR_MISSING_ERROR_TYPE
				);
				return l_appresponse;
			}
			try {
				l_ilqrreviewdata	=	l_qrtportalapp.getReviewData (
											WebUtils.getRequestIntParameter(p_request, "fldsequenceid", p_multipartcontentrequest)
										);
				WebUtils.setRequestAttribute(
					p_request
				, 	"reviewdata"
				, 	l_ilqrreviewdata
				);
			} catch (SciServiceException serExcep) {
				log.error(
				"Error fetching review data of qr case "
				, 	serExcep
				);
				l_appresponse.logError(
				serExcep
				);
				return l_appresponse;
			}
			
				WebUtils.setRequestAttribute(p_request, "cdbrflag", l_ilqrreviewdata.getCdbrflag());
			
			try {
				l_pvcpcasemaster	=	l_qrtportalapp.getPVCPCaseMasterByInternalCaseId(
											l_ilqrreviewdata.getCaseId() + "_" + l_ilqrreviewdata.getCaseVersion()
										, 	l_projectid
										);
				if (l_pvcpcasemaster == null) {
					l_appresponse.logError(
						WebErrorCodes.QR.ERROR_CASE_MASTER_NOTEXIST
					);
					return l_appresponse;
				}
				WebUtils.setRequestAttribute(p_request, "pvcp_case_record", l_pvcpcasemaster);
			} catch (SciServiceException serExcep) {
				log.error(
					"Error fetching case master data"
				, 	serExcep
				);
				l_appresponse.logError(
					serExcep
				);
				return l_appresponse;
			}
			
			l_reporttype	=	WebUtils.getRequestParameter(
									p_request
								, 	"fldreporttype"
								,	p_multipartcontentrequest
								);
			WebUtils.setRequestAttribute(
				p_request
			, 	"fldreporttype"
			, 	l_reporttype
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_STARTDATE_COUNT
			, 	l_startdate
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_ENDDATE_COUNT
			, 	l_enddate
			);
			try
			{
				l_listuser	=	ProjectUserManagementHome.getProjectUserList(l_projectid);
				l_listuser.remove(UserHome.retrieveBootstrapUser());
				l_listuser.remove(p_usersession.getUserInfo());
				WebUtils.setRequestAttribute(
					p_request
				,	"list_of_users"
				, 	l_listuser
				);
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error retrieving list of users", serExcep);
				l_appresponse.logError(serExcep);
			}
			WebUtils.setRequestAttribute(
				p_request
			, 	"ismrnameshow"
			, 	l_qrtportalapp.isMRNameEnabledForProject(
					l_projectid
				)
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	"categoryaliases"
			, 	l_qrtportalapp.getApplicableCategories(
					l_projectid
				)
			);
			WebUtils.setRequestAttribute(p_request, QR_REQUEST_SEARCH_TYPE, l_searchtype);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_CASEID_SEARCH
			, 	WebUtils.getRequestParameter(
					p_request
				, 	QR_REQUEST_CASEID_SEARCH
				,	p_multipartcontentrequest
				)
			);
		} else if (p_actionid.equalsIgnoreCase("ilqr_step4")) {
			
			l_appresponse 		= 	null;
			l_appresponseerror	=	null;
			l_appresponse 		= 	new PortalAppResponse("ilqrconfirm.jsp");
			
			double	l_mf				=	100.00;
			int		l_caseassocid		=  -1,
					l_mrid				=  -1,
					l_scftseqid			=	0,
					l_fieldsreviewed	=	0,
					l_fieldswerror		=	0,
					l_fieldserror		=	0,
					l_sequenceid		=  -1,
					l_reviewversion		=	0,
					l_cat1count			=	0,
					l_cat2count			=	0,
					l_cat3count			=	0,
					l_cat1reviewcount	=	0,
					l_cat2reviewcount	=	0,
					l_cat3reviewcount	=	0,
					l_totalfields		=	0,
					l_reviewround		=	0,
					l_cmseqid			=	0,
					l_cdbrflag			=  -1;
			
			l_reviewtype		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_REVIEWTYPE, p_multipartcontentrequest);
			l_caseid			=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEID, p_multipartcontentrequest);
			l_caseversion		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEVERSION, p_multipartcontentrequest);
			l_caseassocid		=	WebUtils.getRequestIntParameter(p_request, "fldcaseassoc", p_multipartcontentrequest);
			l_scftseqid			=	WebUtils.getRequestIntParameter(p_request, "fldscftseqid", p_multipartcontentrequest);
			l_fieldsreviewed	=	WebUtils.getRequestIntParameter(p_request, "fldfieldsreviewed", p_multipartcontentrequest);
			l_fieldswerror		=	WebUtils.getRequestIntParameter(p_request, "fldfieldsWithoutError", p_multipartcontentrequest);
			l_fieldserror		=	WebUtils.getRequestIntParameter(p_request, "fldfieldsIncorrect", p_multipartcontentrequest);
			l_mode				=	WebUtils.getRequestParameter(p_request, QR_REQUEST_PARAMETER_MODE, p_multipartcontentrequest);
			l_sequenceid		=	WebUtils.getRequestIntParameter(p_request, "fldsequenceid", p_multipartcontentrequest);
			l_reviewversion		=	WebUtils.getRequestIntParameter(p_request, "fldreviewversion", p_multipartcontentrequest);
			l_cat1count			=	WebUtils.getRequestIntParameter(p_request, "fldcat1count", p_multipartcontentrequest);
			l_cat2count			=	WebUtils.getRequestIntParameter(p_request, "fldcat2count", p_multipartcontentrequest);
			l_cat3count			=	WebUtils.getRequestIntParameter(p_request, "fldcat3count", p_multipartcontentrequest);
			l_cat1reviewcount	=	WebUtils.getRequestIntParameter(p_request, "fldcat1reviewcount", p_multipartcontentrequest);
			l_cat2reviewcount	=	WebUtils.getRequestIntParameter(p_request, "fldcat2reviewcount", p_multipartcontentrequest);
			l_cat3reviewcount	=	WebUtils.getRequestIntParameter(p_request, "fldcat3reviewcount", p_multipartcontentrequest);
			l_mrid				=	WebUtils.getRequestIntParameter(p_request, "fldmedicalreviewer", p_multipartcontentrequest);
			l_reviewround		=	WebUtils.getRequestIntParameter(p_request, "reviewround", p_multipartcontentrequest);
			l_casereviewsumm	=	WebUtils.getRequestParameter(p_request, "fldcasereviewsumm",p_multipartcontentrequest);	
			l_cmseqid			=	WebUtils.getRequestIntParameter(p_request, "fldcmseqid", p_multipartcontentrequest);
			l_internalcaseid	=	WebUtils.getRequestParameter(p_request, "fldinternalcaseid", p_multipartcontentrequest);
			l_regclass			=	WebUtils.getRequestParameter(p_request, "fldregclass", p_multipartcontentrequest);
			
			WebUtils.setRequestAttribute(p_request, "caseId", l_caseid);
			if ("create".equals(l_mode)) {
				l_appresponseerror	=	new PortalAppResponse("ilqr.jsp");
			} else if ("edit".equals(l_mode)) {
				l_appresponseerror	=	new PortalAppResponse("qreditcasedata.jsp");
			}
			try {
				if ("create".equals(l_mode)) {
				l_qrtemplate				=	l_qrtportalapp.getTemplateDetailsQRL(l_projectid, l_reviewtype, p_usersession);
				} else if ("edit".equals(l_mode)) {
					l_qrtemplate				=	l_qrtportalapp.getReviewData(l_sequenceid).getQrTemplate();
				} 
				
				l_mapqualityreviewtabdata 	=	l_qrtportalapp.getReviewFieldList(l_qrtemplate);
				for (String l_tabname : l_mapqualityreviewtabdata.keySet()) { 
					l_totalfields	=	l_totalfields + l_mapqualityreviewtabdata.get(l_tabname).size();
				}
				if (l_fieldsreviewed != l_totalfields) {
					l_appresponseerror.logError(WebErrorCodes.QR.ERROR_FIELDS_REVIEWED_COUNT);
					l_iserror	=	true;
				}
				
				if(l_qrtemplate.getRvwAllToCorrect()==1)
				{
					l_cdbrflag	=	WebUtils.getRequestIntParameter(p_request, "cdbrcheckboxvalue", p_multipartcontentrequest);
				}
				
			} catch (SciServiceException serExcep) {
				log.error("Error saving review data of qr ", serExcep);
				l_appresponseerror.logError(serExcep);
				l_iserror	=	true;
			}

			if (l_caseassocid == -1) {
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_INVALID_CASE_ASSOCIATE_ID);
				l_iserror	=	true;
			}
			if (!(l_scftseqid > 0)) {
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_INVALID_SCFT_SEQID);
				l_iserror	=	true;
			}
			if (l_fieldsreviewed != (l_fieldswerror+l_fieldserror)) {
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_FIELDS_COUNT_MISMATCH);
				l_iserror	=	true;
			}
			
			if (l_iserror) {
				l_appresponseerror	=	prepareQRLPage(p_request, l_qrtportalapp, l_projectid, p_usersession, l_caseid, l_caseversion, l_reviewtype, l_appresponseerror, "error", false);
				return l_appresponseerror;
			}
			try {
				l_internalid		=	l_caseid + "_" + l_caseversion;
				l_pvcpcasemaster	=	l_qrtportalapp.getPVCPCaseMasterByInternalCaseId(l_internalid , l_projectid);
			} catch (SciServiceException serExcep) {
				log.error("Exception retrieving pcvp case details" , serExcep);
				l_appresponseerror	=	prepareQRLPage(p_request, l_qrtportalapp, l_projectid, p_usersession, l_caseid, l_caseversion, l_reviewtype, l_appresponseerror, "error", false);
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			l_reviewdata = new QRReviewData();
			if ("edit".equals(l_mode)) {
				if (l_sequenceid == -1) {
					l_appresponseerror	=	prepareQRLPage(p_request, l_qrtportalapp, l_projectid, p_usersession, l_caseid, l_caseversion, l_reviewtype, l_appresponseerror, "error",false);
					l_appresponseerror.logError(WebErrorCodes.QR.ERROR_SEQUENCE_ID);
					return l_appresponseerror;
				}
				l_reviewdata.setReviewVersion(l_reviewversion);
				l_reviewdata.setSequenceId(l_sequenceid);
			} else {
				l_reviewdata.setReviewVersion(1);
			}
			
			setUDFFields(
				p_request
			, 	l_qrtportalapp
			, 	l_projectid
			, 	l_pvcpcasemaster
			, 	p_multipartcontentrequest
			);
			
			l_reviewdata.setCdbrflag(l_cdbrflag);
			l_reviewdata.setCaseId(l_caseid);
			l_reviewdata.setCaseVersion(l_caseversion);
			l_reviewdata.setCaseSeriousness(l_pvcpcasemaster.getCaseSeriousness());
			l_reviewdata.setCaseAssociate(l_caseassocid);
			l_reviewdata.setCaseRecvDate(l_pvcpcasemaster.getInitialRecvDate());
			l_reviewdata.setFieldsReviewed(l_fieldsreviewed);
			l_reviewdata.setFieldsWithoutError(l_fieldswerror);
			l_reviewdata.setFieldsError(l_fieldserror);
			if ("create".equals(l_mode)) {
			l_reviewdata.setReviewRound(l_reviewround+1);
			} else if ("edit".equals(l_mode)) {
				l_reviewdata.setReviewRound(l_reviewround);
			}
			l_reviewdata.setReviewActive(1);
			l_reviewdata.setCreatedBy(p_usersession.getUserInfo().getUserSeqId());
			l_reviewdata.setQrTemplate(l_qrtemplate);
			l_reviewdata.setReviewType(l_reviewtype);
			l_reviewdata.setCaseQuality((l_fieldswerror*l_mf)/l_fieldsreviewed);
			l_reviewdata.setCat1FieldsReviewed(l_cat1reviewcount);
			l_reviewdata.setCat2FieldsReviewed(l_cat2reviewcount);
			l_reviewdata.setCat3FieldsReviewed(l_cat3reviewcount);
			l_reviewdata.setCat1FieldsWithoutError(l_cat1count);
			l_reviewdata.setCat2FieldsWithoutError(l_cat2count);
			l_reviewdata.setCat3FieldsWithoutError(l_cat3count);
			if (l_cat1reviewcount > 0) {
				l_reviewdata.setCaseCat1Quality((l_cat1count*l_mf)/l_cat1reviewcount);
			} else {
				l_reviewdata.setCaseCat1Quality(0);
			}
			if (l_cat2reviewcount > 0) {
				l_reviewdata.setCaseCat2Quality((l_cat2count*l_mf)/l_cat2reviewcount);
			} else {
				l_reviewdata.setCaseCat2Quality(0);
			}
			if (l_cat3reviewcount > 0) {
				l_reviewdata.setCaseCat3Quality((l_cat3count*l_mf)/l_cat3reviewcount);
			} else {
				l_reviewdata.setCaseCat3Quality(0);
			}
			l_reviewdata.setRegClass(l_regclass);
			l_reviewdata.setMedicalReviewerId(l_mrid);
			l_reviewdata.setCaseReviewSummary(l_casereviewsumm);
			l_reviewdata.setFieldDetailsJson(WebUtils.getRequestParameter(p_request, "fldjsondata", p_multipartcontentrequest));
			try {
				if ("create".equals(l_mode)) {
					l_qrtportalapp.saveReviewData(p_usersession, l_reviewdata, l_projectid, l_cmseqid, l_internalcaseid, l_pvcpcasemaster);
				} else if ("edit".equals(l_mode)) {
					l_qrtportalapp.saveUpdateReviewData(p_usersession, l_reviewdata, l_projectid, l_cmseqid, l_internalcaseid, l_pvcpcasemaster);
				}
				WebUtils.setRequestAttribute(p_request, "fldsequenceid", l_reviewdata.getSequenceId());
			} catch (SciServiceException serExcep) {
				l_appresponseerror	=	prepareQRLPage(p_request, l_qrtportalapp, l_projectid, p_usersession, l_caseid, l_caseversion, l_reviewtype, l_appresponseerror, "error", false);
				log.error("Error preparing the QRL page",serExcep);
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_SAVING_QRL_DATA);
				return l_appresponseerror;
			}
		} else if ("viewqrldetails".equals(p_actionid)) {
			QRReviewData	l_ilqrreviewdata	=	null;
			l_appresponse	=	null;
			l_searchtype 	= 	WebUtils.getRequestIntParameter(p_request, QR_REQUEST_SEARCH_TYPE,p_multipartcontentrequest);
			l_appresponse	=	new PortalAppResponse("ilqrreview.jsp");
			try {
				l_layouttype	=	l_qrtportalapp.getUserLayoutType(
										p_usersession.getUserInfo(),
										l_projectid
									);
				WebUtils.setRequestAttribute(
					p_request
				, 	"UserLayoutType"
				, 	l_layouttype
				);
			} catch (SciServiceException serExcep) {
				log.error(
					"Exception retrieving review type from db" , serExcep
				);
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			try {
				l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
				if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewtypelist"
					, 	l_reviewtypelist
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			try {
				l_errortypes	=	l_qrtportalapp.getErrorTypes(l_projectid);
				if (l_errortypes != null && l_errortypes.size()>0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"errortypes"
					, 	l_errortypes
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(
					WebErrorCodes.QR.ERROR_MISSING_ERROR_TYPE
				);
				return l_appresponse;
			}
			try {
				l_ilqrreviewdata	=	l_qrtportalapp.getReviewData (
											WebUtils.getRequestIntParameter(p_request, "fldsequenceid", p_multipartcontentrequest)
										);
				WebUtils.setRequestAttribute(
					p_request
				, 	"reviewdata"
				, 	l_ilqrreviewdata
				);
			} catch (SciServiceException serExcep) {
				log.error(
				"Error fetching review data of qr case "
				, 	serExcep
				);
				l_appresponse.logError(
				serExcep
				);
				return l_appresponse;
			}
			try {
				l_pvcpcasemaster	=	l_qrtportalapp.getPVCPCaseMasterByInternalCaseId(
											l_ilqrreviewdata.getCaseId() + "_" + l_ilqrreviewdata.getCaseVersion()
										, 	l_projectid
										);
				WebUtils.setRequestAttribute(p_request, "pvcp_case_record", l_pvcpcasemaster);
			} catch (SciServiceException serExcep) {
				log.error(
					"Error fetching case master data"
				, 	serExcep
				);
				l_appresponse.logError(
					serExcep
				);
				return l_appresponse;
			}
			l_reporttype	=	WebUtils.getRequestParameter(
									p_request
								, 	"fldreporttype"
								,	p_multipartcontentrequest
								);
			WebUtils.setRequestAttribute(
				p_request
			, 	"fldreporttype"
			, 	l_reporttype
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_STARTDATE_COUNT
			, 	l_startdate
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_ENDDATE_COUNT
			, 	l_enddate
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	"ismrnameshow"
			, 	l_qrtportalapp.isMRNameEnabledForProject(
					l_projectid
				)
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	"categoryaliases"
			, 	l_qrtportalapp.getApplicableCategories(
					l_projectid
				)
			);
			WebUtils.setRequestAttribute(p_request, QR_REQUEST_SEARCH_TYPE, l_searchtype);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_CASEID_SEARCH
			, 	WebUtils.getRequestParameter(
					p_request
				, 	QR_REQUEST_CASEID_SEARCH
				,	p_multipartcontentrequest
				)
			);
		} else if ("preparereport".equals(p_actionid)) {
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			l_reporttype		=	WebUtils.getRequestParameter(
										p_request
									, 	"fldreporttype"
									,	p_multipartcontentrequest
									);
			WebUtils.setRequestAttribute(
				p_request
			, 	"fldreporttype"
			, 	l_reporttype
			);
			
			l_reviewtype		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_REVIEWTYPE, p_multipartcontentrequest);
			
			WebUtils.setRequestAttribute(
				p_request
			, 	"fldreviewtype"
			, 	l_reviewtype
			);
			
			if ("DS".equals(l_reporttype)) {
				l_appresponse	=	null;
				l_appresponse	=	new PortalAppResponse("qrdailysummaryreport.jsp");
				try {
					WebUtils.setRequestAttribute(
						p_request
					, 	"dailysummarydata"
					, 	l_qrtportalapp.getDailySummaryReport(l_projectid)
					);
					/*WebUtils.setRequestAttribute(
						p_request
					, 	"dashboarddata"
					, 	l_qrtportalapp.getReportSummaryDashboardData(l_projectid)
					);
					WebUtils.setRequestAttribute(
						p_request
					, 	"cpreport"
					, 	l_qrtportalapp.getCaseProcessorReport(l_projectid, l_startdate,l_enddate)
					);
					WebUtils.setRequestAttribute(
						p_request
					, 	QR_REQUEST_STARTDATE_COUNT
					, 	l_startdate
					);
					WebUtils.setRequestAttribute(
						p_request
					, 	QR_REQUEST_ENDDATE_COUNT
					, 	l_enddate
					);*/
				} catch (SciServiceException serExcep) {
					log.error(
						"Error generating daily summary report"
					, 	serExcep
					);
					l_appresponseerror.logError(
						serExcep
					);
					return l_appresponseerror;
				}
				
			} else {
				l_appresponse	=	null;
				l_appresponse	=	new PortalAppResponse("qrreviewlanding.jsp");
				l_searchtype 		= 	WebUtils.isRequestParameterAvailable(p_request, QR_REQUEST_SEARCH_TYPE, p_multipartcontentrequest) ? 
											WebUtils.getRequestIntParameter(p_request, QR_REQUEST_SEARCH_TYPE, p_multipartcontentrequest) : 1;
				WebUtils.setRequestAttribute(p_request, QR_REQUEST_SEARCH_TYPE, l_searchtype);
				l_searchedcaseid	=	WebUtils.getRequestParameter(p_request,  QR_REQUEST_CASEID_SEARCH,	p_multipartcontentrequest);
				WebUtils.setRequestAttribute(p_request, QR_REQUEST_CASEID_SEARCH, l_searchedcaseid);
				
				try {
					l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
					if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
						WebUtils.setRequestAttribute(
							p_request
						, 	"reviewtypelist"
						, 	l_reviewtypelist
						);
					}
				} catch (SciServiceException serExcep) {
					l_appresponseerror.logError(serExcep);
					return l_appresponseerror;
				}
				try {
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewdata"
					, 	l_qrtportalapp.getQualityReviewLogAll(
							l_reporttype
						,	p_usersession
						,	l_startdate
						,	l_enddate
						,	l_projectid
						,	l_searchtype
						,	l_searchedcaseid
						,	l_reviewtype
						)
					);
					WebUtils.setRequestAttribute(
						p_request
					, 	QR_REQUEST_STARTDATE_COUNT
					, 	l_startdate
					);
					WebUtils.setRequestAttribute(
						p_request
					, 	QR_REQUEST_ENDDATE_COUNT
					, 	l_enddate
					);
				} catch (SciServiceException serExcep) {
					log.error(
						"Error fetching all the review data of qr "
					, 	serExcep
					);
					l_appresponseerror.logError(
						serExcep
					);
					return l_appresponseerror;
				}
			}
			try {
				l_safetydbcasefieldtemplate	=	SafetyDbConfigurationHelper.getActiveTemplateDetails(l_projectid);
				if (l_safetydbcasefieldtemplate != null) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"fldactivesafetydbtemplate"
					,	l_safetydbcasefieldtemplate
					);
				} else {
					log.error("Active Safety DB Configuration Field template is not present");
					l_appresponseerror.logError(WebErrorCodes.QR.ERROR_SAFETYCASEFILE_TEMPLATE_NOTACTIVATED);
					return l_appresponseerror;
				}
				
			} catch (SciServiceException serExcep) {
				log.error(
					"Error fetching active safety db case field template"
				, 	serExcep
				);
				l_appresponseerror.logError(
					serExcep
				);
				return l_appresponseerror;
			}
		} else if ("exportdsr".equals(p_actionid)) {
			try {
				l_appresponse	=	null;
				l_appresponse	=	new PortalAppResponse("qrdailysummaryreport.jsp");
				try {
					l_dailysummaryreportlist	=	l_qrtportalapp.getDailySummaryReport(l_projectid);
					WebUtils.setRequestAttribute(
						p_request
					, 	"dailysummarydata"
					, 	l_dailysummaryreportlist
					);
				} catch (SciServiceException serExcep) {
					log.error(
						"Error generating daily summary report"
					, 	serExcep
					);
					l_appresponse.logError(
						serExcep
					);
					return l_appresponseerror;
				}
				l_exportfile = new File(WebUtils.createStagingFile(WebUtils.getUserSession(p_request), "DailySummaryReport.csv"));
				l_fwr = new FileWriter(l_exportfile);
				l_exportfile.createNewFile();
				l_fullcontent	=	new StringBuilder(DSR_CSV_HEADER_1);
				l_fullcontent.append(System.lineSeparator());
				l_fullcontent.append(DSR_CSV_HEADER_2);
				l_fullcontent.append(System.lineSeparator());
				for (QRDailySummaryReport l_dailysummaryreport : l_dailysummaryreportlist) {
					appendContent(
						l_fullcontent
					,	String.valueOf(new SimpleDateFormat("dd-MMM-YYYY").format(l_dailysummaryreport.getDate()))
					,	String.valueOf(l_dailysummaryreport.getSeriousTotalCaseReviewed())
					,	String.valueOf(l_dailysummaryreport.getSeriousTotalCasePassed())
					,	String.valueOf(l_dailysummaryreport.getSeriousTotalCaseFailed())
					,	String.valueOf(l_dailysummaryreport.getSeriousAverageQuality())
					,	String.valueOf(l_dailysummaryreport.getNonSeriousTotalCaseReviewed())
					,	String.valueOf(l_dailysummaryreport.getNonSeriousTotalCasePassed())
					,	String.valueOf(l_dailysummaryreport.getNonSeriousTotalCaseFailed())
					,	String.valueOf(l_dailysummaryreport.getNonSeriousAverageQuality())
					);
				}
				l_fwr.write(l_fullcontent.toString());
				l_fwr.close();
				
				l_pwr = p_response.getWriter();  
				p_response.setContentType("text/xml");  
				p_response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
				l_fis = new FileInputStream(l_exportfile);  
	            
				int i;   
				while ((i = l_fis.read()) != -1) {  
					l_pwr.write(i);   
				}
			} catch(Exception excep) {
				log.error("Error during report generation", excep);
				l_appresponse.logError(WebErrorCodes.QR.ERROR_GENERATING_REPORT);
				return l_appresponse;
			} finally {
				try {
					l_appresponse.setIsDownloadStreamPresent(true);
					p_response.flushBuffer();
				    
					if(l_pwr != null) {
						l_pwr.close();
				   	}
				   	
				    if(l_fis != null){
				    	l_fis.close();
				   	}
				} catch (Exception excep) {
					log.error("Error during report generation", excep);
				    l_appresponse.logError(WebErrorCodes.QR.ERROR_GENERATING_REPORT);
					return l_appresponse;
				}
			}
		} else if ("exportfullreport".equals(p_actionid)) {
			boolean	l_isreportgensuccessful	=	true;
			l_appresponse	=	null;
			l_appresponse	=	new PortalAppResponse("qrreviewlanding.jsp");
			try {
				l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
				if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewtypelist"
					, 	l_reviewtypelist
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			try {
				
				try {
					l_reviewtype	=	WebUtils.getRequestParameter(p_request, QR_REQUEST_REVIEWTYPE, p_multipartcontentrequest);
					l_safetydbcasefieldtemplate	=	SafetyDbConfigurationHelper.getActiveTemplateDetails(l_projectid);
					if (l_safetydbcasefieldtemplate != null) {
						WebUtils.setRequestAttribute(
							p_request
						, 	"fldactivesafetydbtemplate"
						,	l_safetydbcasefieldtemplate
						);
					} else {
						log.error("Active Safety DB Configuration Field template is not present");
						l_appresponse.logError(WebErrorCodes.QR.ERROR_SAFETYCASEFILE_TEMPLATE_NOTACTIVATED);
						return l_appresponseerror;
					}
					
				} catch (SciServiceException serExcep) {
					log.error(
						"Error fetching active safety db case field template"
					, 	serExcep
					);
					l_appresponse.logError(
						serExcep
					);
					l_isreportgensuccessful	=	false;
					return l_appresponseerror;
				}
				try {
					l_reporttype		=	WebUtils.getRequestParameter(
												p_request
											, 	"fldreporttype"
											,	p_multipartcontentrequest
											);
					WebUtils.setRequestAttribute(
						p_request
					, 	"fldreporttype"
					, 	l_reporttype
					);
					l_exportfile	=	l_qrtportalapp.getFullSummaryReport(
											l_reporttype
										,	l_reviewtype
										,	p_usersession
										,	l_startdate
										,	l_enddate
										,	l_projectid
										);
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewdata"
					, 	l_qrreviewdatalist
					);
					WebUtils.setRequestAttribute(
						p_request
					, 	QR_REQUEST_STARTDATE_COUNT
					, 	l_startdate
					);
					WebUtils.setRequestAttribute(
						p_request
					, 	QR_REQUEST_ENDDATE_COUNT
					, 	l_enddate
					);
				} catch (SciServiceException serExcep) {
					log.error(
						"Error fetching all the review data of qr "
					, 	serExcep
					);
					l_appresponse.logError(
						serExcep
					);
					l_isreportgensuccessful	=	false;
					return l_appresponse;
				}
				
				l_pwr = p_response.getWriter();  
				p_response.setContentType("text/xml");  
				p_response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
				l_fis = new FileInputStream(l_exportfile);  
	            
				int i;   
				while ((i = l_fis.read()) != -1) {  
					l_pwr.write(i);   
				}
			} catch(Exception excep) {
				log.error("Error during report generation", excep);
				l_appresponse.logError(WebErrorCodes.QR.ERROR_GENERATING_REPORT);
				l_isreportgensuccessful	=	false;
				return l_appresponse;
			} finally {
				try {
					if (l_isreportgensuccessful) {
						l_appresponse.setIsDownloadStreamPresent(true);
						p_response.flushBuffer();
					}
					if(l_pwr != null) {
						l_pwr.close();
				   	}
				   	
				    if(l_fis != null){
				    	l_fis.close();
				   	}
				} catch (Exception excep) {
					log.error("Error during report generation", excep);
				    l_appresponse.logError(WebErrorCodes.QR.ERROR_GENERATING_REPORT);
					return l_appresponse;
				}
			}
		} else if ("ilqr_case_master".equals(p_actionid)) {
			
			l_appresponse		=	null;
			l_appresponse		=	new PortalAppResponse("case_master_upload.jsp");
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			try {
				WebUtils.setRequestAttribute(p_request, "fileformats", l_qrtportalapp.getApplicableFileFormat(l_projectid));
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
		} else if ("view_cases".equals(p_actionid)) {
			List<PvcpCaseMaster> l_caserecordlist = null;
			l_appresponse		=	null;
			l_appresponse		=	new PortalAppResponse("viewpvcpcases.jsp");
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			
			l_filetype		=	WebUtils.getRequestParameter(
									p_request
								, 	QR_REQUEST_FLDFILETYPE
								,	p_multipartcontentrequest
								);
			
			try {
				l_fileformats	=	l_qrtportalapp.getApplicableFileFormat(l_projectid);
				l_fileformat	=	(String)l_fileformats.keySet().toArray()[0];
				WebUtils.setRequestAttribute(p_request, "fileformats", l_qrtportalapp.getApplicableFileFormat(l_projectid));
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			try{
				l_caserecordlist = l_qrtportalapp.getCaseMasterFields(l_projectid);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error getting casemaster records");
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"columnmappingdetails"
				, 	FileDataMapperUtil.getFileMappingData(
						l_projectid
					, 	l_qrtportalapp.getPortalAppId()
					, 	l_filetype
					,	l_fileformat
					)
				);
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			WebUtils.setRequestAttribute(p_request, "caserecordlist", l_caserecordlist);
		} else if ("uploadCaseFile".equals(p_actionid)) {
			
			l_appresponse		=	new PortalAppResponse("caserecorduploadconfirm.jsp");
			l_appresponseerror	=	new PortalAppResponse("case_master_upload.jsp");
			try {
				WebUtils.setRequestAttribute(p_request, "fileformats", l_qrtportalapp.getApplicableFileFormat(l_projectid));
			} catch (SciServiceException serExcep1) {
				l_appresponseerror.logError(serExcep1);
				return l_appresponseerror;
			}
			try {
				l_uploadedfile	=	WebUtils.stageRequestParameterFile( p_request, "casemasterfile",p_multipartcontentrequest);
				l_filetype		=	WebUtils.getRequestParameter(
										p_request
									, 	QR_REQUEST_FLDFILETYPE
									,	p_multipartcontentrequest
									);
				l_fileformat	=	WebUtils.getRequestParameter(
										p_request
									, 	QR_REQUEST_FLDFILEFORMAT
									,	p_multipartcontentrequest
									);
			} catch (SciWebException webExcep) {
				log.error("Error uploading file",webExcep);
				l_appresponseerror.logError(WebErrorCodes.ProjectSetting.ERROR_UPLOADING_FILE);
				return l_appresponseerror;
			}
			if(l_uploadedfile.getStagedFileName().trim().equals(""))
			{
				log.error("Error uploading file");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_UPLOADING_MASTER_FILE);
				return l_appresponseerror;
			}
			if (!l_uploadedfile.getOriginalFileName().toLowerCase().endsWith("csv")&&!l_uploadedfile.getOriginalFileName().toLowerCase().endsWith("xlsx")
					&&!l_uploadedfile.getOriginalFileName().toLowerCase().endsWith("xls"))
			{
				log.error("Error invalid file");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_INVALID_FILE);
				return l_appresponseerror;
			}
			try{
				 l_origdatatable = new DataTable();
				if(l_uploadedfile.getOriginalFileName().toLowerCase().endsWith("csv"))
				{
					OfficeUtils.parseCsvFile(l_uploadedfile.getStagedFilePath(), l_origdatatable, true);
				}
				else
				{
					l_inputdateformat	=	ConfigHome.getPrjConfigSettings(
												l_qrtportalapp.getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_INPUT_DATE_FORMAT
											, 	l_projectid
											);
					l_outputdateformat	=	ConfigHome.getPrjConfigSettings(
												l_qrtportalapp.getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_OUTPUT_DATE_FORMAT
											, 	l_projectid
											);
					l_workbooksheet		=	OfficeUtils.parseWorkbook(
												l_uploadedfile.getStagedFilePath()
											,	true
											, 	0
											, 	l_inputdateformat
											, 	l_outputdateformat
											);
					l_workbooksheet.appendDataIntoDataTable(l_origdatatable);
                    /*l_origdatatable 	=	OfficeUtils.convertToDatatable(
                    							l_workbooksheet
                    						, 	""
                    						);*/
				}
				try {
					WebUtils.setRequestAttribute(
						p_request
					, 	"columnmappingdetails"
					, 	FileDataMapperUtil.getFileMappingData(
							l_projectid
						, 	l_qrtportalapp.getPortalAppId()
						, 	l_filetype
						,	l_fileformat
						)
					);
				} catch (SciServiceException serExcep) {
					l_appresponseerror.logError(serExcep);
					return l_appresponseerror;
				}
				try {
					l_transtable	=	FileDataMapperUtil.transformData(
											l_projectid
										, 	l_qrtportalapp.getPortalAppId()
										, 	l_filetype
										,	l_fileformat
										, 	l_origdatatable
										);
				} catch (SciServiceException serExcep) {
					l_appresponseerror.logError(serExcep);
					return l_appresponseerror;
				}
			}
			catch (SciException excep) 
			{
				log.error("Error invalid file");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_UPLOADING_FILE);
				return l_appresponseerror;
			}
			WebUtils.setRequestAttribute(p_request, "parsedata", l_transtable);
			WebUtils.setRequestAttribute(p_request, "fileformat", l_fileformat);
		} else if("save".equals(p_actionid)) {
			
			l_appresponse		=	new PortalAppResponse("case_master_review.jsp");
			l_appresponseerror	=	new PortalAppResponse("case_master_upload.jsp");
			
			Map<String, Map<String, PvcpCaseMaster>> l_exstingrecordcases = null;
			List<PvcpCaseMaster> l_caserecordlist = null,
							l_savedCaseRecoedList = null;
			l_origdatatable=null;
			l_origdatatable = (DataTable)p_request.getSession().getAttribute("uploadcasemasterdetails");
			
			try {
				WebUtils.setRequestAttribute(p_request, "fileformats", l_qrtportalapp.getApplicableFileFormat(l_projectid));
			} catch (SciServiceException serExcep1) {
				l_appresponseerror.logError(serExcep1);
				return l_appresponseerror;
			}
			l_filetype		=	WebUtils.getRequestParameter(
									p_request
								, 	QR_REQUEST_FLDFILETYPE
								,	p_multipartcontentrequest
								);
			l_fileformat	=	WebUtils.getRequestParameter(
									p_request
								, 	QR_REQUEST_FLDFILEFORMAT
								,	p_multipartcontentrequest
								);
			WebUtils.setRequestAttribute(p_request, "fileformat", l_fileformat);
			try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"columnmappingdetails"
				, 	FileDataMapperUtil.getFileMappingData(
						l_projectid
					, 	l_qrtportalapp.getPortalAppId()
					, 	l_filetype
					,	l_fileformat
					)
				);
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			
			try {
				l_exstingrecordcases	=	l_qrtportalapp.saveCaseMasterFields(
												p_usersession
											, 	l_origdatatable
											, 	l_projectid
											);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error storing case in case master db");
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			
			try {
				l_caserecordlist =QRPortalApp.listcasemasterrecords(p_usersession,l_origdatatable,  l_projectid);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error getting casemaster records");
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			Map<String,PvcpCaseMaster> existRecordMap = new LinkedHashMap<>();
			for(String key : l_exstingrecordcases.get("dbrecord").keySet())
			{
				existRecordMap.put(l_exstingrecordcases.get("dbrecord").get(key).getInternalCaseId(),
						l_exstingrecordcases.get("dbrecord").get(key));
			}
			l_savedCaseRecoedList = new ArrayList<>();
			for(PvcpCaseMaster ocaseMaster  : l_caserecordlist )
			{
				if(!existRecordMap.containsKey(ocaseMaster.getInternalCaseId()))
				{
					l_savedCaseRecoedList.add(ocaseMaster);
				}
			}
			WebUtils.setRequestAttribute(p_request, "duplicatecases", l_exstingrecordcases);
			WebUtils.setRequestAttribute(p_request, "caserecordlist", l_savedCaseRecoedList);
			
		} else if ("updatecaserecord".equals(p_actionid)) {
			String	caseincrementalid	=	WebUtils.getRequestParameter(
												p_request
											, 	"selected_caseid"
											, 	p_multipartcontentrequest
											);
			
			l_origdatatable=null;
			l_origdatatable = (DataTable)p_request.getSession().getAttribute("uploadcasemasterdetails");
			String	backFlag	=	WebUtils.getRequestParameter(
					p_request
				, 	"backflag"
				, 	p_multipartcontentrequest
				);
			List<PvcpCaseMaster> l_caserecordlist = null,
					l_savedCaseRecoedList = null;	
			
			WebUtils.setRequestAttribute(p_request, "backflag", backFlag);
			PvcpCaseMaster o_caseMasteruserrec = null;
			
			Map<String,PvcpCaseMaster> m_dbrecords_mod=new HashMap<String,PvcpCaseMaster>();
			Map<String,PvcpCaseMaster> m_userrecords_mod=new HashMap<String,PvcpCaseMaster>();
			Map<String,PvcpCaseMaster> m_userrecords=null;
			Map<String, Map<String, PvcpCaseMaster>> m_duplicatecasemastermap 	
									=	(Map<String, Map<String, PvcpCaseMaster>>)p_request.getSession().getAttribute("updateCasemastermap");
			l_appresponse	=	null;
			l_appresponse	=	new PortalAppResponse("case_master_review.jsp");
			
			 m_userrecords = m_duplicatecasemastermap.get("userrecord");
			 
			 int j = 0;
			for (int i=0; i<m_userrecords.size(); i++) {
				
				if(m_userrecords.get("UserRecord_"+i).getInternalCaseId().equals(caseincrementalid))
				{
					o_caseMasteruserrec =m_userrecords.get("UserRecord_"+i);
				}
				else
				{
					m_userrecords_mod.put("UserRecord_"+j, m_userrecords.get("UserRecord_"+i));
					m_dbrecords_mod.put("ExistingRecord_"+j, m_duplicatecasemastermap.get("dbrecord").get("ExistingRecord_"+i));
					j++;
				}
			}
			
			m_duplicatecasemastermap.clear();
			m_duplicatecasemastermap.put("userrecord", m_userrecords_mod);
			m_duplicatecasemastermap.put("dbrecord", m_dbrecords_mod);
			WebUtils.setRequestAttribute(p_request, "duplicatecases", m_duplicatecasemastermap);
			l_filetype		=	WebUtils.getRequestParameter(
					p_request
				, 	QR_REQUEST_FLDFILETYPE
				,	p_multipartcontentrequest
				);
			l_fileformat	=	WebUtils.getRequestParameter(
					p_request
				, 	QR_REQUEST_FLDFILEFORMAT
				,	p_multipartcontentrequest
				);
			WebUtils.setRequestAttribute(p_request, "fileformat", l_fileformat);
			try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"columnmappingdetails"
				, 	FileDataMapperUtil.getFileMappingData(
						l_projectid
					, 	l_qrtportalapp.getPortalAppId()
					, 	l_filetype
					,	l_fileformat
					)
				);
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			try 
			{
				l_qrtportalapp.updateCaseMaster(o_caseMasteruserrec, l_projectid, p_usersession);
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			Map<String,PvcpCaseMaster> existRecordMap = new LinkedHashMap<>();
			for(String key : m_duplicatecasemastermap.get("dbrecord").keySet())
			{
				existRecordMap.put(m_duplicatecasemastermap.get("dbrecord").get(key).getInternalCaseId(),
						m_duplicatecasemastermap.get("dbrecord").get(key));
			}
			l_savedCaseRecoedList = new ArrayList<>();
			
			if(backFlag.equals("insert"))
			{
				l_savedCaseRecoedList.add(o_caseMasteruserrec);
			}
			else{
				try {
					l_caserecordlist =QRPortalApp.listcasemasterrecords(p_usersession,l_origdatatable,  l_projectid);
				}
				catch(SciServiceException serExcep)
				{
					log.error("Error getting casemaster records");
					l_appresponse.logError(serExcep);
					return l_appresponseerror;
				}
				for(PvcpCaseMaster ocaseMaster  : l_caserecordlist )
				{
					if(!existRecordMap.containsKey(ocaseMaster.getInternalCaseId()))
					{
						l_savedCaseRecoedList.add(ocaseMaster);
					}
				}
			}
			
			WebUtils.setRequestAttribute(p_request, "caserecordlist", l_savedCaseRecoedList);
		} else if("searchbycaseid".equals(p_actionid)) {
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			l_reporttype		=	WebUtils.getRequestParameter(
										p_request
									, 	"fldreporttype"
									,	p_multipartcontentrequest
									);
			WebUtils.setRequestAttribute(
				p_request
			, 	"fldreporttype"
			, 	l_reporttype
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_CASEID_SEARCH
			, 	WebUtils.getRequestParameter(
					p_request
				, 	QR_REQUEST_CASEID
				,	p_multipartcontentrequest
				)
			);
		}else if("casemasterinsertview".equals(p_actionid)) {
			l_appresponse=	new PortalAppResponse("case_master_insert.jsp");
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
		
			try {
				WebUtils.setRequestAttribute(p_request, "fileformats", l_qrtportalapp.getApplicableFileFormat(l_projectid));
			} catch (SciServiceException serExcep1) {
				l_appresponseerror.logError(serExcep1);
				return l_appresponseerror;
			}
			return l_appresponse;
		} 
		else if("checkFileColumns".equals(p_actionid)) {
			l_appresponse=	new PortalAppResponse("case_master_insert.jsp");
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			Map<String, String> colMappingDetails = null;
			String selectedFileFormat = null; 
			selectedFileFormat = WebUtils.getRequestParameter(p_request, 	QR_REQUEST_FLDFILEFORMAT, 	p_multipartcontentrequest);
			
			WebUtils.setRequestAttribute(p_request, "selfileformat", selectedFileFormat);
			try
			{
				l_listuser	=	ProjectUserManagementHome.getProjectUserList(l_projectid);
				l_listuser.remove(UserHome.retrieveBootstrapUser());
				l_listuser.remove(p_usersession.getUserInfo());
				WebUtils.setRequestAttribute(
					p_request
				,	"list_of_users"
				, 	l_listuser
				);
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error retrieving list of users", serExcep);
				l_appresponse.logError(serExcep);
			}
			try {
				WebUtils.setRequestAttribute(p_request, "fileformats", l_qrtportalapp.getApplicableFileFormat(l_projectid));
			} catch (SciServiceException serExcep1) {
				l_appresponseerror.logError(serExcep1);
				return l_appresponseerror;
			}
			l_filetype		=	WebUtils.getRequestParameter(
									p_request
								, 	"fldfiletype"
								,	p_multipartcontentrequest
								);
			try {
				colMappingDetails = FileDataMapperUtil.getFileMappingData(
						l_projectid
					, 	l_qrtportalapp.getPortalAppId()
					, 	l_filetype
					,	selectedFileFormat
					);
				WebUtils.setRequestAttribute(
					p_request
				, 	"columnmappingdetails"
				, 	colMappingDetails
				);
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			
			if(colMappingDetails !=null)
			{
				for(String key : enableUdfList.keySet())
				{
					if(colMappingDetails.get(key.toUpperCase()) == null)
					{
						l_appresponseerror.logError(WebErrorCodes.QR.ERROR_UDF_FIELD_MAPPING , "Udf mapping not present in file upload");
						return l_appresponseerror;
					}
				}
			}
			return l_appresponse;
		} 
		else if("insertcaserecord".equals(p_actionid)) {
			
			l_appresponse		=	new PortalAppResponse("case_master_review.jsp");
			l_appresponseerror	=	new PortalAppResponse("case_master_insert.jsp");
			
			PvcpCaseMaster o_casemaster = new PvcpCaseMaster(),
						   o_casemasterDbRecord = null;
			Map<String, Map<String, PvcpCaseMaster>> l_exstingrecordcases = null;
			List<PvcpCaseMaster> l_caserecordlist = null;
			Date dt_irddate = new Date();
			
			l_filetype		=	WebUtils.getRequestParameter(
									p_request
								, 	"fldfiletype"
								,	p_multipartcontentrequest
								);
			
			String selectedFileFormat = null; 
			selectedFileFormat = WebUtils.getRequestParameter(p_request, 	QR_REQUEST_FLDFILEFORMAT, 	p_multipartcontentrequest);
			WebUtils.setRequestAttribute(p_request, "fileformat", selectedFileFormat);
			try {
				WebUtils.setRequestAttribute(p_request, "fileformats", l_qrtportalapp.getApplicableFileFormat(l_projectid));
			} catch (SciServiceException serExcep1) {
				l_appresponseerror.logError(serExcep1);
				return l_appresponseerror;
			}
			
			String caseid = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_COL_CASE_ID, 	p_multipartcontentrequest);
			String version = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_COL_VERSION, 	p_multipartcontentrequest);
			String casetype = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_COL_CASE_TYPE, 	p_multipartcontentrequest);
			String seriousness = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_COL_SERIOUSNESS, 	p_multipartcontentrequest);
			String ird = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_IRD, 	p_multipartcontentrequest);
			String product = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_COL_PRODUCT, 	p_multipartcontentrequest);
			int de = WebUtils.getRequestIntParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_DENAME, 	p_multipartcontentrequest);
			
			String udf1  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF1, 	p_multipartcontentrequest);
			String udf2  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF2, 	p_multipartcontentrequest);
			String udf3  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF3, 	p_multipartcontentrequest);
			String udf4  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF4, 	p_multipartcontentrequest);
			String udf5  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF5, 	p_multipartcontentrequest);
			String udf6  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF6, 	p_multipartcontentrequest);
			String udf7  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF7, 	p_multipartcontentrequest);
			String udf8  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF8, 	p_multipartcontentrequest);
			String udf9  = WebUtils.getRequestParameter(p_request, 	QRConstant.CASE_MASTER_INTERNAL_UDF9, 	p_multipartcontentrequest);
			String udf10  = WebUtils.getRequestParameter(p_request, QRConstant.CASE_MASTER_INTERNAL_UDF10, 	p_multipartcontentrequest);
			
			if(StringUtils.isNullOrEmpty(caseid))
			{
				log.error("Empty CaseId");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_CASEID_EMPTY,"caseid");
				return l_appresponseerror;
			}
			if(StringUtils.isNullOrEmpty(version))
			{
				log.error("Empty Case Version");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_CASE_VERSION_EMPTY,"version");
				return l_appresponseerror;
			}
			if(StringUtils.isNullOrEmpty(seriousness))
			{
				log.error("Empty Case Seriousness");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_SERIOUSNESS_EMPTY, "Case Seriousness");
				return l_appresponseerror;
			}
			if(StringUtils.isNullOrEmpty(ird))
			{
				log.error("Empty Initial Receive Date");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_IRD_EMPTY, "Initial Receive Date");
				return l_appresponseerror;
			}else
			{
				try{
					dt_irddate =  new SimpleDateFormat("dd-MMM-yyyy").parse(ird);
				}
				catch(ParseException prex)
				{
					log.error("Invalid Initial Receive Date Format");
					l_appresponseerror.logError(WebErrorCodes.QR.ERROR_INVALID_IRD, "IRD Format");
					return l_appresponseerror;
				}
			}
			
			o_casemaster.setCaseId(caseid);
			o_casemaster.setCaseVersion(version);
			o_casemaster.setCaseSeriousness(seriousness);
			o_casemaster.setInitialRecvDate(dt_irddate);
			o_casemaster.setCaseType(casetype);
			o_casemaster.setProduct(product);
			o_casemaster.setPcmDeUserSeqId(de);
			o_casemaster.setInternalCaseId(caseid+"_"+version);
			//TODO: Need to change it to DATETIMENOW
			o_casemaster.setCreatedOn(new Date());
			o_casemaster.setCreatedBy(p_usersession.getUserInfo().getUserSeqId());
			o_casemaster.setProjectSeqId(l_projectid);
			o_casemaster.setUdf1(udf1);
			o_casemaster.setUdf2(udf2);
			o_casemaster.setUdf3(udf3);
			o_casemaster.setUdf4(udf4);
			o_casemaster.setUdf5(udf5);
			o_casemaster.setUdf6(udf6);
			o_casemaster.setUdf7(udf7);
			o_casemaster.setUdf8(udf8);
			o_casemaster.setUdf9(udf9);
			o_casemaster.setUdf10(udf10);
			
			try {
				l_exstingrecordcases = l_qrtportalapp.saveCaseMasterrecord(p_usersession, o_casemaster, l_projectid);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error storing case in case master db");
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			try {
					o_casemasterDbRecord = l_qrtportalapp.getPVCPCaseMasterByInternalCaseId(caseid+"_"+version, l_projectid);
				}
				catch(SciServiceException serExcep)
				{
					log.error("Error getting case master records");
					l_appresponseerror.logError(serExcep);
					return l_appresponseerror;
				}
			try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"columnmappingdetails"
				, 	FileDataMapperUtil.getFileMappingData(
						l_projectid
					, 	l_qrtportalapp.getPortalAppId()
					, 	l_filetype
					,	selectedFileFormat
					)
				);
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			
			WebUtils.setRequestAttribute(p_request, "duplicatecases", l_exstingrecordcases);
			WebUtils.setRequestAttribute(p_request, "existingCaseRecord", o_casemasterDbRecord);
			WebUtils.setRequestAttribute(p_request, "backflag", "insert");
			
			
		} else if ("view_graph".equals(p_actionid)) {
			List<QRReviewData> l_qualitylid = null;
			int l_caseSeriousnessThreshold = -1;
			int l_caseNonSeriousnessThreshold = -1;
			l_startdate	=	WebUtils.isRequestParameterAvailable(p_request, "fldstartdatecount", p_multipartcontentrequest) ? 
					WebUtils.getRequestIntParameter(p_request, "fldstartdatecount", p_multipartcontentrequest) : -15;
			l_enddate   =	WebUtils.isRequestParameterAvailable(p_request, "fldenddatecount", p_multipartcontentrequest) ? 
					WebUtils.getRequestIntParameter(p_request, "fldenddatecount", p_multipartcontentrequest) : 0;

			try{
				l_qualitylid = l_qrtportalapp.getQualityReviewLogByRange(p_usersession, l_projectid,l_startdate,l_enddate);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error getting casemaster records");
				l_appresponse	=	new PortalAppResponse("case_master_upload.jsp");
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			
			l_caseSeriousnessThreshold = l_qrtportalapp.getQRGraphSeriousnessThreshold(l_projectid);
			l_caseNonSeriousnessThreshold = l_qrtportalapp.getQRGraphNonSeriousnessThreshold(l_projectid);
			WebUtils.setRequestAttribute(p_request, QR_REQUEST_STARTDATE_COUNT, l_startdate);
			WebUtils.setRequestAttribute(p_request, QR_REQUEST_ENDDATE_COUNT, 	l_enddate);
			WebUtils.setRequestAttribute(p_request, QR_GRAPH_SERIOUS_CASE_THRESHOLD, l_caseSeriousnessThreshold);
			WebUtils.setRequestAttribute(p_request, QR_GRAPH_NON_SERIOUS_CASE_THRESHOLD, l_caseNonSeriousnessThreshold);
			WebUtils.setRequestAttribute(p_request, "qualityloglist", l_qualitylid);
			l_appresponse	=	new PortalAppResponse("qrgraphview.jsp");
		} else if ("showcasehistory".equals(p_actionid)) {
			Map<String, List<QRReviewData>> qrreviewdatamap = null;
			l_appresponseerror	=	null;
			l_appresponse	= null;
			l_appresponseerror	=	new PortalAppResponse("qrreviewlanding.jsp");
			l_appresponse = new PortalAppResponse("qrcasehistory.jsp");
			String caseid = null;
			String caseversion = null;
			String fldsearchedcaseid = null;
			
			caseid = WebUtils.getRequestParameter(p_request, "flhistroycaseid", p_multipartcontentrequest);
			caseversion = WebUtils.getRequestParameter(p_request, QR_HISTORY_CASE_VERSION, p_multipartcontentrequest);
			fldsearchedcaseid = WebUtils.getRequestParameter(p_request, "fldsearchedcaseid", p_multipartcontentrequest);
			
			/*for error page redirection*/
			l_reporttype		=	WebUtils.getRequestParameter(p_request,"fldreporttype",	p_multipartcontentrequest);
			WebUtils.setRequestAttribute(p_request,	"fldreporttype",l_reporttype);

			l_reviewtype		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_REVIEWTYPE, p_multipartcontentrequest);
			WebUtils.setRequestAttribute(p_request, "fldreviewtype",l_reviewtype);
			l_searchtype 		= 	WebUtils.isRequestParameterAvailable(p_request, QR_REQUEST_SEARCH_TYPE, p_multipartcontentrequest) ? 
						WebUtils.getRequestIntParameter(p_request, QR_REQUEST_SEARCH_TYPE, p_multipartcontentrequest) : 1;
			WebUtils.setRequestAttribute(p_request, QR_REQUEST_SEARCH_TYPE, l_searchtype);
			l_searchedcaseid	=	WebUtils.getRequestParameter(p_request,  QR_REQUEST_CASEID_SEARCH,	p_multipartcontentrequest);
			WebUtils.setRequestAttribute(p_request, QR_REQUEST_CASEID_SEARCH, l_searchedcaseid);
			
			try {
				l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
				if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewtypelist"
					, 	l_reviewtypelist
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"reviewdata"
				, 	l_qrtportalapp.getQualityReviewLogAll(
						l_reporttype
					,	p_usersession
					,	l_startdate
					,	l_enddate
					,	l_projectid
					,	l_searchtype
					,	l_searchedcaseid
					,	l_reviewtype
					));
				WebUtils.setRequestAttribute(
					p_request
				, 	QR_REQUEST_STARTDATE_COUNT
				, 	l_startdate
				);
				WebUtils.setRequestAttribute(
					p_request
				, 	QR_REQUEST_ENDDATE_COUNT
				, 	l_enddate
				);
			} catch (SciServiceException serExcep) {
				log.error("Error fetching all the review data of qr ", 	serExcep);
				l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			try {
				l_safetydbcasefieldtemplate	=	SafetyDbConfigurationHelper.getActiveTemplateDetails(l_projectid);
				if (l_safetydbcasefieldtemplate != null) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"fldactivesafetydbtemplate"
					,	l_safetydbcasefieldtemplate
					);
				} else {
					log.error("Active Safety DB Configuration Field template is not present");
					l_appresponseerror.logError(WebErrorCodes.QR.ERROR_SAFETYCASEFILE_TEMPLATE_NOTACTIVATED);
					l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
					return l_appresponseerror;
				}
				
			} catch (SciServiceException serExcep) {
				log.error(
					"Error fetching active safety db case field template"
				, 	serExcep
				);
				l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
				l_appresponseerror.logError(
					serExcep
				);
				return l_appresponseerror;
			}
			try {
				qrreviewdatamap = l_qrtportalapp.getCaseHistoryByInternalId(p_usersession, l_projectid, caseid, caseversion);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error getting casemaster records");
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			WebUtils.setRequestAttribute(p_request, "casehistorymap", qrreviewdatamap);
			WebUtils.setRequestAttribute(p_request, "caseid", caseid);
			WebUtils.setRequestAttribute(p_request, "caseversion", caseversion);
			
			//for back button
			l_reporttype		=	WebUtils.getRequestParameter(p_request,"fldreporttype",	p_multipartcontentrequest);
			l_searchtype 		= 	WebUtils.isRequestParameterAvailable(p_request, QR_REQUEST_SEARCH_TYPE, p_multipartcontentrequest) ? 
					WebUtils.getRequestIntParameter(p_request, QR_REQUEST_SEARCH_TYPE, p_multipartcontentrequest) : 1;
					WebUtils.setRequestAttribute(p_request, QR_REQUEST_SEARCH_TYPE, l_searchtype);
			l_searchedcaseid	=	WebUtils.getRequestParameter(p_request,  QR_REQUEST_CASEID_SEARCH,	p_multipartcontentrequest);
					WebUtils.setRequestAttribute(p_request, QR_REQUEST_CASEID_SEARCH, l_searchedcaseid);
			WebUtils.setRequestAttribute(p_request,	"fldreporttype", l_reporttype);
			WebUtils.setRequestAttribute(p_request, "startdate", l_startdate);
			WebUtils.setRequestAttribute(p_request, "enddate", l_enddate);
			WebUtils.setRequestAttribute(p_request, "fldsearchedcaseid", fldsearchedcaseid);
		} else if ("prepareerrdb".equals(p_actionid)) {
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			try {
				try {
					l_errortypes	=	l_qrtportalapp.getErrorTypes(l_projectid);
					if (l_errortypes != null && l_errortypes.size()>0) {
						WebUtils.setRequestAttribute(
							p_request
						, 	"errortypes"
						, 	l_errortypes
						);
					}
				} catch (SciServiceException serExcep) {
					l_appresponseerror.logError(
						WebErrorCodes.QR.ERROR_MISSING_ERROR_TYPE
					);
					return l_appresponseerror;
				}
				try {
					l_qrtportalapp.getProjectStartDate(l_projectid);
				} catch (SciServiceException serExcep) {
					l_appresponseerror.logError(serExcep);
					return l_appresponseerror;
				}
				
				l_errordbdata	=	l_qrtportalapp.getErrorDbData(l_projectid);
				for (QRErrorDashboardData l_errordata : l_errordbdata) {
					if (!(l_errordata.getErrorFieldsCountMap() != null && l_errordata.getErrorFieldsCountMap().size() > 0)) {
						l_iserrordatapresent	=	false;
					}
				}
				WebUtils.setRequestAttribute(
					p_request
				, 	"categoryaliases"
				, 	l_qrtportalapp.getApplicableCategories(
						l_projectid
					)
				);
				
				try {
					l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
					if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
						WebUtils.setRequestAttribute(
							p_request
						, 	"reviewtypelist"
						, 	l_reviewtypelist
						);
					}
				} catch (SciServiceException serExcep) {
					l_appresponseerror.logError(serExcep);
					return l_appresponseerror;
				}
				WebUtils.setRequestAttribute(p_request, "iserrordatapresent", l_iserrordatapresent);
				WebUtils.setRequestAttribute(p_request, "errordbdata", l_errordbdata);
				l_appresponse	=	new PortalAppResponse("qrerrordb.jsp");
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(
					WebErrorCodes.QR.ERROR_CREATING_ERROR_DASHBOARD
				);
				return l_appresponseerror;
			}
		}else if(p_actionid.equals("downloadReviewExcel"))
		{
			PrintWriter oExportFileWriter = null;
			InputStream oInputStream = null;
			
			try
			{
				
				listTemp = new ArrayList<>();
				listTemp = l_qrtportalapp.updateExcel(WebUtils.getRequestParameter(p_request, "fldsequenceid", p_multipartcontentrequest));
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error in updating excel file ", 	serExcep);
				l_appresponse.logError(serExcep);
				return l_appresponse;
				
			} catch (Exception serExcep) {
				log.error("Error in updating excel file ", 	serExcep);
				return l_appresponse;
			}
			
			try
			{
				oExportFileWriter = p_response.getWriter();  
				p_response.setContentType("APPLICATION/OCTET-STREAM");  
				p_response.setHeader("Content-Disposition","attachment; filename=\"" + listTemp.get(0)+".xlsx" + "\"");
				oInputStream = new ByteArrayInputStream((byte[]) listTemp.get(2));
				int nNum;   
				while ((nNum = oInputStream.read()) != -1) 
				{  
					oExportFileWriter.write(nNum);   
				}
			}catch(IOException excep)
			{
				log.error("");
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_MISSING_ERROR_TYPE); //change this
			}finally
			{
				try 
				{
					// request.setAttribute("r_downloadTemplate", "yes");
					l_appresponse.setIsDownloadStreamPresent(true);
					//log.debug("response state after written - {} ", response.isCommitted());
					p_response.flushBuffer();
				    
					if(oExportFileWriter != null)
				   	{
				    	oExportFileWriter.close();
				   	}
				   	
				    if(oInputStream != null)
				   	{
				    	oInputStream.close();
				   	}
				}
				catch (Exception excep) 
				{
					log.error("Error during template downloading", excep);
					l_appresponse.logError(WebErrorCodes.SourceDocUtil.ERROR_IN_FILE_DOWNLOADING);   //change this
					return l_appresponse;
				}
			}
		} else if ("preparecpdb".equals(p_actionid)) {
			l_appresponse		=	null;
			l_appresponse		=	new PortalAppResponse("qrcpdb.jsp");
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("ilqrlanding.jsp");
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_STARTDATE_COUNT
			, 	l_startdate
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_ENDDATE_COUNT
			, 	l_enddate
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	"categoryaliases"
			, 	l_qrtportalapp.getApplicableCategories(
					l_projectid
				)
			);
			try {
				l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
				if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewtypelist"
					, 	l_reviewtypelist
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponseerror.logError(serExcep);
				return l_appresponseerror;
			}
			try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"cpreport"
				, 	l_qrtportalapp.getCaseProcessorReport(l_projectid, l_startdate,l_enddate)
				);
			} catch (SciServiceException serExcep) {
				log.error("Error in creating CP Dashboard", 	serExcep);
				l_appresponseerror.logError(serExcep);
				return	l_appresponseerror;
			}
		}  else if ("cpdbexport".equals(p_actionid)) {
			l_appresponse		=	null;
			l_appresponse		=	new PortalAppResponse("qrcpdb.jsp");
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_STARTDATE_COUNT
			, 	l_startdate
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_ENDDATE_COUNT
			, 	l_enddate
			);
			WebUtils.setRequestAttribute(
				p_request
			, 	"categoryaliases"
			, 	l_qrtportalapp.getApplicableCategories(
					l_projectid
				)
			);
			try {
				l_reviewtypelist	=	l_qrtportalapp.getReviewTypeProjectList(l_projectid);
				if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"reviewtypelist"
					, 	l_reviewtypelist
					);
				}
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"cpreport"
				, 	l_qrtportalapp.getCaseProcessorReport(l_projectid, l_startdate,l_enddate)
				);
			} catch (SciServiceException serExcep) {
				log.error("Error in creating CP Dashboard", 	serExcep);
				l_appresponse.logError(serExcep);
				return	l_appresponse;
			}
			
			try {
				try {
					l_exportfile	=	l_qrtportalapp.exportCaseProcessorReport(
											p_usersession
										,	l_startdate
										,	l_enddate
										,	l_projectid
										);
				} catch (SciServiceException serExcep) {
					log.error(
						"Error Exporting CP Data"
					, 	serExcep
					);
					l_appresponse.logError(
						serExcep
					);
					return l_appresponse;
				}
				
				l_pwr = p_response.getWriter();  
				p_response.setContentType("text/xml");  
				p_response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
				l_fis = new FileInputStream(l_exportfile);  
	            
				int i;   
				while ((i = l_fis.read()) != -1) {  
					l_pwr.write(i);   
				}
			} catch(Exception excep) {
				log.error("Error during report generation", excep);
				l_appresponse.logError(WebErrorCodes.QR.ERROR_GENERATING_REPORT);
				return l_appresponse;
			} finally {
				try {
					l_appresponse.setIsDownloadStreamPresent(true);
					p_response.flushBuffer();
				    
					if(l_pwr != null) {
						l_pwr.close();
				   	}
				   	
				    if(l_fis != null){
				    	l_fis.close();
				   	}
				} catch (Exception excep) {
					log.error("Error during report generation", excep);
				    l_appresponse.logError(WebErrorCodes.QR.ERROR_GENERATING_REPORT);
					return l_appresponse;
				}
			}
		}
		return l_appresponse;
	}
	
	public PortalAppResponse processProjectSettingsAction(
		HttpServletRequest 				p_request
	, 	HttpServletResponse 			p_response
	,	MultipartContentRequestWrapper	p_multipartcontentrequest
	, 	String 							p_actionid
	, 	UserSession 					p_usersession
	, 	List<UserPreference> 			p_listappspecificuserpreferences
	, 	IPortalApp 						p_portalapp
	) {
		
		PortalAppResponse			l_appresponse				=	null,
									l_appresponseerror			=	null;
		String						l_templatename				=	null,
									l_caseclassificationoverall	=	null,
									l_caseclassificationcat1	=	null,
									l_caseclassificationcat2	=	null,
									l_caseclassificationcat3	=	null,
									l_casecriteria				=	null;;
		QRPortalApp					l_qrtportalapp				=	(QRPortalApp)	p_portalapp;
		int							l_projectid					=	0,
									l_correctionallowed			=	0,
									l_casecriteriamode			=	0,
									l_reviewsummrequired		=	0;
		ConfigTplObj				l_safetydbcasefieldtemplate	=	null;
		QRTemplate					l_qrtemplate				=	null;
		String						l_casefieldmapping			=	null,
									l_mode						=	null;
		double						l_casequalitythreshold		=	0,
									l_cat1qualitythreshold		=	0,
									l_cat2qualitythreshold		=	0,
									l_cat3qualitythreshold		=	0;
		
		l_appresponse	=	new PortalAppResponse(getDefaultPageUri());
		
		l_projectid		=	WebUtils.getAuthorizedProjectId(p_request);
		l_mode			=	WebUtils.getRequestParameter(
								p_request
							, 	QR_REQUEST_PARAMETER_MODE
							, 	p_multipartcontentrequest
							);
		if (!StringUtils.isNullOrEmpty(l_mode)) {
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_PARAMETER_MODE
			, 	l_mode
			);
		}
		WebUtils.setRequestAttribute(
			p_request
		, 	"categoryaliases"
		, 	l_qrtportalapp.getApplicableCategories(
				l_projectid
			)
		);
		if (StringUtils.isNullOrEmpty(p_actionid)) {
			List<QRTemplate>	l_templatelist		=	null;
			l_appresponse	=	new PortalAppResponse("qrlisttemplate.jsp");
			try {
				l_templatelist	=	new ArrayList<> ();
				l_templatelist	=	l_qrtportalapp.getTemplates(l_projectid);
				WebUtils.setRequestAttribute(
					p_request
				, 	"templatelist"
				, 	l_templatelist
				);
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
		} else if ("createnewqrtblank".equals(p_actionid)) {
			l_appresponse	=	null;
			l_appresponse	=	new PortalAppResponse("qrcreatetemplate.jsp");
		} else if ("createnewqrt".equals(p_actionid)) {
			l_appresponseerror	=	null;
			l_appresponseerror	=	new PortalAppResponse("qrcreatetemplate.jsp");
			l_appresponse		=	null;
			l_appresponse		=	new PortalAppResponse("qrtemplateview.jsp");
			
			l_templatename	=	WebUtils.getRequestParameter(
									p_request
								, 	QR_REQUEST_PARAMETER_TEMPLATE_NAME
								, 	p_multipartcontentrequest
								);
			if(StringUtils.isNullOrEmpty(l_templatename)) {
				l_appresponseerror.logError(WebErrorCodes.ProjectSetting.ERROR_TEMPLATE_NAME_EMPTY);
				return l_appresponseerror;
			}
			if (!DataValidationUtils.isValidObjectName(l_templatename)) {
				l_appresponseerror.logError(WebErrorCodes.ProjectSetting.ERROR_TEMPLATE_NAME_INVALID,null);
				return l_appresponseerror;
			}
			
			//TODO: Validation need to be added for the new parameters
			try {
				boolean	l_ctonameunique	=	l_qrtportalapp.isObjectNameUnique(
												l_projectid
											,	l_templatename
											);
				if (!l_ctonameunique) {
					l_appresponseerror.logError(WebErrorCodes.ProjectSetting.ERROR_TEMPLATE_NAME_NOT_UNIQUE, null);
					return l_appresponseerror;
				}
			} catch (SciServiceException serExcep) {
				log.error("Tempalte name is not unique", serExcep);
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			
			try {
				l_safetydbcasefieldtemplate	=	SafetyDbConfigurationHelper.getActiveTemplateDetails(l_projectid);
				if (l_safetydbcasefieldtemplate != null) {
					WebUtils.setRequestAttribute(
						p_request
					, 	"safetydbcasefields"
					, 	l_safetydbcasefieldtemplate
					);
				} else {
					l_appresponseerror.logError(WebErrorCodes.QR.ERROR_SAFETYCASEFILE_TEMPLATE_NOTACTIVATED);
					return l_appresponseerror;
				}
			} catch (SciServiceException serExcep) {
				log.error("Active Tempalte for Safety DB Case Field is not defined", serExcep);
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			
			if (!ConfigHome.isPrjConfigSettingPresent(getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_APPLICABLE_CAT, l_projectid)) {
				l_appresponseerror.logError(WebErrorCodes.QR.ERROR_PROJECT_NOT_CONFIGURED);
				return l_appresponseerror;
			}
			
			try {
				l_qrtemplate	=	l_qrtportalapp.saveTemplate(
										l_projectid
									, 	l_templatename
									,	l_safetydbcasefieldtemplate.getSequenceId()
									, 	p_usersession
									);
				WebUtils.setRequestAttribute(
					p_request
				, 	"fldtemplatedata"
				, 	l_qrtemplate
				);
			} catch (SciServiceException serExcep) {
				log.error("Error Saving quality review template data into db ", serExcep);
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
		} else if ("updateqrtattribute".equals(p_actionid)) {
			l_appresponse			=	null;
			l_appresponse			=	new PortalAppResponse("qrtemplateview.jsp");
			
			int	l_templateseqid		=	WebUtils.getRequestIntParameter(
											p_request
										, 	QR_REQUEST_PARAMETER_TEMPLATE_SEQID
										, 	p_multipartcontentrequest
										);
			UploadedFileWrapper oUploadedFileObject = null;
			String sDownloadFlag = null;
			l_correctionallowed			=	WebUtils.getRequestIntParameter(p_request, QR_REQUEST_RVWALLOWEDTOCORRECT, p_multipartcontentrequest);
			if (WebUtils.isRequestParameterAvailable(p_request, QR_REQUEST_CASEQUALITYTHRESHOLD, p_multipartcontentrequest)) {
				l_casequalitythreshold		=	Double.valueOf(WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEQUALITYTHRESHOLD, p_multipartcontentrequest));
			} else {
				l_casequalitythreshold		=	-1;
			}
			
			if (WebUtils.isRequestParameterAvailable(p_request, QR_REQUEST_CAT1QUALITYTHRESHOLD, p_multipartcontentrequest)) {
				l_cat1qualitythreshold		=	Double.valueOf(WebUtils.getRequestParameter(p_request, QR_REQUEST_CAT1QUALITYTHRESHOLD, p_multipartcontentrequest));
			} else {
				l_cat1qualitythreshold		=	-1;
			}
			if (WebUtils.isRequestParameterAvailable(p_request, QR_REQUEST_CAT2QUALITYTHRESHOLD, p_multipartcontentrequest)) {
				l_cat2qualitythreshold		=	Double.valueOf(WebUtils.getRequestParameter(p_request, QR_REQUEST_CAT2QUALITYTHRESHOLD, p_multipartcontentrequest));
			} else {
				l_cat2qualitythreshold		=	-1;
			}
			if (WebUtils.isRequestParameterAvailable(p_request, QR_REQUEST_CAT3QUALITYTHRESHOLD, p_multipartcontentrequest)) {
				l_cat3qualitythreshold		=	Double.valueOf(WebUtils.getRequestParameter(p_request, QR_REQUEST_CAT3QUALITYTHRESHOLD, p_multipartcontentrequest));
			} else {
				l_cat3qualitythreshold		=	-1;
			}
			
			l_caseclassificationoverall	=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASECLASSIFICATIONOVERALL, p_multipartcontentrequest);
			l_caseclassificationcat1	=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASECLASSIFICATIONCAT1, p_multipartcontentrequest);
			l_caseclassificationcat2	=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASECLASSIFICATIONCAT2, p_multipartcontentrequest);
			l_caseclassificationcat3	=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASECLASSIFICATIONCAT3, p_multipartcontentrequest);
			l_casecriteriamode			=	WebUtils.getRequestIntParameter(p_request, "fldcriteriamode", p_multipartcontentrequest);
			l_casecriteria				=	WebUtils.getRequestParameter(p_request, "casecriteria", p_multipartcontentrequest);	
			l_reviewsummrequired		=	WebUtils.getRequestIntParameter(p_request, "fldreviewsummrequired", p_multipartcontentrequest);
			
			try {
				sDownloadFlag = ConfigHome.getPrjConfigSettings(p_portalapp.getPortalAppId()+StringConstants.PERIOD+QRConstant.DOWNLOAD_ALLOWED,WebUtils.getAuthorizedProjectId(p_request));
				if(sDownloadFlag!=null && sDownloadFlag.equals("true")) {
					oUploadedFileObject = WebUtils.stageRequestParameterFile(p_request, QR_REVIEW_DWNLD_TMP_FILE, p_multipartcontentrequest);
				}
					
				l_qrtportalapp.updateQRTAttributes(
					l_projectid	
				,	l_templateseqid
				,	l_correctionallowed
				,	l_casequalitythreshold
				,	l_cat1qualitythreshold
				,	l_cat2qualitythreshold
				,	l_cat3qualitythreshold
				,	l_caseclassificationoverall
				,	l_caseclassificationcat1
				,	l_caseclassificationcat2
				,	l_caseclassificationcat3
				,	l_casecriteriamode
				,	l_casecriteria
				,	l_reviewsummrequired
				,	p_usersession	
				,	oUploadedFileObject
				);
				l_qrtemplate	=	l_qrtportalapp.getQRTDetails(l_templateseqid);
				setCommonRequestAttributes(
					p_request
				,	l_qrtemplate
				,	l_qrtportalapp
				);
			} 
			catch(SciWebException webExcep)
			{
				l_appresponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_NOT_AVAILABLE);
				return l_appresponse;
			}
			catch (SciServiceException serExcep) {
				try {
					l_qrtemplate	=	l_qrtportalapp.getQRTDetails(l_templateseqid);
					setCommonRequestAttributes(
						p_request
					,	l_qrtemplate
					,	l_qrtportalapp
					);
				} catch (SciServiceException serExcep1) {
					l_appresponse.logError(serExcep1);
					return l_appresponse;
				}
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			
		} else if ("updateqrtcasefield".equals(p_actionid)) {
			l_appresponse			=	null;
			l_appresponse			=	new PortalAppResponse("qrtemplateview.jsp");
			
			int	l_templateseqid		=	WebUtils.getRequestIntParameter(
											p_request
										, 	QR_REQUEST_PARAMETER_TEMPLATE_SEQID
										, 	p_multipartcontentrequest
										);
			
			l_casefieldmapping		=	WebUtils.getRequestParameter(p_request, QR_REQUEST_CASEFIELDMAPPING, p_multipartcontentrequest);
			try {
				l_qrtportalapp.updateReviewTypeMappingData(
					l_projectid
				,	l_templateseqid
				, 	l_casefieldmapping
				,	p_usersession
				);
				l_qrtemplate	=	l_qrtportalapp.getQRTDetails(l_templateseqid);
				setCommonRequestAttributes(
					p_request
				,	l_qrtemplate
				,	l_qrtportalapp
				);
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
			
		} else if ("gettemplatedetails".equals(p_actionid)) {
			int	l_templateseqid	=	WebUtils.getRequestIntParameter(
										p_request
									, 	QR_REQUEST_PARAMETER_TEMPLATE_SEQID
									, 	p_multipartcontentrequest
									);
			
			l_appresponse		=	null;
			if (!StringUtils.isNullOrEmpty(l_mode)) {
				if ("view".equals(l_mode)) {
					l_appresponse		=	new PortalAppResponse("qrtemplateview.jsp");
				} else if ("editattribute".equals(l_mode)) {
					l_appresponse		=	new PortalAppResponse("qrtemplateeditattributes.jsp");
				} else  if ("editreviewfield".equals(l_mode)) {
					l_appresponse		=	new PortalAppResponse("qrtemplateeditreviewfields.jsp");
				}
			}

			try {
				l_qrtemplate	=	l_qrtportalapp.getQRTDetails(l_templateseqid);
				setCommonRequestAttributes(
					p_request
				,	l_qrtemplate
				,	l_qrtportalapp
				);
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				return l_appresponse;
			}
		} else if ("updatetemplatestate".equals(p_actionid)) {
			int	l_templateseqid		=	WebUtils.getRequestIntParameter(
											p_request
										, 	QR_REQUEST_PARAMETER_TEMPLATE_SEQID
										, 	p_multipartcontentrequest
										);
			int	l_templatenewstate	=	WebUtils.getRequestIntParameter(
											p_request
										, 	QR_REQUEST_PARAMETER_TEMPLATE_NEWSTATE
										, 	p_multipartcontentrequest
										);
			
			l_appresponse	=	null;
			l_appresponse	=	new PortalAppResponse("qrtemplateview.jsp");
			try {
				l_qrtemplate	=	l_qrtportalapp.updateTemplateState(
										p_usersession
									, 	RecordState.toEnum(l_templatenewstate)
									, 	l_templateseqid
									,	l_projectid
									);
				setCommonRequestAttributes(
					p_request
				,	l_qrtemplate
				,	l_qrtportalapp
				);
			} catch (SciServiceException serExcep) {
				l_appresponse.logError(serExcep);
				try {
					setCommonRequestAttributes(
						p_request
					,	l_qrtportalapp.getQRTDetails(l_templateseqid)
					,	l_qrtportalapp
					);
				} catch (SciServiceException serExcep1) {
					//No need to log or throw error
				}
				return l_appresponse;
			}
		}
		return l_appresponse;
	}
	
	/**
	 * This is used to set all the common request attribute required by
	 * all the actions.
	 * @param p_request			Request object.
	 * @param p_qrtemplate		The template whose data needs to be shown.
	 * @param p_qrtportalapp	The object of portal app.
	 * @throws SciServiceException
	 */
	public void setCommonRequestAttributes (
		HttpServletRequest	p_request	
	,	QRTemplate			p_qrtemplate
	,	QRPortalApp			p_qrtportalapp		
	) throws SciServiceException {
		WebUtils.setRequestAttribute(
			p_request
		, 	"fldtemplatedata"
		, 	p_qrtemplate
		);
		try {
				WebUtils.setRequestAttribute(
					p_request
				, 	"safetydbcasefields"
				, 	SafetyDbConfigurationHelper.getTemplateDetails(
						p_qrtemplate.getSCFTSeqId()
					)
				);
		} catch (SciServiceException serExcep) {
			log.error("Error updating template ruleset state", serExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_UPDATING_TEMPLATE_STATE, serExcep);
		}
		
	}
	
	public PortalAppResponse prepareQRLPage (
		HttpServletRequest							p_request
	,	QRPortalApp									p_qrtportalapp
	,	int											p_projectid
	,	UserSession 								p_usersession
	,	String										p_caseid
	,	String										p_caseversion
	,	String										p_reviewtype
	,	PortalAppResponse							p_appresponseerror
	,	String										p_mode
	,	boolean 									p_newRoundFlag
	) {
		PvcpCaseMaster 								l_pvcpcasemaster 			=	null;
		Map<String,String>							l_errortypes				=	null;
		PortalAppResponse							l_appresponse				=	null;
		String										l_internalid				=	null;
		List<QRReviewData> 							l_reviewdatalist			= 	null;
		QRTemplate									l_qrtemplate				=	null;
		Map<String, ArrayList<SafetyDbCaseFields>>	l_mapqualityreviewtabdata	=	null;
		List<UserInfo>								l_listuser					=	null;
		List<ObjectIdPair<String,String>>			l_reviewtypelist			=	null;
		
		WebUtils.setRequestAttribute(
			p_request
		, 	"loggedinuser"
		, 	p_usersession.getUserInfo()
		);
		l_reviewtypelist	=	p_qrtportalapp.getReviewTypeUserList(p_projectid,p_usersession);
		try {
			WebUtils.setRequestAttribute(
				p_request
			, 	"UserLayoutType"
			, 	p_qrtportalapp.getUserLayoutType(
					p_usersession.getUserInfo()	,
					p_projectid
				)
			);
		} catch (SciServiceException serExcep) {
			log.error(
				"Exception retrieving review type from db" , serExcep
			);
			p_appresponseerror.logError(serExcep);
			return p_appresponseerror;
		}
		try {
			l_internalid		=	p_caseid + "_" + p_caseversion;
			l_pvcpcasemaster	=	p_qrtportalapp.getPVCPCaseMasterByInternalCaseId(l_internalid , p_projectid);
		} catch (SciServiceException serExcep) {
			log.error("Exception retrieving pcvp case details" , serExcep	);
			p_appresponseerror.logError(serExcep);
			prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
			return p_appresponseerror;
		}
		
		if (l_pvcpcasemaster !=null) {
			
			WebUtils.setRequestAttribute(
				p_request
			, 	QR_REQUEST_REVIEWTYPE
			, 	p_reviewtype
			);
			
			try {
				l_reviewdatalist	=	p_qrtportalapp.getQRLReviewTypeRecordDetails(l_pvcpcasemaster, p_reviewtype);
			} catch(SciServiceException sciserexp) {
				log.error("Error fetching quality review data by case master");
				p_appresponseerror.logError(sciserexp);
				prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
				return p_appresponseerror;
			}
			if(l_reviewdatalist != null && !l_reviewdatalist.isEmpty() && p_newRoundFlag )
			{
				int i = l_reviewdatalist.get(l_reviewdatalist.size()-1).getReviewRound();
				WebUtils.setRequestAttribute(p_request,"reviewround" , i );
			}
			else
			{
				WebUtils.setRequestAttribute(p_request,"reviewround" , 0);
			}
			if(l_reviewdatalist != null && !l_reviewdatalist.isEmpty() &&!p_newRoundFlag) {
				WebUtils.setRequestAttribute(p_request, "qrdatalist", l_reviewdatalist);
				WebUtils.setRequestAttribute(p_request, "reviewtypelist",l_reviewtypelist);
				l_appresponse		=	new PortalAppResponse("qrlhistorydata.jsp");
			} else {
				if ("error".equals(p_mode)) {
					l_appresponse		=	p_appresponseerror;
				} else {
					l_appresponse		=	null;
					l_appresponse		=	new PortalAppResponse("ilqr.jsp");
				}

				try {
					l_errortypes	=	p_qrtportalapp.getErrorTypes(p_projectid);
					if (l_errortypes != null && l_errortypes.size()>0) {
						WebUtils.setRequestAttribute(
							p_request
						, 	"errortypes"
						, 	l_errortypes
						);
					}
				} catch (SciServiceException serExcep) {
					p_appresponseerror.logError(
						WebErrorCodes.QR.ERROR_MISSING_ERROR_TYPE
					);
					prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
					return p_appresponseerror;
				}

				WebUtils.setRequestAttribute(p_request, "pvcp_case_record", l_pvcpcasemaster);
				try {
					WebUtils.setRequestAttribute(
						p_request
					, 	"UserLayoutType"
					, 	p_qrtportalapp.getUserLayoutType(
							p_usersession.getUserInfo()
							,p_projectid
						)
					);
				} catch (SciServiceException serExcep) {
					log.error(
						"Exception retrieving review type from db" , serExcep
					);
					p_appresponseerror.logError(serExcep);
					prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
					return p_appresponseerror;
				}
				try {
					l_qrtemplate	=	p_qrtportalapp.getActiveQRLTemplateDetails(p_projectid, p_reviewtype, p_usersession);
					l_mapqualityreviewtabdata = p_qrtportalapp.getReviewFieldList(l_qrtemplate);
					WebUtils.setRequestAttribute(p_request, "qrttemplate", l_qrtemplate);
					WebUtils.setRequestAttribute(p_request, "fldsafetydbcasefields", l_mapqualityreviewtabdata);
				} catch (SciServiceException serExcep) {
					log.error("Exception retrieving review type from db" , serExcep);
					p_appresponseerror.logError(serExcep);
					prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
					return p_appresponseerror;
				}
				try {
					WebUtils.setRequestAttribute(
						p_request
					, 	"fldactivesafetydbtemplate"
					,	SafetyDbConfigurationHelper.getTemplateDetails(l_qrtemplate.getSCFTSeqId())
					);
				} catch (SciServiceException serExcep) {
					log.error("Error fetching active safety db case field template", serExcep);
					p_appresponseerror.logError(serExcep);
					prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
					return p_appresponseerror;
				}
				try {
					l_listuser	=	ProjectUserManagementHome.getProjectUserList(p_projectid);
					l_listuser.remove(UserHome.retrieveBootstrapUser());
					l_listuser.remove(p_usersession.getUserInfo());
					WebUtils.setRequestAttribute(
						p_request
					,	"list_of_users"
					, 	l_listuser
					);
				} catch (SciServiceException serExcep) {
					log.error("Error retrieving list of users", serExcep);
					p_appresponseerror.logError(serExcep);
					prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
					return p_appresponseerror;
				}
			
			}
		} else {
			p_appresponseerror.logError(
			WebErrorCodes.QR.ERROR_EMPTY_CASE_RECORDS);
			prepareQRSearchPage(p_request, p_qrtportalapp, p_projectid,p_appresponseerror, p_usersession);
			return p_appresponseerror;
		}
		WebUtils.setRequestAttribute(
			p_request
		, 	"ismrnameshow"
		, 	p_qrtportalapp.isMRNameEnabledForProject(
				p_projectid
			)
		);
		WebUtils.setRequestAttribute(
			p_request
		, 	"categoryaliases"
		, 	p_qrtportalapp.getApplicableCategories(
				p_projectid
			)
		);
		return l_appresponse;
	}
	
	/**
	 * 
	 * @param p_fullcontent
	 * @param p_appendcontent
	 */
	public void appendContent (
		StringBuilder	p_fullcontent
	,	String ... 		p_appendcontent	
	) {
		for (String p_content : p_appendcontent) {
			p_fullcontent.append(p_content+",");
		}
		p_fullcontent.deleteCharAt(p_fullcontent.length()-1);
		p_fullcontent.append(System.lineSeparator());
	}
	
	public boolean prepareQRSearchPage (
		HttpServletRequest					p_request
	,	QRPortalApp							p_qrtportalapp
	,	int									p_projectid
	,	PortalAppResponse					p_appresponseerror
	,	UserSession							p_usersession
	) {
		String								l_reviewtype		=	null;
		List<ObjectIdPair<String,String>>	l_reviewtypelist	=	null;
		SafetyDbCaseFieldsTemplate			l_activescft		=	null;
		try {
			l_reviewtypelist	=	p_qrtportalapp.getReviewTypeUserList(p_projectid,p_usersession);
			if (l_reviewtypelist != null && l_reviewtypelist.size() > 0) {
				WebUtils.setRequestAttribute(
					p_request
				, 	"reviewtypelist"
				, 	l_reviewtypelist
				);
			} else {
				p_appresponseerror.logError(
					WebErrorCodes.QR.REVIEW_TYPE_LIST_NOT_AVAILABLE
				);
				return false;
			}
			l_reviewtype		=	p_qrtportalapp.getUserReviewType(p_usersession.getUserInfo(), p_projectid);
			if (StringUtils.isNullOrEmpty(l_reviewtype)) {
				p_appresponseerror.logError(
					WebErrorCodes.QR.REVIEW_TYPE_NOT_AVAILABLE
				);
				return false;
			} else {
				WebUtils.setRequestAttribute(
					p_request
				, 	"userreviewtype"
				, 	l_reviewtype
				);
			}
		} catch (SciServiceException serExcep) {
			log.error("Review Type is not selected by the user" , serExcep);
			p_appresponseerror.logError(serExcep);
			return false;
		}
		try {
			l_activescft	=	SafetyDbConfigurationHelper.getActiveTemplateDetails(p_projectid);
			if (l_activescft != null) {
				WebUtils.setRequestAttribute(
					p_request
				, 	"fldactivesafetydbtemplate"
				, 	l_activescft
				);
			} else {
				p_appresponseerror.logError(WebErrorCodes.QR.ERROR_SAFETYCASEFILE_TEMPLATE_NOTACTIVATED);
				return false;
			}
		} catch (SciServiceException serExcep) {
			log.error("Active Tempalte for Safety DB Case Field is not defined", serExcep);
			p_appresponseerror.logError(serExcep);
			return false;
		}
		return true;
	}
	
	/**
	 * This is to set the udf fields defined
	 * for the case master.
	 * @param p_request		HTTP Request
	 * @param p_qrportalapp	Portal App Object	
	 * @param p_projectid	Project Id
	 * @param p_pvcpcasemaster Case Master Object
	 * @param p_multipartcontentrequest
	 */
	public void setUDFFields (
		HttpServletRequest				p_request
	,	QRPortalApp						p_qrportalapp
	,	int								p_projectid
	,	PvcpCaseMaster 					p_pvcpcasemaster
	,	MultipartContentRequestWrapper	p_multipartcontentrequest
	) {
		String	l_temp	=	null;
		for(String l_udfname : p_qrportalapp.getEnableUdfList(p_projectid).keySet()) {
			l_temp	=	WebUtils.getRequestParameter(p_request, QRConstant.REQUEST_PARAM_UDF_PREFIX+l_udfname, p_multipartcontentrequest);	
			
			switch (l_udfname) {
				case QRConstant.CASE_MASTER_INTERNAL_UDF1 :
					p_pvcpcasemaster.setUdf1(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF2 :
					p_pvcpcasemaster.setUdf2(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF3 :
					p_pvcpcasemaster.setUdf3(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF4 :
					p_pvcpcasemaster.setUdf4(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF5 :
					p_pvcpcasemaster.setUdf5(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF6 :
					p_pvcpcasemaster.setUdf6(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF7 :
					p_pvcpcasemaster.setUdf7(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF8 :
					p_pvcpcasemaster.setUdf8(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF9 :
					p_pvcpcasemaster.setUdf9(l_temp);
				break;
				case QRConstant.CASE_MASTER_INTERNAL_UDF10 :
					p_pvcpcasemaster.setUdf10(l_temp);
				break;
			}
		}
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}
}
