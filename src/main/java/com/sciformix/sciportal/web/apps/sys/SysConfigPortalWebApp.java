package com.sciformix.sciportal.web.apps.sys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.PublicKey;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.commons.utils.ValidationUtils;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.sys.SysConfigPortalApp;
import com.sciformix.sciportal.apps.sys.config.EncryptionKey;
import com.sciformix.sciportal.apps.sys.config.SystemConfigConstants;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.integration.ServerConnection;
import com.sciformix.sciportal.integration.ServerConnectionHome;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;

public class SysConfigPortalWebApp  extends BasePortalWebApp
{
	private static final Logger log = LoggerFactory.getLogger(SysConfigPortalWebApp.class);
	
	public SysConfigPortalWebApp()
	{
		super("SYSCONFIG-PORTAL-APP", "system_configuration.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
	}
	
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) throws SciServiceException
	{
		String nextPageUri = null;
		PortalAppResponse portalAppResponce = null;
		PortalAppResponse portalAppErrorResponce = null;
		List<ServerConnection> connectionList = null;
		String errorPageUri = null;
		String keyType = null;
		String keyPurpose = null;
		String keyId = null;
		File l_exportfile =	null;
		
		FileOutputStream fos =	null;
		PublicKey publicKey = null;
		
		FileInputStream	l_fis =	null;
		PrintWriter		l_pwr			=	null;
		Set<String> keySet = null;
		Map<String, String> encrKeyMap = null;
		SysConfigPortalApp portalApp = (SysConfigPortalApp)oPortalApp;
		encrKeyMap = (Map<String, String>)ConfigHome.getSysConfigSettings(SystemConfigConstants.APP_NAME_KEY);
		
		connectionList = ServerConnectionHome.getAllConnections();
		keySet = new LinkedHashSet<String>();
		
		if(encrKeyMap != null){
		 for (String key : encrKeyMap.keySet()){
			 if(key.contains(SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_ID))
			 {
				 keySet.add(encrKeyMap.get(key));
			 }
		 }
		}
		WebUtils.setRequestAttribute(request, "activeTab", WebUtils.getRequestParameter(request, "active_tab", oMultipartContentRequest));
		WebUtils.setRequestAttribute(request, "systemInfo", portalApp.getSystemInfo());
		WebUtils.setRequestAttribute(request, "serverConnectionList", connectionList);
		WebUtils.setRequestAttribute(request, "keySet", keySet);
	
		if(StringUtils.isNullOrEmpty(p_sActionId))
		{
			nextPageUri = getDefaultPageUri();
			portalAppResponce = new PortalAppResponse(nextPageUri);
		}
		else
		{	/*if(p_sActionId.equalsIgnoreCase("create"))
			{
				SysConfigPortalApp.createServerConnection(ServerConnection.STATE.valueOf(request.getParameter("add_status")),
							ServerConnection.TYPE.valueOf(request.getParameter("add_type")),
							request.getParameter("add_Alias"), new String[2]);
			}
			else if(p_sActionId.equalsIgnoreCase("edit"))
			{
				String conn_Id =request.getParameter("edit_conn_id");
				ServerConnectionHome.updateServerConnection( conn_Id, ServerConnection.STATE.valueOf(request.getParameter("edit_status"+conn_Id)), request.getParameter("alias"+conn_Id), new String[2]);
				
			}
			else */
			if(p_sActionId.equalsIgnoreCase("createencrkey"))
			{
				nextPageUri = "createEncryptionKey.jsp";
				portalAppResponce = new PortalAppResponse(nextPageUri);
			}
			else if(p_sActionId.equalsIgnoreCase("saveencryptionkey"))
			{
				String msg = null;
				String keyIdRegex = null;
				nextPageUri =  getDefaultPageUri(); 
				errorPageUri = "createEncryptionKey.jsp";
				portalAppResponce = new PortalAppResponse(nextPageUri);
				portalAppErrorResponce = new PortalAppResponse(errorPageUri);
				EncryptionKey encryptionKey = null;
				keyIdRegex = "^[A-Za-z0-9]*$";
				keyType = WebUtils.getRequestParameter(request, "encrType", oMultipartContentRequest);
				keyPurpose= WebUtils.getRequestParameter(request, "encrPurpose", oMultipartContentRequest);
				keyId = WebUtils.getRequestParameter(request, "keyId", oMultipartContentRequest);
				
				
				encryptionKey = new EncryptionKey();
				
				encryptionKey.setKeyId(keyId);
				//encryptionKey.setKeyName(keyName);
				encryptionKey.setKeyPurpose(keyPurpose);
				encryptionKey.setKeyType(keyType);
				
				if(StringUtils.isNullOrEmpty(keyType))
				{
					log.error("Key Type not selected");
					portalAppErrorResponce.logError(WebErrorCodes.SystemConfig.ERROR_KEY_TYPE_EMPTY);
					return portalAppErrorResponce;
				}
				else if(StringUtils.isNullOrEmpty(keyPurpose))
				{
					log.error("Key Purpose not selected");
					portalAppErrorResponce.logError(WebErrorCodes.SystemConfig.ERROR_KEY_PURPOSE_EMPTY);
					return portalAppErrorResponce;
				}
				else if(!ValidationUtils.validateField(keyId, keyIdRegex))
				{
					log.error("Key Id Invalid");
					portalAppErrorResponce.logError(WebErrorCodes.SystemConfig.ERROR_INVALID_KEY_ID);
					return portalAppErrorResponce;
				}
				else if(SysConfigPortalApp.getPrivateKey(keyId) != null)
				{
					log.error("Key Id already exist");
					portalAppErrorResponce.logError(WebErrorCodes.SystemConfig.ERROR_KEY_ID_EXIST);
					return portalAppErrorResponce;
				}
				try{
					portalApp.generateEncryptionKey( p_oUserSession, encryptionKey);
				}
				catch(SciServiceException sciSerExcp)
				{
					log.error("Error encryption generation");
					portalAppErrorResponce.logError(sciSerExcp);
					return portalAppErrorResponce;
				}
				msg = "Encryption key "+ keyId+ " generated successfully, Please restart the server.";
				
				WebUtils.setRequestAttribute(request, "successmessage", msg);
				
			}
			else if(p_sActionId.equalsIgnoreCase("exportKey"))
			{
				nextPageUri =  getDefaultPageUri(); 
				portalAppResponce = new PortalAppResponse(nextPageUri);
				keyId = WebUtils.getRequestParameter(request, "export_key", oMultipartContentRequest);
				
				try{
					l_exportfile = new File(WebUtils.createStagingFile(WebUtils.getUserSession(request), keyId+".key"));
					
					l_exportfile.getParentFile().mkdirs();
					publicKey = SysConfigPortalApp.getPublicKey(keyId);
					fos = new FileOutputStream(l_exportfile);
					fos.write(publicKey.getEncoded());
					fos.close();
					
					l_pwr = response.getWriter();  
					response.setContentType("text/plain");  
					response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
					l_fis = new FileInputStream(l_exportfile);  
		            
					int i;   
					while ((i = l_fis.read()) != -1) {  
						l_pwr.write(i);   
					}
				 }
				 catch (IOException | SciServiceException sciSerExp) {
					 log.error("Error in export key");
					 portalAppResponce.logError(WebErrorCodes.SystemConfig.ERROR_EXPORT_KEY);
				 }
				 finally {
					try {
						portalAppResponce.setIsDownloadStreamPresent(true);
						response.flushBuffer();
					    
						if(l_pwr != null) {
							l_pwr.close();
					   	}
					   	
					    if(l_fis != null){
					    	l_fis.close();
					   	}
					} catch (IOException ioExcep) {
						log.error("Error export key", ioExcep);
						portalAppResponce.logError(WebErrorCodes.SystemConfig.ERROR_EXPORT_KEY);
						return portalAppResponce;
					}
				}
	
			}
		}
		return portalAppResponce;
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}
}
