package com.sciformix.sciportal.web.apps.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.sciportal.apps.AppInfo;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.sys.AppSettingsPortalApp;
import com.sciformix.sciportal.config.ConfigMetadata;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;

public class AppSettingsPortalWebApp extends BasePortalWebApp
{
	private static final Logger log = LoggerFactory.getLogger(AppSettingsPortalWebApp.class);
	
	public AppSettingsPortalWebApp()
	{
		super("APPSETTINGS-PORTAL-APP", "appsettings.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
	}
	
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp)
	{
		String sNewAction = null;
		PortalAppResponse oAppResponse = null;
		AppSettingsPortalApp oMyPortalApp = null;
		Map<String, String> mapAppConfig = null;
		Map<String, String> mapAppConfigAll = null;
		ConfigMetadata oAppConfigMetadata = null;
		String sNewValue = null;
		String sExistingValue = null;
		@SuppressWarnings("unused")
		String sValueDatatype = null;
		Map<String, String> mapKeysToBeUpdated = null;
		String sAppId = null;
		List<AppInfo> listApps = null;
		
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		
		oMyPortalApp = (AppSettingsPortalApp) oPortalApp;
		
		log.debug("AppSettingsPortalWebApp.processAction: Starting processing for Action - {}", p_sActionId);
		
		if (p_sActionId == null)
		{
			//Nothing to do
		}
		else if (p_sActionId.equalsIgnoreCase("appsettings_step2"))
		{
			mapAppConfigAll = new HashMap<String, String>();
			mapKeysToBeUpdated = new HashMap<String, String>();
			
			mapAppConfig = AppRegistry.getGeneralConfigSettings();
			if (mapAppConfig != null)
			{
				mapAppConfigAll.putAll(mapAppConfig);
			}
			
			listApps = AppRegistry.listBusinessApps();
			listApps.addAll(AppRegistry.listSystemApps());
			for(AppInfo oAppInfo : listApps)
			{
				oPortalApp = AppRegistry.getApp(oAppInfo.getAppId());
				
				mapAppConfig = oPortalApp.getAppConfigSettings();
				if (mapAppConfig != null)
				{
					mapAppConfigAll.putAll(mapAppConfig);
				}
			}
			
			for (String sKey : mapAppConfigAll.keySet())
			{
				oAppConfigMetadata = AppRegistry.getAppConfigMetadata(sKey);
				sValueDatatype = (oAppConfigMetadata != null ? oAppConfigMetadata.getValueDatatype() : "STRING");
				
				sNewValue = WebUtils.getRequestParameter(request, "__appSettings"+sKey, oMultipartContentRequest);
				sExistingValue = mapAppConfigAll.get(sKey);
				
				if ((sNewValue == null && sExistingValue == null) || (sNewValue!=null ? sNewValue.equals(sExistingValue) : false))
				{
					//No change
					if(log.isTraceEnabled())
					{
						log.trace("AppSettingsPortalWebApp.processAction: Value unchanged for key - {}", sKey);
					}
				}
				else
				{
					//TODO:Validate the data
					//Mark for update cycle
					mapKeysToBeUpdated.put(sKey, sNewValue);
				}
			}
			
			//If no errors, then...
			for (String sKeyToUpdate : mapKeysToBeUpdated.keySet())
			{
				//TODO:
				sNewValue = mapKeysToBeUpdated.get(sKeyToUpdate);
				try
				{
					//TODO:extract - sAppId
					oMyPortalApp.updateAppConfig(p_oUserSession, sAppId, sKeyToUpdate, sNewValue);
					log.debug("AppSettingsPortalWebApp.processAction: Value UDPATED for - {}; Old Value - {}; New Value - {} ", sKeyToUpdate, sExistingValue, sNewValue);
				}
				catch (SciServiceException serExcep)
				{
					log.error("AppSettingsPortalWebApp.processAction: Error updating value for key - {}", sKeyToUpdate, serExcep);
					oAppResponse.logError(serExcep);
					return oAppResponse;
				}
			}
			sNewAction = "";
		}
		/*else if (p_sActionId.equalsIgnoreCase("refreshsysconfig"))
		{
			sNewAction = "";
			try {
			oMyPortalApp.refreshConfigSettings();
			}
			catch(SciServiceException sciSerExc)
			{
				log.error("Error Refresh System Configuration");
				oAppResponse.logError(sciSerExc);
			}
		}*/
		else
		{
			sNewAction = null;
			log.error("AppSettingsPortalWebApp.processAction: Unsupported Action - {}", p_sActionId);
			oAppResponse.logError(WebErrorCodes.System.INVALID_ACTION, null);
			return oAppResponse;
		}
		
		log.debug("AppSettingsPortalWebApp.processAction: Completed processing for Action - {}", p_sActionId);
		
		oAppResponse.setNextAction(sNewAction);
		return oAppResponse;
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}
}
