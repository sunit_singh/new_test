package com.sciformix.sciportal.web.apps;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;

public interface IPortalWebApp
{
	public static enum ProjectApplicability
	{
		PROJECT_INDEPEDENT, SINGLE_PROJECT, MULTIPLE_PROJECTS;
	}
	
	public String getPortalAppId();
	
	public String getDefaultPageUri();
	
	public PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
							MultipartContentRequestWrapper oMultipartContentRequest, 
							String p_sActionId);
	
	public PortalAppResponse processProjectSettingsAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp);
}
