package com.sciformix.sciportal.web.apps.user;

import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.user.UserManagementPortalApp;
import com.sciformix.sciportal.user.UserAuthHome;
import com.sciformix.sciportal.user.UserAuthHome.AuthType;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserInfo.UserAddonRole;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;

public class UserManagementWebApp extends BasePortalWebApp {

	private static final Logger log = LoggerFactory.getLogger(UserManagementWebApp.class);

	public UserManagementWebApp() {
		super("USRMGMNT-PORTAL-APP", "usermanagement.jsp",ProjectApplicability.PROJECT_INDEPEDENT);
	}

	@Override
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response,
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession,
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) throws SciServiceException {

		String sNewAction = null;
		PortalAppResponse oAppResponse = null;
		String m_sUserId = null;
		String m_sUserEmail = null;
		String m_sUserDisplayName = null;
		AuthType m_eAuthType;
		String userId = null;
		List<UserInfo> userList = null;
		String sProjectAdminFlag = null;
		String auditor = null;
		String systemAdmin = null;
		String itServiceDesk = null;
		String projectAuditor = null;
		UserManagementPortalApp oMyPortalApp = (UserManagementPortalApp) oPortalApp;

		log.debug("UserManagementWebApp.processAction: Starting processing for Action - {} ", p_sActionId);

		if (p_sActionId == null || p_sActionId.equals("back")) {
			userList = oMyPortalApp.getListOfUsers();

			WebUtils.setRequestAttribute(request, "userList", userList);

			oAppResponse = new PortalAppResponse("usermanagement.jsp");

		} else if (p_sActionId.equals("create")) {
			log.debug("UserManagementWebApp.processAction: Display Blank Create User Form");
			oAppResponse = new PortalAppResponse("usermanagementcreate.jsp");
			UserInfo userInfo =	WebUtils.getUserSession(request).getUserInfo();
			WebUtils.setRequestAttribute(request, "userInfoSession", userInfo);

		} else if (p_sActionId.equals("createuser") || p_sActionId.equals("updateuser")) {
			
			
			m_sUserId =  WebUtils.getRequestParameter(request, "userId", oMultipartContentRequest).trim();
			m_sUserEmail = WebUtils.getRequestParameter(request, "emailId", oMultipartContentRequest).trim();
			m_sUserDisplayName = WebUtils.getRequestParameter(request, "userDisplayName", oMultipartContentRequest).trim();
			m_eAuthType = UserAuthHome.AuthType.toEnum(2);
			
			oAppResponse = new PortalAppResponse("usermanagementcreate.jsp");
			
			if(StringUtils.isNullOrEmpty(m_sUserId))
			{
				oAppResponse.logError(WebErrorCodes.UserManagement.ERROR_USER_ID_EMPTY);
				return oAppResponse;
			}
			else if(StringUtils.isNullOrEmpty(m_sUserDisplayName))
			{
				oAppResponse.logError(WebErrorCodes.UserManagement.ERROR_USER_NAME_EMPTY);
				return oAppResponse;
			}
			else if(StringUtils.isNullOrEmpty(m_sUserEmail))
			{
				oAppResponse.logError(WebErrorCodes.UserManagement.ERROR_USER_EMAIL_EMPTY);
				return oAppResponse;
			}
	
				sProjectAdminFlag = WebUtils.getRequestParameter(request, "projectAdmin", oMultipartContentRequest);
				auditor = WebUtils.getRequestParameter(request, "auditor", oMultipartContentRequest);
				systemAdmin = WebUtils.getRequestParameter(request, "systemAdmin", oMultipartContentRequest);
				itServiceDesk = WebUtils.getRequestParameter(request, "itServiceDesk", oMultipartContentRequest);
				projectAuditor = WebUtils.getRequestParameter(request, "projectAuditor",oMultipartContentRequest);
	
				HashSet<UserAddonRole> setUserAddonRoles = new HashSet<>();
	
				if (sProjectAdminFlag!=null)
				{
					setUserAddonRoles.add(UserAddonRole.PROJECT_ADMIN);
				}
				if (auditor!=null)
				{
					setUserAddonRoles.add(UserAddonRole.AUDITOR);
				}
				if (systemAdmin!=null)
				{
					setUserAddonRoles.add(UserAddonRole.SYSTEM_ADMIN);
				}
				if (itServiceDesk!=null)
				{
					setUserAddonRoles.add(UserAddonRole.IT_SERVICE_DESK);
				}
				if (projectAuditor!=null)
				{
					setUserAddonRoles.add(UserAddonRole.PROJECT_AUDITOR);
				}
				if(p_sActionId.equals("createuser"))
				{
					userList = oMyPortalApp.getListOfUsers();
					//TODO: Can't we hit DB and check
					for( UserInfo user : userList)
					{
						
						if(user.getUserId().equalsIgnoreCase(m_sUserId))
						{
							oAppResponse.logError(WebErrorCodes.UserManagement.ERROR_USER_ID_EXIST);
							UserInfo userInfo =	WebUtils.getUserSession(request).getUserInfo();
							WebUtils.setRequestAttribute(request, "userInfoSession", userInfo);
							return oAppResponse;
						}
					}
					
						oMyPortalApp.createUser(p_oUserSession, m_sUserId, m_sUserDisplayName, m_sUserDisplayName, m_sUserEmail,
						m_eAuthType, setUserAddonRoles,true);					
						log.debug("UserManagementWebApp.processAction: User Created Successfully..");
						WebUtils.setRequestAttribute(request, "message", "User created successfully with User Id : " + m_sUserId);
						UserInfo userInfoSession =	WebUtils.getUserSession(request).getUserInfo();
						WebUtils.setRequestAttribute(request, "userInfoSession", userInfoSession);
						
						
				}
				else if(p_sActionId.equals("updateuser"))
				{
						UserInfo p_oUserInfo = UserHome.retrieveUser(m_sUserId, null);
						UserInfo temp_oUserInfo = UserHome.retrieveUser(m_sUserId, null);
						UserInfo userInfoSession =	WebUtils.getUserSession(request).getUserInfo();
						
						if(p_oUserInfo==null && userInfoSession==null)
						{
							oAppResponse.logError(WebErrorCodes.UserManagement.ERROR_USER_ID_EMPTY);
							oAppResponse = new PortalAppResponse("usermanagement.jsp");
							return oAppResponse;
						}
						
						//TODO: Check whether it is null or not -- done
						
						p_oUserInfo.setAddonUserRole(setUserAddonRoles);			
						
						if (DataUtils.hasChanged(p_oUserInfo.getUserType(), temp_oUserInfo.getUserType())||
								DataUtils.hasChanged(p_oUserInfo.getUserDisplayName(), m_sUserDisplayName)||
								DataUtils.hasChanged(p_oUserInfo.getUserShortName(), m_sUserDisplayName)||
								DataUtils.hasChanged(p_oUserInfo.getUserEmail(), m_sUserEmail)
								)
						{
							if(((!userInfoSession.isItServiceDeskUser())||userInfoSession.isSystemAdmin()))
							{
								oMyPortalApp.updateUserRole(p_oUserSession, p_oUserInfo, setUserAddonRoles);
							}
							oMyPortalApp.updateUserAttributes(p_oUserSession,p_oUserInfo, m_sUserDisplayName, m_sUserDisplayName, m_sUserEmail);
						
							log.debug("UserManagementWebApp.processAction: User Updated Successfully..");
							
							userList = oMyPortalApp.getListOfUsers();

							WebUtils.setRequestAttribute(request, "userList", userList);
							
							oAppResponse = new PortalAppResponse("usermanagement.jsp");
							
							WebUtils.setRequestAttribute(request, "message", "User updated successfully with User Id : " + m_sUserId);
							WebUtils.setRequestAttribute(request, "userInfoSession", userInfoSession);
							
						}else
						{
							UserInfo userInfo = UserHome.retrieveUser(m_sUserId, null);
							WebUtils.setRequestAttribute(request, "userInfo", userInfo);
							WebUtils.setRequestAttribute(request, "userInfoSession", userInfoSession);
							oAppResponse = new PortalAppResponse("usermanagementcreate.jsp");
							oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_UPDATE_VALUES_SAME_AS_EXISTING);
							return oAppResponse;
							
						}
				}

		} else if (p_sActionId.equals("edit")) {
			log.debug("UserManagementWebApp.processAction: Display user for Editing");

			userId = WebUtils.getRequestParameter(request, "select_id", oMultipartContentRequest);
			UserInfo userInfo = UserHome.retrieveUser(userId, null);

			WebUtils.setRequestAttribute(request, "userInfo", userInfo);
			UserInfo userInfoSession =	WebUtils.getUserSession(request).getUserInfo();
			WebUtils.setRequestAttribute(request, "userInfoSession", userInfoSession);

			oAppResponse = new PortalAppResponse("usermanagementcreate.jsp");

		} else if (p_sActionId.contains("view")) {
			log.debug("UserManagementWebApp.processAction: Display user ", request.getParameter("select_id"));

			userId = WebUtils.getRequestParameter(request, "select_id", oMultipartContentRequest);
			UserInfo userInfo = UserHome.retrieveUser(userId, null);

			WebUtils.setRequestAttribute(request, "viewFlag", "view");
			WebUtils.setRequestAttribute(request, "userInfo", userInfo);
			UserInfo userInfoSession =	WebUtils.getUserSession(request).getUserInfo();
			WebUtils.setRequestAttribute(request, "userInfoSession", userInfoSession);

			oAppResponse = new PortalAppResponse("usermanagementcreate.jsp");

		} else {
			// Do nothing
			log.error("UserManagementWebApp.processAction: Unsupported Action - {}", p_sActionId);

			oAppResponse.logError(WebErrorCodes.System.INVALID_ACTION, null);
			return oAppResponse;
		}

		log.debug("UserManagementWebApp.processAction: Completed processing for Action - {} ", p_sActionId);

		oAppResponse.setNextAction(sNewAction);
		return oAppResponse;
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}

}
