package com.sciformix.sciportal.web.apps.sourcedocutil;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.sourcedocutil.SourceDocUtilPortalApp;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.SystemUtils;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

//import test.XlsToXmlConverter;

public class SourceDocUtilPortalWebApp extends BasePortalWebApp
{
	private static final String CASEDATA_FILE_PARSE = "casedata_file_parse";
	private static final String FILE_FOR_HIGHLIGHT = "file_for_highlight";
	
	private static final Logger log = LoggerFactory.getLogger(SourceDocUtilPortalWebApp.class);
	public SourceDocUtilPortalWebApp()
	{
		//super("TOOLS-PORTAL-APP", "tools.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
		super("SOURCE_DOC_UTILS_PORTAL_APP", "sourcedocutillandingpage.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
	}
	public static final String CASEDATA_FILE = "casedata_file";
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response,
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession,
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) 
	{
		
		PortalAppResponse oAppResponse = null;
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
	//	oAppResponse = new PortalAppResponse("tools.jsp");
		List<UploadedFileWrapper> listUploadedFileObject = null;
		SourceDocUtilPortalApp oMyPortalApp = null;
		String sZipFilePath = null;
		String sNewAction = null;
		String userSeqId="";
		UserSession oUserSession = null;
		String sDocSplitterConfig = null;
		String fileNameString = null;
		String activeTabFlag = null;
		String sdParserFileTypeList=null;
		String nextPageUri = null;
	//	String patternString=null;
		String annotationStyleList=null;
		oMyPortalApp = (SourceDocUtilPortalApp) oPortalApp;
		sDocSplitterConfig = oMyPortalApp.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.SOURCE_DOC_SPLITTER);
		sdParserFileTypeList = oMyPortalApp.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.SDPARSER_FILETYPE_LIST);
		annotationStyleList = oMyPortalApp.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.ANNOTATION_STYLE);
		request.setAttribute("r_drop_down_values", sDocSplitterConfig);
		request.setAttribute("sd_Parser_fileType_List", sdParserFileTypeList);
		request.setAttribute("annotation_style_drop_down", annotationStyleList);
		oUserSession = WebUtils.getUserSession(request);
		userSeqId=String.valueOf(oUserSession.getUserInfo().getUserSeqId());
		String userPath=SystemUtils.getTempFolderPath()+"\\u"+userSeqId;
		File userFolder=new File(userPath);
		String parseFileType=null;
		Map<String, Object> annotedTextListMap=null;
		ParsedDataTable oDataTablePageContents=null;
		StringBuffer text=null;
		ParsedDataTable o_ParsedDataTable = null;
		Map<String, Object> keyMap=null;
		Map<String,List<String>> keyValueMap = null;
		String toolAppName=null;
		String frFileName = "fileName";
		String frParsedDataTable = "parseDataTable";
		String pdfExtString = ".pdf";
		String csvExt = ".csv";
		File file = null;
		FileParsingConfiguration  filePassConfig =  null; 
		activeTabFlag = WebUtils.getRequestParameter(request, "activeTabFlag",	oMultipartContentRequest);
		WebUtils.setRequestAttribute(request, "activeTabFlag", activeTabFlag);
		
		toolAppName 	= 	WebUtils.getRequestParameter(request, "tools_appid",	oMultipartContentRequest);
		if((toolAppName!=null && toolAppName.equals("PDF-SPLITTER")) || 
				(p_sActionId!=null && p_sActionId.equalsIgnoreCase("pdfSplitter.jsp")))
		{
			oAppResponse=new PortalAppResponse("pdfSplitter.jsp");
			return oAppResponse;
		}
		else if(toolAppName!=null && toolAppName.equals("PDF-PARSER") || 
				(p_sActionId!=null && p_sActionId.equalsIgnoreCase("pdfParser.jsp")))
		{
			oAppResponse=new PortalAppResponse("pdfParser.jsp");
			return oAppResponse;
		}
		else if(toolAppName!=null && toolAppName.equals("PDF-ANNOTATOR")|| 
				(p_sActionId!=null && p_sActionId.equalsIgnoreCase("pdfAnnotator.jsp")))
		{
			oAppResponse=new PortalAppResponse("pdfAnnotator.jsp");
			return oAppResponse;
		}
		/*if(p_sActionId!=null && p_sActionId.equals("cleanTempFolder"))
		{
			try {
				if(!userFolder.isDirectory())
				SystemUtils.createUserTempFolder("u" + p_oUserSession.getUserInfo().getUserSeqId());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}*/
		
		if( p_sActionId!=null && !p_sActionId.equals(StringConstants.EMPTY) && p_sActionId.equalsIgnoreCase("splitCaseFile"))
		{
			
			
			
			try {
				if(!userFolder.isDirectory())
				SystemUtils.createUserTempFolder("u" + p_oUserSession.getUserInfo().getUserSeqId());
			} catch (IOException e) {
				log.error("Error in staging the uploaded file(s)", e);
			}
			try 
			{
				listUploadedFileObject = WebUtils.stageRequestParameterFiles(request, CASEDATA_FILE, oMultipartContentRequest);
			}
			catch(SciWebException webExcep)
			{
				log.error("Error in staging the uploaded file(s)", webExcep);
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_NOT_UPLOADED);
				return oAppResponse;
			}
				
			try {
					sZipFilePath = oMyPortalApp.splitAndZip(listUploadedFileObject,userPath);
				} 
				catch (SciServiceException excep) 
				{
				log.error("Error in file splitting "+excep.getMessage());
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_SPLITTING);
				return oAppResponse;
			}
			catch (SciException e) {
				log.error("Error in file splitting "+e.getStackTrace());
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_IN_FILE_PARSING);
				return oAppResponse;
			}
			catch (Exception e) {
			log.error("Error in file splitting"+ e.getStackTrace());
			oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_IN_FILE_PARSING);
			return oAppResponse;
			}
			finally
			{
				if(listUploadedFileObject != null)
				{
					for(UploadedFileWrapper fileObject:listUploadedFileObject)
					{
						fileObject.delete();
					}
				}
			}
								
					
			
		sNewAction = "downloadFile";
		request.getSession().setAttribute("s-downloadFile-path", sZipFilePath);
		oAppResponse=new PortalAppResponse("pdfSplitter.jsp");
		oAppResponse.setNextAction(sNewAction);	
		}else if(p_sActionId!=null && p_sActionId.equalsIgnoreCase("download_split_file"))
		{
			try
			{
			sZipFilePath = (String) request.getSession().getAttribute("s-downloadFile-path");
			
			oAppResponse=oMyPortalApp.downloadFile(sZipFilePath, response, oAppResponse);
			log.info("Downloading file completed < "+sZipFilePath+" >");
			file = new File(sZipFilePath);
	//		oAppResponse=new PortalAppResponse("pdfSplitter.jsp");
	//		oAppResponse.setNextAction(sNewAction);
			}
			catch(Exception ex)
			{
				log.error("Error in File download"+ file.getName());
			}
			finally
			{
			
			}
			return oAppResponse;
		}
		else if(p_sActionId!=null && p_sActionId.equalsIgnoreCase("parseFile"))
		{
			
			Map<String, Object> listOfObject = null;
			List<ParsedDataTable>  parsedDataTableList = null;
			Map<String, FileParsingConfiguration.FileParsingField> parsedMap = null;
		//	System.out.println("parseFile action id executed under SourceDocUtil Portal App ");
			parseFileType = WebUtils.getRequestParameter(request, "parseFileType");
			WebUtils.setRequestAttribute(request, "parseFileType", parseFileType);
			
			
			if(parseFileType.equalsIgnoreCase("Health Canada"))
			{
				filePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_BA_KEY);
				
			}else if(parseFileType.equalsIgnoreCase("E2B"))
			{
				filePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AA_KEY);
			}
			else if(parseFileType.equalsIgnoreCase("MESA"))
			{
				filePassConfig = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AB_KEY);
			}
			Map<String, Set<String>> groupMap = null; 
			
			try 
			{
				listUploadedFileObject = WebUtils.stageRequestParameterFiles(request, CASEDATA_FILE_PARSE, oMultipartContentRequest);
			}
			catch(SciWebException webExcep)
			{
				log.error("Error in staging the uploaded file(s)", webExcep);
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_NOT_UPLOADED);
				return oAppResponse;
			}
			try
			{
				
				listOfObject  = oMyPortalApp.parsePdf(listUploadedFileObject, oDataTablePageContents, parseFileType);
				parsedDataTableList =(List<ParsedDataTable>) listOfObject.get("dataTableList");
				if(listOfObject.containsKey("keyMap"))
					keyValueMap =(Map<String, List<String>>) listOfObject.get("keyMap");
				sNewAction = "showParsedFile";
			
			groupMap = new LinkedHashMap<String, Set<String>>();
			Set<String> collationgroup = new LinkedHashSet<>();
			parsedMap = filePassConfig.getFieldMap();
				for(String key : parsedMap.keySet()){ 
					String fieldGroup = parsedMap.get(key).getM_sFieldGroup();
					collationgroup.add(parsedMap.get(key).getCollationgroup());
					
					if (groupMap.get(fieldGroup) == null)
					{
						groupMap.put(fieldGroup, new LinkedHashSet<String>());
					}
						groupMap.get(fieldGroup).add((parsedMap.get(key).getM_sFieldSubGroup()));
				}
				
				request.getSession().setAttribute("filePassConfigSession", filePassConfig);
				if(parsedDataTableList.size()==1)
				{
					o_ParsedDataTable = parsedDataTableList.get(0);
					WebUtils.setRequestAttribute(request, "groupMap", groupMap);
					WebUtils.setRequestAttribute(request, "parseddatatable", o_ParsedDataTable);
					WebUtils.setRequestAttribute(request, "filePassConfig", filePassConfig);
					WebUtils.setRequestAttribute(request, "collationgroup", collationgroup);
					
					nextPageUri = "pdfParsedData.jsp";
				}
				else
				{
					fileNameString = "FileExport";
					try {
						fileNameString = WebUtils.createStagingFileForTools(WebUtils.getUserSession(request),fileNameString, csvExt);
					} catch (IOException e) {
						log.error("Error while exporting the file");
					}
				
					 
					 WebUtils.setRequestAttribute(request, "keyValueArray", keyValueMap);
					 WebUtils.setRequestAttribute(request, "noOfFiles", parsedDataTableList.size());
					 WebUtils.setRequestAttribute(request, "fileNameString", fileNameString);
					 
					
					nextPageUri = "multiPdfParsedData.jsp";
				}
				
				request.getSession().setAttribute(frFileName, listUploadedFileObject.get(0).getOriginalFileName());
				oAppResponse=new PortalAppResponse(nextPageUri);
				oAppResponse.setNextAction(sNewAction);
			}
			catch (SciException e) {
				log.error("Error in file parsing < "+listUploadedFileObject.get(0).getOriginalFileName()+" >");
				
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_NOT_UPLOADED);
				return oAppResponse;
			}
			
			catch (SciServiceException sciServ) {
				log.error("Error in file parsing < "+sciServ.getMessage()+listUploadedFileObject.get(0).getOriginalFileName()+" >");
			//	oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_TYPE_FILE_DIFFERENT);
				oAppResponse.logError(sciServ);
				return oAppResponse;
			}
			
				finally
				{
					if(listUploadedFileObject != null)
					{
						for(UploadedFileWrapper fileObject:listUploadedFileObject)
						{
							fileObject.delete();
						}
					}
				}
		}
		else if(p_sActionId!=null && p_sActionId.equalsIgnoreCase("highlightFile"))
		{
			
			try {
				if(!userFolder.isDirectory())
				SystemUtils.createUserTempFolder("u" + p_oUserSession.getUserInfo().getUserSeqId());
			} catch (IOException e) {
				log.error("Error in staging the uploaded file(s)", e.getMessage());
				//oAppResponse.logError(new SciServiceException());
				//return oAppResponse;
			}
			
			try 
			{
				listUploadedFileObject = WebUtils.stageRequestParameterFiles(request, FILE_FOR_HIGHLIGHT, oMultipartContentRequest);
			}
			catch(SciWebException webExcep)
			{
				log.error("Error in staging the uploaded file(s)", webExcep);
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_NOT_UPLOADED);
				return oAppResponse;
			}
			
			try
			{
				keyMap=oMyPortalApp.highlightPdf(listUploadedFileObject, userPath);
				sZipFilePath=(String) keyMap.get("outputFilePath");
				annotedTextListMap=(Map<String, Object>) keyMap.get("annotedTextMap");
				text=oMyPortalApp.setTextForAnnotedData(annotedTextListMap);
				sNewAction = "showHighlightedFile";
			}
			catch (Exception e) {
				
				log.error("Error in file parsing");
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_NOT_UPLOADED);
				return oAppResponse;
			}
			finally
			{
				if(listUploadedFileObject != null)
				{
					for(UploadedFileWrapper fileObject:listUploadedFileObject)
					{
						fileObject.delete();
					}
				}
			}
				request.getSession().setAttribute("highlightedText", text.toString());
				request.getSession().setAttribute("s-downloadFile-path", sZipFilePath);	
				oAppResponse=new PortalAppResponse("pdfAnnotator.jsp");
				oAppResponse.setNextAction(sNewAction);
		}
		
		else if(p_sActionId!=null && p_sActionId.equalsIgnoreCase("exportcsv"))
		{
			
			String fileName = (String)request.getSession().getAttribute(frFileName);
			fileName = fileName.replace(pdfExtString, SciConstants.StringConstants.EMPTY);
			o_ParsedDataTable = (ParsedDataTable)request.getSession().getAttribute(frParsedDataTable);
			String exportFilePath = null;
			//File file = null;
		
			try {
				exportFilePath = WebUtils.createStagingFileForTools(WebUtils.getUserSession(request),fileName.trim(),csvExt);
				file = new File(exportFilePath);
			} catch (IOException e) {
				log.error("Failed to Export CSV file "+ exportFilePath);
			}
			
			FileParsingConfiguration fileParseConfigSession =  (FileParsingConfiguration) request.getSession().getAttribute("filePassConfigSession");
			try {
			oAppResponse = oMyPortalApp.exportCSVForSingleCase(exportFilePath, fileParseConfigSession, o_ParsedDataTable, oAppResponse, response);
			} catch (SciException e) {
				log.error("Error While exporting the file to CSV"+e.getMessage());
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_EXPORT_AS_CSV);
				return oAppResponse;
			}
			
			
			
				
				
				
				
		}
		//changes for multiple file export start
		
		else if(p_sActionId!=null && p_sActionId.equalsIgnoreCase("multifileexportcsv"))
		{
			keyValueMap = (Map<String, List<String>>)request.getSession().getAttribute("multiplefileData");
			fileNameString =(String) request.getSession().getAttribute("fileNameString");
			List<String> fileNameList = keyValueMap.get("FileName");
		
			//o_ParsedDataTable = (ParsedDataTable)request.getSession().getAttribute(frParsedDataTable);
			
			oAppResponse = oMyPortalApp.exportCSVForMultipleSDs(fileNameString, fileNameList, keyValueMap, oAppResponse, response);
		}
		else if(p_sActionId!=null && p_sActionId.equalsIgnoreCase("multifileExportExcel"))
		{
			
			fileNameString = "FileExport";
			try {
				fileNameString = WebUtils.createStagingFileForTools(WebUtils.getUserSession(request),fileNameString, ".xls");
			} catch (IOException e) {
				log.error("Error while exporting the file");
			}
			
			keyValueMap = (Map<String, List<String>>)request.getSession().getAttribute("multiplefileData");
		//	fileNameString =(String) request.getSession().getAttribute("fileNameString");
			List<String> fileNameList = keyValueMap.get("FileName");
		
			o_ParsedDataTable = (ParsedDataTable)request.getSession().getAttribute(frParsedDataTable);
			
			oAppResponse = oMyPortalApp.exportExcelForMultipleSDs(fileNameString, o_ParsedDataTable, fileNameList, keyValueMap, oAppResponse, response);
		}
		
		//changes for multiple file export ends
		return oAppResponse;
	}
		
	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		return null;
		
	}
}
