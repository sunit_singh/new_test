package com.sciformix.sciportal.web.apps;

import java.io.File;

import org.apache.commons.io.FileUtils;


public class UploadedFileWrapper
{
	private String m_sStagedFilePath = null;
	private String m_sStagedFileName = null;
	private String m_sOriginalFileName = null;
	private boolean m_bIsFileAvailable = false;
	
	
	public UploadedFileWrapper(String p_sStagedFilePath, String p_sStagedFileName, String p_sOriginalFileName)
	{
		m_sStagedFilePath = p_sStagedFilePath;
		m_sStagedFileName = p_sStagedFileName;
		m_sOriginalFileName = p_sOriginalFileName;
		m_bIsFileAvailable = (p_sStagedFilePath != null);
	}
	
	public boolean isFileAvailable()
	{
		return m_bIsFileAvailable;
	}

	public String getStagedFilePath()
	{
		return m_sStagedFilePath;
	}
	
	public String getStagedFileName()
	{
		return m_sStagedFileName;
	}
	
	public String getOriginalFileName()
	{
		return m_sOriginalFileName;
	}
	
	public void delete()
	{
		if(FileUtils.deleteQuietly(new File(m_sStagedFilePath)))
		{
			m_bIsFileAvailable = false;
		}
	}
}
