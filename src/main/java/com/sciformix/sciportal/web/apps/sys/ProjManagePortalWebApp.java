package com.sciformix.sciportal.web.apps.sys;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.DataValidationUtils;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.sys.ProjManagePortalApp;
import com.sciformix.sciportal.project.ProjectInfo;
import com.sciformix.sciportal.project.ProjectInfoDb;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;

public class ProjManagePortalWebApp extends BasePortalWebApp {
	
	private static final Logger log= LoggerFactory.getLogger(ProjManagePortalWebApp.class);

	public ProjManagePortalWebApp() {
		super("PROJMNG-PORTAL-APP", "project_management.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
	}

	@Override
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response,
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession,
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) throws SciServiceException {
		
		ProjManagePortalApp oMyPortalApp = null;
		PortalAppResponse oAppResponse = null;
		oMyPortalApp = (ProjManagePortalApp) oPortalApp;
		String validateInput="";
		List<ProjectInfo> projectList = null; 
		int projectSequebceId;
		String sAction = "";
		UserSession oUserSession = null;
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		oUserSession = WebUtils.getUserSession(request);
		int l_projectid = WebUtils.getAuthorizedProjectId(request);
		
		log.debug("ProjManagePortalWebApp: Starting processing for Action - {}", p_sActionId);
		
		if(p_sActionId!=null){
			
			if(p_sActionId.equalsIgnoreCase("create"))
			{
				sAction="createProject";
			}
			if(p_sActionId.equalsIgnoreCase("createProject")||p_sActionId.equalsIgnoreCase("editProject"))
			{
				ProjectInfoDb projInfoDb = new ProjectInfoDb();
				projInfoDb.setProjectName(WebUtils.getRequestParameter(request, "projName",oMultipartContentRequest).trim());
				projInfoDb.setProjectDescription(WebUtils.getRequestParameter(request, "projDescription",oMultipartContentRequest).trim());
				projInfoDb.setProjectState(ProjectInfo.ProjectState.ACTIVE.value());
				
				
				if(p_sActionId.equalsIgnoreCase("createProject")){
					projInfoDb.setProjectSequenceId(-1);
					
					if(!DataValidationUtils.isValidObjectName(projInfoDb.getProjectName()))
					{
							if(WebUtils.getRequestParameter(request, "projName", oMultipartContentRequest).trim().isEmpty())
								oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_PROJECT_NAME_EMPTY);
							else
								oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_PROJECT_NAME_INVALID);
								sAction="createProject";
								oAppResponse.setNextAction(sAction);
							return oAppResponse;
					}
					else
					if(WebUtils.getRequestParameter(request,"projDescription", oMultipartContentRequest).trim().isEmpty())
					{
						oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_PROJECT_DESCRIPTION_EMPTY);
						sAction="createProject";
						oAppResponse.setNextAction(sAction);
						return oAppResponse;
					}
					else{
						
						projectList= oMyPortalApp.getAllProjects();
						
						for(ProjectInfo o_projectInfo : projectList )
						{
							if(projInfoDb.getProjectName().equals(o_projectInfo.getProjectName()))
							{
								oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_PROJECT_NAME_ALREADY_EXIST);
								sAction="createProject";
								oAppResponse.setNextAction(sAction);
								return oAppResponse;
							}
						}
						validateInput = oMyPortalApp.saveProject(oUserSession, new ProjectInfo(projInfoDb),l_projectid, true);
						request.setAttribute("msg", validateInput);
						sAction=null;
					}
				}
				else
				{
					int project_id=Integer.parseInt(request.getParameter("projectId"));
					projInfoDb.setProjectSequenceId(project_id);
					String sProjectExistingName = oMyPortalApp.getProjectInfoByProjectId(projInfoDb.getProjectSequenceId()).getProjectName();
					String sProjectExistingDescription =  oMyPortalApp.getProjectInfoByProjectId(projInfoDb.getProjectSequenceId()).getProjectDescription();
					if(!DataValidationUtils.isValidObjectName(projInfoDb.getProjectName()))
					{
							if(WebUtils.getRequestParameter(request, "projName", oMultipartContentRequest).trim().isEmpty())
								oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_PROJECT_NAME_EMPTY);
							else
								oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_PROJECT_NAME_INVALID);
							
								request.setAttribute("selectedProjectInfo", ProjManagePortalApp.getProjectInfoByProjectId(project_id));
								sAction="editProject";
								oAppResponse.setNextAction(sAction);
							return oAppResponse;
					}
					else
					if(WebUtils.getRequestParameter(request,"projDescription", oMultipartContentRequest).trim().isEmpty())
					{
						oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_PROJECT_DESCRIPTION_EMPTY);
						request.setAttribute("selectedProjectInfo", ProjManagePortalApp.getProjectInfoByProjectId(project_id));
						sAction="editProject";
						oAppResponse.setNextAction(sAction);
						return oAppResponse;
					}
					else{
						if(DataUtils.hasChanged(sProjectExistingName, projInfoDb.getProjectName())||DataUtils.hasChanged(sProjectExistingDescription, projInfoDb.getProjectDescription()))
						{	
							validateInput = oMyPortalApp.saveProject(oUserSession, new ProjectInfo(projInfoDb), l_projectid, false);
							request.setAttribute("msg", validateInput);
							sAction=null;
						}
						else
						{
							oAppResponse.logError(WebErrorCodes.ProjectManagement.ERROR_UPDATE_VALUES_SAME_AS_EXISTING);
							request.setAttribute("selectedProjectInfo", ProjManagePortalApp.getProjectInfoByProjectId(project_id));
							sAction="editProject";
							oAppResponse.setNextAction(sAction);
							return oAppResponse;
						}
					}
				}
			}
			if(p_sActionId.equalsIgnoreCase("view"))
			{
				projectSequebceId = Integer.parseInt(request.getParameter("selectedProjectId"));
				request.setAttribute("selectedProjectInfo", ProjManagePortalApp.getProjectInfoByProjectId(projectSequebceId));
				sAction="viewProject";
			}
			
			if(p_sActionId.equalsIgnoreCase("edit"))
			{
				projectSequebceId = Integer.parseInt(request.getParameter("selectedProjectId"));
				request.setAttribute("selectedProjectInfo", ProjManagePortalApp.getProjectInfoByProjectId(projectSequebceId));
				sAction="editProject";
			}
		}
		projectList= oMyPortalApp.getAllProjects();
		request.setAttribute("projectsList", projectList);
		oAppResponse.setNextAction(sAction);
		return oAppResponse;
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}
}
