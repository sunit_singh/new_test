package com.sciformix.sciportal.web.apps;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.user.UserUtils;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.WebConstants;

public abstract class BasePortalWebApp implements IPortalWebApp
{
	private static final Logger log = LoggerFactory.getLogger(BasePortalWebApp.class);
	
	private String sPortalAppId = null;
	private String sDefaultPageUri = null;
	private ProjectApplicability eProjectApplicability = null;
	
	public BasePortalWebApp(String p_sPortalAppId, String p_sPortalAppName, ProjectApplicability p_eProjectApplicability)
	{
		sPortalAppId = p_sPortalAppId;
		sDefaultPageUri = p_sPortalAppName;
		eProjectApplicability = p_eProjectApplicability;
	}
	
	public String getPortalAppId()
	{
		return sPortalAppId;
	}
	
	public String getDefaultPageUri()
	{
		return sDefaultPageUri;
	}
	
	public ProjectApplicability getProjectApplicability()
	{
		return eProjectApplicability;
	}
	
	abstract protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) throws SciServiceException;
	
	
	public PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId)
	{
		IPortalApp oPortalApp = null;
		UserInfo oSessionUserInfo = null;
		List<UserPreference> listAppSpecificUserPreferences = null;
		UserSession oUserSession = null;
		PortalAppResponse oAppResponse = null;
		boolean bValidationFlag = true;
		String sNextAction = null;
		List<Object> resultList = null;
		//Enumeration<String> parameterList = null;
		//Step 1: Retrieve parameters
		oPortalApp = AppRegistry.getApp(getPortalAppId());
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		if (oPortalApp == null)
		{
			return PortalAppResponse.INVALID_SESSION_RESPONSE;
		}
		try
		{
			resultList = WebUtils.validateRequestParameters(p_sActionId, oPortalApp.getPortalAppId(), request, oMultipartContentRequest);
			bValidationFlag = (boolean) resultList.get(0);
			if(!bValidationFlag)
			{
				sNextAction = getNextAction(oAppResponse, request, response, p_sActionId, oPortalApp);
				oAppResponse.setNextAction(sNextAction);
				oAppResponse = new PortalAppResponse("../../errorPage.jsp");
				oAppResponse.logError(WebErrorCodes.System.INVALID_REQUEST_PARAMETER, "");
				WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
				return oAppResponse;
				
				
			/*	oAppResponse.logError(WebErrorCodes.Ngv.CASE_VALIDATION_FAILED_FOR_ERROR_CASE, "AAA");
				oAppResponse.setNextAction(null);
				return oAppResponse;*/
			}
			
			//abstract method
		}catch(SciServiceException serExcep)
		{
			
			WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
			oAppResponse.logError(serExcep);
			return oAppResponse;
		}
	
		oUserSession = WebUtils.getUserSession(request);
		oSessionUserInfo = (oUserSession != null ? oUserSession.getUserInfo() : null);
		listAppSpecificUserPreferences = (oSessionUserInfo != null ? oUserSession.getUserPreferences() : null);
		
		if (listAppSpecificUserPreferences == null)
		{
			return PortalAppResponse.INVALID_SESSION_RESPONSE;
		}
		
		
		//Step 2: Access check
		//TODO:PENDING: Privilege checks for the given App, given Action, given Context
		if (!oSessionUserInfo.permittedToAccess(getPortalAppId()))
		{
			log.debug("Access is not there for "+getPortalAppId());
			return PortalAppResponse.INVALID_SESSION_RESPONSE;
		}
		
//		if (oSessionUserInfo.getUserType() == 7) {
//			// do nothing all access are there with the user
//		} else if (getPortalAppId().equals(AppRegistry.getAuditTrailViewerApp().getAppId())) {
//			if (!oSessionUserInfo.isAuditor()) {
//				return PortalAppResponse.INVALID_SESSION_RESPONSE;
//			}
//		} else if (getPortalAppId().equals("PROJSETTINGS-PORTAL-APP")) {
//			if (!oSessionUserInfo.isProjectAdmin()) {
//				return PortalAppResponse.INVALID_SESSION_RESPONSE;
//			}
//		} else if (getPortalAppId().equals("APPSETTINGS-PORTAL-APP") || getPortalAppId().equals("PROJMNG-PORTAL-APP") || getPortalAppId().equals("USRMGMNT-PORTAL-APP")) {
//			if (!oSessionUserInfo.isSystemAdmin()) {
//				return PortalAppResponse.INVALID_SESSION_RESPONSE;
//			}
//		} else {
//			// do nothing it is accessing the applications and user preference
//		}
		
		
		
		
		//Step 3: User Preference configuration
		//Step 3a: If App can be individually enabled or disabled, see if it is enabled for the User
		if(!isApplicationEnabledForUser(oSessionUserInfo, listAppSpecificUserPreferences))
		{
			oAppResponse.logError(WebErrorCodes.System.APPLICATION_NOT_ENABLED_FOR_USER, oSessionUserInfo.getUserId());
			WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
			return oAppResponse;
		}
		
		//Step 3a2: Check Project applicability for the App
		List<ObjectIdPair<Integer,String>> listAuthorizedProjectsForUser = oUserSession.getAuthorizedProjectIds();
		if (this.eProjectApplicability == ProjectApplicability.SINGLE_PROJECT)
		{
			if (listAuthorizedProjectsForUser.size() < 1)
			{
				oAppResponse.logError(WebErrorCodes.System.APPLICATION_REQUIRES_PROJECT_ASSOCIATION, oSessionUserInfo.getUserId());
				WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
				return oAppResponse;
			}
			/*else if (listAuthorizedProjectsForUser.size() > 1)
			{
				oAppResponse.logError(WebErrorCodes.System.APPLICATION_REQUIRES_MAX_ONE_PROJECT_ASSOCIATION, oSessionUserInfo.getUserId());
				WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
				return oAppResponse;
			}*/
		}
		else if (this.eProjectApplicability == ProjectApplicability.MULTIPLE_PROJECTS)
		{
			if (listAuthorizedProjectsForUser.size() < 1)
			{
				oAppResponse.logError(WebErrorCodes.System.APPLICATION_REQUIRES_PROJECT_ASSOCIATION, oSessionUserInfo.getUserId());
				WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
				return oAppResponse;
			}
		}
		
		
		/*//Step 3b: Any other minimum configuration for the given App
		if (listLocalErrors.size() == 0)
		{
			//TODO:Check for other minimum expected User Preferences - this can be done using the App's metadata itself
			
		}*/		
		try
		{
			oAppResponse = processAction(request, response, oMultipartContentRequest, p_sActionId, oUserSession, 
					listAppSpecificUserPreferences, oPortalApp);
			
			//Step 5: Prepare for error display or next page display, as applicable
			WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
			WebUtils.setRequestAttribute(request, "new-portalappid", getPortalAppId());
			WebUtils.setRequestAttribute(request, "new-portalaction", oAppResponse.getNextAction());
			
		}
		catch (SciServiceException serExcep)
		{
			log.error("Error processing action", serExcep);
			oAppResponse.logError(serExcep);
			return oAppResponse;
		}
	
		return oAppResponse;
	}
	
	protected boolean isApplicationEnabledForUser(UserInfo oSessionUserInfo, List<UserPreference> listAppSpecificUserPreferences)
	{
		IPortalApp oPortalApp = null;
		int iSelectedProjectId = -1;
		oPortalApp = AppRegistry.getApp(getPortalAppId());
	//TODO:AR-Pass ProejctId from here onwards
		if(oPortalApp.applicationProjectSpecific())
		{
			try{
				iSelectedProjectId =UserUtils.getCurrentlySelectedUserProject(oSessionUserInfo);
			}
			catch(SciException scExc)
			{
				//TODO: throw SciWebException
				scExc.printStackTrace();
			}
		}
		
		if (listAppSpecificUserPreferences == null || !oPortalApp.isApplicationEnabledForUser(oSessionUserInfo,iSelectedProjectId))
		{
			return false;
		}
		
		return true;
	}
	
	public PortalAppResponse processProjectSettingsAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp)
	{
		//Override this if project settings apply	
		return null;
	}
	
	abstract protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request, HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp);
}
