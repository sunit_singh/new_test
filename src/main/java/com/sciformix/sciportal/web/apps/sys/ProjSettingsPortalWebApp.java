package com.sciformix.sciportal.web.apps.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.DataValidationUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.sys.ProjManagePortalApp;
import com.sciformix.sciportal.apps.sys.ProjSettingsPortalApp;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.mail.MailConfigurationHelper;
import com.sciformix.sciportal.mail.MailTemplate;
import com.sciformix.sciportal.mb.MailBox;
import com.sciformix.sciportal.projectusermanagement.ProjectUserRole;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate;
import com.sciformix.sciportal.safetydb.SafetyDbConfigurationHelper;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;
import com.sciformix.sciportal.web.apps.IPortalWebApp;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class ProjSettingsPortalWebApp extends BasePortalWebApp
{
	private static final String	
		QR_REQUEST_PARAMETER_TEMPLATE_NAME		=	"fldtemplatename"
	;
	private static final String
		QR_REQUEST_PARAMETER_TEMPLATE_SEQID		=	"fldtemplateseqid"
	;
	private static final String
		QR_REQUEST_PARAMETER_TEMPLATE_NEWSTATE	=	"fldtemplatenewstate"
	;
	private static final Logger 
		log	=	LoggerFactory.getLogger(ProjSettingsPortalWebApp.class)
	;
	public ProjSettingsPortalWebApp()
	{
		super("PROJSETTINGS-PORTAL-APP", "projsettings.jsp", ProjectApplicability.SINGLE_PROJECT);
	}
	
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp)
	{
		String 				sSettingsAppName 			=	null;
		IPortalWebApp 		oSettingsPortalWebApp 		=	null;
		IPortalApp			oSettingsPortalApp 			=	null;
		PortalAppResponse	oAppResponse 				=	null,
							oAppResponseError			=	null;
		PortalAppResponse	oSettingsAppResponse 		=	null;
		ProjSettingsPortalApp projectSettingsPortalApp	= 	(ProjSettingsPortalApp) oPortalApp;
		UserInfo 				u_oUserInfo				=	null;
		MailTemplate		m_oMailTemplate				= 	new MailTemplate();
		MailBox					m_oMailBox				= 	new MailBox();
		String 				nextPageUrl					=	null,
							l_templatename				=	null;
		int					l_projectid					=	0;
		UploadedFileWrapper	l_uploadedfile				=	null;
		
		Map<String, String> mapPrjConfig 				= null;
		Map<String, String> mapPrjConfigAll 			= null;
		
		u_oUserInfo			=	WebUtils.getUserSession(request).getUserInfo();
		l_projectid			=	WebUtils.getAuthorizedProjectId(request);
		sSettingsAppName 	= 	WebUtils.getRequestParameter(
									request
								, 	"projsettings_appid"
								, 	oMultipartContentRequest
								);
		
		if(sSettingsAppName !=null)
		{	
			/*NGV Portal APP*/
			if("NGV-PORTAL-APP".equals(sSettingsAppName))
			{	
				nextPageUrl="narrative_template_ruleset_settings.jsp";
				oSettingsPortalWebApp = (sSettingsAppName != null) ? AppRegistry.getWebApp(sSettingsAppName) : null;
				oSettingsPortalApp = (sSettingsAppName != null) ? AppRegistry.getApp(sSettingsAppName) : null;
				
				if (oSettingsPortalWebApp != null)
				{
					p_sActionId=WebUtils.getRequestParameter(request, "clickValue", oMultipartContentRequest);
					oSettingsAppResponse = oSettingsPortalWebApp.processProjectSettingsAction(request, response, oMultipartContentRequest,p_sActionId, 
													p_oUserSession, listAppSpecificUserPreferences, oSettingsPortalApp);
					oAppResponse = new PortalAppResponse(nextPageUrl, oSettingsAppResponse, oSettingsAppResponse.isDownloadStreamPresent());
				}
				return oAppResponse;
			}
			else if(sSettingsAppName.equalsIgnoreCase("NGV_Portal_Validation_App"))
			{	
				nextPageUrl="validationprojsettings.jsp";
				sSettingsAppName	=	"NGV-PORTAL-APP";
				oSettingsPortalWebApp = (sSettingsAppName != null) ? AppRegistry.getWebApp(sSettingsAppName) : null;
				oSettingsPortalApp = (sSettingsAppName != null) ? AppRegistry.getApp(sSettingsAppName) : null;
				
				if (oSettingsPortalWebApp != null)
				{
					if(p_sActionId.equalsIgnoreCase("NGV_Portal_Validation_App"))
						p_sActionId=WebUtils.getRequestParameter(request, "clickValue", oMultipartContentRequest);
					if(StringUtils.isNullOrEmpty(p_sActionId))
					{
						p_sActionId="validate";
					}
					
					oSettingsAppResponse = oSettingsPortalWebApp.processProjectSettingsAction(request, response, oMultipartContentRequest,p_sActionId, 
													p_oUserSession, listAppSpecificUserPreferences, oSettingsPortalApp);
				
					oAppResponse = new PortalAppResponse(nextPageUrl, oSettingsAppResponse, oSettingsAppResponse.isDownloadStreamPresent());
				}
				return oAppResponse;
			} 
			else if ("QR-PORTAL-APP".equals(sSettingsAppName)) {
				oAppResponse	=	new PortalAppResponse("projsettings.jsp");
				
				oSettingsPortalWebApp = (sSettingsAppName != null) ? AppRegistry.getWebApp(sSettingsAppName) : null;
				oSettingsPortalApp = (sSettingsAppName != null) ? AppRegistry.getApp(sSettingsAppName) : null;
				
				if (oSettingsPortalWebApp != null) {
					oSettingsAppResponse	=	oSettingsPortalWebApp.processProjectSettingsAction(
													request
												, 	response
												, 	oMultipartContentRequest
												,	p_sActionId
												, 	p_oUserSession
												, 	listAppSpecificUserPreferences
												, 	oSettingsPortalApp
												);
					oAppResponse			=	null;
					oAppResponse	=	new PortalAppResponse(
											oSettingsAppResponse.getNextPageUri()
										, 	oSettingsAppResponse
										, 	oSettingsAppResponse.isDownloadStreamPresent()
										);
				}
				WebUtils.setRequestAttribute(request, "r_appId", oSettingsPortalApp.getPortalAppId());
				return oAppResponse;
			} else if ("SCFT".equals(sSettingsAppName)) {
				if (StringUtils.isNullOrEmpty(p_sActionId)) {
					List<SafetyDbCaseFieldsTemplate>	l_templatelist		=	null;
					oAppResponse	=	new PortalAppResponse("safetydbcasefieldtemplatelist.jsp");
					try {
						l_templatelist	=	new ArrayList<> ();
						l_templatelist	=	SafetyDbConfigurationHelper.getTemplates(l_projectid);
						WebUtils.setRequestAttribute(
							request
						, 	"templatelist"
						, 	l_templatelist
						);
					} catch (SciServiceException serExcep) {
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
				} else if ("createnewscftblank".equals(p_sActionId)) {
					oAppResponse	=	null;
					oAppResponse	=	new PortalAppResponse("safetydbcasefieldtemplatecreate.jsp");
				}	else if ("createnewscft".equals(p_sActionId)) {
					String	l_safetydbname		=	null,
							l_safetydbversion	=	null,
							l_caseidalias		=	null,
							l_caseversionalias	=	null;
					
					oAppResponse		=	null;
					oAppResponse		=	new PortalAppResponse("safetydbcasefieldtemplatedetails.jsp");
					oAppResponseError	=	null;
					oAppResponseError	=	new PortalAppResponse("safetydbcasefieldtemplatecreate.jsp");
					l_templatename		=	WebUtils.getRequestParameter(
												request
											, 	QR_REQUEST_PARAMETER_TEMPLATE_NAME
											, 	oMultipartContentRequest
											);
					if(StringUtils.isNullOrEmpty(l_templatename)) {
						oAppResponseError.logError(WebErrorCodes.ProjectSetting.ERROR_TEMPLATE_NAME_EMPTY);
						return oAppResponseError;
					}
					if (!DataValidationUtils.isValidObjectName(l_templatename)) {
						oAppResponseError.logError(WebErrorCodes.ProjectSetting.ERROR_TEMPLATE_NAME_INVALID,null);
						return oAppResponseError;
					}
					l_safetydbname		=	WebUtils.getRequestParameter(
												request
											, 	"fldsafetydbname"
											, 	oMultipartContentRequest
											);
					l_safetydbversion	=	WebUtils.getRequestParameter(
												request
											, 	"fldsafetydbversion"
											, 	oMultipartContentRequest
											);
					l_caseversionalias	=	WebUtils.getRequestParameter(
												request
											, 	"fldcaseversionalias"
											, 	oMultipartContentRequest
											);
					l_caseidalias		=	WebUtils.getRequestParameter(
												request
											, 	"fldcaseidalias"
											, 	oMultipartContentRequest
											);
					try {
						l_uploadedfile	=	WebUtils.stageRequestParameterFile(
												request
											, 	"fldmasterfile"
											, 	oMultipartContentRequest
											);
					} catch (SciWebException webExcep) {
						log.error(
							"Error uploading file"
						, 	webExcep
						);
						oAppResponseError.logError(
							WebErrorCodes.ProjectSetting.ERROR_UPLOADING_FILE
						);
						return oAppResponseError;
					}
					try {
						boolean	l_templatenameunique	=	SafetyDbConfigurationHelper.isObjectNameUnique(
																l_projectid
															,	l_templatename
															);
						if (!l_templatenameunique) {
							oAppResponseError.logError(WebErrorCodes.ProjectSetting.ERROR_TEMPLATE_NAME_NOT_UNIQUE, null);
							//TODO: Redirection need to be done
							return oAppResponseError;
						}
					} catch (SciServiceException serExcep) {
						log.error("Error validating template ruleset name", serExcep);
						oAppResponseError.logError(serExcep);
						return oAppResponseError;
					}
					try {
						SafetyDbCaseFieldsTemplate	l_scftdata	=	SafetyDbConfigurationHelper.saveSafetyCaseFileData(
																		l_uploadedfile
																	,	l_projectid
																	,	l_templatename
																	,	p_oUserSession
																	,	l_safetydbname
																	,	l_safetydbversion
																	,	l_caseidalias
																	,	l_caseversionalias
																	);
						WebUtils.setRequestAttribute(request, "fldtemplatedata", l_scftdata);
					} catch (SciServiceException serExcep) {
						log.error("Error saving safety case file in DB", serExcep);
						oAppResponseError.logError(serExcep);
						return oAppResponseError;
					}
				} else if ("gettemplatedetails".equals(p_sActionId)) {
					int	l_templateseqid	=	WebUtils.getRequestIntParameter(
												request
											, 	QR_REQUEST_PARAMETER_TEMPLATE_SEQID
											, 	oMultipartContentRequest
											);
					
					oAppResponse	=	null;
					oAppResponse	=	new PortalAppResponse("safetydbcasefieldtemplatedetails.jsp");
					
					try {
						SafetyDbCaseFieldsTemplate	l_scftdata	=	null;
							
						l_scftdata	=	SafetyDbConfigurationHelper.getTemplateDetails(
											l_templateseqid
										);
						if (l_scftdata == null) {
							oAppResponse.logError(WebErrorCodes.ProjectSetting.ERROR_TEMPLATE_NOT_FOUND);
							return oAppResponse;
						}
						
						WebUtils.setRequestAttribute(request, "fldtemplatedata", l_scftdata);
					} catch (SciServiceException serExcep) {
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
				} else if ("updatetemplatestate".equals(p_sActionId)) {
					int	l_templateseqid		=	WebUtils.getRequestIntParameter(
													request
												, 	QR_REQUEST_PARAMETER_TEMPLATE_SEQID
												, 	oMultipartContentRequest
												);
					int	l_templatenewstate	=	WebUtils.getRequestIntParameter(
													request
												, 	QR_REQUEST_PARAMETER_TEMPLATE_NEWSTATE
												, 	oMultipartContentRequest
												);
					
					oAppResponse	=	null;
					//TODO: The jsp file need to be changed.
					oAppResponse	=	new PortalAppResponse("safetydbcasefieldtemplatedetails.jsp");
					try {
						SafetyDbCaseFieldsTemplate	l_scftdata	=	null;
						l_scftdata	=	SafetyDbConfigurationHelper.updateTemplateState(
											p_oUserSession
										, 	RecordState.toEnum(l_templatenewstate)
										, 	l_templateseqid
										,	l_projectid
										);
						WebUtils.setRequestAttribute(request, "fldtemplatedata", l_scftdata);
					} catch (SciServiceException serExcep) {
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
				}
			} else if("EMAIL-TEMP-APP".equals(sSettingsAppName))
			{   
				List<MailTemplate> mailTemplateList =  new ArrayList<MailTemplate>();
				mailTemplateList=MailConfigurationHelper.getAllEmailTemplates(l_projectid);
				WebUtils.setRequestAttribute(request,"mailTemplateList", mailTemplateList);
				if(StringUtils.isNullOrEmpty(p_sActionId)||p_sActionId.contains("cancel")){
					nextPageUrl="email_template.jsp";
					oAppResponse = new PortalAppResponse(nextPageUrl);
					return oAppResponse;
				}
				else
				{
					if(p_sActionId.contains("createView")) //Go to create template page
					{
						p_sActionId=null;
						nextPageUrl="email_template_form.jsp";
					}else
					if(p_sActionId.contains("EditTemplateView")) //Go to create template page
					{	p_sActionId =  null;
						int templateSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
						WebUtils.setRequestAttribute(request,"mailTemplate",ProjSettingsPortalApp.getMailtemplateBySeqId(templateSeqId));
						nextPageUrl="email_template_edit_form.jsp";
					}else
					if(p_sActionId.contains("create")) //Create new Template
					{
						p_sActionId=null;
						nextPageUrl="email_template_form.jsp";
						oAppResponse = new PortalAppResponse(nextPageUrl);
						String templateName = WebUtils.getRequestParameter(request, "new_Template_Name", oMultipartContentRequest).trim();
						String templatePurpose =WebUtils.getRequestParameter(request, "new_Purpose", oMultipartContentRequest).trim();
						String templateBody =  WebUtils.getRequestParameter(request, "new_body", oMultipartContentRequest).trim();
						String templateSubject =  WebUtils.getRequestParameter(request, "new_Subject", oMultipartContentRequest).trim();
						
						if(!DataValidationUtils.isValidObjectName(templateName))
						{
								if(templateName.isEmpty())
									oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_MAIL_TEMPLATE_NAME_EMPTY);
								else
									oAppResponse.logError(WebErrorCodes.MailSettings. ERROR_MAIL_TEMPLATE_NAME_INVALID);
								return oAppResponse;
						}
						else
						if(templatePurpose.isEmpty())
						{
							oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_MAIL_TEMPLATE_PURPOSE_EMPTY);
							return oAppResponse;
						}
						else
						if(templateSubject.isEmpty())
						{
							oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_MAIL_TEMPLATE_SUBJECT_EMPTY);
							return oAppResponse;
						}
						else
						if(templateBody.isEmpty())
						{
							oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_MAIL_TEMPLATE_BODY_EMPTY);
							return oAppResponse;
						}
						else
						{
							mailTemplateList=MailConfigurationHelper.getAllEmailTemplates(l_projectid);
							for(MailTemplate o_mailTemplate :mailTemplateList )
							{
								if(o_mailTemplate.getTemplateName().equals(templateName))
								{
									oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_MAIL_TEMPLATE_NAME_NOT_UNIQUE);
									return oAppResponse;
								}
							}
							try {
								m_oMailTemplate.setProjectId(l_projectid);
								m_oMailTemplate.setCreationMode(CreationMode.SCRATCH);
								m_oMailTemplate.setState(RecordState.UNDER_CONFIGURATION);
								m_oMailTemplate.setTemplateName(templateName);
								m_oMailTemplate.setSubjectTemplate(templateSubject);
								m_oMailTemplate.setPurpose(templatePurpose);
								m_oMailTemplate.setBodyTemplate(templateBody);
								
								
								String succMsg=projectSettingsPortalApp.saveMailTemplate(p_oUserSession,l_projectid, m_oMailTemplate, u_oUserInfo, true);
								
								WebUtils.setRequestAttribute(request,"msg", succMsg);
								mailTemplateList=MailConfigurationHelper.getAllEmailTemplates(l_projectid);
								WebUtils.setRequestAttribute(request,"mailTemplateList", mailTemplateList);
								nextPageUrl="email_template.jsp";
								oAppResponse = new PortalAppResponse(nextPageUrl);
							}
							catch(SciException scExc)
							{
								log.error("error saving email template");
							}
						catch(SciServiceException sciExc)
						{
							log.error("error saving email template");
							oAppResponse.logError(sciExc);
						}
						return oAppResponse;
					}	
							
					}else
					if(p_sActionId.contains("editPage"))
					{
						int templateSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
						WebUtils.setRequestAttribute(request,"mailTemplate",ProjSettingsPortalApp.getMailtemplateBySeqId(templateSeqId));
						nextPageUrl="email_template_form.jsp";
					}
					else if(p_sActionId.contains("updateTemplate"))
					{
						int templateSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
						nextPageUrl="email_template_form.jsp";
						oAppResponse = new PortalAppResponse(nextPageUrl);
						
						if(WebUtils.getRequestParameter(request, "mail_Subject", oMultipartContentRequest).trim().isEmpty())
						{
							WebUtils.setRequestAttribute(request,"mailTemplate",ProjSettingsPortalApp.getMailtemplateBySeqId(templateSeqId));
							nextPageUrl="email_template_form.jsp"; 
							oAppResponse = new PortalAppResponse(nextPageUrl);	
							oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_MAIL_TEMPLATE_SUBJECT_EMPTY);
							return oAppResponse;
						}
						else
						if(WebUtils.getRequestParameter(request, "mail_body", oMultipartContentRequest).trim().isEmpty())
						{
							WebUtils.setRequestAttribute(request,"mailTemplate",ProjSettingsPortalApp.getMailtemplateBySeqId(templateSeqId));
							nextPageUrl="email_template_form.jsp"; 
							oAppResponse = new PortalAppResponse(nextPageUrl);	
							oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_MAIL_TEMPLATE_BODY_EMPTY);
							return oAppResponse;
						}
						else{
							List<MailTemplate> MailTempList= ProjSettingsPortalApp.getAllEmailTemplates(l_projectid);
							MailTemplate m_oMailTemp = null;
							
							for(MailTemplate mailTemplate: MailTempList)
							{	
								if(mailTemplate.getSequenceId()==templateSeqId)
								{
									m_oMailTemp= mailTemplate;
								}
							}
							try {
							if(DataUtils.hasChanged(m_oMailTemp.getSubjectTemplate(), WebUtils.getRequestParameter(request, "mail_Subject", oMultipartContentRequest).trim())||
									DataUtils.hasChanged(m_oMailTemp.getBodyTemplate(), WebUtils.getRequestParameter(request, "mail_body", oMultipartContentRequest).trim()))
							{
								String updateMsg = projectSettingsPortalApp.updateMailTemplate(l_projectid, WebUtils.getRequestParameter(request, "mail_Subject", oMultipartContentRequest).trim(),
								WebUtils.getRequestParameter(request, "mail_body", oMultipartContentRequest).trim(),p_oUserSession, u_oUserInfo, templateSeqId);
								WebUtils.setRequestAttribute(request,"msg", updateMsg);
								mailTemplateList=MailConfigurationHelper.getAllEmailTemplates(l_projectid);
								WebUtils.setRequestAttribute(request,"mailTemplateList", mailTemplateList);
								nextPageUrl="email_template.jsp";
							}
							else
							{
								WebUtils.setRequestAttribute(request,"mailTemplate",ProjSettingsPortalApp.getMailtemplateBySeqId(templateSeqId));
								nextPageUrl="email_template_form.jsp"; 
								oAppResponse = new PortalAppResponse(nextPageUrl);	
								oAppResponse.logError(WebErrorCodes.MailSettings.ERROR_UPDATED_VALUES_SAME_AS_EXISTING);
								return oAppResponse;
							}
							}catch(SciException sciExp)
							{
								log.error("Error updating mail template", sciExp);
							}
						
						oAppResponse = new PortalAppResponse(nextPageUrl);
						return oAppResponse;
						}
					}
					else if(p_sActionId.contains("changeStatus")) //Create new Template
					{
						int templateSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
						projectSettingsPortalApp.changeMailTemplateStatus(p_oUserSession,l_projectid,templateSeqId, RecordState.toEnum(Integer.parseInt(WebUtils.getRequestParameter(request, "statusValue", oMultipartContentRequest))), u_oUserInfo);
						WebUtils.setRequestAttribute(request,"mailTemplate",ProjSettingsPortalApp.getMailtemplateBySeqId(templateSeqId));
						nextPageUrl="email_template_edit_form.jsp"; 
					}
				}
				oAppResponse = new PortalAppResponse(nextPageUrl);
			}
			else if(sSettingsAppName.equalsIgnoreCase("PROJCET-USER-MANGEMENT"))
			{
				nextPageUrl="project_user_management.jsp";
				WebUtils.setRequestAttribute(request,"selectedProjectInfo", ProjManagePortalApp.getProjectInfoByProjectId(l_projectid));
				WebUtils.setRequestAttribute(request,"currentuserid", p_oUserSession.getUserInfo().getUserSeqId());
				List<ProjectUserRole> prjUserList= null;
				List<UserInfo> userList = null;
				try {
					prjUserList= projectSettingsPortalApp.projectUserListByProjectId(l_projectid);
					userList=projectSettingsPortalApp.UserListByProjectId(l_projectid);
				}
				catch(SciServiceException sciserexp)
				{
					log.error("Error fetching project list by project id");
					oAppResponse = new PortalAppResponse(nextPageUrl);	
					oAppResponse.logError(sciserexp);
					return oAppResponse;
				}
				WebUtils.setRequestAttribute(request,"projectUsersList", userList);
				
				if(p_sActionId!=null)
				{
					
					if(p_sActionId.contains("addUserView"))
					{
						List<UserInfo> assigneduserList = null;
						try {
							userList=ProjSettingsPortalApp.getAllUsersList();
							 assigneduserList = ProjSettingsPortalApp.getAllUsersList();
						}
						catch(SciServiceException sciserexp)
						{
							log.error("Error fetching all userlist");
							oAppResponse = new PortalAppResponse("project_user_management.jsp");	
							oAppResponse.logError(sciserexp);
							return oAppResponse;
						}
						
						if(prjUserList != null)
						{
							for (ProjectUserRole prjUserRole: prjUserList)
							{
								try {
									for(UserInfo user: userList)
									{	
										if(user.getUserSeqId()==(prjUserRole.getUserSeqId())|| user.getUserSeqId() == UserHome.retrieveBootstrapUser().getUserSeqId())
										{
											assigneduserList.remove(user);
										}
									}
								} catch (SciServiceException serExcep) {
									log.error("Error retrieving list of users", serExcep);
									oAppResponse = new PortalAppResponse(nextPageUrl);	
									oAppResponse.logError(serExcep);
									return oAppResponse;
								}
							}
							
							if(prjUserList.isEmpty())
							{
								try{
									assigneduserList.remove(ProjSettingsPortalApp.retrieveUserBySeqId(UserHome.retrieveBootstrapUser().getUserSeqId()));
									}
								catch(SciServiceException serExcep)
								{
									log.error("Error removing bootstrap user from userList", serExcep);
									oAppResponse = new PortalAppResponse(nextPageUrl);	
									oAppResponse.logError(serExcep);
									return oAppResponse;
								}
							}
						}
						WebUtils.setRequestAttribute(request,"usersList", assigneduserList);
						WebUtils.setRequestAttribute(request,"reportMessage", "No User Available");
						nextPageUrl="add_project_user.jsp";
					}else
					if(p_sActionId.contains("addUser"))
					{				
						if(request.getParameterValues("selectUser")==null)
						{
							List<UserInfo> assigneduserList = null;
							try {
								userList=ProjSettingsPortalApp.getAllUsersList();
								assigneduserList = ProjSettingsPortalApp.getAllUsersList();
							}
							catch(SciServiceException sciserexp)
							{
								log.error("Error fetching all userlist");
								oAppResponse = new PortalAppResponse("add_project_user.jsp");	
								WebUtils.setRequestAttribute(request,"usersList", assigneduserList);
								WebUtils.setRequestAttribute(request,"reportMessage", "No User Available");
								oAppResponse.logError(sciserexp);
								return oAppResponse;
							}
							for (ProjectUserRole prjUserRole: prjUserList)
							{
								for(UserInfo user: userList)
								{	if(user.getUserSeqId()==(prjUserRole.getUserSeqId())||user.getUserId().equalsIgnoreCase("@bootstrap_user@"))
									{
										assigneduserList.remove(user);
									}
								}
							}
							WebUtils.setRequestAttribute(request,"usersList", assigneduserList);
							WebUtils.setRequestAttribute(request,"reportMessage", "No User Available");
							nextPageUrl="add_project_user.jsp";
							oAppResponse = new PortalAppResponse(nextPageUrl);	
							oAppResponse.logError(WebErrorCodes.ProjectUserManagement.ERROR_USER_NOT_SELECTED);
							return oAppResponse;
						}else
						{
							String saveProjUser= null;
							String clientName = null;
							String clientEmailId = null;
							String clientSafetyDBId = null;
							ProjectUserRole o_ProjectUser=null;
							ArrayList<ProjectUserRole> projUser = new ArrayList<ProjectUserRole>();
							String [] userId = request.getParameterValues("selectUser");
							
							for(String pUser : userId)
							{
								
								clientName =  WebUtils.getRequestParameter(request, "__clientName"+pUser, oMultipartContentRequest).trim();
								clientEmailId = WebUtils.getRequestParameter(request, "__clientEmailId"+pUser, oMultipartContentRequest).trim();
								clientSafetyDBId = WebUtils.getRequestParameter(request, "__clientSafetyDBId"+pUser, oMultipartContentRequest).trim();
								
								oAppResponse = new PortalAppResponse("project_user_management.jsp");
								
								if(StringUtils.isNullOrEmpty(clientName))
								{
									oAppResponse.logError(WebErrorCodes.ProjectUserManagement.ERROR_USER_CLIENT_NAME_ERROR);
									return oAppResponse;
								}
								else if(StringUtils.isNullOrEmpty(clientEmailId))
								{
									oAppResponse.logError(WebErrorCodes.ProjectUserManagement.ERROR_USER_CLIENT_EMAIID_ERROR);
									return oAppResponse;
								}
								else if(StringUtils.isNullOrEmpty(clientSafetyDBId))
								{
									oAppResponse.logError(WebErrorCodes.ProjectUserManagement.ERROR_USER_CLIENT_SAFETYDBID_ERROR);
									return oAppResponse;
								}
								
								o_ProjectUser= new ProjectUserRole();
								o_ProjectUser.setM_sClientName(request.getParameter("__clientName"+pUser));
								o_ProjectUser.setM_sClientEmailId(request.getParameter("__clientEmailId"+pUser));
								o_ProjectUser.setM_sClientSafetyDBId(request.getParameter("__clientSafetyDBId"+pUser));
								o_ProjectUser.setUserSeqId(Integer.parseInt(pUser));
								projUser.add(o_ProjectUser);
								
							}
							
							try{
								saveProjUser=projectSettingsPortalApp.saveProjectUser(p_oUserSession, projUser , l_projectid,0, true);
							}
							catch(SciServiceException sciserexp)
							{
								log.error("Error saving Project User",sciserexp );
								nextPageUrl="add_project_user.jsp";
								oAppResponse = new PortalAppResponse("project_user_management.jsp");	
								WebUtils.setRequestAttribute(request,"selectedProjectInfo", ProjManagePortalApp.getProjectInfoByProjectId(l_projectid));
								WebUtils.setRequestAttribute(request,"projectUsersList", userList);
								oAppResponse.logError(WebErrorCodes.ProjectUserManagement.ERROR_USER_NOT_SELECTED);
								return oAppResponse;
							}
							WebUtils.setRequestAttribute(request,"msg", saveProjUser);
							nextPageUrl="project_user_management.jsp";
							try {
								prjUserList= projectSettingsPortalApp.projectUserListByProjectId(l_projectid);
								userList=projectSettingsPortalApp.UserListByProjectId(l_projectid);
							}
							catch(SciServiceException sciserexp)
							{
								log.error("Error fetching project list by project id");
								oAppResponse = new PortalAppResponse(nextPageUrl);	
								oAppResponse.logError(sciserexp);
								return oAppResponse;
							}
							WebUtils.setRequestAttribute(request,"projectUsersList", userList);
						}
					}else
					if(p_sActionId.contains("removeUser"))
					{
						String msg= null;
						nextPageUrl="project_user_management.jsp";
						try {
							msg= projectSettingsPortalApp.removeUser(p_oUserSession, l_projectid,Integer.parseInt(WebUtils.getRequestParameter(request,"selectedUserId", oMultipartContentRequest)));
						}
						catch(SciServiceException sciserexp)
						{
							log.error("Error removing user");
							oAppResponse = new PortalAppResponse(nextPageUrl);	
							oAppResponse.logError(sciserexp);
							return oAppResponse;
						}
						try {
							prjUserList= projectSettingsPortalApp.projectUserListByProjectId(l_projectid);
							userList=projectSettingsPortalApp.UserListByProjectId(l_projectid);
						}
						catch(SciServiceException sciserexp)
						{
							log.error("Error Fetching project user list by project id");
							oAppResponse = new PortalAppResponse(nextPageUrl);	
							oAppResponse.logError(sciserexp);
							return oAppResponse;
						}
						WebUtils.setRequestAttribute(request, "projectUsersList", userList);
						WebUtils.setRequestAttribute(request,"msg", msg);
						nextPageUrl="project_user_management.jsp";
					}
				}
				oAppResponse = new PortalAppResponse(nextPageUrl);
			}else if(sSettingsAppName.contains("PRJ-CONFIG"))
				{
					boolean updateSuccessFlag = false;
					String sSuccessMessage = null;
					String sOldValue = null;
					nextPageUrl="prjConfig.jsp";
					
					Map<String, Map<String, String>> map_proj_config = ConfigHome.getPrjConfigSetting(l_projectid); 
					
					if(p_sActionId.contains("prjsettings_update"))
					{
						String sNewValue = null;
						String sExistingValue = null;
						Map<String, String> projSettingMap = new HashMap<String, String>();
						Map<String, String> mapKeysToBeUpdated = new HashMap<String, String>();
						
						for (String sKey : map_proj_config.keySet())
						{
							projSettingMap = map_proj_config.get(sKey);
							for (String mKey : projSettingMap.keySet())
							{
								sNewValue = WebUtils.getRequestParameter(request, "__prjConfig"+mKey, oMultipartContentRequest);
								sExistingValue = projSettingMap.get(mKey);
								
								if ((sNewValue == null && sExistingValue == null) || (sNewValue!=null ? sNewValue.equals(sExistingValue) : false))
								{
									//No change
									if(log.isTraceEnabled())
									{
										log.trace("AppSettingsPortalWebApp.processAction: Value unchanged for key - {}", sKey);
									}
								}
								else
								{
									//TODO:Validate the data
									//Mark for update cycle
									mapKeysToBeUpdated.put(mKey, sNewValue);
								}
							}
						}
						
						for(String key : mapKeysToBeUpdated.keySet() ){
							
						try
						{	
							sSuccessMessage = ProjSettingsPortalApp.updateProjConfig( p_oUserSession,  key, mapKeysToBeUpdated.get(key), l_projectid);
							sOldValue = ConfigHome.getPrjConfigSettings(key, l_projectid);
							if(DataUtils.hasChanged(StringUtils.emptyString(sOldValue), StringUtils.emptyString(mapKeysToBeUpdated.get(key)))){
							updateSuccessFlag = true;
							}
						}
						catch(SciServiceException sciserexp)
						{
							log.error("Error updating projectconfig values");
							oAppResponse = new PortalAppResponse(nextPageUrl);	
							oAppResponse.logError(sciserexp);
							return oAppResponse;
						}
						}
						
					}
					
					if(updateSuccessFlag){
					WebUtils.setRequestAttribute(request, "updateSucccess", sSuccessMessage);
					}
					
//					ProjSettingsPortalApp.updatePrjConfig(mapKeysToBeUpdated)
					WebUtils.setRequestAttribute(request, "projconfigmap", map_proj_config);
					
					
					nextPageUrl="prjConfig.jsp";
					oAppResponse = new PortalAppResponse(nextPageUrl);
					
				} else if (sSettingsAppName.contains("MB-PORTAL-APP")){
				
					
					oAppResponse	=	new PortalAppResponse("email_template.jsp");
					
					oSettingsPortalWebApp = (sSettingsAppName != null) ? AppRegistry.getWebApp(sSettingsAppName) : null;
					oSettingsPortalApp = (sSettingsAppName != null) ? AppRegistry.getApp(sSettingsAppName) : null;
					
					if (oSettingsPortalWebApp != null) {
						oSettingsAppResponse	=	oSettingsPortalWebApp.processProjectSettingsAction(
														request
													, 	response
													, 	oMultipartContentRequest
													,	p_sActionId
													, 	p_oUserSession
													, 	listAppSpecificUserPreferences
													, 	oSettingsPortalApp
													);
						oAppResponse			=	null;
						oAppResponse	=	new PortalAppResponse(
												oSettingsAppResponse.getNextPageUri()
											, 	oSettingsAppResponse
											, 	oSettingsAppResponse.isDownloadStreamPresent()
											);
					}
					WebUtils.setRequestAttribute(request, "mb_appId", oSettingsPortalApp.getPortalAppId());
					return oAppResponse;
					
			}else
			{
				oAppResponse = new PortalAppResponse(getDefaultPageUri());
			}
		} else
		{
			oAppResponse = new PortalAppResponse(getDefaultPageUri());
		}
		return oAppResponse;
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) 
	{
		/*if(p_sActionId.equals("NGV_Protal_App"))
		{
			
			WebUtils.setRequestAttribute(request, "new-portalaction", "ngv_template_createnew_2");
			oAppResponse.logError(WebErrorCodes.System.INVALID_REQUEST_PARAMETER, p_sActionId);
			return "ngv_template_createnew";
		}*/
		return null;
	}
}