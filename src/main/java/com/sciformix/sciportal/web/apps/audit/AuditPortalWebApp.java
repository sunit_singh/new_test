package com.sciformix.sciportal.web.apps.audit;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.audit.AuditPortalApp;
import com.sciformix.sciportal.apps.audit.AuditPortalApp.AUDIT_FETCH_SCOPE;
import com.sciformix.sciportal.apps.audit.AuditPortalApp.AUDIT_FETCH_TYPE;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.project.ProjectHome;
import com.sciformix.sciportal.project.ProjectInfo;
import com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHome;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;

public class AuditPortalWebApp extends BasePortalWebApp
{
	private static final Logger log = LoggerFactory.getLogger(AuditPortalWebApp.class);
	
	public AuditPortalWebApp() 
	{
		super("AUDIT-PORTAL-APP", "auditportal.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
	}

	@Override
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response,
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession,
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) 
	{
		String				sNewAction 			=	null;
		String				l_usertype			=	null;
		PortalAppResponse	oAppResponse		=	null;
		List<String> 		listUsers			=	null;
		String 				sSpecifiedUserId	=	null;
		int 				nSpecifiedUserSeqId =	-1;
		List<AuditInfoDb> 	listAuditTrail 		=	null;
		UserInfo 			oUserInfo 			=	null;
		ProjectInfo			oProjectInfo		=	null;		
		String				sSpecifiedUserProjectName = null;
		int					nSpecifiedUserProjectId = -1;
		List<UserInfo>  	l_userlist 			= null;
		List<String>		projList 			= null;
		
		
		AuditPortalApp oMyPortalApp = (AuditPortalApp) oPortalApp;
		
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		
		projList = new  ArrayList<String>();
		
		log.debug("AuditPortalWebApp.processAction: Starting processing for Action - {} ", p_sActionId);
		
		if(p_sActionId == null)
		{
			sNewAction = null;
			
			try
			{
				
				if(p_oUserSession.getUserInfo().isProjectAuditor()&&(!p_oUserSession.getUserInfo().isAuditor())){
				
				projList = (p_oUserSession != null ? p_oUserSession.getAuthorizedProjects() : null);
				
				sSpecifiedUserProjectName = projList.get(0);
				
				if(!StringUtils.isNullOrEmpty(sSpecifiedUserProjectName)){
					
					oProjectInfo = ProjectHome.getProjectGivenProjectName(sSpecifiedUserProjectName);
					l_userlist = ProjectUserManagementHome.getProjectUserList(oProjectInfo.getProjectSequenceId());
					listUsers = new ArrayList<>();
					for(UserInfo user : l_userlist)
					{
						listUsers.add(user.getUserId());
					}
					
				}else{
					listUsers = UserHome.getListOfUserIds();
					listUsers.remove(UserHome.retrieveBootstrapUser().getUserId());
				}
				
				WebUtils.setRequestAttribute(request, "list_of_users", listUsers);
				
				}else{
				listUsers = UserHome.getListOfUserIds();
				listUsers.remove(UserHome.retrieveBootstrapUser().getUserId());
				WebUtils.setRequestAttribute(request, "list_of_users", listUsers);
				}
				
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error retrieving list of users", serExcep);
				oAppResponse.logError(serExcep);
			}
		}
		else if(p_sActionId.equals("fetch_results"))
		{
			sNewAction 	=	null;
			l_usertype	=	WebUtils.getRequestParameter(request, "auditusertype", oMultipartContentRequest);
			if (!StringUtils.isNullOrEmpty(l_usertype)) {
				if (l_usertype.equals("system")) {
					try {
						listAuditTrail = oMyPortalApp.getAuditForUser(p_oUserSession, AUDIT_FETCH_TYPE.SYSTEM, nSpecifiedUserSeqId, AUDIT_FETCH_SCOPE.LAST_7_DAYS, true,nSpecifiedUserProjectId);
					} catch (SciServiceException serExcep) {
						log.error("Error retrieving audit trail for user - {}", sSpecifiedUserId, serExcep);
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
				} else if (l_usertype.equals("all")) {
					sSpecifiedUserId = WebUtils.getRequestParameter(request, "select_criteria_user", oMultipartContentRequest);
					sSpecifiedUserProjectName = WebUtils.getRequestParameter(request, "select_criteria_project", oMultipartContentRequest);
				
					try
					{
						oUserInfo = UserHome.retrieveUser(sSpecifiedUserId, UserHome.UserPurpose.DATA_SEARCH);
						if (oUserInfo == null)
						{
							log.error("User not found for fetching audit - {}", sSpecifiedUserId);
							oAppResponse.logError(WebErrorCodes.Audit.ERROR_FETCHING_USER_INFO_FOR_AUDIT, sSpecifiedUserId);						
							return oAppResponse;
						}
						
						nSpecifiedUserSeqId = oUserInfo.getUserSeqId();
					}
					catch (SciServiceException serExcep)
					{
						log.error("Error retrieving list of users", serExcep);
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
					
					
					if(!StringUtils.isNullOrEmpty(sSpecifiedUserProjectName) && !sSpecifiedUserProjectName.equalsIgnoreCase("")) 
					{
						
							
//						nSpecifiedUserProjectId = Integer.parseInt(sSpecifiedUserProjectName);
						try
						{							
							oProjectInfo = ProjectHome.getProject(Integer.parseInt(sSpecifiedUserProjectName));
														
							if (oProjectInfo == null)
							{
								log.error("Project not found for fetching audit - {}", sSpecifiedUserProjectName);
								oAppResponse.logError(WebErrorCodes.Audit.ERROR_FETCHING_USER_INFO_FOR_AUDIT, sSpecifiedUserProjectName);
							
								return oAppResponse;
							}
														
							nSpecifiedUserProjectId = oProjectInfo.getProjectSequenceId();
						}
						catch (SciServiceException serExcep)
						{
							log.error("Error retrieving Project", serExcep);
							oAppResponse.logError(serExcep);
							return oAppResponse;
						}	
						
						
					}
					
					try
					{
						listAuditTrail = oMyPortalApp.getAuditForUser(p_oUserSession, AUDIT_FETCH_TYPE.USER, nSpecifiedUserSeqId, AUDIT_FETCH_SCOPE.LAST_7_DAYS, true,nSpecifiedUserProjectId);
						WebUtils.setRequestAttribute(request, "select_criteria_user", sSpecifiedUserId);
						WebUtils.setRequestAttribute(request, "select_criteria_project", sSpecifiedUserProjectName);
						
					}
					catch (SciServiceException serExcep)
					{
						log.error("Error retrieving audit trail for user - {}", sSpecifiedUserId, serExcep);
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
					
				} else {
					log.error("Wrong user type sent - {}", l_usertype);
					oAppResponse.logError(WebErrorCodes.System.INVALID_USERTYPE, null);
					return oAppResponse;
				}
				
				try {
					
					if(!StringUtils.isNullOrEmpty(sSpecifiedUserProjectName) && !sSpecifiedUserProjectName.equalsIgnoreCase("")){
						
						oProjectInfo = ProjectHome.getProject(Integer.parseInt(sSpecifiedUserProjectName));
						l_userlist = ProjectUserManagementHome.getProjectUserList(oProjectInfo.getProjectSequenceId());
						listUsers = new ArrayList<>();
						for(UserInfo user : l_userlist)
						{
							listUsers.add(user.getUserId());
						}
//						listUsers.remove(UserHome.retrieveBootstrapUser().getUserId());
						
					}else{
						listUsers = UserHome.getListOfUserIds();
						listUsers.remove(UserHome.retrieveBootstrapUser().getUserId());
					}
					
					
					WebUtils.setRequestAttribute(request, "list_of_users", listUsers);
					WebUtils.setRequestAttribute(request, "s_audit", listAuditTrail);
					WebUtils.setRequestAttribute(request, "auditusertype", l_usertype);
				} catch (SciServiceException serExcep) {
					log.error("Error fetching list of user ids", serExcep);
					oAppResponse.logError(serExcep);
					return oAppResponse;
				}
				WebUtils.setRequestAttribute(request, "selectedProject", sSpecifiedUserProjectName);
			}
			
			
			//TODO:Check sSpecifiedUserId is present
			
			
		}
		else if(p_sActionId.equals("getuserList"))
		{
			String p_name = WebUtils.getRequestParameter(request, "project_name", oMultipartContentRequest);
			ProjectInfo p_info  = null;
//			List<UserInfo>  l_userlist = null;
			
			if(!StringUtils.isNullOrEmpty(p_name) && !p_name.equals(""))
			{
				
				try{
					p_info = ProjectHome.getProject(Integer.parseInt(p_name));
					l_userlist = ProjectUserManagementHome.getProjectUserList(p_info.getProjectSequenceId());
					
				}
				catch(SciServiceException serExcep)
				{
					log.error("Error fetching list of project based user ids", serExcep);
					oAppResponse.logError(serExcep);
					return oAppResponse;
				}
				listUsers = null;
				listUsers = new ArrayList<>();
				for(UserInfo user : l_userlist)
				{
					listUsers.add(user.getUserId());
				}
				WebUtils.setRequestAttribute(request, "selectedProject", p_name);
			}
			else{
				try{
				listUsers = UserHome.getListOfUserIds();
				listUsers.remove(UserHome.retrieveBootstrapUser().getUserId());
				}
				catch(SciServiceException serExcep)
				{
					log.error("Error fetching list of user ids", serExcep);
					oAppResponse.logError(serExcep);
					return oAppResponse;
				}
			}
			WebUtils.setRequestAttribute(request, "list_of_users", listUsers);
		
		}
		else
		{
			//Do nothing
			log.error("AuditPortalWebApp.processAction: Unsupported Action - {}", p_sActionId);
			oAppResponse.logError(WebErrorCodes.System.INVALID_ACTION, null);
			return oAppResponse;
		}
		log.debug("AuditPortalWebApp.processAction: Completed processing for Action - {} ", p_sActionId);
		
		oAppResponse.setNextAction(sNewAction);
		return oAppResponse;
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}

}
