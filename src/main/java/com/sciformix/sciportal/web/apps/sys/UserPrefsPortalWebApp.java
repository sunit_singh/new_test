package com.sciformix.sciportal.web.apps.sys;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.AppInfo;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.apps.AppUserPreferenceMetadata;
import com.sciformix.sciportal.apps.AppUserPreferenceMetadata.PreferenceType;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.sys.UserPrefsPortalApp;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.user.UserUtils;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;

public class UserPrefsPortalWebApp extends BasePortalWebApp
{
	private static final Logger log = LoggerFactory.getLogger(UserPrefsPortalWebApp.class);
	private static final String USER_PROJECT_ID= "pref_user_default_project";
	
	public UserPrefsPortalWebApp()
	{
		super("USRPREF-PORTAL-APP", "userpref.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
	}
	
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp)
	{
		String sNewAction = null;
		UserPrefsPortalApp oMyPortalApp = null;
		PortalAppResponse oAppResponse = null;
		List<AppInfo> listApps = null;
		String sAppId = null;
		String	sPreferenceKey = null,
				sPreferenceKeyMultiple = null;
		String selectedprojectid = null;
		String sPreferenceNewValue = null;
		List<AppUserPreferenceMetadata> listAppUserPreferences = null;
		boolean bAtleastOncePreferenceValueUpdated = false;
		boolean bValueUpdated = false;
		boolean bErrorUpdatingPreferences = false;
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		
		oMyPortalApp = (UserPrefsPortalApp) oPortalApp;
		
		log.debug("UserPrefsPortalWebApp.processAction: Starting processing for Action - {}", p_sActionId);
		
		if (p_sActionId == null)
		{
			sNewAction = null;
			log.debug("UserPrefsPortalWebApp.processAction: Unsupported Action - {}", p_sActionId);
			
			/*try
			{
				populateInfoForDisplay(request, response, oMultipartContentRequest, oMyPortalApp, p_sActionId, p_oUserSession);
			}
			catch (SciServiceException serExcep) 
			{
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}*/
		}
		else if (p_sActionId.equalsIgnoreCase("userpref_save"))
		{
			listApps = AppRegistry.listBusinessApps();
			selectedprojectid = WebUtils.getRequestParameter(request, USER_PROJECT_ID, oMultipartContentRequest);
			
			if(!StringUtils.isNullOrEmpty(selectedprojectid))
			{	
				try
				{
					bValueUpdated = oMyPortalApp.updatePreferenceValue(p_oUserSession, UserUtils.PROJECT_MANAGEMENT_PORTAL_APP, UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID, selectedprojectid, -1);
					bAtleastOncePreferenceValueUpdated = bAtleastOncePreferenceValueUpdated || bValueUpdated;
				}
				catch (SciServiceException serExcep)
				{
					bErrorUpdatingPreferences = true;
					log.error("Error updating preference value", serExcep);
					oAppResponse.logError(WebErrorCodes.System.ERROR_SAVING_PREFERENCE);
				}
			}
			if(p_oUserSession.getAuthorizedProjectIds().isEmpty())
			{
				for(AppInfo oAppInfo : listApps)
				{
					int projectSeqId = -1;
						sAppId = oAppInfo.getAppId();
					oPortalApp = AppRegistry.getApp(sAppId);
					if (!oPortalApp.requiresUserPreferences())
					{
						continue;
					}
					
					//Application Enable/Disable state
					if (oPortalApp.allowsUserApplicationDisable())
					{
						//Read Portal-App State
						if(!oAppInfo.getPortalApp().applicationProjectSpecific())
						{
							sPreferenceKey = "__userprefenableapp_" + sAppId;
						}
						
						sPreferenceNewValue = WebUtils.getRequestParameter(request, sPreferenceKey, oMultipartContentRequest);
						
						if (sPreferenceNewValue != null && sPreferenceNewValue.equalsIgnoreCase("on"))
						{
							sPreferenceNewValue = UserUtils.PREFERENCE_VALUE_APPLICATION_ENABLED_YES;
						}
						else
						{
							sPreferenceNewValue = UserUtils.PREFERENCE_VALUE_APPLICATION_ENABLED_NO;
						}
						
						try
						{
							bValueUpdated = oMyPortalApp.updatePreferenceValue(p_oUserSession, sAppId, UserUtils.PREFERENCE_KEY_APPLICATION_ENABLED, sPreferenceNewValue, projectSeqId);
							bAtleastOncePreferenceValueUpdated = bAtleastOncePreferenceValueUpdated || bValueUpdated;
						}
						catch (SciServiceException serExcep)
						{
							bErrorUpdatingPreferences = true;
							log.error("Error updating preference value", serExcep);
							oAppResponse.logError(WebErrorCodes.System.ERROR_SAVING_PREFERENCE);
						}
					}
					
					if (bErrorUpdatingPreferences)
					{
						break;
					}
					
					//Other Application User Preferences
					listAppUserPreferences = oPortalApp.getAppUserPreference();
					for (AppUserPreferenceMetadata oAppUserPreference : listAppUserPreferences)
					{
						
						sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey();
						
						if (oAppUserPreference.getPreferenceType() == PreferenceType.ENUMERATED_MULTIPLE_VALUED) {
							sPreferenceKey	=	sPreferenceKey+"_multiple";
						}
						
						sPreferenceNewValue = WebUtils.getRequestParameter(request, sPreferenceKey, oMultipartContentRequest);
						try
						{
							bValueUpdated = oMyPortalApp.updatePreferenceValue(p_oUserSession, sAppId, oAppUserPreference.getPreferenceKey(), sPreferenceNewValue, projectSeqId);
							bAtleastOncePreferenceValueUpdated = bAtleastOncePreferenceValueUpdated || bValueUpdated;
						}
						catch (SciServiceException serExcep)
						{
							bErrorUpdatingPreferences = true;
							log.error("Error updating preference value", serExcep);
							oAppResponse.logError(WebErrorCodes.System.ERROR_SAVING_PREFERENCE);
						}
						
						if (bErrorUpdatingPreferences)
						{
							break;
						}
					}
					if (bErrorUpdatingPreferences)
					{
						break;
					}
				}
			}
			else 
			{
				for(ObjectIdPair<Integer,String> project : p_oUserSession.getAuthorizedProjectIds())
				{
					for(AppInfo oAppInfo : listApps)
					{
						int projectSeqId = -1;
							sAppId = oAppInfo.getAppId();
						oPortalApp = AppRegistry.getApp(sAppId);
						if (!oPortalApp.requiresUserPreferences())
						{
							continue;
						}
						
						//Application Enable/Disable state
						if (oPortalApp.allowsUserApplicationDisable())
						{
							//Read Portal-App State
							if(!oAppInfo.getPortalApp().applicationProjectSpecific())
							{
								sPreferenceKey = "__userprefenableapp_" + sAppId;
							}
							else
							{
								projectSeqId = project.getObjectSeqId();
								sPreferenceKey = "__userprefenableapp_" + sAppId+"_"+projectSeqId;
							}
							sPreferenceNewValue = WebUtils.getRequestParameter(request, sPreferenceKey, oMultipartContentRequest);
							if (sPreferenceNewValue != null && sPreferenceNewValue.equalsIgnoreCase("on"))
							{
								sPreferenceNewValue = UserUtils.PREFERENCE_VALUE_APPLICATION_ENABLED_YES;
							}
							else
							{
								sPreferenceNewValue = UserUtils.PREFERENCE_VALUE_APPLICATION_ENABLED_NO;
							}
							
							try
							{
								bValueUpdated = oMyPortalApp.updatePreferenceValue(p_oUserSession, sAppId, UserUtils.PREFERENCE_KEY_APPLICATION_ENABLED, sPreferenceNewValue, projectSeqId);
								bAtleastOncePreferenceValueUpdated = bAtleastOncePreferenceValueUpdated || bValueUpdated;
							}
							catch (SciServiceException serExcep)
							{
								bErrorUpdatingPreferences = true;
								log.error("Error updating preference value", serExcep);
								oAppResponse.logError(WebErrorCodes.System.ERROR_SAVING_PREFERENCE);
							}
						}
						
						if (bErrorUpdatingPreferences)
						{
							break;
						}
						
						//Other Application User Preferences
						listAppUserPreferences = oPortalApp.getAppUserPreference();
						for (AppUserPreferenceMetadata oAppUserPreference : listAppUserPreferences)
						{
							if(!oAppInfo.getPortalApp().applicationProjectSpecific())
							{
							sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey();
							}
							else
							{
								sPreferenceKey = "__userpref_" + sAppId + "_" + oAppUserPreference.getPreferenceKey()+"_"+project.getObjectSeqId();
							}
							
							if (oAppUserPreference.getPreferenceType() == PreferenceType.ENUMERATED_MULTIPLE_VALUED) {
								sPreferenceKey	=	sPreferenceKey+"_multiple";
							}
							
							sPreferenceNewValue = WebUtils.getRequestParameter(request, sPreferenceKey, oMultipartContentRequest);
							try
							{
								bValueUpdated = oMyPortalApp.updatePreferenceValue(p_oUserSession, sAppId, oAppUserPreference.getPreferenceKey(), sPreferenceNewValue, projectSeqId);
								bAtleastOncePreferenceValueUpdated = bAtleastOncePreferenceValueUpdated || bValueUpdated;
							}
							catch (SciServiceException serExcep)
							{
								bErrorUpdatingPreferences = true;
								log.error("Error updating preference value", serExcep);
								oAppResponse.logError(WebErrorCodes.System.ERROR_SAVING_PREFERENCE);
							}
							
							if (bErrorUpdatingPreferences)
							{
								break;
							}
						}
						if (bErrorUpdatingPreferences)
						{
							break;
						}
					}
					
					if (bAtleastOncePreferenceValueUpdated)
					{
						try
						{
							p_oUserSession.refreshUserPreferences();
						}
						catch (SciServiceException serExcep)
						{
							bErrorUpdatingPreferences = true;
							log.error("Error updating preference value", serExcep);
							oAppResponse.logError(WebErrorCodes.System.ERROR_SAVING_PREFERENCE);
						}
						p_oUserSession.getUserPreferences();
					}
					
					if (bErrorUpdatingPreferences)
					{
						sNewAction = "userpref_save";
					}
				}
			}	
			
			if (bAtleastOncePreferenceValueUpdated)
			{
				try
				{
					p_oUserSession.refreshUserPreferences();
				}
				catch (SciServiceException serExcep)
				{
					bErrorUpdatingPreferences = true;
					log.error("Error updating preference value", serExcep);
					oAppResponse.logError(WebErrorCodes.System.ERROR_SAVING_PREFERENCE);
				}
				p_oUserSession.getUserPreferences();
			}
			
			if (bErrorUpdatingPreferences)
			{
				sNewAction = "userpref_save";
			}
			
			//Done...
			sNewAction = null;
			/*try
			{
				populateInfoForDisplay(request, response, oMultipartContentRequest, oMyPortalApp, p_sActionId, p_oUserSession);
			}
			catch (SciServiceException serExcep) 
			{
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}*/
		}
		else
		{
			sNewAction = null;
			log.error("ProjSettingsPortalApp.processAction: Unsupported Action - {}", p_sActionId);
			oAppResponse.logError(WebErrorCodes.System.INVALID_ACTION, p_sActionId);
			return oAppResponse;
		}
		
		log.debug("UserPrefsPortalWebApp.processAction: Completed processing for Action - {}", p_sActionId);
		
		oAppResponse.setNextAction(sNewAction);
		return oAppResponse;
	}

	@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}
}
