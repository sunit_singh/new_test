package com.sciformix.sciportal.web.apps.ngv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.wink.json4j.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.rulesengine.RuleSetUtils;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.DataValidationUtils;
import com.sciformix.commons.utils.PdfUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.ngv.CaseDataValidationRule;
import com.sciformix.sciportal.apps.ngv.CaseDataValidationRule.Type;
import com.sciformix.sciportal.apps.ngv.CaseDataValidationRuleset;
import com.sciformix.sciportal.apps.ngv.NgvAppUtils;
import com.sciformix.sciportal.apps.ngv.NgvPortalApp;
import com.sciformix.sciportal.apps.ngv.NgvPortalApp.AppConfig;
import com.sciformix.sciportal.apps.ngv.NgvPortalApp.LocationToAdd;
import com.sciformix.sciportal.apps.ngv.NgvTemplate;
import com.sciformix.sciportal.apps.ngv.NgvTemplateRuleset;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.WebConstants;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class NgvPortalWebApp extends BasePortalWebApp
{
	private static final Logger log = LoggerFactory.getLogger(NgvPortalWebApp.class);
	
	public enum Steps
	{
		STEP_1, STEP_2, STEP_3;
	}
	
	
	public static final String NGV_TEMPLATE_TEXT_FILE = "ngv_addtpl_tpltext_file";
	public static final String NGV_REQUEST_PARAMETER_CASEID = "caseID";
	public static final String NGV_REQUEST_PARAMETER_SAFTEY_DB_NAME = "safety_db_name";
	public static final String NGV_CASEDATA_FILE = "ngv_casedata_file[]";
	private static final String QR_REQUEST_PARAMETER_PROJECT_ID			=	"fldprojectid";
	private static final String SAFETY_DB_RSS = "RSS";
	private static final String SAFETY_DB_ARGUS = "ARGUS";
	private static final String SAFETY_DB_SCEPTER = "SCEPTER";
	
	public NgvPortalWebApp()
	{
		super("NGV-PORTAL-APP", "autonarrative.jsp", ProjectApplicability.PROJECT_INDEPEDENT);
	}
	
	
	@SuppressWarnings({ "unchecked", "unused" })
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response, 
						MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
						List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp)
	{
		String sNewAction = null;
		NgvPortalApp oMyPortalApp = null;
		Set<Object>list =null;
		String sInputCaseId = null;
		String sSpecifiedSafetyDatabaseName = null;
		NgvTemplateRuleset oTemplateRuleset = null;
		CaseDataValidationRuleset oCaseValidationRuleset = null;
		String sNarrative = null;
		String sJsonRepresentation = "";
		Map<String, String> mapMissingMarker = null;
		String sCheckMissingMarker = null;
		String[] sMissingMarkerKeyValue = null;
		String[] sarrGetKeyValue =null;
		String sKeyValue = null;
		UploadedFileWrapper oUploadedFileObject = null;
		boolean bMissingMarkerCompletelyFilled = true;
		boolean bCheckMakerContainsSpecialChars = false;
		String sTemplateToUse = null;
		DataTable oDataTable = null;
		List<String> listFileNames = null;
		JSONArray jsonArray = null;
		PortalAppResponse oAppResponse = null;
		String sMandatoryColumnValue = null;
		String sMandatoryColumnsConfig = null;
		Iterator<Object>iterNarrativeObjects = null;
		Map<String, Object> mapTemplateMarkers = null;
		String sExpectedCaseIdPattern = null;
		List<UploadedFileWrapper> listUploadedFileObject = null;
		String[] sarrApplicableSafetyDatabases = null;
		List<String> listMissingMarker = null;
		List<String> listTemplateContentAndTemplateName = null;
		String sTemplateName = null;
		String sColumnsToCollapse = null;
		String sFilePath = null;
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		boolean bMultipleFileAllowledFlag = true;
		int nMultipleFileAllowedCount = 0;
		log.debug("NgvWebPortalApp.processAction: Starting processing for Action - {}", p_sActionId);
		
		oMyPortalApp = (NgvPortalApp) oPortalApp;
		
		
		if (StringUtils.isNullOrEmpty(p_sActionId))
		{   

			try
			{
				oTemplateRuleset = oMyPortalApp.getTemplateRuleset(p_oUserSession.getUserInfo());
				if(oTemplateRuleset==null)	
				{
					oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_NOT_CONFIGURED_IN_USERPREFS);
					return oAppResponse;
				}
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error fetching template Ruleset", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			//TODO:Following items need to be removed
			request.getSession().setAttribute("addMissingMarker", null);
			request.getSession().setAttribute("s_templateRuleset", null);
			request.getSession().setAttribute("s_missingMarkers", null);
			request.getSession().setAttribute("s_json", null);
		}
		
		else if (p_sActionId.equalsIgnoreCase("ngv_step2"))
		{
			mapTemplateMarkers = new HashMap<>();
	
			//TODO:Review this for previous step
			sNewAction = null;
			
			//Extract Form fields
			sInputCaseId = WebUtils.getRequestParameter(request, NGV_REQUEST_PARAMETER_CASEID, oMultipartContentRequest);
			
			if(StringUtils.isNullOrEmpty(sInputCaseId))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.CASEID_EMPTY);
				return oAppResponse;
			}
			
			//This is validating input caseID against regex(caseID should not contain any special character)
			sExpectedCaseIdPattern = oMyPortalApp.getAppConfigSetting(AppConfig.FILE_COLUMN_CASEID_REGEX);
			if (!DataValidationUtils.isCaseIdValid(sInputCaseId, sExpectedCaseIdPattern))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.CASEID_INVALID_PATTERN, null);
				return oAppResponse;
			}
			try
			{
				//DEV-NOTE: Retrieve User's Applicable Safety Database names
				sarrApplicableSafetyDatabases = oMyPortalApp.getListOfApplicableSafetyDatabasesForGivenUser(p_oUserSession.getUserInfo());
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error fetching safety databases", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			sSpecifiedSafetyDatabaseName = sarrApplicableSafetyDatabases.length>0?sarrApplicableSafetyDatabases[0]:null;
		
			if(StringUtils.isNullOrEmpty(sSpecifiedSafetyDatabaseName))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.SAFETYDB_EMPTY);
				return oAppResponse;
			}
			
			try
			{
				oTemplateRuleset = oMyPortalApp.getTemplateRuleset(p_oUserSession.getUserInfo());
				
				
				if(oTemplateRuleset.getState().compareTo(RecordState.DEACTIVATED) == 0)	
				{
					oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_IS_DEACTIVATED);
					return oAppResponse;
				}
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error fetching template Ruleset", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}				
			
			if(oTemplateRuleset == null)
			{
				oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_NOT_CONFIGURED_IN_USERPREFS);
				return oAppResponse;
			}
			
			try
			{
				oCaseValidationRuleset = oMyPortalApp.getCaseDataValidationRuleset(p_oUserSession.getUserInfo());
				
				
				if(oCaseValidationRuleset!=null && oCaseValidationRuleset.getState().compareTo(RecordState.DEACTIVATED) == 0)	
				{
					oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_IS_DEACTIVATED);
					return oAppResponse;
				}
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error fetching template Ruleset", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}				
			
			if(oCaseValidationRuleset == null)
			{
				oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_NOT_CONFIGURED_IN_USERPREFS);
				return oAppResponse;
			}
			//Extract form file fields
			try
			{
				listUploadedFileObject = WebUtils.stageRequestParameterFiles(request, NGV_CASEDATA_FILE, oMultipartContentRequest);
				bMultipleFileAllowledFlag = Boolean.valueOf(oMyPortalApp.getAppConfigSetting(AppConfig.MULTIPLE_FILE_UPLOAD_ALLOWED)==null?"false":oMyPortalApp.getAppConfigSetting(AppConfig.MULTIPLE_FILE_UPLOAD_ALLOWED));
				nMultipleFileAllowedCount = Integer.parseInt(oMyPortalApp.getAppConfigSetting(AppConfig.MULTIPLE_FILE_UPLOAD_MAX_COUNT)==null?"0":oMyPortalApp.getAppConfigSetting(AppConfig.MULTIPLE_FILE_UPLOAD_MAX_COUNT));
				if(listUploadedFileObject!=null && listUploadedFileObject.size() > 1 && bMultipleFileAllowledFlag == false)
				{
					oAppResponse.logError(WebErrorCodes.Ngv.MULTIPLE_FILE_ALLOWED_FLAG, ""+bMultipleFileAllowledFlag);
					return oAppResponse;
				}
				if(listUploadedFileObject!=null && listUploadedFileObject.size() > nMultipleFileAllowedCount)
				{
					oAppResponse.logError(WebErrorCodes.Ngv.MULTIPLE_FILE_MAX_COUNT, ""+nMultipleFileAllowedCount);
					return oAppResponse;
				}
			}
			catch(SciWebException webExcep)
			{
				log.error("Error in staging the uploaded file(s)", webExcep);
				oAppResponse.logError(WebErrorCodes.Ngv.CASE_FILE_NOT_UPLOADED);
				return oAppResponse;
			}
			
			oDataTable = new DataTable();
			listFileNames = new ArrayList<String>();
			for(int i = 0;i < listUploadedFileObject.size(); i++)
			{	
				oUploadedFileObject = listUploadedFileObject.get(i);
				if (oUploadedFileObject.isFileAvailable())
				{
					try
					{
						// Read contents into Table-of-tables
						sFilePath = oUploadedFileObject.getStagedFilePath();
						oDataTable = oMyPortalApp.extractData(sInputCaseId, oUploadedFileObject.getStagedFilePath(), oDataTable, sSpecifiedSafetyDatabaseName);
						listFileNames.add(oUploadedFileObject.getOriginalFileName());
						if(log.isTraceEnabled())
						{
							log.trace("Dumping file="+oUploadedFileObject.getOriginalFileName());
							oDataTable.dumpContents();
						}
					}
					catch(SciServiceException serExcep)
					{
						log.error("Exception parsing provided case file", serExcep);
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
					finally
					{
						oUploadedFileObject.delete();
					}
				}
				else
				{
					oAppResponse.logError(WebErrorCodes.Ngv.CASE_FILE_NOT_AVAILABLE);
					return oAppResponse;
				}
				
			}
			//Data Validation comes here
			//Check 1: Case Id column - Already done earlier
			
			//Check 2: Mandatory columns
			if(sSpecifiedSafetyDatabaseName.equals(SAFETY_DB_ARGUS) && PdfUtils.checkIfPdfFile(sFilePath))
			{
				sColumnsToCollapse = oMyPortalApp.getAppConfigSetting(AppConfig.PDFXML_SKIP_COLUMN_COLLAPSE);
				sMandatoryColumnsConfig = oMyPortalApp.getAppConfigSetting(AppConfig.FILE_MANDATORY_COLUMN_ARGUS);
			}else
			{
				sColumnsToCollapse = oMyPortalApp.getAppConfigSetting(AppConfig.SKIP_COLUMNS_COLLAPSE);
				sMandatoryColumnsConfig = oMyPortalApp.getAppConfigSetting(AppConfig.FILE_MANDATORY_COLUMN);
			}
			sMandatoryColumnValue = NgvAppUtils.doMandatoryColumnsExistWithValues(oDataTable, sMandatoryColumnsConfig);
			if(!StringUtils.isNullOrEmpty(sMandatoryColumnValue))
			{
				log.info("#Activity-Trail-NGV#Status:Error;Date:"+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+";User:"+p_oUserSession.getUserInfo().getUserId() + ";TemplateRuleset:" + oTemplateRuleset.getRulesetName() + ";TemplateUsed:"+sTemplateName
						+ ";CaseId:"+sInputCaseId +";FileName:"+listFileNames+";ErrorDetail:"+WebErrorCodes.Ngv.MANDATORY_COLUMNS_ABSENT+";PayLoadM:"+sMandatoryColumnValue);
				oAppResponse.logError(WebErrorCodes.Ngv.MANDATORY_COLUMNS_ABSENT, null);
				return oAppResponse;
			}
			
			//Check 3: Columns mandated to have data
			//TODO:
			
			//Check 4: Custom Data Validation Rules
			//TODO:
				
			if(log.isTraceEnabled())
			{
				log.trace("-------------------------Dumping all file content---------------------");
				oDataTable.dumpContents();
			}
			
			//Proceed with template replacement
			//Create jsonArray from oDataTable
			if(oCaseValidationRuleset.getValidationRules().size()<=0)
			{
				oAppResponse.logError(WebErrorCodes.Ngv.VALIDATION_RULES_NOT_CONFIGURED,null);
				return oAppResponse;
			}
			try
			{
				
				jsonArray = oDataTable.createJsonRepresentation(sColumnsToCollapse);

				sJsonRepresentation = jsonArray.toString();

				Map<String, List<String>> CaseValidationMap = new HashMap<String, List<String>>();
				
				Map<String, List<String>> retHmap = oMyPortalApp.validateRule(p_oUserSession.getUserInfo(), oCaseValidationRuleset, sJsonRepresentation,oAppResponse);
				
				CaseValidationMap = sortByKey(retHmap);
				
				boolean validFlag = false;
				
				boolean errorFlag = false;

				if(retHmap.size() > 0 )
				{
					Set<String> keys = retHmap.keySet();

					List<String> listofCases;

					for(Entry<String, List<String>> entry : CaseValidationMap.entrySet())
					{
						String key = entry.getKey();

						listofCases = entry.getValue();
						
						

						if(key.equalsIgnoreCase("VALID-CASE-NOTSATISFIED"))
						{
							listofCases = entry.getValue();

							oAppResponse.logError(WebErrorCodes.Ngv.CASE_VALIDATION_FAILED_FOR_VALID_CASE, null);
							
							validFlag = true;
							log.info("#Activity-Trail-NGV#Status:Error;Date:"+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+";User:"+p_oUserSession.getUserInfo().getUserId() + ";TemplateRuleset:" + oTemplateRuleset.getRulesetName() + ";TemplateUsed:"+sTemplateName
									+ ";CaseId:"+sInputCaseId +";FileName:"+listFileNames+";ErrorDetail:"+WebErrorCodes.Ngv.CASE_VALIDATION_FAILED_FOR_VALID_CASE+";PayLoadV:"+listofCases);
						}
//						else if(key.equalsIgnoreCase("VALID-CASE-NOTSATISFIED"))
//						{
//							listofCases = entry.getValue();
//
//							oAppResponse.logError(WebErrorCodes.Ngv.CASE_VALIDATION_FAILED_FOR_ERROR_CASE, "Mismatch found due to these erorr rules "+listofCases.toString());
//							
//							errorFlag = true;
//
//						}
						else if(key.equalsIgnoreCase("ERROR-CASE-SATISFIED"))
						{
							listofCases = entry.getValue();

							oAppResponse.logError(WebErrorCodes.Ngv.CASE_VALIDATION_FAILED_FOR_ERROR_CASE, null);
							
							errorFlag = true;
							log.info("#Activity-Trail-NGV#Status:Error;Date:"+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+";User:"+p_oUserSession.getUserInfo().getUserId() + ";TemplateRuleset:" + oTemplateRuleset.getRulesetName() + ";TemplateUsed:"+sTemplateName
									+ ";CaseId:"+sInputCaseId +";FileName:"+listFileNames+";ErrorDetail"+WebErrorCodes.Ngv.CASE_VALIDATION_FAILED_FOR_VALID_CASE+";PayLoadV:"+listofCases);

						}
						
						
//						else if(key.equalsIgnoreCase("ERROR-CASE-NOTSATISFIED"))
//						{
//							listofCases = entry.getValue();
//
//							oAppResponse.logError(WebErrorCodes.Ngv.CASE_VALIDATION_FAILED_FOR_ERROR_CASE, "Mismatch found due to these erorr rules "+listofCases.toString());
//							
//							errorFlag = true;
//
//						}
						else if(key.equalsIgnoreCase("WARN-CASE-SATISFIED") && !((validFlag == true && errorFlag == true) || (validFlag == true) || (errorFlag == true) ))
						{
							listofCases = entry.getValue();

							WebUtils.setRequestAttribute(request, "WarnSatisfiedCase", listofCases);
							log.info("#Activity-Trail-NGV#Status:Warning;Date:"+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+";User:"+p_oUserSession.getUserInfo().getUserId() + ";TemplateRuleset:" + oTemplateRuleset.getRulesetName() + ";TemplateUsed:"+sTemplateName
									+ ";CaseId:"+sInputCaseId +";FileName:"+listFileNames+";PayLoadV:"+listofCases);


						}
						else if(key.equalsIgnoreCase("WARN-CASE-NOTSATISFIED") && !((validFlag == true && errorFlag == true) || (validFlag == true) || (errorFlag == true) ))
						{
							listofCases = entry.getValue();

							WebUtils.setSessionAttribute(request, "WarnNotSatisfiedCase", listofCases);
							log.info("#Activity-Trail-NGV#Status:Warning;Date:"+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+";User:"+p_oUserSession.getUserInfo().getUserId() + ";TemplateRuleset:" + oTemplateRuleset.getRulesetName() + ";TemplateUsed:"+sTemplateName
									+ ";CaseId:"+sInputCaseId +";FileName:"+listFileNames+";PayLoadV:"+listofCases);

						}


					}
					if((validFlag == true && errorFlag == true) || (validFlag == true) || (errorFlag == true) )
					{
						//TODO:PayloadV
						return oAppResponse;
					}


				}
				
				


			}
			
			
			catch (SciServiceException serExcep) 
			{
			    if(serExcep.errorCode().equals(ServiceErrorCodes.Ngv.CASE_VALIDATION_FAILED_DUE_TO_COLUMN_MISSING))
			    {
			    	oAppResponse.logError(WebErrorCodes.Ngv.COLUMNS_MISSING_IN_CASE_FILE, null);
			    	return oAppResponse;
			    }
			    else
			    {
					log.error("Error parsing file", serExcep);
					return oAppResponse;
			    }
			} 
			catch(SciDbException scidbexp)
			{
				log.error("Db error");
			}
			catch (SciException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
				return oAppResponse;
			}
			
			
			
			try
			{
				listTemplateContentAndTemplateName = oMyPortalApp.identifyTemplateToUse(p_oUserSession.getUserInfo(), oTemplateRuleset, sJsonRepresentation, null);
				sTemplateToUse = listTemplateContentAndTemplateName!=null && listTemplateContentAndTemplateName.size()==2 ?listTemplateContentAndTemplateName.get(0):"";
				sTemplateName = listTemplateContentAndTemplateName!=null && listTemplateContentAndTemplateName.size()==2 ?listTemplateContentAndTemplateName.get(1):"";
				// log.debug("Template content: \n {}", sTemplateToUse);
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error identifying template ", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
			if(StringUtils.isNullOrEmpty(sTemplateToUse))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_CONFIGURED);
				return oAppResponse;
			}
			
			try
			{
				list = oMyPortalApp.generateNarrative(oTemplateRuleset, sTemplateToUse, oDataTable, sJsonRepresentation, null);
				if(list.size() == 3)
				{
					iterNarrativeObjects = list.iterator();
					mapTemplateMarkers = (Map<String, Object>) iterNarrativeObjects.next();
					sNarrative = (String) iterNarrativeObjects.next();
					listMissingMarker =  (List<String>) iterNarrativeObjects.next();
				}
				else
				{
					log.info("#Activity-Trail-NGV#Status:Error;Date:"+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+";User:"+p_oUserSession.getUserInfo().getUserId() + ";TemplateRuleset:" + oTemplateRuleset.getRulesetName() + ";TemplateUsed:"+sTemplateName
							+ ";CaseId:"+sInputCaseId +";FileName:"+listFileNames+";ErrorDetail"+WebErrorCodes.Ngv.CASE_FILE_PARSING_FAILED);
					oAppResponse.logError(WebErrorCodes.Ngv.CASE_FILE_PARSING_FAILED);
					return oAppResponse;
				}
				
				sNarrative = oMyPortalApp.removeSpecialCharactersFromNarrative(sNarrative);
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error generating narrative", serExcep);
				oAppResponse.logError(serExcep);;
				return oAppResponse;
			}
			WebUtils.setRequestAttribute(request, "s_templateName", sTemplateName);
			WebUtils.setRequestAttribute(request, "s_narrative", sNarrative);
			WebUtils.setRequestAttribute(request, "r_missingMarkerList", listMissingMarker);
			WebUtils.setRequestAttribute(request, "r_caseId", sInputCaseId);
			WebUtils.setSessionAttribute(request, "r_fileNameList", listFileNames);
			//TODO:Output sTemplateToUse
			WebUtils.setRequestAttribute(request, "r_dbName", sSpecifiedSafetyDatabaseName);
			request.getSession().setAttribute("s_templateRuleset", oTemplateRuleset);
			request.getSession().setAttribute("s_templateMarker", mapTemplateMarkers);
			
			WebUtils.setSessionAttribute(request, "s_templateRuleset", oTemplateRuleset);
			WebUtils.setSessionAttribute(request, "s_json", sJsonRepresentation);
		
			sNewAction = "ngv_step2";
			log.info("#Activity-Trail-NGV#Status:Success;Date:"+new SimpleDateFormat("dd/MM/yyyy").format(new Date())+";User:"+p_oUserSession.getUserInfo().getUserId() + ";TemplateRuleset:" + oTemplateRuleset.getRulesetName() + ";TemplateUsed:"+sTemplateName
					+ ";CaseId:"+sInputCaseId +";FileName:"+listFileNames+";NarrativeLength:"+sNarrative.length()+";MissingMarkerList:"+listMissingMarker);
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_step3"))
		{
			mapTemplateMarkers = new HashMap<>();
			
			sCheckMissingMarker = (String) WebUtils.getRequestParameter(request, WebConstants.REQUEST_PARAMETER_MISSINGMARKER, oMultipartContentRequest);
			if(!StringUtils.isNullOrEmpty(sCheckMissingMarker))
			{
				mapTemplateMarkers = (Map<String, Object>) WebUtils.getSessionAttribute(request, "s_templateMarker");
				sJsonRepresentation = (String) WebUtils.getSessionAttribute(request, "s_json");
				oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "s_templateRuleset");
				
				//Assuming WebConstants.REQUEST_PARAMETER_MISSINGMARKER value is  not null
				sMissingMarkerKeyValue = sCheckMissingMarker.split(",");
				mapMissingMarker = (LinkedHashMap<String, String>) WebUtils.getSessionAttribute(request, "s_missingMarkers");
			
				try 
				{
					if(mapMissingMarker == null)
					{
						mapMissingMarker = new LinkedHashMap<>();
					}
					
					for(int i=0; i<sMissingMarkerKeyValue.length; i++)
					{
						sarrGetKeyValue = sMissingMarkerKeyValue[i].split("=");
						sKeyValue = sarrGetKeyValue.length==2?sarrGetKeyValue[1]:"";
						if(sKeyValue.equals(""))
						{
							bMissingMarkerCompletelyFilled = false;
						}
						mapMissingMarker.put(sarrGetKeyValue[0], sKeyValue);
						mapTemplateMarkers.put(sarrGetKeyValue[0], sKeyValue);
					}
					bCheckMakerContainsSpecialChars = oMyPortalApp.checkIfMarkerContainsSpecialChars(mapMissingMarker);	
					//TODO:Girish: list = UseNarrativeTemplate.generateNarrative(oTemplateRuleset, null, listErrors, mapTemplateMarkers);
					listTemplateContentAndTemplateName = oMyPortalApp.identifyTemplateToUse(p_oUserSession.getUserInfo(), oTemplateRuleset, null, mapTemplateMarkers);
					
					sTemplateToUse = listTemplateContentAndTemplateName!=null && listTemplateContentAndTemplateName.size()==2 ?listTemplateContentAndTemplateName.get(0):"";
					sTemplateName = listTemplateContentAndTemplateName!=null && listTemplateContentAndTemplateName.size()==2 ?listTemplateContentAndTemplateName.get(1):"";
					
					list = oMyPortalApp.generateNarrative(oTemplateRuleset, sTemplateToUse, oDataTable, null, mapTemplateMarkers);
					Iterator<Object>it = list.iterator();						
					mapTemplateMarkers = (Map<String, Object>) it.next();
					sNarrative = (String) it.next();
					sNarrative = oMyPortalApp.removeSpecialCharactersFromNarrative(sNarrative);
					
					WebUtils.setRequestAttribute(request, "r_missingMarkerFlag", Boolean.toString(bMissingMarkerCompletelyFilled));
					WebUtils.setRequestAttribute(request, "r_caseId", WebUtils.getSessionAttribute(request, "r_caseId"));
					WebUtils.setRequestAttribute(request, "r_dbName", WebUtils.getSessionAttribute(request, "r_dbName"));
					// WebUtils.setRequestAttribute(request, "r_fileNameList", WebUtils.getSessionAttribute(request, ""));
					
					WebUtils.setRequestAttribute(request, "s_templateName", sTemplateName);
					WebUtils.setRequestAttribute(request, "s_narrative", sNarrative);
					WebUtils.setSessionAttribute(request, "s_templateMarker", mapTemplateMarkers);
					WebUtils.setSessionAttribute(request, "s_missingMarkers", mapMissingMarker);
					
					//TODO:Review this code
					/*if(listErrors.size() > 0)
					{
						WebUtils.setRequestAttribute(request, "s_narrative", sNarrative);
						WebUtils.setSessionAttribute(request, "s_templateMarker", mapTemplateMarkers);
						WebUtils.setSessionAttribute(request, "s_missingMarkers", mapMissingMarker);
					}
					else
					{
						WebUtils.setRequestAttribute(request, "s_narrative", sNarrative);
						WebUtils.setSessionAttribute(request, "s_missingMarkers", mapMissingMarker);
						WebUtils.setSessionAttribute(request, "r_copyNarrative", "true");
					}*/
				} 
				catch (SciServiceException serExcep) 
				{
					log.error("Error replacing markers", serExcep);
					oAppResponse.logError(serExcep);
					return oAppResponse;
				}
				WebUtils.setRequestAttribute(request, "s_narrative", sNarrative);
			}
			log.error("#Activity Trail NGV#", "Date is "+new Date().toString()+" User is "+p_oUserSession.getUserInfo().getUserId() + " Template Ruleset used " + oTemplateRuleset.getRulesetName() + " Template Used "+sTemplateName
				+ " Case Id is "+WebUtils.getSessionAttribute(request, "r_caseId"));
		}
		else
		{
			sNewAction = null;
			log.debug("NgvWebPortalApp.processAction: Unsupported Action - {}", p_sActionId);
		}
		
		log.debug("NgvWebPortalApp.processAction: Completed processing for Action - {}", p_sActionId);
		
		oAppResponse.setNextAction(sNewAction);
		return oAppResponse;
	}
	public static Map<String, List<String>> sortByKey(Map<String, List<String>> unsortedMap){
	    Map<String, List<String>> sortedMap = new TreeMap<String, List<String>>();
	    sortedMap.putAll(unsortedMap);
	    return sortedMap;
	  }
	
	public PortalAppResponse processProjectSettingsAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp)
	{
		String sNewAction = null;
		String sTemplateXml = null;
		NgvPortalApp oMyPortalApp = null;
		PortalAppResponse oAppResponse = null;
		NgvTemplateRuleset oTemplateRuleset = null;
		CaseDataValidationRuleset oCaseValidationRuleSet = null;
		FileWriter oFileWriter =null;
		File oTemplateExportFile = null;
		PrintWriter oExportFileWriter = null;
		FileInputStream oFileInputStream = null;
		String sTemplateRulesetName = null;
		ConfigTplObjType sRuleSetType = null;
		int nTemplateRulesetId = -1;
		int CaseValidationRuleSetId = -1;
		String sTemplateHeader = null;
		int nDefaultTemplateId = -1;
		int nSelectedTemplateId = -1;
		List<NgvTemplate> listTemplates = null;
		List<CaseDataValidationRule> listRules = null;
		PortalAppResponse oImportResponse = null;
		
		
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		oMyPortalApp = (NgvPortalApp) oPortalApp;
		
		if (StringUtils.isNullOrEmpty(p_sActionId))
		{
			//No specific action to be undertaken, simply prepare for display
			try
			{
				
				sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfRulesets(request, response, oMultipartContentRequest, oMyPortalApp, p_sActionId, p_oUserSession, 
						listAppSpecificUserPreferences, oPortalApp);
				
			}
			catch (SciServiceException serExcep) 
			{
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
		}
		else if(p_sActionId.equalsIgnoreCase("validate"))
		{
			try
			{
				
				sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfValidationRulesets(request, response, oMultipartContentRequest, oMyPortalApp, p_sActionId, p_oUserSession, 
					listAppSpecificUserPreferences, oPortalApp);
				System.out.println("");
			}
			catch (SciServiceException serExcep) 
			{
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_template_createnew"))
		{
			//Nothing special to do here, simply redirect to next page
			sNewAction = "ngv_template_createnew_2";
		
		}
		else if(p_sActionId.equalsIgnoreCase("ngv_validate_createnew"))
		{
			sNewAction = "ngv_validate_createnew_2";
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_templateruleset_export"))
		{
			oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
			
			if (oTemplateRuleset == null)
			{
				sNewAction = null;
				//Display main page with error
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			//TODO:Export as file
			
			sTemplateXml =  oTemplateRuleset.createXml();
			
			try
			{
				oTemplateExportFile = new File(WebUtils.createStagingFile(WebUtils.getUserSession(request), "template.xml"));
				oFileWriter = new FileWriter(oTemplateExportFile);
				oTemplateExportFile.createNewFile();
				
				oFileWriter.write(sTemplateXml);
				oFileWriter.close();
				
				oExportFileWriter = response.getWriter();  
				response.setContentType("text/xml");  
				response.setHeader("Content-Disposition","attachment; filename=\"" + oTemplateExportFile.getName() + "\""); 
				oFileInputStream = new FileInputStream(oTemplateExportFile);  
	            
				int i;   
				while ((i = oFileInputStream.read()) != -1) 
				{  
					oExportFileWriter.write(i);   
				}
			}
			catch(Exception excep)
			{
				log.error("Error during template export", excep);
			    oAppResponse.logError(WebErrorCodes.Ngv.ERROR_EXPORTING_FILE);
				return oAppResponse;
			}
			finally
			{
				try 
				{
					// request.setAttribute("r_downloadTemplate", "yes");
					oAppResponse.setIsDownloadStreamPresent(true);
					//log.debug("response state after written - {} ", response.isCommitted());
					response.flushBuffer();
				    
					if(oExportFileWriter != null)
				   	{
				    	oExportFileWriter.close();
				   	}
				   	
				    if(oFileInputStream != null)
				   	{
				    	oFileInputStream.close();
				   	}
				}
				catch (Exception excep) 
				{
					log.error("Error during template export", excep);
				    oAppResponse.logError(WebErrorCodes.Ngv.ERROR_EXPORTING_FILE);
					return oAppResponse;
				}
			}
			if(log.isTraceEnabled())
			{
				log.trace("XML-OUTPUT: {}", sTemplateXml);
			}
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleTemplateRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
					listAppSpecificUserPreferences, oPortalApp, oTemplateRuleset);
		}
		
		else if (p_sActionId.equalsIgnoreCase("ngv_templateruleset_changestate"))
		{
			oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
			
			if (oTemplateRuleset == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			if (oTemplateRuleset.getState() == RecordState.UNDER_CONFIGURATION)
			{
				if (!NgvAppUtils.canRulesetBeActivated(oTemplateRuleset))
				{
					//Display main page with error
					try {
						sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfRulesets(request, response, oMultipartContentRequest, oMyPortalApp, p_sActionId, p_oUserSession, 
								listAppSpecificUserPreferences, null);
					} 
					catch (SciServiceException serExcep) 
					{
						oAppResponse.logError(serExcep);
						return oAppResponse;
					}
					oAppResponse.logError(WebErrorCodes.Ngv.RULESET_CANNOT_BE_ACTIVATED, null);
					return oAppResponse;
				}
				
				try
				{
					oMyPortalApp.updateTemplateRulesetState(p_oUserSession, oTemplateRuleset, RecordState.ACTIVE);
				}
				catch (SciServiceException serExcep) 
				{
					log.error("Error updating template ruleset state", serExcep);
				    oAppResponse.logError(serExcep);
					return oAppResponse;
				}
			}
			else if (oTemplateRuleset.getState() == RecordState.ACTIVE)
			{
				if (!NgvAppUtils.canRulesetBeDeleted(oTemplateRuleset))
				{
					//Display main page with error
					sNewAction = null;
					oAppResponse.logError(WebErrorCodes.Ngv.RULESET_CANNOT_BE_DEACTIVATED, null);
					return oAppResponse;
				}
				
				try
				{
					oMyPortalApp.updateTemplateRulesetState(p_oUserSession, oTemplateRuleset, RecordState.DEACTIVATED);
				}
				catch (SciServiceException serExcep)
				{
					log.error("Error updating template ruleset state", serExcep);
				    oAppResponse.logError(serExcep);
					return oAppResponse;
				}
			}
			else
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.ERROR_UPDATING_RULESET_STATE, null);
				return oAppResponse;
			}
			
			WebUtils.setSessionAttribute(request, "ngv_template_ruleset", oTemplateRuleset);
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleTemplateRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
								listAppSpecificUserPreferences, oPortalApp, oTemplateRuleset);
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_validateruleset_changestate"))
		{
			oCaseValidationRuleSet = (CaseDataValidationRuleset) WebUtils.getSessionAttribute(request, "ngv_validate_ruleset");
			
			if (oCaseValidationRuleSet == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			if (oCaseValidationRuleSet.getState() == RecordState.UNDER_CONFIGURATION)
			{
				if (!NgvAppUtils.canRulesBeActivated(oCaseValidationRuleSet))
				{
					//Display main page with error
					sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleValidationRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
							listAppSpecificUserPreferences, oPortalApp, oCaseValidationRuleSet);
					oAppResponse.logError(WebErrorCodes.Ngv.CASE_VALIDATION_RULESET_CANNOT_BE_ACTIVATED, null);
					oAppResponse.setNextAction(sNewAction);
					return oAppResponse;
				}
				
				try
				{
					oMyPortalApp.updateValidationRulesetState(p_oUserSession, oCaseValidationRuleSet, RecordState.ACTIVE);
				}
				catch (SciServiceException serExcep) 
				{
					log.error("Error updating case validation ruleset state", serExcep);
				    oAppResponse.logError(serExcep);
					return oAppResponse;
				}
			}
			else if (oCaseValidationRuleSet.getState() == RecordState.ACTIVE)
			{
				if (!NgvAppUtils.canRulesetBeDeleted(oCaseValidationRuleSet))
				{
					//Display main page with error
					sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleValidationRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
							listAppSpecificUserPreferences, oPortalApp, oCaseValidationRuleSet);
					oAppResponse.logError(WebErrorCodes.Ngv.CASE_VALIDATION_RULESET_CANNOT_BE_DEACTIVATED, null);
					oAppResponse.setNextAction(sNewAction);
					return oAppResponse;
				}
				
				try
				{
					oMyPortalApp.updateValidationRulesetState(p_oUserSession, oCaseValidationRuleSet, RecordState.DEACTIVATED);
				}
				catch (SciServiceException serExcep)
				{
					log.error("Error updating case validation ruleset state", serExcep);
				    oAppResponse.logError(serExcep);
					return oAppResponse;
				}
			}
			else
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.ERROR_UPDATING_RULESET_STATE, null);
				return oAppResponse;
			}
			
			WebUtils.setSessionAttribute(request, "ngv_validate_ruleset", oCaseValidationRuleSet);
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleValidationRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
								listAppSpecificUserPreferences, oPortalApp, oCaseValidationRuleSet);
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_template_createnew_3"))
		{
			sTemplateRulesetName = WebUtils.getRequestParameter(request, "ngv_addtpl_tplrulesetname", oMultipartContentRequest);
			//TODO: Relook
			WebUtils.setRequestAttribute(request, "ngv_addtpl_tplrulesetname", sTemplateRulesetName);
			
			if (!DataValidationUtils.isValidObjectName(sTemplateRulesetName))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.INVALID_TEMPLATE_RULESET_NAME, null);
				sNewAction = "ngv_template_createnew_2";
				oAppResponse.setNextAction(sNewAction);
				return oAppResponse;
			}
			
			try
			{
				boolean bIsObjectNameUnique = oMyPortalApp.isTemplateRulesetNameUnique(sTemplateRulesetName);
				
				if (!bIsObjectNameUnique)
				{
					oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_RULESET_NAME_NOT_UNIQUE, null);
					sNewAction = "ngv_template_createnew_2";
					oAppResponse.setNextAction(sNewAction);
					return oAppResponse;
				}
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error validating template ruleset name", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}

			oTemplateRuleset = new NgvTemplateRuleset(sTemplateRulesetName);
			oTemplateRuleset.setState(RecordState.UNDER_CONFIGURATION);
			
			try
			{
				oMyPortalApp.saveTemplateRuleset(p_oUserSession, oTemplateRuleset, null);
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error saving template ruleset", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleTemplateRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
					listAppSpecificUserPreferences, oPortalApp, oTemplateRuleset);
			
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_validate_createnew_3"))
		{
			sTemplateRulesetName = WebUtils.getRequestParameter(request, "ngv_addval_valrulesetname", oMultipartContentRequest);
			
//			int l_projectid			=	WebUtils.getRequestIntParameter(request	, 	QR_REQUEST_PARAMETER_PROJECT_ID	, 	oMultipartContentRequest);
			
			int l_projectid	= 0;
			sRuleSetType = ConfigTplObjType.CDVR;
			
			//TODO: Relook
			WebUtils.setRequestAttribute(request, "ngv_addval_valrulesetname", sTemplateRulesetName);
			
			if (!DataValidationUtils.isValidObjectName(sTemplateRulesetName))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.INVALID_VALIDATION_RULESET_NAME, null);
				sNewAction = "ngv_validate_createnew_2";
				oAppResponse.setNextAction(sNewAction);
				return oAppResponse;
			}
			
			try
			{
				boolean bIsObjectNameUnique = oMyPortalApp.isValidationRulesetNameUnique(sTemplateRulesetName,l_projectid,sRuleSetType);
				
				if (!bIsObjectNameUnique)
				{
					oAppResponse.logError(WebErrorCodes.Ngv.VALIDATION_RULESET_NAME_NOT_UNIQUE, null);
					sNewAction = "ngv_validate_createnew_2";
					oAppResponse.setNextAction(sNewAction);
					return oAppResponse;
				}
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error validating template ruleset name", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}

			oCaseValidationRuleSet = new CaseDataValidationRuleset(l_projectid,sTemplateRulesetName,RecordState.UNDER_CONFIGURATION,CreationMode.SCRATCH,null,0);
			oCaseValidationRuleSet.setTemplateName(sTemplateRulesetName);
			oCaseValidationRuleSet.setState(RecordState.UNDER_CONFIGURATION);
			
			
			try
			{
				oCaseValidationRuleSet = oMyPortalApp.saveCaseDataValidationRuleset(p_oUserSession, l_projectid,sTemplateRulesetName,oCaseValidationRuleSet);
				
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error saving validation ruleset", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleValidationRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
					listAppSpecificUserPreferences, oPortalApp, oCaseValidationRuleSet);
			
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_tplrs_tplviewedit_sb"))
		{
			oTemplateRuleset = null;
			nTemplateRulesetId = WebUtils.getRequestIntParameter(request, "ngv_tplrulesetid", oMultipartContentRequest);
			oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
			if (oTemplateRuleset == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			WebUtils.setRequestAttribute(request, "ngv_tplrulesetid", nTemplateRulesetId);
			WebUtils.setRequestAttribute(request, "portal_action_mode", WebUtils.getRequestParameter(request, "portal_action_mode", oMultipartContentRequest));
			WebUtils.setRequestAttribute(request, "ngv_template_ruleset", oTemplateRuleset);
			
			sNewAction = "ngv_tplrs_tplviewedit_dp";
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_valrs_valviewedit_sb"))
		{
			oCaseValidationRuleSet = null;
			nTemplateRulesetId = WebUtils.getRequestIntParameter(request, "ngv_tplrulesetid", oMultipartContentRequest);
			oCaseValidationRuleSet = (CaseDataValidationRuleset) WebUtils.getSessionAttribute(request, "ngv_validate_ruleset");
			if (oCaseValidationRuleSet == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			WebUtils.setRequestAttribute(request, "ngv_tplrulesetid", nTemplateRulesetId);
			WebUtils.setRequestAttribute(request, "portal_action_mode", WebUtils.getRequestParameter(request, "portal_action_mode", oMultipartContentRequest));
			WebUtils.setRequestAttribute(request, "ngv_validate_ruleset", oCaseValidationRuleSet);
			
			sNewAction = "ngv_valrs_valviewedit_dp";
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_tplrs_tplviewedit_savetpl"))
		{
			oTemplateRuleset = null;
			String sTemplateName = null;
			String sTemplateText = null;
			String sTemplateCriteria = null;
			boolean bTemplateEditOperation = false;
			NgvTemplate oTemplate = null;
			String sTemplateId = null;
			int nTemplateId = -1;
			String sTempNewAction = "ngv_tplrs_tplviewedit_dp";
			oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
			sTemplateId = WebUtils.getRequestParameter(request, "ngv_selectedtemplateid", oMultipartContentRequest);
			nTemplateId = Integer.parseInt(sTemplateId);
			bTemplateEditOperation = (nTemplateId != -1);

			sTemplateCriteria = WebUtils.getRequestParameter(request, "ngv_addtpl_tplcriteria", oMultipartContentRequest);
			sTemplateText = WebUtils.getRequestParameter(request, "ngv_addtpl_tpltext", oMultipartContentRequest);	
			if (!bTemplateEditOperation)
			{
				sTemplateName = WebUtils.getRequestParameter(request, "ngv_addtpl_tplname", oMultipartContentRequest);
				
				if (!DataValidationUtils.isValidObjectName(sTemplateName))
				{
					WebUtils.setRequestAttribute(request, "r_criteria", sTemplateCriteria);
					WebUtils.setRequestAttribute(request, "r_templateContent", sTemplateText);
					WebUtils.setRequestAttribute(request, "r_templateName", sTemplateName);
					oAppResponse.logError(WebErrorCodes.Ngv.INVALID_TEMPLATE_NAME, null);
					oAppResponse.setNextAction(sTempNewAction);
					return oAppResponse;
				}
				
				if (!NgvAppUtils.isTemplateNameUniqueWithinRuleset(oTemplateRuleset, sTemplateName, nTemplateId))
				{
					WebUtils.setRequestAttribute(request, "r_criteria", sTemplateCriteria);
					WebUtils.setRequestAttribute(request, "r_templateContent", sTemplateText);
					WebUtils.setRequestAttribute(request, "r_templateName", sTemplateName);
					oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_NAME_NOT_UNIQUE, null);
					oAppResponse.setNextAction(sTempNewAction);
					return oAppResponse;
				}
			}
			
			boolean bMixedConjunctionSupported  = Boolean.valueOf(oMyPortalApp.getAppConfigSetting(AppConfig.MIXED_CONJUNCTION_SUPPORTED));
			boolean bEnforceAndOrSequence 		= Boolean.valueOf(oMyPortalApp.getAppConfigSetting(AppConfig.ENFORCE_AND_OR_SEQUENCE));
			
			if (!RuleSetUtils.isValidRuleCriteria(sTemplateCriteria, bMixedConjunctionSupported, bEnforceAndOrSequence))
			{
				sTemplateName = WebUtils.getRequestParameter(request, "ngv_addtpl_tplname", oMultipartContentRequest);
				WebUtils.setRequestAttribute(request, "r_criteria", sTemplateCriteria);
				WebUtils.setRequestAttribute(request, "r_templateContent", sTemplateText);
				WebUtils.setRequestAttribute(request, "r_templateName", sTemplateName);
				oAppResponse.logError(WebErrorCodes.Ngv.INVALID_TEMPLATE_RULE_CRITERIA, null);
				oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}
			
			if (!NgvAppUtils.isTemplateCriteriaUniqueWithinRuleset(oTemplateRuleset, sTemplateCriteria, nTemplateId))
			{
				sTemplateName = WebUtils.getRequestParameter(request, "ngv_addtpl_tplname", oMultipartContentRequest);
				WebUtils.setRequestAttribute(request, "r_criteria", sTemplateCriteria);
				WebUtils.setRequestAttribute(request, "r_templateContent", sTemplateText);
				WebUtils.setRequestAttribute(request, "r_templateName", sTemplateName);
				oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_RULE_CRITERIA_NOT_UNIQUE, null);
				oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}
			
			
			
			if (sTemplateText == null)
			{
				//TODO:Review this portion
				
//				oUploadedFileObject = WebUtils.stageRequestParameterFile(request, NGV_TEMPLATE_TEXT_FILE, oMultipartContentRequest);
//				if (WebUtils.errorIfUploadFailed(oUploadedFileObject, listErrors, "ERROR-0007"))
//				{
//					return oAppResponse;
//				}
//				
//				try
//				{
//					sTemplateText = FileUtils.getFileText(oUploadedFileObject.getStagedFilePath());
//				}
//				catch (SciException sciExcep)
//				{
//					log.error("Error reading template contents");
//					sTemplateText = "";
//				}
			}
			
			try
			{
				if (bTemplateEditOperation)
				{
					oTemplate = oTemplateRuleset.getTemplate(Integer.parseInt(sTemplateId));
					
					oMyPortalApp.modifyTemplate(p_oUserSession, oTemplateRuleset, oTemplate, sTemplateCriteria, sTemplateText);
				}
				else
				{
					oTemplate = new NgvTemplate(sTemplateName, sTemplateText);
					oTemplate.setParentRulesetId(oTemplateRuleset.getRulesetId());
					oTemplate.setTemplateCriteria(sTemplateCriteria);
					
					oMyPortalApp.addTemplate(p_oUserSession, oTemplateRuleset, oTemplate, LocationToAdd.END_OF_LIST);
					
					//Reload the template ruleset object
					oTemplateRuleset = oMyPortalApp.getTemplateRuleset(oTemplateRuleset.getRulesetId());
				}
			}
			catch(SciServiceException serExcep)
			{
				WebUtils.setRequestAttribute(request, "r_criteria", sTemplateCriteria);
				WebUtils.setRequestAttribute(request, "r_templateContent", sTemplateText);
				WebUtils.setRequestAttribute(request, "r_templateName", sTemplateName);
				log.error("Error updating template ruleset", serExcep);
			    oAppResponse.logError(serExcep);
			    oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleTemplateRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
							listAppSpecificUserPreferences, oPortalApp, oTemplateRuleset);
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_valrs_valviewedit_savetpl"))
		{

			oCaseValidationRuleSet = null;
			String sRuleName = null;
			String sRuleType = null;
			String sRuleCriteria = null;
			String sRuleApplicability = null;
			String sapplicabilityCriteria = null;
			boolean bTemplateEditOperation = false;
			CaseDataValidationRule oRule = null;
			String sTemplateId = null;
			int nRuleeId = -1;
			String sTempNewAction = "ngv_valrs_valviewedit_dp";
			nTemplateRulesetId = Integer.parseInt(WebUtils.getRequestParameter(request, "ngv_tplrulesetid", oMultipartContentRequest));
			oCaseValidationRuleSet = (CaseDataValidationRuleset) WebUtils.getSessionAttribute(request, "ngv_validate_ruleset");
			if(oCaseValidationRuleSet!=null)
			{
			WebUtils.setRequestAttribute(request, "ngv_validate_ruleset", "oCaseValidationRuleSet");
			}
			sTemplateId = WebUtils.getRequestParameter(request, "ngv_selectedtemplateid", oMultipartContentRequest);
			nRuleeId = Integer.parseInt(sTemplateId);
			bTemplateEditOperation = (nRuleeId != -1);

			if (!bTemplateEditOperation)
			{
				sRuleName = WebUtils.getRequestParameter(request, "ngv_addval_rulename", oMultipartContentRequest);
				
				if (!DataValidationUtils.isValidObjectName(sRuleName))
				{
					oAppResponse.logError(WebErrorCodes.Ngv.INVALID_RULE_NAME, null);
					oAppResponse.setNextAction(sTempNewAction);
					return oAppResponse;
				}
				
				if (!NgvAppUtils.isRuleNameUniqueWithinRuleset(oCaseValidationRuleSet, sRuleName, nRuleeId))
				{
					oAppResponse.logError(WebErrorCodes.Ngv.RULE_NAME_NOT_UNIQUE, null);
					oAppResponse.setNextAction(sTempNewAction);
					return oAppResponse;
				}
			}
			
			sRuleType = WebUtils.getRequestParameter(request, "ruletype", oMultipartContentRequest);
						
			sRuleCriteria = WebUtils.getRequestParameter(request, "ngv_addval_valcriteria", oMultipartContentRequest);
			
			boolean bMixedConjunctionSupported  = Boolean.valueOf(oMyPortalApp.getAppConfigSetting(AppConfig.MIXED_CONJUNCTION_SUPPORTED));
			boolean bEnforceAndOrSequence 		= Boolean.valueOf(oMyPortalApp.getAppConfigSetting(AppConfig.ENFORCE_AND_OR_SEQUENCE));
			
			if (!RuleSetUtils.isValidRuleCriteria(sRuleCriteria, bMixedConjunctionSupported, bEnforceAndOrSequence))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.INVALID_RULE_CRITERIA, null);
				oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}
			
/*			if (!NgvAppUtils.isRuleeCriteriaUniqueWithinRuleset(oCaseValidationRuleSet, sRuleCriteria, nRuleeId))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_RULE_CRITERIA_NOT_UNIQUE, sRuleCriteria);
				oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}
*/			
			
			sRuleApplicability = WebUtils.getRequestParameter(request, "applicability", oMultipartContentRequest);
			if(sRuleApplicability.equalsIgnoreCase("certain"))
			{
				sapplicabilityCriteria = WebUtils.getRequestParameter(request, "ngv_certaincriteria", oMultipartContentRequest);
			}
			
			if(sapplicabilityCriteria!=null && !sapplicabilityCriteria.equalsIgnoreCase("") && !sRuleApplicability.equalsIgnoreCase("all"))
			{
			if (!RuleSetUtils.isValidRuleCriteria(sapplicabilityCriteria, bMixedConjunctionSupported, bEnforceAndOrSequence))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.INVALID_RULE_APPLICABILITY, null);
				oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}
			}
			
			/*if (!NgvAppUtils.isRuleeCriteriaUniqueWithinRuleset(oCaseValidationRuleSet, sRuleApplicability, nRuleeId))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_RULE_CRITERIA_NOT_UNIQUE, sRuleApplicability);
				oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}*/
			
//			sTemplateText = WebUtils.getRequestParameter(request, "ngv_addval_valtext", oMultipartContentRequest);
//			if (sTemplateText == null)
//			{
				//TODO:Review this portion
				
//				oUploadedFileObject = WebUtils.stageRequestParameterFile(request, NGV_TEMPLATE_TEXT_FILE, oMultipartContentRequest);
//				if (WebUtils.errorIfUploadFailed(oUploadedFileObject, listErrors, "ERROR-0007"))
//				{
//					return oAppResponse;
//				}
//				
//				try
//				{
//					sTemplateText = FileUtils.getFileText(oUploadedFileObject.getStagedFilePath());
//				}
//				catch (SciException sciExcep)
//				{
//					log.error("Error reading template contents");
//					sTemplateText = "";
//				}
//			}
			
			try
			{
				if (bTemplateEditOperation)
				{
					oRule = oCaseValidationRuleSet.getRule(Integer.parseInt(sTemplateId));
					if(sRuleApplicability.equalsIgnoreCase("all"))
					{
						oMyPortalApp.modifyCaseDataValidationRule(p_oUserSession, oCaseValidationRuleSet, oRule, Type.valueOf(sRuleType),sRuleCriteria,sRuleApplicability);	
					}
					else
					{
					oMyPortalApp.modifyCaseDataValidationRule(p_oUserSession, oCaseValidationRuleSet, oRule, Type.valueOf(sRuleType),sRuleCriteria,sapplicabilityCriteria);
					}
				}
				else
				{
					oRule = new CaseDataValidationRule(sRuleName,Type.valueOf(sRuleType));
					oRule.setCriteria(sRuleCriteria);
					if(sRuleApplicability.equalsIgnoreCase("all"))
					{
					oRule.setApplicability(sRuleApplicability);
					}
					else
					{
						oRule.setApplicability(sapplicabilityCriteria);	
					}
					
					
//					int l_projectid			=	WebUtils.getRequestIntParameter(request	, 	QR_REQUEST_PARAMETER_PROJECT_ID	, 	oMultipartContentRequest);
					int l_projectid	= 0;
					oMyPortalApp.addRule(p_oUserSession, oCaseValidationRuleSet, oRule, LocationToAdd.END_OF_LIST);
					
//					//Reload the template ruleset object
//					oValidationRuleSet = (CaseDataValidationRuleset) oMyPortalApp.getCaseDataValidationRuleset(oValidationRuleSet.getSequenceId());
				}
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error updating template ruleset", serExcep);
			    oAppResponse.logError(serExcep);
			    oAppResponse.setNextAction(sTempNewAction);
				return oAppResponse;
			}
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleValidationRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
							listAppSpecificUserPreferences, oPortalApp, oCaseValidationRuleSet);
		
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_template_viewtpl"))
		{
			nTemplateRulesetId = Integer.parseInt(WebUtils.getRequestParameter(request, "ngv_tplrulesetid", oMultipartContentRequest));
			
			try
			{
				oTemplateRuleset = oMyPortalApp.getTemplateRuleset(nTemplateRulesetId);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error fetching template ruleset", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleTemplateRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
										listAppSpecificUserPreferences, oPortalApp, oTemplateRuleset);
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_validate_viewtpl"))
		{
			nTemplateRulesetId = Integer.parseInt(WebUtils.getRequestParameter(request, "ngv_tplrulesetid", oMultipartContentRequest));
			
			System.out.println("");
			try
			{
				oCaseValidationRuleSet = (CaseDataValidationRuleset) oMyPortalApp.getCaseDataValidationRuleset(nTemplateRulesetId);
			}
			catch(SciServiceException serExcep)
			{
				log.error("Error fetching template ruleset", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleValidationRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
										listAppSpecificUserPreferences, oPortalApp, oCaseValidationRuleSet);
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_templateruleset_uploadheader"))
		{
			sNewAction = "ngv_templateruleset_uploadheader_2";
			WebUtils.setRequestAttribute(request, "portal_action_mode", "header");
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_template_savetplheader"))
		{
			oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
			if (oTemplateRuleset == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			sTemplateHeader = WebUtils.getRequestParameter(request, "ngv_addtpl_tplheader", oMultipartContentRequest);
			//TODO:Validate content
			try
			{
				oMyPortalApp.setHeaderTemplate(p_oUserSession, oTemplateRuleset, sTemplateHeader);
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error setting template header", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleTemplateRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
					listAppSpecificUserPreferences, oPortalApp, oTemplateRuleset);
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_template_setdefaulttemplate"))
		{
			oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
			nDefaultTemplateId = Integer.parseInt(WebUtils.getRequestParameter(request, "ngv_selectedtemplateid", oMultipartContentRequest));
			
			if (oTemplateRuleset == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			if (nDefaultTemplateId == -1)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.DEFAULT_TEMPLATE_NOT_SELECTED);
				return oAppResponse;
			}
			
			try
			{
				oMyPortalApp.setDefaultTemplate(p_oUserSession, oTemplateRuleset, nDefaultTemplateId);
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error setting default template", serExcep);
			    oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
			WebUtils.setSessionAttribute(request, "ngv_template_ruleset", oTemplateRuleset);
			sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfSingleTemplateRuleset(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
								listAppSpecificUserPreferences, oPortalApp, oTemplateRuleset);
			
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_template_edittemplate"))
		{
			oTemplateRuleset = (NgvTemplateRuleset) WebUtils.getSessionAttribute(request, "ngv_template_ruleset");
			WebUtils.setRequestAttribute(request, "portal_action_mode", WebUtils.getRequestParameter(request, "portal_action_mode", oMultipartContentRequest));
			WebUtils.setRequestAttribute(request, "ngv_editTemplateFlag", WebUtils.getRequestParameter(request, "ngv_editTemplateFlag", oMultipartContentRequest));
			
			if (oTemplateRuleset == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			nSelectedTemplateId = Integer.parseInt(WebUtils.getRequestParameter(request, "ngv_selectedtemplateid", oMultipartContentRequest));
			listTemplates = oTemplateRuleset.getTemplateList();
			
			for (NgvTemplate oTemplate : listTemplates)
			{
				if (oTemplate.getTemplateSeqId() == nSelectedTemplateId)
				{
					WebUtils.setRequestAttribute(request, "ngv_selectedTemplate", oTemplate);
					break;
				}
			}
			WebUtils.setRequestAttribute(request, "ngv_template_ruleset", oTemplateRuleset);
			sNewAction = "ngv_tplrs_tplviewedit_dp";
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_validate_editrule"))
		{
			oCaseValidationRuleSet = (CaseDataValidationRuleset) WebUtils.getSessionAttribute(request, "ngv_validate_ruleset");
			WebUtils.setRequestAttribute(request, "portal_action_mode", WebUtils.getRequestParameter(request, "portal_action_mode", oMultipartContentRequest));
			WebUtils.setRequestAttribute(request, "ngv_editTemplateFlag", WebUtils.getRequestParameter(request, "ngv_editTemplateFlag", oMultipartContentRequest));
			
			if (oCaseValidationRuleSet == null)
			{
				//Display main page with error
				sNewAction = null;
				oAppResponse.logError(WebErrorCodes.Ngv.EMPTY_TEMPLATE_RULESET);
				return oAppResponse;
			}
			
			nSelectedTemplateId = Integer.parseInt(WebUtils.getRequestParameter(request, "ngv_selectedtemplateid", oMultipartContentRequest));
			listRules = oCaseValidationRuleSet.getValidationRules();
			
			for (CaseDataValidationRule oRule : listRules)
			{
				if (oRule.getRuleSeqId() == nSelectedTemplateId)
				{
					WebUtils.setRequestAttribute(request, "ngv_selectedTemplate", oRule);
					break;
				}
			}
			WebUtils.setRequestAttribute(request, "ngv_validate_ruleset", oCaseValidationRuleSet);
			sNewAction = "ngv_valrs_valviewedit_dp";
		}
		else if(p_sActionId.startsWith("ngv_tplrsimport_"))
		{
			oImportResponse = processTemplateRulesetImportAction(request, response, oMultipartContentRequest, p_sActionId, p_oUserSession, 
					listAppSpecificUserPreferences, oMyPortalApp);
		
			if(oImportResponse.hasError())
			{
				oAppResponse.includeErrorResponse(oImportResponse);
				oAppResponse.setNextAction("ngv_template_createnew_2");
				return oAppResponse;
			}
			
			sNewAction = oImportResponse.getNextAction();
			
		}
		else
		{
			sNewAction = null;
			log.error("ProjSettingsPortalApp.processAction: Unsupported Action - {} ", p_sActionId);
			oAppResponse.logError(WebErrorCodes.System.INVALID_ACTION, null);
			return oAppResponse;
		}
		
		log.debug("ProjSettingsPortalApp.processAction: Completed processing for Action - {}. New Action - {}", p_sActionId, sNewAction);
		oAppResponse.setNextAction(sNewAction);
		
		return oAppResponse;
	}	
	
	private PortalAppResponse processTemplateRulesetImportAction(HttpServletRequest request, HttpServletResponse response, 
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession, 
			List<UserPreference> listAppSpecificUserPreferences, NgvPortalApp oMyPortalApp)
	{
		List<String> listRulesetNameAndXmlDoc = null;
		UploadedFileWrapper oUploadedFileObject = null;
		String sTemplateRulesetXmlDoc = null;
		String sTemplateRulesetName = null;
		boolean bIsObjectNameUnique = false;
		PortalAppResponse oAppResponse = null;
		String byte64EncodeString = null;
		String sNewAction = null;
		NgvTemplateRuleset oTemplateRuleset = null;
		String sImportFileName = null;
		oAppResponse = new PortalAppResponse(getDefaultPageUri());
		
		if (p_sActionId.equalsIgnoreCase("ngv_tplrsimport_new"))
		{
			//Nothing special to do here, simply redirect to next page
			sNewAction = "ngv_tplrsimport_step1dp";
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_tplrsimport_step1sb"))
		{
			try
			{
				oUploadedFileObject = WebUtils.stageRequestParameterFile(request, "ngv_templaters_importfile", oMultipartContentRequest);
			}
			catch (SciWebException webExcep)
			{
				log.error("Error uploading file", webExcep);
				oAppResponse.logError(WebErrorCodes.Ngv.RULESET_IMPORT_FILE_NOT_UPLOADED);
				return oAppResponse;
			}
			
			try
			{
				
				listRulesetNameAndXmlDoc = oMyPortalApp.getImportedTemplate(oUploadedFileObject.getStagedFilePath());
				WebUtils.setRequestAttribute(request, "r_import_file_name", oUploadedFileObject.getStagedFileName());
				
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error importing file", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			finally
			{
				oUploadedFileObject.delete();
			}
			
			sTemplateRulesetName = listRulesetNameAndXmlDoc.get(0);
			sTemplateRulesetXmlDoc = listRulesetNameAndXmlDoc.get(1);
			try{
				byte64EncodeString = Base64.getEncoder().encodeToString(sTemplateRulesetXmlDoc.getBytes("utf-8"));
			}
			catch(UnsupportedEncodingException unExcp)
			{
				log.error("Error In xml ecoding", unExcp);
				oAppResponse.logError(WebErrorCodes.Ngv.ERROR_ENCODING_FILE ,null );
				return oAppResponse;
			}
			
			WebUtils.setRequestAttribute(request, "ngv_addtpl_tplrulesetname", sTemplateRulesetName);
			WebUtils.setRequestAttribute(request, "r_rulesetXmlDocument", byte64EncodeString);
			sNewAction = "ngv_tplrsimport_step2dp";
		}
		else if (p_sActionId.equalsIgnoreCase("ngv_tplrsimport_step2sb"))
		{
			byte64EncodeString = WebUtils.getRequestParameter(request, "r_rulesetXmlDocument", oMultipartContentRequest);
			sTemplateRulesetName = WebUtils.getRequestParameter(request, "ngv_addtpl_tplrulesetname", oMultipartContentRequest);
			sImportFileName = WebUtils.getRequestParameter(request, "r_import_file_name", oMultipartContentRequest);
			
			if (!DataValidationUtils.isValidObjectName(sTemplateRulesetName))
			{
				oAppResponse.logError(WebErrorCodes.Ngv.INVALID_TEMPLATE_RULESET_NAME, null);
				return oAppResponse;
			}
			
			try
			{
				bIsObjectNameUnique = oMyPortalApp.isTemplateRulesetNameUnique(sTemplateRulesetName);
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error validating template ruleset name", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			
			if (!bIsObjectNameUnique)
			{
				oAppResponse.logError(WebErrorCodes.Ngv.TEMPLATE_RULESET_NAME_NOT_UNIQUE, null);
				return oAppResponse;
			}
		
			oTemplateRuleset = new NgvTemplateRuleset(sTemplateRulesetName);
			oTemplateRuleset.setState(RecordState.UNDER_CONFIGURATION);
			
			try
			{
				oMyPortalApp.saveTemplateRuleset(p_oUserSession, oTemplateRuleset, sImportFileName);
			}
			catch (SciServiceException serExcep)
			{
				log.error("Error saving template ruleset", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
			try{
			sTemplateRulesetXmlDoc = new String (Base64.getDecoder().decode(byte64EncodeString), "utf-8");
			}
			catch(UnsupportedEncodingException unExcp)
			{
				log.error("Error saving template ruleset", unExcp);
				oAppResponse.logError(WebErrorCodes.Ngv.ERROR_DECODING_FILE, null);
				return oAppResponse;
			}
			
			try
			{
				oMyPortalApp.saveHeaderAndTemplate(p_oUserSession, oTemplateRuleset, sTemplateRulesetXmlDoc);
			}
			catch (SciServiceException serExcep) 
			{
				log.error("Error saving template header", serExcep);
				oAppResponse.logError(serExcep);
				return oAppResponse;
			} 
			try
			{
				sNewAction = NgvPortalWebAppUtils.prepareForDisplayOfRulesets(request, response, oMultipartContentRequest, oMyPortalApp, p_sActionId, p_oUserSession, 
					listAppSpecificUserPreferences, null);
			}
			catch (SciServiceException serExcep) 
			{
				oAppResponse.logError(serExcep);
				return oAppResponse;
			}
		}
		else
		{
			sNewAction = null;
			log.error("processTemplateRulesetImportAction: Unsupported Action - {}", p_sActionId);
			oAppResponse.logError(WebErrorCodes.System.INVALID_ACTION, null);
			return oAppResponse;
		}
		
		oAppResponse.setNextAction(sNewAction);
		return oAppResponse;
	}


	@Override
	protected String getNextAction(PortalAppResponse oAppResponse,HttpServletRequest request, HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) 
	{
		
		if(p_sActionId.equals("ngv_step2"))
		{
			WebUtils.setRequestAttribute(request, "new-portalappid", oPortalApp.getPortalAppId());
			oAppResponse.logError(WebErrorCodes.System.INVALID_REQUEST_PARAMETER, "");
			return p_sActionId;
		}
		
		oAppResponse.logError(WebErrorCodes.System.INVALID_REQUEST_PARAMETER, "");
		return null;
	}	

}
