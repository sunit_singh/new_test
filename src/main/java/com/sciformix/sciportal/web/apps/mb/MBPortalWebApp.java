package com.sciformix.sciportal.web.apps.mb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciEnums;
import com.sciformix.commons.SciEnums.ApplicationSequencePurpose;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.DataValidationUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.IPortalApp;
import com.sciformix.sciportal.apps.mb.MBPortalApp;
import com.sciformix.sciportal.apps.sys.ProjSettingsPortalApp;
import com.sciformix.sciportal.appseq.ApplicationSequenceHelper;
import com.sciformix.sciportal.mb.MBMailInfo;
import com.sciformix.sciportal.mb.MailBox;
import com.sciformix.sciportal.mb.MailBoxHomeImpl;
import com.sciformix.sciportal.project.ProjectHome;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.BasePortalWebApp;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class MBPortalWebApp extends BasePortalWebApp {

	private static final Logger log = LoggerFactory.getLogger(MBPortalWebApp.class);
	
	private static final String
		QR_REQUEST_STARTDATE_COUNT	=	"fldstartdatecount"
	;
	private static final String
		QR_REQUEST_ENDDATE_COUNT	=	"fldenddatecount"
	;
	
	public MBPortalWebApp()
	{
		super("MB-PORTAL-APP", "mailboxlandingpage.jsp", ProjectApplicability.SINGLE_PROJECT);
	}

	@Override
	protected PortalAppResponse processAction(HttpServletRequest request, HttpServletResponse response,
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession,
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) throws SciServiceException {
		
		MBPortalApp					mbportalapp				=	(MBPortalApp)	oPortalApp;
		PortalAppResponse			oAppResponse 			=	null,
									o_ErrorAppResponce 		=	null;	
		String 						nextPageUrl				=	null;
		int							frMailBoxId				=	-1,
									l_projectid				=	-1,
									l_startdate				=	0,
									l_enddate				=	0;
		List<MailBox>				mailBoxList 			=	null;
		List<MBMailInfo>			mailInfoList 			=	null;
		List<UploadedFileWrapper>	uploadedFileWrapperList	=  null;
		File						l_exportfile			=	null;
		PrintWriter					l_pwr					=	null;
		FileInputStream				l_fis					=	null;
		
		l_projectid			=	WebUtils.getAuthorizedProjectId(request);
		WebUtils.setRequestAttribute(request,"authproject", ProjectHome.getProject(l_projectid));
		l_startdate	=	WebUtils.isRequestParameterAvailable(request, "fldstartdatecount", oMultipartContentRequest) ? 
						WebUtils.getRequestIntParameter(request, "fldstartdatecount", oMultipartContentRequest) : 0;
		l_enddate	=	WebUtils.isRequestParameterAvailable(request, "fldenddatecount", oMultipartContentRequest) ? 
						WebUtils.getRequestIntParameter(request, "fldenddatecount", oMultipartContentRequest) : 0;
		//saveMailBox(p_oUserSession,l_projectid);
		if(StringUtils.isNullOrEmpty(p_sActionId)){
			oAppResponse = new PortalAppResponse(getDefaultPageUri());
		}else if(p_sActionId.equals("mailBox"))
		{
			nextPageUrl = "mailBoxView.jsp";
			oAppResponse = new PortalAppResponse(nextPageUrl);
			try {
				mailBoxList = mbportalapp.getMailBoxListByProjectId(l_projectid);
			}catch(SciServiceException sciExc)
			{
				log.error("error fetching mailbox list");
				o_ErrorAppResponce = new PortalAppResponse(getDefaultPageUri());
				o_ErrorAppResponce.logError(sciExc);
				return o_ErrorAppResponce;
			}	
			WebUtils.setRequestAttribute(request,"mblist", mailBoxList);	
		}
		else if(p_sActionId.equals("viewmailbox")){
			nextPageUrl = "mailBoxMailList.jsp";
			oAppResponse = new PortalAppResponse(nextPageUrl);
			frMailBoxId = WebUtils.getRequestIntParameter(request, "selectMailBox",oMultipartContentRequest);
			request.getSession().setAttribute("mailBox", mbportalapp.getMailBoxBySeqId(frMailBoxId));
		}
		else if(p_sActionId.equals("filterMailList")){
			
			MailBox mailBox = null;
			nextPageUrl = "mailBoxMailList.jsp";
			oAppResponse = new PortalAppResponse(nextPageUrl);
			mailBox= (MailBox)request.getSession().getAttribute("mailBox");
			
			try {
				mailInfoList = mbportalapp.getMailListByDateRange(l_projectid, mailBox.getSequenceId(), l_startdate, l_enddate);
				WebUtils.setRequestAttribute(
					request
				, 	QR_REQUEST_STARTDATE_COUNT
				, 	l_startdate
				);
				WebUtils.setRequestAttribute(
					request
				, 	QR_REQUEST_ENDDATE_COUNT
				, 	l_enddate
				);
			} catch(SciServiceException sciExc)
			{
				log.error("error fetching mail list by mail box id");
				o_ErrorAppResponce = new PortalAppResponse(getDefaultPageUri());
				o_ErrorAppResponce.logError(sciExc);
				return o_ErrorAppResponce;
			}
			
			WebUtils.setRequestAttribute(request,"mailList", mailInfoList);
		}
		else if(p_sActionId.equals("uploadmsgfileview")){
			nextPageUrl = "uploadMsgView.jsp";
			oAppResponse = new PortalAppResponse(nextPageUrl);
			try {
				mailBoxList = mbportalapp.getMailBoxListByProjectId(l_projectid);
			}catch(SciServiceException sciExc)
			{
				log.error("error fetching mailbox list");
				o_ErrorAppResponce = new PortalAppResponse(getDefaultPageUri());
				o_ErrorAppResponce.logError(sciExc);
				return o_ErrorAppResponce;
			}	
			WebUtils.setRequestAttribute(request,"mblist", mailBoxList);
		}
		else if(p_sActionId.equals("uploadmsgfile")){
			Map<String, List<MBMailInfo>> mailInfoListMap = null;
			nextPageUrl = "uploadedMailListView.jsp";
			oAppResponse = new PortalAppResponse(nextPageUrl);
			frMailBoxId = WebUtils.getRequestIntParameter(request, "selectMailBox",oMultipartContentRequest);
			
			
			WebUtils.setRequestAttribute(request,"mailListMap", mailInfoListMap);
			try {
				mailBoxList = mbportalapp.getMailBoxListByProjectId(l_projectid);
			}catch(SciServiceException sciExc)
			{
				log.error("error fetching mailbox list");
				o_ErrorAppResponce = new PortalAppResponse(getDefaultPageUri());
				o_ErrorAppResponce.logError(sciExc);
				return o_ErrorAppResponce;
			}
			WebUtils.setRequestAttribute(request,"mailBox", mbportalapp.getMailBoxBySeqId(frMailBoxId));
			try{
				uploadedFileWrapperList = WebUtils.stageRequestParameterFiles(request, "msgFiles", oMultipartContentRequest);
			}
			catch(SciWebException sciWebExc)
			{
				log.error("Error uploading msg file");
				o_ErrorAppResponce = new PortalAppResponse("uploadMsgView.jsp");
				o_ErrorAppResponce.logError(WebErrorCodes.MailBox.ERROR_UPLOAD_MSG_FILE);
				return o_ErrorAppResponce;
			}
			
			try{
				mailInfoListMap = mbportalapp.uploadMailByMsgFile(p_oUserSession, l_projectid, frMailBoxId, uploadedFileWrapperList);		
			}
			catch(SciServiceException sciserExcp)
			{
				log.error("Error uploading msg file");
				o_ErrorAppResponce = new PortalAppResponse("uploadMsgView.jsp");
				o_ErrorAppResponce.logError(sciserExcp);
				return o_ErrorAppResponce;
			}
			WebUtils.setRequestAttribute(request,"mailListMap", mailInfoListMap);
			
			if(mailInfoListMap.get(MBPortalApp.SAVED_MAIL_KEY) != null && !mailInfoListMap.get(MBPortalApp.SAVED_MAIL_KEY).isEmpty())
			{
				nextPageUrl = "uploadedMailListView.jsp";
			}
			else
			{
				nextPageUrl = "uploadMsgView.jsp";
			}
			oAppResponse = new PortalAppResponse(nextPageUrl);
		} else if ("exportmails".equals(p_sActionId)) {
			boolean	l_isreportgensuccessful	=	true;
			MailBox mailBox= null;
			oAppResponse = new PortalAppResponse(nextPageUrl);
			mailBox= (MailBox)request.getSession().getAttribute("mailBox");
			WebUtils.setRequestAttribute(
				request
			, 	QR_REQUEST_STARTDATE_COUNT
			, 	l_startdate
			);
			WebUtils.setRequestAttribute(
				request
			, 	QR_REQUEST_ENDDATE_COUNT
			, 	l_enddate
			);
			try {
				l_exportfile	=	mbportalapp.exportMails(
										l_projectid
									,	mailBox.getSequenceId()	
									,	l_startdate
									,	l_enddate
									,	p_oUserSession
									);
				l_pwr = response.getWriter();  
				response.setContentType("text/xml");  
				response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
				l_fis = new FileInputStream(l_exportfile);  
	            
				int i;   
				while ((i = l_fis.read()) != -1) {  
					l_pwr.write(i);   
				} 
			} catch(Exception excep) {
				log.error("Error during report generation", excep);
				o_ErrorAppResponce = new PortalAppResponse(getDefaultPageUri());
				o_ErrorAppResponce.logError(WebErrorCodes.MailBox.ERROR_GENERATING_REPORT);
				l_isreportgensuccessful	=	false;
				return o_ErrorAppResponce;
			} finally {
				try {
					if (l_isreportgensuccessful) {
						oAppResponse.setIsDownloadStreamPresent(true);
						response.flushBuffer();
					}
					if(l_pwr != null) {
						l_pwr.close();
				   	}
				   	
				    if(l_fis != null){
				    	l_fis.close();
				   	}
				} catch (Exception excep) {
					log.error("Error during report generation", excep);
					oAppResponse.logError(WebErrorCodes.QR.ERROR_GENERATING_REPORT);
					return oAppResponse;
				}	
			}	
			
		}
		return oAppResponse;
	}

	@Override
	public PortalAppResponse processProjectSettingsAction(HttpServletRequest request, HttpServletResponse response,
			MultipartContentRequestWrapper oMultipartContentRequest, String p_sActionId, UserSession p_oUserSession,
			List<UserPreference> listAppSpecificUserPreferences, IPortalApp oPortalApp) {
        MailBox                m_oMailBox                =     new MailBox();
        int                    l_projectid                =    -1;
        MBPortalApp            mbportalapp                =    (MBPortalApp)    oPortalApp;
        PortalAppResponse    oAppResponse             =    null;
        String                 nextPageUrl                    =    null;
        
        oAppResponse = new PortalAppResponse(nextPageUrl);
        l_projectid            =    WebUtils.getAuthorizedProjectId(request);
        nextPageUrl="email_template.jsp"; //Need to change
        
        List<MailBox> mailBoxList =  new ArrayList<MailBox>();
        
        try {
            mailBoxList=MailBoxHomeImpl.getAllMailBoxes(l_projectid);
        } catch (SciException ScExc) {
            log.error("error fetching list of Mail Box");
        }
        
        WebUtils.setRequestAttribute(request,"mailBoxList", mailBoxList);
        
        if(StringUtils.isNullOrEmpty(p_sActionId)||p_sActionId.contains("cancel")){
            nextPageUrl="email_template.jsp";//Need to change
            oAppResponse = new PortalAppResponse(nextPageUrl);
            return oAppResponse;
        }
        else
        {
            if(p_sActionId.contains("createView")) //Go to create mail box page
            {
                p_sActionId=null;
                nextPageUrl="email_template_form.jsp";//Need to change
            }else
            if(p_sActionId.contains("EditMailBoxView")) //Go to create mail box page
            {    p_sActionId =  null;
                int mailBoxSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
                WebUtils.setRequestAttribute(request,"mailMailBox",ProjSettingsPortalApp.getMailtemplateBySeqId(mailBoxSeqId));
                nextPageUrl="email_template_edit_form.jsp";//Need to change
            }else
            if(p_sActionId.contains("create")) //Create new Template
            {
                p_sActionId=null;
                nextPageUrl="email_template_form.jsp";//Need to change
                oAppResponse = new PortalAppResponse(nextPageUrl);
                
                String mailBoxName = WebUtils.getRequestParameter(request, "new_MailBox_Name", oMultipartContentRequest).trim();
                String mailBoxEmailId =WebUtils.getRequestParameter(request, "new_emailId", oMultipartContentRequest).trim();
                String mailBoxDescription =  WebUtils.getRequestParameter(request, "mailBox_Description", oMultipartContentRequest).trim();
                String mailBoxMnemocis =  WebUtils.getRequestParameter(request, "mailBox_Mnemocis", oMultipartContentRequest).trim();
                
                if(!DataValidationUtils.isValidObjectName(mailBoxName))
                {
                        if(mailBoxName.isEmpty())
                            oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_NAME_EMPTY);
                        else
                            oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_NAME_INVALID);
                        return oAppResponse;
                }
                else
                if(mailBoxEmailId.isEmpty())
                {
                    oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_EMAILID_EMPTY);
                    return oAppResponse;
                }
                else
                if(mailBoxDescription.isEmpty())
                {
                    oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_DESCRIPTION_EMPTY);
                    return oAppResponse;
                }
                else
                if(!DataValidationUtils.isMnemonicValid(mailBoxMnemocis)||(mailBoxMnemocis.length()>20))
                {
                    if(mailBoxMnemocis.isEmpty())
                        oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_MNEMONICS_EMPTY);
                    else
                        oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_MNEMONICS_INVALID);
                    return oAppResponse;
                }
                else
                {
                    try {
                        mailBoxList=MailBoxHomeImpl.getAllMailBoxes(l_projectid);
                    } catch (SciException e) {
                        log.error("error fetching list of Mail Box");
                    }
                    for(MailBox o_mailBox :mailBoxList )
                    {
                        if(o_mailBox.getTemplateName().equals(mailBoxName))
                        {
                            oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_NAME_NOT_UNIQUE);
                            return oAppResponse;
                        }
                    }
                    try {
                        
                        m_oMailBox.setProjectId(l_projectid);
                        m_oMailBox.setCreationMode(CreationMode.SCRATCH);
                        m_oMailBox.setState(RecordState.UNDER_CONFIGURATION);
                        m_oMailBox.setTemplateName("Mail Box");
                        m_oMailBox.setEmailAddress("Email Id");
                        m_oMailBox.setMailDescription("Mail Description");
                        m_oMailBox.setTypeOfMailBox(SciEnums.MailBoxType.USER_PRIVATE.getCode());
                        m_oMailBox.setModeOfOperation(SciEnums.ModeOfOperation.PORATAL_UI.getCode());
                        m_oMailBox.setMailBoxMnemonics("MBAA-100100");
                        
                        String succMsg=mbportalapp.saveMailBox(p_oUserSession,l_projectid, m_oMailBox, true);
                        
                        WebUtils.setRequestAttribute(request,"msg", succMsg);
                        mailBoxList=MailBoxHomeImpl.getAllMailBoxes(l_projectid);
                        WebUtils.setRequestAttribute(request,"mailBoxList", mailBoxList);
                        nextPageUrl="email_template.jsp";//Need to change
                        oAppResponse = new PortalAppResponse(nextPageUrl);
                    }
                    catch(SciException scExc)
                    {
                        log.error("error saving mail box");
                    }
                catch(SciServiceException sciExc)
                {
                    log.error("error saving mail box");
                    oAppResponse.logError(sciExc);
                }
                return oAppResponse;
            }    
                    
            }else
            if(p_sActionId.contains("editPage"))
            {
                int mailBoxSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
                try {
                    m_oMailBox = MailBoxHomeImpl.getMailBoxBySeqId(mailBoxSeqId);
                } catch (SciException scExc) {
                    log.error("error fetching mail box");
                }
                WebUtils.setRequestAttribute(request,"mailBox",m_oMailBox);
                nextPageUrl="email_template_form.jsp";//Need to change
            }
            else if(p_sActionId.contains("updateMailBox"))
            {
                int mailBoxSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
                nextPageUrl="email_template_form.jsp";//Need to change
                oAppResponse = new PortalAppResponse(nextPageUrl);
                try {
                    m_oMailBox = MailBoxHomeImpl.getMailBoxBySeqId(mailBoxSeqId);
                } catch (SciException scExc) {
                    log.error("error fetching mail box");
                }
                
                if(WebUtils.getRequestParameter(request, "mailbox_name", oMultipartContentRequest).trim().isEmpty())
                {
                    WebUtils.setRequestAttribute(request,"mailbox",m_oMailBox);
                    nextPageUrl="email_template_form.jsp"; //Need to change
                    oAppResponse = new PortalAppResponse(nextPageUrl);    
                    oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_NAME_EMPTY);
                    return oAppResponse;
                }
                else
                if(WebUtils.getRequestParameter(request, "mailbox_emailid", oMultipartContentRequest).trim().isEmpty())
                {
                    WebUtils.setRequestAttribute(request,"mailbox",m_oMailBox);
                    nextPageUrl="email_template_form.jsp"; //Need to change
                    oAppResponse = new PortalAppResponse(nextPageUrl);    
                    oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_EMAILID_EMPTY);
                    return oAppResponse;
                }
                else
                if(WebUtils.getRequestParameter(request, "mailbox_description", oMultipartContentRequest).trim().isEmpty())
                {
                    WebUtils.setRequestAttribute(request,"mailbox",m_oMailBox);
                    nextPageUrl="email_template_form.jsp"; //Need to change
                    oAppResponse = new PortalAppResponse(nextPageUrl);    
                    oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_DESCRIPTION_EMPTY);
                    return oAppResponse;
                }
                else
                if(WebUtils.getRequestParameter(request, "mailbox_mnemonics", oMultipartContentRequest).trim().isEmpty())
                {
                    WebUtils.setRequestAttribute(request,"mailbox",m_oMailBox);
                    nextPageUrl="email_template_form.jsp"; //Need to change
                    oAppResponse = new PortalAppResponse(nextPageUrl);    
                    oAppResponse.logError(WebErrorCodes.MailBox.ERROR_MAIL_BOX_MNEMONICS_EMPTY);
                    return oAppResponse;
                }
                else{
                    List<MailBox> MailBoxList = new ArrayList<>();
                    try {
                        MailBoxList = MailBoxHomeImpl.getAllMailBoxes(l_projectid);
                    } catch (SciException ScExc) {
                        log.error("error fetching list of Mail Box");
                    }
                    MailBox m_oMailBoxTemp = null;
                    
                    for(MailBox mailBoxTemp: MailBoxList)
                    {
                        if(mailBoxTemp.getSequenceId() == mailBoxSeqId)
                        {
                            m_oMailBoxTemp= mailBoxTemp;
                        }
                    }
                    try {
                    if(DataUtils.hasChanged(m_oMailBoxTemp.getTemplateName(), WebUtils.getRequestParameter(request, "mailbox_name", oMultipartContentRequest).trim())||
                            DataUtils.hasChanged(m_oMailBoxTemp.getEmailAddress(), WebUtils.getRequestParameter(request, "mailbox_emailid", oMultipartContentRequest).trim())||
                            DataUtils.hasChanged(m_oMailBoxTemp.getMailDescription(), WebUtils.getRequestParameter(request, "mailbox_description", oMultipartContentRequest).trim())||
                            DataUtils.hasChanged(m_oMailBoxTemp.getMailBoxMnemonics(), WebUtils.getRequestParameter(request, "mailbox_mnemonics", oMultipartContentRequest).trim()))
                    {
                        //Need to complete
                        
                        /*String updateMsg = projectSettingsPortalApp.updateMailTemplate(l_projectid, WebUtils.getRequestParameter(request, "mail_Subject", oMultipartContentRequest).trim(),
                        WebUtils.getRequestParameter(request, "mail_body", oMultipartContentRequest).trim(),p_oUserSession, u_oUserInfo, templateSeqId);
                        WebUtils.setRequestAttribute(request,"msg", updateMsg);
                        mailTemplateList=MailConfigurationHelper.getAllEmailTemplates(l_projectid);
                        WebUtils.setRequestAttribute(request,"mailTemplateList", mailTemplateList);
                        nextPageUrl="email_template.jsp";*/
                    }
                    else
                    {
                        WebUtils.setRequestAttribute(request,"mailbox",ProjSettingsPortalApp.getMailtemplateBySeqId(mailBoxSeqId));
                        nextPageUrl="email_template_form.jsp"; //Need to change
                        oAppResponse = new PortalAppResponse(nextPageUrl);    
                        oAppResponse.logError(WebErrorCodes.MailBox.ERROR_UPDATED_VALUES_SAME_AS_EXISTING);
                        return oAppResponse;
                    }
                    }catch(SciException sciExp)
                    {
                        log.error("Error updating mail box", sciExp);
                    }
                
                oAppResponse = new PortalAppResponse(nextPageUrl);
                return oAppResponse;
                }
            }
            else if(p_sActionId.contains("changeStatus")) //Create new Template
            {
                //Need to complete
                
                /*int mailBoxSeqId =Integer.parseInt(WebUtils.getRequestParameter(request, "click_Operation_Project_Id", oMultipartContentRequest));
                projectSettingsPortalApp.changeMailTemplateStatus(p_oUserSession,l_projectid,templateSeqId, RecordState.toEnum(Integer.parseInt(WebUtils.getRequestParameter(request, "statusValue", oMultipartContentRequest))), u_oUserInfo);
                WebUtils.setRequestAttribute(request,"mailTemplate",ProjSettingsPortalApp.getMailtemplateBySeqId(templateSeqId));
                nextPageUrl="email_template_edit_form.jsp"; */
            }
        }
        oAppResponse = new PortalAppResponse(nextPageUrl);
        
        return oAppResponse;
    	}
@Override
	protected String getNextAction(PortalAppResponse oAppResponse, HttpServletRequest request,
			HttpServletResponse response, String p_sActionId, IPortalApp oPortalApp) {
		// TODO Auto-generated method stub
		return null;
	}

	private void saveMailBox(UserSession p_oUserSession,  int projectId)
	{
		try{	
		MBPortalApp mbportalapp = new MBPortalApp();
		
		MailBox m_oMailBox = new MailBox();
		m_oMailBox.setProjectId(2);
		m_oMailBox.setCreationMode(CreationMode.SCRATCH);
		m_oMailBox.setState(RecordState.ACTIVE);
		m_oMailBox.setTemplateName("Project 2 Mail Box Template 1");
		m_oMailBox.setEmailAddress("Email Id");
		m_oMailBox.setMailDescription("PRoject 2 Mail Description");
		m_oMailBox.setTypeOfMailBox(SciEnums.MailBoxType.USER_PRIVATE.getCode());
		m_oMailBox.setModeOfOperation(SciEnums.ModeOfOperation.PORATAL_UI.getCode());
		m_oMailBox.setMailBoxMnemonics("PRG2-DS-{NNNNNN}");
		mbportalapp.saveMailBox(p_oUserSession,projectId, m_oMailBox,  true);
		ApplicationSequenceHelper.registerApplicationSequence(projectId, ApplicationSequencePurpose.MAILBOX,
				m_oMailBox.getSequenceId(), "PRG2-DS-{NNNNNN}");
		
		
		//SciEnums.MailBoxType.toEnum(m_oMailBox.getTypOfMailBox()).getDisplayName();
	
//		mbportalapp.saveMailBox(p_oUserSession,projectId, m_oMailBox,  true);
//		
//		 m_oMailBox = new MailBox();
//		m_oMailBox.setProjectId(projectId);
//		m_oMailBox.setCreationMode(CreationMode.SCRATCH);
//			m_oMailBox.setState(RecordState.ACTIVE);
//		m_oMailBox.setTemplateName("Mail Box Template 2");
//		m_oMailBox.setEmailAddress("Test@test.com");
//	m_oMailBox.setMailDescription("Mail Description");
//		m_oMailBox.setTypeOfMailBox(SciEnums.MailBoxType.USER_PRIVATE.getCode());
//		m_oMailBox.setModeOfOperation(SciEnums.ModeOfOperation.PORATAL_UI.getCode());
//		m_oMailBox.setMailBoxMnemonics("MBAA-100101");
//		
//		//SciEnums.MailBoxType.toEnum(m_oMailBox.getTypOfMailBox()).getDisplayName();
//	
//		mbportalapp.saveMailBox(p_oUserSession,projectId, m_oMailBox,  true);
		
		
		m_oMailBox = new MailBox();
		m_oMailBox.setProjectId(projectId);
		m_oMailBox.setCreationMode(CreationMode.SCRATCH);
		m_oMailBox.setState(RecordState.ACTIVE);
		m_oMailBox.setTemplateName("Drug Safety Mailbox");
		m_oMailBox.setEmailAddress("DrugSafety@scipharma.com");
		m_oMailBox.setMailDescription("SciPharma Drug Safety Mailbox");
		m_oMailBox.setTypeOfMailBox(SciEnums.MailBoxType.TEAM_SHARED.getCode());
		m_oMailBox.setModeOfOperation(SciEnums.ModeOfOperation.PORATAL_UI.getCode());
		m_oMailBox.setMailBoxMnemonics("SCPH-DS-{NNNNNN}");
		mbportalapp.saveMailBox(p_oUserSession,projectId, m_oMailBox,  true);
		
		ApplicationSequenceHelper.registerApplicationSequence(projectId, ApplicationSequencePurpose.MAILBOX,
				m_oMailBox.getSequenceId(), "SCGN-DS-{NNNNNN}");
		
		m_oMailBox = new MailBox();
		m_oMailBox.setProjectId(projectId);
		m_oMailBox.setCreationMode(CreationMode.SCRATCH);
		m_oMailBox.setState(RecordState.ACTIVE);
		m_oMailBox.setTemplateName("CT Drug Safety Mailbox");
		m_oMailBox.setEmailAddress("CTDrugSafety@scipharma.com");
		m_oMailBox.setMailDescription("SciPharma CT Drug Safety Mailbox");
		m_oMailBox.setTypeOfMailBox(SciEnums.MailBoxType.TEAM_SHARED.getCode());
		m_oMailBox.setModeOfOperation(SciEnums.ModeOfOperation.PORATAL_UI.getCode());
		m_oMailBox.setMailBoxMnemonics("SCPH-CT-{NNNNNN}");
		mbportalapp.saveMailBox(p_oUserSession,projectId, m_oMailBox,  true);
		
		ApplicationSequenceHelper.registerApplicationSequence(projectId, ApplicationSequencePurpose.MAILBOX,
				m_oMailBox.getSequenceId(), "SCPH-CT-{NNNNNN}");
		
		m_oMailBox = new MailBox();
		m_oMailBox.setProjectId(projectId);
		m_oMailBox.setCreationMode(CreationMode.SCRATCH);
		m_oMailBox.setState(RecordState.ACTIVE);
		m_oMailBox.setTemplateName("SciGen Drug Safety Mailbox");
		m_oMailBox.setEmailAddress("DrugSafety@scigenerics.com");
		m_oMailBox.setMailDescription("SciGenerics Drug Safety Mailbox");
		m_oMailBox.setTypeOfMailBox(SciEnums.MailBoxType.TEAM_SHARED.getCode());
		m_oMailBox.setModeOfOperation(SciEnums.ModeOfOperation.PORATAL_UI.getCode());
		m_oMailBox.setMailBoxMnemonics("SCGN-DS-{NNNNNN}");
		mbportalapp.saveMailBox(p_oUserSession,projectId, m_oMailBox,  true);
		ApplicationSequenceHelper.registerApplicationSequence(projectId, ApplicationSequencePurpose.MAILBOX,
				m_oMailBox.getSequenceId(), "SCGN-DS-{NNNNNN}");
	}
	catch(SciException scExc)
	{
	log.error("error saving email Box");
	}
	catch(SciServiceException sciExc)
	{
		log.error("error saving email Box");
	}
	}

/*private void saveMailInfo(UserSession p_oUserSession,  int projectId)
{
	MBMailInfo mailInfo = new MBMailInfo();
	MBPortalApp mbportalapp = new MBPortalApp();
	mailInfo.setMailBoxSeqId(39);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setMailAttachmentNames("File1,File 2");
	mailInfo.setMailRecipientsCc("girish,ajay");
	mailInfo.setMailRecipientsTo("Gaurav, Json");
	mailInfo.setMailSender("Girish");	
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	mailInfo.setMailBoxSeqId(39);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailAttachmentNames("File3,File 4");
	mailInfo.setMailRecipientsCc("girish,ajay");
	mailInfo.setMailRecipientsTo("Gaurav, Json");
	mailInfo.setMailSender("Prakash");
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailTimeStamp(new Date());
	mailInfo.setMailType(MailType.MAIL_IN);
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	mailInfo.setMailBoxSeqId(39);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailAttachmentNames("File1,File 2");
	mailInfo.setMailRecipientsCc("girish,Kunal");
	mailInfo.setMailRecipientsTo("Girish, Raul");
	mailInfo.setMailSender("Json");	
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	
	 mailInfo = new MBMailInfo();
	mbportalapp = new MBPortalApp();
	mailInfo.setMailBoxSeqId(40);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailAttachmentNames("Img 1,Img 2");
	mailInfo.setMailRecipientsCc("girish,ajay");
	mailInfo.setMailRecipientsTo("Shambhu, Json");
	mailInfo.setMailSender("Mayur");	
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	mailInfo.setMailBoxSeqId(40);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailAttachmentNames("Doc 1,Doc 2");
	mailInfo.setMailRecipientsCc("Ajay,Gaurav");
	mailInfo.setMailRecipientsTo("Girish, Raul");
	mailInfo.setMailSender("Rohini");	
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	mailInfo.setMailBoxSeqId(40);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(5);
	mailInfo.setMailAttachmentNames("Xlsx 2,Img 3");
	mailInfo.setMailRecipientsCc("girish,Kunal");
	mailInfo.setMailRecipientsTo("Girish, Raul");
	mailInfo.setMailSender("Rohini");	
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	mailInfo = new MBMailInfo();
	mbportalapp = new MBPortalApp();
	mailInfo.setMailBoxSeqId(41);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailAttachmentNames("Img 1,Img 2");
	mailInfo.setMailRecipientsCc("girish,ajay");
	mailInfo.setMailRecipientsTo("Shambhu, Json");
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setMailSender("Mayur");	
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	mailInfo.setMailBoxSeqId(41);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailAttachmentNames("Doc 16,Doc 23");
	mailInfo.setMailRecipientsCc("Roul,Gaurav");
	mailInfo.setMailRecipientsTo("Girish, shambhu");
	mailInfo.setMailSender("Json");	
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	mailInfo.setMailBoxSeqId(41);
	mailInfo.setDateCreated(new Date());
	mailInfo.setMailAttachmentCount(2);
	mailInfo.setMailAttachmentNames("Csv 2,Img 3");
	mailInfo.setMailRecipientsCc("json,Gaurav");
	mailInfo.setMailRecipientsTo("Girish, Json");
	mailInfo.setMailSender("Json");	
	mailInfo.setMailRecordMode(MailRecordMode.MAIL_MANUAL_ENTRY);
	mailInfo.setUserCreated(p_oUserSession.getUserInfo().getUserSeqId());
	mailInfo.setProjectSeqId(projectId);
	mailInfo.setMailType(MailType.MAIL_IN);
	mailInfo.setMailTimeStamp(new Date());
	try{
	mbportalapp.saveMBMail(p_oUserSession, projectId, mailInfo, true);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
}*/
}
