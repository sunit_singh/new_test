package com.sciformix.sciportal.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.AppInfo;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.audit.AuditWebManager;
import com.sciformix.sciportal.user.UserAuthHome;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.BootstrapUtils;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.WebConstants;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet 
{
	private static final Logger log = LoggerFactory.getLogger(LoginServlet.class);
	
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		UserInfo oUserInfo = null;
		String sUserId = null;
		String sUserPassword = null;
		UserSession oUserSession = null;
		String sSessionId = null;
		PortalAppResponse oAppResponse = null;
		List<AppInfo> listApps = null;
		
		oAppResponse = new PortalAppResponse(WebUtils.getLoginPageUri());
		
		if(!BootstrapUtils.isInstanceBootstrapped())
		{
			log.error("Login request rejected as system is not bootstrapped. Kindly contact the Administrator");
			oAppResponse.logError(WebErrorCodes.System.SYSTEM_NOT_BOOTSTRAPPED);
			WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
			WebUtils.forwardRequestToLoginPage(request, response);
			return;
		}
		
		if (WebUtils.getRequestParameter(request, WebConstants.REQUEST_PARAMETER_LOGREQUEST)!=null &&
				WebUtils.getRequestParameter(request, WebConstants.REQUEST_PARAMETER_LOGREQUEST).equals("yes"))
		{
			oUserSession = WebUtils.getUserSession(request);
			sSessionId = (oUserSession != null ? oUserSession.getSessionId() : null); 
			sUserId = (oUserSession != null ? oUserSession.getUserInfo().getUserId() : null);
			log.debug("Logout request received, Session invalidated. User: {}; SessionId: {}", sUserId, sSessionId);
			WebUtils.invalidateSession(request);
			
			if (oUserSession != null)
			{
				try
				{
					WebUtils.cleanUserTempFolder(oUserSession);
					oUserSession.updateClientIp(WebUtils.getSessionClientIp(request));
					oUserSession.updateClientHost(WebUtils.getSessionClientHost(request));
					AuditWebManager.auditLogout(oUserSession);
				}
				catch(SciServiceException serExcep)
				{
					oAppResponse.logError(serExcep);
					WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
					WebUtils.forwardRequestToLoginPage(request, response);
					return;
				}
			}
			else
			{
				log.error("Logout request received, Session invalidated [User: UNKNOWN; SessionId:UNKNOWN]");
			}
			
			WebUtils.forwardRequestToLoginPage(request, response);
		}
		else
		{
			sUserId = WebUtils.getRequestParameter(request, WebConstants.REQUEST_PARAMETER_USERNAME);
			
			if (sUserId != null)
			{
				log.debug("Login request received for user - {}", sUserId);
				
				try
				{
					if (!WebUtils.validateUserid(sUserId)) {
						log.error("Login request rejected for {} as Username contains invalid characters", sUserId);
						sUserId		=	StringUtils.replaceCharLoginUserid(sUserId);
						oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
						
						try
						{
							
							AuditWebManager.auditLoginAttempt(-1, sUserId, "Invalid user id", 
									WebUtils.getSessionServerIp(request), WebUtils.getSessionClientIp(request), WebUtils.getSessionClientHost(request));
						}
						catch(SciServiceException serExcep)
						{
							log.error("Error auditing failed login attempt - {}. Marking as invalid user id.", sUserId, serExcep);
							oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
						}
						
						WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
						WebUtils.forwardRequestToLoginPage(request, response);
						return;
					} else {
						oUserInfo = UserHome.retrieveUser(sUserId, UserHome.UserPurpose.PORTAL_LOGIN);	
					}
					
				}
				catch(SciServiceException serExcep)
				{
					log.error("Login request rejected for user - {}. Marking as incorrect user credentials.", sUserId, serExcep);
					oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
					WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
					WebUtils.forwardRequestToLoginPage(request, response);
					return;
				}
			}
			else
			{
				log.error("User credentials not supplied");
				WebUtils.forwardRequestToLoginPage(request, response);
				return;
			}
			
			if (oUserInfo == null)
			{
				log.error("Login request rejected for {} as User is not registered", sUserId);
				oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
				
				try
				{
					AuditWebManager.auditLoginAttempt(-1, sUserId, "User not registered", 
							WebUtils.getSessionServerIp(request), WebUtils.getSessionClientIp(request), WebUtils.getSessionClientHost(request));
				}
				catch(SciServiceException serExcep)
				{
					log.error("Error auditing failed login attempt - {}. Marking as incorrect user credentials.", sUserId, serExcep);
					oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
				}
				
				WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
				WebUtils.forwardRequestToLoginPage(request, response);
				return;
			}
			else
			{
				//TODO: Check for user state including disabled
				sUserPassword = WebUtils.getRequestParameter(request, WebConstants.REQUEST_PARAMETER_PASSWORD);
				
				boolean bAuthenticationSuccessful = false;
				
				try
				{
					bAuthenticationSuccessful = UserAuthHome.authenticateUserForLogin(oUserInfo, sUserPassword);
				}
				catch(SciWebException webExcep)
				{
					log.error("Error during authentication of user - {}", sUserId, webExcep);
					oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
					WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
					WebUtils.forwardRequestToLoginPage(request, response);
					return;
				}
				
				if (bAuthenticationSuccessful)
				{
					try
					{
						WebUtils.changeSessionId(request);// VAPT : Session Hijacking
						oUserSession = WebUtils.createWebUserSession(request, oUserInfo);
						AuditWebManager.auditLogin(oUserSession);
						sSessionId = oUserSession.getSessionId();
						log.debug("User authentication successful for {}; SessionId: {}", sUserId, sSessionId);
					}
					catch(SciServiceException serExcep)
					{
						log.error("Error auditing login attempt - {}. Marking as incorrect user credentials.", sUserId, serExcep);
						oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
						WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
						WebUtils.forwardRequestToLoginPage(request, response);
						return;
					}

					WebUtils.setUserSession(request, oUserSession);
					listApps = AppRegistry.listBusinessApps();
					WebUtils.setSessionAttribute(request, "listBusinessApps", listApps);
					WebUtils.forwardRequestToLoginRedirectPage(request, response);
					return;
				}
				else
				{
					log.error("Login request rejected for {} as user authentication failed", sUserId);
					
					try
					{
						//TODO: Pass the correct failure reason
						AuditWebManager.auditLoginAttempt(oUserInfo.getUserSeqId(), oUserInfo.getUserId(), "Incorrect user credentials", 
								WebUtils.getSessionServerIp(request), WebUtils.getSessionClientIp(request), WebUtils.getSessionClientHost(request));
						return;
					}
					catch(SciServiceException serExcep)
					{
						log.error("Error auditing failed login attempt - {}. Marking as incorrect user credentials.", sUserId, serExcep);
						return;
					}
					finally
					{
						oAppResponse.logError(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
						WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
						WebUtils.forwardRequestToLoginPage(request, response);
					}
				}
			}
		}
	}
}
