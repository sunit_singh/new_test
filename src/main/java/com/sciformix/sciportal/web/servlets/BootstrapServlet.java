package com.sciformix.sciportal.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.bootstrap.BootstrapInfo;
import com.sciformix.sciportal.utils.BootstrapUtils;
import com.sciformix.sciportal.utils.SystemUtils;
import com.sciformix.sciportal.utils.WebUtils;

/**
 * Servlet implementation class BootstrapServlet
 */
@WebServlet("/BootstrapServlet")
public class BootstrapServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BootstrapServlet() 
    {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String sBootstrapAction = null;
		BootstrapInfo oBootstrapInfo = null;
		
		String sDBHost = null;
		String sDBPort = null;
		String sDBSID = null;
		String sDBUsername = null;
		String sDBPassword = null;
		String sDBConnectionURL = null;
		String sDBSchemaOwner = null;
		
		String sLDAPDomain = null;
		String sLDAPUrl = null;
		String sLDAPUsername = null;
		String sLDAPPassword = null;
		String sNewAction = null;
		boolean bDBTestSuccessful = false;
		boolean bLDAPDetailsExist = false;
		boolean bLDAPSaveSuccessful = false;
		boolean bSystemBootstrapped = false;
		
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		
		if(!SystemUtils.isLocalRequest(request))
		{
			oStartupLogger.fatal("Bootstrap page cannot be accessed from remote address");
			request.getRequestDispatcher("WEB-INF/pages/b_error.jsp").forward(request, response);
			return;
		}
		
		if(StringUtils.isNullOrEmpty(SystemUtils.getInstallPath()))
		{
			sNewAction = "env_not_configured";
			WebUtils.setRequestAttribute(request, "new-portalaction", sNewAction);
			request.getRequestDispatcher("WEB-INF/pages/bootstrap.jsp").forward(request, response);
			return;
		}
		else
		{
			//continue
		}
		
		//TODO: Audit 
		sBootstrapAction = WebUtils.getRequestParameter(request, "bootstrap_action");
		
		if(sBootstrapAction == null)
		{
			bSystemBootstrapped = BootstrapUtils.isInstanceBootstrapped();
			if(bSystemBootstrapped)
			{
				sNewAction = "already_bootstrapped";
			}
			else
			{
				sNewAction = "get_db_details";
			}
		}
		else if (sBootstrapAction.equalsIgnoreCase("reset_bootstrap"))
		{
			sNewAction = "get_db_details";
		}
		else if (sBootstrapAction.equalsIgnoreCase("save_db"))
		{
			sDBHost = WebUtils.getRequestParameter(request, "dbhost");
			sDBPort = WebUtils.getRequestParameter(request, "dbport");
			sDBSID = WebUtils.getRequestParameter(request, "dbsid");
			sDBUsername = WebUtils.getRequestParameter(request, "dbusername");
			sDBPassword = WebUtils.getRequestParameter(request, "dbpassword");
			sDBSchemaOwner = WebUtils.getRequestParameter(request, "dbschemaowner");
			sDBConnectionURL = "jdbc:oracle:thin:@" + sDBHost + ":" + sDBPort + ":" + sDBSID;
			
			oBootstrapInfo = BootstrapInfo.createDBInfo(sDBHost, sDBPort, sDBSID, sDBUsername, sDBPassword, sDBConnectionURL, sDBSchemaOwner);
			
			try 
			{
				BootstrapUtils.initializeWorkingFolders();
				
				bDBTestSuccessful = BootstrapUtils.testAndSaveDBConnection(request, oBootstrapInfo);
			} 
			catch (SciException sciExcep) 
			{
				oStartupLogger.fatal("Error saving DB connection - " + sciExcep.getMessage());
			}
			
			if(!bDBTestSuccessful)
			{
				sNewAction = "get_db_details";
			}
			else
			{
				request.getSession().setAttribute("dbhost", sDBHost);
				request.getSession().setAttribute("dbport", sDBPort);
				request.getSession().setAttribute("dbsid", sDBSID);
				request.getSession().setAttribute("dbusername", sDBUsername);
				request.getSession().setAttribute("dbpassword", sDBPassword);
				request.getSession().setAttribute("dburl", sDBConnectionURL);
				
				try 
				{
					bLDAPDetailsExist = BootstrapUtils.fetchLDAPConnection(request, oBootstrapInfo);
					
					if(bLDAPDetailsExist)
					{
						sNewAction = "ldap_details_exist";
					}
					else
					{
						sNewAction = "get_ldap_details";
					}
				} 
				catch (SciException sciExcep) 
				{
					oStartupLogger.fatal("Error fetching LDAP connection - " + sciExcep.getMessage());
				}
			}
			WebUtils.setRequestAttribute(request, "s_dbtestsuccessful", Boolean.toString(bDBTestSuccessful));
			WebUtils.setRequestAttribute(request, "s_ldapdetailsexit", Boolean.toString(bLDAPDetailsExist));
		
		}
		else if (sBootstrapAction.equalsIgnoreCase("recreate_ldap"))
		{
			sNewAction = "get_ldap_details";
		}
		else if (sBootstrapAction.equalsIgnoreCase("save_ldap"))
		{
			sDBHost = (String) WebUtils.getSessionAttribute(request, "dbhost");
			sDBPort = (String) WebUtils.getSessionAttribute(request, "dbport");
			sDBSID = (String) WebUtils.getSessionAttribute(request, "dbsid");
			sDBUsername = (String) WebUtils.getSessionAttribute(request, "dbusername");
			sDBPassword = (String) WebUtils.getSessionAttribute(request, "dbpassword");
			sDBConnectionURL = (String) WebUtils.getSessionAttribute(request, "dburl");
			sDBSchemaOwner = (String) WebUtils.getSessionAttribute(request, "dbschemaowner");
			
			sLDAPDomain = WebUtils.getRequestParameter(request, "ldapdomain");
			sLDAPUrl = WebUtils.getRequestParameter(request, "ldapurl");
			sLDAPUsername = WebUtils.getRequestParameter(request, "ldapusername");
			sLDAPPassword = WebUtils.getRequestParameter(request, "ldappassword");
			
			oBootstrapInfo = BootstrapInfo.create(sDBHost, sDBPort, sDBSID, sDBUsername, sDBPassword, sDBConnectionURL, sDBSchemaOwner, 
													sLDAPDomain, sLDAPUrl, sLDAPUsername, sLDAPPassword);

			bLDAPSaveSuccessful = BootstrapUtils.testAndSaveLDAPConnection(request, oBootstrapInfo);
			if(!bLDAPSaveSuccessful)
			{
				sNewAction = "get_ldap_details";
			}
			else
			{
				sNewAction = "bootstrap_complete";
			}
			WebUtils.setRequestAttribute(request, "s_ldapsavesuccessful", Boolean.toString(bLDAPSaveSuccessful));
		}
		else if (sBootstrapAction.equalsIgnoreCase("exit"))
		{
			sNewAction = "bootstrap_complete";
		}
		else
		{
			//Invalid Case
			request.getRequestDispatcher("WEB-INF/pages/b_error.jsp").forward(request, response);
			return;
		}
		WebUtils.setRequestAttribute(request, "new-portalaction", sNewAction);
		request.getRequestDispatcher("WEB-INF/pages/bootstrap.jsp").forward(request, response);
	}
}
