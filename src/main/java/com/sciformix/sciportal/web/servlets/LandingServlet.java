package com.sciformix.sciportal.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.web.MultipartContentRequestWrapper;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.WebConstants;
import com.sciformix.sciportal.web.apps.IPortalWebApp;

/**
 * Servlet implementation class LandingServlet
 */
@WebServlet("/LandingServlet")
@MultipartConfig(fileSizeThreshold=WebConstants.FILE_UPLOAD_SERVLET_DISK_FACTORY_MEMORY_THRESHOLD)
public class LandingServlet extends HttpServlet 
{
	private static final Logger log = LoggerFactory.getLogger(LandingServlet.class);
	
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LandingServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		
		MultipartContentRequestWrapper oMultipartContentRequest = null;
		String sContentPageName = null;
		String sPortalAppId = null;
		String sPortalAction = null;
		IPortalWebApp oPortalWebApp = null;
		UserSession oUserSession = null;
		PortalAppResponse oAppResponse = null;
		
		try
		{
			// NOTE: Before processing the REQUEST object, it is IMPORTANT to check
			// if it is MULTI-PART CONTENT Request
			oUserSession = (UserSession) WebUtils.getSessionAttribute(request, WebConstants.SESSION_PARAMETER_SESSION);
			if (oUserSession == null)
			{
				response.sendRedirect(WebUtils.getSessionExpiredPageUri());
				return;
			}
			oUserSession.updateClientIp(WebUtils.getSessionClientIp(request));
			oUserSession.updateClientHost(WebUtils.getSessionClientHost(request));
		
			boolean isMultipartContentRequest = WebUtils.isMultipartContentRequest(request);
			if (isMultipartContentRequest)
			{
				try
				{
					oMultipartContentRequest = WebUtils.createMultipartContentRequestWrapper(request);
					if(log.isTraceEnabled())
					{
						log.trace("Multipart content request received");
					}
				}
				catch (SciWebException webExcep)
				{
					log.error("Error parsing multipart content request", webExcep);
					response.sendRedirect(WebUtils.getSessionExpiredPageUri());
					return;
				}
			}
			
			//Extract primary request parameters
			sPortalAppId = WebUtils.getRequestParameter(request, WebConstants.REQUEST_PARAMETER_PORTALAPPID, oMultipartContentRequest);
			sPortalAction = WebUtils.getRequestParameter(request, WebConstants.REQUEST_PARAMETER_PORTALACTION, oMultipartContentRequest);
			
			if(log.isTraceEnabled())
			{
				log.trace("Request parameters: PortalAppId:{}; PortalAction:{}", sPortalAppId, sPortalAction);
			}
			
			oPortalWebApp = (sPortalAppId != null ? AppRegistry.getWebApp(sPortalAppId) : null);
			if (oPortalWebApp == null)
			{
				//Default page
				sContentPageName = null;
				log.debug("Loading main content");
			}
			else
			{
				oAppResponse = oPortalWebApp.processAction(request, response, oMultipartContentRequest, sPortalAction);
				sContentPageName = oAppResponse.getNextPageUri();
				log.debug("PortalWebApp completed processing the action redirecting to - {}", sContentPageName);
			}
			
			request.setAttribute(WebConstants.REQUEST_PARAMETER_CONTENT_PAGE_NAME, sContentPageName);
			
			if (oAppResponse!=null && oAppResponse.isDownloadStreamPresent())
			{
				// Nothing to do BUT we've prevent double-forward
			}
			else
			{
				// Forward as normal
				if (WebUtils.getSessionExpiredPageUri().equals(sContentPageName)) {
					response.sendRedirect(WebUtils.getSessionExpiredPageUri());
					return;
				} else {
					WebUtils.forwardRequestToLandingPage(request, response);
				}
			}
		}
		catch(RuntimeException rtExcep)
		{
			log.error("Error processing action", rtExcep);
			oAppResponse = new PortalAppResponse(WebUtils.getLandingPageUri());
			oAppResponse.logError(WebErrorCodes.System.SYSTEM_HAS_ENCOUNTERED_AN_ISSUE);
			WebUtils.setRequestAttribute(request, WebConstants.RESPONSE_ERROR_LIST, oAppResponse.getErrorList());
			WebUtils.forwardRequestToLandingPage(request, response);
		}
	}

}
