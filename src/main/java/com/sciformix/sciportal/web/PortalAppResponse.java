package com.sciformix.sciportal.web;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciError;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes.SimpleServiceErrorCode;
import com.sciformix.commons.WebErrorCodes.SimpleWebErrorCode;
import com.sciformix.commons.WebErrorCodes.WebErrorCode;
import com.sciformix.sciportal.system.DisplayMessageHelper;
import com.sciformix.sciportal.utils.WebUtils;

public class PortalAppResponse
{
	private static final Logger log = LoggerFactory.getLogger(PortalAppResponse.class);
	
	private boolean m_bDownloadStreamPresent = false;
	private String m_sNextPageUri = null;
	private String m_sNextAction = null;
	private List<SciError> m_listErrors = null;
	
	public static final PortalAppResponse INVALID_SESSION_RESPONSE = new PortalAppResponse(WebUtils.getSessionExpiredPageUri(), null, null, false);
	
	public PortalAppResponse(String p_sNextPageUri)
	{
		this(p_sNextPageUri, null, null, false);
	}
	
	public PortalAppResponse(String p_sNextPageUri, PortalAppResponse p_oPortalAppResponse, boolean p_bDownLoadStreamPresent)
	{
		this(p_sNextPageUri, p_oPortalAppResponse.getNextAction(), p_oPortalAppResponse.getErrorList(), p_bDownLoadStreamPresent);
	}
	
	private PortalAppResponse(String p_sNextPageUri, String p_sNextAction, List<SciError> p_listErrors, boolean p_bDownLoadStreamPresent)
	{
		m_bDownloadStreamPresent = p_bDownLoadStreamPresent;
		m_sNextPageUri = p_sNextPageUri;
		m_sNextAction = p_sNextAction;
		m_listErrors = (p_listErrors == null ? new ArrayList<SciError>() : p_listErrors);
	}
	
	public boolean isDownloadStreamPresent()
	{
		return m_bDownloadStreamPresent;
	}
	
	public String getNextPageUri()
	{
		return m_sNextPageUri;
	}
	
	public String getNextAction()
	{
		return m_sNextAction;
	}
	
	public List<SciError> getErrorList()
	{
		return m_listErrors;
	}
	
	public boolean hasError()
	{
		if(m_listErrors != null && m_listErrors.size() > 0)
		{
			return true;
		}
		
		return false;
	}
	
	public void setNextAction(String p_sNextAction)
	{
		m_sNextAction = p_sNextAction;
	}

	public void setIsDownloadStreamPresent(boolean p_bIsDownloadStreamPresent)
	{
		m_bDownloadStreamPresent = p_bIsDownloadStreamPresent;
	}

	public void includeErrorResponse(PortalAppResponse p_oPortalAppResponse)
	{
		m_listErrors.addAll(p_oPortalAppResponse.getErrorList());
	}
	
	public void logError(SimpleWebErrorCode p_errorCode)
	{
		String sDisplayMessage = null;
		
		sDisplayMessage = DisplayMessageHelper.getDisplayMessage(p_errorCode);
		log.error("Exception - {}:{}", p_errorCode.errorCode(), sDisplayMessage);
		m_listErrors.add(new SciError(p_errorCode, sDisplayMessage));
	}

	public void logError(WebErrorCode p_errorCode, String p_payload)
	{
		String sDisplayMessage = null;
		
		sDisplayMessage = DisplayMessageHelper.getDisplayMessage(p_errorCode, p_payload);
		log.error("Exception - {}:{}", p_errorCode.errorCode(), sDisplayMessage);
		m_listErrors.add(new SciError(p_errorCode, sDisplayMessage));
	}
	
	public void logError(SciServiceException serExcep)
	{
		String sDisplayMessage = null;
		
		sDisplayMessage = DisplayMessageHelper.getDisplayMessage((SimpleServiceErrorCode)serExcep.errorCode(), serExcep.payload());
		log.error("Exception - {}:{}", serExcep.errorCode().errorCode(), sDisplayMessage, serExcep);
		m_listErrors.add(new SciError((SimpleServiceErrorCode)serExcep.errorCode(), sDisplayMessage));
	}
}
