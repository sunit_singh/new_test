package com.sciformix.sciportal.pvcp;

import java.util.Date;

public class PvcpCaseMaster {
	private int 	seqId;
	private int 	projectSeqId;
	private String 	caseId;
	private String 	caseVersion;
	private Date 	initialRecvDate;
	private String 	caseType;
	private String 	product;
	private String 	caseSeriousness;
	private int 	pcmDeSrc;
	private int 	pcmDeSrcType;
	private int 	pcmDeUserSeqId;
	private String 	pcmDeUserName;
	private int 	reviewType1Src;
	private int 	reviewType1SrcType;
	private int 	reviewType1UserSeqId;
	private String 	reviewType1UserName;
	private int 	reviewType2Src;
	private int 	reviewType2SrcType;
	private int 	reviewType2UserSeqId;
	private String 	reviewType2UserName;
	private int 	reviewType3Src;
	private int 	reviewType3SrcType;
	private int 	reviewType3UserSeqId;
	private String 	reviewType3UserName;
	private int 	reviewType4Src;
	private int 	reviewType4SrcType;
	private int 	reviewType4UserSeqId;
	private String 	reviewType4UserName;
	private int 	reviewType5Src;
	private int 	reviewType5SrcType;
	private int 	reviewType5UserSeqId;
	private String 	reviewType5UserName;
	private String	udf1;
	private String	udf2;
	private String	udf3;
	private String	udf4;
	private String	udf5;
	private String	udf6;
	private String	udf7;
	private String	udf8;
	private String	udf9;
	private String	udf10;
	private String 	internalCaseId;
	private Date	createdOn;
	private int 	createdBy;
	
	
	public int getSeqId() {
		return seqId;
	}
	public void setSeqId(int seqId) {
		this.seqId = seqId;
	}
	public int getProjectSeqId() {
		return projectSeqId;
	}
	public void setProjectSeqId(int projectSeqId) {
		this.projectSeqId = projectSeqId;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getCaseVersion() {
		return caseVersion;
	}
	public void setCaseVersion(String caseVersion) {
		this.caseVersion = caseVersion;
	}
	public Date getInitialRecvDate() {
		return initialRecvDate;
	}
	public void setInitialRecvDate(Date initialRecvDate) {
		this.initialRecvDate = initialRecvDate;
	}
	public String getCaseType() {
		return caseType;
	}
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getCaseSeriousness() {
		return caseSeriousness;
	}
	public void setCaseSeriousness(String caseSeriousness) {
		this.caseSeriousness = caseSeriousness;
	}
	public int getPcmDeSrc() {
		return pcmDeSrc;
	}
	public void setPcmDeSrc(int pcmDeSrc) {
		this.pcmDeSrc = pcmDeSrc;
	}
	public int getPcmDeSrcType() {
		return pcmDeSrcType;
	}
	public void setPcmcDeSrcType(int pcmDeSrcType) {
		this.pcmDeSrcType = pcmDeSrcType;
	}
	public int getPcmDeUserSeqId() {
		return pcmDeUserSeqId;
	}
	public void setPcmDeUserSeqId(int pcmDeUserSeqId) {
		this.pcmDeUserSeqId = pcmDeUserSeqId;
	}
	public String getPcmDeUserName() {
		return pcmDeUserName;
	}
	public void setPcmcDeUserName(String pcmDeUserName) {
		this.pcmDeUserName = pcmDeUserName;
	}
	public int getReviewType1Src() {
		return reviewType1Src;
	}
	public void setReviewType1Src(int reviewType1Src) {
		this.reviewType1Src = reviewType1Src;
	}
	public int getReviewType1SrcType() {
		return reviewType1SrcType;
	}
	public void setReviewType1SrcType(int reviewType1SrcType) {
		this.reviewType1SrcType = reviewType1SrcType;
	}
	public int getReviewType1UserSeqId() {
		return reviewType1UserSeqId;
	}
	public void setReviewType1UserSeqId(int reviewType1UserSeqId) {
		this.reviewType1UserSeqId = reviewType1UserSeqId;
	}
	public String getReviewType1UserName() {
		return reviewType1UserName;
	}
	public void setReviewType1UserName(String reviewType1UserName) {
		this.reviewType1UserName = reviewType1UserName;
	}
	public int getReviewType2Src() {
		return reviewType2Src;
	}
	public void setReviewType2Src(int reviewType2Src) {
		this.reviewType2Src = reviewType2Src;
	}
	public int getReviewType2SrcType() {
		return reviewType2SrcType;
	}
	public void setReviewType2SrcType(int reviewType2SrcType) {
		this.reviewType2SrcType = reviewType2SrcType;
	}
	public int getReviewType2UserSeqId() {
		return reviewType2UserSeqId;
	}
	public void setReviewType2UserSeqId(int reviewType2UserSeqId) {
		this.reviewType2UserSeqId = reviewType2UserSeqId;
	}
	public String getReviewType2UserName() {
		return reviewType2UserName;
	}
	public void setReviewType2UserName(String reviewType2UserName) {
		this.reviewType2UserName = reviewType2UserName;
	}
	public int getReviewType3Src() {
		return reviewType3Src;
	}
	public void setReviewType3Src(int reviewType3Src) {
		this.reviewType3Src = reviewType3Src;
	}
	public int getReviewType3SrcType() {
		return reviewType3SrcType;
	}
	public void setReviewType3SrcType(int reviewType3SrcType) {
		this.reviewType3SrcType = reviewType3SrcType;
	}
	public int getReviewType3UserSeqId() {
		return reviewType3UserSeqId;
	}
	public void setReviewType3UserSeqId(int reviewType3UserSeqId) {
		this.reviewType3UserSeqId = reviewType3UserSeqId;
	}
	public String getReviewType3UserName() {
		return reviewType3UserName;
	}
	public void setReviewType3UserName(String reviewType3UserName) {
		this.reviewType3UserName = reviewType3UserName;
	}
	public int getReviewType4Src() {
		return reviewType4Src;
	}
	public void setReviewType4Src(int reviewType4Src) {
		this.reviewType4Src = reviewType4Src;
	}
	public int getReviewType4SrcType() {
		return reviewType4SrcType;
	}
	public void setReviewType4SrcType(int reviewType4SrcType) {
		this.reviewType4SrcType = reviewType4SrcType;
	}
	public int getReviewType4UserSeqId() {
		return reviewType4UserSeqId;
	}
	public void setReviewType4UserSeqId(int reviewType4UserSeqId) {
		this.reviewType4UserSeqId = reviewType4UserSeqId;
	}
	public String getReviewType4UserName() {
		return reviewType4UserName;
	}
	public void setReviewType4UserName(String reviewType4UserName) {
		this.reviewType4UserName = reviewType4UserName;
	}
	public int getReviewType5Src() {
		return reviewType5Src;
	}
	public void setReviewType5Src(int reviewType5Src) {
		this.reviewType5Src = reviewType5Src;
	}
	public int getReviewType5SrcType() {
		return reviewType5SrcType;
	}
	public void setReviewType5SrcType(int reviewType5SrcType) {
		this.reviewType5SrcType = reviewType5SrcType;
	}
	public int getReviewType5UserSeqId() {
		return reviewType5UserSeqId;
	}
	public void setReviewType5UserSeqId(int reviewType5UserSeqId) {
		this.reviewType5UserSeqId = reviewType5UserSeqId;
	}
	public String getReviewType5UserName() {
		return reviewType5UserName;
	}
	public void setReviewType5UserName(String reviewType5UserName) {
		this.reviewType5UserName = reviewType5UserName;
	}
	public String getUdf1() {
		return udf1;
	}
	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}
	public String getUdf2() {
		return udf2;
	}
	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}
	public String getUdf3() {
		return udf3;
	}
	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}
	public String getUdf4() {
		return udf4;
	}
	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}
	public String getUdf5() {
		return udf5;
	}
	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}
	public String getUdf6() {
		return udf6;
	}
	public void setUdf6(String udf6) {
		this.udf6 = udf6;
	}
	public String getUdf7() {
		return udf7;
	}
	public void setUdf7(String udf7) {
		this.udf7 = udf7;
	}
	public String getUdf8() {
		return udf8;
	}
	public void setUdf8(String udf8) {
		this.udf8 = udf8;
	}
	public String getUdf9() {
		return udf9;
	}
	public void setUdf9(String udf9) {
		this.udf9 = udf9;
	}
	public String getUdf10() {
		return udf10;
	}
	public void setUdf10(String udf10) {
		this.udf10 = udf10;
	}
	public String getInternalCaseId() {
		return internalCaseId;
	}
	public void setInternalCaseId(String internalCaseId) {
		this.internalCaseId = internalCaseId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
}
