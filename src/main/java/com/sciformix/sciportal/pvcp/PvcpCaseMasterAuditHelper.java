package com.sciformix.sciportal.pvcp;

public class PvcpCaseMasterAuditHelper
{
	
	public static class AuditAttributes
	{
		public static final String CASE_SEQ_ID = "Case Sequence Id"; 
		public static final String CASE_ID = "Case Id"; 
		public static final String CASE_VERSION = "Case Version";
		public static final String USERID = "User Id";
		public static final String MR_NAME = "MR Name";
		public static final String PR_NAME = "PR Name";
		public static final String DE_NAME = "DE Name";
		public static final String DE_SEQ_ID = "DE Seq Id";
		
		public static final String CASE_IRD= "Case IRD";
		public static final String CASE_SERIOUSNESS = "Case Seriousness";
		public static final String CASE_TYPE = "Case Type";
		public static final String PRODUCT = "Product";
		public static final String UDF_1 = "UDF 1";
		public static final String UDF_2 = "UDF 2";
		public static final String UDF_3 = "UDF 3";
		public static final String UDF_4 = "UDF 4";
		public static final String UDF_5 = "UDF 5";
		public static final String UDF_6 = "UDF 6";
		public static final String UDF_7 = "UDF 7";
		public static final String UDF_8 = "UDF 8";
		public static final String UDF_9 = "UDF 9";
		public static final String UDF_10 = "UDF 10";
		public static final String REVIEW_TYPE_1 = "Review Type 1";
		public static final String REVIEW_TYPE_2 = "Review Type 2";
		public static final String REVIEW_TYPE_3 = "Review Type 3";
		public static final String REVIEW_TYPE_4 = "Review Type 4";
		public static final String REVIEW_TYPE_5 = "Review Type 5";
		
	}
	
	private PvcpCaseMasterAuditHelper()
	{
		//Nothing to do
	}

}
