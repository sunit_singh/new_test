package com.sciformix.sciportal.pvcp;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class PvcpCaseMasterDbSchema
{
	
	public static CaseMasterDb PCVP_CASEMASTER = new CaseMasterDb();
	
	public static class CaseMasterDb extends DbTable
	{
		public DbColumn CASESEQID= null;
		public DbColumn PRJSEQID= null;
		public DbColumn PCMCASEID= null;
		public DbColumn PCMCASEVERSION= null;
		public DbColumn PCMCASE_IRD= null;
		public DbColumn PCMCASETYPE= null;
		public DbColumn PCMCASE_PRODUCT= null;
		public DbColumn PCMCASESERIOUSNESS= null;
		
		public DbColumn PCMDESRC= null;
		public DbColumn PCMDETYPE= null;
		public DbColumn PCMDEUSERSEQID= null;
		public DbColumn PCMDEUSERNAME= null;
		
		public DbColumn PCMRT1SRC= null;
		public DbColumn PCMRT1TYPE= null;
		public DbColumn PCMRT1USERSEQID= null;
		public DbColumn PCMRT1USERNAME= null;
		public DbColumn PCMRT2SRC= null;
		public DbColumn PCMRT2TYPE= null;
		public DbColumn PCMRT2USERSEQID= null;
		public DbColumn PCMRT2USERNAME= null;
		public DbColumn PCMRT3SRC= null;
		public DbColumn PCMRT3TYPE= null;
		public DbColumn PCMRT3USERSEQID= null;
		public DbColumn PCMRT3USERNAME= null;
		
		public DbColumn PCMRT4SRC= null;
		public DbColumn PCMRT4TYPE= null;
		public DbColumn PCMRT4USERSEQID= null;
		public DbColumn PCMRT4USERNAME= null;
		public DbColumn PCMRT5SRC= null;
		public DbColumn PCMRT5TYPE= null;
		public DbColumn PCMRT5USERSEQID= null;
		public DbColumn PCMRT5USERNAME= null;
		
		
		
		
		public DbColumn PCMUDF1= null;
		public DbColumn PCMUDF2= null;
		public DbColumn PCMUDF3= null;
		public DbColumn PCMUDF4= null;
		public DbColumn PCMUDF5= null;
		public DbColumn PCMUDF6= null;
		public DbColumn PCMUDF7= null;
		public DbColumn PCMUDF8= null;
		public DbColumn PCMUDF9= null;
		public DbColumn PCMUDF10= null;
		
		public DbColumn PCMINTERNALCASEID= null;
		public DbColumn DTCREATED= null;
		public DbColumn USRCREATED= null;
			
		
		public CaseMasterDb()
		{
			super("PVCP_CASE_MASTER");
			setPkSequenceName("PVCP_CASE_MASTER_SEQ");
			
			CASESEQID= addColumn("PCMCASESEQID");
			PRJSEQID=  addColumn("PRJSEQID");
			PCMCASEID= addColumn("PCMCASEID");
			PCMCASEVERSION= addColumn("PCMCASEVERSION");
			PCMCASE_IRD= addColumn("PCMCASE_IRD");
			PCMCASETYPE= addColumn("PCMCASETYPE");
			PCMCASE_PRODUCT= addColumn("PCMCASE_PRODUCT");
			PCMCASESERIOUSNESS= addColumn("PCMCASESERIOUSNESS");
			
			PCMDESRC= addColumn("PCMDESRC");
			PCMDETYPE= addColumn("PCMDETYPE");		
			PCMDEUSERSEQID= addColumn("PCMDEUSERSEQID");
			PCMDEUSERNAME= addColumn("PCMDEUSERNAME");
			
			PCMRT1SRC= addColumn("PCMRT1SRC");
			PCMRT1TYPE= addColumn("PCMRT1TYPE");
			PCMRT1USERSEQID=addColumn("PCMRT1USERSEQID");
			PCMRT1USERNAME= addColumn("PCMRT1USERNAME");
			PCMRT2SRC= addColumn("PCMRT2SRC");
			PCMRT2TYPE= addColumn("PCMRT2TYPE");
			PCMRT2USERSEQID= addColumn("PCMRT2USERSEQID");
			PCMRT2USERNAME= addColumn("PCMRT2USERNAME");
			PCMRT3SRC= addColumn("PCMRT3SRC");
			PCMRT3TYPE= addColumn("PCMRT3TYPE");
			PCMRT3USERSEQID= addColumn("PCMRT3USERSEQID");
			PCMRT3USERNAME= addColumn("PCMRT3USERNAME");
			PCMRT4SRC= addColumn("PCMRT4SRC");
			PCMRT4TYPE= addColumn("PCMRT4TYPE");
			PCMRT4USERSEQID= addColumn("PCMRT4USERSEQID");
			PCMRT4USERNAME= addColumn("PCMRT4USERNAME");
			PCMRT5SRC= addColumn("PCMRT5SRC");
			PCMRT5TYPE= addColumn("PCMRT5TYPE");
			PCMRT5USERSEQID= addColumn("PCMRT5USERSEQID");
			PCMRT5USERNAME= addColumn("PCMRT5USERNAME");
			PCMUDF1= addColumn("PCMUDF1");
			PCMUDF2= addColumn("PCMUDF2");
			PCMUDF3= addColumn("PCMUDF3");
			PCMUDF4= addColumn("PCMUDF4");
			PCMUDF5= addColumn("PCMUDF5");
			PCMUDF6= addColumn("PCMUDF6");
			PCMUDF7= addColumn("PCMUDF7");
			PCMUDF8= addColumn("PCMUDF8");
			PCMUDF9= addColumn("PCMUDF9");
			PCMUDF10= addColumn("PCMUDF10");
			PCMINTERNALCASEID= addColumn("PCMINTERNALCASEID");
			DTCREATED= addColumn("DTCREATED");
			USRCREATED= addColumn("USRCREATED");
		}
	}

}
