package com.sciformix.sciportal.jobs;

import java.util.Map;

import com.sciformix.commons.SciServiceException;

public interface ExecuteJobHelper {

	public void executeJob(Map<String,String> map)throws SciServiceException; 
}
