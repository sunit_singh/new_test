/**
 * 
 */
package com.sciformix.sciportal.authenticator;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.utils.LDAPUtils;

/**
 * @author sshetty1
 *
 */
public class LDAPAuthenticator 
{
	//TODO:convert to interface
	private static final Logger log = LoggerFactory.getLogger(LDAPAuthenticator.class);
	
	public static final String LDAP_BINDING_USERNAME = "ldap.binding.username";

	public static final String LDAP_BINDING_PASSWORD = "ldap.binding.password";

	public static final String LDAP_DOMAIN = "ldap.domain";

	public static final String LDAP_URL = "ldap.url";

	private static final String LDAP_INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	
	private static final String LDAP_SECURITY_MODE = "simple";
	
	public static boolean authenticate(String p_sUserId, String p_sUserPassword) throws SciException
	{
		String sBindingUserName = null;
		String sBindingPassword = null;
		String sEncryptedBindingPassword = null;
		String sLDAPDomain = null;
		String sLDAPUrl = null;
		Hashtable<String, String> htLDAPContextProps = new Hashtable<String, String>();
		LdapContext oAdminLDAPContext = null;
		LdapContext oUserLDAPContext = null;
		SearchControls oSearchControls = null;
		@SuppressWarnings("rawtypes")
		NamingEnumeration oLDAPEnumeration = null;

		try 
		{
			//Validate user id
			if(!LDAPUtils.isValid(p_sUserId))
			{
				log.error("LDAPAuthenticator.authenticate(): Invalid User Id - {}", p_sUserId);
				throw new SciException("Invalid User Id", null);
			}
			
			sBindingUserName = LDAPUtils.getLDAPPrimaryUsername();
			sEncryptedBindingPassword = LDAPUtils.getLDAPPrimaryPassword();
			sLDAPDomain = LDAPUtils.getLDAPPrimaryDomain();
			sLDAPUrl = LDAPUtils.getLDAPPrimaryURL();
			
			//Validate
			if(!LDAPUtils.isValid(sBindingUserName))
			{
				log.error("LDAPAuthenticator.authenticate(): Invalid Binding User Name - {}", sBindingUserName);
				throw new SciException("Invalid Binding User Name", null);
			}
			
			sBindingPassword = LDAPUtils.decrypt(sEncryptedBindingPassword);
	
			htLDAPContextProps.put(Context.SECURITY_PRINCIPAL, sBindingUserName + "@" + sLDAPDomain);
			htLDAPContextProps.put(Context.SECURITY_CREDENTIALS, sBindingPassword);
			htLDAPContextProps.put(Context.INITIAL_CONTEXT_FACTORY,LDAP_INITIAL_CONTEXT_FACTORY);
			htLDAPContextProps.put(Context.PROVIDER_URL, sLDAPUrl);
			htLDAPContextProps.put(Context.SECURITY_AUTHENTICATION, LDAP_SECURITY_MODE);
			
			oAdminLDAPContext = new InitialLdapContext(htLDAPContextProps, null);
		}
		catch (Exception excep)
		{
			log.error("LDAPAuthenticator.authenticate() - Binding error: Please check your LDAP config", excep);
			return false;
		}
		
		try 
		{
			oSearchControls = new SearchControls();
			oSearchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			oSearchControls.setCountLimit(1);
		
			oLDAPEnumeration = oAdminLDAPContext.search(sLDAPUrl, "sAMAccountName="+ p_sUserId, oSearchControls);
			if (oLDAPEnumeration.hasMore()) 
			{
				htLDAPContextProps.put(Context.SECURITY_PRINCIPAL, p_sUserId  + "@" + sLDAPDomain);
				htLDAPContextProps.put(Context.SECURITY_CREDENTIALS, p_sUserPassword);
				oUserLDAPContext = new InitialLdapContext(htLDAPContextProps, null);
				return true;
			}
		} 
		catch (Exception excep)
		{
			log.error("LDAPAuthenticator.authenticate(): Exception for user - {}", p_sUserId, excep);
			return false;
		}
		finally
		{
			if(oUserLDAPContext != null)
			{
				try
				{
					oUserLDAPContext.close();
			
				}
				catch (Exception excep)
				{
					log.error("LDAPAuthenticator.authenticate() - Error while closing the user ldap context. Ignore and continue", excep);
				}
			} 
			
			if(oAdminLDAPContext != null)
			{
				try
				{
					oAdminLDAPContext.close();
				}
				catch (Exception excep)
				{
					log.error("LDAPAuthenticator.authenticate() - Error while closing the admin LDAPContext. Ignore and continue", excep);
				}
			}
		}
	 
		return false;
	}
	
	public static boolean authenticateAdmin(String sBindingUserName, String sBindingPassword, String sLDAPDomain, String sLDAPUrl) throws SciException
	{
		Hashtable<String, String> htLDAPContextProps = new Hashtable<String, String>();
		LdapContext oAdminLDAPContext = null;
		
		try
		{
			if(!LDAPUtils.isValid(sBindingUserName))
			{
				log.error("LDAPAuthenticator.authenticate(): Invalid Binding User Name - {}", sBindingUserName);
				throw new SciException("Invalid Binding User Name", null);
			}
			
			htLDAPContextProps.put(Context.SECURITY_PRINCIPAL, sBindingUserName + "@" + sLDAPDomain);
			htLDAPContextProps.put(Context.SECURITY_CREDENTIALS, sBindingPassword);
			htLDAPContextProps.put(Context.INITIAL_CONTEXT_FACTORY,LDAP_INITIAL_CONTEXT_FACTORY);
			htLDAPContextProps.put(Context.PROVIDER_URL, sLDAPUrl);
			htLDAPContextProps.put(Context.SECURITY_AUTHENTICATION, LDAP_SECURITY_MODE);
			
			oAdminLDAPContext = new InitialLdapContext(htLDAPContextProps, null);
			return true;
		}
		catch (Exception excep)
		{
			log.error("LDAPAuthenticator.testAdminAuthentication() - Binding error: Please check your LDAP config", excep);
			return false;
		}
		finally
		{
			if(oAdminLDAPContext != null)
			{
				try
				{
					oAdminLDAPContext.close();
				}
				catch (Exception excep)
				{
					log.error("LDAPAuthenticator.testAdminAuthentication() - Error while closing the admin LDAPContext. Ignore and continue", excep);
				}
			} 
		}
	}
}
