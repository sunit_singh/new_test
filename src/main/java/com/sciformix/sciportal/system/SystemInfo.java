package com.sciformix.sciportal.system;

import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.utils.SystemUtils;

public class SystemInfo {

	private static final String SYSTEM_NAME_KEY = "SYSTEMCONFIG.system.name";
	private static final String SYSTEM_TYPE_KEY = "SYSTEMCONFIG.system.type";
	private static final String APPLICATION_VERSION_KEY = "SYSTEMCONFIG.system.version";
	public static final String SETTINGS_NOT_AVAILABLE = "(Not Available)";
	
	private String name = null;
	private String type = null;
	private String bootstrap = null;
	private String applicationVersion = null;
	
	public static SystemInfo systemInfo = new SystemInfo();
	
	public static SystemInfo getSystemInfo() {
		return systemInfo;
	}

	public String getName() {
		
		name = ConfigHome.getSysConfigSetting(SYSTEM_NAME_KEY);
		if(StringUtils.isNullOrEmpty(name))
		{
			name  = SETTINGS_NOT_AVAILABLE;
		}
		return name; 
	}

	public String getType() {
		type = ConfigHome.getSysConfigSetting(SYSTEM_TYPE_KEY);
		
		if(StringUtils.isNullOrEmpty(type))
		{
			type  = SETTINGS_NOT_AVAILABLE;
		}
		
		return type;
	}

	public String getApplicationVersion() {
		applicationVersion = ConfigHome.getSysConfigSetting(APPLICATION_VERSION_KEY);
		
		if(StringUtils.isNullOrEmpty(applicationVersion))
		{
			applicationVersion  = SETTINGS_NOT_AVAILABLE;
		}
		
		return applicationVersion;
	}

	public String getBootstrap()
	{
		bootstrap = ConfigHome.getSysConfigSetting(SystemUtils.SYSTEM_CONFIG.SYSTEM_BOOTSTRAPPED.toString());
		if(StringUtils.isNullOrEmpty(bootstrap))
		{
			bootstrap = SETTINGS_NOT_AVAILABLE;
		}
		
		if(bootstrap.equals("true"))
		{
			bootstrap = "Yes";
		}else
		{
			bootstrap = "No";
		}
		return bootstrap;
	}

	public static String getPrimaryDbInfo()
	{
		return ConfigHome.getPrimaryDbInfo();
	}

}
