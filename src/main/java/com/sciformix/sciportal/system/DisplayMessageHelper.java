/**
 * 
 */
package com.sciformix.sciportal.system;

import java.text.MessageFormat;

import com.sciformix.commons.ErrorCode;
import com.sciformix.commons.ServiceErrorCodes.SimpleServiceErrorCode;
import com.sciformix.commons.WebErrorCodes.SimpleWebErrorCode;
import com.sciformix.commons.WebErrorCodes.WebErrorCode;
import com.sciformix.commons.utils.StringUtils;

/**
 * @author sshetty1
 *
 */
public class DisplayMessageHelper 
{

	private static final String UNSPECIFIED_ERROR_MESSAGE = "Unspecified Error";
	
	public static String getDisplayMessage(WebErrorCode p_errorCode, String... p_payload)
	{
		String sMessageText = null;
		
		sMessageText = getWebErrorMessageText(p_errorCode);
		
		return MessageFormat.format(sMessageText, p_payload);
	}

	public static String getDisplayMessage(SimpleWebErrorCode p_errorCode)
	{
		String sMessageText = null;
		
		sMessageText = getWebErrorMessageText(p_errorCode);
		
		return sMessageText;
	}
	
	public static String getDisplayMessage(SimpleServiceErrorCode p_errorCode, String... p_payload)
	{
		String sMessageText = null;
		
		sMessageText = getServiceErrorMessageText(p_errorCode);
		
		return MessageFormat.format(sMessageText, p_payload);
	}
	
	private static String getWebErrorMessageText(ErrorCode p_errorCode)
	{
		String sMessageText = null;
		
		sMessageText = ErrorCodeMessageHome.lookupErrorCode("web", p_errorCode.namespace(), p_errorCode.errorCode());
		
		if(StringUtils.isNullOrEmpty(sMessageText))
		{
			return UNSPECIFIED_ERROR_MESSAGE;
		}
		
		return sMessageText;
	}

	private static String getServiceErrorMessageText(ErrorCode p_errorCode)
	{
		String sMessageText = null;
		
		sMessageText = ErrorCodeMessageHome.lookupErrorCode("svc", p_errorCode.namespace(), p_errorCode.errorCode());
		
		if(StringUtils.isNullOrEmpty(sMessageText))
		{
			return UNSPECIFIED_ERROR_MESSAGE;
		}
		
		return sMessageText;
	}

}
