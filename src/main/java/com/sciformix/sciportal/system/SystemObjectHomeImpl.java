package com.sciformix.sciportal.system;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

public class SystemObjectHomeImpl
{
	private static final Logger log = LoggerFactory.getLogger(SystemObjectHomeImpl.class);

	public static List<ErrorCodeMessage> loadErrorCodeMessages(DbConnection connection) throws SciException
	{
		List<ErrorCodeMessage> listErrorCodeMsgs = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(SystemDbSchema.ERROR_CODE_MESSAGES, null);
		
		listErrorCodeMsgs = fetchErrorCodeMessages(connection, oSelectQuery);
		
		return listErrorCodeMsgs;
	}
	
	private static List<ErrorCodeMessage> fetchErrorCodeMessages(DbConnection connection, DbQuerySelect p_oSelectQuery) throws SciException
	{
		List<ErrorCodeMessage> listErrorCodeMsgs = null;
		ErrorCodeMessage oErrorCodeMsg = null;
		
		listErrorCodeMsgs = new ArrayList<ErrorCodeMessage>();
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(p_oSelectQuery))
			{
				while (resultSet.next())
				{
					oErrorCodeMsg = readErrorCodeMsgObjectFromDb(resultSet);
					listErrorCodeMsgs.add(oErrorCodeMsg);
				}
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading error code messages", dbExcep);
			throw new SciException("error reading error code messages", dbExcep);
		}
		return listErrorCodeMsgs;
	}
	
	private static ErrorCodeMessage readErrorCodeMsgObjectFromDb(DbResultSet resultSet) throws SciDbException
	{
		ErrorCodeMessage oErrorCodeMsg = null;
		
		oErrorCodeMsg = new ErrorCodeMessage();
		oErrorCodeMsg.setCategory(resultSet.readString(SystemDbSchema.ERROR_CODE_MESSAGES.ERRCATEGORY));
		oErrorCodeMsg.setNamespace(resultSet.readString(SystemDbSchema.ERROR_CODE_MESSAGES.ERRNAMESPACE));
		oErrorCodeMsg.setErrorCode(resultSet.readInt(SystemDbSchema.ERROR_CODE_MESSAGES.ERRCODE));
		oErrorCodeMsg.setLang(resultSet.readString(SystemDbSchema.ERROR_CODE_MESSAGES.MSGLANG));
		oErrorCodeMsg.setErrorMessage(resultSet.readString(SystemDbSchema.ERROR_CODE_MESSAGES.MSGTEXT));
		
		return oErrorCodeMsg;
	}


}
