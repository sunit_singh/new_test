package com.sciformix.sciportal.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class ErrorCodeMessageHome
{
	
	private static Map<String, String> s_mapErrorCodeMessages = null;
	
	private static String _FORMAT_ERROR_CODE_MESSAGE_MAP_KEY_ = "%s.%s.%s";
	
	private static String prepareKey(ErrorCodeMessage p_oErrorCodeMsg)
	{
		return String.format(_FORMAT_ERROR_CODE_MESSAGE_MAP_KEY_, p_oErrorCodeMsg.getCategory(), p_oErrorCodeMsg.getNamespace(), p_oErrorCodeMsg.getErrorCode());
	}
	
	private static String prepareKey(String p_sCategory, String p_sNamespace, int p_nErrorCode)
	{
		return String.format(_FORMAT_ERROR_CODE_MESSAGE_MAP_KEY_, p_sCategory, p_sNamespace, p_nErrorCode);
	}
	
	private ErrorCodeMessageHome()
	{
		//Nothing to do
	}
	
	public static boolean init(SciStartupLogger oStartupLogger)
	{
		boolean bSuccessful = false;
		
		List<ErrorCodeMessage> listErrorCodeMessages = null;
		
		s_mapErrorCodeMessages = new HashMap<String, String>();
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			listErrorCodeMessages = SystemObjectHomeImpl.loadErrorCodeMessages(connection);
			
			for (ErrorCodeMessage oErrorMsg : listErrorCodeMessages)
			{
				s_mapErrorCodeMessages.put(prepareKey(oErrorMsg), oErrorMsg.getErrorMessage());
			}
			bSuccessful = true;
		}
		catch (SciException sciExcep)
		{
			oStartupLogger.logError("Error loading Error Code message details" + sciExcep.getMessage());
		}
		catch (SciDbException dbExcep)
		{
			oStartupLogger.logError("Error loading Error Code message details" + dbExcep.getMessage());
		}
		
		return bSuccessful;
	}
	
	public static String lookupErrorCode(String p_sCategory, String p_sNamespace, int p_nErrorCode)
	{
		//TODO:Remove this check after 'init' method has been called
		if (s_mapErrorCodeMessages == null)
		{
			return "System Not Bootstrapped. Kindly contact the Administrator";
		}
		return s_mapErrorCodeMessages.get(prepareKey(p_sCategory, p_sNamespace, p_nErrorCode));
	}
	
	
}
