package com.sciformix.sciportal.system;

public class ErrorCodeMessage
{
	private String m_sCategory = null;
	private String m_sNamespace = null;
	private int m_nErrorCode = -1;
	private String m_sLang = null;
	private String m_sErrorMessage = null;
	
	ErrorCodeMessage()
	{
	}

	public String getCategory() {
		return m_sCategory;
	}

	void setCategory(String p_sCategory) {
		this.m_sCategory = p_sCategory;
	}

	public String getNamespace() {
		return m_sNamespace;
	}

	void setNamespace(String p_sNamespace) {
		this.m_sNamespace = p_sNamespace;
	}

	public int getErrorCode() {
		return m_nErrorCode;
	}

	void setErrorCode(int p_nErrorCode) {
		this.m_nErrorCode = p_nErrorCode;
	}

	public String getLang() {
		return m_sLang;
	}

	void setLang(String p_sLang) {
		this.m_sLang = p_sLang;
	}

	public String getErrorMessage() {
		return m_sErrorMessage;
	}

	void setErrorMessage(String p_sErrorMessage) {
		this.m_sErrorMessage = p_sErrorMessage;
	}
	
}
