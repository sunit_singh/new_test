package com.sciformix.sciportal.system;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciStartupLogger;
import com.sciformix.sciportal.utils.SystemUtils;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;


public class LoggingHome 
{

	public static final String EXTENDED_LOGGING_PROPERTIES_FILE_NAME = "logback-ext.xml";
	
	public static boolean init(SciStartupLogger oStartupLogger)
	{
		boolean bSuccessful = false;
		LoggerContext oLoggerContext = null;
		JoranConfigurator oConfigurator = null;
		File oExtConfigFile = null;
		String sExtConfigFilePath = null;
		
		try
		{
			//Check if ext configuration for logback exists; if yes, override
			sExtConfigFilePath = FilenameUtils.concat(SystemUtils.getConfigFolderPath(), EXTENDED_LOGGING_PROPERTIES_FILE_NAME);
			oExtConfigFile = new File(sExtConfigFilePath);
			
			if(oExtConfigFile.isFile())
			{
				oLoggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
				try 
				{
					oConfigurator = new JoranConfigurator();
				    oConfigurator.setContext(oLoggerContext);
				    oLoggerContext.reset();
				    oConfigurator.doConfigure(sExtConfigFilePath);
				} 
				catch (JoranException jeExcep) 
				{
					oStartupLogger.fatal("Exception overriding log config" + jeExcep.getMessage());
				}
				
				StatusPrinter.printInCaseOfErrorsOrWarnings(oLoggerContext);
				oStartupLogger.log("Log config file overriden - " + sExtConfigFilePath);
			}
			else
			{
				//Continue with default configuration
			}
			oStartupLogger.log("Continuing with default log config file");
			bSuccessful = true;
		}
		catch(Exception excep)
		{
			oStartupLogger.logError("Exception initiliazing logging - " + excep.getMessage());
		}
		
		return bSuccessful;
	}
	
}
