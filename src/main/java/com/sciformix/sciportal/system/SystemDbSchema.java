package com.sciformix.sciportal.system;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class SystemDbSchema
{
	public static final ErrorCodeMessages ERROR_CODE_MESSAGES = new ErrorCodeMessages();
	
	public static class ErrorCodeMessages extends DbTable
	{
		public DbColumn ERRCATEGORY = null;
		public DbColumn ERRNAMESPACE = null;
		public DbColumn ERRCODE = null;
		public DbColumn MSGLANG = null;
		public DbColumn MSGTEXT = null;
		
		public ErrorCodeMessages()
		{
			super("SYS_ERRORMSGS");
			
			ERRCATEGORY = addColumn("ERRCATEGORY");
			ERRNAMESPACE = addColumn("ERRNAMESPACE");
			ERRCODE = addColumn("ERRCODE");
			MSGLANG = addColumn("MSGLANG");
			MSGTEXT = addColumn("MSGTEXT");
		}
	}
}
