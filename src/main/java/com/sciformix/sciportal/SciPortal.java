package com.sciformix.sciportal;

import java.net.InetAddress;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.apps.file.XmlParserHome;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.system.ErrorCodeMessageHome;
import com.sciformix.sciportal.system.LoggingHome;
import com.sciformix.sciportal.threads.JobTaskScheduler;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.utils.PropertiesUtil;
import com.sciformix.sciportal.utils.SystemUtils;
import com.sciformix.sciportal.utils.WebUtils;
import com.sciformix.sciportal.validation.ValidationHome;

public class SciPortal
{
	private static final Logger log	= LoggerFactory.getLogger(SciPortal.class);
	private static boolean m_bSciPortalInited = false;
	private static boolean m_bPropsInited = false;
	private static Properties propsPortal = null;
	
	private SciPortal()
	{
		//Nothing to do
	}
	
	public static boolean init()
	{
		if (m_bSciPortalInited)
		{
			return true;
		}
		
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		
		//Step 1: Load the SciPortal Properties
		initProps();
		
		//Step 2: Initialize Sub-modules
		boolean bInitSuccessful = false;
		
		bInitSuccessful = LoggingHome.init(oStartupLogger);
		
		if (SystemUtils.isPropertyDefined("sciportal.dbconnectpool.primary.initialsize"))
		{
			DbConnectionManager.CONNECTION_POOL_INITIAL_SIZE = SystemUtils.getPortalIntProperty("sciportal.dbconnectpool.primary.initialsize");
		}
		if (SystemUtils.isPropertyDefined("sciportal.dbconnectpool.primary.maxsize"))
		{
			DbConnectionManager.CONNECTION_POOL_MAX_SIZE = SystemUtils.getPortalIntProperty("sciportal.dbconnectpool.primary.maxsize");
		}
		
		//Step 2.1: Initialize Database connectivity
		bInitSuccessful = DbConnectionManager.init(oStartupLogger);
		if (!bInitSuccessful)
		{
			oStartupLogger.logError("SciPortal initialization aborted");
			return false;
		}
		
		//Step 2.2: Initialize App Registry
		bInitSuccessful = AppRegistry.init(oStartupLogger);
		if (!bInitSuccessful)
		{
			oStartupLogger.logError("SciPortal initialization aborted");
			return false;
		}
		//Step 2.3: Initialize error msg
		bInitSuccessful = ErrorCodeMessageHome.init(oStartupLogger);
		if (!bInitSuccessful)
		{
			oStartupLogger.logError("SciPortal initialization aborted");
			return false;
		}
		// //Step 2.4: Initialize validation configuration 
		bInitSuccessful = ValidationHome.init(oStartupLogger);
		if (!bInitSuccessful)
		{
			oStartupLogger.logError("SciPortal initialization aborted");
			return false;
		}
		// //Step 2.5: Initialize XML file Configuration 
		bInitSuccessful = XmlParserHome.init(oStartupLogger);
		if (!bInitSuccessful)
		{
			oStartupLogger.logError("SciPortal initialization aborted");
			return false;
		}
		//Step 2.7: Load User Details from DB
		bInitSuccessful = UserHome.init(oStartupLogger);
		if (!bInitSuccessful)
		{
			oStartupLogger.logError("SciPortal initialization aborted");
			return false;
		}
		
		//Cleanup the temp folder
		WebUtils.cleanTempFolder();
				
		if (SciStartupLogger.ENABLE_TRACE_LOGGING)
		{
			AppRegistry.dumpContentForDevTrace(oStartupLogger);
		}
		
		//Used taskScheudlar for scheduling task at particular time.
		
		String hostName=ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, "SYSTEM.thread.server.hostname");
		
		try{
			if(InetAddress.getLocalHost().getHostName().equals(hostName))
			{
				JobTaskScheduler.init();
			}
		}
		catch(Throwable t)
		{
			log.error("Error initiate job execution",t);
		}
		
		m_bSciPortalInited = true;
		
		return m_bSciPortalInited;
	}

	public static boolean initProps()
	{
		if (m_bPropsInited)
		{
			return true;
		}
		
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		//Step 1: Load the SciPortal Properties
		try
		{
			propsPortal = new Properties();
			oStartupLogger.log("SciPortal Properties - Loading Primary Property File");
			PropertiesUtil.loadProperties(propsPortal, oStartupLogger);
			
			oStartupLogger.log("SciPortal Properties - Loading Extended Property File");
			PropertiesUtil.loadExtendedProperties(propsPortal, oStartupLogger);
			
			oStartupLogger.log("SciPortal Properties - All Loading completed");
			
			if (SystemUtils.getPortalBooleanProperty("sciportal.startuplogger.trace"))
			{
				SciStartupLogger.ENABLE_TRACE_LOGGING = true;
			}

			if (SciStartupLogger.ENABLE_TRACE_LOGGING)
			{
				PropertiesUtil.dumpContentForDevTrace(propsPortal, oStartupLogger);
			}
			
			m_bPropsInited = true;
		}
		catch (SciException sciExcep)
		{
			oStartupLogger.logError(sciExcep.getMessage());
			oStartupLogger.logError("SciPortal initialization aborted");
			return false;
		}
		return m_bPropsInited;
	}
	
	public static Properties getProperties()
	{
		return propsPortal;
	}
}
