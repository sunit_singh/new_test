package com.sciformix.sciportal.threads;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecuteAtClockTime {

	public static void executeAtGivenClockTime(Runnable runnable,int hour,int minutes,int seconds){
		
		//get local time
		LocalDateTime localNow = LocalDateTime.now();
        //get current zone 
		ZoneId currentZone = ZoneId.of("Asia/Calcutta");
        //get date time of zone
		ZonedDateTime zonedNow = ZonedDateTime.of(localNow, currentZone);
        ZonedDateTime zonedNext5 ;
        //set clock time
        zonedNext5 = zonedNow.withHour(hour).withMinute(minutes).withSecond(seconds);
        if(zonedNow.compareTo(zonedNext5) > 0)
            zonedNext5 = zonedNext5.plusDays(1);

        //find duration between zone date time and clock time
        Duration duration = Duration.between(zonedNow, zonedNext5);
        //get duration in seconds for delay till clock time
        long initalDelay = duration.getSeconds();

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);            
        scheduler.scheduleAtFixedRate(runnable, initalDelay, 24*60*60, TimeUnit.SECONDS);
	}
}
