package com.sciformix.sciportal.threads;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.threads.JobExecutionTracker.LastRunExecStatus;
import com.sciformix.sciportal.utils.SystemUtils;

public class JobTaskScheduler
{
	private static final Logger log	= LoggerFactory.getLogger(JobTaskScheduler.class);
	public static void init() throws SciServiceException 
	{
			String jobScheId=null;
			List<JobSchedule> jobScheduleListFromDb=null;
			jobScheId=ConfigHome.getSysConfigSetting(SystemUtils.SYS_CONFIG_KEYCATEGORY, "SYSTEM.thread.server.jobschedulerid");
			int jobSchedulerId=0;
			if(jobScheId!=null){
				jobSchedulerId=Integer.parseInt(jobScheId);
			}
			try {
				//get task scheduler details from db by job Scheduler id
				jobScheduleListFromDb=JobTaskSchedulerHomeImpl.getJobScheduleList(jobSchedulerId);
				
				for(JobSchedule jobScheduleFromDb : jobScheduleListFromDb)
				{
					if(jobScheduleFromDb!=null){
						TimeUnit timeUnit=null;
						int duration=0
						,	maxRunCount=0;
						String 	stringDuration=null
						,		stringMaxRunCount=null;
						int h=0
						,	m=0
						,	s=0;
						String 	hour=null
						,		minutes=null
						,		seconds=null;
						
						//use runnable to run task
						Runnable runnable=new Runnable(){
							@Override
							public void run(){
								String jobId=null;
								boolean success;
								jobId=jobScheduleFromDb.getM_sJobId();
								try {
									//call runjob method to run current job id task
									success=runJob(jobId);
									//use Job Executer Tracker class
									JobExecutionTracker jobExecutionTracker=new JobExecutionTracker();
									//set values to job Execute tracker
									jobExecutionTracker.setM_nJobSchedSeqId(jobScheduleFromDb.getM_nJobSchedseqId());
									jobExecutionTracker.setM_sJobId(jobScheduleFromDb.getM_sJobId());
									jobExecutionTracker.setM_nPrjSeqId(jobScheduleFromDb.getM_nPrjseqId());
									
									jobExecutionTracker.setM_sLastRunData("lastrundata");//HC
									if(success==true){
										jobExecutionTracker.setM_nLastRunExecStatus(LastRunExecStatus.ACTIVE.value());//TODO:enum
									}else{
										jobExecutionTracker.setM_nLastRunExecStatus(LastRunExecStatus.DEACTIVE.value());
									}
									//use taskSChedularImpl class and save record to db
									JobTaskSchedulerHomeImpl.saveTrackOfJobExecution(jobExecutionTracker);
								} catch (SciException sciException){
									log.error("Error loading task details",  sciException);
								
								}//inner try/catch
							}//run method
						};//runnable constructor
						
						//use ScheduledExecuterService class for schedule task at some time interval
						ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
						
						//duration between two successful execution of run method
						stringDuration=((String)(jobScheduleFromDb.getM_mSchedparams()).get("duration"));
						//total count to be execute
						stringMaxRunCount=((String)(jobScheduleFromDb.getM_mSchedparams()).get("executioncount"));
						//get hours
						hour=((jobScheduleFromDb.getM_mSchedparams()).get("h"));
						//get minutes
						minutes=((jobScheduleFromDb.getM_mSchedparams()).get("m"));
						//get seconds
						seconds=((jobScheduleFromDb.getM_mSchedparams()).get("s"));
						
						//check null
						if(stringDuration!=null){
							//parse it to integer
							duration=Integer.parseInt(stringDuration);
						}
						if(stringMaxRunCount!=null){
							//check empty
							if(!(stringMaxRunCount.equals("")))
							maxRunCount=Integer.parseInt(stringMaxRunCount);
						}
						if(hour!=null){
							//parse it to integer
							h=Integer.parseInt(hour);
						}
						if(minutes!=null){
							//parse it to integer
							m=Integer.parseInt(minutes);
						}
						if(seconds!=null){
							//parse it to integer
							s=Integer.parseInt(seconds);
						}
						
						if(jobScheduleFromDb.getM_mSchedparams().containsKey("unit")){
							
							//check if time unit is ms/s/m/h or d
							if(jobScheduleFromDb.getM_mSchedparams().get("unit").equals("ms")){
								//set time unit for milliseconds
								timeUnit=TimeUnit.MILLISECONDS;
							}
							else if(jobScheduleFromDb.getM_mSchedparams().get("unit").equals("s")){
								//set time unit for seconds
								timeUnit=TimeUnit.SECONDS;
							}
							else if(jobScheduleFromDb.getM_mSchedparams().get("unit").equals("m")){
								//set time unit for minutes
								timeUnit=TimeUnit.MINUTES;
							}
							else if(jobScheduleFromDb.getM_mSchedparams().get("unit").equals("h")){
								//set time units for hours
								timeUnit=TimeUnit.HOURS;
							}
							else if(jobScheduleFromDb.getM_mSchedparams().get("unit").equals("d")){
								//set time units for days
								timeUnit=TimeUnit.DAYS;
							}
						}
						if(maxRunCount==0 && !(jobScheduleFromDb.getM_mSchedparams().containsKey("h"))){
							//run for infinite if maxRunCount has given 0 or not given
							//in method parameter 0 indicate initial delay,duration indicate period - the period between successive executions
							//timeUnit indicate the timeUnit(second,minutes etc) of the initialDelay and period parameters
							service.scheduleAtFixedRate(runnable, 0, duration, timeUnit);	
						}
						else if(maxRunCount!=0 && !(jobScheduleFromDb.getM_mSchedparams().containsKey("h"))){
							//call method for fixed execution count if maxRunCount given valid value
							//in method parameter maxRunCount indicate Number of times thread should run
							//service->ScheduledExecutorService class instance
							runNTimes(runnable,maxRunCount,duration ,timeUnit, service);
						}else{
							//it will execute job at fixed clock time
							//method parameters-->h-hour,m-minutes,s-seconds
							ExecuteAtClockTime.executeAtGivenClockTime(runnable,h,m,s);
						}
					}
			}
			
		}catch (SciException sciException){
			log.error("Error loading task details",  sciException);
			throw new SciServiceException(ServiceErrorCodes.Job.ERROR_RUN_JOB);
		}
	}
	
	public static void runNTimes(Runnable runnable, int maxRunCount, long period, TimeUnit unit, ScheduledExecutorService service) {
	    new FixedExecutionRunnable(runnable, maxRunCount).runNTimes(service, period, unit);
	}
	
	public static boolean runJob(String jobId)throws SciException{
		System.out.println("Running job");
		Map<String,String> map=null;
		ClassLoader classLoader=null;
		String className=null;
		String methodName=null;
		Class<?> LoadedClass =null;
		Object newInstance=null;
		Method method=null;
		Class<?>[] paramString = new Class[1];
		paramString[0]=Map.class;
		JobDetails taskDetailsFromDb=null;
		  try
		    {
			  	//load map to store params got from db
			  	map=new HashMap<>();
			  	//get class name from db
			  	taskDetailsFromDb=JobTaskSchedulerHomeImpl.getJobDetails(jobId);
			  	if(taskDetailsFromDb!=null){
			  		//get parames and store into hash map
			  		map=taskDetailsFromDb.getM_mJobParams();
			  		//get class loader to load class in jvm
			  		classLoader = JobDetails.class.getClassLoader();
			  		//class name  from db
			  		className=taskDetailsFromDb.getM_sJobClass();
			  		//method name from db
			  		methodName=taskDetailsFromDb.getM_sJobMethod();
				  	//load class 
			        LoadedClass = classLoader.loadClass(className);
				  	//get the instance of the class
			        newInstance =LoadedClass.newInstance();
			        //get method of loaded class
			    	method=LoadedClass.getDeclaredMethod(methodName,paramString);
			    	//pass instance and method parameters to invoke method
			    	method.invoke(newInstance,new Object[] {map});
			  	}
			  	
		    } catch (SecurityException e) {
		    	log.error("Error loading job details",  e);
				throw new SciException("Error loading job details", e);
			} catch (IllegalArgumentException e) {
				log.error("Error loading job details",  e);
				throw new SciException("Error loading job details", e);
			} catch (ClassNotFoundException e) {
				log.error("Error loading job details",  e);
				throw new SciException("Error loading job details", e);
			} catch (InstantiationException e) {
				log.error("Error loading job details",  e);
				throw new SciException("Error loading job details", e);
			} catch (IllegalAccessException e) {
				log.error("Error loading job details",  e);
				throw new SciException("Error loading job details", e);
			} catch (InvocationTargetException e) {
				log.error("Error loading job details",  e);
				throw new SciException("Error loading job details", e);
			} catch (NoSuchMethodException e) {
				log.error("Error loading job details",  e);
				throw new SciException("Error loading job details", e);
			}
		  return true;
	}

}