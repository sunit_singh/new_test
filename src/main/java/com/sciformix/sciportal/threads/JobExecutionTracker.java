package com.sciformix.sciportal.threads;

import java.util.Date;

public class JobExecutionTracker {

	
	public enum LastRunExecStatus
	{
		ACTIVE(1),DEACTIVE(0);
		
		private int value;
		
		LastRunExecStatus(int value)
		{
			this.value=value;
		}
		public int value()
		{
			return this.value;
		}
	}
	private int m_nJetSeqId;
	private int m_nJobSchedSeqId;
	private String m_sJobId;
	private int m_nPrjSeqId;
	private int m_nLastRunExecStatus;
	private Date m_dLastRun;
	private String m_sLastRunData;
	public int getM_nJetSeqId() {
		return m_nJetSeqId;
	}
	public void setM_nJetSeqId(int m_nJetSeqId) {
		this.m_nJetSeqId = m_nJetSeqId;
	}
	public int getM_nJobSchedSeqId() {
		return m_nJobSchedSeqId;
	}
	public void setM_nJobSchedSeqId(int m_nJobSchedSeqId) {
		this.m_nJobSchedSeqId = m_nJobSchedSeqId;
	}
	public String getM_sJobId() {
		return m_sJobId;
	}
	public void setM_sJobId(String m_sJobId) {
		this.m_sJobId = m_sJobId;
	}
	public int getM_nPrjSeqId() {
		return m_nPrjSeqId;
	}
	public void setM_nPrjSeqId(int m_nPrjSeqId) {
		this.m_nPrjSeqId = m_nPrjSeqId;
	}
	public int getM_nLastRunExecStatus() {
		return m_nLastRunExecStatus;
	}
	public void setM_nLastRunExecStatus(int m_nLastRunExecStatus) {
		this.m_nLastRunExecStatus = m_nLastRunExecStatus;
	}
	public Date getM_dLastRun() {
		return m_dLastRun;
	}
	public void setM_dLastRun(Date m_dLastRun) {
		this.m_dLastRun = m_dLastRun;
	}
	public String getM_sLastRunData() {
		return m_sLastRunData;
	}
	public void setM_sLastRunData(String m_sLastRunData) {
		this.m_sLastRunData = m_sLastRunData;
	}
	
		
}
