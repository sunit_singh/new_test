package com.sciformix.sciportal.threads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

public class JobTaskSchedulerHomeImpl {

	private static final Logger log	= LoggerFactory.getLogger(JobTaskSchedulerHomeImpl.class);
	
	public static JobDetails getJobDetails(String jobId)throws SciException
	{
		JobDetails jobDetails = null;
		DbQuerySelect selectQuery = null;
		
		selectQuery = DbQueryUtils.createSelectAllColumnsQuery(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ,JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBID.bindClause());
		selectQuery.addWhereClauseParameter(jobId);
		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			jobDetails = fetchJobDetailsFromDb(connection, selectQuery);
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading job details",  dbExcep);
			throw new SciException("Error loading job details", dbExcep);
		}
		return jobDetails;
	}
	
	private static JobDetails fetchJobDetailsFromDb(DbConnection connection, DbQuerySelect oSelectQuery)throws SciException{
	
		JobDetails jobDetails = null;
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					jobDetails = readJobDetailsObjFromDb(resultSet);
					break;
				}
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error loading job details",  dbExcep);
			throw new SciException("Error loading Job details", dbExcep);
		}
		return jobDetails;
	}
	
	public static List<JobSchedule> getJobScheduleList(int jobschedseqId)throws SciException
	{
		List<JobSchedule> jobScheduleList=null;
		DbQuerySelect selectQuery = null;
		
		selectQuery = DbQueryUtils.createSelectAllColumnsQuery(JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ,JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ.JOBSCHEDSEQID.bindClause());
		selectQuery.addWhereClauseParameter(jobschedseqId);
		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			jobScheduleList = fetchJobScheduleFromDb(connection, selectQuery);
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading job schedule",  dbExcep);
			throw new SciException("Error loading job schedule", dbExcep);
		}
		return jobScheduleList;
	}
	private static List<JobSchedule> fetchJobScheduleFromDb(DbConnection connection, DbQuerySelect oSelectQuery)throws SciException{
		
		List<JobSchedule> jobScheduleList=null;
		JobSchedule jobSchedule=null;
		jobScheduleList=new ArrayList<>();
		try
		{
			connection.startTransaction();
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					jobSchedule=readJobScheduleObjFromDb(resultSet);
					jobScheduleList.add(jobSchedule);
				}
			}
			connection.commitTransaction();
		}catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error loading Job schedule list",  dbExcep);
			throw new SciException("Error loading job schedule list", dbExcep);
		}
		return jobScheduleList;
	}
	private static JobDetails readJobDetailsObjFromDb(DbResultSet resultSet) throws SciException
	{
		String jsonFromDb1 = null;
		String jsonFromDb2 = null;
		JobDetails jobDetails=new JobDetails();
		
		try {
			jobDetails.setM_sJobId(resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBID));
			jobDetails.setM_sjobAppId(resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBAPPID));
			jobDetails.setM_sjobName(resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBNAME));
			jobDetails.setM_sjobDesc(resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBDESC));
			jobDetails.setM_sJobClass(resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBCLASS));
			jobDetails.setM_sJobMethod(resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBMETHOD));
			
			jsonFromDb1 = resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBPARAMS1);
			jsonFromDb2 = resultSet.readString(JobSchedulerDbSchema.JOB_DETAILS_MODULE_OBJ.JOBPARAMS2);
			
		} catch (SciDbException sciDbException) {
			log.error("Error reading data from db", sciDbException);
			throw new SciException("Error reading data from db",sciDbException);
		}
		if (!StringUtils.isNullOrEmpty(jsonFromDb1) && !StringUtils.isNullOrEmpty(jsonFromDb2))
		{
			jsonFromDb1 += jsonFromDb2;
		}
		try {
			Map<String,String> map = jsonToMap(jsonFromDb1);
			jobDetails.setM_mJobParams(map);
		} catch (JSONException jsonException) {
			log.error("Error converting json to map", jsonException);
			throw new SciException("Error converting json to map",jsonException);
		}
		return jobDetails;
	}
	
	private static JobSchedule readJobScheduleObjFromDb(DbResultSet resultSet) throws SciException
	{
		JobSchedule jobSchedule=new JobSchedule();
		String 	jsonFromDb1=null
		,		jsonFromDb2=null;
		
		try {
			jobSchedule.setM_nJobSchedseqId(resultSet.readInt(JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ.JOBSCHEDSEQID));
			jobSchedule.setM_nPrjseqId(resultSet.readInt(JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ.PRJSEQID));
			jobSchedule.setM_sJobId(resultSet.readString(JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ.JOBID));
		
			jsonFromDb1=resultSet.readString(JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ.SCHEDPARAMS1);
			jsonFromDb2=resultSet.readString(JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ.SCHEDPARAMS2);
			
			jobSchedule.setM_nState(resultSet.readInt(JobSchedulerDbSchema.JOB_SCHEDULER_MODULE_OBJ.STATE));
		} catch (SciDbException sciDbException) {
			log.error("Error reading data from db", sciDbException);
			throw new SciException("Error reading data from db",sciDbException);
		}
		if (!StringUtils.isNullOrEmpty(jsonFromDb1) && !StringUtils.isNullOrEmpty(jsonFromDb2))
		{
			jsonFromDb1 += jsonFromDb2;
		}
			
		try {
			Map<String,String>map1=jsonToMap(jsonFromDb1);
			jobSchedule.setM_mSchedparams(map1);
		}catch (JSONException jsonException) {
			log.error("Error converting json to map", jsonException);
			throw new SciException("Error converting json to map",jsonException);
		}
		
		return jobSchedule;
	}
	
	public static boolean saveTrackOfJobExecution(JobExecutionTracker jobExecutionTracker) throws SciException{
		DbQueryInsert oInsertQuery = null;
		int seqId=DbQueryHome.generateSequenceValueForTablePk(JobSchedulerDbSchema.JOB_EXECUTION_TRACKER_MODULE_OBJ);
		oInsertQuery = DbQueryUtils.createInsertQuery(JobSchedulerDbSchema.JOB_EXECUTION_TRACKER_MODULE_OBJ);
		boolean check=false;
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			check = saveTrackOfJobExecution(connection, oInsertQuery, jobExecutionTracker, seqId);
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading job schedule",  dbExcep);
			throw new SciException("Error loading job schedule", dbExcep);
		}
		return check;
	
	}
	
	private static boolean saveTrackOfJobExecution(DbConnection connection, DbQueryInsert oInsertQuery,JobExecutionTracker jobExecutionTracker,int seqId) throws SciException
	{
		try
		{
			connection.startTransaction();
			
			oInsertQuery.addInsertParameter(seqId);
			oInsertQuery.addInsertParameter(jobExecutionTracker.getM_nJobSchedSeqId());
			oInsertQuery.addInsertParameter(jobExecutionTracker.getM_sJobId());
			oInsertQuery.addInsertParameter(jobExecutionTracker.getM_nPrjSeqId());
			oInsertQuery.addInsertParameter(jobExecutionTracker.getM_nLastRunExecStatus());
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(jobExecutionTracker.getM_sLastRunData());
			connection.executeQuery(oInsertQuery);
			
			connection.commitTransaction();
		}
		catch(SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving track of record- {}", dbExcep);
			throw new SciException("Error saving track of record", dbExcep);
		}
		return true;
	}

	public static Map<String,String> jsonToMap(String jsonObject) throws JSONException {

        Map<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(jsonObject);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value=null;
            if(jObject.get(key)!=null){
            	value= jObject.getString(key).equals(" ")?"":jObject.getString(key);
            }
          
            map.put(key, value);

        }
        return map;
        
    }
}
