package com.sciformix.sciportal.threads;

import java.sql.Date;
import java.util.Map;

public class JobSchedule{

	private int m_nJobSchedseqId;
	private int m_nPrjseqId;
	private String m_sJobId;
	private Map<String,String> m_mSchedparams;
	private int  m_nState;
	private Date m_dDtcreated;
	private int m_nUsrcreated;
	private Date m_dUsrlastupdated;
	public int getM_nJobSchedseqId() {
		return m_nJobSchedseqId;
	}
	public void setM_nJobSchedseqId(int m_nJobSchedseqId) {
		this.m_nJobSchedseqId = m_nJobSchedseqId;
	}
	public int getM_nPrjseqId() {
		return m_nPrjseqId;
	}
	public void setM_nPrjseqId(int m_nPrjseqId) {
		this.m_nPrjseqId = m_nPrjseqId;
	}
	public String getM_sJobId() {
		return m_sJobId;
	}
	public void setM_sJobId(String m_sJobId) {
		this.m_sJobId = m_sJobId;
	}
	public Map<String, String> getM_mSchedparams() {
		return m_mSchedparams;
	}
	public void setM_mSchedparams(Map<String, String> m_mSchedparams) {
		this.m_mSchedparams = m_mSchedparams;
	}
	public int getM_nState() {
		return m_nState;
	}
	public void setM_nState(int m_nState) {
		this.m_nState = m_nState;
	}
	public Date getM_dDtcreated() {
		return m_dDtcreated;
	}
	public void setM_dDtcreated(Date m_dDtcreated) {
		this.m_dDtcreated = m_dDtcreated;
	}
	public int getM_nUsrcreated() {
		return m_nUsrcreated;
	}
	public void setM_nUsrcreated(int m_nUsrcreated) {
		this.m_nUsrcreated = m_nUsrcreated;
	}
	public Date getM_dUsrlastupdated() {
		return m_dUsrlastupdated;
	}
	public void setM_dUsrlastupdated(Date m_dUsrlastupdated) {
		this.m_dUsrlastupdated = m_dUsrlastupdated;
	}
		
}
