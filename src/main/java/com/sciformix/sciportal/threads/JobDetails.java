package com.sciformix.sciportal.threads;

import java.util.Map;

public class JobDetails {

	private String m_sJobId;
	private String m_sjobAppId;
	private String m_sjobName;
	private String m_sjobDesc;
	private Map<String,String> m_mJobParams;
	private String m_sJobClass;
	private String m_sJobMethod;
	public String getM_sJobId() {
		return m_sJobId;
	}
	public void setM_sJobId(String m_sJobId) {
		this.m_sJobId = m_sJobId;
	}
	public String getM_sjobAppId() {
		return m_sjobAppId;
	}
	public void setM_sjobAppId(String m_sjobAppId) {
		this.m_sjobAppId = m_sjobAppId;
	}
	public String getM_sjobName() {
		return m_sjobName;
	}
	public void setM_sjobName(String m_sjobName) {
		this.m_sjobName = m_sjobName;
	}
	public String getM_sjobDesc() {
		return m_sjobDesc;
	}
	public void setM_sjobDesc(String m_sjobDesc) {
		this.m_sjobDesc = m_sjobDesc;
	}
	public Map<String, String> getM_mJobParams() {
		return m_mJobParams;
	}
	public void setM_mJobParams(Map<String, String> m_mJobParams) {
		this.m_mJobParams = m_mJobParams;
	}
	public String getM_sJobClass() {
		return m_sJobClass;
	}
	public void setM_sJobClass(String m_sJobClass) {
		this.m_sJobClass = m_sJobClass;
	}
	public String getM_sJobMethod() {
		return m_sJobMethod;
	}
	public void setM_sJobMethod(String m_sJobMethod) {
		this.m_sJobMethod = m_sJobMethod;
	}
	
}