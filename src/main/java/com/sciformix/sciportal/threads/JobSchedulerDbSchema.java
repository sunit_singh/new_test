package com.sciformix.sciportal.threads;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class JobSchedulerDbSchema {

	public static JobDetailsObj JOB_DETAILS_MODULE_OBJ = new JobDetailsObj();
	public static JobScheduleModuleObj JOB_SCHEDULER_MODULE_OBJ = new JobScheduleModuleObj();
	public static JobExecutionTrackerModuleObj JOB_EXECUTION_TRACKER_MODULE_OBJ = new JobExecutionTrackerModuleObj();
	
	public static class JobDetailsObj extends DbTable 
	{
		public DbColumn JOBID = null;
		public DbColumn JOBAPPID = null;
		public DbColumn JOBNAME = null;
		public DbColumn JOBDESC = null;
		public DbColumn JOBCLASS = null;
		public DbColumn JOBMETHOD = null;
		public DbColumn JOBPARAMS1 = null;
		public DbColumn JOBPARAMS2 = null;
		
		
		public JobDetailsObj() {
			super("SYS_JOB_DETAILS");
			JOBID = addColumn("JOBID");
			JOBAPPID = addColumn("JOBAPPID");
			JOBNAME = addColumn("JOBNAME");
			JOBDESC = addColumn("JOBDESC");
			JOBCLASS = addColumn("JOBCLASS");
			JOBMETHOD = addColumn("JOBMETHOD");
			JOBPARAMS1 =addColumn("JOBPARAMS1");
			JOBPARAMS2 =addColumn("JOBPARAMS2");
		}
		
	}
	
	public static class JobScheduleModuleObj extends DbTable 
	{

		public DbColumn JOBSCHEDSEQID= null;
		public DbColumn PRJSEQID= null;
		public DbColumn JOBID = null;
		public DbColumn SCHEDPARAMS1= null;
		public DbColumn SCHEDPARAMS2= null;
		public DbColumn STATE = null;
		public DbColumn DTCREATED = null;
		public DbColumn USRCREATED = null;
		public DbColumn DTLASTUPDATED = null;
		public DbColumn USRLASTUPDATED = null;
		
		
		public JobScheduleModuleObj() {
			super("CMN_JOB_SCHEDULE");
			setPkSequenceName("CMN_JOB_SCHEDULE_JOBSCHEDSEQID");
			
			JOBSCHEDSEQID = addColumn("JOBSCHEDSEQID");
			PRJSEQID = addColumn("PRJSEQID");
			JOBID = addColumn("JOBID");
			SCHEDPARAMS1= addColumn("SCHEDPARAMS1");
			SCHEDPARAMS2= addColumn("SCHEDPARAMS2");
			STATE= addColumn("STATE");
			DTCREATED= addColumn("DTCREATED");
			USRCREATED= addColumn("USRCREATED");
			DTLASTUPDATED= addColumn("DTLASTUPDATED");
			USRLASTUPDATED= addColumn("USRLASTUPDATED");
			
			
		}
	}
	
	public static class JobExecutionTrackerModuleObj extends DbTable 
	{

		
		public DbColumn JETSEQID = null;
		public DbColumn JOBSCHEDSEQID = null;
		public DbColumn JOBID = null;
		public DbColumn PRJSEQID = null;
		public DbColumn LASTRUNEXECSTATUS= null;
		public DbColumn LASTRUN = null;
		public DbColumn LASTRUNDATA = null;
		
		
		public JobExecutionTrackerModuleObj() {
			super("CMN_JOB_EXECUTIONTRACKER");
			setPkSequenceName("JOB_EXECUTIONTRACKER_JETSEQID");
			
			JETSEQID = addColumn("JETSEQID");
			JOBSCHEDSEQID= addColumn("JOBSCHEDSEQID");
			JOBID= addColumn("JOBID");
			PRJSEQID = addColumn("PRJSEQID");
			LASTRUNEXECSTATUS= addColumn("LASTRUNEXECSTATUS");
			LASTRUN= addColumn("LASTRUN");
			LASTRUNDATA = addColumn("LASTRUNDATA");
		}
	}
}
