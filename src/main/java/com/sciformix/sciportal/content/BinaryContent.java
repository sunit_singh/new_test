package com.sciformix.sciportal.content;

import com.sciformix.commons.SciEnums;

public class BinaryContent
{
	private int m_nContentSeqId = -1;
	private String m_sContentType = null;
	private String m_sContentOwner = null;
	private String m_sContentPurpose = null;
	private SciEnums.RecordState m_eState = null;
	private String m_sContent = null;
	
	BinaryContent()
	{
		//Nothing to do
	}
	
	private BinaryContent(int p_nContentSeqId, String p_sContentType, String p_sContentOwner, String p_sContentPurpose,
			SciEnums.RecordState p_eState, String p_sContent)
	{
		  m_nContentSeqId = p_nContentSeqId;
		  m_sContentType = p_sContentType;
		  m_sContentOwner = p_sContentOwner;
		  m_sContentPurpose = p_sContentPurpose;
		  m_eState = p_eState;
		  m_sContent = p_sContent;
	}
	
	public static BinaryContent createNew(String p_sContentType, String p_sContentOwner, String p_sContentPurpose, String p_sContent)
	{
		 return new BinaryContent(-1, p_sContentType, p_sContentOwner, p_sContentPurpose, SciEnums.RecordState.ACTIVE, p_sContent);
	}

	public int getContentSeqId() {
		return m_nContentSeqId;
	}

	void setContentSeqId(int p_nContentSeqId) {
		this.m_nContentSeqId = p_nContentSeqId;
	}

	public String getContentType() {
		return m_sContentType;
	}

	void setContentType(String p_sContentType) {
		this.m_sContentType = p_sContentType;
	}

	public String getContentOwner() {
		return m_sContentOwner;
	}

	void setContentOwner(String p_sContentOwner) {
		this.m_sContentOwner = p_sContentOwner;
	}

	public String getContentPurpose() {
		return m_sContentPurpose;
	}

	void setContentPurpose(String p_sContentPurpose) {
		this.m_sContentPurpose = p_sContentPurpose;
	}

	public SciEnums.RecordState getState() {
		return m_eState;
	}

	void setState(SciEnums.RecordState p_eState) {
		this.m_eState = p_eState;
	}

	public String getContent() {
		return m_sContent;
	}

	void setContent(String p_sContent) {
		this.m_sContent = p_sContent;
	}
}
