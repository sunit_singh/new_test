package com.sciformix.sciportal.content;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class BinContentDbSchema
{
	
	public static BinContent SCP_BINCONTENT = new BinContent();
	public static BinContentFrag SCP_BINCONTENTFRAG = new BinContentFrag();
	
	public static class BinContent extends DbTable
	{
		public DbColumn CONTENTSEQID = null;
		public DbColumn CONTENTTYPE = null;
		public DbColumn CONTENTOWNER = null;
		public DbColumn CONTENTPURPOSE = null;
		public DbColumn STATE = null;
		public DbColumn DTCREATED = null;
		public DbColumn USRCREATED = null;
		public DbColumn DTLASTUPDATED = null;
		public DbColumn USRLASTUPDATED = null;
		
		public BinContent()
		{
			super("CMN_BINCONTENT");
			setPkSequenceName("CMN_CONTENTSEQID_SEQ");
			
			CONTENTSEQID = addColumn("CONTENTSEQID");
			CONTENTTYPE = addColumn("CONTENTTYPE");
			CONTENTOWNER = addColumn("CONTENTOWNER");
			CONTENTPURPOSE = addColumn("CONTENTPURPOSE");
			STATE = addColumn("STATE");
			DTCREATED = addColumn("DTCREATED");
			USRCREATED = addColumn("USRCREATED");
			DTLASTUPDATED = addColumn("DTLASTUPDATED");
			USRLASTUPDATED = addColumn("USRLASTUPDATED");
		}
	}
	
	public static class BinContentFrag extends DbTable
	{
		public DbColumn CONTENTSEQID = null;
		public DbColumn FRAGMENTNUM = null;
		public DbColumn CONTENT1 = null;
		public DbColumn CONTENT2 = null;
		public DbColumn CONTENT3 = null;
		public DbColumn CONTENT4 = null;
		
		public BinContentFrag()
		{
			super("CMN_BINCONTENT_FRAG");
			setPkSequenceName(null);
			
			CONTENTSEQID = addColumn("CONTENTSEQID");
			FRAGMENTNUM = addColumn("FRAGMENTNUM");
			CONTENT1 = addColumn("CONTENT1");
			CONTENT2 = addColumn("CONTENT2");
			CONTENT3 = addColumn("CONTENT3");
			CONTENT4 = addColumn("CONTENT4");
		}
	}
	
}
