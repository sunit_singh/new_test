package com.sciformix.sciportal.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQueryDelete;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUpdate;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.user.UserInfo;

public class BinaryContentHomeImpl
{
	private static final Logger log = LoggerFactory.getLogger(BinaryContentHomeImpl.class);
	
	private BinaryContentHomeImpl()
	{
		//Nothing to do
	}
	
	public static BinaryContent readContent(DbConnection connection, int p_sContentSeqId) throws SciException
	{
		BinaryContent oContent = null;
		
		DbQuerySelect oBinContentSelectQuery = null;
		DbQuerySelect oBinContentFragSelectQuery = null;
		
		oBinContentSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(BinContentDbSchema.SCP_BINCONTENT, BinContentDbSchema.SCP_BINCONTENT.CONTENTSEQID.bindClause());
		oBinContentSelectQuery.addWhereClauseParameter(p_sContentSeqId);
		
		oBinContentFragSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(BinContentDbSchema.SCP_BINCONTENTFRAG, BinContentDbSchema.SCP_BINCONTENTFRAG.CONTENTSEQID.bindClause());
		oBinContentFragSelectQuery.addWhereClauseParameter(p_sContentSeqId);
		
		oContent = fetchBinContentFromDb(connection, oBinContentSelectQuery, oBinContentFragSelectQuery);
		
		return oContent;
	}	
	
	private static BinaryContent fetchBinContentFromDb(DbConnection connection, DbQuerySelect oBinContentSelectQuery, DbQuerySelect oBinContentFragSelectQuery) throws SciException
	{
		BinaryContent oContent = null;
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oBinContentSelectQuery))
			{
				while (resultSet.next())
				{
					oContent = readBinContentObjectFromDb(resultSet);
					
					break;
				}
			}
			
			if (oContent != null)
			{
				StringBuffer sbContent = null;
				
				try (DbResultSet resultSetFrag = connection.executeQuery(oBinContentFragSelectQuery))
				{
					sbContent = new StringBuffer();
					while (resultSetFrag.next())
					{
						sbContent.append(readBinContentFragmentsFromDb(resultSetFrag));
					}
					
					oContent.setContent(sbContent.toString());
				}
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading Binary Content from Db", dbExcep);
			throw new SciException("Error reading Binary Content from Db", dbExcep);
		}
		return oContent;
	}
	
	private static BinaryContent readBinContentObjectFromDb(DbResultSet resultSet) throws SciDbException
	{
		BinaryContent oContent = null;
		
		oContent = new BinaryContent();
		oContent.setContentSeqId(resultSet.readInt(BinContentDbSchema.SCP_BINCONTENT.CONTENTSEQID));
		oContent.setContentType(resultSet.readString(BinContentDbSchema.SCP_BINCONTENT.CONTENTTYPE));
		oContent.setContentOwner(resultSet.readString(BinContentDbSchema.SCP_BINCONTENT.CONTENTOWNER));
		oContent.setContentPurpose(resultSet.readString(BinContentDbSchema.SCP_BINCONTENT.CONTENTPURPOSE));
		oContent.setState(SciEnums.RecordState.toEnum(resultSet.readInt(BinContentDbSchema.SCP_BINCONTENT.STATE)));
		
		return oContent;
	}
	
	private static String readBinContentFragmentsFromDb(DbResultSet resultSet) throws SciDbException
	{
		StringBuffer sbContent = null;
		String sTempContent = null;
		
		sbContent = new StringBuffer();
		
		sTempContent = resultSet.readString(BinContentDbSchema.SCP_BINCONTENTFRAG.CONTENT1);
		if (sTempContent != null && sTempContent.length() > 0)
		{
			sbContent.append(sTempContent);
		}
		
		sTempContent = resultSet.readString(BinContentDbSchema.SCP_BINCONTENTFRAG.CONTENT2);
		if (sTempContent != null && sTempContent.length() > 0)
		{
			sbContent.append(sTempContent);
		}
		
		sTempContent = resultSet.readString(BinContentDbSchema.SCP_BINCONTENTFRAG.CONTENT3);
		if (sTempContent != null && sTempContent.length() > 0)
		{
			sbContent.append(sTempContent);
		}
		
		sTempContent = resultSet.readString(BinContentDbSchema.SCP_BINCONTENTFRAG.CONTENT4);
		if (sTempContent != null && sTempContent.length() > 0)
		{
			sbContent.append(sTempContent);
		}
		return sbContent.toString();
	}
	
	public static void saveContent(DbConnection connection, UserInfo p_oUserInfo, BinaryContent oContent) throws SciException
	{
		int nContentSeqId = -1;
		boolean bAddNewRecordOperation = false;
		String[] sarrContentFragments = null;
		String sTempContent1 = null;
		String sTempContent2 = null;
		String sTempContent3 = null;
		String sTempContent4 = null;
		@SuppressWarnings("unused")
		int nReturnValue = -1;
		
		bAddNewRecordOperation = (oContent.getContentSeqId() == -1);
		
		try
		{
			connection.startTransaction();
			
			if (bAddNewRecordOperation)
			{
				DbQueryInsert oInsertQuery = null;
				
				nContentSeqId = DbQueryHome.generateSequenceValueForTablePk(BinContentDbSchema.SCP_BINCONTENT);
				
				oInsertQuery = DbQueryUtils.createInsertQuery(BinContentDbSchema.SCP_BINCONTENT);
				oInsertQuery.addInsertParameter(nContentSeqId);
				oInsertQuery.addInsertParameter(oContent.getContentType());
				oInsertQuery.addInsertParameter(oContent.getContentOwner());
				oInsertQuery.addInsertParameter(oContent.getContentPurpose());
				oInsertQuery.addInsertParameter(oContent.getState().value());
				oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
				oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
				oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
				oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
				
				nReturnValue = connection.executeQuery(oInsertQuery);
				
				oContent.setContentSeqId(nContentSeqId);
			}
			else
			{
				DbQueryUpdate oUpdateQuery = null;
				
				nContentSeqId = oContent.getContentSeqId();
				
				oUpdateQuery = DbQueryUtils.createUpdateQuery(BinContentDbSchema.SCP_BINCONTENT,
						DbQueryUtils.bindUpdateClause(BinContentDbSchema.SCP_BINCONTENT.CONTENTTYPE, BinContentDbSchema.SCP_BINCONTENT.CONTENTOWNER,
								BinContentDbSchema.SCP_BINCONTENT.CONTENTPURPOSE, BinContentDbSchema.SCP_BINCONTENT.STATE,
								BinContentDbSchema.SCP_BINCONTENT.DTLASTUPDATED, BinContentDbSchema.SCP_BINCONTENT.USRLASTUPDATED), 
						BinContentDbSchema.SCP_BINCONTENT.CONTENTSEQID.bindClause());
				oUpdateQuery.addUpdateParameter(oContent.getContentType());
				oUpdateQuery.addUpdateParameter(oContent.getContentOwner());
				oUpdateQuery.addUpdateParameter(oContent.getContentPurpose());
				oUpdateQuery.addUpdateParameter(oContent.getState().value());
				oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
				oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());
				
				oUpdateQuery.addWhereClauseParameter(nContentSeqId);
				
				nReturnValue = connection.executeQuery(oUpdateQuery);
			}
			
			//Save the content 
			if (!bAddNewRecordOperation)
			{
				DbQueryDelete oDeleteQuery = null;
				
				oDeleteQuery = DbQueryUtils.createDeleteQuery(BinContentDbSchema.SCP_BINCONTENTFRAG, BinContentDbSchema.SCP_BINCONTENTFRAG.CONTENTSEQID.bindClause());
				oDeleteQuery.addWhereClauseParameter(nContentSeqId);
				
				nReturnValue = connection.executeQuery(oDeleteQuery);
			}
			
			
			//DEV-NOTE: It is important to 'escape' first and then split it into fragments
			sarrContentFragments = DataUtils.splitForDb(oContent.getContent());//TODO:Relook in case of binding
			if (sarrContentFragments != null & sarrContentFragments.length > 0)
			{
				DbQueryInsert oBinContentInsertQuery = null;
				
				for (int nFragmentIndex = 0; nFragmentIndex < sarrContentFragments.length; nFragmentIndex += 4)
				{
					sTempContent1 = sarrContentFragments[nFragmentIndex];
					sTempContent2 = ((nFragmentIndex+1<sarrContentFragments.length) ? sarrContentFragments[nFragmentIndex+1] : null);
					sTempContent3 = ((nFragmentIndex+2<sarrContentFragments.length) ? sarrContentFragments[nFragmentIndex+2] : null);
					sTempContent4 = ((nFragmentIndex+3<sarrContentFragments.length) ? sarrContentFragments[nFragmentIndex+3] : null);
					
					oBinContentInsertQuery = DbQueryUtils.createInsertQuery(BinContentDbSchema.SCP_BINCONTENTFRAG);
					oBinContentInsertQuery.addInsertParameter(nContentSeqId);
					oBinContentInsertQuery.addInsertParameter((nFragmentIndex+1));
					oBinContentInsertQuery.addInsertParameter(sTempContent1);
					oBinContentInsertQuery.addInsertParameter(sTempContent2);
					oBinContentInsertQuery.addInsertParameter(sTempContent3);
					oBinContentInsertQuery.addInsertParameter(sTempContent4);
					
					nReturnValue = connection.executeQuery(oBinContentInsertQuery);
				}
			}
			
			connection.commitTransaction();	
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving content", dbExcep);
			throw new SciException("Error saving content", dbExcep);
		}
		catch (SciException sciExcep) 
		{
			connection.rollbackTransaction();
			log.error("Error saving content", sciExcep);
			throw sciExcep;
		}
	}
}
