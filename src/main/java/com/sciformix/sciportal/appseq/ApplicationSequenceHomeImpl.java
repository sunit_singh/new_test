package com.sciformix.sciportal.appseq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbFunctionExecute;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

public class ApplicationSequenceHomeImpl {
	
	private static final Logger 
		log	= LoggerFactory.getLogger(ApplicationSequenceHomeImpl.class)
	;
	/**
	 * 
	 * @param p_projectid
	 * @param p_appseqpurpose
	 * @param p_contextseqid
	 * @throws SciException 
	 */
	static void registerApplicationSequence(
		int				p_projectid
	,	String			p_appseqpurpose
	,	int				p_contextseqid
	,	String			p_numbercontext
	,	String			p_seqformat
	,	DbConnection	p_connection
	) throws SciException {
		DbQueryInsert	l_insertquery	=	null;
		String			l_maxnumber		=	null;
		AuditInfoDb		l_auditinfo		=	null;
		
		try {
			p_connection.startTransaction();
			l_insertquery = DbQueryUtils.createInsertQuery(ApplicationSequenceDBSchema.APPSEQ);
			l_insertquery.addInsertParameter(p_projectid);
			l_insertquery.addInsertParameter(p_appseqpurpose);
			l_insertquery.addInsertParameter(p_contextseqid);
			l_insertquery.addInsertParameter(p_numbercontext);
			l_insertquery.addInsertParameter(0);
			//TODO: Maximum number need to be calculated
			l_maxnumber	=	p_seqformat.substring(
								p_seqformat.indexOf(SciConstants.StringConstants.CURLY_OPEN)+1
							, 	p_seqformat.indexOf(SciConstants.StringConstants.CURLY_CLOSE)
							);
			l_maxnumber	=	l_maxnumber.replaceAll("N", "9");
			l_insertquery.addInsertParameter(Integer.parseInt(l_maxnumber));
			l_insertquery.addInsertParameter(p_seqformat);
			p_connection.executeQuery(l_insertquery);
			/*TODO: Need to ask Akash whether usersession will be provided to me or not 
			l_auditinfo	=	AuditHome.createRecordInsertStub(
								p_usersession
							, 	this.getPortalAppId()
							, 	"Application Sequence Created"
							,	p_projectid
							);
			l_auditinfo.addPayload(
				ApplicationSequenceAuditHelper.AuditAttributes.CONTEXT_SEQ_ID
			, 	p_contextseqid
			);	
			l_auditinfo.addPayload(
				ApplicationSequenceAuditHelper.AuditAttributes.SEQUENCE_PURPOSE
			, 	p_appseqpurpose
			);
			l_auditinfo.addPayload(
				ApplicationSequenceAuditHelper.AuditAttributes.NUMBER_CONTEXT
			, 	p_numbercontext
			);
			l_auditinfo.addDetails(ApplicationSequenceAuditHelper.AuditAttributes.SEQUENCE_PATTERN, null, p_seqformat);
			l_auditinfo.addDetails(ApplicationSequenceAuditHelper.AuditAttributes.MAX_SEQUENCE_NUMBER, null, l_maxnumber);			
			AuditHome.savePopulatedStub(connection, l_auditinfo);
			*/				
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error registering application sequence into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error registering application sequence into DB"
			,	dbExcep	
			);
		} catch (NumberFormatException nfExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error formatting maximum number. The value of maximum number is:"+l_maxnumber
			);
			throw new SciException(
				"Error registering application sequence into DB"
			,	nfExcep	
			);
		}
	}
	/**
	 * @throws SciException 
	 * 
	 */
	static String getNextApplicationSequenceNumber(
		int				p_projectid
	,	String			p_appseqpurpose
	,	int				p_contextseqid
	,	String			p_numbercontext
	,	DbConnection	p_connection
	) throws SciException {
		String				l_appseqmnemonic	=	null,
							l_nextseqnumber		=	null,
							l_padstring			=	null,
							l_paddedvalue		=	null;
		DbFunctionExecute	l_dbfunctionexecute	=	null;
		int					l_curlystartchar	=	0,
							l_curlyendchar		=	0;
		
		try {
			l_dbfunctionexecute	=	DbQueryUtils.createFunctionForExecution(ApplicationSequenceDBSchema.APPSEQFUNCTION);
			l_dbfunctionexecute.createParameter(p_projectid);
			l_dbfunctionexecute.createParameter(p_appseqpurpose);
			l_dbfunctionexecute.createParameter(p_contextseqid);
		} catch (SciDbException dbExcep) {
			log.error("Error creating object. Parameter passed are project id:" + p_projectid
					+ " sequence purpose:" +p_appseqpurpose + " context sequence id:" + p_contextseqid + " number context:" + p_numbercontext
					, dbExcep
			);
			throw new SciException("Error creating object", dbExcep);
		}
		try {
			p_connection.startTransaction();
			try {
				l_appseqmnemonic	=	getApplicationSequenceMnemonic(
											p_projectid
										,	p_appseqpurpose
										,	p_contextseqid
										,	p_numbercontext
										,	p_connection
										);
				l_curlystartchar	=	l_appseqmnemonic.indexOf(SciConstants.StringConstants.CURLY_OPEN);
				l_curlyendchar		=	l_appseqmnemonic.indexOf(SciConstants.StringConstants.CURLY_CLOSE);
				l_padstring			=	l_appseqmnemonic.substring(
											l_curlystartchar+1
										, 	l_curlyendchar
										).replace("N", "0");
				DbQueryParam oOutputValue = p_connection.executeFunction(l_dbfunctionexecute);
				l_paddedvalue	=	(l_padstring.substring(0, l_padstring.length() - String.valueOf(oOutputValue.value()).length())) + (Integer)oOutputValue.value();
				l_nextseqnumber	=	l_appseqmnemonic.substring(0, l_curlystartchar) + l_paddedvalue + l_appseqmnemonic.substring(l_curlyendchar+1);
			} catch (SciException sciExcep) {
				log.error("Error retrieving application sequence mnemonic with parameter project id:" + p_projectid 
						+ " sequence purpose:" +p_appseqpurpose + " context sequence id:" + p_contextseqid + " number context:" + p_numbercontext , sciExcep);
				throw new SciException("Error retrieving application sequence mnemonic", sciExcep);
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error reading application sequence from db for parameter project id:" + p_projectid 
					+ " sequence purpose:" +p_appseqpurpose + " context sequence id:" + p_contextseqid + " number context:" + p_numbercontext, dbExcep);
			throw new SciException("Error reading application sequence from db", dbExcep);
		}
		return l_nextseqnumber;
	}
	
	/**
	 * 
	 * @return
	 * @throws SciException 
	 */
	static boolean isApplicationSequenceUnique(
		int				p_projectid
	,	String			p_appseqpurpose
	,	int				p_contextseqid
	,	String			p_numbercontext
	,	DbConnection	p_connection
	) throws SciException {
		DbQuerySelect	l_selectquery		=	null;
		boolean			l_isappsequnique	=	true;
		
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								ApplicationSequenceDBSchema.APPSEQ
							,	DbQueryUtils.bindWhereClauseWithAnd(
									ApplicationSequenceDBSchema.APPSEQ.PRJSEQID.bindClause()
								,	ApplicationSequenceDBSchema.APPSEQ.SEQPURPOSE.bindClause()
								,	ApplicationSequenceDBSchema.APPSEQ.CONTEXTSEQID.bindClause()
								,	ApplicationSequenceDBSchema.APPSEQ.NUMBERCONTEXT.bindClause()
								)	
							);
		l_selectquery.addWhereClauseParameter(p_projectid);
		l_selectquery.addWhereClauseParameter(p_appseqpurpose);
		l_selectquery.addWhereClauseParameter(p_contextseqid);
		l_selectquery.addWhereClauseParameter(p_numbercontext);
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs.next()) {
					l_isappsequnique	=	false;
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error reading application sequence from db for parameter project id:" + p_projectid 
					+ " sequence purpose:" +p_appseqpurpose + " context sequence id:" + p_contextseqid + " number context:" + p_numbercontext, dbExcep);
			throw new SciException("Error reading Template Rulesets from Db", dbExcep);
		}
		return l_isappsequnique;
	}
	
	/**
	 * 
	 * @return
	 * @throws SciException 
	 */
	static String getApplicationSequenceMnemonic(
		int				p_projectid
	,	String			p_appseqpurpose
	,	int				p_contextseqid
	,	String			p_numbercontext
	,	DbConnection	p_connection
	) throws SciException {
		String			l_appseqmnemonic	=	null;
		DbQuerySelect	l_selectquery		=	null;
		
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								ApplicationSequenceDBSchema.APPSEQ
							,	DbQueryUtils.bindWhereClauseWithAnd(
									ApplicationSequenceDBSchema.APPSEQ.PRJSEQID.bindClause()
								,	ApplicationSequenceDBSchema.APPSEQ.SEQPURPOSE.bindClause()
								,	ApplicationSequenceDBSchema.APPSEQ.CONTEXTSEQID.bindClause()
								,	ApplicationSequenceDBSchema.APPSEQ.NUMBERCONTEXT.bindClause()
								)	
							);
		l_selectquery.addWhereClauseParameter(p_projectid);
		l_selectquery.addWhereClauseParameter(p_appseqpurpose);
		l_selectquery.addWhereClauseParameter(p_contextseqid);
		l_selectquery.addWhereClauseParameter(p_numbercontext);
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs.next()) {
					l_appseqmnemonic	=	l_rs.readString(ApplicationSequenceDBSchema.APPSEQ.SEQFORMAT);
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error retrieving application sequence mnemonic with parameter project id:" + p_projectid 
					+ " sequence purpose:" +p_appseqpurpose + " context sequence id:" + p_contextseqid + " number context:" + p_numbercontext , dbExcep);
			throw new SciException("Error retrieving application sequence mnemonic", dbExcep);
		}
		return l_appseqmnemonic;
	}
}
