package com.sciformix.sciportal.appseq;

public class ApplicationSequenceAuditHelper {
	public static class AuditAttributes
	{
		public static final String CONTEXT_SEQ_ID	=	"Context Sequence Id";
		public static final String SEQUENCE_PURPOSE	=	"Sequence Purpose";
		public static final String NUMBER_CONTEXT	=	"Number Context";
		public static final String MAX_SEQUENCE_NUMBER	=	"Maximum Sequence Number";
		public static final String SEQUENCE_PATTERN		=	"Sequence Pattern";
	}
	
	private ApplicationSequenceAuditHelper() {
		
	}
}
