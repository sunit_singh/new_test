package com.sciformix.sciportal.appseq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ApplicationSequencePurpose;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class ApplicationSequenceHelper {
	
	private static final Logger
		log	=	LoggerFactory.getLogger(ApplicationSequenceHelper.class)
	;
	private static final String	NUMBER_CONTEXT	=	"U";
	/**
	 * 
	 */
	public static void registerApplicationSequence (
		int							p_projectid
	,	ApplicationSequencePurpose	p_appseqpurpose
	,	int							p_contextseqid
	,	String						p_seqformat
	) throws SciServiceException{
		boolean	l_isappsequnique		=	true;
		if (p_appseqpurpose == null) {
			log.error("Sequence Purpose cannot be null");
			throw new SciServiceException(
				ServiceErrorCodes.ApplicationSequence.ERROR_REGISTERING_SEQUENCE
			);
		}
		if (p_contextseqid < 1) {
			log.error("Context sequence id cannot be less than 1. The passed value is:"+p_contextseqid);
			throw new SciServiceException(
				ServiceErrorCodes.ApplicationSequence.ERROR_REGISTERING_SEQUENCE
			);
		}
		if (StringUtils.isNullOrEmpty(p_seqformat)) {
			log.error("Sequenece Format cannot be null or empty");
			throw new SciServiceException(
				ServiceErrorCodes.ApplicationSequence.ERROR_REGISTERING_SEQUENCE
			);
		}
		//TODO: Sequence Format validation needs to be implemented
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			connection.startTransaction();
			try {
				l_isappsequnique	=	isApplicationSequenceUnique(
											p_projectid
										,	p_appseqpurpose
										,	p_contextseqid
										,	NUMBER_CONTEXT
										,	connection
										);
				if (l_isappsequnique) {
					ApplicationSequenceHomeImpl.registerApplicationSequence(
						p_projectid
					,	p_appseqpurpose.getCode()
					,	p_contextseqid
					,	NUMBER_CONTEXT
					,	p_seqformat
					,	connection
					);
					
				} else {
					connection.rollbackTransaction();
					throw new SciServiceException(
						ServiceErrorCodes.ApplicationSequence.ERROR_UNIQUE_SEQUENCE
					);
				}
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				throw new SciServiceException(
					ServiceErrorCodes.ApplicationSequence.ERROR_REGISTERING_SEQUENCE
				);
			}
			
		} catch (SciDbException dbExcep) {
			log.error(
				"Error registering application sequence with parameter project id:" + p_projectid 
					+ " sequence purpose:" +p_appseqpurpose.getCode() + " context sequence id:" + p_contextseqid
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ApplicationSequence.ERROR_REGISTERING_SEQUENCE
			, 	dbExcep
			);
		}
	}
	
	static boolean isApplicationSequenceUnique(
		int							p_projectid
	,	ApplicationSequencePurpose	p_appseqpurpose
	,	int							p_contextseqid
	,	String						p_numbercontext
	,	DbConnection				p_connection
	) throws SciException {
		return ApplicationSequenceHomeImpl.isApplicationSequenceUnique(
			p_projectid
		,	p_appseqpurpose.getCode()
		,	p_contextseqid
		,	p_numbercontext
		,	p_connection
		);
	}
	/**
	 * @throws SciServiceException 
	 * 
	 */
	public static String getNextApplicationSequenceNumber (
		int							p_projectid
	,	ApplicationSequencePurpose	p_appseqpurpose
	,	int							p_contextseqid
	) throws SciServiceException {
		String	l_appseqnumber	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_appseqnumber	=	ApplicationSequenceHomeImpl.getNextApplicationSequenceNumber(
										p_projectid
									,	p_appseqpurpose.getCode()
									,	p_contextseqid
									,	NUMBER_CONTEXT
									,	connection
									);
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error retrieving application sequence mnemonic with parameter project id:" + p_projectid 
						+ " sequence purpose:" +p_appseqpurpose.getCode() + " context sequence id:" + p_contextseqid
				);
				throw new SciServiceException(
					ServiceErrorCodes.ApplicationSequence.ERROR_RETRIEVING_SEQUENCE
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Error retrieving application sequence mnemonic with parameter project id:" + p_projectid 
					+ " sequence purpose:" +p_appseqpurpose.getCode() + " context sequence id:" + p_contextseqid
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ApplicationSequence.ERROR_RETRIEVING_SEQUENCE
			, 	dbExcep
			);
		}
		return l_appseqnumber;
	}
	
	/**
	 * @throws SciServiceException 
	 * 
	 */
	public static String getApplicationSequenceMnemonic(
		int							p_projectid
	,	ApplicationSequencePurpose	p_appseqpurpose
	,	int							p_contextseqid
	) throws SciServiceException {
		String	l_appseqmnemonic	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_appseqmnemonic	=	ApplicationSequenceHomeImpl.getApplicationSequenceMnemonic(
											p_projectid
										,	p_appseqpurpose.getCode()
										,	p_contextseqid
										,	NUMBER_CONTEXT
										,	connection
										);
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error retrieving application sequence mnemonic with parameter project id:" + p_projectid 
						+ " sequence purpose:" +p_appseqpurpose.getCode() + " context sequence id:" + p_contextseqid
				);
				throw new SciServiceException(
					ServiceErrorCodes.ApplicationSequence.ERROR_RETRIEVING_SEQUENCE
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Error retrieving application sequence mnemonic with parameter project id:" + p_projectid 
					+ " sequence purpose:" +p_appseqpurpose.getCode() + " context sequence id:" + p_contextseqid
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ApplicationSequence.ERROR_RETRIEVING_SEQUENCE
			, 	dbExcep
			);
		}
		return l_appseqmnemonic;
	}
}
