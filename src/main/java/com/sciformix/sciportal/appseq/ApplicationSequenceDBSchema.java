package com.sciformix.sciportal.appseq;

import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbFunction;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class ApplicationSequenceDBSchema {
	static ApplicationSequence APPSEQ	=	new ApplicationSequence();
	static ApplicationSequenceFunction	APPSEQFUNCTION	=	new ApplicationSequenceFunction();
	
	static class ApplicationSequence extends DbTable {
		
		public DbColumn PRJSEQID		=	null;
		public DbColumn SEQPURPOSE		=	null;
		public DbColumn	CONTEXTSEQID	=	null;
		public DbColumn NUMBERCONTEXT	=	null;
		public DbColumn	LASTNUMBER		=	null;
		public DbColumn MAXNUMBER		=	null;
		public DbColumn SEQFORMAT		=	null;

		public ApplicationSequence() {
			super("CMN_APPCONTEXTSEQUENCE");
			PRJSEQID = addColumn("PRJSEQID");
			SEQPURPOSE = addColumn("SEQPURPOSE");
			CONTEXTSEQID = addColumn("CONTEXTSEQID");
			NUMBERCONTEXT = addColumn("NUMBERCONTEXT");
			LASTNUMBER = addColumn("LASTNUMBER");
			MAXNUMBER = addColumn("MAXNUMBER");
			SEQFORMAT = addColumn("SEQFORMAT");
		}
	}
	
	static class ApplicationSequenceFunction extends DbFunction{
		public ApplicationSequenceFunction() {
			super("GETNEXTNUMBER",3,DbQueryParam.DataType.INTEGER);
		}
	}
}
