package com.sciformix.sciportal.audit;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbOperator;

public class AuditHomeImpl
{
	private static final Logger log = LoggerFactory.getLogger(AuditHomeImpl.class);
	
	private AuditHomeImpl()
	{
		//Nothing to do
	}
	
	/**
	 * This method is used for saving Audit Info
	 * @param connection
	 * @param p_oAuditInfo
	 * @return  void
	 * @throws SciException
	 */	
	public static void save(DbConnection connection, AuditInfoDb p_oAuditInfo) throws SciException
	{
		String sPayload1 = null;
		String sPayload2 = null;
		String[] sarrPayloadFragments = null;
		AuditInfoDetailsDb oAuditDetails = null;
		@SuppressWarnings("unused")
		int nReturnValue = -1;
		
		
		if (p_oAuditInfo.getAuditSeqId() > 0)
		{
			//Error - As Update operation not supported
			 log.error("Error fetching user audit trail as update operation not supported");
			 return;
		}
		
		int nAuditSeqId = -1;
		
		sarrPayloadFragments = DataUtils.splitForDb(p_oAuditInfo.getActionPayload());
		if (sarrPayloadFragments != null & sarrPayloadFragments.length > 0)
		{
			sPayload1 = sarrPayloadFragments[0];
			sPayload2 = (sarrPayloadFragments.length > 1 ? sarrPayloadFragments[1] : null);
		}
		
		nAuditSeqId = DbQueryHome.generateSequenceValueForTablePk(AuditDbSchema.AUDITTRAIL);
		
		DbQueryInsert oAuditInsertQuery = null;
		DbQueryInsert oAuditDetailsInsertQuery = null;
		
		
		List<AuditInfoDetailsDb> t_listAuditDetails = null;
		String sFieldOldValue = null;
		String sFieldNewValue = null;
		String[] sarrFieldOldValue = null;
		String[] sarrFieldNewValue = null;
		
		t_listAuditDetails = new ArrayList<AuditInfoDetailsDb>();
		for (AuditInfoDetailsDb oAuditDetailsTemp :  p_oAuditInfo.getDetails())
		{
			sFieldOldValue = (oAuditDetailsTemp.getFieldOldValue() != null ? oAuditDetailsTemp.getFieldOldValue() : StringConstants.EMPTY);
			sFieldNewValue = (oAuditDetailsTemp.getFieldNewValue() != null ? oAuditDetailsTemp.getFieldNewValue() : StringConstants.EMPTY);
			sarrFieldOldValue = DataUtils.splitForDb(sFieldOldValue);
			sarrFieldNewValue = DataUtils.splitForDb(sFieldNewValue);
			
			if (sarrFieldOldValue.length <= 1 && sarrFieldNewValue.length <= 1)
			{
				t_listAuditDetails.add(oAuditDetailsTemp);
			}
			else
			{
				AuditInfoDetailsDb oAuditDetailsNew = null;
				int nFragmentNumber = 0;
				
				while (nFragmentNumber < sarrFieldOldValue.length && nFragmentNumber < sarrFieldNewValue.length)
				{
					oAuditDetailsNew = new AuditInfoDetailsDb();
					oAuditDetailsNew.setFieldName(oAuditDetailsTemp.getFieldName() + "(" + (nFragmentNumber+1) + ")");
					oAuditDetailsNew.setFieldDatatype(oAuditDetailsTemp.getFieldDatatype());
					if (sarrFieldOldValue.length > nFragmentNumber)
					{
						oAuditDetailsNew.setFieldOldValue(sarrFieldOldValue[nFragmentNumber]);
					}
					else
					{
						oAuditDetailsNew.setFieldOldValue(StringConstants.EMPTY);
					}
					if (sarrFieldNewValue.length > nFragmentNumber)
					{
						oAuditDetailsNew.setFieldNewValue(sarrFieldNewValue[nFragmentNumber]);
					}
					else
					{
						oAuditDetailsNew.setFieldNewValue(StringConstants.EMPTY);
					}
					
					t_listAuditDetails.add(oAuditDetailsNew);
					nFragmentNumber++;
				}
				
			}
		}
		 
		oAuditInsertQuery = DbQueryUtils.createInsertQuery(AuditDbSchema.AUDITTRAIL);
		oAuditInsertQuery.addInsertParameter(nAuditSeqId);
		oAuditInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionServer());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionApp());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionType());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionCode());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionUserSeqId());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionUserId());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionClientIp());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getActionClientHost());
		oAuditInsertQuery.addInsertParameter(sPayload1);
		oAuditInsertQuery.addInsertParameter(sPayload2);
		oAuditInsertQuery.addInsertParameter(t_listAuditDetails.size());
		oAuditInsertQuery.addInsertParameter(p_oAuditInfo.getProjectId());
		try
		{
			connection.startTransaction();
			
			nReturnValue = connection.executeQuery(oAuditInsertQuery);
			
			p_oAuditInfo.setAuditSeqId(nAuditSeqId);
			
			for (int index = 0; index < t_listAuditDetails.size(); index++)
			{
				oAuditDetails = t_listAuditDetails.get(index);
				
				oAuditDetailsInsertQuery = DbQueryUtils.createInsertQuery(AuditDbSchema.AUDITTRAIL_DETAILS);
				
				oAuditDetailsInsertQuery.addInsertParameter(nAuditSeqId);
				oAuditDetailsInsertQuery.addInsertParameter((index + 1));
				oAuditDetailsInsertQuery.addInsertParameter(oAuditDetails.getFieldName());
				oAuditDetailsInsertQuery.addInsertParameter(oAuditDetails.getFieldDatatype());
				oAuditDetailsInsertQuery.addInsertParameter(oAuditDetails.getFieldOldValue());
				oAuditDetailsInsertQuery.addInsertParameter(oAuditDetails.getFieldNewValue());
				
				nReturnValue = connection.executeQuery(oAuditDetailsInsertQuery);
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving audit object", dbExcep);
			throw new SciException("Error saving audit object", dbExcep);
		}
	}
	
	/**
	 * This method is used for getting user Audit trail for User
	 * @param connection
	 * @param p_nUserSeqId
	 * @param p_oStartDate
	 * @param p_oEndDate
	 * @param p_bFetchAuditDetails
	 * @param p_nUserProjectId
	 * @return  List AuditInfoDb
	 * @throws SciException
	 */	
	public static List<AuditInfoDb> getUserAuditTrailForUser(DbConnection connection, int p_nUserSeqId, Calendar p_oStartDate, Calendar p_oEndDate,
			boolean p_bFetchAuditDetails, int p_nUserProjectId) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		String sWhereClause = null;
		
		if(p_oEndDate != null)
		{
			sWhereClause = DbQueryUtils.bindWhereClauseWithAnd(AuditDbSchema.AUDITTRAIL.PROJECTID.bindClause(),
									AuditDbSchema.AUDITTRAIL.ACTIONUSERSEQID.bindClause(),
									AuditDbSchema.AUDITTRAIL.ACTIONDATETIME.bindClause(DbOperator.GREATER_THAN_EQUAL_TO),
									AuditDbSchema.AUDITTRAIL.ACTIONDATETIME.bindClause(DbOperator.LESS_THAN_EQUAL_TO));
		}
		else
		{
			sWhereClause = DbQueryUtils.bindWhereClauseWithAnd(AuditDbSchema.AUDITTRAIL.PROJECTID.bindClause(),
					AuditDbSchema.AUDITTRAIL.ACTIONUSERSEQID.bindClause(),
					AuditDbSchema.AUDITTRAIL.ACTIONDATETIME.bindClause(DbOperator.GREATER_THAN_EQUAL_TO));
		}	
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(AuditDbSchema.AUDITTRAIL, sWhereClause, "AUDITSEQID DESC");
		oSelectQuery.addWhereClauseParameter(p_nUserProjectId);
		oSelectQuery.addWhereClauseParameter(p_nUserSeqId);		
		oSelectQuery.addWhereClauseParameter(p_oStartDate.getTime());
		
		if(p_oEndDate != null)
		{
			oSelectQuery.addWhereClauseParameter(p_oEndDate.getTime());
		}
		else
		{
			//Do nothing
		}
		
		return getSystemAuditTrail(connection, oSelectQuery, p_bFetchAuditDetails);
	}
	
	/**
	 * This method is used for getting System Audit Trail
	 * @param connection
	 * @param p_nBootstrapUserSeqId
	 * @param p_oStartDate
	 * @param p_oEndDate
	 * @param p_bFetchAuditDetails
	 * @return  List AuditInfoDb
	 * @throws SciException
	 */	
	public static List<AuditInfoDb> getSystemAuditTrail(DbConnection connection, int p_nBootstrapUserSeqId, Calendar p_oStartDate, Calendar p_oEndDate,
			boolean p_bFetchAuditDetails) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		String sWhereClause = null;
		
		if(p_oEndDate != null)
		{
			sWhereClause = "(ACTIONUSERSEQID <= 0 OR ACTIONUSERSEQID = ?) AND ACTIONDATETIME >= ? AND ACTIONDATETIME <= ?";
		}
		else
		{
			sWhereClause = "(ACTIONUSERSEQID <= 0 OR ACTIONUSERSEQID = ?) AND ACTIONDATETIME >= ?";
		}	
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(AuditDbSchema.AUDITTRAIL, sWhereClause, "AUDITSEQID DESC");
		oSelectQuery.addWhereClauseParameter(p_nBootstrapUserSeqId);
		
		oSelectQuery.addWhereClauseParameter(p_oStartDate.getTime());
		
		if(p_oEndDate != null)
		{
			oSelectQuery.addWhereClauseParameter(p_oEndDate.getTime());
		}
		else
		{
			//Do nothing
		}
		
		return getSystemAuditTrail(connection, oSelectQuery, p_bFetchAuditDetails);
	}
	
	private static List<AuditInfoDb> getSystemAuditTrail(DbConnection connection, DbQuerySelect oSelectQuery, boolean p_bFetchAuditDetails) throws SciException
	{
		List<AuditInfoDb> listAuditTrail = null;
		DbQuerySelect oSelectQueryDetails = null;
		
		listAuditTrail = fetchUserAuditTrail(connection, oSelectQuery);
		
		if (p_bFetchAuditDetails)
		{
			int nHighestAuditSeqId = -1;
			int nLowestAuditSeqId = -1;
			String sWhereClauseDetails = null;
			
			if (listAuditTrail.size() > 0)
			{
				nHighestAuditSeqId = listAuditTrail.get(0).getAuditSeqId();
				nLowestAuditSeqId = ((listAuditTrail.size() > 1) ? listAuditTrail.get(listAuditTrail.size()-1).getAuditSeqId() : nHighestAuditSeqId); 
				
				sWhereClauseDetails = " AUDITSEQID BETWEEN ? AND ? ";
				
				oSelectQueryDetails = DbQueryUtils.createSelectAllColumnsQuery(AuditDbSchema.AUDITTRAIL_DETAILS, sWhereClauseDetails, "AUDITSEQID DESC, AUDITDETNUM ASC");
				oSelectQueryDetails.addWhereClauseParameter(nLowestAuditSeqId);
				oSelectQueryDetails.addWhereClauseParameter(nHighestAuditSeqId);
				
				fetchAndFillUserAuditDetails(connection, listAuditTrail, oSelectQueryDetails);
			}
		}
		
		return listAuditTrail;
	}
	
	private static List<AuditInfoDb> fetchUserAuditTrail(DbConnection connection, DbQuerySelect p_oSelectQuery) throws SciException
	{
		 List<AuditInfoDb> listAuditTrail = null;
		 
		 AuditInfoDb oAuditInfo = null;
		 
		 listAuditTrail = new ArrayList<AuditInfoDb>();
		 
		 try
		 {
			 connection.startTransaction();
		 
			 try (DbResultSet resultSet = connection.executeQuery(p_oSelectQuery))
			 {
				 while (resultSet.next())
				 {
					 oAuditInfo = readAuditInfoObject(resultSet);
					 
					 listAuditTrail.add(oAuditInfo);
				 }
			 }
			 
			 connection.commitTransaction();
		 }
		 catch (SciDbException dbExcep)
		 {
			 connection.rollbackTransaction();
			 log.error("Error fetching user audit trail", dbExcep);
			 throw new SciException("Error fetching audit object", dbExcep);
		 }
		 
		 return listAuditTrail;
	}
	
	private static AuditInfoDb readAuditInfoObject(DbResultSet resultset) throws SciDbException
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = new AuditInfoDb();
		oAuditInfo.setAuditSeqId(resultset.readInt(AuditDbSchema.AUDITTRAIL.AUDITSEQID));
		oAuditInfo.setActionDateTime(resultset.readDate(AuditDbSchema.AUDITTRAIL.ACTIONDATETIME));
		oAuditInfo.setActionServer(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONSERVER));
		oAuditInfo.setActionApp(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONAPP));
		oAuditInfo.setActionType(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONTYPE));
		oAuditInfo.setActionCode(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONCODE));
		oAuditInfo.setActionUserSeqId(resultset.readInt(AuditDbSchema.AUDITTRAIL.ACTIONUSERSEQID));
		oAuditInfo.setActionUserId(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONUSERID));
		oAuditInfo.setActionClientHost(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONCLIENTHOST));
		oAuditInfo.setActionClientIp(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONCLIENTIP));
		oAuditInfo.setActionPayload(StringUtils.concatenateOverflowFields(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONPAYLOAD1), 
						resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONPAYLOAD2)));
		oAuditInfo.setActionApp(resultset.readString(AuditDbSchema.AUDITTRAIL.ACTIONAPP));
		oAuditInfo.setProjectId(resultset.readInt(AuditDbSchema.AUDITTRAIL.PROJECTID));
		
		return oAuditInfo;
	}
	
	private static void fetchAndFillUserAuditDetails(DbConnection connection, List<AuditInfoDb> p_listAuditTrail, DbQuerySelect p_oSelectQuery) throws SciException
	{
		 AuditInfoDetailsDb oAuditInfoDetails = null;
		 int nAuditSeqId = -1;
		 int nAuditTrailListNavigatorIndex = -1;
		 int nTempIndex = -1;
		 
		 try
		 {
			 connection.startTransaction();
			 
			 nAuditTrailListNavigatorIndex = 0;
			 
			 try (DbResultSet resultSet = connection.executeQuery(p_oSelectQuery))
			 {
				 while (resultSet.next())
				 {
					 nAuditSeqId = resultSet.readInt(AuditDbSchema.AUDITTRAIL_DETAILS.AUDITSEQID);
					 oAuditInfoDetails = readAuditDetailsObject(resultSet);
				 
					 nTempIndex = findAuditObjectInList(p_listAuditTrail, nAuditSeqId, nAuditTrailListNavigatorIndex);
					 if (nTempIndex != -1)
					 {
						 nAuditTrailListNavigatorIndex = nTempIndex;
						 p_listAuditTrail.get(nAuditTrailListNavigatorIndex).addDetails(oAuditInfoDetails);
					 }
				 }
			 }
			 
			 connection.commitTransaction();
		 }
		 catch (SciDbException dbExcep)
		 {
			 connection.rollbackTransaction();
			 log.error("Error fetching and filling user audit trail", dbExcep);
			 throw new SciException("Error fetching and filling user audit trail", dbExcep);
		}
	}
	
	private static int findAuditObjectInList(List<AuditInfoDb> p_listAuditTrail, int p_nAuditSeqId, int p_nAuditTrailListNavigatorIndex)
	{
		for (int nTempCounter = p_nAuditTrailListNavigatorIndex; nTempCounter < p_listAuditTrail.size(); nTempCounter++)
		{
			if (p_listAuditTrail.get(nTempCounter).getAuditSeqId() == p_nAuditSeqId)
			{
				return nTempCounter;
			}
			else if (p_listAuditTrail.get(nTempCounter).getAuditSeqId() < p_nAuditSeqId)
			{
				//Record not found
				return -1;
			}
		}
		return -1;
	}
	
	private static AuditInfoDetailsDb readAuditDetailsObject(DbResultSet resultset) throws SciDbException
	{
		AuditInfoDetailsDb oAuditInfoDetails = null;
		
		oAuditInfoDetails = new AuditInfoDetailsDb();
		oAuditInfoDetails.setFieldName(resultset.readString(AuditDbSchema.AUDITTRAIL_DETAILS.FIELDNAME));
		oAuditInfoDetails.setFieldDatatype(resultset.readString(AuditDbSchema.AUDITTRAIL_DETAILS.FIELDDATATYPE));
		oAuditInfoDetails.setFieldOldValue(resultset.readString(AuditDbSchema.AUDITTRAIL_DETAILS.FIELDVALUEOLD));
		oAuditInfoDetails.setFieldNewValue(resultset.readString(AuditDbSchema.AUDITTRAIL_DETAILS.FIELDVALUENEW));
		
		return oAuditInfoDetails;
	}
	
	/**
	 * This method is used for saving Audit Info
	 * @param p_oAuditInfo
	 * @return  void
	 * @throws SciException
	 */	
	public static void save(AuditInfoDb p_oAuditInfo) throws SciException
	{
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				AuditHome.savePopulatedStub(connection, p_oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving audit trail", sciExcep);
				throw sciExcep;
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error saving audit trail", dbExcep);
			throw new SciException("Error saving audit trail", dbExcep);
		}
	}
	
}
