package com.sciformix.sciportal.audit;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class AuditDbSchema
{
	public static AuditTrail AUDITTRAIL = new AuditTrail();
	public static AuditTrailDetails AUDITTRAIL_DETAILS = new AuditTrailDetails();
	
	public static class AuditTrail extends DbTable
	{
		public DbColumn AUDITSEQID = null;
		public DbColumn ACTIONDATETIME = null;
		public DbColumn ACTIONSERVER = null;
		public DbColumn ACTIONAPP = null;
		public DbColumn ACTIONTYPE = null;
		public DbColumn ACTIONCODE = null;
		public DbColumn ACTIONUSERSEQID = null;
		public DbColumn ACTIONUSERID = null;
		public DbColumn ACTIONCLIENTIP = null;
		public DbColumn ACTIONCLIENTHOST = null;
		public DbColumn ACTIONPAYLOAD1 = null;
		public DbColumn ACTIONPAYLOAD2 = null;
		public DbColumn CHILDRECORDS = null;
		public DbColumn PROJECTID = null;
		
		public AuditTrail()
		{
			super("ADT_AUDITTRAIL");
			setPkSequenceName("ADT_AUDITSEQID_SEQ");
			
			AUDITSEQID = addColumn("AUDITSEQID");
			ACTIONDATETIME = addColumn("ACTIONDATETIME");
			ACTIONSERVER = addColumn("ACTIONSERVER");
			ACTIONAPP = addColumn("ACTIONAPP");
			ACTIONTYPE = addColumn("ACTIONTYPE");
			ACTIONCODE = addColumn("ACTIONCODE");
			ACTIONUSERSEQID = addColumn("ACTIONUSERSEQID");
			ACTIONUSERID = addColumn("ACTIONUSERID");
			ACTIONCLIENTIP = addColumn("ACTIONCLIENTIP");
			ACTIONCLIENTHOST = addColumn("ACTIONCLIENTHOST");
			ACTIONPAYLOAD1 = addColumn("ACTIONPAYLOAD1");
			ACTIONPAYLOAD2 = addColumn("ACTIONPAYLOAD2");
			CHILDRECORDS = addColumn("CHILDRECORDS");
			PROJECTID = addColumn("PROJECTID");
		}
	}
	
	
	public static class AuditTrailDetails extends DbTable
	{
		public DbColumn AUDITSEQID = null;
		public DbColumn AUDITDETNUM = null;
		public DbColumn FIELDNAME = null;
		public DbColumn FIELDDATATYPE = null;
		public DbColumn FIELDVALUEOLD = null;
		public DbColumn FIELDVALUENEW = null;
		
		
		public AuditTrailDetails()
		{
			super("ADT_AUDITTRAIL_DETAILS");
			setPkSequenceName(null);
			
			AUDITSEQID = addColumn("AUDITSEQID");
			AUDITDETNUM = addColumn("AUDITDETNUM");
			FIELDNAME = addColumn("FIELDNAME");
			FIELDDATATYPE = addColumn("FIELDDATATYPE");
			FIELDVALUEOLD = addColumn("FIELDVALUEOLD");
			FIELDVALUENEW = addColumn("FIELDVALUENEW");
		}
	}
}
