package com.sciformix.sciportal.audit;

import org.apache.wink.json4j.OrderedJSONObject;

public class AuditInfoPayloadDb
{
	private String m_sKey = null;
	private String m_sValue = null;
	
	public AuditInfoPayloadDb(String p_sKey, String p_sValue)
	{
		m_sKey = p_sKey;
		m_sValue = p_sValue;
	}
	
	public String getKey()
	{
		return m_sKey;
	}
	
	public String getValue()
	{
		return m_sValue;
	}
	
	OrderedJSONObject convertToJson() throws org.apache.wink.json4j.JSONException 
	{
		OrderedJSONObject oJsonRule = new OrderedJSONObject();
		
		oJsonRule.put(m_sKey, m_sValue);
		
		return oJsonRule;
	}

}
