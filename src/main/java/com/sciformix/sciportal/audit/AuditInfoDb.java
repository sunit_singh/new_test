package com.sciformix.sciportal.audit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONException;

import com.sciformix.sciportal.apps.ngv.CaseDataValidationRule.Type;


public class AuditInfoDb
{
	private int m_nAuditSeqId = -1;
	private Date m_dActionDateTime = null;
	private String m_sActionServer = null;
	private String m_sActionApp = null;
	private String m_sActionType = null;
	private String m_sActionCode = null;
	private int m_nActionUserSeqId = -1;
	private String m_sActionUserId = null;
	private String m_sActionClientIp = null;
	private String m_sActionClientHost = null;
	private String m_sActionPayload = null;
	private int m_nProjectId	=	-1;
	
	private transient List<AuditInfoPayloadDb> t_listPayload = null;
	
	private List<AuditInfoDetailsDb> m_listAuditDetails = null;
	
	public AuditInfoDb()
	{
		m_listAuditDetails = new ArrayList<AuditInfoDetailsDb>();
		t_listPayload = new ArrayList<AuditInfoPayloadDb>();
	}
	
	public int getAuditSeqId()
	{
		return m_nAuditSeqId;
	}
	
	public void setAuditSeqId(int p_nAuditSeqId)
	{
		this.m_nAuditSeqId = p_nAuditSeqId;
	}
	
	public Date getActionDateTime()
	{
		return m_dActionDateTime;
	}
	
	public void setActionDateTime(Date p_dActionDateTime)
	{
		this.m_dActionDateTime = p_dActionDateTime;
	}
	
	public String getActionServer()
	{
		return m_sActionServer;
	}
	
	public void setActionServer(String p_sActionServer)
	{
		this.m_sActionServer = p_sActionServer;
	}
	
	public String getActionApp()
	{
		return m_sActionApp;
	}
	
	public void setActionApp(String p_sActionApp)
	{
		this.m_sActionApp = p_sActionApp;
	}
	
	public String getActionType()
	{
		return m_sActionType;
	}
	
	public void setActionType(String p_sActionType)
	{
		this.m_sActionType = p_sActionType;
	}
	
	public String getActionCode()
	{
		return m_sActionCode;
	}
	
	public void setActionCode(String p_sActionCode)
	{
		this.m_sActionCode = p_sActionCode;
	}
	
	public int getActionUserSeqId()
	{
		return m_nActionUserSeqId;
	}
	
	public void setActionUserSeqId(int p_nActionUserSeqId)
	{
		this.m_nActionUserSeqId = p_nActionUserSeqId;
	}
	
	public String getActionUserId()
	{
		return m_sActionUserId;
	}
	
	public void setActionUserId(String p_sActionUserId)
	{
		this.m_sActionUserId = p_sActionUserId;
	}
	
	public String getActionClientIp()
	{
		return m_sActionClientIp;
	}
	
	public void setActionClientIp(String p_sActionClientIp)
	{
		this.m_sActionClientIp = p_sActionClientIp;
	}
	
	public String getActionClientHost()
	{
		return m_sActionClientHost;
	}
	
	public void setActionClientHost(String p_sActionClientHost)
	{
		this.m_sActionClientHost = p_sActionClientHost;
	}
	
	public String getActionPayload()
	{
		return m_sActionPayload;
	}
	
	public int getProjectId()
	{
		return m_nProjectId;
	}
	
	public void setProjectId (int p_nProjectId)
	{
		this.m_nProjectId	=	p_nProjectId;
	}
	
	public void addPayload(String p_sKey, String p_sValue)
	{
		addPayload(new AuditInfoPayloadDb(p_sKey, p_sValue));
	}
	
	public void addPayload(String p_sKey, int p_nValue)
	{
		addPayload(new AuditInfoPayloadDb(p_sKey, Integer.toString(p_nValue)));
	}
	
	public void addPayload(String p_sKey, boolean p_bValue)
	{
		addPayload(new AuditInfoPayloadDb(p_sKey, Boolean.toString(p_bValue)));
	}
	
	private void addPayload(AuditInfoPayloadDb p_oPayload)
	{
		try
		{
			t_listPayload.add(p_oPayload);
			
			JSONArray oJsonRuleArray = new JSONArray();
			
			for (AuditInfoPayloadDb oPayload : t_listPayload)
			{
				oJsonRuleArray.add(oPayload.convertToJson());
			}
			this.m_sActionPayload = oJsonRuleArray.toString();
		}
		catch (JSONException jsonExcep)
		{
			//TODO:Error handling
		}
	}
	
	void setActionPayload(String p_sActionPayload)
	{
		this.m_sActionPayload = p_sActionPayload;
	}
	
	public void addDetails(String p_sFieldName, String p_sFieldOldValue, String p_sFieldNewValue)
	{
		m_listAuditDetails.add(new AuditInfoDetailsDb(p_sFieldName, "String", p_sFieldOldValue, p_sFieldNewValue));
	}
	
	//Added by Rohini
	
	public void addDetails(String p_sFieldName, Type p_sFieldOldValue, Type p_sFieldNewValue)
	{
		
		m_listAuditDetails.add(new AuditInfoDetailsDb(p_sFieldName, "String", p_sFieldOldValue.toString(), p_sFieldNewValue.toString()));
	}
	
	//Ended by Rohini
	public void addDetails(String p_sFieldName, int p_nFieldOldValue, int p_nFieldNewValue)
	{
		m_listAuditDetails.add(new AuditInfoDetailsDb(p_sFieldName, "Integer", Integer.toString(p_nFieldOldValue), Integer.toString(p_nFieldNewValue)));
	}
	
	public void addDetails(String p_sFieldName, String p_sFieldDatatype, String p_sFieldOldValue, String p_sFieldNewValue)
	{
		m_listAuditDetails.add(new AuditInfoDetailsDb(p_sFieldName, p_sFieldDatatype, p_sFieldOldValue, p_sFieldNewValue));
	}
	
	void addDetails(AuditInfoDetailsDb p_oAuditDetails)
	{
		m_listAuditDetails.add(p_oAuditDetails);
	}
	
	public List<AuditInfoDetailsDb> getDetails()
	{
		return m_listAuditDetails;
	}
}
