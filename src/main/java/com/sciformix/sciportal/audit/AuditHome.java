package com.sciformix.sciportal.audit;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.user.UserSession;

public class AuditHome
{
	private AuditHome()
	{
		//Nothing to do
	}
	
	public static void savePopulatedStub(DbConnection connection, AuditInfoDb p_oAuditInfo) throws SciException
	{
		AuditHomeImpl.save(connection, p_oAuditInfo);
	}
	
	static AuditInfoDb createStub(UserSession p_oUserSession)
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = new AuditInfoDb();
		//DEV-NOTE: Never insert date-time allowing the DB to use its value
		//oAuditInfo.setActionDateTime(null);
		oAuditInfo.setActionServer(p_oUserSession.getServerIp());
		oAuditInfo.setActionUserSeqId(p_oUserSession.getUserInfo().getUserSeqId());
		oAuditInfo.setActionUserId(p_oUserSession.getUserInfo().getUserId());;
		oAuditInfo.setActionClientIp(p_oUserSession.getClientIp());
		oAuditInfo.setActionClientHost(p_oUserSession.getClientHost());
		
		return oAuditInfo;
	}

	public static AuditInfoDb createRecordInsertStub(UserSession p_oUserSession, String p_sAppId, String p_sActionCode)
	{	
		return createRecordInsertStub(p_oUserSession, p_sAppId, p_sActionCode, -1);
	} 
	
	public static AuditInfoDb createRecordInsertStub(UserSession p_oUserSession, String p_sAppId, String p_sActionCode, int p_nProjectId)
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = createStub(p_oUserSession);
		oAuditInfo.setActionApp(p_sAppId);
		oAuditInfo.setActionType("DATA-INSERT");
		oAuditInfo.setActionCode(p_sActionCode);
		oAuditInfo.setProjectId(p_nProjectId);
		
		return oAuditInfo;
	}
	
	public static AuditInfoDb createRecordUpdateStub(UserSession p_oUserSession, String p_sAppId, String p_sActionCode)
	{
		return createRecordUpdateStub(p_oUserSession, p_sAppId, p_sActionCode, -1);
	}
	
	public static AuditInfoDb createRecordUpdateStub(UserSession p_oUserSession, String p_sAppId, String p_sActionCode, int p_nProjectId)
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = createStub(p_oUserSession);
		oAuditInfo.setActionApp(p_sAppId);
		oAuditInfo.setActionType("DATA-UPDATE");
		oAuditInfo.setActionCode(p_sActionCode);
		oAuditInfo.setProjectId(p_nProjectId);
		
		return oAuditInfo;
	}
	
}
