package com.sciformix.sciportal.audit;

public class AuditInfoDetailsDb
{
	private String m_sFieldName = null;
	private String m_sFieldDatatype = null;
	private String m_sFieldOldValue = null;
	private String m_sFieldNewValue = null;
	
	public AuditInfoDetailsDb(String p_sFieldName, String p_sFieldDatatype, String p_sFieldOldValue, String p_sFieldNewValue)
	{
		m_sFieldName = p_sFieldName;
		m_sFieldDatatype = p_sFieldDatatype;
		m_sFieldOldValue = p_sFieldOldValue;
		m_sFieldNewValue = p_sFieldNewValue;
	}
	
	public AuditInfoDetailsDb()
	{
		//Nothing to do
	}
	
	public String getFieldName()
	{
		return m_sFieldName;
	}

	public void setFieldName(String p_sFieldName)
	{
		this.m_sFieldName = p_sFieldName;
	}
	
	public String getFieldDatatype()
	{
		return m_sFieldDatatype;
	}
	
	public void setFieldDatatype(String p_sFieldDatatype)
	{
		this.m_sFieldDatatype = p_sFieldDatatype;
	}
	
	public String getFieldOldValue()
	{
		return m_sFieldOldValue;
	}
	
	public void setFieldOldValue(String p_sFieldOldValue)
	{
		this.m_sFieldOldValue = p_sFieldOldValue;
	}
	
	public String getFieldNewValue()
	{
		return m_sFieldNewValue;
	}
	
	public void setFieldNewValue(String p_sFieldNewValue)
	{
		this.m_sFieldNewValue = p_sFieldNewValue;
	}
	
	
}
