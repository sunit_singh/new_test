package com.sciformix.sciportal.audit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.user.UserSession;

public class AuditWebManager
{
	private static final Logger log = LoggerFactory.getLogger(AuditWebManager.class);
	
	public static void auditLogin(UserSession p_oUserSession) throws SciServiceException
	{
		AuditInfoDb oAuditInfo = null;
		
		try
		{
			oAuditInfo = AuditHome.createStub(p_oUserSession);
			oAuditInfo.setActionType("LOGIN");
			oAuditInfo.setActionCode("PORTAL-LOGIN");
			oAuditInfo.addPayload("Authentication Credential", p_oUserSession.getUserInfo().getUserId());
			oAuditInfo.addPayload("Authentication Type", p_oUserSession.getUserInfo().getUserAuthType().toString());
			AuditHomeImpl.save(oAuditInfo);
		}
		catch(SciException sciExcep)
		{
			log.error("Error auditing login", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_SAVING_LOGIN_AUDIT, sciExcep);
		}
	}

	public static void auditLoginAttempt(int p_nUserSeqId, String p_sUserId, String p_sFailureReason,
			String p_sServerIp, String p_sClientIp, String p_sClientHost) throws SciServiceException
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = createNullStub();
		oAuditInfo.setActionServer(p_sServerIp);
		oAuditInfo.setActionUserSeqId(p_nUserSeqId);
		oAuditInfo.setActionUserId(p_sUserId);//TODO:Review this
		oAuditInfo.setActionClientIp(p_sClientIp);
		oAuditInfo.setActionClientHost(p_sClientHost);
		oAuditInfo.setActionType("LOGIN");
		oAuditInfo.setActionCode("PORTAL-LOGIN-ATTEMPT");
		oAuditInfo.addPayload("Authentication Credential", p_sUserId);
		oAuditInfo.addPayload("Failure Reason", p_sFailureReason);
		
		try
		{
			AuditHomeImpl.save(oAuditInfo);
		}
		catch(SciException sciExcep)
		{
			log.error("Error auditing login attempt", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_SAVING_LOGIN_ATTEMPT_AUDIT, sciExcep);
		}
	}

	public static void auditLogout(UserSession p_oUserSession) throws SciServiceException
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = AuditHome.createStub(p_oUserSession);
		oAuditInfo.setActionApp(null);
		oAuditInfo.setActionType("LOGOUT");
		oAuditInfo.setActionCode("PORTAL-LOGOUT");
		
		try
		{
			AuditHomeImpl.save(oAuditInfo);
		}
		catch(SciException sciExcep)
		{
			log.error("Error auditing logout", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_SAVING_LOGOUT_AUDIT, sciExcep);
		}
	}

	private static AuditInfoDb createNullStub()
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = new AuditInfoDb();
		//DEV-NOTE: Never insert date-time allowing the DB to use its value
		//oAuditInfo.setActionDateTime(null);
		
		return oAuditInfo;
	}

}
