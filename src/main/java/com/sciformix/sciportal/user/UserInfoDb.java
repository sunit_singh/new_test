package com.sciformix.sciportal.user;

import java.util.Date;

class UserInfoDb
{
	private int m_nUserSeqId;
	private String m_sUserId;
	private String m_sUserDisplayName;
	private String m_sUserShortName;
	private String m_sUserEmail;
	private int m_nUserType = -1;
	private int m_nUserAuthType;
	private String m_sUserAuthValue;
	private int m_nUserState;
	private Date m_dtLastLogin = null;
	private Date m_dtCurrentLogin = null;
	
	public UserInfoDb()
	{
		//Nothing to do
	}

	public int getUserSeqId() {
		return m_nUserSeqId;
	}

	public void setUserSeqId(int p_nUserSeqId) {
		this.m_nUserSeqId = p_nUserSeqId;
	}

	public String getUserId() {
		return m_sUserId;
	}

	public void setUserId(String p_sUserId) {
		this.m_sUserId = p_sUserId;
	}

	public String getUserDisplayName() {
		return m_sUserDisplayName;
	}

	public void setUserDisplayName(String p_sUserDisplayName) {
		this.m_sUserDisplayName = p_sUserDisplayName;
	}

	public String getUserShortName() {
		return m_sUserShortName;
	}

	public void setUserShortName(String p_sUserShortName) {
		this.m_sUserShortName = p_sUserShortName;
	}

	public int getUserAuthType() {
		return m_nUserAuthType;
	}

	public void setUserAuthType(int p_nUserAuthType) {
		this.m_nUserAuthType = p_nUserAuthType;
	}

	public int getUserState() {
		return m_nUserState;
	}

	public void setUserState(int p_nUserState) {
		this.m_nUserState = p_nUserState;
	}

	public String getUserAuthValue() {
		return m_sUserAuthValue;
	}

	public void setUserAuthValue(String p_sUserAuthValue1, String p_sUserAuthValue2) {
		this.m_sUserAuthValue = p_sUserAuthValue1;
		if (p_sUserAuthValue2 != null)
		{
			this.m_sUserAuthValue += p_sUserAuthValue2;
		}
	}

	public String getUserEmail() {
		return m_sUserEmail;
	}

	public void setUserEmail(String p_sUserEmail) {
		this.m_sUserEmail = p_sUserEmail;
	}
	
	public int getUserType()
	{
		return m_nUserType;
	}
	
	public void setUserType(int p_nUserType)
	{
		m_nUserType = p_nUserType;
	}
	
	public Date getLastLoginDate() {
		return m_dtLastLogin;
	}
	
	public void setLastLoginDate(Date m_dtLastLogin) {
		this.m_dtLastLogin = m_dtLastLogin;
	}
	
	public Date getCurrentLoginDate() {
		return m_dtCurrentLogin;
	}
	
	public void setCurrentLoginDate(Date m_dtCurrentLogin) {
		this.m_dtCurrentLogin = m_dtCurrentLogin;
	}
	
}
