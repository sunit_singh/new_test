package com.sciformix.sciportal.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHome;

public class UserSession
{
	private static final Logger log = LoggerFactory.getLogger(UserSession.class);
	
	public enum LoginMode
	{
		WEB;
	}
	
	private String m_sSessionId = null;
	private UserInfo m_oUserInfo = null;
	private LoginMode m_eLoginMode = null;
	private long m_lLoginTimestamp = 0L;
	private String m_sClientIp = null;
	private String m_sClientHost = null;
	private String m_sServerIp = null;
	private List<UserPreference> m_listUserPreferences = null;
	private Map<String, UserPreference> m_mapUserPreferences = null;
	private  List<ObjectIdPair<Integer,String>> m_listAuthorizedProjectIds = null;
	private  List<String> m_listAuthorizedProjectNames = null;
	
	public static UserSession createWebUserSession(UserInfo p_oUserInfo, String p_sClientIp, String p_sClientHost, String p_sServerIp) throws SciServiceException
	{
		UserSession oSession = null;
		
		oSession = new UserSession(p_oUserInfo.getUserId() + "_" + System.currentTimeMillis(), LoginMode.WEB, p_oUserInfo);
		oSession.m_lLoginTimestamp = System.currentTimeMillis();
		oSession.m_sClientIp = p_sClientIp;
		oSession.m_sClientHost = p_sClientHost;
		oSession.m_sServerIp = p_sServerIp;
		
		try
		{
			oSession.loadUserPreferences();
			
			oSession.loadAuthorizedProjects();
		}
		catch (SciException sciExcep)
		{
			log.error("Error creating web user session", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_CREATING_WEB_USERSESSION, sciExcep);
		}
		
		return oSession;
	}
	
	private UserSession(String p_sSessionId, LoginMode p_eLoginMode, UserInfo p_oUserInfo)
	{
		m_sSessionId = p_sSessionId;
		m_oUserInfo = p_oUserInfo;
		m_eLoginMode = p_eLoginMode;
	}
	
	public String getSessionId()
	{
		return m_sSessionId;
	}
	
	public UserInfo getUserInfo()
	{
		return m_oUserInfo;
	}
	
	public LoginMode getLoginMode()
	{
		return m_eLoginMode;
	}
	
	public long getLoginTimestamp()
	{
		return m_lLoginTimestamp;
	}
	
	public String getClientIp()
	{
		return m_sClientIp;
	}
	
	public String getClientHost()
	{
		return m_sClientHost;
	}
	
	public String getServerIp()
	{
		return m_sServerIp;
	}
	
	public boolean isUserPreferenceAvailable( int projectSeqId, String p_sAppName, String p_sPreferenceKey)
	{
		return m_mapUserPreferences.containsKey(p_sAppName + ":" + p_sPreferenceKey+":"+projectSeqId);
	}
	
	public String getUserPreference(int projectSeqId,String p_sAppName, String p_sPreferenceKey)
	{
		return (m_mapUserPreferences.get(p_sAppName + ":" + p_sPreferenceKey+":"+projectSeqId) != null) ? m_mapUserPreferences.get(p_sAppName + ":" + p_sPreferenceKey+":"+projectSeqId).getValue() : null;
	}
	
	public List<ObjectIdPair<Integer,String>> getAuthorizedProjectIds()
	{
		return m_listAuthorizedProjectIds;
	}
	
	public List<String> getAuthorizedProjects()
	{
		return m_listAuthorizedProjectNames;
	}
	
	public boolean isAuthorizedForProject(int p_nProjectId)
	{
		return m_listAuthorizedProjectIds.contains(p_nProjectId);
	}
	
	private void loadUserPreferences() throws SciException
	{
		m_mapUserPreferences = new HashMap<String, UserPreference>();
		m_listUserPreferences = UserHome.retrieveUserPreferences(m_oUserInfo);
		for (UserPreference oUserPreference : m_listUserPreferences)
		{
			m_mapUserPreferences.put(oUserPreference.getAppName() + ":" + oUserPreference.getKey()+":"+oUserPreference.getProjectSeqId(), oUserPreference);
		}
	}
	
	private void loadAuthorizedProjects() throws SciServiceException
	{
		m_listAuthorizedProjectIds = ProjectUserManagementHome.getUserListOfProjects(m_oUserInfo.getUserSeqId());
		
		m_listAuthorizedProjectNames = new ArrayList<String>(m_listAuthorizedProjectIds.size());
		for (ObjectIdPair<Integer,String> oProject : m_listAuthorizedProjectIds)
		{
			m_listAuthorizedProjectNames.add(oProject.getObjectName());
		}
	}
	
	public List<UserPreference> getUserPreferences()
	{
		return m_listUserPreferences;
	}
	
	public void refreshUserPreferences() throws SciServiceException
	{
		if (m_mapUserPreferences != null)
		{
			m_mapUserPreferences.clear();
			try
			{
				loadUserPreferences();
			}
			catch (SciException sciExcep)
			{
				log.error("Error refreshing user preferences", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_REFRESHING_PREFERENCES, sciExcep);
			}
		}
	}
	public void updateClientIp (
		String	p_sClientIp
	) {
		m_sClientIp	=	p_sClientIp;
	}
	public void updateClientHost (
		String	p_sClientHost
	) {
		m_sClientHost	=	p_sClientHost;
	}
}
