package com.sciformix.sciportal.user;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class UserHome
{
	private static final Logger log = LoggerFactory.getLogger(UserHome.class);
	
	
	/**
	 * This method defines Enum UserPurpose
	 */
	public enum UserPurpose
	{
		PORTAL_LOGIN, BOOTSTRAP_MODE, DATA_SEARCH;
	}

	public static final String SYSTEM_BOOTSTRAP_USERID = "@bootstrap_user@";
	
	private UserHome()
	{
		
	}
	
	public static boolean init(SciStartupLogger oStartupLogger) {
		boolean bSuccessful = false;
		try {
			UserHomeImpl.initCache();
			bSuccessful	=	true;
		} catch (SciException sciExcep) {
			oStartupLogger.logError("Error fetching list of user ids" + sciExcep.getMessage());
			bSuccessful	=	false;
		}
		return bSuccessful;
	}
	
	
	/**
	 * This method is used for retrieving Bootstrap User 
	 * @return UserInfo 
	 * @throws SciServiceException
	 */
	public static UserInfo retrieveBootstrapUser() throws SciServiceException
	{
		return retrieveUser(SYSTEM_BOOTSTRAP_USERID, UserPurpose.DATA_SEARCH);
	}
	
	/**
	 * This method is used for retrieving User by passing user id
	 * @param p_sUserId
	 * @param p_ePurpose
	 * @return UserInfo 
	 * @throws SciServiceException
	 */
	public static UserInfo retrieveUser(String p_sUserId, UserPurpose p_ePurpose) throws SciServiceException
	{
		UserInfo oUserInfo = null;
		
		if (p_sUserId == null)
		{
			log.error("User ID not passed");
			return null;
		}
		
		try
		{
			oUserInfo = UserHomeImpl.getUserGivenUserId(p_sUserId);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving user - {}", p_sUserId, sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_USER_INFO, sciExcep);
		}
		
		return oUserInfo;
	}
	
	
	/**
	 * This method is used for retrieving App Specific User Preferences
	 * @param p_oUserInfo
	 * @param p_sAppId
	 * @param p_bIncludeGeneralPreferences
	 * @return List UserPreference 
	 * @throws SciException
	 */
	public static List<UserPreference> retrieveAppSpecificUserPreferences(UserInfo p_oUserInfo, String p_sAppId, 
			boolean p_bIncludeGeneralPreferences) throws SciException
	{
		List<UserPreference> listUserPreferences = null;
		List<UserPreference> listAppSpecificUserPreferences = null;
		
		listAppSpecificUserPreferences = new ArrayList<UserPreference>();
		
		listUserPreferences = UserHomeImpl.loadUserPreferences(p_oUserInfo);
		for (UserPreference oUserPreference : listUserPreferences)
		{
			if ((p_sAppId != null && oUserPreference.getAppName().equalsIgnoreCase(p_sAppId)) || (p_bIncludeGeneralPreferences && oUserPreference.getAppName().equalsIgnoreCase("GENERAL")))
			{
				listAppSpecificUserPreferences.add(oUserPreference);
			}
		}
		
		return listAppSpecificUserPreferences;
	}
	
	
	/**
	 * This method is used for retrieving list of User Preferences
	 * @param p_oUserInfo
	 * @return List UserPreference 
	 * @throws SciException
	 */
	public static List<UserPreference> retrieveUserPreferences(UserInfo p_oUserInfo) throws SciException
	{
		return UserHomeImpl.loadUserPreferences(p_oUserInfo);
		
	}
	
	/**
	 * This method is used save User Preferences
	 * @param connection
	 * @param p_oUserInfo
	 * @param p_oUserPreference
	 * @param p_bInsert
	 * @return boolean 
	 * @throws SciServiceException
	 */
	public static boolean saveUserPreference(DbConnection connection, UserInfo p_oUserInfo, UserPreference p_oUserPreference,
			boolean p_bInsert) throws SciServiceException
	{
		try
		{
			return UserHomeImpl.saveUserPreference(connection, p_oUserInfo, p_oUserPreference, p_bInsert);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while loading UserPreferences from db for user - {}", p_oUserInfo.getUserId(), sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_SAVING_USER_PREFS, sciExcep);
		}
	}
	
	/**
	 * This method user to remove user preferences by projectSeqId, UserSeqId, Portal app name, and preference key
	 * @param connection is DB connection object
	 * @param p_oUserInfo  remove user preferences of this user object
	 * @param projectSeqId of project for which user preferences to be remove
	 * @param appName	Portal app name
	 * @param key	User Preferences key
	 * @return true if user preferences removed successfully else false
	 * @throws SciServiceException throws if exception occure in homeImpl method while saving user preferences
	 */
	public static boolean removeUserPreference(DbConnection connection, UserInfo p_oUserInfo, 
			int projectSeqId , String appName, String key) throws SciServiceException
	{
		try
		{
			return UserHomeImpl.removeUserPreference(connection, p_oUserInfo, projectSeqId, appName, key);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while loading UserPreferences from db for user - {}", p_oUserInfo.getUserId(), sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_REMOVE_USER_PREF, sciExcep);
		}
	}
	
	
	/**
	 * This method is used get list of all Users Id.
	 * @return boolean 
	 * @throws SciServiceException
	 */
	public static List<String> getListOfUserIds() throws SciServiceException
	{
		List<String> listUserIds = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				listUserIds = UserHomeImpl.getListOfUserIds(connection);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching list of user ids", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_LIST_OF_USERS, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching list of user ids", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_LIST_OF_USERS, dbExcep);
		}
		return listUserIds;
	}
	
	/**
	 * This method is used to save user.
	 * @param connection
	 * @param p_oUserSession
	 * @param p_oUserInfo
	 * @param p_bInsert
	 * @return boolean 
	 * @throws SciServiceException
	 */
	public static boolean saveUser(DbConnection connection, UserSession p_oUserSession, UserInfo p_oUserInfo,
			boolean p_bInsert) throws SciServiceException
	{
		boolean flag = false;
		
		try
		{
			try
			{
				connection.startTransaction();
				
				if (p_bInsert)
				{
					flag = UserHomeImpl.createNewUser(connection, p_oUserInfo);
				}
				else
				{
					flag = UserHomeImpl.updateUser(connection, p_oUserInfo);
				}
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error creating user", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_LIST_OF_USERS, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error creating user", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_LIST_OF_USERS, dbExcep);
		}
		return flag;
	}
	
	/**
	 * This method is used to retrieve user by User SeqId.
	 * @param p_sUserId
	 * @return UserInfo 
	 * @throws SciServiceException
	 */
	public static UserInfo retrieveUserBySeqId(int p_sUserId) throws SciServiceException
	{
		UserInfo oUserInfo = null;
					
		try
		{
			oUserInfo = UserHomeImpl.getUserGivenUserSeqId(p_sUserId);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving user - {}", p_sUserId, sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_USER_INFO, sciExcep);
		}
		
		return oUserInfo;
	}
	
	/**
	 * This method is used to retrieve user by User SeqId.
	 * @param p_sUserId
	 * @return UserInfo 
	 * @throws SciServiceException
	 */
	public static UserInfo retrieveUserBySeqId(DbConnection connection,int p_sUserId) throws SciServiceException
	{
		UserInfo oUserInfo = null;
					
		try
		{
			oUserInfo = UserHomeImpl.getUserGivenUserSeqId(connection,p_sUserId);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving user - {}", p_sUserId, sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_USER_INFO, sciExcep);
		}
		
		return oUserInfo;
	}
	
	public static String retrieveUserDisplayNameBySeqId(DbConnection connection, int p_sUserId) throws SciServiceException
	{
		String sUserDisplayName = null;
					
		try
		{
			sUserDisplayName = UserHomeImpl.getUserDisplayNameGivenUserSeqId(connection, p_sUserId);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while retrieving user - {}", p_sUserId, sciExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_USER_INFO, sciExcep);
		}
		
		return sUserDisplayName;
	}
	
	/**
	 * This method is used to retrieve user list.
	 * @return list of UserInfo 
	 * @throws SciServiceException
	 */
	public static List<UserInfo> getListOfUsers() throws SciServiceException
	{
		List<UserInfo> listUsers = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				listUsers = UserHomeImpl.getListOfUsers(connection);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching list of user ids", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_LIST_OF_USERS, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching list of user ids", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_FETCHING_LIST_OF_USERS, dbExcep);
		}
		return listUsers;
	}
	
	public static void removeUserPrefbyProjId(int projId, int userSeqId) throws SciServiceException
	{
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				UserHomeImpl.removeUserPrefbyProjId(connection, projId,userSeqId );
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error removing user preference by project id", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_REMOVE_USER_PREF, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching list of user ids", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_REMOVE_USER_PREF, dbExcep);
		}
	}
}
