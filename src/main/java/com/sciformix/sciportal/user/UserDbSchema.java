package com.sciformix.sciportal.user;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class UserDbSchema
{
	public static Users USERS = new Users();
	public static UserPreferences USERS_PREFS = new UserPreferences();
		
	public static class Users extends DbTable
	{
		public DbColumn USERSEQID = null;
		public DbColumn USERID = null;
		public DbColumn USERDISPLAYNAME = null;
		public DbColumn USERSHORTNAME = null;
		public DbColumn USEREMAIL = null;
		public DbColumn USERTYPE = null;
		public DbColumn USERAUTHTYPE = null;
		public DbColumn USERAUTHVALUE1 = null;
		public DbColumn USERAUTHVALUE2 = null;
		public DbColumn USERSTATE = null;
		public DbColumn DTCREATED = null;
		public DbColumn DTLASTATTRIBUPDATED = null;
		public DbColumn DTLASTLOGIN = null;
		public DbColumn DTCURRENTLOGIN = null;
		
		public Users()
		{
			super("USR_USERS");
			setPkSequenceName("USR_USERSEQID_SEQ");
			
			USERSEQID = addColumn("USERSEQID");
			USERID = addColumn("USERID");
			USERDISPLAYNAME = addColumn("USERDISPLAYNAME");
			USERSHORTNAME = addColumn("USERSHORTNAME");
			USEREMAIL = addColumn("USEREMAIL");
			USERTYPE = addColumn("USERTYPE");
			USERAUTHTYPE = addColumn("USERAUTHTYPE");
			USERAUTHVALUE1 = addColumn("USERAUTHVALUE1");
			USERAUTHVALUE2 = addColumn("USERAUTHVALUE2");
			USERSTATE = addColumn("USERSTATE");
			DTCREATED = addColumn("DTCREATED");
			DTLASTATTRIBUPDATED = addColumn("DTLASTATTRIBUPDATED");
			DTLASTLOGIN = addColumn("DTLASTLOGIN");
			DTCURRENTLOGIN = addColumn("DTCURRENTLOGIN");
		}
	}
		
	public static class UserPreferences extends DbTable
	{
		public DbColumn PROJECTSEQID = null;
		public DbColumn USERSEQID = null;
		public DbColumn USERPREFAPP = null;
		public DbColumn USERPREFKEY = null;
		public DbColumn USERPREFVAL1 = null;
		public DbColumn USERPREFVAL2 = null;
		public DbColumn DTCREATED = null;
		public DbColumn DTLASTUPDATED = null;
		
		public UserPreferences()
		{
			super("USR_USERS_PREFS");
			setPkSequenceName(null);
			PROJECTSEQID = addColumn("PROJECTSEQID");
			USERSEQID = addColumn("USERSEQID");
			USERPREFAPP = addColumn("USERPREFAPP");
			USERPREFKEY = addColumn("USERPREFKEY");
			USERPREFVAL1 = addColumn("USERPREFVAL1");
			USERPREFVAL2 = addColumn("USERPREFVAL2");
			DTCREATED = addColumn("DTCREATED");
			DTLASTUPDATED = addColumn("DTLASTUPDATED");
		}
	}
}
