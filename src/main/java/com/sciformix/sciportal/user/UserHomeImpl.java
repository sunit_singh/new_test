package com.sciformix.sciportal.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQueryDelete;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUpdate;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

class UserHomeImpl
{
	private static final Logger log = LoggerFactory.getLogger(UserHomeImpl.class);
	
	private static final String _UPDATE_QUERY_USER_LOGINTIME = "UPDATE %s SET %s = %s, %s = ? WHERE %s = ?";
	
	private static Map<Integer,UserInfo> userDetails	=	new HashMap<>();
	
	private UserHomeImpl()
	{
		//Nothing to do
	}
	
	/**
	 * This method is used for retrieving User by passing user id
	 * @param p_sUserId
	 * @return UserInfo 
	 * @throws SciException
	 */
	public static UserInfo getUserGivenUserId(String p_sUserId) throws SciException
	{
		UserInfo user = null;
		UserInfoDb userDb = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(UserDbSchema.USERS, UserDbSchema.USERS.USERID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_sUserId);
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
				
					while (resultSet.next())
					{
						userDb = new UserInfoDb();
						
						userDb.setUserSeqId(resultSet.readInt(UserDbSchema.USERS.USERSEQID));
						userDb.setUserId(resultSet.readString(UserDbSchema.USERS.USERID));
						userDb.setUserDisplayName(resultSet.readString(UserDbSchema.USERS.USERDISPLAYNAME));
						userDb.setUserShortName(resultSet.readString(UserDbSchema.USERS.USERSHORTNAME));
						userDb.setUserEmail(resultSet.readString(UserDbSchema.USERS.USEREMAIL));
						userDb.setUserType(resultSet.readInt(UserDbSchema.USERS.USERTYPE));
						userDb.setUserAuthType(resultSet.readInt(UserDbSchema.USERS.USERAUTHTYPE));
						userDb.setUserAuthValue(resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE1), resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE2));
						userDb.setUserState(resultSet.readInt(UserDbSchema.USERS.USERSTATE));
						userDb.setLastLoginDate(resultSet.readDate(UserDbSchema.USERS.DTLASTLOGIN));
						userDb.setCurrentLoginDate(resultSet.readDate(UserDbSchema.USERS.DTCURRENTLOGIN));
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching user - {}", p_sUserId, dbExcep);
				throw new SciException("Error fetching user", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching user - {}", p_sUserId, dbExcep);
			new SciException("Error fetching user", dbExcep);
		}
		try
		{
			if (userDb != null) {
				user = new UserInfo(userDb);
			}
		}
		catch (IllegalArgumentException iaExcep)
		{
			log.error("Error fetching user", iaExcep);
			throw new SciException("Error fetching user", iaExcep);
		}
		return user;
	}
	
	/**
	 * This method is used for updating login time stamp by passing DbConnection and User SeqId
	 * @param connection
	 * @param p_nUserSeqId
	 * @return void 
	 * @throws SciException
	 */
	public static void updateUserLoginTimestamp(DbConnection connection, int p_nUserSeqId) throws SciException
	{
		DbQueryUpdate oUpdateQuery = null;
		String sCompleteQuery = null;
		
		sCompleteQuery = String.format(_UPDATE_QUERY_USER_LOGINTIME, DbQueryUtils.getQualifiedObjectName(UserDbSchema.USERS.name()),
				UserDbSchema.USERS.DTLASTLOGIN.name(), UserDbSchema.USERS.DTCURRENTLOGIN.name(), UserDbSchema.USERS.DTCURRENTLOGIN.name(), UserDbSchema.USERS.USERSEQID.name());
		
		oUpdateQuery = DbQueryUtils.createUpdateQuery(sCompleteQuery);
		
		oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		
		oUpdateQuery.addWhereClauseParameter(p_nUserSeqId);
		
		try
		{
			connection.startTransaction();
			
			connection.executeQuery(oUpdateQuery);
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating user login timestamp", dbExcep);
			throw new SciException("Error updating user login timestamp", dbExcep);
		}
	}
	
	/**
	 * This method is used for loading user preferences by passing userInfo
	 * @param p_oUserInfo
	 * @return List of User Preferences 
	 * @throws SciException
	 */
	public static List<UserPreference> loadUserPreferences(UserInfo p_oUserInfo) throws SciException
	{
		UserPreference userPreference = null;
		List<UserPreference> listUserPreferences = null;
		DbQuerySelect oSelectQuery = null;
		
		listUserPreferences = new ArrayList<>();
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(UserDbSchema.USERS_PREFS, UserDbSchema.USERS_PREFS.USERSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_oUserInfo.getUserSeqId());
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while (resultSet.next())
					{
						userPreference = readUserPreferenceObjFromDb(resultSet);
	
						listUserPreferences.add(userPreference);
					}				
				}
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error loading user preference - {}", p_oUserInfo.getUserId(), dbExcep);
				throw new SciException("Error loading user preference", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading user preference - {}", p_oUserInfo.getUserId(), dbExcep);
			throw new SciException("Error loading user preference", dbExcep);
		}
		
		return listUserPreferences;
	}
	
	private static UserPreference readUserPreferenceObjFromDb(DbResultSet resultSet) throws SciDbException
	{
		UserPreference userPreference = null;
		
			userPreference = new UserPreference(resultSet.readInt(UserDbSchema.USERS_PREFS.PROJECTSEQID),
									resultSet.readString(UserDbSchema.USERS_PREFS.USERPREFAPP),
									resultSet.readString(UserDbSchema.USERS_PREFS.USERPREFKEY),
									resultSet.readString(UserDbSchema.USERS_PREFS.USERPREFVAL1),
									resultSet.readString(UserDbSchema.USERS_PREFS.USERPREFVAL2));
			
			return userPreference;
	}
	
	/**
	 * This method is used for loading user preferences by passing UserInfo, AppName and PreferenceKey
	 * @param p_oUserInfo
	 * @param p_sAppName
	 * @param p_sPreferenceKey
	 * @return UserPreferences 
	 * @throws SciException
	 */
	public static UserPreference loadUserPreference(UserInfo p_oUserInfo, String p_sAppName, 
			String p_sPreferenceKey) throws SciException
	{
		UserPreference userPreference = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(UserDbSchema.USERS_PREFS, 
				DbQueryUtils.bindWhereClauseWithAnd(UserDbSchema.USERS_PREFS.USERSEQID, UserDbSchema.USERS_PREFS.USERPREFAPP, UserDbSchema.USERS_PREFS.USERPREFKEY));
		oSelectQuery.addWhereClauseParameter(p_oUserInfo.getUserSeqId());
		oSelectQuery.addWhereClauseParameter(p_sAppName);
		oSelectQuery.addWhereClauseParameter(p_sPreferenceKey);
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while (resultSet.next())
					{
						userPreference = readUserPreferenceObjFromDb(resultSet);
	
						break;
					}				
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error loading user preference - {}", p_sPreferenceKey, dbExcep);
				throw new SciException("Error loading user preference", dbExcep);
			}
			
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading user preference - {}", p_sPreferenceKey, dbExcep);
			throw new SciException("Error loading user preference", dbExcep);
		}
		
		return userPreference;
	}
	
	/**
	 * This method is used for save user preferences by passing connection , UserInfo and userPreference
	 * @param connection
	 * @param p_oUserInfo
	 * @param p_oUserPreference
	 * @return boolean 
	 * @throws SciException
	 */
	public static boolean saveUserPreference(DbConnection connection, UserInfo p_oUserInfo, 
			UserPreference p_oUserPreference, boolean p_bInsert) throws SciException
	{
		String[] sarrPreferenValues = null;
		String sPreferenceValue1 = null;
		String sPreferenceValue2 = null;
		
		
		sarrPreferenValues = DataUtils.splitForDb(p_oUserPreference.getValue());
		sPreferenceValue1 = (sarrPreferenValues.length > 0) ? sarrPreferenValues[0] : "";
		sPreferenceValue2 = (sarrPreferenValues.length > 1) ? sarrPreferenValues[1] : "";
		
		try
		{
			connection.startTransaction();
			
			if (p_bInsert)
			{
				DbQueryInsert oInsertQuery = null;
				
				oInsertQuery = DbQueryUtils.createInsertQuery(UserDbSchema.USERS_PREFS);
				oInsertQuery.addInsertParameter(p_oUserPreference.getProjectSeqId());
				oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
				oInsertQuery.addInsertParameter(p_oUserPreference.getAppName());
				oInsertQuery.addInsertParameter(p_oUserPreference.getKey());
				oInsertQuery.addInsertParameter(sPreferenceValue1);
				oInsertQuery.addInsertParameter(sPreferenceValue2);
				oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
				oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
				
				connection.executeQuery(oInsertQuery);
			}
			else
			{
				DbQueryUpdate oUpdateQuery = null;
				
				oUpdateQuery = DbQueryUtils.createUpdateQuery(UserDbSchema.USERS_PREFS, 
						DbQueryUtils.bindUpdateClause(UserDbSchema.USERS_PREFS.USERPREFVAL1, UserDbSchema.USERS_PREFS.USERPREFVAL2, UserDbSchema.USERS_PREFS.DTLASTUPDATED),
						DbQueryUtils.bindWhereClauseWithAnd(UserDbSchema.USERS_PREFS.PROJECTSEQID,UserDbSchema.USERS_PREFS.USERSEQID, UserDbSchema.USERS_PREFS.USERPREFAPP, UserDbSchema.USERS_PREFS.USERPREFKEY));
				oUpdateQuery.addUpdateParameter(sPreferenceValue1);
				oUpdateQuery.addUpdateParameter(sPreferenceValue2);
				oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
				
				oUpdateQuery.addWhereClauseParameter(p_oUserPreference.getProjectSeqId());
				oUpdateQuery.addWhereClauseParameter(p_oUserInfo.getUserSeqId());
				oUpdateQuery.addWhereClauseParameter(p_oUserPreference.getAppName());
				oUpdateQuery.addWhereClauseParameter(p_oUserPreference.getKey());
				
				connection.executeQuery(oUpdateQuery);
			}
			
			connection.commitTransaction();
		}
		catch(SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving user preference - {}", p_oUserInfo.getUserId(), dbExcep);
			throw new SciException("Error saving user preference", dbExcep);
		}
		
		return true;
	}
	
	/**
	 * This method user to remove user preferences by projectSeqId, UserSeqId, Portal app name, and preference key
	 * @param connection is DB connection object
	 * @param p_oUserInfo  remove user preferences of this user object
	 * @param projectSeqId of project for which user preferences to be remove
	 * @param appName	Portal App name
	 * @param key	User Preferences key
	 * @return true if user preferences removed successfully else false
	 * @throws SciException if query execution failed
	 */
	public static boolean removeUserPreference(DbConnection connection, UserInfo p_oUserInfo, 
			int projectSeqId , String appName, String key) throws SciException
	{
		try
		{
			connection.startTransaction();
			
			DbQueryDelete deletequery = null;
				
				deletequery = DbQueryUtils.createDeleteQuery(UserDbSchema.USERS_PREFS, 
						DbQueryUtils.bindWhereClauseWithAnd(UserDbSchema.USERS_PREFS.PROJECTSEQID.bindClause(),
															UserDbSchema.USERS_PREFS.USERSEQID.bindClause(),
															UserDbSchema.USERS_PREFS.USERPREFAPP.bindClause(),
															UserDbSchema.USERS_PREFS.USERPREFKEY.bindClause()));
				
				deletequery.addWhereClauseParameter(projectSeqId);
				deletequery.addWhereClauseParameter(p_oUserInfo.getUserSeqId());
				deletequery.addWhereClauseParameter(appName);
				deletequery.addWhereClauseParameter(key);
				connection.executeQuery(deletequery);
			connection.commitTransaction();
		}
		catch(SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error removing user preference - {}", p_oUserInfo.getUserId(), dbExcep);
			throw new SciException("Error removing user preference", dbExcep);
		}
		
		return true;
	}
	
	/**
	 * This method is used for loading user preferences by passing connection 
	 * @param connection
	 * @param p_oUserInfo
	 * @param p_oUserPreference
	 * @return boolean 
	 * @throws SciException
	 */
	public static List<String> getListOfUserIds(DbConnection connection) throws SciException
	{
		List<String> listUserIds = null;
		DbQuerySelect oSelectQuery = null;
		
		listUserIds = new ArrayList<>();
		
		oSelectQuery = DbQueryUtils.createSelectQuery(UserDbSchema.USERS, UserDbSchema.USERS.USERID.name(), null);
		
		try
		{
			connection.startTransaction();
			
			listUserIds = DbQueryHome.getStringColumnData(connection, oSelectQuery);
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error fetching list of user ids", dbExcep);
			throw new SciException("Error fetching list of user ids", dbExcep);
		}
		
		return listUserIds;
	}
	

	/**
	 * This method is used for creating user by passing userInfo 
	 * @param connection
	 * @param p_oUserInfo
	 * @return boolean 
	 * @throws SciException
	 */
	public static boolean createNewUser(DbConnection connection, UserInfo p_oUserInfo)
			throws SciException {

		String[] sarrUserAuthValues = null;
		
		sarrUserAuthValues = DataUtils.splitForDb(p_oUserInfo.getUserAuthValue());
		
		try {
			connection.startTransaction();

			DbQueryInsert oInsertQuery = null;

			oInsertQuery = DbQueryUtils.createInsertQuery(UserDbSchema.USERS);
			
			oInsertQuery.addInsertParameter(DbQueryHome.generateSequenceValueForTablePk(UserDbSchema.USERS));
			
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserId());
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserDisplayName());
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserShortName());
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserEmail());
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserType());
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserAuthType().value());
			
			oInsertQuery.addInsertParameter(sarrUserAuthValues.length > 0 ? sarrUserAuthValues[0] : null);
			oInsertQuery.addInsertParameter(sarrUserAuthValues.length > 1 ? sarrUserAuthValues[1] : null);
			
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserState().value());
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);

			connection.executeQuery(oInsertQuery);

			connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			connection.rollbackTransaction();
			log.error("Error creating user", dbExcep);
			throw new SciException("Error creating user", dbExcep);
		}

		return true;
	}
	
	/**
	 * This method is used for updating user by passing userInfo 
	 * @param connection
	 * @param p_oUserInfo
	 * @return boolean 
	 * @throws SciException
	 */
	public static boolean updateUser(DbConnection connection, UserInfo p_oUserInfo)
			throws SciException {

		String[] sarrUserAuthValues = null;
		
		sarrUserAuthValues = DataUtils.splitForDb(p_oUserInfo.getUserAuthValue());
		
		try {
			connection.startTransaction();

			DbQueryUpdate oUpdateQuery = null;

			oUpdateQuery = DbQueryUtils.createUpdateQuery(UserDbSchema.USERS,
					DbQueryUtils.bindUpdateClause(UserDbSchema.USERS.USERID,UserDbSchema.USERS.USERDISPLAYNAME,UserDbSchema.USERS.USERSHORTNAME,
							UserDbSchema.USERS.USEREMAIL, UserDbSchema.USERS.USERTYPE,UserDbSchema.USERS.USERAUTHTYPE
							,UserDbSchema.USERS.USERAUTHVALUE1,UserDbSchema.USERS.USERAUTHVALUE2,UserDbSchema.USERS.DTLASTATTRIBUPDATED),UserDbSchema.USERS.USERSEQID.bindClause());
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserId());
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserDisplayName());
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserShortName());
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserEmail());
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserType());
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserAuthType().value());
			
			oUpdateQuery.addUpdateParameter(sarrUserAuthValues.length > 0 ? sarrUserAuthValues[0] : null);
			oUpdateQuery.addUpdateParameter(sarrUserAuthValues.length > 1 ? sarrUserAuthValues[1] : null);
			
			oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);

			oUpdateQuery.addWhereClauseParameter(p_oUserInfo.getUserSeqId());

			connection.executeQuery(oUpdateQuery);

			connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			connection.rollbackTransaction();
			log.error("Error creating user", dbExcep);
			throw new SciException("Error creating user", dbExcep);
		}

		return true;
	}
	
	/**
	 * This method is used for getting user by passing userInfo 
	 * @param p_sUserSeqId
	 * @return UserInfo 
	 * @throws SciException
	 */
	public static UserInfo getUserGivenUserSeqId(int p_sUserSeqId) throws SciException
	{
		UserInfo user = null;
		UserInfoDb userDb = null;
		DbQuerySelect oSelectQuery = null;
		
		user	=	getUserFromCache(p_sUserSeqId);
		if (user == null) {
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(UserDbSchema.USERS, UserDbSchema.USERS.USERSEQID.bindClause());
			oSelectQuery.addWhereClauseParameter(p_sUserSeqId);
			
			try (DbConnection connection = DbConnectionManager.getConnection())
			{
				try
				{
					connection.startTransaction();
					
					try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
					{
					
						while (resultSet.next())
						{
							userDb = new UserInfoDb();
							
							userDb.setUserSeqId(resultSet.readInt(UserDbSchema.USERS.USERSEQID));
							userDb.setUserId(resultSet.readString(UserDbSchema.USERS.USERID));
							userDb.setUserDisplayName(resultSet.readString(UserDbSchema.USERS.USERDISPLAYNAME));
							userDb.setUserShortName(resultSet.readString(UserDbSchema.USERS.USERSHORTNAME));
							userDb.setUserEmail(resultSet.readString(UserDbSchema.USERS.USEREMAIL));
							userDb.setUserType(resultSet.readInt(UserDbSchema.USERS.USERTYPE));
							userDb.setUserAuthType(resultSet.readInt(UserDbSchema.USERS.USERAUTHTYPE));
							userDb.setUserAuthValue(resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE1), resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE2));
							userDb.setUserState(resultSet.readInt(UserDbSchema.USERS.USERSTATE));
							userDb.setLastLoginDate(resultSet.readDate(UserDbSchema.USERS.DTLASTLOGIN));
							userDb.setCurrentLoginDate(resultSet.readDate(UserDbSchema.USERS.DTCURRENTLOGIN));
						}
					}
					connection.commitTransaction();
				}
				catch (SciDbException dbExcep)
				{
					connection.rollbackTransaction();
					log.error("Error fetching user - {}", p_sUserSeqId, dbExcep);
					throw new SciException("Error fetching user", dbExcep);
				}
			}
			catch (SciDbException dbExcep)
			{
				log.error("Error fetching user - {}", p_sUserSeqId, dbExcep);
				new SciException("Error fetching user", dbExcep);
			}
			try
			{
				if (userDb != null) {
					user = new UserInfo(userDb);
					addUserToCache(user);
				}
			}
			catch (IllegalArgumentException iaExcep)
			{
				log.error("Error fetching user", iaExcep);
				throw new SciException("Error fetching user", iaExcep);
			}
		}
		return user;
	}
	
	/**
	 * This method is used for getting user by passing userInfo 
	 * @param p_sUserSeqId
	 * @return UserInfo 
	 * @throws SciException
	 */
	public static UserInfo getUserGivenUserSeqId(DbConnection connection,int p_sUserSeqId) throws SciException
	{
		UserInfo user = null;
		UserInfoDb userDb = null;
		DbQuerySelect oSelectQuery = null;
		
		user	=	getUserFromCache(p_sUserSeqId);
		if (user == null) {
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(UserDbSchema.USERS, UserDbSchema.USERS.USERSEQID.bindClause());
			oSelectQuery.addWhereClauseParameter(p_sUserSeqId);
			try
			{
				connection.startTransaction();
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					if (resultSet.next())
					{
						userDb = new UserInfoDb();
						userDb.setUserSeqId(resultSet.readInt(UserDbSchema.USERS.USERSEQID));
						userDb.setUserId(resultSet.readString(UserDbSchema.USERS.USERID));
						userDb.setUserDisplayName(resultSet.readString(UserDbSchema.USERS.USERDISPLAYNAME));
						userDb.setUserShortName(resultSet.readString(UserDbSchema.USERS.USERSHORTNAME));
						userDb.setUserEmail(resultSet.readString(UserDbSchema.USERS.USEREMAIL));
						userDb.setUserType(resultSet.readInt(UserDbSchema.USERS.USERTYPE));
						userDb.setUserAuthType(resultSet.readInt(UserDbSchema.USERS.USERAUTHTYPE));
						userDb.setUserAuthValue(resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE1), resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE2));
						userDb.setUserState(resultSet.readInt(UserDbSchema.USERS.USERSTATE));
						userDb.setLastLoginDate(resultSet.readDate(UserDbSchema.USERS.DTLASTLOGIN));
						userDb.setCurrentLoginDate(resultSet.readDate(UserDbSchema.USERS.DTCURRENTLOGIN));
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching user - {}", p_sUserSeqId, dbExcep);
				throw new SciException("Error fetching user", dbExcep);
			}
			try
			{
				if (userDb != null) {
					user = new UserInfo(userDb);
					addUserToCache(user);
				}
			}
			catch (IllegalArgumentException iaExcep)
			{
				log.error("Error fetching user", iaExcep);
				throw new SciException("Error fetching user", iaExcep);
			}
		}
		return user;
	}

	public static String getUserDisplayNameGivenUserSeqId(DbConnection connection, int p_sUserSeqId) throws SciException
	{
		String sUserDisplayName = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectQuery(UserDbSchema.USERS, UserDbSchema.USERS.USERDISPLAYNAME.name(), UserDbSchema.USERS.USERSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_sUserSeqId);
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				if (resultSet.next())
				{
					sUserDisplayName = resultSet.readString(UserDbSchema.USERS.USERDISPLAYNAME);
				}
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error fetching user - {}", p_sUserSeqId, dbExcep);
			throw new SciException("Error fetching user", dbExcep);
		}
			
		catch (IllegalArgumentException iaExcep)
		{
			log.error("Error fetching user", iaExcep);
			throw new SciException("Error fetching user", iaExcep);
		}
		return sUserDisplayName;
	}
	
	/**
	 * This method is used for getting list of user
	 * @param connection
	 * @return List of UserInfo 
	 * @throws SciException
	 */
	public static List<UserInfo> getListOfUsers(DbConnection connection) throws SciException
	{
		List<UserInfo> listUsers = null;
		DbQuerySelect oSelectQuery = null;
		UserInfo u_oUserInfo = null;
		
		listUsers = new ArrayList<UserInfo>();
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(UserDbSchema.USERS, null, UserDbSchema.USERS.USERID.name());
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					u_oUserInfo= readUserInfoObjFromDb(resultSet);
					listUsers.add(u_oUserInfo);
				}				
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error fetching list of users", dbExcep);
			throw new SciException("Error fetching list of user ids", dbExcep);
		}
		
		return listUsers;
	}
	
	private static UserInfo readUserInfoObjFromDb(DbResultSet resultSet) throws SciDbException
	{
		UserInfo u_oUserInfo = null;
		UserInfoDb u_oUserInfoDb = new UserInfoDb();
		
		u_oUserInfoDb.setUserSeqId(resultSet.readInt(UserDbSchema.USERS.USERSEQID));
		u_oUserInfoDb.setUserId(resultSet.readString(UserDbSchema.USERS.USERID));
		u_oUserInfoDb.setUserDisplayName(resultSet.readString(UserDbSchema.USERS.USERDISPLAYNAME));
		u_oUserInfoDb.setUserShortName(resultSet.readString(UserDbSchema.USERS.USERSHORTNAME));
		u_oUserInfoDb.setUserEmail(resultSet.readString(UserDbSchema.USERS.USEREMAIL));
		u_oUserInfoDb.setUserType(resultSet.readInt(UserDbSchema.USERS.USERTYPE));
		u_oUserInfoDb.setUserAuthType(resultSet.readInt(UserDbSchema.USERS.USERAUTHTYPE));
		u_oUserInfoDb.setUserAuthValue(resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE1), resultSet.readString(UserDbSchema.USERS.USERAUTHVALUE2));
		u_oUserInfoDb.setUserState(resultSet.readInt(UserDbSchema.USERS.USERSTATE));
		u_oUserInfoDb.setLastLoginDate(resultSet.readDate(UserDbSchema.USERS.DTLASTLOGIN));
		u_oUserInfoDb.setCurrentLoginDate(resultSet.readDate(UserDbSchema.USERS.DTCURRENTLOGIN));
		u_oUserInfo = new UserInfo(u_oUserInfoDb);
		
		return u_oUserInfo;
	}
	
	public static void removeUserPrefbyProjId(DbConnection connection, int n_projSeqId,int userseqId) throws SciException
	{
		DbQueryDelete oDeleteQuery = null;
		
		oDeleteQuery = DbQueryUtils.createDeleteQuery(UserDbSchema.USERS_PREFS, 
				DbQueryUtils.bindWhereClauseWithAnd(UserDbSchema.USERS_PREFS.PROJECTSEQID, UserDbSchema.USERS_PREFS.USERSEQID));
		
		oDeleteQuery.addWhereClauseParameter(n_projSeqId);
		oDeleteQuery.addWhereClauseParameter(userseqId);
		try
		{
			connection.startTransaction();
			connection.executeQuery(oDeleteQuery);
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error removing user preference by project id", dbExcep);
			throw new SciException("Error removing user preference by project id", dbExcep);
		}
	}
	
	/**
	 * This method is created to cache all the user details
	 * in the database at the time of booting up the
	 * server.
	 * @throws SciException
	 */
	static void initCache() throws SciException  {
		List<UserInfo> listUsers = null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				listUsers = UserHomeImpl.getListOfUsers(connection);
				if (listUsers != null && listUsers.size() > 0) {
					for (UserInfo l_user : listUsers) {
						userDetails.put(l_user.getUserSeqId(), l_user);
					}
				}
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching list of user ids", sciExcep);
				throw new SciException("Error fetching list of user ids", sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching list of user ids", dbExcep);
			throw new SciException("Error fetching list of user ids", dbExcep);
		}
	}
	
	/**
	 * This will return the information for the user
	 * passed in the argument.
	 * @param p_sUserSeqId The user whose detail need to be retrieved.
	 * @return UserInfo for the user sequence id passed to the method.
	 * 			If the user is not found then it will return null.
	 */
	private static UserInfo getUserFromCache (int p_sUserSeqId) {
		if (log.isTraceEnabled()) {
			log.trace(userDetails.containsKey(p_sUserSeqId) ? "User " + p_sUserSeqId +" is fetched from the cache" : "User "+ p_sUserSeqId +" is not present in the cache");
		}
		return userDetails.get(p_sUserSeqId);
	}
	
	/**
	 * This will be called to add the user into the cache.
	 * @param p_oUserInfo The user information which need to be
	 * 						added in the cache.
	 */
	private static void addUserToCache (UserInfo p_oUserInfo) {
		userDetails.put(p_oUserInfo.getUserSeqId(), p_oUserInfo);
	}
}
