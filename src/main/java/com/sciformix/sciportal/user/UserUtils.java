package com.sciformix.sciportal.user;

import java.util.List;

import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.StringUtils;

public class UserUtils
{
	public static final String PREFERENCE_KEY_APPLICATION_ENABLED = "_APPLICATION_ENABLED_";
	public static final String PREFERENCE_KEY_MAPPED_PROJECT_ID = "_SYSTEM_CURRENT_PROJECT_ID";
	public static final String PROJECT_MANAGEMENT_PORTAL_APP = "PROJMNG-PORTAL-APP";
	
	public static final String PREFERENCE_VALUE_APPLICATION_ENABLED_YES = "YES";
	public static final String PREFERENCE_VALUE_APPLICATION_ENABLED_NO = "NO";
	
	public static final int PROJECT_INDEPENDENT_ID = -1;
	
	
	private UserUtils()
	{
		//Nothing to do
	}
	
	public static boolean isApplicationEnabledForUser(UserInfo p_oUserInfo, int p_selectedProjSeqId, String p_sPortalAppId) throws SciException
	{
		String sUserPreferenceValue = null;
		
		sUserPreferenceValue = getStringPreferenceValue(p_oUserInfo, p_selectedProjSeqId, p_sPortalAppId, UserUtils.PREFERENCE_KEY_APPLICATION_ENABLED);
		
		return (sUserPreferenceValue!=null ? sUserPreferenceValue.equalsIgnoreCase(PREFERENCE_VALUE_APPLICATION_ENABLED_YES) : false);
	}
	

	public static List<UserPreference> getAppSpecificUserPreferences(UserInfo p_oUserInfo, String p_sPortalAppId) throws SciException 
	{
		List<UserPreference> listAppSpecificUserPreferences = null;
		//TODO:Review this
		listAppSpecificUserPreferences = UserHome.retrieveAppSpecificUserPreferences(p_oUserInfo, p_sPortalAppId, true);
		
		return listAppSpecificUserPreferences;
	}
	
	public static String getStringPreferenceValue(UserInfo p_oUserInfo, int p_selectedProjSeqId, String p_sPortalAppId, String p_sPreferenceKey) throws SciException
	{
		String sUserPreferenceValue = null;
		List<UserPreference> listAppSpecificUserPreferences = null;
		
		listAppSpecificUserPreferences = getAppSpecificUserPreferences(p_oUserInfo, p_sPortalAppId);
		for (UserPreference oUserPreference : listAppSpecificUserPreferences)
		{
			//Get user preference for current selected project
			if (oUserPreference.getProjectSeqId()==p_selectedProjSeqId && oUserPreference.getKey().equalsIgnoreCase(p_sPreferenceKey))
			{
				sUserPreferenceValue = oUserPreference.getValue();
				break;
			}
		}
		
		return sUserPreferenceValue;
	}
	
	public static String[] getMultivaluedStringPreferenceValue(UserInfo p_oUserInfo, String p_sPortalAppId, String p_sPreferenceKey) 
			throws SciException
	{
		String sUserPreferenceValue = null;
		String[] sarrUserPreferenceValues = null;
		List<UserPreference> listAppSpecificUserPreferences = null;
		
		listAppSpecificUserPreferences = getAppSpecificUserPreferences(p_oUserInfo, p_sPortalAppId);
		for (UserPreference oUserPreference : listAppSpecificUserPreferences)
		{
			if (oUserPreference.getKey().equalsIgnoreCase(p_sPreferenceKey))
			{
				sUserPreferenceValue = oUserPreference.getValue();
				break;
			}
		}
		
		if (sUserPreferenceValue != null)
		{
			sarrUserPreferenceValues = StringUtils.split(sUserPreferenceValue, ";");
		}
		
		return sarrUserPreferenceValues;
	}
	
	/**
	 * Fetch user selected project id from user preferences
	 * @param p_oUserInfo	Loged in user object
	 * @return 				If selected project sequence id is there in the user
	 * 						preference then return the project sequence id otherwise
	 *  					return -1
	 * @throws SciException throws by getStringPreferenceValue method
	 */
	
	public static int getCurrentlySelectedUserProject(UserInfo p_oUserInfo) throws SciException
	{
		try{
			 return Integer.parseInt(getStringPreferenceValue(p_oUserInfo, PROJECT_INDEPENDENT_ID, UserUtils.PROJECT_MANAGEMENT_PORTAL_APP, UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID));
		}
		catch(NumberFormatException numForExcep)
		{
			return -1;
		}
		
	}
}
