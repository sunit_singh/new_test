package com.sciformix.sciportal.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciWebException;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.authenticator.LDAPAuthenticator;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class UserAuthHome
{
	private static final Logger log = LoggerFactory.getLogger(UserAuthHome.class);

	public enum AuthType
	{
		DUMMY(0), LOCAL_DATABASE(1), LDAP(2);
		
		private int value;
		
		AuthType(int value)
		{
			this.value = value;
		}
		
		public int value()
		{
			return this.value;
		}
		
		public static AuthType toEnum(int value)
		{
			AuthType[] values = AuthType.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (value == values[counter].value())
				{
					return values[counter];
				}
			}
			log.error("Illegal enum value - {}", value);
			throw new IllegalArgumentException("AuthType: Illegal enum value - " + value);
		}
	}
	
	public static boolean authenticateUserForLogin(UserInfo p_oUser, String p_sUserPassword) throws SciWebException
	{
		boolean bAuthenticationSuccessful = false;
		
		if (p_oUser == null || StringUtils.isNullOrEmpty(p_sUserPassword))
		{
			log.error("User credentials not supplied");
			throw new SciWebException(WebErrorCodes.System.LOGIN_CREDENTIALS_NOT_SUPPLIED);
		}
		
		//TODO:Check for DELETED or LOCKED state
		
		try
		{
			bAuthenticationSuccessful = authenticateUserAsPerAuthType(p_oUser, p_sUserPassword);
		}
		catch (SciException sciExcep)
		{
			log.error("Error authenticating user - {}", p_oUser.getUserId(), sciExcep);
			throw new SciWebException(WebErrorCodes.System.LOGIN_CREDENTIALS_INCORRECT, null);
		}
		
		if(bAuthenticationSuccessful)
		{
			try (DbConnection connection = DbConnectionManager.getConnection())
			{
				try
				{
					connection.startTransaction();
					
					UserHomeImpl.updateUserLoginTimestamp(connection, p_oUser.getUserSeqId());
					
					connection.commitTransaction();
				}
				catch (SciDbException dbExcep)
				{
					bAuthenticationSuccessful = false;
					connection.rollbackTransaction();
					log.error("Error updating user login timestamp - {}", p_oUser.getUserId(), dbExcep);
				}
				catch (SciException sciExcep)
				{
					bAuthenticationSuccessful = false;
					connection.rollbackTransaction();
					log.error("Error updating user login timestamp - {}", p_oUser.getUserId(), sciExcep);
				}
			}
			catch (SciDbException dbExcep)
			{
				bAuthenticationSuccessful = false;
				log.error("Error updating user login timestamp - {}", p_oUser.getUserId(), dbExcep);
			}
		}
		
		return bAuthenticationSuccessful;
	}
	
	private static boolean authenticateUserAsPerAuthType(UserInfo p_oUser, String p_sUserPassword) throws SciException
	{
		if (p_oUser.getUserAuthType() == AuthType.LDAP)
		{
			//Authenticate against LDAP
			return LDAPAuthenticator.authenticate(p_oUser.getUserId(), p_sUserPassword);
		}
		else
		{
			//Invalid Authentication type
			log.error("Invalid Authentication Type. Please check the user config - {}/{}", p_oUser.getUserId(), p_oUser.getUserAuthType());
		}
		
		return false;
	}
}
