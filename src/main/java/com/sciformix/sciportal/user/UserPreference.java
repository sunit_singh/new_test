package com.sciformix.sciportal.user;

public class UserPreference
{
	private String m_sPreferenceAppName = null;
	private String m_sPreferenceKey = null;
	private String m_sPreferenceValue = null;
	private int m_nProjectSeqId = -1;
	
	public UserPreference(int p_nProjectSeqId,String p_sAppName, String p_sKey, String p_sValue )
	{
		m_nProjectSeqId = p_nProjectSeqId;
		m_sPreferenceAppName = p_sAppName;
		m_sPreferenceKey = p_sKey;
		m_sPreferenceValue = p_sValue;
	}
	
	public UserPreference(int p_nProjectSeqId,String p_sAppName, String p_sKey, String p_sValue1, String p_sValue2)
	{
		m_nProjectSeqId = p_nProjectSeqId;
		m_sPreferenceAppName = p_sAppName;
		m_sPreferenceKey = p_sKey;
		m_sPreferenceValue = p_sValue1;
		if (p_sValue2 != null)
		{
			m_sPreferenceValue += p_sValue2;
		}
	}
	public int getProjectSeqId()
	{
		return m_nProjectSeqId;
	}
	public String getAppName()
	{
		return m_sPreferenceAppName;
	}
	public String getKey()
	{
		return m_sPreferenceKey;
	}
	public String getValue()
	{
		return m_sPreferenceValue;
	}
	
}
