package com.sciformix.sciportal.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.AppRegistry;
import com.sciformix.sciportal.project.ProjectInfo;
import com.sciformix.sciportal.user.UserAuthHome.AuthType;

public class UserInfo
{
	private static final int _MASK_PATTERN_USER_TYPE_SYSTEM_ADMIN = 0x01;
	private static final int _MASK_PATTERN_USER_TYPE_PROJECT_ADMIN = 0x02;
	private static final int _MASK_PATTERN_USER_TYPE_AUDITOR = 0x04;
	private static final int _MASK_PATTERN_USER_IT_SERVICE_DESK = 0x08;
	private static final int _MASK_PATTERN_USER_PROJECT_AUDITOR = 0x10;
	
	public static enum UserAddonRole
	{
		SYSTEM_ADMIN(_MASK_PATTERN_USER_TYPE_SYSTEM_ADMIN, "System Admin"), 
		PROJECT_ADMIN(_MASK_PATTERN_USER_TYPE_PROJECT_ADMIN, "Project Admin"), 
		AUDITOR(_MASK_PATTERN_USER_TYPE_AUDITOR, "Auditor"),
		IT_SERVICE_DESK(_MASK_PATTERN_USER_IT_SERVICE_DESK, "IT Servicedesk"),
		PROJECT_AUDITOR(_MASK_PATTERN_USER_PROJECT_AUDITOR, "Project Auditor");
		
		int sMaskPattern = 0x00;
		String sDisplayName = null;
		
		private UserAddonRole(int p_sMaskPattern, String p_sDisplayName)
		{
			sMaskPattern = p_sMaskPattern;
			sDisplayName = p_sDisplayName;
		}
		
		public int getMaskPattern () {
			return sMaskPattern;
		}
		
		public String getDisplayName()
		{
			return sDisplayName;
		}
	}
	
	
	
	public enum UserState
	{
//			PENDING_ACTIVATION(0), ACTIVE(1), TEMPORARILY_DEACTIVATED(2), DEACTIVATED(3);
			ACTIVE(1);
			
			private int value;
			
			UserState(int value)
			{
				this.value = value;
			}
			
			public int value()
			{
				return this.value;
			}
			
			public static UserState toEnum(int value)
			{
				UserState[] values = UserState.values();
				for (int counter = 0; counter < values.length; counter++)
				{
					if (value == values[counter].value())
					{
						return values[counter];
					}
				}
				throw new IllegalArgumentException("UserState: Illegal enum value - " + value);
			}
	}
	
	private int m_nUserSeqId = -1;
	private String m_sUserId;
	private String m_sUserDisplayName;
	private String m_sUserShortName;
	private String m_sUserEmail;
	private int m_nUserType = -1;
	private AuthType m_nUserAuthType;
	private String m_sUserAuthValue;
	private UserState m_nUserState;
	
	private ProjectInfo oProject = null;

	public UserInfo()
	{
		//Nothing to do
	}
	
	public UserInfo(UserInfoDb oUserDb)
	{
		m_nUserSeqId = oUserDb.getUserSeqId();
		m_sUserId = oUserDb.getUserId();
		m_sUserDisplayName = oUserDb.getUserDisplayName();
		m_sUserShortName = oUserDb.getUserShortName();
		m_sUserEmail = oUserDb.getUserEmail();
		m_nUserType = oUserDb.getUserType();
		m_sUserAuthValue = oUserDb.getUserAuthValue();
		
		m_nUserAuthType = AuthType.toEnum(oUserDb.getUserAuthType());
		m_nUserState = UserState.toEnum(oUserDb.getUserState());
		
	}
	
	public int getUserSeqId()
	{
		return m_nUserSeqId;
	}

	public void setUserSeqId(int p_nUserSeqId)
	{
		this.m_nUserSeqId = p_nUserSeqId;
	}

	public String getUserId()
	{
		return m_sUserId;
	}

	public void setUserId(String p_sUserId)
	{
		this.m_sUserId = p_sUserId;
	}

	public String getUserDisplayName()
	{
		return m_sUserDisplayName;
	}

	public void setUserDisplayName(String p_sUserDisplayName)
	{
		this.m_sUserDisplayName = p_sUserDisplayName;
	}

	public String getUserShortName()
	{
		return m_sUserShortName;
	}

	public void setUserShortName(String p_sUserShortName)
	{
		this.m_sUserShortName = p_sUserShortName;
	}

	public AuthType getUserAuthType()
	{
		return m_nUserAuthType;
	}

	public void setUserAuthType(AuthType p_nUserAuthType)
	{
		this.m_nUserAuthType = p_nUserAuthType;
	}

	public UserState getUserState()
	{
		return m_nUserState;
	}

	public void setUserState(int p_nUserState)
	{
		this.m_nUserState = UserState.toEnum(p_nUserState);
	}

	public void setUserState(UserState p_eUserState)
	{
		this.m_nUserState = p_eUserState;
	}

	public String getUserAuthValue()
	{
		return m_sUserAuthValue;
	}

	public void setUserAuthValue(String p_sUserAuthValue)
	{
		this.m_sUserAuthValue = p_sUserAuthValue;
	}

	public String getUserEmail()
	{
		return m_sUserEmail;
	}

	public void setUserEmail(String p_sUserEmail)
	{
		this.m_sUserEmail = p_sUserEmail;
	}
	
	public int getUserType()
	{
		return m_nUserType;
	}
	
	public void setUserType(int p_nUserType)
	{
		m_nUserType = p_nUserType;
	}
	
	private boolean checkType(int p_nMaskPattern)
	{
		return ((m_nUserType & p_nMaskPattern) > 0);
	}
	
	public boolean isSystemAdmin()
	{
		return checkType(UserAddonRole.SYSTEM_ADMIN.sMaskPattern);
	}
	
	public boolean isProjectAdmin()
	{
		return checkType(UserAddonRole.PROJECT_ADMIN.sMaskPattern);
	}
	
	public boolean isAuditor()
	{
		return checkType(UserAddonRole.AUDITOR.sMaskPattern);
	}
	
	public boolean isItServiceDeskUser()
	{
		return checkType(UserAddonRole.IT_SERVICE_DESK.sMaskPattern);
	}
	
	public boolean isProjectAuditor()
	{
		return checkType(UserAddonRole.PROJECT_AUDITOR.sMaskPattern);
	}
	
	public void setAddonUserRole(Set<UserAddonRole> p_oAddonRolesToGrant)
	{
		revokeAddonUserRoles();
		for (UserAddonRole oAddonRole : p_oAddonRolesToGrant)
		{
			grantAddonUserRole(oAddonRole);
			}
		}
	
	public void grantAddonUserRole(UserAddonRole p_oAddonRoleToGrant)
	{
		m_nUserType = (m_nUserType | p_oAddonRoleToGrant.sMaskPattern); 
	}
	
	public void revokeAddonUserRoles()
	{
		m_nUserType = 0; 
	}
	public boolean equals (Object p_object) {
		if (p_object != null && p_object  instanceof UserInfo) {
			if (this.m_sUserId.equals(((UserInfo)p_object).m_sUserId)) {
				return true;
			}
		}
		return false;
	}

	public boolean permittedToAccess(String p_sAppId)
	{
		if (AppRegistry.isUserMgmtApp(p_sAppId) && isItServiceDeskUser())
		{
			return true;
		}
		else if (AppRegistry.isSystemAdminApp(p_sAppId))
		{
			return isSystemAdmin();
		}
		else if (AppRegistry.isAuditTrailViewerApp(p_sAppId) && isProjectAuditor())
		{			
			return true;
		}
		else if (AppRegistry.isAuditTrailViewerApp(p_sAppId))
		{
			return isAuditor();			
		}
		else if (AppRegistry.isProjectAdminApp(p_sAppId))
		{
			return isProjectAdmin();
		}
		else if (AppRegistry.isProjectMangmtApp(p_sAppId))
		{
			return isProjectAdmin();
		}
		else if (AppRegistry.isProjectAuditor(p_sAppId))
		{
			return isProjectAuditor();
		}
		else
		{
			//All non-special Apps are available to all users
			return true;
		}
	}
	
	// Get User Role for display on UserManagement page.
	
	public String getUserRoleNames()
	{	
		List<String> listRoles = null;
		
		listRoles = new ArrayList<String>();
		for (UserAddonRole oRole : UserAddonRole.values())
		{
			if (checkType(oRole.getMaskPattern()))
			{
				listRoles.add(oRole.getDisplayName());
			}
		}
		
		return StringUtils.serialize(listRoles, StringConstants.COMMA_WITH_SPACE);
	}
	
	
	// Get list of project assigned to particular User.	
//	
//	public String getUserListOfProjectsNames(int p_iUserId) throws SciServiceException{	
//	
//	List<Integer> nProjectList = ProjectUserManagementHome.getUserListOfProjects(p_iUserId);
//	StringJoiner joiner = new StringJoiner(", ");
//	String joined =null;
//	for(Integer nProjectId : nProjectList)
//	{
//		oProject = ProjectHome.getProject(nProjectId);
//		joiner.add(oProject.getProjectName());
//	}
//	joined = joiner.toString();
//	
//	return joined;
//		
//	}

	
	
	
}
