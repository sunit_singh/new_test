/**
 * 
 */
package com.sciformix.sciportal.bootstrap;


/**
 * @author sshetty1
 *
 */
public class BootstrapInfo 
{
	
	private String m_sDBHost = null;
	private String m_sDBPort = null;
	private String m_sDBSID = null;
	private String m_sDBUsername = null;
	private String m_sDBPassword = null;
	private String m_sDBSchemaOwner = null;
	private String m_sDBConnectionURL = null;
	
	private String m_sLDAPDomain = null;
	private String m_sLDAPUrl = null;
	private String m_sLDAPUsername = null;
	private String m_sLDAPPassword = null;
	
	private BootstrapInfo(String p_sDBHost, String p_sDBPort, String p_sDBSID, String p_sDBUsername, String p_sDBPassword, String p_sDBConnectionURL, String p_sDBSchemaOwner,
								String p_sLDAPDomain, String p_sLDAPUrl, String p_sLDAPUsername, String p_sLDAPPassword)
	{
		m_sDBHost = p_sDBHost;
		m_sDBPort = p_sDBPort;
		m_sDBSID = p_sDBSID;
		m_sDBUsername = p_sDBUsername;
		m_sDBPassword = p_sDBPassword;
		m_sDBConnectionURL = p_sDBConnectionURL;
		m_sDBSchemaOwner = p_sDBSchemaOwner;
		
		m_sLDAPDomain = p_sLDAPDomain;
		m_sLDAPUrl = p_sLDAPUrl;
		m_sLDAPUsername = p_sLDAPUsername;
		m_sLDAPPassword = p_sLDAPPassword;
	}

	public static BootstrapInfo create(String p_sDBHost, String p_sDBPort, String p_sDBSID, String p_sDBUsername, String p_sDBPassword, 
			String p_sDBConnectionURL, String p_sDBSchemaOwner, String p_sLDAPDomain, String p_sLDAPUrl, String p_sLDAPUsername, String p_sLDAPPassword)
	{
		return new BootstrapInfo(p_sDBHost, p_sDBPort, p_sDBSID, p_sDBUsername, p_sDBPassword, p_sDBConnectionURL, p_sDBSchemaOwner,
				p_sLDAPDomain, p_sLDAPUrl, p_sLDAPUsername, p_sLDAPPassword);
	}
	
	public static BootstrapInfo createDBInfo(String p_sDBHost, String p_sDBPort, String p_sDBSID, String p_sDBUsername, String p_sDBPassword, 
			String p_sDBConnectionURL, String p_sDBSchemaOwner)
	{
		return new BootstrapInfo(p_sDBHost, p_sDBPort, p_sDBSID, p_sDBUsername, p_sDBPassword, p_sDBConnectionURL, p_sDBSchemaOwner, null, null, null, null);
	}
	
	public static BootstrapInfo createLDAPInfo(String p_sLDAPDomain, String p_sLDAPUrl, String p_sLDAPUsername, String p_sLDAPPassword)
	{
		return new BootstrapInfo(null, null, null, null, null, null, null, p_sLDAPDomain, p_sLDAPUrl, p_sLDAPUsername, p_sLDAPPassword);
	}
	
	public String getDBHost()
	{
		return m_sDBHost;
	}
	
	public String getDBPort()
	{
		return m_sDBPort;
	}
	
	public String getDBSID()
	{
		return m_sDBSID;
	}
	
	public String getDBUsername()
	{
		return m_sDBUsername;
	}
	
	public String getDBPassword()
	{
		return m_sDBPassword;
	}
	
	public String getDBConnectionURL()
	{
		return m_sDBConnectionURL;
	}
	
	public String getDBSchemaOwner()
	{
		return m_sDBSchemaOwner;
	}
	
	public String getLDAPDomain()
	{
		return m_sLDAPDomain;
	}
	
	public String getLDAPUrl()
	{
		return m_sLDAPUrl;
	}
	
	public String getLDAPUsername()
	{
		return m_sLDAPUsername;
	}
	
	public String getLDAPPassword()
	{
		return m_sLDAPPassword;
	}
}
