package com.sciformix.sciportal.projectusermanagement;

import java.util.HashSet;
import java.util.Set;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.StringUtils;

public class ProjectUserRole
{
	
	public ProjectUserRole()
	{
		//Nothing to do
	}
	private static final String USERROLE_SERIALIZATION_SEPARATOR = StringConstants.SEMICOLON;
	
	private int m_nUserSeqId = -1;
	private Set<String> m_setUserRoles = null;
	private String m_sClientName = null;
	private String m_sClientEmailId = null;
	private String m_sClientSafetyDBId = null;
	
	
	public ProjectUserRole(int p_nUserSeqId)
	{
		m_nUserSeqId = p_nUserSeqId;
		m_setUserRoles = new HashSet<String>();
	}
	
	public int getUserSeqId()
	{
		return m_nUserSeqId;
	}
	
	public void setUserSeqId(int userSeqId)
	{
		this.m_nUserSeqId = userSeqId;
	}
	
	public Set<String> getUserRoles()
	{
		return m_setUserRoles;
	}
	
	public void setUserRole(Set<String> p_setUserRoles)
	{
		this.m_setUserRoles = p_setUserRoles;
	}
	
	public void setUserRole(String p_sSerializedUserRoles)
	{
		String[] sarrUserRoles = null;
		
		sarrUserRoles = StringUtils.split(p_sSerializedUserRoles, USERROLE_SERIALIZATION_SEPARATOR);
		for (String sUserRole : sarrUserRoles)
		{
			this.m_setUserRoles.add(sUserRole);
		}
	}

	public String getM_sClientName() 
	{
		return m_sClientName;
	}

	public void setM_sClientName(String m_sClientName) 
	{
		this.m_sClientName = m_sClientName;
	}

	public String getM_sClientEmailId() 
	{
		return m_sClientEmailId;
	}

	public void setM_sClientEmailId(String m_sClientEmailId) 
	{
		this.m_sClientEmailId = m_sClientEmailId;
	}

	public String getM_sClientSafetyDBId() 
	{
		return m_sClientSafetyDBId;
	}

	public void setM_sClientSafetyDBId(String m_sClientSafetyDBId) 
	{
		this.m_sClientSafetyDBId = m_sClientSafetyDBId;
	}
	
}
