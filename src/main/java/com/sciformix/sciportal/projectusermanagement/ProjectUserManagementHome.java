package com.sciformix.sciportal.projectusermanagement;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;


public class ProjectUserManagementHome {
	
	private static final Logger log = LoggerFactory.getLogger(ProjectUserManagementHome.class);
	
	/**
	 * This method is used for saving project user
	 * @param connection
	 * @param projUser
	 * @param prjSeqId
	 * @param userSeqId
	 * @param p_bInsert
	 * @return boolean
	 * @throws SciServiceException
	 */
	public static boolean saveProjectUser(DbConnection connection, ArrayList<ProjectUserRole> projUser, int prjSeqId,int userSeqId, boolean p_bInsert )
			throws SciServiceException
		{
			try{
				return	ProjectUserManagementHomeImpl.saveProjectUser(connection, projUser, prjSeqId,userSeqId, p_bInsert);
			}
			catch(SciException sciEx)
			{
				log.error("Error saving project user");
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_SAVING_PROJECT_USER_INFO, sciEx);
			}
		
		}
	
	/**
	 * This method is used for getting list of user in a particular project
	 * @param prjId
	 * @return list userInfo
	 * @throws SciServiceException
	 */
		public static List<UserInfo> getProjectUserList(int prjId) throws SciServiceException
		{
			List<UserInfo> userList = new ArrayList<UserInfo>();
			try (DbConnection connection = DbConnectionManager.getConnection())
			{
				try {
					connection.startTransaction();
					List<ProjectUserRole> projectUserList=ProjectUserManagementHomeImpl.getProjectUserList(connection, prjId);
				
					 for(ProjectUserRole projUser:projectUserList )
					 {
						 userList.add(UserHome.retrieveUserBySeqId(projUser.getUserSeqId()));
					 }
					 connection.commitTransaction();
				} catch (SciException Excep)
				{
					connection.rollbackTransaction();
					log.error("Error fetching Project assigned userList", Excep);
					throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_USER_LIST, Excep);
				}
			}
			catch(SciDbException scDbExp)
			{
				log.error("Error fetching Project assigned userList", scDbExp);
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_USER_LIST, scDbExp);
			}
			return userList ;
	}
		
		/**
		 * This method is used for getting project user list by passing project Id
		 * @param prjId
		 * @return list ProjectUserRole
		 * @throws SciServiceException
		 */
		public static List<ProjectUserRole> getProjectListByProjectId(int prjId) throws SciServiceException
		{
			List<ProjectUserRole>	l_projectuserrole	=	null;
			try (DbConnection connection = DbConnectionManager.getConnection())
			{
				try {
					connection.startTransaction();
					l_projectuserrole	=	ProjectUserManagementHomeImpl.getProjectUserList(connection, prjId);
					connection.commitTransaction();
				} 
				catch (SciException Excep) {
					connection.rollbackTransaction();
					log.error("Error fetching Project assigned userList", Excep);
					throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_USER_LIST, Excep);
				}
			}
			catch(SciDbException scDbExp)
			{
				log.error("Error fetching Project assigned userList", scDbExp);
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_USER_LIST, scDbExp);
			}
			return l_projectuserrole;
		}
	
		/**
		 * This method is used for getting list of project for particular user.
		 * @param p_UserSeqId
		 * @return list ObjectIdPair
		 * @throws SciServiceException
		 */
	public static List<ObjectIdPair<Integer,String>> getUserListOfProjects(int p_UserSeqId) throws SciServiceException
	{
		List<ObjectIdPair<Integer,String>> listProjects = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				listProjects = ProjectUserManagementHomeImpl.getUserListOfProjects(connection, p_UserSeqId);
				
				connection.commitTransaction();
			}
			catch (SciException Excep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching Project assigned user", Excep);
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_PROJECT_USER, Excep);
			}
		}
		catch(SciDbException scDbExp)
		{
			log.error("Error fetching Project assigned user", scDbExp);
			throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_PROJECT_USER, scDbExp);
		}
		
		return listProjects;
	}
	
	/**
	 * This method is used for getting list of Project Users by user Id.
	 * @param userId
	 * @return ProjectUserRole
	 * @throws SciServiceException
	 */
	public static ProjectUserRole getProjectUserByUserSeqId(int userId)throws SciServiceException
	{

		try (DbConnection connection = DbConnectionManager.getConnection())
		{
		 return ProjectUserManagementHomeImpl.getProjectUserByUserSeqId(connection,  userId);
		
		}
		catch(SciDbException scDbExp)
		{
			log.error("Error fetching Project assigned user", scDbExp);
			throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_PROJECT_USER, scDbExp);
		}catch (SciException Excep)
		{
			log.error("Error fetching Project assigned user", Excep);
			throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_FETCHING_PROJECT_USER, Excep);
		}
	}
	
	/**
	 * This method is used for deleting user from project
	 * @param p_ProjectId
	 * @param u_UserId
	 * @return boolean
	 * @throws SciServiceException
	 */
	public static  boolean deleteUser(int p_ProjectId, int u_UserId) throws  SciServiceException
	{
		try (DbConnection connection = DbConnectionManager.getConnection())
		{ 
			try {
				connection.startTransaction();
				 ProjectUserManagementHomeImpl.deleteUser(connection, p_ProjectId, u_UserId);
				 connection.commitTransaction();
			} catch (SciException Excep)
			{
				connection.rollbackTransaction();
				log.error("Error removing project user", Excep);
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_REMOVING_PROJECT_USER, Excep);
			}
			 
		}
		catch(SciDbException scDbExp)
		{
			log.error("Error removing project user", scDbExp);
			throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_REMOVING_PROJECT_USER, scDbExp);
		}
		return true;
	}
	
	/**
	 * This method is used for getting project by user seqId
	 * @param u_UserId
	 * @return ProjectId
	 * @throws SciServiceException
	 */
	public static int getProjectIdByUserSeqId(int u_UserId) throws  SciServiceException
	{
		int projectId = -1;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{ 
			try {
				connection.startTransaction();
				projectId =  ProjectUserManagementHomeImpl.getProjectIdByUserSeqId(connection, u_UserId);
				 connection.commitTransaction();
			} catch (SciException Excep)
			{
				connection.rollbackTransaction();
				log.error("Error removing project user", Excep);
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_REMOVING_PROJECT_USER, Excep);
			}
			 
		}
		catch(SciDbException scDbExp)
		{
			log.error("Error removing project user", scDbExp);
			throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_REMOVING_PROJECT_USER, scDbExp);
		}
		return projectId;
	}
}