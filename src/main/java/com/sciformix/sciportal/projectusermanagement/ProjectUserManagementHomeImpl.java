package com.sciformix.sciportal.projectusermanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQueryDelete;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.project.ProjectHomeImpl;

public class ProjectUserManagementHomeImpl
{
	
	private static final Logger log = LoggerFactory.getLogger(ProjectUserManagementHomeImpl.class);
	
	/**
	 * This method is used for getting list of users in project by project id
	 * @param connection
	 * @param prjId
	 * @return List ProjectUserRole
	 * @throws SciException
	 */
	public static List<ProjectUserRole> getProjectUserList(DbConnection connection, int prjId) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		ProjectUserRole projectUserManagement = new ProjectUserRole(prjId);
		List<ProjectUserRole> projectUserManagementList= new ArrayList<ProjectUserRole>();
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectUserDbSchema.PROJECTUSER,ProjectUserDbSchema.PROJECTUSER.PRJSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(prjId);
		
		try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						projectUserManagement = readProjectUserRoleFromDb(resultSet);
					
						projectUserManagementList.add(projectUserManagement);
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching Project user - {}", prjId, dbExcep);
				throw new SciException("Error fetching project user", dbExcep);
			}
		return projectUserManagementList;
		}
		

	/**
	 * This method is used for getting Project List of Users
	 * @param connection
	 * @param p_ProjectId
	 * @return List ProjectUserRole
	 * @throws SciException
	 */
	public static List<ProjectUserRole> getProjectListOfUsers(DbConnection connection, int p_ProjectId) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectUserDbSchema.PROJECTUSER, ProjectUserDbSchema.PROJECTUSER.PRJSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_ProjectId);
		
		return fetchProjectListOfUsers(connection, oSelectQuery);
	}
	
	/**
	 * This method is used for getting user list of projects
	 * @param connection
	 * @param p_UserSeqId
	 * @return List ObjectIdPair
	 * @throws SciException
	 */
	public static List<ObjectIdPair<Integer,String>> getUserListOfProjects(DbConnection connection, int p_UserSeqId) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectUserDbSchema.PROJECTUSER, ProjectUserDbSchema.PROJECTUSER.USERSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_UserSeqId);
		
		return fetchUserListOfProjects(connection, oSelectQuery);
	}
	
	private static List<ProjectUserRole> fetchProjectListOfUsers(DbConnection connection, DbQuerySelect oSelectQuery) throws SciException
	{
		ProjectUserRole oProjectUserRole = null;
		List<ProjectUserRole> listProjectUserList= null;
		
		listProjectUserList = new ArrayList<ProjectUserRole>();
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while(resultSet.next())
				{
					oProjectUserRole = readProjectUserRoleFromDb(resultSet);
				
					listProjectUserList.add(oProjectUserRole);
				}
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error fetching Project users", dbExcep);
			throw new SciException("Error fetching project user", dbExcep);
		}
		
		return listProjectUserList;
	}
	
	private static List<ObjectIdPair<Integer,String>> fetchUserListOfProjects(DbConnection connection, DbQuerySelect oSelectQuery) throws SciException
	{
		List<ObjectIdPair<Integer,String>> listProjects= null;
		ObjectIdPair<Integer,String> oObjectIdPair = null;
		
		listProjects = new ArrayList<ObjectIdPair<Integer,String>>();
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while(resultSet.next())
				{/*
					oObjectIdPair = new ObjectIdPair<>(resultSet.readInt(ProjectUserDbSchema.PROJECTUSER.PRJSEQID), ""+resultSet.readInt(ProjectUserDbSchema.PROJECTUSER.PRJSEQID));*/
					
					
					oObjectIdPair = new ObjectIdPair<>(resultSet.readInt(ProjectUserDbSchema.PROJECTUSER.PRJSEQID), ""+ProjectHomeImpl.getProjectGivenProjectId(resultSet.readInt(ProjectUserDbSchema.PROJECTUSER.PRJSEQID)).getProjectName());
					
					listProjects.add(oObjectIdPair);
				}
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error fetching Project users", dbExcep);
			throw new SciException("Error fetching project user", dbExcep);
		}
		
		return listProjects;
	}
	
	/**
	 * This method is used for saving Project user
	 * @param connection
	 * @param projUser
	 * @param prjSeqId
	 * @param userSeqId
	 * @param p_bInsert
	 * @return boolean
	 * @throws SciException
	 */
	public static boolean saveProjectUser(DbConnection connection, ArrayList<ProjectUserRole> projUser, int prjSeqId, int userSeqId, boolean p_bInsert )
			throws SciException
		{
			ProjectUserRole projectUserRole = new ProjectUserRole(prjSeqId);
			
			try
			{
				connection.startTransaction();
				if (p_bInsert)
				{	
					DbQueryInsert oInsertQuery = null;
						
				for (ProjectUserRole pu : projUser)
				{
					oInsertQuery = DbQueryUtils.createInsertQuery(ProjectUserDbSchema.PROJECTUSER);
					oInsertQuery.addInsertParameter(prjSeqId);
					oInsertQuery.addInsertParameter(pu.getUserSeqId());
					oInsertQuery.addInsertParameter(pu.getM_sClientName());
					oInsertQuery.addInsertParameter(pu.getM_sClientEmailId());
					oInsertQuery.addInsertParameter(pu.getM_sClientSafetyDBId());
					oInsertQuery.addInsertParameter("null");
					oInsertQuery.addInsertParameter("null");
					oInsertQuery.addInsertParameter(new Date());
					oInsertQuery.addInsertParameter(7);
					connection.executeQuery(oInsertQuery);
					}
				}
				connection.commitTransaction();
			}
			catch(SciDbException dbExcep)
			{
			connection.rollbackTransaction();
			log.error("Error saving project user - {}", dbExcep);
			throw new SciException("Error saving project user", dbExcep);
			}
		return true;
		}
	/**
	 * This method is used for saving Project user
	 * @param connection
	 * @param projUser
	 * @param prjSeqId
	 * @param userSeqId
	 * @param p_bInsert
	 * @return boolean
	 * @throws SciException
	 */
	/*public static boolean saveProjectUser1(DbConnection connection, String[] users,List<Object> roles,int prjSeqId, int userSeqId, boolean p_bInsert )
			throws SciException
		{
			ProjectUserRole projectUserRole = new ProjectUserRole(prjSeqId);
			
			try
			{
				connection.startTransaction();
				if (p_bInsert)
				{	
					DbQueryInsert oInsertQuery = null;
						
				for (String s : users)
				{
					//projectUserRole.setUserRole(new HashSet<>());
					projectUserRole.setUserSeqId(Integer.parseInt(s));
					oInsertQuery = DbQueryUtils.createInsertQuery(ProjectUserDbSchema.PROJECTUSER);
					oInsertQuery.addInsertParameter(prjSeqId);
					oInsertQuery.addInsertParameter(projectUserRole.getUserSeqId());
					oInsertQuery.addInsertParameter("null");
					oInsertQuery.addInsertParameter("null");
					oInsertQuery.addInsertParameter(new Date());
					oInsertQuery.addInsertParameter(7);
					connection.executeQuery(oInsertQuery);
					}
				}
				connection.commitTransaction();
			}
			catch(SciDbException dbExcep)
			{
			connection.rollbackTransaction();
			log.error("Error saving project user - {}", dbExcep);
			throw new SciException("Error saving project user", dbExcep);
			}
		return true;
		}*/
			
	private static ProjectUserRole readProjectUserRoleFromDb(DbResultSet resultSet) throws SciDbException
	{
		ProjectUserRole oProjUserRole = null;
		String sUserRolePayload1 = null;
		String sUserRolePayload2 = null;
		
		sUserRolePayload1 = resultSet.readString(ProjectUserDbSchema.PROJECTUSER.PRJUSERROLE1);
		sUserRolePayload2 = resultSet.readString(ProjectUserDbSchema.PROJECTUSER.PRJUSERROLE2);
		
		oProjUserRole = new ProjectUserRole(resultSet.readInt(ProjectUserDbSchema.PROJECTUSER.USERSEQID));
		if(sUserRolePayload2!=null)
		{
			if(sUserRolePayload1!=null)
			{
				sUserRolePayload1=sUserRolePayload1+sUserRolePayload2;
			}
			else{
				sUserRolePayload1=sUserRolePayload2;
			}
		}
		oProjUserRole.setUserRole(DataUtils.combineSplitFragments(sUserRolePayload1));
		
		return oProjUserRole ;
	}
			
	/**
	 * This method is used for getting ProjectUser by passing User seqId
	 * @param connection
	 * @param p_nUserSeqId
	 * @return ProjectUserRole
	 * @throws SciException
	 */		
	public static ProjectUserRole getProjectUserByUserSeqId(DbConnection connection, int p_nUserSeqId)
			throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		ProjectUserRole projectUserManagement = new ProjectUserRole(p_nUserSeqId);
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectUserDbSchema.PROJECTUSER,ProjectUserDbSchema.PROJECTUSER.USERSEQID.bindClause());
		
		oSelectQuery.addWhereClauseParameter(p_nUserSeqId);
		
		try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						projectUserManagement =  readProjectUserRoleFromDb(resultSet);
					
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching Project user", dbExcep);
				throw new SciException("Error fetching project user", dbExcep);
			}
			
		return projectUserManagement;
		
	}
	
	/**
	 * This method is used for deleting user from ProjectUser by passing Project Id and User Id.
	 * @param connection
	 * @param p_nUserSeqId
	 * @param p_sProjectId
	 * @return ProjectUserRole
	 * @throws SciException
	 */	
	public static boolean deleteUser(DbConnection connection, int p_sProjectId, int  u_sUserId) throws SciException
	{
		DbQueryDelete deleteQuery = null;
		deleteQuery = DbQueryUtils.createDeleteQuery(ProjectUserDbSchema.PROJECTUSER,DbQueryUtils.bindWhereClauseWithAnd(
							ProjectUserDbSchema.PROJECTUSER.PRJSEQID,ProjectUserDbSchema.PROJECTUSER.USERSEQID ) ) ;
		deleteQuery.addWhereClauseParameter(p_sProjectId);
		deleteQuery.addWhereClauseParameter(u_sUserId);
		try
		{
			connection.startTransaction();
			connection.executeQuery(deleteQuery);
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error removing Project user", dbExcep);
			throw new SciException(" project user", dbExcep);
		}
		return true;
	}
	
	/**
	 * This method is used for deleting user from ProjectUser by passing Project Id and User Id.
	 * @param connection
	 * @param p_nUserSeqId
	 * @param p_sProjectId
	 * @return ProjectUserRole
	 * @throws SciException
	 */	
	public static int getProjectIdByUserSeqId(DbConnection connection, int p_nUserSeqId)
			throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		int projectid = -1;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ProjectUserDbSchema.PROJECTUSER,ProjectUserDbSchema.PROJECTUSER.USERSEQID.bindClause());
		
		oSelectQuery.addWhereClauseParameter(p_nUserSeqId);
		
		try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						projectid =  resultSet.readInt(ProjectUserDbSchema.PROJECTUSER.PRJSEQID);
					
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching Project user", dbExcep);
				throw new SciException("Error fetching project user", dbExcep);
			}
			
		return projectid;
		
	}
	
}
