package com.sciformix.sciportal.projectusermanagement;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class ProjectUserDbSchema {
	
	public static ProjectUser PROJECTUSER= new ProjectUser();
	
	public static class ProjectUser extends DbTable
	{
		public DbColumn PRJSEQID = null;
		public DbColumn USERSEQID = null;
		public DbColumn CLIENTNAME = null;
		public DbColumn CLIENTEMAILID = null;
		public DbColumn CLIENTSAFETYDBID = null;
		public DbColumn PRJUSERROLE1 = null;
		public DbColumn PRJUSERROLE2 = null;
		public DbColumn DTCREATED = null;
		public DbColumn USRCREATED = null;
				
		public ProjectUser()
		{
			super("PRJ_PROJECTS_USERS");
			setPkSequenceName(null);
			
			PRJSEQID = addColumn("PRJSEQID");
			USERSEQID = addColumn("USERSEQID");
			CLIENTNAME = addColumn("CLIENTNAME");
			CLIENTEMAILID = addColumn("CLIENTEMAILID");
			CLIENTSAFETYDBID = addColumn("CLIENTSAFETYDBID");
			PRJUSERROLE1 = addColumn("PRJUSERROLE1");
			PRJUSERROLE2 = addColumn("PRJUSERROLE2");
			DTCREATED = addColumn("DTCREATED");
			USRCREATED = addColumn("USRCREATED");
		}
	}
}
