package com.sciformix.sciportal.projectusermanagement;

public class ProjectUserAuditHelper {
	
	public static class ProjectUserAttributes
	{
		public static final String PROJECT_SEQUENCE_ID = "Project Sequence Id"; 
		public static final String User_SEQ_ID = "User Seq Id";
		public static final String User_ID = "User Id";
		
		public static final String User_Role = "User Role";
		
		public static final String PROJECT_CLIENT_NAME = "Project Client Name";
		public static final String PROJECT_CLIENT_EMAIL_ID = "Project Client Email Id";
		public static final String PROJECT_CLIENT_SAFETY_DB_ID = "Project Client Safety DB ID";
			
	}
	
	private ProjectUserAuditHelper()
	{
		//Nothing to do
	}

}
