package com.sciformix.sciportal.apps.activity;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.sciportal.api.activity.ActivityDetailsInfoDb;
import com.sciformix.sciportal.api.activity.ActivityHome;
import com.sciformix.sciportal.api.activity.ActivityHomeImpl;
import com.sciformix.sciportal.api.activity.ActivityInfoDb;
import com.sciformix.sciportal.api.model.ServiceApiContext;
import com.sciformix.sciportal.api.utils.WebApiUtils;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;

public class ActivityPortalApp extends BasePortalApp {

	private static final Logger log = LoggerFactory.getLogger(ActivityPortalApp.class);

	public ActivityPortalApp() {
		super("ACTIVITY-PORTAL-APP","ActivityPortalApp",false, false);
	}

	public static enum ACTIVITY_FETCH_SCOPE
	{
		TIME_TODAY_TILL_NOW, TIME_YESTERDAY_TILL_NOW, LAST_7_DAYS, START_OF_TIME;
	}
	
	public static enum ACTIVITY_FETCH_TYPE
	{
		USER, SYSTEM;
	}

	public int registerNewActivity(UserInfo oUserInfo, int projectSeqId, String actServer, String actApp,
			String actType, String actCode, String payload, Map<String, String> clientParamsMap,
			Map<String, String> systemPayloadMap) throws SciServiceException, ParseException, IOException {

		int respParentActivitySeqId = -1;

		// Creating the respective stubs
		ActivityInfoDb activityInfo = createActivityStub(oUserInfo, projectSeqId, actServer, actApp, actType, actCode,
				payload, clientParamsMap, systemPayloadMap);

		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				
				connection.startTransaction();
				respParentActivitySeqId = ActivityHomeImpl.saveActivity(connection, activityInfo);
				connection.commitTransaction();

			} catch (SciException e) {
				connection.rollbackTransaction();
				log.error("Error fetching audit activity : ", e);
			}
		} catch (SciDbException dbExcep) {
			log.error("Error fetching audit activity : ", dbExcep);
		}

		return respParentActivitySeqId;
	}

	public int registerIntoExistingActivityTrail(int currParentActivityId, String actCode, String actDesc, String payload,
			Map<String, String> clientParamsMap)
			throws SciServiceException, ParseException, IOException {
		int respParentActSeqId = -1;
		ActivityDetailsInfoDb activityDetails = null;
		activityDetails = createActivityDetailsStub(currParentActivityId, actCode, actDesc, payload, clientParamsMap);

		List<ActivityInfoDb> existingActivityList = null;

		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				
				connection.startTransaction();

				existingActivityList = ActivityHomeImpl.getActivityListByActivitySeqId(connection, currParentActivityId);
				if (existingActivityList.size() > 0) {
					respParentActSeqId = ActivityHomeImpl.saveActivityDetails(connection, activityDetails, currParentActivityId);
				} else {
					respParentActSeqId = WebErrorCodes.WebServices.NO_SUCH_ACTIVITY.errorCode();
				}
				
				connection.commitTransaction();

			} catch (SciException e) {
				connection.rollbackTransaction();
				log.error("Error fetching audit activity : ", e);
			}
		} catch (SciDbException e) {
			log.error("Error fetching audit activity : ", e);
		}

		return respParentActSeqId;
	}

	public static ActivityInfoDb createActivityStub(UserInfo oUserInfo, int projectSeqId, String actServer,
			String actApp, String actType, String actCode, String payload, Map<String, String> clientParamsMap,
			Map<String, String> systemPayloadMap) throws ParseException, IOException {
		ActivityInfoDb activity = new ActivityInfoDb();

		activity.setActivityDateTime(WebApiUtils.getFormattedDate(WebApiUtils.getSystemDate()));
		activity.setActivityUserSeqId(oUserInfo.getUserSeqId());
		activity.setActivityUserId(oUserInfo.getUserId());
		activity.setProjectSeqId(projectSeqId);
		activity.setActivityServer(actServer);
		activity.setActivityApp(actApp);
		activity.setActivityType(actType);
		activity.setActivityCode(actCode);
		activity.setActivityClientIp(systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_IPADDRESS));
		activity.setActivityClientHost(systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINENAME));
		activity.setActivityClientHostDomain(systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINEDOMAIN));
		activity.setActivityClientDateTime(WebApiUtils.getFormattedDate(clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TIMESTAMP)));
		activity.setActivityClientTimeZone(clientParamsMap.get(ServiceApiContext.CONTEXT_VARIABLE_TIMEZONE));
		activity.setMachineLoginUser(systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINELOGGEDINUSER));
		activity.setMachineLoginUserDomain(systemPayloadMap.get(ServiceApiContext.CONTEXT_VARIABLE_MACHINELOGGEDINUSERDOMAIN));
		activity.setSystemPayload(WebApiUtils.getJsonStringFromMap(systemPayloadMap));

		return activity;
	}

	public static ActivityDetailsInfoDb createActivityDetailsStub(int actSeqId, String actStepCode,
			String actStepDesc, String actPayload, Map<String, String> clientParamsMap) throws IOException {
		ActivityDetailsInfoDb activityDetails = new ActivityDetailsInfoDb();
		Map<String, String> payloadMap = new HashMap<String, String>();
		payloadMap.put("ActivityPayload", actPayload);
		String payload = WebApiUtils.getJsonStringFromMap(payloadMap);
		payload = payload.replace("\\r\\n","");
		payload = payload.replace("\\","");
		
		activityDetails.setActivitySeqId(actSeqId);
		activityDetails.setActivityDateTime(WebApiUtils.getFormattedDate(WebApiUtils.getSystemDate()));
		activityDetails.setActivityStepCode(actStepCode);
		activityDetails.setActivityStepDesc(actStepDesc);
		activityDetails.setActivityPayload(payload);

		return activityDetails;
	}
	
	/**
	 * This method is used for getting Activity for User
	 * @param p_oUserSession
	 * @param p_eFetchType
	 * @param p_nUserSeqId
	 * @param p_eFetchScope
	 * @param p_bFetchActivityDetails
	 * @param p_nUserProjectId
	 * @return List AuditInfoDb
	 * @throws SciServiceException
	 */	
	public List<ActivityInfoDb> getActivityForUser(UserSession p_oUserSession,ACTIVITY_FETCH_TYPE p_eFetchType, int p_nUserSeqId, ACTIVITY_FETCH_SCOPE p_eFetchScope, boolean p_bFetchActivityDetails, int p_nUserProjectId) 
			throws SciServiceException
	{
		List<ActivityInfoDb> listActivityTrail = null;
		Calendar calThresholdDate = null;
		
		switch (p_eFetchScope)
		{
		case TIME_TODAY_TILL_NOW:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
			
			break;
		case TIME_YESTERDAY_TILL_NOW:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
				 
			calThresholdDate.add(Calendar.DAY_OF_MONTH, -1);
			break;
		case LAST_7_DAYS:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
			
			calThresholdDate.add(Calendar.DAY_OF_MONTH, -7);
			break;
		case START_OF_TIME:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
			
			calThresholdDate.add(Calendar.DAY_OF_MONTH, -365*10);//Start of time is taken as 10-years
			break;
		default:
			break;
		}
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
					

					if (p_eFetchType == ACTIVITY_FETCH_TYPE.USER)
					{
						listActivityTrail =  ActivityHome.getUserActivityTrailForUser(connection, p_nUserSeqId, calThresholdDate, null, p_bFetchActivityDetails,p_nUserProjectId);
					}
					else if (p_eFetchType == ACTIVITY_FETCH_TYPE.SYSTEM)
					{
						listActivityTrail =  ActivityHome.getUserActivityTrailForUser(connection, p_nUserSeqId, calThresholdDate, null, p_bFetchActivityDetails,p_nUserProjectId);
					}
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching Activity trail for user", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Activity.ERROR_FETCHING_USER_ACTIVITY_TRAIL, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching Activity trail for user", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Activity.ERROR_FETCHING_USER_ACTIVITY_TRAIL, dbExcep);
		}
		return listActivityTrail;
	}
	
	public ActivityPortalApp(String p_sPortalAppId, String p_sPortalAppName, boolean p_bAllowsUserApplicationDisable,
			boolean p_bApplicationProjectSpecific) {
		super(p_sPortalAppId, p_sPortalAppName, p_bAllowsUserApplicationDisable, p_bApplicationProjectSpecific);
		// TODO Auto-generated constructor stub
	}

}
