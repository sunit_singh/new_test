package com.sciformix.sciportal.apps;

import java.util.List;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.sciportal.user.UserSession;

public class AppUserPreferenceMetadata
{
	
	public static abstract class PreferenceValue
	{
		public List<ObjectIdPair<String, String>> getPreferenceValues(UserSession p_oUserSession)
		{
			return null;
		}
		public List<ObjectIdPair<String, String>> getPreferenceValuesByProjSeqId(UserSession p_oUserSession,int prjSeqId )
		{
			return null;
		}
	}
	
	public enum PreferenceType
	{
		ENUMERATED_SINGLE_VALUED,ENUMERATED_MULTIPLE_VALUED;
		//RADIO, RADIO_YESNO, ENUMERATED_SINGLE_VALUED, ENUMERATED_MULTIPLE_VALUED;
	}
	
	public enum Type
	{
		INLINE_LIST, CALLBACK_FUNCTION, INLINE_LIST_PAIR;
	}
	
	private String m_sPreferenceKey = null;
	private String m_sPreferenceKeyName = null;
	private PreferenceType m_ePreferenceType = null;
	private boolean m_bMandatory = false;
	private Type m_eType = null;
	private List<ObjectIdPair<String, String>> m_sarrOptionValues = null;
	private PreferenceValue m_functionPreferenceValuesCallback = null;
	
	public static AppUserPreferenceMetadata createSingleValuedPreference(String p_sPreferenceKey, String p_sPreferenceKeyName, List<ObjectIdPair<String, String>> p_sarrOptionValues, boolean p_bMandatory)
	{
		AppUserPreferenceMetadata oAppUserPref = null;
		
		oAppUserPref = new AppUserPreferenceMetadata(p_sPreferenceKey, p_sPreferenceKeyName, PreferenceType.ENUMERATED_SINGLE_VALUED, p_bMandatory);
		oAppUserPref.m_eType = Type.INLINE_LIST;
		oAppUserPref.m_sarrOptionValues = p_sarrOptionValues;
		
		return oAppUserPref;
	}
	
	public static AppUserPreferenceMetadata createSingleValuedPreference(String p_sPreferenceKey, String p_sPreferenceKeyName, PreferenceValue p_functionPreferenceValuesCallback, boolean p_bMandatory)
	{
		AppUserPreferenceMetadata oAppUserPref = null;
		
		oAppUserPref = new AppUserPreferenceMetadata(p_sPreferenceKey, p_sPreferenceKeyName, PreferenceType.ENUMERATED_SINGLE_VALUED, p_bMandatory);
		oAppUserPref.m_eType = Type.CALLBACK_FUNCTION;
		oAppUserPref.m_functionPreferenceValuesCallback = p_functionPreferenceValuesCallback;
		
		return oAppUserPref;
	}
	
	public static AppUserPreferenceMetadata createMultiValuedPreference(String p_sPreferenceKey, String p_sPreferenceKeyName,
			List<ObjectIdPair<String, String>> p_sarrOptionValues, boolean p_bMandatory)
	{
		AppUserPreferenceMetadata oAppUserPref = null;
		
		oAppUserPref = new AppUserPreferenceMetadata(p_sPreferenceKey, p_sPreferenceKeyName, PreferenceType.ENUMERATED_MULTIPLE_VALUED, p_bMandatory);
		oAppUserPref.m_eType = Type.INLINE_LIST_PAIR;
		oAppUserPref.m_sarrOptionValues = p_sarrOptionValues;
		
		return oAppUserPref;
	}
	
	public static AppUserPreferenceMetadata createMultiValuedPreference(String p_sPreferenceKey, String p_sPreferenceKeyName, PreferenceValue p_functionPreferenceValuesCallback, boolean p_bMandatory)
	{
		AppUserPreferenceMetadata oAppUserPref = null;
		
		oAppUserPref = new AppUserPreferenceMetadata(p_sPreferenceKey, p_sPreferenceKeyName, PreferenceType.ENUMERATED_MULTIPLE_VALUED, p_bMandatory);
		oAppUserPref.m_eType = Type.CALLBACK_FUNCTION;
		oAppUserPref.m_functionPreferenceValuesCallback = p_functionPreferenceValuesCallback;
		
		return oAppUserPref;
	}
	
	private AppUserPreferenceMetadata(String p_sPreferenceKey, String p_sPreferenceKeyName, PreferenceType p_ePreferenceType, boolean p_bMandatory)
	{
		m_sPreferenceKey = p_sPreferenceKey;
		m_sPreferenceKeyName = p_sPreferenceKeyName;
		m_ePreferenceType = p_ePreferenceType;
		m_bMandatory = p_bMandatory;
	}
	
	public String getPreferenceKey()
	{
		return m_sPreferenceKey;
	}
	
	public String getPreferenceKeyName()
	{
		return m_sPreferenceKeyName;
	}
	
	public PreferenceType getPreferenceType()
	{
		return m_ePreferenceType;
	}
	
	public boolean isMandatory()
	{
		return m_bMandatory;
	}
	
	public List<ObjectIdPair<String,String>> getOptionValues(UserSession p_oUserSession)
	{
		if (m_eType == Type.INLINE_LIST)
		{
			return m_sarrOptionValues;
		}
		else if (m_eType == Type.CALLBACK_FUNCTION)
		{
			return m_functionPreferenceValuesCallback.getPreferenceValues(p_oUserSession);
		}
		else
		{
			return null;
		}
	}
	
	public List<ObjectIdPair<String,String>> getOptionValues(UserSession p_oUserSession , int prjSeqId)
	{
		if (m_eType == Type.INLINE_LIST)
		{
			return m_sarrOptionValues;
		}
		else if (m_eType == Type.CALLBACK_FUNCTION)
		{
			return m_functionPreferenceValuesCallback.getPreferenceValuesByProjSeqId(p_oUserSession, prjSeqId);
		}
		else
		{
			return null;
		}
	}
	public List<ObjectIdPair<String,String>> getOptionValuesListPair(UserSession p_oUserSession)
	{
		if (m_eType == Type.INLINE_LIST_PAIR)
		{
			return m_sarrOptionValues;
		} else if (m_eType == Type.CALLBACK_FUNCTION)
		{
			return m_functionPreferenceValuesCallback.getPreferenceValues(p_oUserSession);
		} else {
			return null;
		}
	}
	
	public List<ObjectIdPair<String,String>> getOptionValuesListPair(UserSession p_oUserSession , int prjSeqId )
	{
		if (m_eType == Type.INLINE_LIST_PAIR)
		{
			return m_sarrOptionValues;
		} else if (m_eType == Type.CALLBACK_FUNCTION)
		{
			return m_functionPreferenceValuesCallback.getPreferenceValuesByProjSeqId(p_oUserSession, prjSeqId);
		} else {
			return null;
		}
	}
}
