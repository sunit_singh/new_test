package com.sciformix.sciportal.apps.file;

import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.sciportal.config.ConfigHome;

public class XmlParserHome {
	
	public static final String XML_FILE_NAME_KEY =  "SOURCE_DOC_UTILS_PORTAL_APP.parser.configuration";
	
	public static boolean init(SciStartupLogger oStartupLogger) 
	{
		return FileConfigurationHome.init(oStartupLogger, ConfigHome.getAppConfigSetting(XML_FILE_NAME_KEY));
	}
}
