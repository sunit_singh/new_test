package com.sciformix.sciportal.apps.sys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserSession;


public class AppSettingsPortalApp extends BasePortalApp
{
	private static final Logger log = LoggerFactory.getLogger(AppSettingsPortalApp.class);
	
	public AppSettingsPortalApp()
	{
		super("APPSETTINGS-PORTAL-APP", "AppSettingsPortalApp", false, false);
	}
	
	public void updateAppConfig(UserSession p_oUserSession, String p_sAppId, String p_sKey, String p_sNewValue) throws SciServiceException
	{
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				//DEV-NOTE: Following method handle audit trail creation
				ConfigHome.updateAppConfig(connection, p_oUserSession, p_sAppId, p_sKey, p_sNewValue);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating config", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_CONFIG, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error updating config", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_CONFIG, dbExcep);
		}
	}

	/*public void refreshConfigSettings() throws SciServiceException
	{
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
					ConfigHome.init(connection, oStartupLogger);
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error refresh system config", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_REFRESHING_SYS_CONFIG, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error refresh system config", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_REFRESHING_SYS_CONFIG, dbExcep);
		}
	}*/

}
