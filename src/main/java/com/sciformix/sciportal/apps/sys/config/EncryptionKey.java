package com.sciformix.sciportal.apps.sys.config;

import java.security.PrivateKey;
import java.security.PublicKey;

public class EncryptionKey {
	
	private String keyId;
	private String keyType;
	private String keyPurpose;
	private PublicKey publicKey;
	private PrivateKey privateKey;
						
	public PrivateKey getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	public PublicKey getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}
	public String getKeyId() {
		return keyId;
	}
	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
	public String getKeyType() {
		return keyType;
	}
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	public String getKeyPurpose() {
		return keyPurpose;
	}
	public void setKeyPurpose(String keyPurpose) {
		this.keyPurpose = keyPurpose;
	}
	/*public String getKeyName() {
		return keyName;
	}
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}*/
	public enum KeyType {
		PKI("PKI","Public Key Private Key Pair");
		
		private String
			sendCode
		;	
		private String
			displayName
		;	
		KeyType (String p_code, String p_displayname) {
			this.sendCode		=	p_code;
			this.displayName	=	p_displayname;
		}
		
		public String getSendCode () {
			return sendCode;
		}
		public String getDisplayName () {
			return displayName;
		}
		
		public static KeyType toEnum(String p_code)
		{
			KeyType[] values = KeyType.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (p_code.equals(values[counter].getSendCode()))
				{
					return values[counter];
				}
			}
			//TODO: Discuss about throwing the SciException rather than IllegalArgumentException
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + p_code);
		}
	}
	
	public enum KeyPurpose {
		WAPI("WAPI","Web API");
		
		private String
			sendCode
		;	
		private String
			displayName
		;	
		KeyPurpose (String p_code, String p_displayname) {
			this.sendCode		=	p_code;
			this.displayName	=	p_displayname;
		}
		
		public String getSendCode () {
			return sendCode;
		}
		public String getDisplayName () {
			return displayName;
		}
		
		public static KeyPurpose toEnum(String p_code)
		{
			KeyPurpose[] values = KeyPurpose.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (p_code.equals(values[counter].getSendCode()))
				{
					return values[counter];
				}
			}
			//TODO: Discuss about throwing the SciException rather than IllegalArgumentException
			throw new IllegalArgumentException("RecordState: Illegal enum value - " + p_code);
		}
	}
}
