package com.sciformix.sciportal.apps.sys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;


public class UserPrefsPortalApp extends BasePortalApp
{
	private static final Logger log = LoggerFactory.getLogger(UserPrefsPortalApp.class);
	
	public UserPrefsPortalApp()
	{
		super("USRPREF-PORTAL-APP", "UserPrefsPortalApp", false, false);
	}
	
	public boolean updatePreferenceValue(UserSession p_oUserSession, String p_sPreferenceKeyApp, String p_sPreferenceKey, String p_sPreferenceNewValue, int projectSeqId) 
								throws SciServiceException
	{
		String sPreferenceCurrentValue = null;
		AuditInfoDb oAuditInfo = null;
		boolean bRetValue = false;
		boolean bCurrentPreferencesRecordPresent = false;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				bCurrentPreferencesRecordPresent = p_oUserSession.isUserPreferenceAvailable(projectSeqId,p_sPreferenceKeyApp, p_sPreferenceKey );
				sPreferenceCurrentValue = p_oUserSession.getUserPreference(projectSeqId, p_sPreferenceKeyApp, p_sPreferenceKey);
				
				if (sPreferenceCurrentValue == null && p_sPreferenceNewValue == null)
				{
					//No change detected - Nothing to do
					bRetValue = false;
				}
				else if (sPreferenceCurrentValue!=null && p_sPreferenceNewValue != null && sPreferenceCurrentValue.equals(p_sPreferenceNewValue))
				{
					//No change detected - Nothing to do
					bRetValue = false;
				}
				else if (sPreferenceCurrentValue!=null && (p_sPreferenceNewValue == null || !sPreferenceCurrentValue.equals(p_sPreferenceNewValue)))
				{
					//Update the current record
					UserHome.saveUserPreference(connection, p_oUserSession.getUserInfo(), new UserPreference(projectSeqId,p_sPreferenceKeyApp, p_sPreferenceKey, p_sPreferenceNewValue), false);
					
					oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "USER-PREFERENCES-UPDATE");
					oAuditInfo.addPayload(p_sPreferenceKeyApp, p_sPreferenceKey);
					oAuditInfo.addDetails(p_sPreferenceKeyApp + ":" + p_sPreferenceKey, "String", sPreferenceCurrentValue, p_sPreferenceNewValue);
					AuditHome.savePopulatedStub(connection, oAuditInfo);
					
					bRetValue = true;
				}
				else
				{
					//DEV-NOTE: Although Current value is NULL, the user preferences record might still be present
					if (!bCurrentPreferencesRecordPresent)
					{
						//INSERT new record
						UserHome.saveUserPreference(connection, p_oUserSession.getUserInfo(), new UserPreference(projectSeqId, p_sPreferenceKeyApp, p_sPreferenceKey, p_sPreferenceNewValue), true);
					}
					else
					{
						//Update the current record
						UserHome.saveUserPreference(connection, p_oUserSession.getUserInfo(), new UserPreference(projectSeqId, p_sPreferenceKeyApp, p_sPreferenceKey, p_sPreferenceNewValue), false);
					}
					
					oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "USER-PREFERENCES-UPDATE");
					oAuditInfo.addPayload(p_sPreferenceKeyApp, p_sPreferenceKey);
					oAuditInfo.addDetails(p_sPreferenceKeyApp + ":" + p_sPreferenceKey, "String", null, p_sPreferenceNewValue);
					AuditHome.savePopulatedStub(connection, oAuditInfo);
					
					bRetValue = true;
				}
				
				connection.commitTransaction();
			}
			catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating preferences", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_PREFERENCES, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error updating preferences", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_PREFERENCES, dbExcep);
		}
		return bRetValue;
	}
}
