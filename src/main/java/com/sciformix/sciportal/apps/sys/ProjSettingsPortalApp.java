package com.sciformix.sciportal.apps.sys;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.mail.MailAuditHelper;
import com.sciformix.sciportal.mail.MailConfigurationHelper;
import com.sciformix.sciportal.mail.MailTemplate;
import com.sciformix.sciportal.projectusermanagement.ProjectUserAuditHelper;
import com.sciformix.sciportal.projectusermanagement.ProjectUserManagementHome;
import com.sciformix.sciportal.projectusermanagement.ProjectUserRole;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserPreference;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.user.UserUtils;


public class ProjSettingsPortalApp extends BasePortalApp
{
	String s_SuccessMessage = null;
	int userPreflProjIndependentProjId = -1;
	private static final Logger log = LoggerFactory.getLogger(ProjSettingsPortalApp .class);
	
	public ProjSettingsPortalApp()
	{
		super("PROJSETTINGS-PORTAL-APP", "ProjSettingsPortalApp", false, false);
	}
	
	/**
	 * This method is used for getting list Email Templates by passing Project Id
	 * @param p_nProjectId
	 * @return List of MailTemplate 
	 */
	public static List<MailTemplate> getAllEmailTemplates(int p_nProjectId)
	{
		return MailConfigurationHelper.getAllEmailTemplates(p_nProjectId);
	}
	
	/**
	 * This method is used for saving Email Templates 
	 * @param u_Session
	 * @param p_id
	 * @param m_oMailTemplate
	 * @param u_oUserInfo
	 * @param p_bInsert
	 * @return String Success Message 
	 * @throws SciServiceException
	 */
	public String saveMailTemplate(UserSession u_Session,int p_id,MailTemplate m_oMailTemplate,UserInfo u_oUserInfo, boolean p_bInsert) throws SciServiceException
	{
		MailTemplate mailTemplate=null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				AuditInfoDb oAuditInfo = null;
				
				mailTemplate=MailConfigurationHelper.saveMailTemplate(p_id, m_oMailTemplate, u_oUserInfo, true);
				oAuditInfo = AuditHome.createRecordInsertStub(u_Session,this.getPortalAppId(), "Mail Template Created",p_id);
				oAuditInfo.addPayload(MailAuditHelper.MailAttributes.MAIL_SEQUENCE_ID, mailTemplate.getSequenceId());
				oAuditInfo.addPayload(MailAuditHelper.MailAttributes.MAIL_TEMPLATE_NAME, m_oMailTemplate.getTemplateName());
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				s_SuccessMessage = "Email template sequence id: "+mailTemplate.getSequenceId()+" created successfully";
				connection.commitTransaction();
			} catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving mail template", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Email.ERROR_SAVING_EMAIL,sciExcep);
			}
		} catch(SciDbException sciExcep)
		{
			log.error("Error saving mail template", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Email.ERROR_SAVING_EMAIL,sciExcep);
		}
		return s_SuccessMessage;
	}
	
	/**
	 * This method is used for updating Email Templates 
	 * @param p_nProjectId
	 * @param m_Subject
	 * @param m_Body
	 * @param u_Session
	 * @param u_oUserInfo
	 * @param m_SeqId
	 * @return String Success Message 
	 */
	public String updateMailTemplate(int p_nProjectId, String m_Subject, String m_Body,
			UserSession u_Session, UserInfo u_oUserInfo, int m_SeqId ) 
	{
		List<MailTemplate> mailTemplateList= getAllEmailTemplates(p_nProjectId);
		MailTemplate m_oMailTemplate = null;
		
		for(MailTemplate mailTemplate: mailTemplateList)
		{	
			if(mailTemplate.getSequenceId()==m_SeqId)
			{
				m_oMailTemplate= mailTemplate;
			}
		}
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				AuditInfoDb oAuditInfo = null;
				
				oAuditInfo = AuditHome.createRecordUpdateStub(u_Session, this.getPortalAppId(), "Mail Tempalate Updated", p_nProjectId);
				oAuditInfo.addPayload(MailAuditHelper.MailAttributes.MAIL_SEQUENCE_ID,m_oMailTemplate.getSequenceId());
				oAuditInfo.addPayload(MailAuditHelper.MailAttributes.MAIL_TEMPLATE_NAME, m_oMailTemplate.getTemplateName());
				
				if(DataUtils.hasChanged(m_oMailTemplate.getSubjectTemplate(), m_Subject))
					oAuditInfo.addDetails(MailAuditHelper.MailAttributes.MAIL_SUBJECT, m_oMailTemplate.getSubjectTemplate(),m_Subject);
				
				if(DataUtils.hasChanged(m_oMailTemplate.getBodyTemplate(), m_Body))
					oAuditInfo.addDetails(MailAuditHelper.MailAttributes.MAIL_BODY, m_oMailTemplate.getBodyTemplate(),m_Body);
				
				 MailConfigurationHelper.updateMailTemplate(p_nProjectId, m_Subject,m_Body, u_oUserInfo, m_SeqId);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				s_SuccessMessage = "Mail template seq id "+m_oMailTemplate.getSequenceId()+" updated successfully";
				connection.commitTransaction();
			}
			catch(SciException sciExp)
			{
				connection.rollbackTransaction();
				log.error("Error updating mail template", sciExp);
			}
		}
		catch(SciDbException sciExcep)
		{
			log.error("Error updating mail template", sciExcep);
		}
		return s_SuccessMessage;
	}
	
	/**
	 * This method is used for changing Email Template status
	 * @param u_Session
	 * @param p_ProjectId
	 * @param p_sequenceid
	 * @param p_state
	 * @param u_oUserInfo
	 * @return boolean
	 */
	public boolean changeMailTemplateStatus(UserSession u_Session,int p_ProjectId, int p_sequenceid,RecordState p_state, UserInfo u_oUserInfo) 
	{
		MailTemplate mailtemplate=getMailtemplateBySeqId( p_sequenceid);
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				AuditInfoDb oAuditInfo = null;
				
				oAuditInfo = AuditHome.createRecordUpdateStub(u_Session, this.getPortalAppId(), "Mail Tempalate Status Change",p_ProjectId );
				oAuditInfo.addPayload(MailAuditHelper.MailAttributes.MAIL_SEQUENCE_ID,p_sequenceid);
				oAuditInfo.addPayload(MailAuditHelper.MailAttributes.MAIL_TEMPLATE_NAME, mailtemplate.getTemplateName());
				
				oAuditInfo.addDetails(MailAuditHelper.MailAttributes.MAIL_STATUS, mailtemplate.getState().displayName(),p_state.displayName());
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				connection.commitTransaction();
			}catch(SciException sciExp)
			{
				connection.rollbackTransaction();
				log.error("Error updating mail template status", sciExp);
				return false;
			}
		}
		catch(SciDbException sciExcep)
		{
			log.error("Error updating mail template status", sciExcep);
			return false;
		}
		
		return MailConfigurationHelper.changeMailTemplateStatus(p_ProjectId,p_sequenceid, p_state, u_oUserInfo);
	}
	
	/**
	 * This method is used for getting Email Template by passing mail template seq Id 
	 * @param m_sequenceid
	 * @return MailTemplate
	 */
	public static MailTemplate getMailtemplateBySeqId(int m_sequenceid) 
	{
		return MailConfigurationHelper.getMailTemplateBySeqId(m_sequenceid);
	}
	
	/**
	 * This method is used for getting all User List 
	 * @return List UserInfo
	 * @throws SciServiceException
	 */
	public static List<UserInfo> getAllUsersList() throws SciServiceException
	{
		List<UserInfo> l_UserInfoList= null;
		l_UserInfoList = UserHome.getListOfUsers();
		return l_UserInfoList;
	}
	
	/**
	 * This method is used for saving project user
	 * @param u_Session
	 * @param projUser
	 * @param projectSeqId
	 * @param userSeqId
	 * @param p_bInserts
	 * @return String Success Message
	 * @throws SciServiceException
	 */
	public  String saveProjectUser(UserSession u_Session,ArrayList<ProjectUserRole> projUser, int projectSeqId,int userSeqId, boolean p_bInserts ) throws SciServiceException
	{
		AuditInfoDb oAuditInfo = null;
		String successMsg =null;
		String[] users = null;
		UserInfo o_userInfo = null;
		List<UserPreference> l_selUserPref = null;
		boolean b_userPrefAvail = false;
		
		if(p_bInserts)
		{
			if(projUser==null){
				return "Please Select User";
			}
		}
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				if (p_bInserts)
				{
					//TODO:Review the code
					ProjectUserManagementHome.saveProjectUser(connection, projUser, projectSeqId,userSeqId, p_bInserts);
					
					
					users = new String[projUser.size()];
					int count=0;
					for(ProjectUserRole o_projectUserRole : projUser)
					{
						o_userInfo = UserHome.retrieveUserBySeqId(o_projectUserRole.getUserSeqId());
						l_selUserPref = UserHome.retrieveUserPreferences(o_userInfo);
						if(!l_selUserPref.isEmpty())
						{
							for (UserPreference userPref: l_selUserPref) 
									{
									if(userPref.getAppName().equals(UserUtils.PROJECT_MANAGEMENT_PORTAL_APP)   // Check UserPref available for selected user
												&& userPref.getKey().equals(UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID)
												&& userPref.getProjectSeqId()== userPreflProjIndependentProjId)
										{
											b_userPrefAvail = true;
										}
									}
						}else
						{
							UserHome.saveUserPreference(connection,o_userInfo, new UserPreference(userPreflProjIndependentProjId, UserUtils.PROJECT_MANAGEMENT_PORTAL_APP, UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID, ""+projectSeqId+""), true);
						}
						
						oAuditInfo = AuditHome.createRecordInsertStub(u_Session,this.getPortalAppId(), "User added to Project", projectSeqId);
						oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.PROJECT_SEQUENCE_ID, projectSeqId );
						
						oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.User_SEQ_ID, (o_projectUserRole.getUserSeqId()));
						oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.User_ID, UserHome.retrieveUserBySeqId(o_projectUserRole.getUserSeqId()).getUserId());
						
						oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.PROJECT_CLIENT_NAME, (o_projectUserRole.getM_sClientName()));
						oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.PROJECT_CLIENT_EMAIL_ID, (o_projectUserRole.getM_sClientEmailId()));
						oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.PROJECT_CLIENT_SAFETY_DB_ID,(o_projectUserRole.getM_sClientSafetyDBId()));
						
						users[count++]=""+o_projectUserRole.getUserSeqId();
						AuditHome.savePopulatedStub(connection, oAuditInfo);
					} 
					successMsg ="Users seq id "+String.join(",", users)+"  added successfully";
				}
				connection.commitTransaction();
			} catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving project", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_SAVING_PROJECT,sciExcep);
			}
			
		}
		catch(SciDbException sciExcep)
		{
			log.error("Error saving project", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_SAVING_PROJECT,sciExcep);
		}
		
		return successMsg;
	}
	
	/**
	 * This method is used for getting user list by Project Id
	 * @param prjId
	 * @return List UserInfo
	 * @throws SciServiceException
	 */
	public List<UserInfo> UserListByProjectId(int prjId) throws SciServiceException
	{
		List<UserInfo> l_projectUserInfoList = null;
			
		l_projectUserInfoList = ProjectUserManagementHome.getProjectUserList(prjId);
		
		return l_projectUserInfoList;
	}
	
	/**
	 * This method is used for getting Project user list by passing project Id
	 * @param prjId
	 * @return List ProjectUserRole
	 * @throws SciServiceException
	 */
	public List<ProjectUserRole> projectUserListByProjectId(int prjId) throws SciServiceException
	{
		List<ProjectUserRole> l_ProjectUserRoleList=null;
		l_ProjectUserRoleList= ProjectUserManagementHome.getProjectListByProjectId(prjId);
		return l_ProjectUserRoleList;
	}
	
	/**
	 * This method is used for getting UserInfo by passing user Id
	 * @param p_sUserId
	 * @return UserInfo
	 * @throws SciServiceException
	 */
	public static UserInfo retrieveUserBySeqId(int p_sUserId) throws SciServiceException
	{
		UserInfo o_UserInfo = null;
		o_UserInfo= UserHome.retrieveUserBySeqId(p_sUserId);
		return o_UserInfo; 
	}
	
	/**
	 * This method is used for removing user from project
	 * @param u_session
	 * @param p_ProjectId
	 * @param u_UserId
	 * @return String Success Message
	 * @throws SciServiceException
	 */
	public String removeUser(UserSession u_session,int p_ProjectId, int u_UserId) throws SciServiceException
	{
		AuditInfoDb oAuditInfo = null;
		String s_SeccessStatus=null;
		String uesrPrefProjId = null;
		String assignUserPrefProjectId = null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				
				oAuditInfo = AuditHome.createRecordUpdateStub(u_session,this.getPortalAppId(), "User removed from Project", p_ProjectId);
				oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.PROJECT_SEQUENCE_ID, p_ProjectId );
				oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.User_SEQ_ID, u_UserId);
				oAuditInfo.addPayload(ProjectUserAuditHelper.ProjectUserAttributes.User_ID, UserHome.retrieveUserBySeqId(u_UserId).getUserId());
				
				ProjectUserManagementHome.deleteUser(p_ProjectId, u_UserId);
				
				uesrPrefProjId = u_session.getUserPreference(userPreflProjIndependentProjId, UserUtils.PROJECT_MANAGEMENT_PORTAL_APP, UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID);
				
				if(!StringUtils.isNullOrEmpty(uesrPrefProjId))
				{
					if(p_ProjectId == Integer.parseInt(uesrPrefProjId))
					{
						if(ProjectUserManagementHome.getUserListOfProjects(u_UserId).size() == 1)
						{
							assignUserPrefProjectId = ""+ProjectUserManagementHome.getProjectIdByUserSeqId(u_UserId)+"";
						}
						UserHome.saveUserPreference(connection,UserHome.retrieveUserBySeqId(u_UserId) , new UserPreference(userPreflProjIndependentProjId, UserUtils.PROJECT_MANAGEMENT_PORTAL_APP, UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID, assignUserPrefProjectId), false);
					}
				}
				if(ProjectUserManagementHome.getUserListOfProjects(u_UserId).isEmpty())
				{
					UserHome.removeUserPreference(connection, UserHome.retrieveUserBySeqId(u_UserId), UserUtils.PROJECT_INDEPENDENT_ID, UserUtils.PROJECT_MANAGEMENT_PORTAL_APP,UserUtils.PREFERENCE_KEY_MAPPED_PROJECT_ID);
				}	
					
				UserHome.removeUserPrefbyProjId(p_ProjectId, u_UserId);
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				s_SeccessStatus = "User seq id "+u_UserId+" removed successfully";
				connection.commitTransaction();
			} catch(SciException sciExc)
			{
				connection.rollbackTransaction();
				log.error("Error removing project user audit", sciExc);
				throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_REMOVING_PROJECT_USER,sciExc);
			}
		}
		catch(SciDbException sciDbExc)
		{
			log.error("Error removing project user", sciDbExc);
			throw new SciServiceException(ServiceErrorCodes.ProjectUser.ERROR_REMOVING_PROJECT_USER,sciDbExc);
		}
		return s_SeccessStatus;
	}
	
	/**
	 * This method is used for updating ProjConfig
	 * @param p_oUserSession
	 * @param key
	 * @param newValue
	 * @param l_projectid
	 * @return String Success Message
	 * @throws SciServiceException
	 */
	public static  String  updateProjConfig(UserSession p_oUserSession, String key, String newValue, int l_projectid) throws SciServiceException
	{
	
		String s_SeccessStatus=null;
		String Portal_APP = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				ConfigHome.updatePrjConfig(connection, p_oUserSession, Portal_APP, key, newValue, l_projectid);
				s_SeccessStatus = "Property for Project Id "+l_projectid+" updated successfully";
				connection.commitTransaction();
			} catch(SciException sciExc)
			{
				connection.rollbackTransaction();
				log.error("Error updating project config audit", sciExc);
				throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_UPDATING_PROJECTCONFIG,sciExc);
			}
		}
		catch(SciDbException sciDbExc)
		{
			log.error("Error updating project config", sciDbExc);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_UPDATING_PROJECTCONFIG,sciDbExc);
		}
		return s_SeccessStatus;
	}
	
}
