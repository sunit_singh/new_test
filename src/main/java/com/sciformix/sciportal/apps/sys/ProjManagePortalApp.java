package com.sciformix.sciportal.apps.sys;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.project.ProjectAuditHelper;
import com.sciformix.sciportal.project.ProjectHome;
import com.sciformix.sciportal.project.ProjectHomeImpl;
import com.sciformix.sciportal.project.ProjectInfo;
import com.sciformix.sciportal.user.UserSession;

public class ProjManagePortalApp extends BasePortalApp{
	
	private static final Logger log = LoggerFactory.getLogger(ProjManagePortalApp.class);

	public ProjManagePortalApp() {
		super("PROJMNG-PORTAL-APP", "ProjectManagement" ,false, false);
		}
	
	public static ProjectInfo getProjectInfoByProjectId(int p_nProjectId)
	{
		try{
			return ProjectHome.getProject(p_nProjectId);
		}
		catch(SciServiceException sciExcep)
		{
			log.error("Error retriving project", sciExcep);
			return null;
		}
	}
	
	public static List<ProjectInfo> getAllProjects()
	{
		try {
		return ProjectHome.getAllProjects();
		}
		catch(SciServiceException sciExcep)
		{
			log.error("Error retriving list of project", sciExcep);
			return null;
		}
	}
	
	public String saveProject(UserSession p_oUserSession, ProjectInfo p_oProjectInfo,int currentprojectid, boolean p_bInsert) throws SciServiceException 
	{
		AuditInfoDb oAuditInfo = null;
		String succMessage=null;
		//int projectSeqId = 
		try (DbConnection connection = DbConnectionManager.getConnection())
			{
				
				try {
					connection.startTransaction();
					if (p_bInsert)
					{
						ProjectHome.saveProject(p_oProjectInfo, p_oUserSession.getUserInfo(), p_bInsert);
						ProjectHome.saveProjectConfig(p_oProjectInfo, p_oUserSession.getUserInfo());
						oAuditInfo = AuditHome.createRecordInsertStub(p_oUserSession, this.getPortalAppId(), "Project Created", currentprojectid);
						oAuditInfo.addPayload(ProjectAuditHelper.ProjectAttributes.PROJECT_SEQUENCE_ID, p_oProjectInfo.getProjectSequenceId());
						oAuditInfo.addPayload(ProjectAuditHelper.ProjectAttributes.PROJECT_NAME, p_oProjectInfo.getProjectName());
						succMessage="Project created successfully with sequence Id: "+p_oProjectInfo.getProjectSequenceId()+
									" and project name: "+p_oProjectInfo.getProjectName();
					}
					else
					{
						String sProjectExistingName = ProjectHomeImpl.getProjectGivenProjectId(p_oProjectInfo.getProjectSequenceId()).getProjectName();
						String sProjectExistingDescription = ProjectHomeImpl.getProjectGivenProjectId(p_oProjectInfo.getProjectSequenceId()).getProjectDescription();
						
						ProjectHome.saveProject(p_oProjectInfo,  p_oUserSession.getUserInfo(), p_bInsert);
						
							oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Project Updated",currentprojectid);
							oAuditInfo.addPayload(ProjectAuditHelper.ProjectAttributes.PROJECT_SEQUENCE_ID, p_oProjectInfo.getProjectSequenceId());
							oAuditInfo.addPayload(ProjectAuditHelper.ProjectAttributes.PROJECT_NAME, p_oProjectInfo.getProjectName());
							
							if (DataUtils.hasChanged(sProjectExistingName, p_oProjectInfo.getProjectName()))
							{
								oAuditInfo.addDetails(ProjectAuditHelper.ProjectAttributes.PROJECT_NAME, sProjectExistingName, p_oProjectInfo.getProjectName());
							}
							
							if (DataUtils.hasChanged(sProjectExistingDescription, p_oProjectInfo.getProjectDescription()))
							{
								oAuditInfo.addDetails(ProjectAuditHelper.ProjectAttributes.PROJECT_DESCRIPTION, sProjectExistingDescription, p_oProjectInfo.getProjectDescription());
							}
							succMessage="Project seq id: "+p_oProjectInfo.getProjectSequenceId()+" updated successfully" ;
						}
					AuditHome.savePopulatedStub(connection, oAuditInfo);
					connection.commitTransaction();
				} catch(SciException sciExcep)
				{
					//TODO:Change to error codes
					connection.rollbackTransaction();
					log.error("Error saving project", sciExcep);
					throw new SciServiceException(ServiceErrorCodes.Project.ERROR_SAVING_PROJECT,sciExcep);
				}
			}
			catch(SciDbException dbExcep)
			{
				log.error("Error saving project", dbExcep);
				throw new SciServiceException(ServiceErrorCodes.Project.ERROR_SAVING_PROJECT,dbExcep);
			}
			
			//TODO:Following code needs review
			return succMessage;
	}
}

