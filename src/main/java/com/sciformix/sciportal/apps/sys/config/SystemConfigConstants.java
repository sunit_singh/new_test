package com.sciformix.sciportal.apps.sys.config;

public class SystemConfigConstants {
	
	public static final String APP_NAME_KEY = "KEY";
	public static final String CONFIG_KEY_STRING = "encrtyption.key.";
	public static final String CONFIG_KEY_ATTRIBUTE_ID = "id";
	public static final String CONFIG_KEY_ATTRIBUTE_PURPOSE = "purpose";
	public static final String CONFIG_KEY_ATTRIBUTE_TYPE = "type";
	public static final String CONFIG_KEY_ATTRIBUTE_PRIVATE_KEY = "private";
	public static final String CONFIG_KEY_ATTRIBUTE_PUBLIC_KEY = "public";
}
