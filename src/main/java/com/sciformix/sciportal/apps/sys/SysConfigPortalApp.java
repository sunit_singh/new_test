package com.sciformix.sciportal.apps.sys;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.apps.sys.config.EncryptionKey;
import com.sciformix.sciportal.apps.sys.config.EncryptionKeyPairGenerator;
import com.sciformix.sciportal.apps.sys.config.SystemConfigConstants;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.integration.ServerConnection;
import com.sciformix.sciportal.integration.ServerConnectionHome;
import com.sciformix.sciportal.system.SystemInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.LDAPUtils;

public class SysConfigPortalApp extends BasePortalApp {
	
	private static String encryptedKey = null;
	private static String decryptedKey = null;
	private static KeyFactory keyFactory = null;
	private static EncodedKeySpec KeySpec = null;
	private static PrivateKey privateKey = null;
	private static PublicKey  publicKey = null;
	
	private static final Logger log = LoggerFactory.getLogger(SysConfigPortalApp.class);
	
	public SysConfigPortalApp()
	{
		super("SYSCONFIG-PORTAL-APP", "SystemConfigurationApp", false, false);
	}

	public List<ServerConnection> getAllConnections()
	{
		return ServerConnectionHome.getAllConnections();
	}
	
	public ServerConnection createServerConnection(ServerConnection.STATE p_eState, ServerConnection.TYPE p_eType, String p_sAlias, String... p_sarrParameters)//Attributes
	{
		return ServerConnectionHome.createServerConnection(p_eState, p_eType, p_sAlias, p_sarrParameters);
	}

	public SystemInfo getSystemInfo()
	{
		return SystemInfo.systemInfo;
	}
	
	public ServerConnection updateServerConnection(String conn_Id, ServerConnection.STATE p_eState, String p_sAlias, String... p_sarrParameters)//Attributes
	{
		return ServerConnectionHome.updateServerConnection(conn_Id, p_eState, p_sAlias, p_sarrParameters);
	}
	
	public void generateEncryptionKey(UserSession o_userSession, EncryptionKey  encryptionKey) throws SciServiceException
	{
		String keyIdConfigKey = null;
		String keyTypeConfigKey = null;
		String keyPurposeConfigKey = null;
		String privateKeyConfigKey = null;
		String publicKeyConfigKey = null;
		
		
		EncryptionKeyPairGenerator encriptionKeyPairGenerator = null ;
		try {
			encriptionKeyPairGenerator = new  EncryptionKeyPairGenerator(1024);
		} catch (NoSuchAlgorithmException | NoSuchProviderException noSuchProviderEx) {
			log.error("Error Generate encryption key pair", noSuchProviderEx);
			throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_PAIR_GENERATION);
		}
		encriptionKeyPairGenerator.createKeys();
		encryptionKey.setPublicKey(encriptionKeyPairGenerator.getPublicKey());
		encryptionKey.setPrivateKey(encriptionKeyPairGenerator.getPrivateKey());
				
		String privateKey = Base64.getEncoder().encodeToString(encryptionKey.getPrivateKey().getEncoded());
		String publicKey = Base64.getEncoder().encodeToString(encryptionKey.getPublicKey().getEncoded());
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				keyIdConfigKey = generateCommonConfigKey(encryptionKey.getKeyId(),SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_ID );
				
				ConfigHome.updateSysConfig(connection, o_userSession, SystemConfigConstants.APP_NAME_KEY, keyIdConfigKey, encryptionKey.getKeyId());
				
				keyTypeConfigKey = generateCommonConfigKey(encryptionKey.getKeyId(),SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_TYPE );
				ConfigHome.updateSysConfig(connection, o_userSession, SystemConfigConstants.APP_NAME_KEY, keyTypeConfigKey, encryptionKey.getKeyType());
				
				keyPurposeConfigKey = generateCommonConfigKey(encryptionKey.getKeyId(), SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_PURPOSE);
				ConfigHome.updateSysConfig(connection, o_userSession, SystemConfigConstants.APP_NAME_KEY, 
														keyPurposeConfigKey, encryptionKey.getKeyPurpose());
				
				privateKeyConfigKey = generateCommonConfigKey(encryptionKey.getKeyId(), SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_PRIVATE_KEY);
				ConfigHome.updateSysConfig(connection, o_userSession, SystemConfigConstants.APP_NAME_KEY,
								privateKeyConfigKey, LDAPUtils.encrypt(privateKey));
				
				publicKeyConfigKey = generateCommonConfigKey(encryptionKey.getKeyId(), SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_PUBLIC_KEY);
				ConfigHome.updateSysConfig(connection, o_userSession, SystemConfigConstants.APP_NAME_KEY,
											publicKeyConfigKey, LDAPUtils.encrypt(publicKey));
				
				connection.commitTransaction();
			} catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error Generate encryption key pair", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_PAIR_GENERATION);
			}
		} 
		catch(SciDbException dbExcep)
		{
			log.error("Error Generate encryption key pair", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_PAIR_GENERATION);
		}
	}
	public static String generateCommonConfigKey(String keyId, String keyAttribute)
	{
		return SystemConfigConstants.CONFIG_KEY_STRING + keyId + "." + keyAttribute; 
	}
	
	public  static PrivateKey  getPrivateKey(String keyId) throws SciServiceException
	{
		encryptedKey = null;
		privateKey = null;
		encryptedKey = ConfigHome.getSysConfigSetting(generateCommonConfigKey( keyId , SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_PRIVATE_KEY)); 
		
		if(!StringUtils.isNullOrEmpty(encryptedKey))
		{
			try{
				decryptedKey =   LDAPUtils.decrypt(encryptedKey);  	
			}catch (SciException sceEx)
			{
				log.error("Error Key Decryption");
				throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_DECRYPTION);
			}
			
			byte[] privateKeyBytes = Base64.getDecoder().decode(decryptedKey);
		    
			try {
		    keyFactory = KeyFactory.getInstance("RSA");
		    KeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
		    privateKey = keyFactory.generatePrivate(KeySpec);
		    }
			catch( InvalidKeySpecException  |NoSuchAlgorithmException invlidKeyExc) 
			{
				log.error("Error Key Decryption");
				throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_DECRYPTION);
			}
			return  privateKey;
		}
		return privateKey;
	}
	
	public static PublicKey  getPublicKey(String keyId) throws SciServiceException
	{
		encryptedKey = null;
		publicKey = null;
		decryptedKey = null;
		encryptedKey = ConfigHome.getSysConfigSetting(generateCommonConfigKey( keyId , SystemConfigConstants.CONFIG_KEY_ATTRIBUTE_PUBLIC_KEY)); 
		if(!StringUtils.isNullOrEmpty(encryptedKey))
		{
			try{
				decryptedKey =   LDAPUtils.decrypt(encryptedKey);  	
			}catch (SciException sceEx)
			{
				log.error("Error Key Decryption");
				throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_DECRYPTION);
			}
			
			byte[] publicKeyBytes = Base64.getDecoder().decode(decryptedKey);
		    
			try {
		    keyFactory = KeyFactory.getInstance("RSA");
		    KeySpec = new X509EncodedKeySpec(publicKeyBytes);
		    publicKey = keyFactory.generatePublic(KeySpec);
		    }
			catch( InvalidKeySpecException  |NoSuchAlgorithmException invlidKeyExc) 
			{
				log.error("Error Key Decryption");
				throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_DECRYPTION);
			}
			return publicKey;
		}
	return publicKey;
	}
	
	public String getDecryptedKey(String keyId, String attribute ) throws SciServiceException
	{
		encryptedKey = null;
		decryptedKey = null;
		encryptedKey = ConfigHome.getSysConfigSetting( generateCommonConfigKey( keyId , attribute)); 
		if(!StringUtils.isNullOrEmpty(encryptedKey))
		{
			try{
				decryptedKey =   LDAPUtils.decrypt(encryptedKey);  	
			}catch (SciException sceEx)
			{
				log.error("Error Key Decryption");
				throw new SciServiceException(ServiceErrorCodes.ServerConfig.ERROR_KEY_DECRYPTION);
			}
			return  decryptedKey;
		}
	return decryptedKey;
	}
	
}