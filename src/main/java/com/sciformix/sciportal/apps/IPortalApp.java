package com.sciformix.sciportal.apps;

import java.util.List;
import java.util.Map;

import com.sciformix.sciportal.user.UserInfo;

public interface IPortalApp
{
	
	public String getPortalAppId();
	
	public String getPortalAppName();
	
	public boolean allowsUserApplicationDisable();
	
	public boolean requiresUserPreferences();

	public List<AppUserPreferenceMetadata> getAppUserPreference();
	
	public boolean applicationProjectSpecific();
	
//	public List<UserPreference> getAppSpecificUserPreferences(UserInfo p_oUserInfo);
	
	public boolean isApplicationEnabledForUser(UserInfo p_oUserInfo, int p_selectedProjectSeqId);
	
	public Map<String, String> getAppConfigSettings();
	
	@SuppressWarnings("rawtypes")
	public String getAppConfigSetting(Enum p_eKey);
}
