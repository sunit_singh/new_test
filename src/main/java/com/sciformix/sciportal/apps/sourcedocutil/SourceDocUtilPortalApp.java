package com.sciformix.sciportal.apps.sourcedocutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.WebErrorCodes;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.file.IFileParser;
import com.sciformix.commons.file.IFileSplitter;
import com.sciformix.commons.file.annotator.PDFAnnotator;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarAA;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarAB;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarBA;
import com.sciformix.commons.file.splitters.PdfSplitterAvatarAA;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.ParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.web.PortalAppResponse;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class SourceDocUtilPortalApp extends BasePortalApp
{
	private static final String EXPORT_EXCEL_TEMPLATE_XLS = "/exportExcelTemplate.xls";
	private static final String EXPORT_EXCEL_TEMPLATE_XLS_AB = "/exportExcelTemplateAB.xls";


	private static final Logger log = LoggerFactory.getLogger(SourceDocUtilPortalApp.class);
	
	
	private static final String SECTION_HEADING_A_1_SAFETYREPORT = "A.1 Safety Report";
	private static final String SECTION_HEADING_B_1_PATIENT = "B.1 Patient";
	private static final String SECTION_HEADING_B_2_REACTION = "B.2 Reaction";
	
	
	private static final String SECTION_HEADING_B_1_8_PAST_DRUG_THERAPY = "B.1.8 Past drug therapy";
	private static final String SECTION_HEADING_B_1_9_2_PATIENT_DEATH_CAUSE = "B.1.9.2 Patient death cause";
	private static final String SECTION_HEADING_B_1_10_PARENT = "B.1.10 Parent";
	private static final String SECTION_HEADING_B_1_10_8_PARENT_DRUG_THERAPY = "B.1.10.8 Parent drug therapy";
	private static final String SECTION_HEADING_B_4_K_18_DRUG_REACTION_RELATEDNESS = "B.4.k.18 Drug Reaction relatedness";
	private static final String SECTION_HEADING_B_3_TEST = "B.3 Test";
	private static final String SECTION_HEADING_B_4_K_2_2_ACTIVE_SUBSTANCE = "B.4.k.2.2 Active Substance";
	private static final String SECTION_HEADING_B_5_SUMMARY = "B.5 Summary";
	private static final String SECTION_HEADING_B_4_K_17_DRUG_RECURRENCE = "B.4.k.17 Drug Recurrence";
	private static final String SECTION_HEADING_B_4_DRUG = "B.4 Drug";
	private static final String SECTION_HEADING_B_1_10_7_PARENT_MEDICAL_HISTORY = "B.1.10.7 Parent medical history";
	private static final String SECTION_HEADING_B_1_9_4_PATIENT_AUTOPSY = "B.1.9.4 Patient autopsy";
	private static final String SECTION_HEADING_B_1_9_PATIENT_DEATH = "B.1.9 Patient death";
	private static final String SECTION_HEADING_B_1_7_PATIENT_MEDICAL_HISTORY = "B.1.7 Patient medical history";
	private static final String SECTION_HEADING_A_3_2_RECEIVER = "A.3.2 Receiver";
	private static final String SECTION_HEADING_A_3_1_SENDER = "A.3.1 Sender";
	private static final String SECTION_HEADING_A_2_PRIMARY_SOURCE = "A.2 Primary Source";
	
	private static final String DOT_SPLITTER = "\\.";
	public SourceDocUtilPortalApp() 
	{
		super("SOURCE_DOC_UTILS_PORTAL_APP", "SourceDocUtilPoratalApp", false, false);
	}
	
	public static enum AppConfig
	{
		SOURCE_DOC_SPLITTER("SOURCE_DOC_UTILS_PORTAL_APP.splitter.sourceDocumentSplitter"),
		SDPARSER_FILETYPE_LIST("SOURCE_DOC_UTILS_PORTAL_APP.parser.sdParserFileTypeList"),
		HIGHLIGHTER_PATTERN("SOURCE_DOC_UTILS_PORTAL_APP.highlighter.pattern"),
		HEADER_VALUES_FOR_SPLITTER("SOURCE_DOC_UTILS_PORTAL_APP.splitter.sdHeaderList"),
		ANNOTATION_STYLE("SOURCE_DOC_UTILS_PORTAL_APP.highlighter.annotationStyle"),
		KEY_HEADER_LIST_PARSER_AA_IDENTIFICATION("SOURCE_DOC_UTILS_PORTAL_APP.parser.keyHeaderListForAA"),
		KEY_HEADER_LIST_PARSER_AB_IDENTIFICATION("SOURCE_DOC_UTILS_PORTAL_APP.parser.keyHeaderListForAB"),
		KEY_HEADER_LIST_PARSER_BA_IDENTIFICATION("SOURCE_DOC_UTILS_PORTAL_APP.parser.keyHeaderListForBA")
		;
		private String value;
		
		AppConfig(String value)
		{
			this.value = value;
		}
		
		public String toString()
		{
			return this.value;
		}
		
		public static AppConfig toEnum(int value)
		{
			AppConfig[] values = AppConfig.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (values[counter].toString().equals(value))
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("AppConfig: Illegal enum value - " + value);
		}
	}
	public PortalAppResponse downloadFile(String sZipFilePath, HttpServletResponse response, PortalAppResponse oAppResponse)
	{
		File oDownloadFile=null;
		PrintWriter oExportFileWriter=null;
		FileInputStream oFileInputStream = null;
		try{
		oDownloadFile = new File(sZipFilePath);
		if(oDownloadFile.exists())
		{
			oExportFileWriter = response.getWriter();  
			response.setContentType("APPLICATION/OCTET-STREAM");  
			response.setHeader("Content-Disposition","attachment; filename=\"" + oDownloadFile.getName() + "\"");
			oFileInputStream = new FileInputStream(oDownloadFile);
			int nNum;   
			while ((nNum = oFileInputStream.read()) != -1) 
			{  
				oExportFileWriter.write(nNum);   
			}
			oAppResponse.setIsDownloadStreamPresent(true);
			response.flushBuffer();
		    
			if(oExportFileWriter != null)
		   	{
		    	oExportFileWriter.close();
		   	}
		}else
		{
			oAppResponse.setIsDownloadStreamPresent(false);
			log.debug("Error during downloding < "+sZipFilePath+" >");
		    oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_ALREADY_DOWNLOADED);   
			return oAppResponse;
		}
		}catch(Exception excep)
		{
			oAppResponse.setIsDownloadStreamPresent(false);
			log.error("Error during downloding < "+sZipFilePath+" >", excep);
		    oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_FILE_ALREADY_DOWNLOADED);   
			return oAppResponse;
		}
		finally
		{
			try 
			{
				
				
			    if(oFileInputStream != null)
			   	{
			    	oFileInputStream.close();
			   	}
			 //   WebUtils.cleanUserTempFolder(oUserSession);
			}
			catch (Exception excep) 
			{
				log.error("Error during file downloading", excep);
				oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_IN_FILE_DOWNLOADING);   
				return oAppResponse;
			}
		}
		return oAppResponse;
	}
	public String splitAndZip(List<UploadedFileWrapper> listUploadedFileObject, String userPath) throws Exception
	{
		String sOutputFolderpath=null;
		String sZipFilePath=null;
		String[] headerValues = this.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.HEADER_VALUES_FOR_SPLITTER).split(",") ;
		IFileSplitter splitter=new PdfSplitterAvatarAA();
		UploadedFileWrapper oUploadedFileObject=null;
		
		for(int i = 0;i < listUploadedFileObject.size(); i++)
		{	
			oUploadedFileObject = listUploadedFileObject.get(i);
			if (oUploadedFileObject.isFileAvailable())
			{
		
				try
				{
					
					
				sOutputFolderpath = splitter.splitFile(oUploadedFileObject.getStagedFilePath(),headerValues, userPath);
				sZipFilePath = sOutputFolderpath+".zip";
				this.zipDirectory(new File(sOutputFolderpath), sZipFilePath);
				}
				catch(Exception ex)
				{
					log.error("Error while splitting file in the location"+sOutputFolderpath+ex.getMessage());
				}
				finally
				{
					FileUtils.forceDelete(new File(sOutputFolderpath));
					
				}
				
			}
				
			
		}	
		return sZipFilePath; 
	}
	
	
	public void zipDirectory(File oFolderPath, String p_sZipFolderPath)  throws SciServiceException,SciException
	{
		  FileOutputStream oFileOutputStream = null;
		  ZipOutputStream  oZipOutputStream = null;
		  FileInputStream  oFileInputStream = null;
		  File[] oFile = null;
		  List<File> listFiles = null;
		  String sFilePath = null;
        try 
        {
        	oFile = oFolderPath.listFiles();
        	listFiles = new ArrayList<>();
        	Collections.addAll(listFiles, oFile);
            //populateFilesList(oFolderPath);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
             oFileOutputStream = new FileOutputStream(p_sZipFolderPath);
             oZipOutputStream = new ZipOutputStream(oFileOutputStream);
            for(int nIndex = 0 ; nIndex < listFiles.size(); nIndex++)
            {
            	sFilePath = listFiles.get(nIndex).getAbsolutePath();
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry oZipEntry = new ZipEntry(sFilePath.substring(oFolderPath.getAbsolutePath().length()+1, sFilePath.length()));
                oZipOutputStream.putNextEntry(oZipEntry);
                //read the file and write to ZipOutputStream
                oFileInputStream = new FileInputStream(sFilePath);
                byte[] buffer = new byte[(int) folderSize(oFolderPath)];
                int len;
                while ((len = oFileInputStream.read(buffer)) > 0) {
                    oZipOutputStream.write(buffer, 0, len);
                }
                oZipOutputStream.closeEntry();
                oFileInputStream.close();
            }
           
            /*if(oFolderPath!=null && oFolderPath.isDirectory())
            {
            	org.apache.commons.io.FileUtils.deleteDirectory(oFolderPath);
            }*/
            
        } catch (IOException excep) 
        {
        	 log.error("Error while downlaoding file",  excep);
        	 throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_DOWNLOADING_FILE, excep);
        }finally 
        {
        	
        	 try 
        	 {
        		 
        		if(oZipOutputStream!=null) 
        		{
        			oZipOutputStream.flush();
        			oZipOutputStream.close();
        		}
        		if(oFileOutputStream!=null)
        		{
        			 oFileOutputStream.flush();
                     oFileOutputStream.close();
        		}
        		if(oFolderPath.listFiles().length>0)
       		 {
       			 oFolderPath.delete();
       		 }
        	 } catch (IOException excep) 
        	 {
        		 log.error("Error while downlaoding file",  excep);
        		 throw new SciServiceException(ServiceErrorCodes.SourceDocUtil.ERROR_DOWNLOADING_FILE, excep);
        	 }
            
		}
    }
	public static long folderSize(File p_oFolderPath) 
	{
	    long length = 0;
	    for (File file : p_oFolderPath.listFiles()) {
	        if (file.isFile())
	            length += file.length();
	        else
	            length += folderSize(file);
	    }
	    return length;
	}
	public String getTextFromFtl(String path)
	{
		StringBuilder builder=new StringBuilder();
		String sCurrentLine=null;
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			
			while ((sCurrentLine = br.readLine()) != null) {
				builder.append(sCurrentLine);
			//	builder.append("\n");
				
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}
	public Map<String, Object> parsePdf(List<UploadedFileWrapper> listUploadedFileObject, ParsedDataTable oDataTablePageContents, String parseFileType) throws SciServiceException, SciException
	{
		IFileParser oParser = null;
		Map<String,Object> objectMap = null;
		String[] keyHeaderList = null;
		Map<String, String> fileList = new HashMap<String, String>();
		UploadedFileWrapper oUploadedFileObject=null;
		List<ParsedDataTable> listOfParsedDataTables = new ArrayList<ParsedDataTable>();
		
		boolean bTestMode = false;
		bTestMode = Boolean.parseBoolean(ConfigHome.getAppConfigSetting(super.getPortalAppId() + ".parserTestMode." + parseFileType));
		

		for(int i = 0;i < listUploadedFileObject.size(); i++)
		{	
			oUploadedFileObject = listUploadedFileObject.get(i);
			if (oUploadedFileObject.isFileAvailable())
			{
				if(parseFileType.equalsIgnoreCase("Health Canada"))
				{
				oParser = new PdfDocParserAvatarBA(oUploadedFileObject.getStagedFilePath());
				keyHeaderList = this.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.KEY_HEADER_LIST_PARSER_BA_IDENTIFICATION).split(",");
				oParser.setKeyHeaderList(keyHeaderList);
				oDataTablePageContents = oParser.parse();
				
				
				//mapping keys has been shifter here because of splitter issue
				oDataTablePageContents = oParser.MapAutomatedKeysToMnemonics(oDataTablePageContents);
				
				oDataTablePageContents.addColumn("fileType", "File Type", new StringParsedDataValue("Health Canada","Health Canada",DataState.ASIS,ValidationState.UNVALIDATED));
				oDataTablePageContents.addColumn("FileName", "File Name", new StringParsedDataValue(oUploadedFileObject.getStagedFileName(),oUploadedFileObject.getStagedFileName(),DataState.ASIS,ValidationState.UNVALIDATED));
				fileList.put(oUploadedFileObject.getStagedFileName(), oDataTablePageContents.getColumnValues("ReportRuntime").get(0).displayValue().toString());
				listOfParsedDataTables.add(oDataTablePageContents);

				}
				else if(parseFileType.equalsIgnoreCase("E2B"))
				{
					oParser = new PdfDocParserAvatarAA(oUploadedFileObject.getStagedFilePath(), bTestMode);
					keyHeaderList = this.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.KEY_HEADER_LIST_PARSER_AA_IDENTIFICATION).split(",");
					oParser.setKeyHeaderList(keyHeaderList);
					oDataTablePageContents = oParser.parse();
					oDataTablePageContents.addColumn("fileType", "File Type", new StringParsedDataValue("E2B","E2B",DataState.ASIS,ValidationState.UNVALIDATED));	
					oDataTablePageContents.addColumn("FileName", "File Name", new StringParsedDataValue(oUploadedFileObject.getStagedFileName(),oUploadedFileObject.getStagedFileName(),DataState.ASIS,ValidationState.UNVALIDATED));
					fileList.put(oUploadedFileObject.getStagedFileName(), oDataTablePageContents.getColumnValues("TransmissionDate").get(0).displayValue().toString());
					listOfParsedDataTables.add(oDataTablePageContents);
				}
				else if(parseFileType.equalsIgnoreCase("MESA"))
				{
					oParser = new PdfDocParserAvatarAB(oUploadedFileObject.getStagedFilePath(), bTestMode);
					keyHeaderList = this.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.KEY_HEADER_LIST_PARSER_AB_IDENTIFICATION).split(",");
					oParser.setKeyHeaderList(keyHeaderList);
					oDataTablePageContents = oParser.parse();
					
					oDataTablePageContents.addColumn("fileType", "File Type", new StringParsedDataValue("MESA","MESA",DataState.ASIS,ValidationState.UNVALIDATED));
					oDataTablePageContents.addColumn("FileName", "File Name", new StringParsedDataValue(oUploadedFileObject.getStagedFileName(),oUploadedFileObject.getStagedFileName(),DataState.ASIS,ValidationState.UNVALIDATED));
					fileList.put(oUploadedFileObject.getStagedFileName(), oDataTablePageContents.getColumnValues("General_LatestReceivedDate").get(0).displayValue().toString());
					listOfParsedDataTables.add(oDataTablePageContents);
				}
			}
				
			
		}
		//sorting the dataTable list by Transmission date
		if(parseFileType.equalsIgnoreCase("E2B"))
		Collections.sort(listOfParsedDataTables,new DateComparatorForAA());
		else if(parseFileType.equalsIgnoreCase("MESA"))
			Collections.sort(listOfParsedDataTables,new DateComparatorForAB());
		
		//sorting the file list by date
	//	Collections.sort(listOfParsedDataTables,new DateComparator());
		//handle multiple dataTables
		Map<String, List<String>> keyValueArrayMap = oParser.compareMultipleDataTable(listOfParsedDataTables);
		//return text;
		objectMap = new HashMap<String, Object>();
		objectMap.put("dataTableList", listOfParsedDataTables);
		objectMap.put("keyMap", keyValueArrayMap);
		return objectMap;
	}
	
	public static class DateComparatorForAA implements Comparator<ParsedDataTable> {
	    public int compare(ParsedDataTable oDataTable1, ParsedDataTable oDataTable2) {
	    	String transmissionDateString1= oDataTable1.getColumnValues("TransmissionDate").get(0).displayValue().toString();
	    	String transmissionDateString2= oDataTable2.getColumnValues("TransmissionDate").get(0).displayValue().toString();
	    	String messageDateTimeString1= oDataTable1.getColumnValues("MessageDate").get(0).displayValue().toString();
	    	String messageDateTimeString2= oDataTable2.getColumnValues("MessageDate").get(0).displayValue().toString();
	    	Date transmissionDate1 = null;
	    	Date transmissionDate2= null;
	    	Date messageDateTime1 = null;
	    	Date messageDateTime2= null;
			try {
				transmissionDate1 = new SimpleDateFormat("dd-MMM-yyyy").parse(transmissionDateString1);
				transmissionDate2 =new SimpleDateFormat("dd-MMM-yyyy").parse(transmissionDateString2);
				messageDateTime1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(messageDateTimeString1);
				messageDateTime2 =new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(messageDateTimeString2);
			} catch (ParseException e) {
				log.error("Error while parsing date"+e.getMessage());
			}
	    	
	    	if (transmissionDate1.before(transmissionDate2)) {
	            return -1;
	        } else if (transmissionDate1.after(transmissionDate2)) {
	            return 1;
	        } else {
	        	

	        	if (messageDateTime1.before(messageDateTime2)) {
			            return -1;
			        } else if (messageDateTime1.after(messageDateTime2)) {
			            return 1;
			        } else {
			        	return 0;
			        }
	        	
//	            return 0;
	        }        
	    }
	}
	
	public static class DateComparatorForAB implements Comparator<ParsedDataTable> {
	    public int compare(ParsedDataTable oDataTable1, ParsedDataTable oDataTable2) {
	    	String dateString1= oDataTable1.getColumnValues("General_Header_GeneratedOn").get(0).displayValue().toString();
	    	String dateString2= oDataTable2.getColumnValues("General_Header_GeneratedOn").get(0).displayValue().toString();
	    	Date date1 = null;
	    	Date date2= null;
			try {
				date1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(dateString1);
				date2 =new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(dateString2);
			} catch (ParseException e) {
				log.error("Error while parsing date"+e.getMessage());
			}
	    	
	    			
	    	
	    	if (date1.before(date2)) {
	            return -1;
	        } else if (date1.after(date2)) {
	            return 1;
	        } else {
	            return 0;
	        }        
	    }
	} 
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public StringBuffer setTextForAnnotedData(Map<String, Object> annotedMap)
	{
		StringBuffer text=new StringBuffer();
		Map<String, Map> innerAnnotedMap=null;
		List<String> textList=null;
		String htmlPrefix=null;
		
		htmlPrefix="<html><head></head><h4>Annoted Data</h4>"
				+"<table style='border:0px;cellpadding:0;border-spacing:0;font-family:Arial;font-size:12;background-color:#F5F6F7;width:530px;'>"
				+"<th style='width:50%;word-wrap:word-break;'>Field Type</th><th>Page No</th><th style='word-wrap:word-break;'>Annoted Value</th><th style='width:2%;word-wrap:word-break;'>&nbsp;</th>";
		text.append(htmlPrefix);
		
		
		
		for(String key:annotedMap.keySet())
		{
			innerAnnotedMap=(Map<String, Map>)annotedMap.get(key);
			text.append("<tr><td>"+key+"</td><td></td><td></td></tr>");
			for(String innerKey:innerAnnotedMap.keySet())
			{
				textList=(List<String>)innerAnnotedMap.get(innerKey);
				text.append("<tr><td></td><td>"+innerKey+"</td><td></td></tr>");
				for(String annotedString: textList)
				{
					text.append("<tr><td></td><td></td><td>"+annotedString+"</td></tr>");
				}
			}
			
		}
		
		text.append("</table></head></html>");
		return text;
	}
	public Map<String, Object> highlightPdf(List<UploadedFileWrapper> listUploadedFileObject, String userPath) throws Exception
	{
		
		String pattern=null;
		Map<String, Object> keyMap=null;
		UploadedFileWrapper oUploadedFileObject=null;
		
		for(int i = 0;i < listUploadedFileObject.size(); i++)
		{	
			oUploadedFileObject = listUploadedFileObject.get(i);
			if (oUploadedFileObject.isFileAvailable())
			{
		
				pattern=this.getAppConfigSetting(SourceDocUtilPortalApp.AppConfig.HIGHLIGHTER_PATTERN);
				
				PDFAnnotator opPdfHighter=new PDFAnnotator();
				keyMap=opPdfHighter.setHighlighter(oUploadedFileObject.getStagedFilePath(), userPath, pattern);
			}
				
			
		}	
				return keyMap;
	}
	
	public StringBuffer setData(ParsedDataTable oDataTable)
	{
		int nTotalFields=0;
		int nMissingFieldValue=0;
		String sKey=null, sValue=null;
		String sTrimValue=null;
		
		StringBuffer text=new StringBuffer();
		nTotalFields = oDataTable.colCount();
		nMissingFieldValue=setDataCount(nTotalFields, oDataTable);
		text.append(getHtmlPrefix(nTotalFields, nMissingFieldValue));
	
		for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
		{
		 sKey = oDataTable.getMnemonic(nIndex);
		 sTrimValue = sKey.replaceAll(StringConstants.SPACE, StringConstants.EMPTY);
		 if(sTrimValue.contains(StringConstants.PERIOD) && !sTrimValue.contains("product") && (sKey.indexOf(StringConstants.PERIOD)==sKey.lastIndexOf(StringConstants.PERIOD)) )
		 sTrimValue=sTrimValue.split(DOT_SPLITTER)[1];
		 String temp= StringConstants.EMPTY + oDataTable.getColumnValues(nIndex).get(0).displayValue();
		 sValue=temp.trim();
		 sTrimValue=sTrimValue.replace(StringConstants.CARRIAGERETURN, StringConstants.EMPTY);
		 if(sValue.equals(StringConstants.COLON))
			 sValue=StringConstants.EMPTY;
		 sTrimValue = sTrimValue.toLowerCase().trim();
		
		 if(((sTrimValue.length()>=7 && sTrimValue.length()<=9) && (sTrimValue.startsWith("report") || sTrimValue.startsWith("product")))
				 || sTrimValue.equals("reportinformation") || sTrimValue.equals("patientinformation")||sTrimValue.equals("link/duplicatereportinformation")||sTrimValue.equals("productinformation")||sTrimValue.equals("adversereactionterminformation"))
		 {
			 if(sKey.contains(StringConstants.PERIOD))
			 {
				sKey = sKey.split(DOT_SPLITTER)[1];
			 }
			 
			 setHeaderStyle(text, sKey);
			 
		 }else
		 { 
			if(sKey.contains(StringConstants.PERIOD))
			 {
				if(sKey.indexOf(StringConstants.PERIOD)!=sKey.lastIndexOf(StringConstants.PERIOD) && (sKey.contains("product") || sKey.contains("event") || sKey.contains("Link_Duplicate_Record")))
					sKey = sKey.split(DOT_SPLITTER)[2];
				else
					sKey = sKey.split(DOT_SPLITTER)[1];
			 }
			
			 setKeyAndValue(text, sKey, sValue);
			 setColorCodeForAvailability(sValue, text);
		 }
		
	}
//	}

	text.append("<tr><td colspan='3'>&nbsp;</td></tr></table></html>");	
	return text;
	}
	
	
	
	//Messa parser display Start
	public static StringBuffer setDataForMessa(ParsedDataTable oDataTable)
	{

		int nTotalFields=0;
		int nMissingFieldValue=0;
		String sKey=null, sValue=null;
		String sTrimValue=null;
		
		StringBuffer text=new StringBuffer();
		nTotalFields = oDataTable.colCount();
		nMissingFieldValue=setDataCount(nTotalFields, oDataTable);
//		text.append(getHtmlPrefix(nTotalFields, nMissingFieldValue));
		
			text.append(getHtmlPrefix(nTotalFields, nMissingFieldValue));

			for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
			{
				 sKey = oDataTable.getMnemonic(nIndex);
				 
				 
				 sTrimValue = oDataTable.getMnemonic(nIndex).replaceAll(" ", "");
				 if(sTrimValue.contains("."))
				 sTrimValue=sTrimValue.split(DOT_SPLITTER)[sTrimValue.split(DOT_SPLITTER).length-1];
				 String temp= "" + oDataTable.getColumnValues(nIndex).get(oDataTable.getColumnValues(nIndex).size()-1).displayValue();
				 //String temp= "" + oDataTable.getColumnValues(nIndex).get(oDataTable.getColumnValues(nIndex).size()-1);
				 sValue=temp.trim();
				 sTrimValue=sTrimValue.replace("\r", "");
				 
				 if(sTrimValue.equals("Suspect"))
				 continue;
				 if(sValue.equals(":"))
					 sValue="";
				 sTrimValue = sTrimValue.toLowerCase().trim();
				if(sKey.contains("Narrative"))
				{
					
				}
				 
				 if(((sTrimValue.equals("general") || sTrimValue.equals("generalinformation")))
						 || sTrimValue.equals("seriousnesscriteria") || sTrimValue.contains("reporter(")
						 || sTrimValue.equals("patient")||sTrimValue.contains("drug(")||(sTrimValue.contains("drugdosages(") && sTrimValue.contains(")"))
						 || sTrimValue.contains("indicationsforuse(") || sTrimValue.contains("lotnumbers(") 
						 || sTrimValue.equals("concomitantmedications") || sTrimValue.contains("adverseevent(")
						 || sTrimValue.equals("qa") || sTrimValue.equals("protocol")
						 || sTrimValue.equals("standardintakequeries") || sTrimValue.equals("causality/derechallenge")
						 || sTrimValue.contains("causality/derechallenge(") 
						 || sTrimValue.contains("causality/derechallengeperae(") || sTrimValue.equals("study")
						 || sTrimValue.equals("studyinformation") 
						 || sTrimValue.equals("pregnancy")
						 || sTrimValue.equals("previouspregnancy") || sTrimValue.equals("currentpregnancy") 
						 || sTrimValue.contains("currentpregnancyoutcomes(")
						 || sTrimValue.equals("literature") 
						 || sTrimValue.contains("literature(") || sTrimValue.contains("authors(") 
						 || sTrimValue.equals("narrative(max20,000characters)")
						 || sTrimValue.equals("drugallergies/history")
						 || sTrimValue.equals("drugallergies") || sTrimValue.contains("drughistory(")
						 || sTrimValue.equals("medicalhistory")
						 || sTrimValue.equals("nondrugallergiesandsocialhistory") || sTrimValue.contains("medicalhistory(")
						 || sTrimValue.equals("diagnosticdata") 
						 || sTrimValue.contains("diagnostictests(") || sTrimValue.contains("diagnosticresults(")
						 || sTrimValue.equals("otherdiagnosticdata")
						 || sTrimValue.equals("additionalref") || sTrimValue.contains("additionalreference(")
						 || sTrimValue.equals("device"))
				 {
					 if(sKey.contains(StringConstants.PERIOD))
					 {
						sKey = sKey.split(DOT_SPLITTER)[sKey.split(DOT_SPLITTER).length-1];
					 }
					 
					 setHeaderStyle(text, sKey);
				 }else
				 { 
					if(sKey.contains(StringConstants.PERIOD))
					 {
							sKey = sKey.split(DOT_SPLITTER)[sKey.split(DOT_SPLITTER).length-1];
					 }
					
						setKeyAndValue(text, sKey, sValue);
						setColorCodeForAvailability(sValue, text);
				 }
				
			}
	//	}
		
		text.append("<tr><td colspan='3'>&nbsp;</td></tr></table></html>");
		return text;
	}
	//Messa parser display ends
	
	
	public static StringBuffer setDataForE2B(ParsedDataTable oDataTable)
	{
		int nTotalFields=0;
		int nMissingFieldValue=0;
		String temp=null;
		String sKey=null, sValue=null;
		StringBuffer text=new StringBuffer();
		nTotalFields = oDataTable.colCount();
		nMissingFieldValue=setDataCount(nTotalFields,  oDataTable);
		text.append(getHtmlPrefix(nTotalFields, nMissingFieldValue));
	
	for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
	{
		 sKey = oDataTable.getMnemonic(nIndex);
		 if(sKey==null || sKey.equals(StringConstants.EMPTY))
			 continue;
		// else if(sKey.contains(StringConstants.PERIOD) && (sKey.indexOf(StringConstants.PERIOD)!=sKey.lastIndexOf(StringConstants.PERIOD)) && !sKey.endsWith(StringConstants.PERIOD))
		 /*else if(sKey.contains(StringConstants.PERIOD) && (sKey.indexOf(StringConstants.PERIOD)!=sKey.lastIndexOf(StringConstants.PERIOD)))
		 {
			 String[] keySplit=sKey.split(DOT_SPLITTER);
			 sKey="";
			 for(int index=1; index<keySplit.length;index++)
			 {
				 if(sKey.equals(""))
					 sKey=sKey+keySplit[index];
				 else
					 sKey=sKey+"."+keySplit[index];
			//sKey = sKey.split(DOT_SPLITTER)[1];
				 
			 }
			 sKey=sKey.substring(sKey.indexOf(".")+1);
		 }
	//	 else if(sKey.contains(StringConstants.PERIOD) && (sKey.indexOf(StringConstants.PERIOD)==sKey.lastIndexOf(StringConstants.PERIOD)) && !sKey.endsWith(StringConstants.PERIOD))
		 else if(sKey.contains(StringConstants.PERIOD) && (sKey.indexOf(StringConstants.PERIOD)==sKey.lastIndexOf(StringConstants.PERIOD)) && !sKey.endsWith(StringConstants.PERIOD) )
		 {
			//sKey = sKey.split(DOT_SPLITTER)[1];
			 sKey=sKey.substring(sKey.indexOf(".")+1);
		 }*/
		 else if((sKey.contains(StringConstants.PERIOD) && (sKey.indexOf(StringConstants.PERIOD)==sKey.lastIndexOf(StringConstants.PERIOD)) && sKey.endsWith(StringConstants.PERIOD)))
		 continue;
		 
		 temp= StringConstants.EMPTY + oDataTable.getColumnValues(nIndex).get(0).displayValue();
		 sValue=temp.trim();
		 if(sValue.equals(StringConstants.COLON))
			 sValue=StringConstants.EMPTY;
		 /*if (sKey.equalsIgnoreCase("Safety Report") 
					|| sKey.equalsIgnoreCase("Primary Source") || sKey.equalsIgnoreCase("Sender")
					|| sKey.equalsIgnoreCase("Receiver") || sKey.equalsIgnoreCase("Patient")
					|| sKey.equalsIgnoreCase("Patient medical history") || sKey.equalsIgnoreCase("Past drug therapy")
					|| sKey.equalsIgnoreCase("Patient death") || sKey.equalsIgnoreCase("Patient death cause")
					|| sKey.equalsIgnoreCase("Patient autopsy") || sKey.equalsIgnoreCase("Parent")
					|| sKey.equalsIgnoreCase("Parent medical history") || sKey.equalsIgnoreCase("Parent drug therapy")
					|| sKey.equalsIgnoreCase("B_2 Reaction") || sKey.equalsIgnoreCase("Test")
					|| sKey.equalsIgnoreCase("Drug") || sKey.equalsIgnoreCase("Active Substance")
					|| sKey.equalsIgnoreCase("Drug Recurrence") || sKey.equalsIgnoreCase("Drug Reaction relatedness")
					|| sKey.equalsIgnoreCase("Summary") )*/
		 if (sKey.equalsIgnoreCase(SECTION_HEADING_A_1_SAFETYREPORT) 
					|| sKey.equalsIgnoreCase(SECTION_HEADING_A_2_PRIMARY_SOURCE) || sKey.equalsIgnoreCase(SECTION_HEADING_A_3_1_SENDER)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_A_3_2_RECEIVER) || sKey.equalsIgnoreCase(SECTION_HEADING_B_1_PATIENT)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_1_7_PATIENT_MEDICAL_HISTORY) || sKey.equalsIgnoreCase(SECTION_HEADING_B_1_8_PAST_DRUG_THERAPY)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_1_9_PATIENT_DEATH) || sKey.equalsIgnoreCase(SECTION_HEADING_B_1_9_2_PATIENT_DEATH_CAUSE)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_1_9_4_PATIENT_AUTOPSY) || sKey.equalsIgnoreCase(SECTION_HEADING_B_1_10_PARENT)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_1_10_7_PARENT_MEDICAL_HISTORY) || sKey.equalsIgnoreCase(SECTION_HEADING_B_1_10_8_PARENT_DRUG_THERAPY)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_2_REACTION) || sKey.equalsIgnoreCase(SECTION_HEADING_B_3_TEST)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_4_DRUG) || sKey.equalsIgnoreCase(SECTION_HEADING_B_4_K_2_2_ACTIVE_SUBSTANCE)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_4_K_17_DRUG_RECURRENCE) || sKey.equalsIgnoreCase(SECTION_HEADING_B_4_K_18_DRUG_REACTION_RELATEDNESS)
					|| sKey.equalsIgnoreCase(SECTION_HEADING_B_5_SUMMARY) )
		 {
			 setHeaderStyle(text, sKey);
			 
		 }else
		 { 
			
			 setKeyAndValue(text, sKey, sValue);
			 setColorCodeForAvailability(sValue, text);
		 }
		
	}
//	}

	text.append("<tr><td colspan='3'>&nbsp;</td></tr></table></html>");	
	return text;
	}

public static String getHtmlPrefix(int nTotalFields,int nMissingFieldValue)
{
	String htmlPrefix=null;
	
	htmlPrefix="<html><head></head><table style='border:0px;cellpadding:0;border-spacing:0;font-family:Arial;font-size:15;'>"
			+ "<tr><td colspan='2'>&nbsp;</td></tr> <tr style='background-color:#EAF2F8'><td><b><u>Parsing Summary</u></b></td><td>&nbsp;</td></tr>"
			+"<tr style='background-color:#F5F6F7'><td><b>Parsing Status</td><td>100% Success ("+nTotalFields+" out of "+nTotalFields+" fields parsed with NO errors)</td></tr>"
			+"<tr style='background-color:#F5F6F7'><td><b>Missing Data</td><td>"+nMissingFieldValue+" out of "+ nTotalFields+ " are missing values</td></tr>"
			+"<tr style='background-color:#F5F6F7'><td colspan='2'>&nbsp;</td></tr>"
			+"<tr><td colspan='2'>&nbsp;</td></tr></table> "
			+"<table style='border:0px;cellpadding:0;border-spacing:0;font-family:Arial;font-size:12;background-color:#F5F6F7;width:530px;'>"
			+"<th style='width:50%;word-wrap:word-break;'>Fields</th><th style='word-wrap:word-break;'>Value</th><th style='width:2%;word-wrap:word-break;'>&nbsp;</th>";
	
	return htmlPrefix;
}
private static void setColorCodeForAvailability(String sValue, StringBuffer text)
{
	if (!sValue.equalsIgnoreCase("")) {
		text.append("<td style='background-color:green;color:white;text-align:center'>Y</td></tr>");
	} else {
		text.append("<td style='background-color:yellow;color:black;text-align:center'>M</td></tr>");
	}
}
private static void setHeaderStyle(StringBuffer text, String sKey)
{
	text.append("<tr><td colspan='3'>&nbsp;</td></tr>");
	 text.append("<tr style='background-color:#EAF2F8'><td colspan='3'><b><u>"+sKey+"</u></b></td></tr>");
}
private static void setKeyAndValue(StringBuffer text, String sKey, String sValue)
{
	text.append("<tr><td><b>"+sKey+"</td><td>"+sValue+"</td>");
}
	
public static int setDataCount(int nTotalFields, ParsedDataTable oDataTable)
{
	int nMissingFieldValue = 0;
	//nTotalFields = oDataTable.colCount();
	String sValue=null;
	for (int nIndex = 0; nIndex < oDataTable.colCount(); nIndex++) 
	{
		sValue = oDataTable.getColumnValues(nIndex).get(oDataTable.getColumnValues(nIndex).size()-1).displayValue().toString();
		if(sValue.trim().equals(""))
			nMissingFieldValue++;
	}
	return nMissingFieldValue;
}

	/**
	 * @author gwandhare
	 */
	public static String datatableKey(String key)
	{
		 if(key!=null && !key.equals(StringConstants.EMPTY))
		 {
			 if(key.contains(StringConstants.PERIOD) && (key.indexOf(StringConstants.PERIOD)!=key.lastIndexOf(StringConstants.PERIOD)))
			 {
				 key=key.substring(key.indexOf(".")+1);
			 }
			 else if(key.contains(StringConstants.PERIOD) && (key.indexOf(StringConstants.PERIOD)==key.lastIndexOf(StringConstants.PERIOD)) && !key.endsWith(StringConstants.PERIOD) )
			 {
				 key=key.substring(key.indexOf(".")+1);
			 }
		 }
		return key;
	}
	/**
	 * @author gwandhare
	 */
	public static boolean isFieldGroup(FileParsingConfiguration fileParseConfig,  String key, String header)
	{
		boolean inGroup = false;
		
		if(!key.endsWith("_Count") )
		{
			if (fileParseConfig.getField(key).getM_sFieldSubGroup().toLowerCase()
			.equals(header.toLowerCase()) && fileParseConfig.getField(key).isDisplayOnScreen() 
			 && fileParseConfig.getField(key).isDisplayOnScreen())
			{
				inGroup = true  ;
			}	
		}
		else{
			for(String mnKey: fileParseConfig.getFieldMap().keySet())
			{
				if(key.startsWith(mnKey))
				{
					if (fileParseConfig.getField(mnKey).getM_sFieldSubGroup().toLowerCase()
						.equals(header.toLowerCase()) && fileParseConfig.getField(mnKey).isDisplayOnScreen() 
						 && fileParseConfig.getField(mnKey).isDisplayOnScreen())
					{
						inGroup = true  ;
					}	
				}
			}
		}
		 return inGroup;
	}
	/**
	 * @author gwandhare
	 */
	public static String keyConfigKey(FileParsingConfiguration fileParseConfig ,String key)
	{
		String xmlkey = null;
		for(String mnKey: fileParseConfig.getFieldMap().keySet())
		{
			if(key.startsWith(mnKey))
			{
				xmlkey = mnKey;
			}
		}
		return 	 xmlkey;
	}
	
	/**
	 * @author gwandhare
	 */
	public static String keyDisplayValue(Object object)
	{
		String displayValue = null;
		displayValue =  StringConstants.EMPTY + object;
			  
		 if(displayValue.trim().equals(StringConstants.COLON))
		 {
			 displayValue = StringConstants.EMPTY;
		 }
		return 	 displayValue;
	}
	
	/**
	 * @author gwandhare
	 */
	public static List<String> getCollationFieldsMnemonics(String collation, FileParsingConfiguration fileConfig)
	{
		List<String> collationFieldMnemonic = new ArrayList<>();
		for(String key : fileConfig.getFieldMap().keySet())
		{
			if(collation.equals(fileConfig.getField(key).getCollationgroup()))
			{
				collationFieldMnemonic.add(key);
			}
		}
		return collationFieldMnemonic;
	}
	

	public PortalAppResponse exportExcelForMultipleSDs(String fileNameString, ParsedDataTable o_ParsedDataTable, List<String> fileNameList,Map<String, List<String>> keyValueMap,  PortalAppResponse oAppResponse, HttpServletResponse response)
	{
		File l_exportfile = null;
	//	FileWriter l_fwr = null;
		PrintWriter l_pwr = null;
		String fileHeaderString = null;
		String modifiedFileName = null;
		FileInputStream	l_fis = null;
		String subGroup = null;
		String displayName = null;
		
		String[] keyArray = null;
		int excelColumnIndex = 0;
		FileOutputStream fileOut =null;
		InputStream is = null;
		String group = null;
		String fileType = null;
		int headerSkipperCount = 0;
		HSSFWorkbook workbook = null;
		fileType = keyValueMap.get("fileType").get(0);
		
		try{
			fileNameString = fileNameString.replace(".csv", ".xls");
			l_exportfile = new File(fileNameString);
			l_exportfile.createNewFile();
			fileHeaderString = "";
			int excelRowCount = 0;
			fileNameString = l_exportfile.getName();
			for(int index=0; index<fileNameList.size(); index++)
			{
				modifiedFileName = fileNameList.get(index);
				if(modifiedFileName.contains("file_"))
				{
					modifiedFileName = modifiedFileName.substring(modifiedFileName.indexOf("file_")+5);
				}
				if(modifiedFileName.contains("_"))
				{
					modifiedFileName = modifiedFileName.substring(modifiedFileName.indexOf("_")+1);
				}
				
				fileNameList.set(index, modifiedFileName);
				
				if(!(index == fileNameList.size()-1 ))
				{
				fileHeaderString = fileHeaderString+ modifiedFileName+",";
				}
				else
				{
					fileHeaderString = fileHeaderString+ modifiedFileName;
				}
			}
			if(fileType.equalsIgnoreCase("MESA"))
				is= SourceDocUtilPortalApp.class.getResourceAsStream(EXPORT_EXCEL_TEMPLATE_XLS_AB);
			else
		is= SourceDocUtilPortalApp.class.getResourceAsStream(EXPORT_EXCEL_TEMPLATE_XLS);
			
			workbook = new HSSFWorkbook(is);
			
			HSSFSheet sheet = workbook.getSheet("Sheet1");
			HSSFRow rowhead = sheet.getRow(0);
			if(rowhead == null)
				rowhead = sheet.createRow(0);
			if(fileType.equalsIgnoreCase("MESA"))
			{
				headerSkipperCount=3;
				rowhead.createCell(0).setCellValue("Group");
				rowhead.createCell(1).setCellValue("Sub Group");
				rowhead.createCell(2).setCellValue("Field Name");
			}
			else
			{
				headerSkipperCount=2;
			rowhead.createCell(0).setCellValue("Group");
			rowhead.createCell(1).setCellValue("Field Name");
			}
			
			
			
		    for(int fileIndex=0; fileIndex<fileNameList.size();fileIndex++)
		    {
		    	rowhead.createCell(fileIndex+headerSkipperCount).setCellValue(fileNameList.get(fileIndex));
		    }
			for(Map.Entry<String,List<String>> entry : keyValueMap.entrySet()){
				
				
				
				
				if(entry.getKey().equalsIgnoreCase("FileName") || entry.getKey().equalsIgnoreCase("fileType"))
				{}
				else
				{
					excelRowCount++;
					String fieldValue = null;
					HSSFRow row = sheet.getRow((short)excelRowCount);
					if(row == null)
						row = sheet.createRow((short)excelRowCount);
					
					
					if(fileType.equalsIgnoreCase("MESA"))
					{
					if(entry.getKey().contains("_"))
					{
						keyArray = entry.getKey().split("_");
						if(keyArray.length==2)
						{
							group = keyArray[0];
							subGroup = "";
							displayName = keyArray[1];
						}
						else if(keyArray.length==3)
						{
							group = keyArray[0];
							subGroup = keyArray[1];
							displayName = keyArray[2];
						}
						else
						{
							subGroup = "";
							displayName = entry.getKey();
						}
					}
					
					row.createCell(0).setCellValue(group);
					row.createCell(1).setCellValue(subGroup);

                    row.createCell(2).setCellValue(displayName);
					}
					else
					{
					if(entry.getKey().contains("##"))
					{
						keyArray = entry.getKey().split("##");
						if(keyArray.length==2)
						{
							subGroup = keyArray[0];
							displayName = keyArray[1];
						}
						else if(keyArray.length==3)
							{
								group = keyArray[0];
								subGroup = keyArray[1];
								displayName = keyArray[2];
							}
						else
						{
							subGroup = "";
							displayName = entry.getKey();
						}
					}
					
						row.createCell(0).setCellValue(subGroup);

	                    row.createCell(1).setCellValue(displayName);
					}
					
					
					
                    

                
				for (int index=0; index< entry.getValue().size(); index++) {
					fieldValue = entry.getValue().get(index);
					excelColumnIndex = index+headerSkipperCount;
					
					
							if(!SourceDocUtilPortalApp.keyDisplayValue(fieldValue).equalsIgnoreCase(""))
							{
								
								row.createCell(excelColumnIndex).setCellValue(SourceDocUtilPortalApp.keyDisplayValue(fieldValue));
							}
							else
							{
								row.createCell(excelColumnIndex).setCellValue(SourceDocUtilPortalApp.keyDisplayValue(fieldValue));				
							}
							
				}
				
				}
			}
			
			l_pwr = response.getWriter();  
			
			fileOut = new FileOutputStream(l_exportfile.getAbsolutePath());

	    	workbook.write(fileOut);

	    	fileOut.close();

	    	workbook.close();
	    	
			response.setContentType("application/vnd.ms-excel");  
	    //	response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); 
			response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
			l_fis = new FileInputStream(l_exportfile);  
            
			int i;   
			while ((i = l_fis.read()) != -1) {  
				l_pwr.write(i);   
			}

		 }
		 catch (IOException ioExc) {
			 log.error("Error in export as csv");
			 oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_EXPORT_AS_CSV);
		 }finally {
				try {
					oAppResponse.setIsDownloadStreamPresent(true);
					response.flushBuffer();
				    if(fileOut != null)
				    {
				    	fileOut.close();
				    }
				    if(workbook != null)
				    {
				    	workbook.close();
				    }
				    if(is !=null)
				    {
				    	is.close();
				    }
					if(l_pwr != null) {
						l_pwr.close();
				   	}
				   	
				    if(l_fis != null){
				    	l_fis.close();
				   	}
				} catch (IOException ioExcep) {
					log.error("Error export text file", ioExcep);
					oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_EXPORT_AS_CSV);
					return oAppResponse;
				}
		}
	
	return oAppResponse;
	}
	public PortalAppResponse exportCSVForMultipleSDs(String fileNameString, List<String> fileNameList,Map<String, List<String>> keyValueMap,  PortalAppResponse oAppResponse, HttpServletResponse response)
	{
		File l_exportfile = null;
		FileWriter l_fwr = null;
		PrintWriter l_pwr = null;
		String fileHeaderString = null;
		String modifiedFileName = null;
		FileInputStream	l_fis = null;
		String subGroup = null;
		String displayName = null;
		String[] keyArray = null;
		String group = null;
		String fileType = null;
		try{
			l_exportfile = new File(fileNameString);
			l_fwr = new FileWriter(l_exportfile);
			l_exportfile.createNewFile();
			fileHeaderString = "";
			fileNameString = l_exportfile.getName();
			for(int index=0; index<fileNameList.size(); index++)
			{
				modifiedFileName = fileNameList.get(index);
				if(modifiedFileName.contains("file_"))
					modifiedFileName = modifiedFileName.substring(modifiedFileName.indexOf("file_")+5);
				if(modifiedFileName.contains("_"))
					modifiedFileName = modifiedFileName.substring(modifiedFileName.indexOf("_")+1);
				if(!(index == fileNameList.size()-1 ))
				fileHeaderString = fileHeaderString+ modifiedFileName+",";
				else
					fileHeaderString = fileHeaderString+ modifiedFileName;
			}
			fileType = keyValueMap.get("fileType").get(0);
			if(fileType.equalsIgnoreCase("E2B") || fileType.equalsIgnoreCase("Health Canada"))
			l_fwr.write("Group, Field Name," +fileHeaderString +System.lineSeparator());
			else
				l_fwr.write("Group,Sub Group, Field Name," +fileHeaderString +System.lineSeparator());
			
			for(Map.Entry<String,List<String>> entry : keyValueMap.entrySet()){
				
				//subGroup = fileParsingConfig.getField(p_sMnemonic)
				
				if(entry.getKey().equalsIgnoreCase("FileName") || entry.getKey().equalsIgnoreCase("fileType"))
				{}
				else
				{
					
					if(fileType.equalsIgnoreCase("MESA"))
					{
						if(entry.getKey().contains("_"))
						{
							keyArray = entry.getKey().split("_");
							if(keyArray.length==2)
							{
								group = keyArray[0];
								subGroup = "";
								displayName = keyArray[1];
							}
							else if(keyArray.length==3)
							{
								group = keyArray[0];
								subGroup = keyArray[1];
								displayName = keyArray[2];
							}
							else
							{
								subGroup = "";
								displayName = entry.getKey();
							}
						}
						l_fwr.write("\""+group+"\""+","+"\""+subGroup+"\""+","+"\""+displayName+"\""+",");
					}
					else
					{
					if(entry.getKey().contains("##"))
					{
						keyArray = entry.getKey().split("##");
						if(keyArray.length==2)
						{
							subGroup = keyArray[0];
							displayName = keyArray[1];
						}
						else if(keyArray.length==3)
						{
							group = keyArray[0];
							subGroup = keyArray[1];
							displayName = keyArray[2];
						}
						else
						{
							subGroup = "";
							displayName = entry.getKey();
						}
					}
				l_fwr.write("\""+subGroup+"\""+","+"\""+displayName+"\""+",");
					}
					
				
				String fieldValue = null;
				for (int index=0; index< entry.getValue().size(); index++) {
					fieldValue = entry.getValue().get(index);
				
						if(index== entry.getValue().size()-1)
						{
							if(!SourceDocUtilPortalApp.keyDisplayValue(fieldValue).equalsIgnoreCase(""))
							{
								l_fwr.write("\"'"+SourceDocUtilPortalApp.keyDisplayValue(fieldValue)+"\"");
						}
						else
						{
								l_fwr.write("\""+SourceDocUtilPortalApp.keyDisplayValue(fieldValue)+"\"");
						}
					}
						else
						{
							if(!SourceDocUtilPortalApp.keyDisplayValue(fieldValue).equalsIgnoreCase(""))
							{
								l_fwr.write("\"'"+SourceDocUtilPortalApp.keyDisplayValue(fieldValue)+"\""+",");
							}
							else
							{
								l_fwr.write("\""+SourceDocUtilPortalApp.keyDisplayValue(fieldValue)+"\""+",");
						}
					}
				
				}
				
				l_fwr.write(System.lineSeparator());
				}
			}
			
			l_fwr.close();
			l_pwr = response.getWriter();  
			response.setContentType("text/plain");  
			response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
			l_fis = new FileInputStream(l_exportfile);  
            
			int i;   
			while ((i = l_fis.read()) != -1) {  
				l_pwr.write(i);   
			}

		 }
		 catch (IOException ioExc) {
			 log.error("Error in export as csv");
			 oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_EXPORT_AS_CSV);
		 }finally {
				try {
					oAppResponse.setIsDownloadStreamPresent(true);
					response.flushBuffer();
				    
					if(l_pwr != null) {
						l_pwr.close();
				   	}
				   	
				    if(l_fis != null){
				    	l_fis.close();
				   	}
				} catch (IOException ioExcep) {
					log.error("Error export text file", ioExcep);
					oAppResponse.logError(WebErrorCodes.SourceDocUtil.ERROR_EXPORT_AS_CSV);
					return oAppResponse;
				}
		}
	
	return oAppResponse;
	}
	public PortalAppResponse exportCSVForSingleCase(String exportFilePath, FileParsingConfiguration fileParsingConfig, ParsedDataTable o_ParsedDataTable,  PortalAppResponse oAppResponse, HttpServletResponse response) throws SciException
	{
		
	File l_exportfile = null;
		FileWriter l_fwr = null;
		PrintWriter	l_pwr =	null;
		FileInputStream	l_fis = null;
		String subGroup = null;
		String m_sLabel = null;
		ParsedDataValue keyCountValue = null;
		int repeatCountForGroup = 0;
		int repeatCountForSubGroup = 0;
		String dataTableKey = null;
		String displayValue = null;
		ValidationState vState = null;
		String fileType = null;
		String mnemonic = null;
		StringParsedDataValue keyValue = null;
		String groupNamewithCount = null;
		String subGroupNamewithCount = null;
		String keyName = null;
				
		try{
			
			fileType = o_ParsedDataTable.getColumnValues("fileType").get(0).displayValue().toString();
			l_exportfile = new File(exportFilePath);
			
			l_fwr = new FileWriter(l_exportfile);
			l_exportfile.createNewFile();
			
			if(fileType.equalsIgnoreCase("E2B") || fileType.equalsIgnoreCase("Health Canada"))
			{
			l_fwr.write("Group, Field Names, Value, Validation Status " +System.lineSeparator());
			
			
			for(String xmlMnemonic : fileParsingConfig.getFieldMap().keySet())
			{
				//
				m_sLabel = fileParsingConfig.getField(xmlMnemonic).getFieldLabel();
				subGroup = fileParsingConfig.getField(xmlMnemonic).getM_sFieldSubGroup();
					if(o_ParsedDataTable.isColumnPresent(xmlMnemonic+"_Count"))
					{
						keyCountValue = o_ParsedDataTable.getColumnValues(xmlMnemonic+"_Count").get(0);
						repeatCountForGroup = Integer.valueOf(keyCountValue.displayValue().toString());
						for(int index=1; index<= repeatCountForGroup; index++)
						{
							
							m_sLabel = fileParsingConfig.getField(xmlMnemonic).getFieldLabel();
							dataTableKey = xmlMnemonic+"_"+index;
								m_sLabel = m_sLabel+" "+index;
							if(o_ParsedDataTable.isColumnPresent(dataTableKey))
							{
								vState = o_ParsedDataTable.getColumnValues(dataTableKey).get(0).validationState();
								displayValue = o_ParsedDataTable.getColumnValues(dataTableKey).get(0).displayValue().toString();
							}
							else
							{
								//when it does not found the key
								//it should never come to this block
								displayValue = "";
								vState = ValidationState.UNVALIDATED;
							}
							
							if(!displayValue.equalsIgnoreCase(""))
							{
							l_fwr.write("\""+subGroup+"\""+","
									+"\""+ m_sLabel+"\""+","
									+"\"'"+displayValue+"\""+","
									+"\""+vState+"\""+System.lineSeparator());
							}
							else
							{
							l_fwr.write("\""+subGroup+"\""+","
									+"\""+ m_sLabel+"\""+","
									+"\""+displayValue+"\""+","
									+"\""+vState+"\""+System.lineSeparator());
						}
						}
						
						
					}
					else if(o_ParsedDataTable.isColumnPresent(xmlMnemonic))
					{
						dataTableKey = xmlMnemonic;
							
						m_sLabel = fileParsingConfig.getField(xmlMnemonic).getFieldLabel();
						displayValue = o_ParsedDataTable.getColumnValues(xmlMnemonic).get(0).displayValue().toString();
						vState = o_ParsedDataTable.getColumnValues(dataTableKey).get(0).validationState();
							if(!displayValue.equalsIgnoreCase(""))
							{
								l_fwr.write("\""+subGroup+"\""+","
										+"\""+ m_sLabel+"\""+","
										+"\"'"+displayValue+"\""+","
										+"\""+vState+"\""+System.lineSeparator());
							}
							else
						{
							l_fwr.write("\""+subGroup+"\""+","
										+"\""+ m_sLabel+"\""+","
										+"\""+displayValue+"\""+","
										+"\""+vState+"\""+System.lineSeparator());
							}
						//}
					}
					
			}
			
			
			}
			else if(fileType.equalsIgnoreCase("MESA"))
			{
				l_fwr.write("Group,Sub Group Name, Field Names, Value, Validation Status " +System.lineSeparator());
				
				for(String groupName: fileParsingConfig.getFieldGroupList())
				{
					if(o_ParsedDataTable.isColumnPresent(groupName+"_Count"))
					{
						keyCountValue = o_ParsedDataTable.getColumnValues(groupName+"_Count").get(0);
						repeatCountForGroup = Integer.valueOf(keyCountValue.displayValue().toString());
						
						for(int groupIndex=1; groupIndex<= repeatCountForGroup; groupIndex++)
						{
							groupNamewithCount = groupName+groupIndex;
							for(String subgroup : fileParsingConfig.getsubGroupListByGroupName(groupName))
							{
								if(subgroup.equalsIgnoreCase(groupName))
								{
									for(FileParsingField tField: fileParsingConfig.getFieldListBySubGroup(subgroup))
									{
										
										keyName = tField.getMnemonic();
										m_sLabel = tField.getFieldLabel();
										mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
										mnemonic = mnemonic.replace(" ", "");
										mnemonic = mnemonic.replace("/", "");
									if(o_ParsedDataTable.isColumnPresent(mnemonic))
									{
										keyValue = (StringParsedDataValue) o_ParsedDataTable.getColumnValues(mnemonic).get(0);
										displayValue = keyValue.displayValue().toString();
										vState = keyValue.validationState();
									}
									else
									{
										//when it does not found the key
										//it should never come to this block
										keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
										displayValue = keyValue.displayValue().toString();
										vState = keyValue.validationState();
									}
									if(!displayValue.equalsIgnoreCase(""))
										displayValue = "'"+displayValue;
									l_fwr.write("\""+groupNamewithCount+"\""+","
											+","
											+"\""+ m_sLabel+"\""+","
											+"\""+displayValue+"\""+","
											+"\""+vState+"\""+System.lineSeparator());
										
									}
								}
								else  //subgroup is different than group
								{
									if(o_ParsedDataTable.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
									{
										keyCountValue = o_ParsedDataTable.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
										repeatCountForSubGroup = Integer.valueOf(keyCountValue.displayValue().toString());
										for(int subgroupIndex=1; subgroupIndex<= repeatCountForSubGroup; subgroupIndex++)
										{
											subGroupNamewithCount = subgroup+subgroupIndex;
											for(FileParsingField tField: fileParsingConfig.getFieldListBySubGroup(subgroup))
											{
												
												keyName = tField.getMnemonic();
												m_sLabel = tField.getFieldLabel();
												mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
												mnemonic = mnemonic.replace(" ", "");
												mnemonic = mnemonic.replace("/", "");
											if(o_ParsedDataTable.isColumnPresent(mnemonic))
											{
												keyValue = (StringParsedDataValue) o_ParsedDataTable.getColumnValues(mnemonic).get(0);
												displayValue = keyValue.displayValue().toString();
												vState = keyValue.validationState();
											}
											else
											{
												//when it does not found the key
												//it should never come to this block
												keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
												displayValue = keyValue.displayValue().toString();
												vState = keyValue.validationState();
											}
											if(!displayValue.equalsIgnoreCase(""))
												displayValue = "'"+displayValue;
											l_fwr.write("\""+groupNamewithCount+"\""+","
													+"\""+ subGroupNamewithCount+"\""+","
													+"\""+ m_sLabel+"\""+","
													+"\""+displayValue+"\""+","
										+"\""+vState+"\""+System.lineSeparator());
												
											}
											
										}
									}
									else //if subgroup count is not present in the dataTable
									{
										for(FileParsingField tField: fileParsingConfig.getFieldListBySubGroup(subgroup))
										{
											
											keyName = tField.getMnemonic();
											m_sLabel = tField.getFieldLabel();
											mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
											mnemonic = mnemonic.replace(" ", "");
											mnemonic = mnemonic.replace("/", "");
										if(o_ParsedDataTable.isColumnPresent(mnemonic))
										{
											keyValue = (StringParsedDataValue) o_ParsedDataTable.getColumnValues(mnemonic).get(0);
											displayValue = keyValue.displayValue().toString();
											vState = keyValue.validationState();
						}
						else
										{
											//when it does not found the key
											//it should never come to this block
											keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
											displayValue = keyValue.displayValue().toString();
											vState = keyValue.validationState();
										}
							if(!displayValue.equalsIgnoreCase(""))
											displayValue = "'"+displayValue;
										l_fwr.write("\""+groupNamewithCount+"\""+","
												+","
												+"\""+ m_sLabel+"\""+","
												+"\""+displayValue+"\""+","
												+"\""+vState+"\""+System.lineSeparator());
											
										}

									}
									
								}
							}
						}
						
						
						
					}//if group count is not present in datatable
					else
					{
								groupNamewithCount = groupName;
								for(String subgroup : fileParsingConfig.getsubGroupListByGroupName(groupName))
								{
									if(subgroup.equalsIgnoreCase(groupName))
									{
										for(FileParsingField tField: fileParsingConfig.getFieldListBySubGroup(subgroup))
										{
											
											keyName = tField.getMnemonic();
											m_sLabel = tField.getFieldLabel();
											mnemonic = groupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
											mnemonic = mnemonic.replace(" ", "");
											mnemonic = mnemonic.replace("/", "");
										if(o_ParsedDataTable.isColumnPresent(mnemonic))
						{
											keyValue = (StringParsedDataValue) o_ParsedDataTable.getColumnValues(mnemonic).get(0);
											displayValue = keyValue.displayValue().toString();
											vState = keyValue.validationState();
										}
										else
										{
											//when it does not found the key
											//it should never come to this block
											keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
											displayValue = keyValue.displayValue().toString();
											vState = keyValue.validationState();
										}
										if(!displayValue.equalsIgnoreCase(""))
											displayValue = "'"+displayValue;
										l_fwr.write("\""+groupNamewithCount+"\""+","
												+","
										+"\""+ m_sLabel+"\""+","
												+"\""+displayValue+"\""+","
										+"\""+vState+"\""+System.lineSeparator());
											
										}
									}
									else  //subgroup is different than group
									{
										if(o_ParsedDataTable.isColumnPresent(groupNamewithCount+"."+subgroup+"_Count"))
										{
											keyCountValue = o_ParsedDataTable.getColumnValues(groupNamewithCount+"."+subgroup+"_Count").get(0);
											repeatCountForSubGroup = Integer.valueOf(keyCountValue.displayValue().toString());
											for(int subgroupIndex=1; subgroupIndex<= repeatCountForSubGroup; subgroupIndex++)
											{
												subGroupNamewithCount = subgroup+subgroupIndex;
												for(FileParsingField tField: fileParsingConfig.getFieldListBySubGroup(subgroup))
												{
													
													keyName = tField.getMnemonic();
													m_sLabel = tField.getFieldLabel();
													mnemonic = groupNamewithCount+"_"+subGroupNamewithCount+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
													mnemonic = mnemonic.replace(" ", "");
													mnemonic = mnemonic.replace("/", "");
												if(o_ParsedDataTable.isColumnPresent(mnemonic))
												{
													keyValue = (StringParsedDataValue) o_ParsedDataTable.getColumnValues(mnemonic).get(0);
													displayValue = keyValue.displayValue().toString();
													vState = keyValue.validationState();
						}
				else
				{
													//when it does not found the key
													//it should never come to this block
													keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
													displayValue = keyValue.displayValue().toString();
													vState = keyValue.validationState();
												}
												if(!displayValue.equalsIgnoreCase(""))
													displayValue = "'"+displayValue;
												l_fwr.write("\""+groupNamewithCount+"\""+","
														+"\""+ subgroup+"\""+","
										+"\""+ m_sLabel+"\""+","
										+"\""+displayValue+"\""+","
										+"\""+vState+"\""+System.lineSeparator());
													
												}
												
											}
										}
										else //if subgroup count is not present in the dataTable
										{
											for(FileParsingField tField: fileParsingConfig.getFieldListBySubGroup(subgroup))
											{
												
												keyName = tField.getMnemonic();
												m_sLabel = tField.getFieldLabel();
												mnemonic = groupNamewithCount+"_"+subgroup+"_"+keyName.substring(keyName.lastIndexOf("_")+1);
												mnemonic = mnemonic.replace(" ", "");
												mnemonic = mnemonic.replace("/", "");
											if(o_ParsedDataTable.isColumnPresent(mnemonic))
											{
												keyValue = (StringParsedDataValue) o_ParsedDataTable.getColumnValues(mnemonic).get(0);
												displayValue = keyValue.displayValue().toString();
												vState = keyValue.validationState();
											}
											else
											{
												//when it does not found the key
												//it should never come to this block
												keyValue = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.UNVALIDATED);
												displayValue = keyValue.displayValue().toString();
												vState = keyValue.validationState();
							}
											if(!displayValue.equalsIgnoreCase(""))
												displayValue = "'"+displayValue;
											l_fwr.write("\""+groupNamewithCount+"\""+","
													+","
													+"\""+ m_sLabel+"\""+","
													+"\""+displayValue+"\""+","
													+"\""+vState+"\""+System.lineSeparator());
												
			}
			
			}
			
									}
								}
					}
			
				}
					
			}
			
			
			l_fwr.close();
			l_pwr = response.getWriter();  
			response.setContentType("text/plain");  
			response.setHeader("Content-Disposition","attachment; filename=\"" + l_exportfile.getName() + "\""); 
			l_fis = new FileInputStream(l_exportfile);  
            
			int i;   
			while ((i = l_fis.read()) != -1) {  
				l_pwr.write(i);   
			}
		 }
		 catch (IOException ioExc) {
			 log.error("Error in export key");
			 throw new SciException(ioExc.getMessage());
		 }finally {
				try {
					oAppResponse.setIsDownloadStreamPresent(true);
					response.flushBuffer();
				    
					if(l_pwr != null) {
						l_pwr.close();
				   	}
				   	
				    if(l_fis != null){
				    	l_fis.close();
				   	}
				} catch (IOException ioExcep) {
					 throw new SciException(ioExcep.getMessage());
				}
			}
			
		return oAppResponse;
	}
			
	public static String keyDisplayValueMutiField(ParsedDataTable o_parseddatatable, String key)
			{
           Object object = null;
           String displayValue = null;
           if( o_parseddatatable.getColumnValues(key) != null)
				{
                  object = o_parseddatatable.getColumnValues(key).get(0).displayValue();
                  displayValue =  StringConstants.EMPTY + object;
                   if(displayValue.trim().equals(StringConstants.COLON))
                   {
                        displayValue = StringConstants.EMPTY;
			}	
			}
           return displayValue;
	}
}
