package com.sciformix.sciportal.apps.audit;

import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.audit.AuditHomeImpl;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserSession;

public class AuditPortalApp extends BasePortalApp
{

	private static final Logger log = LoggerFactory.getLogger(AuditPortalApp.class);
	
	public static enum AUDIT_FETCH_SCOPE
	{
		TIME_TODAY_TILL_NOW, TIME_YESTERDAY_TILL_NOW, LAST_7_DAYS, START_OF_TIME;
	}
	
	public static enum AUDIT_FETCH_TYPE
	{
		USER, SYSTEM;
	}
	
	
	public AuditPortalApp() 
	{
		super("AUDIT-PORTAL-APP", "AuditPortalApp",false, false);
		
	}
	/**
	 * This method is used for getting Audit for User
	 * @param p_oUserSession
	 * @param p_eFetchType
	 * @param p_nUserSeqId
	 * @param p_eFetchScope
	 * @param p_bFetchAuditDetails
	 * @param p_nUserProjectId
	 * @return List AuditInfoDb
	 * @throws SciServiceException
	 */	
	public List<AuditInfoDb> getAuditForUser(UserSession p_oUserSession, AUDIT_FETCH_TYPE p_eFetchType, int p_nUserSeqId, AUDIT_FETCH_SCOPE p_eFetchScope, boolean p_bFetchAuditDetails, int p_nUserProjectId) 
			throws SciServiceException
	{
		List<AuditInfoDb> listAuditTrail = null;
		Calendar calThresholdDate = null;
		
		switch (p_eFetchScope)
		{
		case TIME_TODAY_TILL_NOW:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
			
			break;
		case TIME_YESTERDAY_TILL_NOW:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
				 
			calThresholdDate.add(Calendar.DAY_OF_MONTH, -1);
			break;
		case LAST_7_DAYS:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
			
			calThresholdDate.add(Calendar.DAY_OF_MONTH, -7);
			break;
		case START_OF_TIME:
			calThresholdDate = Calendar.getInstance();
			calThresholdDate.set(Calendar.HOUR_OF_DAY, 0);
			calThresholdDate.set(Calendar.MINUTE, 0);
			calThresholdDate.set(Calendar.SECOND, 0);
			calThresholdDate.set(Calendar.MILLISECOND, 0);
			
			calThresholdDate.add(Calendar.DAY_OF_MONTH, -365*10);//Start of time is taken as 10-years
			break;
		default:
			break;
		}
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				if (p_eFetchType == AUDIT_FETCH_TYPE.USER)
				{
					listAuditTrail =  AuditHomeImpl.getUserAuditTrailForUser(connection, p_nUserSeqId, calThresholdDate, null, p_bFetchAuditDetails,p_nUserProjectId);
				}
				else if (p_eFetchType == AUDIT_FETCH_TYPE.SYSTEM)
				{
					int nBootstrapUserSeqId = 0;
					
					nBootstrapUserSeqId = UserHome.retrieveBootstrapUser().getUserSeqId();
					listAuditTrail =  AuditHomeImpl.getSystemAuditTrail(connection, nBootstrapUserSeqId, calThresholdDate, null, p_bFetchAuditDetails);
				}
				
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching audit trail for user", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Audit.ERROR_FETCHING_USER_AUDIT_TRAIL, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching audit trail for user", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Audit.ERROR_FETCHING_USER_AUDIT_TRAIL, dbExcep);
		}
		return listAuditTrail;
	}

}
