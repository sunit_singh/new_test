package com.sciformix.sciportal.apps;

class AppInfoDb
{
	
	private String m_sAppId;
	private String m_sAppName;
	private String m_sAppClass;
	private String m_sWebAppClass;
	private int m_nAppType;
	private int m_nAppAvailType;
	
	public AppInfoDb()
	{
		//Nothing to do
	}
	
	public String getAppId() {
		return m_sAppId;
	}

	public void setAppId(String p_sAppId) {
		this.m_sAppId = p_sAppId;
	}

	public String getAppName() {
		return m_sAppName;
	}

	public void setAppName(String p_sAppName) {
		this.m_sAppName = p_sAppName;
	}

	public String getAppClass() {
		return m_sAppClass;
	}

	public void setAppClass(String p_sAppClass) {
		this.m_sAppClass = p_sAppClass;
	}

	public String getWebAppClass() {
		return m_sWebAppClass;
	}

	public void setWebAppClass(String p_sWebAppClass) {
		this.m_sWebAppClass = p_sWebAppClass;
	}

	public int getAppType() {
		return m_nAppType;
	}

	public void setAppType(int p_nAppType) {
		this.m_nAppType = p_nAppType;
	}

	public int getAppAvailType() {
		return m_nAppAvailType;
	}

	public void setAppAvailType(int p_nAppAvailType) {
		this.m_nAppAvailType = p_nAppAvailType;
	}

}
