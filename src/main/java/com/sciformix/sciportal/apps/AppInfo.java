package com.sciformix.sciportal.apps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.web.apps.IPortalWebApp;

public class AppInfo
{
	private static final Logger log = LoggerFactory.getLogger(AppInfo.class);
	
	public enum AppType
	{
			TEST(0), SYSTEM(1), BUSINESS(2);
			
			private int value;
			
			AppType(int value)
			{
				this.value = value;
			}
			
			public int value()
			{
				return this.value;
			}
			
			public static AppType toEnum(int value)
			{
				AppType[] values = AppType.values();
				for (int counter = 0; counter < values.length; counter++)
				{
					if (value == values[counter].value())
					{
						return values[counter];
					}
				}
				log.error("Illegal enum value - {}", value);
				throw new IllegalArgumentException("AppType: Illegal enum value - " + value);
			}
	}
	
	public enum AppAvailType
	{
		ALL(5);
		//SDS(0), SYSTEM(1), SELF(2), ELIGIBLE-GROUP(3), ELIGIBLE-INDIVIDUAL(4), ALL(5)
			
			private int value;
			
			AppAvailType(int value)
			{
				this.value = value;
			}
			
			public int value()
			{
				return this.value;
			}
			
			public static AppAvailType toEnum(int value)
			{
				AppAvailType[] values = AppAvailType.values();
				for (int counter = 0; counter < values.length; counter++)
				{
					if (value == values[counter].value())
					{
						return values[counter];
					}
				}
				log.error("Illegal enum value - {}", value);
				throw new IllegalArgumentException("AppAvailType: Illegal enum value - " + value);
			}
	}
	
	
	private String m_sAppId;
	private String m_sAppName;
	private String m_sAppClass;
	private String m_sWebAppClass;
	private AppType m_nAppType;
	private AppAvailType m_nAppAvailType;
	private IPortalApp m_oPortalApp;
	private IPortalWebApp m_oPortalWebApp;
	
	
	public AppInfo(AppInfoDb p_oAppInfoDb) throws SciException
	{
		m_sAppId = p_oAppInfoDb.getAppId();
		m_sAppName = p_oAppInfoDb.getAppName();
		m_sAppClass = p_oAppInfoDb.getAppClass();
		m_sWebAppClass = p_oAppInfoDb.getWebAppClass();
		try
		{
			m_nAppType = AppType.toEnum(p_oAppInfoDb.getAppType());
			m_nAppAvailType = AppAvailType.toEnum(p_oAppInfoDb.getAppAvailType());
		}
		catch (IllegalArgumentException iaExcep)
		{
			log.error("Error constructing app - {}", m_sAppId, iaExcep);
			throw new SciException("Error constructing app", iaExcep);
		}
		
		ClassLoader classLoader = AppInfo.class.getClassLoader();

	    try
	    {
	        Class<?> classPortalApp = classLoader.loadClass(m_sAppClass);
	        m_oPortalApp = (IPortalApp) classPortalApp.newInstance();
	    }
	    catch (ClassNotFoundException cnfExcep)
	    {
	    	log.error("Error constructing app - {}", m_sAppId, cnfExcep);
			throw new SciException("Error constructing app", cnfExcep);
	    }
	    catch (IllegalAccessException iaExcep)
	    {
	    	log.error("Error constructing app - {}", m_sAppId, iaExcep);
			throw new SciException("Error constructing app", iaExcep);
	    }
	    catch (InstantiationException initiationExcep)
	    {
	    	log.error("Error constructing app - {}", m_sAppId, initiationExcep);
			throw new SciException("Error constructing app", initiationExcep);
	    }
	    
	    try
	    {
	        Class<?> classPortalWebApp = classLoader.loadClass(m_sWebAppClass);
	        m_oPortalWebApp = (IPortalWebApp) classPortalWebApp.newInstance();
	    }
	    catch (ClassNotFoundException cnfExcep)
	    {
	    	log.error("Error constructing app - {}", m_sAppId, cnfExcep);
			throw new SciException("Error constructing app", cnfExcep);
	    }
	    catch (IllegalAccessException iaExcep)
	    {
	    	log.error("Error constructing app - {}", m_sAppId, iaExcep);
			throw new SciException("Error constructing app", iaExcep);
	    }
	    catch (InstantiationException initiationExcep)
	    {
	    	log.error("Error constructing app - {}", m_sAppId, initiationExcep);
			throw new SciException("Error constructing app", initiationExcep);
	    }
	}
	
	public String getAppId() {
		return m_sAppId;
	}

	public void setAppId(String p_sAppId) {
		this.m_sAppId = p_sAppId;
	}

	public String getAppName() {
		return m_sAppName;
	}

	public void setAppName(String p_sAppName) {
		this.m_sAppName = p_sAppName;
	}

	public String getAppClass() {
		return m_sAppClass;
	}

	public void setAppClass(String p_sAppClass) {
		this.m_sAppClass = p_sAppClass;
	}

	public String getWebAppClass() {
		return m_sWebAppClass;
	}

	public void setWebAppClass(String p_sWebAppClass) {
		this.m_sWebAppClass = p_sWebAppClass;
	}

	public AppType getAppType() {
		return m_nAppType;
	}

	public void setAppType(int p_nAppType) {
		this.m_nAppType = AppType.toEnum(p_nAppType);
	}

	public AppAvailType getAppAvailType() {
		return m_nAppAvailType;
	}

	public void setAppAvailType(int p_nAppAvailType) {
		this.m_nAppAvailType = AppAvailType.toEnum(p_nAppAvailType);
	}
	
	public IPortalApp getPortalApp()
	{
		return m_oPortalApp;
	}
	
	public IPortalWebApp getPortalWebApp()
	{
		return m_oPortalWebApp;
	}
}
