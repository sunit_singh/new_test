package com.sciformix.sciportal.apps.user;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.apps.ngv.UserAuditHelper;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserAuthHome;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserInfo.UserAddonRole;
import com.sciformix.sciportal.user.UserSession;

public class UserManagementPortalApp extends BasePortalApp{

	private static final Logger log = LoggerFactory.getLogger(UserManagementPortalApp.class);
	
	public UserManagementPortalApp()
	{
		super("USRMGMNT-PORTAL-APP","UsrmgmntPortalApp",false, false);
	}

	/**
	 * This will fetch the complete list of user present in user table
	 * @return UserInfo List
	 * @throws SciServiceException
	 */
	public List<UserInfo> getListOfUsers() throws SciServiceException
	{
		return UserHome.getListOfUsers();
	}
	
	/**
	 * This method is used for creating user 
	 * @param p_oUserSession
	 * @param p_sUserId
	 * @param p_sUserDisplayName
	 * @param p_sUserShortName
	 * @param p_sUserEmail
	 * @param p_eAuthType
	 * @param p_oAddonRolesToGrant
	 * @param opType
	 * @return boolean 
	 * @throws SciServiceException
	 */
	public boolean createUser(UserSession p_oUserSession, String p_sUserId, String p_sUserDisplayName, String p_sUserShortName, 
			String p_sUserEmail, UserAuthHome.AuthType p_eAuthType, Set<UserAddonRole> p_oAddonRolesToGrant
			,boolean opType) throws SciServiceException
	{
		boolean flag = false;
		UserInfo oUserInfo = null;
		AuditInfoDb oAuditInfo = null;
	
		
		oUserInfo = new UserInfo();
		oUserInfo.setUserId(p_sUserId.toLowerCase());
		oUserInfo.setUserDisplayName(p_sUserDisplayName);
		oUserInfo.setUserShortName(p_sUserDisplayName);
		oUserInfo.setUserEmail(p_sUserEmail);
		oUserInfo.setUserAuthType(p_eAuthType);
		oUserInfo.setAddonUserRole(p_oAddonRolesToGrant);
		oUserInfo.setUserState(UserInfo.UserState.ACTIVE);	

		oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "User Created");
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_USERID, p_sUserId.toLowerCase());
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_DISPLAYNAME, p_sUserDisplayName);
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_EMAIL, p_sUserEmail);
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_AUTHTYPE, p_eAuthType.toString());
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				flag = UserHome.saveUser(connection, p_oUserSession, oUserInfo, opType);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error creating user", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_CREATING_USER, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error creating user", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_CREATING_USER, dbExcep);
		}
		return flag;
	}
	


	/**
	 * This method is used for updating User Attributes such as Display Name, Short Name, Email Id 
	 * @param p_oUserSession
	 * @param p_oUserInfo
	 * @param p_sUserNewDisplayName
	 * @param p_sUserNewShortName
	 * @param p_sUserNewEmail
	 * @return boolean 
	 * @throws SciServiceException
	 */
	public boolean updateUserAttributes(UserSession p_oUserSession, UserInfo p_oUserInfo, String p_sUserNewDisplayName, 
			String p_sUserNewShortName, String p_sUserNewEmail) throws SciServiceException
	{
		boolean flag = false;
		AuditInfoDb oAuditInfo = null;
		
		if(DataUtils.hasChanged(p_oUserInfo.getUserDisplayName(), p_sUserNewDisplayName)||
				DataUtils.hasChanged(p_oUserInfo.getUserShortName(), p_sUserNewShortName)||
				DataUtils.hasChanged(p_oUserInfo.getUserEmail(), p_sUserNewEmail))
		{
		
		oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "User modified");
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_SEQ_ID, p_oUserInfo.getUserSeqId());
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_USERID, p_oUserInfo.getUserId());
		
		if (DataUtils.hasChanged(p_oUserInfo.getUserDisplayName(), p_sUserNewDisplayName))
		{
			oAuditInfo.addDetails(UserAuditHelper.AuditAttributes.USER_DISPLAYNAME, p_oUserInfo.getUserDisplayName(), p_sUserNewDisplayName);
			p_oUserInfo.setUserDisplayName(p_sUserNewDisplayName);
		}		
		if (DataUtils.hasChanged(p_oUserInfo.getUserShortName(), p_sUserNewShortName))
		{
			oAuditInfo.addDetails(UserAuditHelper.AuditAttributes.USER_SHORTNAME, p_oUserInfo.getUserShortName(), p_sUserNewShortName);
			p_oUserInfo.setUserShortName(p_sUserNewShortName);
		}		
		if (DataUtils.hasChanged(p_oUserInfo.getUserEmail(), p_sUserNewEmail))
		{
			oAuditInfo.addDetails(UserAuditHelper.AuditAttributes.USER_EMAIL, p_oUserInfo.getUserEmail(), p_sUserNewEmail);
			p_oUserInfo.setUserEmail(p_sUserNewEmail);
		}
		
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				flag = UserHome.saveUser(connection, p_oUserSession, p_oUserInfo, false);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating user", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_USER, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error updating user", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_USER, dbExcep);
		}
			return flag;
		}
		return flag;
	}
	
	/**
	 * This method is used for updating User roles 
	 * @param p_oUserSession
	 * @param p_oUserInfo
	 * @param p_setUserAddonRolesToGrant
	 * @return boolean 
	 * @throws SciServiceException
	 */
	public boolean updateUserRole(UserSession p_oUserSession, UserInfo p_oUserInfo,
			Set<UserAddonRole> p_setUserAddonRolesToGrant) throws SciServiceException
	{
		boolean flag = false;
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "User modified");
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_SEQ_ID, p_oUserInfo.getUserSeqId());
		oAuditInfo.addPayload(UserAuditHelper.AuditAttributes.USER_USERID, p_oUserInfo.getUserId());
		
		//TODO:Check here for audit trail
		
		UserInfo o_UserCurrentRole = UserHome.retrieveUser(p_oUserInfo.getUserId(), null);
		
		p_oUserInfo.setAddonUserRole(p_setUserAddonRolesToGrant);			
		
		if (DataUtils.hasChanged(p_oUserInfo.getUserType(), o_UserCurrentRole.getUserType()))
		{
		oAuditInfo.addDetails(UserAuditHelper.AuditAttributes.USER_TYPE, o_UserCurrentRole.getUserType(), p_oUserInfo.getUserType());
		p_oUserInfo.setAddonUserRole(p_setUserAddonRolesToGrant);
		
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			flag = UserHome.saveUser(connection, p_oUserSession, p_oUserInfo, false);
			try
			{
				connection.startTransaction();
				
				flag = UserHome.saveUser(connection, p_oUserSession, p_oUserInfo, false);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating user role", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_USER, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error updating user role", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.System.ERROR_UPDATING_USER, dbExcep);
		}
		return flag;		
		}
	return flag;	
	}
	
}
