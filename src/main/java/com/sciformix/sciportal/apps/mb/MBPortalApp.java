package com.sciformix.sciportal.apps.mb;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ApplicationSequencePurpose;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.ServiceErrorCodes.SimpleServiceErrorCode;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.appseq.ApplicationSequenceHelper;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.mb.MBMailInfo;
import com.sciformix.sciportal.mb.MBMailInfo.MailRecordMode;
import com.sciformix.sciportal.mb.MBMailInfo.MailType;
import com.sciformix.sciportal.mb.MailBox;
import com.sciformix.sciportal.mb.MailBoxAuditHelper;
import com.sciformix.sciportal.mb.MailBoxConfigurationHelper;
import com.sciformix.sciportal.mb.MailBoxHomeImpl;
import com.sciformix.sciportal.mb.MailComparator;
import com.sciformix.sciportal.mb.OutlookMsgParser;
import com.sciformix.sciportal.project.ProjectHome;
import com.sciformix.sciportal.project.ProjectInfo;
import com.sciformix.sciportal.system.DisplayMessageHelper;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class MBPortalApp extends BasePortalApp {

	String s_SuccessMessage = null;
	private static final String crcDelimiter = "^~|]";
	public static final String DUPLICATE_MAIL_KEY = "DuplicateMail";
	public static final String SAVED_MAIL_KEY = "SavedMail";
	
	public static final String JSON_MAIL_FROM = "from";
	public static final String JSON_MAIL_TO = "to";
	public static final String JSON_MAIL_CC = "cc";
	public static final String JSON_MAIL_SUBJECT = "subject";
	public static final String JSON_MAIL_DATE = "msgDate";
	public static final String JSON_MAIL_ATTACHMENT = "attachmentName";
	public static final String JSON_MAIL_MESSAGE_TYPE = "messageType";
	public static final String JSON_MAIL_MESSAGE_ID = "msgId";
	public static final String JSON_MAIL_APP_SEQ_ID = "appSeqId";
	public static final String JSON_MAIL_STATUS = "status";
	private static final int DEFAULT_MAIL_SEQID	=	0;
	
	private static final Logger log = LoggerFactory.getLogger(MBPortalApp .class);
	
	public MBPortalApp() 
	{
		super("MB-PORTAL-APP", "MailBoxPortalApp",true);
		
	}
	
	/**
	 * This method is used for saving Mail Box
	 * @param u_Session
	 * @param p_id
	 * @param m_oMailBox
	 * @param u_oUserInfo
	 * @param p_bInsert
	 * @return String Success Message 
	 * @throws SciServiceException
	 */
	public String saveMailBox(UserSession u_oUserSession,int p_id,MailBox m_oMailBox,boolean p_bInsert) throws SciServiceException
	{
		MailBox mailBox=null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				AuditInfoDb oAuditInfo = null;
				
//				mailBox=MailBoxConfigurationHelper.saveMailBox(p_id, m_oMailBox, u_oUserSession.getUserInfo(), true);
				mailBox=MailBoxHomeImpl.saveMailBox(m_oMailBox, u_oUserSession.getUserInfo(), true);
				oAuditInfo = AuditHome.createRecordInsertStub(u_oUserSession,this.getPortalAppId(), "Mail Box Created",p_id);
				oAuditInfo.addPayload(MailBoxAuditHelper.MailBoxAttributes.MAIL_BOX_SEQUENCE_ID,mailBox.getSequenceId());
				oAuditInfo.addPayload(MailBoxAuditHelper.MailBoxAttributes.MAIL_BOX_NAME,mailBox.getTemplateName());
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				s_SuccessMessage = "Email Mail Box sequence id: "+mailBox.getSequenceId()+" created successfully";
				connection.commitTransaction();
			} catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving mail box", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.MB.ERROR_SAVING_EMAIL_BOX,sciExcep);
			}
		} catch(SciDbException sciExcep)
		{
			log.error("Error saving mail box", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_SAVING_EMAIL_BOX,sciExcep);
		}
		return s_SuccessMessage;
	}
	
	public  MailBox getMailBoxBySeqId(int m_sequenceid) 
	{
		return MailBoxConfigurationHelper.getMailBoxBySeqId(m_sequenceid);
	}
	
	/**
	 * This method is used for saving Mail Box Mails
	 * @param u_Session
	 * @param p_id
	 * @param m_oMailBox
	 * @param u_oUserInfo
	 * @param p_bInsert
	 * @return String Success Message 
	 * @throws SciServiceException
	 */
	public String saveMBMail(UserSession u_oUserSession,int p_id,MBMailInfo m_oMailInfo, boolean p_bInsert) throws SciServiceException
	{
		MBMailInfo oMBMail=null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				AuditInfoDb oAuditInfo = null;
				
				oMBMail=MailBoxHomeImpl.saveMBMail(m_oMailInfo, u_oUserSession.getUserInfo(), true);
				oAuditInfo = AuditHome.createRecordInsertStub(u_oUserSession,this.getPortalAppId(), "Mail Box Created",p_id);
				oAuditInfo.addPayload(MailBoxAuditHelper.MailBoxAttributes.MAIL_SEQUENCE_ID,oMBMail.getMailSequenceId());
				oAuditInfo.addPayload(MailBoxAuditHelper.MailBoxAttributes.MAIL_MSG_ID,oMBMail.getMailSubject());
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				s_SuccessMessage = "Email Mail  sequence id: "+oMBMail.getMailSequenceId()+" created successfully";
				connection.commitTransaction();
			} catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving mail", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST,sciExcep);
			}
		} catch(SciDbException sciExcep)
		{
			log.error("Error saving mail", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST,sciExcep);
		}
		return s_SuccessMessage;
	}
	
	private static void saveMBMailWebApi(UserInfo userInfo ,int p_id,MBMailInfo m_oMailInfo, boolean p_bInsert) throws SciServiceException
	{
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				//AuditInfoDb oAuditInfo = null;
				
				MailBoxHomeImpl.saveMBMail(m_oMailInfo, userInfo, true);
				/*oAuditInfo = AuditHome.createRecordInsertStub(u_oUserSession,this.getPortalAppId(), "Mail Box Created",p_id);
				oAuditInfo.addPayload(MailBoxAuditHelper.MailBoxAttributes.MAIL_SEQUENCE_ID,oMBMail.getMailSequenceId());
				oAuditInfo.addPayload(MailBoxAuditHelper.MailBoxAttributes.MAIL_MSG_ID,oMBMail.getMailSubject());
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				s_SuccessMessage = "Email Mail Box sequence id: "+oMBMail.getMailSequenceId()+" created successfully";*/
				connection.commitTransaction();
			} catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving mail box", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST,sciExcep);
			}
		} catch(SciDbException sciExcep)
		{
			log.error("Error saving mail box", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST,sciExcep);
		}
	
	}
	
	public List<MailBox> getMailBoxListByProjectId(int projectId) throws SciServiceException
	{
		List<MailBox> mailBoxList = null;
		try {
			mailBoxList = MailBoxHomeImpl.getAllMailBoxes(projectId);
			}
		catch(SciException sciExc)
		{
			log.error("Error fetching project mail boxes list");
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_BOX);
		}
		return mailBoxList;
		
	}
	public static MailBox  getMailBoxListByMailBoxName(String mailBoxName, int p_projectid) throws SciServiceException
	{
		MailBox mailBox = null; 
		try
		{
			mailBox = MailBoxHomeImpl.getMailBoxByTemplateName(mailBoxName, p_projectid);
		}
		catch(SciException sciExc)
		{
			log.error("Error fetching project mail box list");
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_BOX);
		}
		return mailBox;
		
	}
	
	public static List<MBMailInfo> getMailListByMailBoxId(int projectId, int mailBoxId) throws SciServiceException
	{
		List<MBMailInfo> mailBoxList = null;
		try {
			mailBoxList = MailBoxHomeImpl.getMailListByMailBoxId(projectId, mailBoxId);
			}
		catch(SciException sciExc)
		{
			log.error("Error fetching project mail boxes list");
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST);
		}
		return mailBoxList;
		
	}
	
	public static List<MBMailInfo> getMailListByMailByProjectId(int projectId) throws SciServiceException
	{
		List<MBMailInfo> mailBoxList = null;
		try {
			mailBoxList = MailBoxHomeImpl.getMailListByProjectId(projectId);
			}
		catch(SciException sciExc)
		{
			log.error("Error fetching project mail boxes list");
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST);
		}
		return mailBoxList;
		
	}
	
	public List<MBMailInfo> getMailListByDateRange(int projectId,int mailBoxSeqId, 
			int p_startdate, int p_enddate) throws SciServiceException
	{
		List<MBMailInfo> filterMailList = null;
		
		try{
			filterMailList = MailBoxHomeImpl.getMailListByDateRange(projectId, mailBoxSeqId, p_startdate, p_enddate);
		}
		catch(SciException sciExcp)
		{
			log.error("Error fetching mail list by date range");
			throw new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST);
		}
		return filterMailList;
	}
	
	public Map<String, List<MBMailInfo>> uploadMailByMsgFile(UserSession usersession, int projectId, 
			int mailBoxid, List<UploadedFileWrapper> uploaddFileWrapperList) throws SciServiceException
	{
		MBMailInfo mailInfo = null;
		List<MBMailInfo> mailInfoList = null,
				duplicateMailInfoList = null;
		Map<String, List<MBMailInfo>> mailInfoListMap = null;
		String crcString = null;
		mailInfoList = new ArrayList<>();
		duplicateMailInfoList = new ArrayList<>();
		for(UploadedFileWrapper uploadFilewrapper : uploaddFileWrapperList)
		{
			mailInfo = null;
			OutlookMsgParser oMessageParser = null;
			try{
				oMessageParser = new OutlookMsgParser(uploadFilewrapper.getStagedFilePath());
			}
			catch(SciException sciExc)
			{
				log.error("Error Parsing msg file");
				throw new SciServiceException(ServiceErrorCodes.MB.ERROR_PARSING_MSG_FILE);
			}
			try{
				crcString = generateCrc(oMessageParser.getFromeEmailId(), oMessageParser.getToEmailAddress(),
						oMessageParser.getCcEmailAddress(), oMessageParser.getSubject());
			}
			catch(SciException sciExcp)
			{
				log.error("Error Generating CRC checksum");
				throw new SciServiceException(ServiceErrorCodes.MB.ERROR_PARSING_MSG_FILE);
			}
			
			//TODO: The first field is hard-coded need to be changed to fetch the mail id sequence
			mailInfo = new MBMailInfo(DEFAULT_MAIL_SEQID, mailBoxid, usersession.getUserInfo().getUserSeqId(), projectId, oMessageParser.getMessageId(),
							null
							, oMessageParser.getSubject(), oMessageParser.getAttachementLength(),
					oMessageParser.getAttachmentFileNames(), oMessageParser.getMsgDate(), oMessageParser.getToEmailAddress(), 
					oMessageParser.getCcEmailAddress(), oMessageParser.getFromeEmailId(), MailRecordMode.MAIL_UPLOAD, 
					MailType.toEnum(oMessageParser.getMessageType()), crcString,new Date());
			
			
			if(!isDuplicateMsg(projectId, mailBoxid, mailInfo))
			{
				mailInfoList.add(mailInfo);
			}
			else
			{
				duplicateMailInfoList.add(mailInfo);
			}
		}
		 /*System.out.println("Before");
		 for(MBMailInfo mo: mailInfoList)
		 {
			 System.out.println(mo.getMailTimeStamp());
		 }*/
		 Collections.sort(mailInfoList,new MailComparator());
		// System.out.println("After");
		 for(MBMailInfo mbInfo: mailInfoList)
		 {
			 //System.out.println(mo.getMailTimeStamp());
			 try{
				 mbInfo.setMailAppSeqId(ApplicationSequenceHelper.getNextApplicationSequenceNumber(projectId, ApplicationSequencePurpose.MAILBOX, mailBoxid));
				}
				catch(SciServiceException sciSerExcp)
				{
					log.error("Error ");
					throw new SciServiceException(ServiceErrorCodes.ApplicationSequence.ERROR_MAX_SEQ_NUMBER);
				}
				saveMBMail(usersession, projectId, mbInfo, true);
		 }
		
		mailInfoListMap = new HashMap<>();
		mailInfoListMap.put(SAVED_MAIL_KEY, mailInfoList);
		mailInfoListMap.put(DUPLICATE_MAIL_KEY, duplicateMailInfoList);
		return mailInfoListMap;
	}
	
	private static String generateCrc(String sender, String to, String cc, String sub)throws SciException
	{
		String formCrcString = StringUtils.emptyString(sender)
				+crcDelimiter +StringUtils.emptyString(to)
				+crcDelimiter +StringUtils.emptyString(cc)
				+crcDelimiter +StringUtils.emptyString(sub);
		byte byteData[] = null;
		MessageDigest o_messagedigest = null;
		try {
			o_messagedigest = MessageDigest.getInstance("MD5");
			o_messagedigest.update(formCrcString.getBytes());
			byteData = o_messagedigest.digest();
	    }
	    catch(NoSuchAlgorithmException chexp)
	    {
		  log.error("Error generating crc checksum", chexp);
		  throw new SciException("Error generating crc checksum");
	    }
		 StringBuffer cheksumString = new StringBuffer("");
	        for (int i = 0; i < byteData.length; i++) {
	        	cheksumString.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
		return cheksumString.toString();
	}
	
	private static boolean isDuplicateMsg(int projectId, int mailBoxId, MBMailInfo mailInfo ) throws SciServiceException
	{
		boolean duplicateFlag = false;
		List<MBMailInfo> mailInfoDbList = getMailListByMailByProjectId(projectId);
		
		for(MBMailInfo mailInfoDb : mailInfoDbList)
		{
			if(mailInfoDb.getProjectSeqId() == mailInfo.getProjectSeqId() &&
					mailInfoDb.getMailMessageId().equals(mailInfo.getMailMessageId())
					&& mailInfoDb.getMailBoxSeqId() == mailInfo.getMailBoxSeqId())
			{
				mailInfo.setMailAppSeqId(mailInfoDb.getMailAppSeqId());
				duplicateFlag = true;
			}
			/*else 
			if(mailInfoDb.getProjectSeqId() == mailInfo.getProjectSeqId() &&
				mailInfoDb.getCrc().equals(mailInfo.getCrc()) && 
				mailInfoDb.getMailTimeStamp().compareTo(mailInfo.getMailTimeStamp())== 0 && 
				mailInfoDb.getMailBoxSeqId() == mailInfo.getMailBoxSeqId())
			{
				duplicateFlag =true;
			}*/
		}
		return duplicateFlag;
	}
	
	public static String joinMailIdString(List<MBMailInfo> mailinfoList)
	{
		List<String> mailInfoList = new ArrayList<>();
		for(MBMailInfo mailInfo : mailinfoList)
		{
			mailInfoList.add(mailInfo.getMailSubject());
		}
		return String.join(", ", mailInfoList);
	}
	
	public static Object saveMailByApi(JSONArray jsonArray , String mailBoxName, String userId, String p_projectname) throws SciServiceException
	{
		MBMailInfo mailInfo = null;
		MailBox mailBox = null;
		UserInfo userInfo = null;
		int attachmentCount = 0,
			l_projectid		= 0;
		ProjectInfo	l_projectinfo	=	null;
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		Date msgDate = null;
		MailType mailType = null;
		String crcString = null;
		String [] mailArray = null;
		String [][]	mailListArray = null;
		List<String[]> responceList = new ArrayList<>();
		
				
		for(int count = 0; count< jsonArray.length() ; count++)
		{
			List<String> mailFieldList = new ArrayList<>();
			try {
				l_projectinfo	=	ProjectHome.getProjectGivenProjectName(p_projectname);
				if (l_projectinfo != null) {
					l_projectid	=	l_projectinfo.getProjectSequenceId();
				} else {
					log.error("Project details not found for the project name:" + p_projectname);
					return ServiceErrorCodes.MB.ERROR_PROJECT_DETAILS_NOT_FOUND.errorMessage();
				}
				userInfo =UserHome.retrieveUser(userId, null);
				
				if(jsonArray.getJSONObject(count).get(JSON_MAIL_ATTACHMENT) != null)
				{
					attachmentCount = jsonArray.getJSONObject(count).get(JSON_MAIL_ATTACHMENT).toString().
								split(SciConstants.StringConstants.SEMICOLON).length;
				}
				if(jsonArray.getJSONObject(count).get(JSON_MAIL_MESSAGE_TYPE).toString().trim().equals("Inbox"))
				{
					mailType = MailType.MAIL_IN;
				}
				else
				{
					mailType = MailType.MAIL_OUT;
				}
				try{
					crcString = generateCrc(returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_FROM)), 
							returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_TO)),
							returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_CC)),
							returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_SUBJECT)));
				}
				catch(SciException sciExcp)
				{
					log.error("Error Generating CRC checksum");
					return webApiErrorDisplay(new SciServiceException
							(ServiceErrorCodes.MB.ERROR_PARSING_MAIL_FIELDS,sciExcp)) ;
				}
				
				try {
				msgDate = format.parse(jsonArray.getJSONObject(count).get(JSON_MAIL_DATE).toString());
				}
				catch(ParseException parseExcp)
				{
					log.error("Error Parsing date");
					return  webApiErrorDisplay(new SciServiceException
							(ServiceErrorCodes.MB.ERROR_PARSING_MAIL_FIELDS,parseExcp));
				}
				
				
				mailBox = getMailBoxListByMailBoxName(mailBoxName, l_projectid);
				mailInfo = new MBMailInfo( 0,mailBox.getSequenceId(), userInfo.getUserSeqId(), l_projectid,
						returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_MESSAGE_ID)),	
						null,
						returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_SUBJECT)), attachmentCount,
						returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_ATTACHMENT)), msgDate, 
						returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_TO)),
						returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_CC)),
						returnEmptyString(jsonArray.getJSONObject(count).get(JSON_MAIL_FROM)),
						MailRecordMode.MAIL_WEB_API, mailType, crcString,new Date());
			}
			catch(JSONException jsonExcp)
			{
				log.error("Error getting json array value");
				return webApiErrorDisplay(new SciServiceException(ServiceErrorCodes.MB.ERROR_FETCHING_JSON_ARRAY_VALUES, jsonExcp));
			}
			
			if(!isDuplicateMsg(l_projectid, mailBox.getSequenceId(), mailInfo))
			{
				try {
				mailInfo.setMailAppSeqId(ApplicationSequenceHelper.getNextApplicationSequenceNumber(l_projectid, ApplicationSequencePurpose.MAILBOX, mailBox.getSequenceId()));
				}
				catch(SciServiceException sciSerExc)
				{
					log.error("Error getting next application sequence number");
					return webApiErrorDisplay(new SciServiceException(ServiceErrorCodes.ApplicationSequence
							.ERROR_MAX_SEQ_NUMBER, 	sciSerExc));
				}
				saveMBMailWebApi(userInfo, l_projectid, mailInfo, true);
				mailFieldList.add(JSON_MAIL_MESSAGE_ID+": "+mailInfo.getMailMessageId());
				mailFieldList.add(JSON_MAIL_APP_SEQ_ID+": "+mailInfo.getMailAppSeqId());
				mailFieldList.add(JSON_MAIL_STATUS+": "+"Created");
				
			}else
			{
				mailFieldList.add(JSON_MAIL_MESSAGE_ID+": "+mailInfo.getMailMessageId());
				mailFieldList.add(JSON_MAIL_APP_SEQ_ID+": "+mailInfo.getMailAppSeqId());
				mailFieldList.add(JSON_MAIL_STATUS+": "+"Existing");
			}
			mailArray = new String[mailFieldList.size()];
			responceList.add(mailFieldList.toArray(mailArray));
		}
		mailListArray = new String [responceList.size()][];
		return responceList.toArray(mailListArray);
	}
	private static String returnEmptyString(Object obj)
	{
		return (obj != null ? obj.toString() : StringConstants.EMPTY);
	}
	
	public File exportMails(
		int 		p_projectid
	,	int 		mailBoxSeqId
	,	int 		p_startdate
	,	int 		p_enddate
	,	UserSession p_usersession
	) throws SciServiceException {
		File	l_exportfile	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_exportfile	=	MailBoxHomeImpl.exportMails(p_projectid,mailBoxSeqId, p_startdate, p_enddate, p_usersession);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error exporting mails"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Error exporting mails"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.MB.ERROR_FETCHING_MAIL_LIST
			, 	dbExcep
			);
		}
		return l_exportfile;
	}
	
	private static String webApiErrorDisplay(SciServiceException serExcep)
	{
		String sDisplayMessage = null;
		sDisplayMessage = DisplayMessageHelper.getDisplayMessage((SimpleServiceErrorCode)serExcep.errorCode(), serExcep.payload());
		return sDisplayMessage;
	}
}
