package com.sciformix.sciportal.apps;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

class AppRegistryImpl
{
	private static final Logger log = LoggerFactory.getLogger(AppRegistryImpl.class);
	
	private AppRegistryImpl()
	{
		//Nothing to do
	}
	
	public static List<AppInfoDb> loadListOfApps(DbConnection connection) throws SciException
	{
		List<AppInfoDb> listApps = null;
		AppInfoDb oAppInfoDb = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(AppDbSchema.PORTALAPPS, null);
		
		listApps = new ArrayList<AppInfoDb>();
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				//resultSet = connection.executeSelectQuery("SELECT APPID, APPNAME, APPCLASS, WEBAPPCLASS, APPTYPE, APPAVAILTYPE FROM SCFX_SO.SCFX_PORTALAPPS");
				
				while (resultSet.next())
				{
					oAppInfoDb = new AppInfoDb();
					oAppInfoDb.setAppId(resultSet.readString(AppDbSchema.PORTALAPPS.APPID));
					oAppInfoDb.setAppName(resultSet.readString(AppDbSchema.PORTALAPPS.APPNAME));
					oAppInfoDb.setAppClass(resultSet.readString(AppDbSchema.PORTALAPPS.APPCLASS));
					oAppInfoDb.setWebAppClass(resultSet.readString(AppDbSchema.PORTALAPPS.WEBAPPCLASS));
					oAppInfoDb.setAppType(resultSet.readInt(AppDbSchema.PORTALAPPS.APPTYPE));
					oAppInfoDb.setAppAvailType(resultSet.readInt(AppDbSchema.PORTALAPPS.APPAVAILTYPE));
					
					listApps.add(oAppInfoDb);
				}
			}			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error loading list of apps", dbExcep);
			throw new SciException("Error loading list of apps", dbExcep);
		}
		
		return listApps;
	}

}
