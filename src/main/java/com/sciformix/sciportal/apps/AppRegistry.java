package com.sciformix.sciportal.apps;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.sciportal.apps.AppInfo.AppType;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.config.ConfigMetadata;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.web.apps.IPortalWebApp;

public class AppRegistry
{
	private static Map<String, AppInfo> mapRegisteredPortalApps = new HashMap<String, AppInfo>();
	
	public static AppInfo getAppInfo(String p_sAppId)
	{
		return mapRegisteredPortalApps.get(p_sAppId);
	}
	
	public static IPortalApp getApp(String p_sAppId)
	{
		AppInfo oAppInfo = null;
		
		oAppInfo = mapRegisteredPortalApps.get(p_sAppId);
		return (oAppInfo !=null? oAppInfo.getPortalApp() : null);
	}
	
	public static IPortalWebApp getWebApp(String p_sAppId)
	{
		AppInfo oAppInfo = null;
		
		oAppInfo = mapRegisteredPortalApps.get(p_sAppId);
		return (oAppInfo !=null? oAppInfo.getPortalWebApp() : null);
	}
	
	public static List<AppInfo> listBusinessApps()
	{
		return listApps(AppType.BUSINESS);
	}
	
	public static List<AppInfo> listSystemApps()
	{
		return listApps(AppType.SYSTEM);
	}
	
	public static List<AppInfo> listApps(AppType p_nAppType)
	{
		List<AppInfo> listApps = null;
		Iterator<AppInfo> iterApps = null;
		AppInfo oAppInfo = null;
		
		listApps = new ArrayList<AppInfo>();
		
		iterApps = mapRegisteredPortalApps.values().iterator();
		while (iterApps.hasNext())
		{
			oAppInfo = iterApps.next();
			if (oAppInfo.getAppType() == p_nAppType)
			{
				listApps.add(oAppInfo);
			}
		}
		
		return listApps;
	}
	
	public static boolean init(SciStartupLogger oStartupLogger)
	{
		List<AppInfoDb> listApps = null;
		boolean bPortalAppInitErrors = false;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				listApps = AppRegistryImpl.loadListOfApps(connection);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				oStartupLogger.logError("AppRegistry - Error loading list of Apps [" + sciExcep.getMessage() + "]");
				oStartupLogger.logError("AppRegistry initialization aborted");
				return false;
			}
		}
		catch (SciDbException dbExcep)
		{
			oStartupLogger.logError("AppRegistry - Error loading list of Apps [" + dbExcep.getMessage() + "]");
			oStartupLogger.logError("AppRegistry initialization aborted");
			return false;
		}
		
		for (AppInfoDb oAppInfoDb : listApps)
		{
			try
			{
				oStartupLogger.log("App loading started for '" + oAppInfoDb.getAppId() + "'");
				
				mapRegisteredPortalApps.put(oAppInfoDb.getAppId(), new AppInfo(oAppInfoDb));
				oStartupLogger.log("App loading completed for '" + oAppInfoDb.getAppId() + "'");
			}
			catch (SciException sciExcep)
			{
				//Ignore the app initialization error
				oStartupLogger.logError("App loading ERROR for '" + oAppInfoDb.getAppId() + "' [" + sciExcep.getMessage() + "] - skipping it");
				bPortalAppInitErrors = true;
			}
		}
		

		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			connection.startTransaction();
			
			oStartupLogger.log("App Config loading started");
			
			try
			{
				ConfigHome.init(connection, oStartupLogger);
				oStartupLogger.log("App Config loading completed");
			}
			catch (SciException sciExcep)
			{
				//Ignore the app initialization error
				oStartupLogger.logError("App Config loading ERROR for ' [" + sciExcep.getMessage() + "] - skipping it");
				bPortalAppInitErrors = true;
			}
			
			connection.commitTransaction();
			//DEV-NOTE: Error is swallowed here - hence, no rollback scenario exists
		}
		catch (SciDbException dbExcep)
		{
			//Ignore the app initialization error
			oStartupLogger.logError("App Config loading ERROR for ' [" + dbExcep.getMessage() + "] - skipping it");
			bPortalAppInitErrors = true;
		}
		
		if (!bPortalAppInitErrors)
		{
			oStartupLogger.log("AppRegistry initialization completed");
		}
		else
		{
			oStartupLogger.log("AppRegistry initialization completed with one or more errors");
		}
		return true;
	}
	
	public static AppInfo getUserPrefsApp()
	{
		return mapRegisteredPortalApps.get("USRPREF-PORTAL-APP");
	}
	public static AppInfo getSystemConfigurationSettingApp()
	{
		return mapRegisteredPortalApps.get("SYSCONFIG-PORTAL-APP");
	}
	
	
	public static AppInfo getAppSettingsApp()
	{
		return mapRegisteredPortalApps.get("APPSETTINGS-PORTAL-APP");
	}
	
	public static AppInfo getProjSettingsApp()
	{
		return mapRegisteredPortalApps.get("PROJSETTINGS-PORTAL-APP");
	}
	
	public static AppInfo getToolsApp()
	{
		return mapRegisteredPortalApps.get("SOURCE_DOC_UTILS_PORTAL_APP");
	}
	
	public static boolean isAuditTrailViewerApp(String p_sAppId)
	{
		return ("AUDIT-PORTAL-APP".equals(p_sAppId) || "ACTIVITY-PORTAL-APP".equals(p_sAppId));
	}
	
	public static boolean isActivityTrailViewerApp(String p_sAppId)
	{
		return "ACTIVITY-PORTAL-APP".equals(p_sAppId);
	}
	
	public static boolean isSystemAdminApp(String p_sAppId)
	{
		return ("PROJMNG-PORTAL-APP".equals(p_sAppId) || isUserMgmtApp(p_sAppId) || "APPSETTINGS-PORTAL-APP".equals(p_sAppId)
				 || "SYSCONFIG-PORTAL-APP".equals(p_sAppId));
	}
	
	public static boolean isUserMgmtApp(String p_sAppId)
	{
		return ("USRMGMNT-PORTAL-APP".equals(p_sAppId));
	}
	
	public static boolean isProjectAdminApp(String p_sAppId)
	{
		return "PROJSETTINGS-PORTAL-APP".equals(p_sAppId);
	}
	public static boolean isProjectMangmtApp(String p_sAppId)
	{
		return "PROJMNG-PORTAL-APP".equals(p_sAppId);
	}
	public static boolean isProjectAuditor(String p_sAppId)
	{
		return "AUDIT-PORTAL-APP".equals(p_sAppId);
	}
	public static AppInfo getAuditTrailViewerApp()
	{
		return mapRegisteredPortalApps.get("AUDIT-PORTAL-APP");
	}
	public static AppInfo getActivityTrailViewerApp()
	{
		return mapRegisteredPortalApps.get("ACTIVITY-PORTAL-APP");
	}
	public static AppInfo getUserManagementApp()
	{
		return mapRegisteredPortalApps.get("USRMGMNT-PORTAL-APP");
	}
	public static AppInfo getProjectManagementSettingApp()
	{
		return mapRegisteredPortalApps.get("PROJMNG-PORTAL-APP");
	}
	
	public static void dumpContentForDevTrace(SciStartupLogger oStartupLogger)
	{
		oStartupLogger.trace("Dumping AppRegistry read");
		for (AppInfo oAppInfo : mapRegisteredPortalApps.values())
		{
			oStartupLogger.trace("\t" + oAppInfo.getAppId() + "; Name:" + oAppInfo.getAppName() + "; PoratalAppClass:" + oAppInfo.getAppClass() 
					+ "; PortalWebAppClass:" + oAppInfo.getWebAppClass() + "; AppType:" + oAppInfo.getAppType() + "; AppAvailType:" + oAppInfo.getAppAvailType());
		}
		oStartupLogger.trace("Dumping AppRegistry completed");
	}
	
	public static ConfigMetadata getAppConfigMetadata(String p_sKey)
	{
		return ConfigHome.getAppConfigMetadata(p_sKey);
	}
	
	public static Map<String, String> getAppConfigSettings(String p_sAppId)
	{
		return ConfigHome.getAppConfigSettings(p_sAppId);
	}
	
	public static String getGeneralConfigSetting(String p_sKey)
	{
		return ConfigHome.getAppConfigSetting("_GENERAL_", p_sKey);
	}
	
	public static Map<String, String> getGeneralConfigSettings()
	{
		return ConfigHome.getAppConfigSettings("_GENERAL_");
	}
	
}
