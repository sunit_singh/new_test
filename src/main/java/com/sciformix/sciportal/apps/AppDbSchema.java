package com.sciformix.sciportal.apps;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class AppDbSchema
{
	public static PortalApps PORTALAPPS = new PortalApps();
	
	public static class PortalApps extends DbTable
	{
		public DbColumn APPSEQID = null;
		public DbColumn APPID = null;
		public DbColumn APPNAME = null;
		public DbColumn APPCLASS = null;
		public DbColumn WEBAPPCLASS = null;
		public DbColumn APPTYPE = null;
		public DbColumn APPAVAILTYPE = null;
		
		public PortalApps()
		{
			super("CMN_PORTALAPPS");
			setPkSequenceName("CMN_APPSEQID_SEQ");
			
			APPSEQID = addColumn("APPSEQID");
			APPID = addColumn("APPID");
			APPNAME = addColumn("APPNAME");
			APPCLASS = addColumn("APPCLASS");
			WEBAPPCLASS = addColumn("WEBAPPCLASS");
			APPTYPE = addColumn("APPTYPE");
			APPAVAILTYPE = addColumn("APPAVAILTYPE");
		}
	}
	
}
