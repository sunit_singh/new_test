package com.sciformix.sciportal.apps.ngv;

import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.sciportal.content.BinaryContent;

public class NgvTemplate
{
	private int m_nTemplateSeqId = -1;
	private String m_sTemplateName = null;
	private int m_nParentRulesetId = -1;
	private BinaryContent m_oTemplateContent = null;
	private String m_sTemplateCriteria = null;
	
	private RuleSet m_oRuleSet = null;
	
	public static BinaryContent createContent(String p_sContent)
	{
		return BinaryContent.createNew("text", "", "", p_sContent);
	}
	
	public static BinaryContent createHeaderContent(String p_sContent)
	{
		return BinaryContent.createNew("text", "header", "NgvTemplate", p_sContent);
	}
	
	public NgvTemplate()
	{
		m_nTemplateSeqId = -1;
		m_oRuleSet = null;
		
		m_sTemplateName = null;
		
		m_oTemplateContent = null;
	}

	public NgvTemplate(String p_sTemplateName, String p_sTemplateText)
	{
		m_nTemplateSeqId = -1;
		m_oRuleSet = null;
		
		m_sTemplateName = p_sTemplateName;
		
		//TODO:m_nTemplateSeqId is always going to be '-1' here
		m_oTemplateContent = NgvTemplate.createContent(p_sTemplateText); //BinaryContent.createNew("text", ""+m_nTemplateSeqId, "NgvTemplate", p_sTemplateText);
	}

	public int getParentRulesetId()
	{
		return m_nParentRulesetId;
	}
	
	public void setParentRulesetId(int p_nParentRulsetId)
	{
		m_nParentRulesetId = p_nParentRulsetId;
	}
	
	public int getTemplateSeqId() {
		return m_nTemplateSeqId;
	}

	public void setTemplateSeqId(int p_nTemplateSeqId) {
		this.m_nTemplateSeqId = p_nTemplateSeqId;
	}

	public String getTemplateName() {
		return m_sTemplateName;
	}

	public void setTemplateName(String p_sTemplateName) {
		this.m_sTemplateName = p_sTemplateName;
	}

	public BinaryContent getTemplateContent() {
		return m_oTemplateContent;
	}

	public void setTemplateContent(BinaryContent p_oTemplateContent) {
		this.m_oTemplateContent = p_oTemplateContent;
	}

	public String getTemplateText()
	{
		return (m_oTemplateContent != null ? m_oTemplateContent.getContent() : null);
	}
	
	public RuleSet getRuleSet() {
		return m_oRuleSet;
	}

	public void setRuleSet(RuleSet p_oRuleSet) {
		this.m_oRuleSet = p_oRuleSet;
	}

	public String getTemplateCriteria() {
		return m_sTemplateCriteria;
	}

	public void setTemplateCriteria(String p_sTemplateCriteria) {
		this.m_sTemplateCriteria = p_sTemplateCriteria;
	}
	
}
