package com.sciformix.sciportal.apps.ngv;

import java.util.List;

import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.utils.DataTable;

public class NgvAppUtils
{
	public static boolean isTemplateNameUniqueWithinRuleset(NgvTemplateRuleset p_oTemplateRuleset, String p_sTemplateName, int p_nTemplateIdToSkip)
	{
		NgvTemplate oTemplate = null;
		
		oTemplate = p_oTemplateRuleset.getTemplate(p_sTemplateName);
		
		return (oTemplate == null || ((p_nTemplateIdToSkip != -1) && (oTemplate.getTemplateSeqId() == p_nTemplateIdToSkip)));
	}
	
	public static boolean isTemplateCriteriaUniqueWithinRuleset(NgvTemplateRuleset p_oTemplateRuleset, String p_sTemplateCriteria, int p_nTemplateId)
	{
		for (NgvTemplate oTemplate : p_oTemplateRuleset.getTemplateList())
		{
			if (p_nTemplateId != -1 && oTemplate.getTemplateSeqId() == p_nTemplateId)
			{
				continue;
			}
			if (oTemplate.getTemplateCriteria().equalsIgnoreCase(p_sTemplateCriteria))
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean canRulesetBeActivated(NgvTemplateRuleset p_oTemplateRuleset)
	{
		if ((p_oTemplateRuleset == null) || (p_oTemplateRuleset.getTemplateList().size() == 0) 
				|| (p_oTemplateRuleset.getDefaultTemplateSeqId() == -1)  || (p_oTemplateRuleset.getState() != RecordState.UNDER_CONFIGURATION))
		{
			return false;
		}
		return true;
	}
	
	//Added by Rohini
	public static boolean canRulesBeActivated(CaseDataValidationRuleset p_oValidationRuleset)
	{
		if ((p_oValidationRuleset == null) || (p_oValidationRuleset.getValidationRules().size() == 0) 
				|| (p_oValidationRuleset.getState() != RecordState.UNDER_CONFIGURATION))
		{
			return false;
		}
		return true;
	}
	
	//Ended by Rohini
	public static boolean canRulesetBeDeleted(NgvTemplateRuleset p_oTemplateRuleset)
	{
		if ((p_oTemplateRuleset == null) || (p_oTemplateRuleset.getState() == RecordState.DEACTIVATED))
		{
			return false;
		}
		return true;
	}
	//Added by Rohini 
	
	public static boolean isRuleNameUniqueWithinRuleset(CaseDataValidationRuleset p_oValidationRuleset, String p_sRuleName, int p_nRuleIdToSkip)
	{
		CaseDataValidationRule oRule = null;
		
		oRule = p_oValidationRuleset.getRule(p_sRuleName);
		
		return (oRule == null || ((p_nRuleIdToSkip != -1) && (oRule.getRuleSeqId() == p_nRuleIdToSkip)));
	}
	
	public static boolean isRuleeCriteriaUniqueWithinRuleset(CaseDataValidationRuleset p_oValidationRuleset, String p_sRuleCriteria, int p_nRuleId)
	{
		for (CaseDataValidationRule oRule : p_oValidationRuleset.getValidationRules())
		{
			if (p_nRuleId != -1 && oRule.getRuleSeqId() == p_nRuleId)
			{
				continue;
			}
			if (oRule.getCriteria().equalsIgnoreCase(p_sRuleCriteria))
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean canRulesetBeActivated(CaseDataValidationRuleset p_oValidationRuleset)
	{
		if ((p_oValidationRuleset == null) || (p_oValidationRuleset.getValidationRules().size() == 0) 
				|| (p_oValidationRuleset.getState() != RecordState.UNDER_CONFIGURATION))
		{
			return false;
		}
		return true;
	}
	
	public static boolean canRulesetBeDeleted(CaseDataValidationRuleset p_oValidationRuleset)
	{
		if ((p_oValidationRuleset == null) || (p_oValidationRuleset.getState() == RecordState.DEACTIVATED))
		{
			return false;
		}
		return true;
	}
	
	//Ended by Rohini
	
	public static String doMandatoryColumnsExistWithValues(DataTable oDataTable, String sMandatoryColumnsStr) 
	{
		String[] sarrMandatoryColumns = null;
		String sMissingColsOrValues = null;
		List<Object> m_listValues = null;
		
		if(sMandatoryColumnsStr!=null)
		{
			sarrMandatoryColumns = sMandatoryColumnsStr.split(",");
		}
		
		for (int nLookupColIndex = 0; nLookupColIndex < sarrMandatoryColumns.length; nLookupColIndex++)
		{
			m_listValues = oDataTable.getColumnValues(sarrMandatoryColumns[nLookupColIndex]);
			if(!oDataTable.isColumnPresent(sarrMandatoryColumns[nLookupColIndex]))
			{
				sMissingColsOrValues = (sMissingColsOrValues != null ? sMissingColsOrValues + ", " + sarrMandatoryColumns[nLookupColIndex] : sarrMandatoryColumns[nLookupColIndex]);
			}
		/*	else if (m_listValues == null || m_listValues.size() == 0 || m_listValues.get(0) == null)
			{
				sMissingColsOrValues = (sMissingColsOrValues != null ? sMissingColsOrValues + ", " + sarrMandatoryColumns[nLookupColIndex] : sarrMandatoryColumns[nLookupColIndex]);
			}*/
		}
	
		return sMissingColsOrValues;
	}
}
