package com.sciformix.sciportal.apps.ngv;

import java.util.ArrayList;
import java.util.List;

import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.utils.XmlUtils;
import com.sciformix.sciportal.content.BinaryContent;

public class NgvTemplateRuleset
{
	private int m_nRulesetId = -1;
	private String m_sRulesetName = null;
	private BinaryContent m_oContentTemplateHeader = null; 
	private int m_nDefaultTemplateSeqId = -1;
	private RecordState m_eState = null;
	private List<NgvTemplate> m_listTemplates = null;
	
	public NgvTemplateRuleset(String p_sRulesetName)
	{
		m_nRulesetId = -1;
		m_sRulesetName = p_sRulesetName;
		m_eState = RecordState.ACTIVE;
		m_listTemplates = new ArrayList<NgvTemplate>();
	}

	public void setTemplateList(List<NgvTemplate> p_listTemplates)
	{
		m_listTemplates = p_listTemplates;
	}

	public List<NgvTemplate> getTemplateList()
	{
		return m_listTemplates;
	}
	
	public List<String> getTemplateNameList()
	{
		List<String> listNames = null;
		
		listNames = new ArrayList<String>();
		
		for (NgvTemplate oTemplate : m_listTemplates)
		{
			listNames.add(oTemplate.getTemplateName());
		}
		
		return listNames;
	}
	
	public NgvTemplate getTemplate(int p_nTemplateSeqId)
	{
		if (p_nTemplateSeqId == -1)
		{
			return null;
		}
		for (NgvTemplate oTemplate : m_listTemplates)
		{
			if (oTemplate.getTemplateSeqId() == p_nTemplateSeqId)
			{
				return oTemplate;
			}
		}
		return null;
	}
	
	public NgvTemplate getTemplate(String p_sTemplateName)
	{
		for (NgvTemplate oTemplate : m_listTemplates)
		{
			if (oTemplate.getTemplateName().equalsIgnoreCase(p_sTemplateName))
			{
				return oTemplate;
			}
		}
		return null;
	}
	
	public String getTemplateName(int p_nTemplateSeqId)
	{
		if (p_nTemplateSeqId == -1)
		{
			return null;
		}
		for (NgvTemplate oTemplate : m_listTemplates)
		{
			if (oTemplate.getTemplateSeqId() == p_nTemplateSeqId)
			{
				return oTemplate.getTemplateName();
			}
		}
		return null;
	}
	
	public int getRulesetId()
	{
		return m_nRulesetId;
	}
	
	public void setRulesetId(int p_nRulesetId)
	{
		m_nRulesetId = p_nRulesetId;
	}
	
	public void addTemplateToList (
		NgvTemplate	l_template
	) {
		m_listTemplates.add(l_template);
	}
	
	public String getRulesetName()
	{
		return m_sRulesetName;
	}
	
	public void setRulesetName(String p_sRulesetName)
	{
		m_sRulesetName = p_sRulesetName;
	}
	
	public int getDefaultTemplateSeqId()
	{
		return m_nDefaultTemplateSeqId;
	}
	
	public void setDefaultTemplateSeqId(int p_nDefaultTemplateSeqId)
	{
		m_nDefaultTemplateSeqId = p_nDefaultTemplateSeqId;
	}
	
	public String getDefaultTemplateName()
	{
		return getTemplateName(m_nDefaultTemplateSeqId);
	}
	
	public RecordState getState()
	{
		return m_eState;
	}
	
	public void setState(RecordState p_eState)
	{
		m_eState = p_eState;
	}
	
	public void setHeader(String p_sHeaderText)
	{
		m_oContentTemplateHeader = NgvTemplate.createHeaderContent(p_sHeaderText);
	}

	public void setHeaderBinaryContent(BinaryContent p_oContentTemplateHeader)
	{
		m_oContentTemplateHeader = p_oContentTemplateHeader;
	}
	
	public BinaryContent getHeaderBinaryContent()
	{
		return m_oContentTemplateHeader;
	}
	
	public String getHeader()
	{
		return (m_oContentTemplateHeader != null ? m_oContentTemplateHeader.getContent() : null);
	}

	private final static String _ROOT_ELEMENT_TEMPLATERULESET = "TemplateRuleset";
	private final static String _ATTRIB_TEMPLATERULESET_NAME = "TemplateRulesetName";
	private final static String _ATTRIB_TEMPLATERULESET_DEFAULTTEMPLATE = "TemplateRulesetDefaultTemplate";
	private final static String _ATTRIB_TEMPLATERULESET_TEMPLATEHEADER = "TemplateRulesetTemplateHeader";

	private final static String _INTERMEDIATE_ELEMENT_TEMPLATES = "Templates";
	private final static String _INTERMEDIATE_ELEMENT_TEMPLATE = "Template";
	private final static String _ATTRIB_TEMPLATE_NAME = "TemplateName";
	private final static String _ATTRIB_TEMPLATE_CRITERIA = "TemplateCriteria";
	private final static String _ATTRIB_TEMPLATE_CONTENT = "TemplateContent";
	
	
	public String createXml()
	{
		StringBuffer sb = null;
		
		sb = new StringBuffer();
		
		//Start the Xml
		sb.append(XmlUtils.createElementStartingTag(_ROOT_ELEMENT_TEMPLATERULESET));
		
		//TemplateRuleset Attributes
		sb.append(XmlUtils.createElementTag(_ATTRIB_TEMPLATERULESET_NAME, this.m_sRulesetName));
		
		if (m_nDefaultTemplateSeqId != -1)
		{
			for (NgvTemplate oTemplate : m_listTemplates)
			{
				if (oTemplate.getTemplateSeqId() == m_nDefaultTemplateSeqId)
				{
					sb.append(XmlUtils.createElementTag(_ATTRIB_TEMPLATERULESET_DEFAULTTEMPLATE, oTemplate.getTemplateName()));
					break;
				}
			}
		}
		else
		{
			sb.append(XmlUtils.createElementNullTag(_ATTRIB_TEMPLATERULESET_DEFAULTTEMPLATE));
		}
		
		if (m_oContentTemplateHeader != null)
		{
			sb.append(XmlUtils.createElementTag(_ATTRIB_TEMPLATERULESET_TEMPLATEHEADER, this.m_oContentTemplateHeader.getContent(), true));
		}
		else
		{
			sb.append(XmlUtils.createElementNullTag(_ATTRIB_TEMPLATERULESET_TEMPLATEHEADER));
		}
		
		sb.append(XmlUtils.createElementStartingTag(_INTERMEDIATE_ELEMENT_TEMPLATES));
		for (NgvTemplate oTemplate : m_listTemplates)
		{
			sb.append(XmlUtils.createElementStartingTag(_INTERMEDIATE_ELEMENT_TEMPLATE));
			
			sb.append(XmlUtils.createElementTag(_ATTRIB_TEMPLATE_NAME, oTemplate.getTemplateName()));
			
			if (oTemplate.getTemplateContent() != null)
			{
				sb.append(XmlUtils.createElementTag(_ATTRIB_TEMPLATE_CONTENT, oTemplate.getTemplateContent().getContent(), true));
			}
			else
			{
				sb.append(XmlUtils.createElementNullTag(_ATTRIB_TEMPLATE_CONTENT));
			}
			
			sb.append(XmlUtils.createElementTag(_ATTRIB_TEMPLATE_CRITERIA, oTemplate.getTemplateCriteria(), true));
			
			sb.append(XmlUtils.createElementClosingTag(_INTERMEDIATE_ELEMENT_TEMPLATE));
		}		
		sb.append(XmlUtils.createElementClosingTag(_INTERMEDIATE_ELEMENT_TEMPLATES));
		
		//Close the Xml
		sb.append(XmlUtils.createElementClosingTag(_ROOT_ELEMENT_TEMPLATERULESET));
		
		return sb.toString();
	}
	
	/*public static String getRuleSetName(String sStagedFilePath) {
		File inputFile = null;
		DocumentBuilderFactory oDbFactory = null;
		DocumentBuilder oDbBuilder = null;
		Document oDocument = null;
		NodeList listOfNodes = null;
		Element eElement = null;
		Node nNode = null;
		try{
			inputFile = new File(sStagedFilePath);
			oDbFactory  = DocumentBuilderFactory.newInstance();
			oDbBuilder = oDbFactory.newDocumentBuilder();
			oDocument = oDbBuilder.parse(inputFile);
			oDocument.getDocumentElement().normalize();
			listOfNodes = oDocument.getElementsByTagName("TemplateRulesetName");
		}catch(Exception e){
			
		}
		return listOfNodes.item(0).getTextContent();
		
	}
	*/
}
