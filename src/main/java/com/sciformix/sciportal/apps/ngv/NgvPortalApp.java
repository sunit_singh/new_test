package com.sciformix.sciportal.apps.ngv;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.sciformix.commons.CommonAuditHelper;
import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.rulesengine.Rule;
import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.commons.rulesengine.RuleSetUtils;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.commons.utils.OfficeUtils;
import com.sciformix.commons.utils.PdfUtils;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.commons.utils.WorkbookWorkSheetWrapper;
import com.sciformix.sciportal.apps.AppUserPreferenceMetadata;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.apps.ngv.CaseDataValidationRule.Type;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.config.ConfigTplObj;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.user.UserUtils;
import com.sciformix.sciportal.web.PortalAppResponse;

public class NgvPortalApp extends BasePortalApp
{
	private static final Logger log = LoggerFactory.getLogger(NgvPortalApp.class);
	
	private static final String USERPREF_SAFETY_DB_NAMES = "SAFETY-DB-NAMES";
	private static final String USERPREF_TEMPLATERULESET = "TEMPLATE-RULESET";
	private static final String USERPREF_CASEDATAVALIDATIONRULESET = "CASE-DATA-VALIDATION-RULESET";

	private static final String SAFETY_DB_RSS = "RSS";
	private static final String SAFETY_DB_ARGUS = "ARGUS";
	private static final String SAFETY_DB_SCEPTER = "SCEPTER";
	private static final int PROJECT_INDEPENDENT_SEQ_ID = -1;
	
	public static boolean RULE_CRITERIA = false;
	public static boolean RULE_APPLICABILITY = false;
	

	public enum AppConfig
	{
		MULTIPLE_FILE_UPLOAD_ALLOWED("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.multipleFileUploadAllowed"),
		MULTIPLE_FILE_UPLOAD_INITIAL_COUNT("multipleFileUploadInitialCount"),
		MULTIPLE_FILE_UPLOAD_MAX_COUNT("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.multipleFileUploadMaxCount"),
		
		FILE_MAX_ROW_COUNT("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.maxRowCount"),
		FILE_MAX_SIZE_IN_KB("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.inputFileMaxSizeInKB"),
		FILE_EXTENSIONS("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.fileExtensions"),
		FILE_MAX_DATA_ROW_COUNT("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.maxDataRowCount"),
		FILE_DEFAULT_SHEET_INDEX("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.defaultSheetIndex"),
		FILE_HEADER_ROW_PRESENT("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.headerRowPresent"),
		FILE_MANDATORY_COLUMN("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.mandatoryColumns"),
		FILE_MANDATORY_COLUMN_ARGUS("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryColumns"),
		FILE_CASEID_COLUMN_NAME("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.columnCaseId"),
		FILE_COLUMN_NAME_IF_NULL_REPLACED_WITH_UNSPECIFIED("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.replaceWithUnspecifiedIfNull"),
		NARRATIVE_TEXT_EXCLUDE_CHARACTER_REGEX("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.excludeCharRegex"),
		FILE_COLUMN_NAMES_TO_REMOVE_SPECIAL_CHARS("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.columnNamesToRemoveSpecialChars"),
		FILE_COLUMN_CASEID_REGEX("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.caseIdRegex"),
		FILE_COLUMN_J_J_AWARE_DATE_FORMAT("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.jjAwareDateFormat"),
		COLUMNS_FOR_CAUSALITY_MAP("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.columnsForCausalityMap"),
		COLUMNS_FOR_EVENT_MAP("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.columnsForEventMap"),
		SKIP_COLUMNS_COLLAPSE("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.skipColumnCollapse"),
		MERGE_CASE_IDENTIFIER_COLUMNS("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.mergeCaseIdentifierColumns"),
		COLUMNS_FOR_PRODUCT_MAP("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.columnsForProductMap"),
		REGEX_FOR_UPDATE_MARKES("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.updateMarkerSpecialCharRegex"),
		MIXED_CONJUNCTION_SUPPORTED("NGV-PORTAL-APP.criteria.isMixedConjunctionSupported"),
		ENFORCE_AND_OR_SEQUENCE("NGV-PORTAL-APP.criteria.isEnforceAndOrSequence"),
		ENABLE_UPDATE_MARKER("NGV-PORTAL-APP.safetydbRSS.casefile.workbook.enableUpdateMarker"),
		INCOMING_DATE_FORMAT_CSV("NGV-PORTAL-APP.safetydbRSS.casefile.incomingDateFormat.csv"),
		INCOMING_DATE_FORMAT_XLS("NGV-PORTAL-APP.safetydbRSS.casefile.incomingDateFormat.xls"),
		INCOMING_DATE_FORMAT_PDFXML("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.incomingDateFormat"),
		PDFXML_CASEID_COLUMN_NAME("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.columnCaseId"),
		PDFXML_SKIP_TAGS("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.skipTags"),
		PDFXML_DATE_COLUMNS("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.columnsForDates"),
		PDFXML_MANDATORY_FIELD_OFTAG_DRUG("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.drug"),
		PDFXML_MANDATORY_FIELD_OFTAG_REACTION("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.reaction"),
		PDFXML_MANDATORY_FIELD_OFTAG_PATIENT("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.patient"),
		PDFXML_MANDATORY_FIELD_OFTAG_SAFTEYREPORT("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.safetyreport"),
		PDFXML_MANDATORY_FIELD_OFTAG_PRIMARYSOURCE("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.primarysource"),
		PDFXML_MANDATORY_FIELD_OFTAG_REPORTDUPLICATE("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.reportduplicate"),
		PDFXML_MANDATORY_FIELD_OFTAG_ACTIVESUBSTANCE("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.activesubstance"),
		PDFXML_MANDATORY_FIELD_OFTAG_DRUGREACTION_RELATEDNESS("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.drugreactionrelatedness"),
		PDFXML_MANDATORY_FIELD_OFTAG_MEDICALHISTORY_EPISODE("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.mandatoryFieldsOfTag.medicalhistoryepisode"),
		PDFXML_SKIP_COLUMN_COLLAPSE("NGV-PORTAL-APP.safetydbARGUS.casefile.PDFXml.skipColumnCollapse");
		//TODO: CHange the above string variable
		private String value;
		
		AppConfig(String value)
		{
			this.value = value;
		}
		
		public String toString()
		{
			return this.value;
		}
		
		public static AppConfig toEnum(int value)
		{
			AppConfig[] values = AppConfig.values();
			for (int counter = 0; counter < values.length; counter++)
			{
				if (values[counter].toString().equals(value))
				{
					return values[counter];
				}
			}
			throw new IllegalArgumentException("AppConfig: Illegal enum value - " + value);
		}
	}
	
	public static enum LocationToAdd
	{
		END_OF_LIST;
	}
	
	public NgvPortalApp()
	{
		super("NGV-PORTAL-APP", "NgvPortalApp", false);
		
		AppUserPreferenceMetadata.PreferenceValue callbackFunctionTemplateRulesets = new AppUserPreferenceMetadata.PreferenceValue() 
		{
			public List<ObjectIdPair<String, String>> getPreferenceValues(UserSession p_oUserSession)
			{
				List<ObjectIdPair<String, String>> listUserPreferenceValues = null;
				List<String> listPreferenceValues = null;
				
				try (DbConnection connection = DbConnectionManager.getConnection())
				{
					try 
					{
						connection.startTransaction();
						
						if (p_oUserSession != null && p_oUserSession.getUserInfo().isProjectAdmin())
						{
							listPreferenceValues = NgvPortalAppHomeImpl.getTemplateRulesetNames(connection, RecordState.ACTIVE);
							listPreferenceValues.addAll(NgvPortalAppHomeImpl.getTemplateRulesetNames(connection, RecordState.UNDER_CONFIGURATION));
						}
						else
						{
							listPreferenceValues = NgvPortalAppHomeImpl.getTemplateRulesetNames(connection, RecordState.ACTIVE);
						}
						
						connection.commitTransaction();
					} catch (SciException sciExcep) 
					{
						connection.rollbackTransaction();
						log.error(
								"Error retrieving app preference values for NgvPortalApp.callbackFunctionTemplateRulesets",
								sciExcep);
					}
				} catch (SciDbException dbExcep) 
				{
					log.error(
							"Error retrieving app preference values for NgvPortalApp.callbackFunctionTemplateRulesets",
							dbExcep);
				}

				listUserPreferenceValues = (listPreferenceValues != null ? new ArrayList<>(listPreferenceValues.size())
						: new ArrayList<>());
				
				for (String l_pref : listPreferenceValues) {
					listUserPreferenceValues.add(new ObjectIdPair<String, String>(l_pref, l_pref));
				}
				return listUserPreferenceValues;
			}
		};
		
		AppUserPreferenceMetadata.PreferenceValue callbackFunctionCaseDataValidationRulesets = new AppUserPreferenceMetadata.PreferenceValue() 
		{
			public List<ObjectIdPair<String, String>> getPreferenceValues(UserSession p_oUserSession) 
			{
				List<ObjectIdPair<String, String>> listUserPreferenceValues = null;
				List<String> listPreferenceValues = null;

				try (DbConnection connection = DbConnectionManager.getConnection()) 
				{
					try 
					{
						connection.startTransaction();
						if (p_oUserSession != null && p_oUserSession.getUserInfo().isProjectAdmin()) 
						{
							listPreferenceValues = NgvPortalAppHomeImpl.getCaseValidationRulesetNames(connection,
									RecordState.ACTIVE,ConfigTplObjType.CDVR);
							listPreferenceValues.addAll(NgvPortalAppHomeImpl.getCaseValidationRulesetNames(connection,
									RecordState.UNDER_CONFIGURATION,ConfigTplObjType.CDVR));
						} else 
						{
							listPreferenceValues = NgvPortalAppHomeImpl.getCaseValidationRulesetNames(connection,
									RecordState.ACTIVE,ConfigTplObjType.CDVR);
						}
						connection.commitTransaction();
					} catch (SciException sciExcep) 
					{
						connection.rollbackTransaction();
						log.error(
								"Error retrieving app preference values for NgvPortalApp.callbackFunctionTemplateRulesets",
								sciExcep);
					}
				} catch (SciDbException dbExcep) 
				{
					log.error(
							"Error retrieving app preference values for NgvPortalApp.callbackFunctionTemplateRulesets",
							dbExcep);
				}
				
				listUserPreferenceValues = (listPreferenceValues != null ? new ArrayList<>(listPreferenceValues.size())
						: new ArrayList<>());
				
				for (String l_pref : listPreferenceValues) {
					listUserPreferenceValues.add(new ObjectIdPair<String, String>(l_pref, l_pref));
				}
				return listUserPreferenceValues;
			}
		};
		
		//TODO: Check this
		List<ObjectIdPair<String,String>>	l_safetydbprefs	=	new ArrayList<>();
		l_safetydbprefs.add(new ObjectIdPair<String, String>("RSS", "RSS"));
		l_safetydbprefs.add(new ObjectIdPair<String, String>("ARGUS", "ARGUS"));
		addAppUserPreference(AppUserPreferenceMetadata.createSingleValuedPreference(USERPREF_SAFETY_DB_NAMES, "Safety Database", l_safetydbprefs, true));
		addAppUserPreference(AppUserPreferenceMetadata.createSingleValuedPreference(USERPREF_TEMPLATERULESET, "Template Ruleset Name", callbackFunctionTemplateRulesets, true));
		addAppUserPreference(AppUserPreferenceMetadata.createSingleValuedPreference(USERPREF_CASEDATAVALIDATIONRULESET, "Case Data Validation Ruleset Name", callbackFunctionCaseDataValidationRulesets, true));
	}
	
	public String[] getListOfApplicableSafetyDatabasesForGivenUser(UserInfo p_oUserInfo) throws SciServiceException
	{
		try
		{
			return UserUtils.getMultivaluedStringPreferenceValue(p_oUserInfo, getPortalAppId(), USERPREF_SAFETY_DB_NAMES);
		}
		catch(SciException sciExcep)
		{
			log.error("Error getting safety databases for a given user", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.SAFETY_DATABASE_NOT_AVAILABLE, p_oUserInfo.getUserId(), sciExcep);
		}
		
	}
	
	private String getNarrativeTemplateForGivenUser(UserInfo p_oUserInfo) throws SciException
	{
		return UserUtils.getStringPreferenceValue(p_oUserInfo, PROJECT_INDEPENDENT_SEQ_ID,  getPortalAppId(), USERPREF_TEMPLATERULESET);
	}
private String getCaseDataValidationRulesetForGivenUser(UserInfo p_oUserInfo) throws SciException {
		return UserUtils.getStringPreferenceValue(p_oUserInfo, PROJECT_INDEPENDENT_SEQ_ID,  getPortalAppId(), USERPREF_CASEDATAVALIDATIONRULESET);
	}	
	public List<NgvTemplateRuleset> getTemplateRulesets(RecordState p_eState) throws SciServiceException
	{
		List<NgvTemplateRuleset> listTemplateRulesets = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				listTemplateRulesets =  NgvPortalAppHomeImpl.getTemplateRulesets(connection, p_eState);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching template rulesets", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.TEMPLATE_RULESET_FETCH_FAILED, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching template rulesets", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.TEMPLATE_RULESET_FETCH_FAILED, dbExcep);
		}
		return listTemplateRulesets;
	}
	
		public List<CaseDataValidationRuleset> getCaseDataValidationRulesets(int project_id) throws SciServiceException {
		return CaseDataValidationRulesetHelper.getCaseDataValidationRulesets(project_id);
	}
	public NgvTemplateRuleset getTemplateRuleset(int p_nRulesetId) throws SciServiceException
	{
		NgvTemplateRuleset oTemplateRuleset = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				oTemplateRuleset =  NgvPortalAppHomeImpl.getTemplateRuleset(connection, p_nRulesetId);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching template ruleset", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_FETCHING_TEMPLATE_RULESET, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching template ruleset", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_FETCHING_TEMPLATE_RULESET, dbExcep);
		}
		return oTemplateRuleset;
	}
	public ConfigTplObj getCaseDataValidationRuleset(int p_nValidationRulesetId) throws SciServiceException {
		return CaseDataValidationRulesetHelper.getCaseDataValidationRuleset(p_nValidationRulesetId);
	}
	
		//Added by Rohini
	public CaseDataValidationRuleset getCaseDataValidationRuleset(UserInfo p_oUserInfo) throws SciServiceException {
		String sTemplateRulesetToUse = null;
		CaseDataValidationRuleset oCaseValidationRuleset = null;

		try (DbConnection connection = DbConnectionManager.getConnection()) {
			sTemplateRulesetToUse = getCaseDataValidationRulesetForGivenUser(p_oUserInfo);

			if (!StringUtils.isNullOrEmpty(sTemplateRulesetToUse)) {
				oCaseValidationRuleset = NgvPortalAppHomeImpl.getValidationRuleset(connection, sTemplateRulesetToUse);
			}

		} catch (SciException sciExcep) {
			log.error("Error getting narrative template for user", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE,
					p_oUserInfo.getUserId(), sciExcep);
		} catch (SciDbException dbExcep) {
			log.error("Error getting narrative template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE, dbExcep);
		}

		return oCaseValidationRuleset;
	}
	//Ended by Rohini
	/*public String getTemplateToUse(DbConnection connection, UserInfo p_oUserInfo, NgvTemplateRuleset oTemplateRuleset, 
										Map<String, Object> p_mapRuntimeValues) throws SciServiceException
	{
		String sTemplateRulesetToUse = null;
		String sTemplateToUse = null;
		
		sTemplateToUse = getTemplateToUse(connection, p_oUserInfo, oTemplateRuleset, p_mapRuntimeValues);
		
		catch(SciException sciExcep)
		{
			log.error("Error getting narrative template for user - " + p_oUserInfo.getUserId(), sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE, sciExcep, p_oUserInfo.getUserId());
		}
		
		return sTemplateToUse;
	}*/
	
	public NgvTemplateRuleset getTemplateRuleset(UserInfo p_oUserInfo) throws SciServiceException
	{
		String sTemplateRulesetToUse = null;
		NgvTemplateRuleset oTemplateRuleset = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			sTemplateRulesetToUse = getNarrativeTemplateForGivenUser(p_oUserInfo);
			
			if(!StringUtils.isNullOrEmpty(sTemplateRulesetToUse))
			{
				oTemplateRuleset = NgvPortalAppHomeImpl.getTemplateRuleset(connection, sTemplateRulesetToUse);
			}
		
		}
		catch(SciException sciExcep)
		{
			log.error("Error getting narrative template for user", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE, p_oUserInfo.getUserId(), sciExcep);
		} 
		catch (SciDbException dbExcep) 
		{
			log.error("Error getting narrative template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE, dbExcep);
		}
		
		return oTemplateRuleset;
	}
	
	
	/*public NgvTemplateRuleset getTemplateRuleset(String p_sRulesetName) throws SciServiceException
	{
		NgvTemplateRuleset oTemplateRuleset = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				oTemplateRuleset =  NgvPortalAppHomeImpl.getTemplateRuleset(connection, p_sRulesetName);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				log.error("Error getting narrative template ruleset - " + p_sRulesetName, sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_RULESET_NOT_AVAILABLE, sciExcep, p_sRulesetName);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error getting narrative template ruleset - " + p_sRulesetName, dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_RULESET_NOT_AVAILABLE, dbExcep, p_sRulesetName);
		}
		return oTemplateRuleset;
	}*/
	
	public static List<String> getTemplateToUse(DbConnection connection, UserInfo p_oUserInfo, NgvTemplateRuleset p_oTemplateRuleset, 
											Map<String, Object> p_mapRuntimeValues) throws SciException
	{
		RuleSet oRuleset = null;
		List<NgvTemplate> listTemplates = null;
		String sTemplateHeader = null;
		List<String> listTemp = null;
		sTemplateHeader = p_oTemplateRuleset.getHeader();
		sTemplateHeader = (sTemplateHeader != null ? sTemplateHeader : "");
		
		listTemplates = NgvPortalAppHomeImpl.listTemplates(connection, p_oTemplateRuleset);
		
		for (NgvTemplate oTemplate : listTemplates)
		{
			oRuleset = RuleSetUtils.createRuleSetFromCriteria(oTemplate.getTemplateCriteria());
			
			for (Rule oEvalRule : oRuleset)
			{
				if (oEvalRule.evalute(p_mapRuntimeValues))
				{
					listTemp = new ArrayList<>();
					listTemp.add(sTemplateHeader + oTemplate.getTemplateContent().getContent());
					listTemp.add(oTemplate.getTemplateName());
					return listTemp;
					//return sTemplateHeader + oTemplate.getTemplateContent().getContent();
				}
			}
		}
		
		//Use Default Template
		if (p_oTemplateRuleset.getDefaultTemplateSeqId() != -1)
		{
			for (NgvTemplate oTemplate : listTemplates)
			{
				if (oTemplate.getTemplateSeqId() == p_oTemplateRuleset.getDefaultTemplateSeqId())
				{
					listTemp = new ArrayList<>();
					listTemp.add(sTemplateHeader + oTemplate.getTemplateContent().getContent());
					listTemp.add(oTemplate.getTemplateName());
					return listTemp;
					//return sTemplateHeader + oTemplate.getTemplateContent().getContent();
				}
			}			
		}
		
		return null;
	}
	
//Added by Rohini for Case Validation 
		public Map<String, List<String>> validateRule(UserInfo p_oUserInfo, CaseDataValidationRuleset p_oCaseValidationRuleset, String jsonRepresentation,PortalAppResponse oAppResponse) throws SciException, SciDbException, SciServiceException
		{
			boolean flag = false;
			RuleSet oRulesetCheckCriteria = null;

			RuleSet oRulesetApplicability = null;

			List<CaseDataValidationRule> listCaseDataValidationRules = null;
			Map<String,List<String>> validHmap = new HashMap<String,List<String>>();

					
			List<String> ValidCaseSatidfiedList = new ArrayList<String>();
			
			List<String> ValidCaseNotSatidfiedList = new ArrayList<String>();
			
			List<String> ErrorCaseSatidfiedList = new ArrayList<String>();
			
			List<String> ErrorCaseNotSatidfiedList = new ArrayList<String>();
			
			List<String> WarningCaseSatidfiedList = new ArrayList<String>();
			
			List<String> WarningCaseNotSatidfiedList = new ArrayList<String>();

			try(DbConnection connection = DbConnectionManager.getConnection())
			{
			listCaseDataValidationRules = NgvPortalAppHomeImpl.listRules(connection, p_oCaseValidationRuleset);
			}
			catch (SciDbException dbExcep)
			{
				log.error("Error fetching the list of Rules from Database", dbExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_FETCHING_LIST_OF_RULES_FROM_DB, dbExcep);
			}
			
			boolean bIsCaseDataValidationRuleApplicable = false;
			boolean bIsCheckCriteriaSatisfied = false;

			Map<String,Object> mapCurrentCaseDataRuntimeValues = convertJsonIntoMap(jsonRepresentation, false);

			for (CaseDataValidationRule oCaseDataValidationRule : listCaseDataValidationRules)
			{
				//Step 1: Check if the current Case Data Validation Rule is applicable to the current case
				if(oCaseDataValidationRule.getApplicability().equalsIgnoreCase("all"))
				{
					bIsCaseDataValidationRuleApplicable = true;
				}
				else
				{
					oRulesetApplicability = RuleSetUtils.createRuleSetFromCriteria(oCaseDataValidationRule.getApplicability());
					//TODO:Review this for AND/OR
					bIsCaseDataValidationRuleApplicable = true;
					for (Rule oEvalRuleApplicability : oRulesetApplicability)
					{
						if (!oEvalRuleApplicability.evalute(mapCurrentCaseDataRuntimeValues))
						{
							bIsCaseDataValidationRuleApplicable = false;
						}
					}
				}
				
				if (!bIsCaseDataValidationRuleApplicable)
				{
					//This Validation Rule DOES NOT apply to the current Case, hence skip it
					continue;
				}
				
				//Current Case Data Validation Rule IS APPLICABLE to the current case
				//Step 2: Check if the Check Criteria is satisfied or not (evaluates to 'true' or 'false'
				oRulesetCheckCriteria = RuleSetUtils.createRuleSetFromCriteria(oCaseDataValidationRule.getCriteria());
				bIsCheckCriteriaSatisfied = true;
				
				for (Rule oEvalRuleCheckCriteria : oRulesetCheckCriteria)
				{
					boolean sLhsOperand = false;
					
				    oEvalRuleCheckCriteria.checkForColumn(mapCurrentCaseDataRuntimeValues);
				   
				   
					if (!oEvalRuleCheckCriteria.evalute(mapCurrentCaseDataRuntimeValues))
					{
						
						bIsCheckCriteriaSatisfied = false;
					}
				}
				
				
				//Step 3: Based on the type (validity, error, warning) of the 'Check Criteria' AND evaluation result of the previous step
				//			Determine the action to take
				if (oCaseDataValidationRule.getRuleType().name().equalsIgnoreCase("validity"))
				{
					if (bIsCheckCriteriaSatisfied)
					{
						//This VALIDITY-criteria Case Data Validation Rule is satisfied, GREEN CASE
						
						ValidCaseSatidfiedList.add(oCaseDataValidationRule.getRuleName());
						validHmap.put("VALID-CASE-SATISFIED",ValidCaseSatidfiedList);
					}
					else
					{
						//This VALIDITY-criteria Case Data Validation Rule is not satisfied, ERROR CASE
						//Add ERROR flag for the case data validation rule named 'oCaseDataValidationRule.getM_sRuleeName()'
						//TODO:
						
						ValidCaseNotSatidfiedList.add(oCaseDataValidationRule.getRuleName());
						validHmap.put("VALID-CASE-NOTSATISFIED",ValidCaseNotSatidfiedList);
					}
				}
				else if (oCaseDataValidationRule.getRuleType().name().equalsIgnoreCase("error"))
				{
					if (bIsCheckCriteriaSatisfied)
					{
						//This ERROR-criteria Case Data Validation Rule is satisfied, ERROR CASE
						//Add ERROR flag for the case data validation rule named 'oCaseDataValidationRule.getM_sRuleeName()'
						//TODO:
					
						ErrorCaseSatidfiedList.add(oCaseDataValidationRule.getRuleName());
						validHmap.put("ERROR-CASE-SATISFIED",ErrorCaseSatidfiedList);
					}
					else
					{
						//This VALIDITY-criteria Case Data Validation Rule is not satisfied, GREEN CASE
						
						ErrorCaseNotSatidfiedList.add(oCaseDataValidationRule.getRuleName());
						validHmap.put("ERROR-CASE-NOTSATISFIED",ErrorCaseNotSatidfiedList);
					}
				}
				else if (oCaseDataValidationRule.getRuleType().name().equalsIgnoreCase("warning"))
				{
					if (bIsCheckCriteriaSatisfied)
					{
						//This WARNING-criteria Case Data Validation Rule is satisfied, ERROR CASE
						//Add WARNING flag for the case data validation rule named 'oCaseDataValidationRule.getM_sRuleeName()'
						//TODO:
						
						WarningCaseSatidfiedList.add(oCaseDataValidationRule.getRuleName());
						validHmap.put("WARN-CASE-SATISFIED",WarningCaseSatidfiedList);
					}
					else
					{
						//This VALIDITY-criteria Case Data Validation Rule is not satisfied, GREEN CASE
						
						WarningCaseNotSatidfiedList.add(oCaseDataValidationRule.getRuleName());
						validHmap.put("WARN-CASE-NOTSATISFIED",WarningCaseNotSatidfiedList);
					}
				}
						
			}	
						
			
		return validHmap;
		
		}

	public void setDefaultTemplate(UserSession p_oUserSession, NgvTemplateRuleset p_oTemplateRuleset, int p_nNewDefaultTemplateId) throws SciServiceException
	{
		//Step 1: Check if there is anything to change
		if (!DataUtils.hasChanged(p_oTemplateRuleset.getDefaultTemplateSeqId(), p_nNewDefaultTemplateId))
		{
			//No change detected - simply return
			return;
		}
		
		//Step 2: If changes present, record the change for audit usage
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Default Template changed");
		oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_ID, p_oTemplateRuleset.getRulesetId());
		oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_NAME, p_oTemplateRuleset.getRulesetName());
		
		oAuditInfo.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_DEFAULT_TEMPLATE, p_oTemplateRuleset.getDefaultTemplateName(), p_oTemplateRuleset.getTemplateName(p_nNewDefaultTemplateId));
		
		
		//Step 3: Set the new value and persist the same with audit trail
		p_oTemplateRuleset.setDefaultTemplateSeqId(p_nNewDefaultTemplateId);
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				NgvPortalAppHomeImpl.saveTemplateRuleset(connection, p_oUserSession.getUserInfo(), p_oTemplateRuleset, true, false, false);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving default template", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_DEFAULT_TEMPLATE, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error saving default template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_DEFAULT_TEMPLATE, dbExcep);
		}
	}
	
	public void updateTemplateRulesetState(UserSession p_oUserSession, NgvTemplateRuleset p_oTemplateRuleset, RecordState p_eNewState) throws SciServiceException
	{
		//Step 1: Check if there is anything to change
		if (p_oTemplateRuleset.getState() == p_eNewState)
		{
			//No change detected - simply return
			return;
		}
		if (!RecordState.isCorrectWorkflowState(p_oTemplateRuleset.getState(), p_eNewState))
		{
			//Change not permitted - simply return
			return;
		}
		
		
		//Step 2: If changes present, record the change for audit usage
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Template Ruleset state changed");
		oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_ID, p_oTemplateRuleset.getRulesetId());
		oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_NAME, p_oTemplateRuleset.getRulesetName());
		
		oAuditInfo.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_STATE, p_oTemplateRuleset.getState().displayName(), p_eNewState.displayName());
		
		//Step 3: Set the new value and persist the same with audit trail
		p_oTemplateRuleset.setState(p_eNewState);
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				NgvPortalAppHomeImpl.saveTemplateRuleset(connection, p_oUserSession.getUserInfo(), p_oTemplateRuleset, true, false, false);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating template ruleset state", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_UPDATING_TEMPLATERULESET_STATE, p_oTemplateRuleset.getRulesetName(), sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error updating template ruleset state", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_UPDATING_TEMPLATERULESET_STATE, dbExcep);
		}
	}
	//Added by Rohini
	public void updateValidationRulesetState(UserSession p_oUserSession, CaseDataValidationRuleset p_oValidationRuleset,
			RecordState p_eNewState) throws SciServiceException {
		// Step 1: Check if there is anything to change
		if (p_oValidationRuleset.getState() == p_eNewState) {
			// No change detected - simply return
			return;
		}
		if (!RecordState.isCorrectWorkflowState(p_oValidationRuleset.getState(), p_eNewState)) {
			// Change not permitted - simply return
			return;
		}

		// Step 2: If changes present, record the change for audit usage
		AuditInfoDb oAuditInfo = null;

		oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(),
				"Case Data Validation Ruleset state changed");
		//TODO:Add Project Id
		oAuditInfo.addPayload(CommonAuditHelper.AuditAttributes.CASE_VALIDATION_RULESET_ID, p_oValidationRuleset.getSequenceId());
		oAuditInfo.addPayload(CommonAuditHelper.AuditAttributes.CASE_VALIDATION_RULESET_NAME,
				p_oValidationRuleset.getTemplateName());

		oAuditInfo.addDetails(CommonAuditHelper.AuditAttributes.CASE_VALIDATION_RULESET_STATE,
				p_oValidationRuleset.getState().displayName(), p_eNewState.displayName());

		// Step 3: Set the new value and persist the same with audit trail
		p_oValidationRuleset.setState(p_eNewState);

		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();

			
				ConfigTplObjHomeImpl.updateConfigTplObjState(connection, p_oValidationRuleset.getSequenceId(),p_eNewState , p_oUserSession.getUserInfo());
				AuditHome.savePopulatedStub(connection, oAuditInfo);

				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error updating template ruleset state", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_UPDATING_TEMPLATERULESET_STATE,
						p_oValidationRuleset.getTemplateName(), sciExcep);
			}
		} catch (SciDbException dbExcep) {
			log.error("Error updating template ruleset state", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_UPDATING_TEMPLATERULESET_STATE, dbExcep);
		}
	}
	//Ended by Rohini
	
	public void setHeaderTemplate(UserSession p_oUserSession, NgvTemplateRuleset p_oTemplateRuleset, String p_sNewTemplateHeader) throws SciServiceException
	{
		//Step 1: Check if there is anything to change
		if (!DataUtils.hasChanged(p_oTemplateRuleset.getHeader(), p_sNewTemplateHeader))
		{
			//No change detected - simply return
			return;
		}
		
		
		//Step 2: If changes present, record the change for audit usage
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Template Header changed");
		oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_ID, p_oTemplateRuleset.getRulesetId());
		oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_NAME, p_oTemplateRuleset.getRulesetName());
		
		oAuditInfo.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_HEADER, p_oTemplateRuleset.getHeader(), p_sNewTemplateHeader);
		
		
		//Step 3: Set the new value and persist the same with audit trail
		p_oTemplateRuleset.setHeader(p_sNewTemplateHeader);
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				NgvPortalAppHomeImpl.saveTemplateRuleset(connection, p_oUserSession.getUserInfo(), p_oTemplateRuleset, false, true, false);
				
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving template header", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE_HEADER, sciExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error saving template header", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE_HEADER, dbExcep);
		}
	}
	
	public void saveTemplateRuleset(UserSession p_oUserSession, NgvTemplateRuleset p_oTemplateRuleset, String p_sImportFileName) throws SciServiceException
	{
		AuditInfoDb oAuditInfo = null;
		
		oAuditInfo = AuditHome.createRecordInsertStub(p_oUserSession, this.getPortalAppId(), "Template Ruleset created");
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try 
			{
				connection.startTransaction();
				
				NgvPortalAppHomeImpl.saveNewTemplateRuleset(connection, p_oUserSession.getUserInfo(), p_oTemplateRuleset);
				
				oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_ID, p_oTemplateRuleset.getRulesetId());
				oAuditInfo.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_NAME, p_oTemplateRuleset.getRulesetName());
				
				if(p_sImportFileName!=null)
				{	
					oAuditInfo.addPayload("Import file name", p_sImportFileName);
				}
				AuditHome.savePopulatedStub(connection, oAuditInfo);
				
				connection.commitTransaction();
			}
			catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving template ruleset", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE_RULESET, sciExcep);
			}
		}
		catch(SciDbException dbExcep)
		{
			log.error("Error saving template ruleset", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE_RULESET, dbExcep);
		}
	}
//Added by Rohini
	public CaseDataValidationRuleset saveCaseDataValidationRuleset(UserSession p_oUserSession, int project_Id,String TemplateName,CaseDataValidationRuleset oCaseValidationRuleSet) throws SciServiceException {
		AuditInfoDb oAuditInfo = null;

		oAuditInfo = AuditHome.createRecordInsertStub(p_oUserSession, this.getPortalAppId(),
				"Case Data Validation Ruleset created");
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				oCaseValidationRuleSet = new CaseDataValidationRuleset();
				oCaseValidationRuleSet.setCreationMode(CreationMode.SCRATCH);	
				oCaseValidationRuleSet.setProjectId(project_Id);
				oCaseValidationRuleSet.setTemplateName(TemplateName);
				oCaseValidationRuleSet.setState(RecordState.UNDER_CONFIGURATION);
				
				
			ConfigTplObjHomeImpl.saveConfigTplObj(connection, oCaseValidationRuleSet,p_oUserSession.getUserInfo());

				//TODO:Add Project Id
				oAuditInfo.addPayload(CommonAuditHelper.AuditAttributes.CASE_VALIDATION_RULESET_ID,
						oCaseValidationRuleSet.getSequenceId());
				oAuditInfo.addPayload(CommonAuditHelper.AuditAttributes.CASE_VALIDATION_RULESET_NAME,
						oCaseValidationRuleSet.getTemplateName());

				AuditHome.savePopulatedStub(connection, oAuditInfo);
				oCaseValidationRuleSet.setSequenceId(oCaseValidationRuleSet.getSequenceId());
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error saving template ruleset", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE_RULESET, sciExcep);
			}
		} catch (SciDbException dbExcep) {
			log.error("Error saving template ruleset", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE_RULESET, dbExcep);
		}
		return 	oCaseValidationRuleSet;
	}
	//Ended by Rohini
	
	public void addTemplate(UserSession p_oUserSession, NgvTemplateRuleset p_oTemplateRuleset, NgvTemplate p_oTemplate,
   LocationToAdd p_oLocationToAdd) throws SciServiceException
	{
		boolean bFirstTemplateOfRuleset = false;
		int nNewTemplateSequenceNum = -1;
		AuditInfoDb oAuditInfoTpl = null;
		AuditInfoDb oAuditInfoTplRs = null;
		
		oAuditInfoTpl = AuditHome.createRecordInsertStub(p_oUserSession, this.getPortalAppId(), "Template created");
		
		oAuditInfoTplRs = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Template added");
		
		bFirstTemplateOfRuleset = (p_oTemplateRuleset.getTemplateList().size() == 0);
		
		if (bFirstTemplateOfRuleset)
		{
			oAuditInfoTplRs.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_TEMPLATE_LIST, null, p_oTemplate.getTemplateName());
			oAuditInfoTplRs.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_DEFAULT_TEMPLATE, p_oTemplateRuleset.getDefaultTemplateName(), p_oTemplate.getTemplateName());
		}
		else
		{
			oAuditInfoTplRs.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_TEMPLATE_LIST, StringUtils.serialize(p_oTemplateRuleset.getTemplateNameList(), StringConstants.COMMA), StringUtils.serialize(p_oTemplateRuleset.getTemplateNameList(), StringConstants.COMMA) + "," + p_oTemplate.getTemplateName());
		}
		
		if (!bFirstTemplateOfRuleset)
		{
			if (p_oLocationToAdd == LocationToAdd.END_OF_LIST)
			{
				nNewTemplateSequenceNum = (p_oTemplateRuleset.getTemplateList().size() + 1);
			}
		}
		else
		{
			nNewTemplateSequenceNum = 1;
		}
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try 
			{
				connection.startTransaction();
				
				NgvPortalAppHomeImpl.saveNewTemplate(connection, p_oUserSession.getUserInfo(), p_oTemplate, nNewTemplateSequenceNum);
				
				if (bFirstTemplateOfRuleset)
				{
					//DEV-NOTE: Make the first template of the ruleset as the 'default template'
					p_oTemplateRuleset.setDefaultTemplateSeqId(p_oTemplate.getTemplateSeqId());
					NgvPortalAppHomeImpl.saveTemplateRuleset(connection, p_oUserSession.getUserInfo(), p_oTemplateRuleset, true, false, false);
				}
				
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_ID, p_oTemplateRuleset.getRulesetId());
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_NAME, p_oTemplateRuleset.getRulesetName());
				
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_ID, p_oTemplate.getTemplateSeqId());
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_NAME, p_oTemplate.getTemplateName());
				
				AuditHome.savePopulatedStub(connection, oAuditInfoTpl);
				
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_ID, p_oTemplateRuleset.getRulesetId());
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_NAME, p_oTemplateRuleset.getRulesetName());
				
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_ID, p_oTemplate.getTemplateSeqId());
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_NAME, p_oTemplate.getTemplateName());
				
				AuditHome.savePopulatedStub(connection, oAuditInfoTplRs);
				
				connection.commitTransaction();
			}
			catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving template", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, sciExcep);
			}
		}
		catch(SciDbException dbExcep)
		{
			log.error("Error saving template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, dbExcep);
		}
		p_oTemplateRuleset.addTemplateToList(p_oTemplate);
	}

	public void addRule(UserSession p_oUserSession, CaseDataValidationRuleset p_oCaseValidationRuleset, CaseDataValidationRule p_oRule,
			LocationToAdd p_oLocationToAdd) throws SciServiceException
	{
		
		boolean bFirstTemplateOfRuleset = false;
		int nNewTemplateSequenceNum = -1;
		AuditInfoDb oAuditInfoTpl = null;
		AuditInfoDb oAuditInfoTplRs = null;
		
		oAuditInfoTpl = AuditHome.createRecordInsertStub(p_oUserSession, this.getPortalAppId(), "Rule created");
		
		oAuditInfoTplRs = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Rule added");
		
		bFirstTemplateOfRuleset = (p_oCaseValidationRuleset.getRuleNameList().size() == 0);
		
		if (bFirstTemplateOfRuleset)
		{
			oAuditInfoTplRs.addDetails(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_CASE_LIST, null, p_oRule.getRuleName());
		}
		else
		{
			oAuditInfoTplRs.addDetails(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_CASE_LIST, StringUtils.serialize(p_oCaseValidationRuleset.getRuleNameList(), StringConstants.COMMA), StringUtils.serialize(p_oCaseValidationRuleset.getRuleNameList(), StringConstants.COMMA) + "," + p_oRule.getRuleName());
		}
		
		if (!bFirstTemplateOfRuleset)
		{
			if (p_oLocationToAdd == LocationToAdd.END_OF_LIST)
			{
				nNewTemplateSequenceNum = (p_oCaseValidationRuleset.getRuleNameList().size() + 1);
			}
		}
		else
		{
			nNewTemplateSequenceNum = 1;
		}
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try 
			{
				
				connection.startTransaction();
				
				
				NgvPortalAppHomeImpl.saveNewRule(connection, p_oUserSession.getUserInfo(), p_oRule, nNewTemplateSequenceNum,p_oCaseValidationRuleset.getSequenceId());
				
				if (bFirstTemplateOfRuleset)
				{
					//DEV-NOTE: Make the first template of the ruleset as the 'default template'
					
					NgvPortalAppHomeImpl.saveCaseDataValidationRuleset(connection, p_oUserSession.getUserInfo(), p_oCaseValidationRuleset, true, false, false);
				}
				
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_ID, p_oCaseValidationRuleset.getSequenceId());
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_NAME, p_oCaseValidationRuleset.getTemplateName());
				
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.RULE_ID, p_oRule.getRuleSeqId());
				oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.RULE_NAME, p_oRule.getRuleName());
				
				AuditHome.savePopulatedStub(connection, oAuditInfoTpl);
				
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_ID, p_oCaseValidationRuleset.getSequenceId());
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_NAME, p_oCaseValidationRuleset.getTemplateName());
				
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.RULE_ID, p_oRule.getRuleSeqId());
				oAuditInfoTplRs.addPayload(NgvAuditHelper.AuditAttributes.RULE_NAME, p_oRule.getRuleName());
				
				AuditHome.savePopulatedStub(connection, oAuditInfoTplRs);
				
				connection.commitTransaction();
			}
			catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving template", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, sciExcep);
			}
		}
		catch(SciDbException dbExcep)
		{
			log.error("Error saving template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, dbExcep);
		}
		p_oCaseValidationRuleset.addValidationRule(p_oRule);
	}
	//Ended by Rohini
	
	// Ended by Rohini
	
	public void modifyTemplate(UserSession p_oUserSession, NgvTemplateRuleset p_oTemplateRuleset, NgvTemplate p_oTemplate,
			String p_sNewCriteria, String p_sNewContent) throws SciServiceException
	{
		boolean bCriteriaHasChanged = false;
		boolean bContentHasChanged = false;
		
		//Step 1: Check if there is anything to change
		bCriteriaHasChanged = DataUtils.hasChanged(p_oTemplate.getTemplateCriteria(), p_sNewCriteria);
		bContentHasChanged = DataUtils.hasChanged(p_oTemplate.getTemplateText(), p_sNewContent);
		
		if (!bCriteriaHasChanged && !bContentHasChanged)
		{
			//No change detected - simply return
			return;
		}
			
		//Step 2: If changes present, record the change for audit usage
		AuditInfoDb oAuditInfoTpl = null;
		
		oAuditInfoTpl = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Template modified");
		oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_ID, p_oTemplateRuleset.getRulesetId());
		oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_RULESET_NAME, p_oTemplateRuleset.getRulesetName());
		
		oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_ID, p_oTemplate.getTemplateSeqId());
		oAuditInfoTpl.addPayload(NgvAuditHelper.AuditAttributes.TEMPLATE_NAME, p_oTemplate.getTemplateName());
		
		if (bCriteriaHasChanged)
		{
			oAuditInfoTpl.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_CRITERIA, p_oTemplate.getTemplateCriteria(), p_sNewCriteria);
		}
		if (bContentHasChanged)
		{
			oAuditInfoTpl.addDetails(NgvAuditHelper.AuditAttributes.TEMPLATE_CONTENT, p_oTemplate.getTemplateText(), p_sNewContent);
		}
		
		//Step 3: Set the new value and persist the same with audit trail
		if (bCriteriaHasChanged)
		{
			p_oTemplate.setTemplateCriteria(p_sNewCriteria);
		}
		if (bContentHasChanged)
		{
			p_oTemplate.setTemplateContent(NgvTemplate.createContent(p_sNewContent));
		}
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try 
			{
				connection.startTransaction();
				
				NgvPortalAppHomeImpl.saveTemplate(connection, p_oUserSession.getUserInfo(), p_oTemplate, p_oTemplateRuleset, bCriteriaHasChanged, bContentHasChanged);
				
				AuditHome.savePopulatedStub(connection, oAuditInfoTpl);
				
				connection.commitTransaction();
			}
			catch(SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving template", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, sciExcep);
			}
		}
		catch(SciDbException dbExcep)
		{
			log.error("Error saving template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, dbExcep);
		}
	}
		// Added by Rohini
	public void modifyCaseDataValidationRule(UserSession p_oUserSession, CaseDataValidationRuleset oValidationRuleSet, CaseDataValidationRule p_oRule,
			Type ruletype, String ruleCriteria, String ruleApplicability) throws SciServiceException {
		
		boolean bRuleTypeHasChanged = false;
		boolean bCriteriaHasChanged = false;
		boolean bApplicabilityHasChanged = false;
		

		// Step 1: Check if there is anything to change
		
		bRuleTypeHasChanged = CaseDataValidationRule.hasChanged(p_oRule.getRuleType(), ruletype);
		bCriteriaHasChanged = DataUtils.hasChanged(p_oRule.getCriteria(), ruleCriteria);
		bApplicabilityHasChanged = DataUtils.hasChanged(p_oRule.getApplicability(), ruleApplicability);

		if (!bRuleTypeHasChanged && !bCriteriaHasChanged && !bApplicabilityHasChanged) {
			// No change detected - simply return
			return;
		}

		// Step 2: If changes present, record the change for audit usage
		AuditInfoDb oAuditInfoRule = null;

		oAuditInfoRule = AuditHome.createRecordUpdateStub(p_oUserSession, this.getPortalAppId(), "Rule modified");
		oAuditInfoRule.addPayload(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_ID, oValidationRuleSet.getSequenceId());
		oAuditInfoRule.addPayload(NgvAuditHelper.AuditAttributes.VALIDATION_RULESET_NAME,
				oValidationRuleSet.getTemplateName());

		oAuditInfoRule.addPayload(NgvAuditHelper.AuditAttributes.RULE_ID, p_oRule.getRuleSeqId());
		oAuditInfoRule.addPayload(NgvAuditHelper.AuditAttributes.RULE_NAME, p_oRule.getRuleName());

		if (bRuleTypeHasChanged) {
			oAuditInfoRule.addDetails(NgvAuditHelper.AuditAttributes.RULE_TYPE, p_oRule.getRuleType(), ruletype);
		}
		if (bCriteriaHasChanged) {
			oAuditInfoRule.addDetails(NgvAuditHelper.AuditAttributes.CRITERIA, p_oRule.getCriteria(), ruleCriteria);
		}
		if (bApplicabilityHasChanged) {
			oAuditInfoRule.addDetails(NgvAuditHelper.AuditAttributes.APPLICABILITY, p_oRule.getApplicability(),
					ruleApplicability);
		}

		// Step 3: Set the new value and persist the same with audit trail
		if(bRuleTypeHasChanged)
		{
			p_oRule.setRuleType(ruletype);
		}
		if (bCriteriaHasChanged) {
			p_oRule.setCriteria(ruleCriteria);
		}
		if (bApplicabilityHasChanged) {
			p_oRule.setApplicability(ruleApplicability);
		}

		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				
				
				NgvPortalAppHomeImpl.saveRule(connection, p_oUserSession.getUserInfo(), p_oRule, oValidationRuleSet, bCriteriaHasChanged, bApplicabilityHasChanged);

				AuditHome.savePopulatedStub(connection, oAuditInfoRule);

				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error saving template", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, sciExcep);
			}
		} catch (SciDbException dbExcep) {
			log.error("Error saving template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_SAVING_TEMPLATE, dbExcep);
		}
	}
	
	public boolean isValidationRulesetNameUnique(String p_sProposedName,int l_projectid,ConfigTplObjType sRuleSetType) throws SciServiceException {
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			return ConfigTplObjHomeImpl.isConfigTplObjNameUnique(connection, l_projectid, sRuleSetType, p_sProposedName);
		} catch (SciException sciExcep) {
			log.error("Error checking template ruleset uniqueness", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_CHECKING_TEMPLATE_RULESET_NAME_UNIQUENESS,
					sciExcep);
		} catch (SciDbException dbExcep) {
			log.error("Error checking template ruleset uniqueness", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_CHECKING_TEMPLATE_RULESET_NAME_UNIQUENESS,
					dbExcep);
		}
	}

	// Ended by Rohini
	public boolean isTemplateRulesetNameUnique(String p_sProposedName) throws SciServiceException
	{
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			return NgvPortalAppHomeImpl.isTemplateRulesetNameUnique(connection, p_sProposedName);
		}
		catch (SciException sciExcep)
		{
			log.error("Error checking template ruleset uniqueness", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_CHECKING_TEMPLATE_RULESET_NAME_UNIQUENESS, sciExcep);
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error checking template ruleset uniqueness", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_CHECKING_TEMPLATE_RULESET_NAME_UNIQUENESS, dbExcep);
		}
	}
	
	public DataTable extractData(String p_sExpectedCaseId, String p_sStagedFileName, DataTable p_oDataTable, String p_sSpecifiedSafetyDatabaseName) throws SciServiceException
	{
		boolean bIsWorkbookFile = false;
		boolean bIsCsvFile = false;
		boolean bIsPdfFile = false;
		boolean bHeaderRowPresent = false;
		int nSheetIndex = 0;
		String sCaseIdColumnName = null;
		String sCaseIdAsPerUploadedFile = null;
		WorkbookWorkSheetWrapper oWorksheetWrapper = null;
		List<Object> listDataTableValuesForColumn = null;
		File oStagedFile = null;
		int nInputFileSizeInKB = -1;
		int nFileMaxLimitInKB = -1;
		int nMaxRowCount = -1 ;
		String sTempInput = null;
		String sFilename = null;
		String sColumnsToCollapse = null;
		DataTable oDataTableInitial = null;
		NgvDataTypeUtils oNgvDataTypeUtils = null;
		String sIncommingDateFormat = null;
		//START: checking if input file size limit is exceeded or not 
		sFilename = FilenameUtils.getName(p_sStagedFileName);
		oStagedFile = new File(p_sStagedFileName);
		if(oStagedFile.exists())
		{
			sTempInput = getAppConfigSetting(AppConfig.FILE_MAX_SIZE_IN_KB);
			nFileMaxLimitInKB = Integer.parseInt(sTempInput == null ? "0": sTempInput);
			nInputFileSizeInKB = (int) (oStagedFile.length()/1024);
			
			if(nFileMaxLimitInKB != 0 && nInputFileSizeInKB > nFileMaxLimitInKB)
			{
				log.error("File size exceeds system defined - {}", sFilename);
				throw new SciServiceException(ServiceErrorCodes.Ngv.FILE_SIZE_EXCEEDS_SYSTEM_SUPPORTED, sFilename, null);
			}
			
		}
		else
		{
			log.error("Uploaded file does not exist - {}", sFilename);
			throw new SciServiceException(ServiceErrorCodes.Ngv.UPLOADED_WORKBOOK_FILE_DOES_NOT_EXIST, sFilename, null);
		}
		
		if(p_sSpecifiedSafetyDatabaseName.equals(SAFETY_DB_RSS))
		{
		//bFileFormatWorkbook = ((String) "WORKBOOK").equalsIgnoreCase("WORKBOOK");
		bHeaderRowPresent = Boolean.parseBoolean(getAppConfigSetting(AppConfig.FILE_HEADER_ROW_PRESENT));
		nSheetIndex = Integer.parseInt(getAppConfigSetting(AppConfig.FILE_DEFAULT_SHEET_INDEX));
		sCaseIdColumnName = getAppConfigSetting(AppConfig.FILE_CASEID_COLUMN_NAME);
		
		//Step 1: Check if the file format is supported
		try
		{
			bIsWorkbookFile = OfficeUtils.checkIfWorkbookFile(p_sStagedFileName);
		}
		catch(SciException sciExcep)
		{
			log.error("Illegal file type detected - {}", sFilename, sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ILLEGAL_FILE_TYPE_DETECTED, sFilename, sciExcep);
		}
		if (!bIsWorkbookFile)
		{
			try
			{
				bIsCsvFile = OfficeUtils.checkIfCsvFile(p_sStagedFileName);
			}
			catch(SciException sciExcep)
			{
				log.error("Illegal file type detected - {}", sFilename, sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ILLEGAL_FILE_TYPE_DETECTED, sFilename, sciExcep);
			}
		}
		
			if (!bIsWorkbookFile && !bIsCsvFile && !bIsPdfFile)
		{
			log.error("Illegal file type detected - {}", sFilename);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ILLEGAL_FILE_TYPE_DETECTED, sFilename, null);
		}
		sColumnsToCollapse = getAppConfigSetting(AppConfig.SKIP_COLUMNS_COLLAPSE);
		//Step 2: Parse the file
		if (bIsWorkbookFile)
		{
			try
			{
				// Read contents into Table-of-tables
				oNgvDataTypeUtils = new NgvDataTypeUtils();
				sIncommingDateFormat = getAppConfigSetting(AppConfig.SKIP_COLUMNS_COLLAPSE);
				oWorksheetWrapper = OfficeUtils.parseWorkbook(
										p_sStagedFileName
									, 	bHeaderRowPresent
									, 	nSheetIndex
									,	ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString())
									,	ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT.toString())
									);
				
				oDataTableInitial = OfficeUtils.convertToDatatable(oWorksheetWrapper, sColumnsToCollapse);
			}
			catch(SciException sciExcep)
			{
				log.error("Exception parsing provided case file", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_PARSING_WORKBOOK_FILE, sFilename, sciExcep);
			}
		}
		else if (bIsCsvFile)
		{
			try
			{
				oDataTableInitial = new DataTable();
				
				OfficeUtils.parseCsvFile(p_sStagedFileName, oDataTableInitial, true);
			}
			catch(SciException sciExcep)
			{
				log.error("Exception parsing provided case file", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_PARSING_WORKBOOK_FILE, sFilename, sciExcep);
			}
		}
		
		}else if(p_sSpecifiedSafetyDatabaseName.equals(SAFETY_DB_ARGUS))
		{
			bIsPdfFile = PdfUtils.checkIfPdfFile(p_sStagedFileName);
			oDataTableInitial = new DataTable();
			//
			try 
			{
				if(bIsPdfFile)
				{	
					NgvXmlParser.parseXmlDataPdf(p_sStagedFileName, oDataTableInitial);
					sCaseIdAsPerUploadedFile = (String) oDataTableInitial.getColumnValues(getAppConfigSetting(AppConfig.PDFXML_CASEID_COLUMN_NAME)).get(0);
					sCaseIdAsPerUploadedFile = sCaseIdAsPerUploadedFile.contains("-")?sCaseIdAsPerUploadedFile.substring(sCaseIdAsPerUploadedFile.lastIndexOf("-")+1, sCaseIdAsPerUploadedFile.length()):sCaseIdAsPerUploadedFile;
					if(!sCaseIdAsPerUploadedFile.equals(p_sExpectedCaseId))
					{
						log.error("Case Id Mismatch");
						throw new SciServiceException(ServiceErrorCodes.Ngv.PDFXML_VALIDATION_FAILED_CASEID_MISMATCH, sCaseIdAsPerUploadedFile, null);
					}
				}else
				{
					log.error("Illegal file type detected - {}", sFilename);
					throw new SciServiceException(ServiceErrorCodes.Ngv.ILLEGAL_FILE_TYPE_DETECTED, sFilename, null);
				}	
				// do validation here 
			} catch (SciException sciExcep) 
			{
				log.error("Exception parsing provided case file", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_PARSING_PDFXML_FILE, sFilename, sciExcep);
			}
			
			return oDataTableInitial;
		}
		
		
		
		
		//Step 3: Perform basic validations post parsing
		//Step 3a: Check data row count against expected min row count
		if(OfficeUtils.isMinDataRowCountBreached(oDataTableInitial, 1))
		{
			log.error("Minimum row count breached - 1");
			throw new SciServiceException(ServiceErrorCodes.Ngv.WORKBOOK_VALIDATION_FAILED_MIN_ROW_COUNT, sFilename, null);
		}
		nMaxRowCount = Integer.parseInt(getAppConfigSetting(AppConfig.FILE_MAX_ROW_COUNT));
		
		//Step 3b: Check data row count against permissible max row count
		if(OfficeUtils.isMaxDataRowCountBreached(oDataTableInitial, nMaxRowCount))
		{
			log.error("Maximum row count breached - " + nMaxRowCount);
			throw new SciServiceException(ServiceErrorCodes.Ngv.WORKBOOK_VALIDATION_FAILED_MAX_ROW_COUNT, sFilename, null);
		}
		
		//Step 3c:Validate Case Id (of first row) against expected case id
		if (oDataTableInitial.getColumnValues(sCaseIdColumnName) != null && oDataTableInitial.getColumnValues(sCaseIdColumnName).size() > 0)
		{
			sCaseIdAsPerUploadedFile = (String)DataUtils.extractUniqueValue(oDataTableInitial.getColumnValues(sCaseIdColumnName));//(String)oDataTableInitial.getCellValue(sCaseIdColumnName, 0);
		}
		if(sCaseIdAsPerUploadedFile == null)
		{
			log.error("Empty Case Id");
			throw new SciServiceException(ServiceErrorCodes.Ngv.WORKBOOK_VALIDATION_FAILED_CASEID_EMPTY, sCaseIdColumnName, null);
		}
		
		if(!sCaseIdAsPerUploadedFile.equals(p_sExpectedCaseId))
		{
			log.error("Case Id Mismatch");
			throw new SciServiceException(ServiceErrorCodes.Ngv.WORKBOOK_VALIDATION_FAILED_CASEID_MISMATCH, sCaseIdAsPerUploadedFile, null);
		}
		
		//Step 4: Convert file data into DataTable
		if (p_oDataTable == null)
		{
			p_oDataTable = new DataTable();
		}
		
	
		OfficeUtils.appendDataIntoDataTable(p_oDataTable, oDataTableInitial, true, sColumnsToCollapse);
		
		//Step 5: Validate DataTable contents
		//Step 5a: Check to ensure single value for CaseId
		listDataTableValuesForColumn = p_oDataTable.getColumnValues(sCaseIdColumnName);
		
		if (listDataTableValuesForColumn.size() > 1)
		{
			log.error("Case Id Mismatch. Entered - {}. From File - {}", p_sExpectedCaseId, sCaseIdAsPerUploadedFile);
			throw new SciServiceException(ServiceErrorCodes.Ngv.WORKBOOK_VALIDATION_FAILED_CASEID_MISMATCH, p_sExpectedCaseId + "/" + sCaseIdAsPerUploadedFile, null);
		}
		
		return p_oDataTable;
	}

	public Set<Object> generateNarrative(NgvTemplateRuleset oTemplateRuleset, String p_sTemplateText, DataTable oDataTable, String p_sJsonString, 
					Map<String, Object> input) throws SciServiceException
	{
		Set<Object>oListOfNarrativeObjects = null;
		Pattern oPattern = null;
		Matcher oMatcher = null;
		String sExcludeCharRegex = null;
		String[] sarrColumNames = null;
		List<Object> oListOfNarrativeAndMissingMarker = null;
		oListOfNarrativeObjects = new LinkedHashSet<>();
		
		try
		{
			if(p_sJsonString != null)
			{
				input = convertJsonIntoMap(p_sJsonString, true);
			} 
			 
			oListOfNarrativeObjects.add(input);
			
			String sColumnValue = null;
			sExcludeCharRegex = getAppConfigSetting(AppConfig.NARRATIVE_TEXT_EXCLUDE_CHARACTER_REGEX);
			sarrColumNames = getAppConfigSetting(AppConfig.FILE_COLUMN_NAMES_TO_REMOVE_SPECIAL_CHARS).split(",");
			oPattern = Pattern.compile(sExcludeCharRegex);
			for(int nColumn = 0; nColumn<sarrColumNames.length; nColumn++)
			{
				//Check if column is present or not
				if(input.keySet().contains(sarrColumNames[nColumn]))
				{
					sColumnValue = input.get(sarrColumNames[nColumn]).toString();
					oMatcher = oPattern.matcher(sColumnValue);
					if (oMatcher.find()) 
					{
						input.put(sarrColumNames[nColumn], oMatcher.replaceAll(""));
					}
				}
			}
			oListOfNarrativeAndMissingMarker = FreemarkerUtils.replaceFreemarkerTemplate(p_sTemplateText, input);
			if(oListOfNarrativeAndMissingMarker.size() == 2)
			{	
				oListOfNarrativeObjects.add(oListOfNarrativeAndMissingMarker.get(0));
				oListOfNarrativeObjects.add(oListOfNarrativeAndMissingMarker.get(1));
			}
		}
		catch(SciException sciExcep)
		{
			log.error("Error generating narrative", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_GENERATION_FAILED, sciExcep);
		}
		return oListOfNarrativeObjects;
	}

	public List<String> identifyTemplateToUse(UserInfo p_oUserInfo, NgvTemplateRuleset oTemplateRuleset, String p_sJsonString, Map<String, Object> p_oInputMap) throws SciServiceException
	{
		String sTemplateName = null;
		List<String> listTemp = null;
		if(p_sJsonString != null)
		{
			try
			{
				p_oInputMap = convertJsonIntoMap(p_sJsonString, false);
			}
			catch(SciException sciExcep)
			{
				log.error("Error getting narrative template for user - {}", p_oUserInfo.getUserId(), sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE, p_oUserInfo.getUserId(), sciExcep);
			}
		} 
		
		try(DbConnection connection = DbConnectionManager.getConnection())
		{
			
			listTemp = getTemplateToUse(connection, p_oUserInfo, oTemplateRuleset, p_oInputMap);
		}
		catch(SciException sciExcep)
		{
			log.error("Error getting narrative template for user - {}", p_oUserInfo.getUserId(), sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE, p_oUserInfo.getUserId(), sciExcep);
		}
		catch (SciDbException dbExcep) 
		{
			log.error("Error getting narrative template", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.NARRATIVE_TEMPLATE_NOT_AVAILABLE, dbExcep);
		}
		return listTemp;
	}
	
	private  Map<String, Object> convertJsonIntoMap(String p_sJsonString, boolean p_bNullValueFlag) throws SciException
	{
		
		Map<String, Object> input = null;
		String sColumnToCollapsed = null;
		String[] arrColumnToCollapsed = null;
		List<String> productsNamesList = null;
		String sDepandantColumnsOfProduct = null;
		String[] arrDepandantColumnsOfProduct = null;
		Object sDependantColumnValue = null;
		Object oColumnValue = new ArrayList<>();
		Map<String, Object>mergeColumnMap = new LinkedHashMap<String, Object>();
		Map<String, Object>productMap = new LinkedHashMap<String, Object>();
		Map<String, Object>actionMap = new HashMap<String, Object>();
		Map<String, Object>eventValueMap = new HashMap<String, Object>();
		Map<String, Object>causalityValueMap = new HashMap<String, Object>();
		JSONParser parser = new JSONParser();
		Object obj = null;
		String sReplaceWithUnspecifiedIfNull = null;
		NgvDataTypeUtils oNgvDataTypeUtils = null;
		String sDateFormat = null;
		String sProductName = null;
		List<String> arrTemp = null;
		String sTempProductName = null;
		
		if(StringUtils.isNullOrEmpty(p_sJsonString))
		{
			return null;
		}	
		
		try 
		{
			obj = parser.parse(p_sJsonString);
			
			JSONArray array = (JSONArray) obj;
			JSONObject jsonObject = null;
			
			input = new HashMap<>();
			sReplaceWithUnspecifiedIfNull = getAppConfigSetting(AppConfig.FILE_COLUMN_NAME_IF_NULL_REPLACED_WITH_UNSPECIFIED);
			oNgvDataTypeUtils = new NgvDataTypeUtils();
			sDateFormat = getAppConfigSetting(AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT);
			oNgvDataTypeUtils.setsDateFormat(sDateFormat);
			for(int i=0;i<array.size();i++)
			{
				jsonObject = (JSONObject) array.get(i);
				for (Object key : jsonObject.keySet()) 
				{
					input.put(key.toString(),
							oNgvDataTypeUtils.getKeyValue(key.toString(), jsonObject.get(key), sReplaceWithUnspecifiedIfNull, p_bNullValueFlag));
				}
			}
			
			sColumnToCollapsed = getAppConfigSetting(AppConfig.COLUMNS_FOR_EVENT_MAP);
			arrColumnToCollapsed = sColumnToCollapsed==null?null:sColumnToCollapsed.split(",");
			if(arrColumnToCollapsed!=null && arrColumnToCollapsed.length>0)
			{
				for(int nColumnIndex=0; nColumnIndex<arrColumnToCollapsed.length;nColumnIndex++)
				{	
					oColumnValue = input.get(arrColumnToCollapsed[nColumnIndex]); 
					eventValueMap.put(arrColumnToCollapsed[nColumnIndex], oColumnValue);
				}
				actionMap.put("_event_", eventValueMap);
			}
			
			sColumnToCollapsed = getAppConfigSetting(AppConfig.COLUMNS_FOR_CAUSALITY_MAP);
			arrColumnToCollapsed = sColumnToCollapsed==null?null:sColumnToCollapsed.split(",");
			if(arrColumnToCollapsed!=null && arrColumnToCollapsed.length>0)
			{
				for(int nColumnIndex=0; nColumnIndex<arrColumnToCollapsed.length;nColumnIndex++)
				{	
					oColumnValue = input.get(arrColumnToCollapsed[nColumnIndex]); 
					
					if(oColumnValue!=null && !oColumnValue.toString().contains("<span")) 
					{
						causalityValueMap.put(arrColumnToCollapsed[nColumnIndex], oColumnValue);
					}	
				}
				actionMap.put("_causality_", causalityValueMap);
			}
			
			input.put("eventAndCausalityMap", actionMap);
			
			sDepandantColumnsOfProduct = getAppConfigSetting(AppConfig.COLUMNS_FOR_PRODUCT_MAP);
			sColumnToCollapsed = getAppConfigSetting(AppConfig.MERGE_CASE_IDENTIFIER_COLUMNS);
			arrColumnToCollapsed = sColumnToCollapsed==null?null:sColumnToCollapsed.split(",");
			arrDepandantColumnsOfProduct = sDepandantColumnsOfProduct==null?null:sDepandantColumnsOfProduct.split(",");
			
			if(arrColumnToCollapsed!=null && arrColumnToCollapsed.length>0)
			{
				for(int nIndex = 0; nIndex<arrColumnToCollapsed.length;nIndex++)
				{
					productMap = new LinkedHashMap<String, Object>();
					productsNamesList = new ArrayList<>();
					oColumnValue = input.get(arrColumnToCollapsed[nIndex]);
					
					if(oColumnValue == null)
					{
						continue;
					}
					else if(!(oColumnValue instanceof String))
					{
						productsNamesList = oColumnValue == null ? null : (List<String>)oColumnValue;
					}
					else
					{
						productsNamesList = new ArrayList<String>();
						productsNamesList.add((String)oColumnValue);
					}	
					
					for(int nColumnvalueIndex = 0; nColumnvalueIndex < productsNamesList.size();nColumnvalueIndex++)
					{
						sProductName = productsNamesList.get(nColumnvalueIndex).trim();
						
						if(nColumnvalueIndex == 0 || !sTempProductName.equals(sProductName))
						{
							mergeColumnMap = new LinkedHashMap<String, Object>();	
							for(int nDependantColumnIndex = 0; nDependantColumnIndex < arrDepandantColumnsOfProduct.length; nDependantColumnIndex++)
							{
								sDependantColumnValue = input.get(arrDepandantColumnsOfProduct[nDependantColumnIndex]);
								if(sDependantColumnValue == null || sDependantColumnValue.equals(""))
								{
									mergeColumnMap.put(arrDepandantColumnsOfProduct[nDependantColumnIndex], "");
								}
								else
								{
									if(!(sDependantColumnValue instanceof String))
									{
										arrTemp = (ArrayList<String>)sDependantColumnValue;
										mergeColumnMap.put(arrDepandantColumnsOfProduct[nDependantColumnIndex], nColumnvalueIndex<arrTemp.size()?arrTemp.get(nColumnvalueIndex):"");
									}
									else
									{
										mergeColumnMap.put(arrDepandantColumnsOfProduct[nDependantColumnIndex], sDependantColumnValue);
									}
								}	
							}
							productMap.put(sProductName, mergeColumnMap);	
						}
						sTempProductName = sProductName;
					}
					input.put("_"+arrColumnToCollapsed[nIndex]+"_",productMap);
				}
			}
		}
		catch (Exception excep)
		{
			log.error("Error during convertJsonIntoMap", excep);
			throw new SciException("Error during convertJsonIntoMap", excep);
		}
		
		input.put("ngvDataTypeUtils", oNgvDataTypeUtils);
		log.debug("In Convert JSON\n\n - {}", input);
		return input;
	}

	public List<String> getImportedTemplate(String sStagedFilePath) throws SciServiceException
	{
		File inputFile = null;
		DocumentBuilderFactory oDbFactory = null;
		DocumentBuilder oDbBuilder = null;
		Document oDocument = null;
		NodeList listOfNodes = null;
		List<String> listRulesetNameAndXmlDoc = null;
		
		try
		{
			listRulesetNameAndXmlDoc = new ArrayList<>();
			inputFile = new File(sStagedFilePath);
			oDbFactory  = DocumentBuilderFactory.newInstance();
			oDbBuilder = oDbFactory.newDocumentBuilder();
			oDocument = oDbBuilder.parse(inputFile);
			oDocument.getDocumentElement().normalize();
			listOfNodes = oDocument.getElementsByTagName("TemplateRulesetName");
			listRulesetNameAndXmlDoc.add(listOfNodes.item(0).getTextContent());
			listRulesetNameAndXmlDoc.add(FileUtils.readFileToString(inputFile, "UTF-8"));
		}
		catch(Exception excep)
		{
			log.error("Error parsing import file", excep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_IMPORTING_TEMPLATE, excep);
		}
	
		return listRulesetNameAndXmlDoc;
	}

	public void saveHeaderAndTemplate(UserSession p_oUserSession, NgvTemplateRuleset p_oTemplateRuleset, String p_sTemplateRulesetXmlDoc) throws SciServiceException
	{
		DocumentBuilder oDbBuilder = null;
		DocumentBuilderFactory oDbFactory = null;
		Document oDocument = null;
		InputSource is = null;
		NodeList listOfNodes = null,
				 listTemplateDefault = null;
		NgvTemplate oTemplate = null;
		Node nNode = null;
		Element eElement = null;
		byte[] base64decodedBytes;
		String sTemplateRulesetHeader = null;
		String sTemplateRulesetName = null;
		String sTemplateText = null;
		String sTemplateName = null;
		String sTemplateCriteria = null;
		String sTemplateHeader = null;
		String sDefaultTemplateName = null;
		int		l_defaulttemplateid	=	-1;
		
		try
		{
			oDbFactory = DocumentBuilderFactory.newInstance();
			oDbBuilder = oDbFactory.newDocumentBuilder();
			is = new InputSource();
			is.setCharacterStream(new StringReader(p_sTemplateRulesetXmlDoc));
			oDocument = oDbBuilder.parse(is);//TemplateRulesetTemplateHeader
			
			//Find ruleset name and save it
			listOfNodes = oDocument.getElementsByTagName("TemplateRulesetName");
			listTemplateDefault	=	oDocument.getElementsByTagName("TemplateRulesetDefaultTemplate");
			if (listTemplateDefault != null) {
				if (listTemplateDefault.item(0) != null) {
					sDefaultTemplateName	=	listTemplateDefault.item(0).getTextContent();
				}
			}
			sTemplateRulesetName = listOfNodes.item(0).getTextContent() + "";
			if(sTemplateRulesetName!=null)
			{
				try (DbConnection connection = DbConnectionManager.getConnection())
				{
					try
					{
					connection.startTransaction();
					
					// Find ruleset header, decode it by BASE64 decoding and save it in db
					listOfNodes = oDocument.getElementsByTagName("TemplateRulesetTemplateHeader");
					sTemplateRulesetHeader = listOfNodes.item(0).getTextContent();
					base64decodedBytes = Base64.getDecoder().decode(sTemplateRulesetHeader);
					sTemplateHeader = new String(base64decodedBytes, "UTF-8");
					setHeaderTemplate(p_oUserSession, p_oTemplateRuleset, sTemplateHeader);

					listOfNodes = oDocument.getElementsByTagName("Template");
					for (int nTemplate = 0; nTemplate < listOfNodes.getLength(); nTemplate++) 
					{
						nNode = listOfNodes.item(nTemplate);
						eElement = (Element) nNode ;
						sTemplateName = eElement.getElementsByTagName("TemplateName").item(0).getTextContent();
						sTemplateCriteria = eElement.getElementsByTagName("TemplateCriteria").item(0).getTextContent();
						sTemplateText = eElement.getElementsByTagName("TemplateContent").item(0).getTextContent();
						base64decodedBytes = Base64.getDecoder().decode(sTemplateText);
						oTemplate = new NgvTemplate(sTemplateName, new String(base64decodedBytes, "UTF-8"));
						oTemplate.setParentRulesetId(p_oTemplateRuleset.getRulesetId());
						base64decodedBytes = Base64.getDecoder().decode(sTemplateCriteria);
						oTemplate.setTemplateCriteria( new String(base64decodedBytes, "UTF-8"));
						addTemplate(p_oUserSession, p_oTemplateRuleset, oTemplate, LocationToAdd.END_OF_LIST);
						if ((!StringUtils.isNullOrEmpty(sDefaultTemplateName)) && sDefaultTemplateName.equals(sTemplateName)) {
							l_defaulttemplateid	=	oTemplate.getTemplateSeqId();
						}
					}
					
					if (l_defaulttemplateid != -1) {
						setDefaultTemplate(p_oUserSession,p_oTemplateRuleset,l_defaulttemplateid);
					}
					
					connection.commitTransaction();
					}
					catch (Exception sciExcep)
					{
						connection.rollbackTransaction();
						log.error("Error fetching template ruleset", sciExcep);
						throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_FETCHING_TEMPLATE_RULESET, sciExcep);
					}
				}
				catch(SciDbException scidbexcep)
				{
					log.error("Error saving template ruleset", scidbexcep);
					throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_IMPORTING_TEMPLATE, scidbexcep);
				}
			}
		}
		catch(Exception excep)
		{
			log.error("Error saving template ruleset", excep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.ERROR_IMPORTING_TEMPLATE, excep);
		}
	}

	public String removeSpecialCharactersFromNarrative(String sNarrative) throws SciServiceException 
	{
		Pattern oPattern = null;
		Matcher oMatcher = null;
		String sExcludeCharRegex = null;
		if(sNarrative!=null)
		{	
			try
			{
				sExcludeCharRegex = getAppConfigSetting(AppConfig.NARRATIVE_TEXT_EXCLUDE_CHARACTER_REGEX);
				oPattern = Pattern.compile(sExcludeCharRegex);
				oMatcher = oPattern.matcher(sNarrative);
				sNarrative = oMatcher.replaceAll("");
			}catch(Exception excep)
			{
				log.error("Error in replacing special character in narrative", excep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.REPLCAING_NARRATIVE_TEXT_SPECIAL_CHARS, excep);
			}
		}
		return sNarrative;
	}

	public boolean checkIfMarkerContainsSpecialChars(Map<String, String> mapMissingMarker) throws SciServiceException
	{
		Pattern oPattern = null;
		Matcher oMatcher = null;
		String sUpdateMarkerRegex = null;
		try
		{
			sUpdateMarkerRegex = getAppConfigSetting(AppConfig.REGEX_FOR_UPDATE_MARKES);
			oPattern = Pattern.compile(sUpdateMarkerRegex);
			for(String key : mapMissingMarker.keySet())
			{
				if(mapMissingMarker.get(key)!=null)
				{
					oMatcher = oPattern.matcher(mapMissingMarker.get(key));
					if(oMatcher.find())
					{
						log.error("Error in updating markers");
						throw new SciServiceException(ServiceErrorCodes.Ngv.CHECKING_REGEX_FOR_UPDATE_MARKES);
					}
				}
			}
			
			
		}
		catch(Exception excep)
		{
			log.error("Error in updating markers", excep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.CHECKING_REGEX_FOR_UPDATE_MARKES, excep);
		}
		return false;
	}

}