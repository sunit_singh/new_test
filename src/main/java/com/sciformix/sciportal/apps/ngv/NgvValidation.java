package com.sciformix.sciportal.apps.ngv;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

//TODO:class NgvValidation needs to be removed
public class NgvValidation {

	static Map<String, String> mapValidation = null;
	
	static{
		
		
		
		/*mapValidation.put("fileLeafFolderName", "input_files");
		mapValidation.put("fileExtensions", "xls,xlsx");
		mapValidation.put("caseIdColumnName", "tracking_no");
		mapValidation.put("mandatoryColumnNames", "tracking_no,patnt_gender");
		mapValidation.put("headerRowPresentFlag", "no");
		mapValidation.put("headerRowNames", "tracking_no,case_type,j_j_aware_dt,patnt_curr_age,patnt_curr_age_unit,patnt_gender,cntct_patnt_rel,event_event_cntry,pt_his_desc_med_his,pt_his_food_alg,drug_eff_start_dt,prod_trademark,drug_route,drug_dose_txt,indctn_crs,lot_no,exp_dt,drug_role,event_dt,r_e_prim_src,drug_eff_end_dt,drug_action_taken,r_e_eff_end_dt,r_e_outcome,serious,ich_cause,product_name,med_hist_ind,patnt_ethn,drink_alcohol,patnt_smoke,drinks_day_no,cigs_day_no,how_treat,pqms_file_stat,pqms_cat_disp,pqms_cmnt_txt,patnt_lst_mns_dt,preg_due_dt,reg_class,rpt_malfunction,advr_event_ind");
		*///caseID columnName and mandatory col 
	}

	public static Map<String, String> getMapValidation() {
		FileInputStream oInputStream = null;
		Properties p_oProperties = new Properties(); 
		String sKey = null;
		String sValue = null;
		mapValidation = new HashMap<>();
		
		try
		{
			oInputStream = new FileInputStream("C://test.properties");
			
			if (oInputStream != null)
			{
				p_oProperties.load(oInputStream);
			}
		}
		catch(Exception excep)
		{
			
		}
		
		Iterator<Object> oPropIterator = null;
		oPropIterator = p_oProperties.keySet().iterator();
		
		while(oPropIterator.hasNext())
		{
			sKey = (String)oPropIterator.next();
			sValue = p_oProperties.getProperty(sKey);
			mapValidation.put(sKey, sValue);	
		}
		return mapValidation;
	}
		
}
