package com.sciformix.sciportal.apps.ngv;


import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class ValidationDbSchema {

	public static ValTemplate VALTEMPLATE = new ValTemplate();
	
	public static class ValTemplate extends DbTable
		{
			public DbColumn VALSEQID = null;
			public DbColumn VALNAME = null;
			public DbColumn CTOSEQID = null;
			public DbColumn VALSEQNUM = null;
			public DbColumn RULETYPE = null;
			public DbColumn CRITERIA = null;
			public DbColumn APPLICABILITY = null;
			public DbColumn DTCREATED = null;
			public DbColumn USRCREATED = null;
			public DbColumn DTLASTUPDATED = null;
			public DbColumn USRLASTUPDATED = null;
			
			public ValTemplate()
			{
				super("APP_NGV_VALIDATIONTEMPLATES");
				setPkSequenceName("APP_NGV_VALSEQID_SEQ");
				
				VALSEQID = addColumn("VALSEQID");
				VALNAME = addColumn("VALNAME");
				CTOSEQID = addColumn("CTOSEQID");
				VALSEQNUM = addColumn("VALSEQNUM");
				RULETYPE = addColumn("RULETYPE");
				CRITERIA = addColumn("CRITERIA");
				APPLICABILITY = addColumn("APPLICABILITY");
				DTCREATED = addColumn("DTCREATED");
				USRCREATED = addColumn("USRCREATED");
				DTLASTUPDATED = addColumn("DTLASTUPDATED");
				USRLASTUPDATED = addColumn("USRLASTUPDATED");
			}
		}
		
}
