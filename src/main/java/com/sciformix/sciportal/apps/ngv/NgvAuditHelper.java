package com.sciformix.sciportal.apps.ngv;

public class NgvAuditHelper
{
	public static class AuditAttributes
	{
		public static final String TEMPLATE_RULESET_ID = "Template Ruleset Id"; 
		public static final String TEMPLATE_RULESET_STATE = "Template Ruleset State";
		public static final String TEMPLATE_RULESET_DEFAULT_TEMPLATE = "Template Ruleset Default Template";
		public static final String TEMPLATE_RULESET_HEADER = "Template Ruleset Header";
		public static final String TEMPLATE_RULESET_TEMPLATE_LIST = "Template List";
		public static final String TEMPLATE_RULESET_NAME = "Template Ruleset Name"; 
		
		public static final String TEMPLATE_ID = "Template Id";
		public static final String TEMPLATE_CRITERIA = "Template Criteria";
		public static final String TEMPLATE_CONTENT = "Template Content";
		public static final String TEMPLATE_NAME = "Template Name";
		
		public static final String VALIDATION_RULESET_ID = "Validation Ruleset Id"; 
		public static final String VALIDATION_RULESET_STATE = "Validation Ruleset State";
		public static final String VALIDATION_RULESET_DEFAULT_TEMPLATE = "Validation Ruleset Default Template";
		public static final String VALIDATION_RULESET_HEADER = "Validation Ruleset Header";
		public static final String VALIDATION_RULESET_CASE_LIST = "Validation List";
		public static final String VALIDATION_RULESET_NAME = "Validation Ruleset Name"; 
		
		public static final String RULE_ID = "Rule Id";
		public static final String RULE_NAME = "Rule Name";
		public static final String RULE_TYPE = "Rule Type";
		public static final String CRITERIA = "Criteria";
		public static final String APPLICABILITY = "Applicability";
	}
	
	private NgvAuditHelper()
	{
		//Nothing to do
	}

}
