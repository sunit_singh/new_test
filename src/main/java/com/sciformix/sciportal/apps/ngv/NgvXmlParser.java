package com.sciformix.sciportal.apps.ngv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.PdfUtils;
import com.sciformix.sciportal.config.ConfigHome;

public class NgvXmlParser 
{
	static String DEFAULT_DATE_FORMAT = "dd-MMM-yyyy";
	
	private static final Logger log = LoggerFactory.getLogger(NgvXmlParser.class);
	public static void parseXmlDataPdf(String p_sStagedFileName, DataTable p_oDataTable) throws SciException 
	{
		String sFileText = null;
		String sTagValue = "";
		BufferedReader oBufferedReader = null;
		DocumentBuilderFactory oDocumentBuilderFactory = null;
		DocumentBuilder oDocumentBuilder = null;
		InputSource oInputSource = null;
		Document oDocument = null;
		NodeList nodeList = null;
		NodeList chilNodeList = null;
		Node node = null;
		StringBuffer oOutputStringBuffer = null;
		String sColumnName = null;
		String sColumnValue = null;
		String sTemp = "";
		String[] arrDateColumns = null;
		List<String> listDateColumns = null;
		String[] arrSkipTags = null;
		List<String> listSkipTags = null;
		List<String> listMandatoryFieldTags = null;
		Map<String,String> tempMap = null;
		Map<String,List<String>> mandatoryFieldOfTagMap = null;
		String[] arrTemp = null;
		List<String> temFieldList = null;
		try 
		{
			mandatoryFieldOfTagMap = getMapOfMandaotaryFieldsInTag();
			
			arrDateColumns = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_DATE_COLUMNS.toString()).split(",");
			arrSkipTags = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_SKIP_TAGS.toString()).split(",");
			listDateColumns = new ArrayList<>();
			listSkipTags = new ArrayList<>();
			listMandatoryFieldTags = new ArrayList<>();
			
			Collections.addAll(listDateColumns, arrDateColumns);
			Collections.addAll(listSkipTags, arrSkipTags);
			
			arrTemp = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_DATE_COLUMNS.toString()).split(",");
			
			
			sFileText = PdfUtils.getFileText(p_sStagedFileName);
			
			oBufferedReader = new BufferedReader(new StringReader(sFileText));
			oOutputStringBuffer = new StringBuffer();
			while((sTagValue = oBufferedReader.readLine())!=null)
			{
				if(!(sTagValue.toLowerCase().startsWith("page") ||sTagValue.toLowerCase().contains("http://")||sTagValue.toLowerCase().contains("https://")))
				{
					if(sTagValue.startsWith("-"))
						sTagValue = sTagValue.replaceAll("-", "");
					//oOutputStringBuffer.append(sTagValue).append(StringConstants.CRLF);
					oOutputStringBuffer.append(sTagValue);
				}
			}
			
			
			sFileText = oOutputStringBuffer.toString();
			sFileText = sFileText.replaceAll("&", " ");
			oDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		    oDocumentBuilder = oDocumentBuilderFactory.newDocumentBuilder();
		    oInputSource = new InputSource();
		    oInputSource.setCharacterStream(new StringReader(sFileText));
		    oDocument = oDocumentBuilder.parse(oInputSource);
		    oDocument.getDocumentElement().normalize();
		    for(String key:mandatoryFieldOfTagMap.keySet())
		    {
		    	listMandatoryFieldTags.add(key);
		    }
		    for(String key:mandatoryFieldOfTagMap.keySet())
		    {
		    	listMandatoryFieldTags.remove(key);
			    nodeList = oDocument.getElementsByTagName(key);
			    Element element = null;
			    for(int nNumber = 0 ; nNumber < nodeList.getLength(); nNumber++)
			    {
			    	node = nodeList.item(nNumber);
			    	NodeList nodeList2 = node.getChildNodes();
			    	temFieldList = new ArrayList<>();
			    	for(int nFiledIndex = 0; nFiledIndex < nodeList2.getLength(); nFiledIndex++)
			    	{
			    		Node node1 = nodeList2.item(nFiledIndex);
			    		if(node1.getNodeName()!=null && listMandatoryFieldTags.contains(node1.getNodeName()))
			    				break;	
			    		if(node1.getNodeName().trim()!=null && !node1.getNodeName().trim().equals("#text") && !node1.getNodeName().trim().contains("#"))
			    		{
			    			
			    			sColumnName = node1.getNodeName();
			    			sColumnValue = node1.getTextContent();
			    			temFieldList.add(sColumnName);
			    			
					    	if(listDateColumns.contains(sColumnName))
					    		sColumnValue = getFormattedDate(sColumnValue);
					    	if(p_oDataTable.isColumnPresent(sColumnName))
					    		p_oDataTable.appendColumnValue(sColumnName,sColumnValue);
					    	else
					    		p_oDataTable.addColumn(sColumnName,sColumnValue);
			    		}	
			    	
			    	}
			    	checkMandatoryFieldsOfCurrentTag(p_oDataTable, mandatoryFieldOfTagMap, temFieldList,key);
			    }
		    }
		    
		    p_oDataTable.dumpContents();
		    
		} catch (SciException excep) 
		{
				log.error("Error in reading <"+p_sStagedFileName+"> file");
				throw new SciException("Error parsing PDF file", excep);
		} catch (IOException excep) 
		{
			log.error("Error in reading <"+p_sStagedFileName+"> file");
			throw new SciException("Error parsing PDF file", excep);
		} catch (ParserConfigurationException excep) 
		{
			log.error("Error in parsing <"+p_sStagedFileName+"> file");
			throw new SciException("Error parsing PDF file", excep);
		} catch (SAXException excep) 
		{
			log.error("Error in parsing <"+p_sStagedFileName+"> file");
			excep.printStackTrace();
			throw new SciException("Error parsing PDF file", excep);
		}
		
		finally 
		{
			try
			{
				if(oBufferedReader!=null)
				{
					oBufferedReader.close();
				}
			}
			catch (Exception ioExcep)
			{
				log.error("Error closing pdf file", ioExcep);
			}
		}
		
	}
	private static Map<String, List<String>> getMapOfMandaotaryFieldsInTag() 
	{
		String sTemp = null;
		String[] arrTemp = null;
		Map<String,List<String>> mandaotaryFieldTagMap = null;
		List<String> tempList = null;
		mandaotaryFieldTagMap = new LinkedHashMap();
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_SAFTEYREPORT.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_SAFTEYREPORT.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_REPORTDUPLICATE.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_REPORTDUPLICATE.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_PRIMARYSOURCE.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_PRIMARYSOURCE.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_PATIENT.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_PATIENT.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_MEDICALHISTORY_EPISODE.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_MEDICALHISTORY_EPISODE.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_REACTION.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_REACTION.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_DRUG.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_DRUG.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_ACTIVESUBSTANCE.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_ACTIVESUBSTANCE.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		sTemp = NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_DRUGREACTION_RELATEDNESS.toString();
		arrTemp =  ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.PDFXML_MANDATORY_FIELD_OFTAG_DRUGREACTION_RELATEDNESS.toString()).split(",");
		tempList = new ArrayList<>();
		Collections.addAll(tempList, arrTemp);
		mandaotaryFieldTagMap.put(sTemp.substring(sTemp.lastIndexOf(".")+1, sTemp.length()), tempList);
		
		return mandaotaryFieldTagMap;
	}
	
	private static void checkMandatoryFieldsOfCurrentTag(DataTable p_oDataTable, Map<String, List<String>> p_mandatoryFieldOfTagMap, List<String> temFieldList,String p_sCurrentTag) 
	{
		List<String> listMandatoryFields = null;
		String sTemp = null;
		listMandatoryFields = p_mandatoryFieldOfTagMap.get(p_sCurrentTag);
		for(int nIndex = 0; nIndex < listMandatoryFields.size() ; nIndex++)
		{
			sTemp = listMandatoryFields.get(nIndex);
			if(!temFieldList.contains(sTemp))
			{
				if(p_oDataTable.isColumnPresent(sTemp))
		    		p_oDataTable.appendColumnValue(sTemp,"");
		    	else
		    		p_oDataTable.addColumn(sTemp,"");
			}
		}
		
	}
	private static String getFormattedDate(String p_sDateValue) 
	{
		String sIncomingDateFormat = null;
		String sFormattedDate = null;
		String sDateFormat = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		SimpleDateFormat formatter = null;
		if(p_sDateValue!=null)
		{
			sIncomingDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_PDFXML.toString());
			sDateFormat = null;
			if (sDateFormat == null)
			{
				sDateFormat = DEFAULT_DATE_FORMAT;
			}
			//TODO: Check this
			try
			{
				incomingformatter = new SimpleDateFormat(sIncomingDateFormat);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
				
			}
			catch(ParseException parseExcep)
			{
				log.error("Date formatting failed for: Date/Format - " + p_sDateValue +  "/" + sIncomingDateFormat, parseExcep);
				return "";
			}
			
			formatter = new SimpleDateFormat(sDateFormat);
			sFormattedDate = formatter.format(oFormattedDate);
		}
		else
		{
			return "";
		}
		return sFormattedDate;
	}
}