package com.sciformix.sciportal.apps.ngv;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import freemarker.core.Environment;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

//TODO:Relook at this class
public class NarrativeExceptionHandler implements TemplateExceptionHandler
{

	private List<String> listMissingMarker = null;
	public NarrativeExceptionHandler(List<String> p_listMissingMarker) 
	{
		if(p_listMissingMarker == null)
		{
			this.listMissingMarker = new ArrayList<String>();	
		}
		else
		{
			this.listMissingMarker = p_listMissingMarker;
		}
	}
	
	@Override
	public void handleTemplateException(TemplateException p_templateException, Environment p_env, Writer p_out)
			throws TemplateException 
	{
		String sErrorMsg = null;
		String sTemplateVariableName = null;
		
		try 
		{
			sErrorMsg = p_templateException.getMessage();
			sTemplateVariableName = sErrorMsg.toString().substring(sErrorMsg.indexOf(">")+1, sErrorMsg.indexOf("["));
			if(!(sTemplateVariableName.contains("FTL stack trace") || sTemplateVariableName.contains("#")|| sTemplateVariableName.contains("(") || sTemplateVariableName.equals("")))
			{	
				listMissingMarker.add(sTemplateVariableName.trim());
			}
			p_out.write(sTemplateVariableName.trim());
        }
		catch (Exception e) 
		{
            throw new TemplateException("Failed to print error message. Cause: " + e, p_env);
        }
	}

}
