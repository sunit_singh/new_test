package com.sciformix.sciportal.apps.ngv;

public class UserAuditHelper
{
	public static class AuditAttributes
	{
		public static final String USER_SEQ_ID = "User Seq Id"; 
		public static final String USER_USERID = "User Id";
		public static final String USER_DISPLAYNAME = "User Display Name";
		public static final String USER_SHORTNAME = "User Short Name";
		public static final String USER_EMAIL = "User Email"; 
		public static final String USER_STATE = "User State";
		public static final String USER_TYPE = "User Type"; 
		public static final String USER_AUTHTYPE = "User Auth Type"; 
		public static final String USER_AUTHVALUE = "User Auth Value"; 
		
	}
	
	private UserAuditHelper()
	{
		//Nothing to do
	}

}
