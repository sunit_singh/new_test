package com.sciformix.sciportal.apps.ngv;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.config.ConfigTplObj;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class CaseDataValidationRulesetHelper
{
	private static final Logger log = LoggerFactory.getLogger(CaseDataValidationRulesetHelper.class);
	
	private CaseDataValidationRulesetHelper()
	{
		//Nothing to do
	}
	
	public static List<CaseDataValidationRuleset> getCaseDataValidationRulesets(int project_id) throws SciServiceException {
		List<CaseDataValidationRuleset> listTemplateRulesets = null;
		
		List<ConfigTplObj> listofConfigTpl = null;

		try (DbConnection connection = DbConnectionManager.getConnection()) 
		{
			try {
				connection.startTransaction();
				
				listofConfigTpl = ConfigTplObjHomeImpl.getListOfTemplates(connection, project_id, ConfigTplObjType.CDVR);
				listTemplateRulesets = new ArrayList<>(listofConfigTpl.size());
				for (ConfigTplObj obj : listofConfigTpl)
				{
					listTemplateRulesets.add((CaseDataValidationRuleset) obj);
				}

				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error fetching validation rulesets", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.VALIDATION_RULESET_FETCH_FAILED, sciExcep);
			}
		} catch (SciDbException dbExcep) {
			log.error("Error fetching validation rulesets", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.VALIDATION_RULESET_FETCH_FAILED, dbExcep);
		}
		return listTemplateRulesets;
	}
	
	public static ConfigTplObj getCaseDataValidationRuleset(int p_nValidationRulesetId) throws SciServiceException {
		ConfigTplObj oCaseValidationRuleset = null;

		try (DbConnection connection = DbConnectionManager.getConnection()) 
		{
			try {
				connection.startTransaction();

				ConfigTplObjHomeImpl.getTemplateDetails(connection, p_nValidationRulesetId);
				
				//TODO:Read the child table APP_NGV_VALIDATIONTEMPLATES
				
				oCaseValidationRuleset = NgvPortalAppHomeImpl.getValidationRuleset(connection,p_nValidationRulesetId);

				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error fetching validation ruleset", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.Ngv.VALIDATION_RULESET_FETCH_FAILED, sciExcep);
			}
		} catch (SciDbException dbExcep) {
			log.error("Error fetching validation ruleset", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.Ngv.VALIDATION_RULESET_FETCH_FAILED, dbExcep);
		}
		return oCaseValidationRuleset;
	}

}
