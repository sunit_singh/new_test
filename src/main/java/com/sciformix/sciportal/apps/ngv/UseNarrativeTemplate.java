package com.sciformix.sciportal.apps.ngv;

import com.sciformix.commons.rulesengine.BaseAction;

//TODO:class UseNarrativeTemplate needs to be removed
public class UseNarrativeTemplate extends BaseAction
{
	private String m_sTemplate = null;
	
	public UseNarrativeTemplate(String templateName)
	{
		m_sTemplate = templateName;
	}
	
	
	
	public String convertToString()
	{
		return this.getClass().getCanonicalName() + "(" + m_sTemplate + ")";
	}
	
}
