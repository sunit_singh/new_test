package com.sciformix.sciportal.apps.ngv;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataUtils;
import com.sciformix.sciportal.apps.ngv.CaseDataValidationRule.Type;
import com.sciformix.sciportal.config.ConfigTplObjDbSchema;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.content.BinaryContentHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUpdate;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.user.UserInfo;

public class NgvPortalAppHomeImpl
{
	private static final Logger log = LoggerFactory.getLogger(NgvPortalAppHomeImpl.class);
	
	private NgvPortalAppHomeImpl()
	{
		//Nothing to do
	}

	public static List<String> getTemplateRulesetNames(DbConnection connection, RecordState p_eState) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		List<String> listRulesetNames = null;
		
		if (p_eState != null)
		{
			oSelectQuery = DbQueryUtils.createSelectQuery(NgvDbSchema.TEMPLATE_RULESETS, NgvDbSchema.TEMPLATE_RULESETS.TPLRSNAME.name(), NgvDbSchema.TEMPLATE_RULESETS.STATE.bindClause());
			oSelectQuery.addWhereClauseParameter(p_eState.value());//value for 'STATE'
		}
		else
		{
			oSelectQuery = DbQueryUtils.createSelectQuery(NgvDbSchema.TEMPLATE_RULESETS, NgvDbSchema.TEMPLATE_RULESETS.TPLRSNAME.name(), null);
		}
		
		try
		{
			connection.startTransaction();
			
			listRulesetNames = DbQueryHome.getStringColumnData(connection, oSelectQuery);
			
			connection.commitTransaction();
		}
		catch(SciDbException dbExcep)
		{
			log.error("Error fetching template ruleset names", dbExcep);
			connection.rollbackTransaction();
			throw new SciException("Error fetching template ruleset names", dbExcep);
		}
		catch(SciException sciExcep)
		{
			log.error("Error fetching template ruleset names", sciExcep);
			connection.rollbackTransaction();
			throw sciExcep;
		}
		
		return listRulesetNames;
	}
		public static List<String> getCaseValidationRulesetNames(DbConnection connection, RecordState p_eState,ConfigTplObjType p_etype)
			throws SciException {
		DbQuerySelect oSelectQuery = null;
		List<String> listRulesetNames = null;

		if (p_eState != null) {
			oSelectQuery = DbQueryUtils.createSelectQuery(ConfigTplObjDbSchema.CONFIGOBJECT,ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME.name(), DbQueryUtils.bindWhereClauseWithAnd(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSTATE, ConfigTplObjDbSchema.CONFIGOBJECT.CTOTYPE));
			oSelectQuery.addWhereClauseParameter(p_eState.value());// value for// 'STATE'
			oSelectQuery.addWhereClauseParameter(p_etype.getCode());
			
		} else {
			oSelectQuery = DbQueryUtils.createSelectQuery(ConfigTplObjDbSchema.CONFIGOBJECT,ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME.name(), null);
		}

		try {
			connection.startTransaction();

			listRulesetNames = DbQueryHome.getStringColumnData(connection, oSelectQuery);

			connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			log.error("Error fetching template ruleset names", dbExcep);
			connection.rollbackTransaction();
			throw new SciException("Error fetching template ruleset names", dbExcep);
		} catch (SciException sciExcep) {
			log.error("Error fetching template ruleset names", sciExcep);
			connection.rollbackTransaction();
			throw sciExcep;
		}

		return listRulesetNames;
	}


	public static List<NgvTemplateRuleset> getTemplateRulesets(DbConnection connection, RecordState p_eState) throws SciException
	{
		List<NgvTemplateRuleset> listTemplateRulesets = null;
		DbQuerySelect oSelectQuery = null;
		
		if (p_eState != null)
		{
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(NgvDbSchema.TEMPLATE_RULESETS, NgvDbSchema.TEMPLATE_RULESETS.STATE.bindClause(), NgvDbSchema.TEMPLATE_RULESETS.DTCREATED.name() + " asc");
			oSelectQuery.addWhereClauseParameter(p_eState.value());//value for 'STATE'
		}
		else
		{
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(NgvDbSchema.TEMPLATE_RULESETS, null, NgvDbSchema.TEMPLATE_RULESETS.DTCREATED.name() + " asc");
		}
		
		listTemplateRulesets = fetchTemplateRulesetsFromDb(connection, oSelectQuery);
		
		for (int index = 0; index < listTemplateRulesets.size(); index++)
		{
			listTemplateRulesets.get(index).setTemplateList(listTemplates(connection, listTemplateRulesets.get(index)));
		}
		
		return listTemplateRulesets;
	}
	public static List<CaseDataValidationRuleset> getCaseValidationRulesets(DbConnection connection, RecordState p_eState)
			throws SciException {
		List<CaseDataValidationRuleset> listTemplateRulesets = null;
		DbQuerySelect oSelectQuery = null;

		if (p_eState != null) {
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigTplObjDbSchema.CONFIGOBJECT,
					ConfigTplObjDbSchema.CONFIGOBJECT.CTOSTATE.bindClause(),
					ConfigTplObjDbSchema.CONFIGOBJECT.DTCREATED.name() + " asc");
			oSelectQuery.addWhereClauseParameter(p_eState.value());// value for
																	// 'STATE'
		} else {
			oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigTplObjDbSchema.CONFIGOBJECT, null,
					ConfigTplObjDbSchema.CONFIGOBJECT.DTCREATED.name() + " asc");
		}

		listTemplateRulesets = (List<CaseDataValidationRuleset>) fetchValidationRulesetFromDb(connection, oSelectQuery);

		for (int index = 0; index < listTemplateRulesets.size(); index++) {
			listTemplateRulesets.get(index).setValidationRules(listRules(connection, listTemplateRulesets.get(index)));
		}

		return listTemplateRulesets;
	}

	private static List<NgvTemplateRuleset> fetchTemplateRulesetsFromDb(DbConnection connection, DbQuerySelect oSelectQuery) throws SciException
	{
		List<NgvTemplateRuleset> listTemplateRulesetNames = null;
		NgvTemplateRuleset oTemplateRuleset = null;
		List<Integer> listHeaderContentIds = null;
		int nTemplateHeaderContentSeqId = -1;
		
		listTemplateRulesetNames = new ArrayList<NgvTemplateRuleset>();
		listHeaderContentIds = new ArrayList<Integer>();
		
		try 
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					oTemplateRuleset = readTemplateRulesetObjectFromDb(resultSet);
					listTemplateRulesetNames.add(oTemplateRuleset);
					
					nTemplateHeaderContentSeqId = resultSet.readInt(NgvDbSchema.TEMPLATE_RULESETS.HEADERCONTENTSEQID);
					listHeaderContentIds.add(nTemplateHeaderContentSeqId);
				}
				
				for (int index = 0; index < listHeaderContentIds.size(); index++)
				{
					if (listHeaderContentIds.get(index) != -1)
					{
						try {
							oTemplateRuleset.setHeaderBinaryContent(BinaryContentHomeImpl.readContent(connection, listHeaderContentIds.get(index)));
						} catch (SciException sciExcep) {
							connection.rollbackTransaction();
							log.error("Error saving data in bin content");
							throw sciExcep;
						}
					}
				}
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading Template Rulesets Db", dbExcep);
			throw new SciException("Error reading Template Rulesets from Db", dbExcep);
		}
		
		return listTemplateRulesetNames;
	}
	
	public static NgvTemplateRuleset getTemplateRuleset(DbConnection connection, int p_nRulesetId) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(NgvDbSchema.TEMPLATE_RULESETS, NgvDbSchema.TEMPLATE_RULESETS.TPLRSSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_nRulesetId);//value for 'TPLRSSEQID'
		
		return fetchTemplateRulesetFromDb(connection, oSelectQuery);
	}
	
	public static NgvTemplateRuleset getTemplateRuleset(DbConnection connection, String p_sRulesetName) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(NgvDbSchema.TEMPLATE_RULESETS, NgvDbSchema.TEMPLATE_RULESETS.TPLRSNAME.bindClause());
		oSelectQuery.addWhereClauseParameter(p_sRulesetName);
		
		return fetchTemplateRulesetFromDb(connection, oSelectQuery);
	}
	public static CaseDataValidationRuleset getValidationRuleset(DbConnection connection, int p_nRulesetId) throws SciException {
		DbQuerySelect oSelectQuery = null;

		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigTplObjDbSchema.CONFIGOBJECT,
				ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID.bindClause());
		oSelectQuery.addWhereClauseParameter(p_nRulesetId);// value for
															// 'TPLRSSEQID'

		return fetchValidationRulesetFromDb(connection, oSelectQuery);
	}

	public static CaseDataValidationRuleset getValidationRuleset(DbConnection connection, String p_sRulesetName)
			throws SciException {
		DbQuerySelect oSelectQuery = null;

		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ConfigTplObjDbSchema.CONFIGOBJECT,
				ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME.bindClause());
		oSelectQuery.addWhereClauseParameter(p_sRulesetName);

		return fetchValidationRulesetFromDb(connection, oSelectQuery);
	}

	private static NgvTemplateRuleset fetchTemplateRulesetFromDb(DbConnection connection, DbQuerySelect oSelectQuery) throws SciException
	{
		NgvTemplateRuleset oTemplateRuleset = null;
		int nTemplateHeaderContentSeqId = -1;
		
		try 
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					oTemplateRuleset = readTemplateRulesetObjectFromDb(resultSet);
					
					nTemplateHeaderContentSeqId = resultSet.readInt(NgvDbSchema.TEMPLATE_RULESETS.HEADERCONTENTSEQID);
					break;
				}
				
				if (nTemplateHeaderContentSeqId != -1)
				{
					try {
						oTemplateRuleset.setHeaderBinaryContent(BinaryContentHomeImpl.readContent(connection, nTemplateHeaderContentSeqId));
					} catch (SciException sciExcep) {
						connection.rollbackTransaction();
						log.error("Error saving data in bin content");
						throw sciExcep;
					}
				}
				oTemplateRuleset.setTemplateList(listTemplates(connection, oTemplateRuleset));
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading Template Ruleset Names from Db", dbExcep);
			throw new SciException("Error reading Template Ruleset Names from Db", dbExcep);
		}
		
		return oTemplateRuleset;
	}
	//Added by Rohini
	private static CaseDataValidationRuleset fetchValidationRulesetFromDb(DbConnection connection, DbQuerySelect oSelectQuery)
			throws SciException {
		CaseDataValidationRuleset oCaseValidationRuleset = null;
		
		try {
			connection.startTransaction();

			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery)) {
				while (resultSet.next()) {
					oCaseValidationRuleset = readCaseValidationRulesetObjectFromDb(resultSet);

				}

		
				oCaseValidationRuleset.setValidationRules(listRules(connection, oCaseValidationRuleset));
			}
			connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			connection.rollbackTransaction();
			log.error("Error reading Template Ruleset Names from Db", dbExcep);
			throw new SciException("Error reading Template Ruleset Names from Db", dbExcep);
		}

		return oCaseValidationRuleset;
	}

	//Ended by Rohini
	private static NgvTemplateRuleset readTemplateRulesetObjectFromDb(DbResultSet resultSet) 
			throws SciDbException
	{
		NgvTemplateRuleset oTemplateRuleset = null;
		
		oTemplateRuleset = new NgvTemplateRuleset(null);
		
		oTemplateRuleset.setRulesetId(resultSet.readInt(NgvDbSchema.TEMPLATE_RULESETS.TPLRSSEQID));
		oTemplateRuleset.setRulesetName(resultSet.readString(NgvDbSchema.TEMPLATE_RULESETS.TPLRSNAME));
		oTemplateRuleset.setDefaultTemplateSeqId(resultSet.readInt(NgvDbSchema.TEMPLATE_RULESETS.DEFAULTTPLEQID));
		oTemplateRuleset.setState(RecordState.toEnum(resultSet.readInt(NgvDbSchema.TEMPLATE_RULESETS.STATE)));

		//DEV-NOTE: Header Template ContentSeqId is read separately (NgvDbSchema.TEMPLATE_RULESETS.HEADERCONTENTSEQID)
		
		return oTemplateRuleset;
	}
	private static CaseDataValidationRuleset readCaseValidationRulesetObjectFromDb(DbResultSet resultSet) throws SciDbException {
		CaseDataValidationRuleset oCaseDataValidationRuleset = null;

		oCaseDataValidationRuleset = new CaseDataValidationRuleset();

		oCaseDataValidationRuleset.setSequenceId(resultSet.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSEQID));
		oCaseDataValidationRuleset.setTemplateName(resultSet.readString(ConfigTplObjDbSchema.CONFIGOBJECT.CTONAME));
		oCaseDataValidationRuleset.setState(RecordState.toEnum(resultSet.readInt(ConfigTplObjDbSchema.CONFIGOBJECT.CTOSTATE)));

		// DEV-NOTE: Header Template ContentSeqId is read separately
		// (NgvDbSchema.TEMPLATE_RULESETS.HEADERCONTENTSEQID)

		return oCaseDataValidationRuleset;
	}
	public static boolean isTemplateRulesetNameUnique(DbConnection connection, String p_sProposedName) throws SciException
	{
		boolean bIsTemplateRulesetNameUnique = false;
		
		bIsTemplateRulesetNameUnique = DbQueryHome.isObjectNameUnique(connection, "Template Ruleset Name",NgvDbSchema.TEMPLATE_RULESETS,  p_sProposedName,  
											NgvDbSchema.TEMPLATE_RULESETS.TPLRSNAME.bindClause(), DbQueryParam.createParameter(p_sProposedName));
		
		return bIsTemplateRulesetNameUnique;
	}
	
	
	public static void saveTemplateRuleset(DbConnection connection, UserInfo p_oUserInfo, NgvTemplateRuleset p_oTemplateRuleset, 
			boolean p_bUpdateAttribs, boolean p_bUpdateHeader, boolean p_bUpdateTemplates) throws SciException
	{
		int nExistingHeaderContentSeqId = -1;
		int nNewHeaderContentSeqId = -1;
		
		if (p_bUpdateTemplates)
		{
			log.error("Inline update of templates not supported as yet");
			throw new SciException("Inline update of templates not supported as yet");
		}
		
		try
		{
			connection.startTransaction();
			
			if (p_bUpdateHeader)
			{
				nExistingHeaderContentSeqId = (p_oTemplateRuleset.getHeaderBinaryContent() != null ? p_oTemplateRuleset.getHeaderBinaryContent().getContentSeqId() : -1);
				
				//DEV-NOTE: This can be an INSERT or UPDATE of information
				
				BinaryContentHomeImpl.saveContent(connection, p_oUserInfo, p_oTemplateRuleset.getHeaderBinaryContent());
				nNewHeaderContentSeqId = (p_oTemplateRuleset.getHeaderBinaryContent() != null ? p_oTemplateRuleset.getHeaderBinaryContent().getContentSeqId() : -1);
			}
			
			if (p_bUpdateAttribs || (p_bUpdateHeader && DataUtils.hasChanged(nExistingHeaderContentSeqId, nNewHeaderContentSeqId)))
			{
				updateTemplateRulesetObjectMetadata(connection, p_oUserInfo, p_oTemplateRuleset);
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving template ruleset", dbExcep);
			throw new SciException("Error saving template ruleset", dbExcep);
		}
		catch (SciException sciExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving template ruleset", sciExcep);
			throw sciExcep;
		}
	}
		//Added by Rohini
	public static void saveCaseDataValidationRuleset(DbConnection connection, UserInfo p_oUserInfo,
			CaseDataValidationRuleset p_oValidationRuleset, boolean p_bUpdateAttribs, boolean p_bUpdateHeader,
			boolean p_bUpdateTemplates) throws SciException {
		int nExistingHeaderContentSeqId = -1;
		int nNewHeaderContentSeqId = -1;

		if (p_bUpdateTemplates) {
			log.error("Inline update of templates not supported as yet");
			throw new SciException("Inline update of templates not supported as yet");
		}

		try {
			connection.startTransaction();

			if (p_bUpdateAttribs
					) {
				ConfigTplObjHomeImpl.updateConfigTplObjAttributes(connection, p_oValidationRuleset, p_oUserInfo);
//				updateTemplateRulesetObjectMetadata(connection, p_oUserInfo, p_oValidationRuleset);
			}

			connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			connection.rollbackTransaction();
			log.error("Error saving template ruleset", dbExcep);
			throw new SciException("Error saving template ruleset", dbExcep);
		} catch (SciException sciExcep) {
			connection.rollbackTransaction();
			log.error("Error saving template ruleset", sciExcep);
			throw sciExcep;
		}
	}
	//Ended by Rohini
	public static int saveNewTemplateRuleset(DbConnection connection, UserInfo p_oUserInfo, NgvTemplateRuleset p_oTemplateRuleset) throws SciException
	{
		int nReturnValue = -1;
		int nTemplateRulesetId = -1;
		DbQueryInsert oInsertQuery = null;
		int nTemplateSequenceNum = -1;
		
		nTemplateRulesetId = DbQueryHome.generateSequenceValueForTablePk(NgvDbSchema.TEMPLATE_RULESETS);
		
		oInsertQuery = DbQueryUtils.createInsertQuery(NgvDbSchema.TEMPLATE_RULESETS);
		oInsertQuery.addInsertParameter(nTemplateRulesetId);
		oInsertQuery.addInsertParameter(p_oTemplateRuleset.getRulesetName());
		oInsertQuery.addInsertParameter((p_oTemplateRuleset.getHeaderBinaryContent() != null ? p_oTemplateRuleset.getHeaderBinaryContent().getContentSeqId() : -1));
		oInsertQuery.addInsertParameter(p_oTemplateRuleset.getDefaultTemplateSeqId());
		oInsertQuery.addInsertParameter(p_oTemplateRuleset.getState().value());
		oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
		oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
		oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
		
		try
		{
			connection.startTransaction();

			if (p_oTemplateRuleset.getHeaderBinaryContent() != null)
			{
				BinaryContentHomeImpl.saveContent(connection, p_oUserInfo, p_oTemplateRuleset.getHeaderBinaryContent());
			}
			
			if (p_oTemplateRuleset.getTemplateList().size() > 0)
			{
				nTemplateSequenceNum = 0;
				for (NgvTemplate oTemplate : p_oTemplateRuleset.getTemplateList())
				{
					//DEV-NOTE: This is to make the sequence number 1-based
					nTemplateSequenceNum++;
					saveNewTemplate(connection, p_oUserInfo, oTemplate, nTemplateSequenceNum);
				}
			}
			
			nReturnValue = connection.executeQuery(oInsertQuery);
			
			p_oTemplateRuleset.setRulesetId(nTemplateRulesetId);
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving new template ruleset", dbExcep);
			throw new SciException("Error saving new template ruleset", dbExcep);
		}
		catch (SciException sciExcep) 
		{
			connection.rollbackTransaction();
			log.error("Error saving new template ruleset", sciExcep);
			throw sciExcep;
		}
		return nReturnValue;
	}
	
	private static void updateTemplateRulesetGroupMd(DbConnection connection, UserInfo p_oUserInfo, int p_nTemplateRulesetId) throws SciException
	{
		DbQueryUpdate oUpdateQuery = null;
		
		try
		{
			connection.startTransaction();
			
			oUpdateQuery = DbQueryUtils.createUpdateQuery(NgvDbSchema.TEMPLATE_RULESETS,
					DbQueryUtils.bindUpdateClause(NgvDbSchema.TEMPLATE_RULESETS.DTLASTUPDATEDGRP, NgvDbSchema.TEMPLATE_RULESETS.USRLASTUPDATEDGRP),
					NgvDbSchema.TEMPLATE_RULESETS.TPLRSSEQID.bindClause());
			
			oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());
			
			oUpdateQuery.addWhereClauseParameter(p_nTemplateRulesetId);
			
			try
			{
				connection.startTransaction();
				
				connection.executeQuery(oUpdateQuery);
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error updating template ruleset group", dbExcep);
				throw new SciException("Error updating template ruleset group", dbExcep);
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating template ruleset group", dbExcep);
			throw new SciException("Error updating template ruleset group", dbExcep);
		}
	}
	
	private static int updateTemplateRulesetObjectMetadata(DbConnection connection, UserInfo p_oUserInfo, NgvTemplateRuleset p_oTemplateRuleset) throws SciException
	{
		int nReturnValue = -1;
		DbQueryUpdate oUpdateQuery = null;
		
		oUpdateQuery = DbQueryUtils.createUpdateQuery(NgvDbSchema.TEMPLATE_RULESETS,
				DbQueryUtils.bindUpdateClause(NgvDbSchema.TEMPLATE_RULESETS.TPLRSNAME, NgvDbSchema.TEMPLATE_RULESETS.HEADERCONTENTSEQID, 
						NgvDbSchema.TEMPLATE_RULESETS.DEFAULTTPLEQID, NgvDbSchema.TEMPLATE_RULESETS.STATE,
						NgvDbSchema.TEMPLATE_RULESETS.DTLASTUPDATEDREC, NgvDbSchema.TEMPLATE_RULESETS.USRLASTUPDATEDREC,
						NgvDbSchema.TEMPLATE_RULESETS.DTLASTUPDATEDGRP, NgvDbSchema.TEMPLATE_RULESETS.USRLASTUPDATEDGRP),
				NgvDbSchema.TEMPLATE_RULESETS.TPLRSSEQID.bindClause());
		
		oUpdateQuery.addUpdateParameter(p_oTemplateRuleset.getRulesetName());
		oUpdateQuery.addUpdateParameter((p_oTemplateRuleset.getHeaderBinaryContent() != null ? p_oTemplateRuleset.getHeaderBinaryContent().getContentSeqId() : -1));
		oUpdateQuery.addUpdateParameter(p_oTemplateRuleset.getDefaultTemplateSeqId());
		oUpdateQuery.addUpdateParameter(p_oTemplateRuleset.getState().value());
		oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());
		oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
		oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());
		
		oUpdateQuery.addWhereClauseParameter(p_oTemplateRuleset.getRulesetId());
		
		try
		{
			connection.startTransaction();
			
			nReturnValue = connection.executeQuery(oUpdateQuery);
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating template ruleset metadata", dbExcep);
			throw new SciException("Error updating template ruleset metadata", dbExcep);
		}
		
		return nReturnValue;
	}

	public static List<NgvTemplate> listTemplates(DbConnection connection, NgvTemplateRuleset p_oTemplateRuleset) throws SciException
	{
		List<NgvTemplate> listTemplates = null;
		DbQuerySelect oSelectQuery = null;
		
		listTemplates = new ArrayList<NgvTemplate>();
	
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(NgvDbSchema.TEMPLATE, NgvDbSchema.TEMPLATE.PARENTTPLRSID.bindClause(), NgvDbSchema.TEMPLATE.TEMPLATESEQID.name() + " asc");
		oSelectQuery.addWhereClauseParameter(p_oTemplateRuleset.getRulesetId());
		
		listTemplates = fetchListOfTemplatesFromDb(connection, oSelectQuery);
		
		return listTemplates;
	}

	//Added by Rohini
	
	public static List<CaseDataValidationRule> listRules(DbConnection connection, CaseDataValidationRuleset p_oCaseValidationRuleset)
			throws SciException {
		List<CaseDataValidationRule> listRules = null;
		DbQuerySelect oSelectQuery = null;

		listRules = new ArrayList<CaseDataValidationRule>();

		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ValidationDbSchema.VALTEMPLATE,
				ValidationDbSchema.VALTEMPLATE.CTOSEQID.bindClause(), ValidationDbSchema.VALTEMPLATE.VALSEQID.name() + " asc");
		oSelectQuery.addWhereClauseParameter(p_oCaseValidationRuleset.getSequenceId());

		listRules = fetchListOfRulesFromDb(connection, oSelectQuery);

		return listRules;
	}
	
	private static List<CaseDataValidationRule> fetchListOfRulesFromDb(DbConnection connection, DbQuerySelect oSelectQuery)
			throws SciException {
		List<CaseDataValidationRule> listRules = null;
		CaseDataValidationRule oRule = null;

		listRules = new ArrayList<CaseDataValidationRule>();
		

		try {
			connection.startTransaction();

			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery)) {
				while (resultSet.next()) {
					oRule = readRuleObjectFromDb(resultSet);
					listRules.add(oRule);

					}
			}

	

			connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			connection.rollbackTransaction();
			log.error("Error fetching list of templates", dbExcep);
			throw new SciException("Error fetching list of templates", dbExcep);
		} catch (SciException sciExcep) {
			connection.rollbackTransaction();
			log.error("Error fetching list of templates", sciExcep);
			throw sciExcep;
		}

		return listRules;
	}
	//ended by Rohini

	
	private static List<NgvTemplate> fetchListOfTemplatesFromDb(DbConnection connection, DbQuerySelect oSelectQuery) throws SciException
	{
		List<NgvTemplate> listTemplates = null;
		NgvTemplate oTemplate = null;
		List<Integer> listContentIds = null;
		int nContentSeqId = -1;
		
		listTemplates = new ArrayList<NgvTemplate>();
		listContentIds = new ArrayList<Integer>();
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					oTemplate = readTemplateObjectFromDb(resultSet);
					listTemplates.add(oTemplate);
					
					nContentSeqId = resultSet.readInt(NgvDbSchema.TEMPLATE.CONTENTSEQID);
					listContentIds.add(nContentSeqId);
				}
			}
			
			for (int index = 0; index < listContentIds.size(); index++)
			{
				if (listContentIds.get(index) != -1)
				{
					listTemplates.get(index).setTemplateContent(BinaryContentHomeImpl.readContent(connection, listContentIds.get(index)));
				}
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error fetching list of templates", dbExcep);
			throw new SciException("Error fetching list of templates", dbExcep);
		}
		catch (SciException sciExcep)
		{
			connection.rollbackTransaction();
			log.error("Error fetching list of templates", sciExcep);
			throw sciExcep;
		}
		
		return listTemplates;
	}
	
	// Added by Rohini
	public static void saveRule(DbConnection connection, UserInfo p_oUserInfo, CaseDataValidationRule p_oRule,
			CaseDataValidationRuleset p_oCaseValidationRuleset, boolean p_bUpdateCriteria, boolean p_bUpdateApplicability)
			throws SciException {
		boolean bUpdateOperation = false;
		DbQueryUpdate oUpdateQuery = null;
		@SuppressWarnings("unused")
		int nReturnValue = -1;

		bUpdateOperation = (p_oRule.getRuleSeqId() > 0);

		if (!bUpdateOperation) {
			log.error("Method for existing template save");
			throw new SciException("Method for existing template save");
		}

		// TODO:Use p_bUpdateCriteria

		oUpdateQuery = DbQueryUtils.createUpdateQuery(ValidationDbSchema.VALTEMPLATE,
				DbQueryUtils.bindUpdateClause(ValidationDbSchema.VALTEMPLATE.VALNAME,
						ValidationDbSchema.VALTEMPLATE.CTOSEQID, ValidationDbSchema.VALTEMPLATE.RULETYPE,
						ValidationDbSchema.VALTEMPLATE.CRITERIA, ValidationDbSchema.VALTEMPLATE.APPLICABILITY,
						ValidationDbSchema.VALTEMPLATE.DTLASTUPDATED, ValidationDbSchema.VALTEMPLATE.USRLASTUPDATED),
				ValidationDbSchema.VALTEMPLATE.VALSEQID.bindClause());

		try {
			connection.startTransaction();

			// DEV-NOTE: Bind this after the BinaryContent is saved
			oUpdateQuery.addUpdateParameter(p_oRule.getRuleName());
			oUpdateQuery.addUpdateParameter(p_oCaseValidationRuleset.getSequenceId());
			oUpdateQuery.addUpdateParameter(p_oRule.getRuleType().name());
			oUpdateQuery.addUpdateParameter(p_oRule.getCriteria());
			oUpdateQuery.addUpdateParameter(p_oRule.getApplicability());
			oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());

			oUpdateQuery.addWhereClauseParameter(p_oRule.getRuleSeqId());

			nReturnValue = connection.executeQuery(oUpdateQuery);

			ConfigTplObjHomeImpl.updateConfigTplObjGroup(connection, p_oCaseValidationRuleset.getSequenceId(), p_oUserInfo);
	
			connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			connection.rollbackTransaction();
			log.error("Error saving template", dbExcep);
			throw new SciException("Error saving template", dbExcep);
		}
	}

	// Ended by Rohini
	private static NgvTemplate readTemplateObjectFromDb(DbResultSet resultSet) throws SciDbException, SciException
	{
		NgvTemplate oTemplate = null;
		
		oTemplate = new NgvTemplate();
		oTemplate.setTemplateSeqId(resultSet.readInt(NgvDbSchema.TEMPLATE.TEMPLATESEQID));
		oTemplate.setTemplateName(resultSet.readString(NgvDbSchema.TEMPLATE.TEMPLATENAME));
		oTemplate.setParentRulesetId(resultSet.readInt(NgvDbSchema.TEMPLATE.PARENTTPLRSID));
		oTemplate.setTemplateCriteria(resultSet.readString(NgvDbSchema.TEMPLATE.TEMPLATECRITERIA));
		
		//DEV-NOTE: TemplateSeqId is read and handled separately (NgvDbSchema.TEMPLATE.CONTENTSEQID);
		
		return oTemplate;
	}
	//Added by Rohini
	private static CaseDataValidationRule readRuleObjectFromDb(DbResultSet resultSet) throws SciDbException, SciException {
		CaseDataValidationRule oRule = null;

		oRule = new CaseDataValidationRule();
		oRule.setRuleSeqId(resultSet.readInt(ValidationDbSchema.VALTEMPLATE.VALSEQID));
		oRule.setRuleName(resultSet.readString(ValidationDbSchema.VALTEMPLATE.VALNAME));
		/////////TODO:REVIEW-AND_REMOVE:::oRule.setM_nParentSeqId(resultSet.readInt(ValidationDbSchema.VALTEMPLATE.PARENTVALRSID));
		oRule.setCriteria(resultSet.readString(ValidationDbSchema.VALTEMPLATE.CRITERIA));
		oRule.setRuleType(Type.valueOf(resultSet.readString(ValidationDbSchema.VALTEMPLATE.RULETYPE)));
		oRule.setApplicability(resultSet.readString(ValidationDbSchema.VALTEMPLATE.APPLICABILITY));

		// DEV-NOTE: TemplateSeqId is read and handled separately
		// (NgvDbSchema.TEMPLATE.CONTENTSEQID);

		return oRule;
	}
	//Ended by Rohini
	public static void saveTemplate(DbConnection connection, UserInfo p_oUserInfo, NgvTemplate p_oTemplate, NgvTemplateRuleset p_oTemplateRuleset,
			boolean p_bUpdateCriteria, boolean p_bUpdateContent) throws SciException
	{
		boolean bUpdateOperation = false;
		DbQueryUpdate oUpdateQuery = null;
		@SuppressWarnings("unused")
		int nReturnValue = -1;
		
		bUpdateOperation = (p_oTemplate.getTemplateSeqId()  > 0);
		
		if (!bUpdateOperation)
		{
			log.error("Method for existing template save");
			throw new SciException("Method for existing template save");
		}
		
		//TODO:Use p_bUpdateCriteria
		
		oUpdateQuery = DbQueryUtils.createUpdateQuery(NgvDbSchema.TEMPLATE, 
						DbQueryUtils.bindUpdateClause(NgvDbSchema.TEMPLATE.TEMPLATENAME, 
							NgvDbSchema.TEMPLATE.PARENTTPLRSID, NgvDbSchema.TEMPLATE.CONTENTSEQID, 
							NgvDbSchema.TEMPLATE.TEMPLATECRITERIA,
							NgvDbSchema.TEMPLATE.DTLASTUPDATED, NgvDbSchema.TEMPLATE.USRLASTUPDATED),
						NgvDbSchema.TEMPLATE.TEMPLATESEQID.bindClause()); 
		
		try
		{
			connection.startTransaction();
			
			if (p_bUpdateContent)
			{
				BinaryContentHomeImpl.saveContent(connection, p_oUserInfo, p_oTemplate.getTemplateContent());
			}
			
			//DEV-NOTE: Bind this after the BinaryContent is saved
			oUpdateQuery.addUpdateParameter(p_oTemplate.getTemplateName());
			oUpdateQuery.addUpdateParameter(p_oTemplate.getParentRulesetId());
			oUpdateQuery.addUpdateParameter(p_oTemplate.getTemplateContent().getContentSeqId());
			oUpdateQuery.addUpdateParameter(p_oTemplate.getTemplateCriteria());
			oUpdateQuery.addUpdateParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oUpdateQuery.addUpdateParameter(p_oUserInfo.getUserSeqId());
			
			oUpdateQuery.addWhereClauseParameter(p_oTemplate.getTemplateSeqId());
			
			nReturnValue = connection.executeQuery(oUpdateQuery);
			
			updateTemplateRulesetGroupMd(connection, p_oUserInfo, p_oTemplateRuleset.getRulesetId());
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving template", dbExcep);
			throw new SciException("Error saving template", dbExcep);
		}
		catch (SciException sciExcep) 
		{
			connection.rollbackTransaction();
			log.error("Error saving template", sciExcep);
			throw sciExcep;
		}
	}
	
	public static boolean isTemplateNameUnique(DbConnection connection, String p_sProposedTemplateName, int p_nParentRulesetId) throws SciException
	{
		boolean bIsTemplateNameUnique = false;
		
		bIsTemplateNameUnique = DbQueryHome.isObjectNameUnique(connection, "Template", NgvDbSchema.TEMPLATE, p_sProposedTemplateName, 
								DbQueryUtils.bindWhereClauseWithAnd(NgvDbSchema.TEMPLATE.TEMPLATENAME, NgvDbSchema.TEMPLATE.PARENTTPLRSID),
								DbQueryParam.createParameter(p_sProposedTemplateName), 
								DbQueryParam.createParameter(p_nParentRulesetId));
		
		return bIsTemplateNameUnique;
	}
	
	public static void saveNewTemplate(DbConnection connection, UserInfo p_oUserInfo, NgvTemplate p_oTemplate, int p_nNewTemplateSequenceNum) throws SciException
	{
		boolean bUpdateOperation = false;
		int nTemplateSeqNumber = 0;
		DbQueryInsert oInsertQuery = null;
		@SuppressWarnings("unused")
		int nReturnValue = -1;
		
		bUpdateOperation = (p_oTemplate.getTemplateSeqId()  > 0);
		if (bUpdateOperation)
		{
			log.error("Method for new template save");
			throw new SciException("Method for new template save");
		}
		
		int nTemplateId = -1;
		
		nTemplateId = DbQueryHome.generateSequenceValueForTablePk(NgvDbSchema.TEMPLATE);
		nTemplateSeqNumber = 10 * p_nNewTemplateSequenceNum;//TODO:Change this
		
		
		try
		{
			connection.startTransaction();
			BinaryContentHomeImpl.saveContent(connection, p_oUserInfo, p_oTemplate.getTemplateContent());
			//DEV-NOTE: Following binding needs to be done after content is saved so that the ContentSeqId is available
			oInsertQuery = DbQueryUtils.createInsertQuery(NgvDbSchema.TEMPLATE);
			oInsertQuery.addInsertParameter(nTemplateId);
			oInsertQuery.addInsertParameter(p_oTemplate.getTemplateName());
			oInsertQuery.addInsertParameter(p_oTemplate.getParentRulesetId());
			oInsertQuery.addInsertParameter(nTemplateSeqNumber);
			oInsertQuery.addInsertParameter(p_oTemplate.getTemplateContent().getContentSeqId());
			oInsertQuery.addInsertParameter(p_oTemplate.getTemplateCriteria());
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
			
			nReturnValue = connection.executeQuery(oInsertQuery);
			
			p_oTemplate.setTemplateSeqId(nTemplateId);
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving new template", dbExcep);
			throw new SciException("Error saving new template", dbExcep);
		} 
		catch (SciException sciExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving new template", sciExcep);
			throw sciExcep;
		}
	}
// Added by Rohini
	public static void saveNewRule(DbConnection connection, UserInfo p_oUserInfo, CaseDataValidationRule p_oRule,
			int p_nNewTemplateSequenceNum,int parentSeqId) throws SciException {
		boolean bUpdateOperation = false;

		int nTemplateSeqNumber = 0;
		DbQueryInsert oInsertQuery = null;
		@SuppressWarnings("unused")
		int nReturnValue = -1;

		bUpdateOperation = (p_oRule.getRuleSeqId() > 0);
		if (bUpdateOperation) {
			log.error("Method for new template save");
			throw new SciException("Method for new template save");
		}

		int nRuleId = -1;

		nRuleId = DbQueryHome.generateSequenceValueForTablePk(ValidationDbSchema.VALTEMPLATE);
		nTemplateSeqNumber = 10 * p_nNewTemplateSequenceNum;// TODO:Change this

		try {
			connection.startTransaction();

			// BinaryContentHomeImpl.saveContent(connection, p_oUserInfo,
			// p_oTemplate.getTemplateContent());

			// DEV-NOTE: Following binding needs to be done after content is
			// saved so that the ContentSeqId is available
			oInsertQuery = DbQueryUtils.createInsertQuery(ValidationDbSchema.VALTEMPLATE);
			oInsertQuery.addInsertParameter(nRuleId);
			oInsertQuery.addInsertParameter(p_oRule.getRuleName());
			oInsertQuery.addInsertParameter(parentSeqId);
			oInsertQuery.addInsertParameter(nTemplateSeqNumber);
			oInsertQuery.addInsertParameter(p_oRule.getRuleType().name());
			oInsertQuery.addInsertParameter(p_oRule.getCriteria());
			oInsertQuery.addInsertParameter(p_oRule.getApplicability());

			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());
			oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			oInsertQuery.addInsertParameter(p_oUserInfo.getUserSeqId());

			nReturnValue = connection.executeQuery(oInsertQuery);

			p_oRule.setRuleSeqId(nRuleId);

			connection.commitTransaction();
		} catch (SciDbException sciExcep) {
			connection.rollbackTransaction();
			log.error("Error saving new template", sciExcep);
			throw new SciException("Error saving new template", sciExcep);
		}
	}
	//Ended by Rohini
}
