package com.sciformix.sciportal.apps.ngv;


public class CaseDataValidationRule
{
	
	public static enum Type
	{
		validity, error, warning;
	}
	
	private int m_nRuleSeqId = 0;
	private String m_sRuleName = null;
	private Type m_eRuleType = null;
	private String m_sCriteria = null;
	private String m_Applicability = null;
	
	
	public CaseDataValidationRule(String m_sRuleeName,Type m_sRuleName )
	{
		super();
		this.m_sRuleName = m_sRuleeName;
		this.m_eRuleType = m_sRuleName;
		
	}

	public CaseDataValidationRule()
	{
		// TODO Auto-generated constructor stub
	}
	
	public int getRuleSeqId()
	{
		return m_nRuleSeqId;
	}
	
	public void setRuleSeqId(int p_nRuleSeqId)
	{
		m_nRuleSeqId = p_nRuleSeqId;
	}

	public String getRuleName() {
		return m_sRuleName;
	}

	public void setRuleName(String p_sRuleeName) {
		this.m_sRuleName = p_sRuleeName;
	}

	public Type getRuleType() {
		return m_eRuleType;
	}

	public void setRuleType(Type p_eRuleType) {
		this.m_eRuleType = p_eRuleType;
	}

	public String getCriteria() {
		return m_sCriteria;
	}

	public void setCriteria(String p_sCriteria) {
		this.m_sCriteria = p_sCriteria;
	}

	public String getApplicability() {
		return m_Applicability;
	}

	public void setApplicability(String p_Applicability) {
		this.m_Applicability = p_Applicability;
	}

	//Added by Rohini
	public static boolean hasChanged(Type p_sOldValue, Type p_sNewValue)
	{
		if (p_sOldValue == null && p_sNewValue == null)
		{
			//Both are null - hence, no change
			return false;
		}
		else if (p_sOldValue == null || p_sNewValue == null)
		{
			//At least one is null and one is not - hence, something has changed
			return true;
		}
		else
		{
			//Both are not null - check for equality
			return (!p_sOldValue.equals(p_sNewValue));
		}
	}
	//Ended by Rohini
	
}
