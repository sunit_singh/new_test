package com.sciformix.sciportal.apps.ngv;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemarkerUtils
{
	private static final Logger log = LoggerFactory.getLogger(FreemarkerUtils.class);
	
	private static  Configuration m_cfg = null;
	
	static
	{
		m_cfg = new Configuration(Configuration.VERSION_2_3_25);
		
		m_cfg.setClassForTemplateLoading(UseNarrativeTemplate.class, "templates");
		m_cfg.setLogTemplateExceptions(false);
		m_cfg.setDefaultEncoding("UTF-8");
		m_cfg.setLocale(Locale.US);
    }
	
	public static List<Object> replaceFreemarkerTemplate(String sTemplateText, Map<String, Object> mapMarkerValues) throws SciException
	{
		Template template = null;
        StringWriter stringWriter =null;
        BufferedWriter bufferedWriter = null;
        String sNarrativeText = null;
		List<String> listMissingMarker = new ArrayList<>();
		List<Object> oListOfNarrativeAndMissingMarker = new ArrayList<>();
		HashSet<String> setOfMissingMarker = new HashSet<String>();
		
        try
        {
        	//cfg = createConfiguration(listErrors);
	       	log.debug("Template processing started");
       	 
	       	template = new Template("narrative", new StringReader(sTemplateText), m_cfg);
        }
        catch(IOException ioExcep)
        {
        	log.error("Error in processing template", ioExcep);
        	throw new SciException("Error in processing template", ioExcep);
        }
        
        try
        {
        	stringWriter = new StringWriter();
        	bufferedWriter = new BufferedWriter(stringWriter);
        	m_cfg.setTemplateExceptionHandler(new NarrativeExceptionHandler(listMissingMarker));
    		template.process(mapMarkerValues, bufferedWriter);
    		 
    		bufferedWriter.flush();
    		log.debug("Template processing completed and narrative written into file");
    		 
    		sNarrativeText = stringWriter.getBuffer().toString();
        }
        catch(IOException ioExcep)
        {
        	log.error("Error in processing template", ioExcep);
        	throw new SciException("Error in processing template", ioExcep);
        }
        catch (TemplateException templateExcep)
        {
        	log.error("Error in processing template", templateExcep);
        	throw new SciException("Error in processing template", templateExcep);
		} 
        finally
        {
            if (bufferedWriter != null)
            {
            	try
            	{
            		bufferedWriter.close();
            	}
                catch(IOException ioExcep)
                {
                	log.error("Error in closing buffered writer stream - IGNORE and continue ", ioExcep);
                }
            }
            if (stringWriter != null)
            {
            	try
            	{
                	stringWriter.close();
            	}
                catch(IOException ioExcep)
                {
                	log.error("Error in closing buffered writer stream - IGNORE and continue", ioExcep);
                }
            }
        }
        oListOfNarrativeAndMissingMarker.add(sNarrativeText);
        setOfMissingMarker.addAll(listMissingMarker);
        listMissingMarker.clear();
        listMissingMarker.addAll(setOfMissingMarker);
        oListOfNarrativeAndMissingMarker.add(listMissingMarker);
        return oListOfNarrativeAndMissingMarker;
	}

	/*private static Configuration createConfiguration(List<SciError> listErrors) throws IOException
	{
		 @SuppressWarnings("deprecation")
		//Configuration cfg = new Configuration();
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
			
		cfg.setClassForTemplateLoading(UseNarrativeTemplate.class, "templates");
        cfg.setLogTemplateExceptions(false);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(new NarrativeExceptionHandler(listErrors));
        return cfg;
	}*/
}
