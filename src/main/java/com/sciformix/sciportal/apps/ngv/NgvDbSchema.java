package com.sciformix.sciportal.apps.ngv;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class NgvDbSchema
{
	public static TemplateRulesets TEMPLATE_RULESETS = new TemplateRulesets();
	public static Template TEMPLATE = new Template();
	
	public static class TemplateRulesets extends DbTable
	{
		public DbColumn TPLRSSEQID = null;
		public DbColumn TPLRSNAME = null;
		public DbColumn HEADERCONTENTSEQID = null;
		//public DbColumn HEADERTPLSEQID = null;
		public DbColumn DEFAULTTPLEQID = null;
		public DbColumn STATE = null;
		public DbColumn DTCREATED = null;
		public DbColumn USRCREATED = null;
		public DbColumn DTLASTUPDATEDREC = null;
		public DbColumn USRLASTUPDATEDREC = null;
		public DbColumn DTLASTUPDATEDGRP = null;
		public DbColumn USRLASTUPDATEDGRP = null;
		
		public TemplateRulesets()
		{
			super("APP_NGV_TEMPLATERULESETS");
			setPkSequenceName("APP_NGV_TPLRSSEQID_SEQ");
			
			TPLRSSEQID = addColumn("TPLRSSEQID");
			TPLRSNAME = addColumn("TPLRSNAME");
			HEADERCONTENTSEQID = addColumn("HEADERCONTENTSEQID");
			DEFAULTTPLEQID = addColumn("DEFAULTTPLEQID");
			STATE = addColumn("STATE");
			//HEADERTPLSEQID = addColumn("HEADERTPLSEQID");
			DTCREATED = addColumn("DTCREATED");
			USRCREATED = addColumn("USRCREATED");
			DTLASTUPDATEDREC = addColumn("DTLASTUPDATEDREC");
			USRLASTUPDATEDREC = addColumn("USRLASTUPDATEDREC");
			DTLASTUPDATEDGRP = addColumn("DTLASTUPDATEDGRP");
			USRLASTUPDATEDGRP = addColumn("USRLASTUPDATEDGRP");
		}
	}
	
	public static class Template extends DbTable
	{
		public DbColumn TEMPLATESEQID = null;
		public DbColumn TEMPLATENAME = null;
		public DbColumn PARENTTPLRSID = null;
		public DbColumn TEMPLATESEQNUM = null;
		public DbColumn CONTENTSEQID = null;
		public DbColumn TEMPLATECRITERIA = null;
		public DbColumn DTCREATED = null;
		public DbColumn USRCREATED = null;
		public DbColumn DTLASTUPDATED = null;
		public DbColumn USRLASTUPDATED = null;
		
		public Template()
		{
			super("APP_NGV_TEMPLATES");
			setPkSequenceName("APP_NGV_TPLSEQID_SEQ");
			
			TEMPLATESEQID = addColumn("TPLSEQID");
			TEMPLATENAME = addColumn("TPLNAME");
			PARENTTPLRSID = addColumn("TPLRSSEQID");
			TEMPLATESEQNUM = addColumn("TPLSEQNUM");
			CONTENTSEQID = addColumn("CONTENTSEQID");
			TEMPLATECRITERIA = addColumn("TPLCRITERIA");
			DTCREATED = addColumn("DTCREATED");
			USRCREATED = addColumn("USRCREATED");
			DTLASTUPDATED = addColumn("DTLASTUPDATED");
			USRLASTUPDATED = addColumn("USRLASTUPDATED");
		}
	}
	
}
