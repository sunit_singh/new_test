package com.sciformix.sciportal.apps.ngv;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.sciportal.config.ConfigTplObj;

public class CaseDataValidationRuleset extends ConfigTplObj
{
	private List<CaseDataValidationRule> m_listDataValidationRules = null;
	
	public CaseDataValidationRuleset()
	{
		super(ConfigTplObjType.CDVR);
		
		m_listDataValidationRules = new ArrayList<CaseDataValidationRule>();
	}
	
	public CaseDataValidationRuleset(int p_projectId,  String p_templateName,
			RecordState p_state, CreationMode p_creationMode, Date p_createdOn, int p_createdBy)
	{
		super(p_projectId, ConfigTplObjType.CDVR, p_templateName, p_state, p_creationMode);
		
		m_listDataValidationRules = new ArrayList<CaseDataValidationRule>();
	}

	
	public List<CaseDataValidationRule> getValidationRules()
	{
		return m_listDataValidationRules;
	}

	public void setValidationRules(List<CaseDataValidationRule> m_listRules)
	{
		this.m_listDataValidationRules = m_listRules;
	}
	
	public List<String> getRuleNameList()
	{
		List<String> listRuleNames = null;
		
		listRuleNames = new ArrayList<String>(m_listDataValidationRules.size());
		
		for (CaseDataValidationRule oRule : m_listDataValidationRules)
		{
			listRuleNames.add(oRule.getRuleName());
		}
		
		return listRuleNames;
	}
	
	public CaseDataValidationRule getRule(String p_sRuleName)
	{
		for (CaseDataValidationRule oRule : m_listDataValidationRules)
		{
			if (oRule.getRuleName().equalsIgnoreCase(p_sRuleName))
			{
				return oRule;
			}
		}
		return null;
	}
	
	public CaseDataValidationRule getRule(int p_sRuleSeqId)
	{
		if (p_sRuleSeqId == -1)
		{
			return null;
		}
		for (CaseDataValidationRule ocCaseDataValidationRule : m_listDataValidationRules)
		{
			if (ocCaseDataValidationRule.getRuleSeqId() == p_sRuleSeqId)
			{
				return ocCaseDataValidationRule;
			}
		}
		return null;
	}
	public void addValidationRule(CaseDataValidationRule p_oValdiationRule)
	{
		m_listDataValidationRules.add(p_oValdiationRule);
	}

	
	
}
