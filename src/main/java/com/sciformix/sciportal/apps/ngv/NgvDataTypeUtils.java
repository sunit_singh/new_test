package com.sciformix.sciportal.apps.ngv;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.utils.NumberToWord;
import com.sciformix.sciportal.config.ConfigHome;


public class NgvDataTypeUtils 
{
	private static final Logger log = LoggerFactory.getLogger(NgvDataTypeUtils.class);
	
	static String DEFAULT_DATE_FORMAT = "dd-MMM-yyyy";
	Map<String, String>map;
	private  String sDateFormat = null;

	public String getDateFormat() 
	{
		return sDateFormat;
	}

	public void setsDateFormat(String sDateFormat) 
	{
		this.sDateFormat = sDateFormat;
	}


	public String getExpectedDateFormat(String caseReceivedDate, String dateFormat)
	{
		@SuppressWarnings("deprecation")
		Date newDate=new Date(caseReceivedDate);
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		String formattedDate = formatter.format(newDate);
		return formattedDate;
	}
		
	public  Object getKeyValue(String key, Object keyValue, String p_sReplaceWithUnspecifiedIfNull, boolean p_bNullValueFlag)
	 {	
		boolean isNumeric = false;		
		boolean isDate = false;
		List<String> listOfValues = null;
		String[] sValues = null;
		if(keyValue!=null)
		{
			isNumeric = checkIfNumeric(keyValue.toString());
			

			if (isNumeric) 
			{
				return getNumericValue(key, keyValue.toString());
			}
			
			if(p_sReplaceWithUnspecifiedIfNull.contains(key) && (keyValue==null || keyValue.toString().equals("")))
			{
				return "unspecified";
			}	
			
			if(("".equals(keyValue.toString())|| keyValue == null) && p_bNullValueFlag)
			{
				return "<span style=\"color:blue;font-weight:bold\">["+key+"]</span>";
			}
			else
			{
				
				//if(keyValue.toString().contains(":"))
				if(keyValue.toString().contains("~~~"))
				{
					listOfValues = new ArrayList<>();
					//sValues = keyValue.toString().split(":");
					sValues = keyValue.toString().split("~~~");
					for(int nIndex = 0; nIndex < sValues.length; nIndex++)
					{
						isDate = checkIfDateString(sValues[nIndex].trim());
						if (isDate) 
						{
							listOfValues.add(getActualFormattedDateValue(sValues[nIndex].trim()));
						}
						else
						{
							listOfValues.add(sValues[nIndex].trim());
						}
					}
					return listOfValues;
				}else
				{
					isDate = checkIfDateString(keyValue.toString().trim());
					if (isDate) 
					{
						return getActualFormattedDateValue(keyValue.toString().trim());
					}
					else
					{
						return keyValue.toString();
					}
				}
			}
		}
		return ""; 
	 }

	private  String getActualFormattedDateValue(String p_sDateValue) 
	{
		String sIncomingDateFormat = null;
		String sFormattedDate = null;
		String sDateFormat = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		SimpleDateFormat formatter = null;
		
		if(p_sDateValue!=null)
		{
			sIncomingDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_CSV.toString());
			sDateFormat = getDateFormat();
			if (sDateFormat == null)
			{
				sDateFormat = DEFAULT_DATE_FORMAT;
			}
			//TODO: Check this
			try
			{
				incomingformatter = new SimpleDateFormat(sIncomingDateFormat);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
				
			}
			catch(ParseException parseExcep)
			{
				log.error("Date formatting failed for: Date/Format - " + p_sDateValue +  "/" + sIncomingDateFormat, parseExcep);
				return "";
			}
			
			formatter = new SimpleDateFormat(sDateFormat);
			sFormattedDate = formatter.format(oFormattedDate);
		}
		else
		{
			return "";
		}
		return sFormattedDate;
	}

	@SuppressWarnings("unused")
	private  String getNumericValue(String key, String keyValue) 
	{
		String result = "";
		Integer numberValue = null;
		numberValue = Integer.parseInt(keyValue);
		if(numberValue == null)
		{
			return "unspecified";
		}
		else
		{
			switch(key)
			{
			case "patnt_curr_age": 
			// result = keyValue+" "+getPatientAgeValue(numberValue);
		        result = keyValue ;
		        break;
			default:
				result = keyValue!=null?keyValue:result;
				break;
			}
		}
		return result;
	}

	private  boolean checkIfDateString(String key) 
	{
		String sIncomingDateFormat = null;
		SimpleDateFormat formatter = null;
		
		try
		{
			sIncomingDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_CSV.toString());
			formatter = new SimpleDateFormat(sIncomingDateFormat);
			formatter.parse(key);
		}
		catch(Exception excep)
		{
			return false;
		}
		return true;
	}

	private  boolean checkIfNumeric(String key) 
	{
		try
		{
			Integer.parseInt(key);
		}
		catch(Exception excep)
		{
			return false;
		}
		
		return true;
	}
	
	public static String getDaysDiffBetweenDates(String p_sDateOne, String p_sDateTwo)
	{
		log.debug("getting difference between the dates - {}/{}", p_sDateOne, p_sDateTwo);
		
		SimpleDateFormat format = new SimpleDateFormat(ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT.toString()));
		Date oDateOne = null;
		Date oDateTwo = null;
		long diffDays = 0;
		String sOutput = null;
		try 
		{
			oDateOne = format.parse(p_sDateOne);
			oDateTwo = format.parse(p_sDateTwo);
			long diff = oDateOne.getTime() - oDateTwo.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
			sOutput = NumberToWord.convert((int)diffDays); 
		} 
		catch (ParseException parseExcep) 
		{
			log.error("Error getting difference between days", parseExcep);
		}
		
		return sOutput;
	}
		
}
