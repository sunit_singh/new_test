package com.sciformix.sciportal.apps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserUtils;

public abstract class BasePortalApp implements IPortalApp
{

	private static final Logger log = LoggerFactory.getLogger(BasePortalApp.class);
	
	private String m_sPortalAppId = null;
	private String m_sPortalAppName = null;
	private boolean m_bAllowsUserApplicationDisable = false;
	private boolean m_bApplicationProjectSpecific = false;
	//bProjectspecific
	private List<AppUserPreferenceMetadata> listAppUserPreferences = null;
	
	public BasePortalApp(String p_sPortalAppId, String p_sPortalAppName, boolean p_bApplicationProjectSpecific)
	{
		this(p_sPortalAppId, p_sPortalAppName, true,p_bApplicationProjectSpecific);
	}
	
	public BasePortalApp(String p_sPortalAppId, String p_sPortalAppName, boolean p_bAllowsUserApplicationDisable, boolean p_bApplicationProjectSpecific )
	{
		m_sPortalAppId = p_sPortalAppId;
		m_sPortalAppName = p_sPortalAppName;
		m_bAllowsUserApplicationDisable = p_bAllowsUserApplicationDisable;
		m_bApplicationProjectSpecific = p_bApplicationProjectSpecific;
		
		listAppUserPreferences = new ArrayList<AppUserPreferenceMetadata>();
	}
	
	protected void addAppUserPreference(AppUserPreferenceMetadata p_oAppUserPreference)
	{
		listAppUserPreferences.add(p_oAppUserPreference);
	}
	
	public String getPortalAppId()
	{
		return m_sPortalAppId;
	}

	public String getPortalAppName()
	{
		return m_sPortalAppName;
	}
	
	public boolean allowsUserApplicationDisable()
	{
		return m_bAllowsUserApplicationDisable;
	}
	
	public boolean applicationProjectSpecific()
	{
		return m_bApplicationProjectSpecific;
	}
	
	public boolean requiresUserPreferences()
	{
		return (m_bAllowsUserApplicationDisable || listAppUserPreferences.size() > 0);
	}
	
	public List<AppUserPreferenceMetadata> getAppUserPreference()
	{
		return listAppUserPreferences;
	}
	

	
	public boolean isApplicationEnabledForUser(UserInfo p_oUserInfo , int p_selectedProjSeqId)
	{
		//If Application cannot be disabled by the user, it is 'ENABLED' by default
		if (!m_bAllowsUserApplicationDisable)
		{
			return true;
		}
		try
		{
			return UserUtils.isApplicationEnabledForUser(p_oUserInfo, p_selectedProjSeqId, getPortalAppId());
		}
		catch (SciException sciExcep)
		{
			log.error("Error checking status of application - {}; for user - {}", getPortalAppId(), p_oUserInfo.getUserId(), sciExcep);
			return false;
		}
	}
	
	public Map<String, String> getAppConfigSettings()
	{
		return ConfigHome.getAppConfigSettings(m_sPortalAppId);
	}
	
	@SuppressWarnings("rawtypes")
	public String getAppConfigSetting(Enum p_eKey)
	{
		return ConfigHome.getAppConfigSetting(m_sPortalAppId, p_eKey.toString());
	}
}
