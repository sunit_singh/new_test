package com.sciformix.sciportal.apps.qr;

public class QRConstant {
	public static final String 
		PROP_PROJ_APPLICABLE_CAT					=	"category.aliases"
	;
	public static final String
		PROP_PROJ_MR_NAME_DISPLAY					=	"mr.name.display"
	;
	public static final String
		FTL_CASE_QUALITY_NAME						=	"${case_quality}"
	;
	public static final String
		FTL_CASE_QUALITY_REPLACE					=	"case_quality"
	;	
	public static final String
		FTL_CASE_CAT1_NAME							=	"${cat1_quality}"
	;
	public static final String
		FTL_CASE_CAT1_REPLACE						=	"cat1_quality"
	;
	public static final String
		FTL_CASE_CAT2_NAME							=	"${cat2_quality}"
	;
	public static final String
		FTL_CASE_CAT2_REPLACE						=	"cat2_quality"
	;
	public static final String
		FTL_CASE_CAT3_NAME							=	"${cat3_quality}"
	;	
	public static final String
		FTL_CASE_CAT3_REPLACE						=	"cat3_quality"
	;
	public static final String
		APP_NAME									=	"QR-PORTAL-APP"
	;	
	public static final String
		PROP_PROJ_EMAIL_SEND_CONFIG					=	"email.review.completion.configuration"
	;
	public static final String
		PROP_PROJ_EMAIL_CC_LIST						=	"email.review.completion.cc.list"
	;	
	public static final String
		PROP_PROJ_EMAIL_INITIAL_PURPOSE				=	"email.purpose.initial"
	;
	public static final String
		PROP_PROJ_EMAIL_EDIT_PURPOSE				=	"email.purpose.edit"
	;
	public static final String
		PROP_PROJ_REVIEW_TYPE						=	"review.type.all"
	;	
	public static final String
		PROP_PROJ_START_MONTH_DAY					=	"start.month.day"
	;	
	public static final String
		PROP_PROJ_ERROR_TYPE						=	"review.errortype"
	;	
	public static final String
		PROP_PROJ_FSR_TYPE							=	"report.fsr.type"
	;	
	public static final String
		PROP_PROJ_REVIEW_QRT_MAPPING				=	"review.qrt.mapping"
	;	
	public static final String
		PROP_PROJ_APPLICABLE_REGCLASS				=	"reg.class"
	;
	public static final String
		DOWNLOAD_ALLOWED							=	"review.completion.downloadflag"
	;	
	public static final String
		PROP_PROJ_INPUT_DATE_FORMAT					=	"input.date.format"
	;
	public static final String
		PROP_PROJ_OUTPUT_DATE_FORMAT				=	"output.date.format"
	;
	public static final String
		PROP_PROJ_QR_GRAPH_SERIOUS_THRESHOLD		=	"graph.threshold.seriousness"
	;
	public static final String
		PROP_PROJ_QR_GRAPH_NON_SERIOUS_THRESHOLD	=	"graph.threshold.nonseriousness"
	;
	public static final String
		PROP_PROJ_CASE_REVIEW_DWNLD_SHEET_NUM		=	"casereview.download.sheet.number"
	;	
	public static final String
		PROP_PROJ_CASE_MASTER_UDF					=	"QR-PORTAL-APP.review.udf"
	;
	public static final String
		PROP_PROJ_CASE_MASTER_UDF_DISPLAY_NAME		=	"displayname"
	;	
	public static final String
		PROP_PROJ_CASE_MASTER_UDF_DATATYPE			=	"datatype"
	;
	public static final String
		ENABLE_UDF_MAP_KEY							=	"UDF"
	;
	public static final int
		TOTAL_UDF_IELDS								=	10
	;
	public static final String
		PROP_PROJ_APPLICABLE_FILEFORMAT				=	"cm.file.format"
	;
	public static final String
		CASE_MASTER_INTERNAL_COL_CASE_ID			=	"CASEID"
	;
	public static final String
		CASE_MASTER_INTERNAL_COL_VERSION			=	"VERSION"
	;
	public static final String
		CASE_MASTER_INTERNAL_COL_CASE_TYPE			=	"CASETYPE"
	;
	public static final String
		CASE_MASTER_INTERNAL_COL_PRODUCT			=	"PRODUCT"
	;
	public static final String
		CASE_MASTER_INTERNAL_COL_SERIOUSNESS		=	"CASESERIOUSNESS"
	;
	public static final String
		CASE_MASTER_INTERNAL_IRD					=	"IRD"
	;
	public static final String
		CASE_MASTER_INTERNAL_DENAME					=	"DENAME"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF1					=	"UDF1"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF2					=	"UDF2"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF3					=	"UDF3"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF4					=	"UDF4"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF5					=	"UDF5"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF6					=	"UDF6"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF7					=	"UDF7"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF8					=	"UDF8"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF9					=	"UDF9"
	;
	public static final String
		CASE_MASTER_INTERNAL_UDF10					=	"UDF10"
	;
	/**
	 * This variable is used to hold
	 * the request parameter name of
	 * the udf fields being sent. This
	 * should not be changed. If change
	 * is required then the same must be
	 * change in the corresponding .jsp file.
	 */
	public static final String
		REQUEST_PARAM_UDF_PREFIX					=	"fld"
	;
	public static final String
		OVERALL_THRESHOLD_CONST						=	"Overall Threshold"
	;	
	public static final String
		MAX_VIEW_DAYS								=	"QR-PORTAL-APP.view.max.days"
	;
	public static final String
		MAX_REPORT_DAYS								=	"QR-PORTAL-APP.report.max.days"
	;	
}
