package com.sciformix.sciportal.apps.qr;

public class QRAuditHelper {
	public static class AuditAttributes
	{
		public static final String CASE_ID			=	"Case Id";
		public static final String CASE_VERSION		=	"Case Version";
		public static final String PROJECT_ID		=	"Project Id";
		public static final String CASE_SERIOUSNESS	=	"Seriousness";
		public static final String CASE_ASSOCIATE	=	"Case Associate";
		public static final String CASE_QUALITY		=	"Case Quality";
		public static final String CASE_RESULT		=	"Case Result";
		public static final String CAT1_QUALITY		=	"Cat1 Quality";
		public static final String CAT2_QUALITY		=	"Cat2 Quality";
		public static final String CAT3_QUALITY		=	"Cat3 Quality";
		public static final String FIELD_NAME		=	"Field Name";
		public static final String REG_CLASS		=	"Reg Class";
		public static final String FIELD_MAPPING	=	"Case Field Mapping";
		public static final String RVW_COMPL_NTFN_SETTING	=	"Email Notification Setting";
		public static final String RVW_COMPL_NTFN_CCADDR	=	"CC Address";
		public static final String RVW_ALLOWED_TO_CORRECT	=	"Correction Allowed";
		public static final String CASE_CRITERIA_MODE		=	"Case Criteria";
		public static final String CASE_QUALITY_THRESHOLD	=	" Case Quality Threshold";
		public static final String CAT1_QUALITY_THRESHOLD	=	" Cat1 Quality Threshold";
		public static final String CAT2_QUALITY_THRESHOLD	=	" Cat2 Quality Threshold";
		public static final String CAT3_QUALITY_THRESHOLD	=	" Cat3 Quality Threshold";
		public static final String CASE_CLASSIFICATION_OVERALL	=	"Overall Case Classification";
		public static final String CASE_CLASSIFICATION_CAT1		=	"Cat1 Case Classification";
		public static final String CASE_CLASSIFICATION_CAT2		=	"Cat2 Case Classification";
		public static final String CASE_CLASSIFICATION_CAT3		=	"Cat3 Case Classification";
	}
	private QRAuditHelper ()
	{
		
	}
}