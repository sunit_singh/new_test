package com.sciformix.sciportal.apps.qr;

public final class QRReviewFieldDetails {
	private String
		fieldMnemonic
	;	
	private int 
		fieldStatus
	;
	private String
		fieldErrorCode
	;	
	/*private boolean
		isFieldCorrected
	;*/
	private String
		fieldComment
	;
	public int getFieldStatus() {
		return fieldStatus;
	}
	public void setFieldStatus(int fieldStatus) {
		this.fieldStatus = fieldStatus;
	}
	public String getFieldComment() {
		return fieldComment;
	}
	public void setFieldComment(String fieldComment) {
		this.fieldComment = fieldComment;
	}
	public String getFieldMnemonic() {
		return fieldMnemonic;
	}
	public void setFieldMnemonic(String fieldMnemonic) {
		this.fieldMnemonic = fieldMnemonic;
	}
	public String getFieldErrorCode() {
		return fieldErrorCode;
	}
	public void setFieldErrorCode(String fieldErrorCode) {
		this.fieldErrorCode = fieldErrorCode;
	}
	
}
