package com.sciformix.sciportal.apps.qr;

import java.util.Date;

public class QRDailySummaryReport {
	private Date
		date
	;
	private int
		seriousTotalCaseReviewed
	;
	private int
		seriousTotalCasePassed
	;
	private int
		seriousTotalCaseFailed
	;
	private double
		seriousAverageQuality
	;
	private double
		seriousMinQuality
	;
	private double
		seriousMaxQuality
	;
	private int
		nonSeriousTotalCaseReviewed
	;
	private int
		nonSeriousTotalCasePassed
	;
	private int
		nonSeriousTotalCaseFailed
	;
	private double
		nonSeriousAverageQuality
	;
	private double
		nonSeriousMinQuality
	;
	private double
		nonSeriousMaxQuality
	;
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getSeriousTotalCaseReviewed() {
		return seriousTotalCaseReviewed;
	}
	public void setSeriousTotalCaseReviewed(int seriousTotalCaseReviewed) {
		this.seriousTotalCaseReviewed = seriousTotalCaseReviewed;
	}
	public int getSeriousTotalCasePassed() {
		return seriousTotalCasePassed;
	}
	public void setSeriousTotalCasePassed(int seriousTotalCasePassed) {
		this.seriousTotalCasePassed = seriousTotalCasePassed;
	}
	public int getSeriousTotalCaseFailed() {
		return seriousTotalCaseFailed;
	}
	public void setSeriousTotalCaseFailed(int seriousTotalCaseFailed) {
		this.seriousTotalCaseFailed = seriousTotalCaseFailed;
	}
	public double getSeriousAverageQuality() {
		return seriousAverageQuality;
	}
	public void setSeriousAverageQuality(double seriousAverageQuality) {
		this.seriousAverageQuality = seriousAverageQuality;
	}
	public double getSeriousMinQuality() {
		return seriousMinQuality;
	}
	public void setSeriousMinQuality(double seriousMinQuality) {
		this.seriousMinQuality = seriousMinQuality;
	}
	public double getSeriousMaxQuality() {
		return seriousMaxQuality;
	}
	public void setSeriousMaxQuality(double seriousMaxQuality) {
		this.seriousMaxQuality = seriousMaxQuality;
	}	
	public int getNonSeriousTotalCaseReviewed() {
		return nonSeriousTotalCaseReviewed;
	}
	public void setNonSeriousTotalCaseReviewed(int nonSeriousTotalCaseReviewed) {
		this.nonSeriousTotalCaseReviewed = nonSeriousTotalCaseReviewed;
	}
	public int getNonSeriousTotalCasePassed() {
		return nonSeriousTotalCasePassed;
	}
	public void setNonSeriousTotalCasePassed(int nonSeriousTotalCasePassed) {
		this.nonSeriousTotalCasePassed = nonSeriousTotalCasePassed;
	}
	public int getNonSeriousTotalCaseFailed() {
		return nonSeriousTotalCaseFailed;
	}
	public void setNonSeriousTotalCaseFailed(int nonSeriousTotalCaseFailed) {
		this.nonSeriousTotalCaseFailed = nonSeriousTotalCaseFailed;
	}
	public double getNonSeriousAverageQuality() {
		return nonSeriousAverageQuality;
	}
	public void setNonSeriousAverageQuality(double nonSeriousAverageQuality) {
		this.nonSeriousAverageQuality = nonSeriousAverageQuality;
	}
	public double getNonSeriousMinQuality() {
		return nonSeriousMinQuality;
	}
	public void setNonSeriousMinQuality(double nonSeriousMinQuality) {
		this.nonSeriousMinQuality = nonSeriousMinQuality;
	}
	public double getNonSeriousMaxQuality() {
		return nonSeriousMaxQuality;
	}
	public void setNonSeriousMaxQuality(double nonSeriousMaxQuality) {
		this.nonSeriousMaxQuality = nonSeriousMaxQuality;
	}
	
	
}
