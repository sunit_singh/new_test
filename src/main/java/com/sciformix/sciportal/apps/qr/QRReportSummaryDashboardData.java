package com.sciformix.sciportal.apps.qr;

public class QRReportSummaryDashboardData {
	private int
		totCasesReviewedDay
	;	
	private int
		totCasesReviewedMonth
	;	
	private int
		totCasesReviewedPrevMonth
	;	
	private int
		totCasesPassedDay
	;	
	private int
		totCasesPassedMonth
	;
	private int
		totCasesPassedPrevMonth
	;	
	private int
		totCasesReviewedAvgDay
	;	
	private int
		totCasesReviewedAvgMonthly
	;
	private int
		totCasesReviewedAvgPrevMonth
	;	
	private int
		totCasesPassedAvgDay
	;	
	private int
		totCasesPassedAvgMonth
	;
	private int
		totCasesPassedAvgPrevMonth
	;	
	private double
		totCaseQualityDay
	;
	
	private double
		totCaseQualityMonth
	;
	private double
		totCaseQualityPrevMonth
	;
	public int getTotCasesReviewedAvgDay() {
		return totCasesReviewedAvgDay;
	}
	public void setTotCasesReviewedAvgDay(int totCasesReviewedAvgDay) {
		this.totCasesReviewedAvgDay = totCasesReviewedAvgDay;
	}
	public int getTotCasesReviewedAvgMonthly() {
		return totCasesReviewedAvgMonthly;
	}
	public void setTotCasesReviewedAvgMonthly(int totCasesReviewedAvgMonthly) {
		this.totCasesReviewedAvgMonthly = totCasesReviewedAvgMonthly;
	}
	public int getTotCasesReviewedAvgPrevMonth() {
		return totCasesReviewedAvgPrevMonth;
	}
	public void setTotCasesReviewedAvgPrevMonth(int totCasesReviewedAvgPrevMonth) {
		this.totCasesReviewedAvgPrevMonth = totCasesReviewedAvgPrevMonth;
	}
	public int getTotCasesPassedAvgDay() {
		return totCasesPassedAvgDay;
	}
	public void setTotCasesPassedAvgDay(int totCasesPassedAvgDay) {
		this.totCasesPassedAvgDay = totCasesPassedAvgDay;
	}
	public int getTotCasesPassedAvgMonth() {
		return totCasesPassedAvgMonth;
	}
	public void setTotCasesPassedAvgMonth(int totCasesPassedAvgMonth) {
		this.totCasesPassedAvgMonth = totCasesPassedAvgMonth;
	}
	public int getTotCasesPassedAvgPrevMonth() {
		return totCasesPassedAvgPrevMonth;
	}
	public void setTotCasesPassedAvgPrevMonth(int totCasesPassedAvgPrevMonth) {
		this.totCasesPassedAvgPrevMonth = totCasesPassedAvgPrevMonth;
	}
	public int getTotCasesReviewedDay() {
		return totCasesReviewedDay;
	}
	public void setTotCasesReviewedDay(int totCasesReviewedDay) {
		this.totCasesReviewedDay = totCasesReviewedDay;
	}
	public int getTotCasesReviewedMonth() {
		return totCasesReviewedMonth;
	}
	public void setTotCasesReviewedMonth(int totCasesReviewedMonth) {
		this.totCasesReviewedMonth = totCasesReviewedMonth;
	}
	public int getTotCasesReviewedPrevMonth() {
		return totCasesReviewedPrevMonth;
	}
	public void setTotCasesReviewedPrevMonth(int totCasesReviewedPrevMonth) {
		this.totCasesReviewedPrevMonth = totCasesReviewedPrevMonth;
	}
	public int getTotCasesPassedDay() {
		return totCasesPassedDay;
	}
	public void setTotCasesPassedDay(int totCasesPassedDay) {
		this.totCasesPassedDay = totCasesPassedDay;
	}
	public int getTotCasesPassedMonth() {
		return totCasesPassedMonth;
	}
	public void setTotCasesPassedMonth(int totCasesPassedMonth) {
		this.totCasesPassedMonth = totCasesPassedMonth;
	}
	public int getTotCasesPassedPrevMonth() {
		return totCasesPassedPrevMonth;
	}
	public void setTotCasesPassedPrevMonth(int totCasesPassedPrevMonth) {
		this.totCasesPassedPrevMonth = totCasesPassedPrevMonth;
	}
	public double getTotCaseQualityDay() {
		return totCaseQualityDay;
	}
	public void setTotCaseQualityDay(double totCaseQualityDay) {
		this.totCaseQualityDay = totCaseQualityDay;
	}
	public double getTotCaseQualityMonth() {
		return totCaseQualityMonth;
	}
	public void setTotCaseQualityMonth(double totCaseQualityMonth) {
		this.totCaseQualityMonth = totCaseQualityMonth;
	}
	public double getTotCaseQualityPrevMonth() {
		return totCaseQualityPrevMonth;
	}
	public void setTotCaseQualityPrevMonth(double totCaseQualityPrevMonth) {
		this.totCaseQualityPrevMonth = totCaseQualityPrevMonth;
	}
}
