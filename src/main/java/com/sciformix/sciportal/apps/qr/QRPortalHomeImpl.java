package com.sciformix.sciportal.apps.qr;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.ngv.FreemarkerUtils;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.content.BinaryContent;
import com.sciformix.sciportal.content.BinaryContentHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQueryDelete;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQueryParam;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUpdate;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.mail.MailRuntimeContents;
import com.sciformix.sciportal.mail.MailRuntimeHelper;
import com.sciformix.sciportal.pvcp.PvcpCaseMaster;
import com.sciformix.sciportal.pvcp.PvcpCaseMasterAuditHelper;
import com.sciformix.sciportal.pvcp.PvcpCaseMasterDbSchema;
import com.sciformix.sciportal.pvcp.PvcpCaseMasterDbSchema.CaseMasterDb;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFields;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate;
import com.sciformix.sciportal.safetydb.SafetyDbConfigurationHelper;
import com.sciformix.sciportal.user.UserDbSchema;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;

class QRPortalHomeImpl {
	
	private static final Logger 
		log	= LoggerFactory.getLogger(QRPortalHomeImpl.class)
	;
	private static final int
		MAX_DAYS_DAILY_SUMMARY_REPORT	=	-45
	;
	private static final String
		FSR_REPORT_NAME_CONST			=	"FullSummaryReport.csv"
	;
	private static final String
		CP_REPORT_NAME_CONST			=	"CaseProcessorReport.csv"
	;
	private static final String
		DATE_FORMAT_CONST				=	"dd-MMM-yyyy"
	;	
	private static final String
		IF_CONSTANT						=	"if"
	;	
	private static final String
		CORRECT_NUMERICAL_CONST			=	"1,"
	;
	private static final String
		INCORRECT_NUMERICAL_CONST		=	"0,"
	;
	private static final String
		DAILY_SUMMARY_REPORT	=	" select trunc(dtcreated), "
								+	" sum( case when caseseriousness = 'Serious' then 1 else 0 end ) as Serious_TotalCount, "
								+	" sum( case when caseseriousness = 'Serious' and  caseresult = 'Passed' then 1 else 0 end ) as Serious_PassCount, "
								+	" sum( case when caseseriousness = 'Serious' and caseresult = 'Failed' then 1 else 0 end ) as Serious_FailedCount, "
								+	" min(case when caseseriousness = 'Serious' then casequality else 100 end) as Serious_Min_Case_Quality, "
								+	" max(case when caseseriousness = 'Serious' then casequality else 0 end) as Serious_Max_Case_Quality, "
								+	" sum( case when caseseriousness = 'Non-Serious' then 1 else 0 end ) as NonSerious_TotalCount, "
								+	" sum( case when caseseriousness = 'Non-Serious' and  caseresult = 'Passed' then 1 else 0 end ) as NonSerious_PassCount, "
								+	" sum( case when caseseriousness = 'Non-Serious' and caseresult = 'Failed' then 1 else 0 end ) as NonSerious_FailedCount, "
								+	" min(case when caseseriousness = 'Non-Serious' then casequality else 100 end) as NonSerious_Min_Case_Quality, "
								+	" max(case when caseseriousness = 'Non-Serious' then casequality else 0 end) as NonSerious_Max_Case_Quality "
								+	" from %s where prjseqid = ? and reviewactive = ? " 
								+	" and dtcreated between ? and ? "
								+	" group by trunc(dtcreated) "
								+	" order by trunc(dtcreated) desc"
	;		
	
	private static final String
		DASHBOARD_DATA_MONTHLY	=	" select " 
								+	" sum( case when caseresult = 'Failed' or caseresult = 'Passed' then 1 else 0 end) as TotalCount, "
								+	" sum( case when caseresult = 'Passed' then 1 else 0 end) as TotalPassedCount "
								+	" from %s where prjseqid = ? and reviewactive = ?  and dtcreated between ? and ? "
	;
	
	private static final String
		ERROR_DB_FIELD_COUNT	=	" select fieldmnemonic,count(*) as totalcount "
								+	" from %s where prjseqid = ? and dtcreated between ? and ? "
								+	" group by fieldmnemonic order by totalcount desc "
	;
	
	private static final String
		ERROR_DB_TYPE_COUNT		=	" select errortype,count(*) as totalcount "
								+	" from %s where prjseqid = ? and dtcreated between ? and ? "
								+	" group by errortype "
	;
	private static final String
		ERROR_DB_RT_COUNT		=	" select reviewtype,count(*) as totalcount "
								+	" from %s where prjseqid = ? and dtcreated between ? and ? "
								+	" group by reviewtype "
	;
	private static final String
		CASE_PROCESSOR_REPORT	=	" select USERDISPLAYNAME, "
								+	" sum (case when caseresult = 'Failed' or caseresult = 'Passed' then 1 else 0 end) as TotalReviewedCount,  "
								+	" sum (case when caseresult = 'Passed' then 1 else 0 end) as TotalPassedCount,  "
								+	" sum (case when casequality = 100 then 1 else 0 end) as TotalPassWithoutErrorCount, "
								+	" sum (fieldsreview) as TOTFIELDSREVIEWED, " 
								+	" sum (fieldswerror) as TOTFIELDSWERROR, " 
								+	" sum (fieldserror) as TOTFIELDSERROR, "
								+	" avg (a.CAT1QUALITY) as Cat1Quality, "
								+	" avg (a.CAT2QUALITY) as Cat2Quality, "
								+	" avg (a.CAT3QUALITY) as Cat3Quality, "
								+	" avg (a.casequality) as CaseQuality, "
								+	" sum (CAT1FIELDSREVIEWED) as TOTCAT1FIELDSREVIEWED, " 
								+	" sum (CAT2FIELDSREVIEWED) as TOTCAT2FIELDSREVIEWED, " 
								+	" sum (CAT3FIELDSREVIEWED) as TOTCAT3FIELDSREVIEWED, "
								+	" sum (CAT1FIELDSREVIEWED - CAT1FIELDSWERROR) as TOTCAT1FIELDSERROR, " 
								+	" sum (CAT2FIELDSREVIEWED - CAT2FIELDSWERROR) as TOTCAT2FIELDSERROR, " 
								+	" sum (CAT3FIELDSREVIEWED - CAT3FIELDSWERROR) as TOTCAT3FIELDSERROR "
								+	" from %s a, %s b where CASEASSOC = userseqid and prjseqid = ? and reviewactive = ? " 
								+	" and a.reviewtype = ? "
								+	" and a.dtcreated between ? and ? "
								+	" group by USERDISPLAYNAME "
								+	" order by b.USERDISPLAYNAME "
	;
	private static final String
		GET_CASE_DETAILS_BY_INTERNALID		=	" select * from %s where prjseqid = ? "
											+	" and upper(pcminternalcaseid) = upper(?) "
							
	;
	private static final String
		UPDATE_CASE_DETAILS_BY_INTERNALID	=	" update %s SET PCMDESRC = ?, PCMDETYPE = ?, " 
											+	" PCMDEUSERSEQID = ?, PCMDEUSERNAME = ?, PCMCASE_IRD = ?, "
											+	" PCMCASESERIOUSNESS = ?, PCMCASETYPE = ?, PCMCASE_PRODUCT = ?, "
											+	" PCMUDF1 = ?, PCMUDF2 = ?, PCMUDF3 = ?, PCMUDF4 = ?, PCMUDF5 = ?, "
											+	" PCMUDF6 = ?, PCMUDF7 = ?, PCMUDF8 = ?, PCMUDF9 = ?, PCMUDF10 = ?, "
											+	" PCMRT1USERSEQID = ?, PCMRT2USERSEQID = ?, PCMRT3USERSEQID = ?, "
											+	" PCMRT4USERSEQID = ?, PCMRT5USERSEQID = ? " 
											+	" WHERE prjseqid = ? and upper(PCMINTERNALCASEID) = upper(?) "
	;	
	private static final String
		UPDATE_UDF_DETAILS_BY_INTERNALID	=	" update %s SET PCMUDF1 = ?, PCMUDF2 = ?, "
											+	" PCMUDF3 = ?, PCMUDF4 = ?, PCMUDF5 = ?, PCMUDF6 = ?, "
											+	" PCMUDF7 = ?, PCMUDF8 = ?, PCMUDF9 = ?, PCMUDF10 = ? "
											+	" WHERE prjseqid = ? and upper(PCMINTERNALCASEID) = upper(?) "
	;										
	private static final String
		FSR_CSV_HEADER_1		=	",Initial Received Date,Seriousness,Case Associate,Quality Reviewer,Review Date,Fields Reviewed,"
								+ 	"Fields Without Error,Fields Incorrect,Case Quality(%)"
	;	
	private static final String
		FSR_CSV_HEADER_2		=	",Initial Received Date,Seriousness,Case Associate,Quality Reviewer,Medical Reviewer,Review Date,Fields Reviewed,"
								+ 	"Fields Without Error,Fields Incorrect,Case Quality(%)"
	;
	private static final String
		FSR_CSV_HEADER_3		=	",Initial Received Date,Seriousness,Reg Class,Case Associate,Quality Reviewer,Review Date,Fields Reviewed,"
								+ 	"Fields Without Error,Fields Incorrect,Case Quality(%)"
	;
	private static final String
		CP_CSV_HEADER_NOCAT		=	"CP Name,Review Type,Cases Reviewed,Cases Passed (Without Error),Cases Passed (With Error),Cases Failed,Average Quality(%),Fields Reviewed,"
								+ 	"Fields Correct,Fields Incorrect,Case Quality(%)"
	;
	private static final String
		CP_CSV_HEADER_CAT		=	"CP Name,Review Type,Cases Reviewed,Cases Passed (Without Error),Cases Passed (With Error),Cases Failed,Average Quality(%),Fields Reviewed,"
								+ 	"Fields Correct,Fields Incorrect"
	;
	private static final String
		QUALITY_CONST			=	"Quality(%)"
	;
	private static final String
		CASE_QUALITY_CONST		=	"Case Quality(%)"
	;	
	private static final String
		FIELDS_REVIEWED_CONST	=	"Fields Reviewed"
	;
	private static final String
		FIELDS_ERROR_CONST		=	"Fields Error"
	;
	private static final String
		CASE_RESULT_CONST		=	",Case Result"
	;	
	private static final String
		TAB_NAME_CONST			=	"Tab Name,"
	;	
	private static final String
		FIELD_NAME_CONST		=	"Field Name,"
	;	
	private static final String
		ERROR_DESC_CONST		=	"Error Description"
	;	
	private static final String
		SELECT_MONTHLY_QUALITYREVIEWLOG	= "SELECT trunc(DTCREATED) RECDATE,CASESERIOUSNESS, AVG(CASEQUALITY) Quality"
											+ " FROM %s"
											+" WHERE DTCREATED BETWEEN ? AND ?  AND prjseqid = ? AND REVIEWACTIVE = 1 "
											+" GROUP BY trunc(DTCREATED),CASESERIOUSNESS "
											+" ORDER BY trunc(DTCREATED) "
				
	
	;

	/**
	 * @throws SciException 
	 * 
	 */
	static void saveQRTAttributes (
		DbConnection	p_connection
	,	QRTemplate		p_qrtemplate	
	) throws SciException {
		DbQueryInsert	l_insertquery		=	null;
		try {
			p_connection.startTransaction();
			l_insertquery = DbQueryUtils.createInsertQuery(QRDbSchema.TEMPLATE);
			l_insertquery.addInsertParameter(p_qrtemplate.getSequenceId());
			l_insertquery.addInsertParameter(String.valueOf(p_qrtemplate.getCaseQualityThreshold()));
			l_insertquery.addInsertParameter(String.valueOf(p_qrtemplate.getCat1QualityThreshold()));
			l_insertquery.addInsertParameter(String.valueOf(p_qrtemplate.getCat2QualityThreshold()));
			l_insertquery.addInsertParameter(String.valueOf(p_qrtemplate.getCat3QualityThreshold()));
			l_insertquery.addInsertParameter(p_qrtemplate.getCaseClassificationOverall());
			l_insertquery.addInsertParameter(p_qrtemplate.getCaseClassificationCat1());
			l_insertquery.addInsertParameter(p_qrtemplate.getCaseClassificationCat2());
			l_insertquery.addInsertParameter(p_qrtemplate.getCaseClassificationCat3());
			l_insertquery.addInsertParameter(p_qrtemplate.getRvwAllToCorrect());
			l_insertquery.addInsertParameter(p_qrtemplate.getCaseResultCriteriaMode());
			l_insertquery.addInsertParameter(
				createCaseResultCriteria(
					p_qrtemplate.getCaseQualityThreshold()
				,	p_qrtemplate.getCat1QualityThreshold()
				,	p_qrtemplate.getCat2QualityThreshold()
				,	p_qrtemplate.getCat3QualityThreshold()
				,	p_qrtemplate.getCaseResultCriteriaMode()
				,	p_qrtemplate.getProjectId()
				)
			);
			l_insertquery.addInsertParameter(p_qrtemplate.getDownloadReviewTemplateSeqId());
			p_connection.executeQuery(l_insertquery);
			p_connection.commitTransaction();
			
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving config object data into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error saving config object data into DB"
			,	dbExcep	
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving config object data into DB"
			, 	sciExcep
			);
			throw sciExcep;
		}
	}
	
	/**
	 * This method will be used to create the FTL template on the
	 * fly based on the categories maintained at the project level
	 * and the case criteria mode selected. Case Criteria Mode can be
	 * one of the two: Number(1) or Percent(2). 
	 * Case Result criteria will change if any of the above two parameters
	 * is changed. It should always be created from scratch. 
	 * Please check before modifying this method.
	 * It should be always called after calling the constructor for the
	 * QR Template.
	 * @throws SciException 
	 */
	static String createCaseResultCriteria (
		double 	p_casequalitythreshold
	, 	double 	p_cat1qualitythreshold
	, 	double 	p_cat2qualitythreshold
	, 	double 	p_cat3qualitythreshold
	,	int		p_casecriteriamode
	,	int		p_projectid
	) throws SciException {
		String			l_cataliases	=	null;
		String []		l_categories	=	null;	
		int				l_catcount		=	-1;
		StringBuilder	l_criteria		=	null;
		
		l_cataliases	=	ConfigHome.getPrjConfigSettings(QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_APPLICABLE_CAT, p_projectid);
		if (p_casecriteriamode == 1) {
			l_criteria	=	new StringBuilder();
			if (StringUtils.isNullOrEmpty(l_cataliases)) {
				l_criteria.append(
					"<#assign sTemp = 'true'>"
				+	StringConstants.NEWLINE	
				+	"<#"			
				+	IF_CONSTANT
				+	StringConstants.SPACE
				+	QRConstant.FTL_CASE_QUALITY_REPLACE
				+	StringConstants.SPACE
				+	"gt"
				+	StringConstants.SPACE
				+	p_casequalitythreshold
				+	StringConstants.GREATERTHAN
				+	StringConstants.NEWLINE
				+	"<#assign sTemp = 'false'>"
				+	StringConstants.NEWLINE
				+	"</#if>"
				+	StringConstants.NEWLINE
				+	"${sTemp}"
				);
			} else {
				l_categories	=	l_cataliases.split("~");
				l_catcount		=	l_categories.length;
				if (l_catcount == 1) {
					
				} else if (l_catcount == 2) {
					if (p_cat1qualitythreshold == -1 && p_cat2qualitythreshold == -1
							&& p_casequalitythreshold == -1) {
						throw new SciException("Threshold cannot be null for all the category and overall");
					}
					l_criteria.append(
						"<#assign sTemp = 'true'>"
					+	StringConstants.NEWLINE	
					+	"<#"			
					+	IF_CONSTANT
					+	StringConstants.SPACE
					);	
					if (p_cat1qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT1_REPLACE
						+	StringConstants.SPACE
						+	"gt"
						+	StringConstants.SPACE
						+	p_cat1qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_cat2qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT2_REPLACE
						+	StringConstants.SPACE
						+	"gt"
						+	StringConstants.SPACE
						+	p_cat2qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_casequalitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_QUALITY_REPLACE
						+	StringConstants.SPACE
						+	"gt"
						+	StringConstants.SPACE
						+	p_casequalitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					l_criteria.delete(l_criteria.length()-2, l_criteria.length());
					l_criteria.append(	
						StringConstants.GREATERTHAN
					+	StringConstants.NEWLINE
					+	"<#assign sTemp = 'false'>"
					+	StringConstants.NEWLINE
					+	"</#if>"
					+	StringConstants.NEWLINE
					+	"${sTemp}"
					);
				} else if (l_catcount == 3) {
					if (p_cat1qualitythreshold == -1 && p_cat2qualitythreshold == -1 &&
							p_cat3qualitythreshold == -1 && p_casequalitythreshold == -1) {
						throw new SciException("Threshold cannot be null for all the category and overall");
					}
					l_criteria.append(
						"<#assign sTemp = 'true'>"
					+	StringConstants.NEWLINE	
					+	"<#"			
					+	IF_CONSTANT
					+	StringConstants.SPACE
					);	
					if (p_cat1qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT1_REPLACE
						+	StringConstants.SPACE
						+	"gt"
						+	StringConstants.SPACE
						+	p_cat1qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_cat2qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT2_REPLACE
						+	StringConstants.SPACE
						+	"gt"
						+	StringConstants.SPACE
						+	p_cat2qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_cat3qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT3_REPLACE
						+	StringConstants.SPACE
						+	"gt"
						+	StringConstants.SPACE
						+	p_cat3qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_casequalitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_QUALITY_REPLACE
						+	StringConstants.SPACE
						+	"gt"
						+	StringConstants.SPACE
						+	p_casequalitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					l_criteria.delete(l_criteria.length()-2, l_criteria.length());
					l_criteria.append(	
						StringConstants.GREATERTHAN
					+	StringConstants.NEWLINE
					+	"<#assign sTemp = 'false'>"
					+	StringConstants.NEWLINE
					+	"</#if>"
					+	StringConstants.NEWLINE
					+	"${sTemp}"
					);
				}
			}
		} else if (p_casecriteriamode == 2) {
			l_criteria	=	new StringBuilder();
			if (StringUtils.isNullOrEmpty(l_cataliases)) {
				l_criteria.append(
					"<#assign sTemp = 'true'>"
				+	StringConstants.NEWLINE	
				+	"<#"			
				+	IF_CONSTANT
				+	StringConstants.SPACE
				+	QRConstant.FTL_CASE_QUALITY_REPLACE
				+	StringConstants.SPACE
				+	"lt"
				+	StringConstants.SPACE
				+	p_casequalitythreshold
				+	StringConstants.GREATERTHAN
				+	StringConstants.NEWLINE
				+	"<#assign sTemp = 'false'>"
				+	StringConstants.NEWLINE
				+	"</#if>"
				+	StringConstants.NEWLINE
				+	"${sTemp}"
				);
			} else {
				l_categories	=	l_cataliases.split("~");
				l_catcount		=	l_categories.length;
				if (l_catcount == 1) {
					
				} else if (l_catcount == 2) {
					if (p_cat1qualitythreshold == -1 && p_cat2qualitythreshold == -1
							&& p_casequalitythreshold == -1) {
						throw new SciException("Threshold cannot be null for all the category and overall");
					}
					l_criteria.append(
						"<#assign sTemp = 'true'>"
					+	StringConstants.NEWLINE	
					+	"<#"			
					+	IF_CONSTANT
					+	StringConstants.SPACE
					);	
					if (p_cat1qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT1_REPLACE
						+	StringConstants.SPACE
						+	"lt"
						+	StringConstants.SPACE
						+	p_cat1qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_cat2qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT2_REPLACE
						+	StringConstants.SPACE
						+	"lt"
						+	StringConstants.SPACE
						+	p_cat2qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_casequalitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_QUALITY_REPLACE
						+	StringConstants.SPACE
						+	"lt"
						+	StringConstants.SPACE
						+	p_casequalitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					l_criteria.delete(l_criteria.length()-2, l_criteria.length());
					l_criteria.append(	
						StringConstants.GREATERTHAN
					+	StringConstants.NEWLINE
					+	"<#assign sTemp = 'false'>"
					+	StringConstants.NEWLINE
					+	"</#if>"
					+	StringConstants.NEWLINE
					+	"${sTemp}"
					);
				} else if (l_catcount == 3) {
					if (p_cat1qualitythreshold == -1 && p_cat2qualitythreshold == -1 &&
							p_cat3qualitythreshold == -1 && p_casequalitythreshold == -1) {
						throw new SciException("Threshold cannot be null for all the category and overall");
					}
					l_criteria.append(
						"<#assign sTemp = 'true'>"
					+	StringConstants.NEWLINE	
					+	"<#"			
					+	IF_CONSTANT
					+	StringConstants.SPACE
					);	
					if (p_cat1qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT1_REPLACE
						+	StringConstants.SPACE
						+	"lt"
						+	StringConstants.SPACE
						+	p_cat1qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_cat2qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT2_REPLACE
						+	StringConstants.SPACE
						+	"lt"
						+	StringConstants.SPACE
						+	p_cat2qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_cat3qualitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_CAT3_REPLACE
						+	StringConstants.SPACE
						+	"lt"
						+	StringConstants.SPACE
						+	p_cat3qualitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					if (p_casequalitythreshold != -1) {
						l_criteria.append(
							QRConstant.FTL_CASE_QUALITY_REPLACE
						+	StringConstants.SPACE
						+	"lt"
						+	StringConstants.SPACE
						+	p_casequalitythreshold
						+	StringConstants.SPACE
						+	StringConstants.PIPE
						+	StringConstants.PIPE
						);
					}
					l_criteria.delete(l_criteria.length()-2, l_criteria.length());
					l_criteria.append(	
						StringConstants.GREATERTHAN
					+	StringConstants.NEWLINE
					+	"<#assign sTemp = 'false'>"
					+	StringConstants.NEWLINE
					+	"</#if>"
					+	StringConstants.NEWLINE
					+	"${sTemp}"
					);
				}
			}
		}
		return l_criteria.toString();
	}
	/**
	 * 
	 * @param p_projectid
	 * @return
	 * @throws SciServiceException
	 */
	static Map<String, ArrayList<SafetyDbCaseFields>> getReviewFieldList(
		QRTemplate	p_qrtemplate,
		SafetyDbCaseFieldsTemplate	p_safetydbcasefieldtemplate
	) throws SciServiceException 
	{
		Map<String, ArrayList<SafetyDbCaseFields>>	l_mapqualityreviewtabdata	=	null;
		SafetyDbCaseFieldsTemplate					l_safetydbcasefieldtemplate	=	null;
		ArrayList<SafetyDbCaseFields>				l_safetybdcasefieldslist	=	null;
		
		if (p_safetydbcasefieldtemplate == null) {
			l_safetydbcasefieldtemplate	=	SafetyDbConfigurationHelper.getTemplateDetails(p_qrtemplate.getSCFTSeqId());
		} else {
			l_safetydbcasefieldtemplate	=	p_safetydbcasefieldtemplate;
		}
		l_mapqualityreviewtabdata	=	new LinkedHashMap<>();
		for (SafetyDbCaseFields l_safetydbcasefields : l_safetydbcasefieldtemplate.getCaseFieldList()) {
			if (p_qrtemplate.getTemplateCaseFields().indexOf(l_safetydbcasefields.getCaseFieldMnemonic()) != -1) {
				if(l_mapqualityreviewtabdata.containsKey(l_safetydbcasefields.getCaseFieldTab()))
				{
					l_safetybdcasefieldslist = l_mapqualityreviewtabdata.get(l_safetydbcasefields.getCaseFieldTab());
					l_safetybdcasefieldslist.add(l_safetydbcasefields);
					l_mapqualityreviewtabdata.put(l_safetydbcasefields.getCaseFieldTab(), l_safetybdcasefieldslist);
				}else
				{
					l_safetybdcasefieldslist	=	new ArrayList<>();
					l_safetybdcasefieldslist.add(l_safetydbcasefields);
					l_mapqualityreviewtabdata.put(l_safetydbcasefields.getCaseFieldTab(), l_safetybdcasefieldslist);
				}
			}
		}
		return l_mapqualityreviewtabdata;	
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_usersession
	 * @param p_content
	 * @return
	 * @throws SciException
	 */
	static BinaryContent saveReviewTypeCaseFieldMapping (
		DbConnection	p_connection
	,	UserSession		p_usersession
	,	String			p_content
	) throws SciException {
		BinaryContent	l_bincontent	=	null;
		try {
			p_connection.startTransaction();
			l_bincontent	=	BinaryContent.createNew("text", "", "", p_content);
			BinaryContentHomeImpl.saveContent(p_connection, p_usersession.getUserInfo(), l_bincontent);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error reading Template Rulesets Db", dbExcep);
			throw new SciException("Error reading Template Rulesets from Db", dbExcep);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error reading Template Rulesets Db", sciExcep);
			throw sciExcep;
		}
		return l_bincontent;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_usersession
	 * @param p_content
	 * @throws SciException
	 */
	static void updateReviewTypeMapping (
		DbConnection	p_connection
	,	UserSession		p_usersession
	,	String			p_content
	) throws SciException {
		BinaryContent	l_bincontent	=	null;
		try {
			p_connection.startTransaction();
			l_bincontent	=	BinaryContent.createNew("text", "", "", p_content);
			BinaryContentHomeImpl.saveContent(p_connection, p_usersession.getUserInfo(), l_bincontent);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error reading Template Rulesets Db", dbExcep);
			throw new SciException("Error reading Template Rulesets from Db", dbExcep);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error reading Template Rulesets Db", sciExcep);
			throw sciExcep;
		}
		
	}
	
	static QRTemplate getActiveQRTDetails (
		int	p_projectid
	) throws SciException {
		QRTemplate		l_qrtemplate		=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_qrtemplate	=	(QRTemplate)ConfigTplObjHomeImpl.getActiveTemplateDetails(
										connection
									, 	ConfigTplObjType.QRT
									,	p_projectid
									);
				if (l_qrtemplate != null) {
					getQRTAttributeAndFields(connection, l_qrtemplate);
				}
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error in fetching template details"
				,	sciExcep
				);
				throw new SciException(
					"Error in fetching template details"
				,	sciExcep	
				);
				
			} 	
		} catch (SciDbException dbExcep) {
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciException(
				"Error in fetching template details"
			,	dbExcep	
			);
		}	
		return l_qrtemplate;
	}
	
	static QRTemplate getQRTDetails (
		int p_templateseqid
	) throws SciException {
		QRTemplate		l_qrtemplate		=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			l_qrtemplate = getQRTDetails(connection, p_templateseqid);
		} catch (SciDbException dbExcep) {
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciException(
				"Error in fetching template details"
			,	dbExcep	
			);
			
		}	
		return l_qrtemplate;
	}	
	
	static QRTemplate getQRTDetails (
			DbConnection connection,
			int p_templateseqid
		) throws SciDbException, SciException {
			QRTemplate		l_qrtemplate		=	null;
			
			try {
				connection.startTransaction();
				l_qrtemplate	=	(QRTemplate) ConfigTplObjHomeImpl.getTemplateDetails(
													connection
												, 	p_templateseqid
												);
				if (l_qrtemplate != null) {
					getQRTAttributeAndFields(connection, l_qrtemplate);
				}
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error in fetching template details"
				,	sciExcep
				);
				throw new SciException(
					"Error in fetching template details"
				,	sciExcep	
				);
			} 	
			return l_qrtemplate;
		}	
	
	/**
	 * 
	 * @param p_connection
	 * @param p_qrtemplate
	 * @throws SciException
	 * @throws SciDbException
	 */
	static void getQRTAttributeAndFields (
		DbConnection	p_connection
	,	QRTemplate		p_qrtemplate
	) throws SciException {
		int				l_contentid			=	0;
		List<String>	l_casefieldmapping	=	null;
		String			l_content			=	null;
		DbQuerySelect	l_selectquery		=	null;
		
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
							QRDbSchema.TEMPLATE
						, 	QRDbSchema.TEMPLATE.CTOSEQID.bindClause()
						);
		l_selectquery.addWhereClauseParameter(p_qrtemplate.getSequenceId());
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				while (l_rs.next()) {
					readQRTAttributeDetails(
						l_rs
					,	p_qrtemplate
					);
				}
			}
			
			l_contentid	=	p_qrtemplate.getTemplateCaseFieldMapping();
			if (l_contentid	==	-1) { 
			} else {
				l_content	=	 BinaryContentHomeImpl.readContent(p_connection, l_contentid).getContent();
				if (!StringUtils.isNullOrEmpty(l_content)) {
					l_casefieldmapping	=	new ArrayList<>();
					for (String l_str : l_content.split("~")) {
						l_casefieldmapping.add(l_str);
					}
				}
				p_qrtemplate.setTemplateCaseFields(l_casefieldmapping);
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciException(
				"Error in fetching template details"
			,	dbExcep	
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error in fetching template details"
			,	sciExcep
			);
			throw sciExcep;
		}
	}
	
	/**
	 * This will update whether the review is active or
	 * not.
	 * @param p_connection
	 * @param p_reviewdata
	 * @throws SciException
	 */
	 static void updateQualityReviewLog (
		DbConnection	p_connection
	, 	QRReviewData 	p_reviewdata	
	,	String			p_internalcaseid
	) throws SciException {
		DbQueryUpdate l_updatequery = 	null;
		
		l_updatequery	=	DbQueryUtils.createUpdateQuery(
								QRDbSchema.QUALITY_REVIEW_LOG
							,	DbQueryUtils.bindUpdateClause(
									QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE
								)
							,	QRDbSchema.QUALITY_REVIEW_LOG.RLSEQID.bindClause()
							);
		l_updatequery.addUpdateParameter(0);
		l_updatequery.addWhereClauseParameter(p_reviewdata.getSequenceId());
		try {
			p_connection.startTransaction();
			p_connection.executeQuery(l_updatequery);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error updating state of template into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error updating state of template into DB"
			,	dbExcep	
			);
		}
	}
	/**
	 * 
	 * @param p_connection
	 * @param p_oUserInfo
	 * @param p_reviewdata
	 * @param p_cmseqid
	 * @param p_internalcaseid
	 * @throws SciException
	 * @throws SciServiceException
	 */
	 static void saveQualityReviewLog (
		DbConnection	p_connection
	, 	UserSession 	p_usersession
	, 	QRReviewData 	p_reviewdata
	,	int				p_cmseqid
	,	String			p_internalcaseid
	,	PvcpCaseMaster	p_pvcpcasemaster
	) throws SciException, SciServiceException {
		
		DbQueryInsert						l_insertquery		=	null;
		BinaryContent						l_content			=	null,
											l_casesummcontent	=	null;
        Map<String, Object> 				l_markervalues		=	null;
        List<Object> 						listReturnValue 	=	null;
        BigDecimal							l_cat1quality		=	null,
        									l_cat2quality		=	null,
        									l_cat3quality		=	null,
        									l_casequality		=	null;
        List<ObjectIdPair<String, String>>	l_reviewtypelist	=	null;
        int									l_reviewcount		=	1,
        									l_reviewerid		=	-1;

		try 
		{	
			p_connection.startTransaction();
			l_content			=	BinaryContent.createNew("qrlreviewfieldsdetail", "", "", p_reviewdata.getFieldDetailsJson());
			l_casesummcontent	=	BinaryContent.createNew("casesummdetail", "", "", p_reviewdata.getCaseReviewSummary());
			BinaryContentHomeImpl.saveContent(p_connection, p_usersession.getUserInfo(), l_content);
			BinaryContentHomeImpl.saveContent(p_connection, p_usersession.getUserInfo(), l_casesummcontent);
			l_insertquery = DbQueryUtils.createInsertQuery(QRDbSchema.QUALITY_REVIEW_LOG);
			int	l_sequenceid	=	DbQueryHome.generateSequenceValueForTablePk(QRDbSchema.QUALITY_REVIEW_LOG);
			
			l_cat1quality	=	BigDecimal.valueOf(p_reviewdata.getCaseCat1Quality()).setScale(2, BigDecimal.ROUND_HALF_UP);
			l_cat2quality	=	BigDecimal.valueOf(p_reviewdata.getCaseCat2Quality()).setScale(2, BigDecimal.ROUND_HALF_UP);
			l_cat3quality	=	BigDecimal.valueOf(p_reviewdata.getCaseCat3Quality()).setScale(2, BigDecimal.ROUND_HALF_UP);
			l_casequality	=	BigDecimal.valueOf(p_reviewdata.getCaseQuality()).setScale(2, BigDecimal.ROUND_HALF_UP);
			l_reviewerid	=	p_usersession.getUserInfo().getUserSeqId();
			
			l_insertquery.addInsertParameter(l_sequenceid);
			l_insertquery.addInsertParameter(p_reviewdata.getQrTemplate().getProjectId());
			l_insertquery.addInsertParameter(((QRTemplate)p_reviewdata.getQrTemplate()).getSCFTSeqId());
			l_insertquery.addInsertParameter(p_reviewdata.getQrTemplate().getSequenceId());
			l_insertquery.addInsertParameter(p_reviewdata.getReviewType());
			l_insertquery.addInsertParameter(p_reviewdata.getReviewRound());
			l_insertquery.addInsertParameter(p_reviewdata.getReviewVersion());
			l_insertquery.addInsertParameter(p_reviewdata.getReviewActive());
			l_insertquery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
			l_insertquery.addInsertParameter(l_reviewerid);
			l_insertquery.addInsertParameter(p_reviewdata.getCaseId());
			if (StringUtils.isNullOrEmpty(p_reviewdata.getCaseVersion())) {
				l_insertquery.addInsertParameter("");
			} else {
				l_insertquery.addInsertParameter(p_reviewdata.getCaseVersion());
			}
			l_insertquery.addInsertParameter(p_reviewdata.getCaseRecvDate());
			l_insertquery.addInsertParameter(p_reviewdata.getCaseSeriousness());
			l_insertquery.addInsertParameter(p_reviewdata.getCaseAssociate());
			l_insertquery.addInsertParameter(l_content.getContentSeqId());
			l_insertquery.addInsertParameter(p_reviewdata.getFieldsReviewed());
			l_insertquery.addInsertParameter(p_reviewdata.getFieldsWithoutError());
			l_insertquery.addInsertParameter(p_reviewdata.getFieldsError());
			l_insertquery.addInsertParameter(l_casequality.toString());
			
			l_markervalues	=	new HashMap<>();
			if (p_reviewdata.getQrTemplate().getCaseResultCriteriaMode() == 1) {
				l_markervalues.put(QRConstant.FTL_CASE_CAT1_REPLACE, p_reviewdata.getCat1FieldsReviewed()-p_reviewdata.getCat1FieldsWithoutError());
				l_markervalues.put(QRConstant.FTL_CASE_CAT2_REPLACE, p_reviewdata.getCat2FieldsReviewed()-p_reviewdata.getCat2FieldsWithoutError());
				l_markervalues.put(QRConstant.FTL_CASE_CAT3_REPLACE, p_reviewdata.getCat3FieldsReviewed()-p_reviewdata.getCat3FieldsWithoutError());
				l_markervalues.put(QRConstant.FTL_CASE_QUALITY_REPLACE, p_reviewdata.getFieldsError());
			} else {
				l_markervalues.put(QRConstant.FTL_CASE_CAT1_REPLACE, l_cat1quality);
				l_markervalues.put(QRConstant.FTL_CASE_CAT2_REPLACE, l_cat2quality);
				l_markervalues.put(QRConstant.FTL_CASE_CAT3_REPLACE, l_cat3quality);
				l_markervalues.put(QRConstant.FTL_CASE_QUALITY_REPLACE, l_casequality);
			}
			
			listReturnValue	=	FreemarkerUtils.replaceFreemarkerTemplate(p_reviewdata.getQrTemplate().getCaseResultCriteria(), l_markervalues);
			
			if (((String)listReturnValue.get(0)).equals("false")) {
				p_reviewdata.setCaseResult("Failed");
			} else {
				p_reviewdata.setCaseResult("Passed");
			}
			l_insertquery.addInsertParameter(p_reviewdata.getCaseResult());
			l_insertquery.addInsertParameter(l_cat1quality.toString());
			l_insertquery.addInsertParameter(l_cat2quality.toString());
			l_insertquery.addInsertParameter(l_cat3quality.toString());
			l_insertquery.addInsertParameter(p_reviewdata.getMedicalReviewerId());
			l_insertquery.addInsertParameter(p_reviewdata.getCat1FieldsReviewed());
			l_insertquery.addInsertParameter(p_reviewdata.getCat2FieldsReviewed());
			l_insertquery.addInsertParameter(p_reviewdata.getCat3FieldsReviewed());
			l_insertquery.addInsertParameter(p_reviewdata.getCat1FieldsWithoutError());
			l_insertquery.addInsertParameter(p_reviewdata.getCat2FieldsWithoutError());
			l_insertquery.addInsertParameter(p_reviewdata.getCat3FieldsWithoutError());
			l_insertquery.addInsertParameter(l_casesummcontent.getContentSeqId());
			l_insertquery.addInsertParameter(p_reviewdata.getRegClass());
			l_insertquery.addInsertParameter(p_reviewdata.getCdbrflag());
			p_connection.executeQuery(l_insertquery);
			p_reviewdata.setSequenceId(l_sequenceid);
			readQRLReviewFieldDetails(p_reviewdata,l_content);
			l_reviewtypelist	=	getReviewTypeProjectList(
										p_reviewdata.getQrTemplate().getProjectId()
									);
			for (ObjectIdPair<String, String> l_reviewtype : l_reviewtypelist) {
				if (l_reviewtype.getObjectSeqId().equals(p_reviewdata.getReviewType())) {
					break;
				}
				l_reviewcount++;
			}
			switch(l_reviewcount) {
				case 1 :
					p_pvcpcasemaster.setReviewType1UserSeqId(l_reviewerid);
				break;
				case 2 :
					p_pvcpcasemaster.setReviewType2UserSeqId(l_reviewerid);
				break;
				case 3 :
					p_pvcpcasemaster.setReviewType3UserSeqId(l_reviewerid);
				break;
				case 4 :
					p_pvcpcasemaster.setReviewType4UserSeqId(l_reviewerid);
				break;
				case 5 :
					p_pvcpcasemaster.setReviewType5UserSeqId(l_reviewerid);
				break;
			}
			updateCaseMasterRecord(p_connection, p_pvcpcasemaster, p_usersession, p_reviewdata.getQrTemplate().getProjectId());
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving review data of qr into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error saving review data of qr into DB"
			,	dbExcep	
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving review data of qr into DB"
			, 	sciExcep
			);
			throw sciExcep;
		}
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_projectid
	 * @return
	 * @throws SciException
	 */
	static QRReportSummaryDashboardData getReportSummaryDashboardData (
		DbConnection	p_connection
	,	int				p_projectid	
	) throws SciException {
		DbQuerySelect					l_selectquery	=	null;
		QRReportSummaryDashboardData	l_dashboardata	=	null;
		Calendar						l_startdatetime	=	null,
										l_enddatetime	=	null;
		String 							l_completequery = 	null;
		try {
			p_connection.startTransaction();
			l_startdatetime	=	Calendar.getInstance();
			l_enddatetime	=	Calendar.getInstance();
			
			l_startdatetime.set(Calendar.HOUR_OF_DAY, 0);
			l_startdatetime.set(Calendar.MINUTE, 0);
			l_startdatetime.set(Calendar.SECOND, 0);
			l_startdatetime.set(Calendar.MILLISECOND, 0);
			
			l_enddatetime.set(Calendar.HOUR_OF_DAY, 23);
			l_enddatetime.set(Calendar.MINUTE, 59);
			l_enddatetime.set(Calendar.SECOND, 59);
			l_enddatetime.set(Calendar.MILLISECOND, 999);
			
			l_completequery	=	String.format(DASHBOARD_DATA_MONTHLY, DbQueryUtils.getQualifiedObjectName(QRDbSchema.QUALITY_REVIEW_LOG.name()));
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(1);
			l_selectquery.addWhereClauseParameter(l_startdatetime.getTime());
			l_selectquery.addWhereClauseParameter(l_enddatetime.getTime());
			l_dashboardata	=	new QRReportSummaryDashboardData();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					while (l_rs.next()) {
						l_dashboardata.setTotCasesReviewedDay(l_rs.readInt(1));
						l_dashboardata.setTotCasesPassedDay(l_rs.readInt(2));
						l_dashboardata.setTotCaseQualityDay(
							Math.round(
								(l_dashboardata.getTotCasesPassedDay()*100.00)/l_dashboardata.getTotCasesReviewedDay()
							)
						);	
					}
					
				}
			}
			l_selectquery	=	null;
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			l_startdatetime.set(Calendar.DAY_OF_MONTH,1);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(l_startdatetime.getTime());
			l_selectquery.addWhereClauseParameter(l_enddatetime.getTime());
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					while (l_rs.next()) {
						l_dashboardata.setTotCasesReviewedMonth(l_rs.readInt(1));
						l_dashboardata.setTotCasesPassedMonth(l_rs.readInt(2));
						l_dashboardata.setTotCaseQualityMonth(
							Math.round(
								(l_dashboardata.getTotCasesPassedMonth()*100.00)/l_dashboardata.getTotCasesReviewedMonth()
							)
						);
					}
					
				}
			}
			l_selectquery	=	null;
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			l_startdatetime.add(Calendar.MONTH, -1);
			l_startdatetime.set(Calendar.DAY_OF_MONTH,1);
			l_enddatetime.add(Calendar.MONTH, -1);
			l_enddatetime.set(Calendar.DAY_OF_MONTH,l_enddatetime.getActualMaximum(Calendar.DAY_OF_MONTH));
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(l_startdatetime.getTime());
			l_selectquery.addWhereClauseParameter(l_enddatetime.getTime());
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					while (l_rs.next()) {
						l_dashboardata.setTotCasesReviewedPrevMonth(l_rs.readInt(1));
						l_dashboardata.setTotCasesPassedPrevMonth(l_rs.readInt(2));
						l_dashboardata.setTotCaseQualityPrevMonth(
							Math.round(
								(l_dashboardata.getTotCasesPassedPrevMonth()*100.00)/l_dashboardata.getTotCasesReviewedPrevMonth()
							)
						);	
					}
					
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		}	
		return l_dashboardata;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_projectid
	 * @return
	 * @throws SciException
	 */
	static List<QRDailySummaryReport> getDailySummaryReport (
		DbConnection	p_connection
	,	int				p_projectid	
	) throws SciException {
		DbQuerySelect				l_selectquery				=	null;
		List<QRDailySummaryReport>	l_qrdailysummaryreportlist	=	null;
		QRDailySummaryReport		l_qrdailysummaryreport		=	null;
		Calendar					l_startdatetime				=	null,
									l_enddatetime				=	null;
		String 						l_completequery 			= 	null;
		try {
			p_connection.startTransaction();
			l_startdatetime	=	Calendar.getInstance();
			l_enddatetime	=	Calendar.getInstance();
			
			l_startdatetime.set(Calendar.HOUR_OF_DAY, 0);
			l_startdatetime.set(Calendar.MINUTE, 0);
			l_startdatetime.set(Calendar.SECOND, 0);
			l_startdatetime.set(Calendar.MILLISECOND, 0);
			l_startdatetime.add(Calendar.DAY_OF_MONTH, MAX_DAYS_DAILY_SUMMARY_REPORT);
			
			l_enddatetime.set(Calendar.HOUR_OF_DAY, 23);
			l_enddatetime.set(Calendar.MINUTE, 59);
			l_enddatetime.set(Calendar.SECOND, 59);
			l_enddatetime.set(Calendar.MILLISECOND, 999);
			
			l_completequery	=	String.format(DAILY_SUMMARY_REPORT, DbQueryUtils.getQualifiedObjectName(QRDbSchema.QUALITY_REVIEW_LOG.name()));
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(1);
			l_selectquery.addWhereClauseParameter(l_startdatetime.getTime());
			l_selectquery.addWhereClauseParameter(l_enddatetime.getTime());
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_qrdailysummaryreportlist	=	new ArrayList<>();
					while (l_rs.next()) {
						l_qrdailysummaryreport	=	readDailySummaryReport(l_rs);
						l_qrdailysummaryreportlist.add(l_qrdailysummaryreport);
					}
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		}
		return l_qrdailysummaryreportlist;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_projectid
	 * @param p_startdate
	 * @param p_enddate
	 * @param p_usersession
	 * @return
	 * @throws SciException
	 */
	static File exportCaseProcessorReport (
		DbConnection	p_connection
	,	int				p_projectid
	,	int				p_startdate
	,	int				p_enddate
	, 	UserSession 	p_usersession
	) throws SciException {
		File									l_exportfile			=	null;
		FileWriter								l_fwr					=	null;
		Map<String,List<QRCPDashboardReport>>	l_cpreport				=	null;
		List<ObjectIdPair<String, String>> 		l_reviewtypes			=	null;
		String									l_cataliases			=	null,
												l_catqualityalias		=	null,
												l_catfieldsreviwedalias	=	null,
												l_catfieldserroralias	=	null;
		int										l_catcount				=	0;
		StringBuilder							l_fullcontent			=	null;
		
		try {
			p_connection.startTransaction();
			l_cpreport	=	getCaseProcessorReport(
								p_connection
							,	p_projectid
							,	p_startdate
							,	p_enddate
							);
			try {
				if (l_cpreport.size() > 0) {
					l_exportfile	=	new File(
											WebUtils.createStagingFile(
												p_usersession
											, 	CP_REPORT_NAME_CONST
											)
										);
					l_fwr 			=	new FileWriter(l_exportfile);
					l_reviewtypes	=	getReviewTypeProjectList(p_projectid);
					l_cataliases	=	getApplicableCategories(p_projectid);
					if (!StringUtils.isNullOrEmpty(l_cataliases)) {
						l_catcount				=	l_cataliases.split(StringConstants.TILDE).length;
						l_catqualityalias		=	l_cataliases.replaceAll(StringConstants.TILDE, StringConstants.SPACE + QUALITY_CONST + StringConstants.COMMA);
						l_catqualityalias		=	l_catqualityalias + StringConstants.SPACE + QUALITY_CONST;
						l_catfieldsreviwedalias	=	l_cataliases.replaceAll(StringConstants.TILDE, StringConstants.SPACE + FIELDS_REVIEWED_CONST + StringConstants.COMMA);
						l_catfieldsreviwedalias	=	l_catfieldsreviwedalias + StringConstants.SPACE + FIELDS_REVIEWED_CONST;
						l_catfieldserroralias	=	l_cataliases.replaceAll(StringConstants.TILDE, StringConstants.SPACE + FIELDS_ERROR_CONST + StringConstants.COMMA);
						l_catfieldserroralias	=	l_catfieldserroralias + StringConstants.SPACE + FIELDS_ERROR_CONST;
					}
					l_fullcontent	=	new StringBuilder();
					
					if (l_catcount > 1) {
						l_fullcontent.append(CP_CSV_HEADER_CAT);
						l_fullcontent.append(StringConstants.COMMA);
						l_fullcontent.append(l_catqualityalias);
						l_fullcontent.append(StringConstants.COMMA);
						l_fullcontent.append(CASE_QUALITY_CONST);
						l_fullcontent.append(StringConstants.COMMA);
						l_fullcontent.append(l_catfieldsreviwedalias);
						l_fullcontent.append(StringConstants.COMMA);
						l_fullcontent.append(l_catfieldserroralias);
					} else {
						l_fullcontent.append(CP_CSV_HEADER_NOCAT);
					}
					l_exportfile.createNewFile();
					l_fullcontent.append(System.lineSeparator());
					for (ObjectIdPair<String, String> l_reviewdetails : l_reviewtypes) {
						for (QRCPDashboardReport l_cpdata : l_cpreport.get(l_reviewdetails.getObjectSeqId())) {
							appendContent(
								l_fullcontent
							,	l_cpdata.getUserDisplayName()
							,	l_reviewdetails.getObjectName()
							,	String.valueOf(l_cpdata.getTotalCaseReviewed())
							,	String.valueOf(l_cpdata.getTotalCaseWithoutError())
							,	String.valueOf(l_cpdata.getTotalCasePassed() - l_cpdata.getTotalCaseWithoutError())
							,	String.valueOf(l_cpdata.getTotalCaseReviewed() - l_cpdata.getTotalCasePassed())
							,	String.valueOf(BigDecimal.valueOf(l_cpdata.getTotalAvgQuality()).setScale(2, BigDecimal.ROUND_HALF_UP))
							,	String.valueOf(l_cpdata.getTotalFieldsReviewed())
							,	String.valueOf(l_cpdata.getTotalFieldsPassed())
							,	String.valueOf(l_cpdata.getTotalFieldsFailed())
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContentCatWiseCP(
								l_fullcontent
							,	l_cpdata
							,	l_catcount
							);
							l_fullcontent.append(System.lineSeparator());
						}
					}
					l_fwr.write(l_fullcontent.toString());
					l_fwr.close();
				}
			} catch(Exception excep) {
				log.error("Error exporting the cp dashboard data", excep);
				throw new SciException(
					"Error exporting the cp dashboard data"
				,	excep
				);
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error exporting the cp dashboard data"
			, 	dbExcep
			);
			throw new SciException(
				"Error exporting the cp dashboard data"
			,	dbExcep
			);
		} catch (SciException sciExcep){
			p_connection.rollbackTransaction();
			log.error("Error exporting the cp dashboard data", sciExcep);
			throw sciExcep;
		}
			
		return l_exportfile;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_projectid
	 * @return
	 * @throws SciException
	 */
	static Map<String,List<QRCPDashboardReport>> getCaseProcessorReport (
		DbConnection	p_connection
	,	int				p_projectid
	,	int				p_startdate
	,	int				p_enddate
	) throws SciException {
		Map<String,List<QRCPDashboardReport>>	l_cpreport			=	null;
		List<ObjectIdPair<String, String>> 		l_reviewtypes		=	null;
		try {
			p_connection.startTransaction();
			l_reviewtypes	=	getReviewTypeProjectList(p_projectid);
			if (l_reviewtypes != null && l_reviewtypes.size() > 0) {
				l_cpreport	=	new LinkedHashMap<>();
			}
			for (ObjectIdPair<String, String> l_reviewdetails : l_reviewtypes) {
				l_cpreport.put(
					l_reviewdetails.getObjectSeqId()
				,	createCaseProcessorReport(
						p_connection
					,	p_projectid
					,	p_startdate
					,	p_enddate
					,	l_reviewdetails.getObjectSeqId()
					)
				);
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error Exporting CP Data"
			, 	dbExcep
			);
			throw new SciException(
				"Error Exporting CP Data"
			,	dbExcep
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error Exporting CP Data"
			, 	sciExcep
			);
			throw sciExcep;
		}
		return l_cpreport;
	}
	
	static List<QRCPDashboardReport> createCaseProcessorReport (
		DbConnection	p_connection
	,	int				p_projectid
	,	int				p_startdate
	,	int				p_enddate
	,	String			p_reviewtype
	) throws SciDbException {
		DbQuerySelect							l_selectquery		=	null;
		List<QRCPDashboardReport>				l_qrcpreportlist	=	null;
		QRCPDashboardReport						l_qrcpreport		=	null;
		Calendar []								l_cldr				=	null;
		String 									l_completequery		= 	null;
		
		l_cldr	=	getDate(p_startdate,p_enddate);
		
		l_completequery	=	String.format(
								CASE_PROCESSOR_REPORT
							, 	DbQueryUtils.getQualifiedObjectName(QRDbSchema.QUALITY_REVIEW_LOG.name())
							,	DbQueryUtils.getQualifiedObjectName(UserDbSchema.USERS.name())
							);
		
		l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
		l_selectquery.addWhereClauseParameter(p_projectid);
		l_selectquery.addWhereClauseParameter(1);
		l_selectquery.addWhereClauseParameter(p_reviewtype);
		l_selectquery.addWhereClauseParameter(l_cldr[0].getTime());
		l_selectquery.addWhereClauseParameter(l_cldr[1].getTime());
		
		try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
			if (l_rs != null) {
				l_qrcpreportlist	=	new ArrayList<>();
				while (l_rs.next()) {
					l_qrcpreport	=	readCaseProcessorReport(l_rs);
					l_qrcpreportlist.add(l_qrcpreport);
				}
			}
		}
		return l_qrcpreportlist;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_projectid
	 * @param p_startdate
	 * @param p_enddate
	 * @return
	 * @throws SciException
	 */
	static QRFieldErrorCount getFieldErrorCount (
		DbConnection	p_connection
	,	int				p_projectid
	,	int				p_startdate
	,	int				p_enddate
	) throws SciException {
		QRFieldErrorCount	l_qrfielderrorcount	=	null;
		DbQuerySelect		l_selectquery		=	null;
		Calendar []			l_cldr				=	null;
		try {
			p_connection.startTransaction();
			l_cldr	=	getDate(p_startdate,p_enddate);
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									QRDbSchema.QUALITY_REVIEW_LOG
								,	DbQueryUtils.bindWhereClauseWithAnd(	
										QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.bindClauseBetween()	
									)	
								,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.name()
								);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(1);
			l_selectquery.addWhereClauseParameter(l_cldr[0].getTime());
			l_selectquery.addWhereClauseParameter(l_cldr[1].getTime());
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_qrfielderrorcount	=	new QRFieldErrorCount();
					while (l_rs.next()) {
						try {
							getStatFieldErrorCount(
								BinaryContentHomeImpl.readContent(
									p_connection
								, 	l_rs.readInt(
										QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
									)
								)
							,	l_qrfielderrorcount
							);
						} catch (SciException sciExcep) {
							p_connection.rollbackTransaction();
							log.error("Error saving data in bin content");
							throw sciExcep;
						}
					}
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	sciExcep
			);
			throw sciExcep;
		}
		
		return l_qrfielderrorcount;
	}
	
	/**
	 * 
	 * @param p_rs
	 * @return
	 * @throws SciDbException
	 */
	static QRDailySummaryReport	readDailySummaryReport (
		DbResultSet	p_rs
	) throws SciDbException {
		QRDailySummaryReport	l_qrdailysummaryreport	=	null;
		
		l_qrdailysummaryreport	=	new QRDailySummaryReport();
		l_qrdailysummaryreport.setDate(p_rs.readDate(1));
		l_qrdailysummaryreport.setSeriousTotalCaseReviewed(p_rs.readInt(2));
		l_qrdailysummaryreport.setSeriousTotalCasePassed(p_rs.readInt(3));
		l_qrdailysummaryreport.setSeriousTotalCaseFailed(p_rs.readInt(4));
		l_qrdailysummaryreport.setSeriousMinQuality(Double.valueOf(p_rs.readString(5)));
		l_qrdailysummaryreport.setSeriousMaxQuality(Double.valueOf(p_rs.readString(6)));
		if (l_qrdailysummaryreport.getSeriousTotalCaseReviewed() != 0) {
			l_qrdailysummaryreport.setSeriousAverageQuality(
				Math.round((l_qrdailysummaryreport.getSeriousTotalCasePassed()*100)/l_qrdailysummaryreport.getSeriousTotalCaseReviewed()
			));
		} else {
			l_qrdailysummaryreport.setSeriousAverageQuality(0);
		}
		l_qrdailysummaryreport.setNonSeriousTotalCaseReviewed(p_rs.readInt(7));
		l_qrdailysummaryreport.setNonSeriousTotalCasePassed(p_rs.readInt(8));
		l_qrdailysummaryreport.setNonSeriousTotalCaseFailed(p_rs.readInt(9));
		l_qrdailysummaryreport.setNonSeriousMinQuality(Double.valueOf(p_rs.readString(10)));
		l_qrdailysummaryreport.setNonSeriousMaxQuality(Double.valueOf(p_rs.readString(11)));
		if (l_qrdailysummaryreport.getNonSeriousTotalCaseReviewed() != 0) {
			l_qrdailysummaryreport.setNonSeriousAverageQuality(
				Math.round((l_qrdailysummaryreport.getNonSeriousTotalCasePassed()*100)/l_qrdailysummaryreport.getNonSeriousTotalCaseReviewed()
			));
		} else {
			l_qrdailysummaryreport.setNonSeriousAverageQuality(0);
		}
		return l_qrdailysummaryreport;
	}
	
	/**
	 * 
	 * @param p_rs
	 * @return
	 * @throws SciDbException
	 */
	static QRCPDashboardReport readCaseProcessorReport (
		DbResultSet	p_rs
	) throws SciDbException {
		QRCPDashboardReport	l_qrcpreport	=	null;
		
		l_qrcpreport	=	new QRCPDashboardReport();
		l_qrcpreport.setUserDisplayName(p_rs.readString(1));
		l_qrcpreport.setTotalCaseReviewed(p_rs.readInt(2));
		l_qrcpreport.setTotalCasePassed(p_rs.readInt(3));
		l_qrcpreport.setTotalCaseWithoutError(p_rs.readInt(4));
		l_qrcpreport.setTotalFieldsReviewed(p_rs.readInt(5));
		l_qrcpreport.setTotalFieldsPassed(p_rs.readInt(6));
		l_qrcpreport.setTotalFieldsFailed(p_rs.readInt(7));
		l_qrcpreport.setTotalCat1Quality(p_rs.readInt(8));
		l_qrcpreport.setTotalCat2Quality(p_rs.readInt(9));
		l_qrcpreport.setTotalCat3Quality(p_rs.readInt(10));
		l_qrcpreport.setTotalCaseQuality(Double.valueOf(p_rs.readString(11)));
		l_qrcpreport.setTotalCat1FieldsReviewed(p_rs.readInt(12));
		l_qrcpreport.setTotalCat2FieldsReviewed(p_rs.readInt(13));
		l_qrcpreport.setTotalCat3FieldsReviewed(p_rs.readInt(14));
		l_qrcpreport.setTotalCat1FieldsError(p_rs.readInt(15));
		l_qrcpreport.setTotalCat2FieldsError(p_rs.readInt(16));
		l_qrcpreport.setTotalCat3FieldsError(p_rs.readInt(17));
		
		if (l_qrcpreport.getTotalCaseReviewed() != 0) {
			l_qrcpreport.setTotalAvgQuality(
				Math.round((l_qrcpreport.getTotalCasePassed()*100)/l_qrcpreport.getTotalCaseReviewed()
			));
		} else {
			l_qrcpreport.setTotalAvgQuality(0);
		}
		return l_qrcpreport;
	}
	
	/**
	 * 
	 * @param l_previousday	It denotes the index with reference to current day.
	 * 						If it is 0 then current day else it would be current
	 * 						day - the value of this parameter.
	 * @return
	 */
	static Calendar[] getDate(
		int	p_startdate
	,	int	p_enddate
	) {
		Calendar	l_cldr[]		=	new Calendar [2];
		Calendar	l_startdatetime	=	null,
					l_enddatetime	=	null;
		
		l_startdatetime	=	Calendar.getInstance();
		l_enddatetime	=	Calendar.getInstance();
		
		l_startdatetime.set(Calendar.HOUR_OF_DAY, 0);
		l_startdatetime.set(Calendar.MINUTE, 0);
		l_startdatetime.set(Calendar.SECOND, 0);
		l_startdatetime.set(Calendar.MILLISECOND, 0);
		l_startdatetime.add(Calendar.DAY_OF_MONTH, p_startdate);
		
		l_enddatetime.set(Calendar.HOUR_OF_DAY, 23);
		l_enddatetime.set(Calendar.MINUTE, 59);
		l_enddatetime.set(Calendar.SECOND, 59);
		l_enddatetime.set(Calendar.MILLISECOND, 999);
		l_enddatetime.add(Calendar.DAY_OF_MONTH, p_enddate);
		
		l_cldr[0]	=	l_startdatetime;
		l_cldr[1]	=	l_enddatetime;
		return l_cldr;
	}
	/**
	 * 
	 * @param p_connection
	 * @param p_reporttype
	 * @param p_userinfo
	 * @param l_previousday
	 * @return
	 * @throws SciException
	 */
	static List<QRReviewData> getQualityReviewLogAll (
		DbConnection	p_connection
	,	String			p_reporttype	
	, 	UserInfo 		p_userinfo
	,	int				p_startdate
	,	int				p_enddate
	,	int				p_projectid
	,	int				p_searchtype
	,	String			p_caseid
	,   String			p_reviewtype
	) throws SciException {
		DbQuerySelect			l_selectquery			=	null;
		List<QRReviewData>		l_ilqrreviewdatalist	=	null;
		QRReviewData			l_qrlreviewdata			=	null;
		Calendar []				l_cldr					=	null;
		
		if ("CA".equals(p_reporttype)) {
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									QRDbSchema.QUALITY_REVIEW_LOG
								, 	DbQueryUtils.bindWhereClauseWithAnd(
										QRDbSchema.QUALITY_REVIEW_LOG.CASEASSOC.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
									,	p_searchtype == 1 ? QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.bindClauseBetween() 
											: QRDbSchema.QUALITY_REVIEW_LOG.CASEID.bindClause()
									)	
								,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.name()
								);

			l_selectquery.addWhereClauseParameter(p_userinfo.getUserSeqId());
		} else if ("QR".equals(p_reporttype)) {
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									QRDbSchema.QUALITY_REVIEW_LOG
								, 	DbQueryUtils.bindWhereClauseWithAnd(
										QRDbSchema.QUALITY_REVIEW_LOG.USRCREATED.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()	
									,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
									,	p_searchtype == 1 ? QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.bindClauseBetween() 
											: QRDbSchema.QUALITY_REVIEW_LOG.CASEID.bindClause()
									)
								,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.name()
								);

			l_selectquery.addWhereClauseParameter(p_userinfo.getUserSeqId());
		} else if ("FL".equals(p_reporttype)) {
			
			if(StringUtils.isNullOrEmpty(p_reviewtype))
			{
			
				l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
										QRDbSchema.QUALITY_REVIEW_LOG
									,	DbQueryUtils.bindWhereClauseWithAnd(
												QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
											,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
											,	p_searchtype == 1 ? QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.bindClauseBetween() 
													: QRDbSchema.QUALITY_REVIEW_LOG.CASEID.bindClause()	
											)
									,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.name()
									);
			} else {
				
				l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									QRDbSchema.QUALITY_REVIEW_LOG
								,	DbQueryUtils.bindWhereClauseWithAnd(	
										QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE.bindClause()
									,	p_searchtype == 1 ? QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.bindClauseBetween() 
											: QRDbSchema.QUALITY_REVIEW_LOG.CASEID.bindClause()	
									)	
								,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.name()
								);
			}
			
		}
		l_selectquery.addWhereClauseParameter(p_projectid);
		l_selectquery.addWhereClauseParameter(1);
		if ("FL".equals(p_reporttype)) {
			if(!(StringUtils.isNullOrEmpty(p_reviewtype))){
				l_selectquery.addWhereClauseParameter(p_reviewtype);
			}
		}
		if (p_searchtype == 1) {
			l_cldr	=	getDate(p_startdate,p_enddate);
			l_selectquery.addWhereClauseParameter(l_cldr[0].getTime());
			l_selectquery.addWhereClauseParameter(l_cldr[1].getTime());
		} else {
			l_selectquery.addWhereClauseParameter(p_caseid);
		}
		try {
			p_connection.startTransaction();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_ilqrreviewdatalist	=	new ArrayList<>();
					while (l_rs.next()) {
						long	l_executiontime	=	System.nanoTime();
						l_qrlreviewdata	=	readQRLReviewData(
												p_connection 
											,	l_rs
											,	BinaryContentHomeImpl.readContent(
													p_connection
												, 	l_rs.readInt(
														QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
													)
												)
											,	BinaryContentHomeImpl.readContent(
													p_connection
												, 	l_rs.readInt(
														QRDbSchema.QUALITY_REVIEW_LOG.CASEREVIEWSUMMSEQID
													)
												)
											);
						log.trace(
							"Time taken to execute method in second is - {}"
						,	TimeUnit.SECONDS.convert(System.nanoTime()-l_executiontime, TimeUnit.NANOSECONDS)
						);
						l_ilqrreviewdatalist.add(l_qrlreviewdata);
					}
					log.trace(
						"Numer of records pulled for the report - {}"
					,	l_ilqrreviewdatalist.size()
					);
				}
				
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		} catch (SciException sciExcep){
			p_connection.rollbackTransaction();
			log.error("Error fetching all the review data of qr from DB", sciExcep);
			throw sciExcep;
		}
			
		return l_ilqrreviewdatalist;
	}
	/**
	 * 
	 * @param p_connection
	 * @param p_sequenceid
	 * @return
	 * @throws SciException
	 * @throws SciServiceException
	 */
	static QRReviewData getQualityReviewLog (
		DbConnection	p_connection
	, 	int				p_sequenceid
	) throws SciException {
		DbQuerySelect		l_selectquery		=	null;
		QRReviewData		l_ilqrreviewdata	=	null;
		try {
			p_connection.startTransaction();
			
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									QRDbSchema.QUALITY_REVIEW_LOG
								, 	QRDbSchema.QUALITY_REVIEW_LOG.RLSEQID.bindClause()
								);
			
			l_selectquery.addWhereClauseParameter(p_sequenceid);
			
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs.next()) {
				
					l_ilqrreviewdata	=	readQRLReviewData(
												p_connection
											,	l_rs
											, 	BinaryContentHomeImpl.readContent(
													p_connection
												, 	l_rs.readInt(
														QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
													)
												)
											,	BinaryContentHomeImpl.readContent(
													p_connection
												, 	l_rs.readInt(
														QRDbSchema.QUALITY_REVIEW_LOG.CASEREVIEWSUMMSEQID
													)
												)
											);
				} else {
					
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching the review data of qr from DB"
			,	dbExcep
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching the review data of qr from DB");
			throw sciExcep;
		}	
		return l_ilqrreviewdata;
	}
	/**
	 * This will read the data from the db and set all the
	 * values of the quality review.
	 * @param p_rs		Resultset object holding the details.
	 * @param p_content	Bin content related to the case id and
	 * 					case version.
	 * @return			Quality Review object that can be used
	 * 					to show all the data on UI.
	 * @throws SciDbException
	 * @throws SciException 
	 */
	private static QRReviewData readQRLReviewData (
		DbConnection 	p_connection,
		DbResultSet		p_rs
	,	BinaryContent	p_content
	,	BinaryContent	p_casesummcontent
	) throws SciDbException, SciException {
		QRReviewData	l_qrlreviewdata	=	null;
		
		try {
			l_qrlreviewdata	=	new QRReviewData();
			l_qrlreviewdata.setSequenceId(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.RLSEQID));
			l_qrlreviewdata.setReviewType(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE));
			l_qrlreviewdata.setReviewRound(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWROUND));
			l_qrlreviewdata.setReviewVersion(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWVERSION));
			l_qrlreviewdata.setReviewActive(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE));
			l_qrlreviewdata.setCreatedOn(p_rs.readDate(QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED));
			l_qrlreviewdata.setCreatedByName(UserHome.retrieveUserBySeqId(p_connection, p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.USRCREATED)).getUserDisplayName());
			l_qrlreviewdata.setCreatedBy(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.USRCREATED));
			l_qrlreviewdata.setCaseId(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CASEID));
			l_qrlreviewdata.setCaseVersion(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CASEVERSION));
			l_qrlreviewdata.setCaseRecvDate(p_rs.readDate(QRDbSchema.QUALITY_REVIEW_LOG.CASERECVDATE));
			l_qrlreviewdata.setCaseSeriousness(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CASESERIOUSNESS));
			l_qrlreviewdata.setCaseAssociate(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.CASEASSOC));
			l_qrlreviewdata.setCaseAssociateName(UserHome.retrieveUserBySeqId(p_connection, l_qrlreviewdata.getCaseAssociate()).getUserDisplayName());
			l_qrlreviewdata.setMedicalReviewerId(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.MEDICALREVIEWER));
			if (l_qrlreviewdata.getMedicalReviewerId() == -1) {
				l_qrlreviewdata.setMedicalReviewerName(StringConstants.NOTAPPLICABLE);
			} else {
				l_qrlreviewdata.setMedicalReviewerName(UserHome.retrieveUserBySeqId(p_connection, l_qrlreviewdata.getMedicalReviewerId()).getUserDisplayName());
			}
			l_qrlreviewdata.setFieldsReviewed(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.FIELDSREVIEW));
			l_qrlreviewdata.setFieldsWithoutError(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.FIELDSWERROR));
			l_qrlreviewdata.setFieldsError(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.FIELDSERROR));
			l_qrlreviewdata.setCaseQuality(Double.valueOf(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CASEQUALITY)));
			l_qrlreviewdata.setCaseCat1Quality(Double.valueOf(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CAT1QUALITY)));
			l_qrlreviewdata.setCaseCat2Quality(Double.valueOf(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CAT2QUALITY)));
			l_qrlreviewdata.setCaseCat3Quality(Double.valueOf(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CAT3QUALITY)));
			l_qrlreviewdata.setCaseResult(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.CASERESULT));
			l_qrlreviewdata.setScft(SafetyDbConfigurationHelper.getTemplateDetails(p_connection, p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.SCFTCTOSEQID)));
			l_qrlreviewdata.setRegClass(p_rs.readString(QRDbSchema.QUALITY_REVIEW_LOG.REGCLASS));
			l_qrlreviewdata.setQrTemplate(getQRTDetails(p_connection, p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.QRTCTOSEQID)));
			if (p_casesummcontent != null) {
				l_qrlreviewdata.setCaseReviewSummary(p_casesummcontent.getContent());
			} else {
				l_qrlreviewdata.setCaseReviewSummary("");
			}
			l_qrlreviewdata.setCdbrflag(p_rs.readInt(QRDbSchema.QUALITY_REVIEW_LOG.CDBR));
			getQRLSafetyDBFieldList(l_qrlreviewdata);
			readQRLReviewFieldDetails(l_qrlreviewdata,p_content);
		} catch (SciServiceException serExcep) {
			log.error("Error reading Quality Review Log Data");
			throw new SciException ("Error reading Quality Review Log Data",serExcep);
		}
		return l_qrlreviewdata;
	}
	/**
	 * This is used to read all the case field details.
	 * @param p_qrlreviewdata	The quality review data that will
	 * 							hold all the contents related to
	 * 							the case review.
	 * @param p_content			The content that needs to be parsed.
	 */
	static void	readQRLReviewFieldDetails (
		QRReviewData	p_qrlreviewdata
	,	BinaryContent	p_content
	) {
		
		String []								l_fielddetails				=	null,
												l_fielddetailslist			=	null;
		HashMap<String,QRReviewFieldDetails>	l_qrlreviewfielddetailslist	=	null;
		QRReviewFieldDetails					l_qrlreviewfielddetails		=	null;
		int										l_separatorindex			=	0;
		l_qrlreviewfielddetailslist	=	new HashMap<>();
		if (p_content.getContent() != null) {
			l_fielddetailslist	=	p_content.getContent().split("#,#");
			for (String l_fieldnonuserinput : l_fielddetailslist[0].split(";")) {
				l_qrlreviewfielddetails	=	new QRReviewFieldDetails();
				l_fielddetails = l_fieldnonuserinput.split("~");
				if (l_fielddetails.length == 3) {
				l_qrlreviewfielddetails.setFieldMnemonic(l_fielddetails[0]);
				l_qrlreviewfielddetails.setFieldStatus(Integer.parseInt(l_fielddetails[1]));
					l_qrlreviewfielddetails.setFieldErrorCode(l_fielddetails[2]);
				} else {
					l_qrlreviewfielddetails.setFieldMnemonic(l_fielddetails[0]);
					l_qrlreviewfielddetails.setFieldStatus(Integer.parseInt(l_fielddetails[1]));
				}
				l_qrlreviewfielddetailslist.put(l_qrlreviewfielddetails.getFieldMnemonic(),l_qrlreviewfielddetails);
			}
			if (l_qrlreviewfielddetailslist.keySet().size() > 0) {
				for (String l_fielduserinput : l_fielddetailslist[1].split("#~#")) {
					l_qrlreviewfielddetails	=	null;
					l_separatorindex		=	l_fielduserinput.indexOf(":(");
					l_qrlreviewfielddetails	=	l_qrlreviewfielddetailslist.get(l_fielduserinput.substring(0,l_separatorindex));
					l_qrlreviewfielddetails.setFieldComment(l_fielduserinput.substring(l_separatorindex+2,l_fielduserinput.length()-1));
				}
				p_qrlreviewdata.setQrlReviewFieldDetails(l_qrlreviewfielddetailslist);
			}
		}
	}
	
	static void insertErrorDashboardData (
		DbConnection	p_connection
	,	QRReviewData 	p_reviewdata
	,	int				p_cmseqid
	,	String			p_internalcaseid
	) throws SciException {
		DbQueryInsert				l_insertquery			=	null;
		SafetyDbCaseFieldsTemplate	l_scft					=	null;
		Map<String, Integer> 		l_mnemoniccatmapping	=	null;
		try  {
			p_connection.startTransaction();
			try {
				l_scft	=	SafetyDbConfigurationHelper.getTemplateDetails(p_reviewdata.getQrTemplate().getSCFTSeqId());
			} catch (SciServiceException serExcep) {
				log.error(
					"Error fetching all the review data of qr from DB"
				, 	serExcep
				);
				throw new SciException(
					"Error fetching all the review data of qr from DB"
				,	serExcep
				);
			}
			if (l_scft.getCaseFieldList() != null && l_scft.getCaseFieldList().size() > 0) {
				l_mnemoniccatmapping	=	new HashMap<>();
			}
			for (SafetyDbCaseFields l_fields : l_scft.getCaseFieldList()) {
				l_mnemoniccatmapping.put(l_fields.getCaseFieldMnemonic(), l_fields.getCaseFieldType().getCatCode());
			}
			for (String l_fieldmnemonic : p_reviewdata.getQrlReviewFieldDetails().keySet()) {
				if (p_reviewdata.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldStatus() == 3) {
					l_insertquery = DbQueryUtils.createInsertQuery(QRDbSchema.ERRORDATA);
					l_insertquery.addInsertParameter(p_reviewdata.getSequenceId());
					l_insertquery.addInsertParameter(p_reviewdata.getQrTemplate().getProjectId());
					try {
						l_insertquery.addInsertParameter(((QRTemplate)p_reviewdata.getQrTemplate()).getSCFTSeqId());
					} catch (SciServiceException serExcep) {
						log.error(
							"Error fetching all the review data of qr from DB"
						, 	serExcep
						);
						throw new SciException(
							"Error fetching all the review data of qr from DB"
						,	serExcep
						);
					}
					
					l_insertquery.addInsertParameter(p_reviewdata.getQrTemplate().getSequenceId());
					l_insertquery.addInsertParameter(p_cmseqid);
					l_insertquery.addInsertParameter(p_reviewdata.getReviewType());
					l_insertquery.addInsertParameter(p_reviewdata.getReviewRound());
					l_insertquery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
					l_insertquery.addInsertParameter(p_internalcaseid);
					l_insertquery.addInsertParameter(p_reviewdata.getCaseAssociate());
					l_insertquery.addInsertParameter(p_reviewdata.getCaseSeriousness());
					l_insertquery.addInsertParameter(l_fieldmnemonic);
					l_insertquery.addInsertParameter(p_reviewdata.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldErrorCode());
					l_insertquery.addInsertParameter(p_reviewdata.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldComment());
					l_insertquery.addInsertParameter(l_mnemoniccatmapping.get(l_fieldmnemonic));
					p_connection.executeQuery(l_insertquery);
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving review data of qr into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error saving review data of qr into DB"
			,	dbExcep	
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error saving review data of qr into DB");
			throw sciExcep;
		}
	}
	
	static void updateErrorDashboardData (
		DbConnection	p_connection
	,	QRReviewData 	p_reviewdata
	,	String			p_internalcaseid
	) throws SciException {
		DbQueryDelete	l_deletequery	=	null;
		try 
		{
			p_connection.startTransaction();
			l_deletequery = DbQueryUtils.createDeleteQuery(
								QRDbSchema.ERRORDATA
							,	DbQueryUtils.bindWhereClauseWithAnd(
									QRDbSchema.ERRORDATA.INTERNALCASEID.bindClause()
								,	QRDbSchema.ERRORDATA.REVIEWTYPE.bindClause()
								,	QRDbSchema.ERRORDATA.REVIEWROUND.bindClause()
								)
							);
			l_deletequery.addWhereClauseParameter(p_internalcaseid);
			l_deletequery.addWhereClauseParameter(p_reviewdata.getReviewType());
			l_deletequery.addWhereClauseParameter(p_reviewdata.getReviewRound());
			p_connection.executeQuery(l_deletequery);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error saving review data of qr into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error saving review data of qr into DB"
			,	dbExcep	
			);
		}
	}
	
	/**
	 * This is used to read all the case field details.
	 * @param p_qrlreviewdata	The quality review data that will
	 * 							hold all the contents related to
	 * 							the case review.
	 * @param p_content			The content that needs to be parsed.
	 */
	/* static void	readQRLReviewFieldDetailsError (
		QRReviewData	p_qrlreviewdata
	,	BinaryContent	p_content
	) {
		String []								l_fielddetails				=	null,
												l_fielddetailslist			=	null;
		HashMap<String,QRReviewFieldDetails>	l_qrlreviewfielddetailslist	=	null;
		QRReviewFieldDetails					l_qrlreviewfielddetails		=	null;
		int										l_separatorindex			=	0;
		String									l_fieldmnemonic				=	null;
		
		l_qrlreviewfielddetailslist	=	new HashMap<>();
		if (p_content.getContent() != null) {
			l_fielddetailslist	=	p_content.getContent().split("#,#");
			for (String l_fieldnonuserinput : l_fielddetailslist[0].split(";")) {
				l_qrlreviewfielddetails	=	new QRReviewFieldDetails();
				l_fielddetails = l_fieldnonuserinput.split("~");
				l_qrlreviewfielddetails.setFieldMnemonic(l_fielddetails[0]);
				if (Integer.parseInt(l_fielddetails[1]) == 3) {
					l_qrlreviewfielddetails.setFieldStatus(Integer.parseInt(l_fielddetails[1]));
					l_qrlreviewfielddetailslist.put(l_qrlreviewfielddetails.getFieldMnemonic(),l_qrlreviewfielddetails);
				}
			}
			if (l_qrlreviewfielddetailslist.keySet().size() > 0) {
				for (String l_fielduserinput : l_fielddetailslist[1].split("#~#")) {
					l_qrlreviewfielddetails	=	null;
					l_separatorindex		=	l_fielduserinput.indexOf(":(");
					l_fieldmnemonic			=	null;
					l_fieldmnemonic			=	l_fielduserinput.substring(0,l_separatorindex);
					if (l_qrlreviewfielddetailslist.containsKey(l_fieldmnemonic)) {
						l_qrlreviewfielddetails	=	l_qrlreviewfielddetailslist.get(l_fieldmnemonic);
						l_qrlreviewfielddetails.setFieldComment(l_fielduserinput.substring(l_separatorindex+2,l_fielduserinput.length()-1));
					}
					
				}
				p_qrlreviewdata.setQrlReviewFieldDetails(l_qrlreviewfielddetailslist);
			}
		}
	}*/
	
	/**
	 * This method is used to fetch all the data of safety
	 * db case field maintained at project level and
	 * will map all the fields to the group they belong.
	 * @return	Map of field tab and field details.
	 * @throws SciServiceException
	 */
	static void getQRLSafetyDBFieldList(
		QRReviewData	p_qrreviewdata
	) throws SciServiceException 
	{
		p_qrreviewdata.setSafetyDbCaseFieldList(getReviewFieldList(p_qrreviewdata.getQrTemplate(), p_qrreviewdata.getScft()));	
	}
	
	/**
	 * 
	 */
	static void getStatFieldErrorCount (
		BinaryContent			p_content
	,	QRFieldErrorCount		p_report
	) {
		String []				l_fielddata			=	null,
								l_fielddetails		=	null;
		if (p_content.getContent() != null) {
			for (String l_fielddetailslist : p_content.getContent().split(",")) {
				l_fielddetails = l_fielddetailslist.split(":");
				l_fielddata	=	l_fielddetails[1].split("~");
				if (Integer.parseInt(l_fielddata[0]) == 3) {
					p_report.getStatFieldError().put(
						l_fielddata[1]
					,	p_report.getStatFieldError().get(l_fielddata[1])==null?1:p_report.getStatFieldError().get(l_fielddata[1])+1
					);
				}
			}
		}	
	}
	
	
	
	/**
	 * This method will provide the functionality
	 * to send email as soon as the review is 
	 * completed.
	 * @param p_reviewdata	Quality Review Data with all the required
	 * 						fields to send email.
	 * @throws SciServiceException
	 * @throws SciException 
	 */
	static void sendEmail (
		QRReviewData 	p_reviewdata
	,	int				p_projectid
	,	String			p_appid
	,	String 			p_mailpurpose
	) throws SciServiceException, SciException {
		MailRuntimeContents							l_mailruncontents	=	null;
		List<String>								l_mailcc			=	null,
													l_mailto			=	null;
		Map<String, Object>							l_mailcontents		=	null;
		SafetyDbCaseFieldsTemplate					l_scft				=	null;
		LinkedHashMap<String, QRReviewFieldDetails>	l_errormap			=	null;
		String										l_emailcclist		=	null;
		Map<String,String>							l_errortypesMap			= null;
		
		l_mailruncontents	=	new MailRuntimeContents();
		l_mailcc			=	new ArrayList<>();
		l_mailto			=	new ArrayList<>();
		l_mailcontents		=	new HashMap<String, Object>();
		l_errortypesMap		=	getErrorTypes(p_projectid);
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			l_scft	=	SafetyDbConfigurationHelper.getTemplateDetails(connection,p_reviewdata.getQrTemplate().getSCFTSeqId());
			l_emailcclist	=	ConfigHome.getPrjConfigSettings(
									p_appid+StringConstants.PERIOD+QRConstant.PROP_PROJ_EMAIL_CC_LIST
										+StringConstants.PERIOD+p_reviewdata.getReviewType()
								, 	p_projectid
								);
			if (!StringUtils.isNullOrEmpty(l_emailcclist)) {
				for (String l_ccemail : l_emailcclist.split(",")) {
					l_mailcc.add(l_ccemail);
				}
			}	
			l_errormap			=	new LinkedHashMap<>();
			if (p_reviewdata.getQrlReviewFieldDetails() != null && p_reviewdata.getQrlReviewFieldDetails().keySet().size()>0) {
				for (SafetyDbCaseFields l_casefields : l_scft.getCaseFieldList()) {
					if (p_reviewdata.getQrlReviewFieldDetails().get(l_casefields.getCaseFieldMnemonic()) != null) {
						l_errormap.put(l_casefields.getCaseFieldName(), p_reviewdata.getQrlReviewFieldDetails().get(l_casefields.getCaseFieldMnemonic()));
					}
				}
			}	
			l_mailto.add(UserHome.retrieveUserBySeqId(connection,p_reviewdata.getCaseAssociate()).getUserEmail());
			l_mailcc.add(UserHome.retrieveUserBySeqId(connection,p_reviewdata.getCreatedBy()).getUserEmail());
			l_mailruncontents.setToAddresses(l_mailto);
			l_mailruncontents.setCcAddresses(l_mailcc);
			l_mailcontents.put("caseassociate_name", UserHome.retrieveUserBySeqId(connection,p_reviewdata.getCaseAssociate()).getUserDisplayName());
			l_mailcontents.put("case_id_alias", l_scft.getCaseIdAlias());
			l_mailcontents.put("case_version_alias", l_scft.getCaseVersionAlias());
			l_mailcontents.put("initial_received_date", new SimpleDateFormat(DATE_FORMAT_CONST).format(p_reviewdata.getCaseRecvDate()));
			l_mailcontents.put("ilqrreviewer_name", UserHome.retrieveUserBySeqId(connection,p_reviewdata.getCreatedBy()).getUserDisplayName());
			if (p_reviewdata.getMedicalReviewerId() != -1) {
				l_mailcontents.put("medicalreviewer_name", UserHome.retrieveUserBySeqId(connection,p_reviewdata.getMedicalReviewerId()).getUserDisplayName());
			} else {
				l_mailcontents.put("medicalreviewer_name", StringConstants.NOTAPPLICABLE);
			}
			l_mailcontents.put(QRConstant.FTL_CASE_QUALITY_REPLACE, String.format("%.2f", p_reviewdata.getCaseQuality()));
			l_mailcontents.put(QRConstant.FTL_CASE_CAT1_REPLACE, String.format("%.2f", p_reviewdata.getCaseCat1Quality()));
			l_mailcontents.put(QRConstant.FTL_CASE_CAT2_REPLACE, String.format("%.2f", p_reviewdata.getCaseCat2Quality()));
			l_mailcontents.put(QRConstant.FTL_CASE_CAT3_REPLACE, String.format("%.2f", p_reviewdata.getCaseCat3Quality()));
			l_mailcontents.put("errorMap",l_errormap);
			l_mailcontents.put("errorTypeMap",l_errortypesMap);
			
			l_mailcontents.put("td_style", "width=\"200\" valign=\"top\" align=\"left\" nowrap=\"\" style=\"width:180pt;height:15pt;padding:0 5.4pt;border:1pt solid windowtext;\"");
			l_mailcontents.put("th_style", "width=\"200\" valign=\"top\" align=\"left\" nowrap=\"\" style=\"width:180pt;height:15pt;padding:0 5.4pt;border:1pt solid windowtext;\"");
			l_mailcontents.put("td_font_style", "face=\"Calibri,sans-serif\" size=\"2\" style=\"font-family: Calibri, sans-serif, serif, EmojiFont;\"");
			
			l_mailcontents.put("review_data",p_reviewdata);
		} catch (SciDbException dbExcep) {
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciException(
				"Error in fetching template details"
			,	dbExcep	
			);
		}
		
		
		
		
		/*l_mailcontents.put("caseassociate_name", UserHome.retrieveUserBySeqId(p_reviewdata.getCaseAssociate()).getUserDisplayName());
		l_mailcontents.put("case_id_alias", l_scft.getCaseIdAlias());
		l_mailcontents.put("case_version_alias", l_scft.getCaseVersionAlias());
		l_mailcontents.put("case_id", p_reviewdata.getCaseId());
		l_mailcontents.put("case_version", p_reviewdata.getCaseVersion());
		l_mailcontents.put("case_seriousness", p_reviewdata.getCaseSeriousness());
		l_mailcontents.put("initial_received_date", new SimpleDateFormat(DATE_FORMAT_CONST).format(p_reviewdata.getCaseRecvDate()));
		l_mailcontents.put("ilqrreviewer_name", UserHome.retrieveUserBySeqId(p_reviewdata.getCreatedBy()).getUserDisplayName());
		l_mailcontents.put("case_summary_comment", p_reviewdata.getCaseReviewSummary());
		l_mailcontents.put("fields_total_count", p_reviewdata.getFieldsReviewed());
		l_mailcontents.put("fields_we_count", p_reviewdata.getFieldsWithoutError());
		l_mailcontents.put("fields_error_count", p_reviewdata.getFieldsError());
		l_mailcontents.put(QRConstant.FTL_CASE_QUALITY_REPLACE, String.format("%.2f", p_reviewdata.getCaseQuality()));
		l_mailcontents.put(QRConstant.FTL_CASE_CAT1_REPLACE, String.format("%.2f", p_reviewdata.getCaseCat1Quality()));
		l_mailcontents.put(QRConstant.FTL_CASE_CAT2_REPLACE, String.format("%.2f", p_reviewdata.getCaseCat2Quality()));
		l_mailcontents.put(QRConstant.FTL_CASE_CAT3_REPLACE, String.format("%.2f", p_reviewdata.getCaseCat3Quality()));
		l_mailcontents.put("cat1_fieldsreviewed", p_reviewdata.getCat1FieldsReviewed());
		l_mailcontents.put("cat2_fieldsreviewed", p_reviewdata.getCat2FieldsReviewed());
		l_mailcontents.put("cat3_fieldsreviewed", p_reviewdata.getCat3FieldsReviewed());
		l_mailcontents.put("cat1_fieldswe_count", p_reviewdata.getCat1FieldsWithoutError());
		l_mailcontents.put("cat2_fieldswe_count", p_reviewdata.getCat2FieldsWithoutError());
		l_mailcontents.put("cat3_fieldswe_count", p_reviewdata.getCat3FieldsWithoutError());
		l_mailcontents.put("cat1_fieldserror_count", p_reviewdata.getCat1FieldsReviewed()-p_reviewdata.getCat1FieldsWithoutError());
		l_mailcontents.put("cat2_fieldserror_count", p_reviewdata.getCat2FieldsReviewed()-p_reviewdata.getCat2FieldsWithoutError());
		l_mailcontents.put("cat3_fieldserror_count", p_reviewdata.getCat3FieldsReviewed()-p_reviewdata.getCat3FieldsWithoutError());
		l_mailcontents.put("case_result", p_reviewdata.getCaseResult());
		l_mailcontents.put("errorMap",l_errormap);
		l_mailcontents.put("errorTypeMap",l_errortypesMap);
		l_mailcontents.put("td_style", "width=\"200\" valign=\"top\" align=\"left\" nowrap=\"\" style=\"width:180pt;height:15pt;padding:0 5.4pt;border:1pt solid windowtext;\"");
		l_mailcontents.put("th_style", "width=\"200\" valign=\"top\" align=\"left\" nowrap=\"\" style=\"width:180pt;height:15pt;padding:0 5.4pt;border:1pt solid windowtext;\"");
		l_mailcontents.put("td_font_style", "face=\"Calibri,sans-serif\" size=\"2\" style=\"font-family: Calibri, sans-serif, serif, EmojiFont;\"");*/
		l_mailruncontents.setRuntimeValues(l_mailcontents);
		MailRuntimeHelper.relayEmail(p_projectid, p_mailpurpose, l_mailruncontents);
	}
	
	/**
	 * This will read the data stored in the database and set all the
	 * attributes related to the Quality Review Template.
	 * @param p_rs
	 * @param p_qrtemplate
	 * @throws SciDbException
	 */
	static void readQRTAttributeDetails (
		DbResultSet	p_rs
	,	QRTemplate 	p_qrtemplate	
	) throws SciDbException {
		p_qrtemplate.setCaseClassificationCat1(p_rs.readString(QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT1));
		p_qrtemplate.setCaseClassificationCat2(p_rs.readString(QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT2));
		p_qrtemplate.setCaseClassificationCat3(p_rs.readString(QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT3));
		p_qrtemplate.setCaseClassificationOverall(p_rs.readString(QRDbSchema.TEMPLATE.CASECLASSIFICATIONOVERALL));
		p_qrtemplate.setCaseQualityThreshold(Double.valueOf(p_rs.readString(QRDbSchema.TEMPLATE.CASEQUALITYTHRESHOLD)));
		p_qrtemplate.setCat1QualityThreshold(Double.valueOf(p_rs.readString(QRDbSchema.TEMPLATE.CAT1QUALITYTHRESHOLD)));
		p_qrtemplate.setCat2QualityThreshold(Double.valueOf(p_rs.readString(QRDbSchema.TEMPLATE.CAT2QUALITYTHRESHOLD)));
		p_qrtemplate.setCat3QualityThreshold(Double.valueOf(p_rs.readString(QRDbSchema.TEMPLATE.CAT3QUALITYTHRESHOLD)));
		p_qrtemplate.setRvwAllToCorrect(p_rs.readInt(QRDbSchema.TEMPLATE.REVIEWERALLOWEDCORRECT));
		p_qrtemplate.setCaseResultCriteriaMode(p_rs.readInt(QRDbSchema.TEMPLATE.CASECRITERIAMODE));
		p_qrtemplate.setCaseResultCriteria(p_rs.readString(QRDbSchema.TEMPLATE.CASECRITERIA));
		p_qrtemplate.setDownloadReviewTemplateSeqId(p_rs.readInt(QRDbSchema.TEMPLATE.RVWCMPTDWNLDCONTENTSEQID));
	}
	
	/**
	 * @param p_caseclassificationcat3 
	 * @param p_caseclassificationcat2 
	 * @param p_caseclassificationcat1 
	 * @param p_caseclassificationoverall 
	 * @param p_cat3qualitythreshold 
	 * @param p_cat2qualitythreshold 
	 * @param p_cat1qualitythreshold 
	 * @param p_casequalitythreshold 
	 * @param p_correctionallowed 
	 * @param p_emailccaddress 
	 * @param p_emailsendsetting 
	 * @throws SciException 
	 * 
	 */
	static void updateQRTAttributeDetails (
		DbConnection		p_connection
	,	int					p_templateseqid
	, 	int 				p_correctionallowed
	, 	double 				p_casequalitythreshold
	, 	double 				p_cat1qualitythreshold
	, 	double 				p_cat2qualitythreshold
	, 	double 				p_cat3qualitythreshold
	, 	String 				p_caseclassificationoverall
	, 	String 				p_caseclassificationcat1
	, 	String 				p_caseclassificationcat2
	, 	String 				p_caseclassificationcat3
	,	int					p_casecriteriamode
	,	String				p_casecriteria
	,	int					p_reviewsummaryrequired
	,	UserSession 		p_usersession
	, 	String 				p_sTemplateContent
	) throws SciException {
		DbQueryUpdate l_updatequery = 	null;
		BinaryContent		l_content			=	null;
		if(p_sTemplateContent!=null)
		{
			l_updatequery	=	DbQueryUtils.createUpdateQuery(
									QRDbSchema.TEMPLATE
								,	DbQueryUtils.bindUpdateClause(
										QRDbSchema.TEMPLATE.CASEQUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CAT1QUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CAT2QUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CAT3QUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONOVERALL
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT1
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT2
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT3
									,	QRDbSchema.TEMPLATE.REVIEWERALLOWEDCORRECT
									,	QRDbSchema.TEMPLATE.CASECRITERIAMODE
									,	QRDbSchema.TEMPLATE.CASECRITERIA
									,   QRDbSchema.TEMPLATE.RVWCMPTDWNLDCONTENTSEQID
									)
								,	QRDbSchema.TEMPLATE.CTOSEQID.bindClause()
								);
		
		}
		else
		{
			l_updatequery	=	DbQueryUtils.createUpdateQuery(
									QRDbSchema.TEMPLATE
								,	DbQueryUtils.bindUpdateClause(
										QRDbSchema.TEMPLATE.CASEQUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CAT1QUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CAT2QUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CAT3QUALITYTHRESHOLD
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONOVERALL
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT1
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT2
									,	QRDbSchema.TEMPLATE.CASECLASSIFICATIONCAT3
									,	QRDbSchema.TEMPLATE.REVIEWERALLOWEDCORRECT
									,	QRDbSchema.TEMPLATE.CASECRITERIAMODE
									,	QRDbSchema.TEMPLATE.CASECRITERIA
									)
								,	QRDbSchema.TEMPLATE.CTOSEQID.bindClause()
								);
		}
		try {
			
			p_connection.startTransaction();
			if(p_sTemplateContent != null)
			{	
				l_content		=	BinaryContent.createNew("reviewTemplateContent", "", "", p_sTemplateContent);
				BinaryContentHomeImpl.saveContent(p_connection, p_usersession.getUserInfo()	, l_content);
			}
			
			l_updatequery.addUpdateParameter(String.valueOf(p_casequalitythreshold));
			l_updatequery.addUpdateParameter(String.valueOf(p_cat1qualitythreshold));
			l_updatequery.addUpdateParameter(String.valueOf(p_cat2qualitythreshold));
			l_updatequery.addUpdateParameter(String.valueOf(p_cat3qualitythreshold));
			l_updatequery.addUpdateParameter(p_caseclassificationoverall);
			l_updatequery.addUpdateParameter(p_caseclassificationcat1);
			l_updatequery.addUpdateParameter(p_caseclassificationcat2);
			l_updatequery.addUpdateParameter(p_caseclassificationcat3);
			l_updatequery.addUpdateParameter(p_correctionallowed);
			l_updatequery.addUpdateParameter(p_casecriteriamode);
			l_updatequery.addUpdateParameter(p_casecriteria);
			if(l_content != null) {
				l_updatequery.addUpdateParameter(l_content.getContentSeqId());
			}	
			l_updatequery.addWhereClauseParameter(p_templateseqid);
			p_connection.executeQuery(l_updatequery);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error updating state of template into DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error updating state of template into DB"
			,	dbExcep	
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error saving data in bin content");
			throw sciExcep;
		}
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_projectid
	 * @return
	 * @throws SciException
	 */
	static QRErrorDashboardData[] getErrorDbData (
		DbConnection	p_connection
	,	int				p_projectid
	) throws SciException {
		String							l_categories	=	null;
		LocalDateTime[]					l_datetime		=	null;
		SafetyDbCaseFieldsTemplate		l_scft			=	null;
		Map<String,SafetyDbCaseFields>	l_fielddetails	=	null;
		QRErrorDashboardData[]			l_errordbdata	=	null;
		int								l_catcount		=	-1;
				
		try {
			p_connection.startTransaction();
			l_scft		=	SafetyDbConfigurationHelper.getActiveTemplateDetails(p_projectid);
			if (l_scft == null) {
				log.error("Active SCFT not found");
				throw new SciException("Error fetching all the review data of qr from DB");
			}
			if (l_scft.getCaseFieldList() != null && l_scft.getCaseFieldList().size() > 0) {
				l_fielddetails	=	new HashMap<>();
			}
			for (SafetyDbCaseFields l_fields : l_scft.getCaseFieldList()) {
				l_fielddetails.put(l_fields.getCaseFieldMnemonic(), l_fields);
			}
			l_datetime	=	getApplicableDates(
								Integer.parseInt(
									ConfigHome.getPrjConfigSettings(
										QRConstant.APP_NAME+ StringConstants.PERIOD+ QRConstant.PROP_PROJ_START_MONTH_DAY
									, 	p_projectid
									)
								)
							);
			l_categories	=	getApplicableCategories(p_projectid);
			
			if (StringUtils.isNullOrEmpty(l_categories)) {
				l_catcount	=	0;
			} else {
				l_catcount	=	l_categories.split("~").length;
			}
			
			l_errordbdata		=	new QRErrorDashboardData[3];
			l_errordbdata[0]	=	createErrorDbData(
										p_connection
									,	p_projectid
									,	l_datetime[0]
									,	l_datetime[1]
									,	l_catcount
									);
			l_errordbdata[1]	=	createErrorDbData(
										p_connection
									,	p_projectid
									,	l_datetime[2]
									,	l_datetime[3]
									,	l_catcount	
									);
			l_errordbdata[2]	=	createErrorDbData(
										p_connection
									,	p_projectid
									,	l_datetime[4]
									,	l_datetime[5]
									,	l_catcount	
									);
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	sciExcep
			);
			throw sciExcep;
		} catch (SciServiceException serExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	serExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	serExcep
			);
		}
		return l_errordbdata;
	}
	
	static QRErrorDashboardData createErrorDbData(
		DbConnection		p_connection	
	,	int					p_projectid
	,	LocalDateTime		p_startdatetime
	,	LocalDateTime		p_enddatetime
	,	int					p_catcount
	) throws SciException {
		String							l_completequery		=	null;
		DbQuerySelect					l_selectquery		=	null;
		SafetyDbCaseFieldsTemplate		l_scft				=	null;
		Map<String,SafetyDbCaseFields>	l_fielddetails		=	null;
		QRErrorDashboardData			l_errordbdaata		=	null;
		Map<String,Integer>				l_errorfieldsmap	=	null,
										l_catcountmap		=	null,
										l_errortypemap		=	null,
										l_reviewtypemap		=	null;
		
		try {
			p_connection.startTransaction();
			try {
				l_scft		=	SafetyDbConfigurationHelper.getActiveTemplateDetails(p_projectid);
			} catch (SciServiceException serExcep) {
				log.error(
					"Error fetching all the review data of qr from DB"
				, 	serExcep
				);
				throw new SciException(
					"Error fetching all the review data of qr from DB"
				,	serExcep
				);
			}
			if (l_scft.getCaseFieldList() != null && l_scft.getCaseFieldList().size() > 0) {
				l_fielddetails	=	new HashMap<>();
			}
			for (SafetyDbCaseFields l_fields : l_scft.getCaseFieldList()) {
				l_fielddetails.put(l_fields.getCaseFieldMnemonic(), l_fields);
			}
			l_completequery	=	String.format(ERROR_DB_FIELD_COUNT, DbQueryUtils.getQualifiedObjectName(QRDbSchema.ERRORDATA.name()));
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(Timestamp.valueOf(p_startdatetime));
			l_selectquery.addWhereClauseParameter(Timestamp.valueOf(p_enddatetime));
			l_errordbdaata	=	new QRErrorDashboardData();
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_errorfieldsmap	=	new LinkedHashMap<>();
					if (p_catcount > 0) {
						l_catcountmap	=	new LinkedHashMap<>();
					}
					while (l_rs.next()) {
						l_errorfieldsmap.put(
							l_fielddetails.get(l_rs.readString(1)).getCaseFieldName()
						, 	l_rs.readInt(2)
						);
						if (p_catcount > 0) {
							if (l_catcountmap.containsKey(l_fielddetails.get(l_rs.readString(1)).getCaseFieldType().name())) {
								l_catcountmap.put(
									l_fielddetails.get(l_rs.readString(1)).getCaseFieldType().name()
								, 	l_catcountmap.get(l_fielddetails.get(l_rs.readString(1)).getCaseFieldType().name())+l_rs.readInt(2)
								);
							} else {
								l_catcountmap.put(
									l_fielddetails.get(l_rs.readString(1)).getCaseFieldType().name()
								, 	l_rs.readInt(2)
								);
							}
						}
					}
					l_errordbdaata.setErrorFieldsCountMap(l_errorfieldsmap);
					l_errordbdaata.setFieldCatCountMap(l_catcountmap);
				}
			}
			
			l_selectquery	=	null;
			l_completequery	=	String.format(ERROR_DB_TYPE_COUNT, DbQueryUtils.getQualifiedObjectName(QRDbSchema.ERRORDATA.name()));
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(Timestamp.valueOf(p_startdatetime));
			l_selectquery.addWhereClauseParameter(Timestamp.valueOf(p_enddatetime));
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_errortypemap	=	new LinkedHashMap<>();
					while (l_rs.next()) {
						l_errortypemap.put(
							l_rs.readString(1)
						,	l_rs.readInt(2)	
						);
					}
					l_errordbdaata.setErrorTypeCountMap(l_errortypemap);
				}
			}
			
			l_selectquery	=	null;
			l_completequery	=	String.format(ERROR_DB_RT_COUNT, DbQueryUtils.getQualifiedObjectName(QRDbSchema.ERRORDATA.name()));
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(Timestamp.valueOf(p_startdatetime));
			l_selectquery.addWhereClauseParameter(Timestamp.valueOf(p_enddatetime));
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_reviewtypemap	=	new LinkedHashMap<>();
					while (l_rs.next()) {
						l_reviewtypemap.put(
							l_rs.readString(1)
						, 	l_rs.readInt(2)
						);
					}
					l_errordbdaata.setReviewTypeCountMap(l_reviewtypemap);
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	sciExcep
			);
			throw sciExcep;
		}	
		return l_errordbdaata;
	}
	
	/**
	 */
	static LocalDateTime[] getApplicableDates (
		int	p_projstartday
	) {
		LocalDateTime[]	l_datetime					=	null;
		LocalDateTime	l_todaystartdatetime		=	null,
						l_todayenddatetime			=	null,
						l_mtdstartdatetime			=	null,
						l_mtdenddatetime			=	null,
						l_prevmonthstartdatetime	=	null,
						l_prevmonthenddatetime		=	null;
		LocalDateTime	l_currentdatetime			=	LocalDateTime.now();
		LocalDateTime	l_projdatetime				=	LocalDateTime.of(
															l_currentdatetime.getYear()
														, 	l_currentdatetime.getMonthValue()
														, 	p_projstartday
														, 	l_currentdatetime.getHour()
														,	l_currentdatetime.getMinute()
														,	l_currentdatetime.getSecond()
														,	l_currentdatetime.getNano()
														);
		int	l_datediff	=	l_currentdatetime.compareTo(l_projdatetime);
		if (l_datediff == 0) {
			l_todaystartdatetime		=	LocalDateTime.of(
												l_currentdatetime.getYear()
											, 	l_currentdatetime.getMonthValue()
											, 	l_currentdatetime.getDayOfMonth()
											, 	00
											,	00
											);
			l_todayenddatetime			=	LocalDateTime.of(
												l_currentdatetime.getYear()
											, 	l_currentdatetime.getMonthValue()
											, 	l_currentdatetime.getDayOfMonth()
											, 	23
											,	59
											,	59
											);
			l_mtdstartdatetime			=	l_todaystartdatetime;
			l_mtdenddatetime			=	l_todayenddatetime;
			l_prevmonthstartdatetime	=	l_mtdstartdatetime.minusMonths(1);
			l_prevmonthenddatetime		=	l_mtdenddatetime.minusDays(1);
			log.info("Today Start Date "+l_todaystartdatetime+" End Time "+l_todayenddatetime);
			log.info("Month Start Date "+l_mtdstartdatetime+" End Time "+l_mtdenddatetime);
			log.info("Prev Month Start Date "+l_prevmonthstartdatetime+" End Time "+l_prevmonthenddatetime);
		} else if (l_datediff > 0) {
			l_todaystartdatetime		=	LocalDateTime.of(
												l_currentdatetime.getYear()
											, 	l_currentdatetime.getMonthValue()
											, 	l_currentdatetime.getDayOfMonth()
											, 	00
											,	00
											);
			l_todayenddatetime			=	LocalDateTime.of(
												l_currentdatetime.getYear()
											, 	l_currentdatetime.getMonthValue()
											, 	l_currentdatetime.getDayOfMonth()
											, 	23
											,	59
											,	59
											);
			l_mtdstartdatetime			=	LocalDateTime.of(
												l_todaystartdatetime.getYear()
											, 	l_todaystartdatetime.getMonthValue()
											, 	p_projstartday
											, 	00
											,	00
											);
			l_mtdenddatetime			=	l_todayenddatetime;
			l_prevmonthstartdatetime	=	l_mtdstartdatetime.minusMonths(1);
			l_prevmonthenddatetime		=	l_mtdstartdatetime.minusDays(1);
			log.info("Today Start Date "+l_todaystartdatetime+" End Time "+l_todayenddatetime);
			log.info("Month Start Date "+l_mtdstartdatetime+" End Time "+l_mtdenddatetime);
			log.info("Prev Month Start Date "+l_prevmonthstartdatetime+" End Time "+l_prevmonthenddatetime);
		} else {
			l_todaystartdatetime		=	LocalDateTime.of(
												l_currentdatetime.getYear()
											, 	l_currentdatetime.getMonthValue()
											, 	l_currentdatetime.getDayOfMonth()
											, 	00
											,	00
											);
			l_todayenddatetime			=	LocalDateTime.of(
												l_currentdatetime.getYear()
											, 	l_currentdatetime.getMonthValue()
											, 	l_currentdatetime.getDayOfMonth()
											, 	23
											,	59
											,	59
											);
			l_mtdstartdatetime			=	l_todaystartdatetime.minusMonths(1);
			l_mtdstartdatetime			=	LocalDateTime.of(
												l_mtdstartdatetime.getYear()
											, 	l_mtdstartdatetime.getMonthValue()
											, 	p_projstartday
											, 	00
											,	00
											);
			l_mtdenddatetime			=	LocalDateTime.of(
												l_currentdatetime.getYear()
											, 	l_currentdatetime.getMonthValue()
											, 	p_projstartday-1
											, 	23
											,	59
											,	59
											);
			l_prevmonthstartdatetime	=	l_mtdstartdatetime.minusMonths(1);
			l_prevmonthenddatetime		=	l_mtdenddatetime.minusMonths(1);
			log.info("Today Start Date "+l_todaystartdatetime+" End Time "+l_todayenddatetime);
			log.info("Month Start Date "+l_mtdstartdatetime+" End Time "+l_mtdenddatetime);
			log.info("Prev Month Start Date "+l_prevmonthstartdatetime+" End Time "+l_prevmonthenddatetime);
		}
		l_datetime	=	new LocalDateTime[6];
		l_datetime[0]	=	l_todaystartdatetime;
		l_datetime[1]	=	l_todayenddatetime;
		l_datetime[2]	=	l_mtdstartdatetime;
		l_datetime[3]	=	l_mtdenddatetime;
		l_datetime[4]	=	l_prevmonthstartdatetime;
		l_datetime[5]	=	l_prevmonthenddatetime;
		
		return l_datetime;
	}
	
	static Map<String, Map<String, PvcpCaseMaster>> saveCaseMasterFields(
		UserSession o_usersession
	, 	DataTable  o_datatable
	, 	int n_projectId
	, DbConnection connection
	) throws SciException
	{
		int i = 0;
		boolean b_casematch = false;
		List<PvcpCaseMaster> l_casemaserdblist =null;
		List<PvcpCaseMaster> l_casemaserlist =listcasemasterrecords(o_usersession,o_datatable,  n_projectId);
		Map<String,PvcpCaseMaster > m_existingcases = null;
		Map<String,PvcpCaseMaster > m_similarcases = null;;
		
		Map<String, Map<String, PvcpCaseMaster>> l_exstingrecordcases = null;
		
		String s_dbrecord = null;
		String s_userrecord = null;
		String checksumdbrecord=null;
		String checksumuserrecord=null;
		
		l_casemaserdblist= getCaseMasterFieldsList(connection, n_projectId);
		l_exstingrecordcases =new HashMap<String, Map<String, PvcpCaseMaster>>();
		m_existingcases = new HashMap<String,PvcpCaseMaster>();
		m_similarcases = new HashMap<String,PvcpCaseMaster>();
		
		String checkModDatadb = null;
		String checkModDatauser = null;
		
		String dbUdf = null;
		String newUdf = null;
		
		for(PvcpCaseMaster casemaster : l_casemaserlist)
		{
			b_casematch = false;
			s_userrecord = casemaster.getProjectSeqId()+casemaster.getInternalCaseId();
			checksumuserrecord= generateCheckSum(s_userrecord);
			if(!l_casemaserdblist.isEmpty())
			{
				for(PvcpCaseMaster casemasterdb : l_casemaserdblist)
				{
					s_dbrecord = casemasterdb.getProjectSeqId()+casemasterdb.getInternalCaseId();
					checksumdbrecord=generateCheckSum(s_dbrecord);
					if(checksumuserrecord.equals(checksumdbrecord))
					{
						//Insert Get IRD field
						checkModDatadb= generateCheckSum(casemasterdb.getCaseSeriousness()+StringUtils.emptyString(casemasterdb.getCaseType())+StringUtils.emptyString(casemasterdb.getProduct())+casemasterdb.getInitialRecvDate().getTime()+casemasterdb.getPcmDeUserSeqId());
						checkModDatauser = generateCheckSum(casemaster.getCaseSeriousness()+StringUtils.emptyString(casemaster.getCaseType())+StringUtils.emptyString(casemaster.getProduct())+casemaster.getInitialRecvDate().getTime()+casemaster.getPcmDeUserSeqId());
						
						dbUdf = generateCheckSum(
								StringUtils.emptyString(casemasterdb.getUdf1()) + StringUtils.emptyString(casemasterdb.getUdf2()) +
								StringUtils.emptyString(casemasterdb.getUdf3()) + StringUtils.emptyString(casemasterdb.getUdf4()) +
								StringUtils.emptyString(casemasterdb.getUdf5()) + StringUtils.emptyString(casemasterdb.getUdf6()) +
								StringUtils.emptyString(casemasterdb.getUdf7()) + StringUtils.emptyString(casemasterdb.getUdf8()) +
								StringUtils.emptyString(casemasterdb.getUdf9()) + StringUtils.emptyString(casemasterdb.getUdf10()));
						
						newUdf = generateCheckSum(
								StringUtils.emptyString(casemaster.getUdf1())+StringUtils.emptyString(casemaster.getUdf2())+
								StringUtils.emptyString(casemaster.getUdf3())+StringUtils.emptyString(casemaster.getUdf4())+
								StringUtils.emptyString(casemaster.getUdf5())+StringUtils.emptyString(casemaster.getUdf6())+
								StringUtils.emptyString(casemaster.getUdf7())+StringUtils.emptyString(casemaster.getUdf8())+
								StringUtils.emptyString(casemaster.getUdf9())+StringUtils.emptyString(casemaster.getUdf10()));
						if(!checkModDatadb.equals(checkModDatauser))
						{
							m_existingcases.put("ExistingRecord_"+i, casemasterdb);
							m_similarcases.put("UserRecord_"+i,casemaster);
							i++;
						}
						else if(!dbUdf.equals(newUdf))
						{
							updateCasemasterNamesRecord(casemasterdb, casemaster, o_usersession, n_projectId, connection);
						}
						b_casematch = true;
					}
				}
				if(!b_casematch)
				{
					savecasemasterrecord(casemaster, o_usersession, n_projectId, connection);
				}
			}
			else
			{
				savecasemasterrecord(casemaster, o_usersession, n_projectId, connection);
			}
		}
		l_exstingrecordcases.put("dbrecord", m_existingcases);
		l_exstingrecordcases.put("userrecord", m_similarcases);
		
		return l_exstingrecordcases;
	}
	
	static Map<String, Map<String, PvcpCaseMaster>> saveCaseMasterrecordmanualy(
		UserSession o_usersession
	, 	PvcpCaseMaster  o_casemaster
	, 	int n_projectId
	, DbConnection connection
	) throws SciException
	{

			int i = 0;
			boolean b_casematch = false;
			List<PvcpCaseMaster> l_casemaserdblist =null;
			Map<String,PvcpCaseMaster > m_existingcases = null;
			Map<String,PvcpCaseMaster > m_similarcases = null;;
			
			Map<String, Map<String, PvcpCaseMaster>> l_exstingrecordcases = null;
			
			String s_dbrecord = null;
			String s_userrecord = null;
			String checksumdbrecord=null;
			String checksumuserrecord=null;
			
			l_casemaserdblist= getCaseMasterFieldsList(connection, n_projectId);
			l_exstingrecordcases =new HashMap<String, Map<String, PvcpCaseMaster>>();
			m_existingcases = new HashMap<String,PvcpCaseMaster>();
			m_similarcases = new HashMap<String,PvcpCaseMaster>();
			
			String checkModDatadb = null;
			String checkModDatauser = null;
			
			String dbUdf = null;
			String newUdf = null;
			
			s_userrecord = o_casemaster.getProjectSeqId()+o_casemaster.getInternalCaseId();
				checksumuserrecord= generateCheckSum(s_userrecord);
				if(!l_casemaserdblist.isEmpty())
				{
					for(PvcpCaseMaster casemasterdb : l_casemaserdblist)
					{
						s_dbrecord = casemasterdb.getProjectSeqId()+casemasterdb.getInternalCaseId();
						checksumdbrecord=generateCheckSum(s_dbrecord);
						if(checksumuserrecord.equals(checksumdbrecord))
						{
							//Insert Get IRD field
							checkModDatadb= generateCheckSum(casemasterdb.getCaseSeriousness()+StringUtils.emptyString(casemasterdb.getCaseType())+StringUtils.emptyString(casemasterdb.getProduct())+casemasterdb.getInitialRecvDate().getTime());
							checkModDatauser = generateCheckSum(o_casemaster.getCaseSeriousness()+StringUtils.emptyString(o_casemaster.getCaseType())+StringUtils.emptyString(o_casemaster.getProduct())+o_casemaster.getInitialRecvDate().getTime());
							
							dbUdf = generateCheckSum(
									StringUtils.emptyString(casemasterdb.getUdf1()) + StringUtils.emptyString(casemasterdb.getUdf2()) +
									StringUtils.emptyString(casemasterdb.getUdf3()) + StringUtils.emptyString(casemasterdb.getUdf4()) +
									StringUtils.emptyString(casemasterdb.getUdf5()) + StringUtils.emptyString(casemasterdb.getUdf6()) +
									StringUtils.emptyString(casemasterdb.getUdf7()) + StringUtils.emptyString(casemasterdb.getUdf8()) +
									StringUtils.emptyString(casemasterdb.getUdf9()) + StringUtils.emptyString(casemasterdb.getUdf10()));
							
							newUdf = generateCheckSum(
									StringUtils.emptyString(o_casemaster.getUdf1())+StringUtils.emptyString(o_casemaster.getUdf2())+
									StringUtils.emptyString(o_casemaster.getUdf3())+StringUtils.emptyString(o_casemaster.getUdf4())+
									StringUtils.emptyString(o_casemaster.getUdf5())+StringUtils.emptyString(o_casemaster.getUdf6())+
									StringUtils.emptyString(o_casemaster.getUdf7())+StringUtils.emptyString(o_casemaster.getUdf8())+
									StringUtils.emptyString(o_casemaster.getUdf9())+StringUtils.emptyString(o_casemaster.getUdf10()));
							
							if(!checkModDatadb.equals(checkModDatauser))
							{
								m_existingcases.put("ExistingRecord_"+i, casemasterdb);
								m_similarcases.put("UserRecord_"+i,o_casemaster);
								i++;
							}
							else if(!dbUdf.equals(newUdf))
							{
								updateCasemasterNamesRecord(casemasterdb, o_casemaster, o_usersession, n_projectId, connection);
							}
							b_casematch = true;
						}
					}
					if(!b_casematch)
					{
						savecasemasterrecord(o_casemaster, o_usersession, n_projectId, connection);
					}
				}
				else
				{
					savecasemasterrecord(o_casemaster, o_usersession, n_projectId, connection);
				}
				
			
			l_exstingrecordcases.put("dbrecord", m_existingcases);
			l_exstingrecordcases.put("userrecord", m_similarcases);
			
			return l_exstingrecordcases;
		}
	
	/**
	 * 
	 * @param o_casemasterdb
	 * @param o_casemaster
	 * @param o_usersession
	 * @param n_projectId
	 * @param connection
	 * @return
	 * @throws SciException
	 */
	static boolean updateCasemasterNamesRecord(PvcpCaseMaster o_casemasterdb, PvcpCaseMaster o_casemaster,UserSession o_usersession,  int n_projectId,DbConnection connection)throws SciException
	{
		boolean b_dbststus = false;
		AuditInfoDb oAuditInfo = null;
		DbQueryUpdate l_updatequery = null;
		String	l_completequery	=	null;
		try {
			connection.startTransaction();
			l_completequery	=	String.format(UPDATE_UDF_DETAILS_BY_INTERNALID, DbQueryUtils.getQualifiedObjectName(PvcpCaseMasterDbSchema.PCVP_CASEMASTER.name()));
			l_updatequery	=	DbQueryUtils.createUpdateQuery(l_completequery);
			
			l_updatequery.addUpdateParameter(o_casemaster.getUdf1());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf2());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf3());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf4());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf5());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf6());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf7());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf8());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf9());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf10());
			l_updatequery.addWhereClauseParameter(n_projectId);
			l_updatequery.addWhereClauseParameter(o_casemaster.getInternalCaseId());
			connection.executeQuery(l_updatequery);
			oAuditInfo	=	auditCaseMasterData(o_usersession, o_casemaster, o_casemasterdb);
			AuditHome.savePopulatedStub(connection, oAuditInfo);
			b_dbststus=true;		
			connection.commitTransaction();
		}
		catch(SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating casedata", dbExcep);
			throw new SciException("Error updating casedata", dbExcep);
		} 
		catch (SciException sciExcep) {
			connection.rollbackTransaction();
			log.error("Error updating casedata", sciExcep);
			throw sciExcep;
		}
		return b_dbststus;
	}
	
	/**
	 * 
	 * @param connection
	 * @param o_casemaster
	 * @param o_usersession
	 * @param n_projectId
	 * @return
	 * @throws SciException
	 */
	static boolean updateCaseMasterRecord(DbConnection connection, PvcpCaseMaster o_casemaster,UserSession o_usersession,  int n_projectId)throws SciException
	{
		boolean 		b_dbststus 		=	false;
		AuditInfoDb 	oAuditInfo 		=	null;
		DbQueryUpdate 	l_updatequery 	=	null;
		String			l_completequery	=	null;
		
		try {
			connection.startTransaction();
			PvcpCaseMaster o_casemasterdb = getPVCPCaseMasterFieldsByInternamCaseId(connection, o_casemaster.getInternalCaseId(), n_projectId);
			l_completequery	=	String.format(UPDATE_CASE_DETAILS_BY_INTERNALID, DbQueryUtils.getQualifiedObjectName(PvcpCaseMasterDbSchema.PCVP_CASEMASTER.name()));
			l_updatequery	=	DbQueryUtils.createUpdateQuery(l_completequery);
			l_updatequery.addUpdateParameter(o_casemaster.getPcmDeSrc());
			l_updatequery.addUpdateParameter(o_casemaster.getPcmDeSrcType());
			l_updatequery.addUpdateParameter(o_casemaster.getPcmDeUserSeqId());
			l_updatequery.addUpdateParameter(o_casemaster.getPcmDeUserName());
			l_updatequery.addUpdateParameter(o_casemaster.getInitialRecvDate());
			l_updatequery.addUpdateParameter(o_casemaster.getCaseSeriousness());
			l_updatequery.addUpdateParameter(o_casemaster.getCaseType());
			l_updatequery.addUpdateParameter(o_casemaster.getProduct());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf1());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf2());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf3());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf4());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf5());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf6());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf7());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf8());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf9());
			l_updatequery.addUpdateParameter(o_casemaster.getUdf10());
			l_updatequery.addUpdateParameter(o_casemaster.getReviewType1UserSeqId());
			l_updatequery.addUpdateParameter(o_casemaster.getReviewType2UserSeqId());
			l_updatequery.addUpdateParameter(o_casemaster.getReviewType3UserSeqId());
			l_updatequery.addUpdateParameter(o_casemaster.getReviewType4UserSeqId());
			l_updatequery.addUpdateParameter(o_casemaster.getReviewType5UserSeqId());
			l_updatequery.addWhereClauseParameter(n_projectId);
			l_updatequery.addWhereClauseParameter(o_casemaster.getInternalCaseId());
			
			connection.executeQuery(l_updatequery);
			
			//Move to QR portal app
			oAuditInfo	=	auditCaseMasterData(o_usersession, o_casemaster, o_casemasterdb);
			
			AuditHome.savePopulatedStub(connection, oAuditInfo);
			b_dbststus=true;		
			connection.commitTransaction();
		}
		catch(SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error updating casedata", dbExcep);
			throw new SciException("Error updating casedata", dbExcep);
		}
		catch (SciException sciExcep) 
		{
			connection.rollbackTransaction();
			log.error("Error updating casedata", sciExcep);
			throw sciExcep;
		}
		return b_dbststus;
	}
	
	static AuditInfoDb auditCaseMasterData(
		UserSession 	p_usersession
	,	PvcpCaseMaster 	p_casemaster
	,	PvcpCaseMaster	p_casemasterdb
	) {
		AuditInfoDb			oAuditInfo	=	null;
		SimpleDateFormat 	formatDate = new SimpleDateFormat("dd-MMM-yyyy");
		oAuditInfo = AuditHome.createRecordUpdateStub(p_usersession, QRConstant.APP_NAME, "PVCP CASE MASTER UPDATED", p_casemaster.getProjectSeqId());
		
		oAuditInfo.addPayload(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_SEQ_ID,p_casemasterdb.getSeqId());
		oAuditInfo.addPayload(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_ID, p_casemaster.getCaseId());
		oAuditInfo.addPayload(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_VERSION, p_casemaster.getCaseVersion());
		if(p_casemasterdb.getPcmDeUserSeqId() !=  p_casemaster.getPcmDeUserSeqId())
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.DE_SEQ_ID, p_casemasterdb.getPcmDeUserSeqId(), p_casemaster.getPcmDeUserSeqId());
		}
		if(p_casemasterdb.getInitialRecvDate().getTime()!= p_casemaster.getInitialRecvDate().getTime())
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_IRD, formatDate.format(p_casemasterdb.getInitialRecvDate()),  formatDate.format(p_casemaster.getInitialRecvDate()));
		}
		if(!StringUtils.emptyString(p_casemasterdb.getCaseSeriousness()).equals( p_casemaster.getCaseSeriousness()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_SERIOUSNESS, p_casemasterdb.getCaseSeriousness(), p_casemaster.getCaseSeriousness());
		}
		if(!StringUtils.emptyString(p_casemasterdb.getCaseType()).equals( p_casemaster.getCaseType()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_TYPE, p_casemasterdb.getCaseType(), p_casemaster.getCaseType());
		}
		if(!StringUtils.emptyString(p_casemasterdb.getProduct()).equals( p_casemaster.getProduct()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.PRODUCT, p_casemasterdb.getProduct(), p_casemaster.getProduct());
		}
		if(p_casemasterdb.getReviewType1UserSeqId() != p_casemaster.getReviewType1UserSeqId())
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.REVIEW_TYPE_1, p_casemasterdb.getReviewType1UserSeqId(), p_casemaster.getReviewType1UserSeqId());
		}
		if(p_casemasterdb.getReviewType2UserSeqId() != p_casemaster.getReviewType2UserSeqId())
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.REVIEW_TYPE_2, p_casemasterdb.getReviewType2UserSeqId(), p_casemaster.getReviewType2UserSeqId());
		}
		if(p_casemasterdb.getReviewType3UserSeqId() != p_casemaster.getReviewType3UserSeqId())
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.REVIEW_TYPE_3, p_casemasterdb.getReviewType3UserSeqId(), p_casemaster.getReviewType3UserSeqId());
		}
		if(p_casemasterdb.getReviewType4UserSeqId() != p_casemaster.getReviewType4UserSeqId())
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.REVIEW_TYPE_4, p_casemasterdb.getReviewType4UserSeqId(), p_casemaster.getReviewType4UserSeqId());
		}
		if(p_casemasterdb.getReviewType5UserSeqId() != p_casemaster.getReviewType5UserSeqId())
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.REVIEW_TYPE_5, p_casemasterdb.getReviewType5UserSeqId(), p_casemaster.getReviewType5UserSeqId());
		}
		if(p_casemasterdb.getUdf1() != null	&&	!p_casemasterdb.getUdf1().equals( p_casemaster.getUdf1()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_1, p_casemasterdb.getUdf1(), p_casemaster.getUdf1());
		}
		if(p_casemasterdb.getUdf2() != null && !p_casemasterdb.getUdf2().equals( p_casemaster.getUdf2()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_2, p_casemasterdb.getUdf2(), p_casemaster.getUdf2());
		}
		if(p_casemasterdb.getUdf3() != null && !p_casemasterdb.getUdf3().equals( p_casemaster.getUdf3()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_3, p_casemasterdb.getUdf3(), p_casemaster.getUdf3());
		}
		if(p_casemasterdb.getUdf4() != null && p_casemasterdb.getUdf4() != null && !p_casemasterdb.getUdf4().equals( p_casemaster.getUdf4()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_4, p_casemasterdb.getUdf4(), p_casemaster.getUdf4());
		}
		if(p_casemasterdb.getUdf5() != null && !p_casemasterdb.getUdf5().equals( p_casemaster.getUdf5()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_5, p_casemasterdb.getUdf5(), p_casemaster.getUdf5());
		}
		if(p_casemasterdb.getUdf6() != null && !p_casemasterdb.getUdf6().equals( p_casemaster.getUdf6()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_6, p_casemasterdb.getUdf6(), p_casemaster.getUdf6());
		}
		if(p_casemasterdb.getUdf7() != null && !p_casemasterdb.getUdf7().equals( p_casemaster.getUdf7()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_7, p_casemasterdb.getUdf7(), p_casemaster.getUdf7());
		}
		if(p_casemasterdb.getUdf8() != null && !p_casemasterdb.getUdf8().equals( p_casemaster.getUdf8()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_8, p_casemasterdb.getUdf8(), p_casemaster.getUdf8());
		}
		if(p_casemasterdb.getUdf9() != null && !p_casemasterdb.getUdf9().equals( p_casemaster.getUdf9()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_9, p_casemasterdb.getUdf9(), p_casemaster.getUdf9());
		}
		if(p_casemasterdb.getUdf10() != null && !p_casemasterdb.getUdf10().equals( p_casemaster.getUdf10()))
		{
			oAuditInfo.addDetails(PvcpCaseMasterAuditHelper.AuditAttributes.UDF_10, p_casemasterdb.getUdf10(), p_casemaster.getUdf10());
		}
		return oAuditInfo;
	}
	
	static boolean savecasemasterrecord(PvcpCaseMaster o_casemaster,UserSession o_usersession,  int n_projectId,DbConnection connection)throws SciException
	{
		boolean b_dbststus = false;
		AuditInfoDb oAuditInfo = null;
		QRPortalApp o_Qrportalaapp = null;
		String	l_datettimeblank	=	null;
		
		o_Qrportalaapp = new QRPortalApp();
		
		try {
			connection.startTransaction();
			DbQueryInsert oInsertQuery = null;
			int n_caseSeqId = DbQueryHome.generateSequenceValueForTablePk(PvcpCaseMasterDbSchema.PCVP_CASEMASTER);
			
					oInsertQuery = DbQueryUtils.createInsertQuery(PvcpCaseMasterDbSchema.PCVP_CASEMASTER);
					
					oInsertQuery.addInsertParameter(n_caseSeqId);
					oInsertQuery.addInsertParameter(o_casemaster.getProjectSeqId());
					oInsertQuery.addInsertParameter(o_casemaster.getCaseId());
					oInsertQuery.addInsertParameter(o_casemaster.getCaseVersion());
					if (o_casemaster.getInitialRecvDate() != null) {
						oInsertQuery.addInsertParameter(o_casemaster.getInitialRecvDate());
					} else {
						oInsertQuery.addInsertParameter(l_datettimeblank);
					}
					oInsertQuery.addInsertParameter(o_casemaster.getCaseType());
					oInsertQuery.addInsertParameter(o_casemaster.getProduct());
					oInsertQuery.addInsertParameter(o_casemaster.getCaseSeriousness());
						
					oInsertQuery.addInsertParameter(o_casemaster.getPcmDeSrc());
					oInsertQuery.addInsertParameter(o_casemaster.getPcmDeSrcType());
					oInsertQuery.addInsertParameter(o_casemaster.getPcmDeUserSeqId());
					oInsertQuery.addInsertParameter(o_casemaster.getPcmDeUserName());
						
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType1Src());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType1SrcType());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType1UserSeqId());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType1UserName());
						
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType2Src());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType2SrcType());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType2UserSeqId());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType2UserName());
						
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType3Src());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType3SrcType());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType3UserSeqId());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType3UserName());
						
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType4Src());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType4SrcType());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType4UserSeqId());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType4UserName());
					
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType5Src());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType5SrcType());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType5UserSeqId());
					oInsertQuery.addInsertParameter(o_casemaster.getReviewType5UserName());
						
					oInsertQuery.addInsertParameter(o_casemaster.getUdf1());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf2());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf3());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf4());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf5());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf6());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf7());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf8());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf9());
					oInsertQuery.addInsertParameter(o_casemaster.getUdf10());
						
					oInsertQuery.addInsertParameter(o_casemaster.getInternalCaseId());
					oInsertQuery.addInsertParameter(DbQueryParam.SPECIAL_PARAMETER_DATETIME_NOW);
					oInsertQuery.addInsertParameter(o_casemaster.getCreatedBy());
						
					connection.executeQuery(oInsertQuery);
					oAuditInfo = AuditHome.createRecordInsertStub(o_usersession, o_Qrportalaapp.getPortalAppId(), " PVCP CASE MASTER ID CREATED", o_casemaster.getProjectSeqId());
					
					oAuditInfo.addPayload(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_SEQ_ID,n_caseSeqId);
					oAuditInfo.addPayload(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_ID, o_casemaster.getCaseId());
					oAuditInfo.addPayload(PvcpCaseMasterAuditHelper.AuditAttributes.CASE_VERSION, o_casemaster.getCaseVersion());
			AuditHome.savePopulatedStub(connection, oAuditInfo);
			b_dbststus=true;		
			connection.commitTransaction();
		}
		catch(SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving casedata", dbExcep);
			throw new SciException("Error saving casedata", dbExcep);
		} 
		catch (SciException sciExcep)
		{
			connection.rollbackTransaction();
			log.error("Error saving casedata", sciExcep);
			throw sciExcep;
		}
		return b_dbststus;
	}
	
	static  List<PvcpCaseMaster> listcasemasterrecords(UserSession o_usersession,DataTable o_datatable, int n_projectId) throws SciException
	{
		String s_caseid = null;
		String s_version = null;
		String s_IrdDate = null;
		PvcpCaseMaster o_casemaster= null;
		List<PvcpCaseMaster> l_casemasterlist= null;
		
			l_casemasterlist= new ArrayList<PvcpCaseMaster>();
			for(int i = 0; i< o_datatable.rowCount(); i++) 
			{
				s_caseid = null;
				s_version = null;
				s_IrdDate = null;
				o_casemaster = new PvcpCaseMaster();
				
				o_casemaster.setProjectSeqId(n_projectId);
				
				s_caseid = (String)o_datatable.getColumnValues("CASEID").get(i);
				o_casemaster.setCaseId(s_caseid);
				s_version = (String)o_datatable.getColumnValues("VERSION").get(i);
				o_casemaster.setCaseVersion(s_version);
				
				s_IrdDate=(String)o_datatable.getColumnValues("IRD").get(i);
				
				Date d_IrdDate =null;
				if(s_IrdDate != null){
					if(s_IrdDate != "")
					{
						try {
							d_IrdDate =new SimpleDateFormat("dd-MMM-yyyy").parse(s_IrdDate);
							}
						catch(ParseException prex)
						{
							log.error("Error in IRD date parsing");
							throw new SciException("Error in IRD date parsing");
						}
					}
				}
				
				o_casemaster.setInitialRecvDate(d_IrdDate);
				
				o_casemaster.setCaseType((String)o_datatable.getColumnValues("CASETYPE").get(i));
				
				o_casemaster.setProduct((String)o_datatable.getColumnValues("PRODUCT").get(i));
				
				o_casemaster.setCaseSeriousness((String)o_datatable.getColumnValues("CASESERIOUSNESS").get(i));
				
				o_casemaster.setReviewType1Src(0);	
				o_casemaster.setReviewType1SrcType(0);
				if ((String)o_datatable.getColumnValues("DENAME").get(i) != null) {
					o_casemaster.setPcmDeUserSeqId(Integer.parseInt((String)o_datatable.getColumnValues("DENAME").get(i)));
				} else {
					o_casemaster.setPcmDeUserSeqId(-1);
				}
				
				if (o_datatable.isColumnPresent("UDF1")) {
					o_casemaster.setUdf1((String)o_datatable.getColumnValues("UDF1").get(i));
				}
				
				if (o_datatable.isColumnPresent("UDF2")) {
					o_casemaster.setUdf2((String)o_datatable.getColumnValues("UDF2").get(i));
				}
				if (o_datatable.isColumnPresent("UDF3")) {
					o_casemaster.setUdf3((String)o_datatable.getColumnValues("UDF3").get(i));
				}
				if (o_datatable.isColumnPresent("UDF4")) {
					o_casemaster.setUdf4((String)o_datatable.getColumnValues("UDF4").get(i));
				}
				if (o_datatable.isColumnPresent("UDF5")) {
					o_casemaster.setUdf5((String)o_datatable.getColumnValues("UDF5").get(i));
				}
				if (o_datatable.isColumnPresent("UDF6")) {
					o_casemaster.setUdf6((String)o_datatable.getColumnValues("UDF6").get(i));
				}
				if (o_datatable.isColumnPresent("UDF7")) {
					o_casemaster.setUdf7((String)o_datatable.getColumnValues("UDF7").get(i));
				}
				if (o_datatable.isColumnPresent("UDF8")) {
					o_casemaster.setUdf8((String)o_datatable.getColumnValues("UDF8").get(i));
				}
				if (o_datatable.isColumnPresent("UDF9")) {
					o_casemaster.setUdf9((String)o_datatable.getColumnValues("UDF9").get(i));
				}
				if (o_datatable.isColumnPresent("UDF10")) {
					o_casemaster.setUdf10((String)o_datatable.getColumnValues("UDF10").get(i));
				}
				
				o_casemaster.setInternalCaseId(s_caseid+"_"+s_version);
				o_casemaster.setCreatedBy(o_usersession.getUserInfo().getUserSeqId());
				l_casemasterlist.add(o_casemaster);
			}
			
			return l_casemasterlist;
	}
	
	/**
	 * 
	 * @param connection
	 * @param l_projectid
	 * @return
	 * @throws SciException
	 */
	static List<PvcpCaseMaster> getCaseMasterFieldsList(DbConnection connection, int l_projectid) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		PvcpCaseMaster o_casemaster = null;
		List<PvcpCaseMaster> l_casemasterlist=null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(PvcpCaseMasterDbSchema.PCVP_CASEMASTER, PvcpCaseMasterDbSchema.PCVP_CASEMASTER.PRJSEQID.bindClause(), PvcpCaseMasterDbSchema.PCVP_CASEMASTER.PCMCASE_IRD.name()+" desc");
		oSelectQuery.addWhereClauseParameter(l_projectid);
		try{
			connection.startTransaction();
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				l_casemasterlist = new ArrayList<PvcpCaseMaster>();
				while (resultSet.next())
				{
					o_casemaster = readCaseMasterObjFromDb(resultSet);
					l_casemasterlist.add(o_casemaster);
				}				
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error loading project List - {}",  dbExcep);
			throw new SciException("Error loading project list", dbExcep);
		}
		
		return l_casemasterlist;
	}
	
	/**
	 * 
	 * @param connection
	 * @param n_caseseqid
	 * @return
	 * @throws SciException
	 */
	static PvcpCaseMaster getCaseMasterBySeqId(DbConnection connection, int n_caseseqid) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		PvcpCaseMaster o_casemaster = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(PvcpCaseMasterDbSchema.PCVP_CASEMASTER, PvcpCaseMasterDbSchema.PCVP_CASEMASTER.CASESEQID.bindClause(), PvcpCaseMasterDbSchema.PCVP_CASEMASTER.CASESEQID.name());
		oSelectQuery.addWhereClauseParameter(n_caseseqid);
		try{
			connection.startTransaction();
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					o_casemaster = readCaseMasterObjFromDb(resultSet);
				}				
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error loading Case master record",  dbExcep);
			throw new SciException("Error loading Case master record", dbExcep);
		}
		
		return o_casemaster;
	}
	
	private static PvcpCaseMaster readCaseMasterObjFromDb(DbResultSet resultSet) throws SciDbException
	{
		PvcpCaseMaster o_casemaster = null;
		CaseMasterDb o_casemasterdb = null;
		
		o_casemaster = new PvcpCaseMaster();
		o_casemasterdb = new CaseMasterDb();
		
		o_casemaster.setSeqId(resultSet.readInt(o_casemasterdb.CASESEQID));
		o_casemaster.setProjectSeqId(resultSet.readInt(o_casemasterdb.PRJSEQID));
		o_casemaster.setCaseId(resultSet.readString(o_casemasterdb.PCMCASEID));
		
		o_casemaster.setCaseVersion(resultSet.readString(o_casemasterdb.PCMCASEVERSION));
		
		o_casemaster.setInitialRecvDate(resultSet.readDate(o_casemasterdb.PCMCASE_IRD));
		o_casemaster.setCaseType(resultSet.readString(o_casemasterdb.PCMCASETYPE));
		o_casemaster.setProduct(resultSet.readString(o_casemasterdb.PCMCASE_PRODUCT));
		o_casemaster.setCaseSeriousness(resultSet.readString(o_casemasterdb.PCMCASESERIOUSNESS));
		
		o_casemaster.setPcmDeSrc(resultSet.readInt(o_casemasterdb.PCMDESRC));
		o_casemaster.setPcmcDeSrcType(resultSet.readInt(o_casemasterdb.PCMDETYPE));
		o_casemaster.setPcmDeUserSeqId(resultSet.readInt(o_casemasterdb.PCMDEUSERSEQID));
		o_casemaster.setPcmcDeUserName(resultSet.readString(o_casemasterdb.PCMDEUSERNAME));
		
		
		o_casemaster.setReviewType1UserName(resultSet.readString(o_casemasterdb.PCMRT1USERNAME));
		o_casemaster.setReviewType1Src(resultSet.readInt(o_casemasterdb.PCMRT1SRC));
		o_casemaster.setReviewType1SrcType(resultSet.readInt(o_casemasterdb.PCMRT1TYPE));
		o_casemaster.setReviewType1UserSeqId(resultSet.readInt(o_casemasterdb.PCMRT1USERSEQID));
		
		o_casemaster.setReviewType2UserName(resultSet.readString(o_casemasterdb.PCMRT2USERNAME));
		o_casemaster.setReviewType2Src(resultSet.readInt(o_casemasterdb.PCMRT2SRC));
		o_casemaster.setReviewType2SrcType(resultSet.readInt(o_casemasterdb.PCMRT2TYPE));
		o_casemaster.setReviewType2UserSeqId(resultSet.readInt(o_casemasterdb.PCMRT2USERSEQID));
		
		o_casemaster.setReviewType3UserName(resultSet.readString(o_casemasterdb.PCMRT3USERNAME));
		o_casemaster.setReviewType3Src(resultSet.readInt(o_casemasterdb.PCMRT3SRC));
		o_casemaster.setReviewType3SrcType(resultSet.readInt(o_casemasterdb.PCMRT3TYPE));
		o_casemaster.setReviewType3UserSeqId(resultSet.readInt(o_casemasterdb.PCMRT3USERSEQID));
		
		o_casemaster.setReviewType4UserName(resultSet.readString(o_casemasterdb.PCMRT4USERNAME));
		o_casemaster.setReviewType4Src(resultSet.readInt(o_casemasterdb.PCMRT4SRC));
		o_casemaster.setReviewType4SrcType(resultSet.readInt(o_casemasterdb.PCMRT4TYPE));
		o_casemaster.setReviewType4UserSeqId(resultSet.readInt(o_casemasterdb.PCMRT4USERSEQID));
		
		o_casemaster.setReviewType5UserName(resultSet.readString(o_casemasterdb.PCMRT5USERNAME));
		o_casemaster.setReviewType5Src(resultSet.readInt(o_casemasterdb.PCMRT5SRC));
		o_casemaster.setReviewType5SrcType(resultSet.readInt(o_casemasterdb.PCMRT5TYPE));
		o_casemaster.setReviewType5UserSeqId(resultSet.readInt(o_casemasterdb.PCMRT5USERSEQID));
		
		o_casemaster.setUdf1(resultSet.readString(o_casemasterdb.PCMUDF1));
		o_casemaster.setUdf2(resultSet.readString(o_casemasterdb.PCMUDF2));
		o_casemaster.setUdf3(resultSet.readString(o_casemasterdb.PCMUDF3));
		o_casemaster.setUdf4(resultSet.readString(o_casemasterdb.PCMUDF4));
		o_casemaster.setUdf5(resultSet.readString(o_casemasterdb.PCMUDF5));
		o_casemaster.setUdf6(resultSet.readString(o_casemasterdb.PCMUDF6));
		o_casemaster.setUdf7(resultSet.readString(o_casemasterdb.PCMUDF7));
		o_casemaster.setUdf8(resultSet.readString(o_casemasterdb.PCMUDF8));
		o_casemaster.setUdf9(resultSet.readString(o_casemasterdb.PCMUDF9));
		o_casemaster.setUdf10(resultSet.readString(o_casemasterdb.PCMUDF10));

		o_casemaster.setInternalCaseId(resultSet.readString(o_casemasterdb.PCMINTERNALCASEID));
		o_casemaster.setCreatedBy(resultSet.readInt(o_casemasterdb.USRCREATED));
		
		o_casemaster.setCreatedOn(resultSet.readDate(o_casemasterdb.DTCREATED));
		
		return o_casemaster;
	}
	
	/**
	 * 
	 * @param s_string
	 * @return
	 * @throws SciException
	 */
	static String generateCheckSum(String s_string) throws SciException
	{
		MessageDigest o_messagedigest = null;

		try {
			o_messagedigest = MessageDigest.getInstance("MD5");
			o_messagedigest.update(s_string.toLowerCase().getBytes());
	
	        byte byteData[] = o_messagedigest.digest();
	        return DatatypeConverter.printHexBinary(byteData);
	    }
	    catch(NoSuchAlgorithmException chexp)
	    {
		  log.error("Error generating checksum", chexp);
		  throw new SciException("Error generating checksum");
	    }
    }
	
	/**
	 * 
	 * @param connection
	 * @param s_internalcaseid
	 * @param n_projectid
	 * @return
	 * @throws SciException
	 */
	static PvcpCaseMaster getPVCPCaseMasterFieldsByInternamCaseId(DbConnection connection, String s_internalcaseid, int n_projectid) throws SciException
	{
		DbQuerySelect oSelectQuery = null;
		PvcpCaseMaster o_casemaster = null;
		String			l_completequery	=	null;
		
		l_completequery	=	String.format(GET_CASE_DETAILS_BY_INTERNALID, DbQueryUtils.getQualifiedObjectName(PvcpCaseMasterDbSchema.PCVP_CASEMASTER.name()));
		oSelectQuery	=	DbQueryUtils.createSelectQuery(l_completequery);
		
		oSelectQuery.addWhereClauseParameter(n_projectid);
		oSelectQuery.addWhereClauseParameter(s_internalcaseid);
		try{
			connection.startTransaction();
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				while (resultSet.next())
				{
					o_casemaster = readCaseMasterObjFromDb(resultSet);
				}				
			}
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error loading project List - {}",  dbExcep);
			throw new SciException("Error loading project list", dbExcep);
		}
		
		return o_casemaster;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_casemaster
	 * @param p_reviewtype
	 * @return
	 * @throws SciServiceException
	 * @throws SciException
	 */
	static List<QRReviewData> getQRLReviewTypeRecordDetails(
		DbConnection 	p_connection
	, 	PvcpCaseMaster 	p_casemaster
	, 	String 			p_reviewtype
	) throws SciException {
		DbQuerySelect	l_selectquery 		=	null;
		List<QRReviewData>	l_qualityreviewdatalist =	null;
	
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								QRDbSchema.QUALITY_REVIEW_LOG
							, 	DbQueryUtils.bindWhereClauseWithAnd(
									QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.CASEID.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.CASEVERSION.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE.bindClause()
								)
							,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWROUND.name()
							);
		
		l_selectquery.addWhereClauseParameter(p_casemaster.getProjectSeqId());
		l_selectquery.addWhereClauseParameter(1);
		l_selectquery.addWhereClauseParameter(p_casemaster.getCaseId());
		l_selectquery.addWhereClauseParameter(p_casemaster.getCaseVersion());
		l_selectquery.addWhereClauseParameter(p_reviewtype);
		try{
			p_connection.startTransaction();
			try (DbResultSet resultSet = p_connection.executeQuery(l_selectquery)) {
				l_qualityreviewdatalist = new ArrayList<QRReviewData>();
				while (resultSet.next()) {
					l_qualityreviewdatalist.add(
						readQRLReviewData(
							p_connection	
						,	resultSet
						, 	BinaryContentHomeImpl.readContent(
							p_connection
						, 	resultSet.readInt(
								QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
							)
							)
						,	BinaryContentHomeImpl.readContent(
								p_connection
							, 	resultSet.readInt(
									QRDbSchema.QUALITY_REVIEW_LOG.CASEREVIEWSUMMSEQID
								)
							)
						)
					);
				}				
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error loading project List - {}",  dbExcep);
			throw new SciException("Error loading project List - {}",  dbExcep);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error loading project List - {}",  sciExcep);
			throw sciExcep;
		}
		return l_qualityreviewdatalist;
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_reporttype
	 * @param p_userinfo
	 * @param l_previousday
	 * @return
	 * @throws SciException
	 */
	static File getFullSummaryReport (
		DbConnection		p_connection
	,	String				p_reporttype	
	,	String				p_reviewtype
	, 	UserSession 		p_usersession
	,	int					p_startdate
	,	int					p_enddate
	,	int					p_projectid
	,	String				p_appid
	,	Map<String,String>	p_regclass
	) throws SciException {
		DbQuerySelect				l_selectquery			=	null;
		List<QRReviewData>			l_ilqrreviewdatalist	=	null;
		QRReviewData				l_qrlreviewdata			=	null;
		Calendar []					l_cldr					=	null;	
		File						l_exportfile			=	null;
		FileWriter					l_fwr					=	null;
		StringBuilder				l_fullcontent			=	null,
									l_tempdata				=	null,
									l_errordata				=	null,
									l_udfname				=	null;
		String						l_reportprop			=	null,
									l_cataliases			=	null;
		int							l_reporttype			=	0,
									l_catcount				=	0;
		SafetyDbCaseFieldsTemplate	l_activescft			=	null;
		boolean						l_isrecordexist			=	false;
		Map<String,List<String>>	l_enabledudfmap			=	null;
		try {
			p_connection.startTransaction();
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									QRDbSchema.QUALITY_REVIEW_LOG
								,	DbQueryUtils.bindWhereClauseWithAnd(	
										QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
									,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.bindClauseBetween()	
									)	
								,	QRDbSchema.QUALITY_REVIEW_LOG.DTCREATED.name()
								);
			
			l_selectquery.addWhereClauseParameter(p_projectid);
			l_selectquery.addWhereClauseParameter(p_reviewtype);
			l_selectquery.addWhereClauseParameter(1);
			l_cldr	=	getDate(p_startdate,p_enddate);
			l_selectquery.addWhereClauseParameter(l_cldr[0].getTime());
			l_selectquery.addWhereClauseParameter(l_cldr[1].getTime());
			
			try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
				if (l_rs != null) {
					l_ilqrreviewdatalist	=	new ArrayList<>();
					while (l_rs.next()) {
						l_isrecordexist	=	true;
						l_qrlreviewdata	=	readQRLReviewData(
												p_connection
											,	l_rs
											,	BinaryContentHomeImpl.readContent(
													p_connection
												, 	l_rs.readInt(
														QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
													)
												)
											,	BinaryContentHomeImpl.readContent(
													p_connection
												, 	l_rs.readInt(
														QRDbSchema.QUALITY_REVIEW_LOG.CASEREVIEWSUMMSEQID
													)
												)
											);
						l_ilqrreviewdatalist.add(l_qrlreviewdata);
					}
					if (!l_isrecordexist) {
						log.error("There are no review done for the passed review type "+p_reviewtype);
						throw new SciException(
							"There are no review done for the passed review type"
						);
					}
				}
			}
			try {
				l_activescft	=	SafetyDbConfigurationHelper.getActiveTemplateDetails(p_projectid);
			} catch (SciServiceException serExcep) {
				log.error("Active Safety DB does not exist");
				throw new SciException(
					"Error in fetching template details"
				,	serExcep
				);
			}
			try {
				if (l_ilqrreviewdatalist.size() > 0) {
					l_exportfile	=	new File(
											WebUtils.createStagingFile(
												p_usersession
											, 	FSR_REPORT_NAME_CONST
											)
										);
					l_fwr 			=	new FileWriter(l_exportfile);
					
					l_reportprop	=	ConfigHome.getPrjConfigSettings(
											QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_FSR_TYPE
										, 	p_projectid
										);
					l_fullcontent	=	new StringBuilder(l_activescft.getCaseIdAlias()+","+l_activescft.getCaseVersionAlias());
					if (StringUtils.isNullOrEmpty(l_reportprop)) {
						l_reporttype	=	1;
					} else {
						l_reporttype	=	Integer.parseInt(l_reportprop);
						
					}
					l_cataliases	=	getApplicableCategories(p_projectid);
					if (!StringUtils.isNullOrEmpty(l_cataliases)) {
						l_catcount		=	l_cataliases.split(StringConstants.TILDE).length;
						l_cataliases	=	l_cataliases.replaceAll(StringConstants.TILDE, StringConstants.COMMA);
					}
					l_enabledudfmap	=	getEnableUdfList(p_projectid);
					if (l_enabledudfmap != null && l_enabledudfmap.size()>0) {
						l_udfname	=	new StringBuilder();
						for (String l_name : l_enabledudfmap.keySet()) {
							l_udfname.append(StringConstants.COMMA);
							l_udfname.append(l_enabledudfmap.get(l_name).get(0));
						}
					}
					
					if (l_reporttype == 1 || l_reporttype > 6) {
						l_fullcontent.append(FSR_CSV_HEADER_1);
						if (l_udfname != null) {
							l_fullcontent.append(l_udfname);
						}
						if (l_catcount > 1) {
							l_fullcontent.append(StringConstants.COMMA);
							l_fullcontent.append(l_cataliases);
						}
						l_fullcontent.append(CASE_RESULT_CONST);
					} else if (l_reporttype == 2) {
						l_fullcontent.append(FSR_CSV_HEADER_2);
						if (l_udfname != null) {
							l_fullcontent.append(l_udfname);
						}
						l_fullcontent.append(CASE_RESULT_CONST);
						for (String l_tabname : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().keySet()) { 
							for (SafetyDbCaseFields l_tabdata : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().get(l_tabname)) {
								l_fullcontent.append(StringConstants.COMMA+l_tabdata.getCaseFieldName());
							}
						}	
					} else if (l_reporttype == 3)  {
						l_fullcontent.append(FSR_CSV_HEADER_1);
						if (l_udfname != null) {
							l_fullcontent.append(l_udfname);
						}
						if (l_catcount > 1) {
							l_fullcontent.append(StringConstants.COMMA);
							l_fullcontent.append(l_cataliases);
						}
						l_fullcontent.append(CASE_RESULT_CONST);
						for (String l_tabname : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().keySet()) { 
							for (SafetyDbCaseFields l_tabdata : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().get(l_tabname)) {
								l_fullcontent.append(StringConstants.COMMA+l_tabdata.getCaseFieldName());
							}
						}
					} else if (l_reporttype == 4) {
						l_fullcontent.append(FSR_CSV_HEADER_2);
						if (l_udfname != null) {
							l_fullcontent.append(l_udfname);
						}
						if (l_catcount > 1) {
							l_fullcontent.append(StringConstants.COMMA);
							l_fullcontent.append(l_cataliases);
						}
						l_fullcontent.append(CASE_RESULT_CONST);
						for (String l_tabname : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().keySet()) { 
							for (SafetyDbCaseFields l_tabdata : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().get(l_tabname)) {
								l_fullcontent.append(StringConstants.COMMA+l_tabdata.getCaseFieldName());
							}
						}	
					} else if (l_reporttype == 5) {
						l_fullcontent.append(FSR_CSV_HEADER_1);
						if (l_udfname != null) {
							l_fullcontent.append(l_udfname);
						}
						if (l_catcount > 1) {
							l_fullcontent.append(StringConstants.COMMA);
							l_fullcontent.append(l_cataliases);
						}
						l_fullcontent.append(CASE_RESULT_CONST);
						l_fullcontent.append(StringConstants.COMMA);
						l_fullcontent.append(TAB_NAME_CONST);
						l_fullcontent.append(FIELD_NAME_CONST);
						l_fullcontent.append(ERROR_DESC_CONST);
						
					} else if (l_reporttype == 6) {
						l_fullcontent.append(FSR_CSV_HEADER_2);
						if (l_udfname != null) {
							l_fullcontent.append(l_udfname);
						}
						if (l_catcount > 1) {
							l_fullcontent.append(StringConstants.COMMA);
							l_fullcontent.append(l_cataliases);
						}
						l_fullcontent.append(CASE_RESULT_CONST);
						for (String l_tabname : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().keySet()) { 
							for (SafetyDbCaseFields l_tabdata : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().get(l_tabname)) {
								l_fullcontent.append(StringConstants.COMMA+l_tabdata.getCaseFieldName());
							}
						}	
						l_fullcontent.append(StringConstants.COMMA);
						l_fullcontent.append(ERROR_DESC_CONST);
					}/* else if (l_reporttype == 7) {
						l_fullcontent.append(FSR_CSV_HEADER_3);
						if (l_udfname != null) {
							l_fullcontent.append(l_udfname);
						}
						if (l_catcount > 1) {
							l_fullcontent.append(StringConstants.COMMA);
							l_fullcontent.append(l_cataliases);
						}
						l_fullcontent.append(CASE_RESULT_CONST);
						for (String l_tabname : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().keySet()) { 
							for (SafetyDbCaseFields l_tabdata : l_ilqrreviewdatalist.get(0).getSafetyDbCaseFieldList().get(l_tabname)) {
								l_fullcontent.append(StringConstants.COMMA+l_tabdata.getCaseFieldName());
							}
						}
					}*/
					l_exportfile.createNewFile();
					l_fullcontent.append(System.lineSeparator());
					if (l_reporttype == 1 || l_reporttype > 7) {
						for (QRReviewData l_qrreviewdata : l_ilqrreviewdatalist) {
							appendContent(
								l_fullcontent
							,	l_qrreviewdata.getCaseId()
							,	l_qrreviewdata.getCaseVersion()
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
							,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
							,	l_qrreviewdata.getCaseAssociateName()
							,	l_qrreviewdata.getCreatedByName()
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
							,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
							,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
							,	String.valueOf(l_qrreviewdata.getFieldsError())
							,	String.valueOf(l_qrreviewdata.getCaseQuality())
							);
							appendUDFValues(
								l_fullcontent
							, 	l_qrlreviewdata.getCaseId()
							, 	l_qrlreviewdata.getCaseVersion()
							, 	l_enabledudfmap
							,	p_projectid
							,	p_connection
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContentCatWise(
								l_fullcontent
							,	l_qrreviewdata
							,	l_catcount
							);
							l_fullcontent.append(System.lineSeparator());
						}
					} else if (l_reporttype == 2) {
						for (QRReviewData l_qrreviewdata : l_ilqrreviewdatalist) {
							l_tempdata	=	null;
							l_tempdata	=	new StringBuilder();
							for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
								for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) {
									if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
										l_tempdata.append(CORRECT_NUMERICAL_CONST);
									} else {
										l_tempdata.append(INCORRECT_NUMERICAL_CONST);
									}
									
								}
							}
							l_tempdata.deleteCharAt(l_tempdata.length()-1);
							appendContent(
								l_fullcontent
							,	l_qrreviewdata.getCaseId()
							,	String.valueOf(l_qrreviewdata.getCaseVersion())
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
							,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
							,	l_qrreviewdata.getCaseAssociateName()
							,	l_qrreviewdata.getCreatedByName()
							,	l_qrreviewdata.getMedicalReviewerName()
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
							,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
							,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
							,	String.valueOf(l_qrreviewdata.getFieldsError())
							,	String.valueOf(l_qrreviewdata.getCaseQuality())
							);
							appendUDFValues(
								l_fullcontent
							, 	l_qrlreviewdata.getCaseId()
							, 	l_qrlreviewdata.getCaseVersion()
							, 	l_enabledudfmap
							,	p_projectid
							,	p_connection
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContent(
								l_fullcontent
							,	l_qrreviewdata.getCaseResult()
							,	l_tempdata.toString()
							);
							l_fullcontent.append(System.lineSeparator());
						}
					} else if (l_reporttype == 3) {
						for (QRReviewData l_qrreviewdata : l_ilqrreviewdatalist) {
							l_tempdata	=	null;
							l_tempdata	=	new StringBuilder();
							for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
								for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) {
									if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
										l_tempdata.append(CORRECT_NUMERICAL_CONST);
									} else {
										l_tempdata.append(INCORRECT_NUMERICAL_CONST);
									}
									
								}
							}
							l_tempdata.deleteCharAt(l_tempdata.length()-1);
							appendContent(
								l_fullcontent
							,	l_qrreviewdata.getCaseId()
							,	String.valueOf(l_qrreviewdata.getCaseVersion())
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
							,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
							,	l_qrreviewdata.getCaseAssociateName()
							,	l_qrreviewdata.getCreatedByName()
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
							,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
							,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
							,	String.valueOf(l_qrreviewdata.getFieldsError())
							,	String.valueOf(l_qrreviewdata.getCaseQuality())
							);
							appendUDFValues(
								l_fullcontent
							, 	l_qrlreviewdata.getCaseId()
							, 	l_qrlreviewdata.getCaseVersion()
							, 	l_enabledudfmap
							,	p_projectid
							,	p_connection
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContentCatWise(
								l_fullcontent
							,	l_qrreviewdata
							,	l_catcount
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContent(
								l_fullcontent
							,	l_tempdata.toString()
							);
							l_fullcontent.append(System.lineSeparator());
						}	
					} else if (l_reporttype == 4) {
						for (QRReviewData l_qrreviewdata : l_ilqrreviewdatalist) {
							l_tempdata	=	null;
							l_tempdata	=	new StringBuilder();
							for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
								for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) {
									if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
										l_tempdata.append(CORRECT_NUMERICAL_CONST);
									} else {
										l_tempdata.append(INCORRECT_NUMERICAL_CONST);
									}
									
								}
							}
							l_tempdata.deleteCharAt(l_tempdata.length()-1);
							appendContent(
								l_fullcontent
							,	l_qrreviewdata.getCaseId()
							,	String.valueOf(l_qrreviewdata.getCaseVersion())
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
							,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
							,	l_qrreviewdata.getCaseAssociateName()
							,	l_qrreviewdata.getCreatedByName()
							,	l_qrreviewdata.getMedicalReviewerName()
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
							,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
							,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
							,	String.valueOf(l_qrreviewdata.getFieldsError())
							,	String.valueOf(l_qrreviewdata.getCaseQuality())
							);
							appendUDFValues(
								l_fullcontent
							, 	l_qrlreviewdata.getCaseId()
							, 	l_qrlreviewdata.getCaseVersion()
							, 	l_enabledudfmap
							,	p_projectid
							,	p_connection
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContentCatWise(
								l_fullcontent
							,	l_qrreviewdata
							,	l_catcount
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContent(
								l_fullcontent
							,	l_tempdata.toString()
							);
							l_fullcontent.append(System.lineSeparator());
						}
					} else if (l_reporttype == 5) {
						for (QRReviewData l_qrreviewdata : l_ilqrreviewdatalist) {
							l_tempdata	=	null;
							l_tempdata	=	new StringBuilder();
							if (l_qrreviewdata.getCaseQuality() != 100) {
								for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
									for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) {
										if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 3) {
											l_tempdata	=	null;
											l_tempdata	=	new StringBuilder();
											l_tempdata.append(l_tabname);
											l_tempdata.append(StringConstants.COMMA);
											l_tempdata.append(l_tabdata.getCaseFieldName());
											l_tempdata.append(StringConstants.COMMA);
											l_tempdata.append(l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldComment());
											
											appendContent(
												l_fullcontent
											,	l_qrreviewdata.getCaseId()
											,	String.valueOf(l_qrreviewdata.getCaseVersion())
											,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
											,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
											,	l_qrreviewdata.getCaseAssociateName()
											,	l_qrreviewdata.getCreatedByName()
											,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
											,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
											,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
											,	String.valueOf(l_qrreviewdata.getFieldsError())
											,	String.valueOf(l_qrreviewdata.getCaseQuality())
											);
											appendUDFValues(
												l_fullcontent
											, 	l_qrlreviewdata.getCaseId()
											, 	l_qrlreviewdata.getCaseVersion()
											, 	l_enabledudfmap
											,	p_projectid
											,	p_connection
											);
											l_fullcontent.append(StringConstants.COMMA);
											appendContentCatWise(
												l_fullcontent
											,	l_qrreviewdata
											,	l_catcount
											);
											l_fullcontent.append(StringConstants.COMMA);
											appendContent(
												l_fullcontent
											,	l_tempdata.toString()
											);
											l_fullcontent.append(System.lineSeparator());
										}
									}
								}
								
							} else {
								appendContent(
									l_fullcontent
								,	l_qrreviewdata.getCaseId()
								,	String.valueOf(l_qrreviewdata.getCaseVersion())
								,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
								,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
								,	l_qrreviewdata.getCaseAssociateName()
								,	l_qrreviewdata.getCreatedByName()
								,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
								,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
								,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
								,	String.valueOf(l_qrreviewdata.getFieldsError())
								,	String.valueOf(l_qrreviewdata.getCaseQuality())
								);
								appendUDFValues(
									l_fullcontent
								, 	l_qrlreviewdata.getCaseId()
								, 	l_qrlreviewdata.getCaseVersion()
								, 	l_enabledudfmap
								,	p_projectid
								,	p_connection
								);
								l_fullcontent.append(StringConstants.COMMA);
								appendContentCatWise(
									l_fullcontent
								,	l_qrreviewdata
								,	l_catcount
								);
								l_fullcontent.append(StringConstants.COMMA);
								appendContent(
									l_fullcontent
								,	l_tempdata.toString()
								);
								l_fullcontent.append(System.lineSeparator());
							}
							
						}	
					} else if (l_reporttype == 6) {
						for (QRReviewData l_qrreviewdata : l_ilqrreviewdatalist) {
							l_tempdata	=	null;
							l_errordata	=	null;
							l_tempdata	=	new StringBuilder();
							l_errordata	=	new StringBuilder();
							for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
								for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) {
									if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
										l_tempdata.append(CORRECT_NUMERICAL_CONST);
									} else {
										l_tempdata.append(INCORRECT_NUMERICAL_CONST);
										l_errordata.append(l_tabdata.getCaseFieldName()+StringConstants.COLON);
										l_errordata.append(l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldComment());
										l_errordata.append(StringConstants.CARET);
									}
									
								}
							}
							l_tempdata.deleteCharAt(l_tempdata.length()-1);
							if (l_errordata.length() > 1) {
								l_errordata.deleteCharAt(l_errordata.length()-1);
							}
							
							appendContent(
								l_fullcontent
							,	l_qrreviewdata.getCaseId()
							,	String.valueOf(l_qrreviewdata.getCaseVersion())
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
							,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
							,	l_qrreviewdata.getCaseAssociateName()
							,	l_qrreviewdata.getCreatedByName()
							,	l_qrreviewdata.getMedicalReviewerName()
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
							,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
							,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
							,	String.valueOf(l_qrreviewdata.getFieldsError())
							,	String.valueOf(l_qrreviewdata.getCaseQuality())
							);
							appendUDFValues(
								l_fullcontent
							, 	l_qrlreviewdata.getCaseId()
							, 	l_qrlreviewdata.getCaseVersion()
							, 	l_enabledudfmap
							,	p_projectid
							,	p_connection
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContentCatWise(
								l_fullcontent
							,	l_qrreviewdata
							,	l_catcount
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContent(
								l_fullcontent
							,	l_tempdata.toString()
							,	l_errordata.toString()
							);
							l_fullcontent.append(System.lineSeparator());
						}
					} /*else if (l_reporttype == 7) {
						for (QRReviewData l_qrreviewdata : l_ilqrreviewdatalist) {
							l_tempdata	=	null;
							l_tempdata	=	new StringBuilder();
							for (String l_tabname : l_qrreviewdata.getSafetyDbCaseFieldList().keySet()) { 
								for (SafetyDbCaseFields l_tabdata : l_qrreviewdata.getSafetyDbCaseFieldList().get(l_tabname)) {
									if (l_qrreviewdata.getQrlReviewFieldDetails().get(l_tabdata.getCaseFieldMnemonic()).getFieldStatus() == 1) {
										l_tempdata.append(CORRECT_NUMERICAL_CONST);
									} else {
										l_tempdata.append(INCORRECT_NUMERICAL_CONST);
									}
									
								}
							}
							l_tempdata.deleteCharAt(l_tempdata.length()-1);
							appendContent(
								l_fullcontent
							,	l_qrreviewdata.getCaseId()
							,	String.valueOf(l_qrreviewdata.getCaseVersion())
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCaseRecvDate()))
							,	String.valueOf(l_qrreviewdata.getCaseSeriousness())
							,	(p_regclass != null && p_regclass.keySet().size() > 0) ? p_regclass.get(l_qrreviewdata.getRegClass()) : ""
							,	l_qrreviewdata.getCaseAssociateName()
							,	l_qrreviewdata.getCreatedByName()
							,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_qrreviewdata.getCreatedOn()))
							,	String.valueOf(l_qrreviewdata.getFieldsReviewed())
							,	String.valueOf(l_qrreviewdata.getFieldsWithoutError())
							,	String.valueOf(l_qrreviewdata.getFieldsError())
							,	String.valueOf(l_qrreviewdata.getCaseQuality())
							);
							appendUDFValues(
								l_fullcontent
							, 	l_qrlreviewdata.getCaseId()
							, 	l_qrlreviewdata.getCaseVersion()
							, 	l_enabledudfmap
							,	p_projectid
							,	p_connection
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContentCatWise(
								l_fullcontent
							,	l_qrreviewdata
							,	l_catcount
							);
							l_fullcontent.append(StringConstants.COMMA);
							appendContent(
								l_fullcontent
							,	l_tempdata.toString()
							);
							l_fullcontent.append(System.lineSeparator());
						}	
					}*/
					
					l_fwr.write(l_fullcontent.toString());
				}
			} catch(Exception excep) {
				log.error("Error during report generation", excep);
				throw new SciException(
					"Error during report generation"
				,	excep
				);
			} finally {
				try {
					l_fwr.close();
				} catch (IOException ioExcep) {
					log.error("Error during report generation", ioExcep);
					throw new SciException(
						"Error during report generation"
					,	ioExcep
					);
				}
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		} catch (SciException sciExcep){
			p_connection.rollbackTransaction();
			log.error("Error fetching list of templates", sciExcep);
			throw sciExcep;
		}
		return l_exportfile;
	}
	
	/**
	 * 
	 * @param p_fullcontent
	 * @param p_qrreviewdata
	 * @param p_catcount
	 */
	private static void appendContentCatWise (
		StringBuilder	p_fullcontent
	,	QRReviewData	p_qrreviewdata
	,	int				p_catcount
	) {
		if (p_catcount == 1 || p_catcount == 0) {
			appendContent(
				p_fullcontent
			,	p_qrreviewdata.getCaseResult()
			);
		} else if (p_catcount == 2) {
			appendContent(
				p_fullcontent
			,	String.valueOf(p_qrreviewdata.getCaseCat1Quality())
			,	String.valueOf(p_qrreviewdata.getCaseCat2Quality())
			,	p_qrreviewdata.getCaseResult()
			);
		} else if (p_catcount == 3) {
			appendContent(
				p_fullcontent
			,	String.valueOf(p_qrreviewdata.getCaseCat1Quality())
			,	String.valueOf(p_qrreviewdata.getCaseCat2Quality())
			,	String.valueOf(p_qrreviewdata.getCaseCat3Quality())
			,	p_qrreviewdata.getCaseResult()
			);
		}
	}
	
	/**
	 * 
	 * @param p_fullcontent
	 * @param p_caseid
	 * @param p_caseversion
	 * @param p_enabledudfmap
	 * @param p_projectid
	 * @param p_connection
	 * @throws SciException
	 */
	private static void appendUDFValues (
		StringBuilder				p_fullcontent
	,	String						p_caseid
	,	String						p_caseversion
	,	Map<String,List<String>>	p_enabledudfmap
	,	int							p_projectid
	,	DbConnection				p_connection
	) throws SciException {
		PvcpCaseMaster	l_casemaster	=	null;
		List<String>	l_caseudfvalues	=	null;
		String			l_udfprojvalue	=	null;
		String[]		l_udfkeyvalue	=	null;
		StringBuilder	l_udfvalues		=	null;
		
		if (p_enabledudfmap != null && p_enabledudfmap.size()>0) {
			l_casemaster	=	getPVCPCaseMasterFieldsByInternamCaseId(p_connection, p_caseid + "_" + p_caseversion, p_projectid);
			l_caseudfvalues	=	new ArrayList<>();
			l_caseudfvalues.add(l_casemaster.getUdf1());
			l_caseudfvalues.add(l_casemaster.getUdf2());
			l_caseudfvalues.add(l_casemaster.getUdf3());
			l_caseudfvalues.add(l_casemaster.getUdf4());
			l_caseudfvalues.add(l_casemaster.getUdf5());
			l_caseudfvalues.add(l_casemaster.getUdf6());
			l_caseudfvalues.add(l_casemaster.getUdf7());
			l_caseudfvalues.add(l_casemaster.getUdf8());
			l_caseudfvalues.add(l_casemaster.getUdf9());
			l_caseudfvalues.add(l_casemaster.getUdf10());
			l_udfvalues	=	new StringBuilder();
			for (int l_i=1; l_i<=10; l_i++) {
				if (p_enabledudfmap.containsKey(QRConstant.ENABLE_UDF_MAP_KEY+l_i)) {
					l_udfvalues.append(StringConstants.COMMA);
					l_udfprojvalue	= p_enabledudfmap.get(QRConstant.ENABLE_UDF_MAP_KEY+l_i).get(1);
					if (l_udfprojvalue != null) {
						for (String l_udfvalue : l_udfprojvalue.split(",")) {
							l_udfkeyvalue	=	l_udfvalue.split(":");
							if (l_udfkeyvalue[0].equals(l_caseudfvalues.get(l_i-1))) {
								l_udfvalues.append(l_udfkeyvalue[1]);
							}
						}
					} else {
						l_udfvalues.append(l_caseudfvalues.get(l_i-1));
					}
				}
			}
			p_fullcontent.append(l_udfvalues.toString());
		}
	}
	
	/**
	 * 
	 * @param p_fullcontent
	 * @param p_cpdata
	 * @param p_catcount
	 */
	private static void appendContentCatWiseCP (
		StringBuilder		p_fullcontent
	,	QRCPDashboardReport	p_cpdata
	,	int					p_catcount
	) {
		if (p_catcount == 1 || p_catcount == 0) {
			appendContent(
				p_fullcontent
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCaseQuality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			);
		} else if (p_catcount == 2) {
			appendContent(
				p_fullcontent
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCat1Quality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCat2Quality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCaseQuality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			,	String.valueOf(p_cpdata.getTotalCat1FieldsReviewed())
			,	String.valueOf(p_cpdata.getTotalCat2FieldsReviewed())
			,	String.valueOf(p_cpdata.getTotalCat1FieldsError())
			,	String.valueOf(p_cpdata.getTotalCat2FieldsError())
			);
		} else if (p_catcount == 3) {
			appendContent(
				p_fullcontent
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCat1Quality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCat2Quality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCat3Quality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			,	String.valueOf(BigDecimal.valueOf(p_cpdata.getTotalCaseQuality()).setScale(2, BigDecimal.ROUND_HALF_UP))
			,	String.valueOf(p_cpdata.getTotalCat1FieldsReviewed())
			,	String.valueOf(p_cpdata.getTotalCat2FieldsReviewed())
			,	String.valueOf(p_cpdata.getTotalCat3FieldsReviewed())
			,	String.valueOf(p_cpdata.getTotalCat1FieldsError())
			,	String.valueOf(p_cpdata.getTotalCat2FieldsError())
			,	String.valueOf(p_cpdata.getTotalCat3FieldsError())
			);
		}
	}
	/**
	 * 
	 * @param p_fullcontent
	 * @param p_appendcontent
	 */
	static void appendContent (
		StringBuilder	p_fullcontent
	,	String ... 		p_appendcontent	
	) {
		for (String p_content : p_appendcontent) {
			p_fullcontent.append(p_content+",");
		}
		p_fullcontent.deleteCharAt(p_fullcontent.length()-1);
	}
	
	/**
	 * 
	 * @param p_connection
	 * @param p_userinfo
	 * @param p_projectid
	 * @param n_startdate
	 * @param n_enddate
	 * @return
	 * @throws SciException
	 */
	static List<QRReviewData> getQualityReviewLogbyrange (
		DbConnection	p_connection
	, 	UserInfo 		p_userinfo
	,	int				p_projectid
	, 	int				n_startdate
	,	int 			n_enddate
	) throws SciException {
		DbQuerySelect			l_selectquery			=	null;
		List<QRReviewData>		l_ilqrreviewdatalist	=	null;
		QRReviewData			l_qrlreviewdata			=	null;
		String 					l_completequery 		=	null;
		Calendar []				l_cldr					=	null;
		
		l_cldr	=	getDate(n_startdate,n_enddate);
		try {
			p_connection.startTransaction();
			l_completequery	=	String.format(SELECT_MONTHLY_QUALITYREVIEWLOG, DbQueryUtils.getQualifiedObjectName(QRDbSchema.QUALITY_REVIEW_LOG.name()));
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			
			l_selectquery.addWhereClauseParameter(l_cldr[0].getTime());
			l_selectquery.addWhereClauseParameter(l_cldr[1].getTime());
			l_selectquery.addWhereClauseParameter(p_projectid);
						
			DbResultSet l_rs = p_connection.executeQuery(l_selectquery);
			l_ilqrreviewdatalist = new ArrayList<>();
			while(l_rs.next())
			{
				l_qrlreviewdata = new QRReviewData();
				l_qrlreviewdata.setCreatedOn(l_rs.readDate(1));
				l_qrlreviewdata.setCaseSeriousness(l_rs.readString(2));
				l_qrlreviewdata.setCaseQuality(l_rs.readInt(3));
				
				l_ilqrreviewdatalist.add(l_qrlreviewdata);
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching all the review data of qr from DB"
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching all the review data of qr from DB"
			,	dbExcep
			);
		}
		return l_ilqrreviewdatalist;
	}
	/**
	 * 
	 * @param p_connection
	 * @param s_CaseId
	 * @param p_projectId
	 * @return
	 * @throws SciServiceException
	 * @throws SciException
	 */
	static List<QRReviewData> getQRLByCaseId(
		DbConnection 	p_connection
	, 	String 			s_CaseId
	, 	int 			p_projectId
	) throws SciException {
		DbQuerySelect	l_selectquery 		=	null;
		List<QRReviewData>	l_qualityreviewdatalist =	null;
	
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								QRDbSchema.QUALITY_REVIEW_LOG
							, 	DbQueryUtils.bindWhereClauseWithAnd(
									QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.CASEID.bindClause()
								)
							);
		
		l_selectquery.addWhereClauseParameter(p_projectId);
		l_selectquery.addWhereClauseParameter(s_CaseId);
		
		try{
			p_connection.startTransaction();
			try (DbResultSet resultSet = p_connection.executeQuery(l_selectquery)) {
				l_qualityreviewdatalist = new ArrayList<QRReviewData>();
				while (resultSet.next()) {
					l_qualityreviewdatalist.add(
						readQRLReviewData(
							p_connection	
						,	resultSet
						, 	BinaryContentHomeImpl.readContent(
								p_connection
							, 	resultSet.readInt(
									QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
								)
							)
						,	BinaryContentHomeImpl.readContent(
								p_connection
							, 	resultSet.readInt(
									QRDbSchema.QUALITY_REVIEW_LOG.CASEREVIEWSUMMSEQID
								)
							)
						)
					);
				}				
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching case qrdata",  dbExcep);
			throw new SciException("Error fetching case qrdata", dbExcep);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching case qrdata - {}",  sciExcep);
			throw sciExcep;
		}
		return l_qualityreviewdatalist;
	}
	
	/**
	 * This method will be used to fetch all the review done on the
	 * combination of case id and case version.
	 * @param p_connection	DB Connection
	 * @param p_caseid		Case Id
	 * @param p_caseversion Case Version
	 * @param p_projectId	Project to which the case belongs
	 * @return				It will return the map in which the
	 * 						key will be Review Type Code and the
	 * 						value will be list of all the
	 * 						QRReviewData
	 * @throws SciServiceException 
	 * @throws SciException
	 */
	static Map<String, List<QRReviewData>> getCaseHistoryByIncrementalCaseId(
		DbConnection 	p_connection
	, 	String 			p_caseid
	,	String			p_caseversion
	, 	int 			p_projectId
	) throws SciException {
		Map<String, List<QRReviewData>> l_qrreviewdatamap 	= 	null;
		DbQuerySelect					l_selectquery 		=	null;
	
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								QRDbSchema.QUALITY_REVIEW_LOG
							, 	DbQueryUtils.bindWhereClauseWithAnd(
									QRDbSchema.QUALITY_REVIEW_LOG.CASEID.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.CASEVERSION.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.PRJSEQID.bindClause()
								,	QRDbSchema.QUALITY_REVIEW_LOG.REVIEWACTIVE.bindClause()
								)
							);
		
		l_selectquery.addWhereClauseParameter(p_caseid);
		l_selectquery.addWhereClauseParameter(p_caseversion);
		l_selectquery.addWhereClauseParameter(p_projectId);
		l_selectquery.addWhereClauseParameter(1);
		
		try{
			p_connection.startTransaction();
			try (DbResultSet resultSet = p_connection.executeQuery(l_selectquery)) {
				l_qrreviewdatamap	=	new HashMap<>();
				while (resultSet.next()) {
					if (l_qrreviewdatamap.containsKey(resultSet.readString(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE))) {
						List<QRReviewData>	l_editlist	=	null;
						l_editlist	=	l_qrreviewdatamap.get(resultSet.readString(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE));
						l_editlist.add(
							readQRLReviewData(
								p_connection	
							,	resultSet
							, 	BinaryContentHomeImpl.readContent(
									p_connection
								, 	resultSet.readInt(
										QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
									)
								)
							,	BinaryContentHomeImpl.readContent(
									p_connection
								, 	resultSet.readInt(
										QRDbSchema.QUALITY_REVIEW_LOG.CASEREVIEWSUMMSEQID
									)
								)
							)
						);
						l_qrreviewdatamap.put(
							resultSet.readString(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE)
						, 	l_editlist
						);
					} else {
						List<QRReviewData>	l_newlist	=	new ArrayList<>();						
						l_newlist.add(
							readQRLReviewData(
								p_connection	
							,	resultSet
							, 	BinaryContentHomeImpl.readContent(
									p_connection
								, 	resultSet.readInt(
										QRDbSchema.QUALITY_REVIEW_LOG.CONTENTSEQID
									)
								)
							,	BinaryContentHomeImpl.readContent(
									p_connection
								, 	resultSet.readInt(
										QRDbSchema.QUALITY_REVIEW_LOG.CASEREVIEWSUMMSEQID
									)
								)
							)
						);
						l_qrreviewdatamap.put(resultSet.readString(QRDbSchema.QUALITY_REVIEW_LOG.REVIEWTYPE), l_newlist);
					}
				}				
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching qr data by internal case id",  dbExcep);
			throw new SciException("Error fetching qr data by internal case id", dbExcep);
		} catch (SciException sciExcep) {
			p_connection.rollbackTransaction();
			log.error("Error fetching qr data by internal case id- {}",  sciExcep);
			throw sciExcep;
		}
		return l_qrreviewdatamap;
	}
	
	/**
	 * This will return of all the categories applicable for the
	 * project. This can return null also if categories are not
	 * applicable for the project.
	 * @param p_projectid	The project for which the categories
	 * 						need to be fetched.
	 * @return				Categories applicable for the project.
	 * 						If more than one category is there
	 * 						then it will be separated by ~.
	 */
	static String getApplicableCategories (
		int	p_projectid
	) {
		return	ConfigHome.getPrjConfigSettings(
					QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_APPLICABLE_CAT
				, 	p_projectid
				);
	}
	
	/**
	 * This will return the error types mapped for the project.
	 * @param p_projectid	The project for which the error type
	 * 						needs to be fetched.
	 * @return				The map of error types which contain
	 * 						error code as key and error name as
	 * 						value.
	 * @throws SciException
	 */
	
	static Map<String,String> getErrorTypes (
		int p_projectid
	) throws SciException {
		Map<String,String>	l_errortypes	=	null;
		String								l_errortypeall	=	null;
		String[]							l_errordata		=	null;

		l_errortypeall	=	ConfigHome.getPrjConfigSettings(
								QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_ERROR_TYPE
							, 	p_projectid
							);
		try {
			if (StringUtils.isNullOrEmpty(l_errortypeall)) {
				throw new SciException("File Format cannot be null.");
			} else {
				l_errortypes	=	new LinkedHashMap<>();
				for (String l_errortype : l_errortypeall.split(",")) {
					l_errordata		=	l_errortype.split(":");
					l_errortypes.put(l_errordata[0], l_errordata[1]);
				}
			}
		} catch (Exception excep) {
			throw new SciException("Invalid error type");
		}
		return l_errortypes;
	}
	
	/**
	 * This will return the list of all the review types applicable
	 * for the project.
	 * @param p_projectid	The project for which the review type
	 * 						needs to be fetched.
	 * @return				List of all the review types. The list
	 * 						contains object of ObjectIdPair and the
	 * 						ObjectIdPair will contain the data in
	 * 						code and display value combination.
	 */
	static List<ObjectIdPair<String, String>> getReviewTypeProjectList(
		int	p_projectid	
	) throws SciException {
		String 								l_reviewtypeall		=	null;
		ObjectIdPair<String, String> 		l_reviewtypedata 	= 	null;
		List<ObjectIdPair<String, String>> 	l_reviewtypelist	=	null;
		
		l_reviewtypeall	=	ConfigHome.getPrjConfigSettings(
								QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_REVIEW_TYPE
							,	p_projectid
							);
		
		if (StringUtils.isNullOrEmpty(l_reviewtypeall)) {
			throw new SciException("Review Type is not maintained at project level");
		} else {
			String [] l_values	=	null;
			l_reviewtypelist	=	new ArrayList<>();
			for (String l_reviewtype : l_reviewtypeall.split(",")) {
				l_values	=	l_reviewtype.split(":");
				l_reviewtypedata	=	new ObjectIdPair<>(l_values[0], l_values[1]);
				l_reviewtypelist.add(l_reviewtypedata);
			}
		}
		return l_reviewtypelist;
	}
	
	/**
	 * This method will return all the regulatory
	 * class mapped for the project.
	 * @param p_projectid	The project id for which the
	 * 						regulatory class need to be
	 * 						fetched.
	 * @return				The map in which regulatory
	 * 						code is mapped to the display
	 * 						value.
	 */
	static Map<String,String> getApplicableRegClass(int	p_projectid) {
		String 				l_regclassall		=	null;
		Map<String,String>	l_regclassmap		=	null;
		String[]			l_regclassdetail	=	null;
		
		l_regclassall	=	ConfigHome.getPrjConfigSettings(
								QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_APPLICABLE_REGCLASS
							,	p_projectid
							);
		if (StringUtils.isNullOrEmpty(l_regclassall)) {
			
		} else {
			l_regclassmap	=	new LinkedHashMap<>();
			for (String l_regclass : l_regclassall.split("~")) {
				l_regclassdetail	=	l_regclass.split(":");
				l_regclassmap.put(l_regclassdetail[0], l_regclassdetail[1]);
			}
		}
		return	l_regclassmap;
	}
	
	/**
	 * This method is used to fetch the starting date of the month for project
	 * related dashboard and information.
	 * @param p_projectid	The project for which the starting date is required.
	 * @return				The date between 1 and 28.
	 * @throws 	
	 * @throws SciException If the date is not between 1 and 28 or null
	 */
	static int getProjectStartDate (int p_projectid) throws SciException {
		int 	l_projectstartdate	=	0;
		String	l_propvalue			=	null;
		
		l_propvalue	=	ConfigHome.getPrjConfigSettings(
							QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_START_MONTH_DAY
						, 	p_projectid
						);
		if (!StringUtils.isNullOrEmpty(l_propvalue)) {
			try {
				l_projectstartdate	=	Integer.parseInt(l_propvalue);
				if (l_projectstartdate < 1 && l_projectstartdate < 28) {
					throw new SciException("Starting date of the month is not between 1 and 28");
				}
			} catch (NumberFormatException e) {
				throw new SciException("Starting date of the month is not integer");
			}
		} else {
			throw new SciException("Starting date of the month is blank");
		}
		return l_projectstartdate;
	}
	
	/**
	 * This method return serious case threshold value for the selected project.
	 * @param p_projectid gives user current project sequence id.
	 * @return  if threshold value for serious case is present in project
	 *  		configuration then return threshold value else return 0.
	 */
	static int getQRGraphSeriousnessThreshold (int p_projectid) {
		String l_threshold	=	null;
		
		l_threshold	=	ConfigHome.getPrjConfigSettings(
							QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_QR_GRAPH_SERIOUS_THRESHOLD
						, 	p_projectid
						);
		if(l_threshold != null)
		{
			return Integer.parseInt(l_threshold);
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * This method return non-serious case threshold value for the selected project.
	 * @param p_projectid gives user current project sequence id.
	 * @return  if threshold value for non-serious case is present in project
	 *  		configuration then return threshold value else return 0.
	 */
	static int getQRGraphNonSeriousnessThreshold (int p_projectid) {
		String l_threshold	=	null;
		
		l_threshold	=	ConfigHome.getPrjConfigSettings(
							QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_QR_GRAPH_NON_SERIOUS_THRESHOLD
						, 	p_projectid
						);
		if(l_threshold != null)
		{
			return Integer.parseInt(l_threshold);
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * This method is used to fetch the sheet number
	 * of the template that will be downloaded after
	 * the case review.
	 * @param p_projectid	The project for which the template
	 * 	 					sheet number is required.
	 * @return				The sheet number.	
	 * @throws SciException If the value maintained at project
	 *  					level is non-numeric or null
	 */
	static int getCaseReviewDownloadSheetNum (int p_projectid) throws SciException {
		int 	l_sheetnum		=	0;
		String	l_propvalue		=	null;
		
		l_propvalue	=	ConfigHome.getPrjConfigSettings(
							QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_CASE_REVIEW_DWNLD_SHEET_NUM
						, 	p_projectid
						);
		if (!StringUtils.isNullOrEmpty(l_propvalue)) {
			try {
				l_sheetnum	=	Integer.parseInt(l_propvalue);
			} catch (NumberFormatException e) {
				throw new SciException("Case Review download sheet number is not a number");
			}
		} else {
			throw new SciException("Case Review download sheet number is blank");
		}
		return l_sheetnum;
	}
	
	/**
	 * Thia method returns UDF fields enable by in from project confif settings 
	 * @param p_projectid current selected project by user 
	 * @return Map with Udf field name as key and list of udf field display 
	 * 		   name and datatype, if not found returns empty Map
	 */
	static Map<String,List<String>> getEnableUdfList (int p_projectid) {
		Map<String,List<String>> 	udfEnableMap				=	new LinkedHashMap<>();
		List<String> 				udfValues					=	null;
		String 						projPropUdfDisplayNameKey	=	null,
									projPropUdfDataTypeKey 		=	null,
									enableUdfDisplayName 		=	null,
									enableUdfDataType			=	null;
		
		for(int i = 1 ; i <= QRConstant.TOTAL_UDF_IELDS; i++ )
		{
			udfValues = new ArrayList<>();
			projPropUdfDisplayNameKey	=	QRConstant.PROP_PROJ_CASE_MASTER_UDF + i + StringConstants.PERIOD + QRConstant.PROP_PROJ_CASE_MASTER_UDF_DISPLAY_NAME;
			projPropUdfDataTypeKey 		=	QRConstant.PROP_PROJ_CASE_MASTER_UDF + i + StringConstants.PERIOD + QRConstant.PROP_PROJ_CASE_MASTER_UDF_DATATYPE;
			
			enableUdfDisplayName 		=	ConfigHome.getPrjConfigSetting(
												projPropUdfDisplayNameKey
											,	QRConstant.APP_NAME
											,	p_projectid
											);
			enableUdfDataType 			=	ConfigHome.getPrjConfigSetting(
												projPropUdfDataTypeKey
											, 	QRConstant.APP_NAME
											, 	p_projectid
											);
		
			if(!StringUtils.isNullOrEmpty(enableUdfDisplayName))
			{
				udfValues.add(enableUdfDisplayName);
				udfValues.add(enableUdfDataType);
				udfEnableMap.put(QRConstant.ENABLE_UDF_MAP_KEY+i, udfValues);
			}
		}
		return udfEnableMap;
	}
	
	/**
	 * This is used to return the file formats applicable for the
	 * case master upload in Quality Review.
	 * @param p_projectid The project id for which the file will
	 * 	                  be uploaded.
	 * @return	Map of file format internal code and display. The
	 * 			key will be internal code and the value will be
	 * 			display name.
	 * @throws SciException If the file format maintained in db
	 * 						is null.
	 */
	static Map<String,String> getApplicableFileFormat(int p_projectid) throws SciException {
		String 				l_fileformats		=	null;
		Map<String,String>	l_fileformatmap		=	null;
		String[]			l_fileformatdetail	=	null;
		
		l_fileformats	=	ConfigHome.getPrjConfigSettings(
								QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_APPLICABLE_FILEFORMAT
							,	p_projectid
							);
		if (StringUtils.isNullOrEmpty(l_fileformats)) {
			throw new SciException("File Format cannot be null.");
		} else {
			l_fileformatmap	=	new LinkedHashMap<>();
			for (String l_regclass : l_fileformats.split(",")) {
				l_fileformatdetail	=	l_regclass.split(":");
				l_fileformatmap.put(l_fileformatdetail[0], l_fileformatdetail[1]);
			}
		}
		return	l_fileformatmap;
	}
	
	/**
	 * This method will check whether the MR Name to be displayed
	 * in the Quality Review Page or not.
	 * @param p_projectid Project Id to which the logged in user
	 * 					  belongs.
	 * @return	true if the name to be displayed otherwise false
	 */
	static boolean isMRNameEnabledForProject (
		int	p_projectid
	) {
		String l_mrnameapplicable	=	null;
		
		l_mrnameapplicable	=	ConfigHome.getPrjConfigSettings(
									QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_MR_NAME_DISPLAY
								,	p_projectid
								);
		if (StringUtils.isNullOrEmpty(l_mrnameapplicable)) {
			return false;
		} else if (l_mrnameapplicable.equals("true")) {
			return true;
		} else {
			return false;
		}
	}
}
