package com.sciformix.sciportal.apps.qr;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class QRDbSchema {
	public static QualityReviewLog QUALITY_REVIEW_LOG = new QualityReviewLog();
	public static Template TEMPLATE = new Template();
	public static ErrorDashboard ERRORDATA = new ErrorDashboard();

	public static class QualityReviewLog extends DbTable 
	{
		public DbColumn RLSEQID = null;
		public DbColumn PRJSEQID = null;
		public DbColumn SCFTCTOSEQID = null;
		public DbColumn QRTCTOSEQID = null;
		public DbColumn REVIEWTYPE = null;
		public DbColumn REVIEWROUND = null;
		public DbColumn REVIEWVERSION = null;
		public DbColumn REVIEWACTIVE = null;
		public DbColumn DTCREATED = null;
		public DbColumn USRCREATED = null;
		public DbColumn CASEID = null;
		public DbColumn CASEVERSION = null;
		public DbColumn CASERECVDATE = null;
		public DbColumn CASESERIOUSNESS = null;
		public DbColumn CASEASSOC = null;
		public DbColumn CONTENTSEQID = null;
		public DbColumn FIELDSREVIEW = null;
		public DbColumn FIELDSWERROR = null;
		public DbColumn FIELDSERROR = null;		
		public DbColumn CASEQUALITY = null;
		public DbColumn CASERESULT = null;
		public DbColumn CAT1QUALITY = null;
		public DbColumn CAT2QUALITY = null;
		public DbColumn CAT3QUALITY = null;
		public DbColumn MEDICALREVIEWER = null;
		public DbColumn CAT1FIELDSREVIEWED = null;
		public DbColumn CAT2FIELDSREVIEWED = null;
		public DbColumn CAT3FIELDSREVIEWED = null;
		public DbColumn CAT1FIELDSWERROR = null;
		public DbColumn CAT2FIELDSWERROR = null;
		public DbColumn CAT3FIELDSWERROR = null;
		public DbColumn CASEREVIEWSUMMSEQID = null;
		public DbColumn	REGCLASS		= null;
		public DbColumn CDBR = null;
		

		public QualityReviewLog() 
		{
			super("APP_QRL_REVIEWLOG");
			setPkSequenceName("APP_QRL_RLSEQID_SEQ");

			RLSEQID = addColumn("RLSEQID");
			PRJSEQID = addColumn("PRJSEQID");
			SCFTCTOSEQID = addColumn("SCFTCTOSEQID");
			QRTCTOSEQID = addColumn("QRTCTOSEQID");
			REVIEWTYPE = addColumn("REVIEWTYPE");
			REVIEWROUND = addColumn("REVIEWROUND");
			REVIEWVERSION = addColumn("REVIEWVERSION");
			REVIEWACTIVE = addColumn("REVIEWACTIVE");
			DTCREATED = addColumn("DTCREATED");
			USRCREATED = addColumn("USRCREATED");
			CASEID = addColumn("CASEID");
			CASEVERSION = addColumn("CASEVERSION");
			CASERECVDATE = addColumn("CASERECVDATE");
			CASESERIOUSNESS = addColumn("CASESERIOUSNESS");
			CASEASSOC = addColumn("CASEASSOC");
			CONTENTSEQID = addColumn("CONTENTSEQID");
			FIELDSREVIEW = addColumn("FIELDSREVIEW");
			FIELDSWERROR = addColumn("FIELDSWERROR");
			FIELDSERROR = addColumn("FIELDSERROR");
			CASEQUALITY = addColumn("CASEQUALITY");
			CASERESULT = addColumn("CASERESULT");
			CAT1QUALITY = addColumn("CAT1QUALITY");
			CAT2QUALITY = addColumn("CAT2QUALITY");
			CAT3QUALITY = addColumn("CAT3QUALITY");
			MEDICALREVIEWER	= addColumn("MEDICALREVIEWER");
			CAT1FIELDSREVIEWED = addColumn("CAT1FIELDSREVIEWED");
			CAT2FIELDSREVIEWED = addColumn("CAT2FIELDSREVIEWED");
			CAT3FIELDSREVIEWED = addColumn("CAT3FIELDSREVIEWED");
			CAT1FIELDSWERROR = addColumn("CAT1FIELDSWERROR");
			CAT2FIELDSWERROR = addColumn("CAT2FIELDSWERROR");
			CAT3FIELDSWERROR = addColumn("CAT3FIELDSWERROR");
			CASEREVIEWSUMMSEQID = addColumn("CASEREVIEWSUMMSEQID");
			REGCLASS = addColumn("REGCLASS");
			CDBR = addColumn("CDBR");
		}
	}
	
	public static class Template extends DbTable {
		public DbColumn CTOSEQID                  	= null;
		public DbColumn CASEQUALITYTHRESHOLD      	= null;
		public DbColumn CAT1QUALITYTHRESHOLD      	= null;
		public DbColumn CAT2QUALITYTHRESHOLD      	= null;
		public DbColumn CAT3QUALITYTHRESHOLD      	= null;
		public DbColumn CASECLASSIFICATIONOVERALL 	= null;
		public DbColumn CASECLASSIFICATIONCAT1    	= null;
		public DbColumn CASECLASSIFICATIONCAT2    	= null;
		public DbColumn CASECLASSIFICATIONCAT3    	= null;
		public DbColumn REVIEWERALLOWEDCORRECT    	= null;
		public DbColumn CASECRITERIAMODE   			= null;
		public DbColumn CASECRITERIA   				= null;
		public DbColumn RVWCMPTDWNLDCONTENTSEQID    = null;
		public Template() {
			super("APP_QRT_DETAILS");
			CTOSEQID                  	= addColumn("CTOSEQID");
			CASEQUALITYTHRESHOLD      	= addColumn("CASEQUALITYTHRESHOLD");
			CAT1QUALITYTHRESHOLD      	= addColumn("CAT1QUALITYTHRESHOLD");
			CAT2QUALITYTHRESHOLD      	= addColumn("CAT2QUALITYTHRESHOLD");
			CAT3QUALITYTHRESHOLD      	= addColumn("CAT3QUALITYTHRESHOLD");
			CASECLASSIFICATIONOVERALL 	= addColumn("CASECLASSIFICATIONOVERALL");
			CASECLASSIFICATIONCAT1    	= addColumn("CASECLASSIFICATIONCAT1");
			CASECLASSIFICATIONCAT2    	= addColumn("CASECLASSIFICATIONCAT2");
			CASECLASSIFICATIONCAT3    	= addColumn("CASECLASSIFICATIONCAT3");
			REVIEWERALLOWEDCORRECT    	= addColumn("REVIEWERALLOWEDCORRECT");
			CASECRITERIAMODE   			= addColumn("CASECRITERIAMODE");
			CASECRITERIA   				= addColumn("CASECRITERIA");
			RVWCMPTDWNLDCONTENTSEQID   	= addColumn("RVWCMPTDWNLDCONTENTSEQID");
		}
			
	}
	
	public static class ErrorDashboard extends DbTable 
	{
		public DbColumn RLSEQID       = null;
		public DbColumn PRJSEQID	 = null;
		public DbColumn SCFTCTOSEQID = null;
		public DbColumn QRTCTOSEQID	 = null;
		public DbColumn CMSEQID        = null;
		public DbColumn REVIEWTYPE	 = null;
		public DbColumn REVIEWROUND	 = null;
		public DbColumn DTCREATED	 = null;
		public DbColumn INTERNALCASEID = null;
		public DbColumn CASEASSOC      = null;
		public DbColumn CASESERIOUSNESS = null;
		public DbColumn FIELDMNEMONIC  = null;
		public DbColumn ERRORTYPE      = null;
		public DbColumn ERRORCOMMENT   = null;
		public DbColumn FIELDCAT   		= null;
		public ErrorDashboard() {
			super("APP_QRL_ERRORDATA");
			RLSEQID			= addColumn("RLSEQID");
			PRJSEQID	 	= addColumn("PRJSEQID");
			SCFTCTOSEQID 	= addColumn("SCFTCTOSEQID");
			QRTCTOSEQID	 	= addColumn("QRTCTOSEQID");
			CMSEQID        	= addColumn("CMSEQID");
			REVIEWTYPE	 	= addColumn("REVIEWTYPE");
			REVIEWROUND	 	= addColumn("REVIEWROUND");
			DTCREATED	 	= addColumn("DTCREATED");
			INTERNALCASEID 	= addColumn("INTERNALCASEID");
			CASEASSOC      	= addColumn("CASEASSOC");
			CASESERIOUSNESS = addColumn("CASESERIOUSNESS");
			FIELDMNEMONIC  	= addColumn("FIELDMNEMONIC");
			ERRORTYPE      	= addColumn("ERRORTYPE");
			ERRORCOMMENT   	= addColumn("ERRORCOMMENT");
			FIELDCAT		= addColumn("FIELDCAT");
		}
	}
}