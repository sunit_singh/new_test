package com.sciformix.sciportal.apps.qr;

import java.util.Map;

public class QRErrorDashboardData {
	private Map<String,Integer>
		errorFieldsCountMap
	;
	private Map<String,Integer>
		errorTypeCountMap
	;
	private Map<String,Integer>
		fieldCatCountMap
	;
	private Map<String,Integer>
		reviewTypeCountMap
	;
	public Map<String, Integer> getErrorFieldsCountMap() {
		return errorFieldsCountMap;
	}
	public void setErrorFieldsCountMap(Map<String, Integer> errorFieldsCountMap) {
		this.errorFieldsCountMap = errorFieldsCountMap;
	}
	public Map<String, Integer> getErrorTypeCountMap() {
		return errorTypeCountMap;
	}
	public void setErrorTypeCountMap(Map<String, Integer> errorTypeCountMap) {
		this.errorTypeCountMap = errorTypeCountMap;
	}
	public Map<String, Integer> getFieldCatCountMap() {
		return fieldCatCountMap;
	}
	public void setFieldCatCountMap(Map<String, Integer> fieldCatCountMap) {
		this.fieldCatCountMap = fieldCatCountMap;
	}
	public Map<String, Integer> getReviewTypeCountMap() {
		return reviewTypeCountMap;
	}
	public void setReviewTypeCountMap(Map<String, Integer> reviewTypeCountMap) {
		this.reviewTypeCountMap = reviewTypeCountMap;
	}	
}
