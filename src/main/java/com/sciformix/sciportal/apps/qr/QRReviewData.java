package com.sciformix.sciportal.apps.qr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.sciformix.sciportal.safetydb.SafetyDbCaseFields;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate;

public final class QRReviewData {
		
	/**
	 * This will store the sequence
	 * id of the review.
	 */
	private int
		sequenceId
	;	
	/**
	 * This will store the template
	 * details that was used to review
	 * this case
	 */
	private QRTemplate
		qrTemplate
	;
	
	private SafetyDbCaseFieldsTemplate
		scft
	;	
	
	/**
	 * The type of review.
	 */
	private String
		reviewType
	;
	/**
	 * The number of reviews that have
	 * been taken place for a particular
	 * case.
	 */
	private int
		reviewRound
	;
	/**
	 * The number of times the review
	 * has been corrected of the particular
	 * case.
	 */
	private int
		reviewVersion
	;
	/**
	 * This will store whether the review
	 * is active or not. As soon as the 
	 * review is reviewed once again the
	 * earlier one will become de-active.
	 */
	private int
		reviewActive
	;	
	/**
	 * This is used to store the date on which it
	 * is created.
	 */
	private Date
		createdOn
	;	
	/**
	 * This is used to store the userid of the user
	 * who has created the template.
	 */
	private int
		createdBy
	;
	/**
	 * This is used to store the name of the user
	 * who has created the template.
	 */
	private String
		createdByName
	;	
	/**
	 * This will store the id of the case that is
	 * getting reviewed.
	 */
	private String	
		caseId
	;
	/**
	 * This will store the version of the case that
	 * is getting reviewed.
	 */
	private String
		caseVersion
	;
	/**
	 * This will store the date on which the case
	 * is received in the safety db.
	 */
	private Date
		caseRecvDate
	;
	/**
	 * This will store the seriousness of the case
	 * that is getting reviewed.
	 */
	private String
		caseSeriousness
	;
	/**
	 * This will store the id of the associate
	 * who is processing the case.
	 */
	private int
		caseAssociate
	;
	/**
	 * This will store the name of the associate
	 * who is processing the case.
	 */
	private String
		caseAssociateName
	;
	/**
	 * This will store the id of the associate
	 * who is processing the case.
	 */
	private int
		medicalReviewerId
	;
	/**
	 * This will store the name of the associate
	 * who is processing the case.
	 */
	private String
		medicalReviewerName
	;
	
	/**
	 * This will store the number of fields 
	 * reviewed for the case.
	 */
	private int
		fieldsReviewed
	;
	/**
	 * This will store the number of fields 
	 * without error for the case that was
	 * reviewed.
	 */
	private int
		fieldsWithoutError
	;
	/**
	 * This will store the number of fields in 
	 * error for the case that was
	 * reviewed.
	 */
	private int
		fieldsError
	;
	/**
	 * This will store the quality of the case
	 * that was reviewed.
	 */
	private double
		caseQuality
	;
	/**
	 * This will store the quality of the cat 1
	 * fields that were reviewed.
	 */
	private double
		caseCat1Quality
	;
	/**
	 * This will store the quality of the cat 2
	 * fields that were reviewed.
	 */
	private double
		caseCat2Quality
	;
	/**
	 * This will store the quality of the cat 3
	 * fields that were reviewed.
	 */
	private double
		caseCat3Quality
	;
	private int
		cat1FieldsReviewed
	;
	private int
		cat2FieldsReviewed
	;
	private int
		cat3FieldsReviewed
	;
	private int
		cat1FieldsWithoutError
	;
	private int
		cat2FieldsWithoutError
	;
	private int
		cat3FieldsWithoutError
	;
	
	/**
	 * This will store the result of the case
	 * that was reviewed.
	 */	
	private String
		caseResult
	;
	/**
	 * This will store the sequence id of the
	 * fields content stored.
	 */
	private int
		contentSeqId
	;	
	/**
	 * This will store the list of all the fields
	 * that were part of the review.
	 */
	private HashMap<String,QRReviewFieldDetails>
		qrlReviewFieldDetails
	;
	private Map<String, ArrayList<SafetyDbCaseFields>>
		safetyDbCaseFieldList
	;	
	
	/**
	 * This will be used to store the json data
	 * sent from UI.
	 */
	private String
		fieldDetailsJson
	;	
	
	/**
	 * This is used to store the regulator
	 * class name.
	 */
	private String
		regClass
	;

	/**
	 * This will be used to store the summary for the 
	 * case.
	 */
	private String
		caseReviewSummary
	;
	
	private int
	cdbrflag
	;	
	
	
	public int getCdbrflag() {
		return cdbrflag;
	}
	public void setCdbrflag(int cdbrflag) {
		this.cdbrflag = cdbrflag;
	}
	public int getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(int sequenceId) {
		this.sequenceId = sequenceId;
	}
	public QRTemplate getQrTemplate() {
		return qrTemplate;
	}
	public void setQrTemplate(QRTemplate qrTemplate) {
		this.qrTemplate = qrTemplate;
	}
	public SafetyDbCaseFieldsTemplate getScft() {
		return scft;
	}
	public void setScft(SafetyDbCaseFieldsTemplate scft) {
		this.scft = scft;
	}
	public String getReviewType() {
		return reviewType;
	}
	public void setReviewType(String reviewType) {
		this.reviewType = reviewType;
	}
	public int getReviewRound() {
		return reviewRound;
	}
	public void setReviewRound(int reviewRound) {
		this.reviewRound = reviewRound;
	}
	public int getReviewVersion() {
		return reviewVersion;
	}
	public void setReviewVersion(int reviewVersion) {
		this.reviewVersion = reviewVersion;
	}
	public int getReviewActive() {
		return reviewActive;
	}
	public void setReviewActive(int reviewActive) {
		this.reviewActive = reviewActive;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getCaseVersion() {
		return caseVersion;
	}
	public void setCaseVersion(String caseVersion) {
		this.caseVersion = caseVersion;
	}
	public Date getCaseRecvDate() {
		return caseRecvDate;
	}
	public void setCaseRecvDate(Date caseRecvDate) {
		this.caseRecvDate = caseRecvDate;
	}
	public String getCaseSeriousness() {
		return caseSeriousness;
	}
	public void setCaseSeriousness(String caseSeriousness) {
		this.caseSeriousness = caseSeriousness;
	}
	public int getCaseAssociate() {
		return caseAssociate;
	}
	public void setCaseAssociate(int caseAssociate) {
		this.caseAssociate = caseAssociate;
	}
	public String getCaseAssociateName() {
		return caseAssociateName;
	}
	public void setCaseAssociateName(String caseAssociateName) {
		this.caseAssociateName = caseAssociateName;
	}
	public int getMedicalReviewerId() {
		return medicalReviewerId;
	}
	public void setMedicalReviewerId(int medicalReviewerId) {
		this.medicalReviewerId = medicalReviewerId;
	}
	public String getMedicalReviewerName() {
		return medicalReviewerName;
	}
	public void setMedicalReviewerName(String medicalReviewerName) {
		this.medicalReviewerName = medicalReviewerName;
	}
	public int getFieldsReviewed() {
		return fieldsReviewed;
	}
	public void setFieldsReviewed(int fieldsReviewed) {
		this.fieldsReviewed = fieldsReviewed;
	}
	public int getFieldsWithoutError() {
		return fieldsWithoutError;
	}
	public void setFieldsWithoutError(int fieldsWithoutError) {
		this.fieldsWithoutError = fieldsWithoutError;
	}
	public int getFieldsError() {
		return fieldsError;
	}
	public void setFieldsError(int fieldsError) {
		this.fieldsError = fieldsError;
	}
	public double getCaseQuality() {
		return caseQuality;
	}
	public void setCaseQuality(double caseQuality) {
		this.caseQuality = caseQuality;
	}
	public double getCaseCat1Quality() {
		return caseCat1Quality;
	}
	public void setCaseCat1Quality(double caseCat1Quality) {
		this.caseCat1Quality = caseCat1Quality;
	}
	public double getCaseCat2Quality() {
		return caseCat2Quality;
	}
	public void setCaseCat2Quality(double caseCat2Quality) {
		this.caseCat2Quality = caseCat2Quality;
	}
	public double getCaseCat3Quality() {
		return caseCat3Quality;
	}
	public void setCaseCat3Quality(double caseCat3Quality) {
		this.caseCat3Quality = caseCat3Quality;
	}
	public String getCaseResult() {
		return caseResult;
	}
	public void setCaseResult(String caseResult) {
		this.caseResult = caseResult;
	}
	public int getContentSeqId() {
		return contentSeqId;
	}
	public void setContentSeqId(int contentSeqId) {
		this.contentSeqId = contentSeqId;
	}
	public HashMap<String,QRReviewFieldDetails> getQrlReviewFieldDetails() {
		return qrlReviewFieldDetails;
	}
	public void setQrlReviewFieldDetails(HashMap<String,QRReviewFieldDetails> qrlReviewFieldDetails) {
		this.qrlReviewFieldDetails = qrlReviewFieldDetails;
	}
	public String getFieldDetailsJson() {
		return fieldDetailsJson;
	}
	public void setFieldDetailsJson(String fieldDetailsJson) {
		this.fieldDetailsJson = fieldDetailsJson;
	}
	public Map<String, ArrayList<SafetyDbCaseFields>> getSafetyDbCaseFieldList() {
		return safetyDbCaseFieldList;
	}
	public void setSafetyDbCaseFieldList(Map<String, ArrayList<SafetyDbCaseFields>> safetyDbCaseFieldList) {
		this.safetyDbCaseFieldList = safetyDbCaseFieldList;
	}
	public int getCat1FieldsReviewed() {
		return cat1FieldsReviewed;
	}
	public void setCat1FieldsReviewed(int cat1FieldsReviewed) {
		this.cat1FieldsReviewed = cat1FieldsReviewed;
	}
	public int getCat2FieldsReviewed() {
		return cat2FieldsReviewed;
	}
	public void setCat2FieldsReviewed(int cat2FieldsReviewed) {
		this.cat2FieldsReviewed = cat2FieldsReviewed;
	}
	public int getCat3FieldsReviewed() {
		return cat3FieldsReviewed;
	}
	public void setCat3FieldsReviewed(int cat3FieldsReviewed) {
		this.cat3FieldsReviewed = cat3FieldsReviewed;
	}
	public int getCat1FieldsWithoutError() {
		return cat1FieldsWithoutError;
	}
	public void setCat1FieldsWithoutError(int cat1FieldsWithoutError) {
		this.cat1FieldsWithoutError = cat1FieldsWithoutError;
	}
	public int getCat2FieldsWithoutError() {
		return cat2FieldsWithoutError;
	}
	public void setCat2FieldsWithoutError(int cat2FieldsWithoutError) {
		this.cat2FieldsWithoutError = cat2FieldsWithoutError;
	}
	public int getCat3FieldsWithoutError() {
		return cat3FieldsWithoutError;
	}
	public void setCat3FieldsWithoutError(int cat3FieldsWithoutError) {
		this.cat3FieldsWithoutError = cat3FieldsWithoutError;
	}
	public String getCaseReviewSummary() {
		return caseReviewSummary;
	}
	public void setCaseReviewSummary(String caseReviewSummary) {
		this.caseReviewSummary = caseReviewSummary;
	}

	public String getRegClass() {
		return regClass;
	}
	public void setRegClass(String regClass) {
		this.regClass = regClass;
	}
}