package com.sciformix.sciportal.apps.qr;

import java.util.HashMap;

public class QRFieldErrorCount {
	private HashMap<String,Integer>
		statFieldError
	;
	public HashMap<String, Integer> getStatFieldError() {
		return statFieldError;
	}
	public void setStatFieldError(HashMap<String, Integer> statFieldError) {
		this.statFieldError = statFieldError;
	}
}
