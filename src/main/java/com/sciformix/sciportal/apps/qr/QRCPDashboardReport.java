package com.sciformix.sciportal.apps.qr;

public class QRCPDashboardReport {
	private String 
		userDisplayName
	;
	private int
		totalCaseReviewed
	;
	private int
		totalCasePassed
	;
	private int
		totalCaseWithoutError
	;
	private int
		totalFieldsReviewed
	;
	private int
		totalFieldsPassed
	;
	private int
		totalFieldsFailed
	;
	private double
		totalCat1Quality
	;
	private double
		totalCat2Quality
	;
	private double
		totalCat3Quality
	;	
	private double
		totalCaseQuality
	;
	private double
		totalAvgQuality
	;	
	private int
		totalCat1FieldsReviewed
	;
	private int
		totalCat2FieldsReviewed
	;
	private int
		totalCat3FieldsReviewed
	;
	private int
		totalCat1FieldsError
	;
	private int
		totalCat2FieldsError
	;
	private int
		totalCat3FieldsError
	;
	
	public int getTotalCaseWithoutError() {
		return totalCaseWithoutError;
	}
	public void setTotalCaseWithoutError(int totalCaseWithoutError) {
		this.totalCaseWithoutError = totalCaseWithoutError;
	}
	public double getTotalCat1Quality() {
		return totalCat1Quality;
	}
	public void setTotalCat1Quality(double totalCat1Quality) {
		this.totalCat1Quality = totalCat1Quality;
	}
	public double getTotalCat2Quality() {
		return totalCat2Quality;
	}
	public void setTotalCat2Quality(double totalCat2Quality) {
		this.totalCat2Quality = totalCat2Quality;
	}
	public double getTotalCat3Quality() {
		return totalCat3Quality;
	}
	public void setTotalCat3Quality(double totalCat3Quality) {
		this.totalCat3Quality = totalCat3Quality;
	}
	public double getTotalCaseQuality() {
		return totalCaseQuality;
	}
	public void setTotalCaseQuality(double toalCaseQuality) {
		this.totalCaseQuality = toalCaseQuality;
	}
	public double getTotalAvgQuality() {
		return totalAvgQuality;
	}
	public void setTotalAvgQuality(double totalAvgQuality) {
		this.totalAvgQuality = totalAvgQuality;
	}
	public int getTotalCat1FieldsReviewed() {
		return totalCat1FieldsReviewed;
	}
	public void setTotalCat1FieldsReviewed(int totalCat1FieldsReviewed) {
		this.totalCat1FieldsReviewed = totalCat1FieldsReviewed;
	}
	public int getTotalCat2FieldsReviewed() {
		return totalCat2FieldsReviewed;
	}
	public void setTotalCat2FieldsReviewed(int totalCat2FieldsReviewed) {
		this.totalCat2FieldsReviewed = totalCat2FieldsReviewed;
	}
	public int getTotalCat3FieldsReviewed() {
		return totalCat3FieldsReviewed;
	}
	public void setTotalCat3FieldsReviewed(int totalCat3FieldsReviewed) {
		this.totalCat3FieldsReviewed = totalCat3FieldsReviewed;
	}
	public int getTotalCat1FieldsError() {
		return totalCat1FieldsError;
	}
	public void setTotalCat1FieldsError(int totalCat1FieldsError) {
		this.totalCat1FieldsError = totalCat1FieldsError;
	}
	public int getTotalCat2FieldsError() {
		return totalCat2FieldsError;
	}
	public void setTotalCat2FieldsError(int totalCat2FieldsError) {
		this.totalCat2FieldsError = totalCat2FieldsError;
	}
	public int getTotalCat3FieldsError() {
		return totalCat3FieldsError;
	}
	public void setTotalCat3FieldsError(int totalCat3FieldsError) {
		this.totalCat3FieldsError = totalCat3FieldsError;
	}
	public String getUserDisplayName() {
		return userDisplayName;
	}
	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}
	public int getTotalCaseReviewed() {
		return totalCaseReviewed;
	}
	public void setTotalCaseReviewed(int totalCaseReviewed) {
		this.totalCaseReviewed = totalCaseReviewed;
	}
	public int getTotalCasePassed() {
		return totalCasePassed;
	}
	public void setTotalCasePassed(int totalCasePassed) {
		this.totalCasePassed = totalCasePassed;
	}
	public int getTotalFailedCount() {
		return totalCaseWithoutError;
	}
	public void setTotalFailedCount(int totalFailedCount) {
		this.totalCaseWithoutError = totalFailedCount;
	}
	
	public int getTotalFieldsReviewed() {
		return totalFieldsReviewed;
	}
	public void setTotalFieldsReviewed(int totalFieldsReviewed) {
		this.totalFieldsReviewed = totalFieldsReviewed;
	}
	public int getTotalFieldsPassed() {
		return totalFieldsPassed;
	}
	public void setTotalFieldsPassed(int totalFieldsPassed) {
		this.totalFieldsPassed = totalFieldsPassed;
	}
	public int getTotalFieldsFailed() {
		return totalFieldsFailed;
	}
	public void setTotalFieldsFailed(int totalFieldsFailed) {
		this.totalFieldsFailed = totalFieldsFailed;
	}	
}
