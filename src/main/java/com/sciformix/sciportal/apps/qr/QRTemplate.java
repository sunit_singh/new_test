package com.sciformix.sciportal.apps.qr;

import java.util.List;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes.Common;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.config.ConfigTplObj;

public class QRTemplate extends ConfigTplObj {
	
	private static final int 
		UDFINDEX_SCFTCTOSEQID				=	0
	;
	private static final int 
		UDFINDEX_TEMPLATECASEFIELD_MAPPING	=	1
	;
	/**
	 * This will store whether the reviewer
	 * reviewing the case is allowed to correct
	 * the fields in error or not.
	 */
	private int
		rvwAllowedToCorrect
	;
	/**
	 * This will store whether the summary after
	 * the completion of review needs to be 
	 * generated or not.
	 * The values allowed are:
	 * 1 : Required
	 * 2 : Not-Required
	 */
	
	/**
	 * This will store the field mnemonics
	 * that are mapped to this template.
	 */
	private List<String>
		templateCaseFields
	;
	/**
	 * 
	 */
	private int
		caseResultCriteriaMode
	;	
	private String
		caseResultCriteria
	;	
	/**
	 * 
	 */
	private double
		caseQualityThreshold
	;	
	/**
	 * 
	 */
	private double
		cat1QualityThreshold
	;
	/**
	 * 
	 */
	private double
		cat2QualityThreshold
	;
	/**
	 * 
	 */
	private double
		cat3QualityThreshold
	;

	/**
	 * 
	 */
	private String
		caseClassificationOverall
	;
	/**
	 * 
	 */
	private String
		caseClassificationCat1
	;
	/**
	 * 
	 */
	private String
		caseClassificationCat2
	;
	/**
	 * 
	 */
	private String
		caseClassificationCat3
	;
	/**
	 * Download Review Template sequence Id 
	 */
	private int
		downloadReviewTemplateSeqId;
	;
	/**
	 * 
	 * @param p_scftseqid
	 * @throws SciException
	 */
	public QRTemplate (
		int	p_scftseqid
	,	int	p_projectid	
	) throws SciException {
		this();
		setUdf(
			UDFINDEX_SCFTCTOSEQID
		, 	String.valueOf(
				p_scftseqid
			)
		);
		setUdf(
			UDFINDEX_TEMPLATECASEFIELD_MAPPING
		, 	String.valueOf(-1)
		);
		rvwAllowedToCorrect			=	0;
		cat1QualityThreshold		=	-1;
		cat2QualityThreshold		=	-1;
		cat3QualityThreshold		=	-1;
		caseQualityThreshold		=	-1;
		setCategoryCriteria(p_projectid);
		caseResultCriteriaMode		=	2;
		downloadReviewTemplateSeqId 		=	-1;
	}
	
	/**
	 * 
	 */
	public QRTemplate() {
		super(ConfigTplObjType.QRT);
	}
	
	
	private void setCategoryCriteria (int p_projectid) {
		String		l_cataliases	=	null;
		String []	l_categories	=	null;	
		int			l_catcount		=	-1;
		
		l_cataliases	=	ConfigHome.getPrjConfigSettings(QRConstant.APP_NAME+StringConstants.PERIOD+QRConstant.PROP_PROJ_APPLICABLE_CAT, p_projectid);
		if (StringUtils.isNullOrEmpty(l_cataliases)) {
			
		} else {
			l_categories	=	l_cataliases.split("~");
			l_catcount		=	l_categories.length;
			if (l_catcount == 1) {
				
			} else if (l_catcount == 2) {
				cat1QualityThreshold	=	100;
				cat2QualityThreshold	=	100;
			} else if (l_catcount == 3) {
				cat1QualityThreshold	=	100;
				cat2QualityThreshold	=	100;
				cat3QualityThreshold	=	100;
			}
		}
		caseQualityThreshold	=	100;
	}
	/**
	 * This will fetch all the fields mapped to
	 * the template.
	 * @return	List of Field Mnemonic
	 */
	public List<String> getTemplateCaseFields() {
		return templateCaseFields;
	}
	/**
	 * This will set all the fields that will be
	 * part of this template
	 * @param templateCaseFields	Fields that should
	 * 								be mapped to this template.
	 */
	public void setTemplateCaseFields(
		List<String> templateCaseFields
	) {
		this.templateCaseFields = templateCaseFields;
	}
	
	/**
	 * This will return the SCFT sequence id which is used
	 * to create this template.
	 * @return	SCFT sequence id.
	 * @throws SciServiceException 
	 * @throws NumberFormatException	
	 * @throws SciException
	 */
	public int getSCFTSeqId () throws SciServiceException {
		try {
		return Integer.parseInt(
			getUdf(
				UDFINDEX_SCFTCTOSEQID
			)
		);
		} catch (NumberFormatException | SciException e) {
			//TODO: Change the error code
			throw new SciServiceException(Common.ERROR_ACTIVE_TEMPLATE_NOT_FOUND);
		}
	}
	
	/**
	 * This will return the bin content id which stores the template
	 * and case field mapping.
	 * @return	Bin Content Id.
	 * @throws SciException
	 */
	public int getTemplateCaseFieldMapping () throws SciException {
		return Integer.parseInt(
			getUdf(
				UDFINDEX_TEMPLATECASEFIELD_MAPPING
			)
		);	
	}
	
	/**
	 * This will set the id of the bin content which is
	 * storing the template and case field mapping.
	 * @param p_templatecasefieldmapping	
	 * @throws SciException
	 */
	public void setTemplateCaseFieldMapping (
		int	p_contentid
	) throws SciException {
		setUdf(
			UDFINDEX_TEMPLATECASEFIELD_MAPPING
		, 	String.valueOf(p_contentid)
		);
	}
	/**
	 * This method will return the value true or false
	 * in database for the field "Reviewer Allowed to Correct".
	 * @return	True if the reviewer is allowed to correct otherwise false.
	 */
	public int getRvwAllToCorrect() {
		return rvwAllowedToCorrect;
	}
	/**
	 * This method will set the value 1 or 0
	 * in database for the field "Reviewer Allowed to Correct".
	 * @param isRvwAllToCorrect 1 if the reviewer is allowed
	 * 							to correct otherwise 0.	
	 */
	public void setRvwAllToCorrect(
		int p_rvwallowedtocorrect
	) {
		this.rvwAllowedToCorrect = p_rvwallowedtocorrect;
	}
	/**
	 * 
	 * @return
	 */
	public double getCaseQualityThreshold() {
		return caseQualityThreshold;
	}
	/**
	 * 
	 * @param caseQualityThreshold
	 */
	public void setCaseQualityThreshold(double caseQualityThreshold) {
		this.caseQualityThreshold = caseQualityThreshold;
	}
	/**
	 * 
	 * @return
	 */
	public double getCat1QualityThreshold() {
		return cat1QualityThreshold;
	}
	/**
	 * 
	 * @param cat1QualityThreshold
	 */
	public void setCat1QualityThreshold(double cat1QualityThreshold) {
		this.cat1QualityThreshold = cat1QualityThreshold;
	}
	/**
	 * 
	 * @return
	 */
	public double getCat2QualityThreshold() {
		return cat2QualityThreshold;
	}
	/**
	 * 
	 * @param cat2QualityThreshold
	 */
	public void setCat2QualityThreshold(double cat2QualityThreshold) {
		this.cat2QualityThreshold = cat2QualityThreshold;
	}
	/**
	 * 
	 * @return
	 */
	public double getCat3QualityThreshold() {
		return cat3QualityThreshold;
	}
	/**
	 * 
	 * @param cat3QualityThreshold
	 */
	public void setCat3QualityThreshold(double cat3QualityThreshold) {
		this.cat3QualityThreshold = cat3QualityThreshold;
	}
	/**
	 * 
	 * @return
	 */
	public String getCaseClassificationOverall() {
		return caseClassificationOverall;
	}
	/**
	 * 
	 * @param caseClassificationOverall
	 */
	public void setCaseClassificationOverall(String caseClassificationOverall) {
		this.caseClassificationOverall = caseClassificationOverall;
	}
	public int getDownloadReviewTemplateSeqId() {
		return downloadReviewTemplateSeqId;
	}

	public void setDownloadReviewTemplateSeqId(int reviewTemplateSeqId) {
		this.downloadReviewTemplateSeqId = reviewTemplateSeqId;
	}

	/**
	 * 
	 * @return
	 */
	public String getCaseClassificationCat1() {
		return caseClassificationCat1;
	}
	/**
	 * 
	 * @param caseClassificationCat1
	 */
	public void setCaseClassificationCat1(String caseClassificationCat1) {
		this.caseClassificationCat1 = caseClassificationCat1;
	}
	/**
	 * 
	 * @return
	 */
	public String getCaseClassificationCat2() {
		return caseClassificationCat2;
	}
	/**
	 * 
	 * @param caseClassificationCat2
	 */
	public void setCaseClassificationCat2(String caseClassificationCat2) {
		this.caseClassificationCat2 = caseClassificationCat2;
	}
	/**
	 * 
	 * @return
	 */
	public String getCaseClassificationCat3() {
		return caseClassificationCat3;
	}
	/**
	 * 
	 * @param caseClassificationCat3
	 */
	public void setCaseClassificationCat3(String caseClassificationCat3) {
		this.caseClassificationCat3 = caseClassificationCat3;
	}
	
	public int getCaseResultCriteriaMode() {
		return caseResultCriteriaMode;
	}

	public void setCaseResultCriteriaMode(int caseResultCriteriaMode) {
		this.caseResultCriteriaMode = caseResultCriteriaMode;
	}
	
	public String getCaseResultCriteria() {
		return caseResultCriteria;
	}

	public void setCaseResultCriteria(String caseResultCriteria) {
		this.caseResultCriteria = caseResultCriteria;
	}
	
}
