package com.sciformix.sciportal.apps.qr;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.CommonAuditHelper;
import com.sciformix.commons.ObjectIdPair;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.EmailSendSetting;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.apps.AppUserPreferenceMetadata;
import com.sciformix.sciportal.apps.BasePortalApp;
import com.sciformix.sciportal.audit.AuditHome;
import com.sciformix.sciportal.audit.AuditInfoDb;
import com.sciformix.sciportal.config.ConfigHome;
import com.sciformix.sciportal.config.ConfigTplObj;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.content.BinaryContent;
import com.sciformix.sciportal.content.BinaryContentHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.mail.MailRuntimeHelper;
import com.sciformix.sciportal.pvcp.PvcpCaseMaster;
import com.sciformix.sciportal.safetydb.SafetyDBCaseFieldCategoryType;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFields;
import com.sciformix.sciportal.safetydb.SafetyDbCaseFieldsTemplate;
import com.sciformix.sciportal.safetydb.SafetyDbConfigurationHelper;
import com.sciformix.sciportal.user.UserHome;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.user.UserUtils;
import com.sciformix.sciportal.web.apps.UploadedFileWrapper;

public class 
	QRPortalApp
extends
	BasePortalApp
{
	private static final Logger
		log	=	LoggerFactory.getLogger(QRPortalApp.class)
	;
	private static final String
		USERPREF_LAYOUT					=	"USERPREF_LAYOUT"
	;
	private static final String
		USERPREF_REVIEWTYPE				=	"USERPREF_REVIEWTYPE"
	;	
	
	public QRPortalApp() {
		super("QR-PORTAL-APP", "QRApp", true);
		List<ObjectIdPair<String, String>>	l_layouttypeprefs	=	new ArrayList<>();
		l_layouttypeprefs.add(new ObjectIdPair<String, String>("Single Page", "Single Page"));
		l_layouttypeprefs.add(new ObjectIdPair<String, String>("Tab", "Tab"));
		addAppUserPreference(
			AppUserPreferenceMetadata.createSingleValuedPreference(
				USERPREF_LAYOUT
			, 	"Layout Type"
			, 	l_layouttypeprefs
			, 	true
			)
		);
	
		AppUserPreferenceMetadata.PreferenceValue callbackFunctionTemplateRulesets = new AppUserPreferenceMetadata.PreferenceValue() 
		{
			public List<ObjectIdPair<String, String>> getPreferenceValuesByProjSeqId(UserSession p_oUserSession , int prjSeqId)
			{
				List<ObjectIdPair<String, String>> 	l_reviewtypelist			=	null;
				String 								l_reviewtypeall				=	null;
				
				l_reviewtypeall	=	ConfigHome.getPrjConfigSettings(
										getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_REVIEW_TYPE
									,	prjSeqId
									);
				ObjectIdPair<String, String> l_reviewtypedata = null;
				
				if (StringUtils.isNullOrEmpty(l_reviewtypeall)) {
				} else {
					String [] l_values	=	null;
					l_reviewtypelist	=	new ArrayList<>();
					for (String l_reviewtype : l_reviewtypeall.split(",")) {
			
						l_values	=	l_reviewtype.split(":");
						l_reviewtypedata	=	new ObjectIdPair<>(l_values[0], l_values[1]);
						l_reviewtypelist.add(l_reviewtypedata);
					}
				}
				return l_reviewtypelist;
			}
		};
		
		addAppUserPreference(
			AppUserPreferenceMetadata.createMultiValuedPreference(
				USERPREF_REVIEWTYPE
			, 	"Review Type"
			, 	callbackFunctionTemplateRulesets
			, 	true
			)
		);
	}
	/**
	 * This method can be used to get the value
	 * maintained in the config setting for the
	 * max number of days that can be view on screen.
	 * @return The property value maintained at the
	 * 			config setting or 0 if the property
	 * 			is not maintained, blank or negative.
	 * @throws NumberFormatException if the max view
	 * 			days maintained cannot be converted
	 * 			into an integer.
	 */
	
	public static int getMaxDaysAllowedForView() {
		int		l_viewmaxdays		=	0;
		String	l_viewmaxdaysprop	=	ConfigHome.getAppConfigSetting(QRConstant.MAX_VIEW_DAYS);
		if (!StringUtils.isNullOrEmpty(l_viewmaxdaysprop)) {
			try {
				l_viewmaxdays	=	Integer.parseInt(l_viewmaxdaysprop);
				if (l_viewmaxdays < 0) {
					l_viewmaxdays = 0;
				}
			} catch (NumberFormatException nfExcep) {
				log.error("Invalid property maintained for max view days. The property value maintained is:"+l_viewmaxdaysprop);
				l_viewmaxdays	=	0;
			}
		}
		return l_viewmaxdays;
	}
	
	/**
	 * This method can be used to get the value
	 * maintained in the config setting for the
	 * max number of days for report can be pulled 
	 * from the system.
	 * @return The property value maintained at the
	 * 			config setting or 0 if the property
	 * 			is not maintained, blank or negative.
	 * @throws NumberFormatException if the max report
	 * 			days maintained cannot be converted
	 * 			into an integer.
	 */
	public static int getMaxDaysAllowedForReport() {
		int		l_reportmaxdays		=	0;
		String	l_reportmaxdaysprop	=	ConfigHome.getAppConfigSetting(QRConstant.MAX_REPORT_DAYS);
		if (!StringUtils.isNullOrEmpty(l_reportmaxdaysprop)) {
			try {
				l_reportmaxdays	=	Integer.parseInt(l_reportmaxdaysprop);
				if (l_reportmaxdays < 0) {
					l_reportmaxdays = 0;
				}
			} catch (NumberFormatException nfExcep) {
				log.error("Invalid property maintained for max report days. The property value maintained is:"+l_reportmaxdaysprop);
				l_reportmaxdays	=	0;
			}
		}
		return l_reportmaxdays;
	}
	
	public List<ObjectIdPair<String, String>> getReviewTypeUserList(
		int			p_projectid
	,	UserSession	p_usersession	
	) {
		String 								l_reviewtypeall		=	null;
		ObjectIdPair<String, String> 		l_reviewtypedata 	= 	null;
		String								l_userreviewtype	=	null;
		Set<String>							l_userreviewtypeset	=	null;
		List<ObjectIdPair<String, String>> 	l_reviewtypelist	=	null;
		
		l_userreviewtype	=	p_usersession.getUserPreference(p_projectid, getPortalAppId(), USERPREF_REVIEWTYPE);
		if (StringUtils.isNullOrEmpty(l_userreviewtype)) {
			return null;
		} else {
			l_userreviewtypeset	=	new HashSet<>();
			for (String l_reviewtype : l_userreviewtype.split("~")) {
				l_userreviewtypeset.add(l_reviewtype);
			}
		}
		l_reviewtypeall	=	ConfigHome.getPrjConfigSettings(
								getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_REVIEW_TYPE
							,	p_projectid
							);
		
		if (StringUtils.isNullOrEmpty(l_reviewtypeall)) {
			return null;
		} else {
			String [] l_values	=	null;
			l_reviewtypelist	=	new ArrayList<>();
			for (String l_reviewtype : l_reviewtypeall.split(",")) {
				
				l_values	=	l_reviewtype.split(":");
				if (l_userreviewtypeset.contains(l_values[0])) {
					l_reviewtypedata	=	new ObjectIdPair<>(l_values[0], l_values[1]);
					l_reviewtypelist.add(l_reviewtypedata);
				}
			}
		}
		return l_reviewtypelist;
	}
	
	public List<ObjectIdPair<String, String>> getReviewTypeProjectList(
		int	p_projectid	
	) throws SciServiceException {
		try {
			return QRPortalHomeImpl.getReviewTypeProjectList(p_projectid);
		}	
		catch (SciException sciExcep) {
			throw new SciServiceException(ServiceErrorCodes.QR.REVIEW_TYPE_LIST_NOT_AVAILABLE);
		}
	}
	/**
	 * This is to check whether the template name
	 * is unique or not.
	 * @param p_projectid
	 * @param p_proposedctoname
	 * @return
	 * @throws SciServiceException
	 */
	public boolean isObjectNameUnique (
		int		p_projectid
	,	String	p_proposedctoname
	) throws SciServiceException {
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			return	ConfigTplObjHomeImpl.isConfigTplObjNameUnique(
						connection
					, 	p_projectid
					, 	ConfigTplObjType.QRT
					, 	p_proposedctoname
					);
		}
		catch (SciException sciExcep)
		{
			log.error("Error checking config template object name uniqueness", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_CHECKING_TEMPLATE_NAME_UNIQUE, sciExcep);
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error checking config template object name uniqueness", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_CHECKING_TEMPLATE_NAME_UNIQUE, dbExcep);
		}
	}
	
	/**
	 * This method will save and update the QRT template created
	 * with default values. It will insert the default values in
	 * the attribute table of QRT.
	 * <ul> Default values will be:
	 * 	<li>
	 * 		Review Completion Notification will be set to None.
	 * 	</li>
	 * <li>
	 * 		Review Completion Notification CC will be null.
	 * 	</li>
	 * <li>
	 * 		Reviewer Allowed to Correct will be false.
	 * 	</li>
	 * <li>
	 * 		Review Summary Generation will be set to false.
	 * 	</li>
	 * <li>
	 * 		Review Summary Generation Template will be null.
	 * 	</li>
	 * <li>
	 * 		None of the SCFT fields will be marked.
	 * 	</li>
	 * </ul>
	 * @param p_projectid
	 * @param p_templatename
	 * @param p_usersession
	 * @return
	 * @throws SciServiceException
	 */
	public QRTemplate saveTemplate (
		int			p_projectid
	,	String		p_templatename
	,	int			p_scftseqid
	,	UserSession	p_usersession
	) throws SciServiceException {
		
		QRTemplate	l_qrtemplate	=	null;
		AuditInfoDb	l_auditinfo		=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_qrtemplate	=	new QRTemplate(
										p_scftseqid
									,	p_projectid
									);
				l_qrtemplate.setProjectId(p_projectid);
				l_qrtemplate.setTemplateName(p_templatename);
				l_qrtemplate.setState(RecordState.UNDER_CONFIGURATION);
				l_qrtemplate.setCreationMode(CreationMode.SCRATCH);
				
				ConfigTplObjHomeImpl.saveConfigTplObj(
					connection
				, 	l_qrtemplate
				,	p_usersession.getUserInfo()
				);
				l_auditinfo	=	AuditHome.createRecordInsertStub(
									p_usersession
								, 	this.getPortalAppId()
								, 	"Template Created"
								,	p_projectid
								);
				l_auditinfo.addPayload(
					CommonAuditHelper.AuditAttributes.TEMPLATE_ID
				, 	l_qrtemplate.getSequenceId()
				);
				l_auditinfo.addPayload(
					CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
				, 	l_qrtemplate.getTemplateName()
				);
				QRPortalHomeImpl.saveQRTAttributes(
					connection
				, 	l_qrtemplate
				);
				AuditHome.savePopulatedStub(connection, l_auditinfo);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error Saving quality review template data into db "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.ProjectSetting.ERROR_CREATING_TEMPLATE
				, 	sciExcep
				);
			} 	
		} catch (SciDbException dbExcep) {
			log.error(
				"Error Saving quality review template data into db "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_CREATING_TEMPLATE
			, 	dbExcep
			);
		}
		return l_qrtemplate;
	}
	
	/**
	 * @param l_caseclassificationcat3 
	 * @param l_caseclassificationcat2 
	 * @param l_caseclassificationcat1 
	 * @param l_caseclassificationoverall 
	 * @param l_cat3qualitythreshold 
	 * @param l_cat2qualitythreshold 
	 * @param l_cat1qualitythreshold 
	 * @param l_casequalitythreshold 
	 * @param l_correctionallowed 
	 * @param l_emailccaddress 
	 * @param l_emailsendsetting 
	 * @throws SciServiceException 
	 * 
	 */
	public void updateQRTAttributes (
		int			p_projectid	
	,	int 		p_templateseqid
	, 	int 		p_correctionallowed
	, 	double 		p_casequalitythreshold
	, 	double 		p_cat1qualitythreshold
	, 	double 		p_cat2qualitythreshold
	, 	double 		p_cat3qualitythreshold
	, 	String 		p_caseclassificationoverall
	, 	String 		p_caseclassificationcat1
	, 	String 		p_caseclassificationcat2
	, 	String 		p_caseclassificationcat3
	,	int			p_casecriteriamode
	,	String		p_casecriteria
	,	int			p_reviewsummrequired
	, 	UserSession	p_usersession, UploadedFileWrapper oUploadedFileObject	
	) throws SciServiceException {
		QRTemplate			l_qrtemplate			=	null;
		boolean				l_updaterequired		=	false;	
		AuditInfoDb			l_auditinfo				=	null;
		String sTemplateContent = null;
		File file = null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_qrtemplate	=	getQRTDetails(p_templateseqid);
				l_auditinfo		=	AuditHome.createRecordUpdateStub(
										p_usersession
									, 	this.getPortalAppId()
									, 	"Template Modified"
									,	p_projectid
									);
				l_auditinfo.addPayload(
					CommonAuditHelper.AuditAttributes.TEMPLATE_ID
				, 	l_qrtemplate.getSequenceId()
				);
				l_auditinfo.addPayload(
					CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
				, 	l_qrtemplate.getTemplateName()
				);
				if (!(l_qrtemplate.getRvwAllToCorrect() == p_correctionallowed)) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.RVW_ALLOWED_TO_CORRECT
					, 	(l_qrtemplate.getRvwAllToCorrect() == 1) ? "True" : "False"
					, 	(p_correctionallowed == 1) ? "True" : "False"
					);
				}
				if (!(l_qrtemplate.getCaseResultCriteriaMode() == p_casecriteriamode)) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CRITERIA_MODE
					, 	(l_qrtemplate.getCaseResultCriteriaMode() == 1) ? "Number" : "Percent"
					, 	(p_casecriteriamode == 1) ? "Number" : "Percent"
					);
				}
				if (!(l_qrtemplate.getCaseQualityThreshold() == p_casequalitythreshold)) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_QUALITY_THRESHOLD
					, 	String.valueOf(l_qrtemplate.getCaseQualityThreshold())
					, 	String.valueOf(p_casequalitythreshold)
					);
				}
				if (!(l_qrtemplate.getCat1QualityThreshold() == p_cat1qualitythreshold)) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CAT1_QUALITY_THRESHOLD
					, 	String.valueOf(l_qrtemplate.getCat1QualityThreshold())
					, 	String.valueOf(p_cat1qualitythreshold)
					);
				}
				if (!(l_qrtemplate.getCat2QualityThreshold() == p_cat2qualitythreshold)) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CAT2_QUALITY_THRESHOLD
					, 	String.valueOf(l_qrtemplate.getCat2QualityThreshold())
					, 	String.valueOf(p_cat2qualitythreshold)
					);
				}
				if (!(l_qrtemplate.getCat3QualityThreshold() == p_cat3qualitythreshold)) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CAT3_QUALITY_THRESHOLD
					, 	String.valueOf(l_qrtemplate.getCat3QualityThreshold())
					, 	String.valueOf(p_cat3qualitythreshold)
					);
				}
				if ( StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationOverall()) && StringUtils.isNullOrEmpty(p_caseclassificationoverall)) {
					//Nothing to do
				} else if ((!StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationOverall())) &&
					(!l_qrtemplate.getCaseClassificationOverall().equals(p_caseclassificationoverall))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_OVERALL
					, 	l_qrtemplate.getCaseClassificationOverall()
					, 	p_caseclassificationoverall
					);
				} else if ((!StringUtils.isNullOrEmpty(p_caseclassificationoverall)) && 
					(!p_caseclassificationoverall.equals(l_qrtemplate.getCaseClassificationOverall()))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_OVERALL
					, 	l_qrtemplate.getCaseClassificationOverall()
					, 	p_caseclassificationoverall
					);
					
				}
				if ( StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationCat1()) && StringUtils.isNullOrEmpty(p_caseclassificationcat1)) {
					//Nothing to do
				} else if ((!StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationCat1())) &&
					(!l_qrtemplate.getCaseClassificationCat1().equals(p_caseclassificationcat1))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_CAT1
					, 	l_qrtemplate.getCaseClassificationCat1()
					, 	p_caseclassificationcat1
					);
				} else if ((!StringUtils.isNullOrEmpty(p_caseclassificationcat1)) && 
					(!p_caseclassificationcat1.equals(l_qrtemplate.getCaseClassificationCat1()))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_CAT1
					, 	l_qrtemplate.getCaseClassificationCat1()
					, 	p_caseclassificationcat1
					);
					
				}
				if ( StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationCat2()) && StringUtils.isNullOrEmpty(p_caseclassificationcat2)) {
					//Nothing to do
				} else if ((!StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationCat2())) &&
					(!l_qrtemplate.getCaseClassificationCat2().equals(p_caseclassificationcat2))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_CAT2
					, 	l_qrtemplate.getCaseClassificationCat2()
					, 	p_caseclassificationcat2
					);
				} else if ((!StringUtils.isNullOrEmpty(p_caseclassificationcat2)) && 
					(!p_caseclassificationcat2.equals(l_qrtemplate.getCaseClassificationCat2()))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_CAT2
					, 	l_qrtemplate.getCaseClassificationCat2()
					, 	p_caseclassificationcat2
					);
					
				}
				if ( StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationCat3()) && StringUtils.isNullOrEmpty(p_caseclassificationcat3)) {
					//Nothing to do
				} else if ((!StringUtils.isNullOrEmpty(l_qrtemplate.getCaseClassificationCat3())) &&
					(!l_qrtemplate.getCaseClassificationCat3().equals(p_caseclassificationcat3))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_CAT3
					, 	l_qrtemplate.getCaseClassificationCat3()
					, 	p_caseclassificationcat3
					);
				} else if ((!StringUtils.isNullOrEmpty(p_caseclassificationcat3)) && 
					(!p_caseclassificationcat3.equals(l_qrtemplate.getCaseClassificationCat3()))) {
					l_updaterequired	=	true;
					l_auditinfo.addDetails(
						QRAuditHelper.AuditAttributes.CASE_CLASSIFICATION_CAT3
					, 	l_qrtemplate.getCaseClassificationCat3()
					, 	p_caseclassificationcat3
					);
					
				}
				if(oUploadedFileObject!=null && oUploadedFileObject.isFileAvailable())
				{
					Base64.Encoder encoder = Base64.getEncoder();
					file= new File(oUploadedFileObject.getStagedFilePath());
					sTemplateContent = encoder.encodeToString(FileUtils.readFileToByteArray(file));
				}
				if (l_updaterequired) {
					p_casecriteria	=	QRPortalHomeImpl.createCaseResultCriteria(
											p_casequalitythreshold
										,	p_cat1qualitythreshold
										,	p_cat2qualitythreshold
										,	p_cat3qualitythreshold
										,	p_casecriteriamode
										,	p_projectid
										);
					QRPortalHomeImpl.updateQRTAttributeDetails(
						connection
					, 	p_templateseqid
					,	p_correctionallowed
					,	p_casequalitythreshold
					,	p_cat1qualitythreshold
					,	p_cat2qualitythreshold
					,	p_cat3qualitythreshold
					,	p_caseclassificationoverall
					,	p_caseclassificationcat1
					,	p_caseclassificationcat2
					,	p_caseclassificationcat3
					,	p_casecriteriamode
					,	p_casecriteria
					,	p_reviewsummrequired,p_usersession,sTemplateContent
					);
					ConfigTplObjHomeImpl.updateConfigTplObjGroup(
						connection
					, 	p_templateseqid
					, 	p_usersession.getUserInfo()
					);
					AuditHome.savePopulatedStub(connection, l_auditinfo);
				} else 
				{
					//do nothing
				}
				
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error in fetching template details"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
				, 	sciExcep
				);
			} catch (IOException excep) 
			{
				connection.rollbackTransaction();
				log.error("File not available < "+ oUploadedFileObject.getStagedFilePath()+" >" );
				throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS, excep);
				
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
			, 	dbExcep
			);
		}
	}
	/**
	 * This will fetch you all the templates that
	 * belong to the passed project id and template
	 * type.
	 * @param p_projectid	The id of the project
	 * 						to which user belongs to.
	 * @param p_ctotype		The config template object
	 * 						type to which the template
	 * 						needs to be fetched.
	 * @return				The list of templates.
	 * @throws SciServiceException
	 */
	public List<QRTemplate> getTemplates (
		int	p_projectid
	) throws SciServiceException {
		
		List<QRTemplate>	l_qrtemplate		=	null;
		List<ConfigTplObj>	l_configtplobjlist	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_configtplobjlist	=	ConfigTplObjHomeImpl.getListOfTemplates(
											connection
										, 	p_projectid
										, 	ConfigTplObjType.QRT
										);
				if (l_configtplobjlist.size() > 0) {
					l_qrtemplate	=	new ArrayList<> ();
					for (ConfigTplObj l_template : l_configtplobjlist) {
						l_qrtemplate.add((QRTemplate)l_template);
					}
				}
				
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error fetching templates"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATES
				, 	sciExcep
				);
			} 	
		} catch (SciDbException dbExcep) {
			log.error(
				"Error fetching templates"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATES
			, 	dbExcep
			);
		}	
		return l_qrtemplate;
	}
	
	/**
	 * 
	 * @param p_templateseqid
	 * @return
	 * @throws SciServiceException
	 */
	public QRTemplate getQRTDetails (
		int p_templateseqid
	) throws SciServiceException {
			try {
				return QRPortalHomeImpl.getQRTDetails(p_templateseqid);
			} catch(SciException sciExcep) {
				log.error(
				"Error fetching templates"
				,	sciExcep
				);
				throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATES
				, 	sciExcep
				);
			} 	
	}
	
	/**
	 * This is used to update the content of the
	 * fragment table if any update is done
	 * for a particular review type.
	 * @param p_templateseqid
	 * @param p_reviewtypemapping
	 * @throws SciServiceException
	 */
	public void updateReviewTypeMappingData (
		int			p_projectid	
	,	int 		p_templateseqid
	,	String		p_casefields
	,	UserSession	p_usersession
	) throws SciServiceException {
		QRTemplate	l_qrtemplate	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try
			{
				connection.startTransaction();
				l_qrtemplate	=	getQRTDetails(p_templateseqid);
			
				checkUpdateRequired(
					l_qrtemplate
				, 	p_casefields
				,	connection
				,	p_usersession
				,	p_projectid
				);
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error(
					"Error in fetching template details"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Error in fetching template details"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
			, 	dbExcep
			);
		}
	}
	
	/**
	 * 
	 * @param p_qrtemplate
	 * @param p_reviewtypefieldcasemapping
	 * @param p_connection
	 * @param p_usersession
	 * @throws SciException
	 */
	void checkUpdateRequired (
		QRTemplate		p_qrtemplate
	,	String			p_casefields
	,	DbConnection	p_connection
	,	UserSession		p_usersession
	,	int				p_projectid
	) throws SciException {
		List<String> 	l_arrlist			=	null;
		BinaryContent	l_bincontent		=	null;
		AuditInfoDb		l_auditinfo			=	null;
		StringBuilder	l_prevcontent		=	null;
		
		if (p_qrtemplate.getTemplateCaseFieldMapping() == -1) {
			l_bincontent	=	QRPortalHomeImpl.saveReviewTypeCaseFieldMapping(
									p_connection
								, 	p_usersession
								, 	p_casefields
								);
			
			l_auditinfo		=	AuditHome.createRecordUpdateStub(
									p_usersession
								, 	this.getPortalAppId()
								, 	"Template Modified"
								,	p_projectid
								);
			l_auditinfo.addPayload(
				CommonAuditHelper.AuditAttributes.TEMPLATE_ID
			, 	p_qrtemplate.getSequenceId()
			);
			l_auditinfo.addPayload(
				CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
			, 	p_qrtemplate.getTemplateName()
			);
			l_auditinfo.addDetails(
				QRAuditHelper.AuditAttributes.FIELD_MAPPING
			, 	""
			, 	l_bincontent.getContent()
			);
			p_qrtemplate.setTemplateCaseFieldMapping(l_bincontent.getContentSeqId());
			ConfigTplObjHomeImpl.updateConfigTplObjAttributes(
				p_connection
			, 	p_qrtemplate
			, 	p_usersession.getUserInfo()
			);
			AuditHome.savePopulatedStub(
				p_connection
			, 	l_auditinfo
			);
		} else {
			if (!StringUtils.isNullOrEmpty(p_casefields)) {
				l_arrlist	=	new ArrayList<>();
				for (String l_value : p_casefields.split("~")) {
					l_arrlist.add(l_value);
				}
			}
			
			if (l_arrlist == null && p_qrtemplate.getTemplateCaseFields() == null) {
				return;
			} else if (l_arrlist != null && p_qrtemplate.getTemplateCaseFields() != null && l_arrlist.equals(p_qrtemplate.getTemplateCaseFields())) {
				return;
			} else {
				l_auditinfo		=	AuditHome.createRecordUpdateStub(
										p_usersession
									, 	this.getPortalAppId()
									, 	"Mapping Modified"
									,	p_projectid
									);
				
				l_bincontent	=	QRPortalHomeImpl.saveReviewTypeCaseFieldMapping(
										p_connection
									, 	p_usersession
									, 	p_casefields
									);
				if (p_qrtemplate.getTemplateCaseFields() != null) {
					l_prevcontent	=	new StringBuilder();
					for (String l_content : p_qrtemplate.getTemplateCaseFields()) {
						l_prevcontent.append(l_content+"~");
					}
				}
				if (l_prevcontent != null && l_prevcontent.length()>0) {
					l_prevcontent.deleteCharAt(l_prevcontent.length()-1);
				}
				l_auditinfo.addPayload(
					CommonAuditHelper.AuditAttributes.TEMPLATE_ID
				, 	p_qrtemplate.getSequenceId()
				);
				l_auditinfo.addPayload(
					CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
				, 	p_qrtemplate.getTemplateName()
				);
				l_auditinfo.addDetails(
					QRAuditHelper.AuditAttributes.FIELD_MAPPING
				, 	l_prevcontent != null ? l_prevcontent.toString() : ""
				, 	p_casefields
				);
				p_qrtemplate.setTemplateCaseFieldMapping(l_bincontent.getContentSeqId());
				ConfigTplObjHomeImpl.updateConfigTplObjAttributes(
					p_connection
				, 	p_qrtemplate
				, 	p_usersession.getUserInfo()
				);
				AuditHome.savePopulatedStub(p_connection, l_auditinfo);
			}
		}
	}
	/**
	 * 
	 * @param p_projectid
	 * @return
	 * @throws SciServiceException
	 */
	public QRTemplate getActiveQRTDetails (
		int	p_projectid
	) throws SciServiceException {
		try {
			return QRPortalHomeImpl.getActiveQRTDetails(p_projectid);
		} catch(SciException sciExcep) {
			throw new SciServiceException(
				ServiceErrorCodes.ProjectSetting.ERROR_FETCHING_TEMPLATE_DETAILS
			, 	sciExcep
			);
		} 	
	}
	
	/**
	 * This is used to update the state of the template from under
	 * configuration state to active state.
	 * @param p_usersession			Session information of the user
	 * @param p_eNewState			The state to which it needs to
	 * 								be updated.
	 * @param p_configtemplateobj	The config template object whose
	 * 								state needs to be updated.
	 * @throws SciServiceException
	 */
	public QRTemplate updateTemplateState(
		UserSession	p_usersession	
	,	RecordState p_eNewState
	,	int			p_templateseqid
	,	int			p_projectid
	) throws SciServiceException {
		AuditInfoDb									l_auditinfo		=	null;
		QRTemplate									l_qrtemplate	=	null;
		SafetyDbCaseFieldsTemplate					l_scft			=	null;
		Map<SafetyDBCaseFieldCategoryType, Integer>	l_catcount		=	null;
		int											l_totalcat		=	-1;
		String										l_categories	=	null;
		String[]									l_catarray		=	null;
		
		l_qrtemplate	=	getQRTDetails(
								p_templateseqid
							);
		
		if (l_qrtemplate.getState() == p_eNewState) {
			return l_qrtemplate;
		}
		if (!RecordState.isCorrectWorkflowState(l_qrtemplate.getState(), p_eNewState)) {
			return l_qrtemplate;
		}
		if (p_eNewState == RecordState.ACTIVE) {
			if (l_qrtemplate.getTemplateCaseFields() == null || l_qrtemplate.getTemplateCaseFields().isEmpty()) {
				log.error("None of the fields are selected for review");
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_REVIEW_FIELD_NOT_CHECKED);
			} else {
				l_scft	=	SafetyDbConfigurationHelper.getTemplateDetails(l_qrtemplate.getSCFTSeqId());
				if (l_scft != null) {
					l_catcount = new HashMap<>();
				}
				for (SafetyDbCaseFields l_fields : l_scft.getCaseFieldList()) {
					for (String l_fieldmnemonic : l_qrtemplate.getTemplateCaseFields()) {
						if (l_fields.getCaseFieldMnemonic().equals(l_fieldmnemonic)) {
							l_catcount.put(
								l_fields.getCaseFieldType()
							, 	l_catcount.get(l_fields.getCaseFieldType()) != null ? l_catcount.get(l_fields.getCaseFieldType())+1 : 1
							);
							break;
						}
					}
				}
				l_categories	=	 QRPortalHomeImpl.getApplicableCategories(p_projectid);
				if (StringUtils.isNullOrEmpty(l_categories)) {
					l_totalcat = 1;
				} else {
					l_catarray	=	l_categories.split(StringConstants.TILDE);
					l_totalcat	=	l_catarray.length;
				}
				
				if (l_qrtemplate.getCaseResultCriteriaMode() == 1) {
					if (l_totalcat > 1) {
						if (l_qrtemplate.getCat1QualityThreshold() == -1) {
							if (l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT1)) {
								log.error("Cat1 Quality Threshold is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, l_catarray[0], null);
							}
						} else if (l_qrtemplate.getCat1QualityThreshold() > -1 && !l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT1)) {
							log.error("Cat1 Field is not specified");
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED);
						} else if (l_qrtemplate.getCat1QualityThreshold() < -1) {
							log.error("Cat1 Quality Threshold is not specified correctly "+ l_qrtemplate.getCat1QualityThreshold());
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, l_catarray[0], null);
						}
						if (l_qrtemplate.getCat2QualityThreshold() == -1) {
							if (l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT2)) {
								log.error("Cat2 Quality Threshold is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, l_catarray[1], null);
							}
						} else if (l_qrtemplate.getCat2QualityThreshold() > -1 && !l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT2)) {
							log.error("Cat2 Field is not specified");
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED);
						} else if (l_qrtemplate.getCat2QualityThreshold() < -1) {
							log.error("Cat2 Quality Threshold is not specified correctly "+ l_qrtemplate.getCat2QualityThreshold());
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, l_catarray[1], null);
						}
						if (l_totalcat == 3) {
							if (l_qrtemplate.getCat3QualityThreshold() == -1) {
								if ( l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT3)) {
									log.error("Cat3 Quality Threshold is not specified");
									throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, l_catarray[2], null);
								}
							} else if (l_qrtemplate.getCat3QualityThreshold() > -1 && !l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT3)) {
								log.error("Cat3 Field is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED);
							} else if (l_qrtemplate.getCat3QualityThreshold() < -1) {
								log.error("Cat3 Quality Threshold is not specified correctly "+ l_qrtemplate.getCat3QualityThreshold());
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, l_catarray[2], null);
							}
						}
					} else {
						if (l_qrtemplate.getCaseQualityThreshold() == -1) {
							if (l_catcount.containsKey(SafetyDBCaseFieldCategoryType.NOCAT)) {
								log.error("Cat Overall Quality Threshold is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, QRConstant.OVERALL_THRESHOLD_CONST, null);
							}
						} else if (l_qrtemplate.getCaseQualityThreshold() < -1) {
							log.error("Cat3 Quality Threshold is not specified correctly "+ l_qrtemplate.getCaseQualityThreshold());
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, QRConstant.OVERALL_THRESHOLD_CONST, null);
						}
					}
				} else {
					if (l_totalcat > 1) {
						if (l_qrtemplate.getCat1QualityThreshold() == -1) {
							if (l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT1)) {
								log.error("Cat1 Quality Threshold is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, l_catarray[0], null);
							}
						} else if (l_qrtemplate.getCat1QualityThreshold() > 0 && l_qrtemplate.getCat1QualityThreshold() <= 100 
								&& !l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT1)) {
							log.error("Cat1 Field is not specified");
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED);
						} else if (l_qrtemplate.getCat1QualityThreshold() < -1 || l_qrtemplate.getCat1QualityThreshold() > 100) {
							log.error("Cat1 Quality Threshold is not specified correctly "+ l_qrtemplate.getCat1QualityThreshold());
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, l_catarray[0], null);
						}
						if (l_qrtemplate.getCat2QualityThreshold() == -1) {
							if (l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT2)) {
								log.error("Cat2 Quality Threshold is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, l_catarray[1], null);
							}
						} else if (l_qrtemplate.getCat2QualityThreshold() > 0 && l_qrtemplate.getCat2QualityThreshold() <= 100 
								&& !l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT2)) {
							log.error("Cat2 Field is not specified");
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED);
						} else if (l_qrtemplate.getCat2QualityThreshold() < -1 || l_qrtemplate.getCat2QualityThreshold() > 100) {
							log.error("Cat2 Quality Threshold is not specified correctly "+ l_qrtemplate.getCat2QualityThreshold());
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, l_catarray[1], null);
						}
						if (l_totalcat == 3) {
							if (l_qrtemplate.getCat3QualityThreshold() == -1) {
								if ( l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT3)) {
									log.error("Cat3 Quality Threshold is not specified");
									throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, l_catarray[2], null);
								}
							} else if (l_qrtemplate.getCat3QualityThreshold() > 0 && l_qrtemplate.getCat3QualityThreshold() <= 100 
									&& !l_catcount.containsKey(SafetyDBCaseFieldCategoryType.CAT3)) {
								log.error("Cat3 Field is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED);
							} else if (l_qrtemplate.getCat3QualityThreshold() < -1 || l_qrtemplate.getCat3QualityThreshold() > 100) {
								log.error("Cat3 Quality Threshold is not specified correctly "+ l_qrtemplate.getCat3QualityThreshold());
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, l_catarray[2], null);
							}
						}
						
					} else {
						if (l_qrtemplate.getCaseQualityThreshold() == -1) {
							if (l_catcount.containsKey(SafetyDBCaseFieldCategoryType.NOCAT)) {
								log.error("Cat Overall Quality Threshold is not specified");
								throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_NOT_DEFINED, QRConstant.OVERALL_THRESHOLD_CONST, null);
							}
						} else if (!(l_qrtemplate.getCaseQualityThreshold() > 0 && l_qrtemplate.getCaseQualityThreshold() <= 100)) {
							log.error("Cat3 Quality Threshold is not specified correctly "+ l_qrtemplate.getCaseQualityThreshold());
							throw new SciServiceException(ServiceErrorCodes.QR.ERROR_CAT_THRESHOLD_WRONG_VALUE, QRConstant.OVERALL_THRESHOLD_CONST, null);
						}
					}
				}
			}
		}
		
		l_auditinfo	=	AuditHome.createRecordUpdateStub(
							p_usersession
						, 	"APPSETTINGS-PORTAL-APP"
						, 	"Template State Changed"
						,	p_projectid
						);
		l_auditinfo.addPayload(
			CommonAuditHelper.AuditAttributes.TEMPLATE_ID
		, 	l_qrtemplate.getSequenceId()
		);
		l_auditinfo.addPayload(
			CommonAuditHelper.AuditAttributes.TEMPLATE_NAME
		, 	l_qrtemplate.getTemplateName()
		);
		l_auditinfo.addDetails(
			CommonAuditHelper.AuditAttributes.TEMPLATE_STATE
		, 	l_qrtemplate.getState().displayName()
		, 	p_eNewState.displayName()
		);
		l_qrtemplate.setState(p_eNewState);
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				
				ConfigTplObjHomeImpl.updateConfigTplObjState(
					connection
				,	l_qrtemplate.getSequenceId()
				, 	p_eNewState
				, 	p_usersession.getUserInfo()
				);
				AuditHome.savePopulatedStub(connection, l_auditinfo);
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error updating template ruleset state", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_UPDATING_TEMPLATE_STATE, l_qrtemplate.getTemplateName(), sciExcep);
			}
		} catch (SciDbException dbExcep){
			log.error("Error updating template ruleset state", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.ProjectSetting.ERROR_UPDATING_TEMPLATE_STATE, dbExcep);
		}
		return l_qrtemplate;
	}
	/**
	 * 
	 * @param p_usersession
	 * @param p_reviewdata
	 * @throws SciServiceException
	 */
	public void saveReviewData(
		UserSession 	p_usersession
	, 	QRReviewData	p_reviewdata
	,	int				p_projectid
	,	int				p_cmseqid
	,	String			p_internalcaseid
	,	PvcpCaseMaster	p_pvcpcasemaster
	) throws SciServiceException 
	{

		AuditInfoDb 		l_auditinfodb 	= 	null;
		EmailSendSetting	l_emailsetting	=	null;
		boolean				l_issendemail	=	false;
		String				l_mailpurpose	=	null,
							l_emailconfig	=	null;
		
		l_auditinfodb = AuditHome.createRecordInsertStub(p_usersession, this.getPortalAppId(), "Quality Review Log created", p_projectid);
		l_auditinfodb.addPayload(
			QRAuditHelper.AuditAttributes.CASE_ID
		, 	p_reviewdata.getCaseId()
		);
		l_auditinfodb.addPayload(
			QRAuditHelper.AuditAttributes.CASE_VERSION
		, 	p_reviewdata.getCaseVersion()
		);
		l_auditinfodb.addDetails(
			QRAuditHelper.AuditAttributes.CASE_SERIOUSNESS
		, 	"(None)"
		, 	p_reviewdata.getCaseSeriousness()
		);
		l_auditinfodb.addDetails(
			QRAuditHelper.AuditAttributes.CASE_ASSOCIATE
		, 	"(None)"
		, 	UserHome.retrieveUserBySeqId(p_reviewdata.getCaseAssociate()).getUserDisplayName()
		);
		l_auditinfodb.addDetails(
			QRAuditHelper.AuditAttributes.CASE_QUALITY
		, 	"(None)"
		, 	String.valueOf(p_reviewdata.getCaseQuality())
		);
		l_auditinfodb.addDetails(
			QRAuditHelper.AuditAttributes.CAT1_QUALITY
		, 	"(None)"
		, 	String.valueOf(p_reviewdata.getCaseCat1Quality())
		);
		l_auditinfodb.addDetails(
			QRAuditHelper.AuditAttributes.CAT2_QUALITY
		, 	"(None)"
		, 	String.valueOf(p_reviewdata.getCaseCat2Quality())
		);
		l_auditinfodb.addDetails(
			QRAuditHelper.AuditAttributes.CAT3_QUALITY
		, 	"(None)"
		, 	String.valueOf(p_reviewdata.getCaseCat3Quality())
		);
		try (DbConnection connection = DbConnectionManager.getConnection()) 
		{
			try 
			{
				connection.startTransaction();
				QRPortalHomeImpl.saveQualityReviewLog(connection, p_usersession, p_reviewdata, p_cmseqid, p_internalcaseid, p_pvcpcasemaster);
				QRPortalHomeImpl.insertErrorDashboardData(connection, p_reviewdata, p_cmseqid, p_internalcaseid);
				l_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CASE_RESULT
				, 	"(None)"
				, 	p_reviewdata.getCaseResult()
				);
				AuditHome.savePopulatedStub(connection, l_auditinfodb);
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error saving qr data ", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_REVIEW_DATA, sciExcep);
			}
		} 
		catch (SciDbException dbExcep) 
		{
			log.error("Error saving qr data ", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_REVIEW_DATA, dbExcep);
		}
		try {
			l_emailconfig	=	ConfigHome.getPrjConfigSettings(
									getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_EMAIL_SEND_CONFIG+StringConstants.PERIOD+p_reviewdata.getReviewType()
								, 	p_projectid
								);
			if (StringUtils.isNullOrEmpty(l_emailconfig)) {
				log.error("Email Configuration is not configured correctly for project id:" + p_projectid);
			} else {
				l_emailsetting	=	EmailSendSetting.toEnum(l_emailconfig);
				switch (l_emailsetting) {
					case SUCCESS: 	if (p_reviewdata.getCaseResult() == "Passed") {
										l_issendemail	=	true;
									}
					break;
					case FAILURE:	if (p_reviewdata.getCaseResult() == "Failed") {
										l_issendemail	=	true;
									}
					break;
					case ALL:		l_issendemail	=	true;
					break;
					case NONE:		l_issendemail	=	false;
					break;
				}
				if (l_issendemail) {
					l_mailpurpose =	ConfigHome.getPrjConfigSettings(
											getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_EMAIL_INITIAL_PURPOSE
											+StringConstants.PERIOD+p_reviewdata.getReviewType()
									, 	p_projectid
									);
					if (!StringUtils.isNullOrEmpty(l_mailpurpose)) {
						MailRuntimeHelper.getActiveMailTemplateByProjectId(p_projectid, l_mailpurpose);
						QRPortalHomeImpl.sendEmail(p_reviewdata,p_projectid,getPortalAppId(), l_mailpurpose);
					} else {
						log.error("Email Purpose is not configured for project id:" + p_projectid);
					}
				}
			}
		} catch (Throwable t) {
			log.error("Error sending email", t);
		}
	}
	/**
	 * 
	 * @param p_usersession
	 * @param p_reviewdata
	 * @throws SciServiceException
	 * 
	 * This condition on which the mail to be sent
	 * when the review gets completed will be fetched from
	 * project configuration table.
	 * @return	EmailSendSetting.A	- All Cases
	 * 			EmailSendSetting.S	- Success Cases
	 * 			EmailSendSetting.F	- Failure Cases
	 * 			EmailSendSetting.N	- None
	 */
	public void saveUpdateReviewData(
		UserSession		p_usersession
	, 	QRReviewData 	p_reviewdata
	,	int				p_projectid
	,	int				p_cmseqid
	,	String			p_internalcaseid
	,	PvcpCaseMaster	p_pvcpcasemaster
	) throws SciServiceException 
	{
		AuditInfoDb			l_auditinfodb 	= 	null;
		QRReviewData		l_reviewdataold	=	null,
							l_reviewdatanew	=	null;
		String 				l_mailpurpose	=	null;
		
		l_auditinfodb = AuditHome.createRecordUpdateStub(p_usersession, this.getPortalAppId(), "Quality Review Log Modified", p_projectid);
		l_auditinfodb.addPayload(
			QRAuditHelper.AuditAttributes.CASE_ID
		, 	p_reviewdata.getCaseId()
		);
		l_auditinfodb.addPayload(
			QRAuditHelper.AuditAttributes.CASE_VERSION
		, 	p_reviewdata.getCaseVersion()
		);
		try (DbConnection connection = DbConnectionManager.getConnection()) 
		{
			try 
			{
				connection.startTransaction();
				QRPortalHomeImpl.updateQualityReviewLog(connection, p_reviewdata, p_internalcaseid);
				l_reviewdataold	=	QRPortalHomeImpl.getQualityReviewLog(connection, p_reviewdata.getSequenceId());
				QRPortalHomeImpl.updateErrorDashboardData(connection, l_reviewdataold, p_internalcaseid);
				QRPortalHomeImpl.saveQualityReviewLog(connection, p_usersession, p_reviewdata, p_cmseqid, p_internalcaseid, p_pvcpcasemaster);
				QRPortalHomeImpl.insertErrorDashboardData(connection, p_reviewdata, p_cmseqid, p_internalcaseid);
				l_reviewdatanew	=	QRPortalHomeImpl.getQualityReviewLog(connection, p_reviewdata.getSequenceId());
				compareQualityReviewData(l_reviewdataold,l_reviewdatanew,l_auditinfodb);
				AuditHome.savePopulatedStub(connection, l_auditinfodb);
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				log.error("Error saving qr data ", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_REVIEW_DATA, sciExcep);
			}
		} 
		catch (SciDbException dbExcep) 
		{
			log.error("Error saving qr data ", dbExcep);
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_REVIEW_DATA, dbExcep);
		}
		try {
			
			l_mailpurpose	=	ConfigHome.getPrjConfigSettings(
									getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_EMAIL_EDIT_PURPOSE
										+StringConstants.PERIOD+p_reviewdata.getReviewType()
								, 	p_projectid
								);
			if (!StringUtils.isNullOrEmpty(l_mailpurpose)) {
				MailRuntimeHelper.getActiveMailTemplateByProjectId(p_projectid, l_mailpurpose);
				QRPortalHomeImpl.sendEmail(p_reviewdata,p_projectid,getPortalAppId(), l_mailpurpose);
			} else {
				log.error("Email Purpose is not configured for project id:" + p_projectid);
			}
		} catch (Throwable t) {
			log.error("Error sending email", t);
		}
	}
	
	public void compareQualityReviewData (
		QRReviewData	p_reviewdataold
	,	QRReviewData	p_reviewdatanew
	,	AuditInfoDb		p_auditinfodb 
	) {
		if (p_reviewdataold != null && p_reviewdatanew != null) {
			if (!p_reviewdataold.getCaseSeriousness().equals(p_reviewdatanew.getCaseSeriousness())) {
				p_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CASE_SERIOUSNESS
				, 	p_reviewdataold.getCaseSeriousness()
				, 	p_reviewdatanew.getCaseSeriousness()
				);
			}
			if (!p_reviewdataold.getCaseAssociateName().equals(p_reviewdatanew.getCaseAssociateName())) {
				p_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CASE_ASSOCIATE
				, 	p_reviewdataold.getCaseAssociateName()
				, 	p_reviewdatanew.getCaseAssociateName()
				);
			}
			if (!(p_reviewdataold.getCaseQuality() == p_reviewdatanew.getCaseQuality())) {
				p_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CASE_QUALITY
				, 	String.valueOf(p_reviewdataold.getCaseQuality())
				, 	String.valueOf(p_reviewdatanew.getCaseQuality())
				);
			}
			if (!(p_reviewdataold.getCaseCat1Quality() == p_reviewdatanew.getCaseCat1Quality())) {
				p_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CAT1_QUALITY
				, 	String.valueOf(p_reviewdataold.getCaseCat1Quality())
				, 	String.valueOf(p_reviewdatanew.getCaseCat1Quality())
				);
			}
			if (!(p_reviewdataold.getCaseCat2Quality() == p_reviewdatanew.getCaseCat2Quality())) {
				p_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CAT2_QUALITY
				, 	String.valueOf(p_reviewdataold.getCaseCat2Quality())
				, 	String.valueOf(p_reviewdatanew.getCaseCat2Quality())
				);
			}
			if (!(p_reviewdataold.getCaseCat3Quality() == p_reviewdatanew.getCaseCat3Quality())) {
				p_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CAT3_QUALITY
				, 	String.valueOf(p_reviewdataold.getCaseCat3Quality())
				, 	String.valueOf(p_reviewdatanew.getCaseCat3Quality())
				);
			}
			if (!(p_reviewdataold.getCaseResult().equals(p_reviewdatanew.getCaseResult()))) {
				p_auditinfodb.addDetails(
					QRAuditHelper.AuditAttributes.CASE_RESULT
				, 	p_reviewdataold.getCaseResult()
				, 	p_reviewdatanew.getCaseResult()
				);
			}
			for (String l_fieldmnemonic : p_reviewdatanew.getQrlReviewFieldDetails().keySet()) {
				if (p_reviewdatanew.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldStatus() != 
						p_reviewdataold.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldStatus()) {
					p_auditinfodb.addDetails(
						l_fieldmnemonic
					, 	String.valueOf(p_reviewdataold.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldStatus())
					, 	String.valueOf(p_reviewdatanew.getQrlReviewFieldDetails().get(l_fieldmnemonic).getFieldStatus())
					);
				}
			}
		}
	}
	
	/**
	 * 
	 * @return
	 * @throws SciServiceException
	 */
	public List<QRDailySummaryReport>	getDailySummaryReport (
		int	p_projectid
	) throws SciServiceException {
		List<QRDailySummaryReport>	l_qrdailysummaryreportlist	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_qrdailysummaryreportlist	=	QRPortalHomeImpl.getDailySummaryReport(
													connection
												,	p_projectid	
												);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Fetching all the qr data "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Fetching all the qr data "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
			, 	dbExcep
			);
		}
		return l_qrdailysummaryreportlist;
	}
	
	public QRReportSummaryDashboardData	getReportSummaryDashboardData (
		int	p_projectid
	) throws SciServiceException {
		QRReportSummaryDashboardData	l_dashboardata	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_dashboardata	=	QRPortalHomeImpl.getReportSummaryDashboardData(
										connection
									,	p_projectid	
									);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Fetching all the qr data "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Fetching all the qr data "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
			, 	dbExcep
			);
		}
		return l_dashboardata;
	}
	
	/**
	 * 
	 * @param p_projectid
	 * @param p_startdate
	 * @param p_enddate
	 * @return
	 * @throws SciServiceException
	 */
	public Map<String,List<QRCPDashboardReport>> getCaseProcessorReport (
		int	p_projectid
	,	int	p_startdate
	,	int	p_enddate
	) throws SciServiceException {
		Map<String,List<QRCPDashboardReport>>	l_cpreport	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_cpreport	=	QRPortalHomeImpl.getCaseProcessorReport(
									connection
								,	p_projectid
								,	p_startdate
								,	p_enddate
								);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error creating the cp dashboard"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_CREATING_CP_DASHBOARD
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Error creating the cp dashboard"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_CREATING_CP_DASHBOARD
			, 	dbExcep
			);
		}
		return l_cpreport;
	}
	
	/**
	 * 
	 * @param p_reporttype
	 * @param p_reviewtype
	 * @param p_usersession
	 * @param p_startdate
	 * @param p_enddate
	 * @param p_projectid
	 * @return
	 * @throws SciServiceException
	 */
	public File exportCaseProcessorReport(
		UserSession	p_usersession
	,	int			p_startdate
	,	int			p_enddate
	,	int			p_projectid
	) throws SciServiceException {
		File	l_exportfile	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_exportfile	=	QRPortalHomeImpl.exportCaseProcessorReport(
										connection
									,	p_projectid
									,	p_startdate
									,	p_enddate
									, 	p_usersession
									);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error Exporting CP Data"
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_EXPORTING_CP_DASHBOARD
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Error Exporting CP Data"
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_EXPORTING_CP_DASHBOARD
			, 	dbExcep
			);
		}
		return l_exportfile;
	}
	
	/**
	 * 
	 * @param p_reporttype
	 * @param p_usersession
	 * @return
	 * @throws SciServiceException
	 */
	public List<QRReviewData> getQualityReviewLogAll(
		String		p_reporttype
	,	UserSession	p_usersession
	,	int			p_startdate
	,	int			p_enddate
	,	int			p_projectid
	,	int			p_searchtype
	,	String			p_caseid
	,	String		p_reviewtype
	) throws SciServiceException {
		
		List<QRReviewData>	l_ilqrreviewdatalist	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_ilqrreviewdatalist	=	QRPortalHomeImpl.getQualityReviewLogAll(
												connection
											,	p_reporttype	
											, 	p_usersession.getUserInfo()
											,	p_startdate
											,	p_enddate
											,	p_projectid
											,	p_searchtype
											,	p_caseid
											,	p_reviewtype
											);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Fetching all the qr data "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Fetching all the qr data "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
			, 	dbExcep
			);
		}
		return l_ilqrreviewdatalist;
	}
	
	/**
	 * 
	 * @param p_reporttype
	 * @param p_usersession
	 * @return
	 * @throws SciServiceException
	 */
	public File getFullSummaryReport(
		String		p_reporttype
	,	String		p_reviewtype
	,	UserSession	p_usersession
	,	int			p_startdate
	,	int			p_enddate
	,	int			p_projectid
	) throws SciServiceException {
		File	l_exportfile	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_exportfile	=	QRPortalHomeImpl.getFullSummaryReport(
										connection
									,	p_reporttype	
									,	p_reviewtype
									, 	p_usersession
									,	p_startdate
									,	p_enddate
									,	p_projectid
									,	getPortalAppId()
									,	null
									);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Fetching all the qr data "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Fetching all the qr data "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
			, 	dbExcep
			);
		}
		return l_exportfile;
	}
	
	/**
	 * 
	 * @param p_sequenceid
	 * @return
	 * @throws SciServiceException
	 */
	public QRReviewData getReviewData(
		int		p_sequenceid
	) throws SciServiceException {
		QRReviewData	l_ilqrreviewdata	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_ilqrreviewdata	=	QRPortalHomeImpl.getQualityReviewLog(
											connection
										, 	p_sequenceid
										);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Fetching the qr data for a case "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
					"Fetching the qr data for a case "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA
			, 	dbExcep
			);
		}
		return l_ilqrreviewdata;
	}
	
	/**
	 * 
	 * @param p_projectid
	 * @return
	 * @throws SciServiceException
	 */
	public Map<String, ArrayList<SafetyDbCaseFields>> getActiveSafetyDbCaseFieldList(
		int	p_projectid
	) throws SciServiceException 
	{
		Map<String, ArrayList<SafetyDbCaseFields>>	l_mapqualityreviewtabdata	=	null;
		SafetyDbCaseFieldsTemplate					l_safetydbcasefieldtemplate	=	null;
		ArrayList<SafetyDbCaseFields>				l_safetybdcasefieldslist	=	null;
		
		l_safetydbcasefieldtemplate	=	SafetyDbConfigurationHelper.getActiveTemplateDetails(p_projectid);
		if (l_safetydbcasefieldtemplate == null) {
			log.error(
				"Error in fetching template details"
			);
			throw new SciServiceException(
				ServiceErrorCodes.Common.ERROR_ACTIVE_TEMPLATE_NOT_FOUND
			);
		}
		l_mapqualityreviewtabdata	=	new LinkedHashMap<>();
		for (SafetyDbCaseFields l_safetydbcasefields : l_safetydbcasefieldtemplate.getCaseFieldList()) {
			if(l_mapqualityreviewtabdata.containsKey(l_safetydbcasefields.getCaseFieldTab()))
			{
				l_safetybdcasefieldslist = l_mapqualityreviewtabdata.get(l_safetydbcasefields.getCaseFieldTab());
				l_safetybdcasefieldslist.add(l_safetydbcasefields);
				l_mapqualityreviewtabdata.put(l_safetydbcasefields.getCaseFieldTab(), l_safetybdcasefieldslist);
			}else
			{
				l_safetybdcasefieldslist	=	new ArrayList<>();
				l_safetybdcasefieldslist.add(l_safetydbcasefields);
				l_mapqualityreviewtabdata.put(l_safetydbcasefields.getCaseFieldTab(), l_safetybdcasefieldslist);
			}
		}
		return l_mapqualityreviewtabdata;	
	}
	
	/**
	 * This method will fetch the template sequence id
	 * for the QRT from the project configuration maintained
	 * in project configuration for the project id passed to
	 * the method.
	 * @param p_projectid	The project in which the case is being reviewed.
	 * @param p_reviewtype	The review type for which the case is being reviewed.
	 * @param p_usersession	The session of the logged in user.
	 * @return				The quality review template for the combination of
	 * 						project id and review type.
	 * @throws SciServiceException	If the sequence id maintained is null, the
	 * 								template state is not active or the value
	 * 								cannot be parsed.
	 */
	public QRTemplate getTemplateDetailsQRL (
		int			p_projectid
	,	String		p_reviewtype	
	,	UserSession	p_usersession
	) throws SciServiceException {
		QRTemplate	l_qrtemplate = null;
		try {
			l_qrtemplate	=	getQRTDetails(
									Integer.parseInt(
										ConfigHome.getPrjConfigSettings(
											getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_REVIEW_QRT_MAPPING+StringConstants.PERIOD+p_reviewtype
										, 	p_projectid
										)
									)	
								);
		} catch (NumberFormatException e) {
			throw new SciServiceException(
				ServiceErrorCodes.Common.ERROR_ACTIVE_TEMPLATE_NOT_FOUND
			);
		}
		return l_qrtemplate;
	}
	
	/**
	 * This method will fetch the template sequence id
	 * for the QRT from the project configuration maintained
	 * in project configuration for the project id passed to
	 * the method.
	 * @param p_projectid	The project in which the case is being reviewed.
	 * @param p_reviewtype	The review type for which the case is being reviewed.
	 * @param p_usersession	The session of the logged in user.
	 * @return				The quality review template for the combination of
	 * 						project id and review type.
	 * @throws SciServiceException	If the sequence id maintained is null, the
	 * 								template state is not active or the value
	 * 								cannot be parsed.
	 */
	public QRTemplate getActiveQRLTemplateDetails (
		int			p_projectid
	,	String		p_reviewtype	
	,	UserSession	p_usersession
	) throws SciServiceException {
		QRTemplate	l_qrtemplate = null;
		try {
			l_qrtemplate	=	getQRTDetails(
									Integer.parseInt(
										ConfigHome.getPrjConfigSettings(
											getPortalAppId()+StringConstants.PERIOD+QRConstant.PROP_PROJ_REVIEW_QRT_MAPPING+StringConstants.PERIOD+p_reviewtype
										, 	p_projectid
										)
									)	
								);
			if (l_qrtemplate.getState() != RecordState.ACTIVE) {
				throw new SciServiceException(
					ServiceErrorCodes.Common.ERROR_ACTIVE_TEMPLATE_NOT_FOUND
				);
			}
		} catch (NumberFormatException e) {
			throw new SciServiceException(
				ServiceErrorCodes.Common.ERROR_ACTIVE_TEMPLATE_NOT_FOUND
			);
		}
		return l_qrtemplate;
	}
	
	/**
	 * 
	 * @param p_projectid
	 * @return
	 * @throws SciServiceException
	 */
	public Map<String, ArrayList<SafetyDbCaseFields>> getReviewFieldList(
		QRTemplate	p_qrtemplate	
	) throws SciServiceException 
	{
		return QRPortalHomeImpl.getReviewFieldList(p_qrtemplate,null);	
	}
	/**
	 * 
	 * @param p_userinfo
	 * @return
	 * @throws SciServiceException
	 */
	public String getUserLayoutType (
		UserInfo p_userinfo,
		int p_selectedProjSeqId
	) throws SciServiceException {
		try {
			return	UserUtils.getStringPreferenceValue(
						p_userinfo
					,	p_selectedProjSeqId	
					, 	getPortalAppId()
					, 	USERPREF_LAYOUT
					);
		} catch (SciException sciExcep) {
			log.error(
				"Error getting layout type of a given user"
			,	sciExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.LAYOUT_TYPE_NOT_AVAILABLE
			,	p_userinfo.getUserId()	
			, 	sciExcep
			);
		}
	}
	
	/**
	 * 
	 * @param p_userinfo
	 * @return
	 * @throws SciServiceException
	 */
	public String getUserReviewType (
		UserInfo p_userinfo ,
		int p_selectedProjSeqId
	) throws SciServiceException {
		try {
			return	UserUtils.getStringPreferenceValue(
						p_userinfo
					,   p_selectedProjSeqId
					, 	getPortalAppId()
					, 	USERPREF_REVIEWTYPE
					);
		} catch (SciException sciExcep) {
			log.error(
				"Error getting layout type of a given user"
			,	sciExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.REVIEW_TYPE_NOT_AVAILABLE
			,	p_userinfo.getUserId()	
			, 	sciExcep
			);
		}
	}
	
	public Map<String, Map<String, PvcpCaseMaster>> saveCaseMasterFields(UserSession o_usersession, DataTable o_datatable, int o_projectId) throws SciServiceException
	{
	
		Map<String, Map<String, PvcpCaseMaster>> l_exstingrecordcases = null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
	
				l_exstingrecordcases = QRPortalHomeImpl.saveCaseMasterFields(o_usersession, o_datatable, o_projectId, connection);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error loading project List - {}",  sciExcep);
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_MASTER_DATA);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading project List - {}",  dbExcep);
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_MASTER_DATA);	
		}
		return l_exstingrecordcases;
	}
	
	public List<PvcpCaseMaster> getCaseMasterFields(int n_projectid) throws SciServiceException
	{
		List<PvcpCaseMaster> l_casemasterlist = null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
	
				l_casemasterlist = QRPortalHomeImpl.getCaseMasterFieldsList(connection, n_projectid);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching case records from db", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_UPLOAD_CASE_MASTER_FIle);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching case records from db");
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_UPLOAD_CASE_MASTER_FIle);
		}
		
		return l_casemasterlist;
	}
	public PvcpCaseMaster getPVCPCaseMasterByInternalCaseId(String s_internalcaseid , int n_projectid) throws SciServiceException
	{	
		PvcpCaseMaster o_pvcpcasemaster = null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
	
				o_pvcpcasemaster = QRPortalHomeImpl.getPVCPCaseMasterFieldsByInternamCaseId(connection, s_internalcaseid, n_projectid);
				
				connection.commitTransaction();
			}
			catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching case records from db", sciExcep);
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASEMASTER_RECORDS);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching case records from db");
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASEMASTER_RECORDS);
		}
		return o_pvcpcasemaster;
	}
	
	/**
	 * 
	 * @param o_casemaster
	 * @param s_reviewtype
	 * @return
	 * @throws SciServiceException
	 */
	public List<QRReviewData> getQRLReviewTypeRecordDetails(PvcpCaseMaster o_casemaster, String s_reviewtype) throws SciServiceException
	{
		List<QRReviewData> l_qrreviewdatalist	=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				l_qrreviewdatalist = QRPortalHomeImpl.getQRLReviewTypeRecordDetails(connection, o_casemaster, s_reviewtype);
				connection.commitTransaction();
			} catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching quality review record from db");
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA	);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching quality review record from db");
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA	);
		}
		return l_qrreviewdatalist;
	}
	
	/**
	 * This will fetch all the error type maintained at the project
	 * level.
	 * @param p_projectid The project whose error type needs to be
	 * 					  fetched.
	 * @return List of all the error maintained in the database.
	 */
	public Map<String,String> getErrorTypes (
		int p_projectid
	) throws SciServiceException {
		try {
			return QRPortalHomeImpl.getErrorTypes(p_projectid);
		} catch (SciException sciExcep) {
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_UPDATING_CASEMASTER_DATA);
		}
	}
	/**
	 * 
	 */
	public String getApplicableCategories (
		int	p_projectid
	) {
		return	QRPortalHomeImpl.getApplicableCategories(p_projectid);
	}
	
	public void updateCaseMaster(PvcpCaseMaster o_casemaster, int n_projectid, UserSession o_usersession) throws SciServiceException
	{	
			try (DbConnection connection = DbConnectionManager.getConnection())
			{
				try
				{
					connection.startTransaction();
		
					QRPortalHomeImpl.updateCaseMasterRecord(connection, o_casemaster,o_usersession,  n_projectid);
					
					connection.commitTransaction();
				}
				catch (SciException sciExcep)
				{
					connection.rollbackTransaction();
					log.error("Error Updating CaseMaster", sciExcep );
					throw new SciServiceException(ServiceErrorCodes.QR.ERROR_UPDATING_CASEMASTER_DATA);
				}
			}
			catch (SciDbException dbExcep)
			{
				log.error("Error fetching case records from db",dbExcep );
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_UPDATING_CASEMASTER_DATA, dbExcep);
			}
			
	}
	
	public List<QRReviewData> getQRLReviewTypeRecordDetailsByCaseId(String 			s_CaseId
			, 	int 			i_projectId) throws SciServiceException
	{
		List<QRReviewData> l_qrreviewdatalist	=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				l_qrreviewdatalist = QRPortalHomeImpl.getQRLByCaseId(connection, s_CaseId, i_projectId);
				connection.commitTransaction();
			} catch (SciException sciExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching quality review record from db");
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA	);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching quality review record from db");
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA	);
		}
		return l_qrreviewdatalist;
	}
	public  Map<String, Map<String, PvcpCaseMaster>> saveCaseMasterrecord(UserSession o_usersession, PvcpCaseMaster o_casemaster, int n_projectId) throws SciServiceException
	{
		Map<String, Map<String, PvcpCaseMaster>> l_casemastermap = null;
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				l_casemastermap = QRPortalHomeImpl.saveCaseMasterrecordmanualy( o_usersession,o_casemaster, n_projectId, connection);
				connection.commitTransaction();
			}
			catch (SciException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error Casemaster Record - {}",  dbExcep);
				throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_MASTER_DATA);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error Saving Casemaster Record- {}",  dbExcep);
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_SAVING_MASTER_DATA);	
		}
		return l_casemastermap;
	}
	public List<QRReviewData> getQualityReviewLogByRange(
		UserSession	p_usersession
	,	int			p_projectid
	, 	int startdate
	,	int enddate
	) throws SciServiceException {
		
		List<QRReviewData>	l_ilqrreviewdatalist	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_ilqrreviewdatalist	=	QRPortalHomeImpl.getQualityReviewLogbyrange(
												connection
											,	p_usersession.getUserInfo()
											,	p_projectid
											,	startdate
											,	enddate
											);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Fetching all the qr data "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Fetching all the qr data "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
			, 	dbExcep
			);
		}
		return l_ilqrreviewdatalist;
	}

	public Map<String, List<QRReviewData>> getCaseHistoryByInternalId(
		UserSession	p_usersession
		,	int			p_projectid
		, 	String 		p_caseid
		,	String 		p_caseversion
		) throws SciServiceException {
			
		Map<String, List<QRReviewData>> qrreviewdatamap = null;
			
			try (DbConnection connection = DbConnectionManager.getConnection()) {
				try {
					connection.startTransaction();
					qrreviewdatamap	=	QRPortalHomeImpl.getCaseHistoryByIncrementalCaseId(connection, p_caseid, p_caseversion, p_projectid);
					connection.commitTransaction();
				} catch(SciException sciExcep) {
					connection.rollbackTransaction();
					log.error(
						"Error fetching case history by internal case id  "
					,	sciExcep
					);
					throw new SciServiceException(
						ServiceErrorCodes.QR.ERROR_FETCHING_CASE_HISTORY
					, 	sciExcep
					);
				}
			} catch (SciDbException dbExcep) {
				log.error(
					"Error fetching case history by internal case id"
				,	dbExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCHING_CASE_HISTORY
				, 	dbExcep
				);
			}
			return qrreviewdatamap;
	}

	public QRErrorDashboardData[] getErrorDbData (
		int	p_projectid
	) throws SciServiceException {
		QRErrorDashboardData[]	l_errordbdata	=	null;
		try (DbConnection connection = DbConnectionManager.getConnection()) {
			try {
				connection.startTransaction();
				l_errordbdata	=	QRPortalHomeImpl.getErrorDbData(connection, p_projectid);
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Fetching all the qr data "
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
				, 	sciExcep
				);
			}
		} catch (SciDbException dbExcep) {
			log.error(
				"Fetching all the qr data "
			,	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
			, 	dbExcep
			);
		}
		return l_errordbdata;
	}

	/**
	 * This method will return all the regulator
	 * class mapped for the project.
	 */
	public Map<String,String> getApplicableRegClass(
		int	p_projectid
	) {
		Map<String,String>	l_regclassmap		=	null;
		l_regclassmap	=	QRPortalHomeImpl.getApplicableRegClass(p_projectid);
		return	l_regclassmap;
	}

	/***
	 * This method will read input byte array(which template exel file retrieved from DB) into workbook, and update it by review data
	 * 
	 * 
	 * @param p_sSeqId
	 * @return List<Object>  which contains  caseId, processorName and downloadFileContent(byte[] format)
 	 * @throws SciServiceException
	 */
	public List<Object> updateExcel(String p_sSeqId)  throws SciServiceException
	{
		List<Object> listTemp = null;
		QRReviewData	oQRReviewData	=	null;
		int nSeqId = p_sSeqId!=null?Integer.parseInt(p_sSeqId):0;
		List<SafetyDbCaseFields> listSafetyDbCaseFields = null;
		Map<String, QRReviewFieldDetails> mapQrlReviewFieldDetails = null;
		SafetyDbCaseFields oSafetyDbCaseFields = null;
		QRReviewFieldDetails oQRReviewFieldDetails = null;
		SafetyDbCaseFieldsTemplate oSafetyDbCaseFieldsTemplate = null;
		String sCategories = null;
		String[] arrCategories = null;
		
		List<String> listOfNameRanges = null;
		Sheet oSheet = null;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;
		Workbook oWorkbook = null;
		InputStream inputStream = null;
		ByteArrayOutputStream oByteArrayOutputStream = null;
		Name oNamedCell = null;
		AreaReference oAreaReference = null;
		CellReference[] oCellReference = null;
		int namedCellIndex;
		String sFieldType = null;
		String sField = null;
		String sComment = null;
		int nStatus ;
		listOfNameRanges = new ArrayList<>();
		listOfNameRanges.add("QR_NAME");
		listOfNameRanges.add("QR_CASE_NUMBER");
		listOfNameRanges.add("QR_DATE");
		listOfNameRanges.add("MAJOR_ACC");
		listOfNameRanges.add("MINOR_ACC");
		listOfNameRanges.add("OVERALL_ACC");
		BinaryContent oBinaryContent = null;
		int nDownloadReviewTemplateSeqId ;
		byte[] arrOutputExelBytes = null;
		byte[] arrInputExelBytes = null;
		Base64.Decoder oDecoder = null;
		
		try
		{
			oDecoder = Base64.getDecoder();
			listTemp = new ArrayList<>();
			oQRReviewData = getReviewData(nSeqId);
			nDownloadReviewTemplateSeqId = oQRReviewData.getQrTemplate().getDownloadReviewTemplateSeqId();
			try (DbConnection connection = DbConnectionManager.getConnection()) 
			{
				try {
					connection.startTransaction();
					oBinaryContent = BinaryContentHomeImpl.readContent(connection, nDownloadReviewTemplateSeqId);
					connection.commitTransaction();
				} catch(SciException sciExcep) {
					connection.rollbackTransaction();
					log.error("Error in updating file ", sciExcep);
					throw new SciServiceException(
						ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
					, 	sciExcep
					);
				}
				
			} catch (SciDbException dbExcep) {
				log.error("Error in updating file ", dbExcep);
				throw new SciServiceException(
					ServiceErrorCodes.QR.ERROR_FETCH_ALL_QR_DATA
				, 	dbExcep
				);
			}
			arrInputExelBytes = oDecoder.decode(oBinaryContent.getContent());
			listTemp.add(oQRReviewData.getCaseId());
			listTemp.add(oQRReviewData.getCaseAssociateName());
			sCategories = getApplicableCategories(oQRReviewData.getQrTemplate().getProjectId());
			
			mapQrlReviewFieldDetails = oQRReviewData.getQrlReviewFieldDetails();
			arrCategories = sCategories.split("~");
			inputStream = new ByteArrayInputStream(arrInputExelBytes);
			oWorkbook = WorkbookFactory.create(inputStream);
			oSheet = oWorkbook.getSheetAt(QRPortalHomeImpl.getCaseReviewDownloadSheetNum(oQRReviewData.getQrTemplate().getProjectId()));
			oIteratorWorkbookRows = oSheet.rowIterator();
			
			for(int nNum = 0; nNum < listOfNameRanges.size(); nNum++ )
			{
				oIteratorWorkbookRows.next();
				namedCellIndex = oWorkbook.getNameIndex(listOfNameRanges.get(nNum));
				oNamedCell = oWorkbook.getNameAt(namedCellIndex);
				oAreaReference = new AreaReference(oNamedCell.getRefersToFormula(),null);
				oCellReference = oAreaReference.getAllReferencedCells();
				for (int nIndex=0; nIndex<oCellReference.length; nIndex++) 
				{
				    oSheet = oWorkbook.getSheet(oCellReference[nIndex].getSheetName());
				    oSingleWorkbookRow = oSheet.getRow(oCellReference[nIndex].getRow());
				    oSingleWorkbookCell = oSingleWorkbookRow.getCell(oCellReference[nIndex].getCol());
				    if(nNum == 0)
				    	oSingleWorkbookCell.setCellValue(oQRReviewData.getCaseAssociateName());
				    if(nNum == 1)
				    	oSingleWorkbookCell.setCellValue(oQRReviewData.getCaseId());
				    if(nNum == 2)
				    	oSingleWorkbookCell.setCellValue(oQRReviewData.getCaseRecvDate());
				    if(nNum == 3)
				    	oSingleWorkbookCell.setCellValue(oQRReviewData.getCaseCat1Quality());
				    if(nNum == 4)
				    	oSingleWorkbookCell.setCellValue(oQRReviewData.getCaseCat2Quality());
				    if(nNum == 5)
				    	oSingleWorkbookCell.setCellValue(oQRReviewData.getCaseQuality());
				 
				}
			}
			while(oIteratorWorkbookRows.hasNext())
			{
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				oSingleWorkbookCell = oSingleWorkbookRow.getCell(0);
				if(oSingleWorkbookCell !=null && oSingleWorkbookCell.getStringCellValue().equalsIgnoreCase("field"))
				 break;
			}
			oSafetyDbCaseFieldsTemplate = SafetyDbConfigurationHelper.getTemplateDetails(oQRReviewData.getQrTemplate().getSCFTSeqId());
			listSafetyDbCaseFields = oSafetyDbCaseFieldsTemplate.getCaseFieldList();
			for(int nIndex = 0; nIndex < listSafetyDbCaseFields.size(); nIndex++)
			{
				oSafetyDbCaseFields = listSafetyDbCaseFields.get(nIndex);
				oQRReviewFieldDetails =  mapQrlReviewFieldDetails.get(oSafetyDbCaseFields.getCaseFieldMnemonic());
				sField = oSafetyDbCaseFields.getCaseFieldName();
				sFieldType = arrCategories[oSafetyDbCaseFields.getCaseFieldType().getCatCode()-1];
				nStatus = oQRReviewFieldDetails.getFieldStatus();
				sComment = oQRReviewFieldDetails.getFieldComment();
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				for (int nColumnCellIndex = 0; nColumnCellIndex < oSingleWorkbookRow.getLastCellNum(); nColumnCellIndex++)
				{
					oSingleWorkbookCell = oSingleWorkbookRow.getCell(nColumnCellIndex);
					switch(nColumnCellIndex)
					{
						case 0:
								oSingleWorkbookCell.setCellValue(sField);
								break;
						case 1:
								oSingleWorkbookCell.setCellValue(sFieldType);
								break;
						case 2:
								if(nStatus == 3)
									oSingleWorkbookCell.setCellValue(0);
								else
									oSingleWorkbookCell.setCellValue(nStatus);
								break;
						case 3:
								oSingleWorkbookCell.setCellValue(sComment);
								break;
								
						default:
								break;
					}
				}
				
			}
			
		 	inputStream.close();
		 	oByteArrayOutputStream = new ByteArrayOutputStream();
		 	oWorkbook.write(oByteArrayOutputStream);
		 	oWorkbook.close();
		 	arrOutputExelBytes = oByteArrayOutputStream.toByteArray();
		 	listTemp.add(arrOutputExelBytes);
		 	oByteArrayOutputStream.close();
		
			//TODO:Change the exception object name
		}catch(SciServiceException sciExcep)
		{
			log.error("Fetching the qr data for a case ",	sciExcep);
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA, sciExcep);
		} catch (Exception excep) 
		{
			log.error("Error in updating file ",	excep);
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_FETCH_CASE_QR_DATA, excep); //change this
		} 
		return listTemp;
	}

	/**
	 * This method return serious case threshold value for selected project
	 * @param p_projectid gives user current project sequence id
	 * @return  if threshold value for serious case is present in project
	 *  		configuration then return threshold value else return 0
	 */
	public int getQRGraphSeriousnessThreshold (int p_projectid)
	{
		return QRPortalHomeImpl.getQRGraphSeriousnessThreshold(p_projectid);
	
	}
	
	/**
	 * This method return non-serious case threshold value for the selected project.
	 * @param p_projectid gives user current project sequence id.
	 * @return  if threshold value for non-serious case is present in project
	 *  		configuration then return threshold value else return 0.
	 */
	public int getQRGraphNonSeriousnessThreshold (int p_projectid)
	{
		return QRPortalHomeImpl.getQRGraphNonSeriousnessThreshold(p_projectid);
	
	}
	
	/**
	 * This method is used to fetch the starting date of the month for project
	 * related dashboard and information.
	 * @param p_projectid	The project for which the starting date is required.
	 * @return				The date between 1 and 28.
	 * @throws SciServiceException 
	 */
	public int getProjectStartDate (int p_projectid) throws SciServiceException  {
		try {
			return QRPortalHomeImpl.getProjectStartDate(p_projectid);
		} catch (SciException sciExcep) {
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_PARSING_PRJ_START_DATE);
		}
	}
	
	/**
	 * This method is used to fetch the sheet number
	 * of the template that will be downloaded after
	 * the case review.
	 * @param p_projectid	The project for which the template
	 * 	 					sheet number is required.
	 * @return				The sheet number.	
	 * @throws SciServiceException  If the value maintained at project
	 *  							level is non-numeric or null
	 */
	public int getCaseReviewDownloadSheetNum (int p_projectid) throws SciServiceException {
		try {
			return QRPortalHomeImpl.getCaseReviewDownloadSheetNum(p_projectid);
		} catch (SciException sciExcep) {
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_PARSING_DWNLD_SHEET_NUM);
		}
	}
	
	/**
	 * Thia method returns UDF fields enable by in from project confif settings 
	 * @param p_projectid current selected project by user 
	 * @return Map with Udf field name as key and list of udf field display 
	 * 		   name and datatype, if not found returns empty Map
	 */
	public Map<String,List<String>> getEnableUdfList (int p_projectid){
		return QRPortalHomeImpl.getEnableUdfList(p_projectid);
	}
	
	/**
	 * This is used to return the file formats applicable for the
	 * case master upload in Quality Review.
	 * @param p_projectid The project id for which the file will
	 * 	                  be uploaded.
	 * @return Map of file format internal code and display. The
	 * 		   key will be internal code and the value will be
	 * 		   display name.
	 * @throws SciServiceException If the file format maintained in
	 *  						   database is null.
	 */
	public Map<String,String> getApplicableFileFormat(
		int	p_projectid
	) throws SciServiceException {
		try {
			return QRPortalHomeImpl.getApplicableFileFormat(p_projectid);
		} catch (SciException sciExcep) {
			throw new SciServiceException(ServiceErrorCodes.Common.FILE_FORMAT_NOT_MAINTAINED);
		}
	}
	
	/**
	 * This method will check whether the MR Name to be displayed
	 * in the Quality Review Page or not.
	 * @param p_projectid Project Id to which the logged in user
	 * 					  belongs.
	 * @return	true if the name to be displayed otherwise false
	 */
	public boolean isMRNameEnabledForProject (
		int	p_projectid
	) {
		return QRPortalHomeImpl.isMRNameEnabledForProject(p_projectid);
	}
	
	public static List<PvcpCaseMaster> listcasemasterrecords(UserSession o_usersession,DataTable o_datatable, int n_projectId) throws SciServiceException
	{
		try {
			return  QRPortalHomeImpl.listcasemasterrecords(o_usersession, o_datatable, n_projectId);
		} catch (SciException sciExcep) {
			throw new SciServiceException(ServiceErrorCodes.QR.ERROR_UPLOAD_CASE_MASTER_FIle);
		}
	}
}
