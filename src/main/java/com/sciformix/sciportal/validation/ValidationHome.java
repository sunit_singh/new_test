package com.sciformix.sciportal.validation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class ValidationHome 
{
	private static String _FORMAT_VALIDATION_FIELD_MAP_KEY_ = "%s.%s";
	private static Map<String, Map<String, ValidationFieldDetails>> s_mapValidationFieldsDetails = null;
	private static Map<String, Map<String, ValidationFieldDetails>> s_mapWildCardValidationFieldsDetails = null;
	private ValidationHome()
	{
		// Nothing to do
	}
	
	private static String prepareKey(String contextId, String p_sFieldName)
	{
		return String.format(_FORMAT_VALIDATION_FIELD_MAP_KEY_, contextId, p_sFieldName);
	}
	

	public static boolean init(SciStartupLogger oStartupLogger) 
	{
		boolean bSuccessful = false;
		
		List<ValidationFieldDetails> listValidationFieldDetails = null;
		
		s_mapValidationFieldsDetails = new HashMap<String, Map<String, ValidationFieldDetails>>();
		
		s_mapWildCardValidationFieldsDetails = new HashMap<String, Map<String, ValidationFieldDetails>>();
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			listValidationFieldDetails = ValidationHomeImpl.loadValidationFields(connection);
			
			for (ValidationFieldDetails oValidationFieldDetails : listValidationFieldDetails)
			{
				if(oValidationFieldDetails.getisDynamic().equals("N"))
				{
					if (s_mapValidationFieldsDetails.get(oValidationFieldDetails.getsAppId()) == null)
					{
						s_mapValidationFieldsDetails.put(oValidationFieldDetails.getsAppId(), new HashMap<String, ValidationFieldDetails>());
					}
					s_mapValidationFieldsDetails.get(oValidationFieldDetails.getsAppId()).put(prepareKey(oValidationFieldDetails.getsContext(),oValidationFieldDetails.getsFieldName()), oValidationFieldDetails);
				}
				else
				{
					if (s_mapWildCardValidationFieldsDetails.get(oValidationFieldDetails.getsAppId()) == null)
					{
						s_mapWildCardValidationFieldsDetails.put(oValidationFieldDetails.getsAppId(), new HashMap<String, ValidationFieldDetails>());
					}
					s_mapWildCardValidationFieldsDetails.get(oValidationFieldDetails.getsAppId()).put(prepareKey(oValidationFieldDetails.getsContext(),oValidationFieldDetails.getsFieldName()), oValidationFieldDetails);
				}
			}
			
			bSuccessful = true;
		}
		catch (SciException sciExcep)
		{
			oStartupLogger.logError("Error loading Validation Field details" + sciExcep.getMessage());
		}
		catch (SciDbException dbExcep)
		{
			oStartupLogger.logError("Error loading Validation Field details" + dbExcep.getMessage());
		}
		
		return bSuccessful;
	}
	
	public static Map<String,  Map<String, ValidationFieldDetails>> getValidationFieldDetailsMap()
	{
			return s_mapValidationFieldsDetails;	
	}
	public static Map<String,  Map<String, ValidationFieldDetails>> getWildCardalidationFieldDetailsMap()
	{
			return  s_mapWildCardValidationFieldsDetails;	
	}
	
}
