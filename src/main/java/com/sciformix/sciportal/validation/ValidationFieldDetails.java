package com.sciformix.sciportal.validation;

public class ValidationFieldDetails 
{
	private int    nSeqId;
	private String sAppId;
	private String sActionId;
	private String sContext;
	private String sFieldName;
	private String sDataType;
	private String sRegEx;
	private String isDynamic;
	
	public String getsContext() {
		return sContext;
	}
	public void setsContext(String sContext) {
		this.sContext = sContext;
	}
	public String getisDynamic() {
		return isDynamic;
	}
	public void setisDynamic(String isDynamic) {
		this.isDynamic = isDynamic;
	}
	public int getnErrorCode() {
		return nErrorCode;
	}
	public void setnErrorCode(int nErrorCode) {
		this.nErrorCode = nErrorCode;
	}
	private int nErrorCode;
	
	
	public int getnSeqId() {
		return nSeqId;
	}
	public void setnSeqId(int nSeqId) {
		this.nSeqId = nSeqId;
	}
	public String getsAppId() {
		return sAppId;
	}
	public void setsAppId(String sAppId) {
		this.sAppId = sAppId;
	}
	public String getsActionId() {
		return sActionId;
	}
	public void setsActionId(String sActionId) {
		this.sActionId = sActionId;
	}
	public String getsFieldName() {
		return sFieldName;
	}
	public void setsFieldName(String sFieldName) {
		this.sFieldName = sFieldName;
	}
	public String getsDataType() {
		return sDataType;
	}
	public void setsDataType(String sDataType) {
		this.sDataType = sDataType;
	}
	public String getsRegEx() {
		return sRegEx;
	}
	public void setsRegEx(String sRegEx) {
		this.sRegEx = sRegEx;
	}
	public int getsErrorCode() {
		return nErrorCode;
	}
	public void setsErrorCode(int sErrorCode) {
		this.nErrorCode = sErrorCode;
	}
	
}
