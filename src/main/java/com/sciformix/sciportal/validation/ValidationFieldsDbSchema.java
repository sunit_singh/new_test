package com.sciformix.sciportal.validation;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class ValidationFieldsDbSchema 
{
	public static Validation COMMON_FIELD_VALIDATION = new Validation("CMN_FIELDDATAVALIDATION");
	
	
	public static class Validation extends DbTable
	{
		public DbColumn VALIDATIONSEQID  = null;
		public DbColumn APPID            = null;
		public DbColumn ACTIONID 		 = null;
		public DbColumn CONTEXT 		 = null;
		public DbColumn FIELDNAME   	 = null;
		public DbColumn DATATYPE		 = null;
		public DbColumn REGEX			 = null;
		public DbColumn ISDYNAMIC		 = null;
		public DbColumn ERRORCODE		 = null;
		
		public Validation(String p_sTableName)
		{
			super(p_sTableName);
			setPkSequenceName("CMN_FIELDDATAVALIDATION_SEQ");
			
			VALIDATIONSEQID = addColumn("VALIDATIONSEQID");
			APPID 			= addColumn("APPID");
			ACTIONID 		= addColumn("ACTIONID");
			CONTEXT			= addColumn("CONTEXT");
			FIELDNAME		= addColumn("FIELDNAME");
			DATATYPE		= addColumn("DATATYPE");
			REGEX		    = addColumn("REGEX");
			ISDYNAMIC		= addColumn("ISDYNAMIC");
			ERRORCODE       = addColumn("ERRORCODE");
		}
	}
}
