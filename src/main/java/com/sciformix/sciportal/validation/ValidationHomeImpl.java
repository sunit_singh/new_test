package com.sciformix.sciportal.validation;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

public class ValidationHomeImpl 
{
	private static final Logger log = LoggerFactory.getLogger(ValidationHomeImpl.class);
	/*private static final String STANDARD_MATCH_TYPE = "Standard";
	private static final String WILDCARD_MATCH_TYPE = "WildCard";
	*/
	public static List<ValidationFieldDetails> loadValidationFields(DbConnection connection) throws SciException
	{
		List<ValidationFieldDetails> listValidationFieldDetails = null;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION, null);
		//oSelectQuery.addWhereClauseParameter(Y);
		
		listValidationFieldDetails = fetchValidationFields(connection, oSelectQuery);
		
		return listValidationFieldDetails;	
	}
	
	private static List<ValidationFieldDetails> fetchValidationFields(DbConnection connection, DbQuerySelect p_oSelectQuery) throws SciException
	{
		List<ValidationFieldDetails> listValidationFieldDetails = null;
		ValidationFieldDetails oValidationFieldDetails = null;
		
		listValidationFieldDetails = new ArrayList<ValidationFieldDetails>();
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(p_oSelectQuery))
			{
				while (resultSet.next())
				{
					oValidationFieldDetails = readValidationFieldObjectFromDb(resultSet);
					listValidationFieldDetails.add(oValidationFieldDetails);
				}
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading error code messages", dbExcep);
			throw new SciException("error reading error code messages", dbExcep);
		}
		return listValidationFieldDetails;
	}
	
	private static ValidationFieldDetails readValidationFieldObjectFromDb(DbResultSet resultSet) throws SciDbException
	{
		ValidationFieldDetails oValidationFieldDetails = null;
		
		oValidationFieldDetails = new ValidationFieldDetails();
		oValidationFieldDetails.setsActionId(resultSet.readString(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.ACTIONID));
		oValidationFieldDetails.setsAppId(resultSet.readString(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.APPID));
		oValidationFieldDetails.setsContext(resultSet.readString(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.CONTEXT));
		oValidationFieldDetails.setsFieldName(resultSet.readString(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.FIELDNAME));
		oValidationFieldDetails.setsDataType(resultSet.readString(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.DATATYPE));
		oValidationFieldDetails.setsRegEx(resultSet.readString(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.REGEX));
		oValidationFieldDetails.setisDynamic(resultSet.readString(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.ISDYNAMIC));
		oValidationFieldDetails.setsErrorCode(resultSet.readInt(ValidationFieldsDbSchema.COMMON_FIELD_VALIDATION.ERRORCODE));
		return oValidationFieldDetails;
	}

}
