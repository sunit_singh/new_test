package com.sciformix.sciportal.sqlquery;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class QueryRepoDbSchema {
	public static QueryRepo	QUERY_REPO  = new QueryRepo();
	
	public static class QueryRepo extends DbTable 
	{
		public DbColumn QUERYSEQID = null;
		public DbColumn APPID = null;
		public DbColumn QUERYNAME = null;
		public DbColumn QUERYDESC = null;
		public DbColumn ACTUALQUERY = null;

		public QueryRepo() 
		{
			super("CMN_QUERYREPO");
			setPkSequenceName("CMN_QUERYREPO_QUERYSEQID");
			
			QUERYSEQID = addColumn("QUERYSEQID");
			APPID = addColumn("APPID");
			QUERYNAME = addColumn("QUERYNAME");
			QUERYDESC = addColumn("QUERYDESC");
			ACTUALQUERY = addColumn("ACTUALQUERY");
		}
	}
}
