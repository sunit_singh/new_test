package com.sciformix.sciportal.sqlquery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

class QueryRepoHomeImpl {
	private static final Logger 
		log	= LoggerFactory.getLogger(QueryRepoHomeImpl.class)
	;
	
	/**
	 * This method will be used to return
	 * the query that is mapped in the
	 * database for the given query id.
	 * The query should be modified to 
	 * include the schema owner name and
	 * all the parameters at run-time.
	 * @param p_connection DB Connection
	 * @param p_queryid	   Query Id
	 * @return	The actual query that need
	 * 			to be executed.
	 * @throws SciException If there are some
	 * 						DB exceptions.
	 */
	static String getQueryById(
		DbConnection	p_connection
	,	int				p_queryid
	) throws SciException {
		DbQuerySelect	l_selectquery 		=	null;
		String			l_actualquery		=	null;
		
		l_selectquery	=	DbQueryUtils.createSelectQuery(
								QueryRepoDbSchema.QUERY_REPO
							,	QueryRepoDbSchema.QUERY_REPO.ACTUALQUERY.name()
							, 	QueryRepoDbSchema.QUERY_REPO.QUERYSEQID.bindClause()
							);

		l_selectquery.addWhereClauseParameter(p_queryid);
		
		try{
			p_connection.startTransaction();
			try (DbResultSet resultSet = p_connection.executeQuery(l_selectquery)) {
				if (resultSet.next()) {
					l_actualquery	=	resultSet.readString(QueryRepoDbSchema.QUERY_REPO.ACTUALQUERY);
				}				
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching query details for query id " + p_queryid
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching query details for query id " + p_queryid
			, 	dbExcep
			);
		}
		return l_actualquery;
	}
	
	/**
	 * This method will be used to return
	 * the query that is mapped in the
	 * database for the given query id.
	 * The query should be modified to 
	 * include the schema owner name and
	 * all the parameters at run-time.
	 * @param p_connection DB Connection
	 * @param p_appid	   Application Id	
	 * @param p_queryname  Query Name
	 * @return The actual query that need
	 * 		   to be executed.
	 * @throws SciException If there are some
	 * 						DB exceptions.
	 */
	static String getQueryByName(
		DbConnection	p_connection	
	,	String 			p_appid
	,	String			p_queryname
	) throws SciException {
		DbQuerySelect	l_selectquery 		=	null;
		String			l_actualquery		=	null;
		
		l_selectquery	=	DbQueryUtils.createSelectQuery(
								QueryRepoDbSchema.QUERY_REPO
							,	QueryRepoDbSchema.QUERY_REPO.ACTUALQUERY.name()
							,	DbQueryUtils.bindWhereClauseWithAnd(
									QueryRepoDbSchema.QUERY_REPO.APPID.bindClause()
								, 	QueryRepoDbSchema.QUERY_REPO.QUERYNAME.bindClause()
								)
							);
		l_selectquery.addWhereClauseParameter(p_appid);
		l_selectquery.addWhereClauseParameter(p_queryname);
		
		try{
			p_connection.startTransaction();
			try (DbResultSet resultSet = p_connection.executeQuery(l_selectquery)) {
				if (resultSet.next()) {
					l_actualquery	=	resultSet.readString(QueryRepoDbSchema.QUERY_REPO.ACTUALQUERY);
				}				
			}
			p_connection.commitTransaction();
		} catch (SciDbException dbExcep) {
			p_connection.rollbackTransaction();
			log.error(
				"Error fetching query details for app id " + p_appid + " and query name " + p_queryname
			, 	dbExcep
			);
			throw new SciException(
				"Error fetching query details for app id " + p_appid + " and query name " + p_queryname
			, 	dbExcep
			);
		}
		return l_actualquery;	
	}
}
