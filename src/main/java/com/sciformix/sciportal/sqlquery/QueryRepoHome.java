package com.sciformix.sciportal.sqlquery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class QueryRepoHome {
	
	private static final Logger 
		log	= LoggerFactory.getLogger(QueryRepoHome.class)
	;
	
	/**
	 * This method will be used to return
	 * the query that is mapped in the
	 * database for the given query id.
	 * The query should be modified to 
	 * include the schema owner name and
	 * all the parameters at run-time.
	 * @param p_connection DB Connection
	 * @param p_queryid	   Query Id
	 * @return	The actual query that need
	 * 			to be executed.
	 * @throws SciServiceException If there
	 *  are some DB exceptions or the query
	 *  is not found.
	 */
	public static String getQueryById(
		int	p_queryid
	) throws SciServiceException {
		String	p_query	=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {	
			try {
				connection.startTransaction();
				p_query	=	QueryRepoHomeImpl.getQueryById(connection, p_queryid);
				if (StringUtils.isNullOrEmpty(p_query)) {
					throw new SciServiceException(
						ServiceErrorCodes.Common.ERROR_SQL_QUERY_NOT_FOUND
					);
				}
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error fetching query details for query id " + p_queryid
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.Common.ERROR_SQL_QUERY_NOT_FOUND
				, 	sciExcep
				);
			} 
		} catch (SciDbException dbExcep) {
			log.error(
				"Error fetching query details for query id " + p_queryid
			, 	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.Common.ERROR_SQL_QUERY_NOT_FOUND
			, 	dbExcep
			);
		}
		return p_query;
	}
	
	/**
	 * This method will be used to return
	 * the query that is mapped in the
	 * database for the given query id.
	 * The query should be modified to 
	 * include the schema owner name and
	 * all the parameters at run-time.
	 * @param p_connection DB Connection
	 * @param p_appid	   Application Id	
	 * @param p_queryname  Query Name
	 * @return The actual query that need
	 * 		   to be executed.
	 * @throws SciServiceException If there
	 *  are some DB exceptions or the query
	 *  is not found.
	 */
	public static String getQueryByName(	
		String 			p_appid
	,	String			p_queryname
	) throws SciServiceException {
		String	p_query	=	null;
		
		try (DbConnection connection = DbConnectionManager.getConnection()) {	
			try {
				connection.startTransaction();
				p_query	=	QueryRepoHomeImpl.getQueryByName(connection, p_appid, p_queryname);
				if (StringUtils.isNullOrEmpty(p_query)) {
					throw new SciServiceException(
						ServiceErrorCodes.Common.ERROR_SQL_QUERY_NOT_FOUND
					);
				}
				connection.commitTransaction();
			} catch(SciException sciExcep) {
				connection.rollbackTransaction();
				log.error(
					"Error fetching query details for app id " + p_appid + " and query name " + p_queryname
				,	sciExcep
				);
				throw new SciServiceException(
					ServiceErrorCodes.Common.ERROR_SQL_QUERY_NOT_FOUND
				, 	sciExcep
				);
			} 
		} catch (SciDbException dbExcep) {
			log.error(
				"Error fetching query details for app id " + p_appid + " and query name " + p_queryname
			, 	dbExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.Common.ERROR_SQL_QUERY_NOT_FOUND
			, 	dbExcep
			);
		}
		return p_query;
	}
}
