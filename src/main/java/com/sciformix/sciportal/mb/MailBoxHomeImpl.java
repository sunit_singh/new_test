package com.sciformix.sciportal.mb;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.config.ConfigTplObj;
import com.sciformix.sciportal.config.ConfigTplObjHomeImpl;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQueryHome;
import com.sciformix.sciportal.database.DbQueryInsert;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;
import com.sciformix.sciportal.utils.WebUtils;

public class MailBoxHomeImpl {

	private static final Logger log = LoggerFactory.getLogger(MailBoxHomeImpl.class);
	
	private static final String
		MAIL_REPORT_NAME_CONST	=	"MailReport.csv"
	;
	
	private static final String
		MAIL_REPORT_HEADER		=	"Sciformix Id,Subject,Sender,Recipient,CC,Outlook Timestamp,Attachment Count,Attachment Name"
	;
	private static final String
		DATE_FORMAT_CONST		=	"dd-MMM-yyyy HH:mm:ss.SSS"
	;
	
	private MailBoxHomeImpl()
	{
		// Nothing to do
	}
	
	public static MailBox saveMailBox(MailBox m_oMailBox,UserInfo u_oUserInfo, boolean p_bInsert) throws SciException
	{
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			if (p_bInsert)
			{	
				m_oMailBox.setCreatedOn(new Date());
				m_oMailBox.setCreatedBy(u_oUserInfo.getUserSeqId());
				m_oMailBox.setLastUpdatedGrpOn(new Date());
				m_oMailBox.setLastUpdatedGrpBy(u_oUserInfo.getUserSeqId());
				m_oMailBox.setLastUpdatedOn(new Date());
				m_oMailBox.setLastUpdatedBy(u_oUserInfo.getUserSeqId());
				try {
					connection.startTransaction();
					m_oMailBox=(MailBox)ConfigTplObjHomeImpl.saveConfigTplObj(connection,m_oMailBox,u_oUserInfo);
					connection.commitTransaction();
				} catch (SciException sciExcep) {
					connection.rollbackTransaction();
					log.error("Error saving email box - {}", sciExcep);
					throw new SciException("Error saving email box", sciExcep);
				}
			} 
			else
			{	
				try {
					connection.startTransaction();
					ConfigTplObjHomeImpl.updateConfigTplObjAttributes(connection, m_oMailBox, u_oUserInfo);
					connection.commitTransaction();
				} catch (SciException sciExcep) {
					connection.rollbackTransaction();
					log.error("Error saving email box - {}", sciExcep);
					throw new SciException("Error saving email box", sciExcep);
				}
			}
		} 
		catch(SciDbException dbExcep)
		{
			log.error("Error saving email box - {}", dbExcep);
			throw new SciException("Error saving email box", dbExcep);
		}
		return m_oMailBox;
	}
	
	public static MailBox getMailBoxBySeqId(int m_sequenceid ) throws SciException
	{
		MailBox mailbox = new MailBox();
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				mailbox=(MailBox)ConfigTplObjHomeImpl.getTemplateDetails(connection, m_sequenceid);
				
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				throw sciExcep;
			}
			
		} catch(SciDbException dbExcep)
		{
			log.error("Error getting email box - {}", dbExcep);
			throw new SciException("Error getting email box", dbExcep);
		}
		
		return mailbox;
	}
	
	public static MailBox getMailBoxByTemplateName(String p_templatename, int p_projectid) throws SciException
	{
		MailBox mailbox = new MailBox();
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				connection.startTransaction();
				mailbox=(MailBox)ConfigTplObjHomeImpl.getTemplateDetailsByName(connection, ConfigTplObjType.MBT, p_projectid, p_templatename);
				
				connection.commitTransaction();
			} catch (SciException sciExcep) {
				connection.rollbackTransaction();
				throw sciExcep;
			}
			
		} catch(SciDbException dbExcep)
		{
			log.error("Error getting email box by template name - {}", dbExcep);
			throw new SciException("Error getting email box by template name", dbExcep);
		}
		return mailbox;
	}
	
	public static List<MailBox> getAllMailBoxes(int p_nProjectId) throws SciException
	{
		List<MailBox> listMailBox = null;
		List<ConfigTplObj> listMailBoxStubs = null;
		MailBox o_mailBox = null;
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try {
				listMailBoxStubs = ConfigTplObjHomeImpl.getListOfTemplates(connection, p_nProjectId, ConfigTplObjType.MBT);
				listMailBox = new ArrayList<MailBox>(listMailBoxStubs.size());
				for (ConfigTplObj oConfigObject : listMailBoxStubs)
				{
					o_mailBox =(MailBox)oConfigObject;
					
					//TO Do: uncomment this code after mailbox template activation code written
					
					if(o_mailBox.getState().value()==RecordState.ACTIVE.value()) //add only active templates
					{
						listMailBox.add(o_mailBox);
					}
				}
			} catch (SciException sciExcep) {
				log.error("Error loading mail box List - {}",  sciExcep);
				throw sciExcep;
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error loading mail box List - {}",  dbExcep);
			throw new SciException("Error loading mail box List", dbExcep);
		}
		return listMailBox;
	}
	
	public static MBMailInfo saveMBMail(MBMailInfo m_oMBMail,UserInfo u_oUserInfo, boolean p_bInsert) throws SciException
	{
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				if (p_bInsert)
				{	
					DbQueryInsert oInsertQuery = null;
					
					int nMBMailSeqId = DbQueryHome.generateSequenceValueForTablePk(MailboxDbSchema.MBMAIL);
					
					oInsertQuery = DbQueryUtils.createInsertQuery(MailboxDbSchema.MBMAIL);
					oInsertQuery.addInsertParameter(nMBMailSeqId);
					oInsertQuery.addInsertParameter(m_oMBMail.getProjectSeqId());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailBoxSeqId());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailAppSeqId());
					oInsertQuery.addInsertParameter(m_oMBMail.getCrc());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailType().getCode());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailTimeStamp());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailMessageId());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailRecordMode().getCode());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailSender());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailRecipientsTo());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailRecipientsCc());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailSubject());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailAttachmentCount());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailAttachmentNames());
					oInsertQuery.addInsertParameter(m_oMBMail.getMailSizeInBytes());
					oInsertQuery.addInsertParameter(m_oMBMail.getDateCreated());
					oInsertQuery.addInsertParameter(m_oMBMail.getUserCreated());
		
					connection.executeQuery(oInsertQuery);
					
					m_oMBMail.setMailSequenceId(nMBMailSeqId);
				}
				else
				{
					
					//DbQueryUpdate oUpdateQuery = null;
					
					// Need to complete
				
				}
				connection.commitTransaction();
			}
			catch(SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error saving MailBox Mail - {}", m_oMBMail.getMailSequenceId(), dbExcep);
				throw new SciException("Error saving MailBox Mail", dbExcep);
			}
		}catch(SciDbException dbExcep)
		{
			log.error("Error saving MailBox Mail - {}", m_oMBMail.getMailSequenceId(), dbExcep);
			throw new SciException("Error saving MailBox Mail", dbExcep);
		}
	
		return m_oMBMail;
		
	}
	
	public static List<MBMailInfo> getMailListByMailBoxId(int projectId, int mailBoxId) throws SciException
	{
		List<MBMailInfo> listMailBox = null;
		MBMailInfo m_mailInfo = null;
		DbQuerySelect oSelectQuery = null;
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(MailboxDbSchema.MBMAIL,
				DbQueryUtils.bindWhereClauseWithAnd
				(MailboxDbSchema.MBMAIL.PRJSEQID,MailboxDbSchema.MBMAIL.MAILBOXSEQID),
				MailboxDbSchema.MBMAIL.DTCREATED.name());
		oSelectQuery.addWhereClauseParameter(projectId);
		oSelectQuery.addWhereClauseParameter(mailBoxId);
		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				listMailBox = new ArrayList<>();
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						m_mailInfo = readMailInfo(resultSet);
						listMailBox.add(m_mailInfo);
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching mail list ");
				throw new SciException("Error fetching mail list ", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching mail list ");
			new SciException("Error fetching mail list ", dbExcep);
		}
	
		return listMailBox;
	}
	
	public static List<MBMailInfo> getMailListByProjectId(int projectId) throws SciException
	{
		List<MBMailInfo> listMailBox = null;
		MBMailInfo m_mailInfo = null;
		DbQuerySelect oSelectQuery = null;
		oSelectQuery = DbQueryUtils.createSelectAllColumnsQuery(MailboxDbSchema.MBMAIL,
				MailboxDbSchema.MBMAIL.PRJSEQID.bindClause(),
				MailboxDbSchema.MBMAIL.DTCREATED.name());
		oSelectQuery.addWhereClauseParameter(projectId);
		
		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				listMailBox = new ArrayList<>();
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while(resultSet.next())
					{
						m_mailInfo = readMailInfo(resultSet);
						listMailBox.add(m_mailInfo);
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching mail list ");
				throw new SciException("Error fetching mail list ", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching mail list ");
			new SciException("Error fetching mail list ", dbExcep);
		}
	
		return listMailBox;
	}
	
	static Calendar[] getDate(
		int	p_startdate
	,	int	p_enddate
	) {
		Calendar	l_cldr[]		=	new Calendar [2];
		Calendar	l_startdatetime	=	null,
					l_enddatetime	=	null;
		
		l_startdatetime	=	Calendar.getInstance();
		l_enddatetime	=	Calendar.getInstance();
		
		l_startdatetime.set(Calendar.HOUR_OF_DAY, 0);
		l_startdatetime.set(Calendar.MINUTE, 0);
		l_startdatetime.set(Calendar.SECOND, 0);
		l_startdatetime.set(Calendar.MILLISECOND, 0);
		l_startdatetime.add(Calendar.DAY_OF_MONTH, p_startdate);
		
		l_enddatetime.set(Calendar.HOUR_OF_DAY, 23);
		l_enddatetime.set(Calendar.MINUTE, 59);
		l_enddatetime.set(Calendar.SECOND, 59);
		l_enddatetime.set(Calendar.MILLISECOND, 999);
		l_enddatetime.add(Calendar.DAY_OF_MONTH, p_enddate);
		
		l_cldr[0]	=	l_startdatetime;
		l_cldr[1]	=	l_enddatetime;
		return l_cldr;
	}
	
	public static List<MBMailInfo> getMailListByDateRange(int projectId, int mailBoxSeqId,int p_startdate, int p_enddate) throws SciException
	{
		List<MBMailInfo> listMailList = null;
		DbQuerySelect l_selectquery	= null;
		MBMailInfo m_mailInfo = null;
		Calendar []	l_cldr = null;
		l_cldr	=	getDate(p_startdate,p_enddate);
		
		
		l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
								MailboxDbSchema.MBMAIL
							, 	DbQueryUtils.bindWhereClauseWithAnd(
									MailboxDbSchema.MBMAIL.PRJSEQID.bindClause()
								,	MailboxDbSchema.MBMAIL.MAILBOXSEQID.bindClause()	
								,	MailboxDbSchema.MBMAIL.MAILCLIENTTIMESTAMP.bindClauseBetween()
								)	
							,	MailboxDbSchema.MBMAIL.MAILCLIENTTIMESTAMP.name()
							);

		l_selectquery.addWhereClauseParameter(projectId);
		l_selectquery.addWhereClauseParameter(mailBoxSeqId);
		l_selectquery.addWhereClauseParameter(l_cldr[0].getTime());
		l_selectquery.addWhereClauseParameter(l_cldr[1].getTime());
		
		try (DbConnection connection= DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				listMailList = new ArrayList<>();
				try (DbResultSet resultSet = connection.executeQuery(l_selectquery))
				{
					while(resultSet.next())
					{
						m_mailInfo = readMailInfo(resultSet);
						listMailList.add(m_mailInfo);
					}
				}
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error fetching mail list ");
				throw new SciException("Error fetching mail list ", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error fetching mail list ");
			new SciException("Error fetching mail list ", dbExcep);
		}
	
		return listMailList;
	}
	
	private static MBMailInfo readMailInfo(DbResultSet resultset) throws SciDbException
	{
		MBMailInfo mailInfo = new MBMailInfo(resultset.readInt(MailboxDbSchema.MBMAIL.MAILSEQID),resultset.readInt(MailboxDbSchema.MBMAIL.MAILBOXSEQID),
				resultset.readInt(MailboxDbSchema.MBMAIL.USRCREATED), resultset.readInt(MailboxDbSchema.MBMAIL.PRJSEQID),
				resultset.readString(MailboxDbSchema.MBMAIL.MAILMESSAGEID), resultset.readString(MailboxDbSchema.MBMAIL.MAILAPPSEQID),
				resultset.readString(MailboxDbSchema.MBMAIL.MAILSUBJECT), resultset.readInt(MailboxDbSchema.MBMAIL.MAILATTACHMENTCOUNT),
				resultset.readString(MailboxDbSchema.MBMAIL.MAILATTACHMENTNAMES), resultset.readDate(MailboxDbSchema.MBMAIL.MAILCLIENTTIMESTAMP),
				resultset.readString(MailboxDbSchema.MBMAIL.MAILRECIPIENTSTO), resultset.readString(MailboxDbSchema.MBMAIL.MAILRECIPIENTSCC),
				resultset.readString(MailboxDbSchema.MBMAIL.MAILSENDER), MBMailInfo.MailRecordMode.toEnum(resultset.readString(MailboxDbSchema.MBMAIL.MAILRECORDMODE)),
				MBMailInfo.MailType.toEnum(resultset.readString(MailboxDbSchema.MBMAIL.MAILTYPE)), resultset.readString(MailboxDbSchema.MBMAIL.CRC),
				resultset.readDate(MailboxDbSchema.MBMAIL.DTCREATED));
		
		return mailInfo;
	}
	
	public static File exportMails(
		int 		p_projectid
	,	int 		mailBoxSeqId
	,	int 		p_startdate
	,	int 		p_enddate
	,	UserSession p_usersession
	) throws SciException {
		File				l_exportfile	=	null;
		FileWriter			l_fwr			=	null;
		List<MBMailInfo> 	l_maillist 		=	null;
		StringBuilder		l_fullcontent	=	null;
		l_maillist	=	getMailListByDateRange(p_projectid, mailBoxSeqId, p_startdate, p_enddate);
		if (l_maillist != null && l_maillist.size() > 0) {
			try {
				l_exportfile	=	new File(
										WebUtils.createStagingFile(
											p_usersession
										, 	MAIL_REPORT_NAME_CONST
										)
									);
				l_fwr 			=	new FileWriter(l_exportfile);
				l_fullcontent	=	new StringBuilder(MAIL_REPORT_HEADER);
				l_fullcontent.append(System.lineSeparator());
				for (MBMailInfo l_mailinfo : l_maillist) {
					appendContent(
						l_fullcontent
					,	l_mailinfo.getMailAppSeqId() != null ? 	l_mailinfo.getMailAppSeqId() : StringConstants.EMPTY
					, 	l_mailinfo.getMailSubject() != null ? l_mailinfo.getMailSubject() : StringConstants.EMPTY
					,	l_mailinfo.getMailSender() != null ? l_mailinfo.getMailSender() : StringConstants.EMPTY
					,	l_mailinfo.getMailRecipientsTo() != null ? l_mailinfo.getMailRecipientsTo() : StringConstants.EMPTY
					,	l_mailinfo.getMailRecipientsCc() != null ? l_mailinfo.getMailRecipientsCc() : StringConstants.EMPTY
					,	String.valueOf(new SimpleDateFormat(DATE_FORMAT_CONST).format(l_mailinfo.getMailTimeStamp()))
					,	String.valueOf(l_mailinfo.getMailAttachmentCount())
					,	l_mailinfo.getMailAttachmentCount() == 0 ? StringConstants.EMPTY :l_mailinfo.getMailAttachmentNames()
					);
					l_fullcontent.append(System.lineSeparator());
				}
				l_fwr.write(l_fullcontent.toString());
			} catch (Exception excep) {
				log.error("Error exporting mails", excep);
				throw new SciException(
					"Error exporting mails"
				,	excep
				);
			} finally {
				try {
					l_fwr.close();
				} catch (IOException ioExcep) {
					log.error("Error exporting mails", ioExcep);
					throw new SciException(
						"Error exporting mails"
					,	ioExcep
					);
				}
			}
		} else {
			log.error("Error exporting mails");
			throw new SciException(
				"Error exporting mails"
			);
		}
		return l_exportfile;
	}
	
	static void appendContent (
		StringBuilder	p_fullcontent
	,	String ... 		p_appendcontent	
	) {
		for (String p_content : p_appendcontent) {
			p_fullcontent.append(StringConstants.ESCAPE_CSV+ p_content + StringConstants.ESCAPE_CSV +",");
		}
		p_fullcontent.deleteCharAt(p_fullcontent.length()-1);
	}
}
