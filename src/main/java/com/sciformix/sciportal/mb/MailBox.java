package com.sciformix.sciportal.mb;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciEnums.ConfigTplObjType;
import com.sciformix.commons.SciEnums.CreationMode;
import com.sciformix.commons.SciEnums.RecordState;
import com.sciformix.sciportal.config.ConfigTplObj;

public class MailBox extends ConfigTplObj
{

	private static final int UDFINDEX_EMAIL_ADDRESS = 0;
	private static final int UDFINDEX_MAIL_DESCRIPTION = 1;
	private static final int UDFINDEX_TYPE_OF_MAILBOX = 2;
	private static final int UDFINDEX_PERMITTED_MODE_OF_OPERATION = 3;
	private static final int UDFINDEX_MAILBOX_MNEMONICS = 4;
	
	
	public MailBox(int p_projectId, ConfigTplObjType p_configObjectType, String p_mailBoxName,
			RecordState p_state, CreationMode p_creationMode) 
	{
		super(p_projectId, ConfigTplObjType.MBT, p_mailBoxName, p_state, p_creationMode);
		
	}

	public MailBox()
	{
		super(ConfigTplObjType.MBT);
	}
	
	public String getEmailAddress() throws SciException
	{
		return super.getUdf(UDFINDEX_EMAIL_ADDRESS);
	}
	
	public String getMailDescription() throws SciException
	{
		return super.getUdf(UDFINDEX_MAIL_DESCRIPTION);
	}
	
	public String getTypeOfMailBox() throws SciException
	{
		return super.getUdf(UDFINDEX_TYPE_OF_MAILBOX);
	}
	
	public String getModeOfOperation() throws SciException
	{
		return super.getUdf(UDFINDEX_PERMITTED_MODE_OF_OPERATION);
	}
	
	public String getMailBoxMnemonics() throws SciException
	{
		return super.getUdf(UDFINDEX_MAILBOX_MNEMONICS);
	}
	
	public void setEmailAddress(String p_sEmailAddress) throws SciException
	{
		super.setUdf(UDFINDEX_EMAIL_ADDRESS, p_sEmailAddress);
	}
	
	public void setMailDescription(String p_sMailDescription) throws SciException
	{
		super.setUdf(UDFINDEX_MAIL_DESCRIPTION, p_sMailDescription);
	}
	
	public void setTypeOfMailBox(String p_eTypeOfMailBox) throws SciException
	{
		super.setUdf(UDFINDEX_TYPE_OF_MAILBOX, p_eTypeOfMailBox);
	}
	
	public void setModeOfOperation(String p_eModeOfOperation) throws SciException
	{
		super.setUdf(UDFINDEX_PERMITTED_MODE_OF_OPERATION, p_eModeOfOperation);
	}
	
	public void setMailBoxMnemonics(String p_eMailBoxMnemonics) throws SciException
	{
		super.setUdf(UDFINDEX_MAILBOX_MNEMONICS, p_eMailBoxMnemonics);
	}
	
	
}
