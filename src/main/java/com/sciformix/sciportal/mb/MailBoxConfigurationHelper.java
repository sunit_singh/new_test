package com.sciformix.sciportal.mb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.sciportal.mail.MailConfigurationHelper;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;

public class MailBoxConfigurationHelper {
	
	private static final Logger log = LoggerFactory.getLogger(MailConfigurationHelper.class);
	
	private MailBoxConfigurationHelper()
	{
		// Nothing to do
	}

	public static MailBox saveMailBox(int p_nProjectId, MailBox m_oMailBox,UserInfo u_oUserInfo, boolean p_bInsert) 
	{
		MailBox mailBoxTemp= null;
		try{
		return MailBoxHome.saveMailBox(m_oMailBox, u_oUserInfo, p_bInsert);
		}
		catch(SciServiceException scEx)
		{
			log.error("Error saving email box", scEx);
			return mailBoxTemp;
		}
	}
	
	public static MailBox getMailBoxBySeqId(int m_sequenceid) 
	{
	 try {
			return MailBoxHome.getMailBoxBySeqId(m_sequenceid);
	 	}
	 catch(SciException scExp)
	 {
		 log.error("Error updating mailbox status", scExp);
		return null;	
	 }
	}
	
}
