package com.sciformix.sciportal.mb;

import java.util.Date;

public class MBMailInfo {

	private int m_nMailSequenceId;
	private int m_nProjectSeqId;
	private int m_nMailBoxSeqId;
	private String mailAppSeqId;
	private String crc;
	private Date m_dMailTimestamp;
	private String m_sMailMessageId;
	private String m_sMailSender;
	private String m_sMailRecipientsTo;
	private String m_sMailRecipientsCc;
	private String m_sMailSubject;
	private int m_nMailAttachmentCount;
	private String m_nMailAttachmentNames;
	private int m_nMailSizeInBytes;
	private Date m_dDateCreated;
	private int m_nUserCreated;
	
	private MailType m_sMailType;
	private MailRecordMode m_sMailRecordMode;
	
	
	public MBMailInfo(int mailSeqId, int mailBoxSeqId, int userSeqid, int projectSeqId, String messageId, String appSeqId,
			String subject, int attachmentLenth, String fileNames, Date msgDate, String toEmailAddr, String ccEmailAddr,
			String fromEmail, MailRecordMode recordMode, MailType type, String crc, Date dtCreated)
	{
		this.m_nMailSequenceId = mailSeqId;
		this.m_nProjectSeqId = projectSeqId;
		this.m_nMailBoxSeqId = mailBoxSeqId;
		this.mailAppSeqId = appSeqId;
		this.crc = crc;
		this.m_dMailTimestamp = msgDate;
		this.m_sMailMessageId = messageId;
		this.m_sMailSender = fromEmail;
		this.m_sMailRecipientsTo = toEmailAddr;
		this.m_sMailRecipientsCc = ccEmailAddr;
		this.m_sMailSubject = subject;
		this.m_nMailAttachmentCount = attachmentLenth;
		this.m_nMailAttachmentNames = fileNames;
		this.m_dDateCreated = dtCreated;
		this.m_nUserCreated = userSeqid;
		
		this.m_sMailType = type;
		this.m_sMailRecordMode = recordMode;
		
	}
	
	
	public int getMailSequenceId() {
		return m_nMailSequenceId;
	}

	public void setMailSequenceId(int m_nMailSequenceId) {
		this.m_nMailSequenceId = m_nMailSequenceId;
	}

	public int getMailBoxSeqId() {
		return m_nMailBoxSeqId;
	}

	public void setMailBoxSeqId(int m_nMailBoxSeqId) {
		this.m_nMailBoxSeqId = m_nMailBoxSeqId;
	}

	public int getUserCreated() {
		return m_nUserCreated;
	}

	public void setUserCreated(int m_nUserCreated) {
		this.m_nUserCreated = m_nUserCreated;
	}
	
	public int getProjectSeqId() {
		return m_nProjectSeqId;
	}

	public void setProjectSeqId(int m_nProjectSeqId) {
		this.m_nProjectSeqId = m_nProjectSeqId;
	}
	public String getMailAppSeqId() {
		return mailAppSeqId;
	}

	public void setMailAppSeqId(String mailAppSeqId) {
		this.mailAppSeqId = mailAppSeqId;
	}
	public String getCrc() {
		return crc;
	}

	public void setCrc(String crc) {
		this.crc = crc;
	}
	public Date getMailTimeStamp() {
		return m_dMailTimestamp;
	}

	public void setMailTimeStamp(Date m_dMailTimeStamp) {
		this.m_dMailTimestamp = m_dMailTimeStamp;
	}

	public Date getDateCreated() {
		return m_dDateCreated;
	}

	public void setDateCreated(Date m_dDateCreated) {
		this.m_dDateCreated = m_dDateCreated;
	}
	
	public String getMailMessageId() {
		return m_sMailMessageId;
	}

	public void setMailMessageId(String m_sMailMessageId) {
		this.m_sMailMessageId = m_sMailMessageId;
	}

	public String getMailSender() {
		return m_sMailSender;
	}

	public void setMailSender(String m_sMailSender) {
		this.m_sMailSender = m_sMailSender;
	}

	public String getMailRecipientsTo() {
		return m_sMailRecipientsTo;
	}

	public void setMailRecipientsTo(String m_sMailRecipientsTo) {
		this.m_sMailRecipientsTo = m_sMailRecipientsTo;
	}

	public String getMailRecipientsCc() {
		return m_sMailRecipientsCc;
	}

	public void setMailRecipientsCc(String m_sMailRecipientsCc) {
		this.m_sMailRecipientsCc = m_sMailRecipientsCc;
	}

	public String getMailSubject() {
		return m_sMailSubject;
	}

	public void setMailSubject(String m_sMailSubject) {
		this.m_sMailSubject = m_sMailSubject;
	}

	public int getMailAttachmentCount() {
		return m_nMailAttachmentCount;
	}

	public void setMailAttachmentCount(int m_nMailAttachmentCount) {
		this.m_nMailAttachmentCount = m_nMailAttachmentCount;
	}

	public String getMailAttachmentNames() {
		return m_nMailAttachmentNames;
	}

	public void setMailAttachmentNames(String m_nMailAttachmentNames) {
		this.m_nMailAttachmentNames = m_nMailAttachmentNames;
	}

	public int getMailSizeInBytes() {
		return m_nMailSizeInBytes;
	}

	public void setMailSizeInBytes(int m_nMailSizeInBytes) {
		this.m_nMailSizeInBytes = m_nMailSizeInBytes;
	}

	public MailType getMailType() {
		return m_sMailType;
	}

	public void setMailType(MailType m_sMailType) {
		this.m_sMailType = m_sMailType;
	}

	public MailRecordMode getMailRecordMode() {
		return m_sMailRecordMode;
	}

	public void setMailRecordMode(MailRecordMode m_sMailRecordMode) {
		this.m_sMailRecordMode = m_sMailRecordMode;
	}
	
	public enum MailType {
		MAIL_IN("1","IN"), MAIL_OUT("2","OUT");
		
		private String code;
		private String displayName;
		
		MailType(String code, String displayName) {
			this.code 			=	code;
			this.displayName 	=	displayName;
		}

		public String getCode() {
			return code;
		}

		public String getDisplayName() {
			return displayName;
		}
		
		public static MailType toEnum(String code)
		{	
			for (MailType mailtype : MailType.values())
			{
				if (code.equals(mailtype.getCode()))
				{
					return mailtype;
				}
			}
			throw new IllegalArgumentException("MailType: Illegal enum value - " + code);
		}
	}
	
	public enum MailRecordMode {
		MAIL_WEB_API("1","Web API"), MAIL_MANUAL_ENTRY("2","Manual Entry"),
		MAIL_UPLOAD("3","Upload"), MAIL_ETC("4","etc");
		
		private String code;
		private String displayName;
		
		MailRecordMode(String code, String displayName) {
			this.code 			=	code;
			this.displayName 	=	displayName;
		}

		public String getCode() {
			return code;
		}

		public String getDisplayName() {
			return displayName;
		}
		
		public static MailRecordMode toEnum(String code)
		{	
			for (MailRecordMode l_mailRectype : MailRecordMode.values())
			{
				if (code.equals(l_mailRectype.getCode()))
				{
					return l_mailRectype;
				}
			}
			throw new IllegalArgumentException("MailRecordMode: Illegal enum value - " + code);
		}
	}
	
	
}
