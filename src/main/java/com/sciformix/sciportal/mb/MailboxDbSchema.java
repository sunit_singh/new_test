package com.sciformix.sciportal.mb;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class MailboxDbSchema
{

	public static MBMail MBMAIL= new MBMail();
	
	public static class MBMail extends DbTable
	{
		public DbColumn MAILSEQID = null;
		public DbColumn PRJSEQID = null;
		public DbColumn MAILBOXSEQID = null;
		public DbColumn MAILAPPSEQID= null;
		public DbColumn CRC= null;
		public DbColumn MAILTYPE = null;
		public DbColumn MAILCLIENTTIMESTAMP = null;
		public DbColumn MAILMESSAGEID = null;
		public DbColumn MAILRECORDMODE  = null;
		public DbColumn MAILSENDER = null;
		public DbColumn MAILRECIPIENTSTO = null;
		public DbColumn MAILRECIPIENTSCC = null;
		public DbColumn MAILSUBJECT = null;
		public DbColumn MAILATTACHMENTCOUNT = null;
		public DbColumn MAILATTACHMENTNAMES = null;
		public DbColumn MAILSIZEINBYTES = null;
		public DbColumn DTCREATED = null;
		public DbColumn USRCREATED = null;
		
		public MBMail()
		{
			super("MAILBOX_MAILS");
			setPkSequenceName("MAILBOX_MAILS_SEQID");
			
			MAILSEQID = addColumn("MAILSEQID");
			PRJSEQID= addColumn("PRJSEQID");
			MAILBOXSEQID= addColumn("MAILBOXSEQID");
			MAILAPPSEQID= addColumn("MAILAPPSEQID");
			CRC= addColumn("CRC");
			MAILTYPE= addColumn("MAILTYPE");
			MAILCLIENTTIMESTAMP= addColumn("MAILCLIENTTIMESTAMP");
			MAILMESSAGEID= addColumn("MAILMESSAGEID");
			MAILRECORDMODE= addColumn("MAILRECORDMODE");
			MAILSENDER= addColumn("MAILSENDER");
			MAILRECIPIENTSTO= addColumn("MAILRECIPIENTSTO");
			MAILRECIPIENTSCC= addColumn("MAILRECIPIENTSCC");
			MAILSUBJECT= addColumn("MAILSUBJECT");
			MAILATTACHMENTCOUNT= addColumn("MAILATTACHMENTCOUNT");
			MAILATTACHMENTNAMES= addColumn("MAILATTACHMENTNAMES");
			MAILSIZEINBYTES= addColumn("MAILSIZEINBYTES");
			DTCREATED= addColumn("DTCREATED");
			USRCREATED= addColumn("USRCREATED");
			
		}
	}
	
}
