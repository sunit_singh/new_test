package com.sciformix.sciportal.mb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hsmf.MAPIMessage;
import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.mb.MBMailInfo.MailType;

public class OutlookMsgParser {
	
	private static final Logger log = LoggerFactory.getLogger(OutlookMsgParser.class);
	
	private Map<String,Object> msgMap = null;
	
	public OutlookMsgParser(String stagefilepath) throws SciException
	{
		parseMsgFile(stagefilepath);
	}
	
	private void parseMsgFile(String stagefilepath) throws SciException
	{
		MAPIMessage mpiMessage = null;
		List<Attachment> attachmentList = null;
		String msgType = null;
		
		msgMap = new LinkedHashMap<>();
		attachmentList = new ArrayList<>();
		
		try
		{
			mpiMessage = new MAPIMessage(stagefilepath);
			
			msgType = returnEmptyString(mpiMessage.getMainChunks().getAll().get(MAPIProperty.PARENT_DISPLAY).get(0));
			msgMap.put("fromDisplayName", mpiMessage.getDisplayFrom() );
			msgMap.put("toDisplayName", mpiMessage.getDisplayTo());
			msgMap.put("ccDisplayName", mpiMessage.getDisplayCC());
			
			if(msgType.equals("Inbox"))
			{
				msgMap.put("messageType",MailType.MAIL_IN.getCode());
				msgMap.put("fromemailId", mpiMessage.getMainChunks().getAll().get(MAPIProperty.UNKNOWN).get(0) );
				
			}
			else
			{
				msgMap.put("messageType",MailType.MAIL_OUT.getCode());
				msgMap.put("fromemailId", mpiMessage.getMainChunks().getAll().get(MAPIProperty.UNKNOWN).get(2) );
			}
			
			
			msgMap.put("bccDisplayName", mpiMessage.getMainChunks().displayBCCChunk );
			msgMap.put("attaChmentFiles", mpiMessage.getAttachmentFiles() );
			if(!StringUtils.isNullOrEmpty(returnEmptyString(mpiMessage.getDisplayTo())) ||
					!StringUtils.isNullOrEmpty(returnEmptyString(mpiMessage.getMainChunks().displayCCChunk)))
			{
				msgMap.put("recpNames",returnEmptyString(mpiMessage.getRecipientNames()) );
				msgMap.put("recpEmailAddress" , returnEmptyString(mpiMessage.getRecipientEmailAddress()) );
			}
			
			
			//msgMap.put("conversationTopic", mpiMessage.getConversationTopic() );
			msgMap.put("summaryInfo", mpiMessage.getSummaryInformation() );
			msgMap.put("subject", mpiMessage.getMainChunks().subjectChunk);
			msgMap.put("textBody", mpiMessage.getTextBody() );
			msgMap.put("msgDate", mpiMessage.getMessageDate().getTime() );
			msgMap.put("attachementLength", mpiMessage.getAttachmentFiles().length );
			msgMap.put("messageId", mpiMessage.getMainChunks().messageId );
			
			if (mpiMessage.getAttachmentFiles().length > 0) {
				for (AttachmentChunks chunk : mpiMessage.getAttachmentFiles()) {
					attachmentList.add(new Attachment(chunk));
				}
			}
			msgMap.put("attachment",attachmentList);
			
		}
		catch(IOException ioExc)
		{
			log.error("Error parsing msg file");
			throw new SciException(ioExc.getMessage());
		}
		catch(ChunkNotFoundException chNtExcp)
		{
			log.error("Error getting msg file attributes");
			throw new SciException(chNtExcp.getMessage());
		}
		finally
		{
			if (mpiMessage != null)
			{
				try
				{
					mpiMessage.close();
				}
				catch(IOException ioExc)
				{
					log.error("Error parsing msg file");
					throw new SciException(ioExc.getMessage());
				}
			}
		}
	}
	
	public String getFromDisplayname() {
		return returnEmptyString(msgMap.get("fromDisplayName"));
	}
	
	public String getFromeEmailId() {
		String toMailid = returnEmptyString(msgMap.get("fromemailId"));
		if(toMailid.equals("EX"))
		{
			return  getFromDisplayname();
		}
		else if(getFromDisplayname().equals("HRSupport"))
		{
			return  getFromDisplayname();
		}
		return	toMailid.replaceAll("sip:",SciConstants.StringConstants.EMPTY);
	}

	public String getToDisplayName() {
		return returnEmptyString(msgMap.get("toDisplayName"));
	}
	public String getMessageType() {
		return returnEmptyString(msgMap.get("messageType"));
	}
	
	public String getMessageId() {
		return returnEmptyString(msgMap.get("messageId"));
	}

	public String getCcDisplayName() {
		return returnEmptyString(msgMap.get("ccDisplayName"));
	}

	public String getBccDisplayName() {
		return returnEmptyString(msgMap.get("bccDisplayName"));
	}

	public String getRecpNames() {
		return returnEmptyString(msgMap.get("recpNames"));
	}

	public String getRecpEmailAddress() {
		return returnEmptyString(msgMap.get("recpEmailAddress"));
	}

	public String getToEmailAddress() {
		List<String> recpArray = null;
		List<String> toList = null;
		String toEmails = SciConstants.StringConstants.EMPTY;
		int toCount = 0;
		String recpAddr = returnEmptyString(msgMap.get("recpEmailAddress"));
		String toDisplayNames = returnEmptyString(msgMap.get("toDisplayName"));
		
		if(!StringUtils.isNullOrEmpty(recpAddr) && !StringUtils.isNullOrEmpty(toDisplayNames))
		{
			recpArray = Arrays.asList(recpAddr.split(SciConstants.StringConstants.SEMICOLON));
			toCount = toDisplayNames.split(SciConstants.StringConstants.SEMICOLON).length;
			toList = new ArrayList<>(recpArray.size()-toCount);
			
			if(MailType.MAIL_OUT.getCode().equals(returnEmptyString(msgMap.get("messageType")))
					&&  !returnEmptyString(msgMap.get("subject")).startsWith("Re:"))
				{
					Collections.reverse(recpArray);
				}
			
			for(int count = 0; count<toCount ; count ++)
			{
				toList.add(recpArray.get(count));
				
			}
			
			toEmails = String.join(SciConstants.StringConstants.SEMICOLON, toList); 
		}	
		return toEmails;
	}
	
	public String getCcEmailAddress() {
		List<String> recpArray = null;
		List<String> toList = null;
		String ccEmails = SciConstants.StringConstants.EMPTY;
		int toCount = 0;
		String recpAddr = returnEmptyString(msgMap.get("recpEmailAddress"));
		String toDisplayNames = returnEmptyString(msgMap.get("toDisplayName"));
		
		if(!StringUtils.isNullOrEmpty(recpAddr) && !StringUtils.isNullOrEmpty(toDisplayNames))
		{
			recpArray = Arrays.asList(recpAddr.split(SciConstants.StringConstants.SEMICOLON));
			toCount = toDisplayNames.split(SciConstants.StringConstants.SEMICOLON).length;
			toList = new ArrayList<>(recpArray.size()-toCount);
			
			if(MailType.MAIL_OUT.getCode().equals(returnEmptyString(msgMap.get("messageType")))
					&&  !returnEmptyString(msgMap.get("subject")).startsWith("Re:"))
				{
					Collections.reverse(recpArray);
				}
			for(int count = toCount ; count<recpArray.size() ; count ++)
			{
				toList.add(recpArray.get(count));
			}
			ccEmails = String.join(SciConstants.StringConstants.SEMICOLON, toList); 
		}	
		return ccEmails;
	}
	public String getConversationTopic() {
		return returnEmptyString(msgMap.get("conversationTopic"));
	}

	public String getSummaryInfo() {
		return returnEmptyString(msgMap.get("summaryInfo"));
	}

	public String getSubject() {
		return returnEmptyString(msgMap.get("subject"));
	}

	public String getTextBody() {
		return returnEmptyString(msgMap.get("textBody"));
	}

	public int getAttachementLength() {
			return  (Integer)msgMap.get("attachementLength");
	}
	public Date getMsgDate() {
		return (Date)msgMap.get("msgDate");
	}
	public Map<String,Object> getMsgMap()
	{
		return msgMap;
	}
	/*public  List<Attachment> getAttachments()
	{
		return (List<Attachment>)msgMap.get("attachment");
	}
	public int getAttachmentFileSizeInBytes() {
		int size = 0; 
		for(Attachment attachment :(List<Attachment>)msgMap.get("attachment"))
		{
			size+=attachment.getAttachmentFileSizeInBytes();
		}
		return size;
	}
	*/
	public String getAttachmentFileNames() {
		String name = null;
		List<String> nameList = new ArrayList<>();
		for(Attachment attachment : (List<Attachment>)msgMap.get("attachment"))
		{
			name = attachment.getAttachmentFileName()+"("+attachment.getAttachmentFileSizeInBytes()+" bytes)";
			nameList.add(name);
		}
		return String.join(SciConstants.StringConstants.COMMA, nameList);
	}
	
	public String returnEmptyString(Object obj)
	{
		return (obj != null ? obj.toString() : StringConstants.EMPTY);
	}
}

 class Attachment
{
	private AttachmentChunks attachmetChunk= null;
	
	public Attachment(AttachmentChunks attachmetChunk) {
		this.attachmetChunk =attachmetChunk;
	}
	public String getAttachmentFileName() {
		return attachmetChunk.attachLongFileName.toString();
	}

	public int getAttachmentFileSizeInBytes() {
		return attachmetChunk.getAll().length;
	}
}
