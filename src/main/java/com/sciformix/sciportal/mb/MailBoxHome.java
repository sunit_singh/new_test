package com.sciformix.sciportal.mb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.sciportal.user.UserInfo;
import com.sciformix.sciportal.user.UserSession;

public class MailBoxHome {

	private static final Logger log = LoggerFactory.getLogger(MailBoxHome.class);
	
	private MailBoxHome()
	{
		// Nothing to do
	}
	
	public static MailBox saveMailBox(MailBox m_oMailTemplate,UserInfo u_oUserInfo, boolean p_bInsert) throws SciServiceException
	{
		try
		{
			return MailBoxHomeImpl.saveMailBox(m_oMailTemplate,u_oUserInfo,  p_bInsert);
		}
		catch(SciException sciExcep)
		{
			log.error("Exception while saving email box", sciExcep);
			throw new SciServiceException(ServiceErrorCodes.Email.ERROR_SAVING_EMAIL, sciExcep);
		}
	}
	
	public static MailBox getMailBoxBySeqId(int m_sequenceid) throws SciException
	{
		return MailBoxHomeImpl.getMailBoxBySeqId(m_sequenceid);
		
	}
}
