package com.sciformix.sciportal.mb;

import java.util.Comparator;

public class MailComparator implements Comparator<MBMailInfo> {

	@Override
	public int compare(MBMailInfo mailInfo1, MBMailInfo mailInfo2) {
		
		if(mailInfo1.getMailTimeStamp().before(mailInfo2.getMailTimeStamp()))
		{
			return 1;
		}
		else {
			return -1;
		}
	}
}
