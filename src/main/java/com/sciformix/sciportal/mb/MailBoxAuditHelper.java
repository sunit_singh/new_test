package com.sciformix.sciportal.mb;

public class MailBoxAuditHelper {

	public static class MailBoxAttributes
	{
		public static final String MAIL_BOX_SEQUENCE_ID = "Mail Box Sequence Id"; 
		public static final String MAIL_BOX_NAME = "Mail Box Name";
		public static final String MAIL_BOX_EMAIL_ADDRESS= "Mail Box Email Address";
		public static final String MAIL_BOX_DESCRIPTION = "Mail Box Description";
		public static final String TYPE_OF_MAIL_BOX = "Type of Mail Box";
		public static final String MAIL_BOX_PERMITTED_MODE_OF_OPERATION = "Mail Box Permitted Mode of Operation";
		public static final String MAIL_BOX_STATUS = "Mail Box Status";
		public static final String MAIL_SEQUENCE_ID = "Mail Sequence Id"; 
		public static final String MAIL_MSG_ID = "Mail Message Id"; 
		
	}
	
	private MailBoxAuditHelper()
	{
		//Nothing to do
	}
	
}
