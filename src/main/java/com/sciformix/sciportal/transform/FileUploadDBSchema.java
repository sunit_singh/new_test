package com.sciformix.sciportal.transform;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

class FileUploadDbSchema {
	public static FileUpload	FILE_UPLOAD  = new FileUpload();
	
	public static class FileUpload extends DbTable 
	{
		public DbColumn PRJSEQID = null;
		public DbColumn APPID = null;
		public DbColumn FILETYPE = null;
		public DbColumn FILEFORMAT = null;
		public DbColumn INTERNALNAME = null;
		public DbColumn DISPLAYNAME = null;
		public DbColumn DISPLAYORDERNUM = null;
		public DbColumn COLUMNTYPE = null;
		public DbColumn TRANSFORMATIONTYPE = null;
		public DbColumn TRANSFORMATIONLOGIC = null;

		public FileUpload() 
		{
			super("CMN_FILEUPLCOLUMNMAPPING");
			
			PRJSEQID = addColumn("PRJSEQID");
			APPID = addColumn("APPID");
			FILETYPE = addColumn("FILETYPE");
			FILEFORMAT = addColumn("FILEFORMAT");
			INTERNALNAME = addColumn("INTERNALNAME");
			DISPLAYNAME = addColumn("DISPLAYNAME");
			DISPLAYORDERNUM = addColumn("DISPLAYORDERNUM");
			COLUMNTYPE = addColumn("COLUMNTYPE");
			TRANSFORMATIONTYPE = addColumn("TRANSFORMATIONTYPE");
			TRANSFORMATIONLOGIC = addColumn("TRANSFORMATIONLOGIC");
		}
	}
}
