package com.sciformix.sciportal.transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.ServiceErrorCodes;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.DataTable.Data;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;

public class FileDataMapperUtil {
	
	private static Logger log	=	LoggerFactory.getLogger(FileDataMapperUtil.class);
	
	@SuppressWarnings("unchecked")
	public static DataTable transformData(
		int			p_projectid
	,	String		p_appid
	,	String		p_filetype
	,	String		p_fileformat
	,	DataTable	p_originaldatatable
	) throws SciServiceException {
		FileUploadMappedData					l_filemappeddata		=	null;
		Map<String, FileUploadColumnMapping>	l_columnmapping			=	null;
		FileUploadColumnMapping					l_columnmappingdetails	=	null;
		DataTable								l_tfdatatable			=	null;
		Object[]								l_listcolumndata		=	null;
		Data									l_columndata			=	null;
		List<String>							l_tfparameters			=	null,
												l_actualparameters		=	null;
		Class<? extends IBaseDataTransform>		l_classname				=	null;
		int										l_totalnorows			=	0;
		Map<String,Object>						l_extracteddatamap		=	null;		
		try {
			l_totalnorows		=	p_originaldatatable.rowCount();
			l_filemappeddata	=	FileUploadMappedData.getInstance();
			l_columnmapping		=	l_filemappeddata.getMappedColumnDetails().get(
										p_projectid + p_appid + p_filetype + p_fileformat
									);
			if (l_columnmapping != null) {
				l_tfdatatable	=	new DataTable();
				try (DbConnection connection = DbConnectionManager.getConnection()) {
					for (String l_internalcolumnname : l_columnmapping.keySet()) {
						try {
							l_columnmappingdetails	=	l_columnmapping.get(l_internalcolumnname);
							l_tfparameters			=	IBaseDataTransform.extractParameters(l_columnmappingdetails.getTransformationLogic());
							switch(l_columnmappingdetails.getTransformationType()) {
								case N:
									l_listcolumndata	=	new Object[l_totalnorows];
									for (int l_k=0; l_k < l_totalnorows; l_k++) {
										if (p_originaldatatable.getColumnValues(l_tfparameters.get(0)) != null) {
											l_listcolumndata[l_k]	=	p_originaldatatable.getColumnValues(
																			l_tfparameters.get(0)
																		).get(l_k);
										} else {
											l_listcolumndata[l_k]	=	"";
										}
										
									}
								break;	
								case J:
									try {
										l_classname	=	(Class<? extends IBaseDataTransform>) Class.forName(
															l_tfparameters.get(0)
														);
										
									} catch (ClassNotFoundException classExcep) {
										log.error(
											"Error in finding the Class"
										,	classExcep
										);
										throw new SciServiceException(
											ServiceErrorCodes.Common.ERROR_PARSING_FILE
										,	classExcep	
										);
									}
								break;	
								case F:
									try {
										l_classname	=	(Class<? extends IBaseDataTransform>) Class.forName(
															"com.sciformix.sciportal.transform.FTLDataTransform"
														);
									} catch (ClassNotFoundException classExcep) {
										log.error(
											"Error in finding the Class"
										,	classExcep
										);
										throw new SciServiceException(
											ServiceErrorCodes.Common.ERROR_PARSING_FILE
										,	classExcep	
										);
									}
								break;
								case S:
									try {
										l_classname	=	(Class<? extends IBaseDataTransform>) Class.forName(
															"com.sciformix.sciportal.transform.SQLDataTransform"
														);
									} catch (ClassNotFoundException classExcep) {
										log.error(
											"Error in finding the Class"
										,	classExcep
										);
										throw new SciServiceException(
											ServiceErrorCodes.Common.ERROR_PARSING_FILE
										,	classExcep	
										);
									}
								break;	
							}
							if (!(l_columnmappingdetails.getTransformationType() == TransformationType.N)) {
								l_listcolumndata	=	new Object[l_totalnorows];
								for (int l_i=0; l_i < l_totalnorows; l_i++) {
									if (l_columnmappingdetails.getTransformationType() == TransformationType.S) {
										l_actualparameters	=	new ArrayList<>();
										l_actualparameters.addAll(l_tfparameters);
										if (l_tfparameters.size() > 1) {
											for (int l_j=1; l_j < l_tfparameters.size(); l_j++) {
												if (l_tfparameters.get(l_j).equalsIgnoreCase("prjseqid")) {
													l_actualparameters.set(l_j,String.valueOf(p_projectid));
												} else {
													l_actualparameters.set(
														l_j
													, 	(String)p_originaldatatable.getColumnValues(
															l_tfparameters.get(l_j)
														).get(l_i)
													);
												}
											}
										}
										try {
											l_listcolumndata[l_i]	=	l_classname.newInstance().transform(
																			l_actualparameters
																		, 	null
																		, 	connection
																		);
										} catch (InstantiationException  | IllegalAccessException instExcep) {
											log.error(
												"Error invoking transformation method for transformation type SQL for column name"
													+ l_internalcolumnname
											,	instExcep
											);
											throw new SciServiceException(
												ServiceErrorCodes.Common.ERROR_PARSING_COLUMN
											,	null	
											,	instExcep	
											);
										}
									} else if (l_columnmappingdetails.getTransformationType() == TransformationType.J) {
										if (l_tfparameters.size() > 1) {
											for (int l_j=1; l_j<l_tfparameters.size(); l_j++) {
												l_tfparameters.set(
													l_j
												, 	(String)p_originaldatatable.getColumnValues(
														l_tfparameters.get(l_j)
													).get(l_i)
												);
											}
										}
										try {
											l_listcolumndata[l_i]	=	l_classname.newInstance().transform(
																			l_tfparameters
																		, 	null
																		, 	connection
																		);
										} catch (InstantiationException  | IllegalAccessException instExcep) {
											log.error(
												"Error invoking transformation method for transformation type Java for column name"
													+ l_internalcolumnname
											,	instExcep
											);
											throw new SciServiceException(
												ServiceErrorCodes.Common.ERROR_PARSING_COLUMN
											,	null	
											,	instExcep	
											);
										}
									} else {
										l_extracteddatamap	=	new HashMap<>();
										if (l_tfparameters.size() > 1) {
											for (int l_j=1; l_j<l_tfparameters.size(); l_j++) {
												l_extracteddatamap.put(
													l_tfparameters.get(l_j)
												, 	(String)p_originaldatatable.getColumnValues(
														l_tfparameters.get(l_j)
													).get(l_i)
												);
											}
										}
										try {
											l_listcolumndata[l_i]	=	l_classname.newInstance().transform(
																			l_tfparameters
																		, 	l_extracteddatamap
																		, 	connection
																		);
										} catch (InstantiationException  | IllegalAccessException instExcep) {
											log.error(
												"Error invoking transformation method for transformation type FTL for column name"
													+ l_internalcolumnname
											,	instExcep
											);
											throw new SciServiceException(
												ServiceErrorCodes.Common.ERROR_PARSING_COLUMN
											,	null	
											,	instExcep	
											);
										
										}
										l_extracteddatamap	=	null;
									}
								}
							}
							l_columndata	=	new DataTable.Data(l_internalcolumnname,l_listcolumndata);
							l_tfdatatable.addColumn(l_columndata);
						} catch (Exception excep) {
							log.error(
								"Error in parsing column "+	l_internalcolumnname
							,	excep
							);
							throw new SciServiceException(
								ServiceErrorCodes.Common.ERROR_PARSING_COLUMN
							,	null
							,	excep	
							);
						}
					}
				} catch (SciDbException dbExcep) {
					log.error(
						"Error in parsing uploaded file"
					,	dbExcep
					);
					throw new SciServiceException(
						ServiceErrorCodes.Common.ERROR_PARSING_FILE
					,	dbExcep	
					);
				}
				
			}
		} catch (Exception excep) {
			log.error(
				"Error in reading file upload column mapping table"
			,	excep
			);
			throw new SciServiceException(
				ServiceErrorCodes.Common.ERROR_PARSING_FILE
			,	excep	
			);
		}
		return l_tfdatatable;
	}
	
	/**
	 * This method is created to return the mapping of
	 * internal column name and the display name.
	 * The display name should be used to display on
	 * the UI and wherever we are displaying internal
	 * column name in the UI.
	 * @param p_projectid The project id for which the file
	 * 					  is uploaded.
	 * @param p_appid	  The application id in which the
	 * 					  file is uploaded
	 * @param p_filetype  The file type that need to be
	 * 					  used.
	 * @param p_fileformat The file format that was uploaded.
	 * @return	Map which contains the key as internal column
	 * 			name and value as display name.
	 * @throws SciServiceException The exception will be thrown
	 * 								if the data cannot be
	 * 								retrieved for the parameters
	 * 								passed in the method.
	 */
	public static Map<String, String> getFileMappingData(
		int			p_projectid
	,	String		p_appid
	,	String		p_filetype
	,	String		p_fileformat
	) throws SciServiceException {
		FileUploadMappedData					l_filemappeddata	=	null;
		Map<String, FileUploadColumnMapping>	l_columnmapping		=	null;
		Map<String, String>						l_displaymapping	=	null;
		
		try {
			l_filemappeddata	=	FileUploadMappedData.getInstance();
			l_columnmapping		=	l_filemappeddata.getMappedColumnDetails().get(
										p_projectid + p_appid + p_filetype + p_fileformat
									);
			if (l_columnmapping != null && l_columnmapping.size() > 0) {
				l_displaymapping	=	new LinkedHashMap<>();
				for (String l_internalcolumnname : l_columnmapping.keySet()) {
					l_displaymapping.put(
						l_internalcolumnname
					, 	l_columnmapping.get(l_internalcolumnname).getDisplayName()
					);
				}
			} else {
				log.error(
					"No data found for the combination of project id " + p_projectid + " , app id " + p_appid + " , file type " + p_filetype
					+	" and file format " + p_fileformat
				);
				throw new SciServiceException(
					ServiceErrorCodes.Common.MAPPING_COLUMN_DATA_NULL	
				);
			}
		} catch (SciException sciExcep) {
			log.error(
				"Error in reading file upload column mapping table"
			,	sciExcep
			);
			throw new SciServiceException(
				ServiceErrorCodes.Common.ERROR_PARSING_FILE
			,	sciExcep	
			);
		}
		return l_displaymapping;
	}
	
}
