package com.sciformix.sciportal.transform;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;

interface IBaseDataTransform {
	Object transform(
		List<String>		p_extracteddata
	,	Map<String,Object>	p_extracteddatamap
	,	DbConnection		p_connection
	) throws SciException;
	/**
	 * This method will be used to extract the parameters
	 * that are separated by ~ in the database.
	 * @param p_data	The data can consist of many forms:
	 * <ul>
	 * 	<li>Java: ClassName~param1~param2</li>
	 * 	<li>SQL: SQLQuery~param1~param2</li>
	 * 	<li></li>
	 * </ul>
	 * @return	It will return the list of parameters.
	 */
	static List<String> extractParameters (String p_data) {
		List<String>	l_tfdaata	=	new ArrayList<>();
		for (String l_param: p_data.split("~")) {
			l_tfdaata.add(l_param);
		}
		return l_tfdaata;
	}
}
