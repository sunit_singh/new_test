package com.sciformix.sciportal.transform;

import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;
import com.sciformix.sciportal.sqlquery.QueryRepoHome;
import com.sciformix.sciportal.utils.SystemUtils;

class
	SQLDataTransform
implements
	IBaseDataTransform
{
	@Override
	public Object transform(
		List<String>		p_extracteddatavalue
	,	Map<String,Object>	p_extracteddatamap
	,	DbConnection		p_connection
	) throws SciException {
		String			l_completequery	=	null,
						l_outputdata	=	null;
		DbQuerySelect	l_selectquery	=	null;
		
		if (p_extracteddatavalue != null) {
			try {
				l_completequery	=	QueryRepoHome.getQueryById(
										Integer.parseInt(
											p_extracteddatavalue.get(0)
										)	
									).replaceAll("%s",SystemUtils.getDBSchemaOwnerName());
			} catch (NumberFormatException e) {
				throw new SciException(
					"SQL Query Id cannot be parsed " + p_extracteddatavalue.get(0)
				);
			} catch (SciServiceException e) {
				throw new SciException(
					"Error executing the sql query"
				);
			}
			l_selectquery	=	DbQueryUtils.createSelectQuery(l_completequery);
			for (int l_i=1; l_i < p_extracteddatavalue.size(); l_i++) {
				l_selectquery.addWhereClauseParameter(p_extracteddatavalue.get(l_i));
			}
			try {
				p_connection.startTransaction();
				try (DbResultSet l_rs = p_connection.executeQuery(l_selectquery)) {
					if (l_rs != null) {
						while (l_rs.next()) {
							l_outputdata	=	l_rs.readString(1);
						}
					}
				}
				p_connection.commitTransaction();
			} catch (SciDbException dbExcep) {
				p_connection.rollbackTransaction();
				throw new SciException(
					"Error executing the sql query"
				,	dbExcep
				);
			}
		}
		return l_outputdata;
	}
}
