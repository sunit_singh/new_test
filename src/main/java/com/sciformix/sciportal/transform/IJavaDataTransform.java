package com.sciformix.sciportal.transform;

import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;

public interface 
	IJavaDataTransform
extends
	IBaseDataTransform
{
	Object transform(
		List<String>		p_extracteddata
	,	Map<String,Object>	p_extracteddatamap
	,	DbConnection		p_connection
	) throws SciException;
}
