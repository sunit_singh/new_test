package com.sciformix.sciportal.transform;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciEnums;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbConnection;
import com.sciformix.sciportal.database.DbConnectionManager;
import com.sciformix.sciportal.database.DbQuerySelect;
import com.sciformix.sciportal.database.DbQueryUtils;
import com.sciformix.sciportal.database.DbResultSet;

/**
 * This is a <b>singleton</b> class that will be used to 
 * fetch all the data from the database and map
 * all the internal column to the external column
 * of the file. The same class could have been 
 * implemented using <b>enum</b> but the problem 
 * was accessing the static field in the constructor.
 * The code is breakable through Reflection. 
 * @author gmishra
 *
 */
public class FileUploadMappedData {
	/**
	 * This map will contain the key as Project 
	 * Sequence Id, Application Id, File Type,
	 * File Format and value
	 * as the FileUploadColumnMapping object.
	 */
	private Map<String,Map<String,FileUploadColumnMapping>>
		mappedColumnDetails
	;
	/**
	 * This is the single instance that will be
	 * only there in JVM.
	 */
	private volatile static FileUploadMappedData
		fileUploadMappedData
	;
	
	private static Logger log	=	LoggerFactory.getLogger(FileUploadMappedData.class);
	
	private FileUploadMappedData() throws SciException {
		DbQuerySelect	l_selectquery		=	null;
		mappedColumnDetails	=	new HashMap<>();
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			l_selectquery	=	DbQueryUtils.createSelectAllColumnsQuery(
									FileUploadDbSchema.FILE_UPLOAD
								,	null
								,	FileUploadDbSchema.FILE_UPLOAD.PRJSEQID.name() + ", "
										+ FileUploadDbSchema.FILE_UPLOAD.FILEFORMAT.name() + ", "
										+ FileUploadDbSchema.FILE_UPLOAD.DISPLAYORDERNUM.name()
								);
			
			try (DbResultSet l_rs = connection.executeQuery(l_selectquery)) {
				Map<String,FileUploadColumnMapping>	l_mappedvalue	=	null;
				while (l_rs.next()) {
					FileUploadColumnMapping				l_mapping		=	null;
					String								l_key			=	null;
					
					try {
						l_mapping	=	FileUploadColumnMapping.createFileColumnMapping(
											l_rs.readInt(FileUploadDbSchema.FILE_UPLOAD.PRJSEQID)
										, 	l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.APPID)
										, 	l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.FILETYPE)
										,	l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.FILEFORMAT)
										, 	l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.INTERNALNAME)
										,	l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.DISPLAYNAME)
										,	l_rs.readInt(FileUploadDbSchema.FILE_UPLOAD.DISPLAYORDERNUM)
										, 	SciEnums.DataType.valueOf(l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.COLUMNTYPE))
										, 	TransformationType.valueOf(l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.TRANSFORMATIONTYPE))
										, 	l_rs.readString(FileUploadDbSchema.FILE_UPLOAD.TRANSFORMATIONLOGIC)
										);
						l_key	=	l_mapping.getProjectSequenceId()+l_mapping.getApplicationId()+l_mapping.getFileType()+l_mapping.getFileFormat();
						if (!mappedColumnDetails.containsKey(l_key)) {
							l_mappedvalue	=	new LinkedHashMap<>();
							mappedColumnDetails.put(l_key, l_mappedvalue);
						}
						l_mappedvalue.put(l_mapping.getInternalColumnName(), l_mapping);
						mappedColumnDetails.put(
							l_key
						,	l_mappedvalue
						);
					} catch (IllegalStateException illStateExcep) {
						log.error(
							"Error in loading file mapping column"
						, 	l_mapping.getProjectSequenceId()+l_mapping.getApplicationId()+l_mapping.getFileType()+l_mapping.getFileFormat()+l_mapping.getInternalColumnName()
						);
					}
				}
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error in loading file mapping column", dbExcep);
			throw new SciException("Error in loading file mapping column", dbExcep);
		}
		
	}
	
	/**
	 * This will be used to create the instance of the
	 * class. Double locking synchronization is used to
	 * create the object and make sure that only one
	 * object lives in the memory.
	 * @return
	 * @throws SciException
	 */
	public static FileUploadMappedData getInstance() throws SciException {
		if (fileUploadMappedData == null) {
			synchronized (FileUploadMappedData.class) {
				if (fileUploadMappedData == null) {
					fileUploadMappedData	=	new FileUploadMappedData();
				}
			}
		} 
		return fileUploadMappedData;
	}

	public Map<String,Map<String,FileUploadColumnMapping>> getMappedColumnDetails() {
		Map<String,Map<String,FileUploadColumnMapping>>	l_clonedfilecolumnmapping	=	new LinkedHashMap<>();
		l_clonedfilecolumnmapping	=	mappedColumnDetails;
		return l_clonedfilecolumnmapping;
	}
}
