package com.sciformix.sciportal.transform;

import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciException;
import com.sciformix.sciportal.apps.ngv.FreemarkerUtils;
import com.sciformix.sciportal.database.DbConnection;

public class 
	FTLDataTransform
implements
	IBaseDataTransform
{
	@Override
	public Object transform(
		List<String>		p_extracteddatavalue
	,	Map<String,Object>	p_extracteddatamap
	,	DbConnection		p_connection
	) throws SciException {
		List<Object> 		l_listreturnvalue	=	null;
		
		if (p_extracteddatamap != null) {
			l_listreturnvalue	=	FreemarkerUtils.replaceFreemarkerTemplate(p_extracteddatavalue.get(0), p_extracteddatamap);
		}
		return l_listreturnvalue.get(0);
	}
}
