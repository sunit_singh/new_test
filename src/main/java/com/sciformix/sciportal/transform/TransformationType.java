package com.sciformix.sciportal.transform;

/**
 * This enum will define the type of
 * transformation that will be allowed
 * in SciPortal. Their description
 * is mentioned with the object.
 * <ul>Type Supported
 * <li>N - No Transformation</li>
 * <li>J - Java</li>
 * <li>S - SQL</li>
 * <li>F - FTL</li>
 * </ul>
 * @author gmishra
 */
enum TransformationType {
	N("No"),
	J("Java"),
	S("SQL"),
	F("FTL");
	
	private String
		typeDescription
	;
	
	TransformationType(String p_typedescription) {
		typeDescription	=	p_typedescription;
	}
	
	public String getTypeDescription () {
		return typeDescription;
	}
}
