package com.sciformix.sciportal.transform;

import com.sciformix.commons.SciEnums.DataType;

/**
 * This class is designed to be immutable
 * @author gmishra
 *
 */
class FileUploadColumnMapping {
	/**
	 * This will be used to store the
	 * id of the project to which
	 * the column needs to be mapped
	 * or transformed.
	 */
	private final int
		projectSequenceId
	;
	/**
	 * This will be used to store the
	 * id of the application to which
	 * the column needs to be mapped
	 * or transformed.
	 */
	private final String
		applicationId
	;	
	/**
	 * This will be used to store the
	 * type of the file to which
	 * the column needs to be mapped
	 * or transformed.
	 */
	private final String
		fileType
	;
	/**
	 * This will be used to support
	 * multiple file formats for a
	 * single file type and application
	 * id.
	 */
	private final String
		fileFormat
	;	
	/**
	 * This will be used to store the
	 * name of the internal column that
	 * will be used by the SciPortal to
	 * identify the purpose for which the
	 * data is stored.
	 */
	private final String
		internalColumnName
	;
	/**
	 * This will be used to store the
	 * display name of the column on the
	 * UI. This will be mapped for each
	 * internal column name.
	 */
	private final String
		displayName
	;
	/**
	 * This will be used to store the
	 * sequence number in which the
	 * internal column name need to be
	 * displayed.
	 */
	private final int
		displayOrderNum
	;	
	/**
	 * This will be used to store the
	 * type of data being stored or accessed
	 * in the SciPortal.
	 */
	private final DataType
		internalColumnDataType
	;
	/**
	 * This will store the type of transformation
	 * that will be applicable on the external
	 * column to map it to the internal column.
	 */
	private final TransformationType
		transformationType
	;
	/**
	 * This will store the transformation logic
	 * that will be applied on the external column
	 * to be able to store in the internal column.
	 */
	private final String
		transformationLogic
	;	
	
	private FileUploadColumnMapping(
		int 				projectSequenceId
	, 	String 				applicationId
	, 	String 				fileType
	,	String				fileFormat
	,	String 				internalColumnName
	,	String				displayName
	,	int					displayOrderNum
	, 	DataType 			internalColumnDataType
	, 	TransformationType	transformationType
	,	String 				transformationLogic
	) {
		super();
		this.projectSequenceId = projectSequenceId;
		this.applicationId = applicationId;
		this.fileType = fileType;
		this.fileFormat = fileFormat;
		this.internalColumnName = internalColumnName;
		this.displayName = displayName;
		this.displayOrderNum = displayOrderNum;
		this.internalColumnDataType = internalColumnDataType;
		this.transformationType = transformationType;
		this.transformationLogic = transformationLogic;
	}
	
	/**
	 * Static method that is exposed to create the instance
	 * of the FileUploadColumnMapping class. The class is
	 * created as immutable.
	 * @param p_projectSequenceId
	 * @param p_applicationId
	 * @param p_fileType
	 * @param p_fileFormat
	 * @param p_internalColumnName
	 * @param p_internalColumnDataType
	 * @param p_transformationType
	 * @param p_transformationLogic
	 * @return
	 */
	public static FileUploadColumnMapping createFileColumnMapping (
		int 				p_projectSequenceId
	, 	String 				p_applicationId
	, 	String 				p_fileType
	, 	String 				p_fileFormat
	,	String 				p_internalColumnName
	,	String				p_displayName
	,	int					p_displayOrderNum
	, 	DataType 			p_internalColumnDataType
	, 	TransformationType	p_transformationType
	,	String 				p_transformationLogic
	) {
		return new FileUploadColumnMapping(
			p_projectSequenceId
		,	p_applicationId
		,	p_fileType
		,	p_fileFormat
		,	p_internalColumnName
		,	p_displayName
		,	p_displayOrderNum
		,	p_internalColumnDataType
		,	p_transformationType
		,	p_transformationLogic
		);	
	}
	/**
	 * This method will be used to return
	 * the project sequence id.
	 * @return Project Sequence Id
	 */
	public int getProjectSequenceId() {
		return projectSequenceId;
	}
	/**
	 * This method will be used to return
	 * the application id which is saved
	 * in the database.
	 * @return Application Id
	 */
	public String getApplicationId() {
		return applicationId;
	}
	/**
	 * This will return the file type
	 * maintained in the database.
	 * @return File Type
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * This will be used to return the
	 * different format for the file
	 * types maintained for an application.
	 * @return File Format
	 */
	public String getFileFormat() {
		return fileFormat;
	}
	/**
	 * This will return the internal column
	 * name that is used in the SciPortal
	 * for mapping external columns.
	 * @return Internal Column Name
	 */
	public String getInternalColumnName() {
		return internalColumnName;
	}
	/**
	 * This will return the name to be
	 * displayed on the UI for the internal
	 * column name.
	 * @return  Display Name of the internal
	 * 			column name.
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * This will return the order number
	 * in which it should be displayed.
	 * @return Display Order Number
	 */
	public int getDisplayOrderNum() {
		return displayOrderNum;
	}
	/**
	 * This will return the type in which
	 * the internal column name needs to be
	 * stored.
	 * @return Object of DataType
	 */
	public DataType getInternalColumnDataType() {
		return internalColumnDataType;
	}
	/**
	 * This will return the transformation type
	 * required to map external column value to
	 * the internal column value.
	 * @return Object of TransformationType
	 */
	public TransformationType getTransformationType() {
		return transformationType;
	}
	/**
	 * The will return the logic that is used
	 * to map external column value to the internal
	 * column value.
	 * @return Transformation Logic
	 */
	public String getTransformationLogic() {
		return transformationLogic;
	}
}
