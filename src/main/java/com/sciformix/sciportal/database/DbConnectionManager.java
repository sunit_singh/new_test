package com.sciformix.sciportal.database;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciStartupLogger;

public class DbConnectionManager
{
	private static final Logger log = LoggerFactory.getLogger(DbConnectionManager.class);

	public enum Errors
	{
		ERROR_CREATING_CONNECTION, ERROR_READING_RESULT_SET;
	}
	
	public static int CONNECTION_POOL_INITIAL_SIZE = 2;
	public static int CONNECTION_POOL_MAX_SIZE = 4;
	static AtomicInteger	l_tempconnseqidgenerator	=	new AtomicInteger(1000);
	private static final int _CONNECTIONID_INDEX_BASE = 100;
	
	private static Object oSyncVariable = new String("DbConnectionManager.SyncVariable");
	private static List<DbConnection> listConnectionPool = null;
	private static List<Boolean> listConnectionAvailable = null;
	private static int nNumberOfPooledConnections = 0;
	
	private DbConnectionManager()
	{
		//Nothing to do
	}
	
	public static boolean init(SciStartupLogger oStartupLogger)
	{
		synchronized (oSyncVariable)
		{
			oStartupLogger.log("DbDataManager initialization started");
			listConnectionPool = new ArrayList<DbConnection>(CONNECTION_POOL_MAX_SIZE);
			listConnectionAvailable = new ArrayList<>(CONNECTION_POOL_MAX_SIZE);
			nNumberOfPooledConnections = 0;
		}
		
		oStartupLogger.log("DbDataManager to create Primary Connection Pool [Initial size: " + CONNECTION_POOL_INITIAL_SIZE + "; Max size: " + CONNECTION_POOL_MAX_SIZE + "]");
		for (int index = 0; index < CONNECTION_POOL_INITIAL_SIZE; index++)
		{
			try
			{
				createNewPooledConnection(false);
				oStartupLogger.trace("DbConnectionPool connection #" + (_CONNECTIONID_INDEX_BASE+index+1) + " created");
			}
			catch (SciDbException sciExcep)
			{
				oStartupLogger.logError("DbConnectionPool connection creation error - " + sciExcep.getMessage());
				oStartupLogger.logError("DbDataManager initialization aborted");
				return false;
			}
		}
		oStartupLogger.log(dumpPoolStatus());
		oStartupLogger.log("DbDataManager initialization completed");
		return true;
	}
	
	public static String getPrimaryDbInfo()
	{
		return DbConnectionUtils.getPrimaryDbInfo();
	}
	
	public static DbConnection getConnection(String p_sSecondaryDbId) throws SciDbException
	{
		DbConnection connection = null;
		
		//Create new connection on specified secondary db for temporary purposes
		connection = new DbConnection(p_sSecondaryDbId);
		
		return connection;
	}
	
	public static DbConnection getConnection() throws SciDbException
	{
		DbConnection connection = null;
		
		//Search for any available connections in the Connection Pool
		//If available, return immediately and set it as Available = false
		connection = provideAvailableConnectionFromPool();
		if (connection != null)
		{
			if(log.isTraceEnabled())
			{
				log.trace("DbConnectionPool Connection #" + connection.getConnectionId() + " from pool returned");
			}
		}
		
		if (connection == null)
		{
			//No pooled connection available, create new one (either temporary or permanent)
			if (nNumberOfPooledConnections < CONNECTION_POOL_MAX_SIZE)
			{
				//Create new connection and add it to Pool
				connection = createNewPooledConnection(true);
				if(log.isTraceEnabled())
				{
					log.trace("DbConnectionPool connection #" + (connection.getConnectionId()) + " created");
				}
			}
			else
			{
				//Create new connection for temporary purposes
				connection = createNewTemporaryConnection();
				if(log.isTraceEnabled())
				{
					log.trace("DbConnectionPool TEMPORARY connection created - #{}",connection.getConnectionId());
				}
			}
		}
		
		if(log.isTraceEnabled())
		{
			log.trace(dumpPoolStatus());
		}
		return connection;
	}
	
	private static DbConnection provideAvailableConnectionFromPool()
	{
		if (nNumberOfPooledConnections == 0)
		{
			return null;
		}
		
		synchronized (oSyncVariable)
		{
			//Search
			if (listConnectionAvailable != null)
			{
				for (int index = 0; index < listConnectionAvailable.size(); index++)
				{
					if (listConnectionAvailable.get(index).booleanValue())
					{
						listConnectionAvailable.set(index, false);
						
						return listConnectionPool.get(index);
					}
				}
			}
		}
		
		return null;
	}
	
	private static DbConnection createNewPooledConnection(boolean bMarkAsNotAvailable) throws SciDbException
	{
		DbConnection connection = null;
		
		synchronized (oSyncVariable)
		{
			if (listConnectionAvailable != null)
			{
				connection = new DbConnection((listConnectionPool.size() + _CONNECTIONID_INDEX_BASE + 1));
				listConnectionPool.add(connection);
				listConnectionAvailable.add(!bMarkAsNotAvailable);
				nNumberOfPooledConnections++;
			}			
		}
		
		return connection;
	}
	
	private static DbConnection createNewTemporaryConnection() throws SciDbException
	{
		DbConnection connection = null;
		
		connection = new DbConnection(0);
		
		return connection;
	}
	
	public static DbConnection getBootstrapDBConnection() throws SciDbException
	{
		DbConnection connection = null;
		
		connection = new DbConnection(0);
		
		return connection;
	}
	
	static void markConnectionAsAvailable(int nConnectionId)
	{
		synchronized (oSyncVariable)
		{
			if (listConnectionAvailable != null && listConnectionAvailable.size() > 0)
			{
				listConnectionAvailable.set((nConnectionId - _CONNECTIONID_INDEX_BASE - 1), true);
			}
		}
	}
	
	public static String dumpPoolStatus()
	{
		DbConnection connection = null;
		String s = null;
		
		s = "";
		if (listConnectionPool != null && listConnectionPool.size() > 0)
		{
			for (int nCounter = 0; nCounter < listConnectionPool.size(); nCounter++)
			{
				connection = listConnectionPool.get(nCounter);
				s += "[#" + connection.getConnectionId() + ":(" + listConnectionAvailable.get(nCounter) + ") " + connection.toString() + "]";
			}
		}
		return s;
	}
}
