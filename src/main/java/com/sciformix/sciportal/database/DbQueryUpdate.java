package com.sciformix.sciportal.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DbQueryUpdate
{
	private static final String _QUERY_TEMPLATE_WHERE = "UPDATE %s SET %s WHERE %s";
	private static final String _QUERY_TEMPLATE_BASE = "UPDATE %s SET %s";

	private boolean m_bDirectSqlProvided = false;
	private String m_sRawSql = null;
	private String m_sTableName = null;
	private String m_sColumnNames = null;
	private List<DbQueryParam> m_listUpdateParameters = null;
	private String m_sWhereClause = null;
	private List<DbQueryParam> m_listWhereClauseParameters = null;
	
	static DbQueryUpdate create(String p_sTableName, String p_sColumnNames, String p_sWhereClause)
	{
		return new DbQueryUpdate(p_sTableName, p_sColumnNames, p_sWhereClause);
	}
	
	static DbQueryUpdate create(String p_sRawSql)
	{
		return new DbQueryUpdate(p_sRawSql);
	}
	
	private DbQueryUpdate(String p_sTableName, String p_sColumnNames, String p_sWhereClause)
	{
		m_bDirectSqlProvided = false;
		m_sTableName = p_sTableName;
		m_sColumnNames = p_sColumnNames;
		m_listUpdateParameters = new ArrayList<DbQueryParam>();
		m_sWhereClause = p_sWhereClause;
		m_listWhereClauseParameters = new ArrayList<DbQueryParam>();
	}
	
	private DbQueryUpdate(String p_sRawSql)
	{
		m_bDirectSqlProvided = true;
		m_sRawSql = p_sRawSql;
		m_listUpdateParameters = new ArrayList<DbQueryParam>();
		m_listWhereClauseParameters = new ArrayList<DbQueryParam>();
	}
	
	public void addUpdateParameter(int p_nDataValue)
	{
		m_listUpdateParameters.add(DbQueryParam.createParameter(p_nDataValue));
	}
	
	public void addUpdateParameter(DbQueryParam p_oDbQueryParameter)
	{
		m_listUpdateParameters.add(p_oDbQueryParameter);
	}
	
	public void addUpdateParameter(String p_sDataValue)
	{
		m_listUpdateParameters.add(DbQueryParam.createParameter(p_sDataValue));
	}
	
	public void addUpdateParameter(Date p_dtDataValue)
	{
		m_listUpdateParameters.add(DbQueryParam.createParameter(p_dtDataValue));
	}
	
	public void addWhereClauseParameter(int p_nDataValue)
	{
		m_listWhereClauseParameters.add(DbQueryParam.createParameter(p_nDataValue));
	}
	
	public void addWhereClauseParameter(DbQueryParam p_oDbQueryParameter)
	{
		m_listWhereClauseParameters.add(p_oDbQueryParameter);
	}
	
	public void addWhereClauseParameter(String p_sDataValue)
	{
		m_listWhereClauseParameters.add(DbQueryParam.createParameter(p_sDataValue));
	}
	
	List<DbQueryParam> getListOfWhereClauseParameters()
	{
		return m_listWhereClauseParameters;
	}
	
	List<DbQueryParam> getListOfUpdateParameters()
	{
		return m_listUpdateParameters;
	}
	
	String toSql()
	{
		if (m_bDirectSqlProvided)
		{
			return m_sRawSql;
		}
		else
		{
			if (m_sWhereClause != null)
			{
				return String.format(_QUERY_TEMPLATE_WHERE, DbQueryUtils.getQualifiedObjectName(m_sTableName), m_sColumnNames, m_sWhereClause);
			}
			else
			{
				return String.format(_QUERY_TEMPLATE_BASE, DbQueryUtils.getQualifiedObjectName(m_sTableName), m_sColumnNames);
			}
		}
	}

}
