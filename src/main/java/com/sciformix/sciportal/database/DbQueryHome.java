package com.sciformix.sciportal.database;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.commons.SciException;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class DbQueryHome
{
	private static final Logger log = LoggerFactory.getLogger(DbQueryHome.class);

	private DbQueryHome()
	{
		//Nothing to do
	}

	public static int generateSequenceValueForTablePk(DbTable p_oTable) throws SciException
	{
		int nSeqNextVal = -1;
		DbQuerySelect oSelectQuery = null;
		
		oSelectQuery = DbQuerySelect.createGenerateSequence(p_oTable.pkSequenceName());
		
		try (DbConnection connection = DbConnectionManager.getConnection())
		{
			try
			{
				connection.startTransaction();
				
				try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
				{
					while (resultSet.next())
					{
						nSeqNextVal = resultSet.readInt(1);
						
						break;
					}
				}
				
				connection.commitTransaction();
			}
			catch (SciDbException dbExcep)
			{
				connection.rollbackTransaction();
				log.error("Error reading Sequence next-value from Db", dbExcep);
				throw new SciException("Error reading Sequence next-value from Db", dbExcep);
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error reading Sequence next-value from Db", dbExcep);
			throw new SciException("Error reading Sequence next-value from Db", dbExcep);
		}
		
		return nSeqNextVal;
	}

	public static boolean isObjectNameUnique(DbConnection connection, String p_sObjectDescriptor, DbTable p_oDbTable, String p_sObjectName, 
			String p_sWhereClause, DbQueryParam... p_oarrWhereClauseBindParameters) throws SciException
	{
		int nCount = 0;
		DbQuerySelect oSelectQuery = null;
		boolean bIsObjectNameUnique = false;
		
		oSelectQuery = DbQueryUtils.createSelectQuery(p_oDbTable, DbQuerySelect.COLUMN_COUNT_STAR, p_sWhereClause);
		for (DbQueryParam oDataParameterValue : p_oarrWhereClauseBindParameters)
		{
			oSelectQuery.addWhereClauseParameter(oDataParameterValue);
		}
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(oSelectQuery))
			{
				if(resultSet.next())
				{
					nCount = resultSet.readInt(1);
					
					if (nCount < 1)
					{
						bIsObjectNameUnique = true;
					}
					else
					{
						log.info("Object - {} with name - {} already exists", p_sObjectDescriptor, p_sObjectName);
						bIsObjectNameUnique = false;
					}
				}
				else
				{
					//do nothing
					bIsObjectNameUnique = true;
				}
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("ERROR checking for uniqueness for Object - {} with name - {}", p_sObjectDescriptor, p_sObjectName, dbExcep);
			throw new SciException("Error checking uniqueness", dbExcep);
		}
		
		return bIsObjectNameUnique;
	}
	
	public static List<String> getStringColumnData(DbConnection connection, DbQuerySelect p_oSelectQuery) throws SciException
	{
		List<String> listColumnData = null;
		
		listColumnData = new ArrayList<String>();
		
		try
		{
			connection.startTransaction();
			
			try (DbResultSet resultSet = connection.executeQuery(p_oSelectQuery))
			{
				while (resultSet.next())
				{
					listColumnData.add(resultSet.readString(1));
				}
			}
			
			connection.commitTransaction();
		}
		catch (SciDbException dbExcep)
		{
			connection.rollbackTransaction();
			log.error("Error reading string column data from DB", dbExcep);
			throw new SciException("Error reading string column data from DB", dbExcep);
		}
		
		return listColumnData;
	}
}
