package com.sciformix.sciportal.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.sciportal.utils.SystemUtils;

class DbConnectionUtils
{
	public static final String DBINFOID_PRIMARY = "PRIMARY";
	public static final String DBINFOID_SECONDARY_01 = "SECONDARY_01";
	public static final String DBINFOID_SECONDARY_02 = "SECONDARY_02";
	public static final String DBINFOID_SECONDARY_03 = "SECONDARY_03";
	
	private static final String _PROPERTY_SCIPORTAL_DATABASE_HOST = "sciportal.database.host";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_PORTNO = "sciportal.database.portno";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_INSTANCE = "sciportal.database.instance";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_NAME = "sciportal.database.connectuser.name";
	private static final String _PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_PASSWORD = "sciportal.database.connectuser.password";
	
	private static final String JDBC_DRIVER_NAME_ORACLE = "oracle.jdbc.driver.OracleDriver";
	
	private static final Logger log = LoggerFactory.getLogger(DbConnectionUtils.class);
	
	
	private static Map<String, DbInfo> s_mapDbInfo = null;
	
	static
	{
		try
		{
			Class.forName(JDBC_DRIVER_NAME_ORACLE);
		}
		catch (ClassNotFoundException cnfExcep)
		{
			log.error("Error loading Database driver", cnfExcep);
		}
	}
	
	static
	{
		s_mapDbInfo = new HashMap<>();
		
		s_mapDbInfo.put(DBINFOID_PRIMARY, DbInfo.createOracleDbInfo(
													SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_HOST),
													SystemUtils.getPortalIntProperty(_PROPERTY_SCIPORTAL_DATABASE_PORTNO),
													SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_INSTANCE),
													SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_NAME),
													SystemUtils.getDBUserPassword(SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_PASSWORD), 
													SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_HOST))
													));
	}
	
	private DbConnectionUtils()
	{
		//Nothing to do
	}
	
	public static String getPrimaryDbInfo()
	{
		return 
			SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_HOST) + " : " +
			SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_INSTANCE) + " : " +
			SystemUtils.getPortalProperty(_PROPERTY_SCIPORTAL_DATABASE_CONNECTUSER_NAME);
	}
	
	public static Connection createJdbcConnection() throws SQLException
	{
		return createJdbcConnection(DBINFOID_PRIMARY);
	}
	
	public static Connection createJdbcConnection(String p_sDbInfoId) throws SQLException
	{
		return DriverManager.getConnection(getDbConnectionUrl(p_sDbInfoId), getDbConnectUserUsername(p_sDbInfoId), 
				getDbConnectUserPassword(p_sDbInfoId));
	}
	
	private static String getDbConnectionUrl(String p_sDbInfoId)
	{
		return s_mapDbInfo.get(p_sDbInfoId).getConnectionUrl();				
	}
	
	private static String getDbConnectUserUsername(String p_sDbInfoId)
	{
		return s_mapDbInfo.get(p_sDbInfoId).getConnectUserUserName();				
	}
	
	private static String getDbConnectUserPassword(String p_sDbInfoId)
	{
		return s_mapDbInfo.get(p_sDbInfoId).getConnectUserUserPassword();	
	}
}
