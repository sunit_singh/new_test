package com.sciformix.sciportal.database;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.sciportal.database.DbQueryParam.DataType;
import com.sciformix.sciportal.database.DbQueryParam.ParameterType;

public class DbConnection implements AutoCloseable
{
	private static final Logger log = LoggerFactory.getLogger(DbConnection.class);
	
	private static final int FUNCTION_PARAMTER_INDEX_OUT_PARAMETER = 1;
	
	private int nConnectionId = 0;
	private DbConnectionImpl connection = null;
	private PreparedStatement oStatement = null;
	private CallableStatement oCallableStatement = null;
	private boolean bTemporaryConnection = false;
	
	private boolean bTransactionInProgress = false;
	private int nTransactionDepth = 0;
	
	
	public DbConnection(int p_nConnectionId) throws SciDbException
	{
		nConnectionId = p_nConnectionId;
		bTemporaryConnection = (p_nConnectionId == 0);
		if (bTemporaryConnection)
		{
			nConnectionId = DbConnectionManager.l_tempconnseqidgenerator.incrementAndGet();
		}
		
		connection = new DbConnectionImpl();
		connection.getConnection();
	}
	
	public DbConnection(String p_sSecondaryDbId) throws SciDbException
	{
		nConnectionId = 0;
		bTemporaryConnection = true;
		{
			connection = new DbConnectionImpl();
			connection.getConnection(p_sSecondaryDbId);
		}
	}
	
	public int getConnectionId()
	{
		return nConnectionId;
	}
	
	public DbQueryParam executeFunction(DbFunctionExecute p_ofunctionexecute) throws SciDbException
	{
		int parameterIndex = 0;
		DbQueryParam	l_outparam	=	null;
		
		try
		{
			oCallableStatement	=	connection.prepareCall(p_ofunctionexecute.toSql());
			//DEV-NOTE: Parameter Indexes are 1-based
			bindOutParameter(oCallableStatement, FUNCTION_PARAMTER_INDEX_OUT_PARAMETER, p_ofunctionexecute.getReturnType());
			if (p_ofunctionexecute.getParameterList() != null) {
				//DEV-NOTE: Input Parameters start after OUT Parameter and that is 1-based
				parameterIndex = FUNCTION_PARAMTER_INDEX_OUT_PARAMETER + 1;
				for (DbQueryParam oParam : p_ofunctionexecute.getParameterList())
				{
					bindParameter(oCallableStatement, parameterIndex, oParam);
					parameterIndex++;
				}
			}
			oCallableStatement.execute();
			l_outparam	=	retrieveOutputParameterValue(oCallableStatement, FUNCTION_PARAMTER_INDEX_OUT_PARAMETER,p_ofunctionexecute.getReturnType());
		} catch (SQLException sqlExcep) {
			log.error("Error executing function", sqlExcep);
			throw new SciDbException("Error executing function", sqlExcep);
		}
		finally
		{
			try
			{
				oCallableStatement.close();
			}
			catch (SQLException sqlExcep)
			{
				log.error("Ignoring error closing Statement object",  sqlExcep);
			}
		}
		return l_outparam;
	}
	
	public DbResultSet executeQuery(DbQuerySelect p_oSelectQuery) throws SciDbException
	{
		DbResultSet dbResultSet = null;
		int parameterIndex = 0;
		
		try
		{
			if (log.isTraceEnabled())
			{
				log.trace("[#" + nConnectionId + "] SELECT QUERY is: " + p_oSelectQuery.toSql());
			}
			
			oStatement = connection.prepareStatement(p_oSelectQuery.toSql());
			
			//DEV-NOTE: Following loop is 1-based
			if (p_oSelectQuery.getListOfWhereClauseParameters() != null)
			{
				//DEV-NOTE: Parameter usage at Jdbc is 1-based
				parameterIndex = 1;
				
				for (DbQueryParam oParam : p_oSelectQuery.getListOfWhereClauseParameters())
				{
					bindParameter(oStatement, parameterIndex, oParam);
					parameterIndex++;
				}
			}
			
			dbResultSet = DbResultSet.create(oStatement.executeQuery());
			
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error executing query", sqlExcep);
			throw new SciDbException("Error executing query", sqlExcep);
		}
		return dbResultSet;
	}

	public int executeQuery(DbQueryUpdate p_oUpdateQuery) throws SciDbException
	{
		int nReturnValue = -1;
		int parameterIndex = 0;
		
		try
		{
			if (log.isTraceEnabled())
			{
				log.trace("[#" + nConnectionId + "] UPDATE QUERY is: " + p_oUpdateQuery.toSql());
			}
			
			oStatement = connection.prepareStatement(p_oUpdateQuery.toSql());
			
			//DEV-NOTE: Following loop is 1-based
			if (p_oUpdateQuery.getListOfUpdateParameters() != null)
			{
				//DEV-NOTE: Parameter usage at Jdbc is 1-based
				parameterIndex = 1;
				
				for (DbQueryParam oParam : p_oUpdateQuery.getListOfUpdateParameters())
				{
					bindParameter(oStatement, parameterIndex, oParam);
					parameterIndex++;
				}
			}
			
			//DEV-NOTE: Following loop is 1-based
			if (p_oUpdateQuery.getListOfWhereClauseParameters() != null)
			{
				//DEV-NOTE: Continue usage of parameter-index from previous loop
				
				for (DbQueryParam oParam : p_oUpdateQuery.getListOfWhereClauseParameters())
				{
					bindParameter(oStatement, parameterIndex, oParam);
					parameterIndex++;
				}
			}
			
			nReturnValue = oStatement.executeUpdate();
			
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error executing query", sqlExcep);
			throw new SciDbException("Error executing query", sqlExcep);
		}
		finally
		{
			try
			{
				oStatement.close();
			}
			catch (SQLException sqlExcep)
			{
				log.error("Ignoring error closing Statement object", sqlExcep);
			}
		}
		return nReturnValue;
	}
	
	public int executeQuery(DbQueryInsert p_oInsertQuery) throws SciDbException
	{
		int nReturnValue = -1;
		int parameterIndex = 0;
		
		try
		{
			if (log.isTraceEnabled())
			{
				log.trace("[#" + nConnectionId + "] INSERT QUERY is: " + p_oInsertQuery.toSql());
			}
			oStatement = connection.prepareStatement(p_oInsertQuery.toSql());
			
			//DEV-NOTE: Following loop is 1-based
			if (p_oInsertQuery.getListOfInsertParameters() != null)
			{
				//DEV-NOTE: Parameter usage at Jdbc is 1-based
				parameterIndex = 1;
				
				for (DbQueryParam oParam : p_oInsertQuery.getListOfInsertParameters())
				{
					bindParameter(oStatement, parameterIndex, oParam);
					parameterIndex++;
				}
			}
			
			nReturnValue = oStatement.executeUpdate();
			
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error executing query", sqlExcep);
			throw new SciDbException("Error executing query", sqlExcep);
		}
		finally
		{
			try
			{
				oStatement.close();
			}
			catch (SQLException sqlExcep)
			{
				log.error("Ignoring error closing Statement object", sqlExcep);
			}
		}
		return nReturnValue;
	}
	
	public int executeQuery(DbQueryDelete p_oDeleteQuery) throws SciDbException
	{
		int nReturnValue = -1;
		int parameterIndex = 0;
		
		try
		{
			if (log.isTraceEnabled())
			{
				log.trace("[#" + nConnectionId + "] DELETE QUERY is: " + p_oDeleteQuery.toSql());
			}
			
			 oStatement = connection.prepareStatement(p_oDeleteQuery.toSql());
			if (p_oDeleteQuery.getListOfWhereClauseParameters() != null)
			{
				//DEV-NOTE: Parameter usage at Jdbc is 1-based
				parameterIndex = 1;
				
				for (DbQueryParam oParam : p_oDeleteQuery.getListOfWhereClauseParameters())
				{
					bindParameter(oStatement, parameterIndex, oParam);
					parameterIndex++;
				}
			}
			
			System.out.println(p_oDeleteQuery.toSql());
			nReturnValue = oStatement.executeUpdate();
			 
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error executing query", sqlExcep);
			throw new SciDbException("Error executing query", sqlExcep);
		}
		finally
		{
			try
			{
				oStatement.close();
			}
			catch (SQLException sqlExcep)
			{
				log.error("Ignoring error closing Statement object",  sqlExcep);
			}
		}
		
		return  nReturnValue;
		
	}
	
	public void close() throws SciDbException
	{
		if (oStatement != null)
		{
			try
			{
				oStatement.close();
			}
			catch (SQLException sqlExcep)
			{
				log.error("Ignoring error closing Statement object", sqlExcep);
			}
		}
		
		if (!bTransactionInProgress)
		{
			if (bTemporaryConnection)
			{
				if (connection != null)
				{
					try
					{
						connection.close();
					}
					catch (SciDbException dbExcep)
					{
						log.error("Ignoring error closing Connection object", dbExcep);
					}
				}
				log.trace("DbConnectionPool TEMPORARY connection destroyed #{}", nConnectionId);
			}
			else
			{
				DbConnectionManager.markConnectionAsAvailable(nConnectionId);
				
				log.trace("DbConnectionPool Connection marked as Available - #{}", nConnectionId);
			}
		}
		
		if(log.isTraceEnabled())
		{
			log.trace(DbConnectionManager.dumpPoolStatus());
		}
	}
	
	public void startTransaction() throws SciDbException
	{
		{
			if (!bTransactionInProgress)
			{
				connection.setAutoCommit(false);
				nTransactionDepth = 1;
				bTransactionInProgress = true;
				log.trace("Starting Transaction at DB-level NOW - #{}", nConnectionId);
			}
			else
			{
				nTransactionDepth++;
				log.trace("Simply incrementing Transaction depth to - {} for #{}", nTransactionDepth, nConnectionId);
			}
		}
		if(log.isTraceEnabled())
		{
			log.trace(DbConnectionManager.dumpPoolStatus());
		}
	}
	
	public void commitTransaction() throws SciDbException
	{
		{
			if (!bTransactionInProgress || nTransactionDepth < 1)
			{
				//ERROR
			}
			else
			{
				nTransactionDepth--;
				if (nTransactionDepth == 0)
				{
					log.trace("Committing the transaction at DB-level NOW - #{}", nConnectionId);
					connection.commit();
					bTransactionInProgress = false;
				}
				else
				{
					log.trace("Simply decremented the transaction depth count to - {}  for #{}", nTransactionDepth, nConnectionId);
				}
			}
		}
		if(log.isTraceEnabled())
		{
			log.trace(DbConnectionManager.dumpPoolStatus());
		}
	}
	
	public void rollbackTransaction()
	{
		try
		{
			if (!bTransactionInProgress || nTransactionDepth < 1)
			{
				//ERROR
			}
			else
			{
				nTransactionDepth--;
				if (nTransactionDepth == 0)
				{
					log.trace("ROLLING BACK the transaction at DB-level NOW - #{}", nConnectionId);
					connection.rollback();
					bTransactionInProgress = false;
				}
				else
				{
					log.trace("ROLLBACK: Simply decremented the transaction depth count to - {}  for #{}", nTransactionDepth, nConnectionId);
				}
			}
		}
		catch (SciDbException dbExcep)
		{
			log.error("Error rollbacking the transaction. Swallow and continue", dbExcep);
		}
		if(log.isTraceEnabled())
		{
			log.trace(DbConnectionManager.dumpPoolStatus());
		}
	}
	
	private void bindParameter(PreparedStatement p_oStatement, int parameterIndex, DbQueryParam oParam) throws SQLException, SciDbException
	{
		if (oParam.getParameterType() == ParameterType.OUT)
		{
			((CallableStatement) p_oStatement).registerOutParameter(parameterIndex, oParam.dataType().getSqlType());
			if (log.isTraceEnabled())
			{
				log.trace("\tBinding parameter out#"+ parameterIndex + " [" + oParam.dataType() + "] ");
			}
		}
		else if (oParam.getParameterType() == ParameterType.INOUT)
		{
			
		}
		else if (oParam.getParameterType() == ParameterType.IN)
		{
			switch (oParam.dataType())
			{
				case INTEGER:
				{
					p_oStatement.setInt(parameterIndex,((Integer)oParam.value()).intValue());
					if (log.isTraceEnabled())
					{
						log.trace("\tBinding parameter #"+ parameterIndex + " [" + oParam.dataType() + "] " + oParam.value());
					}
					break;
				}
				case STRING:
				{
					p_oStatement.setString(parameterIndex, ((String)oParam.value()));
					if (log.isTraceEnabled())
					{
						if (oParam.value() != null && ((String)oParam.value()).length() > 32)
						{
							log.trace("\tBinding parameter #"+ parameterIndex + " [" + oParam.dataType() + "] " + ((String)oParam.value()).substring(0, 32) + "(...)");
						}
						else
						{
							log.trace("\tBinding parameter #"+ parameterIndex + " [" + oParam.dataType() + "] " + oParam.value());
						}
					}
					break;
				}
				case DATETIME:
				{
					p_oStatement.setDate(parameterIndex, new java.sql.Date(((Date)oParam.value()).getTime()));
					if (log.isTraceEnabled())
					{
						log.trace("\tBinding parameter #"+ parameterIndex + " [" + oParam.dataType() + "] " + oParam.value());
					}
					break;
				}
				case SPECIAL_DATETIME_NOW:
				{
					p_oStatement.setDate(parameterIndex, new java.sql.Date(System.currentTimeMillis()));
					if (log.isTraceEnabled())
					{
						log.trace("\tBinding parameter #"+ parameterIndex + " [" + oParam.dataType() + "] " + oParam.value());
					}
					break;
				}
				default:
				{
					log.error("Invalid Bind Parameter - {}", oParam.dataType().name());
					throw new SciDbException("Invalid Bind Parameter");
				}
			}
		}
		else
		{
			//TODO:Error
		}
	}
	
	private DbQueryParam retrieveOutputParameterValue(CallableStatement p_oStatement, int parameterIndex, DataType oDataType) throws SQLException
	{
		DbQueryParam	l_oParam	=	null;
		switch(oDataType) {
			case INTEGER:
			{
				l_oParam	=	DbQueryParam.createOutParameter(oDataType, p_oStatement.getInt(parameterIndex));
			}
			break;
			case STRING:
			{
				l_oParam	=	DbQueryParam.createOutParameter(oDataType, p_oStatement.getString(parameterIndex));
			}
			break;
			case DATETIME:
			{
				l_oParam	=	DbQueryParam.createOutParameter(oDataType, p_oStatement.getDate(parameterIndex));
			}
			break;
			case DOUBLE:
			{
				l_oParam	=	DbQueryParam.createOutParameter(oDataType, p_oStatement.getDouble(parameterIndex));
			}
			break;
			case SPECIAL_DATETIME_NOW:
			{
				l_oParam	=	DbQueryParam.createOutParameter(oDataType, p_oStatement.getDate(parameterIndex));
			}
			break;
		}
		return l_oParam;
	}
	
	private void bindOutParameter(PreparedStatement p_oStatement, int parameterIndex, DataType oDataType) throws SQLException 
	{
		((CallableStatement) p_oStatement).registerOutParameter(parameterIndex, oDataType.getSqlType());
		if (log.isTraceEnabled())
		{
			log.trace("\tBinding parameter out#"+ parameterIndex + " [" + oDataType + "] ");
		}
	}
	
	
	
	@Override
	public String toString()
	{
		return bTemporaryConnection+":"+bTransactionInProgress+":"+nTransactionDepth;
	}
}
