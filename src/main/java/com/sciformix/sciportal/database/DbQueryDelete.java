package com.sciformix.sciportal.database;

import java.util.ArrayList;
import java.util.List;

import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;

public class DbQueryDelete
{
	private static final String _QUERY_WHERE = "DELETE %s WHERE %s";
	private static final String _QUERY_BASIC = "DELETE %s";
	
	private boolean m_bDirectSqlProvided = false;
	private String m_sRawSql = null;
	private String m_sTableName = null;
	private String m_sWhereClause = null;
	private List<DbQueryParam> m_listWhereClauseParameters = null;
	
	static DbQueryDelete create(DbTable p_oTable, String p_sWhereClause)
	{
		return new DbQueryDelete(p_oTable.name(), p_sWhereClause);
	}
	
	static DbQueryDelete create(String p_sRawSql)
	{
		return new DbQueryDelete(p_sRawSql);
	}
	
	private DbQueryDelete(String p_sTableName, String p_sWhereClause)
	{
		m_bDirectSqlProvided = false;
		m_sTableName = p_sTableName;
		m_sWhereClause = p_sWhereClause;
		m_listWhereClauseParameters = new ArrayList<DbQueryParam>();
	}
	
	private DbQueryDelete(String p_sRawSql)
	{
		m_bDirectSqlProvided = true;
		m_sRawSql = p_sRawSql;
		m_listWhereClauseParameters = new ArrayList<DbQueryParam>();
	}
	
	public void addWhereClauseParameter(int p_nDataValue)
	{
		m_listWhereClauseParameters.add(DbQueryParam.createParameter(p_nDataValue));
	}
	
	public void addWhereClauseParameter(DbQueryParam p_oDbQueryParameter)
	{
		m_listWhereClauseParameters.add(p_oDbQueryParameter);
	}
	
	public void addWhereClauseParameter(String p_sDataValue)
	{
		m_listWhereClauseParameters.add(DbQueryParam.createParameter(p_sDataValue));
	}
	
	List<DbQueryParam> getListOfWhereClauseParameters()
	{
		return m_listWhereClauseParameters;
	}
	
	String toSql()
	{
		if (m_bDirectSqlProvided)
		{
			return m_sRawSql;
		}
		else
		{
			if (m_sWhereClause != null)
			{
				return String.format(_QUERY_WHERE, DbQueryUtils.getQualifiedObjectName(m_sTableName), m_sWhereClause);
			}
			else
			{
				return String.format(_QUERY_BASIC, DbQueryUtils.getQualifiedObjectName(m_sTableName));
			}
		}
	}
}
