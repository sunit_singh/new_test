package com.sciformix.sciportal.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;

class DbConnectionImpl
{
	private static final Logger log = LoggerFactory.getLogger(DbConnectionImpl.class);

	Connection oJdbcConnection=null;
	
	public DbConnectionImpl()
	{
		//Nothing to do
	}
	
	public boolean getConnection() throws SciDbException
	{
		return getConnection(DbConnectionUtils.DBINFOID_PRIMARY);
	}
	
	public boolean getConnection(String p_sDbInfoId) throws SciDbException
	{
		boolean flag = false;
		try
		{
			oJdbcConnection = DbConnectionUtils.createJdbcConnection(p_sDbInfoId);
			flag = oJdbcConnection.isValid(0);//TODO:Parameterize this
		}
		catch(SQLException sqlExcep)
		{
			log.error("Error getting connection", sqlExcep);
			throw new SciDbException("Error getting connection", sqlExcep);
		}
		return flag;
	}
	
	public PreparedStatement prepareStatement(String query) throws SciDbException
	{
		try
		{
			return oJdbcConnection.prepareStatement(query);
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error preparing statement", sqlExcep);
			throw new SciDbException("Error preparing statement", sqlExcep);
		}
	}
	
	public CallableStatement prepareCall(String query) throws SciDbException {
		try {
			return oJdbcConnection.prepareCall(query);
		} catch (SQLException sqlExcep)
		{
			log.error("Error preparing callable statement", sqlExcep);
			throw new SciDbException("Error preparing callable statement", sqlExcep);
		}
	}
	
	public void setAutoCommit(boolean autoCommit) throws SciDbException
	{
		try
		{
			oJdbcConnection.setAutoCommit(autoCommit);
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error setting auto commit flag", sqlExcep);
			throw new SciDbException("Error setting auto commit flag", sqlExcep);
		}
	}
	
	public void commit() throws SciDbException
	{
		try
		{
			oJdbcConnection.commit();
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error committing", sqlExcep);
			throw new SciDbException("Error committing", sqlExcep);
		}
	}
	
	public void rollback() throws SciDbException
	{
		try
		{
			oJdbcConnection.rollback();
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error in rollback", sqlExcep);
			throw new SciDbException("Error in rollback", sqlExcep);
		}
	}
	
	public void close() throws SciDbException
	{		
		try
		{
			if (oJdbcConnection != null && !oJdbcConnection.isClosed())
			{
				oJdbcConnection.close();
			}
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error closing JDBC connection", sqlExcep);
			throw new SciDbException("Error closing JDBC connection", sqlExcep);
		}				
	}
}
