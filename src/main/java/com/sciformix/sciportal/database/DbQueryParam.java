package com.sciformix.sciportal.database;

import java.sql.Types;
import java.util.Date;

public class DbQueryParam
{
	public static final DbQueryParam SPECIAL_PARAMETER_DATETIME_NOW = new DbQueryParam(DataType.SPECIAL_DATETIME_NOW, null, ParameterType.IN);
	
	public enum DataType
	{
		STRING (Types.VARCHAR), INTEGER (Types.INTEGER), DATETIME (Types.DATE), SPECIAL_DATETIME_NOW (Types.DATE), DOUBLE (Types.DOUBLE);
		
		private int sqlType = Types.NULL;
		private DataType(int p_sqlType)
		{
			sqlType = p_sqlType;
		}
		public int getSqlType() {
			return sqlType;
		}
		
	}

	public enum ParameterType
	{
		IN, OUT, INOUT;
	}
	
	public static DbQueryParam createOutParameter(DataType p_eDataType, Object p_oDataValue)
	{
		return new DbQueryParam(p_eDataType, p_oDataValue, ParameterType.OUT);
	}

	public static DbQueryParam createParameter(String p_sDataValue)
	{
		return new DbQueryParam(DataType.STRING, p_sDataValue, ParameterType.IN);
	}
	
	public static DbQueryParam createParameter(int p_nDataValue)
	{
		return new DbQueryParam(DataType.INTEGER, p_nDataValue, ParameterType.IN);
	}
	
	public static DbQueryParam createParameter(double p_nDataValue)
	{
		return new DbQueryParam(DataType.INTEGER, p_nDataValue, ParameterType.IN);
	}
	
	public static DbQueryParam createParameter(Date p_dtDataValue)
	{
		return new DbQueryParam(DataType.DATETIME, p_dtDataValue, ParameterType.IN);
	}
	
	public static DbQueryParam[] createNullParameters()
	{
		return new DbQueryParam[0];
	}
	
	private DataType m_eDataType = null;
	private Object m_oDataValue = null;
	private ParameterType m_eParameterType = null;
	
	private DbQueryParam(DataType p_eDataType, Object p_oDataValue, ParameterType p_eParameterType)
	{
		m_eDataType = p_eDataType;
		m_oDataValue = p_oDataValue;
		m_eParameterType = p_eParameterType;
	}
	
	DbQueryParam (DataType p_eDataType) {
		m_eDataType = p_eDataType;
	}
	
	public DataType dataType()
	{
		return m_eDataType;
	}
	
	public Object value()
	{
		return m_oDataValue;
	}
	
	public ParameterType getParameterType() {
		return m_eParameterType;
	}

	
}
