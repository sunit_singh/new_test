package com.sciformix.sciportal.database;

import com.sciformix.commons.SciConstants;

class DbInfo
{
	public static enum DbType
	{
		ORACLE;
	}
	
	private static final String _JDBC_URL_TEMPLATE_ORACLE = "jdbc:oracle:thin:@%s:%s:%s";
	
	private DbType eDbType = null;
	private String sDbHostName = null;
	private int nPortNumber = -1;
	private String sDbInstance = null;
	private String sConnectUserUserName = null;
	private String sConnectUserUserPassword = null;
	private String sConnectionUrl = null;
	
	private DbInfo(DbType p_eDbType, String p_sDbHostName, int p_nPortNumber, String p_sConnectUserUserInstance, String p_sConnectUserUserName, 
								String p_sConnectUserUserPassword)
	{
		eDbType = p_eDbType;
		sDbHostName = p_sDbHostName;
		nPortNumber = p_nPortNumber;
		sDbInstance = p_sConnectUserUserInstance;
		sConnectUserUserName = p_sConnectUserUserName;
		sConnectUserUserPassword = p_sConnectUserUserPassword;
		
		if (eDbType == DbType.ORACLE)
		{
			sConnectionUrl = String.format(_JDBC_URL_TEMPLATE_ORACLE, sDbHostName, nPortNumber, sDbInstance);
		}
		else
		{
			//Not supported
			sConnectionUrl = SciConstants.StringConstants.EMPTY;
		}
	}
	
	public static DbInfo createOracleDbInfo(String p_sDbHostName, int p_nPortNumber, String p_sConnectUserUserInstance, String p_sConnectUserUserName, 
				String p_sConnectUserUserPassword)
	{
		return new DbInfo(DbType.ORACLE, p_sDbHostName, p_nPortNumber, p_sConnectUserUserInstance, p_sConnectUserUserName, p_sConnectUserUserPassword);
	}
	
	public String getConnectionUrl()
	{
		return sConnectionUrl;
	}

	public String getConnectUserUserName()
	{
		return sConnectUserUserName;
	}

	public String getConnectUserUserPassword()
	{
		return sConnectUserUserPassword;
	}

	public DbType getDbType()
	{
		return eDbType;
	}

}
