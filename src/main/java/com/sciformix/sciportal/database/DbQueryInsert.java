package com.sciformix.sciportal.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sciformix.commons.SciConstants;

public class DbQueryInsert
{
	private static final String _QUERY_TEMPLATE = "INSERT INTO %s (%s) VALUES (%s)";
	
	private boolean m_bDirectSqlProvided = false;
	private String m_sRawSql = null;
	private String m_sTableName = null;
	private String m_sColumnNames = null;
	private List<DbQueryParam> m_listInsertParameters = null;
	
	static DbQueryInsert create(String p_sTableName, String p_sColumnNames)
	{
		return new DbQueryInsert(p_sTableName, p_sColumnNames);
	}
	
	static DbQueryInsert create(String p_sRawSql)
	{
		return new DbQueryInsert(p_sRawSql);
	}
	
	private DbQueryInsert(String p_sTableName, String p_sColumnNames)
	{
		m_bDirectSqlProvided = false;
		m_sTableName = p_sTableName;
		m_sColumnNames = p_sColumnNames;
		m_listInsertParameters = new ArrayList<DbQueryParam>();
	}
	
	private DbQueryInsert(String p_sRawSql)
	{
		m_bDirectSqlProvided = true;
		m_sRawSql = p_sRawSql;
		m_listInsertParameters = new ArrayList<DbQueryParam>();
	}
	
	public void addInsertParameter(int p_nDataValue)
	{
		m_listInsertParameters.add(DbQueryParam.createParameter(p_nDataValue));
	}
	
	public void addInsertParameter(DbQueryParam p_oDbQueryParameter)
	{
		m_listInsertParameters.add(p_oDbQueryParameter);
	}
	
	public void addInsertParameter(String p_sDataValue)
	{
		m_listInsertParameters.add(DbQueryParam.createParameter(p_sDataValue));
	}
	
	public void addInsertParameter(Date p_dtDataValue)
	{
		m_listInsertParameters.add(DbQueryParam.createParameter(p_dtDataValue));
	}
	
	List<DbQueryParam> getListOfInsertParameters()
	{
		return m_listInsertParameters;
	}
	
	String toSql()
	{
		if (m_bDirectSqlProvided)
		{
			return m_sRawSql;
		}
		else
		{
			StringBuffer sbInsertValuesClause = new StringBuffer();
			
			for (int nFieldCounter = 0; nFieldCounter < m_listInsertParameters.size(); nFieldCounter++)
			{
				if (nFieldCounter > 0)
				{
					sbInsertValuesClause.append(SciConstants.StringConstants.COMMA);
				}
				sbInsertValuesClause.append(SciConstants.StringConstants.QUESTIONMARK);
			}
			
			return String.format(_QUERY_TEMPLATE, DbQueryUtils.getQualifiedObjectName(m_sTableName), m_sColumnNames, sbInsertValuesClause.toString());
		}
	}

}
