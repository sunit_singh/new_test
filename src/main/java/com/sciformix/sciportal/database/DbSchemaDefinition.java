package com.sciformix.sciportal.database;

import com.sciformix.commons.SciConstants;
import com.sciformix.sciportal.database.DbQueryParam.DataType;

public class DbSchemaDefinition
{
	public static enum DbConjunction
	{
		AND, OR;
		
		public String clause()
		{
			return String.format(" %s ", this.name());
		}
	}
	
	public static enum DbOperator
	{
		EQUALS("="), GREATER_THAN(">"), LESS_THAN("<"), GREATER_THAN_EQUAL_TO(">="), LESS_THAN_EQUAL_TO("<="), BETWEEN("BETWEEN");
		
		private String sqlValue;
		
		DbOperator(String sqlValue)
		{
			this.sqlValue = sqlValue;
		}
		
		public String sqlValue()
		{
			return this.sqlValue;
		}
	}
	
	
	static final String _COLUMN_BIND_CLAUSE_FORMAT = "%s %s ?";
	
	static final String _COLUMN_BETWEEN_CLAUSE_FORMAT = "%s %s ? and ?";
	
	static final String _COLUMN_BETWEEN_CLAUSE_FORMAT_DATE = "%s %s to_date(?,%s) and to_date(?,%s)";
	
	public static class DbTable
	{
		private String m_sName = null;
		private String m_sAllColumnNames = null;
		private String m_sPkSequenceName = null;
		
		protected DbTable(String p_sName)
		{
			m_sName = p_sName;
		}
		
		public String name()
		{
			return m_sName;
		}
		
		public String allColumnNames()
		{
			return m_sAllColumnNames;
		}
		
		public String pkSequenceName()
		{
			return m_sPkSequenceName;
		}
		
		protected DbColumn addColumn(String p_sColumnName)
		{
			DbColumn oColumn = new DbColumn(p_sColumnName);
			if (m_sAllColumnNames == null)
			{
				m_sAllColumnNames = p_sColumnName;
			}
			else
			{
				m_sAllColumnNames += SciConstants.StringConstants.COMMA + p_sColumnName;
			}
			return oColumn;
		}
		
		protected void setPkSequenceName(String p_sPkSequenceName)
		{
			m_sPkSequenceName = p_sPkSequenceName;
		}
	}
	
	public static class DbColumn
	{
		private String m_sName = null;
		
		private DbColumn(String p_sName)
		{
			m_sName = p_sName;
		}
		
		public String name()
		{
			return m_sName;
		}
		
		public String bindClause()
		{
			return bindClause(DbOperator.EQUALS);
		}
		
		public String bindClause(DbOperator p_eOperator)
		{
			return String.format(_COLUMN_BIND_CLAUSE_FORMAT, m_sName, p_eOperator.sqlValue);
		}
		
		public String bindClauseBetween ()
		{
			return String.format(_COLUMN_BETWEEN_CLAUSE_FORMAT, m_sName, DbOperator.BETWEEN.sqlValue);
		}
		
//		public String bindClauseBetweenDate (DbOperator p_eOperator, String p_formatter)
//		{
//			return String.format(_COLUMN_BETWEEN_CLAUSE_FORMAT_DATE, m_sName, p_eOperator.sqlValue,p_formatter,p_formatter);
//		}
		
	}
	public static class DbFunction 
	{
		private String name;
		private int numberOfParameters;
		private DataType returnType;
		
		protected DbFunction(String p_name, int p_numberOfParameters, DbQueryParam.DataType p_returntype)
		{
			name = p_name;
			numberOfParameters = p_numberOfParameters;
			returnType = p_returntype;
		}
		
		public DataType getReturnType() {
			return returnType;
		}
		
		public String getName() {
			return name;
		}
		
		public int getNumberOfParameters() {
			return numberOfParameters;
		}
		
	}
	

}
