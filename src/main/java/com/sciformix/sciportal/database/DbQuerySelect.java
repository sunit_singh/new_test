package com.sciformix.sciportal.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sciformix.commons.SciConstants;

public class DbQuerySelect
{
	public static final String COLUMN_COUNT_STAR = "COUNT(*)";
	
	private static final String _QUERY_TEMPLATE_WHERE_ORDER = "SELECT %s FROM %s WHERE %s ORDER BY %s";
	private static final String _QUERY_TEMPLATE_WHERE = "SELECT %s FROM %s WHERE %s";
	private static final String _QUERY_TEMPLATE_ORDER = "SELECT %s FROM %s ORDER BY %s";
	private static final String _QUERY_TEMPLATE_BASE = "SELECT %s FROM %s";
	
	private static final String _QUERY_TEMPLATE_GENERATE_SEQUENCE = "SELECT %s.nextval FROM DUAL";
	
	private boolean m_bDirectSqlProvided = false;
	private String m_sRawSql = null;
	private String m_sTableName = null;
	private String m_sColumnNames = null;
	private String m_sWhereClause = null;
	private List<DbQueryParam> m_listWhereClauseParameters = null;
	private String m_sOrderbyClause = null;
	
	static DbQuerySelect create(String p_sTableName, String p_sColumnNames, String p_sWhereClause, String p_sOrderbyClause)
	{
		return new DbQuerySelect(p_sTableName, p_sColumnNames, p_sWhereClause, p_sOrderbyClause);
	}
	
	static DbQuerySelect createGenerateSequence(String p_sSequenceName)
	{
		return new DbQuerySelect(String.format(_QUERY_TEMPLATE_GENERATE_SEQUENCE, DbQueryUtils.getQualifiedObjectName(p_sSequenceName)));
	}
	
	static DbQuerySelect create(String p_sRawSql) {
		return new DbQuerySelect(p_sRawSql);
	}
	
	private DbQuerySelect(String p_sTableName, String p_sColumnNames, String p_sWhereClause, String p_sOrderbyClause)
	{
		m_bDirectSqlProvided = false;
		m_sTableName = p_sTableName;
		m_sColumnNames = p_sColumnNames;
		m_sWhereClause = p_sWhereClause;
		m_listWhereClauseParameters = new ArrayList<DbQueryParam>();
		m_sOrderbyClause = p_sOrderbyClause;
	}
	
	private DbQuerySelect(String p_sRawSql)
	{
		m_bDirectSqlProvided = true;
		m_sRawSql = p_sRawSql;
		m_listWhereClauseParameters = new ArrayList<DbQueryParam>();
	}
	
	public void addWhereClauseParameter(int p_nDataValue)
	{
		m_listWhereClauseParameters.add(DbQueryParam.createParameter(p_nDataValue));
	}
	
	public void addWhereClauseParameter(Date p_dtDataValue)
	{
		m_listWhereClauseParameters.add(DbQueryParam.createParameter(p_dtDataValue));
	}
	
	public void addWhereClauseParameter(DbQueryParam p_oDbQueryParameter)
	{
		m_listWhereClauseParameters.add(p_oDbQueryParameter);
	}
	
	public void addWhereClauseParameter(String p_sDataValue)
	{
		m_listWhereClauseParameters.add(DbQueryParam.createParameter(p_sDataValue));
	}
	
	List<DbQueryParam> getListOfWhereClauseParameters()
	{
		return m_listWhereClauseParameters;
	}
	
	String toSql()
	{
		if (m_bDirectSqlProvided)
		{
			return m_sRawSql;
		}
		else if (m_sWhereClause != null && m_sOrderbyClause != null)
		{
			return String.format(_QUERY_TEMPLATE_WHERE_ORDER, m_sColumnNames, DbQueryUtils.getQualifiedObjectName(m_sTableName), m_sWhereClause, m_sOrderbyClause);
		}
		else if (m_sWhereClause != null && m_sOrderbyClause == null)
		{
			return String.format(_QUERY_TEMPLATE_WHERE, m_sColumnNames, DbQueryUtils.getQualifiedObjectName(m_sTableName), m_sWhereClause);
		}
		else if (m_sWhereClause == null && m_sOrderbyClause != null)
		{
			return String.format(_QUERY_TEMPLATE_ORDER, m_sColumnNames, DbQueryUtils.getQualifiedObjectName(m_sTableName), m_sOrderbyClause);
		}
		else if (m_sWhereClause == null && m_sOrderbyClause == null)
		{
			return String.format(_QUERY_TEMPLATE_BASE, m_sColumnNames, DbQueryUtils.getQualifiedObjectName(m_sTableName));
		}
		else
		{
			//Each case needs to be handled explicitly above
			return SciConstants.StringConstants.EMPTY;
		}
	}

}
