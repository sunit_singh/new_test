package com.sciformix.sciportal.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.sciportal.database.DbQueryParam.DataType;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbFunction;

public class DbFunctionExecute {
	
	private static final String _FUNCTION_TEMPLATE_BASE = "{? = call %s(%s)}";
	
	private List<DbQueryParam> parameterList;
	private String m_sFunctionName;
	private int m_nNumberOfParameters;
	private DataType returnType;
	
	static DbFunctionExecute prepareFunctionForExecution(DbFunction p_oDbFunction) throws SciDbException{
		return new DbFunctionExecute(p_oDbFunction.getName(), p_oDbFunction.getNumberOfParameters(), p_oDbFunction.getReturnType());
	}
	
	private DbFunctionExecute (String p_sName, int p_nNumberOfParameters, DataType p_returntype) throws SciDbException {
		if (p_returntype == DataType.SPECIAL_DATETIME_NOW) {
			throw new SciDbException("Number of create request parameter are greater than the allowed number of parameters");
		}
		m_sFunctionName			=	p_sName;
		m_nNumberOfParameters	=	p_nNumberOfParameters;
		returnType				=	p_returntype;
		parameterList			=	new ArrayList<>();
	}
	
	public DataType getReturnType() {
		return returnType;
	}
	
	public void createParameter(int p_nDataValue) throws SciDbException {
		createParameter(DbQueryParam.createParameter(p_nDataValue));
	}
	
	public void createParameter(String p_sDataValue) throws SciDbException {
		createParameter(DbQueryParam.createParameter(p_sDataValue));
	}
	
	public void createParameter(Date p_dtDataValue) throws SciDbException
	{
		createParameter(DbQueryParam.createParameter(p_dtDataValue));
	}
	
	private void createParameter(DbQueryParam p_oDbQueryParameter) throws SciDbException
	{
		if (parameterList.size() > m_nNumberOfParameters) {
			throw new SciDbException("Number of create request parameter are greater than the allowed number of parameters");
		}
		parameterList.add(p_oDbQueryParameter);
	}
	
	List<DbQueryParam> getParameterList()
	{
		return parameterList;
	}
	
	
	String toSql() {
		StringBuilder	l_prepareparameters	=	new StringBuilder();
		/**
		 * DEV-NOTE: This is 1-based for loop
		 */
		for (int i=1; i<=m_nNumberOfParameters; i++) {
			if (i == m_nNumberOfParameters) {
				l_prepareparameters.append(StringConstants.QUESTIONMARK);
			} else {
				l_prepareparameters.append(StringConstants.QUESTIONMARK_WITH_COMMA);
			}
		}
		return String.format(_FUNCTION_TEMPLATE_BASE, DbQueryUtils.getQualifiedObjectName(m_sFunctionName), l_prepareparameters.toString());
	}
}
