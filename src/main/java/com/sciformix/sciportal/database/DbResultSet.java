package com.sciformix.sciportal.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciDbException;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;

public class DbResultSet implements AutoCloseable
{
	private static final Logger log = LoggerFactory.getLogger(DbResultSet.class);

	private ResultSet resultSet = null;
	
	public static DbResultSet create(ResultSet resultSet)
	{
		return new DbResultSet(resultSet);
	}
	
	private DbResultSet(ResultSet resultSet)
	{
		this.resultSet = resultSet;
	}
	
	public boolean next() throws SciDbException
	{
		try
		{
			return resultSet.next();
		}
		catch (SQLException sqlExcep)
		{ 
			log.error("Error during next", sqlExcep);
			throw new SciDbException("Error during next", sqlExcep);
		}
	}
	
	public String readString(int p_nColumnIndex) throws SciDbException
	{
		try
		{
			return resultSet.getString(p_nColumnIndex);
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error reading string", sqlExcep);
			throw new SciDbException("Error reading string", sqlExcep);
		}
	}
	
	public String readString(DbColumn p_oColumn) throws SciDbException
	{
		try
		{
			return resultSet.getString(p_oColumn.name());
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error reading string", sqlExcep);
			throw new SciDbException("Error reading string", sqlExcep);
		}
	}
	
	public int readInt(int p_nColumnIndex) throws SciDbException
	{
		try
		{
			return resultSet.getInt(p_nColumnIndex);
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error reading int", sqlExcep);
			throw new SciDbException("Error reading int", sqlExcep);
		}
	}
	
	public Date readDate(DbColumn p_oColumn) throws SciDbException
	{
		try
		{
			return resultSet.getDate(p_oColumn.name());
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error reading date ", sqlExcep);
			throw new SciDbException("Error reading date", sqlExcep);
		}
	}
	
	public Date readDate(int p_nColumnIndex) throws SciDbException
	{
		try
		{
			return resultSet.getDate(p_nColumnIndex);
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error reading date ", sqlExcep);
			throw new SciDbException("Error reading date ", sqlExcep);
		}
	}
	
	public int readInt(DbColumn p_oColumn) throws SciDbException
	{
		try
		{
			return resultSet.getInt(p_oColumn.name());
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error reading int ", sqlExcep);
			throw new SciDbException("Error reading int ", sqlExcep);
		}
	}
	
	public void close() throws SciDbException
	{
		try
		{
			if (resultSet != null && resultSet.getStatement() != null && !resultSet.getStatement().isClosed())
			{
				resultSet.getStatement().close();
			}
		}
		catch (SQLException sqlExcep)
		{
			log.error("Error closing resultset's statement object", sqlExcep);
			throw new SciDbException("Error closing resultset's statement object", sqlExcep);
		}
		finally
		{
			try
			{
				
				if (resultSet != null && !resultSet.isClosed())
				{
					resultSet.close();
				}
			}
			catch (SQLException sqlExcep)
			{
				log.error("Error closing resultset object", sqlExcep);
				throw new SciDbException("Error closing resultset's statement object", sqlExcep);
			}
		}
	}
	
}
