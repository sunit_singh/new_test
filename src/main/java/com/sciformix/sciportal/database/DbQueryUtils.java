package com.sciformix.sciportal.database;

import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.SciDbException;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbFunction;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbColumn;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbConjunction;
import com.sciformix.sciportal.database.DbSchemaDefinition.DbTable;
import com.sciformix.sciportal.utils.SystemUtils;


public class DbQueryUtils
{
	
	private static final String _QUALIFIED_OBJECT_NAME_PATTERN = "%s.%s";
	
	private static final String _SCHEMA_OWNER = SystemUtils.getDBSchemaOwnerName();
	
	public static String getQualifiedObjectName(String p_sTableName)
	{
		return String.format(_QUALIFIED_OBJECT_NAME_PATTERN, _SCHEMA_OWNER, p_sTableName);
	}
	
	public static DbFunctionExecute createFunctionForExecution(DbFunction p_oFunction) throws SciDbException {
		return DbFunctionExecute.prepareFunctionForExecution(p_oFunction);
	}
	
	public static DbQuerySelect createSelectQuery(DbTable p_oTable, String p_sColumnNames, String p_sWhereClause)
	{
		return DbQuerySelect.create(p_oTable.name(), p_sColumnNames, p_sWhereClause, null);
	}
	
	public static DbQuerySelect createSelectQuery(DbTable p_oTable, String p_sColumnNames, String p_sWhereClause, String p_sOrderbyClause)
	{
		return DbQuerySelect.create(p_oTable.name(), p_sColumnNames, p_sWhereClause, p_sOrderbyClause);
	}
	
	public static DbQuerySelect createSelectAllColumnsQuery(DbTable p_oTable, String p_sWhereClause, String p_sOrderbyClause)
	{
		return DbQuerySelect.create(p_oTable.name(), p_oTable.allColumnNames(), p_sWhereClause, p_sOrderbyClause);
	}
	
	public static DbQuerySelect createSelectAllColumnsQuery(DbTable p_oTable, String p_sWhereClause)
	{
		return DbQuerySelect.create(p_oTable.name(), p_oTable.allColumnNames(), p_sWhereClause, null);
	}
	
	public static DbQuerySelect createSelectQuery(String p_sRawSql)
	{
		return DbQuerySelect.create(p_sRawSql);
	}
	
	public static DbQueryInsert createInsertQuery(DbTable p_oTable)
	{
		return DbQueryInsert.create(p_oTable.name(), p_oTable.allColumnNames());
	}
	
	public static DbQueryUpdate createUpdateQuery(DbTable p_oTable, String p_sColumnNames, String p_sWhereClause)
	{
		return DbQueryUpdate.create(p_oTable.name(), p_sColumnNames, p_sWhereClause);
	}
	
	public static DbQueryUpdate createUpdateQuery(String p_sRawSql)
	{
		return DbQueryUpdate.create(p_sRawSql);
	}
	
	public static DbQueryDelete createDeleteQuery(DbTable p_oTable, String p_sWhereClause)
	{
		return DbQueryDelete.create(p_oTable, p_sWhereClause);
	}
	
	public static String bindUpdateClause(DbColumn... p_oarrColumns)
	{
		String sClause = null;
		
		for (DbColumn oColumn : p_oarrColumns)
		{
			if (sClause == null)
			{
				sClause = oColumn.bindClause();
			}
			else
			{
				sClause += SciConstants.StringConstants.COMMA + oColumn.bindClause();
			}
		}
		
		return sClause;
	}
	
	public static String bindWhereClauseWithAnd(DbColumn... p_oarrColumns)
	{
		return bindWhereClause(DbConjunction.AND, p_oarrColumns);
	}
	
	public static String bindWhereClauseWithOr(DbColumn... p_oarrColumns)
	{
		return bindWhereClause(DbConjunction.OR, p_oarrColumns);
	}
	
	public static String bindWhereClauseWithAnd(String... p_sarrColumnClauses)
	{
		
		return bindWhereClause(DbConjunction.AND, p_sarrColumnClauses);
	}
	
	public static String bindWhereClauseWithOr(String... p_sarrColumnClauses)
	{
		return bindWhereClause(DbConjunction.OR, p_sarrColumnClauses);
	}
	
	private static String bindWhereClause(DbConjunction p_eConjunction, DbColumn... p_oarrColumns)
	{
		String sClause = null;
		
		for (DbColumn oColumn : p_oarrColumns)
		{
			if (sClause == null)
			{
				sClause = oColumn.bindClause();
			}
			else
			{
				sClause += p_eConjunction.clause() + oColumn.bindClause();
			}
		}
		
		return sClause;
	}
	
	private static String bindWhereClause(DbConjunction p_eConjunction, String... p_sarrColumnClauses)
	{
		String sClause = null;
		
		for (String sColumnClause : p_sarrColumnClauses)
		{
			if (sClause == null)
			{
				sClause = sColumnClause;
			}
			else
			{
				sClause += p_eConjunction.clause() + sColumnClause;
			}
		}
		
		return sClause;
	}
	
	public static String createOrderbyClause(DbColumn... p_oarrColumns)
	{
		String sClause = null;
		
		for (DbColumn oColumn : p_oarrColumns)
		{
			if (sClause == null)
			{
				sClause = oColumn.name();
			}
			else
			{
				sClause += StringConstants.COMMA + oColumn.name();
			}
		}
		
		return sClause;
	}
}
