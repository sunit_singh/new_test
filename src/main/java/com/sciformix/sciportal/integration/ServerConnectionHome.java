package com.sciformix.sciportal.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.sciportal.config.ConfigHome;

/**
 * 
select * from scfx_so.sys_config order by cfgcategory, cfgkey;

insert into scfx_so.sys_config values ('LDAP.SERVERCONNECTIONS','sciportal.ldap._LIST','PRIMARY',sysdate, 1);

insert into scfx_so.sys_config values ('SMTP.1.CONFIG','sciportal.smtp.1.host','smtp.sciformix.com',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.1.CONFIG','sciportal.smtp.1.port','587',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.1.CONFIG','sciportal.smtp.1.connectuser.email','system-admin@sciformix.com',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.1.CONFIG','sciportal.smtp.1.connectuser.password','abcd1234',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.1.CONFIG','sciportal.smtp.1.fromuser.email','system-user@sciformix.com',sysdate, 1);

insert into scfx_so.sys_config values ('SMTP.2.CONFIG','sciportal.smtp.2.host','smtp.www.com',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.2.CONFIG','sciportal.smtp.2.port','123',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.2.CONFIG','sciportal.smtp.2.connectuser.email','system-admin@www.com',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.2.CONFIG','sciportal.smtp.2.connectuser.password','www1234',sysdate, 1);
insert into scfx_so.sys_config values ('SMTP.2.CONFIG','sciportal.smtp.2.fromuser.email','system-user@www.com',sysdate, 1);

insert into scfx_so.sys_config values ('SMTP.SERVERCONNECTIONS','sciportal.smtp._LIST','1,2',sysdate, 1);

 *
 */
public class ServerConnectionHome
{
	public static final Map<ServerConnection.TYPE, List<ServerConnectionTypeMetadata>> MAP_SERVERCONNECTION_TYPE_METADATA = 
										new HashMap<ServerConnection.TYPE, List<ServerConnectionTypeMetadata>>();

	static
	{
		List<ServerConnectionTypeMetadata> listMetadata = null;
		
		listMetadata = new ArrayList<ServerConnectionTypeMetadata>();
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("url", "LDAP URL", "LDAP URL"));
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("binding.username", "Principcal Account Name", "Principcal Account Name"));
		listMetadata.add(ServerConnectionTypeMetadata.createPasswordField("binding.password", "Principcal Account Password", "Principcal Account Password"));
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("domain", "LDAP Domain", "LDAP Domain"));
		MAP_SERVERCONNECTION_TYPE_METADATA.put(ServerConnection.TYPE.LDAP, listMetadata);
		
		listMetadata = new ArrayList<ServerConnectionTypeMetadata>();
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("host", "SMTP Host", "SMTP Host"));
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("port", "SMTP Port Number", "SMTP Port Number"));
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("connectuser.email", "Connect User Email address", "Connect User Email address"));
		listMetadata.add(ServerConnectionTypeMetadata.createPasswordField("connectuser.password", "Connect User Password", "Connect User Password"));
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("fromuser.email", "From User Email address", "From User Email address"));
		MAP_SERVERCONNECTION_TYPE_METADATA.put(ServerConnection.TYPE.SMTP, listMetadata);
		
		/*listMetadata = new ArrayList<ServerConnectionTypeMetadata>();
		listMetadata.add(ServerConnectionTypeMetadata.createStringField("host", "SharePoint Host", "SharePoint Host"));
		MAP_SERVERCONNECTION_TYPE_METADATA.put(ServerConnection.TYPE.SHAREPOINT, listMetadata);*/
	}
	
	
	static List<ServerConnection> m_TEMP_LIST_OF_CONNECTIONS = new ArrayList<ServerConnection>();
	
	public static List<ServerConnection> getAllConnections()
	{
		List<ServerConnection> listConnections = null;
		ServerConnection oConnection = null;
		String sConnectionTypePrefix = null;
		String sSerializedConnectionAliases = null;
		String[] sarrConnectionAliases = null;
		List<ServerConnectionTypeMetadata> listMetadata = null;
		String sConnectionFieldValue = null;
		
		listConnections = new ArrayList<ServerConnection>();
		
		for (ServerConnection.TYPE eConnectionType : ServerConnection.TYPE.values())
		{
			sConnectionTypePrefix = eConnectionType.name().toLowerCase();
			sSerializedConnectionAliases = ConfigHome.getSysConfigSetting("sciportal." + sConnectionTypePrefix + "._LIST");
			sarrConnectionAliases = StringUtils.split(sSerializedConnectionAliases, StringConstants.COMMA);
			
			listMetadata = MAP_SERVERCONNECTION_TYPE_METADATA.get(eConnectionType);
					
			for (String sConnectionAlias : sarrConnectionAliases)
			{
				oConnection = new ServerConnection(eConnectionType, sConnectionAlias);
				for (ServerConnectionTypeMetadata oConnectionMetadata : listMetadata)
				{
					sConnectionFieldValue = ConfigHome.getSysConfigSetting("sciportal." + sConnectionTypePrefix + "." + sConnectionAlias.toLowerCase()+ "." + oConnectionMetadata.getFieldName());
					oConnection.setFieldValue(oConnectionMetadata.getFieldName(), sConnectionFieldValue);
				}
				listConnections.add(oConnection);
			}
		}
		
		return listConnections;
				
		
		//TODO:
		//return m_TEMP_LIST_OF_CONNECTIONS;
	}
	
	public static ServerConnection createServerConnection(ServerConnection.STATE p_eState, ServerConnection.TYPE p_eType, String p_sAlias, String... p_sarrParameters)//Attributes
	{
		ServerConnection oServerConnection = new ServerConnection(p_sarrParameters);
		oServerConnection.setState(p_eState);
		oServerConnection.setAlias(p_sAlias);
		oServerConnection.setType(p_eType);
		
		m_TEMP_LIST_OF_CONNECTIONS.add(oServerConnection);
		
		return oServerConnection;
	}
	
	public static ServerConnection updateServerConnection(String conn_Id, ServerConnection.STATE p_eState, String p_sAlias, String... p_sarrParameters)//Attributes
	{
		//ServerConnection o_serverConnection = null;
		for (ServerConnection con: m_TEMP_LIST_OF_CONNECTIONS)
		{
			if((con.getType()+con.getAlias()).equalsIgnoreCase(conn_Id))
			{
				con.setState(p_eState);
				con.setAlias(p_sAlias);
				
			}
		}
		
		return null;
	}
	
}
