package com.sciformix.sciportal.integration;

public class ServerConnectionTypeMetadata
{
	public static enum TYPE
	{
		STRING, PASSWORD;
	}
	
	private String m_sFieldName = null;
	private TYPE m_eFieldType = null;
	private String m_sFieldAlias = null;
	private String m_sFieldHint = null;
	
	public static ServerConnectionTypeMetadata createStringField(String p_sFieldName, String p_sFieldAlias, String p_sFieldHint)
	{
		return new ServerConnectionTypeMetadata(p_sFieldName, TYPE.STRING, p_sFieldAlias, p_sFieldHint);
	}
	
	public static ServerConnectionTypeMetadata createPasswordField(String p_sFieldName, String p_sFieldAlias, String p_sFieldHint)
	{
		return new ServerConnectionTypeMetadata(p_sFieldName, TYPE.PASSWORD, p_sFieldAlias, p_sFieldHint);
	}
	
	private ServerConnectionTypeMetadata(String p_sFieldName, TYPE p_eFieldType, String p_sFieldAlias, String p_sFieldHint)
	{
		m_sFieldName = p_sFieldName;
		m_eFieldType = p_eFieldType;
		m_sFieldAlias = p_sFieldAlias;
		m_sFieldHint = p_sFieldHint;
	}

	public String getFieldName() {
		return m_sFieldName;
	}
	
	public TYPE getFieldType() {
		return m_eFieldType;
	}

	public String getFieldAlias() {
		return m_sFieldAlias;
	}

	public String getFieldHint() {
		return m_sFieldHint;
	}

}
