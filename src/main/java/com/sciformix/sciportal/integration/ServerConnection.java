package com.sciformix.sciportal.integration;

public class ServerConnection
{

	public static enum TYPE
	{
		LDAP, SMTP/*, SHAREPOINT*/;
	}
	
	public static enum STATE
	{
		ACTIVE, DEACTIVE;
	}
	
	
	public ServerConnection(ServerConnection.TYPE p_eType, String p_sAlias)
	{
		m_eType = p_eType;
		m_sAlias = p_sAlias;
	}
	
	public void setFieldValue(String p_sFieldName, String p_sFieldValue)
	{
		//TODO:Store into Hashmap
	}
	
	public ServerConnection(String[] parameters)
	{
		
	}
	/*public static ServerConnection serverConnection = new ServerConnection();
	*/
	
	/*public static ServerConnection getServerConnection()
	{
		return serverConnection;
	}*/
	private ServerConnection.TYPE m_eType;
	private String m_sAlias;
	private ServerConnection.STATE state;
	//TODO:Declare Hashmap
	

	public String getAlias() {
		return m_sAlias;
	}
	public void setAlias(String alias) {
		this.m_sAlias = alias;
	}
	public ServerConnection.TYPE getType() {
		return m_eType;
	}
	public void setType(ServerConnection.TYPE type) {
		this.m_eType = type;
	}
	public ServerConnection.STATE getState() {
		return state;
	}
	public void setState(ServerConnection.STATE state) {
		this.state = state;
	}
	
	//Attributes: Type, State, Alias, Parameters...
	//Methods: Ctor, getters, setters
	//Enum: State [ACTIVE, DEACTIVE]
	//Enum: Type [LDAP, SMTP, SHAREPOINT]
	
}
