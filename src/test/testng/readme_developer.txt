
To use TestNG:
(1) Install TestNG Eclipse plug-in from the Eclipse marketplace
(2) Within the SciPortal project, create a new 'Run Configuration' of the type 'TestNG' and specify the 'Run' option as 'Suite' and select the file 'src/test/testng/TestNgMainSuite.xml' from within the project source

