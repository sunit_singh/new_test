package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class ArticleTitleTest {

	ArticleTitle obj = null;

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.literature.articletitle";
	
	final static String sourceDocumentKey = "Reporter_LiteratureReferences";
	
	@Before
	public void initializeClass() {
		obj = new ArticleTitle();
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){
		StringParsedDataValue e2b_literatureReferences = new StringParsedDataValue("TEST", "TEST", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeCaseNotes = new StringParsedDataValue("TEST TEST TEST", "TEST TEST TEST", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_LiteratureReferences", "Reporter_LiteratureReferences", e2b_literatureReferences);
		sourceDocumentData.addColumn("Narrative_CaseNotesEnglishLanguage", "Narrative_CaseNotesEnglishLanguage", e2b_narrativeCaseNotes);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("TEST"));	
	}
	
	@Test
	public void testRun2(){
		StringParsedDataValue e2b_literatureReferences = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeCaseNotes = new StringParsedDataValue("TEST TEST TEST", "TEST TEST TEST", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_LiteratureReferences", "Reporter_LiteratureReferences", e2b_literatureReferences);
		sourceDocumentData.addColumn("Narrative_CaseNotesEnglishLanguage", "Narrative_CaseNotesEnglishLanguage", e2b_narrativeCaseNotes);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("TEST TEST TEST"));	
	}
}
