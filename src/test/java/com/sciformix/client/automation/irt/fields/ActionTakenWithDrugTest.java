package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.automation.irt.IRTWebApplicationWriter;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class ActionTakenWithDrugTest {
	
	static ParsedDataTable
	sourceDocumentData
;	
	static Map<String, String>
	readDataTable;

static Map<String,String>
	dataEntryMap
;

final static String
	writerKey	=	IRTConstants.DataEntry.SUSP_DRUG_ACTION_WITH_DRUG,
	readerKey	=	CpatToolCConstants.ReportSummaryTabKeys.SUSP_DRUG_ACTION_WITH_DRUG;

@BeforeClass
public static void initializeClass() {
	dataEntryMap		=	new HashMap<>();
	readDataTable		=	new HashMap<>();
	
	IRTWebApplicationWriter.receiptDataMap = new HashMap<>();
	IRTWebApplicationWriter.receiptDataMap.put(readerKey, "ABC");
	
}

/**
 * This method checks the value returned is
 * "Unspecified" or not. As per the field
 * mapping if the E2B field (B.4.k.8) and
 * "Route of Administration" is blank then
 * the value returned should be "Unspecified".
 */
@Test
public void testIfValuePresentInE2B() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null;
	externalSourceNO	=	new StringParsedDataValue(
			"ABCD"
		, 	"ABCD"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SUSP_DRUG_ACTION_WITH_DRUG
	, 	"ActionTakenWithDrug"
	, 	externalSourceNO
	);
	
	ActionTakenWithDrug actiontakenWithDrug = new ActionTakenWithDrug();
	
	dataEntryMap.remove(writerKey);
	actiontakenWithDrug.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals("ABCD"));
}

@Test
public void testIfValueNotPresentButDrugEndDate() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
						drugEndDate = null;
	
	
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	drugEndDate =	new StringParsedDataValue(
			"aaaaa"
		, 	"aaa"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SUSP_DRUG_ACTION_WITH_DRUG
	, 	"ActionTakenWithDrug"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.DrugTabKeys.DRUG_END_DATE
			, 	"ActionTakenWithDrug"
			, 	drugEndDate
			);
	
	ActionTakenWithDrug actiontakenWithDrug = new ActionTakenWithDrug();
	
	dataEntryMap.remove(writerKey);
	actiontakenWithDrug.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.DRUG_DISCONTINUED));
}

@Test
public void testIfActionValueDrugEndDateDateAndProductEndDateNotPresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
							drugEndDate = null,
							productEndDate = null;
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	drugEndDate =	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	productEndDate  =	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SUSP_DRUG_ACTION_WITH_DRUG
	, 	"ActionTakenWithDrug"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.DrugTabKeys.DRUG_END_DATE
			, 	"DrugEndDate"
			, 	drugEndDate
			);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.PRODUCT_END_DATE
			, 	"ProductEndDate"
			, 	productEndDate
			);
	
	ActionTakenWithDrug actiontakenWithDrug = new ActionTakenWithDrug();
	
	dataEntryMap.remove(writerKey);
	actiontakenWithDrug.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.UNKNOWN));
}


@Test
public void testIfActionValueAndDrugEndDateDateNotPresentButProductEndDate() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
							drugEndDate = null,
							productEndDate = null;
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	drugEndDate =	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	productEndDate  =	new StringParsedDataValue(
			"123"
		, 	"123"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SUSP_DRUG_ACTION_WITH_DRUG
	, 	"ActionTakenWithDrug"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.DrugTabKeys.DRUG_END_DATE
			, 	"DrugEndDate"
			, 	drugEndDate
			);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.PRODUCT_END_DATE
			, 	"ProductEndDate"
			, 	productEndDate
			);
	
	ActionTakenWithDrug actiontakenWithDrug = new ActionTakenWithDrug();
	
	dataEntryMap.remove(writerKey);
	actiontakenWithDrug.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.DRUG_DISCONTINUED));
}

}
