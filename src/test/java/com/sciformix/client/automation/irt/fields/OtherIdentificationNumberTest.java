package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.automation.irt.IRTWebApplicationWriter;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class OtherIdentificationNumberTest {

	OtherIdentificationNumber obj;

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.additionalreferences.otheridentificationnumber";
	
	final static String sourceDocumentKey = "";

	@Before
	public void initializeClass() {
		obj = new OtherIdentificationNumber();
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
		IRTWebApplicationWriter.receiptDataMap = new HashMap<String, String>();
	}
	
	@Test
	public void whenAllValuesPresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber_1", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~234567~135790~098765~123890"));
	}
	
	@Test
	public void whenSafetyReportIdNotPresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber_1", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("345678~234567~135790~098765~123890"));
	}
	
	@Test
	public void whenDuplicateNumberNotPresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber_1", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~234567~135790~098765~123890"));
	}
	
	@Test
	public void whenGccNumberNotPresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber_1", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~135790~098765~123890"));
	}
	
	@Test
	public void whenLinkedNumberNotPresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber_1", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~234567~098765~123890"));
	}
	
	@Test
	public void whenOtherCaseIdentifiersNotPresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber_1", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~234567~135790~123890"));
	}
	
	@Test
	public void whenMultipleExternalSourcePresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber_1", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~JMX_01"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Operating Company"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~234567~135790~098765"));
	}
	
	@Test
	public void whenSingleExternalSourcePresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource = new StringParsedDataValue("PQMS", "PQMS", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource", "Narrative_ExternalSource", externalSource);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber", "Narrative_ExternalSourceNumber", externalSourceNumber);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~234567~135790~098765~123890"));
	}
	
	@Test
	public void whenNoExternalSourcePresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~JMX_01"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Operating Company"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~234567~135790~098765"));
	}
	
	@Test
	public void testFollowUpLogicWithAllValuesPresent(){
		
		IRTWebApplicationWriter.receiptDataMap.put("dataentry.editreceipt.sender", "JMX_01");
		
		StringParsedDataValue localCaseId = new StringParsedDataValue("12345678", "12345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue duplicateNumber = new StringParsedDataValue("345678", "345678", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue gccPlatforNumber = new StringParsedDataValue("234567", "234567", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue linkedReportNo = new StringParsedDataValue("135790", "135790", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue otherCaseIdentifiers = new StringParsedDataValue("098765", "098765", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceCount = new StringParsedDataValue("2", "2", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource1 = new StringParsedDataValue("Sceptre", "Sceptre", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber1 = new StringParsedDataValue("147073", "147073", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSource2 = new StringParsedDataValue("Pqms", "Pqms", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue externalSourceNumber2 = new StringParsedDataValue("123890", "123890", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SafetyReportID", "SafetyReportID", localCaseId);
		sourceDocumentData.addColumn("DuplicateNumber", "DuplicateNumber", duplicateNumber);
		sourceDocumentData.addColumn("Narrative_GCCPlatformCaseNumber", "Narrative_GCCPlatformCaseNumber", gccPlatforNumber);
		sourceDocumentData.addColumn("LinkedReportNo", "LinkedReportNo", linkedReportNo);
		sourceDocumentData.addColumn("OtherCaseIdentifiersInPreviousTransmission", "OtherCaseIdentifiersInPreviousTransmission", otherCaseIdentifiers);
		sourceDocumentData.addColumn("Narrative_ExternalSource_Count", "Narrative_ExternalSource_Count", externalSourceCount);
		sourceDocumentData.addColumn("Narrative_ExternalSource_1", "Narrative_ExternalSource_1", externalSource1);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_1", "Narrative_ExternalSourceNumber", externalSourceNumber1);
		sourceDocumentData.addColumn("Narrative_ExternalSource_2", "Narrative_ExternalSource_2", externalSource2);
		sourceDocumentData.addColumn("Narrative_ExternalSourceNumber_2", "Narrative_ExternalSourceNumber_2", externalSourceNumber2);
		
		obj.applyBusinessLogicFollowUp(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_COMP_UNIT).equals("JMX_01~JMX_01~JMX_01~JMX_01~JMX_01~PQMS"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_REF).equals("Operating Company~Operating Company~Operating Company~Operating Company~Operating Company~Product Complaint Number"));
		assertTrue(dataEntryMap.get(IRTConstants.DataEntry.ADD_REF_OTHER_INDT_NO).equals("12345678~345678~234567~135790~098765~123890"));
	}
}
