package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class InitialReceivedDateTest {

	InitialReceivedDate obj;

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.generalinformation.initialreceivedate";
	
	final static String sourceDocumentKey = "Patient_DateOfBirth";

	@Before
	public void initializeClass() {
		obj = new InitialReceivedDate();
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}

	@Test
	public void testEvhuman() throws SciException, SciServiceException {

		try {
			
			// TEST RUN 1
			StringParsedDataValue senderIdentifier = new StringParsedDataValue("EVHUMAN", "EVHUMAN", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
			StringParsedDataValue messageNumber = new StringParsedDataValue("ICHICSR-EVHUMAN-20170313", "ICHICSR-EVHUMAN-20170313", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
			StringParsedDataValue firstReceivedDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
			StringParsedDataValue receiveDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
			StringParsedDataValue receiptDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
			StringParsedDataValue dateCount = new StringParsedDataValue("1", "1", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
			StringParsedDataValue awarenessDate = new StringParsedDataValue("15-Feb-2018", "15-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
			
			sourceDocumentData.addColumn("SenderIdentifier", "SenderIdentifier", senderIdentifier);
			sourceDocumentData.addColumn("MessageNumber", "MessageNumber", messageNumber);
			sourceDocumentData.addColumn("FirstReceivedDate", "FirstReceivedDate", firstReceivedDate);
			sourceDocumentData.addColumn("Narrative_ReceiveDate", "Narrative_ReceiveDate", receiveDate);
			sourceDocumentData.addColumn("Narrative_ReceiptDate", "Narrative_ReceiptDate", receiptDate);
			sourceDocumentData.addColumn("Narrative_AwarenessDate_Count", "Narrative_AwarenessDate_Count", dateCount);
			sourceDocumentData.addColumn("Narrative_AwarenessDate", "Narrative_AwarenessDate", awarenessDate);
			
			
			obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, "");
			assertTrue(dataEntryMap.get(writerKey).equals("13-Mar-2017"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testNonEvhuman() throws SciException, SciServiceException {
		
		// TEST RUN 2
		StringParsedDataValue senderIdentifier = new StringParsedDataValue("EVHUMsAN", "EVHUsMAN", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue messageNumber = new StringParsedDataValue("ICHICSR-EVHUMAN-20170313", "ICHICSR-EVHUMAN-20170313", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue firstReceivedDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiveDate = new StringParsedDataValue("09-JAN-2018", "09-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiptDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue dateCount = new StringParsedDataValue("5", "5", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		StringParsedDataValue awarenessDate1 = new StringParsedDataValue("14-Feb-2018", "14-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate2= new StringParsedDataValue("13-Feb-2018", "13-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate3 = new StringParsedDataValue("12-Feb-2018", "12-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate4 = new StringParsedDataValue("11-Feb-2018", "11-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate5 = new StringParsedDataValue("10-Feb-2018", "10-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SenderIdentifier", "SenderIdentifier", senderIdentifier);
		sourceDocumentData.addColumn("MessageNumber", "MessageNumber", messageNumber);
		sourceDocumentData.addColumn("FirstReceivedDate", "FirstReceivedDate", firstReceivedDate);
		sourceDocumentData.addColumn("Narrative_ReceiveDate", "Narrative_ReceiveDate", receiveDate);
		sourceDocumentData.addColumn("Narrative_ReceiptDate", "Narrative_ReceiptDate", receiptDate);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_Count", "Narrative_AwarenessDate_Count", dateCount);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_1", "Narrative_AwarenessDate_1", awarenessDate1);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_2", "Narrative_AwarenessDate_2", awarenessDate2);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_3", "Narrative_AwarenessDate_3", awarenessDate3);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_4", "Narrative_AwarenessDate_4", awarenessDate4);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_5", "Narrative_AwarenessDate_5", awarenessDate5);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, "");
		assertTrue(dataEntryMap.get(writerKey).equals("09-Jan-2018"));
	}
	
	@Test
	public void testNonEvhumanandAwarenessCount1() throws SciException, SciServiceException {
		// TEST RUN 3
		StringParsedDataValue senderIdentifier = new StringParsedDataValue("EVHUSMAN", "EVHSUMAN", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue messageNumber = new StringParsedDataValue("ICHICSR-EVHUMAN-20170313", "ICHICSR-EVHUMAN-20170313", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue firstReceivedDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiveDate = new StringParsedDataValue("23-JAN-2018", "23-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiptDate = new StringParsedDataValue("22-JAN-2018", "22-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue dateCount = new StringParsedDataValue("1", "1", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate = new StringParsedDataValue("15-Feb-2018", "15-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SenderIdentifier", "SenderIdentifier", senderIdentifier);
		sourceDocumentData.addColumn("MessageNumber", "MessageNumber", messageNumber);
		sourceDocumentData.addColumn("FirstReceivedDate", "FirstReceivedDate", firstReceivedDate);
		sourceDocumentData.addColumn("Narrative_ReceiveDate", "Narrative_ReceiveDate", receiveDate);
		sourceDocumentData.addColumn("Narrative_ReceiptDate", "Narrative_ReceiptDate", receiptDate);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_Count", "Narrative_AwarenessDate_Count", dateCount);
		sourceDocumentData.addColumn("Narrative_AwarenessDate", "Narrative_AwarenessDate", awarenessDate);
		
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, "");
		assertTrue(dataEntryMap.get(writerKey).equals("22-Jan-2018"));
	}
	
	@Test
	public void testNonEvhumanandAwarenessCountNotPresent() throws SciException, SciServiceException {
		// TEST RUN 4
		StringParsedDataValue senderIdentifier = new StringParsedDataValue("EVHUSMAN", "EVHSUMAN", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue messageNumber = new StringParsedDataValue("ICHICSR-EVHUMAN-20170313", "ICHICSR-EVHUMAN-20170313", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue firstReceivedDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiveDate = new StringParsedDataValue("23-JAN-2018", "23-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiptDate = new StringParsedDataValue("22-JAN-2018", "22-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate = new StringParsedDataValue("15-Feb-2018", "15-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SenderIdentifier", "SenderIdentifier", senderIdentifier);
		sourceDocumentData.addColumn("MessageNumber", "MessageNumber", messageNumber);
		sourceDocumentData.addColumn("FirstReceivedDate", "FirstReceivedDate", firstReceivedDate);
		sourceDocumentData.addColumn("Narrative_ReceiveDate", "Narrative_ReceiveDate", receiveDate);
		sourceDocumentData.addColumn("Narrative_ReceiptDate", "Narrative_ReceiptDate", receiptDate);
		sourceDocumentData.addColumn("Narrative_AwarenessDate", "Narrative_AwarenessDate", awarenessDate);
		
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, "");
		assertTrue(dataEntryMap.get(writerKey).equals("22-Jan-2018"));
	}
	
	@Test
	public void testInitialReceivedDateFollowup() throws SciException, SciServiceException {
		// TEST RUN 5
		StringParsedDataValue senderIdentifier = new StringParsedDataValue("EVHUSMAN", "EVHSUMAN", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue messageNumber = new StringParsedDataValue("ICHICSR-EVHUMAN-20170313", "ICHICSR-EVHUMAN-20170313", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue recentInfoDate = new StringParsedDataValue("24-JAN-2018", "24-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiptDate = new StringParsedDataValue("22-JAN-2018", "22-JAN-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate = new StringParsedDataValue("15-Feb-2018", "15-Feb-2018", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SenderIdentifier", "SenderIdentifier", senderIdentifier);
		sourceDocumentData.addColumn("MessageNumber", "MessageNumber", messageNumber);
		sourceDocumentData.addColumn("DateOfRecentInfo", "DateOfRecentInfo", recentInfoDate);
		sourceDocumentData.addColumn("Narrative_ReceiptDate", "Narrative_ReceiptDate", receiptDate);
		sourceDocumentData.addColumn("Narrative_AwarenessDate", "Narrative_AwarenessDate", awarenessDate);
		
		
		obj.applyBusinessLogicFollowUp(dataEntryMap, sourceDocumentData, writerKey, "");
		assertTrue(dataEntryMap.get(writerKey).equals("22-Jan-2018"));
	}
	
	@Test
	public void testNonEvhumanAllBlank() throws SciException, SciServiceException {
		
		// TEST RUN 2
		StringParsedDataValue senderIdentifier = new StringParsedDataValue("EVHUMsAN", "EVHUsMAN", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue messageNumber = new StringParsedDataValue("ICHICSR-EVHUMAN-20170313", "ICHICSR-EVHUMAN-20170313", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue firstReceivedDate = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiveDate = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue receiptDate = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue dateCount = new StringParsedDataValue("5", "5", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		StringParsedDataValue awarenessDate1 = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate2= new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate3 = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate4 = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue awarenessDate5 = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("SenderIdentifier", "SenderIdentifier", senderIdentifier);
		sourceDocumentData.addColumn("MessageNumber", "MessageNumber", messageNumber);
		sourceDocumentData.addColumn("FirstReceivedDate", "FirstReceivedDate", firstReceivedDate);
		sourceDocumentData.addColumn("Narrative_ReceiveDate", "Narrative_ReceiveDate", receiveDate);
		sourceDocumentData.addColumn("Narrative_ReceiptDate", "Narrative_ReceiptDate", receiptDate);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_Count", "Narrative_AwarenessDate_Count", dateCount);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_1", "Narrative_AwarenessDate_1", awarenessDate1);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_2", "Narrative_AwarenessDate_2", awarenessDate2);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_3", "Narrative_AwarenessDate_3", awarenessDate3);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_4", "Narrative_AwarenessDate_4", awarenessDate4);
		sourceDocumentData.addColumn("Narrative_AwarenessDate_5", "Narrative_AwarenessDate_5", awarenessDate5);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, "");
		assertTrue(dataEntryMap.get(writerKey).equals(""));
	}

}
