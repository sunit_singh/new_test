package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class PatientSexTest {
	
	PatientSex obj = new PatientSex();

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.patient.sex";
	
	final static String sourceDocumentKey = "Patient_Sex";

	@Before
	public void initializeClass() {
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){
		
		StringParsedDataValue e2b_patientSex = new StringParsedDataValue("Male", "Male", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_Sex", "Patient_Sex", e2b_patientSex);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Male"));
		
	}
	
	@Test
	public void testRun2(){
		
		StringParsedDataValue e2b_patientSex = new StringParsedDataValue("Female", "Female", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_Sex", "Patient_Sex", e2b_patientSex);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Female"));
		
	}
	
	@Test
	public void testRun3(){
		
		StringParsedDataValue e2b_patientSex = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_Sex", "Patient_Sex", e2b_patientSex);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Unknown"));
		
	}

}
