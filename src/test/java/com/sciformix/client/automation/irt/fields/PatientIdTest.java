package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class PatientIdTest {
	
	PatientId obj = new PatientId();

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.patient.patientid";
	
	final static String sourceDocumentKey = "PatientInitial";

	@Before
	public void initializeClass() {
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){
		dataEntryMap.put("emea.countries", "Belgium,Bulgaria,Czech Republic,Denmark,Germany,Estonia,Ireland,Liechtenstein,Portugal,Romania,Slovenia,Slovakia,Finland,Sweden,United Kingdom,Greece,Spain,France,Croatia,Italy,Cyprus,Latvia,Norway,Lithuania,Luxembourg,Hungary,Malta,Netherlands,Austria,Poland,Iceland");
		StringParsedDataValue e2b_patientInitial = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BELGIUM", "BELGIUM", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("PatientInitial", "PatientInitial", e2b_patientInitial);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("private"));	
	}
	
	@Test
	public void testRun2(){
		dataEntryMap.put("emea.countries", "Belgium,Bulgaria,Czech Republic,Denmark,Germany,Estonia,Ireland,Liechtenstein,Portugal,Romania,Slovenia,Slovakia,Finland,Sweden,United Kingdom,Greece,Spain,France,Croatia,Italy,Cyprus,Latvia,Norway,Lithuania,Luxembourg,Hungary,Malta,Netherlands,Austria,Poland,Iceland");
		StringParsedDataValue e2b_patientInitial = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BRAZIL", "BRAZIL", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativePatientInitial = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("PatientInitial", "PatientInitial", e2b_patientInitial);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		sourceDocumentData.addColumn("Narrative_PatientInitials", "Narrative_PatientInitials", e2b_narrativePatientInitial);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("JMX"));	
	}
	
	@Test
	public void testRun3(){
		dataEntryMap.put("emea.countries", "Belgium,Bulgaria,Czech Republic,Denmark,Germany,Estonia,Ireland,Liechtenstein,Portugal,Romania,Slovenia,Slovakia,Finland,Sweden,United Kingdom,Greece,Spain,France,Croatia,Italy,Cyprus,Latvia,Norway,Lithuania,Luxembourg,Hungary,Malta,Netherlands,Austria,Poland,Iceland");
		StringParsedDataValue e2b_patientInitial = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BRAZIL", "BRAZIL", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativePatientInitial = new StringParsedDataValue("RDX", "RDX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("PatientInitial", "PatientInitial", e2b_patientInitial);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		sourceDocumentData.addColumn("Narrative_PatientInitials", "Narrative_PatientInitials", e2b_narrativePatientInitial);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("RDX"));	
	}
	
	@Test
	public void testRun4(){
		dataEntryMap.put("emea.countries", "Belgium,Bulgaria,Czech Republic,Denmark,Germany,Estonia,Ireland,Liechtenstein,Portugal,Romania,Slovenia,Slovakia,Finland,Sweden,United Kingdom,Greece,Spain,France,Croatia,Italy,Cyprus,Latvia,Norway,Lithuania,Luxembourg,Hungary,Malta,Netherlands,Austria,Poland,Iceland");
		StringParsedDataValue e2b_patientInitial = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BRAZIL", "BRAZIL", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativePatientInitial = new StringParsedDataValue("RDX", "RDX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("PatientInitial", "PatientInitial", e2b_patientInitial);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		sourceDocumentData.addColumn("Narrative_PatientInitials", "Narrative_PatientInitials", e2b_narrativePatientInitial);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("RDX"));	
	}

}
