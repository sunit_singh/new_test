package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class AgeGroupTest {
	
	AgeGroup obj = new AgeGroup();

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.patient.agegroup";
	
	final static String sourceDocumentKey = "Patient_AgeGroup";
	
	@Before
	public void initializeClass() {
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){
		
		StringParsedDataValue e2b_dateOfBirth = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeDOB = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_AgeGroup", "Patient_AgeGroup", e2b_dateOfBirth);
		sourceDocumentData.addColumn("Patient_OnsetAge", "Patient_OnsetAge", e2b_narrativeDOB);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals(""));
		
	}
	
	@Test
	public void testRun2(){
		
		StringParsedDataValue e2b_ageGroup = new StringParsedDataValue("Adult", "Adult", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_onsetAge = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_AgeGroup", "Patient_AgeGroup", e2b_ageGroup);
		sourceDocumentData.addColumn("Patient_OnsetAge", "Patient_OnsetAge", e2b_onsetAge);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Adult"));
		
	}
	
	@Test
	public void testRun3(){
		
		StringParsedDataValue e2b_ageGroup = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_onsetAge = new StringParsedDataValue("7", "7   ", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_AgeGroup", "Patient_AgeGroup", e2b_ageGroup);
		sourceDocumentData.addColumn("Patient_OnsetAge", "Patient_OnsetAge", e2b_onsetAge);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Child"));
		
	}
	
	@Test
	public void testRun4(){
		
		StringParsedDataValue e2b_ageGroup = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_onsetAge = new StringParsedDataValue("25", "25", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_AgeGroup", "Patient_AgeGroup", e2b_ageGroup);
		sourceDocumentData.addColumn("Patient_OnsetAge", "Patient_OnsetAge", e2b_onsetAge);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Adult"));
		
	}
	
	@Test
	public void testRun5(){
		
		StringParsedDataValue e2b_ageGroup = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_onsetAge = new StringParsedDataValue("66", "66", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_AgeGroup", "Patient_AgeGroup", e2b_ageGroup);
		sourceDocumentData.addColumn("Patient_OnsetAge", "Patient_OnsetAge", e2b_onsetAge);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Elderly"));
		
	}
	
	@Test
	public void testRun6(){
		
		StringParsedDataValue e2b_ageGroup = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_onsetAge = new StringParsedDataValue("15", "15", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_AgeGroup", "Patient_AgeGroup", e2b_ageGroup);
		sourceDocumentData.addColumn("Patient_OnsetAge", "Patient_OnsetAge", e2b_onsetAge);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Adolescent"));
		
	}
	
	@Test
	public void testRun7(){
		
		StringParsedDataValue e2b_ageGroup = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_onsetAge = new StringParsedDataValue("1", "1", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_AgeGroup", "Patient_AgeGroup", e2b_ageGroup);
		sourceDocumentData.addColumn("Patient_OnsetAge", "Patient_OnsetAge", e2b_onsetAge);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals(""));
		
	}

}
