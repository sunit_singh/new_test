package com.sciformix.client.automation.irt.fields;

import static com.sciformix.client.pat.toolc.CpatToolCHelper.propMap;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class ReporterFirstTest {
	
	ReporterFirst obj = new ReporterFirst();

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.reporter.first";
	
	final static String sourceDocumentKey = "Reporter_GivenName";

	@Before
	public void initializeClass() {
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
		
		propMap = new HashMap<String, String>();
		propMap.put("emea.countries", "Belgium,Bulgaria,Czech Republic,Denmark,Germany,Estonia,Ireland,Liechtenstein,Portugal,Romania,Slovenia,Slovakia,Finland,Sweden,United Kingdom,Greece,Spain,France,Croatia,Italy,Cyprus,Latvia,Norway,Lithuania,Luxembourg,Hungary,Malta,Netherlands,Austria,Poland,Iceland");
	}
	
	@Test
	public void isEmeaCountry(){
		StringParsedDataValue e2b_reporterGivenName = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BELGIUM", "BELGIUM", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_GivenName", "Reporter_GivenName", e2b_reporterGivenName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("private"));	
	}
	
	@Test
	public void isNotEmeaCountry(){
		StringParsedDataValue e2b_reporterGivenName = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("Brazil", "Brazil", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_GivenName", "Reporter_GivenName", e2b_reporterGivenName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("JMX"));	
	}
	
	@Test
	public void whenFirstNameIsBlank(){
		StringParsedDataValue e2b_reporterGivenName = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("Brazil", "Brazil", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_GivenName", "Reporter_GivenName", e2b_reporterGivenName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals(""));	
	}
	
	@Test
	public void whenEmeaListContainsPartOfCountryName(){
		StringParsedDataValue e2b_reporterGivenName = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("Bel", "Bel", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_GivenName", "Reporter_GivenName", e2b_reporterGivenName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("JMX"));	
	}
	
}
