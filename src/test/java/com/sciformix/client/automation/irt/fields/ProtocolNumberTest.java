package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

/**
 * 
 * @author gmishra
 *
 */
public class ProtocolNumberTest {
	static Map<String,String>
		dataEntryMap
	;
	
	final static String
		writerKey	=	"dataentry.protocol.protocolno"
	;
	/*@BeforeClass
	public static void initializeClass() {
		
	}*/
	
	@Before
	public void initBeforeTest() {
		dataEntryMap	=	new HashMap<>();
	}
	
	@Test
	public void getProtocolNumberFromE2B () {
		ParsedDataTable			l_srcdocdata	=	null;
		StringParsedDataValue	l_narrextsrcno	=	null,
								l_protnumber	=	null;
		l_narrextsrcno	=	new StringParsedDataValue(
								""
							, 	""
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_protnumber	=	new StringParsedDataValue(
								"123456789"
							, 	"123456789"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_srcdocdata	=	new ParsedDataTable();
		l_srcdocdata.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_NUMBER
		, 	"Narrative External Source Number"
		, 	l_narrextsrcno
		);
		l_srcdocdata.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE
		, 	"Narrative External Source"
		, 	l_narrextsrcno
		);
		l_srcdocdata.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_COUNT
		, 	"Narrative External Source Count"
		, 	l_narrextsrcno
		);
		l_srcdocdata.addColumn(
			"Reporter_SponsorStudyNo"
		, 	"Protocol Number"
		, 	l_protnumber
		);
		dataEntryMap.remove(writerKey);
		ProtocolNumber	l_prot	=	new ProtocolNumber();
		l_prot.applyBusinessLogicInitial(
			dataEntryMap
		, 	l_srcdocdata
		, 	writerKey
		, 	"Reporter_SponsorStudyNo"
		);
		assertTrue(dataEntryMap.get(writerKey).equals("123456789"));
	}
	
	@Test
	public void getProtocolNumberFromNarrativeWithSingleExternalNumber() {
		ParsedDataTable			l_srcdocdata	=	null;
		StringParsedDataValue	l_narrextsrcno	=	null,
								l_narrextsrc	=	null,
								l_protnumber	=	null;
		
		l_narrextsrcno	=	new StringParsedDataValue(
								"12345"
							, 	"12345"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_narrextsrc	=	new StringParsedDataValue(
								"HIVE"
							, 	"HIVE"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_protnumber	=	new StringParsedDataValue(
								""
							, 	""
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_srcdocdata	=	new ParsedDataTable();
		l_srcdocdata.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_NUMBER
		, 	"Narrative External Source Number"
		, 	l_narrextsrcno
		);
		l_srcdocdata.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE
		, 	"Narrative External Source"
		, 	l_narrextsrc
		);
		/*l_srcdocdata.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_EXTERNAL_SOURCE_COUNT
		, 	"Narrative External Source Count"
		, 	l_narrextsrcno
		);*/
		l_srcdocdata.addColumn(
			"Reporter_SponsorStudyNo"
		, 	"Protocol Number"
		, 	l_protnumber
		);
		dataEntryMap.remove(writerKey);
		ProtocolNumber	l_prot	=	new ProtocolNumber();
		l_prot.applyBusinessLogicInitial(
			dataEntryMap
		, 	l_srcdocdata
		, 	writerKey
		, 	"Reporter_SponsorStudyNo"
		);
		assertTrue(dataEntryMap.get(writerKey).equals("12345"));
	}
	@Test
	public void getProtocolNumberFromNarrativeWithMultipleExternalNumber() {
		ParsedDataTable			l_srcdocdata	=	null;
		StringParsedDataValue	l_narrextsrcno3	=	null,
								l_narrextsrc3	=	null,
								l_narrextsrcno1	=	null,
								l_narrextsrc1	=	null,
								l_narrextsrcno2	=	null,
								l_narrextsrc2	=	null,
								l_protnumber	=	null,
								l_extsrccount	=	null;
		l_narrextsrcno1	=	new StringParsedDataValue(
								"Sceptre-12345"
							, 	"Sceptre-12345"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_narrextsrc1	=	new StringParsedDataValue(
								"Sceptre"
							, 	"Sceptre"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);

		l_narrextsrcno2	=	new StringParsedDataValue(
								"PQMS-12345"
							, 	"PQMS-12345"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_narrextsrc2	=	new StringParsedDataValue(
								"PQMS"
							, 	"PQMS"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		
		l_narrextsrcno3	=	new StringParsedDataValue(
								"567890"
							, 	"567890"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_narrextsrc3	=	new StringParsedDataValue(
								"HIVE"
							, 	"HIVE"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_extsrccount	=	new StringParsedDataValue(
								"3"
							, 	"3"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_protnumber	=	new StringParsedDataValue(
								""
							, 	""
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_srcdocdata	=	new ParsedDataTable();
		l_srcdocdata.addColumn(
			"Narrative_ExternalSourceNumber_1"
		, 	"Narrative External Source 1"
		, 	l_narrextsrcno1
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_1"
		, 	"Narrative External Source 1"
		, 	l_narrextsrc1
		);
			
		l_srcdocdata.addColumn(
			"Narrative_ExternalSourceNumber_2"
		, 	"Narrative External Source Number"
		, 	l_narrextsrcno2
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_2"
		, 	"Narrative External Source 2"
		, 	l_narrextsrc2
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSourceNumber_3"
		, 	"Narrative External Source Number"
		, 	l_narrextsrcno3
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_3"
		, 	"Narrative External Source 3"
		, 	l_narrextsrc3
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_Count"
		, 	"Narrative External Source Count"
		, 	l_extsrccount
		);
		l_srcdocdata.addColumn(
			"Reporter_SponsorStudyNo"
		, 	"Protocol Number"
		, 	l_protnumber
		);
		dataEntryMap.remove(writerKey);
		ProtocolNumber	l_prot	=	new ProtocolNumber();
		l_prot.applyBusinessLogicInitial(
			dataEntryMap
		, 	l_srcdocdata
		, 	writerKey
		, 	"Reporter_SponsorStudyNo"
		);
		assertTrue(dataEntryMap.get(writerKey).equals("567890"));
	}
	
	public void getProtocolNumberFromNarrativeWithoutExternalNumber() {
		
	}
	
	public void getProtocolNumberFromNarrativeWithSingleExternalNumberWithoutHive() {
		
	}
	
	@Test
	public void getProtocolNumberFromNarrativeWithMultipleExternalNumberWithoutHive() {
		ParsedDataTable			l_srcdocdata	=	null;
		StringParsedDataValue	l_narrextsrcno3	=	null,
								l_narrextsrc3	=	null,
								l_narrextsrcno1	=	null,
								l_narrextsrc1	=	null,
								l_narrextsrcno2	=	null,
								l_narrextsrc2	=	null,
								l_protnumber	=	null,
								l_extsrccount	=	null;
		l_narrextsrcno1	=	new StringParsedDataValue(
								"Sceptre-12345"
							, 	"Sceptre-12345"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_narrextsrc1	=	new StringParsedDataValue(
								"Sceptre"
							, 	"Sceptre"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);

		l_narrextsrcno2	=	new StringParsedDataValue(
								"PQMS-12345"
							, 	"PQMS-12345"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_narrextsrc2	=	new StringParsedDataValue(
								"PQMS"
							, 	"PQMS"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		
		l_narrextsrcno3	=	new StringParsedDataValue(
								"567890"
							, 	"567890"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_narrextsrc3	=	new StringParsedDataValue(
								"HIVE"
							, 	"HIVE"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_extsrccount	=	new StringParsedDataValue(
								"2"
							, 	"2"
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_protnumber	=	new StringParsedDataValue(
								""
							, 	""
							, 	DataState.ASIS
							, 	ValidationState.VALIDATION_NOT_APPLICABLE
							);
		l_srcdocdata	=	new ParsedDataTable();
		l_srcdocdata.addColumn(
			"Narrative_ExternalSourceNumber_1"
		, 	"Narrative External Source 1"
		, 	l_narrextsrcno1
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_1"
		, 	"Narrative External Source 1"
		, 	l_narrextsrc1
		);
			
		l_srcdocdata.addColumn(
			"Narrative_ExternalSourceNumber_2"
		, 	"Narrative External Source Number"
		, 	l_narrextsrcno2
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_2"
		, 	"Narrative External Source 2"
		, 	l_narrextsrc2
		);
		/*l_srcdocdata.addColumn(
			"Narrative_ExternalSourceNumber_3"
		, 	"Narrative External Source Number"
		, 	l_narrextsrcno3
		);
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_3"
		, 	"Narrative External Source 3"
		, 	l_narrextsrc3
		);*/
		l_srcdocdata.addColumn(
			"Narrative_ExternalSource_Count"
		, 	"Narrative External Source Count"
		, 	l_extsrccount
		);
		l_srcdocdata.addColumn(
			"Reporter_SponsorStudyNo"
		, 	"Protocol Number"
		, 	l_protnumber
		);
		dataEntryMap.remove(writerKey);
		ProtocolNumber	l_prot	=	new ProtocolNumber();
		l_prot.applyBusinessLogicInitial(
			dataEntryMap
		, 	l_srcdocdata
		, 	writerKey
		, 	"Reporter_SponsorStudyNo"
		);
		assertFalse(dataEntryMap.get(writerKey).equals("567890"));
	}
}
