package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class BirthDefectTest {
	
	static ParsedDataTable
	sourceDocumentData
;	
	static Map<String, String>
	readDataTable;

static Map<String,String>
	dataEntryMap
;

final static String
	writerKey	=	IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,
	readerKey	=	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT;

@BeforeClass
public static void initializeClass() {
	dataEntryMap		=	new HashMap<>();
	readDataTable		=	new HashMap<>();
}

/**
 * This method checks the value returned is
 * "Unspecified" or not. As per the field
 * mapping if the E2B field (B.4.k.8) and
 * "Route of Administration" is blank then
 * the value returned should be "Unspecified".
 */
@Test
public void testForBirthDefectEmptyAndEventAssoBirthDefectEmptyAndReportedCauseEmpty() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.NO));
}

@Test
public void testForBirthDefectEmptyAndEventAssoBirthDefectEmptyAndReportedCausePresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"aaa"
		, 	"aaa"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES));
}

@Test
public void testForBirthDefectEmptyAndEventAssoBirthDefectYES() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES));
}

@Test
public void testForBirthDefectEmptyAndEventAssoBirthDefectNOAndReportedCausePresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES
			));
}

@Test
public void testForBirthDefectEmptyAndEventAssoBirthDefectNOAndReportedCauseNotPresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.NO
			));
}




@Test
public void testForBirthDefectNoAndEventAssoBirthDefectEmptyAndReportedCauseEmpty() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.NO));
}

@Test
public void testForBirthDefectNoAndEventAssoBirthDefectEmptyAndReportedCausePresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"aaa"
		, 	"aaa"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES));
}

@Test
public void testForBirthDefectNoAndEventAssoBirthDefectYES() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES));
}

@Test
public void testForBirthDefectNoAndEventAssoBirthDefectNOAndReportedCausePresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES
			));
}

@Test
public void testForBirthDefectNOAndEventAssoBirthDefectNOAndReportedCauseNotPresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.NO
			));
}



@Test
public void testForBirthDefectNoneAndEventAssoBirthDefectEmptyAndReportedCauseEmpty() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"None"
		, 	"None"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.NO));
}

@Test
public void testForBirthDefectNoneAndEventAssoBirthDefectEmptyAndReportedCausePresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"None"
		, 	"None"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"aaa"
		, 	"aaa"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES));
}

@Test
public void testForBirthDefectNoneAndEventAssoBirthDefectYES() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"None"
		, 	"None"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES));
}

@Test
public void testForBirthDefectNoneAndEventAssoBirthDefectNOAndReportedCausePresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"None"
		, 	"None"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES
			));
}

@Test
public void testForBirthDefectNoneAndEventAssoBirthDefectNOAndReportedCauseNotPresent() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"None"
		, 	"None"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			"No"
		, 	"No"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.NO
			));
}


@Test
public void testForBirthDefectYesAndEventAssoBirthDefectEmptyAndReportedCauseEmpty() {
	
	sourceDocumentData	=	new ParsedDataTable();
	StringParsedDataValue	externalSourceNO	=	null,
			eventAssociatedWithBirthDefect = null,
					reportedCause = null;
	externalSourceNO	=	new StringParsedDataValue(
			"Yes"
		, 	"Yes"
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	eventAssociatedWithBirthDefect = new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);
	
	reportedCause =  new StringParsedDataValue(
			""
		, 	""
		, 	DataState.ASIS
		, 	ValidationState.VALIDATION_NOT_APPLICABLE
		);

	sourceDocumentData.addColumn(
	CpatToolCConstants.ReportSummaryTabKeys.SERUOUSNESS_CRIT_BIRTH_DEFECT
	, 	"BirthDefect"
	, 	externalSourceNO
	);
	
	sourceDocumentData.addColumn(
		CpatToolCConstants.NarrativeKeys.NARRATIVE_EVENT_ASSOCIATED_WITH_BIRTH_DEFECT
		, 	"Event Ass. with BirthDefect"
		, 	eventAssociatedWithBirthDefect
	);
	
	sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_REPORTED_CAUSE_OF_BIRTH_DEFECT
			, 	"Event Ass. with BirthDefect"
			, 	reportedCause
		);
	
	BirthDefect birthDefect = new BirthDefect();
	
	dataEntryMap.remove(writerKey);
	birthDefect.applyBusinessLogicInitial(
		dataEntryMap
	, 	sourceDocumentData
	, 	writerKey
	, 	readerKey
	);
	
	assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.YES));
}
}