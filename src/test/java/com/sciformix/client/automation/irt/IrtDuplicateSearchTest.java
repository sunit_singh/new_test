package com.sciformix.client.automation.irt;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sciformix.client.automation.BaseWebApplicationWriter.BROWSER;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.client.pat.toolc.CpatToolCHelper;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class IrtDuplicateSearchTest {

	public static Map<String, String> propMap = new HashMap<>();
	private static IRTWebApplicationWriter oWebWriter = null;
	
	private static final String CONFIG_FOR_SANDBOX_PROPERTIES = "ConfigForIrt_Sandbox.properties";
	private static final String CONFIG_FOR_VALIDATION_PROPERTIES = "ConfigForIrt_Validation.properties";
	
	static ParsedDataTable	sourceDocumentData;

	@BeforeClass
	public static void prepareForSearch() {
		propMap = CpatToolCHelper.loadProperties(CONFIG_FOR_VALIDATION_PROPERTIES);
		String sUrl = null; 
		
		sUrl = "https://jnj74val.agondemand.bz/AGX/faces/pages/login/login.xhtml";
		
		try 
		{
			
			oWebWriter = new IRTWebApplicationWriter(BROWSER.CHROME, propMap);///(BROWSER.IE, sWebDriver, sWebDriverLocation, true);
			
			oWebWriter.launchBrowser(sUrl);
			//oWebWriter.login("CTESTUSER5", "testuser@15", propMap);
			oWebWriter.login("SSHETT31", "march@2018",propMap);
			
			//oWebWriter.searchCase("PCI2790932", propMap);
			oWebWriter.searchCase("SFL3173215",propMap);
			oWebWriter.editCasePdf("testuser@15", propMap);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	
	public void irtDuplicateSearchTest()
	{
		boolean testResult = false;
			StringParsedDataValue	localCaseId	=	null,
					reporterGivenName = null , 
					reporterFamilyName = null,
					patientDOB = null,
					patientInitials = null,
					country = null,
					patientGender = null;
					
			sourceDocumentData = new ParsedDataTable();
			localCaseId	=	new StringParsedDataValue(
						"049-0073-990003"
					, 	"049-0073-990003"
					, 	DataState.ASIS
					, 	ValidationState.VALIDATION_NOT_APPLICABLE
					);
			
			reporterGivenName	=	new StringParsedDataValue(
					"Gir"
				, 	"Gir"
				, 	DataState.ASIS
				, 	ValidationState.VALIDATION_NOT_APPLICABLE
				);
			
			reporterFamilyName	=	new StringParsedDataValue(
					"WW"
				, 	"WW"
				, 	DataState.ASIS
				, 	ValidationState.VALIDATION_NOT_APPLICABLE
				);
			
			patientDOB	=	new StringParsedDataValue(
					"25-Nov-1990"
				, 	"25-Nov-1990"
				, 	DataState.ASIS
				, 	ValidationState.VALIDATION_NOT_APPLICABLE
				);
			
			patientInitials	=	new StringParsedDataValue(
					"GW"
				, 	"GW"
				, 	DataState.ASIS
				, 	ValidationState.VALIDATION_NOT_APPLICABLE
				);
			
			country	=	new StringParsedDataValue(
					"INDIA"
				, 	"INDIA"
				, 	DataState.ASIS
				, 	ValidationState.VALIDATION_NOT_APPLICABLE
				);
			
			patientGender	=	new StringParsedDataValue(
					"Male"
				, 	"Male"
				, 	DataState.ASIS
				, 	ValidationState.VALIDATION_NOT_APPLICABLE
				);
			
			sourceDocumentData.addColumn(
				CpatToolCConstants.ReportSummaryTabKeys.GENINFO_LOC_CASE_ID
			, 	"Local Case Id"
			, 	localCaseId
			);
			
			sourceDocumentData.addColumn(
					CpatToolCConstants.ReportSummaryTabKeys.REPORTER_FIRST
				, 	"Reporter First"
				, 	reporterGivenName
				);
			
			sourceDocumentData.addColumn(
					CpatToolCConstants.ReportSummaryTabKeys.REPORTER_LAST
				, 	"Reporter Last"
				, 	reporterFamilyName
				);
			
			sourceDocumentData.addColumn(
					CpatToolCConstants.ReportSummaryTabKeys.PATIENT_PATIENT_INITIALS
				, 	"Pat Initials"
				, 	patientInitials
				);
			
			sourceDocumentData.addColumn(
					CpatToolCConstants.ReportSummaryTabKeys.PATIENT_SEX
				, 	"Sex"
				, 	patientGender
				);
			
			sourceDocumentData.addColumn(
					CpatToolCConstants.ReportSummaryTabKeys.PATIENT_DOB
				, 	"DOB"
				, 	patientDOB
				);
			
			sourceDocumentData.addColumn(
					CpatToolCConstants.ReportSummaryTabKeys.REPORTER_COUNTRY
				, 	"Country"
				, 	country
				);
				
			testResult = oWebWriter.feedClassificationMenuTab(null,null,sourceDocumentData,propMap);
		assertTrue(testResult);
	}
}
