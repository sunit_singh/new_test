package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class CountryOfOccurrenceTest {
	
	CountryOfOccurrence obj = new CountryOfOccurrence();

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.generalinformation.countryofoccurence";
	
	final static String sourceDocumentKey = "OccurCountry";
	
	@Before
	public void initializeClass() {
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){
		
		StringParsedDataValue e2b_occurCountry = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeOccurCountry = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("OccurCountry", "OccurCountry", e2b_occurCountry);
		sourceDocumentData.addColumn("Narrative_EventCountryOccurred", "Narrative_EventCountryOccurred", e2b_narrativeOccurCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals(""));
		
	}
	
	@Test
	public void testRun2(){
		
		StringParsedDataValue e2b_occurCountry = new StringParsedDataValue("Brazil", "Brazil", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeOccurCountry = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("OccurCountry", "OccurCountry", e2b_occurCountry);
		sourceDocumentData.addColumn("Narrative_EventCountryOccurred", "Narrative_EventCountryOccurred", e2b_narrativeOccurCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Brazil"));
		
	}
	
	@Test
	public void testRun3(){
		
		StringParsedDataValue e2b_occurCountry = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeOccurCountry = new StringParsedDataValue("India", "India", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("OccurCountry", "OccurCountry", e2b_occurCountry);
		sourceDocumentData.addColumn("Narrative_EventCountryOccurred", "Narrative_EventCountryOccurred", e2b_narrativeOccurCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("India"));
		
	}

}
