package com.sciformix.client.automation.irt.fields;

import static com.sciformix.client.pat.toolc.CpatToolCHelper.propMap;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class ReporterLastTest {

	ReporterLast obj = new ReporterLast();

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;
	

	final static String writerKey = "dataentry.reporter.last";
	
	final static String sourceDocumentKey = "Reporter_FamilyName";

	@Before
	public void initializeClass() {
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
		
		propMap = new HashMap<String, String>();
		propMap.put("emea.countries", "Belgium,Bulgaria,Czech Republic,Denmark,Germany,Estonia,Ireland,Liechtenstein,Portugal,Romania,Slovenia,Slovakia,Finland,Sweden,United Kingdom,Greece,Spain,France,Croatia,Italy,Cyprus,Latvia,Norway,Lithuania,Luxembourg,Hungary,Malta,Netherlands,Austria,Poland,Iceland");
	}
	
	@Test
	public void isEmeaCountry(){
		
		StringParsedDataValue e2b_reporterFamilyName = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BELGIUM", "BELGIUM", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_FamilyName", "Reporter_FamilyName", e2b_reporterFamilyName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("private"));	
	}
	
	@Test
	public void isNotEmeaCountry(){
		StringParsedDataValue e2b_reporterFamilyName = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BRAZIL", "BRAZIL", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativePatientInitial = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_FamilyName", "Reporter_FamilyName", e2b_reporterFamilyName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		sourceDocumentData.addColumn("Narrative_PatientInitials", "Narrative_PatientInitials", e2b_narrativePatientInitial);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("JMX"));	
	}
	
	@Test
	public void whenFirstNameIsBlank(){
		StringParsedDataValue e2b_reporterFamilyName = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BRAZIL", "BRAZIL", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativePatientInitial = new StringParsedDataValue("RDX", "RDX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_FamilyName", "Reporter_FamilyName", e2b_reporterFamilyName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		sourceDocumentData.addColumn("Narrative_PatientInitials", "Narrative_PatientInitials", e2b_narrativePatientInitial);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("RDX"));	
	}
	
	@Test
	public void whenFirstNameIsNotEqualToLastName(){
		StringParsedDataValue e2b_reporterFamilyName = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BRAZIL", "BRAZIL", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativePatientInitial = new StringParsedDataValue("RDX", "RDX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_FamilyName", "Reporter_FamilyName", e2b_reporterFamilyName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		sourceDocumentData.addColumn("Narrative_PatientInitials", "Narrative_PatientInitials", e2b_narrativePatientInitial);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("RDX"));	
	}
	
	@Test
	public void whenEmeaListContainsPartOfCountryName(){
		StringParsedDataValue e2b_reporterFamilyName = new StringParsedDataValue("JMX", "JMX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_primarySourceCountry = new StringParsedDataValue("BEL", "BEL", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativePatientInitial = new StringParsedDataValue("RDX", "RDX", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Reporter_FamilyName", "Reporter_FamilyName", e2b_reporterFamilyName);
		sourceDocumentData.addColumn("PrimarySourceCountry", "PrimarySourceCountry", e2b_primarySourceCountry);
		sourceDocumentData.addColumn("Narrative_PatientInitials", "Narrative_PatientInitials", e2b_narrativePatientInitial);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("RDX"));	
	}
	
	
}
