package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class RouteofAdminTest {
	
	static ParsedDataTable
		sourceDocumentData
	;	
	
	static Map<String,String>
		dataEntryMap
	;
	
	final static String
		writerKey	=	"dataentry.drugtherapy.routofadmin"
	;
	@BeforeClass
	public static void initializeClass() {
		sourceDocumentData	=	new ParsedDataTable();
		dataEntryMap		=	new HashMap<>();
	}
	
	/**
	 * This method checks the value returned is
	 * "Unspecified" or not. As per the field
	 * mapping if the E2B field (B.4.k.8) and
	 * "Route of Administration" is blank then
	 * the value returned should be "Unspecified".
	 */
	@Test
	public void getRouteOfAdminAsUnspecified() {
		StringParsedDataValue	l_narrvalue	=	null,
								l_roavalue	=	null;
		l_narrvalue	=	new StringParsedDataValue(
							""
						, 	""
						, 	DataState.ASIS
						, 	ValidationState.VALIDATION_NOT_APPLICABLE
						);
		l_roavalue	=	new StringParsedDataValue(
							""
						, 	""
						, 	DataState.ASIS
						, 	ValidationState.VALIDATION_NOT_APPLICABLE
						);
		sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION
		, 	"Narrative Route of Admin"
		, 	null
		);
		sourceDocumentData.addColumn(
			"Drug_RouteOfAdministration"
		, 	"Route of Admin"
		, 	null
		);
		RouteofAdmin	l_roa	=	new RouteofAdmin();
		l_roa.applyBusinessLogicInitial(
			dataEntryMap
		, 	sourceDocumentData
		, 	writerKey
		, 	"Drug_RouteOfAdministration"
		);
		assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.UNSPECIFIED));
		
		sourceDocumentData.addColumn(
			CpatToolCConstants.NarrativeKeys.NARRATIVE_PATIENT_ROUTE_OF_ADMINISTRATION
		, 	"Narrative Route of Admin"
		, 	l_narrvalue
		);
		sourceDocumentData.addColumn(
			"Drug_RouteOfAdministration"
		, 	"Route of Admin"
		, 	l_roavalue
		);
		dataEntryMap.remove(writerKey);
		l_roa.applyBusinessLogicInitial(
			dataEntryMap
		, 	sourceDocumentData
		, 	writerKey
		, 	"Drug_RouteOfAdministration"
		);
		assertTrue(dataEntryMap.get(writerKey).equals(CpatToolConstants.FieldsLogicValues.UNSPECIFIED));
	}
}
