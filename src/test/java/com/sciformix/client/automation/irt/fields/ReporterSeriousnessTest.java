package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.commons.utils.ParsedDataTable;

public class ReporterSeriousnessTest {
	
	ReporterSeriousness obj = null;

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "casedata.seriousassessment.reporterseriousness";
	
	final static String sourceDocumentKey = "";
	
	@Before
	public void initializeClass() {
		obj = new ReporterSeriousness();
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"no");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Not Serious"));	
	}
	
	@Test
	public void testRun2(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"no");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("Serious"));	
	}

}
