package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.commons.utils.ParsedDataTable;

public class PriorityTest {
	
	Priority obj = null;

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "casedata.seriousassessment.priority";
	
	final static String sourceDocumentKey = "";
	
	@Before
	public void initializeClass() {
		obj = new Priority();
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"no");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("5"));	
	}
	
	@Test
	public void testRun2(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"yes");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("1"));	
	}
	
	@Test
	public void testRun3(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"yes");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("1"));	
	}
	
	@Test
	public void testRun4(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"yes");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("2"));	
	}
	
	@Test
	public void testRun5(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"yes");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("2"));	
	}
	
	@Test
	public void testRun6(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"yes");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"yes");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("2"));	
	}
	
	@Test
	public void testRun7(){

		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DEATH,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_HOSPT_ON_PROLONGED,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_LIFE_THETENING,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_DISABILITY,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_BIRTH_DEFECT,"no");
		dataEntryMap.put(IRTConstants.DataEntry.SERUOUSNESS_CRIT_OTHER_MED_IMP_CONDITION,"yes");
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("2"));	
	}
	
}
