package com.sciformix.client.automation.irt.fields;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.automation.irt.IRTWebApplicationWriter;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class DateOfBirthTest {

	DateOfBirth obj = new DateOfBirth();

	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;

	final static String writerKey = "dataentry.patient.dateofbirth";
	
	final static String sourceDocumentKey = "Patient_DateOfBirth";

	@Before
	public void initializeClass() {
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
	}
	
	@Test
	public void testRun1(){
		
		StringParsedDataValue e2b_dateOfBirth = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeDOB = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_DateOfBirth", "Patient_DateOfBirth", e2b_dateOfBirth);
		sourceDocumentData.addColumn("Narrative_PatientDOB", "Narrative_PatientDOB", e2b_narrativeDOB);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals(""));
	}
	
	@Test
	public void testRun2(){
		
		StringParsedDataValue e2b_dateOfBirth = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeDOB = new StringParsedDataValue("05-Oct-1981", "05-Oct-1981", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_DateOfBirth", "Patient_DateOfBirth", e2b_dateOfBirth);
		sourceDocumentData.addColumn("Narrative_PatientDOB", "Narrative_PatientDOB", e2b_narrativeDOB);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("05-Oct-1981"));
	}
	
	@Test
	public void testRun3(){
		
		StringParsedDataValue e2b_dateOfBirth = new StringParsedDataValue("22-Sep-1991", "22-Sep-1991", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeDOB = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_DateOfBirth", "Patient_DateOfBirth", e2b_dateOfBirth);
		sourceDocumentData.addColumn("Narrative_PatientDOB", "Narrative_PatientDOB", e2b_narrativeDOB);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("22-Sep-1991"));
	}
	
	@Test
	public void testRun4(){
		
		StringParsedDataValue e2b_dateOfBirth = new StringParsedDataValue("22-Sep-1991", "22-Sep-1991", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeDOB = new StringParsedDataValue("05-Oct-1981", "05-Oct-1981", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		
		sourceDocumentData.addColumn("Patient_DateOfBirth", "Patient_DateOfBirth", e2b_dateOfBirth);
		sourceDocumentData.addColumn("Narrative_PatientDOB", "Narrative_PatientDOB", e2b_narrativeDOB);
		
		obj.applyBusinessLogicInitial(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("22-Sep-1991"));
	}
	
	@Test
	public void testRun5(){
		
		StringParsedDataValue e2b_dateOfBirth = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		StringParsedDataValue e2b_narrativeDOB = new StringParsedDataValue("", "", DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE);
		IRTWebApplicationWriter.sceptreFieldsMap.put(IRTConstants.SceptreFieldData.PAT_DOB,"21-Dec-1990");
		
		sourceDocumentData.addColumn("Patient_DateOfBirth", "Patient_DateOfBirth", e2b_dateOfBirth);
		sourceDocumentData.addColumn("Narrative_PatientDOB", "Narrative_PatientDOB", e2b_narrativeDOB);
		
		obj.applyBusinessLogicFollowUp(dataEntryMap, sourceDocumentData, writerKey, sourceDocumentKey);
		
		assertTrue(dataEntryMap.get(writerKey).equals("21-Dec-1990"));
	}
}
