package com.sciformix.client.automation.irt;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import com.sciformix.TestCpatPreviewScreen;
import com.sciformix.client.pat.commons.CpatDuplicateRecordSummaryScreen;
import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolConstants;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.screens.CpatCasePreviewScreen;
import com.sciformix.client.pat.toolc.CpatToolCConstants;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.utils.ParsedDataTable;

public class TestDuplicateSummaryScreen extends CpatToolBase implements IAction  {
	
	protected TestDuplicateSummaryScreen(String p_sToolTitle, String p_sToolName, String p_sToolDisplayName, String p_sToolVersion, String p_sUserInputsScreenFile)
	{
		super(p_sToolTitle, p_sToolName, p_sToolDisplayName, p_sToolVersion, p_sUserInputsScreenFile);
	}

	@Override
	public void performAction(CPAT_ACTION p_eAction, Object... oParameters) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void loadCpatFieldMap() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> validateUserInputs(Map<String, String> mapExtractedParameters) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void userInputsScreenEventHandler(CpatToolBase oToolBase, IAction oCallbackAction, Component comp,
			JFrame frame) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void previewScreenEventHandler(CpatToolBase oToolBase, IAction m_oCallbackAction, Component comp,
			JFrame oWindowFrame, ParsedDataTable summaryDataTable, Map<String, String> parsedDataMap) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		
		Map<String, Object> params = null;
		Map<String, List<DuplicateRecordFields>> irtDuplicateRecprdsMap = null;
		DuplicateRecordFields dupRecField = null;
		
		List<DuplicateRecordFields> irtRecFieldList = null,
									sceptreRecFieldList = null;
		
		irtRecFieldList = new ArrayList<>();
		
		dupRecField = new DuplicateRecordFields();
		dupRecField.setAerNo("1233233");
		dupRecField.setLocalCaseId("L_ID_1");
		dupRecField.setReporterCountry("INDIA");
		dupRecField.setPatientDOB("	25-MAY-1990");
		dupRecField.setPatientGender("Male");
		dupRecField.setPatientInitials("GW");
		dupRecField.setReporterFamilyName("FM Name");
		dupRecField.setReporterGivenName("Given name");
		
		irtRecFieldList.add(dupRecField);
		
		dupRecField = new DuplicateRecordFields();
		dupRecField.setAerNo("4433322");
		dupRecField.setLocalCaseId("L_ID_2");
		dupRecField.setReporterCountry("US");
		dupRecField.setPatientDOB("	25-JUN-2000");
		//dupRecField.setPatientGender("");
		dupRecField.setPatientInitials("MW");
		dupRecField.setReporterFamilyName("FFF");
		dupRecField.setReporterGivenName("NNN");
		
		irtRecFieldList.add(dupRecField);
		
		dupRecField = new DuplicateRecordFields();
		dupRecField.setAerNo("1232233");
		dupRecField.setLocalCaseId("L_ID_3");
		dupRecField.setReporterCountry("GERMANY");
		//dupRecField.setPatientDOB("	10-MAY-2010");
		dupRecField.setPatientGender("Male");
		dupRecField.setPatientInitials("GW");
		dupRecField.setReporterFamilyName("FM Name");
		dupRecField.setReporterGivenName("NNN");
		
		irtRecFieldList.add(dupRecField);
		
		sceptreRecFieldList = new ArrayList<>();
		
		dupRecField = new DuplicateRecordFields();
		dupRecField.setAerNo("1233233");
		dupRecField.setLocalCaseId("L_ID_1");
		dupRecField.setReporterCountry("UK");
		dupRecField.setPatientDOB("	25-MAY-1990");
		dupRecField.setPatientGender("Male");
		dupRecField.setPatientInitials("MM");
		dupRecField.setReporterFamilyName("FAMILY");
		//dupRecField.setReporterGivenName("name");
		
		sceptreRecFieldList.add(dupRecField);
		
		dupRecField = new DuplicateRecordFields();
		dupRecField.setAerNo("41433322");
		dupRecField.setLocalCaseId("L_ID_5");
		dupRecField.setReporterCountry("USA");
		dupRecField.setPatientDOB("	25-JUN-1980");
		dupRecField.setPatientGender("Male");
		dupRecField.setPatientInitials("MW");
		dupRecField.setReporterFamilyName("FFF");
		dupRecField.setReporterGivenName("");
		
		sceptreRecFieldList.add(dupRecField);
		
		dupRecField = new DuplicateRecordFields();
		dupRecField.setAerNo("1232233");
		dupRecField.setLocalCaseId("L_ID_3");
		dupRecField.setReporterCountry("GERMANY");
		dupRecField.setPatientDOB("");
		dupRecField.setPatientGender("Male");
		dupRecField.setPatientInitials("GW");
		dupRecField.setReporterFamilyName("FM Name");
		dupRecField.setReporterGivenName("NNN");
		
		sceptreRecFieldList.add(dupRecField);
		
		/*sceptreRecFieldList = new ArrayList<>();
		irtRecFieldList = new ArrayList<>();
		*/
		irtDuplicateRecprdsMap = new HashMap<>();

		irtDuplicateRecprdsMap.put(CpatToolCConstants.DUPLICATE_IRT_RECORD_LIST, irtRecFieldList);
		irtDuplicateRecprdsMap.put(CpatToolCConstants.DUPLICATE_SCEPTRE_RECORD_LIST, sceptreRecFieldList);
		
		
		TestDuplicateSummaryScreen oScreen = null;
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		FileConfigurationHome.init(oStartupLogger, "AA_Parser,BA_Parser");
		
		oScreen = new TestDuplicateSummaryScreen("Cpat", "Cpat", "V0","", "");
		
		params = new HashMap<>();
		params.put(CpatToolConstants.UI.DUPLIACTE_REC_MAP, irtDuplicateRecprdsMap);
		
		dupRecField = new DuplicateRecordFields();
		//dupRecField.setAerNo("1232233");
		dupRecField.setLocalCaseId("aaa");
		dupRecField.setReporterCountry("GERMANY");
		dupRecField.setPatientDOB("");
		dupRecField.setPatientGender("Male");
		dupRecField.setPatientInitials("GW");
		dupRecField.setReporterFamilyName("aaaaa");
		dupRecField.setReporterGivenName("NNN");
		
		params.put(CpatToolCConstants.ContentParams.DUPLICATE_RECORD, dupRecField);
		
		CpatDuplicateRecordSummaryScreen oPreviewScreen = new CpatDuplicateRecordSummaryScreen(oScreen, oScreen, params);
		
	}



	@Override
	public void DuplicateSummaryScreenEventHandler(CpatToolBase oToolBase, IAction m_oCallbackAction, Component comp,
			JFrame oWindowFrame, Map<String, List<DuplicateRecordFields>> dupRecList,
			ParsedDataTable summaryDataTable) {
		// TODO Auto-generated method stub
		
	}

}
