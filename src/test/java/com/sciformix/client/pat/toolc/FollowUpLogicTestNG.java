package com.sciformix.client.pat.toolc;

import static com.sciformix.client.automation.irt.IRTWebApplicationWriter.receiptDataMap;
import static com.sciformix.client.pat.toolc.CpatToolCHelper.sceptreFieldsMap;
import static com.sciformix.client.pat.toolc.CpatToolCHelper.propMap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sciformix.client.automation.irt.IFieldBusinessLogic;
import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.commons.utils.ParsedDataTable;
import com.sciformix.commons.utils.StringUtils;
import com.sciformix.commons.utils.ParsedDataTable.DataState;
import com.sciformix.commons.utils.ParsedDataTable.StringParsedDataValue;
import com.sciformix.commons.utils.ParsedDataTable.ValidationState;

public class FollowUpLogicTestNG {
	
	private static final String BUSINESS_FIELDLOGIC_PACKAGE = "com.sciformix.client.automation.irt.fields.";
	private static final String COMMON_FIELD_PACKAGE = "com.sciformix.client.automation.irt.";
	private static final String CSV_LOCATION = "\\Desktop\\testbusinesslogic\\FollowUpLogicInput.csv";
	
	private static int run_counter = 1;
	
	static ParsedDataTable sourceDocumentData;

	static Map<String, String> dataEntryMap;
	
	@BeforeTest
	public void getfilePath() {
		propMap = new HashMap<String, String>();
		propMap.put("emea.countries", "Belgium,Bulgaria,Czech Republic,Denmark,Germany,Estonia,Ireland,Liechtenstein,Portugal,Romania,Slovenia,Slovakia,Finland,Sweden,United Kingdom,Greece,Spain,France,Croatia,Italy,Cyprus,Latvia,Norway,Lithuania,Luxembourg,Hungary,Malta,Netherlands,Austria,Poland,Iceland");
		propMap.put("serious.keywords", "cancer, passed away, painful sores, cortizone, airways shut down, difficulty breathing, emergency inhaler, rescue inhaler, bubble under my tongue, bump under my tongue, shots, epinephrine, anaphylactic shock, IV steroids, IV antibiotics, IV anti-nausea, IV Benadryl, Morphine, needed a stitch, comes apart, cotton left, given stitches, stitches, injection, epi pen, IV, steroid shot, found plastic in her vagina, adrenaline shot, oxygen, death, toothbrush broke, tampon stuck, died, difficulty speaking, throat closed up, would get asthma, hysterectomy, intra ductal carcinoma, surgery, blockage of bile duct, admitted to hospital, cholecystectomy, cholecystitis, choleclocholithiasis, biliaryshincterotomy, ERCP, toxic shock, operation");
		
		receiptDataMap = new HashMap<String, String>();
		receiptDataMap.put(IRTConstants.DataEntry.EDITRECEIPT_SENDER, "Sales-Force");
	}
	
	/**
	 * @param paramArray
	 * paramArray[0] - TEST-SCENARIO-NAME
	 * paramArray[1] - TARGET-CLASS-NAME
	 * paramArray[2] - SOURCE-DOC-KEY
	 * paramArray[3] - WRITER-KEY
	 * paramArray[4] - EXPECTED-VALUE
	 * paramArray[5] - DEPENDENT_SOURCE_DOC_KEYS
	 * paramArray[6] - PrimarySourceCountry
	 * paramArray[7] - Reporter_FamilyName
	 * paramArray[8] - Narrative_PatientInitials
	 * paramArray[9] - Reporter_GivenName
	 */
	@Test(dataProvider = "getParamValuesFromExcel")
	public void evaluateTest(Object... paramArray) {
		
		IFieldBusinessLogic obj = null;
		sourceDocumentData = new ParsedDataTable();
		dataEntryMap = new HashMap<>();
		sceptreFieldsMap = new HashMap<>();
		String []dependentKeys = null;
		String []dependentIrtKeys = null;

		try {
			
			System.out.println("");
			System.out.print("Run #" + StringUtils.leftPad(run_counter, 2) + "> " + paramArray[0]);
			run_counter++;
			
			if(paramArray[1].toString().equalsIgnoreCase("CommonFieldBusinessLogic")){
				obj = (IFieldBusinessLogic) Class.forName(COMMON_FIELD_PACKAGE+paramArray[1].toString()).newInstance();
			} else {
				obj = (IFieldBusinessLogic) Class.forName(BUSINESS_FIELDLOGIC_PACKAGE+paramArray[1].toString()).newInstance();
			}
			
			
			dependentKeys = paramArray[5].toString().split(";",-1);
			int temp = 6;
			
			for (int tempCounter = 0; tempCounter < dependentKeys.length; tempCounter++) {
				
				sourceDocumentData.addColumn(dependentKeys[tempCounter], dependentKeys[tempCounter],
						new StringParsedDataValue(paramArray[temp].toString(), paramArray[temp].toString(),
								DataState.ASIS, ValidationState.VALIDATION_NOT_APPLICABLE));
				temp++;
			}
			
			dependentIrtKeys = paramArray[temp].toString().split(";");
			int temp2 = temp+1;
			for(int tempCounter = 0; tempCounter < dependentIrtKeys.length; tempCounter++){
				sceptreFieldsMap.put(dependentIrtKeys[tempCounter].toString(), paramArray[temp2].toString());
				temp2++;
			}
			
			obj.applyBusinessLogicFollowUp(dataEntryMap, sourceDocumentData, paramArray[3].toString(), paramArray[2].toString());
			
			if(dataEntryMap.get(paramArray[3].toString()).equals(paramArray[4].toString())){
				System.out.println(": PASSED");
			} else {
				System.out.println(": FAILED");
			}
			
			Assert.assertEquals(dataEntryMap.get(paramArray[3].toString()),paramArray[4].toString());
			
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
	}
	
	@DataProvider
	public Object[][] getParamValuesFromExcel() throws Exception {
		Object[][] paramArray = readParameters(); // Read from CVS file
		return (paramArray);
	}

	public Object[][] readParameters() throws Exception {
		List<Object[]> parameterList = new ArrayList<Object[]>();
		Object[][] parameters = null;
		int nCounter = 0;
		
		String csvFilePath = System.getProperty("user.home") + CSV_LOCATION;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {

			br = new BufferedReader(new FileReader(csvFilePath));
			while ((line = br.readLine()) != null) {
				if (line.trim().length() > 0) {
					Object paramArray[] = line.split(cvsSplitBy,-1);
					parameterList.add(paramArray);
				}
			}

			parameters = new Object[parameterList.size()][];
			nCounter = 0;
			for (Object[] obj : parameterList) {
				parameters[nCounter++] = obj;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}
		return parameters;
	}

}
