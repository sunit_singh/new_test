package com.sciformix.client.pat.toolc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sciformix.client.automation.irt.IRTDuplicateFields;
import com.sciformix.client.automation.irt.IRTDuplicateSearchCriteria;
import com.sciformix.client.pat.commons.DuplicateFields;
import com.sciformix.commons.utils.StringUtils;

public class DuplicateSearchTestNG {
	
	private String path;
	
	Map<String, Boolean> searchCriteriaMap = null;
	private static int counter = 1;
	
	@BeforeClass
	@Parameters("path")
	public void getfilePath(String path) {
		this.path = path;
	}
	
	/**
	 * @param paramArray
	 * paramArray[0] - Reporter Given Name 
	 * paramArray[1] - Reporter Family Name
	 * paramArray[2] - Patient DOB
	 * paramArray[3] - Patient ID
	 * paramArray[4] - Patient Gender
	 * paramArray[5] - Country
	 * paramArray[6] - Is Search Applicable (Yes/No)
	 */
	@Test(dataProvider = "getParamValuesFromExcel")
	public void evaluateTest(Object... paramArray) {
		
		DuplicateFields l_dupfielddata = null;
		String reporterGivenName = null;
		String reporterFamilyName = null;
		String patientDOB = null;
		String patientInitials = null;
		String patientGender = null;
		String country = null;
		
		String result = null;
		
		try {
			
			IRTDuplicateSearchCriteria l_dupsearchcriteria = IRTDuplicateSearchCriteria.getInstance(
					DuplicateSearchTestNG.class.getClassLoader().getResourceAsStream("DuplicateSearchCriteria.txt"));
			searchCriteriaMap = l_dupsearchcriteria.getDuplicateSearchCriteriaMap();
			
			reporterGivenName = (String) paramArray[0];
			reporterFamilyName = (String) paramArray[1];
			patientDOB = (String) paramArray[2];
			patientInitials = (String) paramArray[3];
			patientGender = (String) paramArray[4];
			country = (String) paramArray[5];
			
			l_dupfielddata = new IRTDuplicateFields(
					!StringUtils.isNullOrEmpty(reporterGivenName)
				, 	!StringUtils.isNullOrEmpty(reporterFamilyName)
				,	!StringUtils.isNullOrEmpty(patientDOB)
				, 	!StringUtils.isNullOrEmpty(patientInitials)
				, 	!StringUtils.isNullOrEmpty(patientGender)
				, 	!StringUtils.isNullOrEmpty(country)
				);
			
			if (l_dupfielddata.isSearchToBePerformed(searchCriteriaMap)) {
				result = "Yes";
			} else {
				result = "No";
			}
			
			if(paramArray[6].toString().equalsIgnoreCase(result)){
				System.out.println("CSV Case " + counter + " : PASSED");
			} else {
				System.out.println("CSV Case" + counter + " : FAILED");
			}
			counter ++;
			
			Assert.assertEquals(paramArray[6].toString(), result);
			
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
	}
	
	@DataProvider
	public Object[][] getParamValuesFromExcel() throws Exception {
		Object[][] paramArray = readParameters(); // Read from CVS file
		return (paramArray);
	}

	public Object[][] readParameters() throws Exception {
		List<Object[]> parameterList = new ArrayList<Object[]>();
		Object[][] parameters = null;
		int nCounter = 0;
		
		String csvFilePath = "src\\test\\testng\\" + this.path;	
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {

			br = new BufferedReader(new FileReader(csvFilePath));
			while ((line = br.readLine()) != null) {
				if (line.trim().length() > 0) {
					Object paramArray[] = line.split(cvsSplitBy,-1);
					parameterList.add(paramArray);
				}
			}

			parameters = new Object[parameterList.size()][];
			nCounter = 0;
			for (Object[] obj : parameterList) {
				parameters[nCounter++] = obj;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}
		return parameters;
	}

}
