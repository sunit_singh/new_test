package com.sciformix.client.pat.toolc;

import static com.sciformix.client.pat.toolc.CpatToolCHelper.propMap;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sciformix.client.automation.irt.IRTConstants;
import com.sciformix.client.pat.toolb.CpatToolBHelper;

public class CpatToolCHelperTest {

	@BeforeClass
	public static void createPropertiesMap() {
		propMap	=	new HashMap<>();
		Properties configProp = null;
		
		propMap = new HashMap<String, String>();
		InputStream inputStream = CpatToolBHelper.class.getClassLoader().getResourceAsStream("ConfigForIrt_Validation.properties");
        try {
        	configProp = new Properties();
            configProp.load(inputStream);
            
            for (final String name: configProp.stringPropertyNames())
			    propMap.put(name, configProp.getProperty(name));
        
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
	}
	@Test
	public void isCountryEMEACountryNameNullOrBlank() {
		assertEquals(CpatToolCHelper.isCountryEMEA(null), false);
		assertEquals(CpatToolCHelper.isCountryEMEA(""), false);
	}
	@Test
	public void isCountryEMEAFalse() {
		assertEquals(CpatToolCHelper.isCountryEMEA("India"), false);
		assertEquals(CpatToolCHelper.isCountryEMEA("INDIA"), false);
		assertEquals(CpatToolCHelper.isCountryEMEA("india"), false);
		assertEquals(CpatToolCHelper.isCountryEMEA("Czech"), false);
		assertEquals(CpatToolCHelper.isCountryEMEA("Republic"), false);
	}
	@Test
	public void isCountryEMEATrue() {
		assertEquals(CpatToolCHelper.isCountryEMEA("Bulgaria"), true);
		assertEquals(CpatToolCHelper.isCountryEMEA("BULGARIA"), true);
		assertEquals(CpatToolCHelper.isCountryEMEA("Czech Republic"), true);
	}
	@Test
	public void isCountryEMEAPropertyNullOrBlank() {
		propMap.put(IRTConstants.EMEA_COUNTRIES, null);
		assertEquals(CpatToolCHelper.isCountryEMEA("Bulgaria"), false);
		propMap.put(IRTConstants.EMEA_COUNTRIES, "");
		assertEquals(CpatToolCHelper.isCountryEMEA("Bulgaria"), false);
		propMap	=	null;
		assertEquals(CpatToolCHelper.isCountryEMEA("Bulgaria"), false);
		propMap	=	new HashMap<>();
		assertEquals(CpatToolCHelper.isCountryEMEA("Bulgaria"), false);
	}
}
