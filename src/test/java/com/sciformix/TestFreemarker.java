package com.sciformix;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class TestFreemarker {

        public static void main(String[] args) throws TemplateException, IOException, ParseException, SAXException, ParserConfigurationException {
        		//<#local temp = temp+ "," +test[colValue]>
        	
                // 1. Configure FreeMarker
                //
                // You should do this ONLY ONCE, when your application starts,
                // then reuse the same Configuration object elsewhere.
        
        		
        	
        	String jsonString = getJsonString();
        	//System.out.println(jsonString);
    		JSONParser parser = new JSONParser();
    		Object obj = parser.parse(jsonString);
    		JSONArray array = (JSONArray)obj;
    		JSONObject jsonObject = (JSONObject)array.get(0);
    		List<Object> list = new ArrayList<>();
        	String[] sarrTemp = null;	
        		Map<String, Object>map = new HashMap();
        		Map<String, Object>productMap = new HashMap();
        		Map<String, Object>actionMap = new HashMap();
        		Map<String, Object>eventValueMap = new HashMap();
        		Map<String, Object>causalityValueMap = new HashMap();
                Configuration cfg = new Configuration();
                
                // Where do we load the templates from:
                cfg.setClassForTemplateLoading(TestFreemarker.class, "templates");
                cfg.setDirectoryForTemplateLoading(new File("D:\\input_files\\"));
                // Some other recommended settings:
                // cfg.setIncompatibleImprovements(new Version(2, 3, 20));
                cfg.setDefaultEncoding("UTF-8");
                cfg.setLocale(Locale.US);
                cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);


                Map<String, Object> input = new HashMap<String, Object>();

                input.put("title", "Narrative");

                for(Object key : jsonObject.keySet())
                {
                	input.put(key.toString(), Patient.getKeyValue(key.toString(), jsonObject.get(key)));
                }
                
                input.put("pt_his_desc_med_his", "no");
                input.put("pt_his_food_alg", "");
                input.put("r_e_outcome", "");
                input.put("patnt_info", "");
                
                
               /* list.add("12-june-2016");
                list.add("13-june-2016");
                list.add("14-june-2016");
                input.put("drug_eff_start_dt", list);
                
                input.put("dateUtil", new Patient());*/
                
                
                
                list = new ArrayList<>();
                list.add(" contact redness facial");
                list.add(" contact peeling facial");
                list.add(" contact burning facial");
                
                input.put("r_e_prim_src", list);
                
               /* list = new ArrayList<>();
                list.add(" NTG RAPID WRINKLE REPAIR EYE CREAM ");
                list.add(" Rapid Wrinkle Repair Daily Moisturizer ");*/
                input.put("product_name", "Rapid Wrinkle Repair Daily Moisturizer");
                eventValueMap.put("r_e_prim_src", list);
                
                list = new ArrayList<>();
                list.add("not recovered");
                list.add("unknown");
                list.add("unknown");
                eventValueMap.put("r_e_outcome", list);
                
                actionMap.put("_event_", eventValueMap);
                
                
                list = new ArrayList<>();
                list.add("Related");
                list.add("Probable");
                list.add("Related");
                causalityValueMap.put("react_ased_ich_cause", list);
                
                list = new ArrayList<>();
                list.add("Likely");
                list.add("very Likely");
                list.add("Not clearly attributable");
                causalityValueMap.put("react_ased_eea_cause", "");
                
                list = new ArrayList<>();
                list.add("Application site redness");
                list.add("Application site peeling");
                list.add("Application site burning");
                
                causalityValueMap.put("react_ased_llt_name", list);
                
                actionMap.put("_causality_", causalityValueMap);
                
              //  productMap.put("NTG RAPID WRINKLE REPAIR EYE CREAM", actionMap);
                
                // productMap.put("NTG22", actionMap);
              //  input.put("productmap", productMap);
                input.put("eventAndCausalityMap", actionMap);
                
                list = new ArrayList<>();
                list.add(" NTG RAPID WRINKLE REPAIR EYE CREAM ");
                list.add(" Rapid Wrinkle Repair Daily Moisturizer ");
               // input.put("product_name", list);
                
                list = new ArrayList<>();
                list.add("TM NTG RAPID WRINKLE REPAIR EYE CREAM ");
                list.add("TM Rapid Wrinkle Repair Daily Moisturizer ");
                //input.put("prod_trademark", list);
                
               
                input.put("med_hist_ind", "yes");
                input.put("food_alg_ind", "yes");
                input.put("pt_his_food_alg", "apple");
                input.put("concom_rb", "");
                input.put("drug_role", "suspect");
                input.put("drug_route", "topical");
                input.put("drug_dose_txt", "twice daily");
                input.put("indctn_crs", "wrinkle reduction");
                input.put("drink_alcohol", "yes");
                input.put("patnt_smoke", "yes");
                input.put("patnt_smoke", "yes");
                input.put("patnt_ht_cm", "");
                input.put("patnt_onset_wt_kg", "");
                input.put("reg_class", "device");
                input.put("drug_action_taken", "dose decreased");
               // input.put("r_e_prim_src", "redness facial");
               // input.put("r_e_outcome", "recovering");
                input.put("ich_cause", "Probable");
                input.put("patnt_age", "");
                input.put("patnt_curr_age", "");
                input.put("rcvd_mode", "Social Media");
                input.put("patnt_age_grp", "18-64 (n years)- child");
                input.put("patnt_ethn", "caucasion");
                input.put("react_ased_eea_cause", "Likely");
                input.put("patnt_gender", "male");
                input.put("cntct_patnt_rel", "other");
                input.put("event_event_cntry", "United States of America");
                input.put("pt_his_desc_med_his", "diabetes");
                input.put("pt_his_food_alg", "apple");
                input.put("react_ased_llt_name", "Application site redness");
                input.put("prod_eff_start_dt_cd", "01/2016");
                input.put("lot_no", "NA");
                input.put("exp_dt", "01/02/2016");
                /*,\"drug_eff_start_dt\":\"unspecified\",\"prod_trademark\":\"CLEAN & CLEAR\u00c2\u00ae Blackhead Eraser Scrub\", \"concom_product_name\":\"\", \"drug_eff_start_dt\":\"\", \"r_e_prim_src\":\"\", \"patnt_lst_mns_dt\":\"10\", \"preg_due_dt\":\"5\", \"how_treat\":\"\", \"drug_action_taken\":\"\", \"r_e_outcome\":\"\",	\"pqms_file_stat\":\"\", \"pqms_cat_disp\":\"\", \"pqms_cmnt_txt\":\"\", \"serious\":\"Non-serious\", \"cause\":\"\"}]
*/                /*list = new ArrayList<>();patnt_ht_cm
                list.add(" NTG RAPID WRINKLE REPAIR EYE CREAM ");
                list.add(" Rapid Wrinkle Repair Daily Moisturizer ");
                input.put("drug_action_taken", list);*/
           
                 @SuppressWarnings("deprecation")
				Template template = new Template("", new StringReader(FileUtils.readFileToString(new File("D:\\input_files\\Auto Narrative_ND_Standard_modified.ftl"))));

                // Write output to the console
                Writer consoleWriter = new OutputStreamWriter(System.out);
                

                // For the sake of example, also write output into a file:
                Writer fileWriter = new FileWriter(new File("D:\\output.txt"));
              
                try {
                		template.process(input, consoleWriter);
                		//template.process(input, fileWriter);
                }catch(Exception e){
                	System.out.println("in catch");
                } 
                finally {
                        fileWriter.close();
                }
                /*}else
                {
                	System.out.println("Following keys are missing \n"+missingKeys);
                }*/

        }
        
        
        private static Object getInternalState(Object o, String fieldName) throws NoSuchFieldException, IllegalAccessException {
            Field field = o.getClass().getDeclaredField(fieldName);
            boolean wasAccessible = field.isAccessible();
            try {
                field.setAccessible(true);
                return field.get(o);
            } finally {
                field.setAccessible(wasAccessible);
            }
        }
		private static String getJsonString() {
		
			String jsonString = "[{\"rcvd_mode\":\"Social Media\",\"patnt_age\":\"20\",\"patnt_age_grp\":\"65 and over-Elderly\",\"tracking_no\":\"30000352239\",\"case_type\":\"Spontaneous report\",\"j_j_aware_dt\":\"08-December-2016\",\"patnt_ethn\":\"caucasion\", \"patnt_curr_age\":\"20\",\"patnt_curr_age_unit\":\"\",\"patnt_gender\":\"male\",\"cntct_patnt_rel\":\"mother\",\"event_event_cntry\":\"United States of America\",\"pt_his_desc_med_his\":\"diabetes\",\"pt_his_food_alg\":\"apple\",\"drug_eff_start_dt\":\"unspecified\",\"prod_trademark\":\"CLEAN & CLEAR\u00c2\u00ae Blackhead Eraser Scrub\", \"concom_product_name\":\"\", \"drug_eff_start_dt\":\"\", \"patnt_lst_mns_dt\":\"10\", \"preg_due_dt\":\"5\", \"how_treat\":\"\", \"drug_action_taken\":\"\", \"r_e_outcome\":\"\",	\"pqms_file_stat\":\"\", \"pqms_cat_disp\":\"\", \"pqms_cmnt_txt\":\"\", \"serious\":\"Non-serious\", \"cause\":\"\"}]";
			return jsonString;
		}

		private static String getCompanyName() {
			return "Sciformix";
		}
}