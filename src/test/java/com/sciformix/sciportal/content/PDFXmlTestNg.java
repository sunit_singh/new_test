package com.sciformix.sciportal.content;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.wink.json4j.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciConstants.StringConstants;
import com.sciformix.commons.rulesengine.IAction;
import com.sciformix.commons.rulesengine.Rule;
import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.sciportal.apps.ngv.NgvDataTypeUtils;
import com.sciformix.sciportal.apps.ngv.NgvXmlParser;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.FileUtils;
import com.sciformix.commons.utils.PdfUtils;
	

public class PDFXmlTestNg
{
	
	RuleSet oRuleset = null;
	Rule oRule = null;
	IAction oAction = null;
	private String path;
	private static final int PARAMINDEX_EXPECTED_OUTPUT = 0;
	static int nNumberOfDateFormats = 6;
	private static final Logger log = LoggerFactory.getLogger(PDFXmlTestNg.class);
	
	@Test(dataProvider="getParamValuesFromExcel")
	/*@Parameters({"createRuleSetParameter","expectedResult","runtimeKey1", //read parameters from testing.xml
		"runtimeKey2","runtimeKey3","runtimeValue1","runtimeValue2",
		"runtimeValue3"})*/ 
	
	public void evaluateTest(Object... paramArray)
	{	
		String sFileText = null;
		String sTagValue = "";
		BufferedReader oBufferedReader = null;
		DocumentBuilderFactory oDocumentBuilderFactory = null;
		DocumentBuilder oDocumentBuilder = null;
		InputSource oInputSource = null;
		Document oDocument = null;
		StringBuffer oOutputStringBuffer = null;
		//Step 1: Validate if parameters are sufficient for processing
		if (paramArray.length < (PARAMINDEX_EXPECTED_OUTPUT + 1))
		{
			//ERROR
			System.out.println("Insufficient parameters");
			assertTrue(false);
		}
		
		
		String sStagedFileName = (String) paramArray[0];
		try
		{
			sFileText = PdfUtils.getFileText(sStagedFileName);
			oBufferedReader = new BufferedReader(new StringReader(sFileText));
			oOutputStringBuffer = new StringBuffer();
			while((sTagValue = oBufferedReader.readLine())!=null)
			{
				if(!(sTagValue.toLowerCase().startsWith("page") ||sTagValue.toLowerCase().contains("http://")))
				{
					if(sTagValue.startsWith("-"))
						sTagValue = sTagValue.replaceAll("-", "");
					oOutputStringBuffer.append(sTagValue).append(StringConstants.CRLF);
				}
			}
			
			
			sFileText = oOutputStringBuffer.toString();
			sFileText = sFileText.replaceAll("&", " ");
			oDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		    oDocumentBuilder = oDocumentBuilderFactory.newDocumentBuilder();
		    oInputSource = new InputSource();
		    oInputSource.setCharacterStream(new StringReader(sFileText));
		    oDocument = oDocumentBuilder.parse(oInputSource);
		    oDocument.getDocumentElement().normalize();
		}
		catch (SciException e)
		{
			log.error("Error in reading <"+sStagedFileName+"> file");
			assertTrue(false);
		} catch (IOException e) 
		{
			log.error("Error in reading <"+sStagedFileName+"> file");
			assertTrue(false);
		} catch (ParserConfigurationException e) 
		{
			log.error("Error in parsing <"+sStagedFileName+"> file");
			assertTrue(false);
		} catch (SAXException e) 
		{
			log.error("Error in parsing <"+sStagedFileName+"> file");
			assertTrue(false);
		}
		
//		assertEquals (oEvalRule.evalute(mapRuntimeValues),bTestRecordExpectedResult );
	}
	


	@BeforeClass
	@Parameters("path")
	public void getfilePath(String path)
	{
		this.path=path;
	}
	
	@DataProvider
	public Object[][] getParamValuesFromExcel() throws Exception
	{
		Object[][] paramArray = readParameters();   //Read from CVS file
		return (paramArray);
 	}
	
	public Object[][] readParameters() throws Exception
	{
        List<Object[]> parameterList = new ArrayList<Object[]>();
        Object[][] parameters= null;//new Object[4][1];
        int nCounter = 0;
		String csvFile =  "src\\test\\testng\\" + this.path;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "##";
        try {
        	
        	 br = new BufferedReader(new FileReader(csvFile));
             while ((line = br.readLine()) != null)
             {
             	if(line.contains("pathToFiles"))
             		continue;
             	if (line.trim().length() > 0 && !line.trim().startsWith("--"))
             	{
 	               Object paramArray [] = line.split(cvsSplitBy);
 	               parameterList.add(paramArray);
             	}
             }
            
            parameters = new Object[parameterList.size()][];
            nCounter = 0;
            for (Object[] obj : parameterList)
            {
            	parameters[nCounter++] = obj;
            }
        }
        catch (FileNotFoundException e) 
        {
            e.printStackTrace();
            throw e;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if (br != null)
            {
                try 
                {
                    br.close();
                } catch (IOException e) 
                {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        return parameters;
    }
	
	

}