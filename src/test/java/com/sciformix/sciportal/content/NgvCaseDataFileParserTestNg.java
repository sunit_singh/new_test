package com.sciformix.sciportal.content;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.wink.json4j.JSONArray;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.rulesengine.IAction;
import com.sciformix.commons.rulesengine.Rule;
import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.sciportal.apps.ngv.NgvPortalApp;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.OfficeUtils;
import com.sciformix.commons.utils.WorkbookWorkSheetWrapper;


public class NgvCaseDataFileParserTestNg
{
	
	RuleSet oRuleset = null;
	Rule oRule = null;
	IAction oAction = null;
	private String path;
	private String expectedJsonRepresentation = null;
	
	private static final int PARAMINDEX_DATA_REC_ALIAS = 0;
	private static final int PARAMINDEX_RULE_EXPRESSION = 1;
	private static final int PARAMINDEX_EXPECTED_OUTPUT = 0;
	
	
	@Test(dataProvider="getParamValuesFromExcel")
	/*@Parameters({"createRuleSetParameter","expectedResult","runtimeKey1", //read parameters from testing.xml
		"runtimeKey2","runtimeKey3","runtimeValue1","runtimeValue2",
		"runtimeValue3"})*/ 
	
	public void evaluateTest(Object... paramArray)
	{	
		Map<String, Object> mapRuntimeValues = null;
		WorkbookWorkSheetWrapper oWorksheetWrapper = null;
		boolean bHeaderRowPresent = false;
		int nSheetIndex = 0, nCounter = 0;
		String sColumnsToSkip= null;
		DataTable oDataTable = null;
		String sKeyValue = null;
		String[] sarrKeyValue = null;
		String sKey = null;
		String sValue = null;
		int nInputFileSizeInKB = 0;
		File oStagedFile = null;
		Properties prop = null;
		InputStream input = null;
		//Step 1: Validate if parameters are sufficient for processing
		if (paramArray.length < (PARAMINDEX_EXPECTED_OUTPUT + 1))
		{
			//ERROR
			System.out.println("Insufficient parameters");
			assertTrue(false);
		}
		
		
		
		//Step 3: Process
		bHeaderRowPresent = true;//TODO:
		nSheetIndex = 1;
		sColumnsToSkip = null;
		JSONArray jsonArray = null;
		
		String sStagedFileName = (String) paramArray[0];
		oStagedFile = new File(sStagedFileName);
		try
		{
			oDataTable = new DataTable();
			OfficeUtils.parseCsvFile(sStagedFileName, oDataTable, bHeaderRowPresent);
			
			///OfficeUtils.parseCsvFile(sStagedFileName2, oDataTable, bHeaderRowPresent);
			
		//	oDataTable = OfficeUtils.convertToDatatable(oWorksheetWrapper, sColumnsToCollapse);
			
			nCounter = paramArray.length;
			switch(nCounter)
			{
				case 4: 
							// read skip columns from property file
							prop = new Properties();
							try 
							{
								input = new FileInputStream("src\\test\\testng\\FileParsing\\config.properties");
								prop.load(input);
								sColumnsToSkip = prop.getProperty("skipColumnNames");
								if(paramArray[2].toString().equals("true"))
								{
									jsonArray = oDataTable.createJsonRepresentation(sColumnsToSkip);
									if(!jsonArray.toString().equals(paramArray[3]))
									{
										System.out.println("Parsed file output does not matched to expected output");
										assertTrue(false);
									}
								}else
								{
									jsonArray = oDataTable.createJsonRepresentation("");
								}
							}
							catch (IOException e) 
							{
									e.printStackTrace();
							}
						
							
					 	if (!jsonArray.toString().equals(paramArray[3])) 
						{
							//ERROR
							System.out.println("Parsed file output does not matched to expected output");
							assertTrue(false);
						}
						break;
				case 3:
					 if(paramArray[1].toString().trim()!="" && paramArray[2].toString().equals("false"))
						{
							sKeyValue = paramArray[1].toString().trim();
							sarrKeyValue = sKeyValue.split("=");
							sKey = sarrKeyValue[0];
							sValue = sarrKeyValue[1];
							switch(sKey)
							{
								case "columnCount":
													if(oDataTable.colCount() > Integer.parseInt(sValue))
													{
														System.out.println("Input file exceeded column count");
														assertTrue(false);
													}
													break;
								case "rowCount":
													if(oDataTable.rowCount() > Integer.parseInt(sValue))
													{
														System.out.println("Input file exceeded column count");
														assertTrue(false);
													}
													break;
								case "fileSizeInKB":	
													nInputFileSizeInKB = (int) (oStagedFile.length()/1024);
													if(nInputFileSizeInKB > Integer.parseInt(sValue))
													{
														System.out.println("File size limit exceeded");
														assertTrue(false);
													}
													break;
								case "headerPresent":
													if(oDataTable.colCount() == 0)
													{
														System.out.println("Header not present in input file");
														assertTrue(false);
													}	
											
								default:
										break;
							}
							
						}else
						{
							//ERROR
							System.out.println("Invalid input parameter");
							assertTrue(false);
						}
						break;
				default:
						break;
						
			}
		}
		
		catch (SciServiceException e)
		{
			e.printStackTrace();
			assertTrue(false);
		}
		catch (SciException e)
		{
			e.printStackTrace();
			assertTrue(false);
		}
		
//		assertEquals (oEvalRule.evalute(mapRuntimeValues),bTestRecordExpectedResult );
	}
	
	@BeforeClass
	@Parameters("path")
	public void getfilePath(String path)
	{
		this.path=path;
	}
	
	@DataProvider
	public Object[][] getParamValuesFromExcel() throws Exception
	{
		Object[][] paramArray = readParameters();   //Read from CVS file
		return (paramArray);
 	}
	
	public Object[][] readParameters() throws Exception
	{
        List<Object[]> parameterList = new ArrayList<Object[]>();
        Object[][] parameters= null;//new Object[4][1];
        int nCounter = 0;
        boolean flag = false;
		String csvFile =  "src\\test\\testng\\" + this.path;
		String sExpectedOutput = null;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "#";
        File inputFile = new File("src\\test\\testng\\FileParsing\\TestNgFileParsing.xml");
        DocumentBuilderFactory oDbFactory = null;
		DocumentBuilder oDbBuilder = null;
		Document oDocument = null;
		NodeList listOfNodes = null;
		Node node = null;
		NamedNodeMap nameNodeMap = null;
		Element eElement = null;
        try {
        	
        	
        	
        	 br = new BufferedReader(new FileReader(csvFile));
             while ((line = br.readLine()) != null)
             {
             	if(line.contains("pathToFiles"))
             		continue;
             	if (line.trim().length() > 0 && !line.trim().startsWith("--"))
             	{
 	               Object paramArray [] = line.split(cvsSplitBy);
 	               parameterList.add(paramArray);
             	}
             }
			/*oDbFactory  = DocumentBuilderFactory.newInstance();
			oDbBuilder = oDbFactory.newDocumentBuilder();
			oDocument = oDbBuilder.parse(inputFile);
			oDocument.getDocumentElement().normalize();
			listOfNodes = oDocument.getElementsByTagName("parameter");
			for(int i=0;i<listOfNodes.getLength();i++)
			{
				node = listOfNodes.item(i);
				nameNodeMap = node.getAttributes();
				eElement = (Element) node;
				for(int j=0; j<nameNodeMap.getLength(); j++)
				{
					if(nameNodeMap.item(j).toString().equals("name="+'"'+this.expectedJsonRepresentation+'"'))
					{	
						sExpectedOutput = node.getTextContent();
						System.out.println(nameNodeMap.item(j)+"=>"+node.getTextContent());
						flag = true;
						break;
					}
				}
				if(flag)
					break;
			}*/
          
	      
            
            parameters = new Object[parameterList.size()][];
            nCounter = 0;
            for (Object[] obj : parameterList)
            {
            	parameters[nCounter++] = obj;
            }
        }
        catch (FileNotFoundException e) 
        {
            e.printStackTrace();
            throw e;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if (br != null)
            {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        return parameters;
    }
}
