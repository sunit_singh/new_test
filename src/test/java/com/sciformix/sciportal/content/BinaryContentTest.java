package com.sciformix.sciportal.content;

import com.sciformix.sciportal.SciPortal;

public class BinaryContentTest
{
	
	public static void main(String[] sarrArgs)
	{
		int nId = -1;
		
		SciPortal.init();
		
		try
		{
			BinaryContent oContent = BinaryContent.createNew("text-template-ftl", "TEST-HARNESS", "TEST", "Shinzo Abe said he wanted to build trust and work together for prosperity and world peace, before leaving for his trip. The meeting in New York comes amid concern over the foreign policy direction of Tokyos biggest ally. Mr Trump has said Japan needs to pay more to maintain US troops on its soil. He also condemned a major trade deal struck by President Obama with Japan and other Pacific Rim countries. The US and Japan have been key allies since the end of World War Two, when the US helped Japan rebuild its economy.");
			
//			BinaryContentHome.saveContent(oContent);
//			nId = oContent.getContentSeqId();
//			
//			oContent.setContentPurpose("ABC");
//			oContent.setContent("Chinas constitution allows independent candidates to run in local elections but it is a futile exercise for those who dare to get their names on the ballot. In the heart of Beijing alleyways, the polling station is bathed in autumn sunlight. It is busy. An elderly man has turned up with his wife on the back of his tricycle. Three nurses, clutching their voter registration certificates, arrive on foot in deep conversation while election officials and policemen oversee the whole affair. On the surface, it is a scene that would be recognisable to voters in democratic countries the world over. But of course, this is China and the reality is very different. We drive to the outskirts of Beijing to meet Liu Huizhen, a 45-year-old woman who wants nothing more than the right to take part in this election.");
//			BinaryContentHome.saveContent(oContent);
//			
//			oContent = BinaryContentHome.readContent(nId);
//			System.out.println("Content read from DB is:");
//			System.out.println("\t" + oContent.getContent());
		}
		catch (Exception e)
		{
			System.out.println("Error - " + e);
		}
		
	}

}
