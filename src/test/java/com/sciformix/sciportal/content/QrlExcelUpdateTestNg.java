package com.sciformix.sciportal.content;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.sciformix.commons.rulesengine.IAction;
import com.sciformix.commons.rulesengine.Rule;
import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.sciportal.apps.ngv.NgvPortalApp;
import com.sciformix.sciportal.config.ConfigHome;
	

public class QrlExcelUpdateTestNg
{
	
	RuleSet oRuleset = null;
	Rule oRule = null;
	IAction oAction = null;
	private String path;
	private static final int PARAMINDEX_EXPECTED_OUTPUT = 0;
	static int nNumberOfDateFormats = 6;
	private static final Logger log = LoggerFactory.getLogger(QrlExcelUpdateTestNg.class);
	
	@org.testng.annotations.Test(dataProvider="getParamValuesFromExcel")
	
	public void evaluateTest(Object... paramArray)
	{	
		
		String[] arrTemp = null;
		String sQrName = null;
		String sQrCaseNumber = null;
		String sQrDate = null;
		List<String> listOfNameRanges = null;
		Sheet oSheet = null;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;
		Workbook oWorkbook = null;
		FileInputStream inputStream = null;
		Name oNamedCell = null;
		AreaReference oAreaReference = null;
		CellReference[] oCellReference = null;
		int namedCellIndex;
		boolean bStartAddingValueFlag = false;
		listOfNameRanges = new ArrayList<>();
		listOfNameRanges.add("QR_NAME");
		listOfNameRanges.add("QR_CASE_NUMBER");
		listOfNameRanges.add("QR_DATE");
		sQrName = paramArray[1].toString().split("\\.")[1];
		sQrCaseNumber = paramArray[2].toString().split("\\.")[1];
		sQrDate = paramArray[3].toString().split("\\.")[1];
		updateExcel(sQrName, sQrCaseNumber, sQrDate);
		try
		{
			inputStream = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\test\\testng\\FileParsing\\reviewTemplate.xlsx"));
			oWorkbook = WorkbookFactory.create(inputStream);
			oSheet = oWorkbook.getSheetAt(1);
			oIteratorWorkbookRows = oSheet.rowIterator();
			
			for(int nNum = 0; nNum < listOfNameRanges.size(); nNum++ )
			{
				oIteratorWorkbookRows.next();
				namedCellIndex = oWorkbook.getNameIndex(listOfNameRanges.get(nNum));
				oNamedCell = oWorkbook.getNameAt(namedCellIndex);
				oAreaReference = new AreaReference(oNamedCell.getRefersToFormula(),null);
				oCellReference = oAreaReference.getAllReferencedCells();
				for (int nIndex=0; nIndex<oCellReference.length; nIndex++) 
				{
				    oSheet = oWorkbook.getSheet(oCellReference[nIndex].getSheetName());
				    oSingleWorkbookRow = oSheet.getRow(oCellReference[nIndex].getRow());
				    oSingleWorkbookCell = oSingleWorkbookRow.getCell(oCellReference[nIndex].getCol());
				 
				}
				
				switch(nNum)
				{
					case 0: if(!(oSingleWorkbookCell!=null && oSingleWorkbookCell.toString().equalsIgnoreCase(sQrName)))
							{
								System.out.println("Invalid QR Name");
								assertTrue(false);
							
							}
							break;
					case 1: if(!(oSingleWorkbookCell!=null && oSingleWorkbookCell.toString().equalsIgnoreCase(sQrCaseNumber)))
					{
						System.out.println("Invalid QR Case Number");
						assertTrue(false);
						
					}
							break;
					case 2: if(!(oSingleWorkbookCell!=null && oSingleWorkbookCell.toString().equalsIgnoreCase(sQrDate)))
					{
						System.out.println("Invalid QR Date");
						assertTrue(false);
						
					}
							break;
					default:break;
				}
			}
		
			inputStream.close();
		}catch(Exception e)
		{
			assertTrue(false);
		}
		
	}
	


	@BeforeClass
	@Parameters("path")
	public void getfilePath(String path)
	{
		this.path=path;
	}
	
	@DataProvider
	public Object[][] getParamValuesFromExcel() throws Exception
	{
		Object[][] paramArray = readParameters();   //Read from CVS file
		return (paramArray);
 	}
	
	public Object[][] readParameters() throws Exception
	{
        List<Object[]> parameterList = new ArrayList<Object[]>();
        Object[][] parameters= null;//new Object[4][1];
        int nCounter = 0;
		String csvFile =  "src\\test\\testng\\" + this.path;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "##";
        try {
        	
        	 br = new BufferedReader(new FileReader(csvFile));
             while ((line = br.readLine()) != null)
             {
             	if(line.contains("pathToFiles"))
             		continue;
             	if (line.trim().length() > 0 && !line.trim().startsWith("--"))
             	{
 	               Object paramArray [] = line.split(cvsSplitBy);
 	               parameterList.add(paramArray);
             	}
             }
            
            parameters = new Object[parameterList.size()][];
            nCounter = 0;
            for (Object[] obj : parameterList)
            {
            	parameters[nCounter++] = obj;
            }
        }
        catch (FileNotFoundException e) 
        {
            e.printStackTrace();
            throw e;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if (br != null)
            {
                try 
                {
                    br.close();
                } catch (IOException e) 
                {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        return parameters;
    }
	
	public static void updateExcel(String p_sQrName, String p_sQrCaseNumber, String p_sQrDate) 
	{
		List<String> listOfNameRanges = null;
		Sheet oSheet = null;
		Iterator<Row> oIteratorWorkbookRows = null;
		Row oSingleWorkbookRow = null;
		Cell oSingleWorkbookCell = null;
		Workbook oWorkbook = null;
		FileInputStream inputStream = null;
		Name oNamedCell = null;
		AreaReference oAreaReference = null;
		CellReference[] oCellReference = null;
		int namedCellIndex;
		boolean bStartAddingValueFlag = false;
		listOfNameRanges = new ArrayList<>();
		listOfNameRanges.add("QR_NAME");
		listOfNameRanges.add("QR_CASE_NUMBER");
		listOfNameRanges.add("QR_DATE");
		try
		{
			inputStream = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\test\\testng\\FileParsing\\reviewTemplate.xlsx"));
			oWorkbook = WorkbookFactory.create(inputStream);
			oSheet = oWorkbook.getSheetAt(1);
			oIteratorWorkbookRows = oSheet.rowIterator();
			
			for(int nNum = 0; nNum < listOfNameRanges.size(); nNum++ )
			{
				oIteratorWorkbookRows.next();
				namedCellIndex = oWorkbook.getNameIndex(listOfNameRanges.get(nNum));
				oNamedCell = oWorkbook.getNameAt(namedCellIndex);
				oAreaReference = new AreaReference(oNamedCell.getRefersToFormula(),null);
				oCellReference = oAreaReference.getAllReferencedCells();
				for (int nIndex=0; nIndex<oCellReference.length; nIndex++) 
				{
				    oSheet = oWorkbook.getSheet(oCellReference[nIndex].getSheetName());
				    oSingleWorkbookRow = oSheet.getRow(oCellReference[nIndex].getRow());
				    oSingleWorkbookCell = oSingleWorkbookRow.getCell(oCellReference[nIndex].getCol());
				    if(nNum == 0)
				    	oSingleWorkbookCell.setCellValue(p_sQrName);
				    if(nNum == 1)
				    	oSingleWorkbookCell.setCellValue(p_sQrCaseNumber);
				    if(nNum == 2)
				    	oSingleWorkbookCell.setCellValue(p_sQrDate);
				 
				}
			}
			while(oIteratorWorkbookRows.hasNext())
			{
				oSingleWorkbookRow = oIteratorWorkbookRows.next();
				if(oSingleWorkbookRow!=null)
				{
					for (int nColumnCellIndex = 0; nColumnCellIndex < oSingleWorkbookRow.getLastCellNum(); nColumnCellIndex++)
					{
						oSingleWorkbookCell = oSingleWorkbookRow.getCell(nColumnCellIndex);
						if(bStartAddingValueFlag)
						{
							// start inserting value 
						}
						if(getWorkbookCellContent(oSingleWorkbookCell)!=null && getWorkbookCellContent(oSingleWorkbookCell).toString().equalsIgnoreCase("field"))
						{
							bStartAddingValueFlag = true;
							break;
						}	
						
					}
				}
			}
			 inputStream.close();
            FileOutputStream outputStream = new FileOutputStream(System.getProperty("user.dir")+"\\src\\test\\testng\\FileParsing\\reviewTemplate.xlsx");
            oWorkbook.write(outputStream);
            oWorkbook.close();
            outputStream.close();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	private static Object getWorkbookCellContent(Cell oSingleWorkbookCell)
	{
		boolean bCheckIfCellContainsDateValue = false;
		DataFormatter df = new DataFormatter(true);
	
		if (oSingleWorkbookCell!=null)
		{
			switch (oSingleWorkbookCell.getCellType()) 
			{
				case Cell.CELL_TYPE_BLANK:
					return oSingleWorkbookCell.getStringCellValue();
				case Cell.CELL_TYPE_STRING:
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_NUMERIC:
					bCheckIfCellContainsDateValue = checkIfCellContainsDateValue(df.formatCellValue(oSingleWorkbookCell));
					if(bCheckIfCellContainsDateValue)
					{
						System.out.println(df.formatCellValue(oSingleWorkbookCell));
						return getActualFormattedDateValue(df.formatCellValue(oSingleWorkbookCell));
					}
					else
					{
						oSingleWorkbookCell.setCellType(Cell.CELL_TYPE_STRING);
					}
					return oSingleWorkbookCell.getStringCellValue().trim();
				case Cell.CELL_TYPE_BOOLEAN:
					return oSingleWorkbookCell.getBooleanCellValue();
				case Cell.CELL_TYPE_FORMULA:
					return oSingleWorkbookCell.getCellFormula();
				case Cell.CELL_TYPE_ERROR:
					return null;
				default:
					return new String("");
			}
		}
		else
		{
			return null;
		}
	}

	private  static String getActualFormattedDateValue(String p_sDateValue) 
	{
		String sIncommingDateFormat = null;
		String sFormattedDate = null;
		String sDateFormat = null;
		SimpleDateFormat incomingformatter = null;
		Date oFormattedDate = null;
		SimpleDateFormat formatter = null;
		
		if(p_sDateValue!=null)
		{
			sIncommingDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString());
			sDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.FILE_COLUMN_J_J_AWARE_DATE_FORMAT.toString());
			//TODO: Check this
			try
			{
				incomingformatter = new SimpleDateFormat(sIncommingDateFormat);
				oFormattedDate = incomingformatter.parse(p_sDateValue);
			}
			catch(ParseException parseExcep)
			{
				return "";
			}
			
			formatter = new SimpleDateFormat(sDateFormat);
			sFormattedDate = formatter.format(oFormattedDate);
		}
		else
		{
			return "";
		}
		return sFormattedDate;
	}
	private static boolean checkIfCellContainsDateValue(String p_sColumnValue) 
	{
		String sIncomingDateFormat = null;
		
		try
		{
			sIncomingDateFormat = ConfigHome.getAppConfigSetting(NgvPortalApp.AppConfig.INCOMING_DATE_FORMAT_XLS.toString());
			
			@SuppressWarnings("deprecation")
			//TODO: Check this
			SimpleDateFormat formatter = new SimpleDateFormat(sIncomingDateFormat);
			formatter.parse(p_sColumnValue);
		}
		catch(Exception excep)
		{
			return false;
		}
		return true;
	}

}
