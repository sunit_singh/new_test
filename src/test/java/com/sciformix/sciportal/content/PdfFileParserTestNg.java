package com.sciformix.sciportal.content;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.wink.json4j.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.rulesengine.IAction;
import com.sciformix.commons.rulesengine.Rule;
import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.file.parsers.IrpcOneCaseDataFileParser;
import com.sciformix.commons.utils.DataTable;
import com.sciformix.commons.utils.FileUtils;
import com.sciformix.commons.utils.PdfUtils;
	

public class PdfFileParserTestNg
{
	
	RuleSet oRuleset = null;
	Rule oRule = null;
	IAction oAction = null;
	private String path;
	private static final int PARAMINDEX_EXPECTED_OUTPUT = 0;
	
	private static final Logger log = LoggerFactory.getLogger(PdfFileParserTestNg.class);
	
	@Test(dataProvider="getParamValuesFromExcel")
	/*@Parameters({"createRuleSetParameter","expectedResult","runtimeKey1", //read parameters from testing.xml
		"runtimeKey2","runtimeKey3","runtimeValue1","runtimeValue2",
		"runtimeValue3"})*/ 
	
	public void evaluateTest(Object... paramArray)
	{	
		
		DataTable oDataTable = null;
		//Step 1: Validate if parameters are sufficient for processing
		if (paramArray.length < (PARAMINDEX_EXPECTED_OUTPUT + 1))
		{
			//ERROR
			System.out.println("Insufficient parameters");
			assertTrue(false);
		}
		
		JSONArray jsonArray = null;
		String sFileContents = null;
		
		String sStagedFileName = (String) paramArray[0];
		IrpcOneCaseDataFileParser oCaseDataParser = null;
		String sFileFomat = "";
		
		sFileFomat = "IRPC-ONE";
		
		try
		{
			oDataTable = new DataTable();
			
			
			if (FileUtils.isPdfFile(sStagedFileName))
			{
				sFileContents = PdfUtils.getFileText(sStagedFileName);
			}
			else if (FileUtils.isTxtFile(sStagedFileName))
			{
				sFileContents = FileUtils.getFileText(sStagedFileName);
			}
			else
			{
				//Error
			}
			
			if (sFileFomat.equals("IRPC-ONE"))
			{
				oCaseDataParser = new IrpcOneCaseDataFileParser();
			}
			else
			{
				//Error
			}
			oCaseDataParser.parse(sFileContents, oDataTable);
			
			///PdfUtils.parsePdfToTextFile(sStagedFileName, oDataTable, bHeaderRowPresent, sExcudeCharRegex);
			
			jsonArray = oDataTable.createJsonRepresentation("");
			if(!jsonArray.toString().equals(paramArray[3]))
			{
				log.error("Parsed file output does not matched to expected output");
				assertTrue(false);
			}
		}
		catch (SciServiceException e)
		{
			e.printStackTrace();
			assertTrue(false);
		}
	
		catch (SciException e)
		{
			e.printStackTrace();
			assertTrue(false);
		}
		
//		assertEquals (oEvalRule.evalute(mapRuntimeValues),bTestRecordExpectedResult );
	}
	
	@BeforeClass
	@Parameters("path")
	public void getfilePath(String path)
	{
		this.path=path;
	}
	
	@DataProvider
	public Object[][] getParamValuesFromExcel() throws Exception
	{
		Object[][] paramArray = readParameters();   //Read from CVS file
		return (paramArray);
 	}
	
	public Object[][] readParameters() throws Exception
	{
        List<Object[]> parameterList = new ArrayList<Object[]>();
        Object[][] parameters= null;//new Object[4][1];
        int nCounter = 0;
		String csvFile =  "src\\test\\testng\\" + this.path;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "##";
        try {
        	
        	
        	
        	 br = new BufferedReader(new FileReader(csvFile));
             while ((line = br.readLine()) != null)
             {
             	if(line.contains("pathToFiles"))
             		continue;
             	if (line.trim().length() > 0 && !line.trim().startsWith("--"))
             	{
 	               Object paramArray [] = line.split(cvsSplitBy);
 	               parameterList.add(paramArray);
             	}
             }
            
            parameters = new Object[parameterList.size()][];
            nCounter = 0;
            for (Object[] obj : parameterList)
            {
            	parameters[nCounter++] = obj;
            }
        }
        catch (FileNotFoundException e) 
        {
            e.printStackTrace();
            throw e;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if (br != null)
            {
                try 
                {
                    br.close();
                } catch (IOException e) 
                {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        return parameters;
    }
}
