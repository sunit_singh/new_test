package com.sciformix.sciportal.commons;

import com.sciformix.commons.utils.DataValidationUtils;

public class DataValidationUtilsTest
{
	
	private static void assertObjectNameValid(String sDescriptor, String sValue)
	{
		if (DataValidationUtils.isValidObjectName(sValue))
		{
			System.out.println(sDescriptor + ": ERROR - Value expected to be VALID: '" + sValue + "'");
		}
		else
		{
			System.out.println(sDescriptor + ": PASS - Value in indeed VALID: '" + sValue + "'");
		}
	}
	
	private static void assertObjectNameInvalid(String sDescriptor, String sValue)
	{
		if (DataValidationUtils.isValidObjectName(sValue))
		{
			System.out.println(sDescriptor + ": ERROR - Value expected to be INVALID: '" + sValue + "'");
		}
		else
		{
			System.out.println(sDescriptor + ": PASS - Value in indeed INVALID: '" + sValue + "'");
		}
	}
	
	public static void main(String[] args)
	{
		String sObjectDescriptor = null;
		int index = 0;
		
		sObjectDescriptor = "#";
		index = 1;
		
		
		System.out.println("Section 1 - Values expected to be VALID");
		assertObjectNameValid(sObjectDescriptor + (index++), "abc");
		assertObjectNameValid(sObjectDescriptor + (index++), "a_c");
		assertObjectNameValid(sObjectDescriptor + (index++), "aSDSDSDSDc");
		assertObjectNameValid(sObjectDescriptor + (index++), "a9c");
		assertObjectNameValid(sObjectDescriptor + (index++), "000");
		assertObjectNameValid(sObjectDescriptor + (index++), "a c");
		assertObjectNameValid(sObjectDescriptor + (index++), "Project One");
		assertObjectNameValid(sObjectDescriptor + (index++), "Template-v1");
		assertObjectNameValid(sObjectDescriptor + (index++), "Template - version 1");
		assertObjectNameValid(sObjectDescriptor + (index++), "Template - version 2 has a very very long name");
		assertObjectNameValid(sObjectDescriptor + (index++), "a-c");
		
		System.out.println();
		System.out.println();
		System.out.println("Section 2 - Values expected to be INVALID");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "a");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "D");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "0");
		
		assertObjectNameInvalid(sObjectDescriptor + (index++), "_c");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "_abc");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "abc_");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "abc9 ");
	
		assertObjectNameInvalid(sObjectDescriptor + (index++), "a.c");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "a@v");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "~");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "aasd8989!0");
		assertObjectNameInvalid(sObjectDescriptor + (index++), "_+");
	}

}
