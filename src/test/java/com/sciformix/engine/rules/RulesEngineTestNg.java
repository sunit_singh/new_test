package com.sciformix.engine.rules;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sciformix.commons.SciException;
import com.sciformix.commons.rulesengine.IAction;
import com.sciformix.commons.rulesengine.Rule;
import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.commons.rulesengine.RuleSetUtils;


public class RulesEngineTestNg
{
	
	RuleSet oRuleset = null;
	Rule oRule = null;
	IAction oAction = null;
	private String path;
	
	private static final int PARAMINDEX_DATA_REC_ALIAS = 0;
	private static final int PARAMINDEX_RULE_EXPRESSION = 1;
	private static final int PARAMINDEX_EXPECTED_OUTPUT = 2;
	
	
	@Test(dataProvider="getParamValuesFromExcel")
	/*@Parameters({"createRuleSetParameter","expectedResult","runtimeKey1", //read parameters from testing.xml
		"runtimeKey2","runtimeKey3","runtimeValue1","runtimeValue2",
		"runtimeValue3"})*/ 
	
	public void evaluateTest(Object... paramArray)
	{	
		String sRuleExpression = null;
		boolean bTestRecordExpectedResult= false;
		Map<String, Object> mapRuntimeValues = null;
		
		//Step 1: Validate if parameters are sufficient for processing
		if (paramArray.length < (PARAMINDEX_EXPECTED_OUTPUT + 1))
		{
			//ERROR
			System.out.println("Insufficient parameters");
			assertTrue(false);
		}
		/*else if ((paramArray.length - PARAMINDEX_EXPECTED_OUTPUT - 1) % 2 > 0) 
		{
			//ERROR
			System.out.println("Unbalanced runtime value count");
			assertTrue(false);
		}*/
		
		
		System.out.println("Record " + paramArray[PARAMINDEX_DATA_REC_ALIAS] + " has expression " + paramArray[PARAMINDEX_RULE_EXPRESSION]);
		
		
		//Step 2: Extract Parameters for processing
		sRuleExpression = ((String)paramArray[PARAMINDEX_RULE_EXPRESSION]).trim();
		bTestRecordExpectedResult= ((String)paramArray[PARAMINDEX_EXPECTED_OUTPUT]).trim().contains("TRUE");
		
		mapRuntimeValues = new HashMap<String, Object>();
		//DEV-NOTE: Index incremented by 2
		for(int nRuntimeValueIndex = (PARAMINDEX_EXPECTED_OUTPUT+1); (nRuntimeValueIndex + 1)< paramArray.length; nRuntimeValueIndex += 2)
		{
			 mapRuntimeValues.put(((String)paramArray[nRuntimeValueIndex]).trim(), ((String)paramArray[nRuntimeValueIndex+1]).trim());
		}
		
		
		//Step 3: Process
		try
		{
			oRuleset = RuleSetUtils.createRuleSetFromCriteria(sRuleExpression);
		}
		catch (SciException e)
		{
			e.printStackTrace();
			assertTrue(false);
		}
		
		for (Rule oEvalRule : oRuleset)
		{
			try
			{
				oEvalRule.evalute(mapRuntimeValues);
				assertEquals (oEvalRule.evalute(mapRuntimeValues),bTestRecordExpectedResult );
			}
			catch(SciException sciExcep)
			{
				sciExcep.printStackTrace();
				assertTrue(false);
			}
		}
	}
	
	@BeforeClass
	@Parameters({"path"})
	public void getfilePath(String path)
	{
		this.path=path;
	}
	
	@DataProvider
	public Object[][] getParamValuesFromExcel() throws Exception
	{
		Object[][] paramArray = readParameters();   //Read from CVS file
		return (paramArray);
 	}
	
	public Object[][] readParameters() throws Exception
	{
        List<Object[]> parameterList = new ArrayList<Object[]>();
        Object[][] parameters= null;//new Object[4][1];
        int nCounter = 0;
		
		String csvFile = "src\\test\\testng\\" + this.path;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        
        try {
        	System.out.println("Locating file '" + csvFile + "'");
        	
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null)
            {
            	if(line.contains("IN_CSV_LIST") || line.contains("BETWEEN"))
            		cvsSplitBy = "#";
            	if (line.trim().length() > 0 && !line.trim().startsWith("--"))
            	{
	               Object paramArray [] = line.split(cvsSplitBy);
	               parameterList.add(paramArray);
            	}
            }
            
            parameters = new Object[parameterList.size()][];
            nCounter = 0;
            for (Object[] obj : parameterList)
            {
            	parameters[nCounter++] = obj;
            }
            
        }
        catch (FileNotFoundException e) 
        {
            e.printStackTrace();
            throw e;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            if (br != null)
            {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        return parameters;
    }
}
