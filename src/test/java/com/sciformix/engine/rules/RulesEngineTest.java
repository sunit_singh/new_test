package com.sciformix.engine.rules;

import java.util.HashMap;
import java.util.Map;

import org.apache.wink.json4j.JSONException;

import com.sciformix.commons.SciException;
import com.sciformix.commons.rulesengine.Criterion;
import com.sciformix.commons.rulesengine.IAction;
import com.sciformix.commons.rulesengine.Rule;
import com.sciformix.commons.rulesengine.RuleSet;
import com.sciformix.sciportal.apps.ngv.UseNarrativeTemplate;

public class RulesEngineTest
{
	
	public static void main (String[] sarr)
	{
		RuleSet oRuleset = null;
		Rule oRule = null;
		IAction oAction = null;
		
		
		
		oRuleset = new RuleSet();
		
		oAction = new UseNarrativeTemplate("Template_001.docx");
		oRule = new Rule(oAction);
		oRule.add(Criterion.create(Criterion.Operator.MATCHES, "$patient_gender", "F"));
		oRule.add(Criterion.create(Criterion.Operator.MATCHES, "$reg_class", "DEVICE"));
		oRule.add(Criterion.create(Criterion.Operator.NOT_EQUALS, "$patient_age", "18"));
		oRule.add(Criterion.create(Criterion.Operator.MATCHES, "$patient_country", "USA"));
		oRuleset.add(oRule);
		System.out.println();
		
		oAction = new UseNarrativeTemplate("Template_002.docx");
		oRule = new Rule(oAction);
		oRule.add(Criterion.create(Criterion.Operator.MATCHES, "$patient_gender", "F"));
		oRule.add(Criterion.create(Criterion.Operator.MATCHES, "$reg_class", "DEVICE"));
		oRule.add(Criterion.create(Criterion.Operator.NOT_EQUALS, "$patient_age", "18"));
		oRule.add(Criterion.create(Criterion.Operator.DOES_NOT_MATCH, "$patient_country", "USA"));
		oRuleset.add(oRule);
		System.out.println();
		
		oAction = new UseNarrativeTemplate("Template_003.docx");
		oRule = new Rule(oAction);
		oRule.add(Criterion.create(Criterion.Operator.MATCHES, "$patient_gender", "F"));
		oRuleset.add(oRule);
		
		
		System.out.println("Ruleset is:");
		System.out.println(oRuleset.print());
		
		try
		{	
			System.out.println("Ruleset (Json) is:");
			System.out.println(oRuleset.convertToJson());
			System.out.println();
			//System.out.println(oRuleset.convertToJson().get("RULES"));
		}
		catch(JSONException jsonExcep)
		{
			System.err.println("Error creating Json");
		}
		System.out.println();
		
		System.out.println("--- Evaluation time ---");
		Map<String, Object> mapRuntimeValues = null;
		int nRuleSerialNumber = 0;
		
		mapRuntimeValues = new HashMap<String, Object>();
		mapRuntimeValues.put("patient_gender", "F");
		mapRuntimeValues.put("reg_class", "DEVICE");
		mapRuntimeValues.put("patient_age", "99");
		mapRuntimeValues.put("patient_country", "INDIA");
		
		try
		{
			for (Rule oEvalRule : oRuleset)
			{
				nRuleSerialNumber++;
				if (oEvalRule.evalute(mapRuntimeValues))
				{
					System.out.println("Rule #" + nRuleSerialNumber + " has evaluated to TRUE, Action to take: " + oEvalRule.getAction().convertToString());
				}
				else
				{
					System.out.println("Rule #" + nRuleSerialNumber + " has evaluated to FALSE");
				}
			}
		}
		catch(SciException sciExcep)
		{
			sciExcep.printStackTrace();
		}
	}

}
