package com.sciformix;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JFrame;

import com.sciformix.client.pat.commons.CpatToolBase;
import com.sciformix.client.pat.commons.CpatToolHome;
import com.sciformix.client.pat.commons.IAction;
import com.sciformix.client.pat.commons.screens.CpatCasePreviewScreen;
import com.sciformix.client.pat.toolb.CpatToolBConstants;
import com.sciformix.client.pat.toolb.CpatToolBHelper;
import com.sciformix.client.pat.toolb.ParsedDataWithConfiguration;
import com.sciformix.client.pat.toolb.PreviewCaseDoc;
import com.sciformix.commons.SciConstants;
import com.sciformix.commons.SciEnums.CPAT_ACTION;
import com.sciformix.commons.SciException;
import com.sciformix.commons.SciServiceException;
import com.sciformix.commons.SciStartupLogger;
import com.sciformix.commons.file.FileConfigurationHome;
import com.sciformix.commons.file.FileParsingConfiguration;
import com.sciformix.commons.file.FileParsingConfiguration.FileParsingField;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarAA;
import com.sciformix.commons.utils.ParsedDataTable;

public class TestCpatPreviewScreen extends CpatToolBase implements IAction 
{

	protected TestCpatPreviewScreen(String p_sToolName, String p_sToolDisplayName, String p_sToolVersion,
			String p_sUserInputsScreenFile) 
	{
		super(p_sToolName, p_sToolDisplayName, p_sToolVersion, p_sUserInputsScreenFile);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void loadCpatFieldMap() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> validateUserInputs(Map<String, String> mapExtractedParameters) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void userInputsScreenEventHandler(CpatToolBase oToolBase, IAction oCallbackAction, Component comp,
			JFrame frame) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void performAction(CPAT_ACTION p_eAction, Object... oParameters) {
		// TODO Auto-generated method stub
		
	}

	public static void main(String args[]) throws SciException, SciServiceException
	{
		List<PreviewCaseDoc> listPreviewCaseDoc = null;
		Map<String, Object> params = null;
		
		TestCpatPreviewScreen oScreen = null;
		
		
		SciStartupLogger oStartupLogger = SciStartupLogger.getLogger();
		FileConfigurationHome.init(oStartupLogger, "AA_Parser,BA_Parser");
		
		oScreen = new TestCpatPreviewScreen("Cpat", "Cpat", "V0", null);
		
		params = new HashMap<>();
		
		listPreviewCaseDoc = new ArrayList<>();
		
		//TestCpatPreviewScreen obj = new TestCpatPreviewScreen(p_sToolName, p_sToolDisplayName, p_sToolVersion, p_sUserInputsScreenFile)
		
		String s0 = "E:\\Projects\\Selenium\\IcePdf\\SFE3324469.pdf";
		String s1 = "E:\\Projects\\Selenium\\IcePdf\\SFE3328924.pdf";
		String s2 = "E:\\Projects\\Selenium\\IcePdf\\SFE3333315.pdf";
		
		File f0 = new File(s0);
		File f1 = new File(s1);
		File f2 = new File(s2);
		
		
		
		ParsedDataTable pdt1 = new PdfDocParserAvatarAA(s0).parse();
		ParsedDataTable pdt2 = new PdfDocParserAvatarAA(s1).parse();
		ParsedDataTable pdt3 = new PdfDocParserAvatarAA(s2).parse();
		
		PreviewCaseDoc p1 = new PreviewCaseDoc(f0, pdt1, "1");
		PreviewCaseDoc p2 = new PreviewCaseDoc(f1, pdt2, "2");
		PreviewCaseDoc p3 = new PreviewCaseDoc(f2, pdt3, "3");
		
		listPreviewCaseDoc.add(p1);
		listPreviewCaseDoc.add(p2);
		listPreviewCaseDoc.add(p3);
		
		ParsedDataTable summaryDT0 = CpatToolBHelper.mergeAllCaseDocs(listPreviewCaseDoc);
		
		List<String> followUpList = null;
		
		followUpList = oScreen.getFollowUpMnemonicList(listPreviewCaseDoc, "3");
		
		/*followUpList = new ArrayList<>();
		followUpList.add("Reporter_Qualification");
		followUpList.add("Sender_GivenName");
		followUpList.add("Receiver_Type");
		followUpList.add("Receiver_Organization");
		
		followUpList.add("Reaction_ReactionEventMedDRATermLLT");
		followUpList.add("Reaction_ReactionLastTime");
		followUpList.add("Drug_MedicinalProductName");
		followUpList.add("Drug_RouteOfAdministration");
		
		followUpList.add("Drug_DidReactionRecurOnReadministration");
		followUpList.add("Narrative_ProductUseFrequency");
		followUpList.add("Narrative_ProductStopDate");
		followUpList.add("Narrative_ReactionStartDate");
		
		followUpList.add("Narrative_DidChildHaveUnfavorableEvent");
		followUpList.add("Sender_Telno");
		followUpList.add("Reporter_LiteratureReferences");
		followUpList.add("ReasonForNullification");
		
		followUpList.add("SenderIdentifier");
		followUpList.add("OccurCountry");
		followUpList.add("DateOfRecentInfo");
		followUpList.add("CausedProlongedHospitalization");
		
		followUpList.add("CompanyNumber");
		followUpList.add("Receiver_FamilyName");
		followUpList.add("Receiver_Country");
		followUpList.add("Receiver_Emailaddress");*/
		
		params.put(CpatToolBConstants.ContentParams.CASE_DOC_LIST, listPreviewCaseDoc);
		params.put(CpatToolBConstants.ContentParams.CASE_SUMMARY_DATATABLE, summaryDT0);
		params.put(CpatToolBConstants.ContentParams.CASE_FILE_TYPE, "E2B");
		params.put("followUpFieldList", followUpList);
		
		
		CpatCasePreviewScreen oPreviewScreen = new CpatCasePreviewScreen(oScreen, oScreen, params);
	}

	
	private  List<String> getFollowUpMnemonicList(List<PreviewCaseDoc> listPreviewCaseDocs, String version)
	{
		Map<String, String> propMap = null;
		List<String> followUpMnemonicList = null;
		List<PreviewCaseDoc> currentDocList = null;
		List<PreviewCaseDoc> previousDocList = null;
		ParsedDataTable currentSummaryDataTable = null;
		ParsedDataTable previousSummaryDataTable = null;
		FileParsingConfiguration oParsingConfiguration = null;
		int caseVersion = 0,
			currentVersion = 0;
		int maxCount = 1;
		int keyCount = 1;
		currentVersion = Integer.parseInt(version);
		propMap = CpatToolHome.loadProperties("ConfigForSceptre.properties");
		String[] collationMnemonic = propMap.get("FollowUpCollationKeys").split(SciConstants.StringConstants.COMMA);
		currentDocList = new ArrayList<>();
		previousDocList = new ArrayList<>();
		
		oParsingConfiguration = FileConfigurationHome.getFileConfig(SciConstants.XMLParseConstant.XML_PARSER_AA_KEY);
		
		for(PreviewCaseDoc doc : listPreviewCaseDocs)
		{
			caseVersion = Integer.parseInt(doc.getCaseDocVersion());
			
			if(caseVersion < currentVersion)
			{
				previousDocList.add(doc);
			}
			
			if(caseVersion <= currentVersion)
			{
				currentDocList.add(doc);
			}
		}
		
		currentSummaryDataTable = CpatToolBHelper.mergeAllCaseDocs(currentDocList);
		previousSummaryDataTable =  CpatToolBHelper.mergeAllCaseDocs(previousDocList);
		
		followUpMnemonicList = new ArrayList<>();
		if(previousDocList!= null && !previousDocList.isEmpty())
		{
			for(String colMnemonic : collationMnemonic)
			{
				//Check for multiple values
				if (!oParsingConfiguration.getField(colMnemonic.trim()).isMultivalued()) 
				{
					if(currentSummaryDataTable.isColumnPresent(colMnemonic.trim()+"_Count"))
					{
						keyCount =Integer.parseInt(currentSummaryDataTable.getColumnValues(colMnemonic.trim()+"_Count").get(0).displayValue().toString());
						if(maxCount < keyCount)
						{
							maxCount = keyCount;
						}
					}
					for(int index =1; index<= maxCount; index++)
					{
						checkForCollatedDataDiff(currentSummaryDataTable, previousSummaryDataTable, colMnemonic.trim()+"_"+index, followUpMnemonicList);
					}
				}
				else {
					checkForCollatedDataDiff(currentSummaryDataTable, previousSummaryDataTable, colMnemonic.trim(), followUpMnemonicList);
				}
			}
		}
		return followUpMnemonicList;
	}
	
	private void checkForCollatedDataDiff(ParsedDataTable currentSummaryDataTable, 
	ParsedDataTable previousSummaryDataTable, String mnemonic, List<String> followUpMnemonicList )
	{
		if(previousSummaryDataTable.isColumnPresent(mnemonic) &&
				currentSummaryDataTable.isColumnPresent(mnemonic)) {
			if(!previousSummaryDataTable.getColumnValues(mnemonic).get(0).displayValue().equals
					(currentSummaryDataTable.getColumnValues(mnemonic).get(0).displayValue()))
			{
				followUpMnemonicList.add(mnemonic);
			}
		}else
		if(previousSummaryDataTable.isColumnPresent(mnemonic) ||
				currentSummaryDataTable.isColumnPresent(mnemonic))
		{
			followUpMnemonicList.add(mnemonic);
		}
	}
	@Override
	public void previewScreenEventHandler(CpatToolBase oToolBase, IAction m_oCallbackAction, Component comp,
			JFrame oWindowFrame, ParsedDataTable summaryDataTable, Map<String, String> parsedDataMap) {
		// TODO Auto-generated method stub
		
	}
}	

