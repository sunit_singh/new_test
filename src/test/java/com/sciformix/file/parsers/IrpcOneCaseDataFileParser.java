package com.sciformix.file.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sciformix.commons.SciException;
import com.sciformix.commons.utils.DataTable;

public class IrpcOneCaseDataFileParser
{
	private static final Logger log = LoggerFactory.getLogger(IrpcOneCaseDataFileParser.class);
	
	private static final String CHARACTERS_TO_SUPPRESS = "[\u2020\u0192\u2122\u00AE\u00E2\u00C2\u00A9\u20AC\u20B5\u0000\u00A2]";

	public void parse(String p_sFileContentsToParse, DataTable p_oDataTable) throws SciException
	{
		BufferedReader oBufferedReader = null;
		String sLine = null;
		String sKey = null;
		String sTempKey = null;
		String sTemp= null;
		String sValue = null;
		Pattern oPattern = null;
		Matcher oMatcher = null;
		boolean flag = true;
		
		try 
		{
			oBufferedReader = new BufferedReader(new StringReader(p_sFileContentsToParse));
			oPattern = Pattern.compile(CHARACTERS_TO_SUPPRESS);
			
			while((sLine = oBufferedReader.readLine())!=null)
			{
				if(sLine.trim().equals(""))
					continue;
				
				if(sLine.contains(":"))
				{
					sKey = sLine.substring(0, sLine.indexOf(":")).trim();
					oMatcher = oPattern.matcher(sKey);
					if(oMatcher.find())
					{
						sKey = oMatcher.replaceAll("").trim();
					}
					
					
					sValue = sLine.substring(sLine.indexOf(":")+1, sLine.length());
					if(sTempKey!=null && !sKey.equals("HCP Details") && !sKey.equals("Patient Details") && !sKey.equals("Submission Details"))
					{
							sTemp = sTempKey.trim()+"."+sKey;
							if(p_oDataTable.isColumnPresent(sTemp))
							{	
								p_oDataTable.appendColumnValue(sTemp, sValue);
							}else
							{
								p_oDataTable.addColumn(sTemp, sValue);
							}	
					}
					else
					{	
						if(p_oDataTable.isColumnPresent(sKey))
						{	
							p_oDataTable.appendColumnValue(sKey, sValue);
						}else
						{
							p_oDataTable.addColumn(sKey, sValue);
						}	
					}	
				}
				else
				{
					flag = false;
					sTempKey = null;
				}
				
				if(sKey.equals("Patient Details") || sKey.equals("HCP Details") || sKey.equals("Submission Details"))
				{
						flag = true;
						sTempKey = sKey.trim();
				}
				if( ((sKey.trim().equals("AER Description")||sKey.trim().equals("PCM Description")) && (sValue!=null && !sValue.trim().equals("") && !sValue.trim().equals("NA"))))
				{
					flag = true;
					sTempKey = sKey.trim();
				}
			}
			//log.debug("Dumping file=>"+p_sStagedFileName);
			
			
			p_oDataTable.dumpContents();
			
		}
		catch (IOException ioExcep)
		{
			log.error("Error parsing text file ", ioExcep);
			throw new SciException("Error parsing text file", ioExcep);
		}
//		catch (SciException sciExcep)
//		{
//			log.error("Error parsing text file ", sciExcep);
//			throw new SciException("Error parsing text file", sciExcep);
//		}
		finally 
		{
			try
			{
				if(oBufferedReader!=null)
				{
					oBufferedReader.close();
				}
			}
			catch (Exception ioExcep)
			{
				log.error("Error closing text file", ioExcep);
			}
		}
	}
	
}
