package com.sciformix.file.parsers;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

import com.sciformix.commons.SciException;
import com.sciformix.commons.file.IFileParser;
import com.sciformix.commons.file.parsers.PdfDocParserAvatarAA;
import com.sciformix.commons.utils.ParsedDataTable;

public class TestFileParser {

	 
	public static void main(String a[]){
		
		IFileParser oCaseDataParser = null;
		ParsedDataTable oParsedDataTable = null;
		String sKey = null;
		String sValue = null;
		String sKeyFlag = null;
	    File file = new File("F:\\testPdfRead");
	    Collection<File> collFiles = FileUtils.listFiles(file, new String[] {"pdf"}, false);
	    //File[] files = file.listFiles(new FilenameFilter() { public boolean accept(File dir, String name) {return name.toLowerCase().endsWith(".pdf");} });
	    File[] files = new File[collFiles.size()];
	    collFiles.toArray(files);
	    String[] sarrProcessingStatus = new String[files.length];
	    
	    int nFileCounter = 0;
	    
	    for(File oCurrentFile: files)
	    {
	    	sarrProcessingStatus[nFileCounter] = "";
	    	if (oCurrentFile.isDirectory())
	    	{
	    		sarrProcessingStatus[nFileCounter] = "DIRECTORY";
	    		nFileCounter++;
	    		continue;
	    	}
	    	
	    	oCaseDataParser = new PdfDocParserAvatarAA(oCurrentFile.getAbsolutePath());
	    	
	    	try {
	    		oParsedDataTable = oCaseDataParser.parse();
	    		
		    	try (BufferedWriter bw = new BufferedWriter(new FileWriter("F://testPdfRead//"+oCurrentFile.getName().replace(".pdf", ".txt")))) 
		    	{
		    		for (int nIndex = 0; nIndex < oParsedDataTable.colCount() ; nIndex++)
			        {
						sKey = oParsedDataTable.getMnemonic(nIndex);
						sValue = ""+oParsedDataTable.getColumnValues(nIndex).get(oParsedDataTable.getColumnValues(nIndex).size()-1).displayValue();
						sKeyFlag = oParsedDataTable.getColumnValues(sKey).get(0).validationState().toString();//(sValue.indexOf(":") >= 0) ? "<Additional Key Present>" : "";
						//System.out.println(sKey+"="+sValue);
						bw.write(sKey+"="+sValue+"="+sKeyFlag+"\r\n");
			        }
		    		
		    		if (oParsedDataTable.getReviewRequiredCounter() > 0)
		    		{
		    			sarrProcessingStatus[nFileCounter] = "REVIEW-REQUIRED [" + oParsedDataTable.getReviewRequiredCounter() + "]";
		    		}
		    		else
		    		{
		    			sarrProcessingStatus[nFileCounter] = "SUCCESS";
		    		}
				}
		    	catch (IOException e)
		    	{
		    		sarrProcessingStatus[nFileCounter] = "Error " + e.getMessage();
					System.out.println("Error processing file '" + oCurrentFile.getAbsolutePath() + "' - " + e.getMessage());
					e.printStackTrace();
				}
			}
	    	catch (SciException e)
	    	{
	    		sarrProcessingStatus[nFileCounter] = "Error " + e.getMessage();
				System.out.println("Error processing file '" + oCurrentFile.getAbsolutePath() + "' - " + e.getMessage());
				e.printStackTrace();
			}
	    	catch (Throwable t)
	    	{
	    		sarrProcessingStatus[nFileCounter] = "Error " + t.getMessage();
				System.out.println("Error processing file '" + oCurrentFile.getAbsolutePath() + "' - " + t.getMessage());
				t.printStackTrace();
			}
	    	
	    	nFileCounter++;
	    }
	    
	    System.out.println();
	    System.out.println("=== Failures ===");
	    for (int nFileStatusCounter = 0; nFileStatusCounter < files.length; nFileStatusCounter++)
	    {
	    	if (sarrProcessingStatus[nFileStatusCounter] != null)
	    	{
	    		if (!sarrProcessingStatus[nFileStatusCounter].equalsIgnoreCase("SUCCESS") && 
	    			!sarrProcessingStatus[nFileStatusCounter].equalsIgnoreCase("DIRECTORY"))
		    	{
		    		System.out.println("\t" + files[nFileStatusCounter] + " -->" + sarrProcessingStatus[nFileStatusCounter]);
		    	}
	    	}
	    	else
	    	{
	    		System.out.println("\t" + files[nFileStatusCounter] + " --> {NULL Status}");
	    	}
	    }
	}
	
}
	
	


	