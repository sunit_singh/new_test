package com.sciformix;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;

public class Patient {
	
	static Set<String> setOfFieldsToReplaceWithUnspecifiedIfNull = new HashSet<String>();
	static Set<String> setOfFieldsThatAreMandatory = new HashSet<String>();
		
		static Map<String, String> mapDataTypes = new HashMap<String, String>();
		static Map<String, String> mapDateFormats = new HashMap<String, String>();
		static String DEFAULT_DATE_FORMAT = "dd-MMM-yyyy";
		static
		{
			setOfFieldsThatAreMandatory.add("tracking_no");
			setOfFieldsThatAreMandatory.add("j_j_aware_dt");
			setOfFieldsThatAreMandatory.add("seruionuess");
			
			setOfFieldsToReplaceWithUnspecifiedIfNull.add("event_event_cntry");
			setOfFieldsToReplaceWithUnspecifiedIfNull.add("patnt_curr_age");
			setOfFieldsToReplaceWithUnspecifiedIfNull.add("patnt_gender");
			
			// mapDataTypes.put("j_j_aware_dt", "DATE");
			mapDateFormats.put("j_j_aware_dt", "dd-MMM-yyyy");
		}
		
		private Date j_j_aware_dt;
		private Integer patnt_curr_age;
		private String patnt_gender;
		private String patnt_country;
		private String patnt_ethn;
		private String pt_his_desc_med_his;
		private String pt_his_food_alg;
		private String concom_product_name;
		private String patnt_birth_date;
		private String drug_eff_start_date;
		Map<String, String>map;
		JSONObject jsonObject;
		JSONObject value;
		public Map<String, String> getMap() {
			return map;
		}

		public void setMap(Map<String, String> map) {
			this.map = map;
		}

		public String getDrug_eff_start_date() {
			return drug_eff_start_date;
		}

		public void setDrug_eff_start_date(String drug_eff_start_date) {
			this.drug_eff_start_date = drug_eff_start_date;
		}

		public String getPatnt_birth_date() {
			return (patnt_birth_date != null ? patnt_birth_date : "unspecified");
		}

		public String getPatnt_birth_date_raw() {
			return patnt_birth_date;
		}


		public void setPatnt_birth_date(String patnt_birth_date) {
			this.patnt_birth_date = patnt_birth_date;
		}



		public String getConcom_product_name() {
			return concom_product_name;
		}



		public void setConcom_product_name(String concom_product_name) {
			this.concom_product_name = concom_product_name;
		}



		public String getPt_his_food_alg() {
			return pt_his_food_alg;
		}



		public void setPt_his_food_alg(String pt_his_food_alg) {
			this.pt_his_food_alg = pt_his_food_alg;
		}


		public Patient()
		{
			
		}
		public Patient(Date caseReceivedDate, Integer patientAge, String patientGender, String patientCountry, String pt_his_desc_med_his, Map<String, String> map, JSONObject jsonObject)
		{
			this.j_j_aware_dt = caseReceivedDate;
			this.patnt_curr_age = patientAge;
			this.patnt_gender = patientGender;
			this.patnt_country = patientCountry;
			this.pt_his_desc_med_his = pt_his_desc_med_his;
			this.map = map;
			this.jsonObject = jsonObject;
			this.value = jsonObject; 
		}
		
		public String formattedDate(String key)
		{
			String sDateFormat = null;
			sDateFormat = mapDateFormats.get(key);
			
			if (sDateFormat == null) 
			{
				sDateFormat = DEFAULT_DATE_FORMAT;
			}
			System.out.println(sDateFormat+"==>>"+(String)this.value.get(key));
			if (this.value.get(key) == null)
			{
				return "";
			}
			else
				return getExpectedDateFormat((String)this.value.get(key), sDateFormat);
		}

		public JSONObject getValue() {
			return value;
		}

		public void setValue(JSONObject value) {
			this.value = value;
			this.jsonObject = value;
		}
		
		public JSONObject getJsonObject() {
			return jsonObject;
		}

		public void setJsonObject(JSONObject jsonObject) {
			this.jsonObject = jsonObject;
			this.value = jsonObject;
		}

		public String getPt_his_desc_med_his() {
			return pt_his_desc_med_his;
		}



		public void setPt_his_desc_med_his(String pt_his_desc_med_his) {
			this.pt_his_desc_med_his = pt_his_desc_med_his;
		}



		public Date getJ_j_aware_dt() {
			return j_j_aware_dt;
		}



		public void setJ_j_aware_dt(Date j_j_aware_dt) {
			this.j_j_aware_dt = j_j_aware_dt;
		}



		public Integer getPatnt_curr_age() {
			return patnt_curr_age;
		}



		public void setPatnt_curr_age(Integer patnt_curr_age) {
			this.patnt_curr_age = patnt_curr_age;
		}



		public String getPatnt_gender() {
			return patnt_gender;
		}



		public void setPatnt_gender(String patnt_gender) {
			this.patnt_gender = patnt_gender;
		}



		public String getPatnt_country() {
			return patnt_country;
		}



		public void setPatnt_country(String patnt_country) {
			this.patnt_country = patnt_country;
		}



		public String getPatnt_ethn() {
			return patnt_ethn;
		}



		public void setPatnt_ethn(String patnt_ethn) {
			this.patnt_ethn = patnt_ethn;
		}



		public String getExpectedDateFormat(String caseReceivedDate, String dateFormat)
		{
			System.out.println(caseReceivedDate);
			
			Date newDate=new Date(caseReceivedDate);
			
			// Date newDate = new Date(caseReceivedDate);
			
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			String formattedDate = formatter.format(newDate);
			//System.out.println(caseReceivedDate+"==>>"+dateFormat+"==>>"+formattedDate);
			return formattedDate;
		}
		
		public String getExpectedDateFormat1(String key){
			Date newDate=new Date(key);
		//	System.out.println("new date==>> "+newDate);
			// Date newDate = new Date(caseReceivedDate);
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String formattedDate = formatter.format(newDate);
			System.out.println("FOrmattted date is ==>> "+formattedDate);
			return key;
		}
		
	public static String getKeyValue(String key, Object keyValue)
	 {	
		boolean isNumeric = false;		
		boolean isDate = false;
		if(keyValue!=null)
		{
			isNumeric = checkIfNumeric(keyValue.toString());
			isDate = checkIfDateString(keyValue.toString());

			if (isNumeric) 
			{
				return getNumericValue(key, keyValue.toString());
			}
			if (isDate) 
			{
				return getDateValue(key, keyValue.toString());
			}
			return getStringValue(key, keyValue.toString());
		}
		return ""; 
	 }

	private static String getStringValue(String key, String keyValue) {
		String result = "";
		
		if((keyValue == null ||keyValue == ""||keyValue.equalsIgnoreCase("unknown"))&& setOfFieldsToReplaceWithUnspecifiedIfNull.contains(key))
		{
			return "unspecified";
		}
		else
		{
			switch(key)
			{
				case "patnt_gender":
									//result = getGenderValue(keyValue);
									result = keyValue;
									break;
				default:
						result = keyValue!=null?keyValue:result;
						break;					
			
			}
		}
		return result;
		//return keyValue;
	}

	private static String getGenderValue(String keyValue) {
		
		if(keyValue.equalsIgnoreCase("male"))
			return "him";
		if(keyValue.equalsIgnoreCase("female"))
			return "her";
		return "";
	}

	private static String getDateValue(String key, String keyValue) {
	
		return getActualFormattedDateValue(keyValue);
	}

	private static String getActualFormattedDateValue(String key) 
	{
		
		if(key!=null)
		{
			String dateFormat = null;
			// in map j_j_aware_dt name should be different
			dateFormat = mapDataTypes.get("j_j_aware_dt");
			if (dateFormat == null)
				dateFormat = DEFAULT_DATE_FORMAT;
			Date newDate = new Date(key);
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			String formattedDate = formatter.format(newDate);
			// System.out.println(caseReceivedDate+"==>>"+dateFormat+"==>>"+formattedDate);
			return formattedDate;
		}
		else
		{
			return "";
		}
		
	}

	@SuppressWarnings("unused")
	private static String getNumericValue(String key, String keyValue) {
		String result = "";
		Integer numberValue = null;
		numberValue = Integer.parseInt(keyValue);
		if(numberValue == null && setOfFieldsToReplaceWithUnspecifiedIfNull.contains(key))
		{
			return "unspecified";
		}else
		{
			switch(key)
			{
			case "patnt_curr_age": 
									// result = keyValue+" "+getPatientAgeValue(numberValue);
									   result = keyValue ;
									break;
			default:
					result = keyValue!=null?keyValue:result;
					break;
			}
		}
			
		
		return result;
	}

	private static String getPatientAgeValue(int numberValue) {
		
		if(numberValue <2)
			return "year";
		else
			return "years";
	}

	private static boolean checkIfDateString(String key) {
		try{
			Date newDate = new Date(key);
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	private static boolean checkIfNumeric(String key) {
		
		try{
			Integer.parseInt(key);
		}catch(Exception e)
		{
			return false;
		}
		
		return true;
	}
		
}
